using IO.Practiceware.Integration.HL7.Handlers;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IOIMessage = IO.Practiceware.Integration.Messaging.IMessage;
using NHapiIMessage = NHapi.Base.Model.IMessage;

[assembly: Component(typeof(AppointmentMessageHandler.HandleMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.HL7.Handlers
{
    [SupportsUnitOfWork]
    public class AppointmentMessageHandler
    {
        public class HandleMessageSubscriber
        {
            public HandleMessageSubscriber(IMessenger messenger, Func<AppointmentMessageHandler> handler)
            {
                messenger.Subscribe<HandleMessageRequest>(
                    handler: (sender, token, message) => handler().HandleMessage((HL7Message)message.Message),
                    filter: m => handler().HandlesMessage(m.Message));
            }
        }

        #region AppointmentAction enum

        public enum AppointmentAction
        {
            Schedule,
            Cancel,
            NoShow,
            CheckIn,
            CheckOut,
            None
        }

        #endregion

        private static readonly EncounterStatus[] StatusesExcludedFromCancel = (
                                                                                     from i in Enums.GetValues<EncounterStatus>()
                                                                                     where i >= EncounterStatus.ExamRoom && i <= EncounterStatus.Discharged
                                                                                     select i).ToArray();

        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<IPracticeRepository> _practiceRepository;

        public AppointmentMessageHandler(
            Func<IPracticeRepository> practiceRepository,
            Func<IExternalSystemService> externalSystemService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
        }

        [UnitOfWork(AcceptChanges = true, AutoAcceptChanges = true)]
        protected virtual void HandleMessage(NHapiIMessage message)
        {
            if (message is SIU_S12)
            {
                HandleMessage((SIU_S12)message);
            }
            else if (message is SIU_S13)
            {
                HandleMessage((SIU_S13)message);
            }
            else if (message is SIU_S14)
            {
                HandleMessage((SIU_S14)message);
            }
            else if (message is SIU_S15)
            {
                HandleMessage((SIU_S15)message);
            }
            else if (message is SIU_S17)
            {
                HandleMessage((SIU_S17)message);
            }
            else if (message is SIU_S26)
            {
                HandleMessage((SIU_S26)message);
            }
        }

        private void HandleMessage(SIU_S12 message)
        {
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), AppointmentAction.Schedule);
        }

        private void HandleMessage(SIU_S13 message)
        {
            var appointmentAction = AppointmentAction.Schedule;
            var appointmentMessageHandlerSettings = ApplicationSettings.Cached.GetSetting<AppointmentMessageHandlerSettings>(ApplicationSetting.AppointmentMessageHandler).Value;

            if (message.SCH.FillerStatusCode.Identifier.Value != null && appointmentMessageHandlerSettings.CheckInTerms.Contains(message.SCH.FillerStatusCode.Identifier.Value.ToLower()))
            {
                appointmentAction = AppointmentAction.CheckIn;
            }
            else if (message.SCH.FillerStatusCode.Identifier.Value != null && appointmentMessageHandlerSettings.CheckOutTerms.Contains(message.SCH.FillerStatusCode.Identifier.Value.ToLower()))
            {
                appointmentAction = AppointmentAction.CheckOut;
            }
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), appointmentAction);
        }

        private void HandleMessage(SIU_S14 message)
        {
            if (message.SCH.FillerStatusCode.Identifier.Value == null)
            {
                Trace.TraceWarning("SIU_S14 message {0} arrived with no FillerStatusCode.".FormatWith(message));
                return;
            }

            var appointmentAction = AppointmentAction.None;
            var appointmentMessageHandlerSettings = ApplicationSettings.Cached.GetSetting<AppointmentMessageHandlerSettings>(ApplicationSetting.AppointmentMessageHandler).Value;

            if (appointmentMessageHandlerSettings.CheckInTerms.Contains(message.SCH.FillerStatusCode.Identifier.Value.ToLower()))
            {
                appointmentAction = AppointmentAction.CheckIn;
            }
            else if (appointmentMessageHandlerSettings.CheckOutTerms.Contains(message.SCH.FillerStatusCode.Identifier.Value.ToLower()))
            {
                appointmentAction = AppointmentAction.CheckOut;
            }
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), appointmentAction);
        }

        private void HandleMessage(SIU_S15 message)
        {
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), AppointmentAction.Cancel);
        }

        private void HandleMessage(SIU_S17 message)
        {
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), AppointmentAction.Cancel);
        }

        private void HandleMessage(SIU_S26 message)
        {
            HandleMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), AppointmentAction.NoShow);
        }

        private void HandleMessageComponents(MSH msh, SCH sch, PID pid, PV1 pv1, AIL ail, AIP[] aips, AppointmentAction appointmentAction)
        {
            string appointmentId = sch.FillerAppointmentID.EntityIdentifier.Value;
            if (string.IsNullOrWhiteSpace(appointmentId))
            {
                appointmentId = sch.PlacerAppointmentID.EntityIdentifier.Value;
            }

            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.Value.GetMapping<Appointment>(externalSystemName: msh.SendingApplication.NamespaceID.Value,
                                                                                                                  practiceRepositoryEntityId: PracticeRepositoryEntityId.Appointment,
                                                                                                                  externalSystemEntityKey: appointmentId,
                                                                                                                  exceptionOnNotFound: appointmentAction != AppointmentAction.Schedule && appointmentAction != AppointmentAction.CheckIn && appointmentAction != AppointmentAction.None);

            UserAppointment appointment = (appointmentMapping.Id == 0) ? new UserAppointment { Encounter = new Encounter() }
                                              : _practiceRepository.Value.Appointments.OfType<UserAppointment>().
                                                    Include(a => a.Encounter, a => a.Encounter.Patient).
                                                    FirstOrDefault(a => a.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value).EnsureNotDefault("Mapped appointment {0} does not exist.".FormatWith(appointmentMapping.PracticeRepositoryEntityKey));

            ExternalSystemEntityMapping appointmentTypeMapping = _externalSystemService.Value.GetMapping<AppointmentType>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.AppointmentType, string.IsNullOrWhiteSpace(sch.AppointmentType.Identifier.Value) ? sch.AppointmentReason.Identifier.Value : sch.AppointmentType.Identifier.Value, true);
            var appointmentType = ((AppointmentType)_externalSystemService.Value.GetMappedEntity(appointmentTypeMapping));
            string externalPatientId = pid.PatientIDExternalID.ID.Value;
            if (externalPatientId.IsNullOrEmpty())
            {
                externalPatientId = pid.GetPatientIDInternalID(0).ID.Value;
            }
            var patient = ((Patient)_externalSystemService.Value.GetMappedEntity(_externalSystemService.Value.GetMapping<Patient>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Patient, externalPatientId, true)));
            string attendingDoctorId = pv1.AttendingDoctor.IDNumber.Value;
            ExternalSystemEntityMapping scheduledUserMapping = null;
            // Look in PV1.7 for attending doctor.  If it's null OR if there's no mapping, look in AIP for a doctor where the Identifier does not equal R (referring).
            if (attendingDoctorId.IsNotNullOrEmpty())
            {
                scheduledUserMapping = _externalSystemService.Value.GetMapping<User>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.User, attendingDoctorId);
            }
            if (attendingDoctorId.IsNullOrEmpty() || scheduledUserMapping == null || scheduledUserMapping.Id == 0)
            {
                attendingDoctorId = aips.Where(i => i.ResourceRole.Identifier.Value != "R").Select(i => i.PersonnelResourceID.IDNumber.Value).FirstOrDefault();
            }

            if (attendingDoctorId.IsNullOrEmpty()) throw new ArgumentException("Scheduled doctor could not be found in the message. PV1 doctor was: {0}. AIP doctor was: {1}.".FormatWith(pv1.AttendingDoctor.IDNumber.Value, attendingDoctorId));

            scheduledUserMapping = _externalSystemService.Value.GetMapping<User>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.User, attendingDoctorId, true);
            Doctor scheduledUser = _practiceRepository.Value.Users.OfType<Doctor>().FirstOrDefault(u => u.Id == Int32.Parse(scheduledUserMapping.PracticeRepositoryEntityKey)).EnsureNotDefault("Mapped user {0} does not exist.".FormatWith(scheduledUserMapping.PracticeRepositoryEntityKey));

            ExternalProvider referringProvider = null;
            string referringProviderId = pv1.ReferringDoctor.IDNumber.Value;
            var providerIdPulledFromPv1 = true;
            if (referringProviderId.IsNullOrEmpty())
            {
                referringProviderId = aips.Where(i => i.ResourceRole.Identifier.Value == "R").Select(i => i.PersonnelResourceID.IDNumber.Value).FirstOrDefault();
                providerIdPulledFromPv1 = false;
            }

            if (referringProviderId.IsNotNullOrEmpty())
            {
                ExternalSystemEntityMapping referringProviderMapping = _externalSystemService.Value.GetMapping<ExternalContact>(msh.SendingApplication.NamespaceID.Value,
                                                                                                                        PracticeRepositoryEntityId.ExternalContact, referringProviderId);

                if (referringProviderMapping.Id == 0 && providerIdPulledFromPv1) // todo implement ability to create a new external provider from aips as well.
                {
                    var displayName = pv1.ReferringDoctor.GivenName.Value;
                    if (pv1.ReferringDoctor.MiddleInitialOrName.Value != null)
                        displayName = displayName + " " + pv1.ReferringDoctor.MiddleInitialOrName.Value;
                    displayName = displayName + " " + pv1.ReferringDoctor.FamilyName.Value;
                    if (pv1.ReferringDoctor.SuffixEgJRorIII.Value != null)
                        displayName = displayName + " " + pv1.ReferringDoctor.SuffixEgJRorIII.Value;
                    if (pv1.ReferringDoctor.DegreeEgMD.Value != null)
                        displayName = displayName + ", " + pv1.ReferringDoctor.DegreeEgMD.Value;

                    referringProvider = new ExternalProvider
                                            {
                                                DisplayName = displayName,
                                                ExternalOrganizationId = null,
                                                FirstName = pv1.ReferringDoctor.GivenName.Value,
                                                Honorific = pv1.ReferringDoctor.DegreeEgMD.Value,
                                                LastNameOrEntityName = pv1.ReferringDoctor.FamilyName.Value,
                                                MiddleName = pv1.ReferringDoctor.MiddleInitialOrName.Value,
                                                Prefix = pv1.ReferringDoctor.PrefixEgDR.Value,
                                                Suffix = pv1.ReferringDoctor.SuffixEgJRorIII.Value,
                                                IsArchived = false,
                                                ExcludeOnClaim = false
                                            };
                    _practiceRepository.Value.Save(referringProvider);
                    referringProviderMapping.PracticeRepositoryEntityKey = referringProvider.Id.ToString();
                    _practiceRepository.Value.Save(referringProviderMapping);
                }

                if (referringProviderMapping.Id > 0)
                {
                    referringProvider = (ExternalProvider)_practiceRepository.Value.ExternalContacts.FirstOrDefault(
                    i => i.Id == Int32.Parse(referringProviderMapping.PracticeRepositoryEntityKey));
                }
                else
                {
                    Trace.TraceWarning("Referring provider mapping not found for referring provider id: {0}, Appointment Id: {1}".FormatWith(referringProviderId, appointmentId));
                }
            }

            // If there is no location resource in the message, leave location as nothing 
            ServiceLocation location = null;
            if (ail.LocationResourceID.PointOfCare.Value.IsNotNullOrEmpty())
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMapping<ServiceLocation>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ServiceLocation, ail.LocationResourceID.PointOfCare.Value, true);
                location = ((ServiceLocation)_externalSystemService.Value.GetMappedEntity(locationMapping));
            }
            else if (pv1.AssignedPatientLocation.Facility.NamespaceID.Value.IsNotNullOrEmpty())
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMapping<ServiceLocation>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ServiceLocation, pv1.AssignedPatientLocation.Facility.NamespaceID.Value, true);
                location = ((ServiceLocation)_externalSystemService.Value.GetMappedEntity(locationMapping));
            }
            DateTime dateTime = sch.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.GetAsDate();
            // Hang onto the old status before we update stuff
            var oldStatus = appointment.Encounter.EncounterStatus;
            switch (appointmentAction)
            {
                case AppointmentAction.Schedule:
                    appointment.Encounter.EncounterStatus = EncounterStatus.Pending;
                    break;
                case AppointmentAction.Cancel:
                    CancelAppointment(appointment, EncounterStatus.CancelOther);
                    break;
                case AppointmentAction.NoShow:
                    CancelAppointment(appointment, EncounterStatus.CancelNoShow);
                    break;
                case AppointmentAction.CheckIn:
                    if (!((int)appointment.Encounter.EncounterStatus >= (int)EncounterStatus.Questions))
                    {
                        appointment.Encounter.EncounterStatus = EncounterStatus.Questions;
                    }
                    break;
                case AppointmentAction.CheckOut:
                    CheckOutAppointment(appointment);
                    break;
            }

            // Never let the process move the status backward. Keep the status as the old status
            if ((int)oldStatus > (int)appointment.Encounter.EncounterStatus)
            {
                appointment.Encounter.EncounterStatus = oldStatus;
            }
            UpdateAppointment(appointment, patient, appointmentType, scheduledUser, location, dateTime, referringProvider);
            _practiceRepository.Value.Save(appointment);

            appointmentMapping.PracticeRepositoryEntityKey = appointment.Id.ToString();
            _practiceRepository.Value.Save(appointmentMapping);
            if ((int)appointment.Encounter.EncounterStatus >= (int)EncounterStatus.Questions)
            {
                ExternalSystemEntityMapping visitMapping = _externalSystemService.Value.GetMapping<Encounter>(msh.SendingApplication.NamespaceID.Value, 
                    PracticeRepositoryEntityId.Encounter,
                    pv1.VisitNumber.ID.Value.IsNotNullOrEmpty() ? pv1.VisitNumber.ID.Value : sch.FillerAppointmentID.EntityIdentifier.Value);
                visitMapping.PracticeRepositoryEntityKey = appointment.EncounterId.ToString();
                _practiceRepository.Value.Save(visitMapping);
            }
        }

        private void CheckOutAppointment(Appointment appointment)
        {
            if (appointment.Encounter.EncounterStatus == EncounterStatus.Questions)
            {
                appointment.Encounter.EncounterStatus = EncounterStatus.Checkout;
            }
        }

        private void CancelAppointment(Appointment appointment, EncounterStatus status)
        {
            if (StatusesExcludedFromCancel.Contains(appointment.Encounter.EncounterStatus))
            {
                Trace.TraceWarning("Appointment {0} with status {1} was not cancelled by SIU message.".FormatWith(appointment.Id, appointment.Encounter.EncounterStatus.GetDisplayName()));
            }
            else
            {
                appointment.Encounter.EncounterStatus = status;
            }
        }

        private static void UpdateAppointment(UserAppointment appointment, Patient patient, AppointmentType appointmentType, User scheduledUser, ServiceLocation location, DateTime dateTime, ExternalProvider referringProvider)
        {
            appointment.Encounter.Patient = patient;
            appointment.AppointmentType = appointmentType;
            appointment.Encounter.EncounterTypeId = appointmentType.EncounterTypeId ?? default(Int32);
            appointment.User = scheduledUser;
            appointment.DateTime = dateTime;
            if (location != null)
            {
                appointment.Encounter.ServiceLocation = location;
            }
            if (referringProvider != null && patient.ExternalProviders.All(r => r.ExternalProviderId != referringProvider.Id))
            {
                patient.ExternalProviders.Add(new PatientExternalProvider
                                                  {
                                                      IsReferringPhysician = true,
                                                      ExternalProvider = referringProvider
                                                  });
            }
        }

        protected bool HandlesMessage(NHapiIMessage message)
        {
            return (message is SIU_S12 ||
                    message is SIU_S13 ||
                    message is SIU_S14 ||
                    message is SIU_S15 ||
                    message is SIU_S17 ||
                    message is SIU_S26 ||
                    message is NHapi.Model.V231.Message.SIU_S12);
        }

        private AIP[] GetAips(SIU_S12 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S13 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S14 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S15 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S17 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S26 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        protected void HandleMessage(HL7Message message)
        {
            if (message.Message is NHapi.Model.V231.Message.SIU_S12)
            {
                message = new HL7Message(message.ToString().ReplaceFirst("2.3.1", "2.3")) { MessageType = message.MessageType };
            }

            message = FixNonStandardSegments(message);

            HandleMessage(message.Message);
        }

        protected bool HandlesMessage(HL7Message message)
        {
            return HandlesMessage(message.Message);
        }

        protected bool HandlesMessage(IOIMessage message)
        {
            return message is HL7Message && HandlesMessage((HL7Message)message);
        }

        /// <summary>
        ///   Removes any unsupported segments from messages, reorders out of order segments, and fixes other mal-formatted data.
        /// </summary>
        /// <param name="message"> The message. </param>
        /// <returns> </returns>
        internal static HL7Message FixNonStandardSegments(HL7Message message)
        {
            string text = message.ToString();
            List<string> lines = text.Split(new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            int lineCount = lines.Count;

            // remove EVN (not supported in SIU)
            lines = lines.Where(l => !new[] { "EVN|" }.Any(l.StartsWith)).ToList();

            bool isSupported = lineCount == lines.Count;

            // make sure SCH is not after PID
            int schIndex = lines.IndexOf(i => i.StartsWith("SCH|"));
            int pidIndex = lines.IndexOf(i => i.StartsWith("PID|"));
            if (schIndex > pidIndex && schIndex >= 0 && pidIndex >= 0)
            {
                isSupported = false;
                string sch = lines[schIndex];
                lines.RemoveAt(schIndex);
                lines.Insert(1, sch);
            }

            // make sure AIL comes before AIPs
            int ailIndex = lines.IndexOf(i => i.StartsWith("AIL|"));
            int aipIndex = lines.IndexOf(i => i.StartsWith("AIP|"));

            if (ailIndex > aipIndex && ailIndex >= 0 && aipIndex >= 0)
            {
                isSupported = false;
                string ail = lines[ailIndex];
                lines.RemoveAt(ailIndex);
                lines.Insert(aipIndex, ail);
            }

            // return original message if no modifications were made
            if (isSupported) return message;

            return new HL7Message(lines.Join("\r"));
        }
    }
}