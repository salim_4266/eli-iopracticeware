﻿using System.Security;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;

namespace IO.Practiceware.Integration.HL7.Connections
{
    public class WebServiceMessageConnector : IDisposable
    {
        #region [ CONSTANT ]

        public const string ClientUserAgent = @"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36";

        const string MultiPartContentType = @"multipart/form-data; boundary=";
        const string FileFormKey = @"fileUpload";
        const string FileMimeType = @"text/xml";
        const string HeaderTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";

        const string DashForBoundry = @"-{0}";

        const string ResponseCodeInvalidSecurityToken = "301";

        #endregion [ CONSTANT ]

        private readonly Func<IMessageAuditor> _auditor;
        private readonly IMessenger _messenger;
        private readonly WebServiceAuthenticator _webServiceAuthenticator;
        private readonly object _syncRoot = new object();
        private object _subscriptionToken;

        public WebServiceMessageConnector(
            Func<IMessageAuditor> auditor, 
            IMessenger messenger,
            WebServiceAuthenticator webServiceAuthenticator)
        {
            _auditor = auditor;
            _messenger = messenger;
            _webServiceAuthenticator = webServiceAuthenticator;
        }

        public void Start()
        {
            lock (_syncRoot)
            {
                if (_subscriptionToken == null)
                {
                    _subscriptionToken = _messenger.Subscribe<SendMessageRequest>(OnSendMessageRequest, m => IsSupported(m.Message));
                }
            }
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                if (_subscriptionToken != null)
                {
                    _messenger.Unsubscribe(_subscriptionToken);
                }
                _subscriptionToken = null;
            }
        }

        private static bool IsSupported(IMessage message)
        {
            return MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(message.Destination, message.MessageType) != null;
        }

        public void OnSendMessageRequest(IMessenger messenger, object token, SendMessageRequest message)
        {
            message.Handlers.Add(typeof (WebServiceMessageConnector));
            SendMessage(message.Message);
        }

        private void SendMessage(IMessage message)
        {
            if (message is CdaMessage)
            {
                return;
            }

            string receivingApplication = message.Destination;
            WebServiceMessagingOutboundEndpointConfiguration endpoint =
                MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(receivingApplication, message.MessageType);

            if (receivingApplication == null || endpoint == null || endpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find remote endpoint for message type {0} for {1}.".FormatWith(message.MessageType, receivingApplication));
                return;
            }

            if (message is HL7Message)
            {
                SendHl7Message(endpoint, message);
                Trace.TraceInformation("Sent HL7 message {0} to {1}.", message.Id, endpoint.Name);
            }
            else
            {
                throw new NotSupportedException("Message {0} of type {1} is not supported.".FormatWith(message.Id, message.GetType().Name));
            }
        }

        private void SendHl7Message(WebServiceMessagingOutboundEndpointConfiguration endpoint, IMessage message)
        {
            var hl7Message = (HL7Message)message;

            if (endpoint.RequestAcknowledgements)
            {
                hl7Message.Message.RequestAcknowledgement();
            }

            var ms = new MemoryStream();
            var client = new WebClient();

            if (endpoint.ContentType.ToLower() == "json")
            {
                var seralizedMessage = new DataContractJsonSerializer(typeof(string));
                seralizedMessage.WriteObject(ms, message.ToString());
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            }
            else
            {
                var seralizedMessage = new DataContractSerializer(typeof(string));
                seralizedMessage.WriteObject(ms, message.ToString());
                client.Headers.Add(HttpRequestHeader.ContentType, "application/xml");
            }
            byte[] response = client.UploadData(endpoint.Value + endpoint.MethodName, endpoint.MethodType, ms.ToArray());
            string messageString = Encoding.ASCII.GetString(response);

            var doc = new XmlDocument();
            doc.LoadXml(messageString);
            string responseMessage = doc.InnerText;

            var receiveMessage = new HL7Message(responseMessage);
            Trace.TraceInformation("Message {0} of type {1} received.", hl7Message.Id, hl7Message.MessageType);
            _auditor().Audit(receiveMessage, false, ProcessingState.Unprocessed);
        }

        public void Dispose()
        {
            Stop();
        }

        #region [ CCD Implemetation ]

        private void SendCda(WebServiceMessagingOutboundEndpointConfiguration endpoint, IMessage message, bool retryOnExpiredAuthenticationToken)
        {
            string authenticationToken = string.Empty;

            if (endpoint.AuthenticationRequired)
            {
                authenticationToken = _webServiceAuthenticator.GetAuthenticationToken(message.Destination);
                if (string.IsNullOrWhiteSpace(authenticationToken))
                {
                    throw new SecurityException("Could not generate authentication token for {0}.".FormatWith(message.Destination));
                }
            }

            var responseStatus = SendCdaAndGetResponse(endpoint, message, authenticationToken);

            if (responseStatus == null)
            {
                throw new ApplicationException("No response received from {0} (message type {1}) when sending message {2}.".FormatWith(endpoint.Name, endpoint.MessageType, message.Id));
            }

            if (responseStatus.Status.Equals(WebServiceAuthenticator.ResponseStatusError, StringComparison.InvariantCultureIgnoreCase))
            {
                if (responseStatus.ResponseCode.Equals(ResponseCodeInvalidSecurityToken, StringComparison.InvariantCultureIgnoreCase)
                    && retryOnExpiredAuthenticationToken)
                {
                    _webServiceAuthenticator.InvalidateAuthenticationToken(message.Destination);
                    
                    // token expired - try again after removing token
                    SendCda(endpoint, message, false);
                }
                else
                {
                    // unhandled error
                    var serviceError = "Service failed with reason '{0}' and '{1}".FormatWith(responseStatus.ResponseCode, responseStatus.ResponseCodeDetails);
                    throw new ApplicationException(serviceError);
                }
            }
            // success
        }

        private static ResponseStatus SendCdaAndGetResponse(WebServiceMessagingOutboundEndpointConfiguration endpoint, IMessage message, string authenticationToken)
        {
            var cdaMessage = (CdaMessage)message;
            string ccdBaseUrl = endpoint.Value;
            string methodeName = endpoint.MethodName;

            var practiceId = ApplicationSettings.Cached.GetSetting<string>("OmedixPracticeId").EnsureNotDefault(new InteractiveException("OmedixPracticeId is required. Check ApplicationSettings for this value.")).Value;
            var patientId = cdaMessage.PatientId;

            //Omedix file naming convestion has to be followed as Omedix parses filename and uses it as CCD context params
            //format is :: "datetime(YYYYMMDDHHMMSS)_practiceID_patientID.xml";            
            var ccdFilePath = string.Format(@"{0}_{1}_{2}.xml", DateTime.UtcNow.ToString("yyyyMMddHHmmss"), practiceId, patientId);

            var responseStatus = ExecutePostRequestForCcd(new Uri(string.Format("{0}{1}{2}", ccdBaseUrl, methodeName, authenticationToken)),
                ccdFilePath, message);
            return responseStatus;
        }

        private static string CreateFormDataBoundary()
        {
            return DashForBoundry.FormatWith(DateTime.Now.ToClientTime().Ticks.ToString(@"x"));
        }

        private static ResponseStatus ExecutePostRequestForCcd(Uri url, string fileName, IMessage message)
        {
            ResponseStatus responseStatus = null;
            var request = (HttpWebRequest)WebRequest.Create(url.AbsoluteUri);

            ServicePointManager.Expect100Continue = false;
            request.Method = "POST";
            request.UserAgent = ClientUserAgent;

            string boundary = CreateFormDataBoundary();
            request.ContentType = string.Format("{0}{1}", MultiPartContentType, boundary);

            Stream requestStream = request.GetRequestStream();
            WriteMultipartFormData(requestStream, boundary, fileName, message);

            var endBytes = Encoding.UTF8.GetBytes("--" + boundary + "--");
            requestStream.Write(endBytes, 0, endBytes.Length);
            requestStream.Close();

            using (WebResponse response = request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var jsonSerializer = new DataContractJsonSerializer(typeof(ResponseStatus));
                if (dataStream != null)
                {
                    responseStatus = (ResponseStatus)jsonSerializer.ReadObject(dataStream);
                }
            }
            return responseStatus;
        }

        private static void WriteMultipartFormData(Stream requestStream, string mimeBoundary, string fileName, IMessage message)
        {
            string header = String.Format(HeaderTemplate, mimeBoundary, FileFormKey, fileName, FileMimeType);
            byte[] headerbytes = Encoding.UTF8.GetBytes(header);
            requestStream.Write(headerbytes, 0, headerbytes.Length);

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(message.ToString())))
            {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = ms.Read(buffer, 0, buffer.Length)) != 0)
                {
                    requestStream.Write(buffer, 0, bytesRead);
                }
            }

            byte[] newlineBytes = Encoding.UTF8.GetBytes("\r\n");
            requestStream.Write(newlineBytes, 0, newlineBytes.Length);
        }

        #endregion [ CCD Implemetation ]
    }

    #region [ DATA CONTARCT ]

    [DataContract]
    class ResponseStatus
    {
        [DataMember]
        public string ResponseCode { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember]
        public string ResponseCodeDetails { get; set; }

        [DataMember(Name = "token")]
        public string Token { get; set; }
    }

    /// <summary>
    /// This class is used to store Web result form WCF service.
    /// </summary>
    [DataContract]
    class UserCredentials
    {
        public UserCredentials(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        /// <summary>
        /// ResponseCode
        /// </summary>
        [DataMember(Name = "username", Order = 1)]
        public string UserName { get; set; }

        /// <summary>
        /// status
        /// </summary>
        [DataMember(Name = "password", Order = 2)]
        public string Password { get; set; }
    }

    #endregion [ DATA CONTARCT ]
}