using IO.Practiceware.Integration.Messaging;
using Soaf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace IO.Practiceware.Integration.HL7.Connections
{
    public class SocketsMessageConnector : IDisposable
    {
        public const int MaxBacklog = 10;

        [ThreadStatic]
        private static Socket _activeSocket;

        private readonly object _syncRoot;
        private object _subscriptionToken;

        private readonly List<Thread> _listenerThreads;
        private readonly List<Socket> _listeners;

        private readonly IMessageAuditor _auditor;
        private readonly IMessenger _messenger;

        private bool _isStarted;
        private IEnumerable<int> _ports;

        public bool IsStarted
        {
            get { return _isStarted; }
        }

        public SocketsMessageConnector(IMessageAuditor auditor, IMessenger messenger)
        {
            _syncRoot = new object();
            _listenerThreads = new List<Thread>();
            _listeners = new List<Socket>();
            _auditor = auditor;
            _messenger = messenger;
        }

        private static bool IsSupported(IMessage message)
        {
            return (message is HL7Message &&
                    MessagingConfiguration.Current.SocketsMessageConnectorConfiguration.OutboundEndpoints.For(message.Destination, message.MessageType) != null);
        }

        public void OnSendMessageRequest(IMessenger messenger, object token, SendMessageRequest message)
        {
            SendMessage(message.Message);
        }

        private void StartListening()
        {
            lock (_syncRoot)
            {
                if (_isStarted) { throw new InvalidOperationException("Connector is already started."); }
                _isStarted = true;
                foreach (int port in PortsToListenOn)
                {
                    try
                    {
                        int p = port;
                        var listenerThread = new Thread(() => StartListenerSocket(p));
                        _listenerThreads.Add(listenerThread);
                        listenerThread.Start();
                    }
                    catch (Exception ex) { Trace.TraceError(ex.ToString()); }
                }
            }
        }

        public IEnumerable<int> PortsToListenOn
        {
            get
            {
                return _ports ?? MessagingConfiguration.Current.SocketsMessageConnectorConfiguration.InboundEndpoints.Select(e => Convert.ToInt32(e.Value));
            }
            internal set { _ports = value; }
        }

        private void StopListening()
        {
            lock (_syncRoot)
            {
                if (!_isStarted) return;
                try
                {
                    _listeners.ForEach(l =>
                                           {
                                               using (l)
                                               {
                                                   try
                                                   {
                                                       l.Close();
                                                   }
                                                   catch (Exception ex)
                                                   {
                                                       Trace.TraceError(ex.ToString());
                                                   }
                                               }
                                           });
                    _listenerThreads.ForEach(lt => { if (lt.IsAlive) { lt.Abort(); } });
                }
                catch (Exception ex) { Trace.TraceError(ex.ToString()); }
                finally
                {
                    _isStarted = false;
                    _listeners.Clear();
                    _listenerThreads.Clear();
                }
            }
        }

        private void StartListenerSocket(int port)
        {
            try
            {
                lock (_syncRoot)
                {
                    Trace.TraceInformation("Starting listening.");
                    var listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _listeners.Add(listener);
                    listener.Bind(new IPEndPoint(IPAddress.Any, port));
                    listener.Listen(10);
                    Accept(listener);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(new ApplicationException("Failed to start listener on port {0}. Ensure that no other SocketsMessageConnectors are configured to listen on this port.".FormatWith(port), ex).ToString());
            }
        }

        private void Accept(Socket listenerSocket)
        {
            lock (_syncRoot)
            {
                if (!_isStarted || listenerSocket == null) return;
                try
                {
                    var args = new SocketAsyncEventArgs();
                    args.Completed += (s, e) => Receive(args.AcceptSocket, listenerSocket);
                    listenerSocket.AcceptAsync(args);
                }
                catch (Exception ex)
                {

                    Trace.TraceError(ex.ToString());
                    listenerSocket.Dispose();
                    _listeners.Remove(listenerSocket);
                    if (_isStarted) TryRestart(listenerSocket);
                }
            }
        }

        private void Receive(Socket acceptSocket, Socket listenerSocket)
        {
            // listen for next connection async
            Accept(listenerSocket);

            try
            {
                using (acceptSocket) while (ReceiveData(acceptSocket, true)) { }
            }
            catch (Exception ex)
            {
                Trace.TraceError(new Exception("Error receiving message.", ex).ToString());
            }
        }

        private bool ReceiveData(Socket socket, bool keepAlive)
        {
            var bytes = new List<byte>();
            var buffer = new byte[1025];

            while (_isStarted && socket.Connected)
            {
                SocketError error;

                int bytesReceived = socket.Receive(buffer, 0, buffer.Length, SocketFlags.None, out error);

                bytes.AddRange(buffer.Take(bytesReceived));

                if (bytesReceived < buffer.Length || error != SocketError.Success)
                {
                    break;
                }
            }
            string stringData = Encoding.UTF8.GetString(bytes.ToArray());
            if (stringData.Contains(Constants.MessageStartDelimiter) && stringData.Contains(Constants.MessageEndDelimiter))
            {
                ProcessMessagesReceived(stringData, socket);
                return true;
            }

            if (!keepAlive) return false;

            return IsSocketConnected(socket);
        }

        public static bool IsSocketConnected(Socket s)
        {
            return s.Connected && !(s.Poll(1, SelectMode.SelectRead) && s.Available == 0);
        }

        private void TryRestart(Socket listenerSocket)
        {
            try
            {
                var endpoint = listenerSocket.LocalEndPoint.CastTo<IPEndPoint>();
                if (_isStarted)
                {
                    StartListenerSocket(endpoint.Port);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceWarning(new Exception("Failed to restart listener socket.", ex).ToString());
            }
        }

        private void ProcessMessagesReceived(string stringData, Socket socket)
        {
            while (stringData.Contains(Constants.MessageStartDelimiter) &&
                   stringData.Contains(Constants.MessageEndDelimiter) &&
                   (stringData.IndexOf(Constants.MessageStartDelimiter, StringComparison.Ordinal) <
                    stringData.IndexOf(Constants.MessageEndDelimiter, StringComparison.Ordinal)))
            {
                int startIndex = stringData.IndexOf(Constants.MessageStartDelimiter) + Constants.MessageStartDelimiter.Length;
                int length = stringData.IndexOf(Constants.MessageEndDelimiter) - stringData.IndexOf(Constants.MessageStartDelimiter) - Constants.MessageStartDelimiter.Length;
                string messageString = stringData.Substring(startIndex, length).Trim();
                stringData = stringData.Substring(stringData.IndexOf(Constants.MessageEndDelimiter) + 1);
                Socket lastActiveScoket = _activeSocket;

                try
                {
                    _activeSocket = socket;
                    var hl7Message = new HL7Message(messageString);
                    Trace.TraceInformation("Message received of type {0}: {1}", hl7Message.MessageType, messageString);
                    _auditor.Audit(hl7Message, false, ProcessingState.Unprocessed);
                    if (hl7Message.RequestsAcknowledgement)
                    {
                        IMessage acknowledgement = hl7Message.GetAcknowledgement();
                        if (acknowledgement != null) { SendMessage(acknowledgement); }
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(new Exception(string.Format("Error receiving message: {0}", messageString), ex).ToString());
                    throw;
                }
                finally { _activeSocket = lastActiveScoket; }
            }
        }

        private void SendMessage(IMessage message)
        {
            if (_activeSocket == null)
            {
                FindEndpointAndSendMessage(message);
            }
            else
            {
                var data = EncodeMessageForTransmission(message);
                _activeSocket.Send(data);
            }
        }

        private void FindEndpointAndSendMessage(IMessage message)
        {
            var hl7Message = (HL7Message)message;
            var endpoint = FindEndpointForOutboundMessage(message);
            if (endpoint == null)
            {
                Trace.TraceError("Could not find remote endpoint for message for {0}.".FormatWith(message.Destination));
                return;
            }

            if (endpoint.RequestAcknowledgements && !hl7Message.Message.IsAcknowledgement())
            {
                hl7Message.Message.RequestAcknowledgement();
            }

            var data = EncodeMessageForTransmission(message);


            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                string[] tokenizedEndpointAddress = endpoint.Value.Split(new[] { ':' });
                IPHostEntry hostEntry = Dns.GetHostEntry(hostNameOrAddress: tokenizedEndpointAddress[0]);
                IPAddress ipAddress = hostEntry.AddressList.FirstOrDefault(item => item.AddressFamily == AddressFamily.InterNetwork).EnsureNotDefault("Could not find an IP address.");

                var ipEndpoint = new IPEndPoint(ipAddress, port: Int32.Parse(tokenizedEndpointAddress[1]));

                socket.Connect(ipEndpoint);
                socket.Send(data);
                if (message.RequestsAcknowledgement)
                {
                    ReceiveData(socket, false);
                }
            }
        }

        internal event EventHandler<EventArgs<SocketsMessagingOutboundEndpointConfiguration>> ResolvingEndpointForOutboundMessage;

        private SocketsMessagingOutboundEndpointConfiguration FindEndpointForOutboundMessage(IMessage message)
        {
            var args = new EventArgs<SocketsMessagingOutboundEndpointConfiguration>();
            ResolvingEndpointForOutboundMessage.Fire(this, args);

            if (args.Value != null) return args.Value;

            var endpoint = MessagingConfiguration.Current.SocketsMessageConnectorConfiguration.OutboundEndpoints.For(message.Destination, message.MessageType);
            return endpoint;
        }

        private static byte[] EncodeMessageForTransmission(IMessage message)
        {
            string messageString = "{0}{1}{2}".FormatWith(Constants.MessageStartDelimiter,
                message.ToString(),
                Constants.MessageEndDelimiter);
            Trace.TraceInformation("Sending message: {0}", messageString);
            byte[] data = Encoding.UTF8.GetBytes(messageString);
            return data;
        }

        public void Start()
        {
            lock (_syncRoot)
            {
                if (_subscriptionToken == null)
                {
                    _subscriptionToken = _messenger.Subscribe<SendMessageRequest>(OnSendMessageRequest, m => IsSupported(m.Message));
                }
                StartListening();
            }
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                if (_subscriptionToken != null)
                {
                    _messenger.Unsubscribe(_subscriptionToken);
                }
                _subscriptionToken = null;
                StopListening();
            }
        }

        public void Dispose()
        {
            Stop();
        }

    }
}
