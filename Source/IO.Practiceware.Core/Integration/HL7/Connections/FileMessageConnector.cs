using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Storage;
using Soaf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace IO.Practiceware.Integration.HL7.Connections
{
    public class FileMessageConnector : IDisposable
    {
        private readonly object _syncRoot = new object();
        private object _subscriptionToken;

        private readonly Func<IMessageAuditor> _auditor;
        private readonly IMessenger _messenger;
        
        private const int MaxAttemptCount = 10;
        private const int RetryDelay = 2000;
        
        private bool _isStarted;
        private MessageConnectorConfiguration<FileMessagingInboundEndpointConfiguration, FileMessagingOutboundEndpointConfiguration> _configuration;
        
        internal MessageConnectorConfiguration<FileMessagingInboundEndpointConfiguration, FileMessagingOutboundEndpointConfiguration> Configuration
        {
            get { return _configuration ?? MessagingConfiguration.Current.FileMessageConnectorConfiguration; }
            set { _configuration = value; }
        }

        public FileMessageConnector(Func<IMessageAuditor> auditor, IMessenger messenger)
        {
            _auditor = auditor;
            _messenger = messenger;
        }

        private bool IsSupported(IMessage message)
        {
            return (message is HL7Message && Configuration.OutboundEndpoints.For(message.Destination, message.MessageType) != null);
        }

        private void OnSendMessageRequest(IMessenger messenger, object token, SendMessageRequest message)
        {
            message.Handlers.Add(typeof (FileMessageConnector));
            SendMessage(message.Message);
        }

        private void EnsureDirectoriesPresent()
        {
            foreach (var endpoint in Configuration.InboundEndpoints)
            {
                if (!FileManager.Instance.DirectoryExists(endpoint.Value))
                {
                    Trace.TraceWarning("Listening directory for HL7 file watcher did not exist at {0}. Auto-creating.".FormatWith(endpoint.Value));
                    FileManager.Instance.CreateDirectory(endpoint.Value);
                }

                if (!FileManager.Instance.DirectoryExists(endpoint.ProcessedPath)) { FileManager.Instance.CreateDirectory(endpoint.ProcessedPath); }

                if (!FileManager.Instance.DirectoryExists(endpoint.FailedPath)) { FileManager.Instance.CreateDirectory(endpoint.FailedPath); }
            }
        }

        private static string[] GetFiles(string path)
        {
            try
            {
                var filesAtPath = FileManager.Instance.ListFiles(path);
                var fileTimestamps = FileManager.Instance.GetFileInfos(filesAtPath);
                var sortedFiles = fileTimestamps
                    .OrderBy(i => i.LastWriteTimeUtc)
                    .ThenBy(i => i.Name)
                    .Select(i => i.FullName)
                    .ToArray();
                return sortedFiles;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            return new string[0];
        }

        private void ProcessFiles(IEnumerable<string> filePaths, FileMessagingInboundEndpointConfiguration endpoint)
        {
            foreach (string path in filePaths) { TryProcessFile(path, endpoint); }
        }

        private void SendMessage(IMessage message)
        {
            string receivingApplication = message.Destination;

            string endpointValue = null;
            var endpoint = Configuration.OutboundEndpoints.For(receivingApplication, message.MessageType);
            if (endpoint != null) { endpointValue = endpoint.Value; }

            if (receivingApplication == null || endpointValue == null)
            {
                Trace.TraceError("Could not find remote endpoint for message for {0}.".FormatWith(receivingApplication));
            }
            else
            {
                if (!FileManager.Instance.DirectoryExists(endpointValue)) { FileManager.Instance.CreateDirectory(endpointValue); }
                string path = Path.Combine(endpointValue, message.Id.ToString());
                Trace.TraceInformation("Dropping message for {0} at endpoint {1}.", receivingApplication, path);
                string messageString = "{0}{1}{2}".FormatWith(Constants.MessageStartDelimiter, message.ToString(), Constants.MessageEndDelimiter);
                FileManager.Instance.CommitContents(path, messageString);
            }
        }

        private void TryProcessFile(string path, FileMessagingInboundEndpointConfiguration endpoint)
        {
            int attemptCount = 0;
            bool isProcessed = false;

            while (attemptCount < MaxAttemptCount && !isProcessed)
            {
                try
                {
                    lock (_syncRoot)
                    {
                        if (FileManager.Instance.FileExists(path))
                        {
                            ProcessFile(path);
                            isProcessed = true;
                        }
                        else
                        {
                            Trace.TraceError("Could not find file at {0}".FormatWith(path));
                            break;
                        }
                    }
                }

                catch (Exception ex)
                {
                    attemptCount += 1;
                    if (attemptCount >= MaxAttemptCount)
                    {
                        Trace.TraceError("Max processing attempts failed. Aborting message at {0}. {1}.".FormatWith(path, ex.ToString()));
                        try
                        {
                            string destination = Path.Combine(endpoint.FailedPath, Path.GetFileName(path) ?? string.Empty);
                            if (FileManager.Instance.FileExists(destination)) { FileManager.Instance.Delete(destination); }
                            FileManager.Instance.Move(path, destination);
                        }
                        catch (Exception ex2) { Trace.TraceError("Could not move message at {0} to the Failed directory. {1}".FormatWith(path, ex2.ToString())); }
                    }
                    else
                    {
                        Trace.TraceWarning("Processing of file {0} failed. This has occurred {1} times. Processing of this message will be aborted after {2} retries. Retrying in {3} seconds. {4}.".FormatWith(path, attemptCount, MaxAttemptCount, RetryDelay / 1000.0, ex.ToString()));
                        Thread.Sleep(RetryDelay);
                    }
                }
            }
            while (attemptCount < MaxAttemptCount && isProcessed)
            {
                try
                {
                    string destination = Path.Combine(endpoint.ProcessedPath, Path.GetFileName(path) ?? string.Empty);
                    if (FileManager.Instance.FileExists(destination)) { FileManager.Instance.Delete(destination); }
                    FileManager.Instance.Move(path, destination);
                    break;
                }
                catch (Exception ex)
                {
                    attemptCount += 1;
                    if (attemptCount > MaxAttemptCount)
                    {
                        Trace.TraceError("Max move attempts failed. Aborting message at {0}. Please move manually into the Processed folder. {1}.".FormatWith(path, ex.ToString()));
                    }
                    else
                    {
                        Trace.TraceWarning("Moving of file {0} failed. This has occurred {1} times. Moving of this message will be aborted after {2} retries. Retrying in {3} seconds. {4}.".FormatWith(path, attemptCount, MaxAttemptCount, RetryDelay / 1000.0, ex.ToString()));
                        Thread.Sleep(RetryDelay);
                    }
                }
            }
        }

        private void ProcessFile(string path)
        {
            string fileText = FileManager.Instance.ReadContentsAsText(path);
            string[] tokenizedFileText = fileText.Split(new[] { Constants.MessageStartDelimiter, Constants.MessageEndDelimiter }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string messageText in tokenizedFileText)
            {
                try { ProcessFileMessage(messageText); }
                catch (Exception ex) { Trace.TraceError(ex.ToString()); }
            }
        }

        private void ProcessFileMessage(string text)
        {
            var message = new HL7Message();
            message.Load(text);
            _auditor().Audit(message, false, ProcessingState.Unprocessed);
        }

        public void ProcessFiles()
        {
            lock (_syncRoot)
            {
                if (!_isStarted) { return; }

                // pick up from last folder first, but process first folder first (to ensure time order)
                foreach (var endpointFiles in Configuration.InboundEndpoints.Reverse<FileMessagingInboundEndpointConfiguration>()
                    .Select(e => new { Endpoint = e, Files = GetFiles(e.Value) }).Reverse())
                {
                    var files = endpointFiles.Files.Where(FileManager.Instance.FileExists).ToArray();
                    if (!files.Any()) { continue; }
                    Trace.TraceInformation("Beginning to process {0} files from {1}...".FormatWith(files.Count(), endpointFiles.Endpoint.Value));
                    ProcessFiles(files, endpointFiles.Endpoint);
                }
            }
        }

        public void Start()
        {
            lock (_syncRoot)
            {
                if (_isStarted) { throw new InvalidOperationException("Connector is already started."); }

                _isStarted = true;

                EnsureDirectoriesPresent();
                
                if (_subscriptionToken == null)
                {
                    _subscriptionToken = _messenger.Subscribe<SendMessageRequest>(OnSendMessageRequest, m => IsSupported(m.Message));
                }
            }
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                if (_subscriptionToken != null)
                {
                    _messenger.Unsubscribe(_subscriptionToken);
                }
                _subscriptionToken = null;
                _isStarted = false;
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
