﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.Messaging;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Integration.HL7.Connections
{
    public class WebServiceAuthenticator
    {
        public const string GenericUserAgent = @"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)";

        public const string ResponseStatusSuccess = "success";
        public const string ResponseStatusError = "error";

        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, string>> _perClientAuthenticationTokens = 
            new ConcurrentDictionary<string, ConcurrentDictionary<string, string>>();

        public void InvalidateAuthenticationToken(string receivingApplication)
        {
            var authenticationTokens = GetClientAuthenticationTokens();
            string ignoreValue;
            authenticationTokens.TryRemove(receivingApplication, out ignoreValue);
        }

        public string GetAuthenticationToken(string receivingApplication)
        {
            var authenticationTokens = GetClientAuthenticationTokens();

            string result;
            if (authenticationTokens.TryGetValue(receivingApplication, out result))
            {
                return result;
            }

            return GenerateNewAuthenticationToken(receivingApplication);
        }

        public string GenerateNewAuthenticationToken(string receivingApplication)
        {
            string token = string.Empty;

            if (receivingApplication == null)
                receivingApplication = "Omedix";

            WebServiceMessagingOutboundEndpointConfiguration authenticationEndpoint =
                    MessagingConfiguration.Current.WebServiceMessageConnectorConfiguration.OutboundEndpoints.For(receivingApplication, "LOGIN");
            var uri = "{0}{1}".FormatWith(authenticationEndpoint.Value, authenticationEndpoint.MethodName);

            if (authenticationEndpoint.MessageType.IsNullOrWhiteSpace())
            {
                Trace.TraceError("Could not find login endpoint for message for {0}.".FormatWith(receivingApplication));
                return token;
            }

            if (AuthenticateUser(receivingApplication, authenticationEndpoint.UserName, authenticationEndpoint.Password, uri))
            {
                token = GetAuthenticationToken(receivingApplication);
            }

            return token;
        }

        private bool AuthenticateUser(string receivingFacility, string userName, string password, string url)
        {
            bool returnValue = false;

            using (var stream = new MemoryStream())
            {
                var serviceResponse = new ResponseStatus();
                var request = (HttpWebRequest)WebRequest.Create(@url);
                ServicePointManager.Expect100Continue = false;
                request.Method = "POST";
                request.ContentType = @"application/json";
                request.UserAgent = GenericUserAgent;

                var jsonSerializer = new DataContractJsonSerializer(typeof(UserCredentials));
                var user = new UserCredentials(userName, password);

                jsonSerializer.WriteObject(stream, user);
                var byteArray = stream.ToArray();

                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                using (var response = request.GetResponse())
                {
                    dataStream = response.GetResponseStream();
                    jsonSerializer = new DataContractJsonSerializer(typeof(ResponseStatus));
                    if (dataStream != null)
                        serviceResponse = (ResponseStatus)jsonSerializer.ReadObject(dataStream);
                }

                if (serviceResponse.Status.Equals(ResponseStatusSuccess, StringComparison.InvariantCultureIgnoreCase))
                {
                    returnValue = true;
                    var authenticationTokens = GetClientAuthenticationTokens();
                    authenticationTokens[receivingFacility] = serviceResponse.Token;
                }
                else
                    Trace.TraceError("Authentication failed: responsecode:{0}, reason:{1}.", serviceResponse.ResponseCode, serviceResponse.ResponseCodeDetails);
            }

            return returnValue;
        }

        private ConcurrentDictionary<string, string> GetClientAuthenticationTokens()
        {
            // Per-client tokens storage
            return _perClientAuthenticationTokens.GetValue(ApplicationContext.Current.ClientKey, 
                () => new ConcurrentDictionary<string, string>());
        }
     }
}
