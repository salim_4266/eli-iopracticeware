﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Integration.PaperClaims
{
    [DataContract]
    public class PaperClaimGenerationInfo
    {
        [DataMember]
        public virtual int InvoiceId { get; set; }

        [DataMember]
        public virtual String InsurerName { get; set; }

        [DataMember]
        public virtual ObservableCollection<Guid> BillingServiceTransactionIds { get; set; }

        [DataMember]
        public virtual int ClaimFormTypeId { get; set; }
    }
}
