﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.Linq;

namespace IO.Practiceware.Integration.PaperClaims
{
    public class PaperClaimGenerationUtilities
    {

        private readonly IPracticeRepository _practiceRepository;

        public PaperClaimGenerationUtilities(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public ObservableCollection<PaperClaimGenerationInfo> GetPaperClaimInformation(Invoice invoice)
        {
            var paperClaimGenerationInfo = new List<PaperClaimGenerationInfo>();
            var insurers = invoice.InvoiceReceivables
                .Where(ir => ir.BillingServiceTransactions != null
                    && ir.PatientInsurance != null
                    && ir.PatientInsurance.InsurancePolicy != null
                    && ir.PatientInsurance.InsurancePolicy.Insurer != null
                ).Select(ir => ir.PatientInsurance.InsurancePolicy.Insurer).Distinct().ToList();

            foreach (var insurer in insurers)
            {
                Insurer insurer1 = insurer;

                var billingTransactions = invoice.InvoiceReceivables.ToList()
                    .Where(ir => ir.PatientInsurance != null
                                                && ir.PatientInsurance.InsurancePolicy != null
                                                && ir.PatientInsurance.InsurancePolicy.Insurer != null
                                                && ir.PatientInsurance.InsurancePolicy.Insurer.Id == insurer1.Id)
                                                .SelectMany(ir => ir.BillingServiceTransactions)
                                                .Where(bst => bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                                                            && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                                            && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent);
                // We are only interested in Paper Claims here So check to make sure the MethodSent is paper and the Template Document Id is not null (Template Document Ids are null when Method Sent is electronic)
                // Also make sure Invoice Types match so we only get the intended Insurer Claim form
                var insurerClaimForm = insurer1.InsurerClaimForms.FirstOrDefault(x => x.ClaimFormType.TemplateDocumentId != null
                                                                                && x.InvoiceType == invoice.InvoiceType
                                                                                && x.ClaimFormType.MethodSent == MethodSent.Paper);
                var claimFormTypeId = insurerClaimForm != null ? insurerClaimForm.ClaimFormTypeId : (int)ClaimFormTypeId.Cms1500;
                var paperClaim = new PaperClaimGenerationInfo
                {
                    InsurerName = insurer1.Name,
                    InvoiceId = invoice.Id,
                    ClaimFormTypeId = claimFormTypeId
                };
                paperClaim.BillingServiceTransactionIds = billingTransactions.Select(i => i.Id).ToObservableCollection();
                paperClaimGenerationInfo.Add(paperClaim);
            }
            return paperClaimGenerationInfo.Where(pcvm => pcvm.BillingServiceTransactionIds.Count > 0).ToObservableCollection();
        }



        public PaperClaimGenerationInfo GetPaperClaimGenerationInfo(PaperClaimGenerationInfo claimGenerationInfo)
        {
            var invoice = _practiceRepository.Invoices.Single(inv => inv.Id == claimGenerationInfo.InvoiceId);
            _practiceRepository.AsQueryableFactory().Load(invoice,
                inv => inv.InvoiceReceivables.Select(ir => new
                {
                    ir.PatientInsurance.InsurancePolicy.Insurer,
                    cft = ir.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Select(icf => new
                    {
                        icf.ClaimFormType
                    })
                })
            );

            var claimFormType = invoice.InvoiceReceivables
                .Where(ir => ir.PatientInsurance != null
                             && ir.PatientInsurance.InsurancePolicy != null
                             && ir.PatientInsurance.InsurancePolicy.Insurer != null)
                .SelectMany(ir => ir.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms)
                // claimGenerationInfo now passes through a claim form type id, check for it so we get the correct Document type
                .Where(icf => icf.ClaimFormTypeId == claimGenerationInfo.ClaimFormTypeId)
                .Select(icf => icf.ClaimFormType).Distinct().ToList();
            if (claimFormType.Count == 1)
            {
                claimGenerationInfo.ClaimFormTypeId = claimFormType[0].Id;
            }
            else
            {
                claimGenerationInfo.ClaimFormTypeId = (int)ClaimFormTypeId.Cms1500;
            }

            return claimGenerationInfo;
        }
    }
}
