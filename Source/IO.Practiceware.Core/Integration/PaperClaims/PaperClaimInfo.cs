﻿using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly:
    Component(typeof(PaperClaimViewInfoMapper),
        typeof(IMap<IEnumerable<BillingServiceTransaction>, PaperClaim>))]

namespace IO.Practiceware.Integration.PaperClaims
{
    public class PaperClaim
    {
        public PaperClaim()
        {
            Services = new List<PaperClaimServiceInfo>();
            Diagnoses = new List<DiagnosisInfo>();
        }

        public string InvoiceReceivableIds { get; set; }

        public string BilledInsuranceName { get; set; }
        public string BilledInsuranceAddressLine1 { get; set; }
        public string BilledInsuranceAddressLine2 { get; set; }
        public string BilledInsuranceAddressLine3 { get; set; }
        public string BilledInsuranceCity { get; set; }
        public string BilledInsuranceState { get; set; }
        public string BilledInsuranceZip { get; set; }

        public string BilledPolicyHolderLastName { get; set; }
        public string BilledPolicyHolderFirstName { get; set; }
        public string BilledPolicyHolderAddressLine1 { get; set; }
        public string BilledPolicyHolderAddressLine2 { get; set; }
        public string BilledPolicyHolderCity { get; set; }
        public string BilledPolicyHolderState { get; set; }
        public string BilledPolicyHolderZip { get; set; }

        public string PatientId { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientAddressLine1 { get; set; }
        public string PatientAddressLine2 { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public DateTime? PatientBirthDate { get; set; }
        public Gender? PatientGender { get; set; }
        public string AccidentState { get; set; }
        public string RenderingProviderNpi { get; set; }
        public string BillingProviderTaxId { get; set; }
        public decimal TotalCharges { get; set; }
        public string BillingProviderName { get; set; }
        public string BillingProviderAddressLine1 { get; set; }
        public string BillingProviderAddressLine2 { get; set; }
        public string BillingProviderCity { get; set; }
        public string BillingProviderState { get; set; }
        public string BillingProviderZip { get; set; }
        public string BillingProviderNpi { get; set; }
        public string BillingProviderTaxonomy { get; set; }
        public DateTime ServiceDate { get; set; }
        public bool AssignBenefits { get; set; }
        public bool ReleaseInfoIsSigned { get; set; }
        public string ReferringDoctorNpi { get; set; }
        public string ReferringDoctorLastName { get; set; }
        public string ReferringDoctorFirstName { get; set; }
        public string ReferringDoctorFullName { get; set; }

        public List<PaperClaimServiceInfo> Services { get; set; }
        public List<DiagnosisInfo> Diagnoses { get; set; }

        public string AttendingProviderFullName { get; set; }
        public string AttendingProviderLastName { get; set; }
        public string AttendingProviderFirstName { get; set; }
        public string AttendingProviderNpi { get; set; }
        public string OperatingPhysicianLastName { get; set; }
        public string OperatingPhysicianFirstName { get; set; }
        public string OperatingPhysicianNpi { get; set; }
        public int PageNumber { get; set; }

        #region Ub04 Fields

        public string PayToName { get; set; }
        public string PayToAddressLine1 { get; set; }
        public string PayToAddressLine2 { get; set; }
        public string PayToCity { get; set; }
        public string PayToState { get; set; }
        public string PayToZip { get; set; }

        public string PrimaryCoinsuranceCode { get; set; }
        public string PrimaryCoinsuranceAmount { get; set; }
        public string SecondaryCoinsuranceCode { get; set; }
        public string SecondaryCoinsuranceAmount { get; set; }
        public string ThirdCoinsuranceCode { get; set; }
        public string ThirdCoinsuranceAmount { get; set; }
        public string PrimaryDeductibleCode { get; set; }
        public string PrimaryDeductibleAmount { get; set; }
        public string SecondaryDeductibleCode { get; set; }
        public string SecondaryDeductibleAmount { get; set; }
        public string ThirdDeductibleCode { get; set; }
        public string ThirdDeductibleAmount { get; set; }

        public DateTime PrintDate { get; set; }

        public string PrimaryPayerName { get; set; }
        public string PrimaryPayerCode { get; set; }
        public string PrimaryInsuredFirstName { get; set; }
        public string PrimaryInsuredLastName { get; set; }
        public string PrimaryRelationshipCode { get; set; }
        public string PrimaryPolicyNumber { get; set; }
        public string PrimaryPayment { get; set; }
        public string PrimaryBalanceDue { get; set; }
        public string SecondaryPayerName { get; set; }
        public string SecondaryPayerCode { get; set; }
        public string SecondaryInsuredFirstName { get; set; }
        public string SecondaryInsuredLastName { get; set; }
        public string SecondaryRelationshipCode { get; set; }
        public string SecondaryPolicyNumber { get; set; }
        public string SecondaryPayment { get; set; }
        public string SecondaryBalanceDue { get; set; }
        public string ThirdPayerName { get; set; }
        public string ThirdPayerCode { get; set; }
        public string ThirdInsuredFirstName { get; set; }
        public string ThirdInsuredLastName { get; set; }
        public string ThirdRelationshipCode { get; set; }
        public string ThirdPolicyNumber { get; set; }
        public string ThirdPayment { get; set; }
        public string ThirdBalanceDue { get; set; }
        public bool DisplayDecimal { get; set; }

        public string PrincipalDiagnosisCode
        {
            get { return Diagnoses.FirstOrDefault().IfNotNull(x => x.Diagnosis, ""); }
        }

        public string PrincipalProcedureCode { get; set; }

        public string RenderingProviderFirstName { get; set; }
        public string RenderingProviderLastName { get; set; }

        public InvoiceType InvoiceTypeEnum { get; set; }

        #endregion

        #region Cms1500 fields


        public Gender Male
        {
            get { return Gender.Male; }
        }

        public Gender Female
        {
            get { return Gender.Female; }
        }

        public InvoiceType InvoiceTypeProfessional
        {
            get { return InvoiceType.Professional; }
        }

        public InvoiceType InvoiceTypeVision
        {
            get { return InvoiceType.Vision; }
        }

        public InvoiceType InvoiceTypeFacility
        {
            get { return InvoiceType.Facility; }
        }

        public ServiceUnitOfMeasurement Minute
        {
            get { return ServiceUnitOfMeasurement.Minute; }
        }

        public ServiceUnitOfMeasurement Unit
        {
            get { return ServiceUnitOfMeasurement.Unit; }
        }

        public bool InsuranceTypeIsMedicare { get; set; }
        public bool InsuranceTypeIsMedicaid { get; set; }
        public bool InsuranceTypeIsTricare { get; set; }
        public bool InsuranceTypeIsGroup { get; set; }
        public bool InsuranceTypeIsOther { get; set; }
        public bool IsTaxonomyIncludedOnClaims { get; set; }

        public string BilledPolicyHolderPolicyNumber { get; set; }
        public string BilledPolicyHolderMiddleName { get; set; }
        public string BilledPolicyHolderTelephone { get; set; }
        public string BilledPolicyHolderGroupId { get; set; }
        public DateTime? BilledPolicyHolderBirthDate { get; set; }
        public Gender? BilledPolicyHolderGender { get; set; }
        public string BilledPolicyHolderEmployerName { get; set; }

        public string BilledInsurancePlanName { get; set; }
        public bool AnotherPlan { get; set; }
        public string AssignBenefitsSignature { get; set; }
        public string ReleaseInfoSignature { get; set; }

        public string PatientMiddleName { get; set; }
        public string PatientTelephone { get; set; }
        public bool BilledRelationshipSelf { get; set; }
        public bool BilledRelationshipSpouse { get; set; }
        public bool BilledRelationshipChild { get; set; }
        public bool BilledRelationshipOther { get; set; }
        public bool MaritalStatusSingle { get; set; }
        public bool MaritalStatusMarried { get; set; }
        public bool MaritalStatusOther { get; set; }
        public string OtherPolicyHolderPolicyNumber { get; set; }
        public string OtherPolicyHolderLastName { get; set; }
        public string OtherPolicyHolderFirstName { get; set; }
        public string OtherPolicyHolderMiddleName { get; set; }
        public string OtherPolicyHolderAddressLine1 { get; set; }
        public string OtherPolicyHolderAddressLine2 { get; set; }
        public string OtherPolicyHolderCity { get; set; }
        public string OtherPolicyHolderState { get; set; }
        public string OtherPolicyHolderZip { get; set; }
        public string OtherPolicyHolderTelephone { get; set; }
        public string OtherPolicyHolderGroupId { get; set; }
        public DateTime? OtherPolicyHolderBirthDate { get; set; }
        public Gender? OtherPolicyHolderGender { get; set; }
        public string OtherPolicyHolderEmployerName { get; set; }
        public string OtherInsuranceName { get; set; }
        public bool EmploymentRelated { get; set; }

        public bool AutoAccident { get; set; }
        public bool OtherAccident { get; set; }

        public DateTime? IllnessDate { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        public string ResubmitCode { get; set; }
        public string ClaimFrequencyTypeCodeId { get; set; }
        public string AuthorizationCode { get; set; }
        public string ServiceNote { get; set; }
        public string DiagnosisTypeId { get; set; }
        public string Diagnosis1
        {
            get { return GetDiagnosesAtIndex(0); }
        }
        public string Diagnosis2
        {
            get { return GetDiagnosesAtIndex(1); }
        }
        public string Diagnosis3
        {
            get { return GetDiagnosesAtIndex(2); }
        }
        public string Diagnosis4
        {
            get { return GetDiagnosesAtIndex(3); }
        }
        public string Diagnosis5
        {
            get { return GetDiagnosesAtIndex(4); }
        }

        public string Diagnosis6
        {
            get { return GetDiagnosesAtIndex(5); }
        }
        public string Diagnosis7
        {
            get { return GetDiagnosesAtIndex(6); }
        }
        public string Diagnosis8
        {
            get { return GetDiagnosesAtIndex(7); }
        }
        public string Diagnosis9
        {
            get { return GetDiagnosesAtIndex(8); }
        }
        public string Diagnosis10
        {
            get { return GetDiagnosesAtIndex(9); }
        }
        public string Diagnosis11
        {
            get { return GetDiagnosesAtIndex(10); }
        }
        public string Diagnosis12
        {
            get { return GetDiagnosesAtIndex(11); }
        }

        public string RenderingProviderTaxonomy { get; set; }

        public string BillingProviderTaxIdx { get; set; }

        public bool HasAssignedBenefits { get; set; }

        public string TotalPaid { get; set; }
        public string BalanceDue { get; set; }
        public string LocationName { get; set; }
        public string LocationAddressLine1 { get; set; }
        public string LocationAddressLine2 { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public string LocationNpi { get; set; }

        public string BillingProviderTelephone { get; set; }

        public string ProviderSignature { get; set; }
        public bool ProviderAssignBenefits { get; set; }

        public string EinIndicator { get; set; }
        public string SsnIndicator { get; set; }

        public string Clia { get; set; }
        public string ReferralNumber { get; set; }

        public string ReferringOrOrderingOrSupervisingProviderFullName { get; set; }
        public string ReferringOrOrderingOrSupervisingProviderNpi { get; set; }
        #endregion

        #region Helper Methods

        public IEnumerable<IEnumerable<T>> Batch<T>(IEnumerable<T> list, int size)
        {
            return list.Batch(size);
        }

        public IEnumerable<T> Take<T>(IEnumerable<T> list, int count)
        {
            return list.Take(count);
        }

        public string ConvertTrueBoolToX(bool value)
        {
            return value ? "X" : "";
        }

        public string ConvertTrueBoolToY(bool value)
        {
            return value ? "Y" : "";
        }

        public string GetEnumDescription(Enum value)
        {
            return value.GetDescription();
        }

        public string GetEnumDisplay(Enum value)
        {
            return value.GetDisplayName();
        }

        public string ConvertDateTimeToMMDDYYString(DateTime value, string delimiter = "/")
        {
            return value.ToMMDDYYString(delimiter);
        }

        public string ConvertDateTimeToMMDDYYYYString(DateTime value, string delimiter = "/")
        {
            return value.ToMMDDYYYYString(delimiter);
        }

        private string GetDiagnosesAtIndex(int index)
        {
            if (Convert.ToInt32(DiagnosisTypeId) == 9)
            {
                return Diagnoses.Count() > index ? Diagnoses[index].Diagnosis.Replace(".", "") : string.Empty;
            }
            else
            {
                return Diagnoses.Count() > index ? DisplayDecimal ? Diagnoses[index].Diagnosis : Diagnoses[index].Diagnosis.Replace(".", "") : string.Empty;
            }
        }

        #endregion
    }


    public class DiagnosisInfo
    {
        public string Diagnosis { get; set; }
        public int Id { get; set; }
    }

    public class PaperClaimServiceInfo
    {
        public PaperClaim Parent { get; set; }
        public Guid TransactionId { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceCodeModifier { get; set; }
        public DateTime ServiceDate { get; set; }
        public string ServiceUnits { get; set; }
        public decimal? ServiceCharge { get; set; }
        public string RevenueCode { get; set; }
        public string ServiceDescription { get; set; }
        public string DiagnosisLetters { get; set; }
        public string ServiceLocationCode { get; set; }
        public string EncounterServiceType { get; set; }
        public bool IsAnesthesia { get; set; }
        public ServiceUnitOfMeasurement ServiceUnitOfMeasurement { get; set; }
        public string UnclassifiedServiceDescription { get; set; }
        public string Line24 { get; set; }
        public string Emg { get; set; }
        public string Epsdt { get; set; }
    }

    internal class PaperClaimViewInfoMapper : IMap<IEnumerable<BillingServiceTransaction>, PaperClaim>
    {
        private readonly IMapMember<IEnumerable<BillingServiceTransaction>, PaperClaim>[] _members;
        private static readonly String[] DiagnosisLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };

        public PaperClaimViewInfoMapper()
        {
            _members = this.CreateMembers(Map, "PaperClaimInfo").ToArray();
        }

        public void Map(IEnumerable<BillingServiceTransaction> source, PaperClaim destination)
        {

            #region Common Logic

            // there will only be one invoice across all billing service transactions for paper claims
            var invoice = source.Select(x => x.InvoiceReceivable.Invoice).First();

            destination.InvoiceReceivableIds = source.Join(x => x.InvoiceReceivableId.ToString());

            destination.InvoiceTypeEnum = invoice.InvoiceType;

            var patientInsuranceReference = source.Select(x => x.InvoiceReceivable.PatientInsurance).WhereNotDefault().FirstOrDefault().EnsureNotDefault("There are no billing service transactions present with patient insurances");

            var insurer = patientInsuranceReference.InsurancePolicy.Insurer;
            if (insurer != null)
            {
                destination.BilledInsuranceName = insurer.Name;
                var insurerAddress = insurer.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims);
                if (insurerAddress != null)
                {
                    destination.BilledInsuranceAddressLine1 = insurerAddress.Line1;
                    destination.BilledInsuranceAddressLine2 = insurerAddress.Line2;
                    destination.BilledInsuranceCity = insurerAddress.City;
                    destination.BilledInsuranceState = insurerAddress.StateOrProvince.Abbreviation;
                    destination.BilledInsuranceZip = insurerAddress.PostalCode;
                }
            }

            // get policy holder 
            var policyHolder = patientInsuranceReference.InsurancePolicy.PolicyholderPatient;
            if (policyHolder != null)
            {
                destination.BilledPolicyHolderFirstName = policyHolder.FirstName;
                destination.BilledPolicyHolderLastName = policyHolder.LastName;
                destination.BilledPolicyHolderAddressLine1 = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(y => y.Line1);
                destination.BilledPolicyHolderAddressLine2 = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(y => y.Line2);
                destination.BilledPolicyHolderCity = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(y => y.City);
                destination.BilledPolicyHolderState = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(y => y.StateOrProvince.Abbreviation);
                destination.BilledPolicyHolderZip = policyHolder.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).IfNotNull(y => y.PostalCode);
            }

            var patient = invoice.Encounter.Patient;
            if (patient != null)
            {
                destination.PatientId = patient.Id.ToString();
                destination.PatientFirstName = patient.FirstName;
                destination.PatientLastName = patient.LastName;
                destination.PatientBirthDate = patient.DateOfBirth;
                destination.PatientGender = invoice.Encounter.Patient.Gender;

                var patientAddress = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).ValidateNotDefault("Patient {0}, does not have a valid address.".FormatWith(patient.GetFormattedName()));
                if (patientAddress != null)
                {
                    destination.PatientAddressLine1 = patientAddress.Line1;
                    destination.PatientAddressLine2 = patientAddress.Line2;
                    destination.PatientCity = patientAddress.City;
                    destination.PatientState = patientAddress.StateOrProvince.Abbreviation;
                    destination.PatientZip = patientAddress.PostalCode;
                }
            }

            destination.AccidentState = invoice.InvoiceSupplemental.IfNotNull(x => x.AccidentStateOrProvince.IfNotNull(y => y.Abbreviation, () => ""), () => "");


            var billingProvider = invoice.BillingProvider.GetClaimFileOverrideProviderOrCurrent(patientInsuranceReference.InsurancePolicy.InsurerId);

            if (string.IsNullOrEmpty(billingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).IfNull(() => ""))) throw new ValidationException(string.Format("Billing Provider {0} has no NPI", billingProvider.User.IfNotNull(x => x.DisplayName, () => "")));

            if (string.IsNullOrEmpty(billingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy).IfNull(() => ""))) throw new ValidationException(string.Format("Billing Provider {0} has no Taxonomy", billingProvider.User.IfNotNull(x => x.DisplayName, () => "")));

            var billingOrganizationPhysicalAddress = billingProvider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);
            if (billingOrganizationPhysicalAddress != null)
            {
                destination.BillingProviderName = billingProvider.BillingOrganization.Name;
                destination.BillingProviderAddressLine1 = billingOrganizationPhysicalAddress.Line1;
                destination.BillingProviderAddressLine2 = billingOrganizationPhysicalAddress.Line2;
                destination.BillingProviderCity = billingOrganizationPhysicalAddress.City;
                destination.BillingProviderState = billingOrganizationPhysicalAddress.StateOrProvince.Abbreviation;
                destination.BillingProviderZip = billingOrganizationPhysicalAddress.PostalCode;
            }
            destination.BillingProviderTaxId = billingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.TaxId).IfNull(() => "");
            destination.BillingProviderNpi = billingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).IfNull(() => "");
            destination.BillingProviderTelephone = billingProvider.BillingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Billing).IfNotNull(GetPhoneNumberFormattedValue);
            destination.BillingProviderTaxonomy = billingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy).IfNull(() => "");

            destination.ServiceDate = invoice.Encounter.StartDateTime;

            var user = invoice.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.User).FirstOrDefault();
            if (user != null)
            {
                destination.AttendingProviderFullName = user.DisplayName;
                destination.AttendingProviderLastName = user.LastName;
                destination.AttendingProviderFirstName = user.FirstName;

                //the BillingOrganization for the ASC will never be the BillingOrganization for the surgeon.
                //So else part is for ASC invoices
                var provider = user.Providers.FirstOrDefault(x => x.BillingOrganizationId == invoice.BillingProvider.BillingOrganizationId) ?? user.Providers.FirstOrDefault(x => x.ServiceLocationId == invoice.Encounter.ServiceLocationId);
                if (provider != null)
                {
                    destination.AttendingProviderNpi = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                    destination.OperatingPhysicianNpi = provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
                }

                destination.OperatingPhysicianFirstName = user.FirstName;
                destination.OperatingPhysicianLastName = user.LastName;
            }

            destination.Diagnoses = invoice.BillingDiagnoses.OrderBy(x => x.OrdinalId).Select(x => new DiagnosisInfo { Diagnosis = x.ExternalSystemEntityMapping.ExternalSystemEntityKey, Id = x.Id }).ToList();

            destination.Services = source.Where(x => x.InvoiceReceivable.PatientInsurance != null).OrderBy(x => x.BillingService.OrdinalId).Select(x => x.BillingService.FacilityEncounterService != null ? new PaperClaimServiceInfo
            {
                TransactionId = x.Id,
                RevenueCode = x.BillingService.FacilityEncounterService.RevenueCode.IfNotNull(y => y.Code, () => ""),
                ServiceDescription = x.BillingService.FacilityEncounterService.Description,
                ServiceCode = x.BillingService.FacilityEncounterService.Code,
                ServiceCodeModifier = String.Join("", x.BillingService.BillingServiceModifiers.Select(y => y.ServiceModifier.IfNotNull(z => z.Code, () => ""))),
                ServiceDate = invoice.Encounter.StartDateTime,
                ServiceUnits = FormatServiceUnits(x.BillingService),
                ServiceCharge = (x.BillingService.UnitCharge * x.BillingService.Unit ?? 0.00m),
                ServiceUnitOfMeasurement = x.BillingService.FacilityEncounterService.ServiceUnitOfMeasurement,
                EncounterServiceType = x.BillingService.FacilityEncounterService.EncounterServiceType.IfNotNull(y => y.Name, () => ""),
                Emg = x.BillingService.IsEmg ? "Y" : "",
                Epsdt = x.BillingService.IsEpsdt && x.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Medicaid ? "Y" : "",
                UnclassifiedServiceDescription = x.BillingService.UnclassifiedServiceDescription,
                Line24 = x.BillingService.FacilityEncounterService.NDC.IfNotNull(y => "N4" + y + "   ", x.BillingService.Ndc.IfNotNull(z => "N4" + z + "   ", () => "")) +
                            x.BillingService.UnclassifiedServiceDescription.IfNotNull(y => y + "   ", () => "") +
                            x.BillingService.ClaimComment.IfNotNull(y => y, () => "")
            } :
                                                                                                                                                            new PaperClaimServiceInfo
                                                                                                                                                            {
                                                                                                                                                                TransactionId = x.Id,
                                                                                                                                                                RevenueCode = "",
                                                                                                                                                                ServiceDescription = x.BillingService.EncounterService.Description,
                                                                                                                                                                ServiceCode = x.BillingService.EncounterService.Code,
                                                                                                                                                                ServiceCodeModifier = String.Join("", x.BillingService.BillingServiceModifiers.Select(y => y.ServiceModifier.IfNotNull(z => z.Code, () => ""))),
                                                                                                                                                                ServiceDate = invoice.Encounter.StartDateTime,
                                                                                                                                                                ServiceUnits = FormatServiceUnits(x.BillingService),
                                                                                                                                                                ServiceCharge = (x.BillingService.UnitCharge * x.BillingService.Unit ?? 0.00m),
                                                                                                                                                                ServiceUnitOfMeasurement = x.BillingService.EncounterService.ServiceUnitOfMeasurement,
                                                                                                                                                                EncounterServiceType = x.BillingService.EncounterService.EncounterServiceType.IfNotNull(y => y.Name, () => ""),
                                                                                                                                                                UnclassifiedServiceDescription = x.BillingService.UnclassifiedServiceDescription,
                                                                                                                                                                Emg = x.BillingService.IsEmg ? "Y" : "",
                                                                                                                                                                Epsdt = x.BillingService.IsEpsdt && x.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Medicaid ? "Y" : "",
                                                                                                                                                                Line24 = x.BillingService.EncounterService.Ndc.IfNotNull(y => "N4" + y + "   ", x.BillingService.Ndc.IfNotNull(z => "N4" + z + "   ", () => "")) +
                                                                                                                                                                        x.BillingService.UnclassifiedServiceDescription.IfNotNull(y => y + "   ", () => "") +
                                                                                                                                                                        x.BillingService.ClaimComment.IfNotNull(y => y, () => "")
                                                                                                                                                            }
                                                                                                                                                            ).ToList();


            var totalCharges = source.Where(x => x.InvoiceReceivable.PatientInsurance != null).Sum(x => x.BillingService.UnitCharge * x.BillingService.Unit) ?? 0.00m;
            destination.TotalCharges = totalCharges;

            var renderingDocNpi = invoice.BillingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi);
            destination.RenderingProviderNpi = renderingDocNpi;

            var displayName = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceReferral.IfNotNull(x => x.ExternalProvider.IfNotNull(y => y.DisplayName, () => ""), () => "");
            var firstName = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceReferral.IfNotNull(x => x.ExternalProvider.IfNotNull(y => y.FirstName, () => ""), () => "");
            var lastName = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceReferral.IfNotNull(x => x.ExternalProvider.IfNotNull(y => y.LastNameOrEntityName, () => ""), () => "");
            if (string.IsNullOrEmpty(displayName))
            {
                displayName = invoice.ReferringExternalProvider.IfNotNull(x => x.DisplayName, () => "");
                firstName = invoice.ReferringExternalProvider.IfNotNull(x => x.FirstName, () => "");
                lastName = invoice.ReferringExternalProvider.IfNotNull(x => x.LastNameOrEntityName, () => "");
                if (string.IsNullOrEmpty(displayName))
                {
                    if (source.Any(x => x.BillingService.EncounterService != null && x.BillingService.EncounterService.IsOrderingProviderRequiredOnClaim))
                    {
                        displayName = invoice.BillingProvider.User.IfNotNull(x => x.DisplayName, "");
                        firstName = invoice.BillingProvider.User.IfNotNull(x => x.FirstName, "");
                        lastName = invoice.BillingProvider.User.IfNotNull(x => x.LastName, "");
                    }
                }
            }

            destination.ReferringDoctorFullName = displayName;
            destination.ReferringDoctorFirstName = firstName;
            destination.ReferringDoctorLastName = lastName;

            var npi = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceReferral.IfNotNull(x => x.ExternalProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi));
            if (string.IsNullOrEmpty(npi))
            {
                npi = invoice.ReferringExternalProvider.IfNotNull(x => x.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi));
                if (string.IsNullOrEmpty(npi))
                {
                    if (source.Any(x => x.BillingService.EncounterService != null && x.BillingService.EncounterService.IsOrderingProviderRequiredOnClaim))
                    {
                        npi = renderingDocNpi;
                    }
                }
            }

            destination.ReferringDoctorNpi = npi;

            #endregion

            #region Ub04 Logic
            var billingOrganizationPayToAddress = invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PayTo);
            if (billingOrganizationPayToAddress != null)
            {
                destination.PayToName = invoice.BillingProvider.BillingOrganization.Name;
                destination.PayToAddressLine1 = billingOrganizationPayToAddress.Line1;
                destination.PayToAddressLine2 = billingOrganizationPayToAddress.Line2;
                destination.PayToCity = billingOrganizationPayToAddress.City;
                destination.PayToState = billingOrganizationPayToAddress.StateOrProvince.Abbreviation;
                destination.PayToZip = billingOrganizationPayToAddress.PostalCode;
            }

            destination.AccidentState = invoice.InvoiceSupplemental.IfNotNull(x => x.AccidentStateOrProvince.IfNotNull(y => y.Abbreviation, () => ""), () => "");

            // get ordered insurance policies
            var orderedInvoiceReceivablesWithPatientInsurance = invoice.InvoiceReceivables.Where(x => x.PatientInsurance != null && x.PatientInsurance.InsuranceType == patientInsuranceReference.InsuranceType
                            && x.PatientInsurance.IsActiveForDateTime(invoice.Encounter.StartDateTime)).OrderBy(x => x.PatientInsurance.OrdinalId).ToArray();

            // get co-insurance codes and amounts
            var primaryFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance.First().FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Coinsurance);
            var primCoAmount = primaryFinancialInformations.Sum(x => x.Amount);
            destination.PrimaryCoinsuranceAmount = primCoAmount > 0 ? primCoAmount.ToString("F") : "";

            // if primary co-insurance amount has a value print 'A2' for primary insurance code. What qualifies as 'has a value'? The 'Amount' field is required in code but nullable in the database
            destination.PrimaryCoinsuranceCode = primCoAmount > 0 ? "A2" : "";

            // if secondary co insurance exists
            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 1)
            {
                var secondaryCoInsuranceFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance[1].FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Coinsurance);
                var secCoAmount = secondaryCoInsuranceFinancialInformations.Sum(x => x.Amount);
                destination.SecondaryCoinsuranceAmount = secCoAmount > 0 ? secCoAmount.ToString("F") : "";
                destination.SecondaryCoinsuranceCode = secCoAmount > 0 ? "B2" : "";
            }

            // if third co insurance exists
            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 2)
            {
                var thirdCoInsuranceFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance[2].FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Coinsurance);
                var thirdCoAmount = thirdCoInsuranceFinancialInformations.Sum(x => x.Amount);
                destination.ThirdCoinsuranceAmount = thirdCoAmount > 0 ? thirdCoAmount.ToString("F") : "";
                destination.ThirdCoinsuranceCode = thirdCoAmount > 0 ? "C2" : "";
            }

            // get deductible insurance codes and amounts
            primaryFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance.First().FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Deductible);
            primCoAmount = primaryFinancialInformations.Sum(x => x.Amount);
            destination.PrimaryDeductibleAmount = primCoAmount > 0 ? primCoAmount.ToString("F") : "";
            destination.PrimaryDeductibleCode = primCoAmount > 0 ? "A1" : "";

            // if secondary co insurance exists
            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 1)
            {
                var secondaryCoInsuranceFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance[1].FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Deductible);
                var secCoAmount = secondaryCoInsuranceFinancialInformations.Sum(x => x.Amount);
                destination.SecondaryDeductibleAmount = secCoAmount > 0 ? secCoAmount.ToString("F") : "";
                destination.SecondaryDeductibleCode = secCoAmount > 0 ? "B1" : "";
            }

            // if third co insurance exists
            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 2)
            {
                var thirdCoInsuranceFinancialInformations = orderedInvoiceReceivablesWithPatientInsurance[2].FinancialInformations.Where(x => x.FinancialInformationTypeId == (int)FinancialInformationTypeId.Deductible);
                var thirdCoAmount = thirdCoInsuranceFinancialInformations.Sum(x => x.Amount);
                destination.ThirdDeductibleAmount = thirdCoAmount > 0 ? thirdCoAmount.ToString("F") : "";
                destination.ThirdDeductibleCode = thirdCoAmount > 0 ? "C1" : "";
            }

            destination.PrintDate = DateTime.Now.ToClientTime();

            var primaryPatientInsurance = orderedInvoiceReceivablesWithPatientInsurance.First().PatientInsurance;
            destination.PrimaryPayerName = primaryPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.Name, () => "");
            destination.PrimaryPayerCode = primaryPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.PayerCode, () => "");
            destination.PrimaryInsuredFirstName = primaryPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.FirstName, () => ""); // insured patient is null during unit test data. Am i pulling from the correct source?
            destination.PrimaryInsuredLastName = primaryPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.LastName, () => "");
            destination.PrimaryRelationshipCode = GetRelationshipCode(primaryPatientInsurance.PolicyholderRelationshipType);
            destination.PrimaryPolicyNumber = primaryPatientInsurance.InsurancePolicy.PolicyCode;
            var primaryPayment = orderedInvoiceReceivablesWithPatientInsurance.First().Adjustments.Where(x => x.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && x.FinancialSourceType == FinancialSourceType.Insurer).Sum(x => x.Amount);
            destination.PrimaryPayment = primaryPayment.ToString("F");
            destination.PrimaryBalanceDue = patientInsuranceReference.InsurancePolicy.InsurerId == primaryPatientInsurance.InsurancePolicy.InsurerId ? (totalCharges - primaryPayment).ToString("F")
                                                  : "0.00";

            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 1)
            {
                var secondaryPatientInsurance = orderedInvoiceReceivablesWithPatientInsurance[1].PatientInsurance;
                destination.SecondaryPayerName = secondaryPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.Name, () => "");
                destination.SecondaryPayerCode = secondaryPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.PayerCode, () => "");
                destination.SecondaryInsuredFirstName = secondaryPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.FirstName, () => "");
                destination.SecondaryInsuredLastName = secondaryPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.LastName, () => "");
                destination.SecondaryRelationshipCode = GetRelationshipCode(secondaryPatientInsurance.PolicyholderRelationshipType);
                destination.SecondaryPolicyNumber = secondaryPatientInsurance.InsurancePolicy.PolicyCode;
                var secondaryPayment = orderedInvoiceReceivablesWithPatientInsurance[1].Adjustments.Where(x => x.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && x.FinancialSourceType == FinancialSourceType.Insurer).Sum(x => x.Amount);
                destination.SecondaryPayment = secondaryPayment.ToString("F");
                destination.SecondaryBalanceDue = patientInsuranceReference.InsurancePolicy.InsurerId == secondaryPatientInsurance.InsurancePolicy.InsurerId ? (totalCharges - secondaryPayment).ToString("F")
                    : "0.00";
            }

            if (orderedInvoiceReceivablesWithPatientInsurance.Length > 2)
            {
                var thirdPatientInsurance = orderedInvoiceReceivablesWithPatientInsurance[2].PatientInsurance;
                destination.ThirdPayerName = thirdPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.Name, () => "");
                destination.ThirdPayerCode = thirdPatientInsurance.InsurancePolicy.Insurer.IfNotNull(x => x.PayerCode, () => "");
                destination.ThirdInsuredFirstName = thirdPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.FirstName, () => "");
                destination.ThirdInsuredLastName = thirdPatientInsurance.InsurancePolicy.PolicyholderPatient.IfNotNull(x => x.LastName, () => "");
                destination.ThirdRelationshipCode = GetRelationshipCode(thirdPatientInsurance.PolicyholderRelationshipType);
                destination.ThirdPolicyNumber = thirdPatientInsurance.InsurancePolicy.PolicyCode;
                var thirdPayment = orderedInvoiceReceivablesWithPatientInsurance[2].Adjustments.Where(x => x.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && x.FinancialSourceType == FinancialSourceType.Insurer).Sum(x => x.Amount);
                destination.ThirdPayment = thirdPayment.ToString("F");
                destination.ThirdBalanceDue = patientInsuranceReference.InsurancePolicy.InsurerId == thirdPatientInsurance.InsurancePolicy.InsurerId ? (totalCharges - thirdPayment).ToString("F")
                    : "0.00";
            }

            destination.AssignBenefits = invoice.HasPatientAssignedBenefits;
            destination.ReleaseInfoIsSigned = !invoice.IsReleaseOfInformationNotSigned;
            destination.PrincipalProcedureCode = source.Where(x => x.BillingService.OrdinalId == 1).Select(x => x.BillingService.EncounterService.IfNotNull(y => y.Code, "")).FirstOrDefault(); // check is correct            
            destination.DisplayDecimal = insurer.IsIcd10Decimal;

            #endregion

            #region Cms1500 Logic

            if (insurer != null)
            {
                destination.InsuranceTypeIsMedicare = (insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB);
                destination.InsuranceTypeIsMedicaid = insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Medicaid;
                destination.InsuranceTypeIsTricare = insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.Champus;
                destination.InsuranceTypeIsGroup = insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.CommercialInsurance;
                destination.InsuranceTypeIsOther = !destination.InsuranceTypeIsMedicare && !destination.InsuranceTypeIsMedicaid && !destination.InsuranceTypeIsTricare && !destination.InsuranceTypeIsGroup;
                destination.IsTaxonomyIncludedOnClaims = insurer.IsTaxonomyIncludedOnClaims;
            }

            destination.DiagnosisTypeId = invoice.DiagnosisTypeId == 2 ? "0" : "9";
            destination.BilledPolicyHolderPolicyNumber = patientInsuranceReference.InsurancePolicy.PolicyCode;
            destination.BilledPolicyHolderMiddleName = policyHolder.MiddleName;
            destination.BilledPolicyHolderTelephone = policyHolder.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).IfNotNull(GetPhoneNumberFormattedValue, () => "");
            destination.BilledPolicyHolderGroupId = destination.InsuranceTypeIsMedicare ? "NONE" : patientInsuranceReference.InsurancePolicy.GroupCode;
            destination.BilledPolicyHolderBirthDate = policyHolder.DateOfBirth;
            destination.BilledPolicyHolderGender = policyHolder.Gender;
            destination.BilledPolicyHolderEmployerName = policyHolder.EmployerName;

            // check is correct logic with michelle
            destination.BilledInsurancePlanName = destination.InsuranceTypeIsMedicare ? "" : insurer.IfNotNull(x => x.PlanName, () => "");
            destination.AnotherPlan = orderedInvoiceReceivablesWithPatientInsurance.Length > 1;

            destination.PatientMiddleName = invoice.Encounter.Patient.MiddleName;

            destination.PatientTelephone = invoice.Encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).IfNotNull(GetPhoneNumberFormattedValue, "");

            var relationshipType = patientInsuranceReference.PolicyholderRelationshipType;
            destination.BilledRelationshipSelf = relationshipType == PolicyHolderRelationshipType.Self;
            destination.BilledRelationshipSpouse = relationshipType == PolicyHolderRelationshipType.Spouse;
            destination.BilledRelationshipChild = relationshipType == PolicyHolderRelationshipType.Child;
            destination.BilledRelationshipOther = relationshipType != PolicyHolderRelationshipType.Self
                && relationshipType != PolicyHolderRelationshipType.Spouse && relationshipType != PolicyHolderRelationshipType.Child;

            destination.MaritalStatusSingle = invoice.Encounter.Patient.MaritalStatus == MaritalStatus.Single;
            destination.MaritalStatusMarried = invoice.Encounter.Patient.MaritalStatus == MaritalStatus.Married;
            destination.MaritalStatusOther = invoice.Encounter.Patient.MaritalStatus != MaritalStatus.Single
                && invoice.Encounter.Patient.MaritalStatus != MaritalStatus.Married;

            InsurancePolicy otherInsurancePolicy = null;
            if (patientInsuranceReference.InsurancePolicyId == orderedInvoiceReceivablesWithPatientInsurance.First().PatientInsurance.InsurancePolicyId)
            {
                if (orderedInvoiceReceivablesWithPatientInsurance.Length > 1)
                {
                    // check to make sure insurance is active for invoice datetime and that the type of insurance matches
                    otherInsurancePolicy = orderedInvoiceReceivablesWithPatientInsurance.Skip(1).FirstOrDefault(x => x.PatientInsurance.InsuranceType == patientInsuranceReference.InsuranceType).IfNotNull(x => x.PatientInsurance.InsurancePolicy);
                }
            }
            else
            {
                otherInsurancePolicy = orderedInvoiceReceivablesWithPatientInsurance.First().PatientInsurance.InsurancePolicy;
            }

            if (otherInsurancePolicy != null)
            {
                destination.OtherPolicyHolderPolicyNumber = otherInsurancePolicy.PolicyCode;
                destination.OtherPolicyHolderLastName = otherInsurancePolicy.PolicyholderPatient.LastName;
                destination.OtherPolicyHolderFirstName = otherInsurancePolicy.PolicyholderPatient.FirstName;
                destination.OtherPolicyHolderMiddleName = otherInsurancePolicy.PolicyholderPatient.MiddleName;
                var address = otherInsurancePolicy.PolicyholderPatient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
                if (address != null)
                {
                    destination.OtherPolicyHolderAddressLine1 = address.Line1;
                    destination.OtherPolicyHolderAddressLine2 = address.Line2;
                    destination.OtherPolicyHolderCity = address.City;
                    destination.OtherPolicyHolderState = address.StateOrProvince.Abbreviation;
                    destination.OtherPolicyHolderZip = address.PostalCode;
                }

                destination.OtherPolicyHolderTelephone = otherInsurancePolicy.PolicyholderPatient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).IfNotNull(GetPhoneNumberFormattedValue, "");
                destination.OtherPolicyHolderGroupId = otherInsurancePolicy.GroupCode;
                destination.OtherPolicyHolderBirthDate = otherInsurancePolicy.PolicyholderPatient.DateOfBirth;
                destination.OtherPolicyHolderGender = otherInsurancePolicy.PolicyholderPatient.Gender;
                destination.OtherPolicyHolderEmployerName = otherInsurancePolicy.PolicyholderPatient.EmployerName;
                destination.OtherInsuranceName = otherInsurancePolicy.Insurer.Name;
            }

            destination.EmploymentRelated = invoice.InvoiceSupplemental != null && invoice.InvoiceSupplemental.HasEmploymentRelatedCause;

            destination.AutoAccident = invoice.InvoiceSupplemental != null && invoice.InvoiceSupplemental.HasAutoAccidentRelatedCause;

            destination.OtherAccident = invoice.InvoiceSupplemental != null && invoice.InvoiceSupplemental.HasOtherAccidentRelatedCause;

            destination.IllnessDate = invoice.InvoiceSupplemental.IfNotNull(x => x.AccidentDateTime);
            destination.AdmitDate = invoice.InvoiceSupplemental.IfNotNull(x => x.AdmissionDateTime);
            destination.DischargeDate = invoice.InvoiceSupplemental.IfNotNull(x => x.DischargeDateTime);
            destination.ResubmitCode = source.Select(x => x.InvoiceReceivable).First().PayerClaimControlNumber;
            destination.ClaimFrequencyTypeCodeId = source.Select(x => x.InvoiceReceivable).First().ClaimFrequencyTypeCodeId.ToStringIfNotNull();
            destination.AuthorizationCode = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceAuthorization.IfNotNull(x => x.AuthorizationCode, () => "");
            destination.RenderingProviderNpi = renderingDocNpi;
            destination.RenderingProviderTaxonomy = invoice.BillingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy);

            foreach (var comment in source.Select(x => x.BillingService).Select(x => x.Invoice.ClaimComment))
            {
                if (destination.Services.Any(x => x.UnclassifiedServiceDescription.IsNotNullOrEmpty() && x.UnclassifiedServiceDescription.Equals(comment, StringComparison.OrdinalIgnoreCase)))
                {
                    continue;
                }

                destination.ServiceNote = comment;
                break;
            }

            foreach (var service in destination.Services)
            {
                var x = source.Single(y => y.Id == service.TransactionId);
                service.ServiceLocationCode = x.InvoiceReceivable.Invoice.ServiceLocationCode.IfNotNull(y => y.PlaceOfServiceCode, () => "");
                var letters = new StringCollection();
                x.BillingService.BillingServiceBillingDiagnoses.OrderBy(bd => bd.OrdinalId).ForEach(bd => letters.Add(DiagnosisLetters[destination.Diagnoses.IndexOf(d => bd.BillingDiagnosisId == d.Id)]));
                service.DiagnosisLetters = letters.Join(" ");
            }

            destination.ServiceDate = invoice.Encounter.StartDateTime;
            destination.BillingProviderTaxIdx = GetTrueFlagCharacter(destination.BillingProviderTaxId != string.Empty);

            if (string.IsNullOrEmpty(destination.BillingProviderTaxId))
            {
                destination.BillingProviderTaxId = billingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Ssn).IfNull(() => "");
                if (!string.IsNullOrEmpty(destination.BillingProviderTaxId)) destination.SsnIndicator = "X";
            }
            else
            {
                destination.EinIndicator = "X";
            }

            destination.HasAssignedBenefits = invoice.HasPatientAssignedBenefits;

            var applicableAdjustments = invoice.InvoiceReceivables.SelectMany(x => x.Adjustments).Where(x => (x.InsurerId == null || x.InsurerId != patientInsuranceReference.InsurancePolicy.InsurerId) && x.AdjustmentTypeId == (int)AdjustmentTypeId.Payment);

            // now make sure to include only adjustments that are linked to the services that are being printed on this claim
            var currentServicesIncludedOnClaim = source.Select(y => y.BillingServiceId);
            applicableAdjustments = applicableAdjustments.Where(x => x.BillingServiceId.HasValue && currentServicesIncludedOnClaim.Contains(x.BillingServiceId.Value));

            var dia = insurer.DoctorInsurerAssignments.FirstOrDefault(x => x.DoctorId == invoice.BillingProvider.UserId);
            if (dia != null && dia.SuppressPatientPayments)
            {
                applicableAdjustments = applicableAdjustments.Where(x => x.FinancialSourceType != FinancialSourceType.Patient);
            }

            var totalPaid = applicableAdjustments.Sum(y => y.Amount);
            destination.TotalPaid = totalPaid.ToString("F");
            var balanceDue = totalCharges - totalPaid;
            destination.BalanceDue = balanceDue.ToString("F");

            var serviceLocation = invoice.Encounter.ServiceLocation;

            if (!(invoice.InvoiceType == InvoiceType.Vision && destination.InsuranceTypeIsMedicare))
            {
                destination.LocationName = serviceLocation.IfNotNull(x => x.Name);
                var serviceLocationAddress = serviceLocation != null ? serviceLocation.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice) : null;
                if (serviceLocationAddress != null)
                {
                    destination.LocationAddressLine1 = serviceLocationAddress.Line1;
                    destination.LocationAddressLine2 = serviceLocationAddress.Line2;
                    destination.LocationCity = serviceLocationAddress.City;
                    destination.LocationState = serviceLocationAddress.StateOrProvince.Abbreviation;
                    destination.LocationZip = serviceLocationAddress.PostalCode;
                }
                destination.LocationNpi = serviceLocation != null ? serviceLocation.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi) : "";
            }
            destination.ProviderSignature = invoice.InvoiceType == InvoiceType.Facility ? invoice.BillingProvider.BillingOrganization.Name : invoice.BillingProvider.User.IfNotNull(x => x.DisplayName, () => "");
            destination.ProviderAssignBenefits = !invoice.IsAssignmentRefused;

            if (source.Any(x => x.BillingService.FacilityEncounterService != null ? x.BillingService.FacilityEncounterService.IsCliaCertificateRequiredOnClaim : x.BillingService.EncounterService.IsCliaCertificateRequiredOnClaim))
            {
                destination.Clia = billingProvider.BillingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.CliaCertNumber);
            }

            destination.ReferralNumber = source.Select(x => x.InvoiceReceivable).First().PatientInsuranceReferral.IfNotNull(x => x.ReferralCode);


            // assign default values for referring provider
            destination.ReferringOrOrderingOrSupervisingProviderFullName = destination.ReferringDoctorFullName;
            destination.ReferringOrOrderingOrSupervisingProviderNpi = destination.ReferringDoctorNpi;

            // use Ordering Provider, if there is no referring provider
            if (invoice.InvoiceSupplemental.IfNotNull(x => x.OrderingProvider.IfNotNull(y => true)) && invoice.ReferringExternalProvider == null)
            {
                string opFirstName = invoice.InvoiceSupplemental.OrderingProvider.IfNotNull(x => x.FirstName) ?? string.Empty;
                string opMiddleName = invoice.InvoiceSupplemental.OrderingProvider.IfNotNull(x => x.MiddleName) ?? string.Empty;
                string opLastName = invoice.InvoiceSupplemental.OrderingProvider.IfNotNull(x => x.LastNameOrEntityName);
                string opSuffix = invoice.InvoiceSupplemental.OrderingProvider.IfNotNull(x => x.Suffix) ?? string.Empty;
                string opFullName = string.Format("{0} {1} {2} {3}", opFirstName, opMiddleName, opLastName, opSuffix);
                string opNpi = invoice.InvoiceSupplemental.OrderingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).ToStringIfNotNull("");

                destination.ReferringOrOrderingOrSupervisingProviderFullName = opFullName.Truncate(26);
                destination.ReferringOrOrderingOrSupervisingProviderNpi = opNpi;
            }

            // use Supervising Provider, if there is no referring provider and no ordering provider
            if (invoice.InvoiceSupplemental.IfNotNull(x => x.SupervisingProvider.IfNotNull(y => true)) &&
                invoice.ReferringExternalProvider == null && invoice.InvoiceSupplemental.OrderingProvider == null)
            {
                string spFirstName = invoice.InvoiceSupplemental.SupervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.FirstName)) ?? string.Empty;
                string spMiddleName = invoice.InvoiceSupplemental.SupervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.MiddleName)) ?? string.Empty;
                string spLastName = invoice.InvoiceSupplemental.SupervisingProvider.IfNotNull(x => (x.User != null) ? x.User.LastName : x.ServiceLocation.Name);
                string spSuffix = invoice.InvoiceSupplemental.SupervisingProvider.IfNotNull(x => x.User.IfNotNull(y => y.Suffix)) ?? string.Empty;
                string spFullName = string.Format("{0} {1} {2} {3}", spFirstName, spMiddleName, spLastName, spSuffix);
                string spNpi = invoice.InvoiceSupplemental.SupervisingProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).ToStringIfNotNull("");

                destination.ReferringOrOrderingOrSupervisingProviderFullName = spFullName.Truncate(26);
                destination.ReferringOrOrderingOrSupervisingProviderNpi = spNpi;
            }

            #endregion
        }


        /// <summary>
        /// Get the character to output incase of true. False outputs no character.
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public string GetTrueFlagCharacter(bool flag)
        {
            return flag ? "X" : "";
        }


        private static string GetRelationshipCode(PolicyHolderRelationshipType type)
        {
            return X12.Common.PolicyHolderRelationshipTypeMappings[type];
        }

        private static string GetPhoneNumberFormattedValue(IPhoneNumber phoneNumber)
        {
            return phoneNumber.ToString().Replace("(", string.Empty).Replace(")", string.Empty).Replace("-", " ");
        }

        private static string FormatServiceUnits(BillingService billingService)
        {
            var units = billingService.Unit;

            if (!units.HasValue) return "1";

            if (units.Value >= 999.5m) throw new ValidationException(string.Format("The units for service {0} must be less than 4 characters", billingService.EncounterService.Code));

            if (units.Value.IsWholeNumber()) return Math.Round(units.Value, 0).ToString();

            if (units.Value >= 9.9m && units.Value < 999.5m) return Math.Round(units.Value, 0, MidpointRounding.AwayFromZero).ToString();

            if ((units.Value * 10).IsWholeNumber() && (units.Value * 10) <= 10) return units.Value.ToString(".#");

            if (units.Value < 1) return units.Value.ToString(".##");

            return Math.Round(units.Value, 1, MidpointRounding.AwayFromZero).ToString();
        }

        #region IMap<BillingServiceTransaction,PaperClaimInfo>

        public IEnumerable<IMapMember<IEnumerable<BillingServiceTransaction>, PaperClaim>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}
