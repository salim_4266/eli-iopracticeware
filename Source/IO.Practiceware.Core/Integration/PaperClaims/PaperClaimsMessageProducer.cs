﻿using System.Data;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.Common;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(PaperClaimsMessageProducer))]

namespace IO.Practiceware.Integration.PaperClaims
{

    public class PaperClaimsMessageProducer
    {
        protected IPracticeRepository PracticeRepository { get; private set; }
        protected IDocumentGenerationService DocumentGenerationService { get; private set; }

        private readonly IMapper<IEnumerable<BillingServiceTransaction>, PaperClaim> _mapper;

        /// <summary>
        /// Used for testing purposes only
        /// </summary>
        public bool IsTesting { get; set; }

        public PaperClaimsMessageProducer(IPracticeRepository practiceRepository, IDocumentGenerationService documentGenerationService, IMapper<IEnumerable<BillingServiceTransaction>, PaperClaim> mapper)
        {
            PracticeRepository = practiceRepository;
            DocumentGenerationService = documentGenerationService;
            _mapper = mapper;
        }

        private IEnumerable<BillingServiceTransaction> LoadAllInvoicesForBillingServiceTransactions(IEnumerable<Guid> billingServiceTransactionIds, bool includeSentStatuses = false)
        {
            var invoiceIds = PracticeRepository.BillingServiceTransactions.Where(i => billingServiceTransactionIds.Contains(i.Id)).
                    Select(bst => bst.BillingService.InvoiceId).Distinct().ToArray();

            var invoices = PracticeRepository.Invoices.Where(i => invoiceIds.Contains(i.Id)).ToArray();

            invoices.LoadForClaimFileGeneration(PracticeRepository);

            foreach (var invoice in invoices.Where(i => i.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10))
            {
                IntegrationCommon.ReplaceInvoiceWithIcd10(invoice);
            }

            var billingServiceTransactions = invoices
                 .SelectMany(i => i.InvoiceReceivables)
                 .SelectMany(ir => ir.BillingServiceTransactions)
                 .Where(bst => billingServiceTransactionIds.Contains(bst.Id));

            if (includeSentStatuses)
            {
                billingServiceTransactions = billingServiceTransactions.Where(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                                                                                     || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Wait
                                                                                     || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Sent
                                                                                     || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.SentCrossOver);
            }
            else
            {
                billingServiceTransactions = billingServiceTransactions.Where(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                                                                                     || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.SentCrossOver);
            }

            return billingServiceTransactions.Distinct().ToArray();
        }

        private BinaryDocument ProduceMessageWord(PaperClaim paperClaimInfo, byte[] documentTemplate)
        {
            var mergedFile = DocumentGenerationService.PerformMailMerge(documentTemplate, paperClaimInfo);

            var doc = new BinaryDocument();
            doc.Content = mergedFile;
            return doc;
        }

        public Document ProduceMessage(IEnumerable<Guid> billingServiceTransactionIds, int claimFormTypeId, bool includeSentStatuses = false)
        {
            var claimFormType = PracticeRepository.ClaimFormTypes.Include(x => x.TemplateDocument).SingleOrDefault(x => x.Id == claimFormTypeId);

            if (claimFormType == null) return null;

            return ProduceMessage(billingServiceTransactionIds, claimFormType.TemplateDocument.GetContent(), claimFormType.TemplateDocument.ContentType, includeSentStatuses);
        }

        /// <summary>
        /// For Vb6 Call
        /// </summary>
        /// <param name="billingServiceTransactionIds"></param>
        /// <param name="claimFormTypeId"></param>
        /// <param name="includeSentStatuses"></param>
        /// <returns></returns>
        public Document ProduceMessage(IEnumerable<string> billingServiceTransactionIds, int claimFormTypeId, bool includeSentStatuses = false)
        {
            return ProduceMessage(billingServiceTransactionIds.Select(i => new Guid(i.ToString())), claimFormTypeId, includeSentStatuses);
        }

        public Document ProduceMessage(PaperClaim paperClaimInfo, int claimFormTypeId)
        {
            var claimFormType = PracticeRepository.ClaimFormTypes.Include(x => x.TemplateDocument).SingleOrDefault(x => x.Id == claimFormTypeId);

            if (claimFormType == null) return null;

            return ProduceMessage(paperClaimInfo, claimFormType.TemplateDocument.GetContent(), claimFormType.TemplateDocument.ContentType);
        }

        public Dictionary<long, Document> ProduceMessageMany(Dictionary<long, List<Guid>> transactionIdsByInvoiceReceivableId, object template, ContentType contentType, bool includeSentStatuses = false)
        {
            var paperClaimInfo = ProducePaperClaimInfoMany(transactionIdsByInvoiceReceivableId, includeSentStatuses);


            var list = new List<Tuple<long, PaperClaim>>(); // seems weird but doing this to keep the order in tact.
            foreach (var information in paperClaimInfo)
            {
                list.Add(new Tuple<long, PaperClaim>(information.Key, information.Value));
            }

            var documents = ProduceMessageMany(list.Select(x => x.Item2), template, contentType);

            var dict = new Dictionary<long, Document>();

            for (int i = 0; i < documents.Count; i++)
            {
                dict.Add(list[i].Item1, documents[i]);
            }

            return dict;
        }

        public List<Document> ProduceMessageMany(IEnumerable<PaperClaim> paperClaimInfo, object template, ContentType contentType)
        {
            if (contentType == ContentType.RazorTemplate)
            {
                return ProduceMessageManyHtml(paperClaimInfo, (string)template).ToList<Document>();
            }

            return null;
        }

        private IEnumerable<HtmlDocument> ProduceMessageManyHtml(IEnumerable<PaperClaim> paperClaimInfo, string template)
        {
            var htmlDocs = new List<HtmlDocument>();

            var mergedFiles = DocumentGenerationService.CompileRazorAndRun(template, paperClaimInfo);

            // Here we look for multiple html pages by splitting the message into groups of <html>....</html> content.
            const string htmlEndTag = "</html>";

            foreach (var mergedFile in mergedFiles)
            {
                var htmlDoc = new HtmlDocument();
                var index = 0;
                while (index > -1)
                {
                    if (index > mergedFile.Length - 1) break; // index out of bounds

                    var index2 = mergedFile.IndexOf(htmlEndTag, index, StringComparison.OrdinalIgnoreCase);
                    if (index2 > -1)
                    {
                        var length = (index2 + htmlEndTag.Length) - index;
                        htmlDoc.Content.Add(mergedFile.Substring(index, length));
                        index = index2 + htmlEndTag.Length;
                    }
                    else
                    {
                        break;
                    }
                }

                htmlDocs.Add(htmlDoc);
            }

            return htmlDocs;
        }


        public Document ProduceMessage(IEnumerable<Guid> billingServiceTransactionIds, object template, ContentType contentType, bool includeSentStatuses = false)
        {
            var paperClaimInfo = ProducePaperClaimInfo(billingServiceTransactionIds, includeSentStatuses);

            return ProduceMessage(paperClaimInfo, template, contentType);
        }

        public Document ProduceMessage(PaperClaim paperClaimInfo, object template, ContentType contentType)
        {
            if (contentType == ContentType.RazorTemplate)
            {
                return ProduceMessageHtml(paperClaimInfo, (string)template);
            }
            if (contentType == ContentType.WordDocument)
            {
                return ProduceMessageWord(paperClaimInfo, template.CastTo<byte[]>());
            }

            return null;
        }



        private HtmlDocument ProduceMessageHtml(PaperClaim viewModel, string template)
        {
            var htmlDoc = new HtmlDocument();

            // TEST CODE FOR GENERATING MORE THAN 6 SERVICES WHICH WILL FORCE AN ADDITIONAL HTML PAGE TO BE GENERATED
            //------------------------------------------------------------
            //var listTemp = new List<PaperClaimServiceViewModel>();
            //for (int i = 0; i < 5; i++)
            //{
            //    foreach (var s in viewModel.Services)
            //    {
            //        listTemp.Add(s);
            //    }
            //}
            //viewModel.Services = listTemp;
            //----------------------------------------------------------                        

            var mergedFile = DocumentGenerationService.CompileRazorAndRun(template, viewModel);

            // Here we look for multiple html pages by splitting the message into groups of <html>....</html> content.
            const string htmlEndTag = "</html>";
            var index = 0;
            while (index > -1)
            {
                if (index > mergedFile.Length - 1) break; // index out of bounds

                var index2 = mergedFile.IndexOf(htmlEndTag, index, StringComparison.OrdinalIgnoreCase);
                if (index2 > -1)
                {
                    var length = (index2 + htmlEndTag.Length) - index;
                    htmlDoc.Content.Add(mergedFile.Substring(index, length));
                    index = index2 + htmlEndTag.Length;
                }
                else
                {
                    break;
                }
            }


            return htmlDoc;
        }

        private Dictionary<long, PaperClaim> ProducePaperClaimInfoMany(Dictionary<long, List<Guid>> transactionIdsByInvoiceReceivableId, bool includeSentStatuses = false)
        {
            var billingServiceTransactions = LoadAllInvoicesForBillingServiceTransactions(transactionIdsByInvoiceReceivableId.SelectMany(x => x.Value), includeSentStatuses);

            var groupedByInvoiceReceivable = billingServiceTransactions.GroupBy(x => x.InvoiceReceivableId);

            var dict = new Dictionary<long, PaperClaim>();

            foreach (var group in groupedByInvoiceReceivable)
            {
                dict.Add(group.Key, _mapper.Map(group));
            }

            if (IsTesting)
            {
                dict.ForEachWithSinglePropertyChangedNotification(x =>
                                       {
                                           x.Value.PatientId = "999";
                                           x.Value.Services = x.Value.Services.OrderBy(y => y.ServiceCode).ThenBy(y => y.ServiceDate).ToList();
                                           x.Value.PrintDate = new DateTime(2012, 11, 28);
                                           x.Value.SsnIndicator = "X";
                                       });

            }

            return dict;
        }

        private PaperClaim ProducePaperClaimInfo(IEnumerable<Guid> billingServiceTransactionIds, bool includeSentStatuses = false)
        {
            var billingServiceTransactions = LoadAllInvoicesForBillingServiceTransactions(billingServiceTransactionIds, includeSentStatuses);

            var paperClaimInfo = _mapper.Map(billingServiceTransactions);

            if (IsTesting)
            {
                paperClaimInfo.PatientId = "999";
                paperClaimInfo.Services = paperClaimInfo.Services.OrderBy(x => x.ServiceCode).ThenBy(x => x.ServiceDate).ToList();
                paperClaimInfo.PrintDate = new DateTime(2012, 11, 28);
                paperClaimInfo.SsnIndicator = "X";
            }

            return paperClaimInfo;
        }

    }
}
