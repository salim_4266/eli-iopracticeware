﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using System.Text;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    ///     Helpers for working with CDAs in general.
    /// </summary>
    public static class CdaExtensions
    {
        /// <summary>
        /// Gets the root and extension as a pair, based off the entity primary key, PracticeRepositoryEntityId, ClientId
        /// </summary>
        /// <param name="hasId">The has identifier.</param>
        /// <param name="entityKeySuffix">The entity key suffix.</param>
        /// <param name="clientId">The client identifier.</param>
        /// <returns></returns>
        public static RootAndExtension GetOidRootAndExtension(this IHasId hasId, string entityKeySuffix = null, string clientId = null)
        {
            Type type = hasId.GetType();
            while (type.BaseType != null && type.BaseType != typeof(object)) type = type.BaseType;

            var entityId = (int)type.Name.ToEnumFromDisplayNameOrDefault<PracticeRepositoryEntityId>().EnsureNotDefault("Could not find PracticeRepositoryEntity for {0}.".FormatWith(hasId.GetType().Name));

            if (clientId == null)
            {
                clientId = UserContext.Current.IfNotNull(c => c.UserDetails.ClientId);
                // ensure client id is numeric (required to be part of the OID namespace).
                if (clientId != null && !clientId.All(c => Char.IsNumber(c) || c == '.')) clientId = new BigInteger(Encoding.ASCII.GetBytes(clientId)).ToString();
            }

            var root = String.Join(".", new[] { Oids.IOPracticeware, clientId, entityId.ToString() }.WhereNotDefault());

            string entityKey = hasId.Id.ToString();

            if (entityKeySuffix != null) entityKey += entityKeySuffix;
            return new RootAndExtension(root, entityKey);
        }

        public static string Oid(this CodeSystem codeSystem)
        {
            return codeSystem.GetAttribute<DisplayAttribute>().ShortName;
        }

        public static string Name(this CodeSystem codeSystem)
        {
            return codeSystem.GetAttribute<DisplayAttribute>().Name;
        }

        public static string Oid(this QualityDataModelValueSet valueSet)
        {
            return valueSet.GetAttribute<DisplayAttribute>().ShortName;
        }

        public static string Name(this QualityDataModelValueSet valueSet)
        {
            return valueSet.GetAttribute<DisplayAttribute>().Name;
        }

        /// <summary>
        /// Formats a DateTime for CDA using yyyyMMddHHmmss
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FormatForCda(this DateTime date)
        {
            var baseFormattedString = String.Format("{0:yyyyMMddHHmmss}", date);
            var timeZone = String.Format("{0:zz}00", date);
            return baseFormattedString + timeZone;
        }

        /// <summary>
        /// Formats a DateTime for use in the CDA text elements.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FormatForCdaTextElement(this DateTime date)
        {
            return date.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Formats a DateTime for CDA using yyyyMMdd
        /// </summary>
        /// <remarks>
        /// This should be used all other dateTime format 'alternatives' do not apply.
        /// The FormatForCda function is explicit, and FormatForCdaTextElement is used for Text elements.
        /// This function is used for all other cases.
        /// </remarks>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FormatForCdaAlternate(this DateTime date)
        {

            return date.ToString("yyyyMMdd");
        }

        /// <summary>
        /// Formats a DateTime for CDA using yyyyMMddHHmm
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FormatForCdaWithHoursMinutesAndSeconds(this DateTime date)
        {

            return date.ToString("yyyyMMddHHmmss");
        }
    }

    /// <summary>
    /// A root/extension pair for use in CDA files.
    /// </summary>
    public class RootAndExtension : IEquatable<RootAndExtension>
    {
        public string Root { get; set; }
        public string Extension { get; set; }

        public RootAndExtension() { }

        public RootAndExtension(string root, string extension)
        {
            Root = root;
            Extension = extension;
        }

        public bool Equals(RootAndExtension other)
        {
            return other != null && other.Root == Root && other.Extension == Extension;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as RootAndExtension);
        }

        public override int GetHashCode()
        {
            return Root.GetHashCode() ^ Extension.GetHashCode();
        }
    }
}
