﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Cda;
using Soaf.ComponentModel;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    /// A model that a QRDA Category I file.
    /// </summary>
    public class QrdaCategory1Model : QrdaCommonModel
    {
        public Patient Patient { get; set; }
        public ElectronicClinicalQualityMeasure ElectronicClinicalQualityMeasure { get; set; }
        public CdaModelMapper Mapper { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel> CommunicationProviderToProviderEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeModel> QrdaCategory1EntryWithValueSetEntryAndDateRangeModels { get; set; }

        [Dependency]
        public IEnumerable<PlanOfCareActivityActInterventionOrderEntryModel> PlanOfCareActivityActInterventionOrderEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeModel> EncounterActivitiesEncounterPerformedEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel> ProcedureActivityPerformedProcedurePerformedEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeModel> ProcedureActivityActInterventionPerformedEntryModels { get; set; }

        [Dependency]
        public IEnumerable<ProblemObservationDiagnosisActiveEntryModel> ProblemObservationDiagnosisActiveEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel> MedicationActivityMedicationActiveEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel> ResultObservationDiagnosticStudyResultEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel> ProcedureActivityObservationDiagnosticStudyPerformedEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel> TobaccoUseEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel> AssessmentScaleObservationRiskCategoryAssessmentEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel> PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeModel> ProcedureActivityObservationPhysicalExamPerformedEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel> ResultObservationPhysicalExamFindingEntryModels { get; set; }

        [Dependency]
        public IEnumerable<QrdaCategory1EntryWithValueSetEntryModel> SupplementalDataPatientPayerEntryModels { get; set; }

        public void MergeEntries(QrdaCategory1Model other)
        {
            CommunicationProviderToProviderEntryModels = CommunicationProviderToProviderEntryModels.Concat(other.CommunicationProviderToProviderEntryModels).Distinct().ToArray();

            QrdaCategory1EntryWithValueSetEntryAndDateRangeModels = QrdaCategory1EntryWithValueSetEntryAndDateRangeModels.Concat(other.QrdaCategory1EntryWithValueSetEntryAndDateRangeModels).Distinct().ToArray();

            PlanOfCareActivityActInterventionOrderEntryModels = PlanOfCareActivityActInterventionOrderEntryModels.Concat(other.PlanOfCareActivityActInterventionOrderEntryModels).Distinct().ToArray();

            EncounterActivitiesEncounterPerformedEntryModels = EncounterActivitiesEncounterPerformedEntryModels.Concat(other.EncounterActivitiesEncounterPerformedEntryModels).Distinct().ToArray();

            ProcedureActivityPerformedProcedurePerformedEntryModels = ProcedureActivityPerformedProcedurePerformedEntryModels.Concat(other.ProcedureActivityPerformedProcedurePerformedEntryModels).Distinct().ToArray();

            ProcedureActivityActInterventionPerformedEntryModels = ProcedureActivityActInterventionPerformedEntryModels.Concat(other.ProcedureActivityActInterventionPerformedEntryModels).Distinct().ToArray();

            ProblemObservationDiagnosisActiveEntryModels = ProblemObservationDiagnosisActiveEntryModels.Concat(other.ProblemObservationDiagnosisActiveEntryModels).Distinct().ToArray();

            MedicationActivityMedicationActiveEntryModels = MedicationActivityMedicationActiveEntryModels.Concat(other.MedicationActivityMedicationActiveEntryModels).Distinct().ToArray();

            ResultObservationDiagnosticStudyResultEntryModels = ResultObservationDiagnosticStudyResultEntryModels.Concat(other.ResultObservationDiagnosticStudyResultEntryModels).Distinct().ToArray();

            ProcedureActivityObservationDiagnosticStudyPerformedEntryModels = ProcedureActivityObservationDiagnosticStudyPerformedEntryModels.Concat(other.ProcedureActivityObservationDiagnosticStudyPerformedEntryModels).Distinct().ToArray();

            TobaccoUseEntryModels = TobaccoUseEntryModels.Concat(other.TobaccoUseEntryModels).Distinct().ToArray();

            AssessmentScaleObservationRiskCategoryAssessmentEntryModels = AssessmentScaleObservationRiskCategoryAssessmentEntryModels.Concat(other.AssessmentScaleObservationRiskCategoryAssessmentEntryModels).Distinct().ToArray();

            PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels = PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels.Concat(other.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels).Distinct().ToArray();

            ProcedureActivityObservationPhysicalExamPerformedEntryModels = ProcedureActivityObservationPhysicalExamPerformedEntryModels.Concat(other.ProcedureActivityObservationPhysicalExamPerformedEntryModels).Distinct().ToArray();

            ResultObservationPhysicalExamFindingEntryModels = ResultObservationPhysicalExamFindingEntryModels.Concat(other.ResultObservationPhysicalExamFindingEntryModels).Distinct().ToArray();

            SupplementalDataPatientPayerEntryModels = SupplementalDataPatientPayerEntryModels.Concat(other.SupplementalDataPatientPayerEntryModels).Distinct().ToArray();
        }
    }

    /// <summary>
    /// A base model for a repeating QRDA Category I template.
    /// </summary>
    public class QrdaCategory1EntryModel : IEquatable<QrdaCategory1EntryModel>
    {
        public RootAndExtension RootAndExtension { get; set; }

        public bool Equals(QrdaCategory1EntryModel other)
        {
            return other != null && Equals(other.RootAndExtension, RootAndExtension);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as QrdaCategory1EntryModel);
        }

        public override int GetHashCode()
        {
            return RootAndExtension.GetHashCode();
        }
    }

    public class QrdaCategory1EntryWithValueSetEntryModel : QrdaCategory1EntryModel
    {
        public QualityDataModelValueSetEntryModel ValueSetEntry { get; set; }
    }

    public class QrdaCategory1EntryWithValueSetEntryAndDateRangeModel : QrdaCategory1EntryWithValueSetEntryModel
    {
        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

    }

    public class QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel : QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
    {
        public string ResultUnitOfMeasurement { get; set; }

        public string ResultValue { get; set; }

        public string ResultType { get; set; }

        public QualityDataModelValueSetEntryModel ResultValueSetEntry { get; set; }

        public SnomedDescriptionModel Laterality { get; set; }
    }

    public class QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel : QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
    {
        public QualityDataModelValueSetEntryModel NotDoneValueSetEntry { get; set; }
    }


    public class QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel : QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
    {
        public string Description { get; set; }
    }

    public class PlanOfCareActivityActInterventionOrderEntryModel : QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
    {
        public RootAndExtension AuthorRootAndExtension { get; set; }
    }

    public class ProblemObservationDiagnosisActiveEntryModel : QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
    {
        public SnomedDescriptionModel Laterality { get; set; }

        public SnomedDescriptionModel Status { get; set; }

        public SnomedDescriptionModel Severity { get; set; }
    }

}
