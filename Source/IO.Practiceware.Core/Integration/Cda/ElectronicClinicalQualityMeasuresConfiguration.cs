﻿using System.Xml.Serialization;

namespace IO.Practiceware.Integration.Cda
{
    [XmlRoot("electronicClinicalQualityMeasures")]
    public class ElectronicClinicalQualityMeasuresConfiguration
    {
        [XmlElement("category3ReportingService")]
        public Category3ReportingServiceConfiguration Category3ReportingService { get; set; }
    }

    public class Category3ReportingServiceConfiguration
    {
        [XmlAttribute("serviceUri")]
        public string ServiceUri { get; set; }

        [XmlAttribute("login")]
        public string Login { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }
    }
}
