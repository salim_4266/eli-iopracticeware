﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Xml;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Integration.Cda
{
    public static class CdaSchemaExtensions
    {
        #region Problems

        /// <summary>
        /// Locate problem acts
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IQueryable<POCD_MT000040Act> Problems(this POCD_MT000040ClinicalDocument target)
        {
            var returnValue = target.component
                .Item.CastTo<POCD_MT000040StructuredBody>()
                .component
                .Select(c => c.section)
                .Where(c => c.templateId.RootIdEquals(
                    "2.16.840.1.113883.10.20.22.2.5", "2.16.840.1.113883.10.20.22.2.5.1")) // Problem act
                .SelectMany(s => s.entry)
                .Select(e => e.Item.CastTo<POCD_MT000040Act>())
                // Debugging
                .ToList()
                ;

            return returnValue.AsQueryable();
        }

        /// <summary>
        /// Locates primary problem observation in an act
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static POCD_MT000040Observation PrimaryProblemObservation(this POCD_MT000040Act target)
        {
            var result = target.entryRelationship
                .Select(er => er.Item.As<POCD_MT000040Observation>())
                .FirstOrDefault(ob =>
                    ob != null && ob.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.4"));

            return result;
        }

        /// <summary>
        /// Locates clinical condition status of primary observation
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD ClinicalConditionStatus(this POCD_MT000040Act target)
        {
            var observation = target.PrimaryProblemObservation();
            if (observation == null) return null;

            var observationstatus = observation.entryRelationship.IfNull(() => new POCD_MT000040EntryRelationship[0])
                                    .Select(er => er.Item.IfNull(() => new POCD_MT000040Observation()).As<POCD_MT000040Observation>())
                                    .FirstOrDefault(o => o != null && o.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.4"));
            if (observationstatus == null) return null;

            var result = observationstatus.value.OfType<CD>().FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Locates clinical condition code in the act
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD ClinicalConditionCode(this POCD_MT000040Act target)
        {
            var observation = target.PrimaryProblemObservation();
            if (observation == null) return null;

            var result = observation.value.OfType<CD>().FirstOrDefault();
            return result;
        }

        #endregion

        #region Allergy, adverse reactions
        /// <summary>
        /// Locate adverse reactions
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IEnumerable<POCD_MT000040Act> Allergens(this POCD_MT000040ClinicalDocument target)
        {
            var returnValue = target.component
                .Item.CastTo<POCD_MT000040StructuredBody>()
                .component
                .Select(c => c.section)
                .Where(c => c.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.2.6.1")) // Allergy act
                .SelectMany(s => s.entry)
                .Select(e => e.Item.CastTo<POCD_MT000040Act>())
                // Debugging
                .ToList()
                ;

            return returnValue;
        }

        /// <summary>
        /// Filter out acts for drug allergies
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IEnumerable<POCD_MT000040Act> DrugAllergies(this IEnumerable<POCD_MT000040Act> target)
        {
            var returnValue = target
                .Where(a => a.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.30") // Allergy template
                    && a.entryRelationship
                        .Any(er => er
                            .Item.CastTo<POCD_MT000040Observation>()
                            .value.OfType<CD>()
                            // TODO: why sample file has "419511003". It's not listed in our SNOMED CT records???
                            .Any(cd => (cd.code == "416098002" || cd.code == "419511003")
                                && (cd.codeSystemName == "SNOMED CT" || cd.codeSystem == CodeSystem.SnomedCt.ToString())
                                ))) // Drug allergy (disorder)
                // Debugging
                .ToList()
                ;

            return returnValue;
        }

        /// <summary>
        /// Locates primary allergy observation in an act
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static POCD_MT000040Observation PrimaryAllergyObservation(this POCD_MT000040Act target)
        {
            var result = target.entryRelationship
                .Select(er => er.Item.As<POCD_MT000040Observation>())
                .FirstOrDefault(ob => ob != null && ob.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.7"));

            return result;
        }

        /// <summary>
        /// Locates allergy source (medication) code in the act
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD AllergyMedicationCode(this POCD_MT000040Act target)
        {
            var allergyObservation = target.PrimaryAllergyObservation();
            if (allergyObservation == null) return null;

            // Find participant[MANU] -> playing entity[MMAT] -> code
            var result = allergyObservation.participant
                .Select(p => p.participantRole)
                .Where(pr => pr.classCode == "MANU")
                .Select(pr => pr.Item.As<POCD_MT000040PlayingEntity>())
                .Where(pe => pe != null && pe.classCode == "MMAT")
                .Select(pe => pe.code)
                .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Locates ConditionStatus of primary allergy observation.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD DrugAllergyConditionStatus(this POCD_MT000040Act target)
        {
            var allergyObservation = target.PrimaryAllergyObservation();
            if (allergyObservation == null) return null;

            var allergyObservationstatus = allergyObservation.entryRelationship.IfNull(() => new POCD_MT000040EntryRelationship[0])
                                        .Select(er => er.Item.IfNull(() => new POCD_MT000040Observation()).As<POCD_MT000040Observation>())
                                        .FirstOrDefault(o => o != null && o.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.28"));
            if (allergyObservationstatus == null) return null;

            var result = allergyObservationstatus.value.OfType<CD>().FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Recurses the entry relationships in the collection (via nested observations containing other entry relationships).
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static IEnumerable<POCD_MT000040EntryRelationship> RecurseEntryRelationships(this IEnumerable<POCD_MT000040EntryRelationship> source)
        {
            return source.Recurse(er => er.Item.As<POCD_MT000040Observation>().IfNotNull(o => o.entryRelationship)
                                        ?? new POCD_MT000040EntryRelationship[0]);
        }

        /// <summary>
        /// Locates adverse reaction observation
        /// It's "code" will correspond to [Allergen]
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD AllergenObservationCode(this POCD_MT000040Act target)
        {
            var result = target.entryRelationship.RecurseEntryRelationships()
                               .Select(er => er.Item.As<POCD_MT000040Observation>())
                               .Where(ob => ob != null && ob.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.9"))
                               .Select(ob => ob.value.OfType<CD>().FirstOrDefault())
                               .FirstOrDefault(cd => cd != null);

            return result;
        }

        /// <summary>
        /// Locates severity observation
        /// It's "code" will correspond to "Severity" ClinicalQualifier
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static CD SeverityObservationOfAllergenCode(this POCD_MT000040Act target)
        {
            var result = target.entryRelationship.RecurseEntryRelationships()
                .Select(er => er.Item.As<POCD_MT000040Observation>())
                .Where(ob => ob != null && ob.templateId.RootIdEquals("2.16.840.1.113883.10.20.22.4.8"))
                .Select(ob => ob.value.OfType<CD>().FirstOrDefault())
                .FirstOrDefault(cd => cd != null);

            return result;
        }

        #endregion

        public static string SerializeToString(this POCD_MT000040ClinicalDocument source)
        {
            using (var sw = new CustomEncodingStringWriter(Encoding.UTF8))
            using (var writer = XmlWriter.Create(sw, new XmlWriterSettings { Indent = true, OmitXmlDeclaration = false, Encoding = Encoding.UTF8 }))
            {
                writer.WriteProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"CDA.xsl\"");
                var serializer = Serialization.CreateCachedXmlSerializerWithRoot(typeof(POCD_MT000040ClinicalDocument), "ClinicalDocument", "urn:hl7-org:v3");
                serializer.Serialize(writer, source);
                return sw.ToString();
            }
        }

        /// <summary>
        /// Builds a display name for a clinical document (such as "Organization name")
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string DisplayDescription(this POCD_MT000040ClinicalDocument target)
        {
            var organizationName = target.recordTarget
                                         .SelectMany(rt => rt.patientRole.providerOrganization.IfNotNull(o => o.name, () => new ON[0]))
                                         .SelectMany(n => n.Text)
                                         .FirstOrDefault(t => !string.IsNullOrEmpty(t)) ?? "-";
            return organizationName;
        }

        /// <summary>
        /// Retrieves start date of observation
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static DateTime? StartDate(this POCD_MT000040Observation target)
        {
            return target.EffectiveTimeValue(ItemsChoiceType2.low);
        }

        /// <summary>
        /// Retrieves end date of observation
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static DateTime? EndDate(this POCD_MT000040Observation target)
        {
            return target.EffectiveTimeValue(ItemsChoiceType2.high);
        }

        /// <summary>
        /// Gets specific effective time
        /// </summary>
        /// <param name="target"></param>
        /// <param name="timeType"></param>
        /// <returns></returns>
        public static DateTime? EffectiveTimeValue(this POCD_MT000040Observation target, ItemsChoiceType2 timeType)
        {
            // Not found -> null
            if (target == null || target.effectiveTime == null) return null;

            for (int i = 0; i < target.effectiveTime.Items.Length; i++)
            {
                // Skip values we are not interested in
                if (target.effectiveTime.ItemsElementName[i] != timeType) continue;

                // Unknown?
                var ts = target.effectiveTime.Items[i].CastTo<TS>();
                if (ts.GetNullFlavor() == CdaNullFlavor.UNK) break;

                // Parse it and return
                return ts.value.ParseCdaDateTime();
            }

            // Not found -> null
            return null;
        }

        /// <summary>
        /// Parses date and time in CDA file
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? ParseCdaDateTime(this string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            // With time zone
            if (value.Length > 14) return DateTime.ParseExact(value, "yyyyMMddHHmmssK", null);

            // Full date and time
            if (value.Length == 14) return DateTime.ParseExact(value, "yyyyMMddHHmmss", null);

            // Only date
            if (value.Length == 8) return DateTime.ParseExact(value, "yyyyMMdd", null);

            throw new ArgumentException("Provided date in CDA could not be recognized", "value");
        }

        /// <summary>
        /// Checkes if TemplateId root equals specified value
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="rootId"></param>
        /// <returns></returns>
        public static bool RootIdEquals(this II[] templateId, params string[] rootId)
        {
            return templateId.Any(t => rootId.Contains(t.root));
        }
    }

    /// <summary>
    /// Extensions methods used to build a CDA document using a fluid syntax.
    /// All methods found within this class should support the fluid syntax and may take 2 forms, 'Add' and 'Set'.
    /// </summary>
    /// <remarks>
    /// 'Adds' returns a new brand new element that is attached to the source element.
    /// 'Set' returns the source element (self) and sets a value (attribute) on it.
    /// </remarks>
    public static class CdaFluentExtensions
    {
        #region General Elements

        public static POCD_MT000040Section AddSection(this POCD_MT000040Component3 source)
        {
            source.section = new POCD_MT000040Section { classCode = null, moodCode = null };
            return source.section;
        }

        public static II AddTemplateId(this POCD_MT000040Section source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        /// <summary>
        /// Sets the root and extension based on the entity and its key (and the client id).
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="root">The root.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public static II SetRootAndExtension(this II source, string root, string extension)
        {
            source.root = root;
            source.extension = extension;

            return source;
        }

        /// <summary>
        /// Sets the root and extension based on the entity and its key (and the client id).
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="hasId">The has identifier.</param>
        /// <param name="entityKeySuffix">A suffix to append to the entity key.</param>
        /// <param name="clientId">The client identifier.</param>
        /// <returns></returns>
        public static II SetRootAndExtension(this II source, IHasId hasId, string entityKeySuffix = null, string clientId = null)
        {
            var pair = hasId.GetOidRootAndExtension(entityKeySuffix, clientId);

            source.SetRootAndExtension(pair.Root, pair.Extension);

            return source;
        }

        public static II SetRoot(this II source, string root)
        {
            source.root = root;
            return source;
        }

        public static II SetExtension(this II source, string extension)
        {
            source.extension = extension;
            return source;
        }

        public static CS SetCode(this CS source, string code)
        {
            source.code = code;
            return source;
        }

        public static CE AddCode(this POCD_MT000040Section source)
        {
            var newCode = new CE();
            source.code = newCode;
            return newCode;
        }

        public static T SetCode<T>(this T source, string code) where T : CD
        {
            source.code = code;
            return source;
        }


        /// <summary>
        /// Retrieves code system name from provided <see cref="CD"/> even if it wasn't set (based on the template id).
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string GetCodeSystemNameWithLookup(this CD target)
        {
            // If code system name is provided -> return it
            if (!string.IsNullOrEmpty(target.codeSystemName))
            {
                return target.codeSystemName;
            }

            return Enums.GetValues<CodeSystem>().Select(c => c.GetAttribute<DisplayAttribute>()).WhereNotDefault()
                .Where(a => a.ShortName == target.codeSystem)
                .Select(a => a.Name)
                .FirstOrDefault() ?? string.Empty;
        }

        public static T SetCodeSystem<T>(this T source, CodeSystem codeSystem) where T : CD
        {
            source.codeSystem = codeSystem.Oid();
            source.codeSystemName = codeSystem.Name();
            return source;
        }

        public static T SetCodeSystemName<T>(this T source, string codeSystemName) where T : CD
        {
            source.codeSystemName = codeSystemName;
            return source;
        }

        public static T SetDisplayName<T>(this T source, string displayName) where T : CD
        {
            source.displayName = displayName;
            return source;
        }

        public static ST AddTitle(this POCD_MT000040Section source, string titleValue)
        {
            source.title = new ST { Text = new[] { titleValue } };
            return source.title;
        }

        public static ED SetText(this ED source, string text)
        {
            source.Text = new[] { text };
            return source;
        }

        public static TEL AddReference(this ED source)
        {
            source.reference = new TEL();
            return source.reference;
        }

        public static TEL SetValue(this TEL source, string value)
        {
            source.value = value;
            return source;
        }

        public static TEL SetEmailAddress(this TEL source, string value)
        {
            // if the value is an email address, put 'mailto:' in front of the value.
            source.value = string.Format("mailto:{0}", value);

            return source;
        }

        public static TEL SetPhoneNumber(this TEL source, string value)
        {
            if (value.IsNullOrWhiteSpace()) return source;

            value = value.ToLower();

            if (!value.StartsWith("tel:"))
            {
                value = "tel:" + value;
            }

            source.value = value;
            return source;
        }

        public static TEL SetUse(this TEL source, string use)
        {
            if (use == null)
            {
                source.use = null;
                return source;
            }

            source.use = new[] { use };
            return source;
        }

        public static IVL_TS SetValue(this IVL_TS source, string value)
        {
            source.value = value;
            return source;
        }

        public static TS AddLow(this IVL_TS source)
        {
            if (source.Items == null)
            {
                source.Items = new QTY[] { };
            }
            var newLowValue = new TS();
            source.Items = source.Items.Concat(new[] { newLowValue }).ToArray();

            if (source.ItemsElementName == null)
            {
                source.ItemsElementName = new ItemsChoiceType2[] { };
            }
            source.ItemsElementName = source.ItemsElementName.Concat(new[] { ItemsChoiceType2.low }).ToArray();

            return newLowValue;
        }

        public static TS AddCenter(this IVL_TS source)
        {
            if (source.Items == null)
            {
                source.Items = new QTY[] { };
            }
            var newCenterValue = new TS();
            source.Items = source.Items.Concat(new[] { newCenterValue }).ToArray();

            if (source.ItemsElementName == null)
            {
                source.ItemsElementName = new ItemsChoiceType2[] { };
            }
            source.ItemsElementName = source.ItemsElementName.Concat(new[] { ItemsChoiceType2.center }).ToArray();

            return newCenterValue;
        }

        public static TS AddHigh(this IVL_TS source)
        {
            if (source.Items == null)
            {
                source.Items = new QTY[] { };
            }
            var newHighValue = new TS();
            source.Items = source.Items.Concat(new[] { newHighValue }).ToArray();

            if (source.ItemsElementName == null)
            {
                source.ItemsElementName = new ItemsChoiceType2[] { };
            }
            source.ItemsElementName = source.ItemsElementName.Concat(new[] { ItemsChoiceType2.high }).ToArray();

            return newHighValue;
        }

        public static IVXB_TS SetValue(this IVXB_TS source, string value)
        {
            source.value = value;
            return source;
        }

        public static PQ SetUnit(this PQ source, string unit)
        {
            source.unit = unit;
            return source;
        }

        public static PQ SetValue(this PQ source, string value)
        {
            source.value = value;
            return source;
        }

        public static CE SetCode(this CE source, string code)
        {
            source.code = code;
            return source;
        }

        public static T SetNullFlavor<T>(this T source, CdaNullFlavor? nullFlavor) where T : ANY
        {
            if (nullFlavor == null) return null;

            source.nullFlavor = nullFlavor.GetDisplayName();
            return source;
        }

        public static CdaNullFlavor? GetNullFlavor(this ANY source)
        {
            return GetNullFlavor(source.nullFlavor);
        }

        public static CdaNullFlavor? GetNullFlavor(string nullFlavor)
        {
            if (nullFlavor.IsNullOrEmpty()) return null;

            var listOfFlavors = Enum.GetNames(typeof(CdaNullFlavor));
            if (!listOfFlavors.Contains(nullFlavor)) throw new InvalidOperationException("No known CdaNullFlavor value exists for {0}".FormatWith(nullFlavor));

            var cdaNullFlavorEnumList = Enums.GetValues<CdaNullFlavor>();
            return cdaNullFlavorEnumList.Single(flavor => flavor.GetDisplayName() == nullFlavor);
        }

        public static POCD_MT000040EntryRelationship SetTypeCode(this POCD_MT000040EntryRelationship source, x_ActRelationshipEntryRelationship typeCode)
        {
            source.typeCode = typeCode;
            return source;
        }

        public static POCD_MT000040EntryRelationship SetInversionInd(this POCD_MT000040EntryRelationship source, bool inversionInd)
        {
            source.inversionInd = inversionInd;
            source.inversionIndSpecified = true;
            return source;
        }

        public static AD AddAddressLine(this AD source, string addressLine)
        {
            if (source.Items == null)
            {
                source.Items = new ADXP[] { };
            }

            var newAddressLine = new adxpstreetAddressLine { Text = new[] { addressLine } };
            source.Items = source.Items.Concat(new[] { newAddressLine }).ToArray();
            return source;
        }

        public static AD AddCity(this AD source, string city)
        {
            if (source.Items == null)
            {
                source.Items = new ADXP[] { };
            }

            var newCity = new adxpcity { Text = new[] { city } };
            source.Items = source.Items.Concat(new[] { newCity }).ToArray();
            return source;
        }

        public static AD AddState(this AD source, string state)
        {
            if (source.Items == null)
            {
                source.Items = new ADXP[] { };
            }

            var newState = new adxpstate { Text = new[] { state } };
            source.Items = source.Items.Concat(new[] { newState }).ToArray();
            return source;
        }

        public static AD AddPostalCode(this AD source, string postalCode)
        {
            if (source.Items == null)
            {
                source.Items = new ADXP[] { };
            }

            var newPostalCode = new adxppostalCode { Text = new[] { postalCode } };
            source.Items = source.Items.Concat(new[] { newPostalCode }).ToArray();
            return source;
        }

        public static AD AddCountry(this AD source, string country)
        {
            if (source.Items == null)
            {
                source.Items = new ADXP[] { };
            }

            var newCountry = new adxpcountry { Text = new[] { country } };
            source.Items = source.Items.Concat(new[] { newCountry }).ToArray();
            return source;
        }

        public static AD SetUse(this AD source, string use)
        {
            if (use == null)
            {
                source.use = null;
                return source;
            }
            source.use = new[] { use };
            return source;
        }

        public static AD SetValues(this AD source, IAddress address)
        {
            if (address == null) return source;
            source.AddAddressLine(address.Line1)
                .AddCity(address.City)
                .AddState(address.StateOrProvince.Abbreviation)
                .AddPostalCode(address.PostalCode)
                .AddCountry(address.StateOrProvince.Country.Abbreviation2Letters);

            return source;
        }

        public static PN SetText(this PN source, string text)
        {
            source.Text = new[] { text };
            return source;
        }

        public static PN AddFirstName(this PN source, string firstName)
        {
            if (source.Items == null)
            {
                source.Items = new ENXP[] { };
            }

            var newFirstName = new engiven { Text = new[] { firstName } };
            source.Items = source.Items.Concat(new[] { newFirstName }).ToArray();
            return source;
        }

        public static PN AddLastName(this PN source, string lastName)
        {
            if (source.Items == null)
            {
                source.Items = new ENXP[] { };
            }

            var newLastName = new enfamily { Text = new[] { lastName } };
            source.Items = source.Items.Concat(new[] { newLastName }).ToArray();
            return source;
        }

        public static PN AddSuffix(this PN source, string suffix)
        {
            if (source.Items == null)
            {
                source.Items = new ENXP[] { };
            }

            var newSuffixName = new ensuffix { Text = new[] { suffix } };
            source.Items = source.Items.Concat(new[] { newSuffixName }).ToArray();
            return source;
        }

        /// <summary>
        /// Same as <see cref="AddFirstName"/> except it checks for null or empty first.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public static PN TryAddFirstName(this PN source, string firstName)
        {
            if (firstName.IsNullOrEmpty()) { return source; }

            return source.AddFirstName(firstName);
        }

        /// <summary>
        /// Same as <see cref="AddLastName"/> except it checks for null or empty first.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public static PN TryAddLastName(this PN source, string lastName)
        {
            if (lastName.IsNullOrEmpty()) { return source; }

            return source.AddLastName(lastName);
        }

        /// <summary>
        /// Same as <see cref="AddSuffix"/> except it checks for null or empty first.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public static PN TryAddSuffix(this PN source, string suffix)
        {
            if (suffix.IsNullOrEmpty()) { return source; }

            return source.AddSuffix(suffix);
        }

        public static TS SetValue(this TS source, string value)
        {
            source.value = value;
            return source;
        }

        #endregion

        #region ClinicalDocument

        public static POCD_MT000040Component2 AddComponent(this POCD_MT000040ClinicalDocument source)
        {
            var newComponent = new POCD_MT000040Component2();
            source.component = newComponent;
            return newComponent;
        }

        public static POCD_MT000040StructuredBody AddStructuredBody(this POCD_MT000040Component2 source)
        {
            var newStructuredBody = new POCD_MT000040StructuredBody { classCode = null, moodCode = null };
            source.Item = newStructuredBody;
            return newStructuredBody;
        }

        public static POCD_MT000040StructuredBody AddComponent(this POCD_MT000040StructuredBody source, POCD_MT000040Component3 component)
        {
            if (source.component == null)
            {
                source.component = new POCD_MT000040Component3[] { };
            }

            source.component = source.component.Concat(new[] { component }).ToArray();
            return source;
        }

        public static CS AddRealmCode(this POCD_MT000040ClinicalDocument source)
        {
            if (source.realmCode == null)
            {
                source.realmCode = new CS[] { };
            }

            var newRealmCode = new CS();
            source.realmCode = source.realmCode.Concat(new[] { newRealmCode }).ToArray();
            return newRealmCode;
        }

        public static POCD_MT000040InfrastructureRoottypeId AddTypeId(this POCD_MT000040ClinicalDocument source)
        {
            var newTypeId = new POCD_MT000040InfrastructureRoottypeId();
            source.typeId = newTypeId;
            return newTypeId;
        }

        public static POCD_MT000040InfrastructureRoottypeId SetRoot(this POCD_MT000040InfrastructureRoottypeId source, string root)
        {
            source.root = root;
            return source;
        }

        public static POCD_MT000040InfrastructureRoottypeId SetExtension(this POCD_MT000040InfrastructureRoottypeId source, string extension)
        {
            source.extension = extension;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040ClinicalDocument source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040ClinicalDocument source)
        {
            var newId = new II();
            source.id = newId;
            return newId;
        }

        public static CE AddCode(this POCD_MT000040ClinicalDocument source)
        {
            var newCode = new CE();
            source.code = newCode;
            return newCode;
        }

        public static ST AddTitle(this POCD_MT000040ClinicalDocument source, string title)
        {
            var newTitle = new ST { Text = new[] { title } };
            source.title = newTitle;
            return newTitle;
        }

        public static CE AddConfidentialityCode(this POCD_MT000040ClinicalDocument source)
        {
            var newCode = new CE();
            source.confidentialityCode = newCode;
            return newCode;
        }

        public static CS AddLanguageCode(this POCD_MT000040ClinicalDocument source)
        {
            var newCode = new CS();
            source.languageCode = newCode;
            return newCode;
        }

        public static TS AddEffectiveTime(this POCD_MT000040ClinicalDocument source)
        {
            var newEffectiveTime = new TS();
            source.effectiveTime = newEffectiveTime;
            return newEffectiveTime;
        }

        /// <summary>
        /// Checks all of the component tables in the document for empty tbody objects and adds a default tr and td.
        /// </summary>        
        /// <remarks>
        /// This is to make sure there are no empty <tbody/> elements generated when converting ClinicalDocuments to its xml representation.
        /// Empty <tbody /> will cause the CDA schema validation to fail.
        /// </remarks>
        /// <param name="source"></param>
        /// <returns></returns>
        public static POCD_MT000040ClinicalDocument EnsureDocumentTableBodiesAreValid(this POCD_MT000040ClinicalDocument source)
        {
            if (source == null || source.component == null || source.component.Item == null || !source.component.Item.Is<POCD_MT000040StructuredBody>()) return source;

            POCD_MT000040Component3[] components = source.component.Item.As<POCD_MT000040StructuredBody>().component;

            foreach (var component in components)
            {
                if (component.section == null) continue;
                if (component.section.text == null) continue;
                if (component.section.text.Items.IsNullOrEmpty()) continue;

                var componentTables = component.section.text.Items.OfType<StrucDocTable>();

                foreach (StrucDocTable table in componentTables)
                {
                    if (table.tbody.IsNullOrEmpty()) continue;

                    foreach (StrucDocTbody tbody in table.tbody)
                    {
                        if (tbody.tr.IsNullOrEmpty())
                        {
                            // add default table row with default data
                            var cell = tbody.AddTr().AddTd();
                            cell.Text = new[] { "" };
                            continue;
                        }

                        foreach (StrucDocTr trow in tbody.tr)
                        {
                            if (!trow.Items.IsNullOrEmpty()) continue;

                            // add default table row with default data
                            var cell = trow.AddTd();
                            cell.Text = new[] { "" };
                        }
                    }
                }
            }

            return source;
        }

        #endregion

        #region RecordTarget

        public static POCD_MT000040RecordTarget AddRecordTarget(this POCD_MT000040ClinicalDocument source)
        {
            if (source.recordTarget == null)
            {
                source.recordTarget = new POCD_MT000040RecordTarget[] { };
            }
            var newRecordTarget = new POCD_MT000040RecordTarget { typeCode = null, contextControlCode = null };
            source.recordTarget = source.recordTarget.Concat(new[] { newRecordTarget }).ToArray();
            return newRecordTarget;
        }

        public static POCD_MT000040RecordTarget SetPatientRole(this POCD_MT000040RecordTarget source, POCD_MT000040PatientRole patientRole)
        {
            source.patientRole = patientRole;
            return source;
        }

        #endregion

        #region DocumentationOf



        public static POCD_MT000040ServiceEvent AddServiceEvent(this POCD_MT000040DocumentationOf source)
        {
            var serviceEvent = new POCD_MT000040ServiceEvent { moodCode = null };
            source.serviceEvent = serviceEvent;
            return serviceEvent;
        }

        public static POCD_MT000040ServiceEvent SetClassCode(this POCD_MT000040ServiceEvent source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040ServiceEvent source)
        {
            var effectiveTime = new IVL_TS();
            source.effectiveTime = effectiveTime;
            return effectiveTime;
        }

        public static POCD_MT000040Performer1 AddPerformer(this POCD_MT000040ServiceEvent source)
        {
            if (source.performer == null)
            {
                source.performer = new POCD_MT000040Performer1[] { };
            }

            var newPerformer = new POCD_MT000040Performer1();
            source.performer = source.performer.Concat(new[] { newPerformer }).ToArray();
            return newPerformer;
        }

        public static POCD_MT000040Performer1 SetTypeCode(this POCD_MT000040Performer1 source, x_ServiceEventPerformer typeCode)
        {
            source.typeCode = typeCode;
            return source;
        }

        public static POCD_MT000040AssignedEntity AddAssignedEntity(this POCD_MT000040Performer1 source)
        {
            var newAssignedEntity = new POCD_MT000040AssignedEntity { classCode = null };
            source.assignedEntity = newAssignedEntity;
            return newAssignedEntity;
        }

        public static II AddId(this POCD_MT000040AssignedEntity source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static CE AddCode(this POCD_MT000040AssignedEntity source)
        {
            var newCode = new CE();
            source.code = newCode;
            return newCode;
        }

        public static AD AddAddress(this POCD_MT000040AssignedEntity source)
        {
            if (source.addr == null)
            {
                source.addr = new AD[] { };
            }

            var newAddress = new AD();
            source.addr = source.addr.Concat(new[] { newAddress }).ToArray();
            return newAddress;
        }

        public static TEL AddTelecom(this POCD_MT000040AssignedEntity source)
        {
            if (source.telecom == null)
            {
                source.telecom = new TEL[] { };
            }

            var newTelecom = new TEL();
            source.telecom = source.telecom.Concat(new[] { newTelecom }).ToArray();
            return newTelecom;
        }

        public static POCD_MT000040Person AddAssignedPerson(this POCD_MT000040AssignedEntity source)
        {
            var newAssignedPerson = new POCD_MT000040Person { classCode = null, determinerCode = null };
            source.assignedPerson = newAssignedPerson;
            return newAssignedPerson;
        }

        public static PN AddName(this POCD_MT000040Person source)
        {
            var newName = new PN();
            source.name = new[] { newName };
            return newName;
        }

        #endregion

        #region ComponentOf

        public static POCD_MT000040Component1 AddComponentOf(this POCD_MT000040ClinicalDocument source)
        {
            var newComponentOf = new POCD_MT000040Component1();
            source.componentOf = newComponentOf;
            return newComponentOf;
        }

        public static POCD_MT000040EncompassingEncounter AddEncompassingEncounter(this POCD_MT000040Component1 source)
        {
            var newEncompassingEncounter = new POCD_MT000040EncompassingEncounter();
            source.encompassingEncounter = newEncompassingEncounter;
            return newEncompassingEncounter;
        }

        public static II AddId(this POCD_MT000040EncompassingEncounter source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040EncompassingEncounter source)
        {
            var newEffectiveTime = new IVL_TS();
            source.effectiveTime = newEffectiveTime;
            return newEffectiveTime;
        }

        public static POCD_MT000040EncounterParticipant AddEncounterParticipant(this POCD_MT000040EncompassingEncounter source)
        {
            if (source.encounterParticipant == null)
            {
                source.encounterParticipant = new POCD_MT000040EncounterParticipant[] { };
            }

            var newEcounterParticipant = new POCD_MT000040EncounterParticipant();
            source.encounterParticipant = source.encounterParticipant.Concat(new[] { newEcounterParticipant }).ToArray();
            return newEcounterParticipant;
        }

        public static POCD_MT000040EncounterParticipant SetTypeCode(this POCD_MT000040EncounterParticipant source, x_EncounterParticipant typeCode)
        {
            source.typeCode = typeCode;
            return source;
        }

        public static IVL_TS AddTime(this POCD_MT000040EncounterParticipant source)
        {
            var newTime = new IVL_TS();
            source.time = newTime;
            return newTime;
        }

        public static POCD_MT000040AssignedEntity AddAssignedEntity(this POCD_MT000040EncounterParticipant source)
        {
            var newAssignedEntity = new POCD_MT000040AssignedEntity();
            source.assignedEntity = newAssignedEntity;
            return newAssignedEntity;
        }

        public static POCD_MT000040Location AddLocation(this POCD_MT000040EncompassingEncounter source)
        {
            var newLocation = new POCD_MT000040Location();
            source.location = newLocation;
            return newLocation;
        }

        public static POCD_MT000040HealthCareFacility AddHealthCareFacility(this POCD_MT000040Location source)
        {
            var newHealthCareFacility = new POCD_MT000040HealthCareFacility();
            source.healthCareFacility = newHealthCareFacility;
            return newHealthCareFacility;
        }

        public static II AddId(this POCD_MT000040HealthCareFacility source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static CE AddCode(this POCD_MT000040HealthCareFacility source)
        {
            source.code = new CE();
            return source.code;
        }

        public static POCD_MT000040Place AddLocation(this POCD_MT000040HealthCareFacility source)
        {
            source.location = new POCD_MT000040Place();
            return source.location;
        }

        public static EN AddName(this POCD_MT000040Place source, string name)
        {
            source.name = new EN { Text = new[] { name } };
            return source.name;
        }

        public static AD AddAddress(this POCD_MT000040Place source)
        {
            source.addr = new AD();
            return source.addr;
        }

        #endregion

        #region ProviderOrganization

        public static II AddId(this POCD_MT000040Organization source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static ON AddName(this POCD_MT000040Organization source, string name)
        {
            if (source.name == null)
            {
                source.name = new ON[] { };
            }

            var newName = new ON { Text = new[] { name } };
            source.name = source.name.Concat(new[] { newName }).ToArray();
            return newName;
        }

        public static TEL AddTelecom(this POCD_MT000040Organization source)
        {
            if (source.telecom == null)
            {
                source.telecom = new TEL[] { };
            }

            var newTelecom = new TEL();
            source.telecom = source.telecom.Concat(new[] { newTelecom }).ToArray();
            return newTelecom;
        }

        public static AD AddAddress(this POCD_MT000040Organization source)
        {
            if (source.addr == null)
            {
                source.addr = new AD[] { };
            }

            var newAddress = new AD();
            source.addr = source.addr.Concat(new[] { newAddress }).ToArray();
            return newAddress;
        }

        #endregion

        #region PatientRole

        public static POCD_MT000040PatientRole SetProviderOrganization(this POCD_MT000040PatientRole source, POCD_MT000040Organization providerOrganization)
        {
            source.providerOrganization = providerOrganization;
            return source;
        }

        public static II AddId(this POCD_MT000040PatientRole source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static AD AddAddress(this POCD_MT000040PatientRole source)
        {
            if (source.addr == null)
            {
                source.addr = new AD[] { };
            }

            var newAddress = new AD();
            source.addr = source.addr.Concat(new[] { newAddress }).ToArray();
            return newAddress;
        }

        public static TEL AddTelecom(this POCD_MT000040PatientRole source)
        {
            if (source.telecom == null)
            {
                source.telecom = new TEL[] { };
            }

            var newTelecom = new TEL();
            source.telecom = source.telecom.Concat(new[] { newTelecom }).ToArray();
            return newTelecom;
        }

        #endregion

        #region Patient

        public static CE AddRaceCode(this POCD_MT000040Patient source)
        {
            var newRaceCode = new CE();

            if (source.raceCode == null)
            {
                source.raceCode = newRaceCode;
            }
            else
            {
                source.raceCode1 = (source.raceCode1 ?? new CE[] { }).Concat(new[] { newRaceCode }).ToArray();
            }

            return newRaceCode;
        }

        public static PN AddName(this POCD_MT000040Patient source)
        {
            if (source.name == null)
            {
                source.name = new PN[] { };
            }

            var newName = new PN();
            source.name = source.name.Concat(new[] { newName }).ToArray();
            return newName;
        }

        #endregion

        #region Author

        public static POCD_MT000040Author AddAuthor(this POCD_MT000040ClinicalDocument source, BillingOrganization billingOrganization, DateTime timeOfCreation)
        {
            var newAuthor = new POCD_MT000040Author { typeCode = null, contextControlCode = null };
            newAuthor.AddTime().SetValue(CdaExtensions.FormatForCda(timeOfCreation));
            var assignedAuthor = newAuthor.AddAssignedAuthor();
            assignedAuthor.AddId().SetExtension(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                                  .SetRoot("2.16.840.1.113883.4.6");
            assignedAuthor.AddCode().SetCode(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy))
                                    .SetCodeSystem(CodeSystem.Nucc)
                                    .SetCodeSystemName("NUCC");

            var address = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);
            assignedAuthor.AddAddress().SetUse("WP").SetValues(address);

            assignedAuthor.AddTelecom().SetPhoneNumber(billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).ToString())
                                       .SetUse("WP");

            assignedAuthor.AddAuthoringDevice().SetManufacturerModelName("IO Practiceware, Inc.")
                                               .SetSoftwareName("IO Practiceware");

            if (source.author == null)
            {
                source.author = new POCD_MT000040Author[] { };
            }
            source.author = source.author.Concat(new[] { newAuthor }).ToArray();

            return newAuthor;
        }

        public static TS AddTime(this POCD_MT000040Author source)
        {
            source.time = new TS();
            return source.time;
        }

        public static POCD_MT000040AssignedAuthor AddAssignedAuthor(this POCD_MT000040Author source)
        {
            source.assignedAuthor = new POCD_MT000040AssignedAuthor { classCode = null };
            return source.assignedAuthor;
        }

        public static II AddId(this POCD_MT000040AssignedAuthor source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static CE AddCode(this POCD_MT000040AssignedAuthor source)
        {
            source.code = new CE();
            return source.code;
        }

        public static AD AddAddress(this POCD_MT000040AssignedAuthor source)
        {
            if (source.addr == null)
            {
                source.addr = new AD[] { };
            }

            var newAddress = new AD();
            source.addr = source.addr.Concat(new[] { newAddress }).ToArray();
            return newAddress;
        }

        public static TEL AddTelecom(this POCD_MT000040AssignedAuthor source)
        {
            if (source.telecom == null)
            {
                source.telecom = new TEL[] { };
            }

            var newTelecom = new TEL();
            source.telecom = source.telecom.Concat(new[] { newTelecom }).ToArray();
            return newTelecom;
        }

        public static POCD_MT000040AuthoringDevice AddAuthoringDevice(this POCD_MT000040AssignedAuthor source)
        {
            var newAuthoringDevice = new POCD_MT000040AuthoringDevice { determinerCode = null };
            source.Item = newAuthoringDevice;
            return newAuthoringDevice;
        }

        public static POCD_MT000040AuthoringDevice SetManufacturerModelName(this POCD_MT000040AuthoringDevice source, string modelName)
        {
            source.manufacturerModelName = new SC { Text = new[] { modelName } };
            return source;
        }

        public static POCD_MT000040AuthoringDevice SetSoftwareName(this POCD_MT000040AuthoringDevice source, string softwareName)
        {
            source.softwareName = new SC { Text = new[] { softwareName } };
            return source;
        }

        #endregion

        #region Custodian

        public static POCD_MT000040Custodian AddCustodian(this POCD_MT000040ClinicalDocument source, BillingOrganization billingOrganization)
        {
            var newCustodian = new POCD_MT000040Custodian { typeCode = null };
            var custodianOrganization = newCustodian.AddAssignedCustodian().AddRepresentedCustodianOrganization();
            custodianOrganization.AddId().SetExtension(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                                         .SetRoot("2.16.840.1.113883.4.6");
            custodianOrganization.AddName(billingOrganization.Name);
            custodianOrganization.AddTelecom().SetPhoneNumber(billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).ToString())
                                              .SetUse("WP");
            var address = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);
            custodianOrganization.AddAddress().SetUse("WP").SetValues(address);

            source.custodian = newCustodian;
            return newCustodian;
        }

        public static POCD_MT000040AssignedCustodian AddAssignedCustodian(this POCD_MT000040Custodian source)
        {
            var newAssignedCustodian = new POCD_MT000040AssignedCustodian { classCode = null };
            source.assignedCustodian = newAssignedCustodian;
            return newAssignedCustodian;
        }

        public static POCD_MT000040CustodianOrganization AddRepresentedCustodianOrganization(this POCD_MT000040AssignedCustodian source)
        {
            var custodianOrganization = new POCD_MT000040CustodianOrganization { classCode = null, determinerCode = null };
            source.representedCustodianOrganization = custodianOrganization;
            return custodianOrganization;
        }

        public static II AddId(this POCD_MT000040CustodianOrganization source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static ON AddName(this POCD_MT000040CustodianOrganization source, string name)
        {
            var newName = new ON { Text = new[] { name } };
            source.name = newName;
            return newName;
        }

        public static TEL AddTelecom(this POCD_MT000040CustodianOrganization source)
        {
            var newTelecom = new TEL();
            source.telecom = newTelecom;
            return newTelecom;
        }

        public static AD AddAddress(this POCD_MT000040CustodianOrganization source)
        {
            var newAddress = new AD();
            source.addr = newAddress;
            return newAddress;
        }

        #endregion

        #region Text

        public static StrucDocText AddText(this POCD_MT000040Section source)
        {
            source.text = new StrucDocText { mediaType = null };
            return source.text;
        }

        public static StrucDocParagraph AddParagraph(this StrucDocText source)
        {
            if (source.Items == null)
            {
                source.Items = new object[] { };
            }
            var newParagraph = new StrucDocParagraph();
            source.Items = source.Items.Concat(new[] { newParagraph }).ToArray();
            return newParagraph;
        }

        public static StrucDocParagraph SetText(this StrucDocParagraph source, string text)
        {
            source.Text = new[] { text };
            return source;
        }

        public static StrucDocTable AddTable(this StrucDocText source)
        {
            if (source.Items == null)
            {
                source.Items = new object[] { };
            }

            var newTable = new StrucDocTable();
            source.Items = source.Items.Concat(new[] { newTable }).ToArray();
            return newTable;
        }

        public static StrucDocTable SetBorder(this StrucDocTable source, string border)
        {
            source.border = border;
            return source;
        }

        public static StrucDocTable SetWidth(this StrucDocTable source, string width)
        {
            source.width = width;
            return source;
        }

        public static StrucDocThead AddThead(this StrucDocTable source)
        {
            source.thead = new StrucDocThead();
            return source.thead;
        }

        public static StrucDocTbody AddTbody(this StrucDocTable source)
        {
            if (source.tbody == null)
            {
                source.tbody = new StrucDocTbody[] { };
            }

            var newBody = new StrucDocTbody();
            source.tbody = source.tbody.Concat(new[] { newBody }).ToArray();
            return newBody;
        }

        public static StrucDocTr AddTr(this StrucDocThead source)
        {
            if (source.tr == null)
            {
                source.tr = new StrucDocTr[] { };
            }

            var newTr = new StrucDocTr();
            source.tr = source.tr.Concat(new[] { newTr }).ToArray();
            return newTr;
        }

        public static StrucDocTr AddTr(this StrucDocTbody source)
        {
            if (source.tr == null)
            {
                source.tr = new StrucDocTr[] { };
            }

            var newTr = new StrucDocTr();
            source.tr = source.tr.Concat(new[] { newTr }).ToArray();
            return newTr;
        }

        public static StrucDocTr SetId(this StrucDocTr source, string id)
        {
            source.ID = id;
            return source;
        }

        public static StrucDocTr AddTh(this StrucDocTr source, string text, Action<StrucDocTh> configureTh)
        {
            var th = source.AddTh(text);
            if (configureTh != null) configureTh(th);
            return source;
        }

        public static StrucDocTh AddTh(this StrucDocTr source, string text)
        {
            if (source.Items == null)
            {
                source.Items = new object[] { };
            }

            var newTh = new StrucDocTh { Text = new[] { text } };
            source.Items = source.Items.Concat(new object[] { newTh }).ToArray();
            return newTh;
        }

        public static StrucDocTh SetAlign(this StrucDocTh source, StrucDocThAlign align)
        {
            source.align = align;
            source.alignSpecified = true;
            return source;
        }

        public static StrucDocTd AddTd(this StrucDocTr source)
        {
            if (source.Items == null)
            {
                source.Items = new object[] { };
            }

            var newTd = new StrucDocTd();
            source.Items = source.Items.Concat(new object[] { newTd }).ToArray();
            return newTd;
        }

        public static StrucDocTd SetId(this StrucDocTd source, string id)
        {
            source.ID = id;
            return source;
        }

        public static StrucDocTd SetText(this StrucDocTd source, string text)
        {
            source.Text = new[] { text };
            return source;
        }

        public static StrucDocTd SetEmptyText(this StrucDocTd source)
        {
            source.Text = new[] { "" };
            return source;
        }

        public static StrucDocContent AddContent(this StrucDocTd source)
        {
            if (source.Items == null)
            {
                source.Items = new object[] { };
            }

            var newContent = new StrucDocContent();
            source.Items = source.Items.Concat(new object[] { newContent }).ToArray();
            return newContent;
        }

        public static StrucDocContent SetId(this StrucDocContent source, string id)
        {
            source.ID = id;
            return source;
        }

        public static StrucDocContent SetText(this StrucDocContent source, string text)
        {
            source.Text = new[] { text };
            return source;
        }

        #endregion

        #region Entry

        public static POCD_MT000040Entry AddEntry(this POCD_MT000040Section source)
        {
            if (source.entry == null)
            {
                source.entry = new POCD_MT000040Entry[] { };
            }

            var newEntry = new POCD_MT000040Entry();
            source.entry = source.entry.Concat(new[] { newEntry }).ToArray();
            return newEntry;
        }

        public static POCD_MT000040Entry SetTypeCode(this POCD_MT000040Entry source, x_ActRelationshipEntry typeCode)
        {
            source.typeCode = typeCode;
            return source;
        }

        #endregion

        #region Observation Entry

        public static POCD_MT000040Observation AddObservation(this POCD_MT000040Entry source)
        {
            var observation = new POCD_MT000040Observation();
            source.Item = observation;
            return observation;
        }

        public static POCD_MT000040Observation AddObservation(this POCD_MT000040Component4 source)
        {
            var observation = new POCD_MT000040Observation();
            source.Item = observation;
            return observation;
        }

        public static POCD_MT000040Observation SetClassCode(this POCD_MT000040Observation source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040Observation SetMoodCode(this POCD_MT000040Observation source, x_ActMoodDocumentObservation moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040Observation source)
        {
            var newTemplateId = new II();
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();

            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040Observation source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static CD AddCode(this POCD_MT000040Observation source)
        {
            source.code = new CD();
            return source.code;
        }

        public static T AddCode<T>(this POCD_MT000040Observation source) where T : CD, new()
        {
            var newCode = new T();
            source.code = newCode;
            return newCode;
        }

        public static ED AddText(this POCD_MT000040Observation source)
        {
            source.text = new ED();
            return source.text;
        }

        public static CS AddStatusCode(this POCD_MT000040Observation source)
        {
            source.statusCode = new CS();
            return source.statusCode;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040Observation source)
        {
            source.effectiveTime = new IVL_TS();
            return source.effectiveTime;
        }

        public static POCD_MT000040EntryRelationship AddEntryRelationship(this POCD_MT000040Observation source)
        {
            if (source.entryRelationship == null)
            {
                source.entryRelationship = new POCD_MT000040EntryRelationship[] { };
            }
            var entryRelationship = new POCD_MT000040EntryRelationship();
            source.entryRelationship = source.entryRelationship.Concat(new[] { entryRelationship }).ToArray();
            return entryRelationship;
        }

        public static T AddValue<T>(this POCD_MT000040Observation source) where T : ANY, new()
        {
            if (source.value == null)
            {
                source.value = new ANY[] { };
            }

            var newValue = new T();
            source.value = source.value.Concat(new[] { newValue }).ToArray();
            return newValue;
        }

        public static CE AddInterpretationCode(this POCD_MT000040Observation source)
        {
            if (source.interpretationCode == null)
            {
                source.interpretationCode = new CE[] { };
            }

            var newCode = new CE();
            source.interpretationCode = source.interpretationCode.Concat(new[] { newCode }).ToArray();
            return newCode;
        }

        public static POCD_MT000040ReferenceRange AddReferenceRange(this POCD_MT000040Observation source)
        {
            if (source.referenceRange == null)
            {
                source.referenceRange = new POCD_MT000040ReferenceRange[] { };
            }

            var newReferenceRange = new POCD_MT000040ReferenceRange { typeCode = null };
            source.referenceRange = source.referenceRange.Concat(new[] { newReferenceRange }).ToArray();
            return newReferenceRange;
        }

        public static POCD_MT000040ObservationRange AddObservationRange(this POCD_MT000040ReferenceRange source)
        {
            var range = new POCD_MT000040ObservationRange { moodCode = null };
            source.observationRange = range;
            return range;
        }

        public static ED AddText(this POCD_MT000040ObservationRange source, string text)
        {
            var ed = new ED { Text = new[] { text } };
            source.text = ed;
            return ed;
        }

        public static POCD_MT000040Participant2 AddParticipant(this POCD_MT000040Observation source)
        {
            if (source.participant == null)
            {
                source.participant = new POCD_MT000040Participant2[] { };
            }
            var newParticipant = new POCD_MT000040Participant2 { contextControlCode = null };
            source.participant = source.participant.Concat(new[] { newParticipant }).ToArray();
            return newParticipant;
        }

        public static POCD_MT000040Participant2 SetTypeCode(this POCD_MT000040Participant2 source, string typeCode)
        {
            source.typeCode = typeCode;
            return source;
        }

        public static POCD_MT000040ParticipantRole AddParticipantRole(this POCD_MT000040Participant2 source)
        {
            source.participantRole = new POCD_MT000040ParticipantRole();
            return source.participantRole;
        }

        public static POCD_MT000040ParticipantRole SetClassCode(this POCD_MT000040ParticipantRole source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040PlayingEntity AddPlayingEntity(this POCD_MT000040ParticipantRole source)
        {
            var newPlayingEntity = new POCD_MT000040PlayingEntity { determinerCode = null };
            source.Item = newPlayingEntity;
            return newPlayingEntity;
        }

        public static POCD_MT000040PlayingEntity SetClassCode(this POCD_MT000040PlayingEntity source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static CE AddCode(this POCD_MT000040PlayingEntity source)
        {
            source.code = new CE();
            return source.code;
        }

        public static PN AddName(this POCD_MT000040PlayingEntity source)
        {
            if (source.name == null)
            {
                source.name = new PN[] { };
            }

            var newName = new PN();
            source.name = source.name.Concat(new[] { newName }).ToArray();
            return newName;
        }

        #endregion

        #region Organizer Entry

        public static POCD_MT000040Organizer AddOrganizer(this POCD_MT000040Entry source)
        {
            var organizer = new POCD_MT000040Organizer();
            source.Item = organizer;
            return organizer;
        }

        public static POCD_MT000040Organizer SetClassCode(this POCD_MT000040Organizer source, x_ActClassDocumentEntryOrganizer classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040Organizer SetMoodCode(this POCD_MT000040Organizer source, string moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040Organizer source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040Organizer source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static T AddCode<T>(this POCD_MT000040Organizer source) where T : CD, new()
        {
            var newCode = new T();
            source.code = newCode;
            return newCode;
        }

        public static CS AddStatusCode(this POCD_MT000040Organizer source)
        {
            source.statusCode = new CS();
            return source.statusCode;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040Organizer source)
        {
            var newEffectiveTime = new IVL_TS();
            source.effectiveTime = newEffectiveTime;
            return newEffectiveTime;
        }

        public static POCD_MT000040Component4 AddComponent(this POCD_MT000040Organizer source)
        {
            if (source.component == null)
            {
                source.component = new POCD_MT000040Component4[] { };
            }

            var newComponent = new POCD_MT000040Component4();
            source.component = source.component.Concat(new[] { newComponent }).ToArray();
            return newComponent;
        }

        #endregion

        #region Act Entry

        public static POCD_MT000040Act AddAct(this POCD_MT000040Entry source)
        {
            var act = new POCD_MT000040Act();
            source.Item = act;
            return act;
        }

        public static POCD_MT000040Act SetClassCode(this POCD_MT000040Act source, x_ActClassDocumentEntryAct classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040Act SetMoodCode(this POCD_MT000040Act source, x_DocumentActMood moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040Act source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040Act source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static T AddCode<T>(this POCD_MT000040Act source) where T : CD, new()
        {
            var newCode = new T();
            source.code = newCode;
            return newCode;
        }

        public static CD AddCode(this POCD_MT000040Act source)
        {
            source.code = new CD();
            return source.code;
        }

        public static ED AddText(this POCD_MT000040Act source)
        {
            source.text = new ED();
            return source.text;
        }

        public static ED AddText(this POCD_MT000040Act source, string text)
        {
            var newText = new ED { Text = new[] { text } };
            source.text = newText;
            return newText;
        }

        public static CS AddStatusCode(this POCD_MT000040Act source)
        {
            source.statusCode = new CS();
            return source.statusCode;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040Act source)
        {
            source.effectiveTime = new IVL_TS();
            return source.effectiveTime;
        }

        public static POCD_MT000040EntryRelationship AddEntryRelationship(this POCD_MT000040Act source)
        {
            if (source.entryRelationship == null)
            {
                source.entryRelationship = new POCD_MT000040EntryRelationship[] { };
            }

            var newEntryRelationship = new POCD_MT000040EntryRelationship { inversionInd = true };
            source.entryRelationship = source.entryRelationship.Concat(new[] { newEntryRelationship }).ToArray();
            return newEntryRelationship;
        }

        public static POCD_MT000040Observation AddObservation(this POCD_MT000040EntryRelationship source)
        {
            var observation = new POCD_MT000040Observation();
            source.Item = observation;
            return observation;
        }

        #endregion

        #region Procedure Entry

        public static POCD_MT000040Procedure AddProcedure(this POCD_MT000040Entry source)
        {
            var procedure = new POCD_MT000040Procedure();
            source.Item = procedure;
            return procedure;
        }

        public static POCD_MT000040Procedure SetClassCode(this POCD_MT000040Procedure source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040Procedure SetMoodCode(this POCD_MT000040Procedure source, x_DocumentProcedureMood moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040Procedure source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040Procedure source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }

            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static CD AddCode(this POCD_MT000040Procedure source)
        {
            source.code = new CD();
            return source.code;
        }

        public static T AddCode<T>(this POCD_MT000040Procedure source) where T : CD, new()
        {
            var newCode = new T();
            source.code = newCode;
            return newCode;
        }

        public static ED AddOriginalText(this CD source)
        {
            source.originalText = new ED();
            return source.originalText;
        }

        public static CS AddStatusCode(this POCD_MT000040Procedure source)
        {
            source.statusCode = new CS();
            return source.statusCode;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040Procedure source)
        {
            source.effectiveTime = new IVL_TS();
            return source.effectiveTime;
        }

        #endregion

        #region SubstanceAdministration Entry

        public static POCD_MT000040SubstanceAdministration AddSubstanceAdministration(this POCD_MT000040Entry source)
        {
            var substanceAdministration = new POCD_MT000040SubstanceAdministration();
            source.Item = substanceAdministration;
            return substanceAdministration;
        }

        public static POCD_MT000040SubstanceAdministration SetClassCode(this POCD_MT000040SubstanceAdministration source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static POCD_MT000040SubstanceAdministration SetMoodCode(this POCD_MT000040SubstanceAdministration source, x_DocumentSubstanceMood moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static POCD_MT000040SubstanceAdministration SetNegationInd(this POCD_MT000040SubstanceAdministration source, bool negationId)
        {
            source.negationIndSpecified = true;
            source.negationInd = negationId;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040SubstanceAdministration source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }
            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040SubstanceAdministration source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }
            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static ED AddText(this POCD_MT000040SubstanceAdministration source)
        {
            source.text = new ED();
            return source.text;
        }

        public static CS AddStatusCode(this POCD_MT000040SubstanceAdministration source)
        {
            source.statusCode = new CS();
            return source.statusCode;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040SubstanceAdministration source)
        {
            if (source.effectiveTime == null)
            {
                source.effectiveTime = new SXCM_TS[] { };
            }

            var newEffectiveTime = new IVL_TS();
            source.effectiveTime = source.effectiveTime.Concat(new[] { newEffectiveTime }).ToArray();
            return newEffectiveTime;
        }

        public static POCD_MT000040Consumable AddConsumable(this POCD_MT000040SubstanceAdministration source)
        {
            source.consumable = new POCD_MT000040Consumable { typeCode = null };
            return source.consumable;
        }

        public static POCD_MT000040ManufacturedProduct AddManufacturedProduct(this POCD_MT000040Consumable source)
        {
            source.manufacturedProduct = new POCD_MT000040ManufacturedProduct();
            return source.manufacturedProduct;
        }

        public static POCD_MT000040ManufacturedProduct SetClassCode(this POCD_MT000040ManufacturedProduct source, RoleClassManufacturedProduct classCode)
        {
            source.classCode = classCode;
            source.classCodeSpecified = true;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040ManufacturedProduct source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }
            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static POCD_MT000040Material AddMaterial(this POCD_MT000040ManufacturedProduct source)
        {
            var newMaterial = new POCD_MT000040Material { classCode = null };
            source.Item = newMaterial;
            return newMaterial;
        }

        public static CE AddCode(this POCD_MT000040Material source)
        {
            source.code = new CE();
            return source.code;
        }

        #endregion

        #region Encounter Entry

        public static POCD_MT000040Encounter AddEncounter(this POCD_MT000040Entry source)
        {
            var newEncounter = new POCD_MT000040Encounter();
            source.Item = newEncounter;
            return newEncounter;
        }

        public static POCD_MT000040Encounter SetMoodCode(this POCD_MT000040Encounter source, x_DocumentEncounterMood moodCode)
        {
            source.moodCode = moodCode;
            return source;
        }

        public static POCD_MT000040Encounter SetClassCode(this POCD_MT000040Encounter source, string classCode)
        {
            source.classCode = classCode;
            return source;
        }

        public static II AddTemplateId(this POCD_MT000040Encounter source)
        {
            if (source.templateId == null)
            {
                source.templateId = new II[] { };
            }

            var newTemplateId = new II();
            source.templateId = source.templateId.Concat(new[] { newTemplateId }).ToArray();
            return newTemplateId;
        }

        public static II AddId(this POCD_MT000040Encounter source)
        {
            if (source.id == null)
            {
                source.id = new II[] { };
            }
            var newId = new II();
            source.id = source.id.Concat(new[] { newId }).ToArray();
            return newId;
        }

        public static ED AddText(this POCD_MT000040Encounter source)
        {
            var newText = new ED();
            source.text = newText;
            return newText;
        }

        public static IVL_TS AddEffectiveTime(this POCD_MT000040Encounter source)
        {
            source.effectiveTime = new IVL_TS();
            return source.effectiveTime;
        }

        #endregion
    }
}
