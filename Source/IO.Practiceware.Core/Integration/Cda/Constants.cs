﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Integration.Cda
{
    public static class Oids
    {
        /// <summary>
        ///   The IO Practiceware OID assigned by HL7.org
        /// </summary>
        public const string IOPracticeware = "2.16.840.1.113883.3.4015";
        public const string ContinuityOfCareDocumentGeneralHeader = "2.16.840.1.113883.10.20.22.1.1";
        public const string ContinuityOfCareDocumentCcd = "2.16.840.1.113883.10.20.22.1.2";
        public const string Hl7RegisteredModels = "2.16.840.1.113883.1.3";
        public const string Confidentiality = "2.16.840.1.113883.5.25";
        public const string NationalProviderId = "2.16.840.1.113883.4.6";
        public const string TaxIdNumber = "2.16.840.1.113883.4.2";
        public const string Taxonomy = "2.16.840.1.113883.6.101";
        public const string Allergies = "2.16.840.1.113883.10.20.22.2.6.1";
        public const string AllergyProblemAct = "2.16.840.1.113883.10.20.22.4.30";
        public const string AllergyObservation = "2.16.840.1.113883.10.20.22.4.7";
        public const string ReactionObservation = "2.16.840.1.113883.10.20.22.4.9";
        public const string SeverityObservation = "2.16.840.1.113883.10.20.22.4.8";
        public const string MedicationActivity = "2.16.840.1.113883.10.20.22.4.16";
        public const string MedicationInformation = "2.16.840.1.113883.10.20.22.4.23";
        public const string ProblemSectionEntriesOptional = "2.16.840.1.113883.10.20.22.2.5";
        public const string ProblemSectionEntriesRequired = "2.16.840.1.113883.10.20.22.2.5.1";
        public const string ProblemConcernAct = "2.16.840.1.113883.10.20.22.4.3";
        public const string ProblemObservation = "2.16.840.1.113883.10.20.22.4.4";
        public const string ProblemStatus = "2.16.840.1.113883.10.20.22.4.6";
        public const string ProblemStatusActive = "2.16.840.1.113883.10.20.24.3.94";
        public const string ProblemStatusInactive = "2.16.840.1.113883.10.20.24.3.95";
        public const string ProceduresSectionEntriesOptional = "2.16.840.1.113883.10.20.22.2.7";
        public const string ProceduresSectionEntriesRequired = "2.16.840.1.113883.10.20.22.2.7.1";
        public const string ProcedureActivityProcedure = "2.16.840.1.113883.10.20.22.4.14";
        public const string ProcedureActivityObservation = "2.16.840.1.113883.10.20.22.4.13";
        public const string ProcedureActivityAct = "2.16.840.1.113883.10.20.22.4.12";
        public const string RoleClass = "2.16.840.1.113883.5.110";
        public const string SubstanceAdministration = "2.16.840.1.113883.10.20.22.4.52";
        public const string ImmunizationMedicationInformation = "2.16.840.1.113883.10.20.22.4.54";
        public const string ResultsSectionEntriesRequired = "2.16.840.1.113883.10.20.22.2.3.1";
        public const string ResultsOrganizer = "2.16.840.1.113883.10.20.22.4.1";
        public const string ResultsObservation = "2.16.840.1.113883.10.20.22.4.2";
        public const string ImmunizationSectionEntriesRequired = "2.16.840.1.113883.10.20.22.2.2.1";
        public const string SocialHistorySection = "2.16.840.1.113883.10.20.22.2.17";
        public const string SmokingStatusObservation = "2.16.840.1.113883.10.20.22.4.78";
        public const string VitalSignsSectionEntriesRequired = "2.16.840.1.113883.10.20.22.2.4.1";
        public const string VitalSignsOrganizer = "2.16.840.1.113883.10.20.22.4.26";
        public const string VitalSignsObservation = "2.16.840.1.113883.10.20.22.4.27";


        /// <summary>
        ///   QRDA Template OIDs 
        /// </summary>
        public const string Qrda = "2.16.840.1.113883.10.20.24.1.1";
        public const string QrdaClinicalDocument = "2.16.840.1.113883.10.20.24.1.2";
        public const string MedicareHicNumber = "2.16.840.1.113883.4.572";
        public const string MeasureSection = "2.16.840.1.113883.10.20.24.2.2";
        public const string QdmMeasureSection = "2.16.840.1.113883.10.20.24.2.3";
        public const string MeasureReference = "2.16.840.1.113883.10.20.24.3.98";
        public const string QdmElectronicMeasureReference = "2.16.840.1.113883.10.20.24.3.97";
        public const string ReportingParametersSection = "2.16.840.1.113883.10.20.17.2.1";
        public const string ReportingParametersAct = "2.16.840.1.113883.10.20.17.3.8";
        public const string PatientDataSection = "2.16.840.1.113883.10.20.17.2.4";
        public const string QdmPatientDataSection = "2.16.840.1.113883.10.20.24.2.1";
        public const string AssessmentScaleObservation = "2.16.840.1.113883.10.20.22.4.69";
        public const string AssessmentScaleObservationRiskCategoryAssessment = "2.16.840.1.113883.10.20.24.3.69";
        public const string CommunicationFromProviderToProvider = "2.16.840.1.113883.10.20.24.3.4";
        public const string EncounterActivities = "2.16.840.1.113883.10.20.22.4.49";
        public const string EncounterActivitiesEncounterPerformed = "2.16.840.1.113883.10.20.24.3.23";
        public const string EncounterDiagnosis = "2.16.840.1.113883.10.20.22.4.80";
        public const string MedicationActivityMedicationActive = "2.16.840.1.113883.10.20.24.3.41";
        public const string PlanOfCareActivityAct = "2.16.840.1.113883.10.20.22.4.39";
        public const string PlanOfCareActivityActInterventionOrder = "2.16.840.1.113883.10.20.24.3.31";
        public const string PlanOfCareActivityObservation = "2.16.840.1.113883.10.20.22.4.44";
        public const string PlanOfCareActivityObservationDiagnosticStudyOrder = "2.16.840.1.113883.10.20.24.3.17";
        public const string PlanOfCareActivityProcedure = "2.16.840.1.113883.10.20.22.4.41";
        public const string PlanOfCareActivityProcedureProcedureOrder = "2.16.840.1.113883.10.20.24.3.63";
        public const string PlanOfCareActivitySubstanceAdministration = "2.16.840.1.113883.10.20.22.4.42";
        public const string PlanOfCareActivitySubstanceAdministrationMedicationOrder = "2.16.840.1.113883.10.20.24.3.47";
        public const string ProblemObservationDiagnosisActive = "2.16.840.1.113883.10.20.24.3.11";
        public const string ProblemObservationDiagnosisInactive = "2.16.840.1.113883.10.20.24.3.13";
        public const string ProblemObservationDiagnosisResolved = "2.16.840.1.113883.10.20.24.3.14";
        public const string ProcedureActivityActInterventionPerformed = "2.16.840.1.113883.10.20.24.3.32";
        public const string ProcedureActivityObservationDiagnosticStudyPerformed = "2.16.840.1.113883.10.20.24.3.18";
        public const string ProcedureActivityObservationPhysicalExamPerformed = "2.16.840.1.113883.10.20.24.3.59";
        public const string ProcedureActivityProcedureProcedurePerformed = "2.16.840.1.113883.10.20.24.3.64";
        public const string ProviderPreference = "2.16.840.1.113883.10.20.24.3.84";
        public const string ResultObservationDiagnosticStudyResult = "2.16.840.1.113883.10.20.24.3.20";
        public const string ResultObservationPhysicalExamFinding = "2.16.840.1.113883.10.20.24.3.57";
        public const string SupplementalDataPatientPayer = "2.16.840.1.113883.10.20.24.3.55";
        public const string TobaccoUse = "2.16.840.1.113883.10.20.22.4.85";
        public const string OncMeaningfulUseEhrCertificationNumber = "2.16.840.1.113883.3.2074.1";

    }

    public enum CodeSystem
    {
        [Display(Name = "LOINC", ShortName = "2.16.840.1.113883.6.1")]
        Loinc,
        [Display(Name = "HL7 AdministrativeGender", ShortName = "2.16.840.1.113883.5.1")]
        AdministrativeGender,
        [Display(Name = "HL7 Marital status", ShortName = "2.16.840.1.113883.1.11.12212")]
        MaritalStatus,
        [Display(Name = "Race & Ethnicity - CDC", ShortName = "2.16.840.1.113883.6.238")]
        RaceAndEthnicity,
        [Display(Name = "NUCC", ShortName = "2.16.840.1.113883.6.101")]
        Nucc,
        [Display(Name = "ActCode", ShortName = "2.16.840.1.113883.5.4")]
        ActCode,
        [Display(Name = "SNOMED CT", ShortName = "2.16.840.1.113883.6.96")]
        SnomedCt,
        [Display(Name = "RxNorm", ShortName = "2.16.840.1.113883.6.88")]
        RxNorm,
        [Display(Name = "ActClass", ShortName = "2.16.840.1.113883.5.6")]
        ActClass,
        [Display(Name = "CVX", ShortName = "2.16.840.1.113883.12.292")]
        Cvx,
        [Display(Name = "Observation Interpretation", ShortName = "2.16.840.1.113883.5.83")]
        ObservationInterpretation,
        [Display(Name = "NCI Thesaurus", ShortName = "2.16.840.1.113883.3.26.1.1")]
        NciThesaurus,
        [Display(Name = "Observation Value", ShortName = "2.16.840.1.113883.5.1063")]
        ObservationValue,
        [Display(Name = "Source of Payment Typology", ShortName = "2.16.840.1.113883.3.221.5")]
        PayerTypology,
        [Display(Name = "Observation Population Inclusion", ShortName = "2.16.840.1.113883.1.11.20369")]
        ObservationPopulationInclusion,
        [Display(Name = "Medication Product Form", ShortName = "2.16.840.1.113883.3.88.12.3221.8.11")]
        MedicationProductForm,
        [Display(Name = "CPT", ShortName = "2.16.840.1.113883.6.12")]
        Cpt,
        [Display(Name = "ICD9CM", ShortName = "2.16.840.1.113883.6.103")]
        Icd9103,
        [Display(Name = "ICD9CM", ShortName = "2.16.840.1.113883.6.104")]
        Icd9104,
        [Display(Name = "ICD10CM", ShortName = "2.16.840.1.113883.6.90")]
        Icd10,
        [Display(Name = "CDCREC", ShortName = "2.16.840.1.113883.6.238")]
        Cdcrec,
        [Display(Name = "CDT", ShortName = "2.16.840.1.113883.6.13")]
        Cdt,
        [Display(Name = "HCPCS", ShortName = "2.16.840.1.113883.6.285")]
        Hcpcs,
        [Display(Name = "AdministrativeSex", ShortName = "2.16.840.1.113883.18.2")]
        AdministrativeSex

    }

    /// <summary>
    /// Well known groups of value sets.
    /// </summary>
    public static class QualityDataModelValueSetGroup
    {
        public static readonly QualityDataModelValueSet[] PsychCareValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedPsychVisitDiagnosticEvaluation, QualityDataModelValueSet.EncounterPerformedHealthAndBehavioralAssessmentInitial,
            QualityDataModelValueSet.EncounterPerformedHealthAndBehavioralAssessmentIndividual, QualityDataModelValueSet.EncounterPerformedOccupationalTherapyEvaluation,
            QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedPsychVisitPsychotherapy, QualityDataModelValueSet.EncounterPerformedPsychoanalysis,
            QualityDataModelValueSet.EncounterPerformedOphthalmologicalServices
        };

        public static readonly QualityDataModelValueSet[] PreventiveCareValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesGroupCounseling, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesOther,
            QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp,
            QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesindividualCounseling,QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction,
            QualityDataModelValueSet.EncounterPerformedAnnualWellnessVisit
        };

        public static readonly QualityDataModelValueSet[] AnyInteractionValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedOphthalmologicalServices, QualityDataModelValueSet.EncounterPerformedCareServicesInLongtermResidentialFacility, QualityDataModelValueSet.EncounterPerformedNursingFacilityVisit,
            QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedOutpatientConsultation, QualityDataModelValueSet.EncounterPerformedPatientProviderInteraction
        };

        public static readonly QualityDataModelValueSet[] EyeExamValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedOphthalmologicalServices, QualityDataModelValueSet.EncounterPerformedCareServicesInLongtermResidentialFacility, QualityDataModelValueSet.EncounterPerformedNursingFacilityVisit, QualityDataModelValueSet.EncounterPerformedOfficeVisit,
            QualityDataModelValueSet.EncounterPerformedOutpatientConsultation, QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction
        };

        public static readonly QualityDataModelValueSet[] RegularExamValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp,
            QualityDataModelValueSet.EncounterPerformedHomeHealthcareServices, QualityDataModelValueSet.EncounterPerformedAnnualWellnessVisit, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp
        };

        public static readonly QualityDataModelValueSet[] ReferralValueSets =
        {
            QualityDataModelValueSet.EncounterPerformedPreventiveCareInitialOfficeVisit0To17, QualityDataModelValueSet.EncounterPerformedPreventiveCareEstablishedOfficeVisit0To17,
            QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp,
            QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction
        };

    }

    public enum QualityDataModelValueSet
    {
        [Display(Name = "Attribute: Reason: Overweight", ShortName = "2.16.840.1.113883.3.600.2387")]
        AttributeReasonOverweight,
        [Display(Name = "Attribute: Reason: Underweight", ShortName = "2.16.840.1.113883.3.600.2388")]
        AttributeReasonUnderweight,
        [Display(Name = "Attribute: Result: Level of Severity of Retinopathy Findings", ShortName = "2.16.840.1.113883.3.526.3.1283")]
        AttributeResultLevelOfSeverityOfRetinopathyFindings,
        [Display(Name = "Attribute: Result: Macular Edema Findings Absent", ShortName = "2.16.840.1.113883.3.526.3.1284")]
        AttributeResultMacularEdemaFindingsAbsent,
        [Display(Name = "Attribute: Result: Macular Edema Findings Present", ShortName = "2.16.840.1.113883.3.526.3.1320")]
        AttributeResultMacularEdemaFindingsPresent,
        [Display(Name = "Attribute: Result: Negative Finding", ShortName = "2.16.840.1.113883.3.464.1003.195.12.1002")]
        AttributeResultNegativeFinding,
        [Display(Name = "Communication: From Provider to Provider: Consultant Report", ShortName = "2.16.840.1.113883.3.464.1003.121.12.1006")]
        CommunicationFromProviderToProviderConsultantReport,
        [Display(Name = "Communication: From Provider to Provider: Level of Severity of Retinopathy Findings", ShortName = "2.16.840.1.113883.3.526.3.1324")]
        CommunicationFromProviderToProviderLevelOfSeverityOfRetinopathyFindings,
        [Display(Name = "Communication: From Provider to Provider: Macular Edema Findings Absent", ShortName = "2.16.840.1.113883.3.526.3.1286")]
        CommunicationFromProviderToProviderMacularEdemaFindingsAbsent,
        [Display(Name = "Communication: From Provider to Provider: Macular Edema Findings Present", ShortName = "2.16.840.1.113883.3.526.3.1323")]
        CommunicationFromProviderToProviderMacularEdemaFindingsPresent,
        [Display(Name = "Communication: From Provider to Provider not done: Medical Reason", ShortName = "2.16.840.1.113883.3.526.3.1007")]
        CommunicationFromProviderToProviderNotDoneMedicalReason,
        [Display(Name = "Communication: From Provider to Provider not done: Patient Reason", ShortName = "2.16.840.1.113883.3.526.3.1008")]
        CommunicationFromProviderToProviderNotDonePatientReason,
        [Display(Name = "Diagnosis, Active: Acute and Subacute Iridocyclitis", ShortName = "2.16.840.1.113883.3.526.3.1241")]
        DiagnosisActiveAcuteAndSubacuteIridocyclitis,
        [Display(Name = "Diagnosis, Active: Adhesions and Disruptions of Iris and Ciliary Body", ShortName = "2.16.840.1.113883.3.526.3.1405")]
        DiagnosisActiveAdhesionsAndDisruptionsOfIrisAndCiliaryBody,
        [Display(Name = "Diagnosis, Active: Amblyopia", ShortName = "2.16.840.1.113883.3.526.3.1448")]
        DiagnosisActiveAmblyopia,
        [Display(Name = "Diagnosis, Active: Anomalies of Puillary Function", ShortName = "2.16.840.1.113883.3.526.3.1406")]
        DiagnosisActiveAnomaliesOfPuillaryFunction,
        [Display(Name = "Diagnosis, Active: Aphakia and other Disorders of Lens", ShortName = "2.16.840.1.113883.3.526.3.1407")]
        DiagnosisActiveAphakiaAndOtherDisordersOfLens,
        [Display(Name = "Diagnosis, Active: Burn Confined to Eye and Adnexa", ShortName = "2.16.840.1.113883.3.526.3.1409")]
        DiagnosisActiveBurnConfinedToEyeAndAdnexa,
        [Display(Name = "Diagnosis, Active: Cataract Secondary to Ocular Disorders", ShortName = "2.16.840.1.113883.3.526.3.1410")]
        DiagnosisActiveCataractSecondaryToOcularDisorders,
        [Display(Name = "Diagnosis, Active: Cataract, Congenital", ShortName = "2.16.840.1.113883.3.526.3.1412")]
        DiagnosisActiveCataractCongenital,
        [Display(Name = "Diagnosis, Active: Cataract, Mature or Hypermature", ShortName = "2.16.840.1.113883.3.526.3.1413")]
        DiagnosisActiveCataractMatureOrHypermature,
        [Display(Name = "Diagnosis, Active: Cataract, Posterior Polar", ShortName = "2.16.840.1.113883.3.526.3.1414")]
        DiagnosisActiveCataractPosteriorPolar,
        [Display(Name = "Diagnosis, Active: Central Corneal Ulcer", ShortName = "2.16.840.1.113883.3.526.3.1428")]
        DiagnosisActiveCentralCornealUlcer,
        [Display(Name = "Diagnosis, Active: Certain Types of Iridocyclitis", ShortName = "2.16.840.1.113883.3.526.3.1415")]
        DiagnosisActiveCertainTypesOfIridocyclitis,
        [Display(Name = "Diagnosis, Active: Chorioretinal Scars", ShortName = "2.16.840.1.113883.3.526.3.1449")]
        DiagnosisActiveChorioretinalScars,
        [Display(Name = "Diagnosis, Active: Choroidal Degenerations", ShortName = "2.16.840.1.113883.3.526.3.1450")]
        DiagnosisActiveChoroidalDegenerations,
        [Display(Name = "Diagnosis, Active: Choroidal Detachment", ShortName = "2.16.840.1.113883.3.526.3.1451")]
        DiagnosisActiveChoroidalDetachment,
        [Display(Name = "Diagnosis, Active: Choroidal Hemorrhage and Rupture", ShortName = "2.16.840.1.113883.3.526.3.1452")]
        DiagnosisActiveChoroidalHemorrhageAndRupture,
        [Display(Name = "Diagnosis, Active: Chronic Iridocyclitis", ShortName = "2.16.840.1.113883.3.526.3.1416")]
        DiagnosisActiveChronicIridocyclitis,
        [Display(Name = "Diagnosis, Active: Chronic Kidney Disease, Stage 5", ShortName = "2.16.840.1.113883.3.526.3.1002")]
        DiagnosisActiveChronicKidneyDiseaseStage5,
        [Display(Name = "Diagnosis, Active: Cloudy Cornea", ShortName = "2.16.840.1.113883.3.526.3.1417")]
        DiagnosisActiveCloudyCornea,
        [Display(Name = "Diagnosis, Active: Corneal Edema", ShortName = "2.16.840.1.113883.3.526.3.1418")]
        DiagnosisActiveCornealEdema,
        [Display(Name = "Diagnosis, Active: Corneal Opacity and other Disorders of Cornea", ShortName = "2.16.840.1.113883.3.526.3.1419")]
        DiagnosisActiveCornealOpacityAndOtherDisordersOfCornea,
        [Display(Name = "Diagnosis, Active: Cysts of Iris, Ciliary Body, and Anterior Chamber", ShortName = "2.16.840.1.113883.3.526.3.1420")]
        DiagnosisActiveCystsOfIrisCiliaryBodyAndAnteriorChamber,
        [Display(Name = "Diagnosis, Active: Degeneration of Macula and Posterior Pole", ShortName = "2.16.840.1.113883.3.526.3.1453")]
        DiagnosisActiveDegenerationOfMaculaAndPosteriorPole,
        [Display(Name = "Diagnosis, Active: Degenerative Disorders of Globe", ShortName = "2.16.840.1.113883.3.526.3.1454")]
        DiagnosisActiveDegenerativeDisordersOfGlobe,
        [Display(Name = "Diagnosis, Active: Diabetes", ShortName = "2.16.840.1.113883.3.464.1003.103.12.1001")]
        DiagnosisActiveDiabetes,
        [Display(Name = "Diagnosis, Active: Diabetic Macular Edema", ShortName = "2.16.840.1.113883.3.526.3.1455")]
        DiagnosisActiveDiabeticMacularEdema,
        [Display(Name = "Diagnosis, Active: Diabetic Retinopathy", ShortName = "2.16.840.1.113883.3.526.3.327")]
        DiagnosisActiveDiabeticRetinopathy,
        [Display(Name = "Diagnosis, Active: Disorders of Optic Chiasm", ShortName = "2.16.840.1.113883.3.526.3.1457")]
        DiagnosisActiveDisordersOfOpticChiasm,
        [Display(Name = "Diagnosis, Active: Disorders of Visual Cortex", ShortName = "2.16.840.1.113883.3.526.3.1458")]
        DiagnosisActiveDisordersOfVisualCortex,
        [Display(Name = "Diagnosis, Active: Disseminated Chorioretinitis and Disseminated Retinochoroiditis", ShortName = "2.16.840.1.113883.3.526.3.1459")]
        DiagnosisActiveDisseminatedChorioretinitisAndDisseminatedRetinochoroiditis,
        [Display(Name = "Diagnosis, Active: End Stage Renal Disease", ShortName = "2.16.840.1.113883.3.526.3.353")]
        DiagnosisActiveEndStageRenalDisease,
        [Display(Name = "Diagnosis, Active: Enophthalmos", ShortName = "2.16.840.1.113883.3.526.3.1421")]
        DiagnosisActiveEnophthalmos,
        [Display(Name = "Diagnosis, Active: Essential Hypertension", ShortName = "2.16.840.1.113883.3.464.1003.104.12.1011")]
        DiagnosisActiveEssentialHypertension,
        [Display(Name = "Diagnosis, Active: Focal Chorioretinitis and Focal Retinochoroiditis", ShortName = "2.16.840.1.113883.3.526.3.1460")]
        DiagnosisActiveFocalChorioretinitisAndFocalRetinochoroiditis,
        [Display(Name = "Diagnosis, Active: Gestational Diabetes", ShortName = "2.16.840.1.113883.3.464.1003.103.12.1010")]
        DiagnosisActiveGestationalDiabetes,
        [Display(Name = "Diagnosis, Active: Glaucoma Associated with Congenital Anomalies, Dystrophies, and Systemic Syndromes", ShortName = "2.16.840.1.113883.3.526.3.1461")]
        DiagnosisActiveGlaucomaAssociatedWithCongenitalAnomaliesDystrophiesAndSystemicSyndromes,
        [Display(Name = "Diagnosis, Active: Glaucoma", ShortName = "2.16.840.1.113883.3.526.3.1423")]
        DiagnosisActiveGlaucoma,
        [Display(Name = "Diagnosis, Active: Hereditary Choroidal Dystrophies", ShortName = "2.16.840.1.113883.3.526.3.1462")]
        DiagnosisActiveHereditaryChoroidalDystrophies,
        [Display(Name = "Diagnosis, Active: Hereditary Corneal Dystrophies", ShortName = "2.16.840.1.113883.3.526.3.1424")]
        DiagnosisActiveHereditaryCornealDystrophies,
        [Display(Name = "Diagnosis, Active: Hereditary Retinal Dystrophies", ShortName = "2.16.840.1.113883.3.526.3.1463")]
        DiagnosisActiveHereditaryRetinalDystrophies,
        [Display(Name = "Diagnosis, Active: High Hyperopia", ShortName = "2.16.840.1.113883.3.526.3.1425")]
        DiagnosisActiveHighHyperopia,
        [Display(Name = "Diagnosis, Active: Hypotony of Eye", ShortName = "2.16.840.1.113883.3.526.3.1426")]
        DiagnosisActiveHypotonyOfEye,
        [Display(Name = "Diagnosis, Active: Injury to Optic Nerve and Pathways", ShortName = "2.16.840.1.113883.3.526.3.1427")]
        DiagnosisActiveInjuryToOpticNerveAndPathways,
        [Display(Name = "Diagnosis, Active: Limited Life Expectancy", ShortName = "2.16.840.1.113883.3.526.3.1259")]
        DiagnosisActiveLimitedLifeExpectancy,
        [Display(Name = "Diagnosis, Active: Moderate or Severe Impairment, Better Eye, Profound Impairment Lesser Eye", ShortName = "2.16.840.1.113883.3.526.3.1464")]
        DiagnosisActiveModerateOrSevereImpairmentBetterEyeProfoundImpairmentLesserEye,
        [Display(Name = "Diagnosis, Active: Nystagmus and other Irregular Eye Movements", ShortName = "2.16.840.1.113883.3.526.3.1465")]
        DiagnosisActiveNystagmusAndOtherIrregularEyeMovements,
        [Display(Name = "Diagnosis, Active: Open Wound of Eyeball", ShortName = "2.16.840.1.113883.3.526.3.1430")]
        DiagnosisActiveOpenWoundOfEyeball,
        [Display(Name = "Diagnosis, Active: Optic Atrophy", ShortName = "2.16.840.1.113883.3.526.3.1466")]
        DiagnosisActiveOpticAtrophy,
        [Display(Name = "Diagnosis, Active: Optic Neuritis", ShortName = "2.16.840.1.113883.3.526.3.1467")]
        DiagnosisActiveOpticNeuritis,
        [Display(Name = "Diagnosis, Active: Other and Unspecified forms of Chorioretinitis and Retinochoroiditis", ShortName = "2.16.840.1.113883.3.526.3.1468")]
        DiagnosisActiveOtherAndUnspecifiedFormsOfChorioretinitisAndRetinochoroiditis,
        [Display(Name = "Diagnosis, Active: Other Background Retinopathy and Retinal Vascular Changes", ShortName = "2.16.840.1.113883.3.526.3.1469")]
        DiagnosisActiveOtherBackgroundRetinopathyAndRetinalVascularChanges,
        [Display(Name = "Diagnosis, Active: Other Corneal Deformities", ShortName = "2.16.840.1.113883.3.526.3.1470")]
        DiagnosisActiveOtherCornealDeformities,
        [Display(Name = "Diagnosis, Active: Other Disorders of Optic Nerve", ShortName = "2.16.840.1.113883.3.526.3.1471")]
        DiagnosisActiveOtherDisordersOfOpticNerve,
        [Display(Name = "Diagnosis, Active: Other Disorders of Sclera", ShortName = "2.16.840.1.113883.3.526.3.1472")]
        DiagnosisActiveOtherDisordersOfSclera,
        [Display(Name = "Diagnosis, Active: Other Endophthalmitis", ShortName = "2.16.840.1.113883.3.526.3.1473")]
        DiagnosisActiveOtherEndophthalmitis,
        [Display(Name = "Diagnosis, Active: Other Proliferative Retinopathy", ShortName = "2.16.840.1.113883.3.526.3.1480")]
        DiagnosisActiveOtherProliferativeRetinopathy,
        [Display(Name = "Diagnosis, Active: Other Retinal Disorders", ShortName = "2.16.840.1.113883.3.526.3.1474")]
        DiagnosisActiveOtherRetinalDisorders,
        [Display(Name = "Diagnosis, Active: Pathologic Myopia", ShortName = "2.16.840.1.113883.3.526.3.1432")]
        DiagnosisActivePathologicMyopia,
        [Display(Name = "Diagnosis, Active: Posterior Lenticonus", ShortName = "2.16.840.1.113883.3.526.3.1433")]
        DiagnosisActivePosteriorLenticonus,
        [Display(Name = "Diagnosis, Active: Pregnancy Dx", ShortName = "2.16.840.1.113883.3.600.1.1623")]
        DiagnosisActivePregnancyDx,
        [Display(Name = "Diagnosis, Active: Pregnancy", ShortName = "2.16.840.1.113883.3.526.3.378")]
        DiagnosisActivePregnancy,
        [Display(Name = "Diagnosis, Active: Primary Open Angle Glaucoma (POAG)", ShortName = "2.16.840.1.113883.3.526.3.326")]
        DiagnosisActivePrimaryOpenAngleGlaucoma,
        [Display(Name = "Diagnosis, Active: Prior Penetrating Keratoplasty", ShortName = "2.16.840.1.113883.3.526.3.1475")]
        DiagnosisActivePriorPenetratingKeratoplasty,
        [Display(Name = "Diagnosis, Active: Profound Impairment, Both Eyes", ShortName = "2.16.840.1.113883.3.526.3.1476")]
        DiagnosisActiveProfoundImpairmentBothEyes,
        [Display(Name = "Diagnosis, Active: Pseudoexfoliation Syndrome", ShortName = "2.16.840.1.113883.3.526.3.1435")]
        DiagnosisActivePseudoexfoliationSyndrome,
        [Display(Name = "Diagnosis, Active: Purulent Endophthalmitis", ShortName = "2.16.840.1.113883.3.526.3.1477")]
        DiagnosisActivePurulentEndophthalmitis,
        [Display(Name = "Diagnosis, Active: Retinal Detachment with Retinal Defect", ShortName = "2.16.840.1.113883.3.526.3.1478")]
        DiagnosisActiveRetinalDetachmentWithRetinalDefect,
        [Display(Name = "Diagnosis, Active: Retinal Vascular Occlusion", ShortName = "2.16.840.1.113883.3.526.3.1479")]
        DiagnosisActiveRetinalVascularOcclusion,
        [Display(Name = "Diagnosis, Active: Retrolental Fibroplasias", ShortName = "2.16.840.1.113883.3.526.3.1438")]
        DiagnosisActiveRetrolentalFibroplasias,
        [Display(Name = "Diagnosis, Active: Scleritis and Episcleritis", ShortName = "2.16.840.1.113883.3.526.3.1481")]
        DiagnosisActiveScleritisAndEpiscleritis,
        [Display(Name = "Diagnosis, Active: Senile Cataract", ShortName = "2.16.840.1.113883.3.526.3.1441")]
        DiagnosisActiveSenileCataract,
        [Display(Name = "Diagnosis, Active: Separation of Retinal Layers", ShortName = "2.16.840.1.113883.3.526.3.1482")]
        DiagnosisActiveSeparationOfRetinalLayers,
        [Display(Name = "Diagnosis, Active: Traumatic Cataract", ShortName = "2.16.840.1.113883.3.526.3.1443")]
        DiagnosisActiveTraumaticCataract,
        [Display(Name = "Diagnosis, Active: Uveitis", ShortName = "2.16.840.1.113883.3.526.3.1444")]
        DiagnosisActiveUveitis,
        [Display(Name = "Diagnosis, Active: Vascular Disorders of Iris and Ciliary Body", ShortName = "2.16.840.1.113883.3.526.3.1445")]
        DiagnosisActiveVascularDisordersOfIrisAndCiliaryBody,
        [Display(Name = "Diagnosis, Active: Visual Field Defects", ShortName = "2.16.840.1.113883.3.526.3.1446")]
        DiagnosisActiveVisualFieldDefects,
        [Display(Name = "Diagnostic Study, Performed not done: Medical Reason", ShortName = "2.16.840.1.113883.3.526.3.1007")]
        DiagnosticStudyPerformednotdoneMedicalReason,
        [Display(Name = "Diagnostic Study, Performed not done: Patient Reason", ShortName = "2.16.840.1.113883.3.526.3.1008")]
        DiagnosticStudyPerformednotdonePatientReason,
        [Display(Name = "Diagnostic Study, Performed: Macular Exam", ShortName = "2.16.840.1.113883.3.526.3.1251")]
        DiagnosticStudyPerformedMacularExam,
        [Display(Name = "Diagnostic Study, Result not done: Medical Reason", ShortName = "2.16.840.1.113883.3.526.3.1007")]
        DiagnosticStudyResultnotdoneMedicalReason,
        [Display(Name = "Diagnostic Study, Result: Cup to Disc Ratio", ShortName = "2.16.840.1.113883.3.526.3.1333")]
        DiagnosticStudyResultCupToDiscRatio,
        [Display(Name = "Diagnostic Study, Result: Macular Exam", ShortName = "2.16.840.1.113883.3.526.3.1251")]
        DiagnosticStudyResultMacularExam,
        [Display(Name = "Diagnostic Study, Result: Optic Disc Exam for Structural Abnormalities", ShortName = "2.16.840.1.113883.3.526.3.1334")]
        DiagnosticStudyResultOpticDiscExamForStructuralAbnormalities,
        [Display(Name = "Diagnostic Study, Result: Visual acuity 20/40 or Better", ShortName = "2.16.840.1.113883.3.526.3.1483")]
        DiagnosticStudyResultVisualAcuity2040OrBetter,
        [Display(Name = "Encounter, Performed: Annual Wellness Visit", ShortName = "2.16.840.1.113883.3.526.3.1240")]
        EncounterPerformedAnnualWellnessVisit,
        [Display(Name = "Encounter, Performed: BMI Encounter Code Set", ShortName = "2.16.840.1.113883.3.600.1.1751")]
        EncounterPerformedBmiEncounterCodeSet,
        [Display(Name = "Encounter, Performed: Care Services in Long-Term Residential Facility", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1014")]
        EncounterPerformedCareServicesInLongtermResidentialFacility,
        [Display(Name = "Encounter, Performed: ESRD Monthly Outpatient Services", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1014")]
        EncounterPerformedEsrdMonthlyOutpatientServices,
        [Display(Name = "Encounter, Performed: Face-to-Face Interaction", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1048")]
        EncounterPerformedFacetofaceInteraction,
        [Display(Name = "Encounter, Performed: Health & Behavioral Assessment - Individual", ShortName = "2.16.840.1.113883.3.526.3.1020")]
        EncounterPerformedHealthAndBehavioralAssessmentIndividual,
        [Display(Name = "Encounter, Performed: Health and Behavioral Assessment - Initial", ShortName = "2.16.840.1.113883.3.526.3.1245")]
        EncounterPerformedHealthAndBehavioralAssessmentInitial,
        [Display(Name = "Encounter, Performed: Home Healthcare Services", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1016")]
        EncounterPerformedHomeHealthcareServices,
        [Display(Name = "Encounter, Performed: Medications Encounter Code Set", ShortName = "2.16.840.1.113883.3.600.1.1834")]
        EncounterPerformedMedicationsEncounterCodeSet,
        [Display(Name = "Encounter, Performed: Nursing Facility Visit", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1012")]
        EncounterPerformedNursingFacilityVisit,
        [Display(Name = "Encounter, Performed: Occupational Therapy Evaluation", ShortName = "2.16.840.1.113883.3.526.3.1011")]
        EncounterPerformedOccupationalTherapyEvaluation,
        [Display(Name = "Encounter, Performed: Office Visit", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1001")]
        EncounterPerformedOfficeVisit,
        [Display(Name = "Encounter, Performed: Ophthalmological Services", ShortName = "2.16.840.1.113883.3.526.3.1285")]
        EncounterPerformedOphthalmologicalServices,
        [Display(Name = "Encounter, Performed: Outpatient Consultation", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1008")]
        EncounterPerformedOutpatientConsultation,
        [Display(Name = "Encounter, Performed: Patient Provider Interaction", ShortName = "2.16.840.1.113883.3.526.3.1012")]
        EncounterPerformedPatientProviderInteraction,
        [Display(Name = "Encounter, Performed: Preventive Care - Established Office Visit, 0 to 17", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1024")]
        EncounterPerformedPreventiveCareEstablishedOfficeVisit0To17,
        [Display(Name = "Encounter, Performed: Preventive Care- Initial Office Visit, 0 to 17", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1022")]
        EncounterPerformedPreventiveCareInitialOfficeVisit0To17,
        [Display(Name = "Encounter, Performed: Preventive Care Services - Established Office Visit, 18 and Up", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1025")]
        EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp,
        [Display(Name = "Encounter, Performed: Preventive Care Services - Group Counseling", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1027")]
        EncounterPerformedPreventiveCareServicesGroupCounseling,
        [Display(Name = "Encounter, Performed: Preventive Care Services - Other", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1030")]
        EncounterPerformedPreventiveCareServicesOther,
        [Display(Name = "Encounter, Performed: Preventive Care Services-Individual Counseling", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1026")]
        EncounterPerformedPreventiveCareServicesindividualCounseling,
        [Display(Name = "Encounter, Performed: Preventive Care Services-Initial Office Visit, 18 and Up", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1023")]
        EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp,
        [Display(Name = "Encounter, Performed: Psych Visit - Diagnostic Evaluation", ShortName = "2.16.840.1.113883.3.526.3.1492")]
        EncounterPerformedPsychVisitDiagnosticEvaluation,
        [Display(Name = "Encounter, Performed: Psych Visit - Psychotherapy", ShortName = "2.16.840.1.113883.3.526.3.1496")]
        EncounterPerformedPsychVisitPsychotherapy,
        [Display(Name = "Encounter, Performed: Psychoanalysis", ShortName = "2.16.840.1.113883.3.526.3.1141")]
        EncounterPerformedPsychoanalysis,
        [Display(Name = "Intervention, Order: Above Normal Follow-up", ShortName = "2.16.840.1.113883.3.600.1.1525")]
        InterventionOrderAboveNormalFollowup,
        [Display(Name = "Intervention, Order: Below Normal Follow up", ShortName = "2.16.840.1.113883.3.600.1.1528")]
        InterventionOrderBelowNormalFollowUp,
        [Display(Name = "Intervention, Order: Referrals where weight assessment may occur", ShortName = "2.16.840.1.113883.3.600.1.1527")]
        InterventionOrderReferralsWhereWeightAssessmentMayOccur,
        [Display(Name = "Intervention, Performed: Dialysis Education", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1016")]
        InterventionPerformedDialysisEducation,
        [Display(Name = "Intervention, Performed: Other Services Related to Dialysis", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1015")]
        InterventionPerformedOtherServicesRelatedToDialysis,
        [Display(Name = "Intervention, Performed: Referral", ShortName = "2.16.840.1.113883.3.464.1003.101.12.1046")]
        InterventionPerformedReferral,
        [Display(Name = "Intervention, Performed: Tobacco Use Cessation Counseling", ShortName = "2.16.840.1.113883.3.526.3.509")]
        InterventionPerformedTobaccoUseCessationCounseling,
        [Display(Name = "Medication, Active: Tobacco Use Cessation Pharmacotherapy", ShortName = "2.16.840.1.113883.3.526.3.1190")]
        MedicationActiveTobaccoUseCessationPharmacotherapy,
        [Display(Name = "Medication, Active: Use of Systemic Sympathetic alpha-1a Antagonist Medication for Treatment of Prostatic Hypertrophy", ShortName = "2.16.840.1.113883.3.526.3.1442")]
        // ReSharper disable once InconsistentNaming
        MedicationActiveUseOfSystemicSympatheticAlpha1aAntagonistMedicationForTreatmentOfProstaticHypertrophy,
        [Display(Name = "Medication, Order: Above Normal Medications", ShortName = "2.16.840.1.113883.3.600.1.1498")]
        MedicationOrderAboveNormalMedications,
        [Display(Name = "Medication, Order: Below Normal Medications", ShortName = "2.16.840.1.113883.3.600.1.1499")]
        MedicationOrderBelowNormalMedications,
        [Display(Name = "Medication, Order: High Risk Medications for the Elderly", ShortName = "2.16.840.1.113883.3.464.1003.196.12.1253")]
        MedicationOrderHighRiskMedicationsForTheElderly,
        [Display(Name = "Medication, Order: High-Risk Medications With Days Supply Criteria", ShortName = "2.16.840.1.113883.3.464.1003.196.12.1254")]
        MedicationOrderHighriskMedicationsWithDaysSupplyCriteria,
        [Display(Name = "Medication, Order: Tobacco Use Cessation Pharmacotherapy", ShortName = "2.16.840.1.113883.3.526.3.1190")]
        MedicationOrderTobaccoUseCessationPharmacotherapy,
        [Display(Name = "Patient Characteristic Birthdate: birth date", ShortName = "2.16.840.1.113883.3.560.100.4")]
        PatientCharacteristicBirthdateBirthDate,
        [Display(Name = "Patient Characteristic Ethnicity: Ethnicity", ShortName = "2.16.840.1.114222.4.11.837")]
        PatientCharacteristicEthnicityEthnicity,
        [Display(Name = "Patient Characteristic Payer: Payer", ShortName = "2.16.840.1.114222.4.11.3591")]
        PatientCharacteristicPayerPayer,
        [Display(Name = "Patient Characteristic Race: Race", ShortName = "2.16.840.1.114222.4.11.836")]
        PatientCharacteristicRaceRace,
        [Display(Name = "Patient Characteristic Sex: ONC Administrative Sex", ShortName = "2.16.840.1.113762.1.4.1")]
        PatientCharacteristicSexOncAdministrativeSex,
        [Display(Name = "Patient Characteristic: Tobacco Non-User", ShortName = "2.16.840.1.113883.3.526.3.1189")]
        PatientCharacteristicTobaccoNonuser,
        [Display(Name = "Patient Characteristic: Tobacco User", ShortName = "2.16.840.1.113883.3.526.3.1170")]
        PatientCharacteristicTobaccoUser,
        [Display(Name = "Physical Exam, Finding: Best Corrected Visual Acuity", ShortName = "2.16.840.1.113883.3.526.3.1488")]
        PhysicalExamFindingBestCorrectedVisualAcuity,
        [Display(Name = "Physical Exam, Finding: BMI LOINC Value", ShortName = "2.16.840.1.113883.3.600.1.681")]
        PhysicalExamFindingBmiLoincValue,
        [Display(Name = "Physical Exam, Finding: Diastolic Blood Pressure", ShortName = "2.16.840.1.113883.3.526.3.1033")]
        PhysicalExamFindingDiastolicBloodPressure,
        [Display(Name = "Physical Exam, Finding: Retinal or Dilated Eye Exam", ShortName = "2.16.840.1.113883.3.464.1003.115.12.1088")]
        PhysicalExamFindingRetinalOrDilatedEyeExam,
        [Display(Name = "Physical Exam, Finding: Systolic Blood Pressure", ShortName = "2.16.840.1.113883.3.526.3.1032")]
        PhysicalExamFindingSystolicBloodPressure,
        [Display(Name = "Physical Exam, Performed not done: Medical or Other reason not done", ShortName = "2.16.840.1.113883.3.600.1.1502")]
        PhysicalExamPerformednotdoneMedicalOrOtherReasonNotDone,
        [Display(Name = "Physical Exam, Performed not done: Patient Reason Refused", ShortName = "2.16.840.1.113883.3.600.1.1503")]
        PhysicalExamPerformednotdonePatientReasonRefused,
        [Display(Name = "Physical Exam, Performed: BMI LOINC Value", ShortName = "2.16.840.1.113883.3.600.1.681")]
        PhysicalExamPerformedBmiLoincValue,
        [Display(Name = "Physical Exam, Performed: Retinal or Dilated Eye Exam", ShortName = "2.16.840.1.113883.3.464.1003.115.12.1088")]
        PhysicalExamPerformedRetinalOrDilatedEyeExam,
        [Display(Name = "Procedure, Order: Palliative Care", ShortName = "2.16.840.1.113883.3.600.1.1579")]
        ProcedureOrderPalliativeCare,
        [Display(Name = "Procedure, Performed not done: Medical or Other reason not done", ShortName = "2.16.840.1.113883.3.600.1.1502")]
        ProcedurePerformednotdoneMedicalOrOtherReasonNotDone,
        [Display(Name = "Procedure, Performed: Aspiration and Injection Procedures", ShortName = "2.16.840.1.113883.3.526.3.1408")]
        ProcedurePerformedAspirationAndInjectionProcedures,
        [Display(Name = "Procedure, Performed: Cataract Surgery", ShortName = "2.16.840.1.113883.3.526.3.1411")]
        ProcedurePerformedCataractSurgery,
        [Display(Name = "Procedure, Performed: Current Medications Documented SNMD", ShortName = "2.16.840.1.113883.3.600.1.462")]
        ProcedurePerformedCurrentMedicationsDocumentedSnmd,
        [Display(Name = "Procedure, Performed: Dialysis Services", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1013")]
        ProcedurePerformedDialysisServices,
        [Display(Name = "Procedure, Performed: Excision of Adhesions", ShortName = "2.16.840.1.113883.3.526.3.1422")]
        ProcedurePerformedExcisionOfAdhesions,
        [Display(Name = "Procedure, Performed: Kidney Transplant", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1012")]
        ProcedurePerformedKidneyTransplant,
        [Display(Name = "Procedure, Performed: Lens Procedure", ShortName = "2.16.840.1.113883.3.526.3.1429")]
        ProcedurePerformedLensProcedure,
        [Display(Name = "Procedure, Performed: Paracentesis Procedures", ShortName = "2.16.840.1.113883.3.526.3.1431")]
        ProcedurePerformedParacentesisProcedures,
        [Display(Name = "Procedure, Performed: Prior Pars Plana Vitrectomy", ShortName = "2.16.840.1.113883.3.526.3.1434")]
        ProcedurePerformedPriorParsPlanaVitrectomy,
        [Display(Name = "Procedure, Performed: Removal Procedures", ShortName = "2.16.840.1.113883.3.526.3.1436")]
        ProcedurePerformedRemovalProcedures,
        [Display(Name = "Procedure, Performed: Retinal Repair Procedures", ShortName = "2.16.840.1.113883.3.526.3.1437")]
        ProcedurePerformedRetinalRepairProcedures,
        [Display(Name = "Procedure, Performed: Revision Procedures", ShortName = "2.16.840.1.113883.3.526.3.1439")]
        ProcedurePerformedRevisionProcedures,
        [Display(Name = "Procedure, Performed: Scleral Procedures", ShortName = "2.16.840.1.113883.3.526.3.1440")]
        ProcedurePerformedScleralProcedures,
        [Display(Name = "Procedure, Performed: Vascular Access for Dialysis", ShortName = "2.16.840.1.113883.3.464.1003.109.12.1011")]
        ProcedurePerformedVascularAccessForDialysis,
        [Display(Name = "Procedure, Performed: Vitreous Procedures", ShortName = "2.16.840.1.113883.3.526.3.1447")]
        ProcedurePerformedVitreousProcedures,
        [Display(Name = "Risk Category Assessment not done: Medical Reason", ShortName = "2.16.840.1.113883.3.526.3.1007")]
        RiskCategoryAssessmentnotdoneMedicalReason,
        [Display(Name = "Risk Category Assessment: Tobacco Use Screening", ShortName = "2.16.840.1.113883.3.526.3.1278")]
        RiskCategoryAssessmentTobaccoUseScreening,
        [Display(Name = "Individual Characteristic: Payer", ShortName = "2.16.840.1.114222.4.11.3591")]
        IndividualCharacteristicPayer

    }

    /// <summary>
    /// </summary>
    public class Constants
    {
        public static string LocalApplicationId = "IO PRACTICEWARE";

        /// <summary>
        ///   The IO Practiceware OID Key assigned by HL7.org
        /// </summary>
        public static string IOPracticewareOidKey = " 0CD9CFD8-410A-415B-BD63-DCCC6B5E88A4";

        public static string CvxAdministeredExternalSystemName = "CvxVaccineAdministered";
        public static string CvxAdministeredExternalSystemNameForDisplay = "CVX";
        public static string CvxAdministeredVaccineExternalEntityName = "Vaccine";

        public static string LoincExternalSystemName = "LOINC";
        public static string LoincLaboratoryTestExternalEntityName = "LaboratoryTest";

        public static string SnomedExternalSystemName = "SNOMEDCT";
        public static string SnomedExternalSystemnameForDisplay = "SNOMED-CT";
        public static string SnomedClinicalProcedureExternalEntityName = "ClinicalProcedure";
        public static string SnomedClinicalConditionExternalEntityName = "ClinicalCondition";
        public static string SnomedClinicalConditionStatusExternalEntityName = "ClinicalConditionStatus";
        public static string SnomedLateralityEntityName = "Laterality";
        public static string SnomedTreatmentGoalExternalEntityName = "TreatmentGoal";
        public static string SnomedSmokingConditionExternalEntityName = "SmokingCondition";
        public static string SnomedAllergenReactionTypeExternalEntityName = "AllergenReactionType";
        public static string SnomedClinicalQualifierExternalEntityName = "ClinicalQualifier";

        public static string RxNormExternalSystemName = "RxNorm";
        public static string RxNormMedicationsExternalEntityName = "Medication";
        public static string CdcExternalSystemName = "CDC";
        public static string CdcRaceAndEthnicityExtenalEntityName = "RaceAndEthnicityCodeSet";
        public static string RxNormAllergenExternalEntityName = "Allergen";

        public static string QualityDataModelExternalSystemName = "QualityDataModel";
    }


    // ReSharper disable InconsistentNaming
    /// <summary>
    ///   A list of values that represent the HL7 valid list of "Null Flavors" that are acceptable in a CDA message. These
    ///   values are used in place of areas or component entries in the CDA where the data is not available.  Instead of 
    ///   leaving out that particular entry, the standard specifies you must add a default entry with one of the values listed below.
    /// </summary>
    /// <remarks>
    ///   The enum values are arbitrary.  What's important is the string representation of these values.  You should use for example:
    ///   CdaNullFlavor.NAV.GetDisplayName() when entering null values into the CDA message.
    /// </remarks>
    public enum CdaNullFlavor
    {
        /// <summary>
        ///   Asked, but not known
        /// </summary>
        [Description("Asked, but not known")]
        [Display(Name = "ASKU")]
        ASKU = 1,

        /// <summary>
        ///   There is information on this item available but it has not been provided by the sender due to security, privacy, or other reasons.
        /// </summary>
        [Description("There is information on this item available but it has not been provided by the sender due to security, privacy, or other reasons.")]
        [Display(Name = "MSK")]
        MSK = 2,

        /// <summary>
        ///   Not asked
        /// </summary>
        [Description("Not asked")]
        [Display(Name = "NASK")]
        NASK = 3,

        /// <summary>
        ///   Temporarily unavailable
        /// </summary>
        [Description("Temporarily unavailable")]
        [Display(Name = "NAV")]
        NAV = 4,

        /// <summary>
        ///   Not applicable
        /// </summary>
        [Description("Not applicable")]
        [Display(Name = "NA")]
        NA = 5,

        /// <summary>
        ///   No information. This is the most general and default null flavor
        /// </summary>
        [Description("No information. This is the most general and default null flavor")]
        [Display(Name = "NI")]
        NI = 6,

        /// <summary>
        ///   Unknown. A proper value is applicable, but is not known
        /// </summary>
        [Description("Unknown. A proper value is applicable, but is not known")]
        [Display(Name = "UNK")]
        UNK = 7
    }


    /// <summary>
    ///   The phone number code values specified the HL7 MU guideline for use in the telecom element
    /// </summary>
    /// <remarks>
    ///   Value Set: Telecom Use (US Realm Header) 2.16.840.1.113883.11.20.9.20 DYNAMIC Code System(s): AddressUse 2.16.840.1.113883.5.1119
    /// </remarks>
    public enum CdaTelecomPhoneNumberCode
    {
        /// <summary>
        ///   Primary Home
        /// </summary>
        [Description("primary home")]
        [Display(Name = "HP")]
        HP = 1,

        /// <summary>
        ///   Vacation Home
        /// </summary>
        [Description("vacation home")]
        [Display(Name = "HV")]
        HV,

        /// <summary>
        ///   Mobile Contact
        /// </summary>
        [Description("mobile contact")]
        [Display(Name = "MC")]
        MC,

        /// <summary>
        ///   Work Place
        /// </summary>
        [Description("work place")]
        [Display(Name = "WP")]
        WP
    }


    /// <summary>
    ///   The address code values specified in the HL7 MU guideline for use in the addr element
    /// </summary>
    /// <remarks>
    ///   Value Set: PostalAddressUse 2.16.840.1.113883.1.11.10637 STATIC 2005-05-01 Code System(s): AddressUse 2.16.840.1.113883.5.1119
    /// </remarks>
    public enum CdaPostalAddressCode
    {
        /// <summary>
        ///   bad address
        /// </summary>
        [Description("bad address")]
        [Display(Name = "BAD")]
        BAD,

        /// <summary>
        ///   direct
        /// </summary>
        [Description("direct")]
        [Display(Name = "DIR")]
        DIR,

        /// <summary>
        ///   home address
        /// </summary>
        [Description("home address")]
        [Display(Name = "H")]
        H,

        /// <summary>
        ///   primary home
        /// </summary>
        [Description("primary home")]
        [Display(Name = "HP")]
        HP,

        /// <summary>
        ///   vacation home
        /// </summary>
        [Description("vacation home")]
        [Display(Name = "HV")]
        HV,

        /// <summary>
        ///   physical visit address
        /// </summary>
        [Description("physical visit address")]
        [Display(Name = "PHYS")]
        PHYS,

        /// <summary>
        ///   postal address
        /// </summary>
        [Description("postal address")]
        [Display(Name = "PST")]
        PST,

        /// <summary>
        ///   public
        /// </summary>
        [Description("public")]
        [Display(Name = "PUB")]
        PUB,

        /// <summary>
        ///   temporary
        /// </summary>
        [Description("temporary")]
        [Display(Name = "TMP")]
        TMP,

        /// <summary>
        ///   work place
        /// </summary>
        [Description("work place")]
        [Display(Name = "WP")]
        WP
        // ReSharper restore InconsistentNaming

    }
}