﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;

using Soaf;
using Soaf.Collections;
using System.Xml.Serialization;
using System.IO;
using Soaf.ComponentModel;
using System.Xml;
using System.Text;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    /// Produces CDA messages that contain transition of care documents.
    /// </summary>
    /// <remarks>
    /// Any static string values that are hardcoded come from Michelle's spec in TFS.
    /// The specifications are named VisitSummSpec.xml and TocSpec.xml
    /// </remarks>
    public class TransitionOfCareMessageProducer
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly CdaMessageProducerUtilities _cdaUtilities;
        private readonly CdaModelMapper _cdaModelMapper;

        public TransitionOfCareMessageProducer(IPracticeRepository practiceRepository, CdaMessageProducerUtilities cdaUtilities, CdaModelMapper cdaModelMapper)
        {
            _practiceRepository = practiceRepository;
            _cdaUtilities = cdaUtilities;
            _cdaModelMapper = cdaModelMapper;
            DocumentEffectiveTime = DateTime.Now.ToClientTime();
        }

        /// <summary>
        /// Produces cda messages containing a transition of care document using the given patient ids.
        /// </summary>
        public IMessage[] ProduceMessages(List<int> patientIds)
        {
            var practiceName = Application.UserContext.Current.UserDetails.BillingOrganizationName ?? _practiceRepository.BillingOrganizations.FirstOrDefault(b => b.IsMain).IfNotNull(b => b.Name);
            
            var result = new List<IMessage>();
            var allPatients = _practiceRepository.Patients
                .Where(p => patientIds.Contains(p.Id))
                .ToList();
            var allEncounters = _practiceRepository.Encounters
                .Where(e => patientIds.Contains(e.PatientId))
                .ToArray();

            //_cdaUtilities.LoadForCdaMessageProduction(allEncounters);
            //_cdaUtilities.LoadForCdaMessageProduction(allPatients);

            foreach (var patientId in patientIds)
            {
                //var document = CdaMessageProducerUtilities.AddClinicalDocument(DocumentEffectiveTime, practiceName, DocumentId);
                //document.SerializeToString();
                ClinicalDocument.ClinicalDocument document = new ClinicalDocument.ClinicalDocument(patientId.ToString(), Application.UserContext.Current.UserDetails.Id.ToString());
                //var patient = allPatients.First(p => p.Id == patientId);
                //var encounters = allEncounters.Where(e => e.PatientId == patientId).ToArray();
                //CdaMessageProducerUtilities.InitializeCdaModelMapper(_cdaModelMapper, patient, encounters);

                //var patientRole = _cdaUtilities.AddNewPatientRole(patient).SetProviderOrganization(CdaMessageProducerUtilities.AddNewProviderOrganization(patient.BillingOrganization));
                //document.AddRecordTarget().SetPatientRole(patientRole);
                //document.AddAuthor(patient.BillingOrganization, DocumentEffectiveTime);
                //document.AddCustodian(patient.BillingOrganization);
                //CdaMessageProducerUtilities.AddDocumentationOf(document, encounters.Where(e => e.EncounterStatus == EncounterStatus.Discharged));

                //var structuredBody = document.AddComponent().AddStructuredBody();

                //structuredBody.AddComponent(_cdaUtilities.AddNewAllergiesComponent(patient.PatientAllergens))
                //              .AddComponent(AddNewFunctionalAndCognitiveStatusComponent(patient))
                //              .AddComponent(_cdaUtilities.AddNewLabResultsComponent(patient.PatientLaboratoryTestResults))
                //              .AddComponent(_cdaUtilities.AddNewMedicationsComponent(patient.PatientMedications))
                //              .AddComponent(AddNewPlanOfCareComponent(encounters))
                //              .AddComponent(_cdaUtilities.AddNewProblemsComponent(patient.PatientDiagnoses))
                //              .AddComponent(_cdaUtilities.AddNewProceduresComponent(patient))
                //              .AddComponent(AddNewReasonForReferralComponent(encounters.OrderByDescending(e => e.StartDateTime).FirstOrDefault()))
                //              .AddComponent(_cdaUtilities.AddNewSocialHistoryComponent(patient.PatientSmokingStatus))
                //              .AddComponent(_cdaUtilities.AddNewVitalSignsComponent(encounters))
                //              .AddComponent(_cdaUtilities.AddNewImmunizationsComponent(patient.PatientVaccinations));

                //// TODO Uncomment once dbo Immunization tables and PatientImmunization view are up to date
                //// After uncommenting, please add PatientImmunization data to the unit tests, inspect the output, and compare to
                //// Michelle's specs in TFS.  These specs are named VisitSummSpec.xml and TocSpec.xml.

                //document.EnsureDocumentTableBodiesAreValid();
                XmlSerializer xmlSerializer = new XmlSerializer(document.GetType());
                using (var sw = new CustomEncodingStringWriter(Encoding.UTF8))
                using (var writer = XmlWriter.Create(sw, new XmlWriterSettings { Indent = true, OmitXmlDeclaration = false, Encoding = Encoding.UTF8 }))
                {
                    writer.WriteProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"CDA.xsl\"");
                    var serializer = Serialization.CreateCachedXmlSerializerWithRoot(typeof(ClinicalDocument.ClinicalDocument), "ClinicalDocument", "urn:hl7-org:v3");
                    serializer.Serialize(writer, document);

                    var cdaMessage = new CdaMessage(Guid.NewGuid()) { PatientId = patientId.ToString() };
                    cdaMessage.Load(sw.ToString());
                    result.Add(cdaMessage);
                }

            } 

            return result.ToArray();
        }

        /// <summary>
        /// Produces an IMessage containing a transition of care document using the given patient ids.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IMessage ProduceMessage(int patientId)
        {
            return ProduceMessages(new List<int> { patientId }).Single();
        }

        #region Components used exclusively in the Transition of Care CDA document

        #region Functional/Cognitive Status

        public POCD_MT000040Component3 AddNewFunctionalAndCognitiveStatusComponent(Patient source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.14");
            section.AddCode().SetCode("47420-5").SetCodeSystem(CodeSystem.Loinc);
            section.AddTitle("Functional Status");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Type");
            tr.AddTh("Date");
            tr.AddTh("Status");

            var body = table.AddTbody();

            #endregion

            InitializeFunctionalAndCognitiveStatusComponentTableRows(body, source);
            InitializeFunctionalAndCognitiveStatusComponentEntries(section, source);

            return component;
        }

        private void InitializeFunctionalAndCognitiveStatusComponentEntries(POCD_MT000040Section section, Patient source)
        {
            var functionalAssessments = source.PatientFunctionalStatusAssessments.Where(i => !i.IsDeactivated).ToArray();
            var cognitiveAssessments = source.PatientCognitiveStatusAssessments.Where(i => !i.IsDeactivated).ToArray();
            var clinicalConditionMappings = _cdaModelMapper.GetSnomedModelMap(functionalAssessments.Select(i => i.ClinicalCondition)
                                                                                        .Concat(cognitiveAssessments.Select(j => j.ClinicalCondition))
                                                                                        .Distinct());
            var count = 0;

            foreach (var functionalAssessment in functionalAssessments.OrderByDescending(i => i.EnteredDateTime))
            {
                var snomedModel = clinicalConditionMappings.GetValue(functionalAssessment.ClinicalCondition);
            

                var observation = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV)
                    .AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);

                observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.68");
                observation.AddId().SetRootAndExtension(functionalAssessment);
                observation.AddCode<CD>().SetCode("404684003").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Finding of Functional Performance and activity");
                observation.AddText().AddReference().SetValue("#functional_status_{0}".FormatWith(++count));
                observation.AddStatusCode().SetCode("completed");
                observation.AddEffectiveTime().AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(functionalAssessment.EnteredDateTime));
                if (snomedModel == null)
                {
                    CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for clinical condition {0} (FunctionalAssessment Id {1}).".FormatWith(functionalAssessment.ClinicalCondition.Name, functionalAssessment.Id));
                    observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(functionalAssessment.ClinicalCondition.Name);
                }
                else
                {
                    observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                }
            }

            count = 0;

            foreach (var cognitiveAssessment in cognitiveAssessments.OrderByDescending(i => i.EnteredDateTime))
            {
                var snomedModel = clinicalConditionMappings.GetValue(cognitiveAssessment.ClinicalCondition);

                var observation = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV).AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.73");
                observation.AddId().SetRootAndExtension(cognitiveAssessment);
                observation.AddCode<CD>().SetCode("373930000").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Cognitive function finding");
                observation.AddText().AddReference().SetValue("#cognitive_status_{0}".FormatWith(++count));
                observation.AddStatusCode().SetCode("completed");
                observation.AddEffectiveTime().AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(cognitiveAssessment.EnteredDateTime));
                if (snomedModel == null)
                {
                    CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for clinical condition {0} (CognitiveAssessment Id {1}).".FormatWith(cognitiveAssessment.ClinicalCondition.Name, cognitiveAssessment.Id));
                    observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(cognitiveAssessment.ClinicalCondition.Name);
                }
                else
                {
                    observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                }
            }
        }

        private void InitializeFunctionalAndCognitiveStatusComponentTableRows(StrucDocTbody body, Patient source)
        {
            var functionalAssessments = source.PatientFunctionalStatusAssessments.Where(i => !i.IsDeactivated).ToArray();
            var cognitiveAssessments = source.PatientCognitiveStatusAssessments.Where(i => !i.IsDeactivated).ToArray();
            var clinicalConditionMappings = _cdaModelMapper.GetSnomedModelMap(functionalAssessments.Select(i => i.ClinicalCondition)
                                                                                        .Concat(cognitiveAssessments.Select(j => j.ClinicalCondition))
                                                                                        .Distinct());

            var count = 0;
            foreach (var functionalAssessment in functionalAssessments.OrderByDescending(i => i.EnteredDateTime))
            {
                var snomedModel = clinicalConditionMappings.GetValue(functionalAssessment.ClinicalCondition);

                var tr = body.AddTr().SetId("functional_status_{0}".FormatWith(++count));
                if (snomedModel == null)
                {
                    CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for clinical condition {0} (FunctionalAssessment Id {1}).".FormatWith(functionalAssessment.ClinicalCondition.Name, functionalAssessment.Id));
                    tr.AddTd().SetText("(Functional Status) {0}, {1}: {2}".FormatWith(functionalAssessment.ClinicalCondition.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                }
                else
                {
                    tr.AddTd().SetText("(Functional Status) {0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                }
                tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(functionalAssessment.EnteredDateTime));
                tr.AddTd().SetText("active");
            }

            count = 0;
            foreach (var cognitiveAssessment in cognitiveAssessments.OrderByDescending(i => i.EnteredDateTime))
            {
                var snomedModel = clinicalConditionMappings.GetValue(cognitiveAssessment.ClinicalCondition);

                var tr = body.AddTr().SetId("cognitive_status_{0}".FormatWith(++count));
                if (snomedModel == null)
                {
                    CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for clinical condition {0} (CognitiveAssessment Id {1}).".FormatWith(cognitiveAssessment.ClinicalCondition.Name, cognitiveAssessment.Id));
                    tr.AddTd().SetText("(Cognitive Status) {0}, {1}: {2}".FormatWith(cognitiveAssessment.ClinicalCondition.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                }
                else
                {
                    tr.AddTd().SetText("(Cognitive Status) {0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                }
                tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(cognitiveAssessment.EnteredDateTime));
                tr.AddTd().SetText("active");
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();                
                tr.AddTd().SetText("None");                                     
                tr.AddTd().SetEmptyText();
                tr.AddTd().SetEmptyText();
            }
        }

        #endregion

        #region Plan Of Care

        public POCD_MT000040Component3 AddNewPlanOfCareComponent(IEnumerable<Encounter> source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.10");
            section.AddCode().SetCode("18776-5").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Treatment Plan");
            section.AddTitle("Plan Of Care");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Name");
            tr.AddTh("Type");
            tr.AddTh("Date");

            var body = table.AddTbody();

            #endregion

            var encounters = source.ToArray();

            InitializePlanOfCareComponentTableRows(body, encounters);
            InitializePlanOfCareComponentEntries(section, encounters);

            return component;
        }

        private void InitializePlanOfCareComponentEntries(POCD_MT000040Section section, IEnumerable<Encounter> source)
        {
            foreach (var encounter in source.OrderByDescending(i => i.StartDateTime))
            {
                var treatmentGoalAndInstructions = encounter.EncounterTreatmentGoalAndInstructions.Select(i => i.TreatmentGoalAndInstruction).ToArray();
                var treatmentGoalMappings = _cdaModelMapper.GetSnomedModelMap(treatmentGoalAndInstructions.Select(i => i.TreatmentGoal));

                foreach (var treatmentGoalAndInstruction in treatmentGoalAndInstructions)
                {
                    var snomedModel = treatmentGoalMappings.GetValue(treatmentGoalAndInstruction.TreatmentGoal);

                    var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.GOL);
                    observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.44");
                    observation.AddId().SetRootAndExtension(treatmentGoalAndInstruction, "G");
                    if (snomedModel == null)
                    {
                        CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for treatment goal {0} (TreatmentGoalAndInstruction Id {1}).".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, treatmentGoalAndInstruction.Id));
                        observation.AddCode<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(treatmentGoalAndInstruction.TreatmentGoal.DisplayName);
                    }
                    else
                    {
                        observation.AddCode<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                    }
                    observation.AddStatusCode().SetCode("new");
                    observation.AddEffectiveTime().AddCenter().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));

                    var act = section.AddEntry().AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.INT);
                    act.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.20");
                    act.AddId().SetRootAndExtension(treatmentGoalAndInstruction, "I");
                    
                    // Instructions will always be coded as static code/system "Patient Education"
                    act.AddCode<CD>().SetCode("311401005").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Patient Education");
                    act.AddText(treatmentGoalAndInstruction.Value);
                    act.AddStatusCode().SetCode("completed");
                }
            }
        }

        private void InitializePlanOfCareComponentTableRows(StrucDocTbody body, IEnumerable<Encounter> source)
        {
            var count = 0;
            foreach (var encounter in source.OrderByDescending(i => i.StartDateTime))
            {
                var treatmentGoalAndInstructions = encounter.EncounterTreatmentGoalAndInstructions.Select(i => i.TreatmentGoalAndInstruction).ToArray();
                var treatmentGoalMappings = _cdaModelMapper.GetSnomedModelMap(treatmentGoalAndInstructions.Select(i => i.TreatmentGoal));

                foreach (var treatmentGoalAndInstruction in treatmentGoalAndInstructions)
                {
                    var snomedModel = treatmentGoalMappings.GetValue(treatmentGoalAndInstruction.TreatmentGoal);
                    var tr = body.AddTr().SetId("Goal_{0}".FormatWith(++count));
                    if (snomedModel == null)
                    {
                        CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for treatment goal {0} (TreatmentGoalAndInstruction Id {1}).".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, treatmentGoalAndInstruction.Id));
                        tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                    }
                    else
                    {
                        tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                    }
                    tr.AddTd().SetText("Goal");
                    tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(encounter.StartDateTime));

                    tr = body.AddTr().SetId("Instructions_{0}".FormatWith(count));
                    tr.AddTd().SetText(treatmentGoalAndInstruction.Value);
                    tr.AddTd().SetText("Instructions");
                    tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(encounter.StartDateTime));
                }
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                tr.AddTd().SetText("None");
                tr.AddTd().SetEmptyText();

                tr = body.AddTr();
                tr.AddTd().SetEmptyText();
                tr.AddTd().SetEmptyText();
            }
        }

        #endregion

        #region Reason For Referral

        public POCD_MT000040Component3 AddNewReasonForReferralComponent(Encounter source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot("1.3.6.1.4.1.19376.1.5.3.1.3.1");
            section.AddCode().SetCode("42349-1").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("CHIEF COMPLAINT AND REASON FOR VISIT");
            section.AddTitle("Reason For Referral");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            table.AddThead().AddTr().AddTh("Reason For Referral");
            var body = table.AddTbody();

            #endregion

            InitializeReasonForReferralComponentTableRows(body, source);

            return component;
        }

        private static void InitializeReasonForReferralComponentTableRows(StrucDocTbody body, Encounter source)
        {
            foreach (var transitionOfCareReason in source.EncounterTransitionOfCareOrders.Select(i => i.TransitionOfCareReason.IfNotNull(r => r.Value)))
            {
                body.AddTr().AddTd().SetText(transitionOfCareReason);
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                tr.AddTd().SetText("None");               
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Gets or sets the current time.  This is internal for unit testing purposes.
        /// </summary>
        internal DateTime DocumentEffectiveTime { get; set; }

        /// <summary>
        /// Gets or sets the document id.  This is internal for unit testing purposes.
        /// </summary>
        internal Guid? DocumentId { get; set; }
    }
}
