﻿using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;

namespace IO.Practiceware.Integration.Cda
{
    public static class ConstantValueSetMaps
    {
        public static Dictionary<MaritalStatus, string> MaritalStatusToHL7Map = new Dictionary<string, MaritalStatus>
                                                                           {
                                                                               {"A", MaritalStatus.Annulled}, /* Annulled. Marriage contract has been declared null and to not have existed. */
 
                                                                               {"D", MaritalStatus.Divorced}, /* Divorced. Marriage contract has been declared dissolved and inactive. */
 
                                                                               {"I", MaritalStatus.Interlocutory}, /* Interlocutory. Subject to an Interlocutory Decree. */
 
                                                                               {"L", MaritalStatus.LegallySeparated}, /* Legally Separated. */
      
                                                                               {"M", MaritalStatus.Married}, /* Married. A current marriage contract is active. */
 
                                                                               {"P", MaritalStatus.Polygamous}, /* Polygamous. More than 1 current spouse. */
 
                                                                               {"N", MaritalStatus.NeverMarried}, /* Never Married. No marriage contract has ever been entered. */

                                                                               {"S", MaritalStatus.Single}, /* Single. No marriage contract has ever been entered. */
 
                                                                               {"T", MaritalStatus.DomesticPartner}, /* Domestic partner Person declares that a domestic partner relationship exists. */
 
                                                                               {"W", MaritalStatus.Widowed}, /* Widowed. The spouse has died */
                                                                           }.ToDictionary(i => i.Value, i => i.Key);

        public static Dictionary<ClinicalConditionStatus, string> ClinicalConditionStatusToSnomedMap = new Dictionary<ClinicalConditionStatus, string>
                                                                                                        {
                                                                                                           {ClinicalConditionStatus.Active, "55561003"},
                                                                                                           {ClinicalConditionStatus.Inactive, "73425007"},
                                                                                                           {ClinicalConditionStatus.Resolved, "413322009"}
                                                                                                       };

        public static Dictionary<PatientPhoneNumberType, CdaTelecomPhoneNumberCode> PatientPhoneTypeToCdaTelecomUsePhoneType = new Dictionary<PatientPhoneNumberType, CdaTelecomPhoneNumberCode>
                                                                                                                          {
                                                                                                                              {PatientPhoneNumberType.Business, CdaTelecomPhoneNumberCode.WP},
                                                                                                                              {PatientPhoneNumberType.Cell, CdaTelecomPhoneNumberCode.MC},
                                                                                                                              {PatientPhoneNumberType.Home, CdaTelecomPhoneNumberCode.HP},
                                                                                                                              //{PatientPhoneNumberType.Home, CdaTelecomAddressCode.HV}, /* Vacation Home - we don't currently support */                                                                                                                              
                                                                                                                          };

        public static Dictionary<BillingOrganizationPhoneNumberTypeId, CdaTelecomPhoneNumberCode> BillOrganizationPhoneTypeToCdaTelecomUsePhoneType = new Dictionary<BillingOrganizationPhoneNumberTypeId, CdaTelecomPhoneNumberCode>
                                                                                                                          {
                                                                                                                              {BillingOrganizationPhoneNumberTypeId.Billing, CdaTelecomPhoneNumberCode.WP},
                                                                                                                              {BillingOrganizationPhoneNumberTypeId.Main, CdaTelecomPhoneNumberCode.WP},                                                                                                                             
                                                                                                                          };


        public static Dictionary<PatientAddressTypeId, CdaPostalAddressCode> PatientAddressTypeToCdaPostalAddressCode = new Dictionary<PatientAddressTypeId, CdaPostalAddressCode>
                                                                                                                          {
                                                                                                                              {PatientAddressTypeId.Business, CdaPostalAddressCode.WP},                                                                                                                              
                                                                                                                              {PatientAddressTypeId.Home, CdaPostalAddressCode.H},                                                                                                                            
                                                                                                                          };
    }
}