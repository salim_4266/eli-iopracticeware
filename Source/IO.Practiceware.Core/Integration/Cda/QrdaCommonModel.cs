﻿using System;
using IO.Practiceware.Model;

namespace IO.Practiceware.Integration.Cda
{
    public class QrdaCommonModel
    {
        public QrdaCommonModel()
        {
            UniqueIdentifier = Guid.NewGuid();
            GenerationDateTime = DateTime.UtcNow;
        }

        public User User { get; set; }

        public BillingOrganization MainBillingOrganization { get; set; }

        public DateTime MeasurementPeriodStartDate { get; set; }

        public DateTime MeasurementPeriodEndDate { get; set; }

        public DateTime GenerationDateTime { get; set; }

        public string GenerationDateTimeWithTimeZoneOffset
        {
            get
            {
                return String.Format("{0:yyyyMMddHHmmss}{0:zz}00", GenerationDateTime);
            }
        }

        public Guid UniqueIdentifier { get; set; }
    }
}