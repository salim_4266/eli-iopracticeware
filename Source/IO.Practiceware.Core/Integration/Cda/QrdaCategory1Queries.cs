﻿using System;
using System.Linq;
using System.Linq.Expressions;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Linq.Expressions;

namespace IO.Practiceware.Integration.Cda
{
    public class QrdaCategory1Queries
    {
        private readonly CdaModelMapper _cdaModelMapper;

        private readonly Lazy<ClinicalCondition[]> _clinicalConditions;
        private readonly Lazy<EncounterService[]> _encounterServices;
        private readonly DateTime _measurementPeriodEndDate;
       private readonly DateTime _measurementPeriodStartDate;
        private readonly IPracticeRepository _practiceRepository;
        private readonly Lazy<ClinicalProcedure[]> _clinicalProcedures;

        public QrdaCategory1Queries(DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate, int userId, IPracticeRepository practiceRepository, CdaModelMapper cdaModelMapper)
        {
            _measurementPeriodStartDate = measurementPeriodStartDate;
            _measurementPeriodEndDate = measurementPeriodEndDate;
            UserId = userId;
            _practiceRepository = practiceRepository;
            _cdaModelMapper = cdaModelMapper;

            // TO DO: Optimize me so we don't need to load ALL this data      
            _clinicalConditions = Lazy.For(() => _practiceRepository.ClinicalConditions.ToArray());
            _encounterServices = Lazy.For(() => _practiceRepository.EncounterServices.ToArray());
            _clinicalProcedures = Lazy.For(() => _practiceRepository.ClinicalProcedures.ToArray());
        }

        public int UserId { get; set; }

        public Expression<Func<Encounter, bool>> IsEncounterForUser
        {
            get { return e => e.Appointments.OfType<UserAppointment>().Any(a => a.UserId == UserId); }
        }

        public Expression<Func<Encounter, bool>> IsDischargedEncounterInMeasurementPeriodForUser
        {
            get
            {
                //Get discharged encounters that occurred during the Measurement Period.
                return e => e.EncounterStatus == EncounterStatus.Discharged && e.Appointments.OfType<UserAppointment>().Any(a => a.UserId == UserId)
                            && e.StartDateTime >= _measurementPeriodStartDate && e.EndDateTime <= _measurementPeriodEndDate;
            }
        }

        /// <summary>
        /// Patients whose age is between x and y on the Measurement Period start date.
        /// </summary>
        public Expression<Func<Patient, int, int, bool>> HasAgeRangeAtMeasurementPeriodStartDateInclusiveMaxAge
        {
            get
            {
                return (patient, minAge, maxAge) => patient.DateOfBirth <= CommonQueries.AddYears(_measurementPeriodStartDate, minAge * -1)
                                                    && patient.DateOfBirth >= CommonQueries.AddYears(_measurementPeriodStartDate, maxAge * -1);
            }
        }

        /// <summary>
        /// Get patients whose age is between x and y before the Measurement Period start date.
        /// </summary>
        public Expression<Func<Patient, int, int, bool>> HasAgeRangeAtMeasurementPeriodStartDateExclusiveMaxAge
        {
            get
            {
                return (patient, minAge, maxAge) => patient.DateOfBirth <= CommonQueries.AddYears(_measurementPeriodStartDate, minAge * -1)
                                                    && patient.DateOfBirth > CommonQueries.AddYears(_measurementPeriodStartDate, maxAge * -1);
            }
        }

        /// <summary>
        /// Patients with a certain minimum age on the Measurement Period's start date.
        /// </summary>
        public Expression<Func<Patient, int, bool>> IsMinimumAgeAtMeasurementPeriodStartDate
        {
            get { return (patient, age) => patient.DateOfBirth <= CommonQueries.AddYears(_measurementPeriodStartDate, age * -1); }
        }

        /// <summary>
        /// Patient encounters that were discharged during the Measurement Period and map to CQM Encounter Performed Value Set(s).  Mapping is done on the Encounter.Invoice.BillingService.
        /// </summary>
        /// <param name="valueSets">The value sets.</param>
        /// <returns></returns>
        public Expression<Func<Encounter, bool>> HasValueSetsDuringMeasurementPeriod(params QualityDataModelValueSet[] valueSets)
        {
            return e => IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && e.Invoices.Any(i => i.BillingServices.Any(bs => IsEncounterServicesMappedToValueSets(valueSets).Invoke(bs.EncounterService)));
        }

        /// <summary>
        ///Patient encounters that map to CQM Encounter Performed Value Set(s).  Mapping is done on the Encounter.Invoice.BillingService. 
        /// </summary>
        public Expression<Func<Encounter, bool>> HasEncounterServicesMappedToValueSets(params QualityDataModelValueSet[] valueSets)
        {
            return e => e.Invoices.Any(i => i.BillingServices.Any(bs => IsEncounterServicesMappedToValueSets(valueSets).Invoke(bs.EncounterService)));
        }

        /// <summary>
        /// Encounter services that are mapped within the above CQM value sets
        /// </summary>
        public Expression<Func<EncounterService, bool>> IsEncounterServicesMappedToValueSets(params QualityDataModelValueSet[] valueSets)
        {
            int[] encounterServiceIds = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_encounterServices.Value, valueSets).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return es => encounterServiceIds.Contains(es.Id);
        }

        /// <summary>
        /// PatientExamPerformeds that are mapped within the above CQM value sets
        /// </summary>
        public Expression<Func<Encounter, bool>> HasExamPerformedMappedToValueSets(params QualityDataModelValueSet[] valueSets)
        {
            int[] ids = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalProcedures.Value, valueSets).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return e => e.PatientExamPerformeds.Any(pep => ids.Contains(pep.ClinicalProcedureId));
        }

        /// <summary>
        /// Active diagnoses starting on or before the Measurement Period End Date and not ending before Measurement Period Start Date and are mapped to CQM value set(s).  
        /// Used in Diabetes Eye Exam, BMI exclusion, Tobacco Screen exception, High Blood Pressure exclusions.
        /// </summary>
        /// <param name="valueSet"></param>
        /// <returns></returns>
        public Expression<Func<PatientDiagnosis, bool>> IsDiagnosisInMeasurementPeriodMappedToValueSets(QualityDataModelValueSet valueSet)
        {
            // TO DO: Optimize me
            int[] clinicalConditionIds = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalConditions.Value, valueSet).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return pd => clinicalConditionIds.Contains(pd.ClinicalConditionId.Value)
                         && pd.PatientDiagnosisDetails.Any(pdd => pdd.Id == pd.PatientDiagnosisDetails.OrderByDescending(x => x.EnteredDateTime).Select(i => i.Id).FirstOrDefault()
                                                                  && pdd.StartDateTime <= _measurementPeriodEndDate && (pdd.EndDateTime == null || pdd.EndDateTime >= _measurementPeriodStartDate));
        }

        /// <summary>
        /// Active diagnoses that start on or before a certain date and are mapped to CQM Diagnosis Active Value Set(s).
        /// </summary>
        /// <param name="valueSet"></param>
        /// <returns></returns>
        public Expression<Func<Patient, DateTime, bool>> HasDiagnosisBefore(QualityDataModelValueSet valueSet)
        {
            // TO DO: Optimize me
            int[] clinicalConditionIds = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalConditions.Value, valueSet).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return (patient, dateTime) => patient.PatientDiagnoses.Any(d => d.ClinicalConditionId.HasValue && clinicalConditionIds.Contains(d.ClinicalConditionId.Value) && d.PatientDiagnosisDetails.Any(pdd => pdd.StartDateTime <= dateTime));
        }

        /// <summary>
        /// Active diagnoses that start, and don't end, before the Measurement Period Start Date.
        /// </summary>
        public Expression<Func<PatientDiagnosisDetail, bool>> IsDiagnosisStillActiveDuringMeasurementPeriod
        {
            get
            {
                return pdd => pdd.StartDateTime < _measurementPeriodStartDate && (pdd.EndDateTime == null || pdd.EndDateTime > _measurementPeriodStartDate);
            }
        }

        /// <summary>
        /// Active diagnoses that start after the Measurement Period Start Date but before the Measurement Start Date + x months.
        /// </summary>
        /// <param name="months"></param>
        /// <returns></returns>
        public Expression<Func<PatientDiagnosisDetail, bool>> DiagnosisStartsWithinMonthsOfMeasurementPeriod(int months)
        {
            return pdd => pdd.StartDateTime > _measurementPeriodStartDate && pdd.StartDateTime <= _measurementPeriodStartDate.AddMonths(months);
        }

        /// <summary>
        /// Diagnoses that are mapped to CQM value set(s).
        /// </summary>
        /// <param name="valueSets"></param>
        /// <returns></returns>
        public Expression<Func<ClinicalCondition, bool>> IsClinicalConditionMappedToValueSets(params QualityDataModelValueSet[] valueSets)
        {
            int[] clinicalConditionIds = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalConditions.Value, valueSets).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return cc => clinicalConditionIds.Contains(cc.Id);
        }

        /// <summary>
        /// Clinical procedures that are mapped to CQM value set(s).
        /// </summary>
        /// <param name="valueSets"></param>
        /// <returns></returns>
        public Expression<Func<ClinicalProcedure, bool>> IsClinicalProcedureMappedToValueSets(params QualityDataModelValueSet[] valueSets)
        {
            int[] clinicalProcedureIds = valueSets.SelectMany(valueSet => _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalProcedures.Value, valueSet).Where(p => p.Value != null).Select(p => p.Key.Id)).ToArray();

            return cp => clinicalProcedureIds.Contains(cp.Id);
        }

        /// <summary>
        /// Clinical procedures that are mapped to CQM value set(s) and were performed between the Measurement Period Start and End Dates.
        /// </summary>
        /// <param name="valueSets"></param>
        /// <returns></returns>
        public Expression<Func<PatientProcedurePerformed, bool>> IsPatientProcedurePerformedMappedToValueSetsDuringMeasurementPeriod(params QualityDataModelValueSet[] valueSets)
        {
            int[] clinicalProcedureIds = _cdaModelMapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModelMap(_clinicalProcedures.Value, valueSets).Where(p => p.Value != null).Select(p => p.Key.Id).ToArray();

            return ppp => clinicalProcedureIds.Contains(ppp.ClinicalProcedureId) && ppp.PerformedDateTime >= _measurementPeriodStartDate && ppp.PerformedDateTime <= _measurementPeriodEndDate;
        }

        /// <summary>
        /// Clinical procedures that were performed x days before the end of the Measurement Period End Date. 
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public Expression<Func<PatientProcedurePerformed, bool>> IsPatientProcedurePerformedBeforeMeasurementPeriodEndDateInDays(int days)
        {
            return ppp => ppp.PerformedDateTime < CommonQueries.AddDays(_measurementPeriodEndDate, days);
        }

    }
}