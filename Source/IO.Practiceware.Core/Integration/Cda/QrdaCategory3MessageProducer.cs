﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using IO.Practiceware.Configuration;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Linq;
using RestSharp;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    ///     Produces QRDA Category 3 message for specified measures from bunch of QRDA Cat 1 messages
    /// </summary>
    public class QrdaCategory3MessageProducer
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly IDocumentGenerationService _documentGenerationService;
        private readonly Func<QrdaCategory3Model> _createQrdaCategory3Model;

        private readonly ElectronicClinicalQualityMeasuresConfiguration _configuration;

        public QrdaCategory3MessageProducer(IPracticeRepository practiceRepository,
            IDocumentGenerationService documentGenerationService,
            Func<QrdaCategory3Model> createQrdaCategory3Model)
        {
            _practiceRepository = practiceRepository;
            _documentGenerationService = documentGenerationService;
            _createQrdaCategory3Model = createQrdaCategory3Model;
            _configuration = ConfigurationManager.GetSectionWithRetry<ElectronicClinicalQualityMeasuresConfiguration>();
        }

        public IMessage ProduceMessage(int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate, ICollection<ElectronicClinicalQualityMeasure> qualityMeasures, ICollection<IMessage> qrdaCategory1Messages)
        {
            // Create archive of Category 1 messages
            var cat1ZipPath = FileSystem.GetTempPathName() + ".zip";
            using (var zip = new ZipOutputStream(new FileStream(cat1ZipPath, FileMode.Create)))
            {
                foreach (var category1Message in qrdaCategory1Messages)
                {
                    // Add entry
                    var zipEntry = new ZipEntry(category1Message.Id.ToString());
                    zip.PutNextEntry(zipEntry);

                    // Add content
                    using (var source = new MemoryStream(Encoding.UTF8.GetBytes(category1Message.ToString())))
                    {
                        source.CopyTo(zip);
                    }
                }
            }

            IRestResponse response;
            try
            {
                // Initialize client
                var client = new RestClient(_configuration.Category3ReportingService.ServiceUri);
                client.Authenticator = new HttpBasicAuthenticator(_configuration.Category3ReportingService.Login,
                    _configuration.Category3ReportingService.Password);
                

                // Prepare request
                var request = new RestRequest("api/reports/qrda_cat3_inplace", Method.POST);
                foreach (var measureId in qualityMeasures.Select(qm => qm.VersionSpecificId))
                {
                    request.AddParameter("measure_ids[]", measureId);
                }
                request.AddParameter("start_date", measurementPeriodStartDate.ToUnixEpoch());
                request.AddParameter("effective_date", measurementPeriodEndDate.ToUnixEpoch());
                request.AddFile("cat1_zip", cat1ZipPath);

                // Execute request and retrieve response
                response = client.Execute(request);

                // Verify server successfuly processed the request
                if (response.ResponseStatus != ResponseStatus.Completed
                    || response.StatusCode != HttpStatusCode.OK)
                {
                    throw new InvalidOperationException("Failed to generate Qrda Summary Report. Server replied: {0}".FormatWith(response.Content), 
                        response.ErrorException);
                }
            }
            finally
            {
                FileSystem.TryDeleteFile(cat1ZipPath, true);
            }

            var fixedContent = FixQrdaSummaryReport(userId, measurementPeriodStartDate, measurementPeriodEndDate, response.Content);
            
            // Prepare result message
            var message = new CdaMessage(Guid.NewGuid());
            message.Load(fixedContent);

            return message;
        }

        /// <summary>
        /// Fixes the qrda summary report.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="measurementPeriodStartDate">The measurement period start date.</param>
        /// <param name="measurementPeriodEndDate">The measurement period end date.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        private string FixQrdaSummaryReport(int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate, string content)
        {
            // Prepare model required by the templates
            var model = _createQrdaCategory3Model();
            model.MeasurementPeriodStartDate = measurementPeriodStartDate;
            model.MeasurementPeriodEndDate = measurementPeriodEndDate;
            model.MainBillingOrganization = _practiceRepository.BillingOrganizations.First(o => o.IsMain);
            model.User = _practiceRepository
                .Users
                .Include(u => u.Providers.SelectMany(p => p.IdentifierCodes))
                .WithId(userId);

            // Render parts we will override in summary report
            var author = RenderTemplate("QrdaCategory3.Author.cshtml", model);
            var custodian = RenderTemplate("QrdaShared.Custodian.cshtml", model);
            var legalAuthenticator = RenderTemplate("QrdaShared.LegalAuthenticator.cshtml", model);

            // Replace parts
            var fixedContent = content;
            fixedContent = Regex.Replace(fixedContent, "<author.*?/author>", author, RegexOptions.Singleline);
            fixedContent = Regex.Replace(fixedContent, "<custodian.*?/custodian>", custodian, RegexOptions.Singleline);
            fixedContent = Regex.Replace(fixedContent, "<legalAuthenticator.*?/legalAuthenticator>", legalAuthenticator, RegexOptions.Singleline);

            // Remove documentationOf which appeared in later versions of popHealth (certified for MU2 without it)
            fixedContent = Regex.Replace(fixedContent, "<documentationOf.*?/documentationOf>", string.Empty, RegexOptions.Singleline);

            // Remove BOM (Cypress doesn't like it within file contents)
            fixedContent = fixedContent.CleanFromBom();

            return fixedContent;
        }

        /// <summary>
        /// Renders the template.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private string RenderTemplate(string templateName, object model)
        {
            TemplateDocument template = _practiceRepository
                .TemplateDocuments
                .Single(td => td.Name == templateName);

            string result = _documentGenerationService
                .CompileRazorAndRun(template.GetContent() as string, model);

            return result;
        }
    }
}