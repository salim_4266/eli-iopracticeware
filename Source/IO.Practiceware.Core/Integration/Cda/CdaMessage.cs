﻿using System;
using System.Diagnostics;
using IO.Practiceware.Integration.Messaging;

namespace IO.Practiceware.Integration.Cda
{
    internal class CdaMessage : IMessage
    {
        private string _content;
        private readonly string _createdBy;

        public CdaMessage(Guid id)
        {
            Id = id;
            Type declaringType = new StackTrace().GetFrame(1).GetMethod().DeclaringType;
            _createdBy = declaringType != null ? declaringType.Name : Constants.LocalApplicationId;
        }

        public void Load(string value)
        {
            _content = value;
        }

        public IMessage GetAcknowledgement()
        {
            return null;
        }

        public override string ToString()
        {
            return _content;
        }

        public string Source
        {
            get { return Constants.LocalApplicationId; }
        }

        public bool ShouldProcess
        {
            get { return true; }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
        }

        public object Id { get; set; }
        public object AcknowledgementForId { get; private set; }
        public bool RequestsAcknowledgement { get; private set; }
        public string Destination { get; set; }
        public string MessageType { get; set; }

        public string PatientId { get; set; }
        public string EncounterId { get; set; }
    }
}