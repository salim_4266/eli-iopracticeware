using System.Collections.Generic;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Linq;

[assembly: Component(typeof(CdaMessageProducer.ProduceMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.Cda
{
    public class CdaMessageProducer
    {
        public class ProduceMessageSubscriber
        {
            public ProduceMessageSubscriber(IMessenger messenger, Func<CdaMessageProducer> producer)
            {
                messenger.Subscribe((IMessenger s, object t, ProduceMessageRequest m) => producer().ProduceMessage(m),
                                    (s, t, m) => IsCda((ExternalSystemMessageTypeId)m.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId));
            }
        }

        private readonly Func<EncounterClinicalSummaryMessageProducer> _encounterClinicalSummaryMessageProducer;
        private readonly Func<TransitionOfCareMessageProducer> _transitionOfCareMessageProducer;

        public CdaMessageProducer(
            Func<EncounterClinicalSummaryMessageProducer> encounterClinicalSummaryMessageProducer, 
            Func<TransitionOfCareMessageProducer> transitionOfCareMessageProducer)
        {
            _encounterClinicalSummaryMessageProducer = encounterClinicalSummaryMessageProducer;
            _transitionOfCareMessageProducer = transitionOfCareMessageProducer;
        }

        private static bool IsCda(ExternalSystemMessageTypeId messageType)
        {
            return new[] { ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound, ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound }
                .Contains(messageType);
        }

        public void ProduceMessage(ProduceMessageRequest request)
        {
            ProduceMessages(new List<ProduceMessageRequest> {request});
        }

        public void ProduceMessages(List<ProduceMessageRequest> produceRequests)
        {
            foreach (var requestsGroup in produceRequests.GroupBy(pr => pr.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId))
            {
                var messageType = (ExternalSystemMessageTypeId)requestsGroup.Key;
                if (messageType != ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound
                    && messageType != ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound)
                {
                    throw new Exception("{0} is not supported".FormatWith(messageType));
                }

                var entityType = messageType == ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound
                    ? PracticeRepositoryEntityId.Patient
                    : PracticeRepositoryEntityId.Appointment;

                var requestToEntityIds = requestsGroup
                    .ToDictionary(r => r, r => r.ExternalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities
                        .Where(e => e.PracticeRepositoryEntityId == (int)entityType)
                        .Select(e => e.PracticeRepositoryEntityKey).First());

                // Generate messages for new requests
                var newRequests = requestsGroup.Where(r => r.ExternalSystemMessage.Value.IsNullOrWhiteSpace()).ToList();
                if (newRequests.Any())
                {
                    var entityIds = newRequests.Select(r => int.Parse(requestToEntityIds[r])).Distinct().ToList();

                    CdaMessage[] producedMessages;
                    if (messageType == ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound)
                    {
                        CdaMessageProducerUtilities.SynchronizePatientDiagnosisDetails(entityIds, null);
                        producedMessages = _transitionOfCareMessageProducer().ProduceMessages(entityIds).OfType<CdaMessage>().ToArray();
                    }
                    else
                    {
                        CdaMessageProducerUtilities.SynchronizePatientDiagnosisDetails(null, entityIds);
                        producedMessages = _encounterClinicalSummaryMessageProducer().ProduceMessages(entityIds).OfType<CdaMessage>().ToArray();
                    }

                    // Map messages back to requests
                    foreach (var newRequest in newRequests)
                    {
                        var matchedMessage = producedMessages.FirstOrDefault(m => entityType == PracticeRepositoryEntityId.Patient 
                            ? m.PatientId == requestToEntityIds[newRequest] 
                            : m.EncounterId == requestToEntityIds[newRequest]);
                        if (matchedMessage == null) continue;

                        newRequest.ProducedMessage = matchedMessage;
                    }
                }

                // Load messages for previously generated messages
                foreach (var request in requestsGroup.Where(r => !r.ExternalSystemMessage.Value.IsNullOrWhiteSpace()))
                {
                    var message = new CdaMessage(request.ExternalSystemMessage.Id);
                    message.Load(request.ExternalSystemMessage.Value);
                    request.ProducedMessage = message;
                }

                // Set cda message properties
                foreach (var request in requestsGroup)
                {
                    var cdaMessage = request.ProducedMessage.As<CdaMessage>();
                    if (cdaMessage == null) continue;

                    cdaMessage.Id = request.ExternalSystemMessage.Id;
                    switch (entityType)
                    {
                        case PracticeRepositoryEntityId.Patient:
                            cdaMessage.PatientId = requestToEntityIds[request];
                            break;
                        case PracticeRepositoryEntityId.Appointment:
                            cdaMessage.EncounterId = requestToEntityIds[request];
                            break;
                    }

                    cdaMessage.MessageType = messageType.ToString().Split('_')[0];
                    cdaMessage.Destination = request.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystem.IfNotNull(s => s.Name, () => string.Empty);
                }
            }
        }
    }
}