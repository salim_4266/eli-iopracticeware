﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Data;
using Soaf.Data;

namespace IO.Practiceware.Integration.ClinicalDocument
{
    [XmlRoot(ElementName = "realmCode", Namespace = "urn:hl7-org:v3")]
    public class RealmCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "typeId", Namespace = "urn:hl7-org:v3")]
    public class TypeId
    {
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
    }

    [XmlRoot(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
    public class TemplateId
    {
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }

        public TemplateId()
        {

        }
        public TemplateId(string rootValue)
        {
            Root = rootValue;
        }
    }

    [XmlRoot(ElementName = "id", Namespace = "urn:hl7-org:v3")]
    public class Id
    {
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
        [XmlAttribute(AttributeName = "assigningAuthorityName")]
        public string AssigningAuthorityName { get; set; }
    }

    [XmlRoot(ElementName = "code", Namespace = "urn:hl7-org:v3")]
    public class Code
    {
        [XmlAttribute(AttributeName = "code")]
        public string _code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlElement(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
        public OriginalText OriginalText { get; set; }
        [XmlElement(ElementName = "translation", Namespace = "urn:hl7-org:v3")]
        public Translation Translation { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
    public class EffectiveTime
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlElement(ElementName = "low", Namespace = "urn:hl7-org:v3")]
        public Low Low { get; set; }
        [XmlElement(ElementName = "high", Namespace = "urn:hl7-org:v3")]
        public High High { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }

        public EffectiveTime()
        {

        }

        public EffectiveTime(string _Value)
        {
            Low = new Low() { Value = _Value };
        }
    }

    [XmlRoot(ElementName = "confidentialityCode", Namespace = "urn:hl7-org:v3")]
    public class ConfidentialityCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
    }

    [XmlRoot(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
    public class LanguageCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        public LanguageCode()
        {

        }
        public LanguageCode(string _Code)
        {
            Code = _Code;
        }

    }

    [XmlRoot(ElementName = "setId", Namespace = "urn:hl7-org:v3")]
    public class SetId
    {
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }

        public SetId()
        {

        }
        public SetId(string _extension, string _root)
        {
            Root = _root;
            Extension = _extension;
        }
    }

    [XmlRoot(ElementName = "versionNumber", Namespace = "urn:hl7-org:v3")]
    public class VersionNumber
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        public VersionNumber()
        {

        }

        public VersionNumber(string _value)
        {
            Value = _value;
        }
    }

    [XmlRoot(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
    public class Addr
    {
        [XmlElement(ElementName = "streetAddressLine", Namespace = "urn:hl7-org:v3")]
        public string StreetAddressLine { get; set; }
        [XmlElement(ElementName = "city", Namespace = "urn:hl7-org:v3")]
        public string City { get; set; }
        [XmlElement(ElementName = "state", Namespace = "urn:hl7-org:v3")]
        public string State { get; set; }
        [XmlElement(ElementName = "country", Namespace = "urn:hl7-org:v3")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlElement(ElementName = "postalCode", Namespace = "urn:hl7-org:v3")]
        public string postalCode { get; set; }

        public Addr()
        { }
        public Addr(string _cityName, string _stateName, string _countryName, string _postalcode, string _Use, string _streetAddressLine)
        {
            StreetAddressLine = _streetAddressLine;
            City = _cityName;
            State = _stateName;
            Country = _countryName;
            postalCode = _postalcode;
            Use = _Use;
        }

        public Addr(DataTable dtPatientDetail, string _Use)
        {
            if (dtPatientDetail.Rows.Count > 0)
            {
                StreetAddressLine = dtPatientDetail.Rows[0]["StreetAddressLine"].ToString();
                City = dtPatientDetail.Rows[0]["City"].ToString();
                State = dtPatientDetail.Rows[0]["State"].ToString();
                Country = dtPatientDetail.Rows[0]["Country"].ToString();
                postalCode = dtPatientDetail.Rows[0]["postalCode"].ToString();
                Use = _Use;
            }
        }

    }

    [XmlRoot(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
    public class Telecom
    {
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }

        public Telecom(string _Use, string _NullFlavor)
        {
            Use = _Use;
            NullFlavor = _NullFlavor;
        }
        public Telecom()
        {

        }
    }

    [XmlRoot(ElementName = "name", Namespace = "urn:hl7-org:v3")]
    public class Name
    {
        [XmlElement(ElementName = "given", Namespace = "urn:hl7-org:v3")]
        public string Given { get; set; }
        [XmlElement(ElementName = "family", Namespace = "urn:hl7-org:v3")]
        public string Family { get; set; }
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlElement(ElementName = "suffix", Namespace = "urn:hl7-org:v3")]
        public string Suffix { get; set; }

        public Name(string _Given, string _Suffix, string _Use)
        {
            Given = _Given;
            Suffix = _Suffix;
            Use = _Use;
        }
        public Name()
        {

        }

        public Name(DataTable dtPatient, string _Use)
        {
            if (dtPatient.Rows.Count > 0)
            {
                Given = dtPatient.Rows[0]["FirstName"].ToString();
                Suffix = dtPatient.Rows[0]["LastName"].ToString();
                Use = _Use;
            }
        }

    }

    [XmlRoot(ElementName = "administrativeGenderCode", Namespace = "urn:hl7-org:v3")]
    public class AdministrativeGenderCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }

        public AdministrativeGenderCode()
        {

        }

        public AdministrativeGenderCode(string _Code, string _DisplayName)
        {
            Code = _Code;
            DisplayName = _DisplayName;
            CodeSystem = "2.16.840.1.113883.5.1";
            CodeSystemName = "HL7 AdministrativeGender";
        }

        public AdministrativeGenderCode(DataTable dtPatient)
        {
            Code = dtPatient.Rows.Count == 0 ? "" : dtPatient.Rows[0]["GenderCode"].ToString();
            DisplayName = dtPatient.Rows.Count == 0 ? "" : dtPatient.Rows[0]["GenderDisplayName"].ToString();
            CodeSystem = "2.16.840.1.113883.5.1";
            CodeSystemName = "HL7 AdministrativeGender";
        }
    }

    [XmlRoot(ElementName = "birthTime", Namespace = "urn:hl7-org:v3")]
    public class BirthTime
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }

        public BirthTime(string _BirthTime)
        {
            Value = _BirthTime;
        }
        public BirthTime()
        {

        }
        public BirthTime(DataTable dtPatient)
        {
            if (dtPatient.Rows.Count > 0)
            {
                DateTime TempDOB = Convert.ToDateTime(dtPatient.Rows[0]["DateOfBirth"].ToString());
                Value = TempDOB.Year.ToString() + (TempDOB.Month.ToString().Length == 1 ? "0" + TempDOB.Month.ToString() : TempDOB.Month.ToString()) + (TempDOB.Day.ToString().Length == 1 ? "0" + TempDOB.Day.ToString() : TempDOB.Day.ToString());
            }
        }
    }

    [XmlRoot(ElementName = "maritalStatusCode", Namespace = "urn:hl7-org:v3")]
    public class MaritalStatusCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }

        public MaritalStatusCode()
        {

        }
        public MaritalStatusCode(string _code, string _DisplayName)
        {
            Code = _code;
            DisplayName = _DisplayName;
            CodeSystem = "";
            CodeSystemName = "";
        }
    }

    [XmlRoot(ElementName = "religiousAffiliationCode", Namespace = "urn:hl7-org:v3")]
    public class ReligiousAffiliationCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }

        public ReligiousAffiliationCode(string _NullFlavor)
        {
            NullFlavor = _NullFlavor;
            CodeSystem = "";
            CodeSystemName = "";
        }
        public ReligiousAffiliationCode()
        {

        }
    }

    [XmlRoot(ElementName = "raceCode", Namespace = "urn:hl7-org:v3")]
    public class RaceCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }

        public RaceCode(DataTable dt)
        {
            CodeSystemName = "HL7 Religious Affiliation";
            CodeSystem = "2.16.840.1.113883.6.238";
            if (dt.Rows.Count > 0)
            {
                NullFlavor = dt.Rows[0]["nullFlavour"].ToString();
                DisplayName = dt.Rows[0]["DisplayName"].ToString();
            }
            else
            {
                NullFlavor = "NI";
                DisplayName = "Declined to Specify";
            }
        }
        public RaceCode()
        {

        }
    }

    [XmlRoot(ElementName = "ethnicGroupCode", Namespace = "urn:hl7-org:v3")]
    public class EthnicGroupCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
        public EthnicGroupCode()
        { }
        public EthnicGroupCode(DataTable dt)
        {
            CodeSystemName = "Race and Ethnicity - CDC";
            CodeSystem = "2.16.840.1.113883.6.238";
            if (dt.Rows.Count > 0)
            {
                NullFlavor = dt.Rows[0]["nullFlavour"].ToString();
                DisplayName = dt.Rows[0]["DisplayName"].ToString();
            }
            else
            {
                NullFlavor = "NI";
                DisplayName = "Declined to Specify";
            }
        }
    }

    [XmlRoot(ElementName = "place", Namespace = "urn:hl7-org:v3")]
    public class Place
    {
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public string Addr { get; set; }
    }

    [XmlRoot(ElementName = "birthplace", Namespace = "urn:hl7-org:v3")]
    public class Birthplace
    {
        [XmlElement(ElementName = "place", Namespace = "urn:hl7-org:v3")]
        public Place Place { get; set; }
    }

    [XmlRoot(ElementName = "modeCode", Namespace = "urn:hl7-org:v3")]
    public class ModeCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }

        public ModeCode()
        { }
        public ModeCode(string _nullFlavor)
        {
            CodeSystemName = "LanguageAbilityMode";
            CodeSystem = "2.16.840.1.113883.5.60";
            NullFlavor = _nullFlavor;
        }

    }

    [XmlRoot(ElementName = "proficiencyLevelCode", Namespace = "urn:hl7-org:v3")]
    public class ProficiencyLevelCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem = "2.16.840.1.113883.5.61";
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName = "LanguageAbilityProficiency";
        public ProficiencyLevelCode()
        { }

        public ProficiencyLevelCode(string _nullFlavor)
        {
            NullFlavor = _nullFlavor;
        }
    }

    [XmlRoot(ElementName = "preferenceInd", Namespace = "urn:hl7-org:v3")]
    public class PreferenceInd
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        public PreferenceInd()
        { }
        public PreferenceInd(string _value)
        {
            Value = _value;
        }
    }

    [XmlRoot(ElementName = "languageCommunication", Namespace = "urn:hl7-org:v3")]
    public class LanguageCommunication
    {
        [XmlElement(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
        public LanguageCode LanguageCode { get; set; }
        [XmlElement(ElementName = "modeCode", Namespace = "urn:hl7-org:v3")]
        public ModeCode ModeCode { get; set; }
        [XmlElement(ElementName = "proficiencyLevelCode", Namespace = "urn:hl7-org:v3")]
        public ProficiencyLevelCode ProficiencyLevelCode { get; set; }
        [XmlElement(ElementName = "preferenceInd", Namespace = "urn:hl7-org:v3")]
        public PreferenceInd PreferenceInd { get; set; }

        public LanguageCommunication()
        { }
        public LanguageCommunication(DataTable dtPatientLanguage)
        {
            if (dtPatientLanguage.Rows.Count > 0)
                LanguageCode = new LanguageCode(dtPatientLanguage.Rows[0]["DisplayName"].ToString());
            else
                LanguageCode = new LanguageCode("");
            ModeCode = new ModeCode("");
            ProficiencyLevelCode = new ProficiencyLevelCode("");
            PreferenceInd = new PreferenceInd("");
        }
    }

    [XmlRoot(ElementName = "patient", Namespace = "urn:hl7-org:v3")]
    public class Patient
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "administrativeGenderCode", Namespace = "urn:hl7-org:v3")]
        public AdministrativeGenderCode AdministrativeGenderCode { get; set; }
        [XmlElement(ElementName = "birthTime", Namespace = "urn:hl7-org:v3")]
        public BirthTime BirthTime { get; set; }
        [XmlElement(ElementName = "maritalStatusCode", Namespace = "urn:hl7-org:v3")]
        public MaritalStatusCode MaritalStatusCode { get; set; }
        [XmlElement(ElementName = "religiousAffiliationCode", Namespace = "urn:hl7-org:v3")]
        public ReligiousAffiliationCode ReligiousAffiliationCode { get; set; }
        [XmlElement(ElementName = "raceCode", Namespace = "urn:hl7-org:v3")]
        public RaceCode RaceCode { get; set; }
        [XmlElement(ElementName = "ethnicGroupCode", Namespace = "urn:hl7-org:v3")]
        public EthnicGroupCode EthnicGroupCode { get; set; }
        [XmlElement(ElementName = "birthplace", Namespace = "urn:hl7-org:v3")]
        public Birthplace Birthplace { get; set; }
        [XmlElement(ElementName = "languageCommunication", Namespace = "urn:hl7-org:v3")]
        public LanguageCommunication LanguageCommunication { get; set; }
    }

    [XmlRoot(ElementName = "providerOrganization", Namespace = "urn:hl7-org:v3")]
    public class ProviderOrganization
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "standardIndustryClassCode", Namespace = "urn:hl7-org:v3")]
        public string StandardIndustryClassCode { get; set; }
        [XmlElement(ElementName = "asOrganizationPartOf", Namespace = "urn:hl7-org:v3")]
        public string AsOrganizationPartOf { get; set; }

        public ProviderOrganization()
        { }
        public ProviderOrganization(DataTable dtProviderInformation)
        {
            Id = new List<Id>()
            {
                new Id
                {
                    Root = "2.16.840.1.113883.19"
                },
                new Id
                {
                    Extension = "64",
                    Root = "2.16.840.1.113883.4.6"
                },
            };
            if (dtProviderInformation.Rows.Count > 0)
            {
                Name = dtProviderInformation.Rows[0]["PracticeName"].ToString();
                Telecom = new Telecom("WP", "NI");
                Addr = new Addr(dtProviderInformation.Rows[0]["PracticeCity"].ToString(), dtProviderInformation.Rows[0]["PracticeState"].ToString(), "", dtProviderInformation.Rows[0]["PracticeZip"].ToString(), "WP", dtProviderInformation.Rows[0]["PracticeAddress"].ToString());
            }
        }
    }

    [XmlRoot(ElementName = "patientRole", Namespace = "urn:hl7-org:v3")]
    public class PatientRole
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "patient", Namespace = "urn:hl7-org:v3")]
        public Patient Patient { get; set; }
        [XmlElement(ElementName = "providerOrganization", Namespace = "urn:hl7-org:v3")]
        public ProviderOrganization ProviderOrganization { get; set; }
    }

    [XmlRoot(ElementName = "recordTarget", Namespace = "urn:hl7-org:v3")]
    public class RecordTarget
    {
        [XmlElement(ElementName = "patientRole", Namespace = "urn:hl7-org:v3")]
        public PatientRole PatientRole { get; set; }
    }

    [XmlRoot(ElementName = "time", Namespace = "urn:hl7-org:v3")]
    public class Time
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlElement(ElementName = "low", Namespace = "urn:hl7-org:v3")]
        public Low Low { get; set; }

        public Time()
        {

        }

        public Time(string Datetime)
        {
            Low = new Low() { Value = Datetime };
        }

    }

    [XmlRoot(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
    public class AssignedPerson
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public Name Name { get; set; }

        public AssignedPerson()
        {

        }
    }

    [XmlRoot(ElementName = "assignedAuthor", Namespace = "urn:hl7-org:v3")]
    public class AssignedAuthor
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
        public AssignedPerson AssignedPerson { get; set; }

        public AssignedAuthor()
        {

        }

        public AssignedAuthor(DataTable dtTable, string _Use)
        {
            if (dtTable.Rows.Count > 0)
            {
                AssignedPerson = new AssignedPerson { Name = new Name(dtTable.Rows[0]["PracticeName"].ToString(), "", _Use) };
                Addr = new Addr(dtTable.Rows[0]["PracticeCity"].ToString(), dtTable.Rows[0]["PracticeState"].ToString(), "", dtTable.Rows[0]["PracticeZip"].ToString(), "WP", dtTable.Rows[0]["PracticeAddress"].ToString());
            }
            Code = new Code() { CodeSystem = "2.16.840.1.113883.6.101", CodeSystemName = "NUCC" };
            Telecom = new Telecom("WP", "NI");
            Id = new Id() { AssigningAuthorityName = "", Extension = "", Root = "2.16.840.1.113883.4.6" };
        }
    }

    [XmlRoot(ElementName = "author", Namespace = "urn:hl7-org:v3")]
    public class Author
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "assignedAuthor", Namespace = "urn:hl7-org:v3")]
        public AssignedAuthor AssignedAuthor { get; set; }

        public Author()
        {

        }

        public Author(DataTable dt, string _Use)
        {
            AssignedAuthor = new AssignedAuthor(dt, _Use);
            Time = new Time(DateTime.Now.ToString("MMddyyyyhhmmss"));
        }
    }

    [XmlRoot(ElementName = "postalCode", Namespace = "urn:hl7-org:v3")]
    public class PostalCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
    public class AssignedEntity
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
        public AssignedPerson AssignedPerson { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "representedOrganization", Namespace = "urn:hl7-org:v3")]
        public RepresentedOrganization RepresentedOrganization { get; set; }

        public AssignedEntity()
        {

        }

        public AssignedEntity(string Name, string Address, string City, string ZipCode)
        {
            Id = new Id() { Root = "2.16.840.1.113883.4.6" };
            Telecom = new Telecom("WP", "NI");
            Addr = new Addr(City, "", "", ZipCode, "WP", Address);
            AssignedPerson = new AssignedPerson() { Name = new Name(Name, "", "WP") };
        }
    }

    [XmlRoot(ElementName = "dataEnterer", Namespace = "urn:hl7-org:v3")]
    public class DataEnterer
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }

        public DataEnterer()
        {

        }

        public DataEnterer(DataTable dtUserInformation)
        {
            if (dtUserInformation.Rows.Count > 0)
            {
                string Col1 = dtUserInformation.Rows[0]["Col1"].ToString();
                string Col2 = dtUserInformation.Rows[0]["Col2"].ToString();
                string Col3 = dtUserInformation.Rows[0]["Col3"].ToString();
                string Col4 = dtUserInformation.Rows[0]["Col4"].ToString();
                AssignedEntity = new AssignedEntity(Col1, Col2, Col3, Col4);
            }
        }
    }

    [XmlRoot(ElementName = "informant", Namespace = "urn:hl7-org:v3")]
    public class Informant
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }

        public Informant()
        {

        }

        public Informant(DataTable dtUser)
        {
            if (dtUser.Rows.Count > 0)
            {
                string Col1 = dtUser.Rows[0]["Col1"].ToString();
                string Col2 = dtUser.Rows[0]["Col2"].ToString();
                string Col3 = dtUser.Rows[0]["Col3"].ToString();
                string Col4 = dtUser.Rows[0]["Col4"].ToString();
                AssignedEntity = new AssignedEntity(Col1, Col2, Col3, Col4);
            }
        }
    }

    [XmlRoot(ElementName = "representedCustodianOrganization", Namespace = "urn:hl7-org:v3")]
    public class RepresentedCustodianOrganization
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }

        public RepresentedCustodianOrganization()
        {

        }

        public RepresentedCustodianOrganization(DataTable dtUser)
        {

        }
    }

    [XmlRoot(ElementName = "assignedCustodian", Namespace = "urn:hl7-org:v3")]
    public class AssignedCustodian
    {
        [XmlElement(ElementName = "representedCustodianOrganization", Namespace = "urn:hl7-org:v3")]
        public RepresentedCustodianOrganization RepresentedCustodianOrganization { get; set; }

        public AssignedCustodian()
        {

        }

        public AssignedCustodian(DataTable dt)
        {
            RepresentedCustodianOrganization = new RepresentedCustodianOrganization(dt);
        }
    }

    [XmlRoot(ElementName = "custodian", Namespace = "urn:hl7-org:v3")]
    public class Custodian
    {
        [XmlElement(ElementName = "assignedCustodian", Namespace = "urn:hl7-org:v3")]
        public AssignedCustodian AssignedCustodian { get; set; }

        public Custodian()
        {

        }

        public Custodian(DataTable dt)
        {
            AssignedCustodian = new AssignedCustodian(dt);
        }
    }

    [XmlRoot(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
    public class InformationRecipient
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "receivedOrganization", Namespace = "urn:hl7-org:v3")]
    public class ReceivedOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "intendedRecipient", Namespace = "urn:hl7-org:v3")]
    public class IntendedRecipient
    {
        [XmlElement(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
        public InformationRecipient InformationRecipient { get; set; }
        [XmlElement(ElementName = "receivedOrganization", Namespace = "urn:hl7-org:v3")]
        public ReceivedOrganization ReceivedOrganization { get; set; }
    }

    [XmlRoot(ElementName = "signatureCode", Namespace = "urn:hl7-org:v3")]
    public class SignatureCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "legalAuthenticator", Namespace = "urn:hl7-org:v3")]
    public class LegalAuthenticator
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "signatureCode", Namespace = "urn:hl7-org:v3")]
        public SignatureCode SignatureCode { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "low", Namespace = "urn:hl7-org:v3")]
    public class Low
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "performer", Namespace = "urn:hl7-org:v3")]
    public class Performer
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }

        public Performer()
        {

        }

        public Performer(DataTable dtUser)
        {
            TemplateId = new TemplateId("2.16.840.1.113883.10.20.6.2.1");
            if (dtUser.Rows.Count > 0)
            {
                string Col1 = dtUser.Rows[0]["Col1"].ToString();
                string Col2 = dtUser.Rows[0]["Col2"].ToString();
                string Col3 = dtUser.Rows[0]["Col3"].ToString();
                string Col4 = dtUser.Rows[0]["Col4"].ToString();
                AssignedEntity = new AssignedEntity(Col1, Col2, Col3, Col4);
            }
        }
    }

    [XmlRoot(ElementName = "serviceEvent", Namespace = "urn:hl7-org:v3")]
    public class ServiceEvent
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "performer", Namespace = "urn:hl7-org:v3")]
        public Performer Performer { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }

        public ServiceEvent()
        {

        }

        public ServiceEvent(DataTable dt)
        {
            ClassCode = "ACT";
            Id = new List<Integration.ClinicalDocument.Id> { new Id() { Root = "1.2.840.113619.2.62.994044785528.114289542805" } };
            Code = new Code() { CodeSystem = "CPT4", CodeSystemName = "2.16.840.1.113883.6.12" };
            EffectiveTime = new EffectiveTime(DateTime.Now.ToString("MMddyyyy"));
            
        }
    }

    [XmlRoot(ElementName = "documentationOf", Namespace = "urn:hl7-org:v3")]
    public class DocumentationOf
    {
        [XmlElement(ElementName = "serviceEvent", Namespace = "urn:hl7-org:v3")]
        public ServiceEvent ServiceEvent { get; set; }

        public DocumentationOf()
        {

        }

        public DocumentationOf(DataTable dtUser)
        {
            ServiceEvent = new ServiceEvent(dtUser);
        }
    }

    [XmlRoot(ElementName = "representedOrganization", Namespace = "urn:hl7-org:v3")]
    public class RepresentedOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }

        public RepresentedOrganization()
        {

        }

        public RepresentedOrganization(string _Name)
        {
            Name = _Name;
        }
    }

    [XmlRoot(ElementName = "responsibleParty", Namespace = "urn:hl7-org:v3")]
    public class ResponsibleParty
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "encounterParticipant", Namespace = "urn:hl7-org:v3")]
    public class EncounterParticipant
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "location", Namespace = "urn:hl7-org:v3")]
    public class Location
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
    }

    [XmlRoot(ElementName = "healthCareFacility", Namespace = "urn:hl7-org:v3")]
    public class HealthCareFacility
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "location", Namespace = "urn:hl7-org:v3")]
        public Location Location { get; set; }
    }

    [XmlRoot(ElementName = "encompassingEncounter", Namespace = "urn:hl7-org:v3")]
    public class EncompassingEncounter
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "responsibleParty", Namespace = "urn:hl7-org:v3")]
        public ResponsibleParty ResponsibleParty { get; set; }
        [XmlElement(ElementName = "encounterParticipant", Namespace = "urn:hl7-org:v3")]
        public EncounterParticipant EncounterParticipant { get; set; }
        [XmlElement(ElementName = "location", Namespace = "urn:hl7-org:v3")]
        public Location Location { get; set; }
    }

    [XmlRoot(ElementName = "componentOf", Namespace = "urn:hl7-org:v3")]
    public class ComponentOf
    {
        [XmlElement(ElementName = "encompassingEncounter", Namespace = "urn:hl7-org:v3")]
        public EncompassingEncounter EncompassingEncounter { get; set; }
    }

    [XmlRoot(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
    public class Tr
    {
        [XmlElement(ElementName = "td", Namespace = "urn:hl7-org:v3")]
        public List<Td> Td { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlElement(ElementName = "th", Namespace = "urn:hl7-org:v3")]
        public List<Th> Th { get; set; }
        public Tr()
        { }
    }

    [XmlRoot(ElementName = "thead", Namespace = "urn:hl7-org:v3")]
    public class Thead
    {
        [XmlElement(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
        public Tr Tr { get; set; }
        public Thead()
        { }
        public Thead(string type)
        {
            if (type == "5")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Type" });
                ListTH.Add(new Th() { Text = "Substance" });
                ListTH.Add(new Th() { Text = "Reaction" });
                ListTH.Add(new Th() { Text = "Status" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }
            if (type == "6")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Medication" });
                ListTH.Add(new Th() { Text = "Start Date" });
                ListTH.Add(new Th() { Text = "Frequency" });
                ListTH.Add(new Th() { Text = "Status" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }
            if (type == "7")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Problem" });
                ListTH.Add(new Th() { Text = "Effective Dates" });
                ListTH.Add(new Th() { Text = "Problem Status" });
                ListTH.Add(new Th() { Text = "ICD Code" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }
            if (type == "8")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Reason for Visit/Chief Complaint" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }
            if (type == "9")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Name" });
                ListTH.Add(new Th() { Text = "Body Site" });
                ListTH.Add(new Th() { Text = "Date" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }
            if (type == "10")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Social History Element" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;

            }
            if (type == "11")
            {
                List<Th> ListTH = new List<Th>();
                ListTH.Add(new Th() { Text = "Social History Element" });
                ListTH.Add(new Th() { Text = "Description" });
                ListTH.Add(new Th() { Text = "Effective Dates" });
                Tr tr1 = new Tr();
                tr1.Th = ListTH;
                Tr = tr1;
            }

        }
    }

    [XmlRoot(ElementName = "td", Namespace = "urn:hl7-org:v3")]
    public class Td
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlElement(ElementName = "content", Namespace = "urn:hl7-org:v3")]
        public Content Content { get; set; }
        public Td()
        { }
    }

    [XmlRoot(ElementName = "tbody", Namespace = "urn:hl7-org:v3")]
    public class Tbody
    {
        [XmlElement(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
        public List<Tr> Tr { get; set; }
        public Tbody()
        { }
        public Tbody(string type, DataTable dsPatientComponent)
        {
            if (type == "5")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td51" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td52" + i.ToString(), Text = dr["Col2"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td53" + i.ToString(), Text = dr["Col3"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td54" + i.ToString(), Text = dr["Col4"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = tablerow;
            }
            if (type == "6")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td61" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td62" + i.ToString(), Text = dr["Col2"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td63" + i.ToString(), Text = dr["Col3"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td64" + i.ToString(), Text = dr["Col4"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = tablerow;
            }
            if (type == "7")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td71" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td72" + i.ToString(), Text = dr["Col2"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td73" + i.ToString(), Text = dr["Col3"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td74" + i.ToString(), Text = dr["Col4"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = tablerow;
            }
            if (type == "8")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td81" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = (tablerow);
            }
            if (type == "9") // Procedures
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td91" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td92" + i.ToString(), Text = dr["Col2"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td93" + i.ToString(), Text = dr["Col3"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = (tablerow);
            }
            if (type == "10")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td101" + i.ToString(), Text = dr["Col1"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td102" + i.ToString(), Text = dr["Col2"].ToString(), Content = new Content("", "") });
                    tdMaster.Add(new Td { ID = "td103" + i.ToString(), Text = dr["Col3"].ToString(), Content = new Content("", "") });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = (tablerow);
            }
            if (type == "11")
            {
                int i = 1;
                List<Tr> tablerow = new List<Tr>();
                foreach (DataRow dr in dsPatientComponent.Rows)
                {
                    List<Td> tdMaster = new List<Td>();
                    Tr row = new Integration.ClinicalDocument.Tr();
                    tdMaster.Add(new Td { ID = "td111" + i.ToString(), Text = dr["Col1"].ToString() });
                    tdMaster.Add(new Td { ID = "td112" + i.ToString(), Text = dr["Col2"].ToString() });
                    tdMaster.Add(new Td { ID = "td113" + i.ToString(), Text = dr["Col3"].ToString() });
                    row.Td = tdMaster;
                    tablerow.Add(row);
                    i++;
                }
                Tr = (tablerow);
            }
        }
    }

    [XmlRoot(ElementName = "table", Namespace = "urn:hl7-org:v3")]
    public class Table
    {
        [XmlElement(ElementName = "thead", Namespace = "urn:hl7-org:v3")]
        public Thead Thead { get; set; }
        [XmlElement(ElementName = "tbody", Namespace = "urn:hl7-org:v3")]
        public Tbody Tbody { get; set; }
        [XmlAttribute(AttributeName = "border")]
        public string Border { get; set; }
        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }
        public Table()
        { }
    }

    [XmlRoot(ElementName = "text", Namespace = "urn:hl7-org:v3")]
    public class Text
    {
        [XmlElement(ElementName = "table", Namespace = "urn:hl7-org:v3")]
        public Table Table { get; set; }
        [XmlElement(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
        public Reference Reference { get; set; }
        public Text()
        {
        }
    }

    [XmlRoot(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
    public class StatusCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "high", Namespace = "urn:hl7-org:v3")]
    public class High
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
    public class Reference
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
    public class OriginalText
    {
        [XmlElement(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
        public Reference Reference { get; set; }
    }

    [XmlRoot(ElementName = "value", Namespace = "urn:hl7-org:v3")]
    public class Value
    {
        [XmlElement(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
        public OriginalText OriginalText { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "playingEntity", Namespace = "urn:hl7-org:v3")]
    public class PlayingEntity
    {
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "participantRole", Namespace = "urn:hl7-org:v3")]
    public class ParticipantRole
    {
        [XmlElement(ElementName = "playingEntity", Namespace = "urn:hl7-org:v3")]
        public PlayingEntity PlayingEntity { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "participant", Namespace = "urn:hl7-org:v3")]
    public class Participant
    {
        [XmlElement(ElementName = "participantRole", Namespace = "urn:hl7-org:v3")]
        public ParticipantRole ParticipantRole { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
    public class Observation
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "value", Namespace = "urn:hl7-org:v3")]
        public Value Value { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
        public EntryRelationship EntryRelationship { get; set; }
        [XmlElement(ElementName = "interpretationCode", Namespace = "urn:hl7-org:v3")]
        public InterpretationCode InterpretationCode { get; set; }

        public Participant Participant { get; set; }
    }

    [XmlRoot(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
    public class EntryRelationship
    {
        [XmlElement(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
        public Observation Observation { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
        [XmlAttribute(AttributeName = "inversionInd")]
        public string InversionInd { get; set; }
    }

    [XmlRoot(ElementName = "act", Namespace = "urn:hl7-org:v3")]
    public class Act
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
        public EntryRelationship EntryRelationship { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    [XmlRoot(ElementName = "entry", Namespace = "urn:hl7-org:v3")]
    public class Entry
    {
        [XmlElement(ElementName = "act", Namespace = "urn:hl7-org:v3")]
        public Act Act { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "substanceAdministration", Namespace = "urn:hl7-org:v3")]
        public SubstanceAdministration SubstanceAdministration { get; set; }
        [XmlElement(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
        public Observation Observation { get; set; }
        [XmlElement(ElementName = "organizer", Namespace = "urn:hl7-org:v3")]
        public Organizer Organizer { get; set; }
    }

    [XmlRoot(ElementName = "section", Namespace = "urn:hl7-org:v3")]
    public class Section
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public List<TemplateId> TemplateId { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "title", Namespace = "urn:hl7-org:v3")]
        public string Title { get; set; }
        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }
        [XmlElement(ElementName = "entry", Namespace = "urn:hl7-org:v3")]
        public List<Entry> Entry { get; set; }

        public Section()
        { }
    }

    [XmlRoot(ElementName = "component", Namespace = "urn:hl7-org:v3")]
    public class component
    {
        [XmlElement(ElementName = "section", Namespace = "urn:hl7-org:v3")]
        public Section Section { get; set; }

        public component()
        { }
        public component(string _componentType, DataTable dtPatientComponent)
        {
            // Allergies
            if (_componentType == "5")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.1.1")
                    },
                    Code = new Code()
                    {
                        CodeSystem = "2.16.840.1.113883.6.1"
                    },
                    Title = "ALLERGIES, ADVERSE REACTIONS, ALERTS",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // Medication
            if (_componentType == "6")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.6.1")
                    },
                    Title = "MEDICATIONS",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // Problems 
            if (_componentType == "7")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.5"),
                        new TemplateId("2.16.840.1.113883.10.20.22.2.5.1")
                    },
                    Title = "PROBLEMS",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // CHIEF COMPLIANT 
            if (_componentType == "8")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.13")
                    },
                    Title = "CHIEF COMPLAINT",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // PROCEDURES 
            if (_componentType == "9")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.7"),
                        new TemplateId("2.16.840.1.113883.10.20.22.2.7.1")
                    },
                    Code = new Code()
                    {
                        CodeSystem = "2.16.840.1.113883.6.1"
                    },
                    Title = "PROCEDURES",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // VITAL SIGNS 
            if (_componentType == "10")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.4.1")
                    },
                    Title = "VITAL SIGNS",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }
            // SOCIAL HISTORY 
            if (_componentType == "11")
            {
                Section = new Section()
                {
                    TemplateId = new List<TemplateId>()
                    {
                        new TemplateId("2.16.840.1.113883.10.20.22.2.17")
                    },
                    Title = "SOCIAL HISTORY",
                    Text = new Text
                    {
                        Table = new Table
                        {
                            Width = "100%",
                            Border = "1",
                            Thead = new Thead(_componentType),
                            Tbody = new Tbody(_componentType, dtPatientComponent)
                        }
                    },
                };
            }

        }

    }

    [XmlRoot(ElementName = "content", Namespace = "urn:hl7-org:v3")]
    public class Content
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlText]
        public string Text { get; set; }
        public Content()
        {

        }

        public Content(string Id, string _Text)
        {
            ID = Id;
            Text = _Text;
        }

    }

    [XmlRoot(ElementName = "doseQuantity", Namespace = "urn:hl7-org:v3")]
    public class DoseQuantity
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "unit")]
        public string Unit { get; set; }
    }

    [XmlRoot(ElementName = "translation", Namespace = "urn:hl7-org:v3")]
    public class Translation
    {
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "manufacturedMaterial", Namespace = "urn:hl7-org:v3")]
    public class ManufacturedMaterial
    {
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "manufacturerOrganization", Namespace = "urn:hl7-org:v3")]
    public class ManufacturerOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "manufacturedProduct", Namespace = "urn:hl7-org:v3")]
    public class ManufacturedProduct
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "manufacturedMaterial", Namespace = "urn:hl7-org:v3")]
        public ManufacturedMaterial ManufacturedMaterial { get; set; }
        [XmlElement(ElementName = "manufacturerOrganization", Namespace = "urn:hl7-org:v3")]
        public ManufacturerOrganization ManufacturerOrganization { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "consumable", Namespace = "urn:hl7-org:v3")]
    public class Consumable
    {
        [XmlElement(ElementName = "manufacturedProduct", Namespace = "urn:hl7-org:v3")]
        public ManufacturedProduct ManufacturedProduct { get; set; }
    }

    [XmlRoot(ElementName = "substanceAdministration", Namespace = "urn:hl7-org:v3")]
    public class SubstanceAdministration
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "doseQuantity", Namespace = "urn:hl7-org:v3")]
        public DoseQuantity DoseQuantity { get; set; }
        [XmlElement(ElementName = "consumable", Namespace = "urn:hl7-org:v3")]
        public Consumable Consumable { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    [XmlRoot(ElementName = "th", Namespace = "urn:hl7-org:v3")]
    public class Th
    {
        [XmlAttribute(AttributeName = "colspan")]
        public string Colspan { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "align")]
        public string Align { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "interpretationCode", Namespace = "urn:hl7-org:v3")]
    public class InterpretationCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "organizer", Namespace = "urn:hl7-org:v3")]
    public class Organizer
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public Component Component { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    public class Component
    {
        [XmlElement(ElementName = "structuredBody", Namespace = "urn:hl7-org:v3")]
        public StructuredBody structuredBody { get; set; }
        public Component()
        {

        }
    }


    [XmlRoot(ElementName = "structuredBody", Namespace = "urn:hl7-org:v3")]
    public class StructuredBody
    {
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public List<component> component { get; set; }

        public StructuredBody()
        { }
        public StructuredBody(string _patientId, IDbConnection dbConnection)
        {
            List<component> componentlist = new List<component>();

            // Allergies
            string _selectQry = "Exec Model.CCDA_PatientAllergies '" + _patientId + "'";
            DataTable dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("5", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Medication
            _selectQry = "Exec Model.CCDA_PatientMedication '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("6", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Problems 
            _selectQry = "Exec Model.CCDA_PatientProblem '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("7", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Chief Complaint
            _selectQry = "Exec Model.CCDA_PatientChiefComplaint '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("8", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Encounters 
            _selectQry = "Exec Model.CCDA_PatientEncounters '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("9", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Vital Sign 
            _selectQry = "Exec Model.CCDA_PatientVitalSign '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("10", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            // Smoking Status
            _selectQry = "Exec Model.CCDA_PatientSmokingStatus '" + _patientId + "'";
            dtTemp = dbConnection.Execute<DataTable>(_selectQry);
            if (dtTemp != null)
            {
                component tempComponent = new component("11", dtTemp);
                componentlist.Add(tempComponent);
                dtTemp.Dispose();
            }

            component = componentlist;

        }
    }

    [XmlRoot(ElementName = "ClinicalDocument", Namespace = "urn:hl7-org:v3")]
    [Serializable]
    public class ClinicalDocument
    {
        public ClinicalDocument()
        { }

        [XmlElement(ElementName = "realmCode", Namespace = "urn:hl7-org:v3")]
        public RealmCode RealmCode { get; set; }
        [XmlElement(ElementName = "typeId", Namespace = "urn:hl7-org:v3")]
        public TypeId TypeId { get; set; }
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public List<TemplateId> TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "title", Namespace = "urn:hl7-org:v3")]
        public string Title { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "confidentialityCode", Namespace = "urn:hl7-org:v3")]
        public ConfidentialityCode ConfidentialityCode { get; set; }
        [XmlElement(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
        public LanguageCode LanguageCode { get; set; }
        [XmlElement(ElementName = "setId", Namespace = "urn:hl7-org:v3")]
        public SetId SetId { get; set; }
        [XmlElement(ElementName = "versionNumber", Namespace = "urn:hl7-org:v3")]
        public VersionNumber VersionNumber { get; set; }
        [XmlElement(ElementName = "recordTarget", Namespace = "urn:hl7-org:v3")]
        public RecordTarget RecordTarget { get; set; }
        [XmlElement(ElementName = "author", Namespace = "urn:hl7-org:v3")]
        public Author Author { get; set; }
        [XmlElement(ElementName = "dataEnterer", Namespace = "urn:hl7-org:v3")]
        public DataEnterer DataEnterer { get; set; }
        [XmlElement(ElementName = "informant", Namespace = "urn:hl7-org:v3")]
        public Informant Informant { get; set; }
        [XmlElement(ElementName = "custodian", Namespace = "urn:hl7-org:v3")]
        public Custodian Custodian { get; set; }
        [XmlElement(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
        public InformationRecipient InformationRecipient { get; set; }
        [XmlElement(ElementName = "legalAuthenticator", Namespace = "urn:hl7-org:v3")]
        public LegalAuthenticator LegalAuthenticator { get; set; }
        [XmlElement(ElementName = "documentationOf", Namespace = "urn:hl7-org:v3")]
        public DocumentationOf DocumentationOf { get; set; }
        [XmlElement(ElementName = "componentOf", Namespace = "urn:hl7-org:v3")]
        public ComponentOf ComponentOf { get; set; }
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public Component Component { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "mif", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Mif { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }


        public ClinicalDocument(string patientId, string UserId)
        {
            string _SelectQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _SelectQry = "Select FirstName, LastName, DateOfBirth, p2.Line1 + ' ' + p2.Line2 as streetAddressLine, p2.City,  m1.Abbreviation as State, m2.Abbreviation2Letters as Country, p2.PostalCode, m3.Code as GenderCode, m3.Type as GenderDisplayName From model.Patients p1 left join model.SexType_Enumeration m3 on m3.Id = p1.GenderId left join model.PatientAddresses p2 on p1.Id = p2.PatientId left join model.StateOrProvinces m1 on m1.Id = p2.StateOrProvinceId left join model.Countries m2 on m1.CountryId = m2.Id Where p1.Id = '" + patientId + "' and p2.OrdinalId = 1";
                DataTable dtPatientDetail = dbConnection.Execute<DataTable>(_SelectQry);

                _SelectQry = "Select top(1) m1.Name as DisplayName, 'NI' as nullFlavour From model.PatientRace p1 inner join model.Races m1 on p1.Races_Id = m1.Id where p1.Patients_Id = '" + patientId + "'";
                DataTable dtPatientRace = dbConnection.Execute<DataTable>(_SelectQry);

                _SelectQry = "Select top(1) m1.Name as DisplayName, 'NI' as nullFlavour From model.PatientLanguages p1 inner join model.Languages m1 on p1.LanguageId = m1.Id where p1.PatientId ='" + patientId + "'";
                DataTable dtPatientEthnicties = dbConnection.Execute<DataTable>(_SelectQry);

                _SelectQry = "Select top(1) m1.Name as DisplayName, 'NI' as nullFlavour From model.PatientEthnicities p1 inner join model.Ethnicities m1 on p1.EthnicityId = m1.Id where p1.PatientId = '" + patientId + "'";
                DataTable dtPatientLanguages = dbConnection.Execute<DataTable>(_SelectQry);

                _SelectQry = "Select PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip From dbo.PracticeName a inner join model.Patients b on b.BillingOrganizationId = a.PracticeId Where b.Id = '" + patientId + "'";
                DataTable dtPatientProvider = dbConnection.Execute<DataTable>(_SelectQry);

                RealmCode = new RealmCode
                {
                    Code = "US"
                };
                TypeId = new TypeId
                {
                    Extension = "POCD_HD000040",
                    Root = "2.16.840.1.113883.1.3"
                };
                TemplateId = new List<TemplateId>
                {
                new TemplateId("2.16.840.1.113883.10.20.22.1.1"),
                new TemplateId("2.16.840.1.113883.10.20.22.1.2")
                };
                Id = new Id
                {
                    Root = "IOPW-CCDA-"+ patientId,
                    Extension = "2.16.840.1.113883.19",
                    AssigningAuthorityName = "IOPracticeware INC"
                };
                Code = new Code
                {
                    CodeSystem = "LOINC",
                    CodeSystemName = "2.16.840.1.113883.6.1",
                    DisplayName = "Summarization of episode note",
                    _code = "34133-9"
                };
                Title = "2015 VDT AMB: Consolidated CDA - Ambulatory";
                EffectiveTime = new EffectiveTime
                {
                    Value = DateTime.Now.ToString("yyyyMMddhhmmss-000")
                };
                ConfidentialityCode = new ConfidentialityCode
                {
                    Code = "N",
                    CodeSystem = "2.16.840.1.113883.5.25"
                };
                LanguageCode = new LanguageCode("en-US");
                SetId = new SetId(patientId, "2.16.840.1.113883.19");
                VersionNumber = new VersionNumber("1");
                RecordTarget = new RecordTarget
                {
                    PatientRole = new PatientRole
                    {
                        Id = new List<Id>
                               {
                                   new Id
                                   {
                                       Extension = patientId,
                                       Root = "2.16.840.1.113883.19"
                                   }
                               },
                        Addr = new Addr(dtPatientDetail, "HP"),
                        Telecom = new Telecom("", ""),
                        Patient = new Patient()
                        {
                            Name = new Name(dtPatientDetail, "L"),
                            AdministrativeGenderCode = new AdministrativeGenderCode(dtPatientDetail),
                            BirthTime = new BirthTime(dtPatientDetail),
                            MaritalStatusCode = new MaritalStatusCode("", ""),
                            ReligiousAffiliationCode = new ReligiousAffiliationCode(""),
                            RaceCode = new RaceCode(dtPatientRace),
                            EthnicGroupCode = new EthnicGroupCode(dtPatientEthnicties),
                            LanguageCommunication = new LanguageCommunication(dtPatientLanguages),
                        },
                        ProviderOrganization = new ProviderOrganization(dtPatientProvider)
                    }
                };

                Author = new Author(dtPatientProvider, "WP");

                DataTable dtUserInformation = dbConnection.Execute<DataTable>("Select a.UserName as Col1, ISNULL(b.Line1,'') + ISNULL(b.Line2,'') as Col2, b.City as Col3, b.PostalCode as Col4 From model.Users a inner join model.UserAddresses b on a.Id = b.UserId where UserId = '"+ UserId + "'");

                DataEnterer = new DataEnterer(dtUserInformation);

                Informant = new Informant(dtUserInformation);

                //Custodian = new Custodian(dtPatientProvider);

                InformationRecipient = new InformationRecipient
                {
                    Name = ""
                };

                //LegalAuthenticator = new LegalAuthenticator
                //{
                //    AssignedEntity = new AssignedEntity
                //    {
                //        Addr = new Addr("", "", "", "", "", ""),
                //        AssignedPerson = new AssignedPerson
                //        {
                //            Name = new Name("", "", "")
                //        },
                //        Code = new Code
                //        {
                //            CodeSystem = "Test Data"
                //        },
                //        Id = new Id
                //        {
                //            AssigningAuthorityName = "Test Data",
                //            Extension = "Test Data",
                //            Root = "Test Data"
                //        },
                //        Telecom = new Telecom("", "")
                //    }
                //};

                DocumentationOf = new DocumentationOf(dtUserInformation);

                Component = new Component()
                {
                    structuredBody = new StructuredBody(patientId, dbConnection)
                };
            }
        }
    }
}











