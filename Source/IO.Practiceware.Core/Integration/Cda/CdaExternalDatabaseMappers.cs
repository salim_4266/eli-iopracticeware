﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Cda;
using IO.Practiceware.Services.ExternalSystems;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    /// Class provided for convenient access to the various external database model mappers.
    /// Be sure to initialize the Properties of each mapper within this class before use.
    /// </summary>
    [ScopedLifestyle(LifestyleType.Singleton)] // TODO: make it support transient lifestyle
    public class CdaModelMapper
    {
        public CdaModelMapper(SnomedModelMapper snomedModelMapper, CvxModelMapper cvxModelMapper,
            LoincModelMapper loincModelMapper, RxNormModelMapper rxNormModelMapper,
            CdcModelMapper cdcModelMapper, QualityDataModelValueSetModelMapper qualityDataModelValueSetModelMapper)
        {
            CdcModelMapper = cdcModelMapper;
            QualityDataModelValueSetModelMapper = qualityDataModelValueSetModelMapper;
            SnomedModelMapper = snomedModelMapper;
            CvxModelMapper = cvxModelMapper;
            LoincModelMapper = loincModelMapper;
            RxNormModelMapper = rxNormModelMapper;
        }

        #region Snomed

        public virtual Dictionary<ClinicalProcedure, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<ClinicalProcedure> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedClinicalProcedureExternalEntityName);
        }

        public virtual Dictionary<ClinicalCondition, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<ClinicalCondition> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedClinicalConditionExternalEntityName);
        }

        public virtual Dictionary<TreatmentGoal, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<TreatmentGoal> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedTreatmentGoalExternalEntityName);
        }

        public virtual Dictionary<SmokingCondition, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<SmokingCondition> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedSmokingConditionExternalEntityName);
        }

        public virtual Dictionary<AllergenReactionType, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<AllergenReactionType> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedAllergenReactionTypeExternalEntityName);
        }

        public virtual Dictionary<ClinicalQualifier, SnomedDescriptionModel> GetSnomedModelMap(IEnumerable<ClinicalQualifier> source)
        {
            return SnomedModelMapper.GetSnomedModelMap(source, Constants.SnomedClinicalQualifierExternalEntityName);
        }

        #endregion

        #region Cvx

        public Dictionary<Vaccine, CvxAdministeredModel> GetCvxModelMap(IEnumerable<Vaccine> source)
        {
            return CvxModelMapper.GetCvxModelMap(source, Constants.CvxAdministeredVaccineExternalEntityName);
        }

        #endregion

        #region RxNorm

        public virtual Dictionary<Allergen, RxNormModel> GetRxNormDescriptionMap(IEnumerable<Allergen> source)
        {
            return RxNormModelMapper.GetRxNormModelMap(source, Constants.RxNormAllergenExternalEntityName);
        }

        public virtual Dictionary<Medication, RxNormModel> GetRxNormDescriptionMap(IEnumerable<Medication> source)
        {
            return RxNormModelMapper.GetRxNormModelMap(source, Constants.RxNormMedicationsExternalEntityName);
        }

        #endregion

        #region Loinc

        public virtual Dictionary<LaboratoryTest, LoincModel> GetLoincDescriptionMap(IEnumerable<LaboratoryTest> source)
        {
            return LoincModelMapper.GetLoincModelMap(source, Constants.LoincLaboratoryTestExternalEntityName);
        }

        #endregion

        #region Cdc

        public virtual Dictionary<long, CdcRaceAndEthnicityModel> GetCdcRaceModelMap(IEnumerable<Race> source)
        {
            return CdcModelMapper.GetCdcRaceAndEthnicityModelMap(source, Constants.CdcRaceAndEthnicityExtenalEntityName);
        }

        public virtual Dictionary<long, CdcRaceAndEthnicityModel> GetCdcEthnicityModelMap(IEnumerable<Ethnicity> source)
        {
            return CdcModelMapper.GetCdcRaceAndEthnicityModelMap(source, Constants.CdcRaceAndEthnicityExtenalEntityName);
        }

        #endregion

        public SnomedModelMapper SnomedModelMapper { get; private set; }
        public CvxModelMapper CvxModelMapper { get; private set; }
        public LoincModelMapper LoincModelMapper { get; private set; }
        public RxNormModelMapper RxNormModelMapper { get; private set; }
        public CdcModelMapper CdcModelMapper { get; private set; }
        public QualityDataModelValueSetModelMapper QualityDataModelValueSetModelMapper { get; private set; }
    }

    /// <summary>
    /// Provides common convenience methods accessible and used by each derived external database model mapper.
    /// </summary>
    /// <remarks>
    /// When implementing a new ExternalModelMapper, please follow the pattern expressed in SnomedModelMapper, RxNormModelMapper etc.
    /// The derived properties should express the entities that map to the specified external clinical database.
    /// The other 3 methods, GetModelMap, GetExternalEntityKeys, and TryInitializeModels should be implemented similarly to the established pattern.
    /// </remarks>
    [SupportsNotifyPropertyChanged(true, true)]
    public abstract class ExternalModelMapper
    {
        protected ExternalModelMapper()
        {
            this.As<INotifyPropertyChanged>().PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged).MakeWeak();
        }

        /// <summary>
        /// Method that allows derived external model mappers to (re)initialize its Data if any properties have changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CanInitialize = true;
        }

        /// <summary>
        /// Convenience method to get external entity keys using the given parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityIds"></param>
        /// <param name="externalSystemName"></param>
        /// <param name="externalEntityName"></param>
        /// <returns></returns>
        protected IEnumerable<string> GetExternalEntityKeys<T>(IEnumerable<string> entityIds, string externalSystemName, string externalEntityName)
        {
            var source = entityIds != null ? entityIds.ToArray() : new string[] { };
            if (!source.Any())
            {
                return new string[] { };
            }

            var mappings = ExternalSystemService.GetMappings<T>(source, externalSystemName, externalEntityName);

            return mappings.Select(i => i.ExternalSystemEntityKey);
        }

        /// <summary>
        /// Convenience method to get the int Ids of the given source.
        /// The items of the source must have a property named 'Id'.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected IEnumerable<int> GetIds(IEnumerable<object> source)
        { 
            return source.WhereNotDefault().Select(GetId).Distinct();
        }

        /// <summary>
        /// Convenience method to get the Ids of the given source.
        /// The items of the source must have a property named 'Id'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        protected IEnumerable<T> GetIds<T>(IEnumerable<object> source)
        {
            return source.WhereNotDefault().Select(GetId<T>).Distinct();
        }

        /// <summary>
        /// Gets the int Id of the given item.
        /// The item must have a property named 'Id'.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected static int GetId(object item)
        {
            return GetId<int>(item);
        }

        /// <summary>
        /// Gets the int Id of the given item.
        /// The item must have a property named 'Id'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        protected static T GetId<T>(object item)
        {
            try
            {
                if (item is IHasId)
                {
                    return (T)((IHasId)item).Id;
                }
                if (item is Enum)
                {
                    return (T)item;
                }

                return (T)item;
            }
            catch (Exception ex)
            {
                var exception = new Exception("Does not have 'Id' property of type {0}".FormatWith(typeof(T).Name), ex);
                throw exception;
            }
        }

        /// <summary>
        /// Allows derived external model mappers to (re)initialize its Data if any properties have changed.
        /// </summary>
        protected bool CanInitialize { get; set; }

        [Dependency]
        protected virtual IExternalSystemService ExternalSystemService { get; set; }

        [Dependency]
        protected virtual ICdaService CdaService { get; set; }
    }

    /// <summary>
    /// Snomed implementation of the ExternalModelMapper.
    /// </summary>
    public class SnomedModelMapper : ExternalModelMapper
    {
        private SnomedDescriptionModel[] _snomedModels = { };
        private IEnumerable<int> _clinicalQualifierIds;
        private IEnumerable<int> _allergenReactionTypeIds;
        private IEnumerable<int> _clinicalConditionIds;
        private IEnumerable<int> _clinicalProcedureIds;
        private IEnumerable<int> _smokingConditionIds;
        private IEnumerable<int> _treatmentGoalIds;
        private IEnumerable<int> _clinicalConditionStatusIds;
        private IEnumerable<int> _lateralityIds;

        /// <summary>
        /// Gets a mapping from the given source, to the SnomedModels.
        /// The source type must be defined as a public property of this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        public virtual Dictionary<T, SnomedDescriptionModel> GetSnomedModelMap<T>(IEnumerable<T> source, string externalSystemEntityName) where T : new()
        {
            TryInitializeSnomedModels();

            var array = source.Distinct().WhereNotDefault().ToArray();
            var mappings = ExternalSystemService.GetMappings(array, Constants.SnomedExternalSystemName, externalSystemEntityName).ToArray();

            var result = new Dictionary<T, SnomedDescriptionModel>();
            foreach (var a in array)
            {
                var item = a;
                var externalEntityKeys = mappings.Where(i => i.PracticeRepositoryEntityKey == GetId(item).ToString()).Select(i => i.ExternalSystemEntityKey).ToList();
                var externalModel = _snomedModels.FirstOrDefault(i => externalEntityKeys.Contains(i.ConceptId));

                result.Add(item, externalModel);
            }
            return result;
        }

        private IEnumerable<string> GetExternalEntityKeys<T>(IEnumerable<int> entityIds, string externalEntityName)
        {
            return GetExternalEntityKeys<T>(entityIds.Select(i => i.ToString()), Constants.SnomedExternalSystemName, externalEntityName);
        }

        /// <summary>
        /// Initializes the SnomedModels using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeSnomedModels()
        {
            if (!CanInitialize) return;

            var externalEntityKeys = new List<string>();
            externalEntityKeys.AddRange(GetExternalEntityKeys<AllergenReactionType>(_allergenReactionTypeIds, Constants.SnomedAllergenReactionTypeExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<ClinicalCondition>(_clinicalConditionIds, Constants.SnomedClinicalConditionExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<ClinicalProcedure>(_clinicalProcedureIds, Constants.SnomedClinicalProcedureExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<ClinicalQualifier>(_clinicalQualifierIds, Constants.SnomedClinicalQualifierExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<SmokingCondition>(_smokingConditionIds, Constants.SnomedSmokingConditionExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<TreatmentGoal>(_treatmentGoalIds, Constants.SnomedTreatmentGoalExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<ClinicalConditionStatus>(_clinicalConditionStatusIds, Constants.SnomedClinicalConditionStatusExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<Laterality>(_lateralityIds, Constants.SnomedLateralityEntityName));

            var existingKeys = _snomedModels.Select(i => i.ConceptId);
            var idsToQuery = externalEntityKeys.Where(i => !existingKeys.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _snomedModels = _snomedModels.Concat(CdaService.GetSnomedDescriptionModels(idsToQuery.Distinct()))
                                             .Distinct()
                                             .ToArray();
            }

            // Reset
            CanInitialize = false;
        }

        public virtual IEnumerable<ClinicalQualifier> ClinicalQualifiers { set { _clinicalQualifierIds = GetIds(value); } }
        public virtual IEnumerable<AllergenReactionType> AllergenReactionTypes { set { _allergenReactionTypeIds = GetIds(value); } }
        public virtual IEnumerable<ClinicalCondition> ClinicalConditions { set { _clinicalConditionIds = GetIds(value); } }
        public virtual IEnumerable<ClinicalProcedure> ClinicalProcedures { set { _clinicalProcedureIds = GetIds(value); } }
        public virtual IEnumerable<SmokingCondition> SmokingConditions { set { _smokingConditionIds = GetIds(value); } }
        public virtual IEnumerable<TreatmentGoal> TreatmentGoals { set { _treatmentGoalIds = GetIds(value); } }
        public virtual IEnumerable<ClinicalConditionStatus> ClinicalConditionStatuses { set { _clinicalConditionStatusIds = value.Select(i => (int)i).ToArray(); } }

        public virtual IEnumerable<Laterality> Lateralities { set { _lateralityIds = value.Select(i => (int)i).ToArray(); } }
    }

    /// <summary>
    /// Cvx implementation of the ExternalModelMapper.
    /// </summary>
    public class CvxModelMapper : ExternalModelMapper
    {
        private CvxAdministeredModel[] _cvxModels = { };
        private IEnumerable<int> _vaccineIds;

        /// <summary>
        /// Gets a mapping from the given source, to the CvxModels.
        /// The source type must be defined as a public property of this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        public virtual Dictionary<T, CvxAdministeredModel> GetCvxModelMap<T>(IEnumerable<T> source, string externalSystemEntityName) where T : new()
        {
            TryInitializeCvxModels();

            var array = source.Distinct().ToArray();
            var mappings = ExternalSystemService.GetMappings(array, Constants.CvxAdministeredExternalSystemName, externalSystemEntityName);
                                            
            var result = new Dictionary<T, CvxAdministeredModel>();
            foreach (var item in array)
            {
                var externalEntityKey = mappings.First(i => i.PracticeRepositoryEntityKey == GetId(item).ToString()).ExternalSystemEntityKey;
                var externalModel = _cvxModels.First(i => i.CvxCode.Equals(double.Parse(externalEntityKey)));
                result.Add(item, externalModel);
            }
            return result;
        }

        private IEnumerable<double> GetExternalEntityKeys<T>(IEnumerable<int> entityIds, string externalEntityName)
        {
            return GetExternalEntityKeys<T>(entityIds.Select(i => i.ToString()), Constants.CvxAdministeredExternalSystemName, externalEntityName).Select(double.Parse);
        }

        /// <summary>
        /// Initializes the CvxModels using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeCvxModels()
        {
            if (!CanInitialize) return;

            var externalEntityKeys = new List<double>();
            externalEntityKeys.AddRange(GetExternalEntityKeys<Vaccine>(_vaccineIds, Constants.CvxAdministeredVaccineExternalEntityName));

            var existingKeys = _cvxModels.Select(i => i.CvxCode);
            var idsToQuery = externalEntityKeys.Where(i => !existingKeys.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _cvxModels = _cvxModels.Concat(CdaService.GetCvxAdministeredModels(idsToQuery.Distinct()))
                                       .OrderBy(i => i.FullVaccineName) // We order by FullVaccineName because there may be multiple Cvx Rows per Id
                                       .ToArray();
            }

            // Reset
            CanInitialize = false;
        }

        public virtual IEnumerable<Vaccine> Vaccines { set { _vaccineIds = GetIds(value); } }
    }

    /// <summary>
    /// RxNorm implementation of the ExternalModelMapper.
    /// </summary>
    public class RxNormModelMapper : ExternalModelMapper
    {
        private RxNormModel[] _rxNormModels = { };
        private IEnumerable<int> _allergenIds;
        private IEnumerable<int> _medicationIds;

        /// <summary>
        /// Gets a mapping from the given source, to the RxNormModels.
        /// The source type must be defined as a public property of this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        public virtual Dictionary<T, RxNormModel> GetRxNormModelMap<T>(IEnumerable<T> source, string externalSystemEntityName) where T : new()
        {
            TryInitializeRxNormModels();

            var array = source.Distinct().ToArray();
            var mappings = ExternalSystemService.GetMappings(array, Constants.RxNormExternalSystemName, externalSystemEntityName);
            var result = new Dictionary<T, RxNormModel>();
            foreach (var item in array)
            {
                var externalEntityKey = mappings.FirstOrDefault(i => i.PracticeRepositoryEntityKey == GetId(item).ToString()).IfNotNull(x => x.ExternalSystemEntityKey);
                if (externalEntityKey != null)
                {
                    var externalModel = _rxNormModels.FirstOrDefault(i => i.RxCui.Equals(externalEntityKey));
                    if (externalModel != null)
                    {
                        result.Add(item, externalModel);
                    }
                }
            }
            return result;
        }

        private IEnumerable<string> GetExternalEntityKeys<T>(IEnumerable<int> entityIds, string externalEntityName)
        {
            return GetExternalEntityKeys<T>(entityIds.Select(i => i.ToString()), Constants.RxNormExternalSystemName, externalEntityName);
        }

        /// <summary>
        /// Initializes the RxNormModels using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeRxNormModels()
        {
            if (!CanInitialize) return;

            var externalEntityKeys = new List<string>();
            externalEntityKeys.AddRange(GetExternalEntityKeys<Allergen>(_allergenIds, Constants.RxNormAllergenExternalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<Medication>(_medicationIds, Constants.RxNormMedicationsExternalEntityName));

            var existingKeys = _rxNormModels.Select(i => i.RxCui);
            var idsToQuery = externalEntityKeys.Where(i => !existingKeys.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _rxNormModels = _rxNormModels.Concat(CdaService.GetRxNormModels(idsToQuery.Distinct()))
                                             .Distinct()
                                             .ToArray();
            }

            // Reset
            CanInitialize = false;
        }

        public virtual IEnumerable<Allergen> Allergens { set { _allergenIds = GetIds(value); } }
        public virtual IEnumerable<Medication> Medications { set { _medicationIds = GetIds(value); } }
    }

    /// <summary>
    /// Loinc implementation of the ExternalModelMapper.
    /// </summary>
    public class LoincModelMapper : ExternalModelMapper
    {
        private LoincModel[] _loincModels = { };
        private IEnumerable<int> _laboratoryTestIds;

        /// <summary>
        /// Gets a mapping from the given source, to the LoincModels.
        /// The source type must be defined as a public property of this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        public virtual Dictionary<T, LoincModel> GetLoincModelMap<T>(IEnumerable<T> source, string externalSystemEntityName) where T : new()
        {
            TryInitializeLoincModels();

            var array = source.Distinct().ToArray();
            var mappings = ExternalSystemService.GetMappings(array, Constants.LoincExternalSystemName, externalSystemEntityName);

            var result = new Dictionary<T, LoincModel>();
            foreach (var item in array)
            {
                var externalEntityKey = mappings.FirstOrDefault(i => i.PracticeRepositoryEntityKey == GetId(item).ToString()).IfNotNull(x => x.ExternalSystemEntityKey);
                if (externalEntityKey != null)
                {
                    var externalModel = _loincModels.FirstOrDefault(i => i.LoincNum.Equals(externalEntityKey));
                    if (externalModel != null)
                    {
                        result.Add(item, externalModel);
                    }
                }

            }
            return result;
        }

        private IEnumerable<string> GetExternalEntityKeys<T>(IEnumerable<int> entityIds, string externalEntityName)
        {
            return GetExternalEntityKeys<T>(entityIds.Select(i => i.ToString()), Constants.LoincExternalSystemName, externalEntityName);
        }

        /// <summary>
        /// Initializes the LoincModels using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeLoincModels()
        {
            if (!CanInitialize) return;

            var externalEntityKeys = new List<string>();
            externalEntityKeys.AddRange(GetExternalEntityKeys<LaboratoryTest>(_laboratoryTestIds, Constants.LoincLaboratoryTestExternalEntityName));

            var existingKeys = _loincModels.Select(i => i.LoincNum);
            var idsToQuery = externalEntityKeys.Where(i => !existingKeys.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _loincModels = _loincModels.Concat(CdaService.GetLoincModels(idsToQuery.Distinct()))
                                           .OrderBy(i => i.Component) // We order by Component because there may be multiple Loinc Rows per Id
                                           .ToArray();
            }

            // Reset
            CanInitialize = false;
        }

        public virtual IEnumerable<LaboratoryTest> LaboratoryTests { set { _laboratoryTestIds = GetIds(value); } }
    }

    /// <summary>
    /// A mapper
    /// </summary>
    public class QualityDataModelValueSetModelMapper : ExternalModelMapper
    {
        private QualityDataModelValueSetEntryModel[] _entryModels;
        private IEnumerable<QualityDataModelValueSet> _valueSets;
        public virtual IEnumerable<QualityDataModelValueSetEntryModel> GetQualityDataModelValueSetEntryModels(params QualityDataModelValueSet[] valueSets)
        {
            TryInitializeModels();

            var oids = valueSets.Select(v => v.Oid()).ToArray();
            return _entryModels.Where(e => !valueSets.Any() || oids.Contains(e.ValueSetOid))
                .Select(externalModel => new QualityDataModelValueSetEntryModel
                                         {
                                             Code = externalModel.Code,
                                             CodeSystemOid = externalModel.CodeSystemOid,
                                             Description = externalModel.Description,
                                             Id = externalModel.Id,
                                             ValueSetOid = externalModel.ValueSetOid,
                                             ValueSet = valueSets.FirstOrDefault(v => v.Oid() == externalModel.ValueSetOid)
                                         })
                .ToArray();
        }

        public virtual QualityDataModelValueSetEntryModel GetQualityDataModelValueSetEntryModel(object source, params QualityDataModelValueSet[] valueSets)
        {
            if (source == null) return null;
            return GetQualityDataModelValueSetEntryModelMap(new[] { source }, valueSets).Select(i => i.Value).FirstOrDefault();
        }

        /// <summary>
        /// Gets a mapping from the given source, to the QualityDataModelValueSetModel.
        /// The source type must be defined as a public property of this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="valueSets">The value sets.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual Dictionary<T, QualityDataModelValueSetEntryModel> GetQualityDataModelValueSetEntryModelMap<T>(IEnumerable<T> source, params QualityDataModelValueSet[] valueSets)
        {
            TryInitializeModels();

            var array = source.Distinct().ToArray();

            var mappings = new ConcurrentBag<ExternalSystemEntityMapping>();
            valueSets.ForAllInParallel(valueSet =>
            {
                var foundMappings = ExternalSystemService.GetMappings(array, Constants.QualityDataModelExternalSystemName, valueSet.ToString());
                foundMappings.ForEachWithSinglePropertyChangedNotification(mappings.Add);
            });

            var result = new Dictionary<T, QualityDataModelValueSetEntryModel>();
            foreach (T item in array)
            {
                var externalEntityKey = mappings
                    .FirstOrDefault(i => i.PracticeRepositoryEntityKey == GetId(item).ToString())
                    .IfNotNull(m => m.ExternalSystemEntityKey);

                if (externalEntityKey == null) continue;

                var externalModel = _entryModels.FirstOrDefault(m => m.Id.ToString().Equals(externalEntityKey));
                if (externalModel != null)
                    result.Add(item, new QualityDataModelValueSetEntryModel
                                     {
                                         Code = externalModel.Code,
                                         CodeSystemOid = externalModel.CodeSystemOid,
                                         Description = externalModel.Description,
                                         Id = externalModel.Id,
                                         ValueSetOid = externalModel.ValueSetOid,
                                         ValueSet = valueSets.FirstOrDefault(v => v.Oid() == externalModel.ValueSetOid)
                                     });
            }
            return result;
        }

        /// <summary>
        /// Initializes the model using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeModels()
        {
            if (!CanInitialize) return;

            _entryModels = CdaService.GetQualityDataModelValueSetModels(_valueSets.IsNotNullOrEmpty() ? _valueSets : Enums.GetValues<QualityDataModelValueSet>()).ToArray();

            CanInitialize = false;
        }

        public virtual IEnumerable<QualityDataModelValueSet> ValueSets { set { _valueSets = value; } }
    }

    /// <summary>
    /// Cdc implementation of the ExternalModelMapper.
    /// </summary>
    public class CdcModelMapper : ExternalModelMapper
    {
        private CdcRaceModel[] _cdcRaceModels = { };
        private CdcEthnicityModel[] _cdcEthnicityModels = { };

        private IEnumerable<int> _patientRaceIds;
        private IEnumerable<int> _patientEthnicityIds;

        /// <summary>
        /// Gets a mapping from the given source, to the CdcRaceAndEthnicityModel.        
        /// </summary>                
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="externalSystemEntityName"></param>
        /// <returns></returns>
        public virtual Dictionary<long, CdcRaceAndEthnicityModel> GetCdcRaceAndEthnicityModelMap<TSource>(IEnumerable<TSource> source, string externalSystemEntityName) where TSource : new()
        {
            TryInitializeCdcModels();

            var array = source.Distinct().ToArray();

            var mappings = ExternalSystemService.GetMappings(array, Constants.CdcExternalSystemName, externalSystemEntityName).ToArray();

            var result = new Dictionary<long, CdcRaceAndEthnicityModel>();
            foreach (var item in array)
            {
                var externalEntityKey = mappings.FirstOrDefault(i => i.PracticeRepositoryEntityKey == GetId(item).ToString()).IfNotNull(m => m.ExternalSystemEntityKey);
                if (externalEntityKey == null) continue;

                if (item is Race)
                {
                    var externalModel = _cdcRaceModels.FirstOrDefault(race => race.Id.Equals(int.Parse(externalEntityKey)));
                    if (externalModel != null) result.Add(GetId(item), externalModel);
                }
                else if (item is Ethnicity)
                {
                    var externalModel = _cdcEthnicityModels.FirstOrDefault(race => race.Id.Equals(int.Parse(externalEntityKey)));
                    if (externalModel != null) result.Add(GetId(item), externalModel);
                }

            }
            return result;
        }

        private IEnumerable<int> GetExternalEntityKeys<T>(IEnumerable<int> entityIds, string externalEntityName)
        {
            return GetExternalEntityKeys<T>(entityIds.Select(i => i.ToString()), Constants.CdcExternalSystemName, externalEntityName).Select(int.Parse);
        }

        /// <summary>
        /// Initializes the CdcModels using the values set in this instance's public properties.
        /// </summary>
        /// <remarks>
        /// Uses the values from this instances public properties to get ExternalSystemMappings, then ExternalSystemEntityKeys, then values from external database.
        /// </remarks>
        private void TryInitializeCdcModels()
        {
            if (!CanInitialize) return;

            var externalEntityKeys = new List<int>();
            externalEntityKeys.AddRange(GetExternalEntityKeys<Race>(_patientRaceIds, Constants.CdcRaceAndEthnicityExtenalEntityName));
            externalEntityKeys.AddRange(GetExternalEntityKeys<Ethnicity>(_patientEthnicityIds, Constants.CdcRaceAndEthnicityExtenalEntityName));

            var existingKeys = _cdcRaceModels.Select(i => i.Id).ToList();
            var idsToQuery = externalEntityKeys.Where(i => !existingKeys.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _cdcRaceModels = _cdcRaceModels.Concat(CdaService.GetCdcRaceModels(idsToQuery.Distinct().Select(i => i.ToString())))
                                       .OrderBy(i => i.Name)
                                       .ToArray();
            }

            var existingKeys2 = _cdcEthnicityModels.Select(i => i.Id).ToList();
            idsToQuery = externalEntityKeys.Where(i => !existingKeys2.Contains(i)).ToArray();
            if (idsToQuery.Any())
            {
                _cdcEthnicityModels = _cdcEthnicityModels.Concat(CdaService.GetCdcEthnicityModels(idsToQuery.Distinct().Select(i => i.ToString())))
                                       .OrderBy(i => i.Name)
                                       .ToArray();
            }

            // Reset
            CanInitialize = false;
        }

        public virtual IEnumerable<Race> PatientRaces { set { _patientRaceIds = GetIds(value); } }

        public virtual IEnumerable<Ethnicity> PatientEthnicities { set { _patientEthnicityIds = GetIds(value); } }
    }
}
