﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Cda;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.EntityLink;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Encounter = IO.Practiceware.Model.Encounter;
using QuestionAnswer = IO.Practiceware.Model.QuestionAnswer;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    ///     Produces QRDA Category I messages
    /// </summary>
    public class QrdaCategory1MessageProducer
    {
        private readonly CdaModelMapper _cdaModelMapper;
        private readonly CdaMessageProducerUtilities _cdaUtilities;
        private readonly IDocumentGenerationService _documentGenerationService;
        private readonly IEntityLinkService _entityLinkService;
        private readonly Func<QrdaCategory1Model> _createQrdaCategory1Model;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IPracticeRepository _practiceRepository;

        public QrdaCategory1MessageProducer(IPracticeRepository practiceRepository,
            CdaMessageProducerUtilities cdaUtilities,
            CdaModelMapper cdaModelMapper,
            IDocumentGenerationService documentGenerationService,
            IEntityLinkService entityLinkService,
            Func<QrdaCategory1Model> createQrdaCategory1Model,
            IUnitOfWorkProvider unitOfWorkProvider)
        {
            _practiceRepository = practiceRepository;
            _cdaUtilities = cdaUtilities;
            _cdaModelMapper = cdaModelMapper;
            _documentGenerationService = documentGenerationService;
            _entityLinkService = entityLinkService;
            _createQrdaCategory1Model = createQrdaCategory1Model;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        /// <summary>
        /// Creates a single message for all measures
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="electronicClinicalQualityMeasures">The electronic clinical quality measures.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="measurementPeriodStartDate">The measurement period start date.</param>
        /// <param name="measurementPeriodEndDate">The measurement period end date.</param>
        /// <returns></returns>
        public IMessage ProduceMessage(int patientId, IEnumerable<ElectronicClinicalQualityMeasure> electronicClinicalQualityMeasures, int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate)
        {
            QrdaCategory1Model model = null;

            var queries = new QrdaCategory1Queries(measurementPeriodStartDate, measurementPeriodEndDate, userId, _practiceRepository, _cdaModelMapper);

            foreach (var electronicClinicalQualityMeasure in electronicClinicalQualityMeasures)
            {
                if (!FindPatients(electronicClinicalQualityMeasure, queries, userId).Contains(patientId)) continue;

                var newModel = ProduceMessageModel(patientId, electronicClinicalQualityMeasure, userId, measurementPeriodStartDate, measurementPeriodEndDate, queries);

                if (model == null) model = newModel;
                else model.MergeEntries(newModel);
            }

            return CreateMessageFromModel(patientId, null, model);
        }

        public IEnumerable<IMessage> ProduceMessages(IEnumerable<ElectronicClinicalQualityMeasure> electronicClinicalQualityMeasures, IEnumerable<int> userIds, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate)
        {
            var results = new List<IMessage>();

            var queries = new QrdaCategory1Queries(measurementPeriodStartDate, measurementPeriodEndDate, 0, _practiceRepository, _cdaModelMapper);

            foreach (var userId in userIds)
            {
                queries.UserId = userId;
                foreach (var electronicClinicalQualityMeasure in electronicClinicalQualityMeasures)
                {
                    var patientIds = FindPatients(electronicClinicalQualityMeasure, queries, userId);
                    foreach (var patientId in patientIds)
                    {
                        results.Add(ProduceMessage(patientId, electronicClinicalQualityMeasure, userId, measurementPeriodStartDate, measurementPeriodEndDate, queries));
                    }
                }
            }

            return results;
        }

        public IMessage ProduceMessage(int patientId, ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate)
        {
            return ProduceMessage(patientId, electronicClinicalQualityMeasure, userId, measurementPeriodStartDate, measurementPeriodEndDate, null);
        }

        private IMessage ProduceMessage(int patientId, ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate, QrdaCategory1Queries queries)
        {
            var model = ProduceMessageModel(patientId, electronicClinicalQualityMeasure, userId, measurementPeriodStartDate, measurementPeriodEndDate, queries);

            return CreateMessageFromModel(patientId, electronicClinicalQualityMeasure, model);
        }

        private IMessage CreateMessageFromModel(int patientId, ElectronicClinicalQualityMeasure? electronicClinicalQualityMeasure, QrdaCategory1Model model)
        {
            TemplateDocument template = _practiceRepository.TemplateDocuments.Single(td => td.Name == "QrdaCategory1.Main.cshtml");

            string result = _documentGenerationService.CompileRazorAndRun(template.GetContent() as string, model);
            result = result.CleanFromBom(); // Cypress doesn't like BOM within file contents

            var message = new CdaMessage(model.UniqueIdentifier) { PatientId = patientId.ToString(), MessageType = electronicClinicalQualityMeasure == null ? string.Empty : electronicClinicalQualityMeasure.ToString() };
            message.Load(result);

            return message;
        }

        private QrdaCategory1Model ProduceMessageModel(int patientId, ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate, QrdaCategory1Queries queries)
        {
            if (queries == null)
            {
                queries = new QrdaCategory1Queries(measurementPeriodStartDate, measurementPeriodEndDate, userId, _practiceRepository, _cdaModelMapper);
            }

            Patient patient;
            User user;
            BillingOrganization billingOrganization;
            using (_unitOfWorkProvider.Create()) // Allow entity resolution from UoW cache
            {
                var encounters = _practiceRepository.Encounters.Where(e => e.PatientId == patientId && queries.IsEncounterForUser.Invoke(e)).OrderBy(i => i.StartDateTime).ToArray();

                if (!encounters.Any())
                {
                    encounters = _practiceRepository.Encounters.Where(e => e.PatientId == patientId).OrderBy(i => i.StartDateTime).ToArray();
                }

                _cdaUtilities.LoadForCdaMessageProduction(encounters);

                patient = encounters.Select(e => e.Patient).FirstOrDefault() ?? _practiceRepository.Patients.WithId(patientId);

                _cdaUtilities.LoadForCdaMessageProduction(patient);

                CdaMessageProducerUtilities.InitializeCdaModelMapper(_cdaModelMapper, patient, patient.Encounters.ToArray());

                user = _practiceRepository.Users
                    .Include(u => u.Providers.SelectMany(p => p.IdentifierCodes))
                    .Include(u => u.UserAddresses.Select(a => new { a.UserAddressType, a.StateOrProvince.Country }))
                    .Include(u => u.UserPhoneNumbers.Select(pn => new { pn.UserPhoneNumberType }))
                    .WithId(userId);

                billingOrganization = _practiceRepository.BillingOrganizations
                    .Include(b => b.IdentifierCodes)
                    .Include(b => b.BillingOrganizationAddresses.Select(a => new { a.BillingOrganizationAddressType, a.StateOrProvince.Country }))
                    .Include(b => b.BillingOrganizationPhoneNumbers.Select(pn => new { pn.BillingOrganizationPhoneNumberType }))
                    .First(o => o.IsMain);
            }

            var model = _createQrdaCategory1Model();

            model.Patient = patient;
            model.ElectronicClinicalQualityMeasure = electronicClinicalQualityMeasure;
            model.GenerationDateTime = DateTime.Now.ToClientTime();
            model.MainBillingOrganization = billingOrganization;
            model.MeasurementPeriodStartDate = measurementPeriodStartDate;
            model.MeasurementPeriodEndDate = measurementPeriodEndDate;
            model.UniqueIdentifier = Guid.NewGuid();
            model.User = user;
            model.Mapper = _cdaModelMapper;

            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop1)
                new BmiModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, user, true).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop2)
                new BmiModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, user, false).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.TobaccoScreening)
                new TobaccoScreeningModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabetesEyeExam)
                new DiabetesEyeExamModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyDocumentation)
                new DiabeticRetinopathyDocumentationModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyCommunication)
                new DiabeticRetinopathyCommunicationModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.Poag)
                new PoagModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DocumentationOfMedications)
                new DocumentationOfMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications1)
                new HighRiskMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, false).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications2)
                new HighRiskMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, true).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.ControllingHighBloodPressure)
                new ControllingHighBloodPressureModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CloseTheReferralLoop)
                new CloseTheReferralLoopModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractComplications)
                new CataractSurgeryComplicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractSurgeryVisualAcuity)
                new CataractSurgeryVisualAcuityModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FillModel(model);
            return model;
        }

        public IEnumerable<int> FindPatients(ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime measurementPeriodStartDate, DateTime measurementPeriodEndDate)
        {
            var queries = new QrdaCategory1Queries(measurementPeriodStartDate, measurementPeriodEndDate, userId, _practiceRepository, _cdaModelMapper);

            return FindPatients(electronicClinicalQualityMeasure, queries, userId);
        }

        private int[] FindPatients(ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, QrdaCategory1Queries queries, int userId)
        {
            var user = _practiceRepository.Users.WithId(userId);

            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop1)
                return new BmiModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, user, true).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop2)
                return new BmiModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, user, false).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.TobaccoScreening)
                return new TobaccoScreeningModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabetesEyeExam)
                return new DiabetesEyeExamModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyDocumentation)
                return new DiabeticRetinopathyDocumentationModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyCommunication)
                return new DiabeticRetinopathyCommunicationModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.Poag)
                return new PoagModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DocumentationOfMedications)
                return new DocumentationOfMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications1)
                return new HighRiskMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, false).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications2)
                return new HighRiskMedicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService, true).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.ControllingHighBloodPressure)
                return new ControllingHighBloodPressureModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CloseTheReferralLoop)
                return new CloseTheReferralLoopModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractComplications)
                return new CataractSurgeryComplicationsModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();
            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractSurgeryVisualAcuity)
                return new CataractSurgeryVisualAcuityModelProducer(_practiceRepository, queries, _cdaModelMapper, _entityLinkService).FindPatients();


            return null;
        }

        /// <summary>
        /// Body Mass Index (BMI) measure has two Initial Patient Populations (IPP):  Patients with birthdate
        /// a) 65 years and older before Measurement Period Start Date and
        /// b) 18-64 years old before Measurement Period Start Date
        /// All patients had a discharged encounter during the Measurement Period mapped to the BMI Encounter Code Set
        /// Patients will be excluded from the IPP if the BMI was not performed for a medical or patient reason.
        /// 
        /// TODO: AND NOT: finding whether BMI wasn't measured because Patient refused or for a Medical Reason,
        /// </summary>
        private class BmiModelProducer : ModelProducer
        {
            private readonly User _user;
            private readonly Expression<Func<Encounter, IEnumerable<BillingService>>> _billingServicesFilter;
            private readonly bool _belowAge65;

            public BmiModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService, User user, bool belowAge65)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
                _user = user;
                _billingServicesFilter = e => e.Invoices.SelectMany(i => i.BillingServices).Where(bs =>
                    Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e)
                    && !e.EncounterProcedureOrders.Any(epo => Queries.IsClinicalProcedureMappedToValueSets(QualityDataModelValueSet.ProcedureOrderPalliativeCare).Invoke(epo.ProcedureOrder.ClinicalProcedure))
                    && Queries.IsEncounterServicesMappedToValueSets(QualityDataModelValueSet.EncounterPerformedBmiEncounterCodeSet).Invoke(bs.EncounterService));
                _belowAge65 = belowAge65;
            }

            public override int[] FindPatients()
            {
                int[] result;

                if (_belowAge65)
                    result= PracticeRepository.Patients
                        .Where(p => Queries.HasAgeRangeAtMeasurementPeriodStartDateExclusiveMaxAge.Invoke(p, 18, 64))
                        .Where(p => p.Encounters.SelectMany(e => _billingServicesFilter.Invoke(e)).Any())
                        .Select(p => p.Id).ToArray();
                else
                    result = PracticeRepository.Patients
                        .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 65))
                    .Where(p => p.Encounters.SelectMany(e => _billingServicesFilter.Invoke(e)).Any())
                    .Select(p => p.Id).ToArray();

                return result;
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                var billingServices = model.Patient.Encounters
                    .SelectMany(e => _billingServicesFilter.Invoke(e)).ToArray();

                model.EncounterActivitiesEncounterPerformedEntryModels =
                    billingServices
                    .Select(bs => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(bs, QualityDataModelValueSet.EncounterPerformedBmiEncounterCodeSet)).ToArray();

                //Get patient's diagnoses of pregnancy that start before MeasurementPeriod end date and do not end before the MeasurementPeriod start date.  This will be a denominator Exclusion.
                model.ProblemObservationDiagnosisActiveEntryModels = model.Patient.PatientDiagnoses
                    .Select(pd => pd.LatestPatientDiagnosisDetail)
                    .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActivePregnancyDx))
                    .Where(m => m.ValueSetEntry != null)
                    .ToArray();

                //Get the patient's body mass index (BMI) measurements documented within 6 months before the start date of any of the above specified encounters.
                model.ResultObservationPhysicalExamFindingEntryModels =
                    model.Patient.PatientHeightAndWeights
                        .Where(haw => billingServices.Any(bs => haw.MeasuredDateTime >= bs.Invoice.Encounter.StartDateTime.AddMonths(-6) && haw.MeasuredDateTime <= bs.Invoice.Encounter.EndDateTime))
                        .Where(haw => haw.HeightUnit != null && haw.WeightUnit != null)
                        .Select(haw => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
                                       {
                                           Start = haw.MeasuredDateTime,
                                           End = haw.MeasuredDateTime,
                                           RootAndExtension = haw.GetOidRootAndExtension(QualityDataModelValueSet.PhysicalExamFindingBmiLoincValue.ToString()),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.PhysicalExamFindingBmiLoincValue).FirstOrDefault(),
                                           Laterality = null,
                                           ResultValueSetEntry = null,
                                           ResultValue = haw.WeightUnitInKilograms.GetBodyMassIndex(haw.HeightUnitInMeters).ToString(),
                                           ResultUnitOfMeasurement = "kg/m2"
                                       }).ToArray();

                //Get any of the patient's intervention orders (EncounterTreatmentGoalAndInstructionOrder) entered within 6 months before the start date of the above specified encounters 
                //where the TreatmentGoal is mapped to the specified value set.
                model.PlanOfCareActivityActInterventionOrderEntryModels =
                    model.Patient.Encounters.SelectMany(e => e.EncounterTreatmentGoalAndInstructions)
                        .Where(gai => billingServices.Any(bs => gai.Encounter.StartDateTime >= bs.Invoice.Encounter.StartDateTime.AddMonths(-6) && gai.Encounter.StartDateTime <= bs.Invoice.Encounter.EndDateTime))
                        .Select(gai => new PlanOfCareActivityActInterventionOrderEntryModel
                                       {
                                           Start = gai.Encounter.StartDateTime,
                                           End = gai.Encounter.EndDateTime,
                                           RootAndExtension = gai.GetOidRootAndExtension(new[] { QualityDataModelValueSet.InterventionOrderAboveNormalFollowup, QualityDataModelValueSet.InterventionOrderBelowNormalFollowUp }.Join(".")),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(gai.TreatmentGoalAndInstruction.TreatmentGoal, QualityDataModelValueSet.InterventionOrderAboveNormalFollowup, QualityDataModelValueSet.InterventionOrderBelowNormalFollowUp),
                                           AuthorRootAndExtension = _user.GetOidRootAndExtension()
                                       })
                        .Where(m => m.ValueSetEntry != null).ToArray();

                //Get any of the patient's medication orders entered within 6 months before the start date of the above specified encounters
                //where the Medication is mapped to the MedicationOrderAboveNormalMedications or BelowNormalMedications value setes.
                model.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels =
                    model.Patient.Encounters.SelectMany(e => e.EncounterMedicationOrders)
                        .Where(gai => billingServices.Any(bs => gai.Encounter.StartDateTime >= bs.Invoice.Encounter.StartDateTime.AddMonths(-6) && gai.Encounter.StartDateTime <= bs.Invoice.Encounter.EndDateTime))
                        .Select(emo => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(emo, QualityDataModelValueSet.MedicationOrderAboveNormalMedications, QualityDataModelValueSet.MedicationOrderBelowNormalMedications))
                        .Where(m => m.ValueSetEntry != null).ToArray();

            }
        }

        /// Tobacco Use: Screening and Cessation Intervention measure gets patients aged 18 years or older before Measurement Period Start Date
        /// who, during the Measurement Period, had either
        /// a) one or more discharged encounters performed by the specified user with billing services mapped to the PreventiveCare ValueSet group or
        /// b) two or more discharged encounters performed by the specified user with billing services mapped to the PsychCare ValueSet group.
        private class TobaccoScreeningModelProducer : ModelProducer
        {
            private readonly QualityDataModelValueSet[] _preventiveCareValueSets;
            private readonly QualityDataModelValueSet[] _psychCareValueSets;

            public TobaccoScreeningModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
                _preventiveCareValueSets = QualityDataModelValueSetGroup.PreventiveCareValueSets;

                _psychCareValueSets = QualityDataModelValueSetGroup.PsychCareValueSets;

            }


            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p =>
                        p.Encounters.Count(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(_preventiveCareValueSets).Invoke(e)) >= 1
                        ||
                        p.Encounters.Count(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(_psychCareValueSets).Invoke(e)) >= 2
                    )
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                var preventiveServicesEncounters = model.Patient.Encounters.Where(e =>
                    Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(_preventiveCareValueSets).Invoke(e)).ToArray();

                var psychCareEncounters = model.Patient.Encounters.Where(e =>
                    Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(_psychCareValueSets).Invoke(e)).ToArray();

                if (psychCareEncounters.Count() < 2)
                {
                    psychCareEncounters = new Encounter[0];
                }

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period with billing services mapped to PreventiveServices or PsychCare value set groups.
                model.EncounterActivitiesEncounterPerformedEntryModels =
                    preventiveServicesEncounters
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Where(bs => Queries.IsEncounterServicesMappedToValueSets(_preventiveCareValueSets).Invoke(bs.EncounterService))
                        .Select(bs => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(bs, _preventiveCareValueSets))
                        .Union(
                            psychCareEncounters
                                .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                                .Where(bs => Queries.IsEncounterServicesMappedToValueSets(_psychCareValueSets).Invoke(bs.EncounterService))
                                .Select(bs => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(bs, _psychCareValueSets))
                        );

                //Get the patient's smoking statuses documented within 24 months before the Measurement Period end date.
                model.TobaccoUseEntryModels =
                    model.Patient.PatientSmokingStatus
                        .Where(s => s.EnteredDateTime >= model.MeasurementPeriodEndDate.AddMonths(-24))
                        .Select(s => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
                                     {
                                         RootAndExtension = s.GetOidRootAndExtension(new[] { QualityDataModelValueSet.PatientCharacteristicTobaccoUser, QualityDataModelValueSet.PatientCharacteristicTobaccoNonuser }.Join(".")),
                                         Start = s.SmokingStartDateTime ?? s.Encounter.StartDateTime,
                                         End = s.SmokingEndDateTime,
                                         ResultValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(
                                            s.SmokingCondition, QualityDataModelValueSet.PatientCharacteristicTobaccoUser, QualityDataModelValueSet.PatientCharacteristicTobaccoNonuser)
                                     }
                        )
                        .Where(m => m.ResultValueSetEntry != null)
                        .ToArray();

                // Get the patient's medication orders where the order date was within 24 months before the Measurement Period end date and the medication maps to the TobaccoUseCessationPharmacotherapy value set.
                model.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels =
                    model.Patient.Encounters
                        .Where(e => e.StartDateTime >= model.MeasurementPeriodEndDate.AddMonths(-24))
                        .SelectMany(e => e.EncounterMedicationOrders)
                        .Where(o => o != null)
                        .Where(o => o.PatientMedicationDetail != null)
                        .Select(o => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(o.PatientMedicationDetail, QualityDataModelValueSet.MedicationOrderTobaccoUseCessationPharmacotherapy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                // Render this template only the tobacco use risk assessment was NOT DONE for a Medical Reason during the Measurement Period.  Use the entryRelationship section with "RSON".
                model.AssessmentScaleObservationRiskCategoryAssessmentEntryModels =
                    model.Patient.PatientSmokingStatus.Where(s => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(s.Encounter))
                        .Select(s => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.RiskCategoryAssessmentTobaccoUseScreening).FirstOrDefault(),
                                         NotDoneValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(s.SmokingCondition, QualityDataModelValueSet.RiskCategoryAssessmentnotdoneMedicalReason),
                                         Start = s.Encounter.StartDateTime,
                                         End = s.Encounter.EndDateTime,
                                         RootAndExtension = s.GetOidRootAndExtension(QualityDataModelValueSet.RiskCategoryAssessmentnotdoneMedicalReason.ToString())
                                     }
                        )
                        .Where(m => m.NotDoneValueSetEntry != null)
                        .ToArray();

                // Get the patient's active diagnoses that started before the Measurement Period end date and did not end before the Measurement Period start date, 
                // with ClinicalConditions mapped to LimitedLifeExpectancy value Set 
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd != null)
                        .Where(pdd => pdd.StartDateTime <= model.MeasurementPeriodEndDate && (pdd.EndDateTime > model.MeasurementPeriodStartDate || pdd.EndDateTime == null))
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveLimitedLifeExpectancy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get the patient's active medications where the patient start date was within 24 months before the Measurement Period end date and the medication maps to the TobaccoUseCessationPharmacotherapy value set.
                model.MedicationActivityMedicationActiveEntryModels =
                    model.Patient.PatientMedications.SelectMany(pm => pm.PatientMedicationDetails)
                        .Where(pmd => pmd.MedicationEventType == MedicationEventType.PatientStarted
                                      && pmd.MedicationEventDateTime >= model.MeasurementPeriodEndDate.AddMonths(-24) && pmd.MedicationEventDateTime < model.MeasurementPeriodEndDate)
                        .Select(pmd => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(pmd, QualityDataModelValueSet.MedicationActiveTobaccoUseCessationPharmacotherapy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                // Get the patient's InterventionPerformeds when the LinkedQuestionAnswerValue maps to TobaccoUseCessationCounseling.
                model.ProcedureActivityActInterventionPerformedEntryModels =
                    model.Patient.PatientInterventionPerformeds
                        .Where(p => p.PerformedDateTime >= model.MeasurementPeriodEndDate.AddMonths(-24) && p.PerformedDateTime < model.MeasurementPeriodEndDate)
                        .Select(i => new QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
                                     {
                                         ValueSetEntry = GetLinkedAnswerMappedValueSetEntry(i.PatientInterventionPerformedQuestionAnswers, QualityDataModelValueSet.InterventionPerformedTobaccoUseCessationCounseling),
                                         Start = i.Encounter.StartDateTime,
                                         End = i.Encounter.EndDateTime,
                                         RootAndExtension = i.GetOidRootAndExtension(QualityDataModelValueSet.InterventionPerformedTobaccoUseCessationCounseling.ToString())
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();
            }
        }


        /// <summary>
        /// Diabetes Eye Exam gets patients aged 18-74 years (inclusive) before the Measurement Period Start Date
        /// who had a discharged encounter performed by the specified user during the Measurement Period with billing services mapped to the RegularExam ValueSet group
        /// and who had an active diagnosis of Diabetes that started before or during the Measurement Period and did not end before the Measurement Period Start Date.
        /// </summary>
        private class DiabetesEyeExamModelProducer : ModelProducer
        {
            public DiabetesEyeExamModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Encounters.Where(Queries.HasEncounterServicesMappedToValueSets(QualityDataModelValueSetGroup.RegularExamValueSets))
                    .Where(Queries.IsDischargedEncounterInMeasurementPeriodForUser)
                    .Where(e => Queries.HasAgeRangeAtMeasurementPeriodStartDateInclusiveMaxAge.Invoke(e.Patient, 18, 75))
                    .Where(e => e.Patient.PatientDiagnoses.Any(d => Queries.IsDiagnosisInMeasurementPeriodMappedToValueSets(QualityDataModelValueSet.DiagnosisActiveDiabetes).Invoke(d)))
                    .Select(e => e.PatientId).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period with billing services mapped to the RegularExam value set group.
                model.EncounterActivitiesEncounterPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.RegularExamValueSets))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get the patient's active diagnoses of Diabetes that started before the Measurement Period end date and did not end before the Measurement Period start date.  Gestational diabetes will be a denominator exclusion.
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime <= model.MeasurementPeriodEndDate && (pdd.EndDateTime == null || pdd.EndDateTime > model.MeasurementPeriodStartDate))
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveDiabetes, QualityDataModelValueSet.DiagnosisActiveGestationalDiabetes))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get all the patient's diagnostic tests performed during the Measurement Period with ClinicalProcedures mapped to the PhysicalExamPeformedRetinalOrDilatedEyeExam value set.  Exam results are not included in the template.
                model.ProcedureActivityObservationPhysicalExamPerformedEntryModels =
                    model.Patient.PatientDiagnosticTestPerformeds
                        .Where(pep => pep.PerformedDateTime >= model.MeasurementPeriodStartDate && pep.PerformedDateTime <= model.MeasurementPeriodEndDate)
                        .Select(pep => new QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
                                       {
                                           Start = pep.PerformedDateTime,
                                           End = pep.PerformedDateTime,
                                           RootAndExtension = pep.GetOidRootAndExtension(QualityDataModelValueSet.PhysicalExamPerformedRetinalOrDilatedEyeExam.ToString()),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pep.ClinicalProcedure, QualityDataModelValueSet.PhysicalExamPerformedRetinalOrDilatedEyeExam)
                                       })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                // Get the results of the physical exam RetinalOrDilatedEyeExam performed within 12 months before the Measurement Period start date.  Must report the attribute Negative Finding for Diabetic Retinopathy.
                // TODO: fix criteria 
                model.ResultObservationPhysicalExamFindingEntryModels =
                    model.Patient.PatientExamPerformeds
                        .Where(pep => pep.PerformedDateTime >= model.MeasurementPeriodStartDate.AddMonths(-12) && pep.PerformedDateTime < model.MeasurementPeriodStartDate)
                        .Select(pep => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
                                       {
                                           Start = pep.PerformedDateTime,
                                           End = pep.PerformedDateTime,
                                           RootAndExtension = pep.GetOidRootAndExtension(QualityDataModelValueSet.PhysicalExamFindingRetinalOrDilatedEyeExam.ToString()),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pep.ClinicalProcedure, QualityDataModelValueSet.PhysicalExamFindingRetinalOrDilatedEyeExam),
                                           Laterality = null,
                                           ResultValue = null,
                                           ResultUnitOfMeasurement = null,
                                           ResultValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.AttributeResultNegativeFinding).FirstOrDefault(),
                                       })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();
            }
        }

        /// <summary>
        /// Diabetic Retinopathy Documentation measure gets patients aged 18 or older before the Measurement Period Start Date
        /// who had a discharged encounter performed by the specified user with billing services mapped to the EyeExam ValueSet group during the Measurement Period 
        /// and have an active diagnosis of Diabetic Retinopathy starting on or before the (most recent) Measurement Period EyeExam encounter
        /// and who had at least 2 discharged encounters from the AnyInteraction ValueSet group during the Measurement Period.
        /// </summary>
        private class DiabeticRetinopathyDocumentationModelProducer : ModelProducer
        {
            public DiabeticRetinopathyDocumentationModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p => p.Encounters.Any(e =>
                        Queries.HasValueSetsDuringMeasurementPeriod(QualityDataModelValueSetGroup.EyeExamValueSets).Invoke(e)
                        && Queries.HasDiagnosisBefore(QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy).Invoke(e.Patient, e.EndDateTime)
                        ))
                    .Where(p => p.Encounters.Count(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(QualityDataModelValueSetGroup.AnyInteractionValueSets).Invoke(e)) >= 2)
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period with billing services mapped to the EyeExam group of value sets.
                var encounterActivitiesEncounterPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => new { Encounter = e, Entry = ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.EyeExamValueSets) })
                        .Where(e => e.Entry.ValueSetEntry != null)
                        .ToArray();

                model.EncounterActivitiesEncounterPerformedEntryModels = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Entry).ToArray();

                var lastEncounterStart = model.EncounterActivitiesEncounterPerformedEntryModels.OrderByDescending(m => m.Start).Select(m => m.Start).First();

                //Get the patient's active diagnoses of Diabetic Retinopathy that started on or before the above Measurement Period EyeExams.
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime <= lastEncounterStart)
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //This template is rendered only when the macular exam is NOT DONE for a patient or medical reason.  The template will include an entryRelationship section using "RSON".
                model.ProcedureActivityObservationDiagnosticStudyPerformedEntryModels =
                    model.Patient.PatientExamPerformeds
                        .Where(pep => encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Encounter.Invoice.EncounterId).Contains(pep.EncounterId))
                        .Select(pep => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                       {
                                           Start = pep.Encounter.StartDateTime,
                                           End = pep.Encounter.EndDateTime,
                                           RootAndExtension = pep.GetOidRootAndExtension(QualityDataModelValueSet.DiagnosticStudyPerformedMacularExam.ToString()),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pep.ClinicalProcedure, QualityDataModelValueSet.DiagnosticStudyPerformedMacularExam),
                                           NotDoneValueSetEntry = GetLinkedAnswerMappedValueSetEntry(pep.PatientExamPerformedQuestionAnswers,
                                               QualityDataModelValueSet.DiagnosticStudyPerformednotdonePatientReason, QualityDataModelValueSet.DiagnosticStudyPerformednotdoneMedicalReason)
                                       })
                        .Where(m => m.ValueSetEntry != null && m.NotDoneValueSetEntry != null)
                        .ToArray();

                //Get the results of the macular exam, including a) severity of the Diabetic Retinopathy, and b) presence or absence of Macular Edema.  If both are present, the template will be rendered twice.  
                //We will get the results of the macular exam from model.PatientDiagnoses.ClinicalCondition
                //TODO: Capture AxisQualifier CNF on Macular Edema
                model.ResultObservationDiagnosticStudyResultEntryModels =
                    model.Patient.PatientExamPerformeds
                        .Where(pep => encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Encounter.Invoice.EncounterId).Contains(pep.EncounterId))
                        .SelectMany(pep => pep.Encounter.PatientDiagnosisDetails
                            .Select(pdd => new { Detail = pdd, Exam = pep }))
                        .Select(i => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                       {
                                           Start = i.Exam.Encounter.StartDateTime,
                                           End = i.Exam.Encounter.EndDateTime,
                                           RootAndExtension = i.Detail.GetOidRootAndExtension(QualityDataModelValueSet.DiagnosticStudyResultMacularExam.ToString()),
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(i.Exam.ClinicalProcedure, QualityDataModelValueSet.DiagnosticStudyResultMacularExam),
                                           ResultValueSetEntry =
                                             Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(i.Detail.PatientDiagnosis.ClinicalCondition,
                                                 QualityDataModelValueSet.AttributeResultLevelOfSeverityOfRetinopathyFindings, QualityDataModelValueSet.AttributeResultMacularEdemaFindingsPresent, QualityDataModelValueSet.AttributeResultMacularEdemaFindingsAbsent)
                                       })
                        .Where(m => m.ValueSetEntry != null && m.ResultValueSetEntry != null)
                        .ToArray();
            }
        }

        /// <summary>
        /// Diabetic Retinopathy Communication with Physician Managing Ongoing Diabetes Care Measure gets patients aged 18 and older before the Measurement Period start date
        /// who had a discharged encounter mapped to the EyeExam ValueSet Group during the Measurement Period
        /// and who had an active diagnosis of Diabetic Retinopathy starting on or before the (most recent) Measurement Period EyeExam encounter
        /// and who had at least 2 discharged encounters from the AnyInteraction ValueSet group during the Measurement Period.
        /// </summary>
        private class DiabeticRetinopathyCommunicationModelProducer : ModelProducer
        {
            public DiabeticRetinopathyCommunicationModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    // diagnosis of diabetic retinopathy made on or before the Measurement Period EyeExam.
                    .Where(p => p.Encounters.Any(e =>
                        Queries.HasValueSetsDuringMeasurementPeriod(QualityDataModelValueSetGroup.EyeExamValueSets).Invoke(e)
                        && Queries.HasDiagnosisBefore(QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy).Invoke(e.Patient, e.EndDateTime)
                        ))
                    //and user must have seen the patient at least twice during the Measurement Period.
                    .Where(p => p.Encounters.Count(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(QualityDataModelValueSetGroup.AnyInteractionValueSets).Invoke(e)) >= 2)
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period with billing services that are mapped to the EyeExam value set group.
                var encounterActivitiesEncounterPerformedEntryModels = model.Patient.Encounters
                    .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                    .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                    .Select(e => new { e.Invoice.Encounter, Model = ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.EyeExamValueSets) })
                    .Where(m => m.Model.ValueSetEntry != null)
                    .ToArray();

                model.EncounterActivitiesEncounterPerformedEntryModels = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Model).ToArray();

                var lastEncounterStart = model.EncounterActivitiesEncounterPerformedEntryModels.OrderByDescending(m => m.Start).Select(m => m.Start).First();

                //Get the patient's active diagnoses of Diabetic Retinopathy that start before or during the most recent of the above Measurement Period EyeExams.
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime <= lastEncounterStart)
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get the results of the patient's macular exam, which should include a) severity of diabetic retinopathy and b) presence or absence of macular edema.
                model.ResultObservationDiagnosticStudyResultEntryModels =
                                    model.Patient.PatientExamPerformeds
                                        .Where(pep => encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Encounter.Id).Contains(pep.EncounterId))
                        .SelectMany(pep => pep.Encounter.PatientDiagnosisDetails
                            .Select(pdd => new { Detail = pdd, Exam = pep }))
                        .Select(i => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                        {
                                            Start = i.Exam.Encounter.StartDateTime,
                                            End = i.Exam.Encounter.EndDateTime,
                                            RootAndExtension = i.Detail.GetOidRootAndExtension(QualityDataModelValueSet.DiagnosticStudyResultMacularExam.ToString()),
                                            ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(i.Exam.ClinicalProcedure, QualityDataModelValueSet.DiagnosticStudyResultMacularExam),
                                            ResultValueSetEntry =
                                             Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(i.Detail.PatientDiagnosis.ClinicalCondition,
                                                 QualityDataModelValueSet.AttributeResultLevelOfSeverityOfRetinopathyFindings, QualityDataModelValueSet.AttributeResultMacularEdemaFindingsPresent, QualityDataModelValueSet.AttributeResultMacularEdemaFindingsAbsent)
                                        })
                                        .Where(m => m.ValueSetEntry != null && m.ResultValueSetEntry != null)
                                        .ToArray();

                //Use CommunicationFromProviderToProvider to report level of severity, macular edema and also NOT DONE.  This is different from DiabeticRetinopathyDocumentation.
                //TODO:  CommunicationFromProviderToProvider entryRelationship "RSON" section if there was a patient or medical reason why these communication of a) diabetic retinopathy severity was NOT DONE and/or b) presence or absence of macular edema was NOT DONE.
                //wrong model, and how do you get the Not Done in?
                model.CommunicationProviderToProviderEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.EncounterCommunicationWithOtherProviderOrders)
                        .SelectMany(o => o.Encounter.PatientDiagnosisDetails.Select(
                            pdd =>
                                new
                                {
                                    ValueSets = new[]
                                                {
                                        QualityDataModelValueSet.CommunicationFromProviderToProviderLevelOfSeverityOfRetinopathyFindings,
                                        QualityDataModelValueSet.CommunicationFromProviderToProviderMacularEdemaFindingsPresent,
                                                    QualityDataModelValueSet.CommunicationFromProviderToProviderMacularEdemaFindingsAbsent
                                                },
                                    Order = o,
                                    Detail = pdd
                                }
                            ))
                        .Select(o => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         Start = o.Order.Encounter.EndDateTime,
                                         End = o.Order.Encounter.EndDateTime,
                                         RootAndExtension = o.Detail.GetOidRootAndExtension(o.ValueSets.Join(".")),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(o.Detail.PatientDiagnosis.ClinicalCondition, o.ValueSets)
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

            }
        }

        /// <summary>
        /// Primary Open Angle Glaucoma (POAG) measure gets patients aged 18 years or older before Measurement Period Start Date
        /// who had a discharged encounter mapped to the EyeExam ValueSet Group during the Measurement Period
        /// and have an active diagnosis of POAG starting on or before the (most recent) Measurement Period EyeExam encounter
        /// and who had at least 2 discharged encounters from the AnyInteraction ValueSet group during the Measurement Period.
        /// </summary>
        private class PoagModelProducer : ModelProducer
        {
            public PoagModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p => p.Encounters.Any(e =>
                        Queries.HasValueSetsDuringMeasurementPeriod(QualityDataModelValueSetGroup.EyeExamValueSets).Invoke(e)
                        && Queries.HasDiagnosisBefore(QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy).Invoke(e.Patient, e.EndDateTime)
                        ))
                    .Where(p => p.Encounters.Count(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(QualityDataModelValueSetGroup.AnyInteractionValueSets).Invoke(e)) >= 2)
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period with billing services mapped to the specified value sets.
                var encounterActivitiesEncounterPerformedEntryModels = model.Patient.Encounters
                    .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                    .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                    .Select(e => new { e.Invoice.Encounter, Model = ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.EyeExamValueSets) })
                    .Where(m => m.Model.ValueSetEntry != null)
                    .ToArray();

                var encounters = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Encounter).ToArray();

                model.EncounterActivitiesEncounterPerformedEntryModels = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Model).ToArray();

                var lastEncounterStart = model.EncounterActivitiesEncounterPerformedEntryModels.OrderByDescending(m => m.Start).Select(m => m.Start).First();

                //Get the patient's active diagnoses that started before or during the most recent of the above encounters that are mapped to the Primary Open Angle Glaucoma (POAG) value set.
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime <= lastEncounterStart)
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActivePrimaryOpenAngleGlaucoma))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get the patient's cup to disc measurements taken during any of the above encounters.  
                //Render the entryRelationship section of the template using "RSON" if the user documented cup to disc measurement was NOT DONE for a MedicalReason.
                var cupToDiscMeasurements =
                    model.Patient.PatientDiagnosticTestPerformeds.Where(d => d.EncounterId.HasValue && encounters.Select(e => e.Id).Contains(d.EncounterId.Value))
                        .SelectMany(t => t.PatientDiagnosticTestPerformedQuestionAnswers
                            .SelectMany(a => a.QuestionAnswerValues.Select(v => new
                                                                                {
                                                                                    Test = t,
                                                                                    Value = v,
                                                                                    Entity = EntityLinkService.GetMappedEntity(v as LinkedQuestionAnswerValue) as IHasId
                                                                                })))
                        .Where(v => v.Entity != null)
                        .Select(v => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         Start = v.Test.Encounter.StartDateTime,
                                         End = v.Test.Encounter.StartDateTime,
                                         RootAndExtension = v.Value.GetOidRootAndExtension(QualityDataModelValueSet.DiagnosticStudyResultCupToDiscRatio.ToString()),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.DiagnosticStudyResultCupToDiscRatio),
                                         ResultValue = v.Entity.ToString(),
                                         Laterality = Mapper.SnomedModelMapper.GetSnomedModelMap(new[] { v.Value.QuestionAnswer.Laterality }, Constants.SnomedClinicalQualifierExternalEntityName).Select(i => i.Value).FirstOrDefault(),
                                         NotDoneValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.DiagnosticStudyResultnotdoneMedicalReason)
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //TODO: Get the patient's optic disc exam results (POSITIVE or NEGATIVE) performed during any of the above encounters.  
                //Render the entryRelationship section of the template using "RSON" if the user documented the optic disc exam was NOT DONE for a MedicalReason.
                var opticDiscMeasurements =
                   model.Patient.PatientDiagnosticTestPerformeds.Where(d => d.EncounterId.HasValue && encounters.Select(e => e.Id).Contains(d.EncounterId.Value))
                        .SelectMany(t => t.PatientDiagnosticTestPerformedQuestionAnswers
                            .SelectMany(a => a.QuestionAnswerValues.Select(v => new
                            {
                                Test = t,
                                Value = v,
                                Entity = EntityLinkService.GetMappedEntity(v as LinkedQuestionAnswerValue) as IHasId
                            })))
                        .Where(v => v.Entity != null)
                        .Select(v => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                        {
                            Start = v.Test.Encounter.StartDateTime,
                            End = v.Test.Encounter.EndDateTime,
                            RootAndExtension = v.Value.GetOidRootAndExtension(QualityDataModelValueSet.DiagnosticStudyResultOpticDiscExamForStructuralAbnormalities.ToString()),
                            ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.DiagnosticStudyResultOpticDiscExamForStructuralAbnormalities),
                            ResultValue = v.Entity.ToString(),
                            ResultType = "ST",
                            Laterality = Mapper.SnomedModelMapper.GetSnomedModelMap(new[] { v.Value.QuestionAnswer.Laterality }, Constants.SnomedClinicalQualifierExternalEntityName).Select(i => i.Value).FirstOrDefault(),
                            NotDoneValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.DiagnosticStudyResultnotdoneMedicalReason)
                        })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                // Combine measurement results and save them to model
                model.ResultObservationDiagnosticStudyResultEntryModels = cupToDiscMeasurements
                    .Union(opticDiscMeasurements)
                    .ToArray();
            }
        }

        /// <summary>
        /// Documentation of Current Medications Measure gets patients aged 18 or older before the Measurement Period start date
        /// who had a discharged encounter performed by the specified user during the Measurement Period mapped to the ValueSet "Encounter Performed: Medications Encounter Code Set"
        /// </summary>
        private class DocumentationOfMedicationsModelProducer : ModelProducer
        {
            public DocumentationOfMedicationsModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients.Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p => p.Encounters.Any(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e)
                                                      && Queries.HasEncounterServicesMappedToValueSets(new[] { QualityDataModelValueSet.EncounterPerformedMedicationsEncounterCodeSet }).Invoke(e)))
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get the patient's discharged encounters performed by the specified user during the Measurement Period and mapped to the specified value set.
                var encounterActivitiesEncounterPerformedEntryModels = model.Patient.Encounters
                    .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                    .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                    .Select(e => new { e.Invoice.Encounter, Model = ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSet.EncounterPerformedMedicationsEncounterCodeSet) })
                    .Where(m => m.Model.ValueSetEntry != null)
                    .ToArray();

                var encounters = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Encounter).ToArray();

                model.EncounterActivitiesEncounterPerformedEntryModels = encounterActivitiesEncounterPerformedEntryModels.Select(e => e.Model).ToArray();


                //Get the user's confirmation that all meds were documented as required, or the meds documentation was NOT DONE for a Medical Or Other Reason
                model.ProcedureActivityPerformedProcedurePerformedEntryModels =
                    model.Patient.PatientProcedurePerformeds.Where(p => p.EncounterId.HasValue && encounters.Select(e => e.Id).Contains(p.EncounterId.Value))
                        .SelectMany(t => t.PatientProcedurePerformedQuestionAnswers
                            .SelectMany(a => a.QuestionAnswerValues.Select(v => new
                                                                                {
                                                                                    Test = t,
                                                                                    Value = v,
                                                                                    Entity = EntityLinkService.GetMappedEntity(v as LinkedQuestionAnswerValue) as IHasId
                                                                                })))
                        .Where(v => v.Entity != null)
                        .Select(v => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         Start = v.Test.Encounter.StartDateTime,
                                         End = v.Test.Encounter.EndDateTime,
                                         RootAndExtension = v.Value.GetOidRootAndExtension(QualityDataModelValueSet.ProcedurePerformedCurrentMedicationsDocumentedSnmd.ToString()),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.ProcedurePerformedCurrentMedicationsDocumentedSnmd),
                                         NotDoneValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(v.Entity, QualityDataModelValueSet.ProcedurePerformednotdoneMedicalOrOtherReasonNotDone)
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();
            }

        }

        /// <summary>
        /// Use of High-Risk Medications in the Elderly gets patients aged 66 years and older before the Measurement Period Start Date
        /// who had a discharged encounter during the Measurement Period mapped to the RegularExam ValueSet group.
        /// </summary>
        private class HighRiskMedicationsModelProducer : ModelProducer
        {
            private readonly bool _minimumTwoHighRiskMeds;

            public HighRiskMedicationsModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService, bool minimumTwoHighRiskMeds)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
                _minimumTwoHighRiskMeds = minimumTwoHighRiskMeds;
            }

            public override int[] FindPatients()
            {
                var valueSets = new[]
                                {
                                    QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction,
                                    QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp,
                                    QualityDataModelValueSet.EncounterPerformedHomeHealthcareServices, QualityDataModelValueSet.EncounterPerformedAnnualWellnessVisit
                                };

                if (_minimumTwoHighRiskMeds)
                {
                    return PracticeRepository.Patients
                        .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 66))
                        .Where(p => p.Encounters.Any(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(valueSets).Invoke(e)))
                        .GroupBy(p => p.Id).Where(g => g.Count() > 1).Select(g => g.Key).ToArray();
                }

                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 66))
                    .Where(p => p.Encounters.Any(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(valueSets).Invoke(e)))
                    .Select(p => p.Id).ToArray();

            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Discharged encounters performed by the specified user during the Measurement Period with billing services mapped to the specified value sets.
                model.EncounterActivitiesEncounterPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.RegularExamValueSets))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Medications ordered during any of the above encounters that map to the specified high-risk medication value sets.
                model.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.EncounterMedicationOrders)
                        .Where(o => o.MedicationOrderAction != MedicationOrderAction.Discontinue)
                        .Select(o => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(o, QualityDataModelValueSet.MedicationOrderHighRiskMedicationsForTheElderly))
                        .Where(m => m.ValueSetEntry != null).ToArray();

                //Medications ordered during any of the above encounters that map to the specified high-risk medications-with-days-supply-criteria value set, that have a days supply over 90 days.
                model.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels = model.PlanOfCareActivitySubstanceAdministrationMedicationOrderEntryModels.Concat(
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.EncounterMedicationOrders)
                        .Where(o => o.MedicationOrderAction != MedicationOrderAction.Discontinue && o.PatientMedicationDetail.DaysSupply > 90)
                        .Select(o => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(o, QualityDataModelValueSet.MedicationOrderHighriskMedicationsWithDaysSupplyCriteria))
                        .Where(m => m.ValueSetEntry != null))
                    .ToArray();
            }
        }

        /// <summary>
        /// Controlling High Blood Pressure gets patients aged 18-84 years (inclusive) before the Measurement Period start date
        /// who had an active diagnosis of Essential Hypertension that
        ///     a) started within six months after the Measurement Period Start Date OR
        ///     b) started before the Measurement Period Start Date and did not end before the Measurement Period Start Date
        /// and had a discharged encounter during the Measurement Period mapped to the RegularExam ValueSet Group.
        /// </summary>
        private class ControllingHighBloodPressureModelProducer : ModelProducer
        {
            public ControllingHighBloodPressureModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                var valueSets = new[]
                            {
                                QualityDataModelValueSet.EncounterPerformedOfficeVisit, QualityDataModelValueSet.EncounterPerformedFacetofaceInteraction,
                                QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesEstablishedOfficeVisit18AndUp, QualityDataModelValueSet.EncounterPerformedPreventiveCareServicesinitialOfficeVisit18AndUp,
                                QualityDataModelValueSet.EncounterPerformedHomeHealthcareServices, QualityDataModelValueSet.EncounterPerformedAnnualWellnessVisit
                            };

                return PracticeRepository.Patients
                    .Where(p => Queries.HasAgeRangeAtMeasurementPeriodStartDateExclusiveMaxAge.Invoke(p, 18, 85))
                    .Where(p => p.Encounters.Any(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e) && Queries.HasEncounterServicesMappedToValueSets(valueSets).Invoke(e)))
                    .Where(p => p.PatientDiagnoses.Any(pd =>
                        pd.PatientDiagnosisDetails.Any(pdd =>
                            Queries.IsDiagnosisStillActiveDuringMeasurementPeriod.Invoke(pdd) || Queries.DiagnosisStartsWithinMonthsOfMeasurementPeriod(6).Invoke(pdd))
                            && Queries.IsClinicalConditionMappedToValueSets(QualityDataModelValueSet.DiagnosisActiveEssentialHypertension).Invoke(pd.ClinicalCondition)))
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get all discharged encounters performed by the specified user during the Measurement Period with billing services mapped to the RegularExam value set group.
                model.EncounterActivitiesEncounterPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.RegularExamValueSets))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get any discharged encounters performed before or during the Measurement Period with billing services mapped to the specified value set.  These will be denominator exclusions.
                model.EncounterActivitiesEncounterPerformedEntryModels = model.EncounterActivitiesEncounterPerformedEntryModels.Concat(
                    model.Patient.Encounters
                        .Where(e => e.StartDateTime <= model.MeasurementPeriodEndDate)
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSet.EncounterPerformedEsrdMonthlyOutpatientServices))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray()
                    );

                //Get any procedures performed before or during the Measurement Period where the clinical procedure is mapped to the specified value sets.  These will be denominator exclusions.
                model.ProcedureActivityPerformedProcedurePerformedEntryModels =
                    model.Patient.PatientProcedurePerformeds
                       .Where(z => z.PerformedDateTime <= model.MeasurementPeriodEndDate)
                       .Select(ppp => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                    {
                                        ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(ppp.ClinicalProcedure, QualityDataModelValueSet.ProcedurePerformedVascularAccessForDialysis, QualityDataModelValueSet.ProcedurePerformedKidneyTransplant, QualityDataModelValueSet.ProcedurePerformedDialysisServices),
                                        Start = ppp.PerformedDateTime,
                                        End = ppp.PerformedDateTime,
                                        RootAndExtension = ppp.GetOidRootAndExtension(new[] { QualityDataModelValueSet.ProcedurePerformedVascularAccessForDialysis, QualityDataModelValueSet.ProcedurePerformedKidneyTransplant, QualityDataModelValueSet.ProcedurePerformedDialysisServices }.Join(".")),
                                        Laterality = null,
                                        NotDoneValueSetEntry = null
                                    })
                       .Where(m => m.ValueSetEntry != null)
                       .ToArray();

                //Get any interventions performed before or during the Measurement Period that are mapped to the specified value sets.  These will be denominator exclusions.
                model.ProcedureActivityActInterventionPerformedEntryModels =
                    model.Patient.PatientInterventionPerformeds
                        .Where(p => p.PerformedDateTime < model.MeasurementPeriodEndDate)
                        .Select(pip => new QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
                                       {
                                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pip.ClinicalProcedure, QualityDataModelValueSet.InterventionPerformedDialysisEducation, QualityDataModelValueSet.InterventionPerformedOtherServicesRelatedToDialysis),
                                           Start = pip.PerformedDateTime,
                                           End = pip.PerformedDateTime,
                                           RootAndExtension = pip.GetOidRootAndExtension(new[] { QualityDataModelValueSet.InterventionPerformedDialysisEducation, QualityDataModelValueSet.InterventionPerformedOtherServicesRelatedToDialysis }.Join("."))
                                       })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get any active diagnoses of Essential Hypertension that started before, or within six months after, the Measurement Period Start Date, started before the Measurement Period end date, and did not end before the Measurement Period start date.
                model.ProblemObservationDiagnosisActiveEntryModels = model.Patient.PatientDiagnoses
                    .Select(pd => pd.LatestPatientDiagnosisDetail)
                    .Where(pdd => pdd.StartDateTime <= model.MeasurementPeriodStartDate.AddMonths(6) && pdd.StartDateTime < model.MeasurementPeriodEndDate && (pdd.EndDateTime == null || pdd.EndDateTime > model.MeasurementPeriodStartDate))
                    .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveEssentialHypertension))
                    .Where(m => m.ValueSetEntry != null)
                    .ToArray();

                //Get any active diagnoses that started before the Measurement Period end date and did not end before the Measurement Period start date, mapped to the specified value sets.  These are denominator exclusions.
                model.ProblemObservationDiagnosisActiveEntryModels = model.ProblemObservationDiagnosisActiveEntryModels.Concat(
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime <= model.MeasurementPeriodEndDate && (pdd.EndDateTime == null || pdd.EndDateTime > model.MeasurementPeriodStartDate))
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd, QualityDataModelValueSet.DiagnosisActiveChronicKidneyDiseaseStage5, QualityDataModelValueSet.DiagnosisActiveEndStageRenalDisease, QualityDataModelValueSet.DiagnosisActivePregnancy))
                        .Where(m => m.ValueSetEntry != null))
                    .ToArray();

                //Get the patient's blood pressure from the Measurement Period encounters.
                model.ResultObservationPhysicalExamFindingEntryModels =
                    model.Patient.Encounters.Where(e => Queries.HasEncounterServicesMappedToValueSets(
                        QualityDataModelValueSetGroup.RegularExamValueSets.Concat(new[] { QualityDataModelValueSet.EncounterPerformedOutpatientConsultation }).ToArray()).Invoke(e))
                        .SelectMany(e => e.PatientBloodPressures)
                        .SelectMany(bp => new[]
                        {
                            new
                            {
                                BloodPressure = bp,
                                Value = bp.SystolicPressure,
                                RootAndExtension = bp.GetOidRootAndExtension("Systolic"),
                                ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.PhysicalExamFindingSystolicBloodPressure).FirstOrDefault()
                            },
                            new
                            {
                                BloodPressure = bp,
                                Value = bp.DiastolicPressure,
                                RootAndExtension = bp.GetOidRootAndExtension("Diastolic"),
                                ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.PhysicalExamFindingDiastolicBloodPressure).FirstOrDefault()
                            }
                        }).Where(i => i.Value != null)

                        .Select(bp => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
                        {
                            Start = bp.BloodPressure.Encounter.StartDateTime,
                            End = bp.BloodPressure.Encounter.EndDateTime,
                            RootAndExtension = bp.RootAndExtension,
                            ValueSetEntry = bp.ValueSetEntry,
                            Laterality = null,
                            ResultValue = bp.Value.ToString(),
                            ResultUnitOfMeasurement = "mm[Hg]",
                            ResultValueSetEntry = null
                        })
                        .ToArray();
            }
        }

        /// <summary>
        /// Closing the Referral Loop: Receipt of Specialist Report -- Patients who had a referral ordered (EncounterTransitionOfCareOrder) during the Measurement Period 
        /// and who had a discharged encounter with billing services mapped to the ReferralVisits ValueSet group, performed by the specified user during the Measurement Period.
        /// </summary>
        private class CloseTheReferralLoopModelProducer : ModelProducer
        {
            public CloseTheReferralLoopModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients.Where(
                    p =>
                        p.Encounters.Any(e => Queries.HasEncounterServicesMappedToValueSets(QualityDataModelValueSetGroup.ReferralValueSets).Invoke(e) && Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        &&
                        p.Encounters.Any(e => e.EncounterTransitionOfCareOrders.Any() && Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                    )
                    .Select(p => p.Id).ToArray();
            }

            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                //Get all discharged encounters performed by the specified user during the Measurement Period that are mapped to the specified value setse.
                model.EncounterActivitiesEncounterPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.Invoices.SelectMany(i => i.BillingServices))
                        .Select(e => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(e, QualityDataModelValueSetGroup.ReferralValueSets))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get all the referrals (EncounterTransitionOfCareOrders) ordered during the Measurement Period.
                model.ProcedureActivityActInterventionPerformedEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.EncounterTransitionOfCareOrders)
                        .Select(o => new QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
                                     {
                                         Start = o.Encounter.StartDateTime,
                                         End = o.Encounter.StartDateTime,
                                         RootAndExtension = o.GetOidRootAndExtension(QualityDataModelValueSet.InterventionPerformedReferral.ToString()),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.InterventionPerformedReferral).FirstOrDefault(i => i.Code == "183515008")
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get all the referral responses linked to (and received after) the referrals obtained above.  This is the numerator.
                model.CommunicationProviderToProviderEntryModels =
                    model.Patient.Encounters
                        .Where(e => Queries.IsDischargedEncounterInMeasurementPeriodForUser.Invoke(e))
                        .SelectMany(e => e.EncounterTransitionOfCareOrders)
                        .Where(o => o.ResponseReceivedDateTime > o.Encounter.StartDateTime)
                        .Select(o => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         Start = o.ResponseReceivedDateTime,
                                         End = o.ResponseReceivedDateTime,
                                         RootAndExtension = o.GetOidRootAndExtension(QualityDataModelValueSet.CommunicationFromProviderToProviderConsultantReport.ToString()),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.CommunicationFromProviderToProviderConsultantReport).FirstOrDefault(i => i.Code == "371530004"),
                                         NotDoneValueSetEntry = null
                                     })
                        .ToArray();
            }
        }

        /// <summary>
        /// Cataract Surgery Complications measure finds patients aged 18 or older before the Measurement Period Start Date
        /// who had cataract surgery during the Measurement Period.
        /// </summary>
        private class CataractSurgeryComplicationsModelProducer : ModelProducer
        {
            public CataractSurgeryComplicationsModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }

            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p => p.PatientProcedurePerformeds.Any(ppp => Queries.IsPatientProcedurePerformedMappedToValueSetsDuringMeasurementPeriod(QualityDataModelValueSet.ProcedurePerformedCataractSurgery).Invoke(ppp)))
                    .Select(p => p.Id).Distinct().ToArray();
            }

            /// <summary>
            /// Get the patient's cataract surgeries and the operative eye (laterality) performed by the specified user during the Measurement Period.
            /// </summary>
            /// <param name="model"></param>
            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                var cataractSurgeries =
                    (from qa in model.Patient.PatientProcedurePerformeds.SelectMany(pep => pep.PatientProcedurePerformedQuestionAnswers)
                     let laterality = Mapper.SnomedModelMapper.GetSnomedModelMap(new[] { qa.Laterality }, Constants.SnomedClinicalQualifierExternalEntityName).Select(i => i.Value).FirstOrDefault()
                     where Queries.IsPatientProcedurePerformedMappedToValueSetsDuringMeasurementPeriod(QualityDataModelValueSet.ProcedurePerformedCataractSurgery).Invoke(qa.PatientProcedurePerformed)
                     select new
                            {
                                Laterality = laterality,
                                Answer = qa,
                                Model = ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel(qa, laterality, QualityDataModelValueSet.ProcedurePerformedCataractSurgery)
                            }).ToArray();

                if (!cataractSurgeries.Any()) return;

                var maxStartDate = cataractSurgeries.Select(s => s.Answer.PatientProcedurePerformed.PerformedDateTime).Max();

                //Get any pars plana vitrectomy procedures performed before the cataract surgery on the same eye as the cataract surgery.  This will be a denominator exclusion.
                var models2 =
                    (from qa in model.Patient.PatientProcedurePerformeds.SelectMany(pep => pep.PatientProcedurePerformedQuestionAnswers)
                     let cataractSurgery = cataractSurgeries.FirstOrDefault(s => s.Answer.Laterality == qa.Laterality)
                     where qa.PatientProcedurePerformed.PerformedDateTime < cataractSurgery.Answer.PatientProcedurePerformed.PerformedDateTime
                     select ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel(qa, cataractSurgery.Laterality, QualityDataModelValueSet.ProcedurePerformedPriorParsPlanaVitrectomy))
                      .Where(m => m.ValueSetEntry != null)
                      .ToArray();

                //Get any procedures mapped to a group of value sets for surgical complications that were performed within 30 days of the patient's cataract surgery on the same eye as the cataract surgery.  This is the numerator of the measure.
                var models3 =
                    (from qa in model.Patient.PatientProcedurePerformeds.SelectMany(pep => pep.PatientProcedurePerformedQuestionAnswers)
                     let cataractSurgery = cataractSurgeries.FirstOrDefault(s => s.Answer.Laterality == qa.Laterality)
                     where qa.PatientProcedurePerformed.PerformedDateTime <= cataractSurgery.Answer.PatientProcedurePerformed.PerformedDateTime.AddDays(30)
                     select ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel(qa, cataractSurgery.Laterality,
                        QualityDataModelValueSet.ProcedurePerformedRemovalProcedures,
                        QualityDataModelValueSet.ProcedurePerformedParacentesisProcedures,
                        QualityDataModelValueSet.ProcedurePerformedExcisionOfAdhesions,
                        QualityDataModelValueSet.ProcedurePerformedAspirationAndInjectionProcedures,
                        QualityDataModelValueSet.ProcedurePerformedLensProcedure,
                        QualityDataModelValueSet.ProcedurePerformedVitreousProcedures,
                        QualityDataModelValueSet.ProcedurePerformedRetinalRepairProcedures,
                        QualityDataModelValueSet.ProcedurePerformedScleralProcedures,
                        QualityDataModelValueSet.ProcedurePerformedRevisionProcedures))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                model.ProcedureActivityPerformedProcedurePerformedEntryModels = cataractSurgeries.Select(c => c.Model).Concat(models2).Concat(models3).ToArray();

                //Get active diagnoses mapped to the specified value sets that began before the cataract surgery.  These will be denominator exclusions.
                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                        .Select(pd => pd.LatestPatientDiagnosisDetail)
                        .Where(pdd => pdd.StartDateTime < maxStartDate)
                        .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd,
                            QualityDataModelValueSet.DiagnosisActiveAcuteAndSubacuteIridocyclitis,
                            QualityDataModelValueSet.DiagnosisActiveAdhesionsAndDisruptionsOfIrisAndCiliaryBody,
                            QualityDataModelValueSet.DiagnosisActiveCentralCornealUlcer,
                            QualityDataModelValueSet.DiagnosisActiveBurnConfinedToEyeAndAdnexa,
                            QualityDataModelValueSet.DiagnosisActivePseudoexfoliationSyndrome,
                            QualityDataModelValueSet.DiagnosisActiveSenileCataract,
                            QualityDataModelValueSet.DiagnosisActiveHypotonyOfEye,
                            QualityDataModelValueSet.DiagnosisActiveCataractSecondaryToOcularDisorders,
                            QualityDataModelValueSet.DiagnosisActiveHighHyperopia,
                            QualityDataModelValueSet.DiagnosisActiveInjuryToOpticNerveAndPathways,
                            QualityDataModelValueSet.DiagnosisActiveCataractCongenital,
                            QualityDataModelValueSet.DiagnosisActiveCystsOfIrisCiliaryBodyAndAnteriorChamber,
                            QualityDataModelValueSet.DiagnosisActiveGlaucoma,
                            QualityDataModelValueSet.DiagnosisActiveCataractMatureOrHypermature,
                            QualityDataModelValueSet.DiagnosisActiveTraumaticCataract,
                            QualityDataModelValueSet.DiagnosisActiveCataractPosteriorPolar,
                            QualityDataModelValueSet.DiagnosisActivePathologicMyopia,
                            QualityDataModelValueSet.DiagnosisActiveOpenWoundOfEyeball,
                            QualityDataModelValueSet.DiagnosisActiveCertainTypesOfIridocyclitis,
                            QualityDataModelValueSet.DiagnosisActiveVascularDisordersOfIrisAndCiliaryBody,
                            QualityDataModelValueSet.DiagnosisActiveHereditaryCornealDystrophies,
                            QualityDataModelValueSet.DiagnosisActiveEnophthalmos,
                            QualityDataModelValueSet.DiagnosisActiveUveitis,
                            QualityDataModelValueSet.DiagnosisActiveChronicIridocyclitis,
                            QualityDataModelValueSet.DiagnosisActiveCornealOpacityAndOtherDisordersOfCornea,
                            QualityDataModelValueSet.DiagnosisActiveCloudyCornea,
                            QualityDataModelValueSet.DiagnosisActivePosteriorLenticonus,
                            QualityDataModelValueSet.DiagnosisActiveAphakiaAndOtherDisordersOfLens,
                            QualityDataModelValueSet.DiagnosisActiveCornealEdema,
                            QualityDataModelValueSet.DiagnosisActiveRetrolentalFibroplasias,
                            QualityDataModelValueSet.DiagnosisActiveAnomaliesOfPuillaryFunction
                            ))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Get any medication mapped to the specified value set that was started before the cataract surgery.  This will be a denominator exclusion.
                model.MedicationActivityMedicationActiveEntryModels =
                    model.Patient.PatientMedications.SelectMany(pm => pm.PatientMedicationDetails)
                        .Where(pmd => pmd.MedicationEventType == MedicationEventType.PatientStarted && pmd.MedicationEventDateTime < maxStartDate)
                        .Select(pmd => ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(pmd, QualityDataModelValueSet.MedicationActiveUseOfSystemicSympatheticAlpha1aAntagonistMedicationForTreatmentOfProstaticHypertrophy))
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

            }

        }

        /// <summary>
        /// Visual Acuity after Cataract Surgery measure looks for patients aged 18 or older before the Measurement Period Start Date
        /// who had cataract surgery during the Measurement Period no later than 92 days before the Measure Period End Date.
        /// </summary>
        private class CataractSurgeryVisualAcuityModelProducer : ModelProducer
        {
            public CataractSurgeryVisualAcuityModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
                : base(practiceRepository, queries, mapper, entityLinkService)
            {
            }
            /// <summary>
            /// Find patients with a minimum age of 18 at the Measurement Period start date who had a procedure mapped to the Cataract Surgery value set performed by the specified user 
            /// during the Measurement Period, but not later than 92 days before the end of the Measurement Period.
            /// </summary>
            /// <returns></returns>
            public override int[] FindPatients()
            {
                return PracticeRepository.Patients
                    .Where(p => Queries.IsMinimumAgeAtMeasurementPeriodStartDate.Invoke(p, 18))
                    .Where(p => p.PatientProcedurePerformeds.Any(ppp => Queries.IsPatientProcedurePerformedMappedToValueSetsDuringMeasurementPeriod(QualityDataModelValueSet.ProcedurePerformedCataractSurgery).Invoke(ppp)
                                                                        && Queries.IsPatientProcedurePerformedBeforeMeasurementPeriodEndDateInDays(92).Invoke(ppp)
                        ))
                    .Select(p => p.Id).ToArray();
            }

            /// <summary>
            /// Gets the patient's cataract surgery procedures and laterality (which eye) that were performed between the Measurement Period Start Date and 92 days before the Measurement Period End Date.
            /// </summary>
            /// <param name="model"></param>
            public override void FillModel(QrdaCategory1Model model)
            {
                base.FillModel(model);

                var procedures =
                    model.Patient.PatientProcedurePerformeds
                        .Where(ppp => ppp.PerformedDateTime >= model.MeasurementPeriodStartDate && ppp.PerformedDateTime < model.MeasurementPeriodEndDate.AddDays(-92))
                        .SelectMany(ppp =>
                            ppp.PatientProcedurePerformedQuestionAnswers.Select(qa => new
                                                                                      {
                                                                                          Answer = qa,
                                                                                          Procedure = ppp,
                                                                                          Laterality = Mapper.SnomedModelMapper.GetSnomedModelMap(new[] { qa.Laterality }, Constants.SnomedClinicalQualifierExternalEntityName).Select(i => i.Value).FirstOrDefault()
                                                                                      })).ToArray();

                model.ProcedureActivityPerformedProcedurePerformedEntryModels =
                    procedures
                        .Select(p => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                                     {
                                         Start = p.Procedure.PerformedDateTime,
                                         End = p.Procedure.PerformedDateTime,
                                         RootAndExtension = p.Answer.GetOidRootAndExtension(),
                                         ValueSetEntry = model.Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(p.Procedure.ClinicalProcedure, QualityDataModelValueSet.ProcedurePerformedCataractSurgery),
                                         Laterality = p.Laterality
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();

                //Using the most recent (MAX) date of the patient's cataract surgeries brought back above, get any of these diagnoses that the patient had before the cataract surgery.  These patients will be Denominator Exclusions.
                var maxStart = model.ProcedureActivityPerformedProcedurePerformedEntryModels.Select(i => i.Start).Max();

                model.ProblemObservationDiagnosisActiveEntryModels =
                    model.Patient.PatientDiagnoses
                    .Select(pd => pd.LatestPatientDiagnosisDetail)
                    .Where(pdd => pdd.StartDateTime < maxStart)
                    .Select(pdd => ToProblemObservationDiagnosisActiveEntryModel(pdd,
                        QualityDataModelValueSet.DiagnosisActiveAcuteAndSubacuteIridocyclitis,
                        QualityDataModelValueSet.DiagnosisActivePathologicMyopia,
                        QualityDataModelValueSet.DiagnosisActiveRetinalDetachmentWithRetinalDefect,
                        QualityDataModelValueSet.DiagnosisActiveRetinalVascularOcclusion,
                        QualityDataModelValueSet.DiagnosisActivePurulentEndophthalmitis,
                        QualityDataModelValueSet.DiagnosisActiveCertainTypesOfIridocyclitis,
                        QualityDataModelValueSet.DiagnosisActiveGlaucomaAssociatedWithCongenitalAnomaliesDystrophiesAndSystemicSyndromes,
                        QualityDataModelValueSet.DiagnosisActiveChronicIridocyclitis,
                        QualityDataModelValueSet.DiagnosisActiveChoroidalDegenerations,
                        QualityDataModelValueSet.DiagnosisActiveOpticNeuritis,
                        QualityDataModelValueSet.DiagnosisActiveOtherBackgroundRetinopathyAndRetinalVascularChanges,
                        QualityDataModelValueSet.DiagnosisActiveOtherCornealDeformities,
                        QualityDataModelValueSet.DiagnosisActiveCloudyCornea,
                        QualityDataModelValueSet.DiagnosisActiveOtherRetinalDisorders,
                        QualityDataModelValueSet.DiagnosisActivePriorPenetratingKeratoplasty,
                        QualityDataModelValueSet.DiagnosisActiveOtherAndUnspecifiedFormsOfChorioretinitisAndRetinochoroiditis,
                        QualityDataModelValueSet.DiagnosisActiveAmblyopia,
                        QualityDataModelValueSet.DiagnosisActiveCornealEdema,
                        QualityDataModelValueSet.DiagnosisActiveNystagmusAndOtherIrregularEyeMovements,
                        QualityDataModelValueSet.DiagnosisActiveModerateOrSevereImpairmentBetterEyeProfoundImpairmentLesserEye,
                        QualityDataModelValueSet.DiagnosisActiveHereditaryChoroidalDystrophies,
                        QualityDataModelValueSet.DiagnosisActiveHereditaryRetinalDystrophies,
                        QualityDataModelValueSet.DiagnosisActiveBurnConfinedToEyeAndAdnexa,
                        QualityDataModelValueSet.DiagnosisActiveScleritisAndEpiscleritis,
                        QualityDataModelValueSet.DiagnosisActiveOtherDisordersOfSclera,
                        QualityDataModelValueSet.DiagnosisActiveFocalChorioretinitisAndFocalRetinochoroiditis,
                        QualityDataModelValueSet.DiagnosisActiveDegenerativeDisordersOfGlobe,
                        QualityDataModelValueSet.DiagnosisActiveDegenerationOfMaculaAndPosteriorPole,
                        QualityDataModelValueSet.DiagnosisActiveDisseminatedChorioretinitisAndDisseminatedRetinochoroiditis,
                        QualityDataModelValueSet.DiagnosisActiveDiabeticMacularEdema,
                        QualityDataModelValueSet.DiagnosisActiveUveitis,
                        QualityDataModelValueSet.DiagnosisActiveProfoundImpairmentBothEyes,
                        QualityDataModelValueSet.DiagnosisActiveOtherProliferativeRetinopathy,
                        QualityDataModelValueSet.DiagnosisActiveCataractSecondaryToOcularDisorders,
                        QualityDataModelValueSet.DiagnosisActiveDisordersOfVisualCortex,
                        QualityDataModelValueSet.DiagnosisActiveChoroidalDetachment,
                        QualityDataModelValueSet.DiagnosisActiveDisordersOfOpticChiasm,
                        QualityDataModelValueSet.DiagnosisActiveInjuryToOpticNerveAndPathways,
                        QualityDataModelValueSet.DiagnosisActiveChoroidalHemorrhageAndRupture,
                        QualityDataModelValueSet.DiagnosisActiveOtherDisordersOfOpticNerve,
                        QualityDataModelValueSet.DiagnosisActiveGlaucoma,
                        QualityDataModelValueSet.DiagnosisActiveChorioretinalScars,
                        QualityDataModelValueSet.DiagnosisActiveHereditaryCornealDystrophies,
                        QualityDataModelValueSet.DiagnosisActiveCentralCornealUlcer,
                        QualityDataModelValueSet.DiagnosisActiveCornealOpacityAndOtherDisordersOfCornea,
                        QualityDataModelValueSet.DiagnosisActiveOpticAtrophy,
                        QualityDataModelValueSet.DiagnosisActiveOpenWoundOfEyeball,
                        QualityDataModelValueSet.DiagnosisActiveOtherEndophthalmitis,
                        QualityDataModelValueSet.DiagnosisActiveSeparationOfRetinalLayers,
                        QualityDataModelValueSet.DiagnosisActiveDiabeticRetinopathy,
                        QualityDataModelValueSet.DiagnosisActiveVisualFieldDefects))
                    .Where(m => m.ValueSetEntry != null)
                    .ToArray();

                //Gets the patient's Visual Acuity for the cataract surgical eye, that was documented within 90 days after the cataract surgery, and has an acuity of 20/40 or better.  This is the numerator of the measure.
                model.ResultObservationPhysicalExamFindingEntryModels =
                    model.Patient.PatientDiagnosticTestPerformeds
                        .SelectMany(d => d.PatientDiagnosticTestPerformedQuestionAnswers.Select(qa => new
                                                                                                      {
                                                                                                          Answer = qa,
                                                                                                          Test = d,
                                                                                                          Procedure = procedures.FirstOrDefault(p => p.Answer.Laterality == qa.Laterality),
                                                                                                          Result = qa.QuestionAnswerValues.OfType<LinkedQuestionAnswerValue>().Select(v => (IHasId)EntityLinkService.GetMappedEntity(v))
                                                                                                              .Select(e => Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(e, QualityDataModelValueSet.DiagnosticStudyResultVisualAcuity2040OrBetter))
                                                                                                              .WhereNotDefault().FirstOrDefault()
                                                                                                      }))
                        .Where(i => i.Procedure != null && i.Test.PerformedDateTime <= i.Procedure.Procedure.PerformedDateTime.AddDays(90))
                        .Select(t => new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultModel
                                     {
                                         Start = t.Test.PerformedDateTime,
                                         End = t.Test.PerformedDateTime,
                                         RootAndExtension = t.Answer.GetOidRootAndExtension(QualityDataModelValueSet.PhysicalExamFindingBestCorrectedVisualAcuity.ToString()),
                                         ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModels(QualityDataModelValueSet.PhysicalExamFindingBestCorrectedVisualAcuity).FirstOrDefault(),
                                         Laterality = t.Procedure.Laterality,
                                         ResultValueSetEntry = t.Result,
                                     })
                        .Where(m => m.ValueSetEntry != null)
                        .ToArray();
            }
        }

        /// <summary>
        /// Produces information for a specific measure.
        /// </summary>
        private abstract class ModelProducer
        {
            protected IEntityLinkService EntityLinkService { get; private set; }
            protected IPracticeRepository PracticeRepository { get; private set; }
            protected QrdaCategory1Queries Queries { get; private set; }
            protected CdaModelMapper Mapper { get; private set; }

            public ModelProducer(IPracticeRepository practiceRepository, QrdaCategory1Queries queries, CdaModelMapper mapper, IEntityLinkService entityLinkService)
            {
                PracticeRepository = practiceRepository;
                Queries = queries;
                Mapper = mapper;
                EntityLinkService = entityLinkService;
            }

            public abstract int[] FindPatients();

            public virtual void FillModel(QrdaCategory1Model model)
            {
                model.SupplementalDataPatientPayerEntryModels =
                    model.Patient.PatientInsurances.Where(pi => pi.InsuranceType == InsuranceType.MajorMedical && (pi.EndDateTime == null || pi.EndDateTime >= model.MeasurementPeriodEndDate)
                                                                && pi.InsurancePolicy.StartDateTime <= model.MeasurementPeriodEndDate)
                        .OrderBy(pi => pi.OrdinalId)
                        .Select(pi => new QrdaCategory1EntryWithValueSetEntryModel
                                      {
                                          ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pi.InsurancePolicy.Insurer.ClaimFilingIndicatorCode, QualityDataModelValueSet.PatientCharacteristicPayerPayer),
                                          RootAndExtension = pi.InsurancePolicy.Insurer.GetOidRootAndExtension(QualityDataModelValueSet.PatientCharacteristicPayerPayer.ToString())
                                      })
                        .Concat(new[]
                                {
                                    // for null flavor
                                    new QrdaCategory1EntryWithValueSetEntryModel()
                                })
                        .Take(1).ToArray();
            }

            /// <summary>
            /// Gets a patient's billing services, and start and end dates, for the specified value sets.
            /// </summary>
            /// <param name="bs"></param>
            /// <param name="valueSets"></param>
            /// <returns></returns>
            protected QrdaCategory1EntryWithValueSetEntryAndDateRangeModel ToQrdaCategory1EntryWithValueSetEntryAndDateRangeModel(BillingService bs, params QualityDataModelValueSet[] valueSets)
            {
                return new QrdaCategory1EntryWithValueSetEntryAndDateRangeModel
                       {
                           Start = bs.Invoice.Encounter.StartDateTime,
                           End = bs.Invoice.Encounter.EndDateTime,
                           RootAndExtension = bs.GetOidRootAndExtension(),
                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(bs.EncounterService, valueSets)
                       };
            }

            /// <summary>
            /// Gets a patient's active diagnoses with start and end dates and status.  Does not bring back laterality or severity.
            /// </summary>
            /// <param name="pdd"></param>
            /// <param name="valueSets"></param>
            /// <returns></returns>
            protected ProblemObservationDiagnosisActiveEntryModel ToProblemObservationDiagnosisActiveEntryModel(PatientDiagnosisDetail pdd, params QualityDataModelValueSet[] valueSets)
            {
                return new ProblemObservationDiagnosisActiveEntryModel
                                          {
                                              ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pdd.PatientDiagnosis.ClinicalCondition, valueSets),
                                              Start = pdd.StartDateTime,
                                              End = pdd.EndDateTime,
                                              RootAndExtension = pdd.PatientDiagnosis.GetOidRootAndExtension(valueSets.Join(".")),
                                              Laterality = null,
                                              Severity = null,
                                              Status = Mapper.SnomedModelMapper.GetSnomedModelMap(new[] { pdd.ClinicalConditionStatus }, Constants.SnomedClinicalConditionStatusExternalEntityName).Select(i => i.Value).FirstOrDefault(),
                                          };
            }

            /// <summary>
            /// Gets a patient's procedures and laterality (which eye), and start and end dates for the specified value sets.
            /// </summary>
            /// <param name="qa"></param>
            /// <param name="laterality"></param>
            /// <param name="valueSets"></param>
            /// <returns></returns>
            protected QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel(PatientProcedurePerformedQuestionAnswer qa, SnomedDescriptionModel laterality, params QualityDataModelValueSet[] valueSets)
            {
                return new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndResultAndNotDoneModel
                       {
                           Start = qa.PatientProcedurePerformed.PerformedDateTime,
                           End = qa.PatientProcedurePerformed.PerformedDateTime,
                           Laterality = laterality,
                           RootAndExtension = qa.GetOidRootAndExtension(),
                           ValueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(qa.PatientProcedurePerformed.ClinicalProcedure, valueSets)
                       };
            }

            /// <summary>
            /// Gets patient's medications and start date for the specified value sets.
            /// </summary>
            /// <param name="pmd"></param>
            /// <param name="valueSets"></param>
            /// <returns></returns>
            protected QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(PatientMedicationDetail pmd, params QualityDataModelValueSet[] valueSets)
            {
                var valueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(pmd.PatientMedication.Medication, valueSets);

                return new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel
                       {
                           Start = pmd.MedicationEventDateTime,
                           End = null,
                           Description = valueSetEntry != null ? valueSetEntry.Description : null,
                           RootAndExtension = pmd.PatientMedication.GetOidRootAndExtension(valueSets.Join(".")),
                           ValueSetEntry = valueSetEntry
                       };
            }

            /// <summary>
            /// Gets patient's medications and start date for the specified value sets.
            /// </summary>
            /// <param name="emo"></param>
            /// <param name="valueSets"></param>
            /// <returns></returns>
            protected QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel ToQrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel(EncounterMedicationOrder emo, params QualityDataModelValueSet[] valueSets)
            {
                var valueSetEntry = Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(emo.PatientMedicationDetail.PatientMedication.Medication, valueSets);

                return new QrdaCategory1EntryWithValueSetEntryAndDateRangeAndDescriptionModel
                       {
                           Start = emo.Encounter.StartDateTime,
                           End = emo.Encounter.EndDateTime,
                           RootAndExtension = emo.GetOidRootAndExtension(valueSets.Join(".")),
                           Description = valueSetEntry != null ? valueSetEntry.Description : null,
                           ValueSetEntry = valueSetEntry
                       };
            }

            /// <summary>
            /// Gets the value set entry from a set of linked question answer values for the specified value sets.
            /// </summary>
            /// <param name="answers">The answers.</param>
            /// <param name="valueSets">The value sets.</param>
            /// <returns></returns>
            protected QualityDataModelValueSetEntryModel GetLinkedAnswerMappedValueSetEntry(IEnumerable<QuestionAnswer> answers, params QualityDataModelValueSet[] valueSets)
            {
                return answers.SelectMany(a => a.QuestionAnswerValues).OfType<LinkedQuestionAnswerValue>().Select(v => (IHasId)EntityLinkService.GetMappedEntity(v)).WhereNotDefault()
                    .Select(e => Mapper.QualityDataModelValueSetModelMapper.GetQualityDataModelValueSetEntryModel(e, valueSets)).FirstOrDefault();
            }



        }
    }
}