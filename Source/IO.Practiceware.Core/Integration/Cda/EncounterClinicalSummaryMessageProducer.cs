﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    /// Produces CDA messages that contain visit summary documents.
    /// </summary>
    /// <remarks>
    /// Any static string values that are hardcoded come from Michelle's spec in TFS.
    /// The specifications are named VisitSummSpec.xml and TocSpec.xml
    /// </remarks>
    public class EncounterClinicalSummaryMessageProducer
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly CdaMessageProducerUtilities _cdaUtilities;
        private readonly CdaModelMapper _cdaModelMapper;

        public EncounterClinicalSummaryMessageProducer(IPracticeRepository practiceRepository, CdaMessageProducerUtilities cdaUtilities, CdaModelMapper cdaModelMapper)
        {
            _practiceRepository = practiceRepository;
            _cdaUtilities = cdaUtilities;
            _cdaModelMapper = cdaModelMapper;
            DocumentEffectiveTime = DateTime.Now.ToClientTime();
        }

        /// <summary>
        /// Produces Cda messages containing a visit summary document using the given discharged encounter ids.
        /// </summary>
        public IMessage[] ProduceMessages(List<int> encounterIds)
        {
            var practiceName = Application.UserContext.Current.UserDetails.BillingOrganizationName ?? _practiceRepository.BillingOrganizations.FirstOrDefault(b => b.IsMain).IfNotNull(b => b.Name);
            
            var result = new List<IMessage>();
            var allPatients = _practiceRepository.Encounters
                .Where(e => encounterIds.Contains(e.Id))
                .Select(e => e.Patient)
                .Distinct()
                .ToArray();
            var patientIds = allPatients.Select(p => p.Id).ToList();
            var allEncounters = _practiceRepository.Encounters
                .Where(e => patientIds.Contains(e.PatientId))
                .ToArray();
            _cdaUtilities.LoadForCdaMessageProduction(allEncounters);
            _cdaUtilities.LoadForCdaMessageProduction(allPatients);

            foreach (var encounterId in encounterIds)
            {
                var dischargedEncounterId = encounterId;
                var dischargedEncounter = allEncounters.First(i => i.Id == dischargedEncounterId);
                var patient = allPatients.First(p => p.Id == dischargedEncounter.PatientId);
                var encounters = allEncounters.Where(e => e.PatientId == patient.Id).ToArray();

                var document = CdaMessageProducerUtilities.AddClinicalDocument(DocumentEffectiveTime, practiceName, DocumentId);
                CdaMessageProducerUtilities.InitializeCdaModelMapper(_cdaModelMapper, patient, encounters);

                // TODO Using the assumption that there is only one user per encounter.
                // TODO When an encounter can support multiple users, we need to update the logic for how we get the billing organization
                var billingOrganization = dischargedEncounter.ClinicalInvoiceProviders.First(i => i.Provider.UserId.HasValue).Provider.BillingOrganization;

                var patientRole = _cdaUtilities.AddNewPatientRole(patient).SetProviderOrganization(CdaMessageProducerUtilities.AddNewProviderOrganization(billingOrganization));
                document.AddRecordTarget().SetPatientRole(patientRole);
                document.AddAuthor(billingOrganization, DocumentEffectiveTime);
                document.AddCustodian(billingOrganization);
                CdaMessageProducerUtilities.AddDocumentationOf(document, encounters.Where(e => e.EncounterStatus == EncounterStatus.Discharged));
                document.componentOf = CreateComponentOf(dischargedEncounter);

                var structuredBody = document.AddComponent().AddStructuredBody();
                structuredBody.AddComponent(_cdaUtilities.AddNewAllergiesComponent(patient.PatientAllergens))
                              .AddComponent(_cdaUtilities.AddNewMedicationsComponent(patient.PatientMedications))
                              .AddComponent(_cdaUtilities.AddNewProblemsComponent(patient.PatientDiagnoses))
                              .AddComponent(_cdaUtilities.AddNewProceduresComponent(patient))
                              .AddComponent(_cdaUtilities.AddNewLabResultsComponent(dischargedEncounter.EncounterLaboratoryTestOrders.Select(i => i.PatientLaboratoryTestResult)))
                              .AddComponent(AddNewPlanOfCareComponent(encounters))
                              .AddComponent(_cdaUtilities.AddNewVitalSignsComponent(new[] { dischargedEncounter }))
                              .AddComponent(AddNewMedicationsAdministeredComponent(dischargedEncounter.EncounterAdministeredMedications))
                              .AddComponent(AddNewReasonForVisitComponent(dischargedEncounter))
                              .AddComponent(_cdaUtilities.AddNewSocialHistoryComponent(patient.PatientSmokingStatus.Where(ss => ss.EncounterId == dischargedEncounterId)))
                              .AddComponent(AddNewInstructionsComponent(dischargedEncounter))
                              .AddComponent(_cdaUtilities.AddNewImmunizationsComponent(patient.PatientVaccinations));

                document.EnsureDocumentTableBodiesAreValid();

                var cdaMessage = new CdaMessage(Guid.NewGuid()) { EncounterId = dischargedEncounterId.ToString() };
                cdaMessage.Load(document.SerializeToString());
                result.Add(cdaMessage);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Produces an IMessage containing a visit summary document using the given discharged encounter id.
        /// </summary>
        /// <param name="dischargedEncounterId"></param>
        /// <returns></returns>
        public IMessage ProduceMessage(int dischargedEncounterId)
        {
            return ProduceMessages(new List<int> { dischargedEncounterId }).Single();
        }

        #region Components used exclusively in the Transition of Care CDA document

        private static POCD_MT000040Component1 CreateComponentOf(Encounter encounter)
        {
            var componentOf = new POCD_MT000040Component1();
            var encompassingEncounter = componentOf.AddEncompassingEncounter();
            encompassingEncounter.AddId().SetRootAndExtension(encounter);
            encompassingEncounter.AddEffectiveTime().AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));

            var encounterParticipant = encompassingEncounter.AddEncounterParticipant().SetTypeCode(x_EncounterParticipant.ATND);
            encounterParticipant.AddTime().AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));

            var billingOrganization = encounter.ClinicalInvoiceProviders.First().Provider.BillingOrganization;
            var assignedEntity = encounterParticipant.AddAssignedEntity();
            assignedEntity.AddId().SetExtension(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                                                            .SetRoot("2.16.840.1.113883.4.6");
            assignedEntity.AddCode().SetCode(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy))
                                    .SetCodeSystem(CodeSystem.Nucc);
            assignedEntity.AddAddress().SetValues(billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation));
            assignedEntity.AddTelecom().SetPhoneNumber(billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).ToString())
                                       .SetUse("WP");

            var user = encounter.ClinicalInvoiceProviders.First().Provider.User;
            assignedEntity.AddAssignedPerson().AddName().AddFirstName(user.FirstName)
                                                        .AddLastName(user.LastName);

            var healthCareFacility = encompassingEncounter.AddLocation().AddHealthCareFacility();
            healthCareFacility.AddId().SetExtension(encounter.ServiceLocation.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                                      .SetRoot("2.16.840.1.113883.19");
            healthCareFacility.AddCode().SetCode(encounter.ServiceLocation.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy))
                .SetCodeSystem(CodeSystem.Nucc);

            var location = healthCareFacility.AddLocation();
            location.AddName(encounter.ServiceLocation.Name);
            location.AddAddress().SetValues(encounter.ServiceLocation.ServiceLocationAddresses.WithAddressType(ServiceLocationAddressTypeId.MainOffice));

            return componentOf;
        }

        #region Plan Of Care

        private POCD_MT000040Component3 AddNewPlanOfCareComponent(IEnumerable<Encounter> source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.10");
            section.AddCode().SetCode("18776-5").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Treatment Plan");
            section.AddTitle("Plan Of Care");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Name");
            tr.AddTh("Type");
            tr.AddTh("Date");
            var body = table.AddTbody();

            var encounters = source.ToArray();
            InitializePlanOfCareComponentTableRows(body, encounters);
            InitializePlanOfCareComponentEntries(section, encounters);

            return component;
        }

        private void InitializePlanOfCareComponentEntries(POCD_MT000040Section section, IEnumerable<Encounter> source)
        {
            foreach (var encounter in source.OrderByDescending(i => i.StartDateTime))
            {
                var encounterDateText = CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime);

                if (encounter.EncounterStatus == EncounterStatus.Pending)
                {
                    foreach (var appointment in encounter.Appointments.OfType<UserAppointment>())
                    {
                        var billingOrganization = encounter.ClinicalInvoiceProviders.First(i => i.Provider.UserId.HasValue).Provider.BillingOrganization;
                        var billingOrgAddress = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);

                        var nameText = "{0} {1} {2}".FormatWith(appointment.User.FirstName, appointment.User.LastName, appointment.User.Honorific);
                        var phoneText = billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).ToString();
                        var addressText = "{0} {1} {2} {3} {4}".FormatWith(billingOrgAddress.Line1, billingOrgAddress.Line2, billingOrgAddress.City, billingOrgAddress.StateOrProvince.Abbreviation, billingOrgAddress.PostalCode);
                        var reasonForVisitDataText = encounter.EncounterReasonForVisits.Select(rv => rv.ReasonForVisit.Name).Where(name => name.IsNotNullOrEmpty()).Distinct().Join(". ");
                        var reasonForVisitNoteText = encounter.EncounterReasonForVisitComments.Select(i => i.Value).Join(". ");

                        var encounterElement = section.AddEntry().AddEncounter().SetMoodCode(x_DocumentEncounterMood.INT).SetClassCode("ENC");
                        encounterElement.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.40");
                        encounterElement.AddId().SetRootAndExtension(appointment);
                        encounterElement.AddText().SetText("{0}, {1}, {2}, {3}, {4}, {5}".FormatWith(nameText, phoneText, addressText, encounterDateText, reasonForVisitDataText, reasonForVisitNoteText));
                        encounterElement.AddEffectiveTime().AddCenter().SetValue(encounterDateText);
                    }
                }

                if (encounter.EncounterStatus == EncounterStatus.Discharged)
                {
                    foreach (var transitionOfCareOrder in encounter.EncounterTransitionOfCareOrders.Where(e => e.ExternalProvider != null))
                    {
                        var contact = transitionOfCareOrder.ExternalProvider;
                        // Cannot use WithAddressType because of name mismatch on ContactAddressTypeId to ExternalContactAddressTypeId.  (The convention is to have the same name such as, ExternalContactAddressTypeId).
                        var address = contact.ExternalContactAddresses.FirstOrDefault(i => i.ContactAddressTypeId == (int)ExternalContactAddressTypeId.MainOffice);
                        address.EnsureNotDefault(new InvalidOperationException("No address exists for the external provider {0}".FormatWith(contact.DisplayName)));

                        var nameText = "{0} {1} {2}".FormatWith(contact.FirstName, contact.LastNameOrEntityName, contact.Honorific);
                        var phoneText = contact.ExternalContactPhoneNumbers.WithPhoneNumberType(ExternalContactPhoneNumberTypeId.Main).ToString();
                        if (address != null)
                        {
                            var addressText = "{0} {1} {2} {3} {4}".FormatWith(address.Line1, address.Line2, address.City, address.StateOrProvince.Abbreviation, address.PostalCode);
                            var reasonText = transitionOfCareOrder.TransitionOfCareReason.IfNotNull(r => r.Value) ?? string.Empty;

                            var encounterElement = section.AddEntry().AddEncounter().SetMoodCode(x_DocumentEncounterMood.PRP).SetClassCode("ENC");
                            encounterElement.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.40");
                            encounterElement.AddId().SetRootAndExtension(transitionOfCareOrder);
                            encounterElement.AddText().SetText("{0}, {1}, {2}, {3}".FormatWith(nameText, phoneText, addressText, reasonText))
                                .AddReference().SetValue("Referral_{0}".FormatWith(section.entry.Length));
                            encounterElement.AddEffectiveTime().AddCenter().SetValue(transitionOfCareOrder.SentDateTime.HasValue ? CdaExtensions.FormatForCdaAlternate(transitionOfCareOrder.SentDateTime.Value) : encounterDateText);
                        }
                    }

                    var clinicalProcedures = encounter.EncounterDiagnosticTestOrders.Where(i => !i.PatientDiagnosticTestPerformedId.HasValue && !i.AppointmentId.HasValue)
                                                      .Select(i => i.DiagnosticTestOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var encounterDiagnosticTestOrder = encounter.EncounterDiagnosticTestOrders.First(i => i.DiagnosticTestOrder.ClinicalProcedure == clinicalProcedureMapping.Key);
                        var snomedModel = clinicalProcedureMapping.Value;

                        var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.INT);
                        observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.44");
                        observation.AddId().SetRootAndExtension(encounterDiagnosticTestOrder);
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            observation.AddCode<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedureMapping.Key.Name);
                        }
                        else
                        {
                            observation.AddCode<CD>().SetCode(snomedModel.ConceptId)
                                       .SetCodeSystem(CodeSystem.SnomedCt)
                                       .SetDisplayName(snomedModel.Term);
                        }
                        observation.AddStatusCode().SetCode("new");
                        observation.AddEffectiveTime().AddCenter().SetValue(encounterDateText);
                    }


                    clinicalProcedures = encounter.EncounterDiagnosticTestOrders.Where(i => !i.PatientDiagnosticTestPerformedId.HasValue && i.AppointmentId.HasValue)
                                                      .Select(i => i.DiagnosticTestOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var encounterDiagnosticTestOrder = encounter.EncounterDiagnosticTestOrders.First(i => i.DiagnosticTestOrder.ClinicalProcedure == clinicalProcedureMapping.Key);
                        var snomedModel = clinicalProcedureMapping.Value;

                        var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.INT);
                        observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.44");
                        observation.AddId().SetRootAndExtension(encounterDiagnosticTestOrder);
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            observation.AddCode().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedureMapping.Key.Name);
                        }
                        else
                        {
                            observation.AddCode().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                        }
                        observation.AddStatusCode().SetCode("new");
                        observation.AddEffectiveTime().AddCenter().SetValue(encounterDateText);
                    }


                    var labTests = encounter.EncounterLaboratoryTestOrders.Where(i => !i.PatientLaboratoryTestResultId.HasValue && i.Encounter.Appointments.Any())
                                            .Select(i => i.LaboratoryTestOrder.LaboratoryTest);

                    foreach (var labTestMapping in _cdaModelMapper.GetLoincDescriptionMap(labTests))
                    {
                        var labTestOrder = encounter.EncounterLaboratoryTestOrders.First(i => i.LaboratoryTestOrder.LaboratoryTest == labTestMapping.Key);
                        var loincModel = labTestMapping.Value;

                        var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.INT);
                        observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.44");
                        observation.AddId().SetRootAndExtension(labTestOrder);
                        if (loincModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Laboratory Test {0} (Id {1}) mapping to LOINC was not found.".FormatWith(labTestMapping.Key.Name, labTestMapping.Key.Id));
                            observation.AddCode<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(labTestMapping.Key.Name);
                        }
                        else
                        {
                            observation.AddCode<CD>().SetCode(loincModel.LoincNum)
                                       .SetCodeSystem(CodeSystem.SnomedCt)
                                       .SetDisplayName(loincModel.Component);
                        }
                        observation.AddStatusCode().SetCode("new");
                        observation.AddEffectiveTime().AddCenter().SetValue(encounterDateText);
                    }


                    clinicalProcedures = encounter.EncounterProcedureOrders.Where(i => !i.PatientProcedurePerformedId.HasValue && i.Encounter.Appointments.Any())
                                                  .Select(i => i.ProcedureOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var procedureOrder = encounter.EncounterProcedureOrders.First(i => i.ProcedureOrder.ClinicalProcedure == clinicalProcedureMapping.Key);
                        var snomedModel = clinicalProcedureMapping.Value;

                        var procedure = section.AddEntry().AddProcedure().SetMoodCode(x_DocumentProcedureMood.INT).SetClassCode("PROC");
                        procedure.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.41");
                        procedure.AddId().SetRootAndExtension(procedureOrder);
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            procedure.AddCode().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedureMapping.Key.Name);
                        }
                        else
                        {
                            procedure.AddCode().SetCode(snomedModel.ConceptId)
                                     .SetCodeSystem(CodeSystem.SnomedCt)
                                     .SetDisplayName(snomedModel.Term);
                        }
                        procedure.AddStatusCode().SetCode("new");
                        procedure.AddEffectiveTime().AddCenter().SetValue(encounterDateText);
                    }

                    var treatmentGoalAndInstructions = encounter.EncounterTreatmentGoalAndInstructions.Select(i => i.TreatmentGoalAndInstruction).ToArray();
                    var treatmentGoalMappings = _cdaModelMapper.GetSnomedModelMap(treatmentGoalAndInstructions.Select(i => i.TreatmentGoal));

                    foreach (var treatmentGoalAndInstruction in treatmentGoalAndInstructions)
                    {
                        var snomedModel = treatmentGoalMappings[treatmentGoalAndInstruction.TreatmentGoal];

                        var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.GOL);
                        observation.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.44");

                        var encounterTreatmentGoalAndInstruction = encounter.EncounterTreatmentGoalAndInstructions.First(i => i.TreatmentGoalAndInstruction == treatmentGoalAndInstruction);
                        observation.AddId().SetRootAndExtension(encounterTreatmentGoalAndInstruction, "G");
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Treatment Goal {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, treatmentGoalAndInstruction.TreatmentGoal.Id));
                            observation.AddCode().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(treatmentGoalAndInstruction.TreatmentGoal.DisplayName);
                        }
                        else
                        {
                            observation.AddCode().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                        }
                        observation.AddStatusCode().SetCode("new");
                        observation.AddEffectiveTime().AddCenter().SetValue(encounterDateText);

                        var act = section.AddEntry().AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.INT);
                        act.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.20");
                        act.AddId().SetRootAndExtension(encounterTreatmentGoalAndInstruction, "I");
                        act.AddCode<CE>().SetCode("311401005").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Patient Education");
                        act.AddText(encounterTreatmentGoalAndInstruction.Value).AddReference().SetValue("#Instructions_{0}".FormatWith(section.entry.Length));
                        act.AddStatusCode().SetCode("completed");
                    }
                }
            }
        }

        private void InitializePlanOfCareComponentTableRows(StrucDocTbody body, IEnumerable<Encounter> source)
        {
            foreach (var encounter in source.OrderByDescending(i => i.StartDateTime))
            {

                var encounterDateText = CdaExtensions.FormatForCdaTextElement(encounter.StartDateTime);
                if (encounter.EncounterStatus == EncounterStatus.Pending)
                {
                    foreach (var appointment in encounter.Appointments.OfType<UserAppointment>())
                    {
                        var billingOrganization = encounter.ClinicalInvoiceProviders.First(i => i.Provider.UserId.HasValue).Provider.BillingOrganization;
                        var billingOrgAddress = billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation);

                        var nameText = "{0} {1} {2}".FormatWith(appointment.User.FirstName, appointment.User.LastName, appointment.User.Honorific);
                        var phoneText = billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).ToString();
                        var addressText = "{0} {1} {2} {3} {4}".FormatWith(billingOrgAddress.Line1, billingOrgAddress.Line2, billingOrgAddress.City, billingOrgAddress.StateOrProvince.Abbreviation, billingOrgAddress.PostalCode);
                        var reasonForVisitDataText = encounter.EncounterReasonForVisits.Select(rv => rv.ReasonForVisit.Name).Where(name => name.IsNotNullOrEmpty()).Distinct().Join(". ");
                        var reasonForVisitNoteText = encounter.EncounterReasonForVisitComments.Select(i => i.Value).Join(". ");

                        var tr = body.AddTr();
                        tr.AddTd().SetId("FutureAppointment_{0}".FormatWith(body.tr.Length))
                            .SetText(new List<string> { nameText, phoneText, addressText, encounterDateText, reasonForVisitDataText, reasonForVisitNoteText }.Join(excludeEmptyItems: true));
                        tr.AddTd().SetText("Future Appointment");
                        tr.AddTd().SetText(encounterDateText);
                    }
                }

                if (encounter.EncounterStatus == EncounterStatus.Discharged)
                {
                    foreach (var transitionOfCareOrder in encounter.EncounterTransitionOfCareOrders.Where(e => e.ExternalProvider != null))
                    {
                        var contact = transitionOfCareOrder.ExternalProvider;
                        // Cannot use WithAddressType because of name mismatch on ContactAddressTypeId to ExternalContactAddressTypeId.  (The convention is to have the same name such as, ExternalContactAddressTypeId).
                        var address = contact.ExternalContactAddresses.FirstOrDefault(i => i.ContactAddressTypeId == (int)ExternalContactAddressTypeId.MainOffice);
                        if (address == null) throw new InvalidOperationException("No address exists for the external provider {0}".FormatWith(contact.DisplayName));

                        var nameText = "{0} {1} {2}".FormatWith(contact.FirstName, contact.LastNameOrEntityName, contact.Honorific);
                        var phoneText = contact.ExternalContactPhoneNumbers.WithPhoneNumberType(ExternalContactPhoneNumberTypeId.Main).ToString();
                        var addressText = "{0} {1} {2} {3} {4}".FormatWith(address.Line1, address.Line2, address.City, address.StateOrProvince.Abbreviation, address.PostalCode);
                        var reasonText = transitionOfCareOrder.TransitionOfCareReason.IfNotNull(r => r.Value) ?? string.Empty;
                        var tr = body.AddTr();
                        tr.AddTd().SetId("Referral_{0}".FormatWith(body.tr.Length))
                            .SetText(new List<string> { nameText, phoneText, addressText, reasonText }.Join(excludeEmptyItems: true));
                        tr.AddTd().SetText("Referral");
                        tr.AddTd().SetText(transitionOfCareOrder.SentDateTime.HasValue ? CdaExtensions.FormatForCdaTextElement(transitionOfCareOrder.SentDateTime.Value) : encounterDateText);
                    }

                    var clinicalProcedures = encounter.EncounterDiagnosticTestOrders.Where(i => !i.PatientDiagnosticTestPerformedId.HasValue && !i.AppointmentId.HasValue)
                                                      .Select(i => i.DiagnosticTestOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var snomedModel = clinicalProcedureMapping.Value;

                        var tr = body.AddTr();
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            tr.AddTd().SetId("PendingTest_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(clinicalProcedureMapping.Key.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                        }
                        else
                        {
                            tr.AddTd().SetId("PendingTest_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                        }
                        tr.AddTd().SetText("Pending Diagnostic Test");
                        tr.AddTd().SetText(encounterDateText);
                    }

                    clinicalProcedures = encounter.EncounterDiagnosticTestOrders.Where(i => !i.PatientDiagnosticTestPerformedId.HasValue && i.AppointmentId.HasValue)
                                                  .Select(i => i.DiagnosticTestOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var snomedModel = clinicalProcedureMapping.Value;

                        var tr = body.AddTr();
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            tr.AddTd().SetId("FutureTest_{0}".FormatWith(body.tr.Length))
                            .SetText("{0}, {1}: {2}".FormatWith(clinicalProcedureMapping.Key.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                        }
                        else
                        {
                            tr.AddTd().SetId("FutureTest_{0}".FormatWith(body.tr.Length))
                            .SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                        }

                        tr.AddTd().SetText("Future Scheduled Test");
                        tr.AddTd().SetText(encounterDateText);
                    }

                    var labTests = encounter.EncounterLaboratoryTestOrders.Where(i => !i.PatientLaboratoryTestResultId.HasValue && i.Encounter.Appointments.Any())
                                            .Select(i => i.LaboratoryTestOrder.LaboratoryTest);

                    foreach (var labTestMapping in _cdaModelMapper.GetLoincDescriptionMap(labTests))
                    {
                        var loincModel = labTestMapping.Value;

                        var tr = body.AddTr();
                        tr.AddTd().SetId("FutureLaboratoryTests_{0}".FormatWith(body.tr.Length))
                          .SetText("{0}, {1}: {2}".FormatWith(loincModel.Component, Constants.LoincExternalSystemName, loincModel.LoincNum));
                        tr.AddTd().SetText("Future Scheduled Test");
                        tr.AddTd().SetText(encounterDateText);
                    }

                    clinicalProcedures = encounter.EncounterProcedureOrders.Where(i => !i.PatientProcedurePerformedId.HasValue && i.Encounter.Appointments.Any())
                                                  .Select(i => i.ProcedureOrder.ClinicalProcedure);

                    foreach (var clinicalProcedureMapping in _cdaModelMapper.GetSnomedModelMap(clinicalProcedures))
                    {
                        var snomedModel = clinicalProcedureMapping.Value;

                        var tr = body.AddTr();
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Clinical Procedure {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(clinicalProcedureMapping.Key.Name, clinicalProcedureMapping.Key.Id));
                            tr.AddTd().SetId("FutureProcedure_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(clinicalProcedureMapping.Key.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                        }
                        else
                        {
                            tr.AddTd().SetId("FutureProcedure_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                        }
                        tr.AddTd().SetText("Future Scheduled Procedure");
                        tr.AddTd().SetText(encounterDateText);
                    }

                    var treatmentGoalAndInstructions = encounter.EncounterTreatmentGoalAndInstructions.Select(i => i.TreatmentGoalAndInstruction).ToArray();
                    var treatmentGoalMappings = _cdaModelMapper.GetSnomedModelMap(treatmentGoalAndInstructions.Select(i => i.TreatmentGoal));

                    foreach (var treatmentGoalAndInstruction in treatmentGoalAndInstructions)
                    {
                        var snomedModel = treatmentGoalMappings[treatmentGoalAndInstruction.TreatmentGoal];

                        var tr = body.AddTr();
                        if (snomedModel == null)
                        {
                            CdaMessageProducerUtilities.VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Treatment Goal {0} (Id {1}) mapping to SNOMED was not found.".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, treatmentGoalAndInstruction.TreatmentGoal.Id));
                            tr.AddTd().SetId("Goal_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(treatmentGoalAndInstruction.TreatmentGoal.DisplayName, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                        }
                        else
                        {
                            tr.AddTd().SetId("Goal_{0}".FormatWith(body.tr.Length))
                                .SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                        }
                        tr.AddTd().SetText("Goal");
                        tr.AddTd().SetText(encounterDateText);

                        tr = body.AddTr();
                        tr.AddTd().SetId("Instructions_{0}".FormatWith(body.tr.Length))
                          .SetText("{0}".FormatWith(treatmentGoalAndInstruction.Value));
                        tr.AddTd().SetText("Instruction");
                        tr.AddTd().SetText(encounterDateText);
                    }
                }
            }
        }

        #endregion

        #region Medications Administered

        private POCD_MT000040Component3 AddNewMedicationsAdministeredComponent(IEnumerable<EncounterAdministeredMedication> source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.38");
            section.AddCode().SetCode("29549-3").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("MEDICATIONS ADMINISTERED");
            section.AddTitle("Medications Administered");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Medications");
            tr.AddTh("Date Administered");
            tr.AddTh("Medication Info");

            var body = table.AddTbody();

            var invalidMedicationEventTypes = new[]
                {
                    MedicationEventType.PatientDiscontinued, 
                    MedicationEventType.ExternalProviderDiscontinued, 
                    MedicationEventType.ProviderDiscontinued, 
                    MedicationEventType.DeactivatedAsErroneous
                };

            var medications = source.Where(i => i.PatientMedication.PatientMedicationDetails.Any(j => !invalidMedicationEventTypes.Contains(j.MedicationEventType))).ToArray();
            InitializeMedicationsAdministeredComponentTableRows(body, medications);
            InitializeMedicationsAdministeredComponentEntries(section, medications);

            return component;
        }

        private void InitializeMedicationsAdministeredComponentTableRows(StrucDocTbody body, IEnumerable<EncounterAdministeredMedication> source)
        {
            var encounterMedications = source.ToArray();
            var mappings = _cdaModelMapper.GetRxNormDescriptionMap(encounterMedications.Select(i => i.PatientMedication.Medication));

            foreach (var medicationMapping in mappings)
            {
                var medication = medicationMapping.Key;
                var rxNormModel = medicationMapping.Value;

                var tr = body.AddTr().SetId("AdministeredMedication{0}".FormatWith(body.tr.Length));
                tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(rxNormModel.Str, Constants.RxNormExternalSystemName, rxNormModel.RxCui));

                var encounter = encounterMedications.First(i => i.PatientMedication.Medication == medicationMapping.Key).Encounter;
                tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(encounter.StartDateTime));

                var patientMedication = encounterMedications.First(i => i.PatientMedication.Medication == medication).PatientMedication;
                tr.AddTd().SetText(patientMedication.PatientMedicationDetails.Select(i => i.AsPrescribedComment).Join(". "));
            }
        }

        private void InitializeMedicationsAdministeredComponentEntries(POCD_MT000040Section section, IEnumerable<EncounterAdministeredMedication> source)
        {
            var encounterMedications = source.ToArray();
            var mappings = _cdaModelMapper.GetRxNormDescriptionMap(encounterMedications.Select(i => i.PatientMedication.Medication));

            foreach (var medicationMapping in mappings)
            {
                var rxNormModel = medicationMapping.Value;

                var substanceAdmin = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV).AddSubstanceAdministration().SetClassCode("SBADM").SetMoodCode(x_DocumentSubstanceMood.EVN);
                substanceAdmin.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.16");

                var encounterMedication = encounterMedications.First(i => i.PatientMedication.Medication == medicationMapping.Key);
                substanceAdmin.AddId().SetRootAndExtension(encounterMedication);
                substanceAdmin.AddText().SetText("{0}, {1}".FormatWith(rxNormModel.Str, rxNormModel.RxCui)).AddReference().SetValue("#AdministeredMedication{0}".FormatWith(section.entry.Length));
                substanceAdmin.AddStatusCode().SetCode("completed");
                var effectiveTime = substanceAdmin.AddEffectiveTime();
                effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(encounterMedication.Encounter.StartDateTime));
                effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(encounterMedication.Encounter.StartDateTime));
                var manufacturedProduct = substanceAdmin.AddConsumable().AddManufacturedProduct().SetClassCode(RoleClassManufacturedProduct.MANU);
                manufacturedProduct.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.23");
                var code = manufacturedProduct.AddMaterial().AddCode().SetCode(rxNormModel.RxCui).
                                                                       SetCodeSystem(CodeSystem.RxNorm).
                                                                       SetDisplayName(rxNormModel.Str);
                code.AddOriginalText().SetText("{0}, {1}".FormatWith(rxNormModel.Str, rxNormModel.RxCui)).AddReference().SetValue("#AdministeredMedication{0}".FormatWith(section.entry.Length));
            }

            if (section.entry.IsNullOrEmpty())
            {
                var substanceAdmin = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV).AddSubstanceAdministration().SetClassCode("SBADM").SetMoodCode(x_DocumentSubstanceMood.EVN);
                substanceAdmin.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.16");

                substanceAdmin.AddId().SetRootAndExtension(new EncounterAdministeredMedication());
                substanceAdmin.AddText();
                substanceAdmin.AddStatusCode().SetCode("completed");
                var effectiveTime = substanceAdmin.AddEffectiveTime();
                effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
                var manufacturedProduct = substanceAdmin.AddConsumable().AddManufacturedProduct().SetClassCode(RoleClassManufacturedProduct.MANU);
                manufacturedProduct.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.23");
                manufacturedProduct.AddMaterial().AddCode().SetNullFlavor(CdaNullFlavor.NI)
                                                                       .SetCodeSystem(CodeSystem.RxNorm);
            }
        }

        #endregion

        #region Reason For Visit

        private static POCD_MT000040Component3 AddNewReasonForVisitComponent(Encounter source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.12");
            section.AddCode().SetCode("29299-5").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("REASON FOR VISIT");
            section.AddTitle("Reason For Visit");

            var reasonForVisitDataText = source.EncounterReasonForVisits.Select(i => i.ReasonForVisit.Name).Distinct().Where(i => i.IsNotNullOrEmpty()).Join(". ");
            var reasonForVisitNoteText = source.EncounterReasonForVisitComments.Select(i => i.Value).Distinct().Where(i => i.IsNotNullOrEmpty()).Join(". ");

            section.AddText().AddParagraph().SetText("{0}{1}{2}".FormatWith(reasonForVisitDataText, reasonForVisitDataText.IsNotNullOrEmpty() ? ", " : string.Empty, reasonForVisitNoteText));

            return component;
        }

        #endregion

        #region Instructions

        private static POCD_MT000040Component3 AddNewInstructionsComponent(Encounter source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.2.45");
            section.AddCode().SetCode("69730-0").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("INSTRUCTIONS");
            section.AddTitle("Instructions");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("InstructionDescription");
            tr.AddTh("Date");

            var body = table.AddTbody();
            InitializeInstructionsComponentTableRows(body, source);
            InitializeInstructionsComponentEntries(section, source);

            return component;
        }

        private static void InitializeInstructionsComponentTableRows(StrucDocTbody body, Encounter source)
        {
            foreach (var clinicalInstruction in source.EncounterClinicalInstructions)
            {
                var tr = body.AddTr().SetId("ClinicalInstruction_{0}".FormatWith(body.tr.Length));
                tr.AddTd().SetText(clinicalInstruction.Value);
                tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(source.StartDateTime));
            }

            foreach (var patientEducation in source.EncounterPatientEducations)
            {
                var tr = body.AddTr().SetId("PatientDecisionAid_{0}".FormatWith(body.tr.Length));
                tr.AddTd().SetText(patientEducation.PatientEducation.Name);
                tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(source.StartDateTime));
            }
        }

        private static void InitializeInstructionsComponentEntries(POCD_MT000040Section section, Encounter source)
        {

            foreach (var clinicalInstruction in source.EncounterClinicalInstructions)
            {

                var act = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV)
                                 .AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.INT);
                act.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.20");
                act.AddCode<CE>().SetCode("311401005").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Patient Education");
                act.AddText(clinicalInstruction.Value).AddReference().SetValue("#ClinicalInstruction_{0}".FormatWith(section.entry.Length));
                act.AddStatusCode().SetCode("completed");
            }

            foreach (var patientEducation in source.EncounterPatientEducations)
            {

                var act = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV)
                                 .AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.INT);
                act.AddTemplateId().SetRoot("2.16.840.1.113883.10.20.22.4.20");
                act.AddCode<CE>().SetCode("311401005").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Patient Education");
                act.AddText(patientEducation.PatientEducation.Name).AddReference().SetValue("#PatientDecisionAid_{0}".FormatWith(section.entry.Length));
                act.AddStatusCode().SetCode("completed");
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Gets or sets the current time.  This is internal for unit testing purposes.
        /// </summary>
        internal DateTime DocumentEffectiveTime { get; set; }

        /// <summary>
        /// Gets or sets the document id.  This is internal for unit testing purposes.
        /// </summary>
        internal Guid? DocumentId { get; set; }
    }
}
