﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Cda;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;

namespace IO.Practiceware.Integration.Cda
{
    /// <summary>
    /// Provides common methods used by the various CDA Message producer.
    /// In particular it provides the following functions:
    /// - Queryable Factory Methods
    /// - CDA model Mapper initialization
    /// - CDA Message Common Components
    /// - Common CDA Formatting functions such ConvertToGuid and FormatForCdaTextElement
    /// </summary>
    /// <remarks>
    /// Any static string values that are hardcoded come from Michelle's spec in TFS.
    /// The specifications are named VisitSummSpec.xml and TocSpec.xml
    /// </remarks>
    public class CdaMessageProducerUtilities
    {
        private readonly CdaModelMapper _cdaModelMapper;
        private readonly IPracticeRepository _practiceRepository;
        public static string[] ExcludedTraceProcedureNames =
        {
            "blood pressure",
            "cognitive status",
            "functional status", 
            "height and weight",
            "smoking"
        };

        private readonly MedicationEventType[] _invalidMedicationEventTypes = 
                {
                    MedicationEventType.PatientDiscontinued, 
                    MedicationEventType.ExternalProviderDiscontinued, 
                    MedicationEventType.ProviderDiscontinued, 
                    MedicationEventType.DeactivatedAsErroneous,
                    MedicationEventType.MedicationAdministeredDuringVisit
                };

        /// <summary>
        /// Specialized trace source for verbose logging within Monitoring Service
        /// </summary>
        public static readonly TraceSource VerboseLog = new TraceSource("CdaMessageProducer");

        /// <summary>
        /// Synchronizes the model.PatientDiagnosisDetails table through the dbo.PatientClinical table via model.SynchronizePatientDiagnosisDetails.
        /// </summary>
        public static void SynchronizePatientDiagnosisDetails(List<int> patientIds, List<int> appointmentIds)
        {
            patientIds = patientIds ?? new List<int>();
            appointmentIds = appointmentIds ?? new List<int>();

            // Cannot use the current ambient transaction when querying external data sources, so we suppress the current TransactionScope
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                var command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                connection.Execute(@"model.SynchronizePatientDiagnosisDetails", new Dictionary<string, object> { { "patientIds", patientIds.ToXml() }, { "appointmentIds", appointmentIds.ToXml() } }
                    , createCommand: () => command);
            }
        }

        public CdaMessageProducerUtilities(CdaModelMapper cdaModelMapper, IPracticeRepository practiceRepository)
        {
            _cdaModelMapper = cdaModelMapper;
            _practiceRepository = practiceRepository;
        }

        /// <summary>
        /// Initializes the model mapper with entities used to query for mapped external database values.
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="patient"></param>
        /// <param name="encounters"></param>
        public static void InitializeCdaModelMapper(CdaModelMapper mapper, Patient patient, Encounter[] encounters)
        {
            #region cvx

            var cvxMapper = mapper.CvxModelMapper;
            cvxMapper.Vaccines = patient.PatientVaccinations.Select(i => i.Vaccine);

            #endregion

            #region loinc

            var loincMapper = mapper.LoincModelMapper;
            var laboratoryTests = patient.PatientLaboratoryTestResults.Select(i => i.LaboratoryTest);
            laboratoryTests = laboratoryTests.Concat(encounters.SelectMany(i => i.EncounterLaboratoryTestOrders.Select(j => j.PatientLaboratoryTestResult.LaboratoryTest)));
            loincMapper.LaboratoryTests = laboratoryTests;

            #endregion

            #region rxNorm

            var rxNormMapper = mapper.RxNormModelMapper;
            rxNormMapper.Allergens = patient.PatientAllergens.Select(i => i.Allergen);

            var medications = encounters.SelectMany(i => i.EncounterAdministeredMedications.Where(j => j.PatientMedication.Medication != null).Select(j => j.PatientMedication.Medication));
            medications = medications.Concat(patient.PatientMedications.Where(i => i.Medication != null).Select(i => i.Medication));
            rxNormMapper.Medications = medications;

            #endregion

            #region snomed

            var snomedMapper = mapper.SnomedModelMapper;
            snomedMapper.AllergenReactionTypes = patient.PatientAllergens.SelectMany(i => i.PatientAllergenAllergenReactionTypes.Select(j => j.AllergenReactionType));

            var clinicalConditions = patient.PatientDiagnoses.Select(i => i.ClinicalCondition);
            clinicalConditions = clinicalConditions.Concat(patient.PatientFunctionalStatusAssessments.Select(i => i.ClinicalCondition));
            clinicalConditions = clinicalConditions.Concat(patient.PatientCognitiveStatusAssessments.Select(i => i.ClinicalCondition));
            snomedMapper.ClinicalConditions = clinicalConditions;
            snomedMapper.ClinicalConditionStatuses = Enums.GetValues<ClinicalConditionStatus>();
            snomedMapper.ClinicalConditionStatuses = Enums.GetValues<ClinicalConditionStatus>();
            snomedMapper.Lateralities = Enums.GetValues<Laterality>();

            var clinicalProcedures = patient.PatientProcedurePerformeds
                .Select(i => i.ClinicalProcedure)
                .Concat(encounters
                    .SelectMany(e => e.EncounterDiagnosticTestOrders)
                    .Where(ed => ed.DiagnosticTestOrder != null)
                    .Select(ed => ed.DiagnosticTestOrder.ClinicalProcedure))
                .Concat(encounters
                    .SelectMany(e => e.EncounterProcedureOrders)
                    .Where(e => e.ProcedureOrder != null)
                    .Select(p => p.ProcedureOrder.ClinicalProcedure))
                .Distinct();

            clinicalProcedures = clinicalProcedures.Concat(encounters.SelectMany(i => i.PatientProcedurePerformeds.Select(j => j.ClinicalProcedure)));
            snomedMapper.ClinicalProcedures = clinicalProcedures;

            var clinicalQualifiers = patient.PatientAllergens.SelectMany(i => i.PatientAllergenAllergenReactionTypes.Select(j => j.AllergenReactionType.ClinicalQualifier));
            snomedMapper.ClinicalQualifiers = clinicalQualifiers;

            snomedMapper.SmokingConditions = patient.PatientSmokingStatus.Select(i => i.SmokingCondition);
            snomedMapper.TreatmentGoals = encounters.SelectMany(i => i.EncounterTreatmentGoalAndInstructions.Select(j => j.TreatmentGoalAndInstruction.TreatmentGoal));

            #endregion

            #region cdc

            var cdcMapper = mapper.CdcModelMapper;
            cdcMapper.PatientRaces = patient.Races;
            cdcMapper.PatientEthnicities = patient.EthnicityId.HasValue ? new List<Ethnicity> { patient.Ethnicity } : new List<Ethnicity>();

            #endregion
        }

        #region Queryable Factory Methods

        private static Expression<Func<Patient, object>>[] GetPatientPathsToLoad()
        {
            return new Expression<Func<Patient, object>>[]
            {
                p => p.PatientAddresses.Select(i => i.StateOrProvince.Country),
                p => p.PatientAddresses.Select(i => i.PatientAddressType),
                p => p.PatientPhoneNumbers,
                p => p.PatientEmailAddresses,
                p => p.Races,
                p => p.Language,
                p => p.Ethnicity,
                p => p.BillingOrganization.BillingOrganizationAddresses.Select(i => i.StateOrProvince.Country),
                p => p.BillingOrganization.BillingOrganizationAddresses.Select(i => i.BillingOrganizationAddressType),
                p => p.BillingOrganization.BillingOrganizationPhoneNumbers.Select(i => i.BillingOrganizationPhoneNumberType),
                p => p.BillingOrganization.IdentifierCodes,
                p => p.ExternalProviders.Select(i => i.ExternalProvider.ExternalContactAddresses.Select(j => j.StateOrProvince.Country)),
                p => p.ExternalProviders.Select(i => i.ExternalProvider.ExternalContactAddresses.Select(j => j.ExternalContactAddressType)),
                p => p.ExternalProviders.Select(i => i.ExternalProvider.ExternalContactPhoneNumbers.Select(j => j.ExternalContactPhoneNumberType)),
                p => p.ExternalProviders.Select(i => i.ExternalProvider.IdentifierCodes),
                p => p.PatientDiagnoses.Select(j => j.ClinicalCondition),
                p => p.PatientDiagnoses.Select(j => j.PatientDiagnosisDetails.Select(k => k.PatientDiagnosisDetailAxisQualifiers)),
                p => p.PatientDiagnoses.Select(j => j.PatientProblemConcernLevels),
                p => p.PatientFunctionalStatusAssessments.Select(j => j.ClinicalCondition),
                p => p.PatientCognitiveStatusAssessments.Select(j => j.ClinicalCondition),
                p => p.PatientAllergens.Select(j => j.Allergen),
                p => p.PatientAllergens.Select(j => j.PatientAllergenDetails),
                p => p.PatientAllergens.Select(j => j.PatientAllergenAllergenReactionTypes.Select(k => k.AllergenReactionType.ClinicalQualifier)),
                p => p.PatientAllergens.Select(j => j.PatientProblemConcernLevels),
                p => p.PatientMedications.Select(j => j.Medication),
                p => p.PatientMedications.Select(j => j.PatientMedicationDetails),
                p => p.PatientVaccinations.Select(j => j.Vaccine),
                p => p.PatientLaboratoryTestResults.Select(j => j.LaboratoryTest),
                p => p.PatientLaboratoryTestResults.Select(j => j.LaboratoryTestAbnormalFlag),
                p => p.PatientLaboratoryTestResults.Select(j => j.LaboratoryNormalRange),
                p => p.PatientLaboratoryTestResults.Select(j => j.EncounterLaboratoryTestOrders.Select(k => k.Encounter)),
                p => p.PatientLaboratoryTestResults.Select(j => j.UnitOfMeasurement),
                p => p.PatientSmokingStatus.Select(j => new { j.SmokingCondition, j.Encounter }),
                p => p.PatientProcedurePerformeds.Select(i => i.ClinicalProcedure),
                p => p.PatientProcedurePerformeds.Select(i => i.Encounter.PatientDiagnosisDetails.Select(pdd => pdd.PatientDiagnosis.ClinicalCondition)),
                p => p.PatientProcedurePerformeds.Select(i => i.PatientProcedurePerformedQuestionAnswers.Select(a => a.QuestionAnswerValues)),
                p => p.PatientDiagnosticTestPerformeds.Select(i => i.ClinicalProcedure),
                p => p.PatientDiagnosticTestPerformeds.Select(i => i.Encounter.PatientDiagnosisDetails.Select(pdd => pdd.PatientDiagnosis.ClinicalCondition)),
                p => p.PatientDiagnosticTestPerformeds.Select(i => i.PatientDiagnosticTestPerformedQuestionAnswers.Select(a => a.QuestionAnswerValues)),
                p => p.PatientExamPerformeds.Select(i => i.ClinicalProcedure),
                p => p.PatientExamPerformeds.Select(i => i.Encounter.PatientDiagnosisDetails.Select(pdd => pdd.PatientDiagnosis.ClinicalCondition)),
                p => p.PatientExamPerformeds.Select(i => i.PatientExamPerformedQuestionAnswers.Select(a => a.QuestionAnswerValues)),
                p => p.PatientHeightAndWeights,
                p => p.PatientInsurances.Select(pi => pi.InsurancePolicy.Insurer),
                p => p.PatientBloodPressures
            };
        }

        private static Expression<Func<Encounter, object>>[] GetEncounterPathsToLoad()
        {
            return new Expression<Func<Encounter, object>>[]
                   {
                       i => i.Appointments.OfType<UserAppointment>().Select(j => j.User),
                       i => i.ClinicalInvoiceProviders.Select(j => j.Provider.BillingOrganization.BillingOrganizationAddresses.Select(k => k.StateOrProvince.Country)),
                       i => i.ClinicalInvoiceProviders.Select(j => j.Provider.BillingOrganization.BillingOrganizationAddresses.Select(k => k.BillingOrganizationAddressType)),
                       i => i.ClinicalInvoiceProviders.Select(j => j.Provider.BillingOrganization.BillingOrganizationPhoneNumbers.Select(k => k.BillingOrganizationPhoneNumberType)),
                       i => i.ClinicalInvoiceProviders.Select(j => j.Provider.User.UserPhoneNumbers.Select(up => up.UserPhoneNumberType)),
                       i => i.ClinicalInvoiceProviders.Select(j => j.Provider.IdentifierCodes),
                       i => i.EncounterReasonForVisitComments,
                       i => i.EncounterClinicalInstructions,
                       i => i.EncounterReasonForVisits.Select(rfv => rfv.ReasonForVisit),
                       i => i.EncounterPatientEducations.Select(j => j.PatientEducation),
                       i => i.EncounterTransitionOfCareOrders.Select(j => j.TransitionOfCareReason),
                       i => i.EncounterTransitionOfCareOrders.Select(j => j.ExternalProvider.ExternalContactAddresses.Select(k => k.StateOrProvince.Country)),
                       i => i.EncounterTransitionOfCareOrders.Select(j => j.ExternalProvider.ExternalContactAddresses.Select(k => k.ExternalContactAddressType)),
                       i => i.EncounterTransitionOfCareOrders.Select(j => j.ExternalProvider.ExternalContactPhoneNumbers.Select(k => k.ExternalContactPhoneNumberType)),
                       i => i.EncounterProcedureOrders.Select(j => j.ProcedureOrder.ClinicalProcedure),
                       i => i.Patient.ExternalProviders.Select(j => j.ExternalProvider.ExternalContactAddresses.Select(k => k.StateOrProvince.Country)),
                       i => i.Patient.ExternalProviders.Select(j => j.ExternalProvider.ExternalContactAddresses.Select(k => k.ExternalContactAddressType)),
                       i => i.Patient.ExternalProviders.Select(j => j.ExternalProvider.ExternalContactPhoneNumbers.Select(k => k.ExternalContactPhoneNumberType)),
                       i => i.EncounterTreatmentGoalAndInstructions.Select(j => j.TreatmentGoalAndInstruction.TreatmentGoal),
                       i => i.EncounterLaboratoryTestOrders.Select(j => j.PatientLaboratoryTestResult.LaboratoryTestAbnormalFlag),
                       i => i.EncounterLaboratoryTestOrders.Select(j => j.PatientLaboratoryTestResult.LaboratoryNormalRange),
                       i => i.EncounterLaboratoryTestOrders.Select(j => j.PatientLaboratoryTestResult.LaboratoryTest),
                       i => i.EncounterLaboratoryTestOrders.Select(j => j.PatientLaboratoryTestResult.UnitOfMeasurement),
                       i => i.EncounterDiagnosticTestOrders.Select(j => j.DiagnosticTestOrder.ClinicalProcedure),
                       i => i.EncounterType,
                       i => i.ServiceLocation.ServiceLocationAddresses.Select(j => j.StateOrProvince.Country),
                       i => i.ServiceLocation.ServiceLocationAddresses.Select(j => j.ServiceLocationAddressType),
                       i => i.ServiceLocation.IdentifierCodes,
                       i => i.PatientProcedurePerformeds.Select(j => j.ClinicalProcedure),
                       i => i.PatientHeightAndWeights,
                       i => i.PatientBloodPressures,
                       i => i.PatientDiagnosisDetails.Select(pdd => pdd.PatientDiagnosis.ClinicalCondition),
                       i => i.EncounterAdministeredMedications.Select(j => j.PatientMedication.Medication),
                       i => i.EncounterAdministeredMedications.Select(j => j.PatientMedication.PatientMedicationDetails),
                       i => i.Invoices.SelectMany(j => j.BillingServices.Select(bs => bs.EncounterService)),
                       i => i.EncounterMedicationOrders.Select(e => e.PatientMedicationDetail.PatientMedication.Medication),
                       i => i.EncounterCommunicationWithOtherProviderOrders
                   };
        }

        public void LoadForCdaMessageProduction(Patient source)
        {
            IEnumerable<Tuple<object, PropertyInfo>> failures;

            var factory = _practiceRepository.AsQueryableFactory();
            factory.Load(source, out failures, GetPatientPathsToLoad());

            if (failures != null)
            {
                var message = failures.Select(f => "Property {0} failed to load on {1} with Id {2}.".FormatWith(f.Item2.Name, f.Item2.DeclaringType.IfNotNull(t => t.Name) ?? f.Item1.IfNotNull(i => i.GetType().Name), f.Item1 is IHasId ? f.Item1.CastTo<IHasId>().Id : f.Item1.IfNotNull(i => i.ToString())))
                    .Join(Environment.NewLine);
                VerboseLog.TraceEvent(TraceEventType.Warning, 0, message);
            }
        }

        public void LoadForCdaMessageProduction(IEnumerable<Patient> source)
        {
            IEnumerable<Tuple<object, PropertyInfo>> failures;

            var factory = _practiceRepository.AsQueryableFactory();
            var sourceItems = source.ToArray();
            factory.Load(sourceItems, out failures, GetPatientPathsToLoad());

            if (failures != null)
            {
                var message = failures.Select(f => "Property {0} failed to load on {1} with Id {2}.".FormatWith(f.Item2.Name, f.Item2.DeclaringType.IfNotNull(t => t.Name) ?? f.Item1.IfNotNull(i => i.GetType().Name), f.Item1 is IHasId ? f.Item1.CastTo<IHasId>().Id : f.Item1.IfNotNull(i => i.ToString())))
                    .Join(Environment.NewLine);
                VerboseLog.TraceEvent(TraceEventType.Warning, 0, message);
            }
        }

        public void LoadForCdaMessageProduction(Encounter source)
        {
            IEnumerable<Tuple<object, PropertyInfo>> failures;

            var factory = _practiceRepository.AsQueryableFactory();
            factory.Load(source, out failures, GetEncounterPathsToLoad());

            if (failures != null)
            {
                var message = failures.Select(f => "Property {0} failed to load on {1} with Id {2}.".FormatWith(f.Item2.Name, f.Item2.DeclaringType.IfNotNull(t => t.Name) ?? f.Item1.IfNotNull(i => i.GetType().Name), f.Item1 is IHasId ? f.Item1.CastTo<IHasId>().Id : f.Item1.IfNotNull(i => i.ToString())))
                    .Join(Environment.NewLine);
                VerboseLog.TraceEvent(TraceEventType.Warning, 0, message);
            }

        }

        public void LoadForCdaMessageProduction(IEnumerable<Encounter> source)
        {
            IEnumerable<Tuple<object, PropertyInfo>> failures;

            var factory = _practiceRepository.AsQueryableFactory();
            var sourceItems = source.ToArray();
            factory.Load(sourceItems, out failures, GetEncounterPathsToLoad());

            if (failures != null)
            {
                var message = failures.Select(f => "Property {0} failed to load on {1} with Id {2}.".FormatWith(f.Item2.Name, f.Item2.DeclaringType.IfNotNull(t => t.Name) ?? f.Item1.IfNotNull(i => i.GetType().Name), f.Item1 is IHasId ? f.Item1.CastTo<IHasId>().Id : f.Item1.IfNotNull(i => i.ToString())))
                    .Join(Environment.NewLine);
                VerboseLog.TraceEvent(TraceEventType.Warning, 0, message);
            }
        }

        #endregion

        #region Clinical Document

        public static POCD_MT000040ClinicalDocument AddClinicalDocument(DateTime timeOfCreation, string practiceName, Guid? clinicalDocumentId = null)
        {
            var source = new POCD_MT000040ClinicalDocument { moodCode = null };
            source.AddRealmCode().SetCode("US");
            source.AddTypeId().SetExtension("POCD_HD000040")
                              .SetRoot(Oids.Hl7RegisteredModels);
            source.AddTemplateId().SetRoot(Oids.ContinuityOfCareDocumentGeneralHeader);
            source.AddTemplateId().SetRoot(Oids.ContinuityOfCareDocumentCcd);
            source.AddId().SetRootAndExtension("0", Guid.NewGuid().ToString("N"));
            source.AddCode().SetCode("34133-9")
                            .SetDisplayName("Summarization of episode note")
                .SetCodeSystem(CodeSystem.Loinc);

            source.AddTitle(string.Format("{0}{1}{2}", practiceName, practiceName.IsNotNullOrEmpty() ? " : " : string.Empty, "Health Summary"));
            source.AddConfidentialityCode().SetCode("N")
                                           .SetCodeSystemName(Oids.Confidentiality);
            source.AddLanguageCode().SetCode("en-US");
            source.AddEffectiveTime().SetValue(CdaExtensions.FormatForCda(timeOfCreation));
            return source;
        }

        #endregion

        #region PatientRole

        public POCD_MT000040PatientRole AddNewPatientRole(Patient patient)
        {
            var role = new POCD_MT000040PatientRole();
            role.AddId().SetRootAndExtension(patient);

            var homeAddress = patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            if (homeAddress != null) role.AddAddress().SetUse(CdaPostalAddressCode.H.ToString()).SetValues(homeAddress);

            patient.PatientPhoneNumbers.ForEachWithSinglePropertyChangedNotification(phone => role.AddTelecom()
                                                             .SetUse(ConstantValueSetMaps.PatientPhoneTypeToCdaTelecomUsePhoneType.GetValue(phone.PatientPhoneNumberType).IfNotDefault(code => code.ToString(), CdaTelecomPhoneNumberCode.HP.ToString()))
                                                             .SetPhoneNumber(phone.ToString()));

            patient.PatientEmailAddresses.ToList().ForEach(i => role.AddTelecom().SetEmailAddress(i.Value));

            role.patient = AddNewPatient(patient);

            return role;
        }

        #endregion

        #region Record Target

        private POCD_MT000040Patient AddNewPatient(Patient patient)
        {
            var element = new POCD_MT000040Patient
                {
                    classCode = null,
                    determinerCode = null
                };

            element.AddName().AddFirstName(patient.FirstName).AddLastName(patient.LastName).TryAddSuffix(patient.Honorific);

            #region administrativeGenderCode - gender

            if (patient.Gender.HasValue)
            {
                element.administrativeGenderCode = new CE
                {
                    code = patient.Gender.Value.GetDescription(),
                    displayName = patient.Gender.Value.ToString(),
                    codeSystem = CodeSystem.AdministrativeGender.ToString(),
                    codeSystemName = "HL7 AdministrativeGender"
                };
            }

            #endregion

            if (patient.DateOfBirth.HasValue)
            {
                element.birthTime = new TS { value = CdaExtensions.FormatForCda(patient.DateOfBirth.Value) };
            }

            #region maritalStatusCode - maritalStatus

            if (patient.MaritalStatus.HasValue)
            {
                string code;
                if (ConstantValueSetMaps.MaritalStatusToHL7Map.TryGetValue(patient.MaritalStatus.Value, out code))
                {
                    element.maritalStatusCode = new CE
                    {
                        code = code,
                        displayName = patient.MaritalStatus.Value.ToString(),
                        codeSystem = CodeSystem.MaritalStatus.ToString(),
                        codeSystemName = "HL7 Marital status"
                    };
                }
            }

            #endregion

            #region raceCode - patientRace

            var raceMappings = _cdaModelMapper.GetCdcRaceModelMap(patient.Races);

            foreach (var patientRace in patient.Races)
            {
                raceMappings.GetValue(patientRace.Id)
                    .IfNotNull(cdcRaceAndEthnicityModel => element.AddRaceCode()
                        .SetCode(cdcRaceAndEthnicityModel.Code)
                        .SetDisplayName(cdcRaceAndEthnicityModel.Name)
                        .SetCodeSystem(CodeSystem.RaceAndEthnicity));
            }

            #endregion

            #region ethnicGroupCode - patientEthnicity

            var ethnicityMappings = _cdaModelMapper.GetCdcEthnicityModelMap(new[] { patient.Ethnicity });
            if (patient.EthnicityId.HasValue && ethnicityMappings.ContainsKey(patient.EthnicityId.Value))
            {
                var cdcRaceAndEthnicityModel = ethnicityMappings[patient.EthnicityId.Value];
                element.ethnicGroupCode = new CE().SetCode(cdcRaceAndEthnicityModel.Code)
                    .SetDisplayName(patient.Ethnicity.Name.IsNullOrWhiteSpace() ? "Unavailable" : patient.Ethnicity.Name)
                    .SetCodeSystem(CodeSystem.RaceAndEthnicity);
            }

            #endregion

            #region languageCommunication - patientLanguageId

            if (patient.LanguageId.HasValue)
            {
                element.languageCommunication = new[]
                                                {
                                                    new POCD_MT000040LanguageCommunication
                                                    {
                                                        languageCode = new CS {code = patient.Language.Abbreviation},
                                                        preferenceInd = new BL {value = true, valueSpecified = true}
                                                    }
                                                };
            }

            #endregion

            return element;
        }

        public static POCD_MT000040Organization AddNewProviderOrganization(BillingOrganization billingOrganization)
        {
            var element = new POCD_MT000040Organization { classCode = null, determinerCode = null };
            element.AddId().SetExtension(billingOrganization.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                           .SetRoot(Oids.NationalProviderId);
            element.AddName(billingOrganization.Name);

            billingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main).IfNotNull(phone => element.AddTelecom().SetUse(CdaTelecomPhoneNumberCode.WP.ToString()).SetPhoneNumber(phone.ToString()));
            billingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation).IfNotNull(address => element.AddAddress().SetUse(CdaPostalAddressCode.WP.ToString()).SetValues(address));

            element.standardIndustryClassCode = new CE();
            element.asOrganizationPartOf = new POCD_MT000040OrganizationPartOf { classCode = null };

            return element;
        }

        #endregion

        #region documentationOf

        public static POCD_MT000040DocumentationOf AddDocumentationOf(POCD_MT000040ClinicalDocument source, IEnumerable<Encounter> encounters)
        {
            var newDocumentationOf = new POCD_MT000040DocumentationOf { typeCode = null };
            var dischargedEncounters = encounters.Where(e => e.EncounterStatus == EncounterStatus.Discharged).OrderByDescending(i => i.StartDateTime).ToArray();
            if (dischargedEncounters.Any())
            {
                var providers = dischargedEncounters.SelectMany(e => e.ClinicalInvoiceProviders).Select(i => i.Provider).Distinct();
                var externalProviders = dischargedEncounters.FirstOrDefault().IfNotDefault(e => e.Patient).ExternalProviders.Select(ep => ep.ExternalProvider).Distinct();

                var serviceEvent = newDocumentationOf.AddServiceEvent().SetClassCode("PCPR");
                var effectiveTime = serviceEvent.AddEffectiveTime();

                var encounterDateTime = dischargedEncounters.Select(i => i.StartDateTime).First();
                effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCda(encounterDateTime));
                effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCda(encounterDateTime));

                serviceEvent.performer = providers.Select(CreatePerformer).Concat(externalProviders.Select(CreatePerformer)).ToArray();
            }

            if (source.documentationOf == null)
            {
                source.documentationOf = new POCD_MT000040DocumentationOf[] { };
            }
            source.documentationOf = source.documentationOf.Concat(new[] { newDocumentationOf }).ToArray();

            return newDocumentationOf;
        }

        public static POCD_MT000040Performer1 CreatePerformer(Provider provider)
        {
            var performer = new POCD_MT000040Performer1 { typeCode = x_ServiceEventPerformer.PRF };
            var assignedEntity = performer.AddAssignedEntity();
            assignedEntity.AddId().SetExtension(provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi))
                                  .SetRoot(Oids.NationalProviderId);
            assignedEntity.AddCode().SetCode(provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy))
                                    .SetCodeSystem(CodeSystem.Nucc);

            provider.BillingOrganization.BillingOrganizationAddresses.WithAddressType(BillingOrganizationAddressTypeId.PhysicalLocation).IfNotNull(address => assignedEntity.AddAddress().SetValues(address));

            IPhoneNumber phoneNumber;
            if (provider.User != null && provider.User.UserPhoneNumbers.IsNotNullOrEmpty())
            {
                phoneNumber = provider.User.UserPhoneNumbers.WithPhoneNumberType(UserPhoneNumberTypeId.Main);
            }
            else
            {
                phoneNumber = provider.BillingOrganization.BillingOrganizationPhoneNumbers.WithPhoneNumberType(BillingOrganizationPhoneNumberTypeId.Main);
            }

            assignedEntity.AddTelecom().SetPhoneNumber(phoneNumber.ToString())
                                       .SetUse(CdaTelecomPhoneNumberCode.WP.ToString());

            assignedEntity.AddAssignedPerson().AddName()
                          .TryAddFirstName(provider.User.IfNotNull(u => u.FirstName) ?? "")
                          .TryAddLastName(provider.User.IfNotNull(u => u.LastName) ?? "")
                          .TryAddSuffix(provider.User.IfNotNull(u => u.Honorific) ?? "");

            return performer;
        }

        public static POCD_MT000040Performer1 CreatePerformer(ExternalProvider provider)
        {
            var performer = new POCD_MT000040Performer1 { typeCode = x_ServiceEventPerformer.PRF };

            var assignedEntity = performer.AddAssignedEntity();
            assignedEntity.AddId().SetRoot(Oids.NationalProviderId).SetExtension(provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi));
            assignedEntity.AddCode().SetCode(provider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy)).SetCodeSystem(CodeSystem.Nucc);

            // Cannot use WithAddressType because of name mismatch on ContactAddressTypeId to ExternalContactAddressTypeId.  (The convention is to have the same name such as, ExternalContactAddressTypeId).
            provider.ExternalContactAddresses.FirstOrDefault(i => i.ContactAddressTypeId == (int)ExternalContactAddressTypeId.MainOffice).IfNotNull(address => assignedEntity.AddAddress().SetValues(address));

            provider.ExternalContactPhoneNumbers.WithPhoneNumberType(ExternalContactPhoneNumberTypeId.Main)
                    .IfNotNull(phone => assignedEntity.AddTelecom().SetPhoneNumber(phone.ToString()).SetUse(CdaTelecomPhoneNumberCode.WP.ToString()));

            assignedEntity.AddAssignedPerson().AddName()
                          .TryAddFirstName(provider.FirstName)
                          .TryAddLastName(provider.LastNameOrEntityName)
                          .TryAddSuffix(provider.Honorific);
            return performer;
        }

        #endregion

        #region Allergies

        public POCD_MT000040Component3 AddNewAllergiesComponent(IEnumerable<PatientAllergen> source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.Allergies);
            section.AddCode().SetCode("48765-2").SetDisplayName("Allergies, Adverse Reactions, Alerts").SetCodeSystem(CodeSystem.Loinc);
            section.AddTitle("Medication Allergies");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            var headers = new[] { "Type", "Substance", "Reaction", "Status" };
            headers.ToList().ForEach(i => tr.AddTh(i));

            var body = table.AddTbody();

            var allergies = source.Where(i => i.Allergen.IsAdverseDrugReaction
                                            && i.PatientAllergenDetails.Any()
                                            && !i.PatientAllergenDetails.OrderByDescending(j => j.EnteredDateTime).First().IsDeactivated).ToArray();

            InitializeAllergiesComponentTableRows(body, allergies);
            InitializeAllergiesComponentEntries(section, allergies);
            InitializeEmptyComponentTableRow(body, body.tr, headers.Count());

            return component;
        }

        private void InitializeAllergiesComponentTableRows(StrucDocTbody body, IEnumerable<PatientAllergen> source)
        {
            var patientAllergens = source.ToArray();
            var rxNormMappings = _cdaModelMapper.GetRxNormDescriptionMap(patientAllergens.Select(i => i.Allergen));
            var snomedMappings = _cdaModelMapper.GetSnomedModelMap(patientAllergens.SelectMany(i => i.PatientAllergenAllergenReactionTypes.Select(j => j.AllergenReactionType)).Distinct());

            foreach (var patientAllergen in patientAllergens)
            {
                var rxNormModel = rxNormMappings.GetValue(patientAllergen.Allergen);

                var tr = body.AddTr();
                var rowNumber = body.tr.Length;

                tr.SetId("ALGSUMMARY_{0}".FormatWith(rowNumber));
                tr.AddTd().SetId("ALGTYPE_{0}".FormatWith(rowNumber)).SetText("Drug Allergy");
                if (rxNormModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find RxNorm mapping for allergen {0} (PatientAllergen Id {1}).".FormatWith(patientAllergen.Allergen.Name, patientAllergen.Id));
                    tr.AddTd().SetId("ALGSUBSTANCE_{0}".FormatWith(rowNumber)).SetText("{0}, {1}: {2}".FormatWith(patientAllergen.Allergen.Name, Constants.RxNormExternalSystemName, "Unknown"));
                }
                else
                {
                    tr.AddTd().SetId("ALGSUBSTANCE_{0}".FormatWith(rowNumber)).SetText("{0}, {1}: {2}".FormatWith(rxNormModel.Str, Constants.RxNormExternalSystemName, rxNormModel.RxCui));
                }

                var currentChar = 'A';

                var allergenReactionTypes = patientAllergen.PatientAllergenAllergenReactionTypes.Select(al => al.AllergenReactionType).ToList();
                if (allergenReactionTypes.IsNullOrEmpty())
                {
                    tr.AddTd().SetId("ALGREACT_{0}{1}".FormatWith(rowNumber, currentChar)).SetText("None");
                }
                else
                {
                    foreach (var reactionType in allergenReactionTypes)
                    {
                        var snomedModel = snomedMappings[reactionType];
                        if (snomedModel == null)
                        {
                            VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for reaction type {0} (PatientAllergen Id {1}).".FormatWith(reactionType.Name, patientAllergen.Id));
                            tr.AddTd().SetId("ALGREACT_{0}{1}".FormatWith(rowNumber, currentChar)).SetText("{0}, {1}".FormatWith(reactionType.Name, "Unknown"));
                        }
                        else
                        {
                            tr.AddTd().SetId("ALGREACT_{0}{1}".FormatWith(rowNumber, currentChar)).SetText("{0}, {1}".FormatWith(snomedModel.Term, snomedModel.ConceptId));
                        }

                        currentChar++;
                    }
                }

                var reactionDetail = patientAllergen.PatientAllergenDetails.OrderByDescending(i => i.EnteredDateTime).First();
                tr.AddTd().SetId("ALGSTATUS_{0}".FormatWith(rowNumber)).SetText(reactionDetail.ClinicalConditionStatus.ToString());
            }
        }

        private void InitializeAllergiesComponentEntries(POCD_MT000040Section section, IEnumerable<PatientAllergen> source)
        {
            var patientAllergens = source.ToArray();
            var rxNormMappings = _cdaModelMapper.GetRxNormDescriptionMap(patientAllergens.Select(i => i.Allergen));
            var snomedReactionTypeMappings = _cdaModelMapper.GetSnomedModelMap(patientAllergens.SelectMany(i => i.PatientAllergenAllergenReactionTypes.Select(j => j.AllergenReactionType)).Distinct());
            var snomedClinicalQualifierMappings = _cdaModelMapper.GetSnomedModelMap(patientAllergens.SelectMany(i => i.PatientAllergenAllergenReactionTypes.Select(j => j.AllergenReactionType.ClinicalQualifier)).Distinct());

            foreach (var patientAllergen in patientAllergens)
            {
                AddAllergyEntry(section, patientAllergen, rxNormMappings, snomedReactionTypeMappings, snomedClinicalQualifierMappings);
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddAllergyEntry(section, null, rxNormMappings, snomedReactionTypeMappings, snomedClinicalQualifierMappings);
            }
        }

        private static void AddAllergyEntry(POCD_MT000040Section section, PatientAllergen patientAllergen, IDictionary<Allergen, RxNormModel> rxNormMappings,
                                     IDictionary<AllergenReactionType, SnomedDescriptionModel> snomedReactionTypeMappings,
                                     IDictionary<ClinicalQualifier, SnomedDescriptionModel> snomedClinicalQualifierMappings)
        {
            var entry = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV);

            // set up required values
            var act = entry.AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.EVN);
            act.AddId().SetRootAndExtension(patientAllergen ?? new PatientAllergen());
            act.AddTemplateId().SetRoot(Oids.AllergyProblemAct);
            act.AddCode().SetCode("48765-2").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Allergies, Adverse Reactions, Alerts");
            var effectiveTime = act.AddEffectiveTime();

            // set up observation required values
            var observation = act.AddEntryRelationship().SetTypeCode(x_ActRelationshipEntryRelationship.SUBJ)
                                 .AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
            observation.AddTemplateId().SetRoot(Oids.AllergyObservation); // spec lists 2.16.840.1.113883.10.20.22.4.28 as the correct template ?      
            observation.AddCode().SetCode("ASSERTION").SetCodeSystem(CodeSystem.ActCode); // spec says code="33999-4", codeSystemLOINC 2.16.840.1.113883.6.1 STATIC
            observation.AddStatusCode().SetCode("completed");
            var effectiveTimeForObservation = observation.AddEffectiveTime();

            var rowNumber = section.entry.Length;

            observation.AddId().SetRootAndExtension(patientAllergen ?? new PatientAllergen());

            var playingEntity = observation.AddParticipant()
                                           .SetTypeCode("CSM")
                                           .AddParticipantRole().SetClassCode("MANU")
                                           .AddPlayingEntity().SetClassCode("MMAT");


            // case where there is patientAllergen data
            if (patientAllergen != null)
            {
                // todo - we seem to be hardcoding the 'A' value for row differentiation. 
                observation.AddValue<CD>()
                           .SetCode("416098002")
                           .SetDisplayName("Drug allergy (disorder)")
                           .SetCodeSystem(CodeSystem.SnomedCt)
                           .AddOriginalText()
                           .AddReference()
                           .SetValue("#ALGREACT_{0}A".FormatWith(rowNumber));

                var concernLevel = patientAllergen.PatientProblemConcernLevels.OrderByDescending(i => i.EnteredDateTime).FirstOrDefault() ?? new PatientProblemConcernLevel { ConcernLevel = ConcernLevel.Active };

                act.AddStatusCode().SetCode(concernLevel.ConcernLevel.ToString().ToLower());

                if (concernLevel.ConcernLevel == ConcernLevel.Active)
                {
                    if (concernLevel.ConcernStartDate.HasValue)
                    {
                        effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(concernLevel.ConcernStartDate.Value));
                    }
                    else
                    {
                        effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.UNK);
                    }
                }
                else if (concernLevel.ConcernLevel == ConcernLevel.Completed)
                {
                    if (concernLevel.ConcernEndDate.HasValue)
                    {
                        effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(concernLevel.ConcernEndDate.Value));
                    }
                    else
                    {
                        effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.UNK);
                    }
                }

                var allergenDetail = patientAllergen.PatientAllergenDetails.OrderByDescending(i => i.EnteredDateTime).First();
                var low = effectiveTimeForObservation.AddLow();
                if (allergenDetail.StartDateTime.HasValue)
                {
                    low.SetValue(CdaExtensions.FormatForCdaAlternate(allergenDetail.StartDateTime.Value));
                }
                else
                {
                    low.SetNullFlavor(CdaNullFlavor.UNK);
                }

                var endDate = allergenDetail.IfNotDefault(i => i.EndDateTime);
                if (endDate != null)
                {
                    effectiveTimeForObservation.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(endDate.Value));
                }

                RxNormModel rxNormModel = rxNormMappings.GetValue(patientAllergen.Allergen);
                if (rxNormModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find RxNorm mapping for allergen {0} (PatientAllergen Id {1}).".FormatWith(patientAllergen.Allergen.Name, patientAllergen.Id));
                    playingEntity.AddCode().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.RxNorm).SetDisplayName(patientAllergen.Allergen.Name)
                        .AddOriginalText().AddReference().SetValue("#ALGSUBSTANCE_{0}".FormatWith(rowNumber));
                }
                else
                {
                    playingEntity.AddCode().SetCode(rxNormModel.RxCui).SetCodeSystem(CodeSystem.RxNorm).SetDisplayName(rxNormModel.Str)
                             .AddOriginalText().AddReference().SetValue("#ALGSUBSTANCE_{0}".FormatWith(rowNumber));

                    playingEntity.AddName().SetText("{0}, {1}".FormatWith(rxNormModel.Str, rxNormModel.RxCui));
                }

                foreach (var reactionType in patientAllergen.PatientAllergenAllergenReactionTypes.Select(i => i.AllergenReactionType))
                {
                    var snomedModel = snomedReactionTypeMappings.GetValue(reactionType);

                    observation = act.AddEntryRelationship().SetTypeCode(x_ActRelationshipEntryRelationship.MFST).SetInversionInd(true)
                                     .AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.ReactionObservation);
                    observation.AddId().SetRootAndExtension(allergenDetail.PatientAllergen);
                    observation.AddCode().SetCode("ASSERTION").SetCodeSystem(CodeSystem.ActCode);
                    observation.AddText().AddReference().SetValue("#ALGREACT_{0}A".FormatWith(rowNumber));
                    observation.AddStatusCode().SetCode("completed");

                    if (snomedModel == null)
                    {
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for reaction type {0} (PatientAllergen Id {1}).".FormatWith(reactionType.Name, patientAllergen.Id));
                        observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(reactionType.Name);
                    }
                    else
                    {
                        observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                    }
                }

                foreach (var reactionType in patientAllergen.PatientAllergenAllergenReactionTypes.Select(i => i.AllergenReactionType.ClinicalQualifier))
                {
                    var snomedModel = snomedClinicalQualifierMappings.GetValue(reactionType);

                    observation = act.AddEntryRelationship().SetTypeCode(x_ActRelationshipEntryRelationship.SUBJ).SetInversionInd(true)
                                     .AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.SeverityObservation);
                    observation.AddCode().SetCode("SEV").SetCodeSystem(CodeSystem.ActCode).SetDisplayName("SeverityObservation");
                    observation.AddText().AddReference().SetValue("#ALGSUMMARY_{0}".FormatWith(rowNumber));
                    observation.AddStatusCode().SetCode("completed");

                    if (snomedModel == null)
                    {
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for clinical qualifier {0} (PatientAllergen Id {1}).".FormatWith(reactionType.Name, patientAllergen.Id));
                        observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(reactionType.Name);
                    }
                    else
                    {
                        observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                    }
                }
            }
            else // case where there is no patientAllergen data
            {
                act.AddStatusCode().SetCode(ConcernLevel.Active.ToString().ToLower());
                effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);

                effectiveTimeForObservation.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                effectiveTimeForObservation.AddHigh().SetNullFlavor(CdaNullFlavor.NI);

                playingEntity.AddCode().SetNullFlavor(CdaNullFlavor.NI);

                observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.NI);
            }
        }

        #endregion

        #region Medications

        public POCD_MT000040Component3 AddNewMedicationsComponent(IEnumerable<PatientMedication> source)
        {
            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.ContinuityOfCareDocumentGeneralHeader);
            section.AddCode().SetCode("10160-0").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("HISTORY OF MEDICATION USE");
            section.AddTitle("Medications");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");

            var headers = new[] { "Medication", "Start Date", "Medication Details", "Status" };
            var tr = table.AddThead().AddTr();
            headers.ToList().ForEach(i => tr.AddTh(i));

            var body = table.AddTbody();

            var medications = source.Where(i => i.PatientMedicationDetails.Any() && !i.PatientMedicationDetails.OrderByDescending(m => m.MedicationEventDateTime).First().IsDeactivated 
                && !_invalidMedicationEventTypes.Contains(i.PatientMedicationDetails.OrderByDescending(m => m.MedicationEventDateTime).First().MedicationEventType)).ToArray();

            var patientMedicationDetails = medications.SelectMany(m => m.PatientMedicationDetails).Where(i => !i.IsDeactivated) 
                        .OrderByDescending(d => d.MedicationEventDateTime).ToArray();

            medications = patientMedicationDetails.Select(m => m.PatientMedication).ToArray();

            InitializeMedicationsComponentTableRows(body, medications.Distinct());
            InitializeMedicationsComponentEntries(section, medications);
            InitializeEmptyComponentTableRow(body, body.tr, headers.Count());

            return component;
        }

        private void InitializeMedicationsComponentTableRows(StrucDocTbody body, IEnumerable<PatientMedication> source)
        {
            var patientMedications = source.ToArray();
            var mappings = _cdaModelMapper.GetRxNormDescriptionMap(patientMedications.Select(i => i.Medication));

            foreach (var pm in patientMedications)
            {
                var latestDetail = pm.PatientMedicationDetails.OrderByDescending(i => i.MedicationEventDateTime).FirstOrDefault();
                if (latestDetail != null && (latestDetail.IsDeactivated || _invalidMedicationEventTypes.Contains(latestDetail.MedicationEventType))) continue;

                var rxNormModel = mappings.GetValue(pm.Medication);

                var tr = body.AddTr();
                var rowCount = body.tr.Length;
                if (rxNormModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find RxNorm mapping for Medication {0} (Medication Id {1}).".FormatWith(pm.Medication.Name, pm.MedicationId));
                    tr.AddTd().AddContent().SetId("Med_{0}".FormatWith(rowCount)).SetText("{0}, {1}: {2}".FormatWith(pm.Medication.Name, Constants.RxNormExternalSystemName, "Unknown"));
                }
                else
                {
                    tr.AddTd().AddContent().SetId("Med_{0}".FormatWith(rowCount)).SetText("{0}, {1}: {2}".FormatWith(rxNormModel.Str, Constants.RxNormExternalSystemName, rxNormModel.RxCui));
                }

                var startDetail = pm.PatientMedicationDetails
                    .Where(i => !i.IsDeactivated
                        && i.MedicationEventType == MedicationEventType.PatientStarted)
                    .OrderByDescending(i => i.MedicationEventDateTime)
                    .FirstOrDefault();

                var startDate = startDetail != null
                    ? startDetail.MedicationEventDateTime.FormatForCdaTextElement()
                    : "unknown";

                var comments = pm.PatientMedicationDetails
                    .Select(i => i.AsPrescribedComment)
                    .Where(i => i.IsNotNullOrEmpty())
                    .Join(" .");

                tr.AddTd().SetText(startDate);
                tr.AddTd().SetText(comments);
                tr.AddTd().SetText("active");
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                var rowCount = body.tr.Length;
                tr.AddTd().AddContent().SetId("Med_{0}".FormatWith(rowCount)).SetText("None");
                tr.AddTd().SetEmptyText();
                tr.AddTd().SetEmptyText();
                tr.AddTd().SetEmptyText();
            }
        }

        private void InitializeMedicationsComponentEntries(POCD_MT000040Section section, IEnumerable<PatientMedication> source)
        {
            var patientMedications = source.ToArray();
            var mappings = _cdaModelMapper.GetRxNormDescriptionMap(patientMedications.Select(i => i.Medication));

            foreach (var pm in patientMedications)
            {
                var latestDetail = pm.PatientMedicationDetails.OrderByDescending(i => i.MedicationEventDateTime).FirstOrDefault();
                if (latestDetail != null && (latestDetail.IsDeactivated || _invalidMedicationEventTypes.Contains(latestDetail.MedicationEventType))) continue;

                AddPatientMedicationEntry(section, pm, mappings);
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddPatientMedicationEntry(section, null, mappings);
            }
        }

        private static void AddPatientMedicationEntry(POCD_MT000040Section section, PatientMedication patientMedication, IDictionary<Medication, RxNormModel> mappings)
        {
            // set up all the required elements for the component section
            var substanceAdministration = section.AddEntry()
                                        .SetTypeCode(x_ActRelationshipEntry.DRIV)
                                        .AddSubstanceAdministration()
                                        .SetClassCode("SBADM")
                                        .SetMoodCode(x_DocumentSubstanceMood.EVN);
            substanceAdministration.AddTemplateId().SetRoot(Oids.MedicationActivity);
            substanceAdministration.AddId().SetRootAndExtension(patientMedication ?? new PatientMedication());
            substanceAdministration.AddStatusCode().SetCode("active");

            var effectiveTime = substanceAdministration.AddEffectiveTime();

            var product = substanceAdministration.AddConsumable().AddManufacturedProduct().SetClassCode(RoleClassManufacturedProduct.MANU);
            product.AddTemplateId().SetRoot(Oids.MedicationInformation);

            // set element values based on patient's medication data
            if (patientMedication != null)
            {
                var rowNumber = section.entry.Length;
                RxNormModel rxNormModel = mappings.GetValue(patientMedication.Medication);
                if (rxNormModel != null)
                {
                    // add rxNorm medication text
                    substanceAdministration.AddText().SetText("{0}, {1}".FormatWith(rxNormModel.Str, rxNormModel.RxCui)).AddReference().SetValue("#Med_{0}".FormatWith(rowNumber));

                    // set rxNorm medication details
                    product.AddMaterial().AddCode().SetCode(rxNormModel.RxCui).SetCodeSystem(CodeSystem.RxNorm).SetDisplayName(rxNormModel.Str)
                                 .AddOriginalText().SetText("{0}, {1}".FormatWith(rxNormModel.Str, rxNormModel.RxCui)).AddReference().SetValue("#Med{0}".FormatWith(rowNumber));
                }
                else
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find RxNorm mapping for Medication {0} (Medication Id {1}).".FormatWith(patientMedication.Medication.Name, patientMedication.MedicationId));
                    substanceAdministration.AddText().SetText("{0}".FormatWith("Unknown RxNorm medication")).AddReference().SetValue("#Med_{0}".FormatWith(rowNumber));
                    // set null medication details
                    product.AddMaterial().AddCode().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.RxNorm).SetDisplayName(patientMedication.Medication.Name)
                                 .AddOriginalText().SetText("{0}".FormatWith("Unknown RxNorm medication")).AddReference().SetValue("#Med{0}".FormatWith(rowNumber));
                }

                var medicationDetail = patientMedication.PatientMedicationDetails
                    .Where(i => !i.IsDeactivated
                        && i.MedicationEventType == MedicationEventType.PatientStarted)
                    .OrderByDescending(i => i.MedicationEventDateTime)
                    .FirstOrDefault();

                if (medicationDetail != null)
                {
                    effectiveTime.AddLow().SetValue(medicationDetail.MedicationEventDateTime.FormatForCdaAlternate());
                }
            }
            else // default values for when there are no patient medications
            {
                effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                product.AddMaterial().AddCode().SetNullFlavor(CdaNullFlavor.NI).SetCodeSystem(CodeSystem.RxNorm);
            }

            effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
        }

        #endregion

        #region Encounter Diagnoses and Problem List

        public POCD_MT000040Component3 AddNewProblemsComponent(IEnumerable<PatientDiagnosis> source)
        {
            #region static values

            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.ProblemSectionEntriesOptional);
            section.AddTemplateId().SetRoot(Oids.ProblemSectionEntriesRequired);
            section.AddCode().SetCode("11450-4").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("PROBLEM LIST");
            section.AddTitle("Problems");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();

            var tableHeaders = new[] { "Type", "Name And Code", "Date Range", "Status" };
            tableHeaders.ToList().ForEach(headerText => tr.AddTh(headerText));

            var body = table.AddTbody();

            #endregion

            IList<PatientDiagnosisDetail> diagnosisDetails = new List<PatientDiagnosisDetail>();

            source.Select(i => i.PatientDiagnosisDetails.Where(pdd => !pdd.IsDeactivated).OrderByDescending(pdd => pdd.EnteredDateTime))
                .ForEachWithSinglePropertyChangedNotification(a => a.ForEachWithSinglePropertyChangedNotification(diagnosisDetails.Add));

            InitializeProblemsComponentTableRows(body, diagnosisDetails);
            InitializeProblemsComponentEntries(section, diagnosisDetails);
            InitializeEmptyComponentTableRow(body, body.tr, tableHeaders.Count());

            return component;
        }

        private void InitializeProblemsComponentTableRows(StrucDocTbody body, IEnumerable<PatientDiagnosisDetail> source)
        {
            var diagnosisDetails = source.ToArray();
            var clinicalConditionMappings = _cdaModelMapper.GetSnomedModelMap(diagnosisDetails.Where(i => i.PatientDiagnosis.ClinicalConditionId.HasValue).Select(i => i.PatientDiagnosis.ClinicalCondition).Distinct());

            foreach (var patientDiagnsosisDetail in diagnosisDetails.Where(i => i.IsEncounterDiagnosis).OrderByDescending(i => i.EnteredDateTime))
            {
                AddProblemTableRow("Encounter Diagnosis", body, clinicalConditionMappings, patientDiagnsosisDetail);
            }

            foreach (var patientDiagnsosisDetail in diagnosisDetails.Where(i => i.IsOnProblemList).OrderByDescending(i => i.EnteredDateTime))
            {
                AddProblemTableRow("Problem", body, clinicalConditionMappings, patientDiagnsosisDetail);
            }

            if (body.tr.IsNullOrEmpty())
            {
                var row = body.AddTr();
                var rowNum = body.tr.Length;
                row.SetId("ProblemSummary_{0}".FormatWith(rowNum));
                row.AddTd().SetEmptyText();
                row.AddTd().SetText("None");
                row.AddTd().SetEmptyText();
            }
        }

        private static void AddProblemTableRow(string problemName, StrucDocTbody body, Dictionary<ClinicalCondition, SnomedDescriptionModel> clinicalConditionMappings, PatientDiagnosisDetail patientDiagnsosisDetail)
        {
            var snomedModel = clinicalConditionMappings.GetValue(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition);
            var row = body.AddTr();
            var rowNum = body.tr.Length;
            row.SetId("ProblemSummary_{0}".FormatWith(rowNum));

            var axisQualifiers = string.Join(", ", patientDiagnsosisDetail.PatientDiagnosisDetailAxisQualifiers
                .Select(aq => aq.AxisQualifier.GetDisplayName()));

            row.AddTd().SetText(problemName);
            if (snomedModel == null)
            {
                VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for diagnosis detail for {0} (PatientDiagnsosisDetail Id {1}).".FormatWith(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition.Name, patientDiagnsosisDetail.Id));
                row.AddTd().SetText("{0}, {1}: {2}".FormatWith(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
            }
            else
            {
                row.AddTd().SetText("{0} {1}, {2}: {3}"
                .FormatWith(axisQualifiers, snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId)
                .TrimStart()); // Remove whitespaces if no axis qualifiers
            }

            // Date column
            var dateCellTd = row.AddTd();
            if (patientDiagnsosisDetail.StartDateTime.HasValue && patientDiagnsosisDetail.EndDateTime.HasValue)
            {
                dateCellTd.SetText("{0}-{1}".FormatWith(CdaExtensions.FormatForCdaTextElement(patientDiagnsosisDetail.StartDateTime.Value), CdaExtensions.FormatForCdaTextElement(patientDiagnsosisDetail.EndDateTime.Value)));
            }
            else if (patientDiagnsosisDetail.StartDateTime.HasValue)
            {
                dateCellTd.SetText(CdaExtensions.FormatForCdaTextElement(patientDiagnsosisDetail.StartDateTime.Value));
            }
            else
            {
                dateCellTd.SetText(CdaExtensions.FormatForCdaTextElement(patientDiagnsosisDetail.EnteredDateTime));
            }

            row.AddTd().SetText(patientDiagnsosisDetail.ClinicalConditionStatus.ToString());
        }

        private void InitializeProblemsComponentEntries(POCD_MT000040Section section, IEnumerable<PatientDiagnosisDetail> source)
        {
            var diagnosisDetails = source.ToArray();
            var clinicalConditionMappings = _cdaModelMapper.GetSnomedModelMap(diagnosisDetails.Select(i => i.PatientDiagnosis.ClinicalCondition).Distinct());

            foreach (var patientDiagnsosisDetail in diagnosisDetails.Where(i => i.IsEncounterDiagnosis).OrderByDescending(i => i.EnteredDateTime))
            {
                AddNewProblemEntry("Diagnosis", section, patientDiagnsosisDetail, clinicalConditionMappings);
            }

            foreach (var patientDiagnsosisDetail in diagnosisDetails.Where(i => i.IsOnProblemList).OrderByDescending(i => i.EnteredDateTime))
            {
                AddNewProblemEntry("Problem", section, patientDiagnsosisDetail, clinicalConditionMappings);
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddNewProblemEntry("", section, null, clinicalConditionMappings);
            }
        }

        private static void AddNewProblemEntry(string problemName, POCD_MT000040Section section, PatientDiagnosisDetail patientDiagnsosisDetail, IDictionary<ClinicalCondition, SnomedDescriptionModel> clinicalConditionMappings)
        {
            var act = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV).AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.EVN);
            act.AddTemplateId().SetRoot(Oids.ProblemConcernAct);
            act.AddId().SetRootAndExtension(patientDiagnsosisDetail ?? new PatientDiagnosisDetail());
            act.AddCode().SetCode("CONC").SetCodeSystem(CodeSystem.ActClass).SetDisplayName("Concern");

            var effectiveTime = act.AddEffectiveTime();

            var observation = act.AddEntryRelationship().SetTypeCode(x_ActRelationshipEntryRelationship.SUBJ)
                                .AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
            observation.AddTemplateId().SetRoot(Oids.ProblemObservation);

            if (patientDiagnsosisDetail == null || patientDiagnsosisDetail.PatientDiagnosis == null)
            {
                observation.negationInd = true;
                observation.AddId().SetRootAndExtension(new PatientDiagnosis());
                observation.AddCode<CD>().SetCode("ASSERTION").SetCodeSystem(CodeSystem.ActCode);
            }
            else
            {
                // Check whether it is actually a negation diagnosis
                var isNegation = patientDiagnsosisDetail.PatientDiagnosisDetailAxisQualifiers.Any(aq =>
                    aq.AxisQualifier == AxisQualifier.ConfirmationOfNegativeFindings
                    || aq.AxisQualifier == AxisQualifier.HistoryOf
                    || aq.AxisQualifier == AxisQualifier.RuleOut);

                if (isNegation)
                {
                    observation.negationInd = true;
                    observation.negationIndSpecified = true;
                }

                // Set observation id
                observation.AddId().SetRootAndExtension(patientDiagnsosisDetail.PatientDiagnosis);
                observation.AddCode<CD>().SetCode("282291009").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(problemName);
            }

            var rowNumber = section.entry.Length;
            observation.AddText().AddReference().SetValue("#ProblemSummary_{0}".FormatWith(rowNumber));
            observation.AddStatusCode().SetCode("completed");
            var observationEffectiveTime = observation.AddEffectiveTime();

            if (patientDiagnsosisDetail != null && patientDiagnsosisDetail.PatientDiagnosis != null)
            {
                var concernLevel = patientDiagnsosisDetail.PatientDiagnosis.PatientProblemConcernLevels.OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();

                act.AddStatusCode().SetCode((concernLevel == null ? ConcernLevel.Active : concernLevel.ConcernLevel).ToString().ToLower());

                if (concernLevel != null && concernLevel.ConcernLevel == ConcernLevel.Completed)
                {
                    if (concernLevel.ConcernEndDate.HasValue)
                    {
                        effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(concernLevel.ConcernEndDate.Value));
                    }
                    else
                    {
                        effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
                    }
                }
                else
                {
                    if (concernLevel != null && concernLevel.ConcernStartDate.HasValue)
                    {
                        effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(concernLevel.ConcernStartDate.Value));
                    }
                    else if (patientDiagnsosisDetail.Encounter != null)
                    {
                        effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(patientDiagnsosisDetail.Encounter.StartDateTime));
                    }
                    else
                    {
                        effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                    }
                }

                if (patientDiagnsosisDetail.ClinicalConditionStatus == ClinicalConditionStatus.Resolved)
                {
                    if (patientDiagnsosisDetail.EndDateTime.HasValue)
                    {
                        observationEffectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(patientDiagnsosisDetail.EndDateTime.Value));
                    }
                    else
                    {
                        observationEffectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.UNK);
                    }
                }
                else
                {
                    if (patientDiagnsosisDetail.StartDateTime.HasValue)
                    {
                        observationEffectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(patientDiagnsosisDetail.StartDateTime.Value));
                    }
                    else
                    {
                        observationEffectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.UNK);
                    }
                    //observationEffectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
                }

                SnomedDescriptionModel snomedModel = clinicalConditionMappings.GetValue(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition);
                if (snomedModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find SNOMED mapping for patient diagnosis clinical condition {0} (PatientDiagnosis Id {1}).".FormatWith(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition.Name, patientDiagnsosisDetail.PatientDiagnosisId));
                    observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(patientDiagnsosisDetail.PatientDiagnosis.ClinicalCondition.Name);
                }
                else
                {
                    observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term);
                }
            }
            else
            {
                act.AddStatusCode().SetCode(ConcernLevel.Active.ToString());
                effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);

                observation.AddValue<CD>().SetCode("55607006").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Problem");

                observationEffectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                observationEffectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
            }
        }

        #endregion

        #region Procedures

        public POCD_MT000040Component3 AddNewProceduresComponent(Patient patient)
        {
            var source = patient.PatientProcedurePerformeds.OfType<IClinicalProcedurePerformed>()
                .Concat(patient.PatientDiagnosticTestPerformeds)
                .ToArray();

            return AddNewProceduresComponent(source);
        }

        public POCD_MT000040Component3 AddNewProceduresComponent(IEnumerable<IClinicalProcedurePerformed> source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.ProceduresSectionEntriesOptional);
            section.AddTemplateId().SetRoot(Oids.ProceduresSectionEntriesRequired);
            section.AddCode().SetCode("47519-4").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("PROCEDURES");
            section.AddTitle("Procedures");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Name");
            tr.AddTh("Date");

            var body = table.AddTbody();

            #endregion

            var supportedCategories = new[] { ClinicalProcedureCategory.Procedure, ClinicalProcedureCategory.DiagnosticTest, ClinicalProcedureCategory.Exam, ClinicalProcedureCategory.Act };

            source = source
                .Where(i => supportedCategories.Contains(i.ClinicalProcedure.ClinicalProcedureCategory))
                .OrderByDescending(i => i.PerformedDateTime).ToArray();

            InitializeProceduresComponentTableRows(body, source);
            InitializeProceduresComponentEntries(section, source);
            InitializeEmptyComponentTableRow(body, source, 2);

            return component;
        }

        private void InitializeProceduresComponentTableRows(StrucDocTbody body, IEnumerable<IClinicalProcedurePerformed> source)
        {
            source = source.ToArray();
            var snomedMap = _cdaModelMapper.GetSnomedModelMap(source.Select(i => i.ClinicalProcedure));

            foreach (var procedurePerformed in source)
            {
                var snomedModel = snomedMap.GetValue(procedurePerformed.ClinicalProcedure);
                var tr = body.AddTr();
                var rowNumber = body.tr.Length;
                tr.SetId("Procedure_{0}".FormatWith(rowNumber));
                if (snomedModel == null)
                {
                    if (IsProcedureNotExcluded(procedurePerformed.ClinicalProcedure))
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for procedure for {0} (ClinicalProcedure Id {1}).".FormatWith(procedurePerformed.ClinicalProcedure.Name, procedurePerformed.ClinicalProcedure.Id));
                    tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(procedurePerformed.ClinicalProcedure.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                }
                else
                {
                    tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                }
                tr.AddTd().SetText(procedurePerformed.PerformedDateTime.FormatForCdaTextElement());
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                var rowNumber = body.tr.Length;
                tr.SetId("Procedure_{0}".FormatWith(rowNumber));
                tr.AddTd().SetText("None");
                tr.AddTd().SetEmptyText();
            }
        }

        private void InitializeProceduresComponentEntries(POCD_MT000040Section section, IEnumerable<IClinicalProcedurePerformed> source)
        {
            var patientProcedurePerformeds = source.ToArray();
            var snomedMap = _cdaModelMapper.GetSnomedModelMap(patientProcedurePerformeds.Select(i => i.ClinicalProcedure));

            // Get Procedures whose Category is Procedure ordered by (desc) date first,
            // then Procedures whose Category is Observation ordered by (desc) date next,
            // then Procedures whose Category is Act ordered by (desc) date.

            foreach (var clinicalProcedurePerformed in patientProcedurePerformeds.Where(i => i.ClinicalProcedure.ClinicalProcedureCategory == ClinicalProcedureCategory.Procedure))
            {
                var snomedModel = snomedMap.GetValue(clinicalProcedurePerformed.ClinicalProcedure);

                if (snomedModel == null)
                {
                    if (IsProcedureNotExcluded(clinicalProcedurePerformed.ClinicalProcedure))
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for procedure {0} (ClinicalProcedure Id {1}).".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, clinicalProcedurePerformed.Id));
                }

                AddProcedureEntryCategoryProcedure(section, clinicalProcedurePerformed, snomedModel);
            }


            foreach (var clinicalProcedurePerformed in patientProcedurePerformeds.Where(i => i.ClinicalProcedure.ClinicalProcedureCategory == ClinicalProcedureCategory.DiagnosticTest || i.ClinicalProcedure.ClinicalProcedureCategory == ClinicalProcedureCategory.Exam))
            {
                var snomedModel = snomedMap.GetValue(clinicalProcedurePerformed.ClinicalProcedure);

                if (snomedModel == null)
                {
                    if (IsProcedureNotExcluded(clinicalProcedurePerformed.ClinicalProcedure))
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for procedure {0} (ClinicalProcedure Id {1}).".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, clinicalProcedurePerformed.Id));
                }

                AddProcedureEntryCategoryObservation(section, clinicalProcedurePerformed, snomedModel);
            }

            foreach (var clinicalProcedurePerformed in patientProcedurePerformeds.Where(i => i.ClinicalProcedure.ClinicalProcedureCategory == ClinicalProcedureCategory.Act))
            {
                var snomedModel = snomedMap.GetValue(clinicalProcedurePerformed.ClinicalProcedure);

                if (snomedModel == null)
                {
                    if (IsProcedureNotExcluded(clinicalProcedurePerformed.ClinicalProcedure))
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for procedure {0} (ClinicalProcedure Id {1}).".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, clinicalProcedurePerformed.Id));
                }

                AddProcedureEntryCategoryAct(section, clinicalProcedurePerformed, snomedModel);
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddProcedureEntryCategoryProcedure(section, null, null);
            }
        }

        private static void AddProcedureEntryCategoryProcedure(POCD_MT000040Section section, IClinicalProcedurePerformed clinicalProcedurePerformed, SnomedDescriptionModel snomedModel)
        {
            var procedure = section.AddEntry().AddProcedure().SetClassCode("PROC").SetMoodCode(x_DocumentProcedureMood.EVN);
            procedure.AddTemplateId().SetRoot(Oids.ProcedureActivityProcedure);
            procedure.AddId().SetRootAndExtension(clinicalProcedurePerformed ?? new PatientProcedurePerformed());
            procedure.AddStatusCode().SetCode("completed");

            var rowNumber = section.entry.Length;

            if (clinicalProcedurePerformed != null)
            {
                ED originalText;
                if (snomedModel == null)
                {
                    originalText = procedure.AddCode<CE>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedurePerformed.ClinicalProcedure.Name).AddOriginalText();
                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, "Unknown"));
                }
                else
                {
                    originalText = procedure.AddCode<CE>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term).AddOriginalText();
                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(snomedModel.Term, snomedModel.ConceptId));
                }
                procedure.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(clinicalProcedurePerformed.PerformedDateTime));
            }
            else
            {
                procedure.AddCode<CE>().SetNullFlavor(CdaNullFlavor.NI).SetCodeSystem(CodeSystem.SnomedCt);

                procedure.AddEffectiveTime().SetNullFlavor(CdaNullFlavor.NI);
            }
        }

        private static void AddProcedureEntryCategoryObservation(POCD_MT000040Section section, IClinicalProcedurePerformed clinicalProcedurePerformed, SnomedDescriptionModel snomedModel)
        {
            var observation = section.AddEntry().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
            observation.AddTemplateId().SetRoot(Oids.ProcedureActivityObservation);
            observation.AddId().SetRootAndExtension(clinicalProcedurePerformed);
            observation.AddStatusCode().SetCode("completed");

            var rowNumber = section.entry.Length;

            if (clinicalProcedurePerformed != null)
            {
                if (snomedModel == null)
                {
                    var originalText = observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedurePerformed.ClinicalProcedure.Name)
                                              .AddOriginalText();

                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, "Unknown"));
                }
                else
                {
                    var originalText = observation.AddCode<CE>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term)
                                                  .AddOriginalText();

                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(snomedModel.Term, snomedModel.ConceptId));
                }

                observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(clinicalProcedurePerformed.PerformedDateTime));

                // add an empty CD
                observation.AddValue<CD>();
            }
            else
            {
                var originalText = observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.NI).SetCodeSystem(CodeSystem.SnomedCt)
                                              .AddOriginalText();

                originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                observation.AddEffectiveTime().SetNullFlavor(CdaNullFlavor.NI);

                // add an empty CD
                observation.AddValue<CD>();
            }
        }

        private static void AddProcedureEntryCategoryAct(POCD_MT000040Section section, IClinicalProcedurePerformed clinicalProcedurePerformed, SnomedDescriptionModel snomedModel)
        {
            var observation = section.AddEntry().AddAct().SetClassCode(x_ActClassDocumentEntryAct.ACT).SetMoodCode(x_DocumentActMood.INT);
            observation.AddTemplateId().SetRoot(Oids.ProcedureActivityAct);
            observation.AddId().SetRootAndExtension(clinicalProcedurePerformed ?? new PatientProcedurePerformed());
            observation.AddStatusCode().SetCode("completed");

            var rowNumber = section.entry.Length;

            if (clinicalProcedurePerformed != null)
            {
                if (snomedModel == null)
                {
                    var originalText = observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(clinicalProcedurePerformed.ClinicalProcedure.Name)
                                              .AddOriginalText();

                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(clinicalProcedurePerformed.ClinicalProcedure.Name, "Unknown"));
                }
                else
                {
                    var originalText = observation.AddCode<CE>().SetCode(snomedModel.ConceptId).SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName(snomedModel.Term)
                                                  .AddOriginalText();

                    originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                    originalText.SetText("{0}, {1}".FormatWith(snomedModel.Term, snomedModel.ConceptId));
                }
                observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(clinicalProcedurePerformed.PerformedDateTime));
            }
            else
            {
                var originalText = observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.NI).SetCodeSystem(CodeSystem.SnomedCt)
                                              .AddOriginalText();

                originalText.AddReference().SetValue("#Procedure_{0}".FormatWith(rowNumber));
                observation.AddEffectiveTime().SetNullFlavor(CdaNullFlavor.NI);
            }
        }

        #endregion

        #region Immunizations

        public POCD_MT000040Component3 AddNewImmunizationsComponent(IEnumerable<PatientVaccination> source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.ImmunizationSectionEntriesRequired);
            section.AddCode().SetCode("11369-6")
                             .SetCodeSystem(CodeSystem.Loinc)
                             .SetDisplayName("History of immunizations");
            section.AddTitle("Immunizations");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Vaccine");
            tr.AddTh("Date");

            var body = table.AddTbody();

            #endregion

            var vaccinations = source.ToArray();

            InitializeImmunizationsComponentTableRows(body, vaccinations);
            InitializeImmunizationsComponentEntries(section, vaccinations);

            return component;
        }

        private void InitializeImmunizationsComponentTableRows(StrucDocTbody body, IEnumerable<PatientVaccination> source)
        {
            var patientVaccinations = source.ToArray();
            var vaccinationMappings = _cdaModelMapper.GetCvxModelMap(patientVaccinations.Select(i => i.Vaccine));

            foreach (var patientVaccination in patientVaccinations.OrderByDescending(i => i.AdministeredDateTime))
            {
                var cvxAdministeredModel = vaccinationMappings.GetValue(patientVaccination.Vaccine);
                var tr = body.AddTr();
                var rowNumber = body.tr.Length;
                if (cvxAdministeredModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find CVX mapping for Vaccine for {0} (VaccineId Id {1}).".FormatWith(patientVaccination.Vaccine.Name, patientVaccination.VaccineId));
                    tr.AddTd().AddContent().SetText("{0}, {1}: {2}".FormatWith(patientVaccination.Vaccine.Name, Constants.CvxAdministeredExternalSystemNameForDisplay, "Unknown"))
                                       .SetId("immun{0}".FormatWith(rowNumber));
                }
                else
                {
                    tr.AddTd().AddContent().SetText("{0}, {1}: {2}".FormatWith(cvxAdministeredModel.FullVaccineName, Constants.CvxAdministeredExternalSystemNameForDisplay, cvxAdministeredModel.CvxCode))
                                       .SetId("immun{0}".FormatWith(rowNumber));
                }
                tr.AddTd().SetText(patientVaccination.AdministeredDateTime.HasValue
                                       ? CdaExtensions.FormatForCdaTextElement(patientVaccination.AdministeredDateTime.Value)
                                       : "Unknown");
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                var rowNumber = body.tr.Length;
                tr.AddTd().AddContent().SetText("None")
                                       .SetId("immun{0}".FormatWith(rowNumber));
                tr.AddTd().SetEmptyText();
            }
        }

        private void InitializeImmunizationsComponentEntries(POCD_MT000040Section section, IEnumerable<PatientVaccination> source)
        {
            var patientVaccinations = source.ToArray();
            var vaccinationMappings = _cdaModelMapper.GetCvxModelMap(patientVaccinations.Select(i => i.Vaccine));

            foreach (var patientVaccination in patientVaccinations.OrderByDescending(i => i.AdministeredDateTime))
            {
                AddImmunizationEntry(section, patientVaccination, vaccinationMappings);
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddImmunizationEntry(section, null, null);
            }
        }

        private static void AddImmunizationEntry(POCD_MT000040Section section, PatientVaccination patientVaccination, IDictionary<Vaccine, CvxAdministeredModel> vaccinationMappings)
        {
            var substanceAdmin = section.AddEntry().AddSubstanceAdministration()
                                                      .SetClassCode("SBADM")
                                                      .SetMoodCode(x_DocumentSubstanceMood.EVN)
                                                      .SetNegationInd(false);
            substanceAdmin.AddTemplateId().SetRoot(Oids.SubstanceAdministration);
            substanceAdmin.AddId().SetRootAndExtension(patientVaccination ?? new PatientVaccination());

            substanceAdmin.AddStatusCode().SetCode("completed");


            var manufacturedProduct = substanceAdmin.AddConsumable().AddManufacturedProduct().SetClassCode(RoleClassManufacturedProduct.MANU);
            manufacturedProduct.AddTemplateId().SetRoot(Oids.ImmunizationMedicationInformation);

            var code = manufacturedProduct.AddMaterial().AddCode();

            if (patientVaccination != null)
            {
                var rowNum = section.entry.Length;
                substanceAdmin.AddText().AddReference().SetValue("#immun{0}".FormatWith(rowNum));

                substanceAdmin.AddEffectiveTime().SetValue(patientVaccination.AdministeredDateTime.HasValue
                                                               ? CdaExtensions.FormatForCdaAlternate(patientVaccination.AdministeredDateTime.Value)
                                                               : string.Empty)
                              .SetNullFlavor(!patientVaccination.AdministeredDateTime.HasValue ? CdaNullFlavor.NI : new CdaNullFlavor?());

                // weird cvx code rule - when the values are 0-9 then you must represent them as 01-09 (padded left with 1 zero character).  Otherwise validation fails.
                // The Cvx codes listed here have them that way as well http://www2a.cdc.gov/vaccines/iis/iisstandards/vaccines.asp?rpt=cpt
                CvxAdministeredModel cvxAdministeredModel = vaccinationMappings.GetValue(patientVaccination.Vaccine);
                if (cvxAdministeredModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find CVX mapping for Vaccine for {0} (VaccineId Id {1}).".FormatWith(patientVaccination.Vaccine.Name, patientVaccination.VaccineId));
                    code.SetNullFlavor(CdaNullFlavor.UNK).SetCodeSystem(CodeSystem.Cvx)
                        .SetCodeSystemName("CVX").SetDisplayName(patientVaccination.Vaccine.Name);
                }
                else
                {
                    var paddedCvxCode = cvxAdministeredModel.CvxCode > 0 && cvxAdministeredModel.CvxCode < 10 ? cvxAdministeredModel.CvxCode.ToString().PadLeft(2, '0') : cvxAdministeredModel.CvxCode.ToString();
                    code.SetCode(paddedCvxCode)
                                              .SetCodeSystem(CodeSystem.Cvx)
                                              .SetDisplayName(cvxAdministeredModel.FullVaccineName);
                }
                code.AddOriginalText().AddReference().SetValue("#immun{0}".FormatWith(rowNum));
            }
            else
            {
                substanceAdmin.AddEffectiveTime().SetNullFlavor(CdaNullFlavor.NI);
                code.SetNullFlavor(CdaNullFlavor.NI).SetCodeSystem(CodeSystem.Cvx);
            }
        }

        #endregion

        #region Lab test / Lab values / results

        public POCD_MT000040Component3 AddNewLabResultsComponent(IEnumerable<PatientLaboratoryTestResult> source)
        {
            #region static values

            var component = new POCD_MT000040Component3();
            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.ResultsSectionEntriesRequired);
            section.AddCode().SetCode("30954-2").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("RESULTS");
            section.AddTitle("Results");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();

            var tableHeaders = new[] { "Name", "Result Values", "Date" };
            tableHeaders.ToList().ForEach(headerText => tr.AddTh(headerText));

            var tableBody = table.AddTbody();

            #endregion

            var testResults = source.ToArray();

            InitializeLabResultsComponentTableRows(tableBody, testResults);
            InitializeLabResultsComponentEntries(section, testResults);
            InitializeEmptyComponentTableRow(tableBody, tableBody.tr, tableHeaders.Count());
            return component;
        }

        private void InitializeLabResultsComponentTableRows(StrucDocTbody body, IEnumerable<PatientLaboratoryTestResult> source)
        {
            var patientLabTestResults = source.ToArray();
            var mappings = _cdaModelMapper.GetLoincDescriptionMap(patientLabTestResults.Select(i => i.LaboratoryTest));

            foreach (var patientLabTestResult in patientLabTestResults)
            {
                // ReSharper disable UnusedVariable
                // This is according to spec
                foreach (var labTestOrder in patientLabTestResult.EncounterLaboratoryTestOrders)
                // ReSharper restore UnusedVariable
                {
                    var resultDate = GetResultDate(patientLabTestResult);

                    var loincModel = mappings.GetValue(patientLabTestResult.LaboratoryTest);
                    var tr = body.AddTr();
                    var rowNumber = body.tr.Length;

                    var contentId = "result{0}".FormatWith(rowNumber);
                    string contentText;
                    if (loincModel == null)
                    {
                        VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped LOINC entity for LaboratoryTest for {0} (LabTestResult Id {1}).".FormatWith(patientLabTestResult.LaboratoryTest.Name, patientLabTestResult.Id));
                        contentText = "{0}, {1}: {2}".FormatWith(patientLabTestResult.LaboratoryTest.Name, Constants.LoincExternalSystemName, "Unknown");
                    }
                    else
                    {
                        contentText = "{0}, {1}: {2}".FormatWith(loincModel.Component, Constants.LoincExternalSystemName, loincModel.LoincNum);
                    }
                    tr.AddTd().AddContent().SetId(contentId).SetText(contentText);

                    var unitValueText = patientLabTestResult.ResultUnit.HasValue ? patientLabTestResult.ResultUnit.Value.ToString() : "--";
                    var unitOfMeasureText = patientLabTestResult.UnitOfMeasurement.IfNotNull(u => u.Abbreviation.IsNotNullOrEmpty() ? string.Format("({0})", u.Abbreviation) : "");

                    tr.AddTd().SetText(string.Format("{0}{1}", unitValueText, unitOfMeasureText));
                    tr.AddTd().SetText(resultDate.HasValue ? CdaExtensions.FormatForCdaTextElement(resultDate.Value) : "unknown");
                }
            }

            if (body.tr.IsNullOrEmpty())
            {
                var tr = body.AddTr();
                var rowNumber = body.tr.Length;

                var contentId = "result{0}".FormatWith(rowNumber);
                const string contentText = "None";

                tr.AddTd().AddContent().SetId(contentId).SetText(contentText);
                tr.AddTd().SetEmptyText();
                tr.AddTd().SetEmptyText();
            }
        }

        private void InitializeLabResultsComponentEntries(POCD_MT000040Section section, IEnumerable<PatientLaboratoryTestResult> source)
        {
            var patientLabTestResults = source.ToArray();
            var mappings = _cdaModelMapper.GetLoincDescriptionMap(patientLabTestResults.Select(i => i.LaboratoryTest));

            foreach (var patientLabTestResult in patientLabTestResults)
            {
                var resultDate = GetResultDate(patientLabTestResult);

                var loincModel = mappings.GetValue(patientLabTestResult.LaboratoryTest);
                if (loincModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped LOINC entity for LaboratoryTest for {0} (LabTestResult Id {1}).".FormatWith(patientLabTestResult.LaboratoryTest.Name, patientLabTestResult.Id));
                }

                foreach (var labTestOrder in patientLabTestResult.EncounterLaboratoryTestOrders)
                {
                    AddLabResultEntry(section, patientLabTestResult, labTestOrder, loincModel, resultDate);
                }
            }

            if (section.entry.IsNullOrEmpty())
            {
                AddLabResultEntry(section, null, null, null, null);
            }
        }

        private static void AddLabResultEntry(POCD_MT000040Section section, PatientLaboratoryTestResult patientLabTestResult, EncounterLaboratoryTestOrder labTestOrder, LoincModel loincModel, DateTime? resultDate)
        {
            var organizer = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV)
                       .AddOrganizer().SetClassCode(x_ActClassDocumentEntryOrganizer.BATTERY).SetMoodCode("EVN");

            organizer.AddTemplateId().SetRoot(Oids.ResultsOrganizer);
            organizer.AddId().SetRootAndExtension(labTestOrder ?? new EncounterLaboratoryTestOrder());
            organizer.AddStatusCode().SetCode("completed");

            var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
            observation.AddTemplateId().SetRoot(Oids.ResultsObservation);
            observation.AddId().SetRootAndExtension(patientLabTestResult ?? new PatientLaboratoryTestResult());
            observation.AddStatusCode().SetCode("completed");

            var rowNumber = section.entry.Length;

            if (patientLabTestResult != null)
            {
                if (loincModel == null)
                {
                    organizer.AddCode<CE>().SetNullFlavor(CdaNullFlavor.UNK).SetDisplayName(patientLabTestResult.LaboratoryTest.Name).SetCodeSystem(CodeSystem.Loinc);

                    observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.UNK).SetDisplayName(patientLabTestResult.LaboratoryTest.Name).SetCodeSystem(CodeSystem.Loinc);
                }
                else
                {
                    organizer.AddCode<CE>().SetCode(loincModel.LoincNum).SetDisplayName(loincModel.Component).SetCodeSystem(CodeSystem.Loinc);

                    observation.AddCode<CE>().SetCode(loincModel.LoincNum).SetDisplayName(loincModel.Component).SetCodeSystem(CodeSystem.Loinc);
                }
                observation.AddText().AddReference().SetValue("#result{0}".FormatWith(rowNumber));
                // The validation rules for hl7 will fail if a <value> element is not added to the observation.  Should we consider null ResultUnit bad data?
                if (patientLabTestResult.ResultUnit != null) observation.AddValue<PQ>().SetUnit(patientLabTestResult.UnitOfMeasurement.IfNotNull(u => u.Abbreviation)).SetValue(patientLabTestResult.ResultUnit.ToString());
                if (patientLabTestResult.LaboratoryTestAbnormalFlag != null) observation.AddInterpretationCode().SetCode(patientLabTestResult.LaboratoryTestAbnormalFlag.Abbreviation).SetCodeSystem(CodeSystem.ObservationInterpretation);
                if (patientLabTestResult.LaboratoryNormalRange != null) observation.AddReferenceRange().AddObservationRange().AddText(patientLabTestResult.LaboratoryNormalRange.Name);
            }
            else
            {
                organizer.AddCode<CE>().SetNullFlavor(CdaNullFlavor.NI);

                observation.AddCode<CE>().SetNullFlavor(CdaNullFlavor.NI);
                observation.AddValue<PQ>().SetNullFlavor(CdaNullFlavor.NI);
                observation.AddInterpretationCode().SetCodeSystem(CodeSystem.ObservationInterpretation).SetNullFlavor(CdaNullFlavor.NI);
                observation.AddReferenceRange().AddObservationRange().AddText("").SetNullFlavor(CdaNullFlavor.NI);
            }

            if (resultDate.HasValue)
            {
                observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(resultDate.Value));
            }
            else
            {
                observation.AddEffectiveTime().SetNullFlavor(CdaNullFlavor.NI);
            }
        }

        #endregion

        #region Social History

        public POCD_MT000040Component3 AddNewSocialHistoryComponent(IEnumerable<PatientSmokingStatus> source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.SocialHistorySection);
            section.AddCode().SetCode("29762-2").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Social History");
            section.AddTitle("Social History");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Social History Element");
            tr.AddTh("Description");
            tr.AddTh("Effective Dates");

            var body = table.AddTbody();

            #endregion

            var smokingStatuses = source.ToArray();

            InitializeSocialHistoryComponentTableRows(body, smokingStatuses);
            InitializeSocialHistoryComponentEntries(section, smokingStatuses);

            return component;
        }

        private void InitializeSocialHistoryComponentTableRows(StrucDocTbody body, IEnumerable<PatientSmokingStatus> source)
        {
            var patientSmokingStatuses = source.ToArray();
            var smokingStatusMappings = _cdaModelMapper.GetSnomedModelMap(patientSmokingStatuses.Select(i => i.SmokingCondition));

            foreach (var patientSmokingStatus in patientSmokingStatuses.OrderByDescending(i => i.EnteredDateTime))
            {
                var snomedModel = smokingStatusMappings.GetValue(patientSmokingStatus.SmokingCondition);
                var tr = body.AddTr();
                tr.AddTd().SetText("Smoking Status");
                if (snomedModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for smokingStatus for {0} (SmokingStatus Id {1}).".FormatWith(patientSmokingStatus.SmokingCondition.Name, patientSmokingStatus.Id));
                    tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(patientSmokingStatus.SmokingCondition.Name, Constants.SnomedExternalSystemnameForDisplay, "Unknown"));
                }
                else
                {
                    tr.AddTd().SetText("{0}, {1}: {2}".FormatWith(snomedModel.Term, Constants.SnomedExternalSystemnameForDisplay, snomedModel.ConceptId));
                }

                if (patientSmokingStatus.SmokingStartDateTime.HasValue && patientSmokingStatus.SmokingEndDateTime.HasValue)
                {
                    tr.AddTd().SetText("{0}-{1}".FormatWith(CdaExtensions.FormatForCdaTextElement(patientSmokingStatus.SmokingStartDateTime.Value), CdaExtensions.FormatForCdaTextElement(patientSmokingStatus.SmokingEndDateTime.Value)));
                }
                else if (patientSmokingStatus.SmokingStartDateTime.HasValue)
                {
                    tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(patientSmokingStatus.SmokingStartDateTime.Value));
                }
                else
                {
                    tr.AddTd().SetText(CdaExtensions.FormatForCdaTextElement(patientSmokingStatus.EnteredDateTime));
                }
            }
        }

        private void InitializeSocialHistoryComponentEntries(POCD_MT000040Section section, IEnumerable<PatientSmokingStatus> source)
        {
            var patientSmokingStatuses = source.ToArray();
            var smokingStatusMappings = _cdaModelMapper.GetSnomedModelMap(patientSmokingStatuses.Select(i => i.SmokingCondition));

            foreach (var patientSmokingStatus in patientSmokingStatuses.OrderByDescending(i => i.EnteredDateTime))
            {
                var observation = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV).AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                observation.AddTemplateId().SetRoot(Oids.SmokingStatusObservation);
                observation.AddId().SetRootAndExtension(patientSmokingStatus);
                observation.AddCode().SetCode("ASSERTION").SetCodeSystem(CodeSystem.ActCode);
                observation.AddStatusCode().SetCode("completed");
                var effectiveTime = observation.AddEffectiveTime();


                var snomedModel = smokingStatusMappings.GetValue(patientSmokingStatus.SmokingCondition);
                if (snomedModel == null)
                {
                    VerboseLog.TraceEvent(TraceEventType.Warning, 0, "Could not find mapped SNOMED entity for smokingStatus for {0} (SmokingStatus Id {1}).".FormatWith(patientSmokingStatus.SmokingCondition.Name, patientSmokingStatus.Id));
                    if (patientSmokingStatus.SmokingStartDateTime.HasValue)
                    {
                        effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(patientSmokingStatus.SmokingStartDateTime.Value));
                    }
                    else
                    {
                        effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                    }
                    if (patientSmokingStatus.SmokingEndDateTime.HasValue)
                    {
                        effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(patientSmokingStatus.SmokingEndDateTime.Value));
                    }
                    else
                    {
                        effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
                    }
                    observation.AddValue<CD>().SetNullFlavor(CdaNullFlavor.UNK).SetDisplayName(patientSmokingStatus.SmokingCondition.Name).SetCodeSystem(CodeSystem.SnomedCt);
                }
                else
                {
                    if (snomedModel.ConceptId == "266927001" || snomedModel.ConceptId == "266919005")
                    {
                        effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(patientSmokingStatus.EnteredDateTime));
                    }
                    else
                    {
                        if (patientSmokingStatus.SmokingStartDateTime.HasValue)
                        {
                            effectiveTime.AddLow().SetValue(CdaExtensions.FormatForCdaAlternate(patientSmokingStatus.SmokingStartDateTime.Value));
                        }
                        else
                        {
                            effectiveTime.AddLow().SetNullFlavor(CdaNullFlavor.NI);
                        }
                    }

                    if (snomedModel.ConceptId == "8517006")
                    {
                        if (patientSmokingStatus.SmokingEndDateTime.HasValue)
                        {
                            effectiveTime.AddHigh().SetValue(CdaExtensions.FormatForCdaAlternate(patientSmokingStatus.SmokingEndDateTime.Value));
                        }
                        else
                        {
                            effectiveTime.AddHigh().SetNullFlavor(CdaNullFlavor.NI);
                        }
                    }

                    observation.AddValue<CD>().SetCode(snomedModel.ConceptId).SetDisplayName(snomedModel.Term).SetCodeSystem(CodeSystem.SnomedCt);
                }
            }
        }

        #endregion

        #region Vital Signs

        public POCD_MT000040Component3 AddNewVitalSignsComponent(IEnumerable<Encounter> source)
        {
            var component = new POCD_MT000040Component3();

            #region static values

            var section = component.AddSection();
            section.AddTemplateId().SetRoot(Oids.VitalSignsSectionEntriesRequired);
            section.AddCode().SetCode("8716-3").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("VITAL SIGNS");
            section.AddTitle("Vital Signs");
            var table = section.AddText().AddTable().SetBorder("1").SetWidth("100%");
            var tr = table.AddThead().AddTr();
            tr.AddTh("Date / Time:").SetAlign(StrucDocThAlign.right);

            var body = table.AddTbody();

            #endregion

            Func<Encounter, bool> hasVitalSigns = encounter => encounter.PatientBloodPressures.Any(bp => bp.DiastolicPressure.HasValue || bp.SystolicPressure.HasValue)
                                                               || encounter.PatientHeightAndWeights.Any(i => i.HeightUnit.HasValue || i.WeightUnit.HasValue);
            var encounters = source.Where(hasVitalSigns).OrderByDescending(i => i.StartDateTime);

            foreach (var encounter in encounters)
            {
                tr.AddTh(CdaExtensions.FormatForCdaTextElement(encounter.StartDateTime));
            }

            InitializeVitalSignsComponentTableRows(body, encounters);
            InitializeVitalSignsComponentEntries(section, encounters);

            return component;
        }

        public static void InitializeVitalSignsComponentEntries(POCD_MT000040Section section, IEnumerable<Encounter> source)
        {
            foreach (var encounter in source)
            {
                var organizer = section.AddEntry().SetTypeCode(x_ActRelationshipEntry.DRIV)
                                       .AddOrganizer().SetClassCode(x_ActClassDocumentEntryOrganizer.CLUSTER).SetMoodCode("EVN");
                organizer.AddTemplateId().SetRoot(Oids.VitalSignsOrganizer);
                organizer.AddId().SetRootAndExtension(encounter);
                organizer.AddCode<CD>().SetCode("46680005").SetCodeSystem(CodeSystem.SnomedCt).SetDisplayName("Vital Signs");
                organizer.AddStatusCode().SetCode("completed");
                organizer.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));

                var patientHeight = encounter.PatientHeightAndWeights.Where(i => i.HeightUnit.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var patientWeight = encounter.PatientHeightAndWeights.Where(i => i.WeightUnit.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var diastolicPressure = encounter.PatientBloodPressures.Where(i => i.DiastolicPressure.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var systolicPressure = encounter.PatientBloodPressures.Where(i => i.SystolicPressure.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();

                if (patientHeight != null)
                {
                    var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.VitalSignsObservation);
                    // Height = A
                    observation.AddId().SetRootAndExtension(patientHeight, "A");
                    observation.AddCode<CD>().SetCode("8302-2").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Height");
                    observation.AddText().AddReference().SetValue("#date_{0}_height".FormatWith(section.entry.Length));
                    observation.AddStatusCode().SetCode("completed");
                    observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));
                    var heightInMeters = patientHeight.HeightUnit * 0.001m;
                    observation.AddValue<PQ>().SetValue(heightInMeters.ToString()).SetUnit("m");
                }

                if (patientWeight != null)
                {
                    var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.VitalSignsObservation);
                    // Weight = B
                    observation.AddId().SetRootAndExtension(patientWeight, "B");
                    observation.AddCode<CD>().SetCode("3141-9").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("Patient Body Weight - Measured");
                    observation.AddText().AddReference().SetValue("#date_{0}_weight".FormatWith(section.entry.Length));
                    observation.AddStatusCode().SetCode("completed");
                    observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));
                    var weightInKg = patientWeight.WeightUnit * 0.001m;
                    observation.AddValue<PQ>().SetValue(weightInKg.ToString()).SetUnit("kg");
                }

                if (patientWeight != null && patientHeight != null)
                {
                    var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.VitalSignsObservation);
                    // Weight = C
                    observation.AddId().SetRootAndExtension(patientWeight, "C");
                    observation.AddCode<CD>().SetCode("39156-5").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("BMI (Body Mass Index)");
                    observation.AddText().AddReference().SetValue("#date_{0}_bmi".FormatWith(section.entry.Length));
                    observation.AddStatusCode().SetCode("completed");
                    observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));
                    var weightInKg = patientWeight.WeightUnit * 0.001m;
                    var heightInMeters = patientHeight.HeightUnit * 0.001m;
                    var bmi = Math.Round((double)(weightInKg / (heightInMeters * heightInMeters)), 2);
                    observation.AddValue<PQ>().SetValue(bmi.ToString());
                }

                if (systolicPressure != null)
                {
                    var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.VitalSignsObservation);
                    // Systolic Pressure = D
                    observation.AddId().SetRootAndExtension(systolicPressure, "D");
                    observation.AddCode<CD>().SetCode("8480-6").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("BP Systolic");
                    observation.AddText().AddReference().SetValue("#date_{0}_bp".FormatWith(section.entry.Length));
                    observation.AddStatusCode().SetCode("completed");
                    observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));
                    observation.AddValue<PQ>().SetValue(systolicPressure.SystolicPressure.ToString()).SetUnit("mmHg");
                }

                if (diastolicPressure != null)
                {
                    var observation = organizer.AddComponent().AddObservation().SetClassCode("OBS").SetMoodCode(x_ActMoodDocumentObservation.EVN);
                    observation.AddTemplateId().SetRoot(Oids.VitalSignsObservation);
                    // Diastolic Pressure = E
                    observation.AddId().SetRootAndExtension(diastolicPressure, "E");
                    observation.AddCode<CD>().SetCode("8462-4").SetCodeSystem(CodeSystem.Loinc).SetDisplayName("BP Diastolic");
                    observation.AddText().AddReference().SetValue("#date_{0}_bp".FormatWith(section.entry.Length));
                    observation.AddStatusCode().SetCode("completed");
                    observation.AddEffectiveTime().SetValue(CdaExtensions.FormatForCdaAlternate(encounter.StartDateTime));
                    observation.AddValue<PQ>().SetValue(diastolicPressure.DiastolicPressure.ToString()).SetUnit("mmHg");
                }
            }
        }

        private static void InitializeVitalSignsComponentTableRows(StrucDocTbody body, IEnumerable<Encounter> source)
        {
            StrucDocTr
                heightTr = body.AddTr().AddTh("Height", th => th.SetAlign(StrucDocThAlign.left)),
                weightTr = body.AddTr().AddTh("Weight", th => th.SetAlign(StrucDocThAlign.left)),
                bmiTr = body.AddTr().AddTh("BMI", th => th.SetAlign(StrucDocThAlign.left)),
                bloodPressureTr = body.AddTr().AddTh("Blood Pressure", th => th.SetAlign(StrucDocThAlign.left));

            foreach (var encounter in source)
            {
                var patientHeight = encounter.PatientHeightAndWeights.Where(i => i.HeightUnit.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var patientWeight = encounter.PatientHeightAndWeights.Where(i => i.WeightUnit.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var diastolicPressure = encounter.PatientBloodPressures.Where(i => i.DiastolicPressure.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();
                var systolicPressure = encounter.PatientBloodPressures.Where(i => i.SystolicPressure.HasValue).OrderByDescending(i => i.EnteredDateTime).FirstOrDefault();

                var heightContent = heightTr.AddTd().AddContent();
                if (patientHeight != null)
                {
                    var heightInMeters = patientHeight.HeightUnit * 0.001m;
                    heightContent.SetId("date_{0}_height".FormatWith(heightTr.Items.OfType<StrucDocTd>().Count())).SetText("{0} m".FormatWith(heightInMeters.ToString()));
                }

                var weightContent = weightTr.AddTd().AddContent();
                if (patientWeight != null)
                {
                    var weightInKg = patientWeight.WeightUnit * 0.001m;
                    weightContent.SetId("date_{0}_weight".FormatWith(weightTr.Items.OfType<StrucDocTd>().Count())).SetText("{0} kg".FormatWith((weightInKg).ToString()));
                }

                var bmiContent = bmiTr.AddTd().AddContent();
                if (patientWeight != null && patientHeight != null)
                {
                    var weightInKg = patientWeight.WeightUnit * 0.001m;
                    var heightInMeters = patientHeight.HeightUnit * 0.001m;
                    var bmi = Math.Round((double)(weightInKg / (heightInMeters * heightInMeters)), 2);
                    bmiContent.SetId("date_{0}_bmi".FormatWith(bmiTr.Items.OfType<StrucDocTd>().Count())).SetText(bmi.ToString());
                }

                var bloodPressureContent = bloodPressureTr.AddTd().AddContent();
                if (diastolicPressure != null || systolicPressure != null)
                {
                    var diastolicValue = diastolicPressure != null ? diastolicPressure.DiastolicPressure.ToString() : "--";
                    var systolicValue = systolicPressure != null ? systolicPressure.SystolicPressure.ToString() : "--";
                    bloodPressureContent.SetId("date_{0}_bp".FormatWith(bloodPressureTr.Items.OfType<StrucDocTd>().Count())).SetText("{0}/{1} mmHg".FormatWith(systolicValue, diastolicValue));
                }
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Checks the source for null or empty, and if so, adds a table row with <see cref="numberOfColumns"/> empty columns.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="source"></param>
        /// <param name="numberOfColumns"></param>
        private static void InitializeEmptyComponentTableRow(StrucDocTbody body, IEnumerable source, int numberOfColumns)
        {
            if (source.IsNotNullOrEmpty()) return;

            var tr = body.AddTr();
            tr.AddTd().AddContent();

            for (int i = 0; i < numberOfColumns; i++)
            {
                tr.AddTd();
            }
        }


        /// <summary>
        /// Gets the result date that is calculated according to Michelle's spec.
        /// These specs can be found in TFS under the names, TocSpec.xml and VisitSummSpec.xml.
        /// </summary>
        /// <param name="labTestResult"></param>
        /// <returns></returns>
        public static DateTime? GetResultDate(PatientLaboratoryTestResult labTestResult)
        {
            return labTestResult.ObservationDateTime ?? labTestResult.AnalysisDateTime ?? labTestResult.SpecimenCollectedDateTime ?? labTestResult.ReportDateTime;
        }

        /// <summary>
        /// Determines whether a procedure is excluded from bring traced.
        /// </summary>
        /// <param name="cp">The cp.</param>
        /// <returns></returns>
        public static bool IsProcedureNotExcluded(ClinicalProcedure cp)
        {
            return ExcludedTraceProcedureNames.All(c => c != cp.Name.ToLower());
        }

        #endregion
    }
    //public class NullFormat : IFormatProvider, ICustomFormatter
    //{
    //    public object GetFormat(Type service)
    //    {
    //        return service == typeof(ICustomFormatter) ? this : null;
    //    }

    //    public string Format(string format, object arg, IFormatProvider provider)
    //    {
    //        if (arg == null)
    //        {
    //            return "NULL";
    //        }
    //        var formattable = arg as IFormattable;
    //        return formattable != null ? formattable.ToString(format, provider) : arg.ToString();
    //    }
    //}
}
