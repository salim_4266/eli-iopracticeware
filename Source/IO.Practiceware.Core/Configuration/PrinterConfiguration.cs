﻿using Soaf;
using System;

namespace IO.Practiceware.Configuration
{
    public class PrinterConfiguration
    {
        public const string OverridePrinterConfigurationKey = "OverridePrinterConfiguration";
        public const string DefaultPrinterConfigurationKey = "DefaultPrinterConfiguration";

        /// <summary>
        ///     If this is true, user will always be prompted for printer, regardless of document printer settings.
        /// </summary>
        /// <value>
        ///     The override printer configuration.
        /// </value>
        public static Boolean OverridePrinterConfiguration
        {
            get { return ConfigurationManager.CustomAppSettings[OverridePrinterConfigurationKey].ToBoolean() ?? false; }

            set
            {
                ConfigurationManager.CustomAppSettings[OverridePrinterConfigurationKey] = value.ToString();
                ConfigurationManager.Save();
            }
        }

        /// <summary>
        /// If this is true, we will always print to the computer's default printer, ignoring any document printer settings.     
        /// </summary>
        /// <value>
        /// The default printer configuration.
        /// </value>
        public static Boolean DefaultPrinterConfiguration
        {
            get { return ConfigurationManager.CustomAppSettings[DefaultPrinterConfigurationKey].ToBoolean() ?? false; }
            set
            {
                ConfigurationManager.CustomAppSettings[DefaultPrinterConfigurationKey] = value.ToString();
                ConfigurationManager.Save();
            }
        }
    }
}