using System.Threading.Tasks;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Logging;
using IO.Practiceware.Services.FileService;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Presentation;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Threading;
using Soaf.Web.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Xml.Serialization;
using ConnectionStringSettings = System.Configuration.ConnectionStringSettings;
using Enumerable = System.Linq.Enumerable;
using KeyValueConfigurationCollection = Soaf.Configuration.KeyValueConfigurationCollection;

[assembly: Component(typeof(IOEndpointConfigurationProvider), typeof(IEndpointConfigurationProvider), Priority = 1)]
[assembly: Component(typeof(IOConfigurationContainer), typeof(IConfigurationContainer), Priority = 1)]

namespace IO.Practiceware.Configuration
{
    public static class ConfigurationManager
    {
        public const string SystemConfigurationFileName = "IO.Practiceware.System.config";
        public const string CustomConfigurationFileName = "IO.Practiceware.Custom.config";

        public const string DefaultPracticeRepositoryConnectionStringKey = "PracticeRepository";

        public const string DisableVerifyClientServerAssemblyVersionsKey = "DisableVerifyClientServerAssemblyVersions";

        public const string DefaultConnectionStringFormat = "Data Source=;Initial Catalog=;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200";

        internal static bool IsInitialized { get; private set; }

        public static bool IsLoaded { get; private set; }

        public static System.Configuration.Configuration Configuration { get; private set; }

        private static readonly Lazy<string> ResolvedApplicationPath;
        private static readonly Lazy<string> ResolvedApplicationDataPath;
        private static readonly RefreshableCachedValue<string> ResolvedServerDataPaths = new RefreshableCachedValue<string>(CacheHoldInterval.XLong);
        private static readonly RefreshableCachedValue<object> CachedConfigurationSections = new RefreshableCachedValue<object>(CacheHoldInterval.Long);

        private static readonly FileSystemWatcher ConfigurationFileWatcher = new FileSystemWatcher();
        private static readonly object SyncRoot = new object();
        private static CancellationTokenSource _waitToken;

        static ConfigurationManager()
        {
            // Application path and data path do not change during execution
            ResolvedApplicationPath = new Lazy<string>(() =>
            {
                var entryAssembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();

                // Verify whether assembly loaded from GAC. This happens only while debugging VB6
                if (entryAssembly.GlobalAssemblyCache)
                {
                    // Try locate in user's profile
                    var guessedPath = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"IO Practiceware");

                    if (!Directory.Exists(guessedPath))
                    {
                        throw new ApplicationException("Application not found in expected location. Assembly is running from GAC");
                    }

                    return guessedPath;
                }

                var applicationPath = entryAssembly.Location;
                applicationPath = Path.GetDirectoryName(applicationPath);
                return applicationPath;

            });

            ResolvedApplicationDataPath = new Lazy<string>(() =>
            {
                var applicationDataPath = Path.Combine(ApplicationPath, "Pinpoint");
                FileSystem.EnsureDirectory(applicationDataPath);
                return applicationDataPath;
            });

            ConfigurationFileWatcher.NotifyFilter = NotifyFilters.Attributes
                | NotifyFilters.LastWrite | NotifyFilters.Size
                | NotifyFilters.DirectoryName | NotifyFilters.FileName;
            ConfigurationFileWatcher.Changed += OnConfigurationFileWatcherChanged;

            ConfigurationChanged += RestartIfHosted;
        }

        /// <summary>
        /// Restarts the environment if running in a worker process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void RestartIfHosted(object sender, EventArgs e)
        {
            if (HostingEnvironment.IsHosted)
            {
                HostingEnvironment.InitiateShutdown();
            }
        }

        private static void OnConfigurationFileWatcherChanged(object sender, FileSystemEventArgs e)
        {
            lock (SyncRoot)
            {
                // Get modified file names
                var changedFileName = Path.GetFileName(e.FullPath) ?? string.Empty;
                var configurationFileNames = new[]
                {
                    SystemConfigurationFileName,
                    CustomConfigurationFileName,
                    Path.GetFileName(Configuration.FilePath)
                };

                // Is change for one of configured configuration files?
                // Note: Starts with is used, since in web environment file names are suffixed with temp string. Like: "IO.Practiceware.Custom.config~RFf9f89b6.TMP"
                if (Configuration == null || (!configurationFileNames.Any(f => changedFileName.StartsWith(f, StringComparison.OrdinalIgnoreCase))))
                {
                    return;
                }

                // Delay configuration reload, since FileWatcher can notify multiple times of same update
                if (_waitToken != null && !_waitToken.IsCancellationRequested)
                {
                    _waitToken.Cancel();
                }
                _waitToken = new CancellationTokenSource();
                var cancellationToken = _waitToken;
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(2)); // Wait 2 seconds
                    if (cancellationToken.IsCancellationRequested)
                    {
                        return;
                    }

                    // Note that this should ONLY happen when changed by an external program (not when saving internally, since 
                    // we should suppress this behavior for internal saves by setting _isSaving to true). Since file locks may not be released
                    // immediately, we will wait for 500ms before attempting a reload. This is hacky code, but file locks by external programs
                    // are unpredictable.
                    RetryUtility.ExecuteWithRetry(() => Load(Path.GetDirectoryName(Configuration.FilePath)), 5, TimeSpan.FromMilliseconds(500),
                        // only retry for file access exceptions
                        ex => ex.SearchFor<IOException>().IfNotNull(ioEx => ioEx.Message.StartsWith("The process cannot access the file")));

                    ConfigurationChanged.Fire(Configuration);
                }, cancellationToken.Token);
            }
        }

        /// <summary>
        /// Occurs when configuration is changed and needs to be reloaded.
        /// </summary>
        public static event EventHandler<EventArgs> ConfigurationChanged;

        /// <summary>
        /// Set the transaction timeout for the current session.
        /// </summary>
        /// <param name="timeout">time out</param>
        public static void OverrideTransactionScopeMaximumTimeout(TimeSpan timeout)
        {
            try
            {
                Type transType = typeof(System.Transactions.TransactionManager);
                FieldInfo cachedMaxTimeOut = transType.GetField("_cachedMaxTimeout", BindingFlags.NonPublic | BindingFlags.Static);
                FieldInfo maximumTimeOut = transType.GetField("_maximumTimeout", BindingFlags.NonPublic | BindingFlags.Static);

                if (null != cachedMaxTimeOut)
                {
                    cachedMaxTimeOut.SetValue(null, true);
                }
                if (null != maximumTimeOut)
                {
                    maximumTimeOut.SetValue(null, timeout);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        /// <summary>
        /// Gets the section with retries (in case configuration got updated and haven't been reloaded yet).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static T GetSectionWithRetry<T>(string name = null) where T : class
        {
            var result = CachedConfigurationSections.GetOrRefreshValue(() =>
            {
                var section = RetryUtility.ExecuteWithRetry(() =>
                {
                    // Lock between retries, so that configuration has a chance to be reloaded
                    lock (SyncRoot)
                    {
                        return Configuration == null ? null : Configuration.GetSection<T>(name);
                    }
                }, 5, TimeSpan.FromMilliseconds(1500));
                return section;
            }, name ?? typeof(T).FullName);
            return (T)result;
        }

        /// <summary>
        /// Sets the section with retries (in case configuration got updated and haven't been reloaded yet.
        /// </summary>
        /// <typeparam name="T">Configuration type</typeparam>
        /// <param name="section"></param>
        /// <param name="sectionName">The name.</param>
        /// <returns></returns>
        public static T SetSectionWithRetry<T>(T section = null, string sectionName = null) where T : class
        {
            var result = RetryUtility.ExecuteWithRetry(() =>
            {
                // Lock between retries, so that configuration has a chance to be reloaded
                lock (SyncRoot)
                {
                    return Configuration == null ? null : Configuration.SetSection(section, sectionName);
                }
            }, 5, TimeSpan.FromMilliseconds(1500));

            CachedConfigurationSections.Reset();
            ResolvedServerDataPaths.Reset();

            return result;
        }

        public static KeyValueConfigurationCollection CustomAppSettings
        {
            get { return GetSectionWithRetry<KeyValueConfigurationCollection>("customAppSettings"); }
        }

        public static KeyValueConfigurationCollection SystemAppSettings
        {
            get { return GetSectionWithRetry<KeyValueConfigurationCollection>("systemAppSettings"); }
        }

        public static ConnectionStringConfigurationCollection ConnectionStrings
        {
            get { return GetSectionWithRetry<ConnectionStringConfigurationCollection>("customConnectionStrings"); }
        }

        /// <summary>
        /// Returns full path to server data location
        /// </summary>
        public static string ServerDataPath
        {
            get
            {
                var result = ResolvedServerDataPaths.GetOrRefreshValue(() =>
                {
                    string configuredServerDataPath;

                    // In application server mode -> read from clients list
                    var appServersConfig = ApplicationServerConfiguration;
                    if (appServersConfig != null && !appServersConfig.Clients.IsNullOrEmpty())
                    {
                        // Read configuration either of connected client or default one
                        configuredServerDataPath = (ClientConfiguration ?? DefaultClientConfiguration)
                            .IfNotNull(c => c.ServerDataPath);
                    }
                    else
                    {
                        // In non-server mode -> read server data path from app settings
                        configuredServerDataPath = CustomAppSettings["ServerDataPath"];
                    }

                    // ServerDataPath must be set by now
                    if (string.IsNullOrEmpty(configuredServerDataPath))
                    {
                        throw new InvalidOperationException("ServerDataPath setting not found.");
                    }

                    // Resolve to full path
                    var path = configuredServerDataPath;
                    path = Environment.ExpandEnvironmentVariables(path); // Expand variables
                    path = Path.GetFullPath(path); // Resolve short paths

                    return path;
                }, ApplicationContext.Current.ClientKey);

                return result;
            }
            set
            {
                if (ClientConfiguration != null)
                {
                    ClientConfiguration.ServerDataPath = value;
                }
                else
                {
                    CustomAppSettings["ServerDataPath"] = value;
                }
                ResolvedServerDataPaths.Reset();
            }
        }

        /// <summary>
        /// Path to where main application executables are located
        /// </summary>
        /// <remarks>
        ///  Doesn't change while application is running
        /// </remarks>
        public static string ApplicationPath
        {
            get { return ResolvedApplicationPath.Value; }
        }

        /// <summary>
        /// Path to main application data directory.
        /// </summary>
        /// <remarks>
        ///  Doesn't change while application is running
        /// </remarks>
        public static string ApplicationDataPath
        {
            get { return ResolvedApplicationDataPath.Value; }
        }

        /// <summary>
        /// The ApplicationServerConfiguration section.
        /// </summary>
        public static ApplicationServerConfiguration ApplicationServerConfiguration
        {
            get { return GetSectionWithRetry<ApplicationServerConfiguration>(); }
        }

        /// <summary>
        /// Returns configuration of the currently connected client. Applicable only for application server.
        /// </summary>
        public static ClientConfiguration ClientConfiguration
        {
            get
            {
                var clientConfiguration = ApplicationServerConfiguration.IfNotNull(asc => asc.Clients.FirstOrDefault(c => c.AuthenticationToken == AuthenticationToken));
                return clientConfiguration;
            }
        }

        /// <summary>
        /// Returns configuration of a default client (the one which uses default PracticeRepository connection string). Applicable only for application server.
        /// </summary>
        public static ClientConfiguration DefaultClientConfiguration
        {
            get
            {
                var clientConfiguration = ApplicationServerConfiguration
                    .IfNotNull(asc => asc.Clients
                        .FirstOrDefault(ac => ac.ConnectionStringName == DefaultPracticeRepositoryConnectionStringKey));

                return clientConfiguration;
            }
        }

        public static string PracticeRepositoryConnectionStringKey
        {
            get
            {
                var clientConfiguration = ClientConfiguration;
                if (clientConfiguration != null)
                {
                    return clientConfiguration.ConnectionStringName;
                }
                return DefaultPracticeRepositoryConnectionStringKey;
            }
        }

        public static string PracticeRepositoryConnectionString
        {
            get
            {
                return ConnectionStrings[PracticeRepositoryConnectionStringKey].IfNotNull(cs => cs.ConnectionString);
            }
            set
            {
                PreviousPracticeRepositoryConnectionString = ConnectionStrings[PracticeRepositoryConnectionStringKey].IfNotNull(cs => cs.ConnectionString);
                ConnectionStrings[PracticeRepositoryConnectionStringKey].IfNotNull(cs => cs.ConnectionString = value);
                UpdateSystemConfigurationManagerConnectionStrings();
            }
        }

        /// <summary>
        /// Returns the value of the practice repository connection string when it was last set.
        /// </summary>
        public static string PreviousPracticeRepositoryConnectionString { get; private set; }

        /// <summary>
        /// Updates the system configuration manager connection strings with the ones from the IO configuration manager. 
        /// Required to support in built functionality around ConfigurationManager.ConnectionStrings.
        /// </summary>
        private static void UpdateSystemConfigurationManagerConnectionStrings()
        {
            Action<ConnectionStringSettingsCollection> update = connectionStrings =>
                                                          {
                                                              connectionStrings.SetReadOnly(false);

                                                              foreach (var cs in ConnectionStrings.Items)
                                                              {
                                                                  var systemSetting = connectionStrings[cs.Name];
                                                                  if (systemSetting == null)
                                                                  {
                                                                      connectionStrings.Add(new ConnectionStringSettings(cs.Name, cs.ConnectionString, cs.ProviderName));
                                                                  }
                                                                  else
                                                                  {
                                                                      systemSetting.SetReadOnly(false);
                                                                      systemSetting.ConnectionString = cs.ConnectionString;
                                                                      systemSetting.ProviderName = cs.ProviderName;
                                                                  }
                                                              }
                                                          };

            update(System.Configuration.ConfigurationManager.ConnectionStrings);
            System.Configuration.ConfigurationManager.GetSection("connectionStrings").CastTo<ConfigurationSection>().ResetModified();

            update(Configuration.ConnectionStrings.ConnectionStrings);
            Configuration.GetSection("connectionStrings").ResetModified();
        }

        public static void Save()
        {
            lock (SyncRoot)
            {
                ConfigurationFileWatcher.EnableRaisingEvents = false;
                try
                {
                    Configuration.Save(ConfigurationSaveMode.Modified, false);

                    Load();
                }
                finally
                {
                    ConfigurationFileWatcher.EnableRaisingEvents = true;
                }
            }
        }

        /// <summary>
        /// Client application configuration
        /// </summary>
        public static ClientApplicationConfiguration ClientApplicationConfiguration
        {
            get
            {
                return GetSectionWithRetry<ClientApplicationConfiguration>()
                    ?? SetSectionWithRetry(new ClientApplicationConfiguration());
            }
        }

        /// <summary>
        /// Client side configuration for an application server to connect to.
        /// </summary>
        public static ApplicationServerClientConfiguration ApplicationServerClientConfiguration
        {
            get
            {
                return GetSectionWithRetry<ApplicationServerClientConfiguration>()
                    ?? SetSectionWithRetry(new ApplicationServerClientConfiguration());
            }
        }

        /// <summary>
        /// Gets the authentication token for current client
        /// </summary>
        public static string AuthenticationToken
        {
            get
            {
                return GetAuthenticationTokenFromIdentity()
                    ?? GetAuthenticationTokenFromContext()
                    ?? (ApplicationServerClientConfiguration.IsEnabled ? ApplicationServerClientConfiguration.AuthenticationToken : null);
            }
            set
            {
                ApplicationServerClientConfiguration.AuthenticationToken = value;
            }
        }

        /// <summary>
        /// Gets authentication token which is present in context
        /// </summary>
        /// <returns></returns>
        private static string GetAuthenticationTokenFromContext()
        {
            var token = ContextData.Get("__Authentication_Token__") as string;
            return token;
        }

        /// <summary>
        /// Gets the authentication token from the current principal identity. Token is text following last @ sign.
        /// </summary>
        /// <returns></returns>
        private static string GetAuthenticationTokenFromIdentity()
        {
            if (ServiceProvider.IsInitialized)
            {
                var principal = PrincipalContext.Current.Principal;

                if (principal != null
                    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                    && principal.Identity != null
                    && principal.Identity.Name != null
                    && principal.Identity.Name.Contains("@")
                    && !principal.Identity.Name.EndsWith("@"))
                {
                    return principal.Identity.Name.Split('@').Skip(1).Join("@");
                }
            }
            return null;
        }

        /// <summary>
        /// Flag indicating whether or not to check the server assemblies (when in cloud mode) against the client assemblies
        /// </summary>
        public static Boolean DisableVerifyClientServerAssemblyVersions
        {
            get { return CustomAppSettings[DisableVerifyClientServerAssemblyVersionsKey].ToBoolean() ?? false; }
            set
            {
                CustomAppSettings[DisableVerifyClientServerAssemblyVersionsKey] = value.ToString();

            }
        }

        /// <summary>
        /// Gets the version of the current running assembly with which this class resides
        /// </summary>
        public static Version AssemblyVersion { get { return GetAssemblyVersion.Value; } }

        private static readonly Lazy<Version> GetAssemblyVersion = new Lazy<Version>(() =>
                                                                              {
                                                                                  var coreAssembly = Assembly.GetAssembly(typeof(ConfigurationManager));
                                                                                  var coreAssemblyFullName = new AssemblyName(coreAssembly.FullName);
                                                                                  return coreAssemblyFullName.Version;
                                                                              });

        /// <summary>
        ///   Loads the configuration file(s) from specified path. Will search if file is not specified or doesn't exist.
        /// </summary>
        /// <param name="path"> The path. </param>
        /// <returns> </returns>
        public static void Load(string path = null)
        {
            lock (SyncRoot)
            {
                if (HostingEnvironment.IsHosted)
                {
                    Configuration = WebConfigurationManager.OpenWebConfiguration("~");
                }
                else
                {
                    // Use current configuration path if it is set and path is not provided
                    if (path == null && Configuration != null)
                    {
                        path = Path.GetDirectoryName(Configuration.FilePath);
                    }

                    // Locate configuration
                    var systemConfigurationFilePath = LocateSystemConfigurationFilePath(path);

                    // Still not found? Write default configuration to disk
                    if (string.IsNullOrEmpty(systemConfigurationFilePath))
                    {
                        // extract default system.config file
                        systemConfigurationFilePath = Path.Combine(ApplicationPath, SystemConfigurationFileName);
                        FileSystem.TryWriteFile(systemConfigurationFilePath, ConfigurationResources.IO_Practiceware_System);
                    }

                    // if for some reason system.config is present but custom.config isn't present in same directory, extract default custom.config
                    var customConfigurationFilePath = Path.Combine(Path.GetDirectoryName(systemConfigurationFilePath) ?? string.Empty, CustomConfigurationFileName);
                    if (!File.Exists(customConfigurationFilePath) && File.Exists(systemConfigurationFilePath))
                    {
                        FileSystem.TryWriteFile(customConfigurationFilePath, ConfigurationResources.IO_Practiceware_Custom);
                    }

                    // Read configuration
                    Configuration = OpenConfigurationFile(systemConfigurationFilePath);

                    // set default .NET ConfigurationManager to use this config
                    AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", Configuration.FilePath);

                    // Force .NET to use configuration at newly specified location
                    ResetNetConfigurationMechanism();

                    AdoServiceProviderFactory.Register();

                    // We must only configure client endpoints for the client, not when running on the web server.
                    // Even if ApplicationServerClient.IsEnabled == false on the web server,
                    // loading the endpoints will cause Soaf to try to host the WebAuthenticationService twice.
                    // TODO: Handle this situation more elegantly in IO or in Soaf.
                    IOEndpointConfigurationProvider.Load();
                }

                ResolvedServerDataPaths.Reset();
                CachedConfigurationSections.Reset();

                // Watch over config changes
                ConfigurationFileWatcher.Path = Path.GetDirectoryName(Configuration.FilePath);
                ConfigurationFileWatcher.EnableRaisingEvents = true;

                SetOdbcConfiguration();

                UpdateSystemConfigurationManagerConnectionStrings();

                IsLoaded = true;
            }
        }

        /// <summary>
        /// Locates path to System.config
        /// </summary>
        /// <param name="configurationPath">The configuration path which can represent either path to folder which contains configuration or actual file. Can be null</param>
        /// <returns></returns>
        private static string LocateSystemConfigurationFilePath(string configurationPath)
        {
            var configurationPathToVerify = configurationPath;

            // -- First, try to use passed parameter and use it to find configuration

            // If we have been provided with direct path to file -> use it
            if (!string.IsNullOrEmpty(configurationPathToVerify) && File.Exists(configurationPathToVerify))
            {
                return Path.GetFullPath(configurationPathToVerify);
            }

            // If we have been provided with folder path -> search in it and a couple of directories up
            if (!string.IsNullOrEmpty(configurationPathToVerify) && Directory.Exists(configurationPathToVerify))
            {
                configurationPathToVerify = FileSystem.SearchFileInFolderAndUp(configurationPathToVerify, SystemConfigurationFileName, 3);
                if (!string.IsNullOrEmpty(configurationPathToVerify))
                {
                    return configurationPathToVerify;
                }
            }

            // Search for configuration file in current folder and up
            configurationPathToVerify = FileSystem.SearchFileInFolderAndUp(ApplicationPath, SystemConfigurationFileName, 3);
            return configurationPathToVerify;
        }

        /// <summary>
        /// Sets up the ODBC configuration.
        /// </summary>
        [DebuggerNonUserCode]
        private static void SetOdbcConfiguration()
        {
            try
            {

                var builder = DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(PracticeRepositoryConnectionString);

                if (builder == null || builder.DataSource.IsNullOrEmpty() || builder.InitialCatalog.IsNullOrEmpty()) return;

                SetOdbcDsn("PracticeDB", builder.DataSource, builder.InitialCatalog);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
                // expected to fail when UAC/not permissioned to edit registry
            }
        }

        [DllImport("ODBCCP32.DLL", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern long SQLConfigDataSource(int hwndParent, int fRequest, string byVallpszDriver, string lpszAttributes);

        /// <summary>
        /// Sets the ODBC DSN in the registry.
        /// </summary>
        /// <param name="dsnName">Name of the DSN.</param>
        /// <param name="serverName">Name of the server.</param>
        /// <param name="databaseName">Name of the database.</param>
        public static void SetOdbcDsn(string dsnName, string serverName, string databaseName)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("DSN=" + dsnName);
            stringBuilder.Append(char.MinValue);
            stringBuilder.Append("Server=" + serverName);
            stringBuilder.Append(char.MinValue);
            stringBuilder.Append("Database=" + databaseName);
            stringBuilder.Append(char.MinValue);
            stringBuilder.Append("Trusted_Connection=Yes");
            stringBuilder.Append(char.MinValue);
            SQLConfigDataSource(0, 4, "SQL Server", stringBuilder.ToString());
        }

        /// <summary>
        ///   Forces System.Configuration.ConfigurationManager to reload. Pretty bad, but .NET doesn't provide a built in way to do this.
        /// </summary>
        private static void ResetNetConfigurationMechanism()
        {
            // See System.Configuration.ConfigurationManager.EnsureConfigurationSystem()
            var sInitLock = typeof(System.Configuration.ConfigurationManager)
                .GetField("s_initLock", BindingFlags.NonPublic |
                                         BindingFlags.Static)
                .EnsureNotDefault("Could not reset configuration mechanism.")
                .GetValue(null);

            lock (sInitLock)
            {
                // Reset ConfigurationManager initialized state
                typeof(System.Configuration.ConfigurationManager)
                    .GetField("s_initState", BindingFlags.NonPublic |
                                             BindingFlags.Static)
                    .EnsureNotDefault("Could not reset configuration mechanism.")
                    .SetValue(null, 0);

                typeof(System.Configuration.ConfigurationManager)
                    .GetField("s_configSystem", BindingFlags.NonPublic |
                                                BindingFlags.Static)
                    .EnsureNotDefault("Could not reset configuration mechanism.")
                    .SetValue(null, null);

                // Reset ClientConfigPaths state
                typeof(System.Configuration.ConfigurationManager)
                    .Assembly.GetTypes().First(x => x.FullName ==
                                                    "System.Configuration.ClientConfigPaths")
                    .GetMethod("RefreshCurrent", BindingFlags.NonPublic |
                                           BindingFlags.Static)
                    .EnsureNotDefault("Could not reset configuration mechanism.")
                    .GetInvoker().Invoke(null, new object[0]);

                // Reset system.diagnostics listeners
                typeof(TraceListener).Assembly.GetType("System.Diagnostics.DiagnosticsConfiguration")
                    .EnsureNotDefault("Could not resolve type DiagnosticsConfiguration.")
                    .GetField("initState", BindingFlags.Static | BindingFlags.NonPublic)
                    .EnsureNotDefault("Could not reset configuration mechanism for system diagnostics.")
                    .SetValue(null, 0);
            }
        }

        /// <summary>
        ///   Opens a configuration from a file. Loads machine config if path is null.
        /// </summary>
        /// <param name="path"> The path. </param>
        /// <returns> </returns>
        private static System.Configuration.Configuration OpenConfigurationFile(string path)
        {
            if (!path.IsNullOrEmpty() && File.Exists(path))
            {
                var fileMap = new ExeConfigurationFileMap { ExeConfigFilename = path };
                return System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            }
            return System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        internal static void Initialize()
        {
            if (IsInitialized) return;

            using (new TimedScope(s => Debug.WriteLine("Initialized ConfigurationManager in {0}.".FormatWith(s))))
            {
                AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
                TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
                LoggingConfigurationManager.Initialize();
            }

            OverrideTransactionScopeMaximumTimeout(TimeSpan.Zero);

            IsInitialized = true;
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError(e.ExceptionObject.ToString());
            if (Environment.UserInteractive && System.Windows.Application.Current != null)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(
                    new Action(() => InteractionManager.Current.DisplayDetailedException(e.ExceptionObject as Exception)));
            }
        }

        private static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            Trace.TraceError(e.Exception.ToString());
            if (Environment.UserInteractive && System.Windows.Application.Current != null)
            {
                e.SetObserved();
                System.Windows.Application.Current.Dispatcher.Invoke(
                    new Action(() => InteractionManager.Current.DisplayDetailedException(e.Exception)));
            }
        }

        /// <summary>
        /// Resets the modified flag on the configuration section using private reflection. Required so as to not save "temporary" changes made to the connection strings, and service model client endpoints 
        /// generated at runtime.
        /// </summary>
        /// <param name="section">The section.</param>
        public static void ResetModified(this ConfigurationSection section)
        {
            // Marks as unmodified so that any saves don't get persisted to the actual file
            typeof(ConfigurationSection).GetMethod("ResetModified", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).EnsureNotDefault().Invoke(section, new object[0]);
        }
    }

    /// <summary>
    /// All clients served by the application server.
    /// </summary>
    [XmlRoot("applicationServer")]
    public class ApplicationServerConfiguration
    {
        private List<ClientConfiguration> _clients;

        public ApplicationServerConfiguration()
        {
            Clients = new List<ClientConfiguration>();
        }

        [XmlArray("clients")]
        [XmlArrayItem("client")]
        public List<ClientConfiguration> Clients
        {
            get { return _clients ?? (_clients = new List<ClientConfiguration>()); }
            set { _clients = value; }
        }
    }

    /// <summary>
    /// A single client served by the application server.
    /// </summary>
    public class ClientConfiguration
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("authenticationToken")]
        public string AuthenticationToken { get; set; }

        [XmlAttribute("connectionStringName")]
        public string ConnectionStringName { get; set; }

        [XmlAttribute("serverDataPath")]
        public string ServerDataPath { get; set; }
    }

    /// <summary>
    /// Application initialization configuration settings.
    /// </summary>
    [XmlRoot(ElementName = "clientApplicationConfiguration")]
    public class ClientApplicationConfiguration
    {
        /// <summary>
        /// Experimental (not for usage in production): Sets that no local files in this session are permanent and can be wiped upon exit
        /// </summary>
        [XmlAttribute(Namespace = "", AttributeName = "treatLocaFilesAsTransient")]
        public bool TreatLocaFilesAsTransient { get; set; }

        /// <summary>
        /// Sets that application client runs on terminal server where all users of a client share the same account
        /// </summary>
        [XmlAttribute(Namespace = "", AttributeName = "rdsWithSharedAccountMode")]
        public bool RdsWithSharedAccountMode { get; set; }

        /// <summary>
        /// Sets that advanced login options should not be available for users
        /// </summary>
        [XmlAttribute(Namespace = "", AttributeName = "hideAdvancedOptions")]
        public bool HideAdvancedOptions { get; set; }
    }

    /// <summary>
    /// Client side configuration for an application server to connect to.
    /// </summary>
    [XmlRoot(ElementName = "applicationServerClient")]
    public class ApplicationServerClientConfiguration
    {
        [XmlAttribute(Namespace = "", AttributeName = "bindingType")]
        public string BindingType { get; set; }

        [XmlAttribute(Namespace = "", AttributeName = "bindingConfiguration")]
        public string BindingConfiguration { get; set; }

        [XmlAttribute(Namespace = "", AttributeName = "behaviorConfiguration")]
        public string BehaviorConfiguration { get; set; }

        [XmlAttribute(Namespace = "", AttributeName = "servicesUriFormat")]
        public string ServicesUriFormat { get; set; }

        [XmlAttribute(Namespace = "", AttributeName = "authenticationToken")]
        public string AuthenticationToken { get; set; }

        /// <summary>
        /// Specifies whether FileService should be handled via Application Server or executed locally
        /// Allows Documents to be stored in Cloud storage, directly inaccessible to clients (unless this is still a locally mounted network drive)
        /// </summary>
        [XmlAttribute(Namespace = "", AttributeName = "remoteFileService")]
        public bool RemoteFileService { get; set; }

        [XmlAttribute(Namespace = "", AttributeName = "isEnabled")]
        public bool IsEnabled { get; set; }

        [XmlIgnore]
        public string HostName
        {
            get
            {
                return ServicesUriFormat == null ? null : new Uri(ServicesUriFormat).Host;
            }
            set
            {
                var uriBuilder = new UriBuilder(ServicesUriFormat);
                uriBuilder.Host = value;
                ServicesUriFormat = Uri.UnescapeDataString(uriBuilder.Uri.ToString());
            }
        }
    }

    /// <summary>
    /// Returns no remote endpoints if cloud mode is not enabled (so that remote services are not used).
    /// </summary>
    [Singleton]
    public class IOEndpointConfigurationProvider : ConfigurationFileEndpointConfigurationProvider
    {
        public IOEndpointConfigurationProvider(IConfigurationContainer configurationContainer)
            : base(configurationContainer)
        {
        }

        public static void Load()
        {
            var remoteServiceTypes = GetRemoteServiceTypes();

            // need to add to default ConfigruationManager so that built in WCF configuration system picks them up...we can change the URI later via
            // the individual EndpointConfigurations.
            AddContractTypes(System.Configuration.ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection, remoteServiceTypes);
            AddContractTypes(ConfigurationManager.Configuration.GetSection("system.serviceModel/client") as ClientSection, remoteServiceTypes);
        }

        private static Type[] GetRemoteServiceTypes()
        {
            var typesFromBootstrappedAssemblies = (from assembly in Bootstrapper.BootstrappedAssemblies
                                                   from componentAttribute in assembly.GetAttributes<ComponentAttribute>()
                                                   from componentType in componentAttribute.For.Concat(new[] { componentAttribute.Type })
                                                   where (componentType.HasAttribute<RemoteServiceAttribute>() || componentType.GetTypeBaseTypesAndInterfaces().Any(x => x.HasAttribute<ServiceContractAttribute>()))
                                                         && !componentType.IsAbstract
                                                   select componentType);

            // Add required services
            var result = typesFromBootstrappedAssemblies
                .Concat(new[]
                {
                    typeof(AuthenticationService), typeof(AdoService), typeof(AdoComService)
                })
                .Where(rst => rst.GetInterfaces().Any())
                .ToList();

            // Configure whether FileService is remoted
            if (!ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService)
            {
                result.RemoveAll(t => t == typeof(FileService));
            }

            return result.ToArray();
        }

        private static void AddContractTypes(ClientSection clientSection, IEnumerable<Type> contactTypes)
        {
            if (clientSection == null) return;

            var applicationServerClientConfiguration = ConfigurationManager.ApplicationServerClientConfiguration;

            // We will be modifying endpoints collection -> synchronize
            using (new WriteLock(EndpointsLock))
            {
                clientSection.Endpoints.SetReadOnly(false);

                foreach (Type remoteServiceType in contactTypes)
                {
                    var contractName = remoteServiceType.GetInterfaces().First().FullName;
                    if (applicationServerClientConfiguration.ServicesUriFormat == null) return;
                    var address = new Uri(applicationServerClientConfiguration.ServicesUriFormat.FormatWith(remoteServiceType.Name));

                    var endpoint = clientSection.Endpoints.OfType<ChannelEndpointElement>().FirstOrDefault(ep => ep.Name.Equals(remoteServiceType.Name, StringComparison.OrdinalIgnoreCase));

                    if (endpoint == null)
                    {
                        clientSection.Endpoints.Add(new ChannelEndpointElement(new EndpointAddress(address), contractName)
                        {
                            BehaviorConfiguration = applicationServerClientConfiguration.BehaviorConfiguration,
                            Binding = applicationServerClientConfiguration.BindingType,
                            BindingConfiguration = applicationServerClientConfiguration.BindingConfiguration,
                            Name = remoteServiceType.Name
                        });
                    }
                    else
                    {
                        endpoint.SetReadOnly(false);
                        endpoint.Address = address;
                    }
                }

                clientSection.ResetModified();
            }
        }

        public override IEnumerable<EndpointConfiguration> EndpointConfigurations
        {
            get
            {
                if (Bootstrapper.HasRun && !ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled)
                {
                    return Enumerable.Empty<EndpointConfiguration>();
                }

                return base.EndpointConfigurations;
            }
        }
    }

    /// <summary>
    /// Loads config information contextual to the current client name in context (may vary in cloud mode).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ClientContextConfigurationSection<T> : ContextSpecificXmlConfigurationSection<T> where T : class
    {
        protected override object CurrentContext
        {
            get { return ApplicationContext.Current.ClientName; }
        }
    }

    /// <summary>
    /// An IConfigurationContainer implementation that uses the loaded Configuration in the IO ConfigurationManager.
    /// </summary>
    public class IOConfigurationContainer : IConfigurationContainer
    {
        public System.Configuration.Configuration Configuration
        {
            get { return ConfigurationManager.Configuration; }
        }
    }
}