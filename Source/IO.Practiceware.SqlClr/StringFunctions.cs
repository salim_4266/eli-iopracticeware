﻿using System;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Server;

namespace IO.Practiceware.SqlClr
{
    public class StringFunctions
    {
        [SqlFunction]
        public static SqlString SearchRegexPattern(SqlString subject, SqlString pattern)
        {
            try
            {
                return Regex.Match(subject.Value, pattern.Value, 
                    RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled).Value;
                 
            }
            catch (Exception ex)
            {
                if (SqlContext.Pipe != null) SqlContext.Pipe.Send("Error searching Pattern " + ex.Message);
                return "";
            }
        }
    }
}