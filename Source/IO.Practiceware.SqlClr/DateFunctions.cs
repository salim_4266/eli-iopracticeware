﻿using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace IO.Practiceware.SqlClr
{
    public class DateFunctions
    {
        [SqlFunction]
        public static SqlDateTime ConvertToLocalDateTime(SqlDateTime utcDateTime)
        {
            if (utcDateTime.IsNull) return utcDateTime;
            var dt = DateTime.SpecifyKind(utcDateTime.Value, DateTimeKind.Utc);
            return dt.ToLocalTime();
        }

        [SqlFunction(DataAccess = DataAccessKind.Read)]
        public static SqlDateTime ConvertToClientDateTime(SqlDateTime localDateTime)
        {
            if (localDateTime.IsNull) return localDateTime;

            using (var c = new SqlConnection(Common.ConnectionString))
            {
                c.Open();

                using (var cmd = c.CreateCommand())
                {
                    const string sql = @"
IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
BEGIN
	SELECT ClientTimeZoneOffset FROM model.UserContext;
END
ELSE
SELECT 0";
                    cmd.CommandText = sql;
                    var offset = cmd.ExecuteScalar();
                    if (offset is int && (int)offset != 0)
                    {
                        return localDateTime.Value.AddMinutes((int)offset);
                    }
                }
            }
            return localDateTime;
        }
    }
}