﻿using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;
using IO.Practiceware.SqlClr.NewCropUpdate1Service;
using Microsoft.SqlServer.Server;

namespace IO.Practiceware.SqlClr
{
    public class NewCropFunctions
    {
        private static string _newCropClientConfigurationXml;
        private static NewCropClientConfiguration _newCropClientConfiguration;

        [SqlProcedure]
        public static void GenerateMeaningfulUseUtilizationReport(SqlString userId, SqlString userType, SqlString licensedPrescriberId, SqlDateTime startDate, SqlDateTime endDate, SqlInt32 quarterNumber, SqlString quarterYear, SqlInt16 timePeriodQueryType, SqlString includeSchema, out SqlString transactionId)
        {
            NewCropClientConfiguration configuration = GetNewCropClientConfiguration();
            Update1 service = GetUpdate1Service(configuration);

            ReportDetailResult result = service.GenerateMeaningfulUseUtilizationReport(GetCredentials(configuration),
                GetAccountRequest(configuration),
                new PatientInformationRequester { UserId = userId.Value, UserType = userType.Value },
                licensedPrescriberId.Value,
                startDate.IsNull ? null : startDate.Value.ToString("yyyyMMdd"),
                endDate.IsNull ? null : endDate.Value.ToString("yyyyMMdd"),
                quarterNumber.IsNull ? 0 : quarterNumber.Value,
                quarterYear.IsNull ? null : quarterYear.Value,
                timePeriodQueryType.IsNull ? TimePeriodQueryType.DateRange : (TimePeriodQueryType)timePeriodQueryType.Value,
                includeSchema.IsNull ? null : includeSchema.Value);

            if (String.IsNullOrEmpty(result.reportDetail.TransactionID))
                throw new Exception("No transaction id was returned by NewCrop.");

            if (!string.Equals("success", result.reportDetail.Status, StringComparison.OrdinalIgnoreCase))
                throw new Exception(string.Format("Report generation was not successful: {0}.", result.result.Message));

            StoreParametersAndResult(userId, userType, licensedPrescriberId, startDate, endDate, quarterNumber, quarterYear, timePeriodQueryType, includeSchema, result);

            transactionId = result.reportDetail.TransactionID;
        }

        private static void StoreParametersAndResult(SqlString userId, SqlString userType, SqlString licensedPrescriberId, SqlDateTime startDate, SqlDateTime endDate, SqlInt32 quarterNumber, SqlString quarterYear, SqlInt16 timePeriodQueryType, SqlString includeSchema, ReportDetailResult result)
        {
            using (var connection = new SqlConnection(Common.ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    const string sql = @"

INSERT INTO [model].[NewCropUtilizationReports]
           ([TransactionId]
           ,[Timestamp]
           ,[Status]
           ,[StatusTimestamp]
           ,[Message]
           ,[UserId]
           ,[UserType]
           ,[LicensedPrescriberId]
           ,[StartDate]
           ,[EndDate]
           ,[QuarterNumber]
           ,[QuarterYear]
           ,[TimePeriodType]
           ,[IncludeSchema]
           ,[ReportContent])
     VALUES
           (@transactionId
           ,@timestamp
           ,@status
           ,@statusTimestamp
           ,@message
           ,@userId
           ,@userType
           ,@licensedPrescriberId
           ,@startDate
           ,@endDate
           ,@quarterNumber
           ,@quarterYear
           ,@timePeriodType
           ,@includeSchema
           ,NULL)
";

                    command.CommandText = sql;

                    command.Parameters.Add(new SqlParameter("transactionId", result.reportDetail.TransactionID));
                    command.Parameters.Add(new SqlParameter("timestamp", DateTime.UtcNow));
                    command.Parameters.Add(new SqlParameter("status", result.reportDetail.Status));
                    command.Parameters.Add(new SqlParameter("statusTimestamp", System.Data.SqlDbType.DateTime) { Value = result.reportDetail.StatusTimestamp == DateTime.MinValue ? null : result.reportDetail.StatusTimestamp as object });
                    command.Parameters.Add(new SqlParameter("message", result.reportDetail.Message));
                    command.Parameters.Add(new SqlParameter("userId", userId));
                    command.Parameters.Add(new SqlParameter("userType", userType));
                    command.Parameters.Add(new SqlParameter("licensedPrescriberId", licensedPrescriberId));
                    command.Parameters.Add(new SqlParameter("startDate", startDate));
                    command.Parameters.Add(new SqlParameter("endDate", endDate));
                    command.Parameters.Add(new SqlParameter("quarterNumber", quarterNumber));
                    command.Parameters.Add(new SqlParameter("quarterYear", quarterYear));
                    command.Parameters.Add(new SqlParameter("timePeriodType", timePeriodQueryType));
                    command.Parameters.Add(new SqlParameter("includeSchema", includeSchema));

                    command.ExecuteNonQuery();
                }
            }
        }

        [SqlProcedure]
        public static void GetMeaningfulUseUtilizationReport(SqlString transactionId, SqlString userId, SqlString userType,
// ReSharper disable OptionalParameterRefOut
            [Optional]out SqlInt32 cpoeMedNumerator,
            [Optional]out SqlInt32 cpoeRadNumerator,
            [Optional]out SqlInt32 cpoeLabNumerator,
            [Optional]out SqlInt32 medListNumerator,
            [Optional]out SqlInt32 medAllergyListNumerator,
            [Optional] out SqlInt32 patientEducationNumerator,
            [Optional]out SqlInt32 eRxStage1Numerator,
            [Optional]out SqlInt32 eRxStage1Denominator,
            [Optional]out SqlInt32 eRxStage2Numerator,
            [Optional]out SqlInt32 eRxStage2Denominator,
            [Optional]out SqlInt32 medReconcilationNumerator,
            [Optional]out SqlInt32 clinicalSummaryStage1Numerator,
            [Optional]out SqlInt32 clinicalSummaryStage2Numerator,
            [Optional] out SqlInt32 vdtHealthInfoProvidedNumerator,
            [Optional]out SqlInt32 vdtHealthInfoAccessedNumerator,
            [Optional]out SqlInt32 secureMessagingNumerator,
            [Optional]out SqlInt32 summaryOfCareNumerator
// ReSharper restore OptionalParameterRefOut
            )
        {

            if (transactionId == null || transactionId == SqlString.Null) throw new ArgumentNullException("transactionId");

            NewCropClientConfiguration configuration = GetNewCropClientConfiguration();
            Update1 service = GetUpdate1Service(configuration);

            var result = service.GetMeaningfulUseUtilizationReport(GetCredentials(configuration),
                GetAccountRequest(configuration),
                new PatientInformationRequester { UserId = userId.Value, UserType = userType.Value },
                transactionId.IsNull ? null : transactionId.Value);

            if (!string.Equals("Complete", result.reportDownloadDetail.ReportGenerationStatusMessage, StringComparison.OrdinalIgnoreCase))
                throw new Exception(string.Format("The report has a status of {0}, please wait and try again.", result.reportDownloadDetail.ReportGenerationStatusMessage));

            var reportXml = DecodeAndDecompress(result.reportDownloadDetail.ReportContents);

            SaveReport(transactionId.Value, reportXml);
            /*
             * SAMPLE RESPONSE XML
             * <MUPrescriberReport>
              <Table>
                <CPOEMedNumerator>0</CPOEMedNumerator>
                <CPOERadNumerator>0</CPOERadNumerator>
                <CPOELabNumerator>0</CPOELabNumerator>
                <MedListNumerator>2</MedListNumerator>
                <MedAllergyListNumerator>1</MedAllergyListNumerator>
                <PatientEducationNumerator>0</PatientEducationNumerator>
                <eRxStage1Numerator>0</eRxStage1Numerator>
                <eRxStage1Denominator>0</eRxStage1Denominator>
                <eRxStage2Numerator>0</eRxStage2Numerator>
                <eRxStage2Denominator>0</eRxStage2Denominator>
                <MedReconcilationNumerator>0</MedReconcilationNumerator>
                <ClinicalSummaryStage1Numerator>2</ClinicalSummaryStage1Numerator>
                <ClinicalSummaryStage2Numerator>2</ClinicalSummaryStage2Numerator>
                <VDTHealthInfoProvidedNumerator>0</VDTHealthInfoProvidedNumerator>
                <VDTHealthInfoAccessedNumerator>0</VDTHealthInfoAccessedNumerator>
                <SecureMessagingNumerator>0</SecureMessagingNumerator>
                <SummaryOfCareNumerator>0</SummaryOfCareNumerator>
              </Table>
            </MUPrescriberReport>
             */

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(reportXml);

            cpoeMedNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//CPOEMedNumerator");

            cpoeRadNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//CPOERadNumerator");

            cpoeLabNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//CPOELabNumerator");

            medListNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//MedListNumerator");

            medAllergyListNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//MedAllergyListNumerator");

            patientEducationNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//PatientEducationNumerator");

            eRxStage1Numerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//eRxStage1Numerator");

            eRxStage1Denominator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//eRxStage1Denominator");

            eRxStage2Numerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//eRxStage2Numerator");

            eRxStage2Denominator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//eRxStage2Denominator");

            medReconcilationNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//MedReconcilationNumerator");

            clinicalSummaryStage1Numerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//ClinicalSummaryStage1Numerator");

            clinicalSummaryStage2Numerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//ClinicalSummaryStage2Numerator");

            vdtHealthInfoProvidedNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//VDTHealthInfoProvidedNumerator");

            vdtHealthInfoAccessedNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//VDTHealthInfoAccessedNumerator");

            secureMessagingNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//SecureMessagingNumerator");

            summaryOfCareNumerator = GetSqlInt32OrDefaultFromXml(xmlDocument, "//SummaryOfCareNumerator");
        }

        /// <summary>
        /// Gets the SqlInt32 or SqlInt32.Null from XML element selected via xpath. Returns SqlInt32.Null if not found or not an int.
        /// </summary>
        /// <param name="xmlDocument">The XML document.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns></returns>
        private static SqlInt32 GetSqlInt32OrDefaultFromXml(XmlDocument xmlDocument, string xpath)
        {
            if (xmlDocument.DocumentElement == null) return SqlInt32.Null;

            var node = xmlDocument.DocumentElement.SelectSingleNode(xpath);

            int i;
            if (node != null && int.TryParse(node.InnerText, out i)) return i;

            return SqlInt32.Null;
        }

        /// <summary>
        /// Saves the report to the NewCropUtilizationReports table.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="report">The report.</param>
        private static void SaveReport(string transactionId, string report)
        {
            if (string.IsNullOrEmpty(report)) return;

            using (var connection = new SqlConnection(Common.ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText =
                        string.Format("UPDATE [model].[NewCropUtilizationReports] SET [ReportContent] = '{0}' WHERE [TransactionId] = '{1}'",
                            report, transactionId);

                    command.ExecuteNonQuery();
                }

            }
        }

        /// <summary>
        /// Decodes the and decompresses the base 64 encoded data.
        /// </summary>
        /// <param name="reportContents">The report contents.</param>
        /// <returns></returns>
        private static string DecodeAndDecompress(string reportContents)
        {
            var compressedBytes = Convert.FromBase64String(reportContents);

            using (var ms = new MemoryStream(compressedBytes))
            using (var zipStream = new GZipStream(ms, CompressionMode.Decompress))
            using (var sr = new StreamReader(zipStream))
            {
                return sr.ReadToEnd();
            }
        }

        /// <summary>
        ///     Gets the NewCrop Update1 service and sets the url based on the configuration provided.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        private static Update1 GetUpdate1Service(NewCropClientConfiguration configuration)
        {
            var service = new Update1();
            service.Url = configuration.Update1ServiceUrl;
            return service;
        }

        /// <summary>
        ///     Creates credentials from the configuration provided.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        private static Credentials GetCredentials(NewCropClientConfiguration configuration)
        {
            return new Credentials
                   {
                       Name = configuration.UserName,
                       PartnerName = configuration.PartnerName,
                       Password = configuration.Password
                   };
        }

        /// <summary>
        ///     Creates an account request from the configuration provided.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        private static AccountRequest GetAccountRequest(NewCropClientConfiguration configuration)
        {
            return new AccountRequest
                   {
                       AccountId = configuration.AccountId,
                       SiteId = configuration.SiteId
                   };
        }

        /// <summary>
        ///     Gets the new crop client configuration setting. Checks the DB if the xml value has changed since we last
        ///     deserialized it. If it has, deserializes it again and caches the new value.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">NewCropClientConfiguration not found.</exception>
        private static NewCropClientConfiguration GetNewCropClientConfiguration()
        {
            using (var connection = new SqlConnection(Common.ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT TOP 1 VALUE FROM model.ApplicationSettings WHERE Name = 'NewCropClientConfiguration'";
                    var value = command.ExecuteScalar() as string;

                    if (value == null) throw new Exception("NewCropClientConfiguration not found.");

                    if (_newCropClientConfigurationXml != value || _newCropClientConfiguration == null)
                    {
                        _newCropClientConfigurationXml = value;
                        _newCropClientConfiguration = DeserializeClientConfiguration(value);
                    }
                }
            }
            return _newCropClientConfiguration;
        }

        /// <summary>
        ///     Deserializes the specified value into a NewCropClientConfiguration.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static NewCropClientConfiguration DeserializeClientConfiguration(string value)
        {
            using (var sr = new StringReader(value))
            using (XmlReader xr = XmlReader.Create(sr))
            {
                // don't use XmlSerializer - even though we generate the XmlSerializers assembly, .NET will not always find it
                xr.Read();
                return new NewCropClientConfiguration
                       {
                           Update1ServiceUrl = xr.GetAttribute("update1ServiceUrl"),
                           PartnerName = xr.GetAttribute("partnerName"),
                           UserName = xr.GetAttribute("userName"),
                           Password = xr.GetAttribute("password"),
                           AccountId = xr.GetAttribute("accountId"),
                           SiteId = xr.GetAttribute("siteId")
                       };
            }
        }

    }

    /// <summary>
    ///     Xml serializable information about how to connect to NewCrop
    /// </summary>
    [XmlRoot("newCropClientConfiguration")]
    [XmlType]
    [Serializable]
    public class NewCropClientConfiguration
    {
        [XmlAttribute("update1ServiceUrl")]
        public string Update1ServiceUrl { get; set; }

        [XmlAttribute("partnerName")]
        public string PartnerName { get; set; }

        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("accountId")]
        public string AccountId { get; set; }

        [XmlAttribute("siteId")]
        public string SiteId { get; set; }
    }
}