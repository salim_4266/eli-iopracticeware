﻿using System.Data.SqlClient;

namespace IO.Practiceware.SqlClr
{
    /// <summary>
    ///     Common class used for assembly reference.
    /// </summary>
    public class Common
    {
        static Common()
        {
            ConnectionString = "context connection=true";
        }

        /// <summary>
        /// Gets the application server URL using the context connection or localhost if not found.
        /// </summary>
        /// <returns></returns>
        internal static string ApplicationServerUrl
        {
            get
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT TOP 1 Value FROM model.ApplicationSettings WHERE Name = 'ApplicationServerUrl'";
                        return command.ExecuteScalar() as string ?? "https://localhost/IOPracticeware";
                    }
                }
            }
        }

        /// <summary>
        /// Gets the application services root URL using the context connection or localhost if not found.
        /// </summary>
        /// <value>
        /// The application services root URL.
        /// </value>
        internal static string ApplicationServicesRootUrl
        {
            get { return ApplicationServerUrl + "/Services/"; }
        }


        internal static string ConnectionString { get; set; }
    }
}