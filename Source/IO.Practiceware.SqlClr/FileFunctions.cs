﻿using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using Microsoft.SqlServer.Server;

namespace IO.Practiceware.SqlClr
{
    public class FileFunctions
    {
        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillGetFiles")]
        public static IEnumerable GetFiles(SqlString path, SqlString searchPattern)
        {
            if (!Directory.Exists(path.Value))
                return new string[0];
            return Directory.GetFiles(path.Value, searchPattern.Value);
        }

        public static void FillGetFiles(object o, out SqlString path)
        {
            path = (string) o;
        }
    }
}