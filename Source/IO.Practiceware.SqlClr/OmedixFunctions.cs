﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using IO.Practiceware.SqlClr.AuthenticationService;
using IO.Practiceware.SqlClr.OmedixIntegrationService;
using Microsoft.SqlServer.Server;

namespace IO.Practiceware.SqlClr
{
    public class OmedixFunctions
    {
        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorSummaryForPatientEducation")]
        public static IEnumerable GetNumeratorSummaryForPatientEducation(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorSummaryForPatientEducation(param);

            return result.providers;
        }

        public static void FillNumeratorSummaryForPatientEducation(object o, out SqlString remoteProviderId, out SqlString providerFirstName, out SqlString providerLastName, out SqlInt32 providerCount)
        {
            var model = (ProviderInfo)o;

            providerFirstName = model.ProviderFirstName;
            providerLastName = model.ProviderLastName;
            providerCount = model.ProviderCount;
            remoteProviderId = model.RemoteProviderID;
        }

        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorDetailsForPatientEducation")]
        public static IEnumerable GetNumeratorDetailsForPatientEducation(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorDetailsForPatientEducation(param);

            return result;
        }

        public static void FillNumeratorDetailsForPatientEducation(object o,
            out SqlDateTime dateTimeOfAction,
            out SqlString emailSubject,
            out SqlDateTime patientDob,
            out SqlString patientEmailId,
            out SqlString patientFirstName,
            out SqlString patientGender,
            out SqlString patientLastName,
            out SqlString patientRemoteId,
            out SqlString providerFirstName,
            out SqlString providerLastName,
            out SqlString providerRemoteId)
        {
            var model = (NumeratorPatientEducationDetailsModel)o;

            patientRemoteId = model.PatientRemoteId;
            patientFirstName = model.PatientFirstName;
            patientLastName = model.PatientLastName;
            patientGender = model.PatientGender;
            patientDob = model.PatientDob.HasValue ? (SqlDateTime)model.PatientDob : SqlDateTime.Null;
            patientEmailId = model.PatientEmailId;
            emailSubject = model.EmailSubject;
            providerRemoteId = model.ProviderRemoteId;
            providerFirstName = model.ProviderFirstName;
            providerLastName = model.ProviderLastName;
            dateTimeOfAction = model.DateTimeOfAction.HasValue ? (SqlDateTime)model.DateTimeOfAction : SqlDateTime.Null;
        }

        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorSummaryForSecureMessage")]
        public static IEnumerable GetNumeratorSummaryForSecureMessage(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorSummaryForSecureMessage(param);

            return result.providers;
        }


        public static void FillNumeratorSummaryForSecureMessage(object o, out SqlString remoteProviderId, out SqlString providerFirstName, out SqlString providerLastName, out SqlInt32 providerCount)
        {
            var model = (ProviderInfo)o;

            providerFirstName = model.ProviderFirstName;
            providerLastName = model.ProviderLastName;
            providerCount = model.ProviderCount;
            remoteProviderId = model.RemoteProviderID;
        }

        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorDetailsForSecureMessage")]
        public static IEnumerable GetNumeratorDetailsForSecureMessage(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorDetailsForSecureMessage(param);

            return result;
        }

        public static void FillNumeratorDetailsForSecureMessage(object o,
            out SqlString providerFirstName,
            out SqlString providerLastName,
            out SqlDateTime dateTimeOfAction,
            out SqlString patientRemoteId,
            out SqlDateTime patientDob,
            out SqlString patientFirstName,
            out SqlString patientGender,
            out SqlString patientLastName)
        {
            var model = (NumeratorSecureMessageDetailsModel)o;

            providerFirstName = model.ProviderFirstName;
            providerLastName = model.ProviderLastName;
            dateTimeOfAction = model.DateTimeOfActionSpecified && model.DateTimeOfAction.HasValue ? (SqlDateTime)model.DateTimeOfAction : SqlDateTime.Null;
            patientDob = model.PatientDobSpecified && model.PatientDob.HasValue ? (SqlDateTime)model.PatientDob : SqlDateTime.Null;
            patientRemoteId = model.PatientRemoteId;
            patientFirstName = model.PatientFirstName;
            patientLastName = model.PatientLastName;
            patientGender = model.PatientGender;

        }

        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorSummaryForViewDownloadTransmit")]
        public static IEnumerable GetNumeratorSummaryForViewDownloadTransmit(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorSummaryForHealthInfoViewDownloadTransmit(param);

            return result.report;
        }

        public static void FillNumeratorSummaryForViewDownloadTransmit(object o, out SqlString patientRemoteId,
            out SqlString patientDob,
            out SqlString patientFirstName,
            out SqlString patientGender,
            out SqlString patientLastName,
            out SqlString dateTimeOfAction,
            out SqlDateTime actionDateTime)
        {
            var model = (VdtReport)o;

            patientRemoteId = model.PatientRemoteID;
            patientFirstName = model.PatientFirstName;
            patientLastName = model.PatientLastName;
            patientGender = model.PatientGender;
            patientDob = model.PatientDOB;
            dateTimeOfAction = model.DateTimeOfAction;
            actionDateTime = model.ActionDateTimeSpecified && model.ActionDateTime.HasValue ? (SqlDateTime)model.ActionDateTime : SqlDateTime.Null;
        }

        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillNumeratorDetailsForViewDownloadTransmit")]
        public static IEnumerable GetNumeratorDetailsForViewDownloadTransmit(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = CreateNumeratorSearchModel(fromDate, toDate, physicianId, receivingApplication);

            var result = GetOmedixIntegrationService().GetNumeratorDetailsForHealthInfoViewDownloadTransmit(param);

            return result;
        }

        public static void FillNumeratorDetailsForViewDownloadTransmit(object o,
            out SqlDateTime dateTimeOfAction,
            out SqlString patientRemoteId,
            out SqlDateTime patientDob,
            out SqlString patientFirstName,
            out SqlString patientGender,
            out SqlString patientLastName)
        {
            var model = (NumeratorVdtDetailsModel)o;

            patientRemoteId = model.PatientRemoteId;
            patientFirstName = model.PatientFirstName;
            patientLastName = model.PatientLastName;
            patientGender = model.PatientGender;
            patientDob = model.PatientDob.HasValue ? (SqlDateTime)model.PatientDob : SqlDateTime.Null;
            dateTimeOfAction = model.DateTimeOfAction.HasValue ? (SqlDateTime)model.DateTimeOfAction : SqlDateTime.Null;

        }

        /// <summary>
        /// Gets the omedix integration service (pre-authenticated).
        /// </summary>
        /// <returns></returns>
        private static CustomBinding_IOmedixIntegrationService GetOmedixIntegrationService()
        {
            // login and set auth cookie 
            var omedixIntegrationService = new CustomBinding_IOmedixIntegrationService();
            omedixIntegrationService.Url = Common.ApplicationServicesRootUrl + "OmedixIntegrationService";
            AuthenticationProvider.Current.Login(omedixIntegrationService);
            return omedixIntegrationService;
        }

        private static NumeratorSearchModel CreateNumeratorSearchModel(SqlDateTime fromDate, SqlDateTime toDate, SqlString physicianId, SqlString receivingApplication)
        {
            var param = new NumeratorSearchModel
            {
                FromDateSpecified = !fromDate.IsNull,
                FromDate = !fromDate.IsNull ? (DateTime)fromDate : default(DateTime),
                ToDateSpecified = !toDate.IsNull,
                ToDate = !toDate.IsNull ? (DateTime)toDate : default(DateTime),
                PhysicianId = !physicianId.IsNull ? (string)physicianId : null,
                ReceivingApplication = !receivingApplication.IsNull ? (string)receivingApplication : null
            };
            return param;
        }
    }
}