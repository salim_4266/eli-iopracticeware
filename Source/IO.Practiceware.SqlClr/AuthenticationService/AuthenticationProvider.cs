﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Security;
using System.Web.Services.Protocols;

namespace IO.Practiceware.SqlClr.AuthenticationService
{
    /// <summary>
    ///     Helper for authenticating with IO Practiceware web services. Leverages caching to only authenticate once.
    /// </summary>
    public class AuthenticationProvider
    {
        static AuthenticationProvider()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }

        private static readonly object SyncRoot = new object();
        private CookieCollection _cookies;
        private string _lastApplicationServerUrl;

        /// <summary>
        /// A singleton authentication service instance.
        /// </summary>
        internal static readonly AuthenticationProvider Current = new AuthenticationProvider();

        /// <summary>
        ///     Logs in using the specified username and password in the DB (accessed from the context connection). Stores auth
        ///     cookie in the container.
        /// </summary>
        /// <param name="cookieContainer">The cookie container.</param>
        public void Login(CookieContainer cookieContainer)
        {
            Login(null, null, cookieContainer);
        }

        /// <summary>
        /// Logs in using the specified username and password in the DB (accessed from the context connection). Stores auth
        /// cookie in the container.
        /// </summary>
        /// <param name="httpWebClientProtocol">The HTTP web client protocol.</param>
        public void Login(HttpWebClientProtocol httpWebClientProtocol)
        {
            if (httpWebClientProtocol.CookieContainer == null) httpWebClientProtocol.CookieContainer = new CookieContainer();

            Login(httpWebClientProtocol.CookieContainer);
        }

        /// <summary>
        /// Logs in using the specified username and password. Stores auth cookie in the container.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="httpWebClientProtocol">The HTTP web client protocol.</param>
        public void Login(string username, string password, HttpWebClientProtocol httpWebClientProtocol)
        {
            if (httpWebClientProtocol.CookieContainer == null) httpWebClientProtocol.CookieContainer = new CookieContainer();

            Login(username, password, httpWebClientProtocol.CookieContainer);
        }

        /// <summary>
        ///     Logs in using the specified username and password. Stores auth cookie in the container.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="cookieContainer">The cookie container.</param>
        public void Login(string username, string password, CookieContainer cookieContainer)
        {
            lock (SyncRoot)
            {
                if (RequiresLogin())
                {
                    _lastApplicationServerUrl = Common.ApplicationServerUrl;
                    _cookies = LoginWithWebService(username, password);
                }

                SetCookies(cookieContainer);
            }
        }

        /// <summary>
        ///     Sets the cookies from the authentication service in the cookie container.
        /// </summary>
        /// <param name="cookieContainer">The cookie container.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void SetCookies(CookieContainer cookieContainer)
        {
            foreach (Cookie c in _cookies)
            {
                cookieContainer.Add(c);
            }
        }

        /// <summary>
        ///     Logs in using the AuthenticateService web service reference. Returns associated cookies. If username and password are unspecified, uses username and password in the DB (accessed from the context connection)
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <exception cref="System.Security.SecurityException">Could not login to IO Practiceware AuthenticationService.</exception>
        private static CookieCollection LoginWithWebService(string username, string password)
        {
            if (username == null && password == null)
            {
                GetDefaultUsernameAndPassword(out username, out password);
            }

            var service = new CustomBinding_AuthenticationService { CookieContainer = new CookieContainer() };
            service.Url = Common.ApplicationServicesRootUrl + "AuthenticationService";
            bool result;
            bool resultSpecified;
            service.Login(username, password, null, true, true, out result, out resultSpecified);

            if (!result || !resultSpecified) throw new SecurityException("Could not login to IO Practiceware AuthenticationService. Login failed.");

            var cookies = service.Cookies;
            foreach (Cookie cookie in cookies)
            {
                cookie.Domain = new Uri(service.Url).Host;
            }

            if (cookies == null || cookies.Count == 0) throw new InvalidOperationException(string.Format("Could not login to IO Practiceware AuthenticationService. No authentication cookie was returned from {0}.", service.Url));

            return cookies;
        }

        /// <summary>
        ///     Returns true if a new login call to the web service is required (cookies have not been set or any are expired).
        /// </summary>
        /// <returns></returns>
        private bool RequiresLogin()
        {
            if (_cookies == null || _lastApplicationServerUrl != Common.ApplicationServerUrl) return true;
            foreach (Cookie c in _cookies)
            {
                if (c.Expired || c.Expires <= DateTime.UtcNow) return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the default username and password from the context connection using #UserContext if it exists, otherwise using the default IOP user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="System.InvalidOperationException">Could not find user from UserContext or using default values.</exception>
        private static void GetDefaultUsernameAndPassword(out string username, out string password)
        {
            using (var connection = new SqlConnection(Common.ConnectionString))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    string filter;

                    command.CommandText = @"IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL SELECT UserId FROM #UserContext";

                    int userId;
                    if (int.TryParse(command.ExecuteScalar() as string ?? "", out userId))
                    {
                        // got User Id from UserContext
                        filter = string.Format("Id = {0}", userId);
                    }
                    else
                    {
                        filter = "UserName = 'IOP'"; // use default IOP user
                    }

                    command.CommandText = string.Format("SELECT TOP 1 PID FROM model.Users WHERE {0}", filter);

                    var pid = (string)command.ExecuteScalar();

                    command.CommandText = @"SELECT TOP 1 Value FROM model.ApplicationSettings WHERE Name = 'AuthenticationToken'";
                    var authenticationToken = command.ExecuteScalar() as string ?? "PracticeRepository";

                    username = string.Join("@", new[] { pid, authenticationToken });
                    password = pid;
                }
            }

            if (username == null || password == null)
                throw new InvalidOperationException("Could not find user from UserContext or using default values.");
        }

    }

    // ReSharper disable InconsistentNaming
    public partial class CustomBinding_AuthenticationService
    // ReSharper restore InconsistentNaming
    {
        public CookieCollection Cookies { get; private set; }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            Cookies = null;

            var response = base.GetWebResponse(request);

            if (response is HttpWebResponse)
            {
                Cookies = ((HttpWebResponse)response).Cookies;
            }

            return response;
        }

    }
}