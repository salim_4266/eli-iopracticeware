﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service.Common;
using IO.Practiceware.Services.ApplicationSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quartz;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class MonitoringServiceTests : TestBase
    {
        [TestMethod]
        public void TenantRegistryTests()
        {
            var registry = Common.ServiceProvider.GetService<ITenantRegistry>();
            TenantActiveStateChangedArgs lastArgs = null;
            registry.TenantActiveStateChanged += (e, args) => lastArgs = args;

            // Remove all tenants but one unit test runs in
            registry.ConfiguredTenants.Keys
                .Where(t => t != ApplicationContext.Current.ClientKey)
                .ToList()
                .ForEach(k => registry.ConfiguredTenants.Remove(k));

            Assert.IsTrue(registry.ConfiguredTenants.Count == 1, "One tenant is expected to be configured");
            Assert.AreEqual(0, registry.GetActiveTenantsCount(), "No tenants should be active yet");
            Assert.IsTrue(registry.IsTenantAccessible(registry.PrimaryTenantKey), "Tenant should be accessible");
            Assert.IsFalse(registry.IsTenantActive(registry.PrimaryTenantKey), "Tenant shouldn't be active");

            const string dropOnCleanupValue = "testValue_dropOnCleanup";
            const string dropOnDeactivateValue = "testValue_dropOnDeactivate";
            var valueOnCleanup = new TestTenantStoredValue { DisposalType = TenantStoredValueDisposalType.UponCleanup };
            var valueOnDeactivate = new TestTenantStoredValue { DisposalType = TenantStoredValueDisposalType.UponDeactivate };

            // Test deactivating non-activated tenant
            var tenantStorage = registry.GetTenantInMemoryStorage(registry.PrimaryTenantKey);
            tenantStorage.TryAdd(dropOnCleanupValue, valueOnCleanup);
            tenantStorage.TryAdd(dropOnDeactivateValue, valueOnDeactivate);

            // Test activating and then deactivating
            registry.SetTenantActivationLockState(registry.PrimaryTenantKey, TenantActivationLockState.Active);
            Assert.IsTrue(registry.IsTenantActive(registry.PrimaryTenantKey), "Tenant should be active");
            Assert.IsTrue(lastArgs != null 
                && lastArgs.LockState == TenantActivationLockState.Active 
                && lastArgs.TenantKey == registry.PrimaryTenantKey, 
                "Tenant activation event should fire");
            lastArgs = null;
            registry.SetTenantActivationLockState(registry.PrimaryTenantKey, TenantActivationLockState.Active);
            Assert.IsNull(lastArgs, "Duplicate activation events shouldn't fire");

            registry.SetTenantActivationLockState(registry.PrimaryTenantKey, TenantActivationLockState.NotActive);
            Assert.IsFalse(registry.IsTenantActive(registry.PrimaryTenantKey), "Tenant shouldn't be active after deactivation");
            Assert.IsTrue(lastArgs != null 
                && lastArgs.LockState == TenantActivationLockState.NotActive 
                && lastArgs.TenantKey == registry.PrimaryTenantKey, 
                "Tenant deactivation event should fire");
            lastArgs = null;
            registry.SetTenantActivationLockState(registry.PrimaryTenantKey, TenantActivationLockState.NotActive);
            Assert.IsNull(lastArgs, "Duplicate deactivation events shouldn't fire");
            Assert.IsTrue(valueOnDeactivate.IsDisposed, "Value (on deactivate) should be disposed upon deactivation");
            Assert.IsFalse(tenantStorage.ContainsKey(dropOnDeactivateValue), "Value (on deactivate) should not be among stored values for tenant");
            Assert.IsTrue(tenantStorage.ContainsKey(dropOnCleanupValue), "Deactivating tenant should not drop stored values with UponCleanup disposal");
            Assert.IsFalse(valueOnCleanup.IsDisposed, "Value (on cleanup) should not be disposed");

            // Test resetting registry
            valueOnDeactivate.IsDisposed = false;
            tenantStorage.TryAdd(dropOnDeactivateValue, valueOnDeactivate);

            registry.SetTenantActivationLockState(registry.PrimaryTenantKey, TenantActivationLockState.Active);
            lastArgs = null;
            registry.Reset();

            Assert.IsTrue(lastArgs != null 
                && lastArgs.LockState == TenantActivationLockState.NotActive 
                && lastArgs.TenantKey == registry.PrimaryTenantKey, 
                "Tenant deactivation event should fire upon reset");
            Assert.IsTrue(valueOnCleanup.IsDisposed, "On cleanup value is now disposed");
            Assert.IsTrue(valueOnDeactivate.IsDisposed, "On deactivate value is now disposed");
            Assert.AreNotSame(tenantStorage, registry.GetTenantInMemoryStorage(registry.PrimaryTenantKey), "New storage should have been initialized");
        }

        [TestMethod]
        public void TenantJobWithStateTests()
        {
            string testJobName = typeof (TestTenantJobWithState).Name;
            var target = new TestTenantJobWithState();
            var registry = new TestTenantRegistry { ConcurrencyLimit = 4, PrimaryTenantKey = "key1" };
            target.TenantRegistry = registry;

            // Configure 15 tenants
            Enumerable.Range(1, 15).ForEach(i => registry.ConfiguredTenants.Add("key" + i, "name" + i));

            var accessibleTenants = new List<string> {"key14", "key15"};
            registry.IsTenantAccessibleFunc = accessibleTenants.Contains;
            registry.SetTenantActivationLockState("key1", TenantActivationLockState.Active);
            registry.SetTenantActivationLockState("key2", TenantActivationLockState.Active);

            // Execute job supporting concurrency with accessible tenants differ from active ones
            target.SetSupportsConcurrency = true;
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(target.Executions.Count == 2 
                && target.Executions.All(e => accessibleTenants.Contains(e.Item1)), 
                "Only accessible tenants should execute");
            Assert.IsTrue(registry.TenantStorage.Count == 2 
                && registry.TenantStorage.All(ts => ts.Value.All(s => s.Value.DisposalType == TenantStoredValueDisposalType.UponCleanup)), 
                "Stored state of concurrent tenant jobs should be disposed upon cleanup only");
            Assert.IsTrue(registry.TenantTotalExecutionTime.Count(t => accessibleTenants.Contains(t.Key)) == 2
                && registry.TenantTotalExecutionTime
                    .Where(t => accessibleTenants.Contains(t.Key))
                    .All(et => et.Value.GetRecentExecutionsTotalTime(testJobName) > TimeSpan.Zero),
                "Execution times must be logged");

            // Execute job with accessible tenants differ from active ones 
            target.Executions.Clear();
            target.SetSupportsConcurrency = false;
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.AreEqual(0, target.Executions.Count, "No executions when accessible tenants are not active");

            // Execute job for active tenants
            registry.IsTenantAccessibleFunc = t => true;
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(target.Executions.Count == 2 
                && target.Executions.All(e => e.Item1 == "key1" || e.Item1 == "key2"),  
                "Executes only for active tenants");

            // Activate all tenants, execute twice and verify
            target.Executions.Clear();
            registry.TenantStorage.Clear();
            Enumerable.Range(1, 15).ForEach(i => registry.SetTenantActivationLockState("key" + i, TenantActivationLockState.Active));
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(target.Executions.Count == 15
                          && target.Executions.GroupBy(e => e.Item2).Count() == registry.ConcurrencyLimit, 
                          "Must execute for all tenants according to concurrency limit: " + registry.ConcurrencyLimit);
            target.Executions.Clear();
            registry.ConcurrencyLimit = 3;
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(target.Executions.Count == 15
              && target.Executions.GroupBy(e => e.Item2).Count() == registry.ConcurrencyLimit,
              "Must execute for all tenants according to concurrency limit: " + registry.ConcurrencyLimit);

            Assert.IsTrue(registry.TenantStorage.Count == 15 
                && registry.TenantStorage.All(tv => tv.Value.All(jv => 
                    jv.Key == testJobName + "_State" 
                    && jv.Value.DisposalType == TenantStoredValueDisposalType.UponDeactivate
                    && jv.Value.CastTo<TenantState<TestTenantJobState>>().State.TenantsPassedInto.Count == 1
                    && jv.Value.CastTo<TenantState<TestTenantJobState>>().State.TenantsPassedInto.First() == tv.Key
                    && jv.Value.CastTo<TenantState<TestTenantJobState>>().State.ExecutionsPerCurrentTenant == 2)),
                "Job state should be properly initialized and passed between job executions");

            var tenantState = (TenantState<TestTenantJobState>)registry.TenantStorage.First().Value.First().Value;
            tenantState.Dispose();
            Assert.IsTrue(tenantState.State.IsDisposed, "Disposing TenantState<> should also dispose enclosed state object");
        }

        [TestMethod]
        public void TenantActivationMonitorTests()
        {
            const string primaryTenantKey = "key1";
            const string secondaryTenantKey = "key2";

            var settingsService = new TestApplicationSettingsService();
            var target = new TenantJobsActivationMonitor(settingsService);
            var registry = new TestTenantRegistry { ConcurrencyLimit = 1, PrimaryTenantKey = primaryTenantKey };
            registry.ConfiguredTenants.Add(primaryTenantKey, "name1");
            registry.ConfiguredTenants.Add(secondaryTenantKey, "name2");
            target.TenantRegistry = registry;

            var getState = new Func<string, TenantJobsActivationMonitor.ActivationInfo>(
                tenantKey => registry.TenantStorage[tenantKey].Values.First().CastTo<TenantState<TenantJobsActivationMonitor.ActivationInfo>>().State);
            var getSetting = new Func<string, ApplicationSettingContainer<TenantJobsActivationMonitor.JobsProcessingInstance>>(
                tenantKey =>
                {
                    using (new SystemUserContext(tenantKey))
                    {
                        return settingsService.GetSetting<TenantJobsActivationMonitor.JobsProcessingInstance>(TenantJobsActivationMonitor.ActiveInstanceSettingKey);
                    }
                });
            var setSetting = new Action<string, ApplicationSettingContainer<TenantJobsActivationMonitor.JobsProcessingInstance>>(
                (tenantKey, settingValue) =>
                {
                    using (new SystemUserContext(tenantKey))
                    {
                        settingsService.SetSetting(settingValue);
                    }
                });

            // Execute job first time and verify
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsFalse(registry.IsTenantActive(primaryTenantKey));
            Assert.IsFalse(registry.IsTenantActive(secondaryTenantKey));
            Assert.AreEqual(2, settingsService.Settings.Count, "Should store activation lock as setting");
            var setting = getSetting(primaryTenantKey);
            var state = getState(primaryTenantKey);
            var settingSecondary = getSetting(secondaryTenantKey);
            var stateSecondary = getState(secondaryTenantKey);
            Assert.IsTrue(!setting.Value.ReadyToShare && state.ReadyToShare != null && !state.ReadyToShare.Value, 
                "Stored lock should be non-sharable while activated");
            Assert.IsTrue(setting.Value.LastConfirmedActivityUtc > DateTime.UtcNow.AddMinutes(-1), "Last confirmed activity must be recent");
            Assert.IsTrue(settingSecondary.Value.ReadyToShare && stateSecondary.ReadyToShare != null && stateSecondary.ReadyToShare.Value, 
                "Secondary tenant lock should be sharable while activating");
            Assert.IsTrue(settingSecondary.Value.LastConfirmedActivityUtc > DateTime.UtcNow.AddMinutes(-1), "Secondary last confirmed activity must be recent");

            // Execute job second time and verify tenant is active now
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(registry.IsTenantActive(primaryTenantKey));
            Assert.IsTrue(registry.IsTenantActive(secondaryTenantKey));
            setting = getSetting(primaryTenantKey);
            settingSecondary = getSetting(secondaryTenantKey);
            Assert.IsFalse(setting.Value.ReadyToShare, "Should still be not ready to share");
            Assert.IsTrue(settingSecondary.Value.ReadyToShare, "Secondary should still be ready to share");

            // Test deactivating
            setting.Value.InstanceId = "takingover";
            setSetting(primaryTenantKey, setting);
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsFalse(registry.IsTenantActive(primaryTenantKey), "Tenant deactivates after take over");
            Assert.IsNull(state.ReadyToShare, "Upon deactivation ReadToShare switches to unknown on state");

            // Expire lock and try locking again by current
            setting = getSetting(primaryTenantKey);
            setting.Value.LastConfirmedActivityUtc = DateTime.UtcNow.AddMinutes(-3);
            setSetting(primaryTenantKey, setting);
            // Also simulate we already in process of activating enough tenants
            Enumerable.Range(100, 100 + registry.ConcurrencyLimit).ForEach(i => registry.ActivatedTenants.Add("key" + i, TenantActivationLockState.Pending));
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            Assert.IsTrue(registry.IsTenantActive(primaryTenantKey), "Lock on primary tenant should be reacquired");
            setting = getSetting(primaryTenantKey);
            Assert.IsTrue(setting.Value.ReadyToShare, "Should lock as sharable, since we have enough activated already");

            // Take over activate lock which is set ready to share
            setting.Value.InstanceId = "openfortakeover";
            setSetting(primaryTenantKey, setting);
            state.ReadyToShare = null;
            registry.ActivatedTenants.Clear();
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            target.CastTo<IJob>().Execute(new TestJobExecutionContext());
            setting = getSetting(primaryTenantKey);
            Assert.IsTrue(registry.IsTenantActive(primaryTenantKey), "Shareable lock must be acquired by current instance");
            Assert.IsFalse(setting.Value.ReadyToShare, "Should lock as non-sharable, since we don't have enough activated instances");
        }

        public class TestJobExecutionContext : IJobExecutionContext
        {
            public TestJobExecutionContext()
            {
                JobDetail = new TestJobDetail();
            }

            public void Put(object key, object objectValue)
            {
                throw new NotImplementedException();
            }

            public object Get(object key)
            {
                throw new NotImplementedException();
            }

            public IScheduler Scheduler { get; private set; }
            public ITrigger Trigger { get; private set; }
            public ICalendar Calendar { get; private set; }
            public bool Recovering { get; private set; }
            public int RefireCount { get; private set; }
            public JobDataMap MergedJobDataMap { get; private set; }
            public IJobDetail JobDetail { get; private set; }
            public IJob JobInstance { get; private set; }
            public DateTimeOffset? FireTimeUtc { get; private set; }
            public DateTimeOffset? ScheduledFireTimeUtc { get; private set; }
            public DateTimeOffset? PreviousFireTimeUtc { get; private set; }
            public DateTimeOffset? NextFireTimeUtc { get; private set; }
            public string FireInstanceId { get; private set; }
            public object Result { get; set; }
            public TimeSpan JobRunTime { get; private set; }

            public class TestJobDetail : IJobDetail
            {
                public TestJobDetail()
                {
                    Key = new JobKey(typeof(TestTenantJobWithState).Name);
                }

                public object Clone()
                {
                    throw new NotImplementedException();
                }

                public JobBuilder GetJobBuilder()
                {
                    throw new NotImplementedException();
                }

                public JobKey Key { get; private set; }
                public string Description { get; private set; }
                public Type JobType { get; private set; }
                public JobDataMap JobDataMap { get; private set; }
                public bool Durable { get; private set; }
                public bool PersistJobDataAfterExecution { get; private set; }
                public bool ConcurrentExecutionDisallowed { get; private set; }
                public bool RequestsRecovery { get; private set; }
            }
        }

        public class TestApplicationSettingsService : IApplicationSettingsService
        {
            private IDictionary<string, ICollection<ApplicationSettingContainer<string>>> _settings;

            public IDictionary<string, ICollection<ApplicationSettingContainer<string>>> Settings
            {
                get { return _settings; }
                private set { _settings = value; }
            }

            public TestApplicationSettingsService()
            {
                Settings = new Dictionary<string, ICollection<ApplicationSettingContainer<string>>>();
            }

            public ICollection<ApplicationSettingContainer<string>> GetSettings(string nameFilter, string machineName = null, int? userId = null)
            {
                return Settings.GetValue(ConfigurationManager.AuthenticationToken, () => null) ?? new Collection<ApplicationSettingContainer<string>>();
            }

            public ICollection<int> SetSettings(ICollection<ApplicationSettingContainer<string>> settings)
            {
                Settings[ConfigurationManager.AuthenticationToken] = settings;
                return new Collection<int>();
            }

            public void DeleteSettings(ICollection<int> settingIds)
            {
                Settings[ConfigurationManager.AuthenticationToken] = null;
            }
        }

        public class TestTenantJobState : IDisposable
        {
            public TestTenantJobState()
            {
                TenantsPassedInto = new HashSet<string>();    
            }

            public HashSet<string> TenantsPassedInto { get; set; }
            public int ExecutionsPerCurrentTenant { get; set; }
            public bool IsDisposed { get; set; }

            public void Dispose()
            {
                IsDisposed = true;
            }
        }

        public class TestTenantJobWithState : TenantJobWithState<TestTenantJobState>
        {
            public ConcurrentStack<Tuple<string, int>> Executions = new ConcurrentStack<Tuple<string, int>>();

            public bool SetSupportsConcurrency {
                set { SupportsConcurrency = value; }
            }

            protected override void Execute(IJobExecutionContext context, TestTenantJobState stateObject)
            {
                Thread.Sleep(100); // To ensure some execution time is always logged
                stateObject.TenantsPassedInto.Add(TenantRegistry.CurrentTenantKey);
                stateObject.ExecutionsPerCurrentTenant++;
                Executions.Push(new Tuple<string, int>(TenantRegistry.CurrentTenantKey, Thread.CurrentThread.ManagedThreadId));
            }

            protected override TestTenantJobState Initialize()
            {
                return new TestTenantJobState();
            }
        }

        public class TestTenantRegistry : ITenantRegistry
        {
            public readonly IDictionary<string, TenantExecutionLog> TenantTotalExecutionTime = new Dictionary<string, TenantExecutionLog>();
            public readonly IDictionary<string, ConcurrentDictionary<string, ITenantStoredValue>> TenantStorage = new Dictionary<string, ConcurrentDictionary<string, ITenantStoredValue>>();

            public int ConcurrencyLimit { get; set; }
            public IDictionary<string, string> ConfiguredTenants { get; set; }
            public Dictionary<string, TenantActivationLockState> ActivatedTenants { get; set; }
            public string PrimaryTenantKey { get; set; }

            public string CurrentTenantKey
            {
                get { return ConfigurationManager.AuthenticationToken; }
            }

            public Func<string, bool> IsTenantAccessibleFunc { get; set; }

            public TestTenantRegistry()
            {
                ConfiguredTenants = new ConcurrentDictionary<string, string>();
                ActivatedTenants = new Dictionary<string,TenantActivationLockState>();
                IsTenantAccessibleFunc = s => true;
            }

            public void SetTenantActivationLockState(string tenantKey, TenantActivationLockState lockState)
            {
                lock (ActivatedTenants)
                {
                    var oldLockState = ActivatedTenants.GetValue(tenantKey, () => TenantActivationLockState.NotActive);
                    if (oldLockState == lockState) return;

                    ActivatedTenants[tenantKey] = lockState;
                }

                if (lockState == TenantActivationLockState.NotActive)
                {
                    ConcurrentDictionary<string, ITenantStoredValue> storage;
                    if (TenantStorage.TryGetValue(tenantKey, out storage))
                    {
                        lock (storage)
                        {
                            foreach (var item in storage
                                .Where(kv => kv.Value.DisposalType == TenantStoredValueDisposalType.UponDeactivate)
                                .ToList())
                            {
                                ITenantStoredValue itemValue;
                                if (storage.TryRemove(item.Key, out itemValue))
                                {
                                    itemValue.Dispose();
                                }
                            }
                        }
                    }
                }
            }

            public bool IsTenantActive(string tenantKey)
            {
                lock (ActivatedTenants)
                {
                    return ActivatedTenants.GetValue(tenantKey,
                    () => TenantActivationLockState.NotActive) == TenantActivationLockState.Active; 
                }
            }

            public int GetActiveTenantsCount(bool includePendingActivation = false)
            {
                lock (ActivatedTenants)
                {
                    return ActivatedTenants.Count;
                }
            }

            public bool IsTenantAccessible(string tenantKey)
            {
                return IsTenantAccessibleFunc(tenantKey);
            }

            public TenantExecutionLog GetTenantExecutionLog(string tenantKey)
            {
                return TenantTotalExecutionTime.GetValue(tenantKey, () => new TenantExecutionLog());
            }

            public event EventHandler<TenantActiveStateChangedArgs> TenantActiveStateChanged;

            protected virtual void OnTenantActiveStateChanged(TenantActiveStateChangedArgs e)
            {
                var handler = TenantActiveStateChanged;
                if (handler != null) handler(this, e);
            }

            public ConcurrentDictionary<string, ITenantStoredValue> GetTenantInMemoryStorage(string tenantKey)
            {
                return TenantStorage.GetValue(tenantKey, () => new ConcurrentDictionary<string, ITenantStoredValue>());
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }
        }

        public class TestTenantStoredValue : ITenantStoredValue 
        {
            public TenantStoredValueDisposalType DisposalType { get; set; }
            public bool IsDisposed { get; set; }

            public void Dispose()
            {
                IsDisposed = true;
            }
        }
    }
}
