﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IO.Practiceware.Tests {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CdaResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CdaResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IO.Practiceware.Tests.CdaResources", typeof(CdaResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Diabetes__qrda {
            get {
                object obj = ResourceManager.GetObject("Diabetes__qrda", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;ClinicalDocument xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
        /// xmlns=&quot;urn:hl7-org:v3&quot;
        /// xmlns:cda=&quot;urn:hl7-org:v3&quot;&gt;
        ///
        ///  &lt;!-- 
        ///    ********************************************************
        ///    CDA Header
        ///    ********************************************************
        ///  --&gt;
        ///  &lt;realmCode code=&quot;US&quot;/&gt;
        ///  &lt;typeId root=&quot;2.16.840.1.113883.1.3&quot; extension=&quot;POCD_HD000040&quot;/&gt;
        ///  &lt;!-- QRDA Category III Release 1 template ID (this template ID differs from QRDA III comment only temp [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Diabetes__qrda3 {
            get {
                return ResourceManager.GetString("Diabetes__qrda3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
        ///&lt;?xml-stylesheet type=&quot;text/xsl&quot; href=&quot;CDA.xsl&quot;?&gt;
        ///
        ///&lt;ClinicalDocument xmlns:xsi = &quot;http://www.w3.org/2001/XMLSchema-instance&quot; xsi:schemaLocation = &quot;urn:hl7-org:v3 file:///C:/MDHT_CDATools/workspace-consol/org.openhealthtools.mdht.uml.cda.consol.model/CDA_Schema_Files/infrastructure/cda/CDA_SDTC.xsd&quot; xmlns = &quot;urn:hl7-org:v3&quot; xmlns:mif = &quot;urn:hl7-org:v3/mif&quot;&gt;
        ///  &lt;realmCode code = &quot;US&quot;/&gt;
        ///  &lt;typeId root = &quot;2.16.840.1.113883.1.3&quot; extension = &quot;POCD_HD000040&quot;/&gt;
        ///  &lt;templat [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string IO_TOC_CDA {
            get {
                return ResourceManager.GetString("IO_TOC_CDA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;?xml-stylesheet type=&quot;text/xsl&quot; href=&quot;CDA.xsl&quot;?&gt;
        ///&lt;ClinicalDocument xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns=&quot;urn:hl7-org:v3&quot;&gt;
        ///  &lt;realmCode code=&quot;US&quot; /&gt;
        ///  &lt;typeId root=&quot;2.16.840.1.113883.1.3&quot; extension=&quot;POCD_HD000040&quot; /&gt;
        ///  &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.1.1&quot; /&gt;
        ///  &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.1.2&quot; /&gt;
        ///  &lt;id root=&quot;0&quot; extension=&quot;03a4d720361a4c03ac2e36b4c5a56525&quot; /&gt;
        ///  &lt;code [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string TransitionOfCareDocument {
            get {
                return ResourceManager.GetString("TransitionOfCareDocument", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;?xml-stylesheet type=&quot;text/xsl&quot; href=&quot;CDA.xsl&quot;?&gt;
        ///&lt;ClinicalDocument xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns=&quot;urn:hl7-org:v3&quot;&gt;
        ///  &lt;realmCode code=&quot;US&quot; /&gt;
        ///  &lt;typeId root=&quot;2.16.840.1.113883.1.3&quot; extension=&quot;POCD_HD000040&quot; /&gt;
        ///  &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.1.1&quot; /&gt;
        ///  &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.1.2&quot; /&gt;
        ///  &lt;id root=&quot;0&quot; extension=&quot;f958724b15fd44d39c2c50015e72c0e4&quot; /&gt;
        ///  &lt;code [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string VisitSummaryDocument {
            get {
                return ResourceManager.GetString("VisitSummaryDocument", resourceCulture);
            }
        }
    }
}
