﻿using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Services.ApplicationSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class ApplicationSettingsTests : TestBase
    {
        [TestMethod]
        public void TestApplicationSettingsInheritance()
        {
            DbConnectionFactory.PracticeRepository.Execute("DELETE FROM model.ApplicationSettings");

            var service = SetupTestApplicationSettingsService();
            var settings = service.GetSettings<string>(null, "TestMachine", UserContext.Current.UserDetails.Id);
           
            // 10 settings
            Assert.AreEqual(10, settings.Count);
            Assert.AreEqual(settings.Count(s => s.Name == "1"), 1);
            Assert.AreEqual(settings.Count(s => s.Name == "2"), 2);
            Assert.AreEqual(settings.Count(s => s.Name == "3"), 3);
            Assert.AreEqual(settings.Count(s => s.Name == "4"), 4);

            var setting1Value = service.GetSetting<string>("1", "TestMachine", UserContext.Current.UserDetails.Id).Value;
            Assert.AreEqual("1", setting1Value);
            var setting2Value = service.GetSetting<string>("2", "TestMachine", UserContext.Current.UserDetails.Id).Value;
            Assert.AreEqual("2", setting2Value);
            var setting3Value = service.GetSetting<string>("3", null, UserContext.Current.UserDetails.Id).Value;
            Assert.AreEqual("3", setting3Value);
            var setting4Value = service.GetSetting<string>("4", "TestMachine", null).Value;
            Assert.AreEqual("2", setting4Value);
        }

        private static IApplicationSettingsService SetupTestApplicationSettingsService()
        {
            var settingsToSave = new List<ApplicationSettingContainer<string>>();

            // 4 settings on global level (Value = 1)
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "1",
                                   Value = "1",
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "2",
                                   Value = "1",
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "3",
                                   Value = "1",
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "4",
                                   Value = "1",
                               });

            // 3 settings on machine level (Value = 2)
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "2",
                                   Value = "2",
                                   MachineName = "TestMachine",
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "3",
                                   Value = "2",
                                   MachineName = "TestMachine",
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "4",
                                   Value = "2",
                                   MachineName = "TestMachine",
                               });

            // 2 settings on user level (Value = 3)
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "3",
                                   Value = "3",
                                   UserId = UserContext.Current.UserDetails.Id,
                               });
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "4",
                                   Value = "3",
                                   UserId = UserContext.Current.UserDetails.Id,
                               });

            // 1 setting on user & machine level (Value = 4)
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "4",
                                   Value = "4",
                                   MachineName = "TestMachine",
                                   UserId = UserContext.Current.UserDetails.Id,
                               });

            // 1 another random global setting (to ensure additional settings are not included)
            settingsToSave.Add(new ApplicationSettingContainer<string>
                               {
                                   Name = "1",
                                   Value = "-1",
                                   MachineName ="TestMachine1"
                               });

            // Save settings
            var service = Common.ServiceProvider.GetService<IApplicationSettingsService>();
            service.SetSettings(settingsToSave);

            return service;
        }
    }
}
