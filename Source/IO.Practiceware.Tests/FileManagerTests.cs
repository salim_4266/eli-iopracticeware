﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.FileService;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using File = System.IO.File;

namespace IO.Practiceware.Tests
{
    /// <summary>
    /// Running tests: if you have "ConfigureWcfServices" set to "true" in Common.cs, ensure IIS's AppPool 
    /// has access to your %TEMP%\IOP. Otherwise you'll get "access denied" exceptions
    /// </summary>
    [TestClass]
    public class FileManagerTests : TestBase
    {
        private string _applicationTestDirectory;
        private FileManager _fileManager;

        protected override void OnAfterTestInitialize()
        {
            base.OnAfterTestInitialize();

            // Prepare Pinpoint folder for tests
            // %TEMP% parts can represent short directory names. Get full path
            var tempFullPath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(@"%TEMP%\IOP"));
            // Note: we expect ServerDataPath to be initialized to some test location (done by Common.cs)
            // If we want to redefine it here, we would need to also redefine it on Web when we run via Application Server
            // Another solution is to FileManager instance with non-remote serviced FileService, but right now FileManager is singleton
            if (!ConfigurationManager.ServerDataPath.StartsWith(tempFullPath, StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("ServerDataPath must be a temp location when running FileManagerTests. Was: " + ConfigurationManager.ServerDataPath);
            }

            var fileManagerConfiguration = new FileManagerConfiguration();
            var fileService = Common.ServiceProvider.GetService<IFileService>();
            _fileManager = new FileManager(fileManagerConfiguration, new FileManagerEngine(fileManagerConfiguration, fileService));
            
            // Configure so that tests can run side by side
            _fileManager.Configuration.IsRedirectionEnabled = true;
            _fileManager.Configuration.RedirectedRootPath = Path.Combine(tempFullPath, @"FileManagerTests\Redirect\" + ApplicationContext.Current.ClientKey);
            _applicationTestDirectory = Path.Combine(tempFullPath, @"FileManagerTests\Local\" + ApplicationContext.Current.ClientKey);
            FileSystem.EnsureDirectory(_applicationTestDirectory);
            _fileManager.Configuration.ApplicationDataPath = _applicationTestDirectory;

            CleanupFolders();
        }

        protected override void OnBeforeTestCleanup()
        {
            base.OnBeforeTestCleanup();

            CleanupFolders();
        }

        private void CleanupFolders()
        {
            if (_fileManager != null)
            {
                _fileManager.ClearCache();
                FileSystem.DeleteDirectory(_applicationTestDirectory);
            }
        }

        [TestMethod]
        [TestRunMode(
            ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresGlobalLock, // Measuring performance
            UseDedicatedTestDatabase = true,
            UseTransactionScope = true)]
        public void TestFileManagerPerformance()
        {
            const int iterations = 100;
            var currentSwitch = FileManager.TraceSource.Switch;
            try
            {
                // Disable file manager logging for performance test
                FileManager.TraceSource.Switch = new SourceSwitch("FileManagerTraceSwitch") { Level = SourceLevels.Off };

                var serverDataPath = ConfigurationManager.ServerDataPath;

                var testDir1Path = Path.Combine(serverDataPath, "TestDir1");
                var testDir2Path = Path.Combine(serverDataPath, "TestDir2");
                var testDir3Path = Path.Combine(FileSystem.GetTempPath(), "TestDir3");

                FileSystem.EnsureDirectory(testDir1Path);
                FileSystem.EnsureDirectory(testDir2Path);
                FileSystem.EnsureDirectory(testDir3Path);

                // Create file manager which uses local file service (we are not measuring over the wire performance)
                var subject = new FileManager(_fileManager.Configuration,
                    new FileManagerEngine(_fileManager.Configuration, new FileService()));

                var start = DateTime.Now;
                
                for (int i = 0; i < iterations; i++)
                {
                    var filePath = Path.Combine(testDir1Path, "TestFile{0}.txt".FormatWith(i));
                    subject.CommitContents(filePath, "My test contents");
                    var localPath = subject.Read(filePath);
                    File.AppendAllText(localPath, "updated");
                    subject.Commit(filePath);
                    subject.Delete(filePath);
                }

                subject.DeleteDirectory(testDir1Path);
                subject.Engine.WaitForIncompleteOperations();
                var localViaFileManager = DateTime.Now - start;

                // Over the wire performance test
                subject = _fileManager;
                start = DateTime.Now;
                for (int i = 0; i < iterations; i++)
                {
                    var filePath = Path.Combine(testDir1Path, "TestFile{0}.txt".FormatWith(i));
                    subject.CommitContents(filePath, "My test contents");
                    var localPath = subject.Read(filePath);
                    File.AppendAllText(localPath, "updated");
                    subject.Commit(filePath);
                    subject.Delete(filePath);
                }

                subject.DeleteDirectory(testDir1Path);
                subject.Engine.WaitForIncompleteOperations();
                var serverViaFileManager = DateTime.Now - start;

                start = DateTime.Now;
                for (int i = 0; i < iterations; i++)
                {
                    var filePath = Path.Combine(testDir2Path, "TestFile{0}.txt".FormatWith(i));
                    var localPath = Path.Combine(testDir3Path, "TestFile{0}.txt".FormatWith(i));

                    File.WriteAllText(localPath, "My test contents");
                    File.Copy(localPath, filePath, true);
                    File.AppendAllText(localPath, "updated");
                    File.Copy(localPath, filePath, true);
                    File.Delete(localPath);
                    File.Delete(filePath);
                }

                FileSystem.DeleteDirectory(testDir2Path);
                FileSystem.DeleteDirectory(testDir3Path);
                var localDirect = DateTime.Now - start;

                var cloudSlowdown = serverViaFileManager.TotalMilliseconds / localDirect.TotalMilliseconds;
                var localSlowdown = localViaFileManager.TotalMilliseconds / localDirect.TotalMilliseconds;
                Assert.IsTrue(localSlowdown <= 3, "FileManager performance on local file system should be not more than 2.5x slower in test scenario. Current: " + localSlowdown);
                Assert.IsTrue(cloudSlowdown <= 17, "FileManager performance using application server should be not more than 17x slower. Current: " + cloudSlowdown);
            }
            finally
            {
                FileManager.TraceSource.Switch = currentSwitch;
            }
        }

        [TestMethod]
        public void TestMovingFilesBetweenLocalAndServer()
        {
            var optimizedFileManager = new LocalFileOptimizedFileManager(_fileManager.Configuration, _fileManager.Engine);

            var localTestFile = FileSystem.GetTempPathName();
            FileSystem.TryWriteFile(localTestFile, DateTime.UtcNow.ToString());

            var serverTestDirectory = Path.Combine(ConfigurationManager.ServerDataPath, "MoveTestDirectory");
            var serverTestFile = Path.Combine(serverTestDirectory, Path.GetFileName(localTestFile));

            // Test copy to server
            optimizedFileManager.Copy(localTestFile, serverTestFile);
            Assert.IsTrue(optimizedFileManager.FileExists(serverTestFile), "File must exist on server after copy");
            optimizedFileManager.Delete(serverTestFile);
            optimizedFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(File.Exists(localTestFile), "File must exist locally after copy");

            // Test move to server
            optimizedFileManager.Move(localTestFile, serverTestFile);
            Assert.IsTrue(optimizedFileManager.FileExists(serverTestFile), "File must exist on server after move");
            optimizedFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsFalse(File.Exists(localTestFile), "Local test file must have been moved to server");

            // Test copy from server to client
            optimizedFileManager.Copy(serverTestFile, localTestFile);
            Assert.IsTrue(File.Exists(localTestFile), "Local test file must exist right after copy");
            FileSystem.TryDeleteFile(localTestFile);
            Assert.IsTrue(optimizedFileManager.FileExists(serverTestFile), "File must still exist on server after copy to local");
            optimizedFileManager.Engine.WaitForIncompleteOperations();

            // Test move from server to client
            optimizedFileManager.Move(serverTestFile, localTestFile);
            Assert.IsTrue(File.Exists(localTestFile), "Local test file must exist right after move");
            Assert.IsFalse(optimizedFileManager.FileExists(serverTestFile), "File must be removed from server after move");
            optimizedFileManager.Engine.WaitForIncompleteOperations();

            // Ensure there is no concurrency issues copying and moving files
            for (int i = 0; i < 7; i++)
            {
                optimizedFileManager.Copy(localTestFile, serverTestFile);
                optimizedFileManager.Delete(localTestFile);
                optimizedFileManager.Move(serverTestFile, localTestFile);
                optimizedFileManager.Delete(serverTestFile);
            }
            optimizedFileManager.Delete(localTestFile);
            optimizedFileManager.Engine.WaitForIncompleteOperations();

            // Cleanup
            FileSystem.TryDeleteDirectory(serverTestDirectory);
            FileSystem.TryDeleteFile(localTestFile, true);

            optimizedFileManager.Engine.WaitForIncompleteOperations();
            optimizedFileManager.ClearCache();
        }

        [TestMethod]
        public void TestDirectWritePermissionDeniedScenario()
        {
            var optimizedFileManager = new LocalFileOptimizedFileManager(_fileManager.Configuration, _fileManager.Engine);
            var testPath = GetFileServiceTestFilePath(0);

            // Ensure performing direct writes doesn't throw Permission Denied due to file being busy
            for (int i = 0; i < 5; i++)
            {
                optimizedFileManager.CommitContents(testPath, i.ToString());
            }

            optimizedFileManager.Engine.WaitForIncompleteOperations();
            optimizedFileManager.ClearCache();
        }

        [TestMethod]
        public void TestRefractionScreenFileNotFoundScenario()
        {
            var optimizedFileManager = new LocalFileOptimizedFileManager(_fileManager.Configuration, _fileManager.Engine);
            var serverBaseDirectory = Path.Combine(ConfigurationManager.ServerDataPath, "DoctorInterface");
            var clientBaseDirectory = Path.Combine(_applicationTestDirectory, "DoctorInterface");
            var clientBackupBaseDirectory = Path.Combine(clientBaseDirectory, "Backup");

            var clientListingFilter = Path.Combine(clientBaseDirectory, "T*_385203_12.txt");
            var serverListingFilter = Path.Combine(serverBaseDirectory, "T*_385203_12.txt");
            
            var serverFilePath = Path.Combine(serverBaseDirectory, "T07S_385203_12.txt");
            var subjectFilePath = Path.Combine(clientBaseDirectory, "T07S_385203_12.txt");
            var backupFilePath = Path.Combine(clientBackupBaseDirectory, @"T07S_385203_12.txt");

            var scratchFilePath = Path.Combine(clientBaseDirectory, "T07S_385203_12.tmp");
            
            Action<string> updateFileContents = f =>
            {
                var directFilePath = optimizedFileManager.Read(f);
                File.WriteAllText(directFilePath, DateTime.Now.ToString());
                optimizedFileManager.Commit(f);
            };

            for (int i = 0; i < 3; i++)
            {
                // Bring down files from server
                var serverFiles = optimizedFileManager.ListFiles(serverListingFilter);

                if (i > 0)
                {
                    Assert.IsTrue(serverFiles.Contains(serverFilePath), "File must exist on server after 1st cycle, since we pushed it there");
                }

                foreach (var serverFile in serverFiles)
                {
                    var destinationPath = Path.Combine(clientBaseDirectory, Path.GetFileName(serverFile));
                    optimizedFileManager.Move(serverFile, destinationPath);
                }

                // Emulate listing and updates (as in VB6)
                optimizedFileManager.ListFiles(clientListingFilter);
                if (optimizedFileManager.FileExists(subjectFilePath))
                {
                    updateFileContents(subjectFilePath);
                }
                
                optimizedFileManager.ListFiles(clientListingFilter);
                updateFileContents(subjectFilePath);

                // Cleanup file before writing scratch file
                optimizedFileManager.Delete(subjectFilePath);

                // Create scratch file with new contents and copy it back as real file
                updateFileContents(scratchFilePath);
                optimizedFileManager.Copy(scratchFilePath, subjectFilePath);
                optimizedFileManager.Delete(scratchFilePath);
                optimizedFileManager.Copy(subjectFilePath, backupFilePath);

                // PushLists in VB6 fails after reading file as it's not on disk
                var directFilePath = optimizedFileManager.Read(subjectFilePath);
                Assert.IsTrue(File.Exists(directFilePath), "File must be found on disk");
                File.WriteAllText(directFilePath, DateTime.Now.ToString());
                optimizedFileManager.Commit(subjectFilePath);

                // 2nd update in PushLists
                updateFileContents(subjectFilePath);

                // Emulate further listing and updates (as in VB6)
                optimizedFileManager.ListFiles(clientListingFilter);
                if (optimizedFileManager.FileExists(subjectFilePath))
                {
                    updateFileContents(subjectFilePath);
                }

                // Send back to server
                var clientFiles = optimizedFileManager.ListFiles(clientListingFilter);
                foreach (var clientFile in clientFiles)
                {
                    var destinationPath = Path.Combine(serverBaseDirectory, Path.GetFileName(clientFile));
                    optimizedFileManager.Copy(clientFile, destinationPath);
                    destinationPath = Path.Combine(clientBackupBaseDirectory, Path.GetFileName(clientFile));
                    optimizedFileManager.Move(clientFile, destinationPath);
                }
            }

            optimizedFileManager.Engine.WaitForIncompleteOperations();
            optimizedFileManager.ClearCache();
        }

        [TestMethod]
        public void TestRead()
        {
            const int fileNumber = 0;
            var testFilePath = GetFileServiceTestFilePath(fileNumber);

            // 1. File does not exist locally, so read from FileService
            // 2. File does not exist on FileService yet, so assert that nothing was read.
            // 3. Create test file on FileService, perform another read.
            // 4. Validate that the file is located on the "Redirected Root Path"
            // 5. Modify file, then read again.  Files hash codes will not match, so read from FileService (and overwrite the modified file)

            var redirectedPath = _fileManager.Read(testFilePath);
            Assert.IsFalse(File.Exists(redirectedPath));

            CreateTestFileOnFileService(fileNumber);
            _fileManager.Read(testFilePath);

            Assert.IsTrue(File.Exists(redirectedPath));
            Assert.IsTrue(File.ReadAllText(redirectedPath).Equals(GetFileServiceTestFileContents(fileNumber)));

            File.WriteAllText(redirectedPath, "new contents");
            _fileManager.Read(testFilePath);
            Assert.IsFalse(File.ReadAllText(redirectedPath).Equals("new contents"));
            Assert.IsTrue(File.ReadAllText(redirectedPath).Equals(GetFileServiceTestFileContents(fileNumber)));
        }

        [TestMethod]
        public void TestCommit()
        {
            // 1. Read file from FileService
            // 2. Modify read file
            // 3. Commit file and assert file on FileService has been updated
            for (int i = 0; i < 15; i++)
            {
                const int fileNumber = 0;
                var testFilePath = CreateTestFileOnFileService(fileNumber);
                var redirectedPath = _fileManager.Read(testFilePath);

                File.WriteAllText(redirectedPath, "new contents");
                _fileManager.Commit(redirectedPath);
                
                _fileManager.Engine.WaitForIncompleteOperations();
                Assert.AreEqual("new contents", File.ReadAllText(testFilePath));
            }
        }

        [TestMethod]
        public void TestExists()
        {
            const int fileNumber = 0;
            var testFilePath = GetFileServiceTestFilePath(fileNumber);
            var redirectedPath = _fileManager.Read(testFilePath);
            Assert.IsFalse(File.Exists(testFilePath));
            Assert.IsFalse(File.Exists(redirectedPath));
            Assert.IsFalse(_fileManager.FileExists(redirectedPath));

            CreateTestFileOnFileService(fileNumber);
            Assert.IsTrue(File.Exists(testFilePath));
            Assert.IsTrue(_fileManager.FileExists(testFilePath));

            // Verify exists checks with excluded files and dirs from redirection
            var excludedFilePath = Path.Combine(_applicationTestDirectory, @"WithExclusions\ExcludedFile1.txt");
            FileSystem.TryWriteFile(excludedFilePath, "test contents");
            Assert.IsFalse(_fileManager.FileExists(excludedFilePath));
            _fileManager.ExcludeFileFromRedirection(excludedFilePath);
            Assert.IsTrue(_fileManager.FileExists(excludedFilePath));

            // Verify contents carries over from existing file when excluded
            var excludedFilePath2 = Path.Combine(_applicationTestDirectory, @"WithExclusions2\ExcludedFile.txt");
            _fileManager.CommitContents(excludedFilePath2, "Test2");
            Assert.IsTrue(_fileManager.FileExists(excludedFilePath2));
            _fileManager.ExcludeFileFromRedirection(excludedFilePath2);
            Assert.IsTrue(_fileManager.FileExists(excludedFilePath2));
            Assert.AreEqual("Test2", _fileManager.ReadContentsAsText(excludedFilePath2));

            var excludedDirectoryPath = Path.Combine(_applicationTestDirectory, @"WithExclusions");
            FileSystem.EnsureDirectory(excludedDirectoryPath);
            Assert.IsFalse(_fileManager.DirectoryExists(excludedDirectoryPath));
            _fileManager.Configuration.ExcludePath(excludedDirectoryPath, true);
            Assert.IsTrue(_fileManager.DirectoryExists(excludedDirectoryPath));

            // Verify delete
            _fileManager.DeleteDirectory(_applicationTestDirectory);
            Assert.IsFalse(Directory.Exists(excludedDirectoryPath), "Delete should also get rid of excluded files/folders");
        }

        [TestMethod]
        public void TestCopy()
        {
            const int fileNumber = 0;
            var testFilePath = CreateTestFileOnFileService(fileNumber);

            // 1. Read file from FileService
            // 2. Copy read file to another local path using FileManager
            // 3. Assert that the file has been copied locally and on the FileService

            var redirectedPath = _fileManager.Read(testFilePath);
            var destinationPath = GetFileServiceTestFilePath(1);
            _fileManager.Copy(redirectedPath, destinationPath);
            
            _fileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(File.Exists(destinationPath));
            Assert.IsTrue(_fileManager.FileExists(destinationPath));
        }

        [TestMethod]
        public void TestDelete()
        {
            // 1. Create file on FileService
            // 2. Assert file exists on FileService
            // 3. Delete file and assert it does not exist

            const int fileNumber = 0;
            var testFilePath = CreateTestFileOnFileService(fileNumber);
            var redirectedPath = _fileManager.Read(testFilePath);
            Assert.IsTrue(_fileManager.FileExists(redirectedPath));

            _fileManager.Delete(redirectedPath);

            _fileManager.Engine.WaitForIncompleteOperations();
            Assert.IsFalse(File.Exists(redirectedPath), "Delete operation should remove locally cached file");
            Assert.IsFalse(_fileManager.FileExists(redirectedPath));
        }

        [TestMethod]
        public void TestMove()
        {
            // 1. Create file on FileService
            // 2. Read file to local
            // 3. Assert file does not exist on destination path
            // 4. Move file using file manager, and assert file is moved on local and on FileService
            // 5. Assert file does not exist at original locations

            const int fileNumber = 0;
            var testFilePath = CreateTestFileOnFileService(fileNumber);
            var redirectedPath = _fileManager.Read(testFilePath);
            var destinationPath = GetFileServiceTestFilePath(1);

            Assert.IsFalse(File.Exists(destinationPath));
            _fileManager.Move(redirectedPath, destinationPath);

            _fileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(File.Exists(destinationPath));
            Assert.IsTrue(_fileManager.FileExists(destinationPath));

            Assert.IsFalse(File.Exists(redirectedPath), "Local file copy should have been moved to different location");
            Assert.IsFalse(_fileManager.FileExists(redirectedPath));
        }

        [TestMethod]
        public void TestGetFiles()
        {
            var filePath = CreateTestFileOnFileService(0);
            for (int i = 1; i < 5; i++)
            {
                CreateTestFileOnFileService(i);
            }
            var directory = Path.GetDirectoryName(filePath);
            if (directory == null) throw new Exception("Could not find directory from {0}".FormatWith(filePath));
            
            // Ensure all files are listed using path expression
            var foundFiles = _fileManager.ListFiles(Path.Combine(ConfigurationManager.ServerDataPath, "*.txt"));
            Assert.AreEqual(5, foundFiles.Count());
            Assert.IsTrue(foundFiles.Contains(filePath));

            // VB6 often uses GetFiles to check whether single file exists
            // Verify we support that
            foundFiles = _fileManager.ListFiles(filePath);
            Assert.AreEqual(filePath, foundFiles.Single(), true);

            // Verify listing from redirected folder with exclusions
            var file1InData = Path.Combine(_fileManager.Configuration.ApplicationDataPath, @"WithExclusions\File1.txt");
            var file2InData = Path.Combine(_fileManager.Configuration.ApplicationDataPath, @"WithExclusions\File2.txt");

            _fileManager.CommitContents(file1InData, "test contents");
            FileSystem.TryWriteFile(file2InData, "test contents2");

            var files = _fileManager.ListFiles(Path.Combine(_applicationTestDirectory, @"WithExclusions\File*.txt"));
            Assert.AreEqual(1, files.Length, "Excluded file should not be listed until added to exclusions");
            _fileManager.Configuration.ExcludePath(file2InData);
            files = _fileManager.ListFiles(Path.Combine(_applicationTestDirectory, @"WithExclusions\File*.txt"));
            Assert.AreEqual(2, files.Length, "Excluded file should be listed once excluded");
        }

        [TestMethod]
        public void TestGetDirectories()
        {
            for (int i = 0; i < 5; i++)
            {
                // Create 5 directories on server data path
                var directory = Path.Combine(ConfigurationManager.ServerDataPath, "TestDirectory{0}".FormatWith(i));
                FileSystem.EnsureDirectory(directory);
            }

            Assert.AreEqual(5, _fileManager.ListDirectories(Path.Combine(ConfigurationManager.ServerDataPath, "*")).Count());

            // Verify listing from redirected folder with exclusions
            var dir1InData = Path.Combine(_fileManager.Configuration.ApplicationDataPath, @"WithExclusions\Dir1");
            var dir2InData = Path.Combine(_fileManager.Configuration.ApplicationDataPath, @"WithExclusions\Dir2");

            _fileManager.CreateDirectory(dir1InData);
            FileSystem.EnsureDirectory(dir2InData);

            var dirs = _fileManager.ListDirectories(Path.Combine(_applicationTestDirectory, @"WithExclusions\Dir*"));
            Assert.AreEqual(1, dirs.Length, "Excluded directory should not be listed until added to exclusions");
            _fileManager.Configuration.ExcludePath(dir2InData, true);
            dirs = _fileManager.ListDirectories(Path.Combine(_applicationTestDirectory, @"WithExclusions\Dir*"));
            Assert.AreEqual(2, dirs.Length, "Excluded directory should be listed once excluded");
        }

        [TestMethod]
        public void TestClear()
        {
            for (int i = 0; i < 5; i++)
            {
                var filePath = CreateTestFileOnFileService(i);
                _fileManager.Read(filePath);
            }

            Assert.AreEqual(Directory.GetFiles(_fileManager.Configuration.RedirectedRootPath, "*", SearchOption.AllDirectories).Length, 5);
            _fileManager.ClearCache();
            Assert.IsFalse(Directory.Exists(_fileManager.Configuration.RedirectedRootPath));
        }

        [TestMethod]
        public void TestPullFromServerScenario()
        {
            // Prepare source file and destination path for the test
            var testSourcePath = CreateTestFileOnFileService(0);
            var testDestinationPath = Path.Combine(_applicationTestDirectory, Path.GetFileName(testSourcePath ?? ""));

            // We'll be moving files back and forth and there should be no issue with that
            for (int i = 0; i < 5; i++)
            {
                _fileManager.Copy(testSourcePath, testDestinationPath);
                _fileManager.Delete(testSourcePath);

                // Swap them
                string tempPathSwitchVariable = testSourcePath;
                testSourcePath = testDestinationPath;
                testDestinationPath = tempPathSwitchVariable;
            }

            // Wait until completes
            _fileManager.Engine.WaitForIncompleteOperations();

            // Check
            Assert.IsTrue(_fileManager.FileExists(testSourcePath));
            Assert.IsFalse(_fileManager.FileExists(testDestinationPath));

            // Cleanup
            _fileManager.Delete(testSourcePath);
            _fileManager.Engine.WaitForIncompleteOperations();
        }

        [TestMethod]
        public void TestPathRedirection()
        {
            var redirectedTestPath = _fileManager.Configuration
                .ToRedirectedPath(Path.Combine(_applicationTestDirectory, "*_234234_234234.txt"));
            Assert.IsTrue(redirectedTestPath.StartsWith(_fileManager.Configuration.RedirectedRootPath),
                "Expected redirected test path '{0}' to start with '{1}'", redirectedTestPath, _fileManager.Configuration.RedirectedRootPath);
            redirectedTestPath = _fileManager.Configuration
                .ToRedirectedPath(Path.Combine(ConfigurationManager.ServerDataPath, "*_234234_234234.txt"));
            Assert.IsTrue(redirectedTestPath.StartsWith(_fileManager.Configuration.RedirectedRootPath),
                "Expected redirected test path '{0}' to start with '{1}'", redirectedTestPath, _fileManager.Configuration.RedirectedRootPath);
            redirectedTestPath = _fileManager.Configuration
                .ToRedirectedPath(Path.Combine(_applicationTestDirectory, "SomeDir"));
            Assert.IsTrue(redirectedTestPath.StartsWith(_fileManager.Configuration.RedirectedRootPath),
                "Expected redirected test path '{0}' to start with '{1}'", redirectedTestPath, _fileManager.Configuration.RedirectedRootPath);
            redirectedTestPath = _fileManager.Configuration
                .ToRedirectedPath(Path.Combine(_applicationTestDirectory, "SomeDir\\"));
            Assert.IsTrue(redirectedTestPath.StartsWith(_fileManager.Configuration.RedirectedRootPath),
                "Expected redirected test path '{0}' to start with '{1}'", redirectedTestPath, _fileManager.Configuration.RedirectedRootPath);
            redirectedTestPath = _fileManager.Configuration
                .ToRedirectedPath(Path.Combine(_applicationTestDirectory, "SomeDir\\?_*32423_23432.txt"));
            Assert.IsTrue(redirectedTestPath.StartsWith(_fileManager.Configuration.RedirectedRootPath),
                "Expected redirected test path '{0}' to start with '{1}'", redirectedTestPath, _fileManager.Configuration.RedirectedRootPath);
        }

        [TestMethod]
        public void TestSampleScenario()
        {
            // Check if file exists at path.  If so delete it.
            // Create new file at path.  
            // Write to file
            // Copy new file to destination path.

            var path = CreateTestFileOnFileService(0);
            if (_fileManager.FileExists(path))
            {
                _fileManager.Delete(path);
            }

            _fileManager.Engine.WaitForIncompleteOperations();
            Assert.IsFalse(_fileManager.FileExists(path));

            var redirectedPath = _fileManager.Read(path);
            File.WriteAllText(redirectedPath, "test scenario 1");
            _fileManager.Commit(redirectedPath);

            _fileManager.Engine.WaitForIncompleteOperations();
            var destination = GetFileServiceTestFilePath(1);

            _fileManager.Copy(redirectedPath, destination);

            _fileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(_fileManager.FileExists(destination));
            Assert.IsTrue(File.ReadAllBytes(destination).ComputeMd5Hash().SequenceEqual(File.ReadAllBytes(path).ComputeMd5Hash()));
        }

        [TestMethod]
        public void TestVbOptimizedScenario()
        {
            // Create file manager using engine which relies on test file service
            var testFileService = Common.ServiceProvider.GetService<TestFileService>();
            var testFileManager = new LocalFileOptimizedFileManager(_fileManager.Configuration, 
                new FileManagerEngine(_fileManager.Configuration, testFileService));

            // Init local paths for test
            var rootFile1Local = GetLocalPinpointFolderFilePath(@"file1_test.txt", testFileManager);
            var diFile1Local = GetLocalPinpointFolderFilePath(@"DoctorInterface\file1_test.txt", testFileManager);
            var diFile2Local = GetLocalPinpointFolderFilePath(@"DoctorInterface\file2_test.txt", testFileManager);
            var diBackupFile1Local = GetLocalPinpointFolderFilePath(@"DoctorInterface\Backup\file1_test.txt", testFileManager);

            // Init server paths and save files there
            var rootFile1Server = GetDirectServerPath(rootFile1Local, testFileManager);
            FileSystem.TryWriteFile(rootFile1Server, rootFile1Local);
            var diFile1Server = GetDirectServerPath(diFile1Local, testFileManager);
            FileSystem.TryWriteFile(diFile1Server, diFile1Local);
            var diFile2Server = GetDirectServerPath(diFile2Local, testFileManager);
            FileSystem.TryWriteFile(diFile2Server, diFile2Local);
            var diBackupFile1Server = GetDirectServerPath(diBackupFile1Local, testFileManager);
            FileSystem.TryWriteFile(diBackupFile1Server, diBackupFile1Local);

            // Optimized file manager no longer caches all local files upon first operation
            // so cache it here for the test
            Assert.AreEqual(1, testFileManager.ListFiles(GetLocalPinpointFolderFilePath(string.Empty, testFileManager)).Length);
            Assert.AreEqual(2, testFileManager.ListFiles(GetLocalPinpointFolderFilePath(@"DoctorInterface\*_test.txt", testFileManager)).Length);
            Assert.AreEqual(1, testFileManager.ListFiles(GetLocalPinpointFolderFilePath(@"DoctorInterface\Backup\*", testFileManager)).Length);

            // Exists (cached)
            Assert.IsTrue(testFileManager.FileExists(rootFile1Local), "Root file 1 not found");
            Assert.IsTrue(testFileManager.FileExists(diFile1Local), "DI file 1 not found");
            Assert.IsTrue(testFileManager.FileExists(diBackupFile1Local), "DI backup file 1 not found");

            // Directory exists for folder with files goes cached
            Assert.IsTrue(testFileManager.DirectoryExists(GetLocalPinpointFolderFilePath(@"DoctorInterface", testFileManager)));

            // First read goes to the server
            testFileService.IsReadExpected = true;
            string diBackupFile1Redirected = testFileManager.Read(diBackupFile1Local);
            Assert.IsTrue(File.Exists(diBackupFile1Redirected), "Not read backup file 1");
            testFileService.IsReadExpected = false;

            // Next read is cached
            var localContentsOfBackupFile1 = FileSystem.TryReadAllText(diBackupFile1Redirected);
            FileSystem.TryWriteFile(diBackupFile1Server, "Modified backup file on server, but client ignores");
            diBackupFile1Redirected = testFileManager.Read(diBackupFile1Local);
            Assert.AreEqual(localContentsOfBackupFile1, FileSystem.TryReadAllText(diBackupFile1Redirected));

            // Non-modified file write is ignored
            testFileManager.Commit(diBackupFile1Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.AreNotEqual(localContentsOfBackupFile1, FileSystem.TryReadAllText(diBackupFile1Server));

            // Modified file write is pushed
            localContentsOfBackupFile1 += "... Modified";
            FileSystem.TryWriteFile(diBackupFile1Redirected, localContentsOfBackupFile1);
            testFileService.IsWriteExpected = true;
            testFileManager.Commit(diBackupFile1Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.AreEqual(localContentsOfBackupFile1, FileSystem.TryReadAllText(diBackupFile1Server));
            testFileService.IsWriteExpected = false;

            // Cache file locally for copy test
            testFileService.IsReadExpected = true;
            testFileManager.Read(diFile2Local);
            testFileService.IsReadExpected = false;

            // Copied files are properly handled
            var diBackupFile2Local = GetLocalPinpointFolderFilePath(@"DoctorInterface\Backup\file2_test.txt", testFileManager);
            var diBackupFile2Server = GetDirectServerPath(diBackupFile2Local, testFileManager);
            testFileManager.Copy(diFile2Local, diBackupFile2Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(testFileManager.FileExists(diBackupFile2Local), "Copied to backup file exist check");
            Assert.IsTrue(File.Exists(diBackupFile2Server), "Copied to backup file must exist");

            // Copied files reads and writes do not travel to the server until modified
            var diBackupFile2Redirected = testFileManager.Read(diBackupFile2Local);
            testFileManager.Commit(diBackupFile2Local);
            testFileService.IsWriteExpected = true;
            string diBackupFile2Contents = FileSystem.TryReadAllText(diBackupFile2Redirected) + "...Modified";
            FileSystem.TryWriteFile(diBackupFile2Redirected, diBackupFile2Contents);
            testFileManager.Commit(diBackupFile2Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.AreEqual(diBackupFile2Contents, FileSystem.TryReadAllText(diBackupFile2Server));
            testFileService.IsWriteExpected = false;

            // Cache information of source and destination of copy are not tight together
            testFileManager.Commit(diFile2Local); // FileService.Write will not be called

            // Delete operation check
            testFileManager.Delete(diBackupFile2Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsFalse(testFileManager.FileExists(diBackupFile2Local), "Deleted file should not exist");
            Assert.IsFalse(File.Exists(diBackupFile2Server), "Deleted file should not exist on server");

            // Move checks
            testFileManager.Move(diFile2Local, diBackupFile2Local);
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsFalse(testFileManager.FileExists(diFile2Local), "Moved file source should not exist");
            Assert.IsTrue(testFileManager.FileExists(diBackupFile2Local), "Moved file destination should exist");

            // Delete folder should be properly handled
            testFileManager.DeleteDirectory(Path.GetDirectoryName(diFile1Local));
            testFileManager.Engine.WaitForIncompleteOperations();
            Assert.IsTrue(testFileManager.FileExists(rootFile1Local), "File outside of delete folder should remain");
            Assert.IsFalse(testFileManager.FileExists(diFile1Local), "File from DI folder shouldn't exist after delete");
            Assert.IsFalse(testFileManager.FileExists(diBackupFile1Local), "File from Backup folder shouldn't exist after delete");

            // Shared instance, so clear cache
            testFileManager.ClearCache();
        }

        private static string GetDirectServerPath(string localPath, IFileManager fileManager)
        {
            var serverPath = Path.Combine(ConfigurationManager.ServerDataPath,
                fileManager.Configuration.ToFileServiceRelativePath(localPath).TrimStart(@"\"));
            FileSystem.EnsureFilePath(serverPath);
            return serverPath;
        }

        private static string GetLocalPinpointFolderFilePath(string relativePath, IFileManager fileManager)
        {
            var localPath = Path.Combine(fileManager.Configuration.ApplicationDataPath, relativePath);
            FileSystem.EnsureFilePath(localPath);
            return localPath;
        }

        private static string GetFileServiceTestFilePath(int fileNumber)
        {
            var fileName = "FileServiceTestFile{0}.txt".FormatWith(fileNumber);
            return Path.Combine(ConfigurationManager.ServerDataPath, fileName);
        }

        private static string GetFileServiceTestFileContents(int fileNumber)
        {
            return "test contents for file {0}".FormatWith(fileNumber);
        }

        private static string CreateTestFileOnFileService(int fileNumber)
        {
            var filePath = GetFileServiceTestFilePath(fileNumber);
            FileSystem.EnsureFilePath(filePath);
            File.WriteAllText(filePath, GetFileServiceTestFileContents(fileNumber));

            return filePath;
        }

        /// <summary>
        /// Test file service which throws exceptions when operations are not expected
        /// </summary>
        public class TestFileService : IFileService
        {
            private readonly IFileService _realFileService;

            public TestFileService(IFileService realFileService)
            {
                _realFileService = realFileService;
            }

            /// <summary>
            /// GetFiles is expected to be called only upon very first call to FileManagerForVb (when it caches server contents)
            /// </summary>
            private int _allocatedGetFilesCalls = 3;

            public bool IsWriteExpected { get; set; }
            public bool IsReadExpected { get; set; }

            // Allowed only when expected

            public bool FileExists(string path)
            {
                throw new InvalidOperationException("FileService was not expected to be called during this phase of optimized FileManager for VB test");
                /*
                return _realFileService.FileExists(path);
                */
            }

            public bool DirectoryExists(string path)
            {
                throw new InvalidOperationException("FileService was not expected to be called during this phase of optimized FileManager for VB test");
                /*
                return _realFileService.DirectoryExists(path);
                */
            }

            public void Write(WriteFileArguments arguments)
            {
                if (!IsWriteExpected)
                    throw new InvalidOperationException("FileService was not expected to be called during this phase of optimized FileManager for VB test");

                _realFileService.Write(arguments);
            }

            public ReadFileResult Read(ReadFileArguments arguments)
            {
                if (!IsReadExpected)
                    throw new InvalidOperationException("FileService was not expected to be called during this phase of optimized FileManager for VB test");

                return _realFileService.Read(arguments);
            }

            public string[] GetFiles(string path, bool allDirectories)
            {
                if (_allocatedGetFilesCalls-- == 0)
                    throw new InvalidOperationException("FileService was not expected to be called during this phase of optimized FileManager for VB test");

                return _realFileService.GetFiles(path, allDirectories);
            }

            // Always executed

            public void Copy(string sourcePath, string destinationPath)
            {
                _realFileService.Copy(sourcePath, destinationPath);
            }

            public void Move(string sourcePath, string destinationPath)
            {
                _realFileService.Move(sourcePath, destinationPath);
            }

            public void Delete(string path)
            {
                _realFileService.Delete(path);
            }

            public void CreateDirectory(string path)
            {
                _realFileService.CreateDirectory(path);
            }

            public void DeleteDirectory(string directoryPath)
            {
                _realFileService.DeleteDirectory(directoryPath);
            }

            public string[] GetDirectories(string path, bool allDirectories)
            {
                return _realFileService.GetDirectories(path, allDirectories);
            }

            public FileInfoRecord[] GetFileInfos(string[] filePaths)
            {
                return _realFileService.GetFileInfos(filePaths);
            }
        }
    }
}
