﻿using System.Data.SqlClient;
using System.Diagnostics;
using Soaf;
using Soaf.Data;
using Soaf.Logging;

namespace IO.Practiceware.Tests
{
    public class X12MetadataTestsBase : TestBase
    {
        protected override TestDatabasePoolName DatabasePoolName
        {
            get { return TestDatabasePoolName.X12Tests; }
        }

        protected override void InitializeTestDatabase()
        {
            using (new TimedScope(s => Trace.TraceInformation("Ran X12 metadata script in {0}.".FormatWith(s))))
            {
                using (var c = new SqlConnection(Configuration.ConfigurationManager.PracticeRepositoryConnectionString))
                {
                    // Ran test database initialization outside of transaction
                    c.RunScript(X12Resources.SetupICD9TableForMappings, false);
                    c.RunScript(X12Resources.X12Metadata, false);
                }
            }
        }

    }
}
