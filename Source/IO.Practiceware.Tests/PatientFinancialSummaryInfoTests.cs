﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Services.Documents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientFinancialSummary;
using Soaf;
using Soaf.Linq;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class PatientFinancialSummaryInfoTests : X12MetadataTestsBase
    {
        PatientFinancialSummaryViewService PatientFinancialSummaryViewService { get; set; }
        PaperClaimsMessageProducer Producer { get; set; }
        PaperClaimGenerationUtilities PaperClaimGenerationUtilities { get; set; }
        public PatientFinancialSummaryInfoTests()
        {
            PatientFinancialSummaryViewService = Common.ServiceProvider.GetService<PatientFinancialSummaryViewService>();
            Producer = Common.ServiceProvider.GetService<PaperClaimsMessageProducer>();
            PaperClaimGenerationUtilities = Common.ServiceProvider.GetService<PaperClaimGenerationUtilities>();
        }
        [TestMethod]
        public void TestFinancialSummaryMapperTotals()
        {
            var patientAccountViewModel = PatientFinancialSummaryViewService.LoadPatientFinancialSummaryInformation(1, new FilterSelectionViewModel { Id = 1, Name = "All Invoices", Color = new SolidColorBrush(Color.FromRgb(31, 132, 181)) }, new List<int>());
            Assert.AreEqual(patientAccountViewModel.Invoices.Count, 3);
            Assert.AreEqual(patientAccountViewModel.AccountBalance, (Decimal)8374.75);
            Assert.AreEqual(patientAccountViewModel.InsurerBalance, 4185);
            Assert.AreEqual(patientAccountViewModel.PatientBalance, 0);
            Assert.AreEqual(patientAccountViewModel.UnbilledTotal, (Decimal)4189.75);
        }

        [TestMethod]
        public void TestFinancialSummaryMessageProduction()
        {
            var invoice = PracticeRepository.Invoices.Single(inv => inv.Id == 1);
            PracticeRepository.AsQueryableFactory().Load(invoice,
                inv => inv.InvoiceReceivables.Select(ir => new
                {
                    ir.PatientInsurance.InsurancePolicy.Insurer,
                    BST = ir.BillingServiceTransactions.Select(bst => new
                    {
                        bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer
                    })
                })
                );
            var paperClaimGenerationInfoViewModels = PaperClaimGenerationUtilities.GetPaperClaimInformation(invoice);
            Assert.AreEqual(paperClaimGenerationInfoViewModels.Count, 1);
            var paperClaimGenerationInfoViewModel = paperClaimGenerationInfoViewModels[0];
            paperClaimGenerationInfoViewModel = PaperClaimGenerationUtilities.GetPaperClaimGenerationInfo(paperClaimGenerationInfoViewModel);
            Producer.IsTesting = false;
            var htmlContents = (HtmlDocument)Producer.ProduceMessage(paperClaimGenerationInfoViewModel.BillingServiceTransactionIds, paperClaimGenerationInfoViewModel.ClaimFormTypeId, true);
            Assert.AreEqual(htmlContents.Content.Count, 2);
        }

        [TestMethod]
        public void TestLoadFullInvoice()
        {
            var patientAccountViewModel = PatientFinancialSummaryViewService.LoadPatientFinancialSummaryInformation(1, new FilterSelectionViewModel { Id = 1, Name = "All Invoices", Color = new SolidColorBrush(Color.FromRgb(31, 132, 181)) }, new List<int>());
            var invoice = patientAccountViewModel.Invoices.Count > 0 ? patientAccountViewModel.Invoices.First() : null;
            Assert.IsNotNull(invoice);
            var fullInvoice = PatientFinancialSummaryViewService.LoadFullInvoiceViewModel(invoice);
            Assert.AreEqual(fullInvoice.Diagnoses.Count, 5);
            var service = fullInvoice.EncounterServices.First();
            Assert.AreEqual(service.Transactions.Count, 1);
            Assert.AreEqual(service.Modifiers.Count, 1);
            Assert.AreEqual(service.LinkedDiagnoses.Count, 1);

        }

        [TestMethod]
        public void TestLoadFullInvoiceWithInitialLoad()
        {
            var patientAccountViewModel = PatientFinancialSummaryViewService.LoadPatientFinancialSummaryInformation(1, new FilterSelectionViewModel { Id = 1, Name = "All Invoices", Color = new SolidColorBrush(Color.FromRgb(31, 132, 181)) }, new List<int> { 1 });
            var invoice = patientAccountViewModel.Invoices.Count > 0 ? patientAccountViewModel.Invoices.First() : null;
            Assert.IsNotNull(invoice);
            Assert.AreEqual(invoice.Diagnoses.Count, 5);
            var service = invoice.EncounterServices.First();
            Assert.AreEqual(service.Transactions.Count, 1);
            Assert.AreEqual(service.Modifiers.Count, 1);
            Assert.AreEqual(service.LinkedDiagnoses.Count, 1);

        }
    }
}
