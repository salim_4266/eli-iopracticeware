﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using IO.Practiceware.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using IO.Practiceware.SqlCmd;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class SqlCmdTest : TestBase
    {
        private const string LoginPin = "9911";
        private const string Query = "SELECT * FROM model.AxisQualifier_Enumeration";
        [TestMethod]
        public void TestCommandLineParser()
        {
            string[] args = { "-p", LoginPin, "-q", Query };
            var options = new Options();
            CommandLine.Parser.Default.ParseArguments(args, options);
            Assert.AreEqual(options.Pin, args[1]);
            Assert.AreEqual(options.Query, args[3]);
        }

        [TestMethod]
        public void TestOptionsAreValid()
        {
            var options = new Options();
            options.Pin = LoginPin;
            options.Query = Query;
            var context = new ValidationContext(options, null, null);
            var results = new List<ValidationResult>();
            Assert.IsTrue(Validator.TryValidateObject(options, context, results));
        }

        [Ignore]
        [TestMethod]
        public void TestConnectionAndQueryResults()
        {
            string outputFile = String.Format("C:\\Users\\{0}\\AppData\\Local\\IO Practiceware\\Output.txt", Environment.UserName);
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                using (var output = new Output(outputFile, true, ","))
                {
                    var dt = connection.Execute<DataTable>(Query);
                    output.PrintResults(dt);
                }
            }
            using (var reader = new StreamReader(outputFile))
            {
                int i = 0;
                string line;
                while (!string.IsNullOrEmpty(line = reader.ReadLine()) )
                {
                    Assert.AreEqual(line, GetValue(i));
                    i++;
                }
            }
        }

        [Ignore]
        [TestMethod]
        public void TestConnectionAndQueryResultsWithFixedColumnLength()
        {
            const int fixedColumnLength = 20;
            const string delimiter = "";
            string outputFile = String.Format("C:\\Users\\{0}\\AppData\\Local\\IO Practiceware\\OutputWithFixedColumnLength.txt", Environment.UserName);
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                using (var output = new Output(outputFile, true, delimiter, false, fixedColumnLength))
                {
                    var dt = connection.Execute<DataTable>(Query);
                    output.PrintResults(dt);
                }
            }
            using (var reader = new StreamReader(outputFile))
            {
                int i = 0;
                string line;
                while (!string.IsNullOrEmpty(line = reader.ReadLine()))
                {
                    Assert.AreEqual(line, GetValueWithFixedLength(i));
                    i++;
                }
            }
        }

        public string GetValue(int i)
        {
            switch (i)
            {
                case 0:
                    return "Id,Name";
                case 1:
                    return "1, ConfirmationOfNegativeFindings";
                case 2:
                    return "2, RuleOut";
                case 3:
                    return "3, HistoryOf";
                case 4:
                    return "4, StatusPost";
            }
            return null;
        }

        public string GetValueWithFixedLength(int i)
        {
            switch (i)
            {
                case 0:
                    return "Id                  Name";
                case 1:
                    return "1                   ConfirmationOfNegati";
                case 2:
                    return "2                   RuleOut";
                case 3:
                    return "3                   HistoryOf";
                case 4:
                    return "4                   StatusPost";
            }
            return null;
        }

    }
}
