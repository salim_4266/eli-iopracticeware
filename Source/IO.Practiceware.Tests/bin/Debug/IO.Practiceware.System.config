﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <!-- IO Sections -->
    <section name="electronicClinicalQualityMeasures" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Integration.Cda.ElectronicClinicalQualityMeasuresConfiguration, IO.Practiceware.Core]], Soaf" />
    <section name="dbMigrations" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.DbMigration.DbMigrationsConfiguration, IO.Practiceware.DbMigration]], Soaf" />

    <!-- Application Server and Client -->
    <section name="applicationServer" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Configuration.ApplicationServerConfiguration, IO.Practiceware.Core]], Soaf" />
    <section name="applicationServerClient" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Configuration.ApplicationServerClientConfiguration, IO.Practiceware.Core]], Soaf" />

    <!-- Monitoring Service -->
    <section name="monitoringServiceConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Common.MonitoringServiceConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="updateMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.UpdateMonitor.UpdateMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="newCropUpdateMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.UpdateMonitor.UpdateMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="newCropMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.NewCropMonitor.NewCropMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="messageProcessorMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Integration.MessageProcessorMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="syncServerConfigsMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Maintenance.StartProcessMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="cleanupIisLogsMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Maintenance.StartProcessMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="cacheCredentialsMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Maintenance.StartProcessMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    <section name="collectMetricsMonitorConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Monitoring.Service.Maintenance.StartProcessMonitorConfiguration, IO.Practiceware.Monitoring.Service]], Soaf" />
    
    <!-- Entity Framework -->
    <section name="entityFrameworkConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[Soaf.EntityFramework.EntityFrameworkConfiguration, Soaf]], Soaf" />

    <!-- Settings -->
    <section name="systemAppSettings" type="Soaf.Configuration.XmlConfigurationSection`1[[Soaf.Configuration.KeyValueConfigurationCollection, Soaf]], Soaf" />
    <section name="customAppSettings" type="IO.Practiceware.Configuration.ClientContextConfigurationSection`1[[Soaf.Configuration.KeyValueConfigurationCollection, Soaf]], IO.Practiceware.Core" />
    <section name="customConnectionStrings" type="IO.Practiceware.Configuration.ClientContextConfigurationSection`1[[Soaf.Configuration.ConnectionStringConfigurationCollection, Soaf]], IO.Practiceware.Core" />

    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
  </configSections>

  <dbMigrations source="IO.Practiceware.Custom.config" xpath="/configuration/dbMigrations" />

  <entityFrameworkConfiguration>
    <connection repositoryTypeName="IO.Practiceware.Model.Legacy.ILegacyPracticeRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository" metadata="res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.csdl|res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.ssdl|res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.msl" lazyLoadingEnabled="false" proxyCreationEnabled="false" />
    <connection repositoryTypeName="IO.Practiceware.Model.IPracticeRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository"  lazyLoadingEnabled="false" proxyCreationEnabled="false" metadata="res://IO.Practiceware.Model/PracticeRepositoryModel.csdl|res://IO.Practiceware.Model/PracticeRepositoryModel.ssdl|res://IO.Practiceware.Model/PracticeRepositoryModel.msl" cacheTypeName="Edm_EntityMappingGeneratedViews.ViewsForBaseEntitySetsOfIOPracticewareModelPracticeRepository" />
    <connection repositoryTypeName="IO.Practiceware.Model.Auditing.IAuditRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository"  lazyLoadingEnabled="false" proxyCreationEnabled="false" metadata="res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.csdl|res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.ssdl|res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.msl" />
    <connection repositoryTypeName="IO.Practiceware.Integration.MVE.Model.IMVERepository, IO.Practiceware.Integration.MVE" connectionStringName="MVERepository" metadata="res://IO.Practiceware.Integration.MVE/Model.MVERepositoryModel.csdl|res://IO.Practiceware.Integration.MVE/Model.MVERepositoryModel.ssdl|res://IO.Practiceware.Integration.MVE/Model.MVERepositoryModel.msl" lazyLoadingEnabled="false" proxyCreationEnabled="false" />
  </entityFrameworkConfiguration>

  <systemAppSettings>
    <add key="HelpServerUrl" value="http://help.iopracticeware.com/" />
    <add key="SupportSiteUrl" value ="https://support.iopracticeware.com/"/>
    <add key="ArchiveDataMonitorAgeThresholdInDays" value="450"/>
    <add key="LOGENTRIES_TOKEN" value="c8d9d819-92cf-4420-b39b-a62384bdbb9e" />
  </systemAppSettings>

  <customAppSettings source="IO.Practiceware.Custom.config" xpath="/configuration/appSettings" />

  <applicationServer source="IO.Practiceware.Custom.config" xpath="/configuration/applicationServer" />

  <applicationServerClient source="IO.Practiceware.Custom.config"
    xpath="/configuration/applicationServerClient" />

  <customConnectionStrings source="IO.Practiceware.Custom.config"
    xpath="/configuration/connectionStrings" />

  <nlog>
    <targets>
      <target name="logentries" type="Logentries" debug="false" httpPut="false" ssl="false"
      layout="${date:format=ddd MM-dd-yyy} ${time:format=HH:mm:ss} ${date:format=zzz} | LoggerSource = ${logger} | Level = ${LEVEL} | ${message}"/>
    </targets>
    <rules>
      <logger name="*" minLevel="Warn" appendTo="logentries"/>
    </rules>
  </nlog>

  <connectionStrings>
    <add name="ExternalData" connectionString="Data Source=publicdata.iopracticeware.com,50950;Initial Catalog=ExternalData;User ID=iopublic;Password=c6S3mcWAGHbctay;Connect Timeout=60;Encrypt=True;TrustServerCertificate=True" />
  </connectionStrings>

  <monitoringServiceConfiguration maximumMemoryInKb="1048576" tenantsConcurrencyLimit="4">
    <schedules>
      <add name="ReplicationMonitor" minutes="720" />
      <add name="MemoryMonitor" minutes="10" />
      <add name="UpdateMonitor" minutes="5" />
      <add name="FileMessageConnectorMonitor" minutes=".2"/>
      <add name="WebServiceMessageConnectorMonitor" minutes=".5" />
      <add name="SocketsMessageConnectorMonitor" minutes=".5" />
      <add name="MessageProcessorMonitor" minutes="1"/>
      <add name="TenantJobsActivationMonitor" minutes=".34" />
      <!--<add name="SyncServerConfigsMonitor" typeName="StartProcessMonitor" minutes="1" />
	  <add name="CollectMetricsMonitor" typeName="StartProcessMonitor" minutes="1" />
      <add name="CacheCredentialsMonitor" typeName="StartProcessMonitor" minutes="480" />-->

      <!-- All times must be adjusted to UTC(!) -->
      <!-- Execution slot:
        - start = 21:45 EST (02:45 UTC)
        - final job start = 03:00 EST (08:00 UTC)
        - maintenance end = 06:00 EST (11:00 UTC) -->
      <!--<add name="CleanupIisLogsMonitor" typeName="StartProcessMonitor" time="02:45" />-->
      <add name="ArchiveDataMonitor" time="03:00"/>
      <!--<add name="NewCropMonitor" time="04:00" />-->
      <add name="DynamicFormsAndDiagnosisMasterMonitor" time="04:30"/>
      <add name="PatientDiagnosisDetailsMonitor" time="06:00"/>
      <add name="CcdaMessageProcessorMonitor" time="06:15" endTime="11:00" minutes="45" />
      <add name="RxNormAllergenMapperMonitor" time="07:00" />
      <add name="DatabaseBackupMonitor" time="07:45"/>
      <add name="NewCropUpdateMonitor" typeName="UpdateMonitor" time="08:00"/>
      <!--<add name="VestrumIntegrationMonitor" time="12:00" daysOfWeek="Saturday"/>-->
    </schedules>
  </monitoringServiceConfiguration>

  <electronicClinicalQualityMeasures>
    <category3ReportingService serviceUri="https://cqm.iopracticeware.com" login="cqm" password="GMw4wsg8gV" />
  </electronicClinicalQualityMeasures>

  <updateMonitorConfiguration source="IO.Practiceware.Custom.config" xpath="/configuration/updateMonitorConfiguration" />

  <newCropUpdateMonitorConfiguration>
    <updateClients>
      <updateClient userName="NewCrop" password="H)=eY9mI" ftpAddress="ftps://ftp.iopracticeware.com:990/" localPath="%TEMP%\IOP\Temp\NewCrop" uploadLogs="false" />
    </updateClients>
  </newCropUpdateMonitorConfiguration>

  <newCropMonitorConfiguration
    endpointAddress="https://secure.newcropaccounts.com/V7/webservices/Update1.asmx"
    partnerName="IOPracticeware" name="652c5f33-653d-40ca-8d41-ec5c343ca984" password="fae51cca-fe66-4b41-ae61-f10db8c65ffa"
    accountId="1" siteId="1"
    ftpPath="ftps://ftp.iopracticeware.com:990/" ftpUserName="NewCrop" ftpPassword="H)=eY9mI" >
  </newCropMonitorConfiguration>

  <messageProcessorMonitorConfiguration>
    <excludeMessageTypes>
      <messageType>EncounterClinicalSummaryCCDA_Outbound</messageType>
      <messageType>PatientTransitionOfCareCCDA_Outbound</messageType>
    </excludeMessageTypes>
  </messageProcessorMonitorConfiguration>

  <!--<syncServerConfigsMonitorConfiguration fileName="C:\CopyConfigFiles.bat" arguments="?ioEnvName?" waitForExit="false" />
  <cleanupIisLogsMonitorConfiguration fileName="C:\DeleteIISLogFiles.bat" waitForExit="false" />
  <collectMetricsMonitorConfiguration fileName="C:\CollectMetrics.cmd" waitForExit="false" />
  <cacheCredentialsMonitorConfiguration fileName="C:\CacheNetworkCredentials.bat" waitForExit="false" />-->

  <system.web>
    <membership defaultProvider="MembershipProvider">
      <providers>
        <clear />
        <add name="MembershipProvider" type="Soaf.Security.MembershipProvider, Soaf.Extensions" />
      </providers>
    </membership>

    <roleManager enabled="true" defaultProvider="RoleProvider">
      <providers>
        <clear />
        <add name="RoleProvider" type="Soaf.Security.RoleProvider, Soaf.Extensions"/>
      </providers>
    </roleManager>
  </system.web>

  <system.serviceModel>
    <!-- The empty client section be present because endpoints are added dynamically to it 
    and those endpoints must have the contextual information from this config file. -->
    <client></client>
    <bindings>
      <customBinding>
        <binding name="WsHttpBinding" openTimeout="00:02:00" receiveTimeout="00:20:00" sendTimeout="00:20:00">
          <transactionFlow transactionProtocol="WSAtomicTransactionOctober2004" />
          <textMessageEncoding maxReadPoolSize="2147483647" maxWritePoolSize="2147483647">
            <readerQuotas maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" maxNameTableCharCount="2147483647" maxStringContentLength="2147483647" />
          </textMessageEncoding>
          <httpsTransport maxReceivedMessageSize="2147483647" maxBufferPoolSize="2147483647" allowCookies="false" maxBufferSize="2147483647" />
        </binding>
      </customBinding>
    </bindings>
    <behaviors>
      <endpointBehaviors>
        <behavior name="ProtoEndpointBehavior">
          <protobuf/>
        </behavior>
      </endpointBehaviors>
    </behaviors>
    <extensions>
      <behaviorExtensions>
        <add name="protobuf" type="Soaf.Web.ProtoBehaviorExtension, Soaf"/>
      </behaviorExtensions>
    </extensions>
  </system.serviceModel>

  <system.net>
    <mailSettings>
      <smtp deliveryMethod="Network" from="alerts@iopracticeware.com">
        <network  host="smtp.gmail.com" defaultCredentials="false" port="465" userName="alerts@iopracticeware.com" password="alert417" enableSsl="true"  />
      </smtp>
    </mailSettings>
  </system.net>

  <system.diagnostics>
    <trace autoflush="true"/>
    <sources>
      <!--
      <source name="AdoService" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener"
            type="Soaf.Logging.RawTextWriterTraceListener, Soaf.Extensions"
            initializeData="AdoServiceLog.log" traceOutputOptions="None" />
          <remove name="Default" />
        </listeners>
      </source>
      <source name="AdoComService" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener"
            type="Soaf.Logging.RawTextWriterTraceListener, Soaf.Extensions"
            initializeData="AdoComServiceLog.log" traceOutputOptions="None" />
          <remove name="Default" />
        </listeners>
      </source>
      <source name="FileManager" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener" type="IO.Practiceware.Logging.ExtendedFileTraceListener, IO.Practiceware.Core" initializeData="FileManager.log" />
          <remove name="Default" />
        </listeners>
      </source>
      <source name="MonitoringService" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener" type="System.Diagnostics.EventLogTraceListener" initializeData="MonitoringServiceVerboseLog" />
          <remove name="Default" />
        </listeners>
      </source>
      -->
    </sources>
    <switches>
      <add name="SourceSwitch" value="All" />
    </switches>
  </system.diagnostics>
</configuration>
