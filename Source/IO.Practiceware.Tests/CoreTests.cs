﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class CoreTests : TestBase
    {
        [TestMethod]
        public void TestLoggingPerformance()
        {
            const int iterations = 100;
            var start = DateTime.Now;
            for (int i = 0; i < iterations; i++)
            {
                Trace.TraceInformation("TestLoggingPerformance: iteration " + i);
            }
            var elapsed = DateTime.Now - start;
            Assert.IsTrue(elapsed <= TimeSpan.FromMilliseconds(iterations * 2), "Average logging performance should be less than 2ms/message");
        }
    }
}
