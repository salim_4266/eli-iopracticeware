﻿using System.Collections;
using System.Drawing;
using System.IO;
using System.Linq;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Services.Imaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class ImagingTests : X12MetadataTestsBase
    {
        /// <summary>
        ///   Tests the image manager. Saves an empty image and tries to retrieve it.
        /// </summary>
        [TestMethod]
        public void TestImageService()
        {
            string myScanPath = Path.Combine(ConfigurationManager.ServerDataPath, "MyScan");
            if (!Directory.Exists(myScanPath))
            {
                Directory.CreateDirectory(myScanPath);
            }

            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);

            string photoPath = Path.Combine(myScanPath, ("Photo_" + patient.Id + ".jpg"));

            var bitmap = new Bitmap(1, 1);
            bitmap.Save(photoPath);

            var imageService = Common.ServiceProvider.GetService<IImageService>();
            Image image = imageService.GetPatientPhoto(patient.Id).IfNotNull(i => Image.FromStream(new MemoryStream(i)));

            Assert.IsNotNull(image);
            Assert.IsTrue(image.Width == 1 && image.Height == 1);
        }

        [TestMethod]
        public void TestEyeRouteApplication()
        {
            string insertExternalApplication = "SET QUOTED_IDENTIFIER OFF " +
                                               "INSERT INTO model.ExternalApplications (UserId, ApplicationName, UserName, [Password], ApplicationPath, ArgumentFormatString) VALUES " +
                                               "({0}, {1}, {2}, {3}, {4}, {5}) ".FormatWith("NULL",
                                                                                            "\'EyeRoute\'",
                                                                                            "\'TestUserName\'",
                                                                                            "NULL",
                                                                                            "'https://host.topconsynergy.com/CCRR0000/'",
                                                                                            "\'#u={username}&id={patientId}&ln={lastName}&fn={firstName}\'") +
                                               "SET QUOTED_IDENTIFIER ON";
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(insertExternalApplication);
            ExternalApplication externalApplication =
                PracticeRepository.ExternalApplications.First(ea => ea.ApplicationName == "EyeRoute");

            Patient patient = PracticeRepository.Patients.First();

            var namedArguments = new Hashtable();
            namedArguments.Add("patientId", patient.Id.ToString().Trim());
            namedArguments.Add("username", "TestUserName");

            var documentViewManager = Common.ServiceProvider.GetService<DocumentViewManager>();
            string commandLineString = documentViewManager.GetExternalApplicationArguments(externalApplication, namedArguments);

            Assert.IsTrue(commandLineString.Contains("#u=TestUserName"));
            Assert.IsTrue(commandLineString.Contains("&id={0}".FormatWith(patient.Id)));
            Assert.IsTrue(commandLineString.Contains("&fn={0}".FormatWith(patient.FirstName)));
            Assert.IsTrue(commandLineString.Contains("&ln={0}".FormatWith(patient.LastName)));
            Assert.IsTrue(commandLineString.Contains(@"#u=TestUserName&id={0}&ln={1}&fn={2}".FormatWith(patient.Id, patient.LastName, patient.FirstName)));
            Assert.IsTrue(externalApplication.ApplicationPath == "https://host.topconsynergy.com/CCRR0000/");
        }

        [TestMethod]
        public void TestForumApplication()
        {
            string insertExternalApplication = "SET QUOTED_IDENTIFIER OFF " +
                                               "INSERT INTO model.ExternalApplications (UserId, ApplicationName, UserName, [Password], ApplicationPath, ArgumentFormatString) VALUES " +
                                               "({0}, {1}, {2}, {3}, {4}, {5}) ".FormatWith("NULL",
                                                                                            "\'FORUM\'",
                                                                                            "\'TestUserName\'",
                                                                                            "\'TestPassword\'",
                                                                                            "\'C:\\Program Files\\CZM\\FORUM Viewer\\api\\launchFORUM.cmd\'",
                                                                                            "\'-username \"{username}\"\'") +
                                               "SET QUOTED_IDENTIFIER ON";
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(insertExternalApplication);
            ExternalApplication externalApplication =
                PracticeRepository.ExternalApplications.First(ea => ea.ApplicationName == "FORUM");

            var namedArguments = new Hashtable();
            namedArguments.Add("patientId", "1");
            namedArguments.Add("username", "TestUserName");
            namedArguments.Add("password", "TestPassword");

            var documentViewManager = Common.ServiceProvider.GetService<DocumentViewManager>();
            string commandLineString = documentViewManager.GetExternalApplicationArguments(externalApplication, namedArguments);

            Assert.IsTrue(commandLineString.Contains("-username \"TestUserName\""));
            Assert.IsFalse(commandLineString.Contains("-password \"TestPassword\""));
            Assert.IsFalse(commandLineString.Contains("-patientId \"1\""));

            string updateExternalApplication = "SET QUOTED_IDENTIFIER OFF " +
                                               "UPDATE model.ExternalApplications SET ArgumentFormatString = {0}".
                                                   FormatWith(
                                                       "\'-username \"{username}\" -password \"{password}\"\'") +
                                               "WHERE Id = {0}".
                                                   FormatWith(externalApplication.Id.ToString()) +
                                               "SET QUOTED_IDENTIFIER ON";
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(updateExternalApplication);
            externalApplication = PracticeRepository.ExternalApplications.First(ea => ea.ApplicationName == "FORUM");

            commandLineString = documentViewManager.GetExternalApplicationArguments(externalApplication,
                                                                                    namedArguments);
            Assert.IsTrue(commandLineString.Contains("-username \"TestUserName\""));
            Assert.IsTrue(commandLineString.Contains("-password \"TestPassword\""));
            Assert.IsFalse(commandLineString.Contains("-patientId \"1\""));

            updateExternalApplication = "SET QUOTED_IDENTIFIER OFF " +
                                        "UPDATE model.ExternalApplications SET ArgumentFormatString = {0}".
                                            FormatWith(
                                                "\'-username \"{username}\" -password \"{password}\" -patientId \"{patientId}\"\'") +
                                        "WHERE Id = {0}".
                                            FormatWith(externalApplication.Id.ToString()) +
                                        "SET QUOTED_IDENTIFIER ON";
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(updateExternalApplication);
            externalApplication = PracticeRepository.ExternalApplications.First(ea => ea.ApplicationName == "FORUM");

            commandLineString = documentViewManager.GetExternalApplicationArguments(externalApplication,
                                                                                    namedArguments);
            Assert.IsTrue(commandLineString.Contains("-username \"TestUserName\""));
            Assert.IsTrue(commandLineString.Contains("-password \"TestPassword\""));
            Assert.IsTrue(commandLineString.Contains("-patientId \"1\""));
        }
    }
}