﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Transactions;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.HL7;
using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Integration.HL7.Handlers;
using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.MVE.HL7;
using IO.Practiceware.Integration.MVE.Model;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Services.ExternalSystems;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHapi.Base.Model;
using NHapi.Model.V23.Datatype;
using NHapi.Model.V23.Segment;
using NHapi.Model.V231.Group;
using NHapi.Model.V231.Message;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Threading;
using Soaf.Collections;
using Appointment = IO.Practiceware.Model.Appointment;
using Constants = IO.Practiceware.Integration.HL7.Constants;
using IMessage = NHapi.Base.Model.IMessage;
using SIU_S12 = NHapi.Model.V23.Message.SIU_S12;
using V23ADT_A28 = NHapi.Model.V23.Message.ADT_A28;
using V23ADT_A31 = NHapi.Model.V23.Message.ADT_A31;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class HL7Tests : TestBase
    {
        private const string AthenaExternalSystemName = "ATHENANET";
        private const string MveExternalSystemName = "MVE";
        private const string NextGenExternalSystemName = "NextGen Rosetta";
        private const string ForumExternalSystemName = "FORUM";

        private IExternalSystemService _externalSystemService;
        private ILegacyPracticeRepository _legacyPracticeRepository;
        private SocketsMessageConnector _socketsMessageConnector;
        private MessageProcessor _messageProcessor;
        private FileMessageConnector _fileMessageConnector;

        private static int _portNumber = 10541;
        private int _currentPortNumber;

        protected override void OnBeforeTestCleanup()
        {
            _fileMessageConnector.Stop();
            _socketsMessageConnector.Stop();

            _fileMessageConnector.Configuration.InboundEndpoints.SelectMany(ep => new[] { ep.ProcessedPath, ep.FailedPath, ep.Value })
                .Concat(_fileMessageConnector.Configuration.OutboundEndpoints.Select(ep => ep.Value))
                .ForEachWithSinglePropertyChangedNotification(p => FileSystem.DeleteDirectory(p));

            if (!_messageProcessor.WaitForIdle(TimeSpan.FromSeconds(10))) throw new Exception("MessageProcessor never finished processing.");

            base.OnBeforeTestCleanup();
        }

        protected override void InitializeTestDatabase()
        {
            var externalSystemService = Common.ServiceProvider.GetService<IExternalSystemService>();

            // Register systems, messages types and entities.

            // MVE
            externalSystemService.EnsureExternalSystemMessageType(MveExternalSystemName, ExternalSystemMessageTypeId.ORM_O01_V231_Outbound);
            externalSystemService.EnsureExternalSystemMessageType(MveExternalSystemName, ExternalSystemMessageTypeId.ORM_O01_V231_Inbound);

            // Athena
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.ACK_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.ADT_A31_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.SIU_S12_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.SIU_S14_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.SIU_S15_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.MFN_M02_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(AthenaExternalSystemName, ExternalSystemMessageTypeId.DFT_P03_V231_Outbound);

            // NextGen
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.ACK_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.ADT_A04_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.ADT_A08_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.ADT_A31_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S12_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S13_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S14_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S15_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S17_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.SIU_S26_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.MFN_M02_V23_Inbound);
            externalSystemService.EnsureExternalSystemMessageType(NextGenExternalSystemName, ExternalSystemMessageTypeId.DFT_P03_V231_Outbound);

            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Appointment", PracticeRepositoryEntityId.Appointment);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "AppointmentType", PracticeRepositoryEntityId.AppointmentType);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "AttendingDoctor", PracticeRepositoryEntityId.User);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "ReferringDoctor", PracticeRepositoryEntityId.ExternalContact);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "LocationResource", PracticeRepositoryEntityId.ServiceLocation);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Insurer", PracticeRepositoryEntityId.Insurer);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Visit", PracticeRepositoryEntityId.Encounter);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Language", PracticeRepositoryEntityId.Language);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Race", PracticeRepositoryEntityId.Race);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "MaritalStatus", PracticeRepositoryEntityId.MaritalStatus);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Gender", PracticeRepositoryEntityId.Gender);
            externalSystemService.EnsureExternalSystemEntity(AthenaExternalSystemName, "Ethnicity", PracticeRepositoryEntityId.Ethnicity);

            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Appointment", PracticeRepositoryEntityId.Appointment);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "AppointmentType", PracticeRepositoryEntityId.AppointmentType);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "AttendingDoctor", PracticeRepositoryEntityId.User);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "ReferringDoctor", PracticeRepositoryEntityId.ExternalContact);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "LocationResource", PracticeRepositoryEntityId.ServiceLocation);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Insurer", PracticeRepositoryEntityId.Insurer);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Visit", PracticeRepositoryEntityId.Encounter);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Ethnicity", PracticeRepositoryEntityId.Ethnicity);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Language", PracticeRepositoryEntityId.Language);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Race", PracticeRepositoryEntityId.Race);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "MaritalStatus", PracticeRepositoryEntityId.MaritalStatus);
            externalSystemService.EnsureExternalSystemEntity(NextGenExternalSystemName, "Gender", PracticeRepositoryEntityId.Gender);

            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Contact");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "PatientInsurance");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Insurance");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Physician");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Location");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "ClaimFilingIndicatorCode");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "MaritalStatus");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Language");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Race");
            externalSystemService.EnsureExternalSystemEntity(MveExternalSystemName, "Gender");

            externalSystemService.EnsureExternalSystemEntity(ForumExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            externalSystemService.EnsureExternalSystemEntity(ForumExternalSystemName, "Appointment", PracticeRepositoryEntityId.Appointment);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.HL7Mappings, false);

            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Open();
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    foreach (string sqlStatement in HL7Resources.HL7Initialize.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        cmd.CommandText = sqlStatement;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        protected override TestDatabasePoolName DatabasePoolName
        {
            get { return TestDatabasePoolName.HL7Tests; }
        }

        protected override void OnAfterTestInitialize()
        {
            _legacyPracticeRepository = Common.ServiceProvider.GetService<ILegacyPracticeRepository>();
            _externalSystemService = Common.ServiceProvider.GetService<IExternalSystemService>();

            IMessenger messenger = new Messenger();

            // ReSharper disable ObjectCreationAsStatement
            ServiceProvider.GetService<Func<IMessenger, AdtA28MessageProducer.ProduceMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, AdtA31MessageProducer.ProduceMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, DftP03MessageProducer.ProduceMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, HL7MessageProducer.ProduceMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, OrmO01MessageProducer.ProduceMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, AppointmentMessageHandler.HandleMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, MasterFilesMessageHandler.HandleMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, PatientMessageHandler.HandleMessageSubscriber>>()(messenger);
            ServiceProvider.GetService<Func<IMessenger, MVEMessageHandler.HandleMessageSubscriber>>()(messenger);
            // ReSharper enable ObjectCreationAsStatement

            _messageProcessor = new MessageProcessor(PracticeRepository, messenger);
            var messageAuditor = new MessageAuditor(PracticeRepository, _messageProcessor);

            _currentPortNumber = Interlocked.Increment(ref _portNumber);
            _socketsMessageConnector = new SocketsMessageConnector(messageAuditor, messenger);
            _socketsMessageConnector.PortsToListenOn = new[] { _currentPortNumber };
            _socketsMessageConnector.ResolvingEndpointForOutboundMessage += (sender, e) => e.Value = new SocketsMessagingOutboundEndpointConfiguration
            {
                Name = "Test",
                Value = "localhost:" + _currentPortNumber,
                RequestAcknowledgements = true
            };

            var fileMessageConnectorDirectory = FileManager.Instance.GetTempPathName();
            _fileMessageConnector = new FileMessageConnector(() => messageAuditor, messenger);
            _fileMessageConnector.Configuration = new MessageConnectorConfiguration<FileMessagingInboundEndpointConfiguration, FileMessagingOutboundEndpointConfiguration>
            {
                InboundEndpoints = new List<FileMessagingInboundEndpointConfiguration> {new FileMessagingInboundEndpointConfiguration {Value = fileMessageConnectorDirectory, Name = "MVE"}},
                OutboundEndpoints = new List<FileMessagingOutboundEndpointConfiguration> {new FileMessagingOutboundEndpointConfiguration {Value = fileMessageConnectorDirectory, Name = "MVE"}}
            };

            _fileMessageConnector.Configuration.InboundEndpoints.SelectMany(ep => new[] { ep.ProcessedPath, ep.FailedPath, ep.Value })
                .Concat(_fileMessageConnector.Configuration.OutboundEndpoints.Select(ep => ep.Value))
                .ForEachWithSinglePropertyChangedNotification(FileSystem.EnsureDirectory);

            messageAuditor.Transaction = Transaction.Current;
            _messageProcessor.Transaction = Transaction.Current;
        }

        /// <summary>
        ///   Tests sending a message over sockets.
        /// </summary>
        /// <param name="messageText"> The message text. </param>
        private void SendSocketMessage(string messageText)
        {
            string host = "localhost:" + _currentPortNumber;

            // add ACK to MSH
            messageText = messageText.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Select((l, i) => i == 0 ? l + "|||AL" : l).Join(Environment.NewLine);

            messageText = string.Format("{0}{1}{2}", Constants.MessageStartDelimiter, messageText.TrimStart(Constants.MessageStartDelimiter).TrimEnd(Constants.MessageEndDelimiter), Constants.MessageEndDelimiter);

            IMessage message = HL7Messages.GetPipeParser().Parse(messageText.TrimStart(Constants.MessageStartDelimiter).TrimEnd(Constants.MessageEndDelimiter));

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Connect(new IPEndPoint(Dns.GetHostEntry(host.Split(':')[0]).AddressList.First(item => item.AddressFamily == AddressFamily.InterNetwork),
                                              host.Split(':')[1].ToInt() ?? 0));
                socket.Send(Encoding.UTF8.GetBytes(messageText).ToArray());

                if (message.RequestsAcknowledgement())
                {
                    var response = new byte[2049];
                    int size = socket.Receive(response);

                    string responseText = Encoding.UTF8.GetString(response.Take(size).ToArray());

                    if (responseText.StartsWith(Constants.MessageStartDelimiter) && responseText.EndsWith(Constants.MessageEndDelimiter))
                    {
                        responseText = responseText.Substring(Constants.MessageStartDelimiter.Length, responseText.Length - Constants.MessageStartDelimiter.Length - Constants.MessageEndDelimiter.Length).Trim();
                    }

                    Assert.AreEqual(HL7Messages.GetPipeParser().GetAckID(responseText).Trim(), message.GetMessageControlId().Trim());
                }
            }
        }


        /// <summary>
        ///   Sets up the data for SIU messages.
        /// </summary>
        private static void SetUpSiu()
        {
            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Open();
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    foreach (string sqlStatement in HL7Resources.SetUpSIU.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        cmd.CommandText = sqlStatement;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        ///   Sets up the data for an ORM_O01 message.
        /// </summary>
        /// <returns> </returns>
        private static int SetUpOrmO01()
        {
            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Open();
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    foreach (string sqlStatement in HL7Resources.SetUpORM_O01.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        cmd.CommandText = sqlStatement;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            return Common.ServiceProvider.GetService<ILegacyPracticeRepository>().Appointments.OrderByDescending(i => i.AppointmentId).First().AppointmentId;
        }

        /// <summary>
        ///   Sets up the data for an DFT_P03 message.
        /// </summary>
        /// <returns> </returns>
        private static int SetUpDftP03()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.HL7Metadata);

            return Common.ServiceProvider.GetService<ILegacyPracticeRepository>().Appointments.OrderByDescending(i => i.AppointmentId).First().AppointmentId;
        }

        /// <summary>
        ///   Tests ORM message creation.
        /// </summary>
        [TestMethod]
        public void TestHL7OutboundOrmO01MessageCreation()
        {
            int appointmentId = SetUpOrmO01();

            int financialsCount = _legacyPracticeRepository.PatientFinancials.Count();
            Assert.IsTrue(financialsCount > 0);

            ORM_O01 message = ((OrmO01MessageProducer)Common.ServiceProvider.GetService(typeof(OrmO01MessageProducer))).CreateMessage(appointmentId, MveExternalSystemName, Guid.NewGuid().ToString("N"));
            string messageString = HL7Messages.GetPipeParser().Encode(message).Trim();
            Assert.IsTrue(!messageString.Trim().IsNullOrEmpty());

            Assert.IsTrue(message.PATIENT.INSURANCERepetitionsUsed == 1);

            // Sync up the parts of the message that would be different        
            //MSH Segment
            var expectedMessage = (ORM_O01)HL7Messages.GetPipeParser().Parse(HL7Resources.ORM_O01Message);
            expectedMessage.MSH.SendingFacility.NamespaceID.Value = message.MSH.SendingFacility.NamespaceID.Value;
            expectedMessage.MSH.SendingFacility.UniversalID.Value = message.MSH.SendingFacility.UniversalID.Value;
            expectedMessage.MSH.DateTimeOfMessage.TimeOfAnEvent.Value = message.MSH.DateTimeOfMessage.TimeOfAnEvent.Value;
            expectedMessage.MSH.MessageControlID.Value = message.MSH.MessageControlID.Value;

            //PID Segment
            expectedMessage.PATIENT.PID.PatientID.ID.Value = message.PATIENT.PID.PatientID.ID.Value;
            expectedMessage.PATIENT.PID.GetPatientIdentifierList(0).ID.Value = message.PATIENT.PID.GetPatientIdentifierList(0).ID.Value;
            expectedMessage.PATIENT.PID.GetPatientName(0).GivenName.Value = message.PATIENT.PID.GetPatientName(0).GivenName.Value;
            expectedMessage.PATIENT.PID.GetPatientName(0).FamilyLastName.FamilyName.Value = message.PATIENT.PID.GetPatientName(0).FamilyLastName.FamilyName.Value;
            expectedMessage.PATIENT.PID.GetPatientName(0).MiddleInitialOrName.Value = message.PATIENT.PID.GetPatientName(0).MiddleInitialOrName.Value;
            expectedMessage.PATIENT.PID.MaritalStatus.Identifier.Value = message.PATIENT.PID.MaritalStatus.Identifier.Value;

            //IN1 Segment
            for (int i = 0; i <= expectedMessage.PATIENT.INSURANCERepetitionsUsed - 1; i++)
            {
                ORM_O01_INSURANCE insuranceRepitition = message.PATIENT.GetINSURANCE(i);
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.InsurancePlanID.Identifier.Value = insuranceRepitition.IN1.InsurancePlanID.Identifier.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.GetInsuranceCompanyID(0).ID.Value = insuranceRepitition.IN1.GetInsuranceCompanyID(0).ID.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.GetNameOfInsured(0).GivenName.Value = insuranceRepitition.IN1.GetNameOfInsured(0).GivenName.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.GetNameOfInsured(0).FamilyLastName.FamilyName.Value = insuranceRepitition.IN1.GetNameOfInsured(0).FamilyLastName.FamilyName.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.GetNameOfInsured(0).MiddleInitialOrName.Value = insuranceRepitition.IN1.GetNameOfInsured(0).MiddleInitialOrName.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.PlanEffectiveDate.Value = insuranceRepitition.IN1.PlanEffectiveDate.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.PlanExpirationDate.Value = insuranceRepitition.IN1.PlanExpirationDate.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.PlanType.Value = insuranceRepitition.IN1.PlanType.Value;
                expectedMessage.PATIENT.GetINSURANCE(i).IN1.InsuredSRelationshipToPatient.Identifier.Value = insuranceRepitition.IN1.InsuredSRelationshipToPatient.Identifier.Value;
            }
            for (int i = 0; i <= expectedMessage.ORDERRepetitionsUsed - 1; i++)
            {
                ORM_O01_ORDER order = message.GetORDER(i);
                expectedMessage.GetORDER(i).ORDER_DETAIL.RQD.ItemCodeInternal.Identifier.Value = order.ORDER_DETAIL.RQD.ItemCodeInternal.Identifier.Value;
                expectedMessage.GetORDER(i).ORDER_DETAIL.RQD.ItemCodeInternal.Text.Value = order.ORDER_DETAIL.RQD.ItemCodeInternal.Text.Value;
                expectedMessage.GetORDER(i).ORDER_DETAIL.RQD.ItemCodeInternal.AlternateText.Value = order.ORDER_DETAIL.RQD.ItemCodeInternal.AlternateText.Value;
                expectedMessage.GetORDER(i).ORC.PlacerOrderNumber.EntityIdentifier.Value = order.ORC.PlacerOrderNumber.EntityIdentifier.Value;
                expectedMessage.GetORDER(i).ORC.DateTimeOfTransaction.TimeOfAnEvent.Value = order.ORC.DateTimeOfTransaction.TimeOfAnEvent.Value;
                expectedMessage.GetORDER(i).ORC.GetOrderingProvider(0).IDNumber.Value = order.ORC.GetOrderingProvider(0).IDNumber.Value;
                expectedMessage.GetORDER(i).ORC.EntererSLocation.Facility.NamespaceID.Value = order.ORC.EntererSLocation.Facility.NamespaceID.Value;
                expectedMessage.GetORDER(i).ORC.EntererSLocation.PointOfCare.Value = order.ORC.EntererSLocation.PointOfCare.Value;
            }

            Assert.AreEqual(HL7Messages.GetPipeParser().Encode(expectedMessage).Trim(), messageString);
        }

        /// <summary>
        ///   Tests the creation and delivery of a message.
        /// </summary>
        [TestMethod]
        public void TestHL7OutboundOrmO01MessageProcessing()
        {
            int appointmentId = SetUpOrmO01();

            DateTime start = DateTime.UtcNow;

            // Create a message and save it
            int messageTypeId = PracticeRepository.ExternalSystemExternalSystemMessageTypes.First(i => i.ExternalSystem.Name == MveExternalSystemName).Id;
            var message = new ExternalSystemMessage
                {
                    CreatedBy = "Test",
                    CreatedDateTime = DateTime.UtcNow,
                    UpdatedDateTime = DateTime.UtcNow,
                    Description = string.Empty,
                    ExternalSystemExternalSystemMessageTypeId = messageTypeId,
                    ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                    Value = string.Empty
                };

            message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
                {
                    PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.Appointment,
                    PracticeRepositoryEntityKey = appointmentId.ToString()
                });
            PracticeRepository.Save(message);

            // Start the integration monitor so it delivers the message
            StartProcessorAndConnectors();

            message = WaitForMessageProcessed(start);

            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());

            // Check that the message was dropped and it was written with the correct contents
            string remoteDirectory = _fileMessageConnector.Configuration.OutboundEndpoints.For(MveExternalSystemName).Value;
            string messageControlId = HL7Messages.GetPipeParser().Parse(message.Value).GetMessageControlId();
            string path = Path.Combine(remoteDirectory, messageControlId);
            Assert.IsTrue(FileManager.Instance.FileExists(path));
            Assert.IsTrue(FileManager.Instance.ReadContentsAsText(path).TrimStart(Constants.MessageStartDelimiter).TrimEnd(Constants.MessageEndDelimiter) == message.Value.Trim());


        }

        /// <summary>
        ///   Tests the pickup of an ORM message.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundOrmO01MessagePickup()
        {
            int appointmentId = SetUpOrmO01();

            DateTime start = DateTime.UtcNow;

            IEnumerable<string> pickupDirectories = _fileMessageConnector.Configuration.InboundEndpoints.Select(e => e.Value);

            ORM_O01 message = ((OrmO01MessageProducer)Common.ServiceProvider.GetService(typeof(OrmO01MessageProducer))).CreateMessage(appointmentId, MveExternalSystemName, Guid.NewGuid().ToString("N"));

            // Move the file to the pickup directory so it will get picked up
            string destinationDirectory = pickupDirectories.Where(i => !i.Trim().IsNullOrEmpty()).First(Directory.Exists);
            FileManager.Instance.CommitContents(Path.Combine(destinationDirectory, message.MSH.MessageControlID.Value), HL7Messages.GetPipeParser().Encode(message));

            string processedPath = Path.Combine(Path.Combine(destinationDirectory, "Processed"), message.MSH.MessageControlID.Value);

            StartProcessorAndConnectors();

            WaitForMessageProcessed(start);

            // Message move happens milliseconds after message processing completes
            Thread.Sleep(500);

            Assert.IsTrue(FileManager.Instance.FileExists(processedPath));

            ExternalSystemMessage newMessage = WaitForMessageProcessed(start);

            Assert.IsNotNull(newMessage);

            Assert.IsTrue(newMessage.Value.Trim() == HL7Messages.GetPipeParser().Encode(message).Trim());
            Assert.IsTrue(newMessage.ExternalSystemMessageProcessingStateId == 3);



            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var mveRepository = Common.ServiceProvider.GetService<IMVERepository>();
                prescription prescription = mveRepository.prescriptions.Where(i => i.lastupdateddt.Value >= start.ToLocalTime()).OrderByDescending(i => i.prescriptionid).FirstOrDefault();
                Assert.IsNotNull(prescription);
                ts.Complete();
            }
        }

        [TestMethod]
        public void TestHL7PracticeActivityCheckoutExternalSystemMessageTrigger()
        {
            int appointmentId = SetUpOrmO01();

            PracticeActivity activity = _legacyPracticeRepository.PracticeActivities.First(a => a.AppointmentId.Value == appointmentId);
            if (!(activity.Status == "G"))
            {
                activity.Status = "G";
            }
            _legacyPracticeRepository.Save(activity);

            activity.Status = "D";

            _legacyPracticeRepository.Save(activity);

            Assert.IsTrue(PracticeRepository.ExternalSystemMessages.Count() == 3);
        }

        [TestMethod]
        public void TestHL7PatientDemographicsExternalSystemMessageUpdateTrigger()
        {
            // Forum
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertPatientDemographicRow);
            int rowId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().PatientDemographics.First(a => a.BirthDate == "20990621").PatientId;
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose("UPDATE [dbo].[PatientDemographics] SET LastName = 'Last' WHERE PatientId = {0}".FormatWith(rowId.ToString()));

            int messageCount = (from esm in PracticeRepository.ExternalSystemMessages
                                join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on esm.Id equals esmpre.ExternalSystemMessageId
                                where esmpre.PracticeRepositoryEntityKey == rowId.ToString() && esm.CreatedBy == "PatientUpdate"
                                select esm).Count();

            Assert.IsTrue(messageCount == 1);
        }

        [TestMethod]
        public void TestHL7PatientDemographicsExternalSystemMessageInsertTrigger()
        {
            // Forum
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertPatientDemographicRow);
            int rowId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().PatientDemographics.First(a => a.BirthDate == "20990621").PatientId;

            int messageCount = (from esm in PracticeRepository.ExternalSystemMessages
                                join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on esm.Id equals esmpre.ExternalSystemMessageId
                                where esmpre.PracticeRepositoryEntityKey == rowId.ToString() && esm.CreatedBy == "PatientInsert"
                                select esm).Count();

            Assert.IsTrue(messageCount == 1);
        }

        [TestMethod]
        public void TestHL7AppointmentsExternalSystemMessageUpdateTrigger()
        {
            // Forum
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertAppointmentRow);
            int rowId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().Appointments.First(a => a.AppDate == "20990621").AppointmentId;
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose("UPDATE [dbo].[Appointments] SET Duration = 100 WHERE AppointmentId = {0}".FormatWith(rowId.ToString()));

            int messageCount = (from esm in PracticeRepository.ExternalSystemMessages
                                join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on esm.Id equals esmpre.ExternalSystemMessageId
                                where esmpre.PracticeRepositoryEntityKey == rowId.ToString() && esm.CreatedBy == "AppointmentUpdate"
                                select esm).Count();

            Assert.IsTrue(messageCount == 1);
        }

        [TestMethod]
        public void TestHL7AppointmentsExternalSystemMessageInsertTrigger()
        {
            // Forum
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertAppointmentRow);
            int rowId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().Appointments.First(a => a.AppDate == "20990621").AppointmentId;

            int messageCount = (from esm in PracticeRepository.ExternalSystemMessages
                                join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on esm.Id equals esmpre.ExternalSystemMessageId
                                where esmpre.PracticeRepositoryEntityKey == rowId.ToString() && esm.CreatedBy == "AppointmentInsert"
                                select esm).Count();

            Assert.IsTrue(messageCount == 1);
        }

        /// <summary>
        ///   Tests an inbound ADT_A28 (create patient) message using sockets.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundAdtA28MessagePickup()
        {
            DateTime start = DateTime.UtcNow;

            int lastPatientId = PracticeRepository.Patients.OrderByDescending(i => i.Id).Select(i => i.Id).FirstOrDefault();

            StartProcessorAndConnectors();

            string messageText = HL7Resources.ADT_A28Message;
            SendSocketMessage(messageText);

            var message = (V23ADT_A28)HL7Messages.GetPipeParser().Parse(messageText);

            WaitForMessageProcessed(start);



            Patient newPatient = PracticeRepository.Patients.FirstOrDefault(i => i.Id > lastPatientId);
            Assert.IsNotNull(newPatient);

            PatientInsurance financial = PracticeRepository.PatientInsurances.Include(p => p.InsurancePolicy).OrderByDescending(i => i.Id).First(i => newPatient.Id == i.InsuredPatientId);
            Assert.IsTrue(newPatient.Id == PracticeRepository.Patients.OrderByDescending(i => i.Id).First().Id);

            ExternalSystemEntityMapping mapping = PracticeRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity.ExternalSystem, i => i.PracticeRepositoryEntity).FirstOrDefault(i => i.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient && i.PracticeRepositoryEntityKey == newPatient.Id.ToString() && i.ExternalSystemEntity.Name == "Patient" && i.ExternalSystemEntity.ExternalSystem.Name == message.MSH.SendingApplication.NamespaceID.Value);
            Assert.IsNotNull(mapping);
            Assert.IsTrue(mapping.ExternalSystemEntityKey == message.PID.PatientIDExternalID.ID.Value);
            Assert.IsTrue(mapping.PracticeRepositoryEntityKey == newPatient.Id.ToString());
            Assert.IsTrue(mapping.ExternalSystemEntity.Name == "Patient");
            Assert.IsTrue(mapping.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient);

            Assert.IsTrue(newPatient.LastName == "DAVIS");
            Assert.IsTrue(newPatient.FirstName == "LEALA");

            Assert.IsTrue(financial.InsuredPatientId == newPatient.Id);

            ExternalSystemEntityMapping insurerMapping = _externalSystemService.GetMappings<Insurer>(financial.InsurancePolicy.InsurerId.ToString(), AthenaExternalSystemName, "Insurer", message.GetINSURANCE().IN1.InsuranceCompanyID.ID.Value).FirstOrDefault();
            Assert.IsNotNull(insurerMapping);
            var insurer = (Insurer)_externalSystemService.GetMappedEntity(insurerMapping);
            Assert.IsNotNull(insurer);
        }


        /// <summary>
        ///   Tests an inbound TestInboundAdtA28A31Message (update insurances table) message using sockets.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundAdtA28A31MessagePickup()
        {
            StartProcessorAndConnectors();

            //#1
            DateTime start = DateTime.UtcNow;

            string messageText = HL7Resources.ADT_A28Message;
            SendSocketMessage(messageText);
            WaitForMessageProcessed(start, new HL7Message(messageText).Id.ToString());
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" UPDATE model.Insurers SET InsurerBusinessClassId = (SELECT Id FROM model.InsurerBusinessClasses WHERE Name = 'Commercial')");
            PracticeInsurer practiceinsurer = _legacyPracticeRepository.PracticeInsurers.Where(i => i.InsurerName == "BCBS-AZ: ARIZONA").OrderByDescending(i => i.InsurerId).First();
            Assert.IsTrue(practiceinsurer.InsurerBusinessClass == "COMM");

            //#2
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" UPDATE model.Insurers SET InsurerBusinessClassId = (SELECT Id FROM model.InsurerBusinessClasses WHERE Name = 'BlueCrossBlueShield')");
            PracticeInsurer practiceinsurer2 = _legacyPracticeRepository.PracticeInsurers.OrderByDescending(i => i.InsurerId).First();
            Assert.IsTrue(practiceinsurer2.InsurerBusinessClass == "BLUES");

            //#3
            DateTime start3 = DateTime.UtcNow;

            string messageText3 = HL7Resources.ADT_A28_A31;
            SendSocketMessage(messageText3);
            WaitForMessageProcessed(start3, new HL7Message(messageText3).Id.ToString());
            PracticeInsurer practiceinsurer3 = _legacyPracticeRepository.PracticeInsurers.OrderByDescending(i => i.InsurerId).First();
            Assert.IsTrue(practiceinsurer3.InsurerBusinessClass == "BLUES");

            Insurer insurer = PracticeRepository.Insurers.Where(i => i.Name == "BCBS-AZ: ARIZONA").OrderByDescending(i => i.Id).First();
            Assert.IsNotNull(insurer);
            Assert.IsTrue(insurer.ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.OtherNonFederalPrograms);
        }

        /// <summary>
        /// Waits for the first message to show as processed created after the specified time.
        /// </summary>
        /// <param name="after">The after.</param>
        /// <param name="correlationId">The correlation id.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Failed to find processed message after {0}. ExternalSystemMessage table has {1} rows. Last row was created at {2}..FormatWith(after, PracticeRepository.ExternalSystemMessages.Count(), PracticeRepository.ExternalSystemMessages.OrderByDescending(i => i.CreatedDateTime).Select(i => i.CreatedDateTime).FirstOrDefault())</exception>
        private ExternalSystemMessage WaitForMessageProcessed(DateTime? after = null, string correlationId = null, Expression<Func<ExternalSystemMessage, bool>> predicate = null)
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                int count = 0;
                while (count < 30)
                {
                    IQueryable<ExternalSystemMessage> query = PracticeRepository.ExternalSystemMessages.OrderByDescending(m => m.UpdatedDateTime).Where(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed);

                    if (after.HasValue) query = query.Where(m => m.UpdatedDateTime >= after);

                    if (correlationId != null) query = query.Where(i => i.CorrelationId == correlationId);

                    if (predicate != null) query = query.Where(predicate);

                    ExternalSystemMessage message = query.FirstOrDefault();

                    if (message != null)
                    {
                        Func<bool> hasUnprocessedMessages = () => PracticeRepository.ExternalSystemMessages.All(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed);
                        hasUnprocessedMessages.Wait(TimeSpan.FromSeconds(10), TimeSpan.FromMilliseconds(300));

                        Trace.WriteLine("Waited {0}ms for message to be processed.".FormatWith(count * 300));
                        return message;
                    }
                    Thread.Sleep(300);
                    count += 1;
                }
            }
            throw new Exception("Failed to find processed message after {0}. ExternalSystemMessage table has {1} rows. Last row was created at {2}.".FormatWith(after, PracticeRepository.ExternalSystemMessages.Count(), PracticeRepository.ExternalSystemMessages.OrderByDescending(i => i.CreatedDateTime).Select(i => i.CreatedDateTime).FirstOrDefault()));
        }

        private ExternalSystemMessage WaitForMessageProcessed(IEnumerable<ExternalSystemMessage> excludedMessages)
        {
            IEnumerable<Guid> messageIds = excludedMessages.Select(em => em.Id);
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                int count = 0;
                while (count < 30)
                {
                    ExternalSystemMessage message = PracticeRepository.ExternalSystemMessages.OrderByDescending(m => m.UpdatedDateTime).FirstOrDefault(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed && !messageIds.Contains(m.Id));
                    if (message != null)
                    {
                        Trace.WriteLine("Waited {0}ms for message to be processed.".FormatWith(count * 300));
                        return message;
                    }
                    Thread.Sleep(300);
                    count += 1;
                }
            }
            throw new Exception("Failed to find processed message. ExternalSystemMessage table has {0} rows. Last row was created at {1}.".FormatWith(PracticeRepository.ExternalSystemMessages.Count(), PracticeRepository.ExternalSystemMessages.OrderByDescending(i => i.CreatedDateTime).Select(i => i.CreatedDateTime).FirstOrDefault()));
        }

        /// <summary>
        ///   Tests inbound SIU messages (schedule, check in, then cancel).
        /// </summary>
        [TestMethod]
        public void TestHL7InboundSiuScheduleCheckInCancelMessage()
        {
            SetUpSiu();

            // Schedule the appointment

            var s12Message = (SIU_S12)HL7Messages.GetPipeParser().Parse(HL7Resources.SIU_S12Message);

            DateTime start = DateTime.UtcNow;

            StartProcessorAndConnectors();

            SendSocketMessage(HL7Resources.SIU_S12Message);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);

            Assert.IsNotNull(externalSystemMessage);

            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.GetMapping(AthenaExternalSystemName, PracticeRepositoryEntityId.Appointment, s12Message.SCH.FillerAppointmentID.EntityIdentifier.Value);
            Assert.IsTrue(appointmentMapping != null && appointmentMapping.PracticeRepositoryEntityKey != "0");

            var appointment = (Appointment)_externalSystemService.GetMappedEntity(appointmentMapping);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(appointment.DateTime == s12Message.SCH.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.GetAsDate());
            // Check in the appointment
            DateTime start2 = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.SIU_S14Message);
            externalSystemMessage = WaitForMessageProcessed(start2, null, e => e.Description == "SIU_S14_V23");

            Assert.IsNotNull(externalSystemMessage);

            Assert.AreEqual("SIU_S14_V23", externalSystemMessage.Description);

            appointment = Common.PracticeRepository.Appointments.Include(a => a.Encounter).FirstOrDefault(i => i.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value);

            Assert.IsNotNull(appointment);
            Assert.AreEqual((int)EncounterStatus.Questions, appointment.Encounter.EncounterStatusId);

            int appointmentId = appointment.Id;
            PracticeActivity[] practiceActivities = _legacyPracticeRepository.PracticeActivities.Where(i => i.AppointmentId.Value == appointmentId).ToArray();
            Assert.IsTrue(practiceActivities.Length == 1);

            // Cancel the appointment
            DateTime start3 = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.SIU_S15Message);
            externalSystemMessage = WaitForMessageProcessed(start3, null, e => e.Description == "SIU_S15_V23");

            try
            {
                Assert.IsNotNull(externalSystemMessage);
                Assert.AreEqual("SIU_S15_V23", externalSystemMessage.Description);
            }
            catch
            {
                Trace.TraceInformation("ExternalSystemMessages: {0}".FormatWith(PracticeRepository.ExternalSystemMessages.OrderBy(i => i.UpdatedDateTime).Select(i => i.Description).ToArray().Join()));
                throw;
            }
            appointment = Common.PracticeRepository.Appointments.Include(i => i.Encounter).FirstOrDefault(i => i.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value);

            Assert.IsNotNull(appointment);

            Trace.TraceInformation("Encounter has status {0} after cancel.", appointment.Encounter.EncounterStatus);

            Assert.AreEqual((int)EncounterStatus.CancelOther, appointment.Encounter.EncounterStatusId);


        }

        private void StartProcessorAndConnectors()
        {
            _fileMessageConnector.Start();
            _fileMessageConnector.ProcessFiles();
            _socketsMessageConnector.Start();

            _messageProcessor.ProcessUnprocessedMessages();
        }

        /// <summary>
        ///   Tests creating a DFT_P03 message.
        /// </summary>
        [TestMethod]
        public void TestHL7OutboundDftP03Message()
        {
            int appointmentId = SetUpDftP03();

            DFT_P03 message = Common.ServiceProvider.GetService<DftP03MessageProducer>().CreateDftP03(appointmentId, AthenaExternalSystemName, "8097fd4a-a250-4007-959d-41f2e25aab8c");
            Assert.IsNotNull(message);

            string messageString = HL7Messages.GetPipeParser().Encode(message);
            Assert.IsFalse(messageString.IsNullOrEmpty());
        }

        /// <summary>
        ///   Tests creating an ADT_A28 message.
        /// </summary>
        [TestMethod]
        public void TestHL7OutboundAdt28Message()
        {
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertPatientDemographicRow);
            int patientId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().PatientDemographics.First(a => a.BirthDate == "20990621").PatientId;
            Patient patient = PracticeRepository.Patients.Include(p => p.Language).First(p => p.Id == patientId);
            string externalMessageId = Guid.NewGuid().ToString();

            V23ADT_A28 message = Common.ServiceProvider.GetService<AdtA28MessageProducer>().CreateAdtA28(patient, ForumExternalSystemName, externalMessageId);
            Assert.IsNotNull(message);
            string messageString = HL7Messages.GetPipeParser().Encode(message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            var reparsedMessage = (V23ADT_A28)HL7Messages.GetPipeParser().Parse(messageString);
            Assert.IsNotNull(reparsedMessage);

            // Test MSH contents
            MSH msh = reparsedMessage.MSH;
            Assert.AreEqual(msh.SendingApplication.NamespaceID.Value, Integration.Messaging.Constants.LocalApplicationId);
            Assert.AreEqual(msh.ProcessingID.ProcessingID.Value, MessagingConfiguration.Current.HL7MessagingConfiguration.OutboundMessagesProcessingId);
            Assert.AreEqual(msh.ReceivingApplication.NamespaceID.Value, ForumExternalSystemName);
            Assert.AreEqual(msh.MessageControlID.Value, externalMessageId);

            // Test PID contents
            PID pid = reparsedMessage.PID;
            Assert.AreEqual(pid.SetIDPatientID.Value, "1");
            Assert.AreEqual(pid.PatientName.GivenName.Value, "James");
            Assert.AreEqual(pid.PatientName.FamilyName.Value, "Bond");
            Assert.AreEqual(pid.DateOfBirth.TimeOfAnEvent.Value, "20990621");
            Assert.AreEqual(pid.Sex.Value, "M");
            Assert.AreEqual(message.PID.GetPatientIDInternalID(0).ID.Value, patient.Id.ToString());
            var mappedMaritalStatus = _externalSystemService.GetMappings<MaritalStatus>(patient.MaritalStatusId.ToString()).FirstOrDefault(i => i.ExternalSystemEntity.Name == "MaritalStatus");
            Assert.IsTrue(pid.GetMaritalStatus(0).Value == "S" || pid.GetMaritalStatus(0).Value == mappedMaritalStatus.IfNotDefault(i => i.ExternalSystemEntityKey));

            // Test EVN contents
            EVN evn = reparsedMessage.EVN;
            Assert.AreEqual(evn.EventTypeCode.Value, "A28");

            // Test PV1 contents
            PV1 pv1 = reparsedMessage.PV1;
            Assert.AreEqual(pv1.SetIDPatientVisit.Value, 1.ToString());
        }

        [TestMethod]
        public void TestHL7OutboundAdt28MessageProcessing()
        {
            const string ioExternalSystemName = "IO PRACTICEWARE";
            SocketsMessagingOutboundEndpointConfiguration endpoint = MessagingConfiguration.Current.SocketsMessageConnectorConfiguration.OutboundEndpoints.First(e => e.Name.ToUpper() == ForumExternalSystemName);
            if (endpoint.RequestAcknowledgements)
            {
                _externalSystemService.EnsureExternalSystemMessageType(ioExternalSystemName, ExternalSystemMessageTypeId.ACK_V23_Inbound);
                _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            }

            // In the real system, we will never process incoming A28 messages from FORUM as that scenario would never occur.
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Outbound);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Race", PracticeRepositoryEntityId.Race);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Language", PracticeRepositoryEntityId.Language);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "MaritalStatus", PracticeRepositoryEntityId.MaritalStatus);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Gender", PracticeRepositoryEntityId.Gender);
            _externalSystemService.EnsureExternalSystemEntity(ioExternalSystemName, "Ethnicity", PracticeRepositoryEntityId.Ethnicity);

            // account for the fact that SQL Server may have a different value as a result of GETUTCDATE() vs DateTime.UtcNow. The table should be empty at this point
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertPatientDemographicRow);

            StartProcessorAndConnectors();

            var processedMessages = new List<ExternalSystemMessage>();

            // Process external message From PatientInsert trigger
            ExternalSystemMessage message = WaitForMessageProcessed();
            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());
            processedMessages.Add(message);

            // Process produced A28 message from SocketsMessageConnector
            message = WaitForMessageProcessed(processedMessages);
            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());
            processedMessages.Add(message);

            // ACK_V23 is not handled by anyone.  Manually set it to processed here.
            message = PracticeRepository.ExternalSystemMessages.First(m => m.Description == "ACK_V23");
            message.ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Processed;
            message = WaitForMessageProcessed(processedMessages);
            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());


        }

        /// <summary>
        ///   Tests creating an ADT_A31 message.
        /// </summary>
        [TestMethod]
        public void TestHL7OutboundAdt31Message()
        {
            _externalSystemService.EnsureExternalSystemMessageType(ForumExternalSystemName, ExternalSystemMessageTypeId.ADT_A31_V23_Outbound);

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.InsertPatientDemographicRow);
            int patientId = Common.ServiceProvider.GetService<ILegacyPracticeRepository>().PatientDemographics.First(a => a.BirthDate == "20990621").PatientId;
            Patient patient = PracticeRepository.Patients.First(p => p.Id == patientId);
            string externalMessageId = Guid.NewGuid().ToString();

            V23ADT_A31 message = Common.ServiceProvider.GetService<AdtA31MessageProducer>().CreateAdtA31(patient, ForumExternalSystemName, externalMessageId);
            Assert.IsNotNull(message);

            string messageString = HL7Messages.GetPipeParser().Encode(message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            var reparsedMessage = (V23ADT_A31)HL7Messages.GetPipeParser().Parse(messageString);
            Assert.IsNotNull(reparsedMessage);

            // Test MSH contents
            MSH msh = reparsedMessage.MSH;
            Assert.AreEqual(msh.SendingApplication.NamespaceID.Value, Integration.Messaging.Constants.LocalApplicationId);
            Assert.AreEqual(msh.ProcessingID.ProcessingID.Value, MessagingConfiguration.Current.HL7MessagingConfiguration.OutboundMessagesProcessingId);
            Assert.AreEqual(msh.ReceivingApplication.NamespaceID.Value, ForumExternalSystemName);
            Assert.AreEqual(msh.MessageControlID.Value, externalMessageId);

            // Test PID contents
            PID pid = reparsedMessage.PID;
            Assert.AreEqual(pid.SetIDPatientID.Value, "1");
            Assert.AreEqual(pid.PatientName.GivenName.Value, "James");
            Assert.AreEqual(pid.PatientName.FamilyName.Value, "Bond");
            Assert.AreEqual(pid.DateOfBirth.TimeOfAnEvent.Value, "20990621");
            Assert.AreEqual(pid.Sex.Value, "M");
            Assert.AreEqual(message.PID.GetPatientIDInternalID(0).ID.Value, patient.Id.ToString());

            var mappedMaritalStatus = _externalSystemService.GetMappings<MaritalStatus>(patient.MaritalStatusId.ToString()).FirstOrDefault(i => i.ExternalSystemEntity.Name == "MaritalStatus");
            Assert.IsTrue(pid.GetMaritalStatus(0).Value == "S" || pid.GetMaritalStatus(0).Value == mappedMaritalStatus.IfNotDefault(i => i.ExternalSystemEntityKey));

            // Test EVN contents
            EVN evn = reparsedMessage.EVN;
            Assert.AreEqual(evn.EventTypeCode.Value, "A31");

            // Test PV1 contents
            PV1 pv1 = reparsedMessage.PV1;
            Assert.AreEqual(pv1.SetIDPatientVisit.Value, 1.ToString());
        }

        /// <summary>
        ///   Tests processing an the inbound MFN M02 MAD message.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundMfnM02MadMessage()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.MFN_M02_MADMessage);

            WaitForMessageProcessed(start);



            string externalProviderId = PracticeRepository.ExternalSystemEntityMappings.OrderByDescending(j => j.Id).First().PracticeRepositoryEntityKey;
            ExternalProvider externalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().First(i => i.Id == externalProviderId.ToInt().Value);
            Assert.IsTrue(externalProvider.LastNameOrEntityName == "PHILLIPS");
        }

        /// <summary>
        ///   Tests processing an the inbound MFN M02 MAD message.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundMfnM02MadMessage2()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.MFN_M02_MADMessage2);
            SendSocketMessage(HL7Resources.MFN_M02_MADMessage2);

            WaitForMessageProcessed(start);

            string externalProviderId = PracticeRepository.ExternalSystemEntityMappings.OrderByDescending(j => j.Id).First().PracticeRepositoryEntityKey;
            ExternalProvider externalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().First(i => i.Id == externalProviderId.ToInt().Value);
            Assert.IsTrue(externalProvider.LastNameOrEntityName == "JAMAL");
        }


        [TestMethod]
        public void TestHL7InboundAdtA04Message()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95007);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);

            start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A04MessagePatient95007);

            externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);


        }

        [TestMethod]
        public void TestHL7InboundAdtA08Message()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95007);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);

            start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A08MessagePatient95007);
            externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);


        }

        /// <summary>
        ///   Tests the inbound S14 message from NG for patient 95010.
        /// </summary>
        [TestMethod]
        public void TestHL7InboundMessagePatient95010()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);

            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.SIU_S12MessagePatient95010);

            externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.SIU_S12MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.SIU_S14MessagePatient95010);

            externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.SIU_S14MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.GetMapping(NextGenExternalSystemName, PracticeRepositoryEntityId.Appointment, HL7Messages.GetPipeParser().Parse(HL7Resources.SIU_S12MessagePatient95010).CastTo<SIU_S12>().SCH.FillerAppointmentID.EntityIdentifier.Value);
            Assert.IsTrue(appointmentMapping != null && appointmentMapping.PracticeRepositoryEntityKey != "0");

            Appointment appointment = Common.PracticeRepository.Appointments.Include(i => i.Encounter).FirstOrDefault(i => i.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value);

            Assert.IsNotNull(appointment);
            Assert.IsTrue(appointment.Encounter.EncounterStatusId == (int)EncounterStatus.Questions);

            PracticeActivity[] practiceActivities = _legacyPracticeRepository.PracticeActivities.Where(i => i.AppointmentId.Value == appointment.Id).ToArray();
            Assert.IsTrue(practiceActivities.Length == 1);


        }

        /// <summary>
        ///   Do not change status from Questions with update and reschedule messages. Ok to cancel from Questions. No checkin after cancel.
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase1()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient9501CHECKIN, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S13MessagePatient95010, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010ARRIVED, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S15MessagePatient95010, "X", "X");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010KEPT, "X", "X");



        }

        /// <summary>
        ///   Do not change status from Questions with non-checkin updates or create appt msg. Checkout ok from Questions
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase2()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010KEPT, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010CHECKOUT, "H", "A");

            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, "H", "A");



        }

        /// <summary>
        ///   Do not change status from exam room; ok to check out from Waiting Room
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase3()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient9501CHECKIN, "W", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'E' ");
            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010CHECKOUT, "E", "A");

            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, "E", "A");



        }

        /// <summary>
        ///   Do not cancel from exam room ; Do not change status after Discharge
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase4()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S13MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient9501CHECKIN, "W", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'E' ");
            ProcessSiuMessages(HL7Resources.SIU_S15MessagePatient95010, "E", "A");

            ProcessSiuMessages(HL7Resources.SIU_S26MessagePatient95010, "E", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'D' ");
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update Appointments set ScheduleStatus = 'D' ");
            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "D", "D");

            ProcessSiuMessages(HL7Resources.SIU_S26MessagePatient95010, "D", "D");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient9501CHECKIN, "D", "D");

            ProcessSiuMessages(HL7Resources.SIU_S13MessagePatient95010, "D", "D");

            ProcessSiuMessages(HL7Resources.SIU_S17MessagePatient95010, "D", "D");


        }

        /// <summary>
        ///   Ok to checkout from Questions; do not cancel from checkout
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase5()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient9501CHECKIN, "W", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010CHECKOUT, "H", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "H", "A");

            ProcessSiuMessages(HL7Resources.SIU_S17MessagePatient95010, "H", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'E' ");
            // to prevent outbound message trigger
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'D' ");
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update Appointments set ScheduleStatus = 'D' ");
            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010CHECKOUT, "D", "D");



        }

        /// <summary>
        ///   Do not change from express checkout or after discharge
        /// </summary>
        [TestMethod]
        public void TestHL7SiuMessagesCase6()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(HL7Resources.SetUpPatient95010);
            StartProcessorAndConnectors();

            // prepare case
            DateTime start = DateTime.UtcNow;
            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95010);
            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start, new HL7Message(HL7Resources.ADT_A28MessagePatient95010).Id.ToString());
            Assert.IsNotNull(externalSystemMessage);

            // run case
            ProcessSiuMessages(HL7Resources.SIU_S12MessagePatient95010, null, "P");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010KEPT, "W", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'G' ");
            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010KEPT, "G", "A");

            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "G", "A");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update PracticeActivity set status = 'D' ");
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(" update Appointments set ScheduleStatus = 'D' ");
            ProcessSiuMessages(HL7Resources.SIU_S14MessagePatient95010BOOKED, "D", "D");

            ProcessSiuMessages(HL7Resources.SIU_S15MessagePatient95010, "D", "D");


        }

        private void ProcessSiuMessages(string message, string paStatus, string apScheduleStatus)
        {
            PracticeActivity practiceActivities;
            var myPaStatus = new string[2];
            var myApScheduleStatus = new string[2];

            //check values before update
            Model.Legacy.Appointment appointment = _legacyPracticeRepository.Appointments.OrderByDescending(i => i.AppointmentId).FirstOrDefault();
            if ((appointment != null))
            {
                myApScheduleStatus[0] = appointment.ScheduleStatus;
                int appointmentId = appointment.AppointmentId;
                practiceActivities = _legacyPracticeRepository.PracticeActivities.FirstOrDefault(i =>
                                                                                                 i.AppointmentId.Value == appointmentId)
                    ;
                if ((practiceActivities != null))
                {
                    myPaStatus[0] = practiceActivities.Status;
                }
            }

            // process message
            DateTime start = DateTime.UtcNow.AddMinutes(-5);
            SendSocketMessage(message);
            ExternalSystemMessage processedMessage = WaitForMessageProcessed(start, new HL7Message(message).Id.ToString());
            Assert.IsNotNull(processedMessage);

            //check values after update
            var l = new int[2];
            appointment = _legacyPracticeRepository.Appointments.OrderByDescending(i => i.AppointmentId).FirstOrDefault();
            if ((appointment != null))
            {
                l[0] = _legacyPracticeRepository.Appointments.ToArray().Length;
                Assert.AreEqual(l[0], 1);
                myApScheduleStatus[1] = appointment.ScheduleStatus;
                practiceActivities = _legacyPracticeRepository.PracticeActivities.FirstOrDefault(i => i.AppointmentId.Value == appointment.AppointmentId);
                if ((practiceActivities != null))
                {
                    l[1] = _legacyPracticeRepository.PracticeActivities.ToArray().Length;
                    Assert.AreEqual(l[1], 1);
                    myPaStatus[1] = practiceActivities.Status;
                }
            }
            bool result = (myPaStatus[1] == paStatus) & (myApScheduleStatus[1] == apScheduleStatus);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestHL7OutboundAdt28PatientLanguageAndEthnicity()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95012);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);



            Varies ethinicityComponent = HL7Messages.GetPipeParser().Parse(HL7Resources.ADT_A28MessagePatient95012).CastTo<V23ADT_A28>().PID.EthnicGroup.ExtraComponents.getComponent(0);
            string ethinicityDescription = ((GenericPrimitive)(ethinicityComponent.Data)).Value;
            ExternalSystemEntityMapping patientEthnicityMapping = PracticeRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity, i => i.PracticeRepositoryEntity).FirstOrDefault(i => i.ExternalSystemEntityKey == ethinicityDescription);
            Assert.IsNotNull(patientEthnicityMapping);

            Ethnicity patientEthnicity = Common.PracticeRepository.Ethnicities.FirstOrDefault(i => i.Name == patientEthnicityMapping.ExternalSystemEntityKey);
            Assert.IsNotNull(patientEthnicity);

            CE languageComponent = HL7Messages.GetPipeParser().Parse(HL7Resources.ADT_A28MessagePatient95012).CastTo<V23ADT_A28>().PID.PrimaryLanguage;
            ExternalSystemEntityMapping patientLanguageMapping = PracticeRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity.ExternalSystem, i => i.PracticeRepositoryEntity).FirstOrDefault(i => i.ExternalSystemEntityKey == languageComponent.Text.Value);
            Assert.IsNotNull(patientLanguageMapping);

            Language patientlanguage = Common.PracticeRepository.Languages.FirstOrDefault(i => i.Name == patientLanguageMapping.ExternalSystemEntityKey);
            Assert.IsNotNull(patientlanguage);
        }

        [TestMethod]
        public void TestHL7InboundAdtA04MessageWithEmailAddress()
        {
            StartProcessorAndConnectors();

            DateTime start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A28MessagePatient95007Email);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);

            start = DateTime.UtcNow;

            SendSocketMessage(HL7Resources.ADT_A04MessagePatient95007Email);

            externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);


        }
    }
}