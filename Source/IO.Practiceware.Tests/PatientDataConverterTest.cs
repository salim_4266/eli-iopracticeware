﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using IO.Practiceware;
using IO.Practiceware.DataConversion;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class PatientDataConverterTest : TestBase
    {
      
        private readonly IServiceProvider _serviceProvider;
        public PatientDataConverterTest()
        {
            _serviceProvider = Bootstrapper.Run<IServiceProvider>();
        }

        [TestMethod]
        public void TestPatientConverterRun()
        {
            if (DataConversionConfiguration.Current != null)
            {
                var argument = DataConversionConfiguration.Current.Converters.Where(c => c.ConverterName.ToLower() == "patientdataconverter" && c.IsEnabled == true).Select(c => c).FirstOrDefault();
                if (argument != null)
                {

                    var instance = (IDataConverter) _serviceProvider.GetService(argument.Converter);
                    instance.Run(argument);

                    IList<Patient> cachedPatients = PracticeRepository.Patients.ToList();
                    IList<PatientPhoneNumber> cachedPatientPhoneNumbers = PracticeRepository.PatientPhoneNumbers.ToList();
                    IList<Race> cachedRaces = PracticeRepository.Races.ToList();
                    IList<StateOrProvince> cachedStateOrProvinces = PracticeRepository.StateOrProvinces.ToList();
                    IList<Ethnicity> cachedEthinicities = PracticeRepository.Ethnicities.ToList();
                    IList<Language> cachedLanguages = PracticeRepository.Languages.ToList();
                    IList<PatientExternalProvider> cachedPatientExternalProviders = PracticeRepository.PatientExternalProviders.ToList();
                    IList<PatientAddress> cachedPatientAddresses = PracticeRepository.PatientAddresses.ToList();
                    IList<PatientEmailAddress> cachedPatientEmailAddresses = PracticeRepository.PatientEmailAddresses.ToList();
                    IList<PatientComment> cachedPatientComments = PracticeRepository.PatientComments.ToList();
                    PatientPhoneNumber patientHomePhone, patientWorkPhone, patientCellPhone;
                    PatientAddress address;
                    PatientPhoneNumberCommunicationPreference patientCommunicationPreference;
                    PatientEmailAddress email;
                    PatientComment patientComment, patientCommentNext;

                    var testPatientTable = argument.DataTable;
                    foreach (DataRow row in testPatientTable.Rows)
                    {
                        patientHomePhone = patientWorkPhone = patientCellPhone = null;
                        patientComment = patientCommentNext = null;
                        address = null;
                        patientCommunicationPreference = null;
                        email = null;

                        var patientAddress = new DataConversion.Utilities.Address(row.GetValueOrDefault<string>("PatientStreetAddress"), row.GetValueOrDefault<string>("PatientSuite"));

                        if (!string.IsNullOrEmpty(patientAddress.StreetAddress) || !string.IsNullOrEmpty(patientAddress.Suite))
                        {
                            patientAddress.ParseAddress(true);
                        }

                        string oldPrimaryDoctorId = row.GetValueOrDefault<string>("PatientPrimaryCarePhysician").ToUpper();
                        string oldReferringDoctorId = row.GetValueOrDefault<string>("PatientReferringPhysician").ToUpper();

                        /*Patient*/
                        var gender = row.GetValueOrDefault<string>("IOPatientGender");
                        var maritalStatus = row.GetValueOrDefault<string>("IOPatientMarital");
                        var language = row.GetValueOrDefault<string>("Language");
                        var patient = new Patient
                        {
                            PriorPatientCode = row.GetValueOrDefault<string>("OldPatientId"),
                            LastName = row.GetValueOrDefault<string>("PatientLastName"),
                            FirstName = row.GetValueOrDefault<string>("PatientFirstName"),
                            MiddleName = row.GetValueOrDefault<string>("PatientMiddleInitial"),
                            Salutation = row.GetValueOrDefault<string>("PatientSalutation"),
                            Suffix = row.GetValueOrDefault<string>("PatientNameReference"),
                            SocialSecurityNumber = row.GetValueOrDefault<string>("PatientSSN").ToAlphanumeric(),
                            Gender = gender.IsNullOrEmpty() ? Gender.Male : Enums.ToEnumFromDisplayNameOrDefault<Gender>(gender),
                            DateOfBirth = row.GetValueOrDefault<string>("PatientBirthDate").IsNullOrEmpty() ? null : DataConversion.Utilities.ValidateDate(row.GetValueOrDefault<string>("PatientBirthDate")).ToDateTime("yyyyMMdd"),
                            MaritalStatus = gender.IsNullOrEmpty() ? MaritalStatus.Single : Enums.ToEnumFromDisplayNameOrDefault<MaritalStatus>(maritalStatus),
                            DefaultUserId = row.GetValueOrDefault<string>("IOPracticeDoctorId").ToInt() ?? 0
                        };
                        if (!language.IsNullOrEmpty())
                        {
                            patient.LanguageId = cachedLanguages.Where(l => l.Name == language).Select(l => l.Id).FirstOrDefault();
                        }

                        /* Ethnicity */
                        string drEnthinicity = row.GetValueOrDefault<string>("Ethnicity");
                        if (!drEnthinicity.IsNullOrEmpty())
                        {
                            var enthnicity = cachedEthinicities.Where(e => e.Name == drEnthinicity).Select(e => e).FirstOrDefault();
                            if (enthnicity != null)
                            {
                                patient.EthnicityId = enthnicity.Id;
                            }
                        }

                        /* PatientPhoneNumber */
                        var homeNumber = row.GetValueOrDefault<string>("PatientHomePhone").ToAlphanumeric();
                        if (!homeNumber.IsNullOrEmpty() && homeNumber.Length == 10)
                        {
                            patientHomePhone = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Home,
                                AreaCode = row.GetValueOrDefault<string>("PatientHomePhone").ToAlphanumeric().Substring(0, 3),
                                ExchangeAndSuffix = row.GetValueOrDefault<string>("PatientHomePhone").ToAlphanumeric().Substring(3, row.GetValueOrDefault<string>("PatientHomePhone").Length - 3)
                            };
                            patient.PatientPhoneNumbers.Add(patientHomePhone);

                        }
                        var workNumber = row.GetValueOrDefault<string>("PatientWorkPhone").ToAlphanumeric();
                        if (!workNumber.IsNullOrEmpty() && workNumber.Length == 10)
                        {
                            patientWorkPhone = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Business,
                                AreaCode = workNumber.Substring(0, 3),
                                ExchangeAndSuffix = workNumber.Substring(3, workNumber.Length - 3)
                            };
                            patient.PatientPhoneNumbers.Add(patientWorkPhone);

                        }
                        var cellNumber = row.GetValueOrDefault<string>("PatientCellPhone").ToAlphanumeric();
                        if (!cellNumber.IsNullOrEmpty() && cellNumber.Length == 10)
                        {
                            patientCellPhone = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Cell,
                                AreaCode = cellNumber.Substring(0, 3),
                                ExchangeAndSuffix = cellNumber.Substring(3, cellNumber.Length - 3)
                            };
                            patient.PatientPhoneNumbers.Add(patientCellPhone);
                        }

                        /* PatientAddress */
                        string abbreviation = row.GetValueOrDefault<string>("PatientState");
                        var stateOrProvince = cachedStateOrProvinces.Where(sop => sop.Abbreviation == abbreviation).Select(sop => sop).EnsureNotDefault();
                        if (stateOrProvince.Any())
                        {
                            address = new PatientAddress
                            {
                                Line1 = patientAddress.StreetAddress,
                                Line2 = patientAddress.Suite,
                                PatientAddressTypeId = (int) PatientAddressTypeId.Home,
                                City = row.GetValueOrDefault<string>("PatientCity"),
                                PostalCode = row.GetValueOrDefault<string>("PatientZip"),
                                StateOrProvinceId = stateOrProvince.First().Id
                            };
                            patient.PatientAddresses.Add(address);
                        }
                        /*PatientPhoneNumberCommunicationPreference*/
                        string emergencyPhone = row.GetValueOrDefault<string>("PatientEmergencyContactPhone").ToAlphanumeric();
                        if (!emergencyPhone.IsNullOrEmpty() && emergencyPhone.Length >= 10)
                        {
                            var areaCode = emergencyPhone.Substring(0, 3);
                            var exchangeAndSuffix = emergencyPhone.Substring(3, cellNumber.Length - 3);

                            var patientPrefferedPhone = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Home,
                                AreaCode = areaCode,
                                ExchangeAndSuffix = exchangeAndSuffix
                            };

                            var matchinPrefferedPhone = cachedPatientPhoneNumbers.Where(ppn => ppn.AreaCode == areaCode && ppn.ExchangeAndSuffix == exchangeAndSuffix).Select(ppn => ppn).FirstOrDefault();
                            if (matchinPrefferedPhone == null)
                            {
                                patient.PatientPhoneNumbers.Add(patientPrefferedPhone);
                            }
                            patientCommunicationPreference = new PatientPhoneNumberCommunicationPreference
                            {
                                CommunicationMethodType = CommunicationMethodType.PhoneCall,
                                PatientCommunicationType = PatientCommunicationType.Emergency,
                                DestinationId = patient.PatientAddresses.Select(pa => pa.Id).FirstOrDefault(),
                                PatientPhoneNumber = patientPrefferedPhone,
                                IsOptOut = true //not sure if it is true always
                            };
                            patient.PatientCommunicationPreferences.Add(patientCommunicationPreference);

                        }
                        /* PatientEmail */
                        var emailAddress = row.GetValueOrDefault<string>("PatientEmail");
                        if (!emailAddress.IsNullOrEmpty())
                        {
                            email = new PatientEmailAddress
                            {
                                EmailAddressType = EmailAddressType.Personal, //Defaulted to personal as no value are coming in DR.
                                Value = emailAddress,
                            };
                            patient.PatientEmailAddresses.Add(email);
                        }

                        /* PatientComment */
                        var patientComment1 = row.GetValueOrDefault<string>("PatientComments");
                        if (!patientComment1.IsNullOrEmpty())
                        {
                            patientComment = new PatientComment
                            {
                                PatientCommentType = PatientCommentType.PatientCommunicationPreferenceNotes,
                                Value = patientComment1,
                            };
                            patient.PatientComments.Add(patientComment);
                        }
                        var patientComment2 = row.GetValueOrDefault<string>("OldPatientId2");
                        if (!patientComment2.IsNullOrEmpty())
                        {

                            patientCommentNext = new PatientComment
                            {
                                PatientCommentType = PatientCommentType.PatientCommunicationPreferenceNotes,
                                Value = patientComment2,
                            };
                            patient.PatientComments.Add(patientCommentNext);

                        }


                        string raceValue = row.GetValueOrDefault<string>("Race");
                        if (!raceValue.IsNullOrEmpty())
                        {
                            var race = cachedRaces.Where(r => r.Name == raceValue).Select(r => r).FirstOrDefault();
                            if (race != null)
                            {
                                patient.Races.Add(race);
                                cachedRaces.Add(race);
                            }
                        }

                        // Lookup referring doctor and primary care physician.
                        if (!String.IsNullOrEmpty(oldReferringDoctorId))
                        {
                            var referringPhysician = cachedPatientExternalProviders.Where(pep => pep.ExternalProviderId.ToString() == oldReferringDoctorId).Select(pep => pep).FirstOrDefault();
                            if (referringPhysician != null)
                            {
                                patient.ExternalProviders.Add(referringPhysician);
                            }
                        }

                        if (!String.IsNullOrEmpty(oldPrimaryDoctorId))
                        {
                            var primaryCarePhysician = cachedPatientExternalProviders.Where(pep => pep.ExternalProviderId.ToString() == oldPrimaryDoctorId).Select(pep => pep).FirstOrDefault();
                            if (primaryCarePhysician != null)
                            {
                                patient.ExternalProviders.Add(primaryCarePhysician);
                            }

                        }
                        address.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();
                        patient.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                        var comparerDob = patient.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty);

                        var matchingPatients = from ptn in cachedPatients
                            where ptn.FirstName == patient.FirstName &&
                                  ptn.LastName == patient.LastName &&
                                  CommonQueries.ConvertToMMDDYYYYString(ptn.DateOfBirth) == comparerDob
                            select ptn;

                        bool isDuplicatePatient = matchingPatients.Any();

                        if (!isDuplicatePatient)
                        {
                            if (patientHomePhone != null)
                            {
                                Assert.IsNotNull(cachedPatientPhoneNumbers.Where(phn => phn.AreaCode == patientHomePhone.AreaCode &&
                                                                                        phn.ExchangeAndSuffix == patientHomePhone.ExchangeAndSuffix &&
                                                                                        phn.PatientPhoneNumberType == PatientPhoneNumberType.Home)
                                    .Select(phn => phn).First(), "Could not validate the patient home phone number");
                            }
                            if (patientWorkPhone != null)
                            {
                                Assert.IsNotNull(cachedPatientPhoneNumbers.Where(phn => phn.AreaCode == patientWorkPhone.AreaCode &&
                                                                                        phn.ExchangeAndSuffix == patientWorkPhone.ExchangeAndSuffix &&
                                                                                        phn.PatientPhoneNumberType == PatientPhoneNumberType.Business)
                                    .Select(phn => phn).FirstOrDefault(), "Could not validate the patient work phone number");
                            }

                            if (patientCellPhone != null)
                            {
                                Assert.IsNotNull(
                                    cachedPatientPhoneNumbers.Where(phn => phn.AreaCode == patientCellPhone.AreaCode &&
                                                                           phn.ExchangeAndSuffix == patientCellPhone.ExchangeAndSuffix &&
                                                                           phn.PatientPhoneNumberType == PatientPhoneNumberType.Cell)
                                        .Select(phn => phn).FirstOrDefault(), "Could not validate the patient cell phone number");
                            }

                            if (address != null)
                            {
                                var tempAddress = cachedPatientAddresses.Where(pa => pa.Line1 == address.Line1 && pa.Line2 == address.Line2 && pa.PatientAddressTypeId == address.PatientAddressTypeId && pa.City == address.City &&
                                                                                     pa.PostalCode == address.PostalCode && pa.StateOrProvinceId == address.StateOrProvinceId)
                                    .Select(pa => pa).FirstOrDefault();
                                Assert.IsNotNull(cachedPatientAddresses.Where(pa => pa.Line1 == address.Line1 && pa.Line2 == address.Line2 && pa.PatientAddressTypeId == address.PatientAddressTypeId && pa.City == address.City &&
                                                                                    pa.PostalCode == address.PostalCode && pa.StateOrProvinceId == address.StateOrProvinceId)
                                    .Select(pa => pa).FirstOrDefault(), "Could not validate the patient address");
                            }

                            if (email != null)
                            {
                                Assert.IsNotNull(
                                    cachedPatientEmailAddresses.Where(pe => pe.EmailAddressType == EmailAddressType.Personal &&
                                                                            pe.Value == email.Value).Select(pe => pe).FirstOrDefault(), "Could not validate the patient email address");
                            }
                            if (patientComment != null)
                            {
                                Assert.IsNotNull(cachedPatientComments.Where(pc => pc.PatientCommentType == patientComment.PatientCommentType &&
                                                                                   pc.Value == patientComment.Value).Select(pc => pc).FirstOrDefault(), "Could not validate the patient comment");

                            }
                            if (patientCommentNext != null)
                            {
                                Assert.IsNotNull(cachedPatientComments.Where(pc => pc.PatientCommentType == patientComment.PatientCommentType &&
                                                                                   pc.Value == patientComment.Value).Select(pc => pc).FirstOrDefault(), "Could not validate the patient comment");
                            }

                            Assert.IsNotNull(cachedPatients.Where(p => p.PriorPatientCode == patient.PriorPatientCode && p.LastName == patient.LastName && p.FirstName == patient.FirstName && p.MiddleName == patient.MiddleName &&
                                                                       p.Salutation == patient.Salutation && p.Salutation == patient.Salutation && p.Suffix == patient.Suffix && p.SocialSecurityNumber == patient.SocialSecurityNumber &&
                                                                       p.DateOfBirth == patient.DateOfBirth && p.MaritalStatus == patient.MaritalStatus && p.DefaultUserId == patient.DefaultUserId).Select(p => p).SingleOrDefault(),
                                "Could not validate the patient");
                        }
                    }

                }
            }
        }

    }
}
