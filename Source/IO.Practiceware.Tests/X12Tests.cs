﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    /// <summary>
    ///   Test methods for X12 message generation and parsing.
    /// </summary>
    [TestClass]
    public class X12Tests : X12MetadataTestsBase
    {
        private static readonly DateTime TestMessageDate = new DateTime(2012, 2, 1, 11, 23, 30);

        // Test #1
        [TestMethod]
        public void Test837MessageGenerationAetnaOnly()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837AetnaOnly);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldLL");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837AetnaOnly);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #2
        [TestMethod]
        public void Test837MessageGenerationBlueShieldPrimarySelfAetnaSecondarySpouse()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837BlueShieldPrimarySelfAetnaSecondarySpouse);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldKK");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837BlueShieldPrimarySelfAetnaSecondarySpouse);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #3
        [TestMethod]
        public void Test837MessageGenerationMedicarePrimaryOnly()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MedicarePrimaryOnly);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldJJ");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MedicarePrimaryOnly);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #4
        [TestMethod]
        public void Test837MessageGenerationMedicaidOnly()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MedicaidOnly);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldMM");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MedicaidOnly);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #5
        [TestMethod]
        public void Test837MessageGenerationMedicareMedicaid()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MedicareMedicaid);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldNN");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MedicareMedicaid);
            expectedMessage = ReplaceDateTimes(expectedMessage);
            Assert.AreEqual(expectedMessage, x12Message);
        }


        //Test #6
        [TestMethod]
        public void Test837MessageGenerationAetnaPrimarySpouseCignaSecSelf()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837AetnaPrimarySpouseCignaSecSelf);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldPP");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837AetnaPrimarySpouseCignaSecSelf);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        //Test #7
        [TestMethod]
        public void Test837MessageGenerationBlueShieldSelfPriCignaSpouseSec()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837BlueShieldSelfPriCignaSpouseSec);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldQQ");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837BlueShieldSelfPriCignaSpouseSec);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #8
        [TestMethod]
        public void Test837MessageGenerationBlueShieldSelfVisionSpouse()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837BlueShieldSelfVisionSpouse);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldRR");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837BlueShieldSelfVisionSpouse);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #9
        [TestMethod]
        public void Test837MessageGenerationVspDrugServiceOnly()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837VSPDrugServiceOnly);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldSS");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837VSPDrugServiceOnly);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        //Test #10
        [TestMethod]
        public void Test837MessageGenerationCignaPrimaryMedicarePrimaryArchivedAetnaSec()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837CignaPrimaryMedicarePrimaryArchivedAetnaSec);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldTT");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837CignaPrimaryMedicarePrimaryArchivedAetnaSec);
            Assert.AreEqual(expectedMessage, x12Message);
        }


        // Test #11
        [TestMethod]
        public void Test837MessageGenerationMedicareReportPatientPaid()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MedicareReportPatientPaid);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldUU");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MedicareReportPatientPaid);
            Assert.AreEqual(expectedMessage, x12Message);
        }


        // Test #12
        [TestMethod]
        public void Test837MessageGenerationMedicaidResubmit()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MedicaidResubmit);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldVV");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MedicaidResubmit);
            expectedMessage = ReplaceDateTimes(expectedMessage);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #13 
        [TestMethod]
        public void Test837MessageGenerationThreeInsurers()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837ThreeInsurers);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldWW");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837ThreeInsurers);
            expectedMessage = ReplaceDateTimes(expectedMessage);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #14
        [TestMethod]
        public void Test837MessageGenerationVisionClaim()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837VisionClaim);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldXX");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837VisionClaim);
            Assert.AreEqual(expectedMessage, x12Message);
        }


        // Test #15
        [TestMethod]
        public void Test837MessageGenerationMultipleClaims()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837MultipleClaims);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldOO");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837MultipleClaims);
            expectedMessage = ReplaceDateTimes(expectedMessage);
            Assert.AreEqual(expectedMessage, x12Message);
        }


        // Test #16
        [TestMethod]
        public void Test837MessageGenerationCignaPrimaryMedicareSecondaryBakersTertiary()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837CignaPrimaryMedicareSecondaryBakersTertiary);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldYY");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837CignaPrimaryMedicareSecondaryBakersTertiary);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #17
        [TestMethod]
        public void Test837MessageGenerationFacilityClaims()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837FacilityClaims);

            ExternalSystemMessage externalSystemMessage = AddQueuedBillingServiceTransactionsAndGetMessage();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldII");

            // Create the X12 message
            IMessage message = producer.ProduceMessage(externalSystemMessage);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837Facility);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #18
        [TestMethod]
        public void Test837MessageGenerationFacilitySecondaryClaim()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837FacilitySecondaryClaims);

            ExternalSystemMessage externalSystemMessage = AddQueuedBillingServiceTransactionsAndGetMessage();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldZZ");

            // Create the X12 message
            IMessage message = producer.ProduceMessage(externalSystemMessage);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837FacilitySecondary);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #19
        [TestMethod]
        public void Test837MessageGenerationAetnaOnlyWithCliaCertificationNumber()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837AetnaOnlyCliaCertification);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldLL");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837AetnaOnlyCliaCertification);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        // Test #20
        [TestMethod]
        public void Test837MessageDmeClaim()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(X12Resources.SetUp837DMEClaim);

            IEnumerable<BillingServiceTransaction> billingServiceTransactions = AddQueuedBillingServiceTransactions();

            // Get the message producer
            var producer = GetMessageProducer837ForTest("abcdaldLL");

            // Create the X12 message

            var interchange = ((X12Message)producer.ProduceMessage(billingServiceTransactions.Select(t => t.Id).ToArray())).Messages.FirstOrDefault();

            var message = new X12Message(interchange);
            string x12Message = message.ToString();

            var expectedMessage = ReplaceIds(X12Resources.Message837DMEClaim);
            Assert.AreEqual(expectedMessage, x12Message);
        }

        private static MessageProducer837 GetMessageProducer837ForTest(string reference)
        {
            var utilities = Common.ServiceProvider.GetService<MessageProducerUtilities>();
            utilities.DateTime = TestMessageDate;
            utilities.ReferenceIdentifier = reference;
            var getProducer = Common.ServiceProvider.GetService<Func<MessageProducerUtilities, MessageProducer837>>();
            var producer = getProducer(utilities);
            return producer;
        }

        private static string ReplaceDateTimes(string text)
        {
            const string dateTimeVariableName = "{DateTime}";
            int stringDateIndex = text.IndexOf(dateTimeVariableName);
            while (stringDateIndex != -1)
            {
                text = ReplaceFirst(text, dateTimeVariableName, DateTime.Today.ToString("yyyyMMdd"));
                stringDateIndex = text.IndexOf(dateTimeVariableName);
            }
            return text;
        }

        private static string ReplaceIds(string text)
        {
            return ReplaceItemIds(ReplacePatientIds(text));
        }

        private static string ReplacePatientIds(string text)
        {
            foreach (int patientid in GetPatientIdsInOrder())
            {
                text = ReplaceFirst(text, "{PatientId}", patientid.ToString());
            }
            return text;
        }

        private static IEnumerable<BillingServiceTransaction> AddQueuedBillingServiceTransactions()
        {
            var externalSystemMessage = new ExternalSystemMessage
            {
                ExternalSystemMessagePracticeRepositoryEntities = GetQueuedBillingServiceTransactions().Select(bst => new ExternalSystemMessagePracticeRepositoryEntity
                {
                    PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.BillingServiceTransaction,
                    PracticeRepositoryEntityKey = bst.Id.ToString()
                }).ToList()
            };

            IEnumerable<Guid> billingServiceTransactionIds = externalSystemMessage.ExternalSystemMessagePracticeRepositoryEntities.Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.BillingServiceTransaction).Select(i => new Guid(i.PracticeRepositoryEntityKey)).ToArray();
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            int?[] invoiceIds = practiceRepository.BillingServiceTransactions.Where(i => billingServiceTransactionIds.Contains(i.Id)).
                    Select(bst => bst.BillingService.InvoiceId).Distinct().ToArray();

            Invoice[] invoices = practiceRepository.Invoices.Where(i => invoiceIds.Contains(i.Id)).ToArray();

            invoices.LoadForClaimFileGeneration(practiceRepository);

            BillingServiceTransaction[] billingServiceTransactions = invoices.SelectMany(i => i.InvoiceReceivables).
                SelectMany(ir => ir.BillingServiceTransactions).
                Where(bst => billingServiceTransactionIds.Contains(bst.Id)).Distinct().ToArray();

            BillingServiceTransaction[] electronicServiceTransactions = billingServiceTransactions.RemoveAll(billingServiceTransactions.Where(bst => bst.MethodSent == MethodSent.Paper).ToArray());
            return electronicServiceTransactions;
        }

        // Creates ExternalSystemMessage that is sent to 837MessageProducer; currently used in unit tests
        private static ExternalSystemMessage AddQueuedBillingServiceTransactionsAndGetMessage()
        {
            var externalSystemMessage = new ExternalSystemMessage
            {
                ExternalSystemMessagePracticeRepositoryEntities = GetQueuedBillingServiceTransactions().Select(bst => new ExternalSystemMessagePracticeRepositoryEntity
                {
                    PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.BillingServiceTransaction,
                    PracticeRepositoryEntityKey = bst.Id.ToString()
                }).ToList()
            };

            return externalSystemMessage;
        }

        // Used in replacing PatientIds and ItemIds for expected unit test files
        public static BillingServiceTransaction[] GetQueuedBillingServiceTransactions()
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWorkScope())
            {
                var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
                practiceRepository.IsLazyLoadingEnabled = true;

                return practiceRepository.BillingServiceTransactions.Where(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued && bst.MethodSent == MethodSent.Electronic).ToArray();
            }
        }

        private static IEnumerable<int> GetPatientIdsInOrder()
        {
            IEnumerable<Guid> ids = GetQueuedBillingServiceTransactions().Select(t => t.Id).Distinct();

            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWorkScope())
            {
                var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
                practiceRepository.IsLazyLoadingEnabled = true;

                List<BillingServiceTransaction> billingServiceTransactions = practiceRepository.BillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToList();

                //if ((patientId.HasValue))
                //{
                //    billingServiceTransactions = billingServiceTransactions.Where(bst => bst.InvoiceReceivable.Invoice.Encounter.PatientId == patientId.Value).ToList();
                //}

                IEnumerable<BillingServiceTransaction> institutionalBillingServiceTransactions = billingServiceTransactions.Where(est => practiceRepository.InsurerClaimForms.
                                                                                                                                             Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                                                        icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                                        icf.ClaimFormTypeId == (int)ClaimFormTypeId.Institutional837));

                List<BillingServiceTransaction> professionalBillingServiceTransactions = billingServiceTransactions.Where(est => practiceRepository.InsurerClaimForms.
                                                                                                            Where(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                         icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType).
                                                                                                            IsNullOrEmpty() ||
                                                                                                            practiceRepository.InsurerClaimForms.Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                                                       icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                                       icf.ClaimFormTypeId == (int)ClaimFormTypeId.Professional837)).ToList();

                IEnumerable<BillingServiceTransaction> professionalFacilityBillingServiceTransactions = professionalBillingServiceTransactions.Where(pbst => pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Facility);
                IEnumerable<BillingServiceTransaction> professionalMedicalBillingServiceTransactions = professionalBillingServiceTransactions.Where(pbst => pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Professional ||
                                                                                                                                                            pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Vision);

                // grab pids from each bst then union them together
                List<int> patientIds = institutionalBillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToArray().GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.First().InvoiceReceivable.Invoice.Encounter.PatientId).ToList();
                patientIds.AddRange(professionalFacilityBillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToArray().GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.First().InvoiceReceivable.Invoice.Encounter.PatientId));
                patientIds.AddRange(professionalMedicalBillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToArray().GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.First().InvoiceReceivable.Invoice.Encounter.PatientId));
                return patientIds.ToArray();
                //IEnumerable<int> patientIds = practiceRepository.BillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToArray().GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.First().InvoiceReceivable.Invoice.Encounter.PatientId);
                //return patientIds.ToArray();

                //List<int> billingServiceIds = institutionalBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingServiceId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)).Select(i => i).ToList();
                //billingServiceIds.AddRange(professionalFacilityBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingServiceId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)).Select(i => i));
                //billingServiceIds.AddRange(professionalMedicalBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingServiceId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)).Select(i => i));

            }
        }


        private static IEnumerable<int> GetBillingServiceIdsInOrder(int? patientId = null)
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWorkScope())
            {
                var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
                practiceRepository.IsLazyLoadingEnabled = true;

                IEnumerable<Guid> ids = GetQueuedBillingServiceTransactions().Select(t => t.Id).Distinct();

                List<BillingServiceTransaction> billingServiceTransactions = practiceRepository.BillingServiceTransactions.Where(bs => ids.Contains(bs.Id)).ToList();

                if ((patientId.HasValue))
                {
                    billingServiceTransactions = billingServiceTransactions.Where(bst => bst.InvoiceReceivable.Invoice.Encounter.PatientId == patientId.Value).ToList();
                }

                IEnumerable<BillingServiceTransaction> institutionalBillingServiceTransactions = billingServiceTransactions.Where(est => practiceRepository.InsurerClaimForms.
                                                                                                                                             Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                                                        icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                                        icf.ClaimFormTypeId == (int)ClaimFormTypeId.Institutional837));

                List<BillingServiceTransaction> professionalBillingServiceTransactions = billingServiceTransactions.Where(est => practiceRepository.InsurerClaimForms.
                                                                                                            Where(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                         icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType).
                                                                                                            IsNullOrEmpty() ||
                                                                                                            practiceRepository.InsurerClaimForms.Any(icf => icf.InsurerId == est.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId &&
                                                                                                                                                       icf.InvoiceType == est.InvoiceReceivable.Invoice.InvoiceType &&
                                                                                                                                                       icf.ClaimFormTypeId == (int)ClaimFormTypeId.Professional837)).ToList();

                IEnumerable<BillingServiceTransaction> professionalFacilityBillingServiceTransactions = professionalBillingServiceTransactions.Where(pbst => pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Facility);
                IEnumerable<BillingServiceTransaction> professionalMedicalBillingServiceTransactions = professionalBillingServiceTransactions.Where(pbst => pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Professional ||
                                                                                                                                                            pbst.InvoiceReceivable.Invoice.InvoiceType == InvoiceType.Vision);

                //List<int> professionalBillingServiceIds = professionalBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingServiceId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)).Select(i => i).ToList();
                List<int> billingServiceIds = institutionalBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingService.OrdinalId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)).ToList();
                billingServiceIds.AddRange(professionalFacilityBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingService.OrdinalId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)));
                billingServiceIds.AddRange(professionalMedicalBillingServiceTransactions.GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingService.OrdinalId)).SelectMany(i => i.Select(bst => bst.BillingServiceId)));

                return billingServiceIds.ToArray();

                //IQueryable<BillingServiceTransaction> billingServiceTransactions = practiceRepository.BillingServiceTransactions.Where(bs => ids.Contains(bs.Id));

                //if ((patientId.HasValue))
                //{
                //    billingServiceTransactions = billingServiceTransactions.Where(bst => bst.InvoiceReceivable.Invoice.Encounter.PatientId == patientId.Value);
                //}

                //IEnumerable<int> billingServiceIds = billingServiceTransactions.ToArray().GroupBy(bs => bs.InvoiceReceivableId).OrderBy(i => i.Key).Select(i => i.OrderBy(bst => bst.BillingService.OrdinalId)).SelectMany(i => i.Select(bst => bst.BillingServiceId));
                //return billingServiceIds.ToArray();
            }
        }

        private static string ReplaceItemIds(string text)
        {
            IEnumerable<int> billingServiceIds = GetBillingServiceIdsInOrder();
            foreach (int id in billingServiceIds)
            {
                text = ReplaceFirst(text, "{ItemId}", id.ToString());
            }
            return text;
        }

        private static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
}
