﻿using System.Data.SqlClient;
using System.Security.Authentication;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using Soaf;
using Soaf.Data;
using Soaf.Security;

namespace IO.Practiceware.Tests
{
    public static class PrepAuthentication
    {
        private const string UserPid = "9997";

        public static void CreateUser()
        {
            new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString).RunScriptAndDispose(ModelTestsResources.ModelTestsCreateUser);
        }

        public static UserPrincipal Login()
        {
            return Login(ConfigurationManager.AuthenticationToken);
        }

        public static UserPrincipal Login(string authenticationToken)
        {
            var userName = "{0}@{1}".FormatWith(UserPid, authenticationToken);
            if (UserContext.Current.Login(userName, UserPid) <= 0)
            {
                throw new AuthenticationException("Failed to authenticate as: " + userName);
            }
            return (UserPrincipal)PrincipalContext.Current.Principal;
        }

        public static void Logout()
        {
            UserContext.Current.Logout();
        }

    }
}
