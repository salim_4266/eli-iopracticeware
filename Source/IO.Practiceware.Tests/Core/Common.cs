using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using Aspose.Words;
using ICSharpCode.SharpZipLib.Zip;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.DbMigration.Replication;
using IO.Practiceware.DbMigration;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Presentation;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using IO.Practiceware.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Web.Administration;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Presentation;
using Soaf.Configuration;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Web.Client;
using AppointmentType = IO.Practiceware.Model.AppointmentType;
using ConfigurationManager = IO.Practiceware.Configuration.ConfigurationManager;
using Task = IO.Practiceware.Model.Task;
using Encounter = IO.Practiceware.Model.Encounter;
using Group = IO.Practiceware.Model.Group;
using User = IO.Practiceware.Model.User;

namespace IO.Practiceware.Tests
{
    [TestClass]
    internal static class Common
    {
        /// <summary>
        ///   Set to false to increase unit test startup time by skipping database initialization. Do NOT check in if you change this.
        /// </summary> 
        public static bool InitializeTestDatabases = true;

        /// <summary>
        /// The create practice repository test from backup. If false, uses a creation SQL script. If true, uses embedded bak file.
        /// </summary>
        public static bool CreatePracticeRepositoryTestFromBackup = true;

        /// <summary>
        ///   Set to false to skip running the migrator. Do NOT check in if you change this.
        /// </summary>
        public static bool RunMigrator = true;

        /// <summary>
        /// Set to true to configure wcf end points in the configuration file to tell the ado services to use wcf to make data calls, instead of locally.
        /// </summary>
        public static bool ConfigureWcfServices = true;

        /// <summary>
        /// Do not change or check in. Keep this set to false.  Special use.
        /// </summary>
        internal static bool DoNotInitializeApplication = false;


        private static string _baseBakFilePath;
        private static Lazy<string> _cachedDbSuffix;

        public static IServiceProvider ServiceProvider { get; set; }

        public static IPracticeRepository PracticeRepository { get; set; }

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool Wow64RevertWow64FsRedirection(ref IntPtr ptr);

        private const string PrTestDatabase = "PracticeRepositoryTest";
        private const string MveTestDatabase = "MVERepositoryTest";
        internal const string DefaultApplicationServerClientName = "Default";

        internal static UserPrincipal Principal { get; private set; }
#if DISABLE_REPLICATION_TESTS
        [AssemblyInitialize]
#endif
        public static void OnAssemblyInitialize(TestContext testContext)
        {
            using (new TimedScope(s => Trace.TraceInformation("Ran initialization for unit tests in {0}.".FormatWith(s))))
            {
                BackgroundThreadOnlyAttribute.IsEnabled = false;

                UpdateMonitorTests.TryUninstallCurrentIoServer();

                EnableDtcNetworkAccess();

                var iisReset = Process.Start(new ProcessStartInfo { FileName = "iisreset.exe", UseShellExecute = false, CreateNoWindow = true });

                ConfigurationManager.Load();

                if (ConfigureWcfServices)
                {
                    // change configuration so that ado goes to the cloud
                    ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService = true;
                    ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = true;
                    ConfigurationManager.Save();
                }

                // always need new MVE test DB since it doesn't support running twice
                var dbCreationTasks = CreateTestDatabase("MVERepository", Resources.Create_MVERepositoryTest, MveTestDatabase).CreateEnumerable().ToList();

                if (InitializeTestDatabases)
                {
                    using (new TimedScope(s => Trace.TraceInformation("Created test database in {0}.".FormatWith(s))))
                    {
                        // Create PracticeRepositoryTest database                
                        if (CreatePracticeRepositoryTestFromBackup)
                        {
                            dbCreationTasks.Add(CreateTestDatabaseFromBackup(Resources.PracticeRepositoryTest, PrTestDatabase, "PracticeRepository"));
                        }
                        else
                        {
                            dbCreationTasks.Add(CreateTestDatabase("PracticeRepository", Resources.Create_PracticeRepositoryTest, PrTestDatabase));
                        }
                    }
                }
                else // alter config file connection strings
                {
                    var prTestDatabaseName = PrepareComputerSpecificDatabaseName(PrTestDatabase);
                    // set the initial catalog of the PracticeRepository connection string to the computer specific name
                    UpdateConnectionStringAndServerDataPath("PracticeRepository", prTestDatabaseName);
                    UpdateConnectionStringAndServerDataPath("PracticeRepositoryReplicating", prTestDatabaseName);

                    var mveTestDatabaseName = PrepareComputerSpecificDatabaseName(MveTestDatabase);
                    // set the initial catalog of the MVERepository connection string to the computer specific name
                    UpdateConnectionStringAndServerDataPath("MVERepository", mveTestDatabaseName);
                }

                System.Threading.Tasks.Task.WaitAll(dbCreationTasks.ToArray());

                // warm up web site                
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    if (iisReset != null && !iisReset.HasExited)
                    {
                        iisReset.WaitForExit();
                    }
                    try
                    {
                        new WebClient().DownloadData(new Uri("https://localhost/IOPracticeware/Services/AuthenticationService"));
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceWarning("Warming up IIS failed " + ex);
                    }
                });

                if (InitializeTestDatabases && !CreatePracticeRepositoryTestFromBackup)
                {
                    // add test data to the database tables
                    using (new TimedScope(s => Trace.TraceInformation("Ran first metadata script in {0}.".FormatWith(s))))
                        new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString).RunScriptAndDispose(Resources.PracticeRepositoryMetadata);
                }

                if (RunMigrator)
                {
                    // Force any changed migration to run
                    var section = ConfigurationManager.GetSectionWithRetry<DbMigrationsConfiguration>();
                    section.Migrations.ForEach(m => m.MigrationLockoutPeriod = DateTime.MinValue.ToString("u"));

                    // start migrating
                    Migrator.ShowElapsedTime = true;
                    Migrator.Run();

                    // Reset
                    section.Migrations.ForEach(m => m.MigrationLockoutPeriod = null);
                }

                AddTestingProcedures();

                // must create user BEFORE backing up the baseline DB
                PrepAuthentication.CreateUser();

                _baseBakFilePath = BackupDatabase(DbConnectionFactory.PracticeRepository.ConnectionString, "Base");

                var license = new License();
                license.SetLicense((typeof(IDocumentGenerationService).Assembly.GetManifestResourceStream("IO.Practiceware.Services.Documents.Aspose.Words.lic")));

                if (!DoNotInitializeApplication)
                {
                    InitializeApplication();
                }

                // Switch to thread based principal context and remote service authentication
                RemoteServiceAttribute.AuthenticationScope = RemoteServiceAuthenticationScope.PrincipalIdentity;
                PrincipalContext.Current.Scope = PrincipalScope.Thread;

                using (new TimedScope(s => Trace.TraceInformation("Logged in in {0}.".FormatWith(s))))
                    Principal = PrepAuthentication.Login();

                Trace.Listeners.OfType<LoggingDatabaseTraceListener>().ToList().ForEach(Trace.Listeners.Remove);

                // Turn on file manager's tracing
                FileManager.TraceSource.Switch = new SourceSwitch("FileManagerTraceSwitch") { Level = SourceLevels.All };

                // Initialize support for parallel tests execution
                TestBase.Initialize();
            }
        }


        private static void AddTestingProcedures()
        {
            using (var connection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
            {
                connection.RunScript(ADODBVbScripts.CreateProcedurePatientDocumentList);
                connection.RunScript(Resources.PatientFinancialViewOnInsertTrigger);
                connection.RunScriptAndDispose(Resources.AlterIndexes);
            }
        }

        /// <summary>
        /// Copies the database at the connection string specified to the specified target.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="targetDatabaseName">Name of the X12 database.</param>
        internal static void CopyDatabase(string connectionString, string targetDatabaseName)
        {
            var bakFilePath = BackupDatabase(connectionString);

            RestoreDatabase(bakFilePath, targetDatabaseName, connectionString);
        }


        /// <summary>
        /// Creates the database from _baseBakFilePath.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        public static void CreateDatabaseFromBase(string databaseName)
        {
            RestoreDatabase(_baseBakFilePath, databaseName, ConfigurationManager.PracticeRepositoryConnectionString);
        }

        internal static string BackupDatabase(string connectionString, string suffix = "")
        {
            string bakFilePath = GetDatabaseBackupPath(connectionString, suffix);

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Execute("BACKUP DATABASE [" + connection.Database + @"] TO DISK ='" + bakFilePath + @"' WITH INIT");

                return bakFilePath;
            }
        }

        internal static string GetDatabaseBackupPath(string connectionString, string suffix = "")
        {
            var builder = new SqlConnectionStringBuilder(connectionString);
            var databaseName = builder.InitialCatalog;
            builder.InitialCatalog = "master";
            string bakFilePath = Path.Combine(GetDefaultBackupPath(builder.ToString()), new[] { databaseName, suffix }.Join("_") + ".bak");
            return bakFilePath;
        }

        internal static string GetDefaultBackupPath(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var backupDirectoryQuery =
                    connection.Execute<DataTable>(
                        @"EXEC  master.dbo.xp_instance_regread  
 N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'");


                // e.g. C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup 
                var backupDirectory = backupDirectoryQuery.Rows[0]["Data"].ToString();

                return backupDirectory;
            }
        }

        private static System.Threading.Tasks.Task CreateTestDatabaseFromBackup(byte[] zipFile, string databaseName, string connectionStringName)
        {
            // get computer specific name
            string computerSpecificName = PrepareComputerSpecificDatabaseName(databaseName);
            // set the initial catalog of the connection string to the computer specific name
            UpdateConnectionStringAndServerDataPath(connectionStringName, computerSpecificName);

            var connectionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings[connectionStringName].ConnectionString);

            var masterConnectionStringBuilder = new SqlConnectionStringBuilder(connectionStringBuilder.ConnectionString);
            masterConnectionStringBuilder.InitialCatalog = "master";

            return System.Threading.Tasks.Task.Factory.StartNew(() =>
                                                   {
                                                       using (var connection = new SqlConnection(masterConnectionStringBuilder.ToString()))
                                                       {
                                                           var backupPath = GetDefaultBackupPath(connection.ConnectionString);
                                                           var dataPath = GetDefaultDataPath(connection.ConnectionString);
                                                           var uncBackupPath = @"\\{0}\{1}".FormatWith(connectionStringBuilder.DataSource.Split('\\')[0], backupPath.ReplaceFirst(":", "$"));
                                                           var uncDataPath = @"\\{0}\{1}".FormatWith(connectionStringBuilder.DataSource.Split('\\')[0], dataPath.ReplaceFirst(":", "$"));

                                                           string bakFilePath = Path.Combine(uncBackupPath, connectionStringBuilder.InitialCatalog + ".bak");

                                                           // if previous test run passed, we can just use the base line backup it made so we don't need to migrate again

                                                           if (!File.Exists(bakFilePath))
                                                           {
                                                               // restore from the embedded zipped bak file
                                                               using (var ms = new MemoryStream(zipFile))
                                                               {
                                                                   var directory = Path.Combine(uncBackupPath, computerSpecificName);

                                                                   if (Directory.Exists(directory))
                                                                       Directory.GetFiles(directory, "*.bak").ToList().ForEach(File.Delete);

                                                                   new FastZip().ExtractZip(ms, directory, FastZip.Overwrite.Always, null, null, null, true, false);

                                                                   bakFilePath = Path.GetFullPath(Directory.GetFiles(directory, "*.bak").Single()).EnsureNotDefault();
                                                               }
                                                           }

                                                           DropDatabase(connection, computerSpecificName);

                                                           var conflictingFiles = Directory.GetFiles(uncDataPath, "*_{0}.*".FormatWith(computerSpecificName)).ToList();

                                                           conflictingFiles.ForEach(f => DropDatabaseWithFile(connection, f.Replace(uncDataPath, dataPath)));

                                                           conflictingFiles.ForEach(File.Delete);

                                                           RestoreDatabase(bakFilePath.Replace(uncBackupPath, backupPath), computerSpecificName, connectionStringBuilder.ConnectionString);
                                                       }
                                                   });
        }

        public static void RestoreDatabase(string bakFilePath, string targetDatabaseName, string connectionString = null)
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString ?? ConfigurationManager.PracticeRepositoryConnectionString);

            connectionStringBuilder.InitialCatalog = "master";

            using (var connection = new SqlConnection(connectionStringBuilder.ToString()))
            {
                connection.Open();
                RestoreDatabase(bakFilePath, targetDatabaseName, connection);
            }
        }

        private static void RestoreDatabase(string bakFilePath, string targetDatabaseName, IDbConnection connection)
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder(connection.ConnectionString);
            if (connectionStringBuilder.DataSource.Equals("dev-sql03", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException("Please don't use dev-sql03 for unit tests.");

            var @restoreSql = @"
--DECLARE @restoredDatabaseName nvarchar(max)
--DECLARE @bakPath nvarchar(max)
--SET @restoredDatabaseName = 'PracticeRepositoryTest'
--SET @bakPath = 'PracticeRepositoryTest.bak'

IF OBJECT_ID('tempdb..#fileListTable') IS NOT NULL
    EXEC('DROP TABLE #fileListTable')

CREATE TABLE #fileListTable
    (
        LogicalName          nvarchar(128),
        PhysicalName         nvarchar(260),
        [Type]               char(1),
        FileGroupName        nvarchar(128),
        Size                 numeric(20,0),
        MaxSize              numeric(20,0),
        FileID               bigint,
        CreateLSN            numeric(25,0),
        DropLSN              numeric(25,0),
        UniqueID             uniqueidentifier,
        ReadOnlyLSN          numeric(25,0),
        ReadWriteLSN         numeric(25,0),
        BackupSizeInBytes    bigint,
        SourceBlockSize      int,
        FileGroupID          int,
        LogGroupGUID         uniqueidentifier,
        DifferentialBaseLSN  numeric(25,0),
        DifferentialBaseGUID uniqueidentifier,
        IsReadOnl            bit,
        IsPresent            bit,
    )
IF SUBSTRING(CONVERT(varchar,SERVERPROPERTY('productversion')),1,1) <> '9'
BEGIN
   ALTER TABLE #fileListTable ADD TDEThumbprint varbinary(32)
END

INSERT INTO #fileListTable EXEC('RESTORE FILELISTONLY FROM DISK = ''' + @bakPath + '''')
SELECT * FROM #fileListTable

DECLARE @dataPath nvarchar(max)
SELECT TOP 1  @dataPath = REVERSE(RIGHT(REVERSE(physical_name),(LEN(physical_name)-CHARINDEX('\', REVERSE(physical_name),1))+1))
FROM sys.database_files WHERE name = 'master'
SELECT @dataPath

DECLARE @moveSql nvarchar(max)
SELECT @moveSql = STUFF 
(
(SELECT ', MOVE ''' + LogicalName + ''' TO ''' + @dataPath + LogicalName + '_' + @restoredDatabaseName + + (CASE WHEN [Type] = 'L' THEN '.ldf' ELSE '.mdf' END) + '''' FROM #fileListTable
FOR XML PATH('')
), 1, 1, '')

----Make Database to single user Mode
IF EXISTS(SELECT * FROM sys.databases WHERE name = @restoredDatabaseName)
	EXEC('ALTER DATABASE [' + @restoredDatabaseName + '] SET OFFLINE WITH ROLLBACK IMMEDIATE; DROP DATABASE [' + @restoredDatabaseName + ']')

EXEC('
RESTORE DATABASE [' + @restoredDatabaseName + ']
FROM DISK = ''' + @bakPath + '''
WITH REPLACE, ' + @moveSql
)

EXEC('ALTER DATABASE [' + @restoredDatabaseName + '] SET MULTI_USER')
EXEC('ALTER DATABASE [' + @restoredDatabaseName + '] SET TRUSTWORTHY ON')

DECLARE @owner nvarchar(255)
SET @owner = SYSTEM_USER

EXEC('
ALTER AUTHORIZATION ON DATABASE::[' + @restoredDatabaseName + '] TO [' + @owner + ']'
)

DROP TABLE #fileListTable
";

            var parameters = new Dictionary<string, object> { { "restoredDatabaseName", targetDatabaseName }, { "bakPath", bakFilePath } };

            connection.Execute(@restoreSql, parameters);
        }

        internal static void DropDatabase(IDbConnection connection, string name)
        {
            connection.Execute(@"----Make Database to single user Mode
IF EXISTS(SELECT * FROM sys.databases WHERE name = @name)
	EXEC('ALTER DATABASE [' + @name + '] SET OFFLINE WITH ROLLBACK IMMEDIATE; DROP DATABASE [' + @name + ']')
", new Dictionary<string, object> { { "name", name } });
        }

        internal static void DropDatabaseWithFile(IDbConnection connection, string path)
        {
            var databaseName = connection.Execute<string>(@"SELECT TOP 1 DB_NAME(database_id) FROM sys.master_files
WHERE physical_name = '{0}'".FormatWith(path));

            if (!string.IsNullOrEmpty(databaseName))
            {
                DropDatabase(connection, databaseName);
            }
        }
        internal static string GetDefaultDataPath(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                // delete files if they still exist
                var dataPath = connection.Execute<string>(@"
SELECT SUBSTRING(physical_name, 1, CHARINDEX(N'master.mdf', LOWER(physical_name)) - 1)
FROM master.sys.master_files
WHERE database_id = 1 AND file_id = 1");
                return dataPath;
            }
        }

        private static void InitializeApplication()
        {
            bool isInitialized = false;
            var applicationThread = new Thread(o =>
                                                   {
                                                       Bootstrapper.BootstrappedAssemblies = Bootstrapper.BootstrappedAssemblies.Concat(new[] { Assembly.GetExecutingAssembly() }).ToArray();
                                                       ServiceProvider = Bootstrapper.Run<IServiceProvider>();
                                                       PresentationBootstrapper.Run();

                                                       // Update schema, etc.
                                                       System.Windows.Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                                                       PracticeRepository = ServiceProvider.GetService<IPracticeRepository>();
                                                       isInitialized = true;
                                                       System.Windows.Application.Current.Run();
                                                   });

            applicationThread.SetApartmentState(ApartmentState.STA);
            applicationThread.Start();
            while (!isInitialized)
            {
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Updates the connection string for the test database in the config.
        /// </summary>
        /// <param name="connectionStringName">Name of the connection string.</param>
        /// <param name="initialCatalog">The initial catalog.</param>
        private static void UpdateConnectionStringAndServerDataPath(string connectionStringName, string initialCatalog)
        {
            if (ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings[connectionStringName] == null)
                return;

            // Update config to use this new  db
            var connectionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings[connectionStringName].ConnectionString);
            connectionStringBuilder.InitialCatalog = initialCatalog;
            ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString = connectionStringBuilder.ToString();
            ConfigurationManager.ServerDataPath = Environment.ExpandEnvironmentVariables(@"%TEMP%\IOP\TestsServerData");
            ConfigurationManager.Save();

            UpdateConnectionStringAndServerDataPathInWebConfiguration(connectionStringName, connectionStringBuilder.ToString());
        }

        public static void ModifyWebConfiguration(Action<System.Configuration.Configuration> makeChangesCallback)
        {
            // Locate config
            var serverManager = new ServerManager();
            var site = serverManager.Sites.First(s => s.Name == "Default Web Site");
            var app = site.Applications["/IOPracticeware"];
            var path = Path.Combine(app.VirtualDirectories[0].PhysicalPath, "Web.config");

            // Drop readonly
            new FileInfo(path).IsReadOnly = false;
            Directory.GetFiles(app.VirtualDirectories[0].PhysicalPath, "*.config").ToList().ForEach(f => new FileInfo(f).IsReadOnly = false);

            // Must set current directory to web root so configuration system finds other files next to web.config
            using (new DisposableScope<string>(Environment.CurrentDirectory,
                s => Environment.CurrentDirectory = Path.GetDirectoryName(Path.GetFullPath(path)) ?? "", 
                s => Environment.CurrentDirectory = s))
            {

                // Modify config
                var configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap { ExeConfigFilename = path }, ConfigurationUserLevel.None);
                makeChangesCallback(configuration);
                configuration.Save();
            }
        }

        private static void UpdateConnectionStringAndServerDataPathInWebConfiguration(string connectionStringName, string value)
        {
            ModifyWebConfiguration(configuration =>
            {
                configuration.GetSection<ConnectionStringConfigurationCollection>("customConnectionStrings")[connectionStringName].ConnectionString = value;

                // Set server data path to be the same as server data path on the client
                configuration.GetSection<ApplicationServerConfiguration>("applicationServer").Clients
                    .First(c => c.Name == DefaultApplicationServerClientName).ServerDataPath = Environment.ExpandEnvironmentVariables(@"%TEMP%\IOP\TestsServerData");

            });

            var directoryToGrantAccess = new DirectoryInfo(Environment.ExpandEnvironmentVariables(@"%LOCALAPPDATA%")).Parent.IfNotNull(i => i.FullName, () => ConfigurationManager.ServerDataPath);
            GrantFullAccess(directoryToGrantAccess, @"NT AUTHORITY\NETWORK SERVICE");
        }

        private static void GrantFullAccess(string directory, string account)
        {
            try
            {
                // First determine if the account already has full access to the directory 
                DirectorySecurity directorySecurity = Directory.GetAccessControl(directory);
                var directoryRules = directorySecurity.GetAccessRules(true, true, typeof(NTAccount));
                for (int i = 0; i < directoryRules.Count; i++)
                {
                    var currentRule = directoryRules[i] as FileSystemAccessRule;
                    if (currentRule != null)
                    {
                        // Determine if the matching 'rule' for the 'account' has full control or not.
                        // If not, then continue creating granting full access.
                        if (account.Equals(currentRule.IdentityReference.Value, StringComparison.OrdinalIgnoreCase))
                        {
                            if (currentRule.FileSystemRights.HasFlag(FileSystemRights.FullControl))
                            {
                                return;
                            }
                            break;
                        }
                    }
                }

                // Grant full access to the directory for the account
                var rule = new FileSystemAccessRule(account, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow);
                directorySecurity.AddAccessRule(rule);
                directorySecurity.SetAccessRule(rule);
                Directory.SetAccessControl(directory, directorySecurity);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Could not grant full access to the directory {0} for the account {1}", directory, account), ex);
            }
        }

        /// <summary>
        ///   Enables DTC network access.
        /// </summary>
        private static void EnableDtcNetworkAccess()
        {
            File.WriteAllText("MSDTC_Security.reg", Resources.MSDTC_Security);

            var oldWowRedirect = new IntPtr(0);
            if (Environment.Is64BitOperatingSystem)
                Wow64DisableWow64FsRedirection(ref oldWowRedirect);

            var regeditProcess = new Process
                                     {
                                         StartInfo = new ProcessStartInfo
                                                         {
                                                             FileName = "regedit",
                                                             Arguments = "/s \"" + Path.GetFullPath("MSDTC_Security.reg") + "\"",
                                                             CreateNoWindow = true,
                                                             UseShellExecute = false
                                                         }
                                     };
            regeditProcess.Start();
            regeditProcess.WaitForExit();

            var restartMsdtc = new Process
                                  {
                                      StartInfo = new ProcessStartInfo
                                                      {
                                                          FileName = "cmd",
                                                          Arguments = "/c net stop msdtc && net start msdtc",
                                                          CreateNoWindow = true,
                                                          UseShellExecute = false
                                                      }
                                  };
            restartMsdtc.Start();

            if (Environment.Is64BitOperatingSystem)
                Wow64RevertWow64FsRedirection(ref oldWowRedirect);
        }

        /// <summary>
        ///   Creates a database for testing based on an existing connection string name from config.
        /// </summary>
        /// <param name="connectionStringName"> Name of the connection string. </param>
        /// <param name="sqlScript"> The SQL script. </param>
        /// <param name="scriptDatabaseName"> Name of the script database. </param>
        internal static System.Threading.Tasks.Task CreateTestDatabase(string connectionStringName, string sqlScript, string scriptDatabaseName)
        {
            // get computer specific name
            string computerSpecificName = PrepareComputerSpecificDatabaseName(scriptDatabaseName);

            // set the initial catalog of the connection string to the computer specific name
            UpdateConnectionStringAndServerDataPath(connectionStringName, computerSpecificName);
            var computerSpecificConnectionString = ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings[connectionStringName].ConnectionString;

            // Need the hub connection string
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(computerSpecificConnectionString);
            var dataSource = sqlConnectionStringBuilder.DataSource;

            if (dataSource.Equals("dev-sql03", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException("Please don't use dev-sql03 for unit tests.");


            // Drop replication (fail elegantly if DB doesn't exist)
            var sqlReplicationConfiguration = new SqlReplicationConfiguration();
            sqlReplicationConfiguration.HubConnectionString = computerSpecificConnectionString;
            var replicationManager = new SqlReplicationManager();
            sqlReplicationConfiguration.SetReplicationManager(replicationManager);

            replicationManager.Delete(sqlReplicationConfiguration, true);

            // Use connection string with master catalog (since DB most likely doesn't exist yet)
            sqlConnectionStringBuilder.InitialCatalog = "master";
            var masterConnectionString = sqlConnectionStringBuilder.ToString();

            // Drop DB 
            using (IDbConnection connection = new SqlConnection(masterConnectionString))
            {
                DropDatabase(connection, computerSpecificName);

                var dataPath = GetDefaultDataPath(connection.ConnectionString);

                var uncPath = @"\\{0}\{1}".FormatWith(dataSource.Split('\\')[0], dataPath.ReplaceFirst(":", "$"));

                var conflictingFiles =
                    Directory.GetFiles(uncPath, "*_{0}.*".FormatWith(computerSpecificName)).Concat(
                    Directory.GetFiles(uncPath, "{0}_*.*".FormatWith(computerSpecificName))).Concat(
                    Directory.GetFiles(uncPath, "*{0}.*".FormatWith(computerSpecificName)))
                    .ToList();

                conflictingFiles.ForEach(f => DropDatabaseWithFile(connection, f.Replace(uncPath, dataPath)));
                conflictingFiles.ForEach(File.Delete);
            }

            // create the DB
            sqlScript = sqlScript.Replace(scriptDatabaseName, computerSpecificName);

            return System.Threading.Tasks.Task.Factory.StartNew(() =>
                                       {
                                           using (IDbConnection connection = new SqlConnection(masterConnectionString))
                                           {
                                               connection.RunScript(sqlScript, false);
                                           }
                                       });

        }

        [AssemblyCleanup]
        public static void OnAssemblyCleanup()
        {
            // Avoid configuration access if tests failed very early
            if (ConfigurationManager.IsLoaded)
            {
                TestBase.Cleanup();

                if (ConfigureWcfServices)
                {
                    ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService = false;
                    ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = false;
                    ConfigurationManager.Save();
                }

                if (System.Windows.Application.Current != null)
                {
                    System.Windows.Application.Current.Dispatcher.Invoke(new Action(() => System.Windows.Application.Current.Shutdown()));
                    if (System.Windows.Application.Current != null)
                        System.Windows.Application.Current.Dispatcher.Thread.Join();
                }

                // Cleanup server data path
                FileSystem.DeleteDirectory(ConfigurationManager.ServerDataPath);

                BackgroundThreadOnlyAttribute.IsEnabled = false;
            }

            if (TestBase.TestRuns.Values.All(o => o.Outcome == UnitTestOutcome.Passed)
                && Assembly.GetExecutingAssembly().GetTypes().Where(t =>
                    t.HasAttribute<TestClassAttribute>() && !t.HasAttribute<IgnoreAttribute>()).SelectMany(tc => tc.GetMethods())
                    .Where(m => m.HasAttribute<TestMethodAttribute>() && !m.HasAttribute<IgnoreAttribute>()).All(m => TestBase.TestRuns.ContainsKey(m)))
            {
                // if all tests bassed (and we ran all tests), use bak as new baseline
                var uncBakFilePath = @"\\{0}\{1}".FormatWith(DbConnections.TryGetConnectionStringBuilder<SqlConnectionStringBuilder>(ConfigurationManager.PracticeRepositoryConnectionString).DataSource,
                    _baseBakFilePath.ReplaceFirst(":", "$"));

                var destination = Path.Combine(Path.GetDirectoryName(uncBakFilePath) ?? "", Path.GetFileNameWithoutExtension(uncBakFilePath).TrimEnd("_Base")) + ".bak";

                if (File.Exists(destination))
                    File.Delete(destination);

                // move to path where baseline is expeted for next run
                File.Move(uncBakFilePath, destination);
            }

            LogTestRuns();
        }

        private static void LogTestRuns()
        {
            foreach (var run in TestBase.TestRuns.OrderByDescending(r => r.Value.Duration).ToArray())
            {
                Trace.TraceInformation("Unit test {0} ran with outcome of {1} in {2}ms.", run.Key.Name, run.Value.Outcome, run.Value.Duration.TotalMilliseconds);
            }
        }

        private static string PrepareComputerSpecificDatabaseName(string databaseName)
        {
            if (_cachedDbSuffix == null)
            {
                _cachedDbSuffix = new Lazy<string>(() =>
                {
                    // Attempt to capture branch name to make database names branch specific
                    var pathWithBranchName = Path.GetFullPath(ConfigurationManager.ApplicationPath);

                    // Remove drive letter
                    pathWithBranchName = pathWithBranchName.TrimStart(Path.GetPathRoot(pathWithBranchName));

                    // Cut test output folder
                    var parts = Regex.Split(pathWithBranchName, @"(\\IO\.Practiceware\.Tests|\\TestResults)", RegexOptions.IgnoreCase);
                    pathWithBranchName = parts[0];

                    // Build machine name + branch name suffix
                    var result = string.Format("_{0}_{1}", Environment.MachineName, pathWithBranchName);

                    // Make suffix database name safe
                    result = result.Replace('.', '_').Replace('\\', '_').Replace(':', '_').Replace(' ', '_');

                    return result;
                });
            }

            return (databaseName + _cachedDbSuffix.Value).Truncate(128); // SQL allows 128 characters (-2 characters for "_1" suffix for each parallel thread)
        }

        /// <summary>
        ///   Creates an appointment for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static UserAppointment CreateAppointment(Patient patient = null)
        {
            Patient patient2 = patient ?? CreatePatient();

            var appointment = new UserAppointment
                                  {
                                      Encounter = new Encounter
                                                      {
                                                          Patient = patient2,
                                                          EncounterType = PracticeRepository.EncounterTypes.OrderBy(i => i.Id).First(),
                                                          ServiceLocation = PracticeRepository.ServiceLocations.OrderBy(i => i.Id).First()
                                                      }
                                  };

            appointment.DateTime = new DateTime(2010, 1, 1, 10, 15, 0);
            appointment.Encounter.EncounterStatusId = (int)EncounterStatus.Pending;
            appointment.AppointmentType = CreateAppointmentType();
            appointment.User = CreateUser();
            appointment.Comment = "TestComment";
            appointment.Encounter.StartDateTime = appointment.DateTime;

            return appointment;
        }

        /// <summary>
        ///   Creates an appointment type for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static AppointmentType CreateAppointmentType()
        {
            var appointmentType = new AppointmentType();
            appointmentType.Name = "Test Type";
            appointmentType.PatientQuestionSet = PracticeRepository.PatientQuestionSets.OrderBy(i => i.Id).FirstOrDefault();
            appointmentType.MaximumRecallsPerEncounter = 1;
            appointmentType.FrequencyBetweenRecallsPerEncounter = 1;
            return appointmentType;
        }

        /// <summary>
        /// Creates a user schedule block for use by unit tests.  Does not save it.
        /// </summary>
        /// <returns></returns>
        public static UserScheduleBlock CreateUserScheduleBlock()
        {
            var userScheduleBlock = new UserScheduleBlock
                {
                    User = PracticeRepository.Users.OrderBy(i => i.Id).First(),
                    StartDateTime = new DateTime(2010, 1, 1, 10, 15, 0),
                    IsUnavailable = false,
                    ServiceLocation = PracticeRepository.ServiceLocations.OrderBy(i => i.Id).First(),
                };
            return userScheduleBlock;
        }

        /// <summary>
        /// Create an Appointment Category for use by unit tests.  Does not save it.
        /// </summary>
        /// <returns></returns>
        public static AppointmentCategory CreateAppointmentCategory()
        {
            var appointmentCategory = new AppointmentCategory
                {
                    Name = "Long Appointment",
                    HexColor = "112233",
                };

            return appointmentCategory;
        }

        /// <summary>
        /// Adds a Schedule Block Appointment Category to a Schedule Block.
        /// </summary>
        /// <param name="scheduleBlock">The schedule block.</param>
        public static void AddScheduleBlockAppointmentCategory(ScheduleBlock scheduleBlock)
        {
            scheduleBlock.ScheduleBlockAppointmentCategories.Add(new ScheduleBlockAppointmentCategory
                {
                    AppointmentCategoryId = PracticeRepository.AppointmentCategories.First().Id,
                    ScheduleBlockId = scheduleBlock.Id
                });
        }

        /// <summary>
        ///   Adds test email addresses to a patient.
        /// </summary>
        /// <param name="patient"> The patient. </param>
        public static void AddEmailAddresses(Patient patient)
        {
            patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Test@aol.com" });
        }

        /// <summary>
        ///   Adds test phone numbers to a patient.
        /// </summary>
        /// <param name="patient"> The patient. </param>
        public static void AddPhoneNumbers(Patient patient)
        {
            var patientNumber = new PatientPhoneNumber();
            patientNumber.PatientPhoneNumberTypeId = PatientPhoneNumberType.Home;
            patient.PatientPhoneNumbers.Add(patientNumber);
            patientNumber.SetValuesFromString("212-123-4567");
        }

        /// <summary>
        ///   Adds test addresses to insurer.
        /// </summary>
        /// <param name="insurer">The insurer. </param>
        public static void AddAddresses(Insurer insurer)
        {
            var insurerAddress = new InsurerAddress();
            insurerAddress.City = "New York";
            insurerAddress.Line1 = "TestLine1";
            insurerAddress.Line2 = "TestLine2";
            insurerAddress.PostalCode = "12345";
            insurerAddress.Insurer = insurer;
            insurerAddress.InsurerAddressTypeId = (int)InsurerAddressTypeId.Claims;
            StateOrProvince stateOrProvince = PracticeRepository.StateOrProvinces.FirstOrDefault(s => s.Name == insurerAddress.City);
            if (stateOrProvince != null)
                insurerAddress.StateOrProvinceId = stateOrProvince.Id;

            insurer.InsurerAddresses.Add(insurerAddress);
        }

        /// <summary>
        ///   Adds test addresses to a patient.
        /// </summary>
        /// <param name="patient"> The patient. </param>
        public static void AddAddresses(Patient patient)
        {
            var patientAddress = new PatientAddress();
            patientAddress.City = "New York";
            patientAddress.Line1 = "TestLine1";
            patientAddress.Line2 = "TestLine2";
            patientAddress.PostalCode = "12345";
            patientAddress.Patient = patient;
            patientAddress.PatientAddressTypeId = (int)PatientAddressTypeId.Home;
            StateOrProvince stateOrProvince = PracticeRepository.StateOrProvinces.FirstOrDefault(s => s.Name == patientAddress.City);
            if (stateOrProvince != null)
                patientAddress.StateOrProvinceId = stateOrProvince.Id;

            patient.PatientAddresses.Add(patientAddress);
        }

        /// <summary>
        ///   Creates a patient for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static Patient CreatePatient()
        {
            // TO DO: Populate more metadata

            var patient = new Patient();
            patient.FirstName = "John";
            patient.LastName = "Doe";
            patient.MiddleName = "J";

            patient.IsClinical = true;

            patient.DateOfBirth = new DateTime(2000, 1, 1);
            patient.Gender = Gender.Male;
            patient.MaritalStatus = MaritalStatus.Single;

            patient.SocialSecurityNumber = "123-45-6789";


            BillingOrganization billingOrganization = PracticeRepository.BillingOrganizations.FirstOrDefault();
            if ((billingOrganization != null))
            {
                patient.BillingOrganizationId = billingOrganization.Id;
            }

            return patient;
        }

        public static Patient CreatePatient2()
        {
            var patientTwo = new Patient();
            patientTwo.FirstName = "Seteve";
            patientTwo.LastName = "Jobs";
            patientTwo.MiddleName = "S";
            patientTwo.IsClinical = true;
            patientTwo.DateOfBirth = new DateTime(2001, 1, 1);
            patientTwo.Gender = Gender.Male;
            patientTwo.MaritalStatus = MaritalStatus.Single;

            patientTwo.SocialSecurityNumber = "123-45-6780";

            return patientTwo;
        }

        /// <summary>
        ///   Creates Patients for Use by unit Test.And also Save the Patients.
        /// </summary>
        /// <returns> </returns>
        /// <remarks>
        /// </remarks>
        public static IEnumerable<Patient> CreatePatients()
        {
            var patientList = new List<Patient>();

            var patientOne = new Patient();
            patientOne.FirstName = "John";
            patientOne.LastName = "Doe";
            patientOne.MiddleName = "Jay";
            patientOne.IsClinical = true;
            patientOne.DateOfBirth = new DateTime(2000, 1, 1);
            patientOne.Gender = Gender.Male;
            patientOne.MaritalStatus = MaritalStatus.Single;
            patientOne.SocialSecurityNumber = "123-45-6789";
            BillingOrganization billingOrganization = PracticeRepository.BillingOrganizations.FirstOrDefault();
            if ((billingOrganization != null))
            {
                patientOne.BillingOrganizationId = billingOrganization.Id;
            }
            patientOne.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Test@aol.com" });


            var patientTwo = new Patient();
            patientTwo.FirstName = "Seteve";
            patientTwo.LastName = "Jobs";
            patientTwo.MiddleName = "S";
            patientTwo.IsClinical = true;
            patientTwo.DateOfBirth = new DateTime(2001, 1, 1);
            patientTwo.Gender = Gender.Male;
            patientTwo.MaritalStatus = MaritalStatus.Single;
            patientTwo.SocialSecurityNumber = "123-45-6789";
            if ((billingOrganization != null))
            {
                patientTwo.BillingOrganizationId = billingOrganization.Id;
            }
            //patientTwo.PatientPhoneNumbers.Add(New PatientPhoneNumber With {.Value = "212-123-4567", .AreaCode = "60", .PatientPhoneNumberTypeId = PatientPhoneNumberTypeId.Cell})
            patientTwo.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Testtwo@aol.com" });

            patientList.Add(patientOne);
            patientList.Add(patientTwo);

            return patientList;
        }

        /// <summary>
        /// Creates a Patient with almost all data filled out.
        /// </summary>
        /// <returns></returns>
        public static Patient CreateFullPatient()
        {
            var patient = new Patient();
            patient.FirstName = "John";
            patient.LastName = "Doe";
            patient.MiddleName = "Jay";
            patient.IsClinical = true;
            patient.DateOfBirth = new DateTime(2000, 1, 1);
            patient.Gender = Gender.Male;
            patient.MaritalStatus = MaritalStatus.Single;
            patient.SocialSecurityNumber = "123-45-6789";

            patient.EmployerName = "IO Practiceware";
            patient.Honorific = "King";
            patient.IsHipaaConsentSigned = true;
            patient.NickName = "Johnny Boy";
            patient.Occupation = "Software Tester";
            patient.PatientComments = new List<PatientComment> { new PatientComment { PatientCommentType = PatientCommentType.PatientDemographicSummary, Value = "Test comment created in Common.cs" } };
            patient.PatientStatus = PatientStatus.Active;
            patient.PaymentOfBenefitsToProvider = PaymentOfBenefitsToProvider.N;
            patient.Prefix = "Mr.";
            patient.PriorFirstName = "BadFirstName";
            patient.PriorLastName = "BadLastName";
            patient.PriorPatientCode = "002839";
            patient.ReleaseOfInformationCode = ReleaseOfInformationCode.Y;
            patient.ReleaseOfInformationDateTime = new DateTime(2000, 2, 27);
            patient.Salutation = "Hi";
            patient.Suffix = "Jr.";

            var phoneNumber1 = ServiceProvider.GetService<PatientPhoneNumber>();
            phoneNumber1.AreaCode = "917";
            phoneNumber1.CountryCode = "1";
            phoneNumber1.ExchangeAndSuffix = "5555555";
            phoneNumber1.IsInternational = false;
            phoneNumber1.OrdinalId = 1;
            phoneNumber1.PatientPhoneNumberType = (int)PatientPhoneNumberType.Home;

            var phoneNumber2 = ServiceProvider.GetService<PatientPhoneNumber>();
            phoneNumber2.AreaCode = "212";
            phoneNumber2.CountryCode = "1";
            phoneNumber2.IsInternational = false;
            phoneNumber2.ExchangeAndSuffix = "2224334";
            phoneNumber2.OrdinalId = 2;
            phoneNumber2.PatientPhoneNumberType = (int)PatientPhoneNumberType.Business;

            patient.PatientPhoneNumbers = new FixupCollection<PatientPhoneNumber> { phoneNumber1, phoneNumber2 };

            patient.PatientEmailAddresses = new FixupCollection<PatientEmailAddress>
                                                {
                                                    new PatientEmailAddress
                                                        {
                                                            EmailAddressType = EmailAddressType.Personal,
                                                            OrdinalId = 1,
                                                            Value = "johndoe@example.com",
                                                        },
                                                    new PatientEmailAddress
                                                        {
                                                            EmailAddressType = EmailAddressType.Business,
                                                            OrdinalId = 2,
                                                            Value = "johndoe@examplework.com",
                                                        }
                                                };

            patient.PatientAddresses = new FixupCollection<PatientAddress>
                                           {
                                               new PatientAddress
                                                   {
                                                       City = "new york",
                                                       Line1 = "4 Tulip Road",
                                                       Line2 = "Second Floor",
                                                       OrdinalId = 1,
                                                       PatientAddressTypeId = (int) PatientAddressTypeId.Home,
                                                       PostalCode = "555555",
                                                       StateOrProvinceId = PracticeRepository.StateOrProvinces.FirstOrDefault(x => x.Name.ToLower() == "new york").IfNotNull(x => x.Id, PracticeRepository.StateOrProvinces.First().Id)
                                                   },
                                               new PatientAddress
                                                   {
                                                       City = "new york",
                                                       Line1 = "887 Brentwood Ave",
                                                       Line2 = "45th Floor",
                                                       OrdinalId = 2,
                                                       PatientAddressTypeId = (int) PatientAddressTypeId.Business,
                                                       PostalCode = "66777",
                                                       StateOrProvinceId = PracticeRepository.StateOrProvinces.FirstOrDefault(x => x.Name.ToLower() == "new york").IfNotNull(x => x.Id, PracticeRepository.StateOrProvinces.First().Id)
                                                   }
                                           };

            patient.DefaultUserId = PracticeRepository.Users.OfType<Doctor>().FirstOrDefault().IfNotNull(x => x.Id);

            patient.ExternalProviders = new List<PatientExternalProvider> { CreatePatientExternalProvider() };
            patient.ExternalProviders.First().Patient = null;

            patient.EthnicityId = PracticeRepository.Ethnicities.FirstOrDefault().IfNotNull(x => x.Id);
            patient.LanguageId = PracticeRepository.Languages.FirstOrDefault().IfNotNull(x => x.Id);
            if (patient.Races == null)
                patient.Races = new List<Race>();
            var patientRace = PracticeRepository.Races.FirstOrDefault();
            if (patientRace != null)
            {
                patient.Races.Add(new Race { Id = patientRace.Id, Name = patientRace.Name });
            }

            var tag = PracticeRepository.Tags.FirstOrDefault();
            if (tag == null)
            {
                PracticeRepository.Save(new Tag { Name = "TestTag1", DisplayName = "Test Tag 1", TagTypeId = 1, HexColor = "FFFFFF" });
                tag = PracticeRepository.Tags.FirstOrDefault();
            }

            patient.PatientTags = new FixupCollection<PatientTag>
                                      {
                                          new PatientTag
                                              {
                                                  OrdinalId = 1,
                                                  Tag = tag                                                  
                                              }                                          
                                      };

            return patient;
        }

        /// <summary>
        ///   Creates an appointments for use by unit tests.And Does save it.
        /// </summary>
        /// <returns> </returns>
        public static IEnumerable<UserAppointment> CreateAppointments()
        {
            IEnumerable<Patient> patientList = CreatePatients().ToArray();
            Patient patientOne = patientList.FirstOrDefault();
            Patient patientTwo = patientList.LastOrDefault();


            var appointmentList = new List<UserAppointment>();
            var appointmentOne = new UserAppointment
                                     {
                                         Encounter = new Encounter
                                                         {
                                                             Patient = patientOne,
                                                             EncounterType = PracticeRepository.EncounterTypes.OrderBy(i => i.Id).First(),
                                                             ServiceLocation = PracticeRepository.ServiceLocations.OrderBy(i => i.Id).First()
                                                         }
                                     };
            appointmentOne.DateTime = new DateTime(2010, 1, 1);
            appointmentOne.Encounter.EncounterStatusId = (int)EncounterStatus.Pending;
            appointmentOne.AppointmentType = CreateAppointmentType();
            appointmentOne.User = CreateUser();

            var appointmentTwo = new UserAppointment
                                     {
                                         Encounter = new Encounter
                                                         {
                                                             Patient = patientTwo,
                                                             EncounterType = PracticeRepository.EncounterTypes.OrderBy(i => i.Id).First(),
                                                             ServiceLocation = PracticeRepository.ServiceLocations.OrderBy(i => i.Id).First()
                                                         }
                                     };
            appointmentTwo.DateTime = new DateTime(2011, 2, 2);
            appointmentTwo.Encounter.EncounterStatusId = (int)EncounterStatus.Pending;
            appointmentTwo.AppointmentType = CreateAppointmentType();
            appointmentTwo.User = CreateUser();

            var appointmentThree = new UserAppointment
                                       {
                                           Encounter = new Encounter
                                                           {
                                                               Patient = null,
                                                               EncounterType = PracticeRepository.EncounterTypes.OrderBy(i => i.Id).First(),
                                                               ServiceLocation = PracticeRepository.ServiceLocations.OrderBy(i => i.Id).First()
                                                           }
                                       };
            appointmentThree.DateTime = new DateTime(2011, 5, 3);
            appointmentThree.Encounter.EncounterStatusId = (int)EncounterStatus.Pending;
            appointmentThree.AppointmentType = CreateAppointmentType();
            appointmentThree.User = CreateUser();

            appointmentList.Add(appointmentOne);
            appointmentList.Add(appointmentTwo);
            appointmentList.Add(appointmentThree);

            return appointmentList.ToList();
        }

        public static Ethnicity CreatePatientEthnicity()
        {
            var patientEthnicity = new Ethnicity();
            patientEthnicity.Name = "TestEthnicity";
            return patientEthnicity;
        }

        public static Language CreatePatientLanguage()
        {
            var patientLanguage = new Language();
            patientLanguage.Name = "Language";
            return patientLanguage;
        }

        public static Race CreatePatientRace(string raceName = null)
        {
            var patientRace = new Race();
            patientRace.Name = raceName ?? "TestRace";
            return patientRace;
        }

        public static PatientAddress CreatePatientFirstAddress(Patient patient)
        {
            var patientAddress = new PatientAddress();
            patientAddress.City = "New York";
            patientAddress.Line1 = "TestLine1";
            patientAddress.Line2 = "TestLine2";
            patientAddress.PostalCode = "12345";
            patientAddress.Patient = patient;
            patientAddress.PatientAddressTypeId = (int)PatientAddressTypeId.Home;
            StateOrProvince stateOrProvince = PracticeRepository.StateOrProvinces.FirstOrDefault(s => s.Name == patientAddress.City);
            if (stateOrProvince != null)
                patientAddress.StateOrProvinceId = stateOrProvince.Id;
            return patientAddress;
        }

        public static PatientDemoAlt CreatePatientSecondaryAddress(Patient patient)
        {
            //Create PatientDemoAlt Data
            var patientDemoAlt = new PatientDemoAlt();
            patientDemoAlt.Address = "TestAddress";
            patientDemoAlt.CellPhone = "122332323";
            patientDemoAlt.City = "Chicago";
            patientDemoAlt.Email = "TestSecond@aol.com";
            patientDemoAlt.HomePhone = "09877652222";
            patientDemoAlt.Zip = "12345678";
            return patientDemoAlt;
        }


        /// <summary>
        ///   Creates a Extrenalprovider for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static ExternalProvider CreateExternalProvider()
        {
            var externalProvider = new ExternalProvider();
            externalProvider.FirstName = "STEPHEN";
            externalProvider.LastNameOrEntityName = "COX";
            externalProvider.MiddleName = "M";
            externalProvider.DisplayName = "STEPHEN M COX";
            externalProvider.Salutation = "STEVE";
            externalProvider.NickName = "STEVE";
            externalProvider.ExcludeOnClaim = false;

            externalProvider.ExternalContactAddresses.Add(new ExternalContactAddress
                {
                    City = "Brooklyn",
                    Line1 = "123 Fake St",
                    PostalCode = "11201",
                    StateOrProvinceId = PracticeRepository.StateOrProvinces.First(i => i.Name == "New York").Id,
                    ContactAddressTypeId = (int)ExternalContactAddressTypeId.MainOffice
                });

            externalProvider.ExternalContactPhoneNumbers.Add(new ExternalContactPhoneNumber
                {
                    CountryCode = "1",
                    AreaCode = "212",
                    ExchangeAndSuffix = "123-1234",
                    ExternalContactPhoneNumberTypeId = (int)ExternalContactPhoneNumberTypeId.Main
                });

            return externalProvider;
        }

        public static ExternalOrganization CreateExternalOrganization()
        {
            var externalOrganization = new ExternalOrganization();
            externalOrganization.DisplayName = "Test your firm";
            externalOrganization.ExternalOrganizationTypeId = 2;
            externalOrganization.Name = "Test your firm";
            return externalOrganization;
        }


        public static ExternalSystemEntity CreateExternalSystemEntity(string entityName)
        {
            var externalSystemEntity = new ExternalSystemEntity();
            externalSystemEntity.Name = entityName;
            return externalSystemEntity;
        }

        public static ExternalSystemEntityMapping CreateExternalSystemEntityMapping()
        {
            var externalSystemEntityMapping = new ExternalSystemEntityMapping();
            return externalSystemEntityMapping;
        }

        public static PracticeCodeTable CreatePracticeCode(string referenceType, string code = "")
        {
            var pracitceCodeTable = new PracticeCodeTable();
            pracitceCodeTable.ReferenceType = referenceType;
            pracitceCodeTable.Code = code;
            return pracitceCodeTable;
        }

        public static Insurer CreateInsurer()
        {
            var insurer = new Insurer();
            insurer.ClaimFilingIndicatorCode = ClaimFilingIndicatorCode.CentralCertification;
            insurer.MedigapCode = "1";
            insurer.GroupName = "TestGroup";
            insurer.IsReferralRequired = true;
            StateOrProvince stateOrProvince = PracticeRepository.StateOrProvinces.FirstOrDefault();
            if (stateOrProvince != null)
                insurer.JurisdictionStateOrProvinceId = stateOrProvince.Id;
            insurer.Name = "TestInsurer";
            insurer.AllowDependents = true;
            insurer.IsSecondaryClaimPaper = false;
            insurer.PolicyNumberFormat = "";
            insurer.PayerCode = "";
            insurer.PlanName = "TestPlanName";
            insurer.PriorInsurerCode = string.Empty;
            insurer.DiagnosisTypeId = (int) DiagnosisTypeId.Icd9;
            return (insurer);
        }

        public static PatientInsuranceReferral CreatePatientInsuranceReferral(Patient patient = null)
        {
            var patientInsuranceReferral = new PatientInsuranceReferral();
            patientInsuranceReferral.Patient = patient ?? CreatePatient();
            patientInsuranceReferral.ExternalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().OrderBy(i => i.Id).FirstOrDefault() ?? CreateExternalProvider();
            patientInsuranceReferral.Doctor = PracticeRepository.Users.OfType<Doctor>().OrderBy(i => i.Id).FirstOrDefault();
            patientInsuranceReferral.StartDateTime = DateTime.Today;
            patientInsuranceReferral.TotalEncountersCovered = 2;
            patientInsuranceReferral.ReferralCode = "A";
            if (patient != null)
            {
                patientInsuranceReferral.PatientInsurance = CreatePatientInsurance(patient);
            }
            else
            {
                patientInsuranceReferral.PatientInsurance = PracticeRepository.PatientInsurances.OrderBy(i => i.Id).FirstOrDefault() ?? CreatePatientInsurance();
            }
            patientInsuranceReferral.PatientId = patientInsuranceReferral.PatientInsurance.InsuredPatientId;
            patientInsuranceReferral.IsArchived = false;
            patientInsuranceReferral.EndDateTime = DateTime.Today.AddDays(365);
            return patientInsuranceReferral;
        }

        /// <summary>
        ///   Creates a user for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static User CreateUser()
        {
            var user = new Doctor();
            user.FirstName = "NewFirstName";
            user.LastName = "NewLastName";
            user.MiddleName = "NewMiddleName";
            user.Color = Color.Aqua;
            user.DisplayName = "Dr. Test for User";
            user.Honorific = "Sir Henry";
            user.IsArchived = false;
            user.IsLoggedIn = false;
            user.IsSchedulable = true;
            user.Pid = "9876";
            user.Prefix = "the third";
            user.UserName = "9876";
            return user;
        }

        /// <summary>
        ///   Creates Users for Use by unit Test.Does not save it.
        /// </summary>
        /// <returns> </returns>
        /// <remarks>
        /// </remarks>
        public static IEnumerable<User> CreateUsers()
        {
            var userlist = new List<User>();

            var userone = new User();
            userone.FirstName = "NewFirstName";
            userone.LastName = "NewLastName";
            userone.MiddleName = "NewMiddleName";
            userone.Color = Color.Aqua;
            userone.DisplayName = "Dr. Test for User";
            userone.Honorific = "Sir Henry";
            userone.IsArchived = false;
            userone.IsLoggedIn = false;
            userone.IsSchedulable = true;
            userone.Pid = "8765";
            userone.Prefix = "the third";
            userone.UserName = "8765";

            var usertwo = new User();
            usertwo.FirstName = "NewFirstName1";
            usertwo.LastName = "NewLastName1";
            usertwo.MiddleName = "NewMiddleName1";
            usertwo.Color = Color.Aqua;
            usertwo.DisplayName = "Dr. Test for User1";
            usertwo.Honorific = "Sir Henry1";
            usertwo.IsArchived = false;
            usertwo.IsLoggedIn = false;
            usertwo.IsSchedulable = true;
            usertwo.Pid = "7654";
            usertwo.Prefix = "the third";
            usertwo.UserName = "7654";

            userlist.Add(userone);
            userlist.Add(usertwo);

            return userlist;
        }

        /// <summary>
        ///   Creates groups for Use by unit Test.Does not save it.
        /// </summary>
        /// <returns> </returns>
        /// <remarks>
        /// </remarks>
        public static IEnumerable<Group> CreateGroups()
        {
            var grouplist = new List<Group>();

            var groupone = new Group();
            groupone.Name = "Billing";

            var grouptwo = new Group();
            grouptwo.Name = "Billing1";


            grouplist.Add(groupone);
            grouplist.Add(grouptwo);

            return grouplist;
        }


        /// <summary>
        ///   Creates a Group for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static Group CreateGroup()
        {
            var @group = new Group();
            @group.Name = "Billing";
            return @group;
        }


        /// <summary>
        ///   Creates a PatientExtrenalprovider for use by unit tests. Does not save it.
        /// </summary>
        /// <returns> </returns>
        public static PatientExternalProvider CreatePatientExternalProvider()
        {
            var patientExternalProvider = new PatientExternalProvider();

            patientExternalProvider.ExternalProvider = CreateExternalProvider();
            patientExternalProvider.IsReferringPhysician = true;
            patientExternalProvider.IsPrimaryCarePhysician = false;
            patientExternalProvider.Patient = CreatePatient();

            return patientExternalProvider;
        }

        public static InsurerPlanType CreateInsurerPlanType()
        {
            var patientRace = new InsurerPlanType();
            patientRace.Name = "TestInsurerPlanType";
            return patientRace;
        }

        public static BillingOrganization CreateBillingOrganization()
        {
            var billingOrganization = new BillingOrganization();
            billingOrganization.Name = "TestBillingOrganization";
            billingOrganization.ShortName = "Office-Office";
            billingOrganization.IsMain = false;
            return (billingOrganization);
        }

        public static ServiceLocation CreateServiceLocation()
        {
            var serviceLocation = new ServiceLocation();
            serviceLocation.Name = "TestServiceLocation";
            serviceLocation.ShortName = "Office";
            serviceLocation.ServiceLocationCode = PracticeRepository.ServiceLocationCodes.FirstOrDefault(t => t.Name == "Office");
            serviceLocation.HexColor = "FFFFFF";
            return (serviceLocation);
        }

        public static TaskActivityType CreateTaskActivityType()
        {
            var taskActivityType = new TaskActivityType();
            taskActivityType.Name = "Contact Lens Queation";
            taskActivityType.DefaultContent = "Problem with new lenses:";
            taskActivityType.OrdinalId = 1;
            taskActivityType.AlertDefault = true;
            taskActivityType.AssignIndividuallyDefault = true;
            taskActivityType.LinkToPatientDefault = true;
            return (taskActivityType);
        }

        public static Task CreateTask()
        {
            var task = new Task();
            task.Patient = CreatePatient();
            task.Title = "New Task";
            return (task);
        }

        public static PaperClaim CreatePaperClaimViewModel()
        {
            var paperClaimInfo = new PaperClaim();



            paperClaimInfo.BillingProviderName = "test billing provider";
            paperClaimInfo.BillingProviderAddressLine1 = "6 tulip place";
            paperClaimInfo.BillingProviderAddressLine2 = "Suite 500";
            paperClaimInfo.BillingProviderCity = "Howard Beach";
            paperClaimInfo.BillingProviderState = "NY";
            paperClaimInfo.BillingProviderZip = "11414";
            paperClaimInfo.BillingProviderTaxId = "5829304fasd";
            paperClaimInfo.PayToName = "Doctor House";
            paperClaimInfo.PayToAddressLine1 = "45-23 198 avenue";
            paperClaimInfo.PayToAddressLine2 = "second floor";
            paperClaimInfo.PayToCity = "Ozone Park";
            paperClaimInfo.PayToState = "NY";
            paperClaimInfo.PayToZip = "11417";
            paperClaimInfo.PatientId = "555";
            paperClaimInfo.PatientFirstName = "TestFirstName";
            paperClaimInfo.PatientLastName = "TestLastName";
            paperClaimInfo.PatientBirthDate = new DateTime(1982, 1, 7);
            paperClaimInfo.PatientGender = Gender.Male;
            paperClaimInfo.PatientAddressLine1 = "6 tulip place";
            paperClaimInfo.PatientAddressLine2 = "Suite 500";
            paperClaimInfo.PatientCity = "Howard Beach";
            paperClaimInfo.PatientState = "NY";
            paperClaimInfo.PatientZip = "11414";
            paperClaimInfo.AccidentState = "NY";
            paperClaimInfo.BilledPolicyHolderFirstName = "TestGuarantorFirstName";
            paperClaimInfo.BilledPolicyHolderLastName = "TestGuarantorLastName";
            paperClaimInfo.BilledPolicyHolderAddressLine1 = "45-23 198 avenue";
            paperClaimInfo.BilledPolicyHolderAddressLine2 = "second floor";
            paperClaimInfo.BilledPolicyHolderCity = "Ozone Park";
            paperClaimInfo.BilledPolicyHolderState = "NY";
            paperClaimInfo.BilledPolicyHolderZip = "11417";
            paperClaimInfo.PrimaryCoinsuranceCode = "e509";
            paperClaimInfo.PrimaryCoinsuranceAmount = "25.95";
            paperClaimInfo.SecondaryCoinsuranceCode = "4j9";
            paperClaimInfo.SecondaryCoinsuranceAmount = "10.95";
            paperClaimInfo.ThirdCoinsuranceCode = "r3t5";
            paperClaimInfo.ThirdCoinsuranceAmount = "11.95";
            paperClaimInfo.PrimaryDeductibleCode = "jjjj";
            paperClaimInfo.PrimaryDeductibleAmount = "100.00";
            paperClaimInfo.SecondaryDeductibleCode = "3344";
            paperClaimInfo.SecondaryDeductibleAmount = "50.00";
            paperClaimInfo.ThirdDeductibleCode = "2323";
            paperClaimInfo.ThirdDeductibleAmount = "25.00";

            paperClaimInfo.PrintDate = DateTime.Now;

            paperClaimInfo.PrimaryPayerName = "Test Primary Payer Name";
            paperClaimInfo.PrimaryInsuredFirstName = "TestPrimaryInsuredFirstName";
            paperClaimInfo.PrimaryInsuredLastName = "TestPrimaryInsuredLastName";
            paperClaimInfo.PrimaryRelationshipCode = "TestRelationshipcode 234";
            paperClaimInfo.PrimaryPolicyNumber = "34872";
            paperClaimInfo.PrimaryPayment = "50.00";
            paperClaimInfo.PrimaryBalanceDue = "26.49";
            paperClaimInfo.SecondaryPayerName = "TestSecondaryPayerName";
            paperClaimInfo.SecondaryInsuredFirstName = "TestSecondaryINsuredFirstName";
            paperClaimInfo.SecondaryInsuredLastName = "TestSecondaryINsuredLastName";
            paperClaimInfo.SecondaryRelationshipCode = "234lkj";
            paperClaimInfo.SecondaryPolicyNumber = "sdf";
            paperClaimInfo.SecondaryPayment = "20.30";
            paperClaimInfo.SecondaryBalanceDue = "10.34";
            paperClaimInfo.ThirdPayerName = "TestThirdPayerName";
            paperClaimInfo.ThirdInsuredFirstName = "TestThirdInsuredFirstName";
            paperClaimInfo.ThirdInsuredLastName = "TestThirdInsuredLastName";
            paperClaimInfo.ThirdRelationshipCode = "asdf3";
            paperClaimInfo.ThirdPolicyNumber = "234";
            paperClaimInfo.ThirdPayment = "55.55";
            paperClaimInfo.ThirdBalanceDue = "12.54";
            paperClaimInfo.AssignBenefits = true;
            paperClaimInfo.ReleaseInfoIsSigned = true;
            paperClaimInfo.BillingProviderNpi = "TestBillingProviderNpi";
            paperClaimInfo.PrincipalProcedureCode = "TestPrincipalProcedureCode";
            paperClaimInfo.BilledInsuranceName = "TestBilledINsuredName";
            paperClaimInfo.BilledInsuranceAddressLine1 = "Test Billed Insured Address 1";
            paperClaimInfo.BilledInsuranceAddressLine2 = "Test Billed INsured Address 2";
            paperClaimInfo.BilledInsuranceCity = "Test Billed INsurance City";
            paperClaimInfo.BilledInsuranceState = "Test Billed Insurance State";
            paperClaimInfo.BilledInsuranceZip = "Test Billed Insurance Zip";
            paperClaimInfo.BillingProviderTaxonomy = "Test BillingProviderTaxonomy";
            paperClaimInfo.RenderingProviderFirstName = "TestAttendingDoctorFirstName";
            paperClaimInfo.RenderingProviderLastName = "TestAttendingDoctorLastName";
            paperClaimInfo.RenderingProviderNpi = "TestAttendingDoctorNPI";
            paperClaimInfo.ReferringDoctorLastName = "TestReferringDoctorLastName";
            paperClaimInfo.ReferringDoctorFirstName = "TestReferringDoctorFirstName";
            paperClaimInfo.ReferringDoctorNpi = "TestReferringDoctorNPI";

            paperClaimInfo.Diagnoses = new List<DiagnosisInfo>
                                      {
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "Test Other Diagnoses 1",                                                  
                                              },
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "Test Other Diagnoses 2",                                                  
                                              }
                                      };


            paperClaimInfo.BillingProviderName = "test billing provider";
            paperClaimInfo.BillingProviderAddressLine1 = "6 tulip place";
            paperClaimInfo.BillingProviderAddressLine2 = "Suite 500";
            paperClaimInfo.BillingProviderCity = "Howard Beach";
            paperClaimInfo.BillingProviderState = "NY";
            paperClaimInfo.BillingProviderZip = "11414";
            paperClaimInfo.BillingProviderTaxId = "5829304fasd";
            paperClaimInfo.PatientId = "555";
            paperClaimInfo.PatientFirstName = "TestFirstName";
            paperClaimInfo.PatientLastName = "TestLastName";
            paperClaimInfo.PatientBirthDate = new DateTime(1982, 1, 7);

            paperClaimInfo.PatientAddressLine1 = "6 tulip place";
            paperClaimInfo.PatientAddressLine2 = "Suite 500";
            paperClaimInfo.PatientCity = "Howard Beach";
            paperClaimInfo.PatientState = "NY";
            paperClaimInfo.PatientZip = "11414";
            paperClaimInfo.AccidentState = "NY";
            paperClaimInfo.BilledPolicyHolderFirstName = "TestGuarantorFirstName";
            paperClaimInfo.BilledPolicyHolderLastName = "TestGuarantorLastName";
            paperClaimInfo.BilledPolicyHolderAddressLine1 = "45-23 198 avenue";
            paperClaimInfo.BilledPolicyHolderAddressLine2 = "second floor";
            paperClaimInfo.BilledPolicyHolderCity = "Ozone Park";
            paperClaimInfo.BilledPolicyHolderState = "NY";
            paperClaimInfo.BilledPolicyHolderZip = "11417";


            paperClaimInfo.BillingProviderNpi = "TestBillingProviderNpi";

            paperClaimInfo.BilledInsuranceName = "TestBilledINsuredName";
            paperClaimInfo.BilledInsuranceAddressLine1 = "Test Billed Insured Address 1";
            paperClaimInfo.BilledInsuranceAddressLine2 = "Test Billed INsured Address 2";
            paperClaimInfo.BilledInsuranceCity = "Test Billed INsurance City";
            paperClaimInfo.BilledInsuranceState = "Test Billed Insurance State";
            paperClaimInfo.BilledInsuranceZip = "Test Billed Insurance Zip";
            paperClaimInfo.BillingProviderTaxonomy = "Test BillingProviderTaxonomy";

            paperClaimInfo.RenderingProviderNpi = "TestAttendingDoctorNPI";



            paperClaimInfo.Services = new List<PaperClaimServiceInfo>
                                     {
                                         new PaperClaimServiceInfo
                                             {
                                                 ServiceCharge = 25.95m,
                                                 ServiceUnits = "45",
                                                 DiagnosisLetters="AC",
                                                 RevenueCode = "3",                                                 
                                                 ServiceDescription = "my test description",      
                                                 ServiceUnitOfMeasurement = ServiceUnitOfMeasurement.Minute, 
                                                 ServiceLocationCode = "LOC"
                                             },
                                         new PaperClaimServiceInfo
                                             {
                                                 ServiceCharge = 22.95m,
                                                 ServiceUnits = "67",
                                                 DiagnosisLetters="AC",
                                                 RevenueCode = "3",                                                
                                                 ServiceDescription = "my test description",     
                                                 ServiceUnitOfMeasurement = ServiceUnitOfMeasurement.Unit
                                             }
                                     };

            return paperClaimInfo;
        }

        public static PatientInsurance CreatePatientInsurance(Patient patient = null)
        {
            var patientInsurance = new PatientInsurance();
            patientInsurance.EndDateTime = new DateTime(2012, 1, 1);
            patientInsurance.InsurancePolicy = new InsurancePolicy { StartDateTime = new DateTime(2009, 1, 1), Insurer = CreateInsurer(), PolicyholderPatient = patient ?? CreatePatient(), PolicyCode = "TESTPOLICYCODE" };
            patientInsurance.InsuranceType = InsuranceType.Ambulatory;
            patientInsurance.InsuredPatient = patient ?? CreatePatient();
            patientInsurance.PolicyholderRelationshipType = PolicyHolderRelationshipType.Employee;

            return patientInsurance;
        }

        public static PatientInsurance CreatePatientInsuranceForPolicyholder()
        {
            var policyholder = CreatePatient();
            var patientInsurance = new PatientInsurance();
            patientInsurance.EndDateTime = new DateTime(2012, 1, 1);
            patientInsurance.InsurancePolicy = new InsurancePolicy { StartDateTime = new DateTime(2009, 1, 1), Insurer = CreateInsurer(), PolicyholderPatient = policyholder, PolicyCode = "TESTPOLICYCODE" };
            patientInsurance.InsuranceType = InsuranceType.Ambulatory;
            patientInsurance.InsuredPatient = policyholder;
            patientInsurance.PolicyholderRelationshipType = PolicyHolderRelationshipType.Employee;

            return patientInsurance;
        }

        public static PatientInsuranceAuthorization CreatePatientInsuranceAuthorization()
        {
            var pia = new PatientInsuranceAuthorization();
            pia.AuthorizationCode = "TESTCODE";
            pia.AuthorizationDateTime = new DateTime(2011, 1, 1);
            pia.Comment = "TEST COMMENT";
            var appointment = CreateAppointment();
            pia.Encounter = appointment.Encounter;
            if (pia.Encounter.Appointments == null)
                pia.Encounter.Appointments = new List<Model.Appointment>();
            pia.Encounter.Appointments.Add(appointment);
            pia.IsArchived = false;
            pia.PatientInsurance = PracticeRepository.PatientInsurances.OrderBy(i => i.Id).FirstOrDefault() ?? CreatePatientInsurance();
            if (pia.Encounter.Patient.PatientInsurances == null)
                pia.Encounter.Patient.PatientInsurances = new List<PatientInsurance>();
            pia.Encounter.Patient.PatientInsurances.Add(pia.PatientInsurance);

            return pia;
        }

        public static PatientRecall CreatePatientRecall()
        {
            var patientRecall = new PatientRecall();
            patientRecall.Patient = CreatePatient();
            patientRecall.User = CreateUser();
            patientRecall.AppointmentType = CreateAppointmentType();
            patientRecall.DueDateTime = new DateTime(2012, 1, 1);
            patientRecall.MethodSent = MethodSent.Electronic;
            patientRecall.RecallStatus = RecallStatus.Pending;
            return patientRecall;
        }

        public static ClinicalCondition CreateClinicalCondition()
        {
            var condition = new ClinicalCondition
            {
                Name = "Test condition",
                IsSocial = false,
                IsDeactivated = false,
                OrdinalId = 0
            };
            return condition;
        }

        public static void RecreateViewAsTable<TEntity>()
        {
            var dbEntityName = Model.PracticeRepository.GetDbObjectName(typeof(TEntity));
            var entityNameParts = dbEntityName.Split('.').ToArray();
            var noSchemaDbEntityName = string.Join(".", entityNameParts.Skip(1));
            var schemaName = entityNameParts.First();

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(string.Format(@"
SELECT *
INTO {0}_1
FROM {0}

-- If there an Id column -> recreate it as identity column
IF EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'Id' AND [object_id] = OBJECT_ID(N'{0}_1'))
BEGIN
    -- Get current column type
    DECLARE @columnType nvarchar(30); 
    SELECT TOP 1 @columnType = DATA_TYPE
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '{1}' AND TABLE_SCHEMA = '{2}' AND COLUMN_NAME = 'Id'
    
    ALTER TABLE {0}_1 DROP COLUMN Id
    EXEC('ALTER TABLE {0}_1 ADD Id ' + @columnType + ' IDENTITY')
END

DROP VIEW {0}
GO

sp_rename '{0}_1', '{1}'
"
                , dbEntityName, noSchemaDbEntityName, schemaName), false);
        }
    }
}
