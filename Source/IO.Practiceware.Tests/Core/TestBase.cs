﻿using System;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Transactions;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Collections;
using Soaf.Security;
using Soaf.Threading;

namespace IO.Practiceware.Tests
{
    /// <summary>
    /// Database pool name for the test
    /// </summary>
    /// <remarks>
    /// A list is needed, so we can prepare tenants on Web's end
    /// </remarks>
    public enum TestDatabasePoolName
    {
        Default,
        X12Tests,
        HL7Tests,
        CdaTests,
        DocumentGenerationTests
    }

    /// <summary>
    ///     A base class for unit tests.
    /// </summary>
    [TestRunMode(
        UseDedicatedTestDatabase = true,
        ParallelExecutionTolerance = TestParallelExecutionTolerance.SupportsParallelExecution,
        UseTransactionScope = true)]
    public abstract class TestBase
    {
        /// <summary>
        /// Number of parallel threads configured via .testsettings
        /// </summary>
        private const int ParallelThreadsSetInConfig = 6;

        /// <summary>
        /// Notes: 
        /// 1. Can't use [ClassInitialize] or [ClassCleanup] in base class - it's not recognized because of static context.
        /// 2. If parallel unit tests are enabled, multiple instances of the test class under test may be created - so instance variables aren't safe to use.
        /// </summary>

        private static readonly ConcurrentDictionary<TestDatabasePoolName, DatabasePool> DatabasePools = new ConcurrentDictionary<TestDatabasePoolName, DatabasePool>();
        private static readonly ConcurrentDictionary<TestDatabasePoolName, string> DatabasePoolBakFiles = new ConcurrentDictionary<TestDatabasePoolName, string>();

        /// <summary>
        /// Authentication token to connection string mapping
        /// </summary>
        private static readonly ConcurrentDictionary<string, string> PreConfiguredConnectionStrings = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// A lock which is used to by every test. Set it as CustomKeyToLockOn in TestRunMode to run test exclusively on global level
        /// </summary>
        private const string GlobalTestExecutionLockKey = "__Global_Testing_Lock__";

        /// <summary>
        /// Recorded outcome of every test
        /// </summary>
        public static readonly ConcurrentDictionary<MethodInfo, UnitTestRun> TestRuns = new ConcurrentDictionary<MethodInfo, UnitTestRun>();

        private static readonly ConcurrentDictionary<string, ReaderWriterLockSlim> TestExecutionLocks = new ConcurrentDictionary<string, ReaderWriterLockSlim>();
        private static readonly ConcurrentDictionary<MethodInfo, bool> CurrentlyRunningTests = new ConcurrentDictionary<MethodInfo, bool>();

        private IPracticeRepository _practiceRepository;
        private readonly int _threadId = Thread.CurrentThread.ManagedThreadId;
        private DateTime _testExecutionStart;

        public IPracticeRepository PracticeRepository
        {
            get
            {
                return _practiceRepository ?? (_practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>());
            }
        }

        public IServiceProvider ServiceProvider
        {
            get { return Common.ServiceProvider; }
        }

        /// <summary>
        /// Gets the name of the database pool.
        /// If anything else than Default is returned -> InitializeTestDatabase is called upon first request of such database from pool
        /// </summary>
        /// <value>
        /// The name of the database pool.
        /// </value>
        protected virtual TestDatabasePoolName DatabasePoolName
        {
            get { return TestDatabasePoolName.Default; }
        }

        /// <summary>
        ///     Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        private IScope Scope { get; set; }
        private TransactionScope TransactionScope { get; set; }

        /// <summary>
        /// Principal which was acquired to point to correct test database
        /// </summary>
        /// <value>
        /// The database pool principal.
        /// </value>
        private UserPrincipal DatabasePoolPrincipal { get; set; }

        public MethodInfo TestMethod { get; private set; }
        public Type TestClass { get; private set; }
        public TestRunModeAttribute RunningModeAttribute { get; private set; }

        /// <summary>
        /// Provides test assertion capabilities which allow extensions
        /// </summary>
        public static readonly IAssertion Assert = new Assertion();

        /// <summary>
        /// Initialize support for parallel tests execution
        /// </summary>
        public static void Initialize()
        {
            SqlConnectionStringBuilder scb;
            DbConnections.TryGetConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString, out scb);

            foreach (var testPoolName in Enums.GetValues<TestDatabasePoolName>())
            {
                for (int i = 1; i <= ParallelThreadsSetInConfig; i++)
                {
                    DbConnections.TryGetConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString, out scb);

                    // Prepare pool specific suffix
                    var initialCatalogSuffix = testPoolName == TestDatabasePoolName.Default
                        ? "_" + i
                        : string.Format("_{0}_{1}", testPoolName, i);

                    // Set pool specific Db name as initial catalog
                    scb.InitialCatalog = string.Format("{0}{1}",
                        scb.InitialCatalog.Truncate(128 - initialCatalogSuffix.Length), // Ensure total Db name is not greater than 128 characters
                        initialCatalogSuffix);

                    // Add to the list
                    var poolItemConnection = scb.ToString();
                    var poolItemKey = string.Concat(testPoolName, i).ToLower();

                    PreConfiguredConnectionStrings.AddOrUpdate(poolItemKey, poolItemConnection, (k, v) => v);
                }
            }

            var clientConfigurations = PreConfiguredConnectionStrings
                .Select(s => new
                {
                    AuthenticationToken = s.Key,
                    ClientName = s.Key + "Client",
                    ConnectionName = s.Key + "Connection",
                    ServerDataPath = Path.Combine(ConfigurationManager.ServerDataPath, s.Key),
                    ConnectionString = s.Value
                })
                .ToList();

            // Configure test clients for test process
            foreach (var cc in clientConfigurations)
            {
                ConfigurationManager.ConnectionStrings.Items.Add(
                    new ConnectionStringConfiguration(cc.ConnectionName, cc.ConnectionString));

                ConfigurationManager.ApplicationServerConfiguration.Clients.Add(new ClientConfiguration
                {
                    AuthenticationToken = cc.AuthenticationToken,
                    ConnectionStringName = cc.ConnectionName,
                    Name = cc.ClientName,
                    ServerDataPath = cc.ServerDataPath
                });
            }

            ConfigurationManager.Save();

            // Configure test clients for web process
            Common.ModifyWebConfiguration(configuration =>
            {
                var serverConfig = configuration.GetSection<ApplicationServerConfiguration>("applicationServer");
                var connectionStringsConfig = configuration.GetSection<ConnectionStringConfigurationCollection>("customConnectionStrings");

                foreach (var cc in clientConfigurations)
                {
                    // Add connection string
                    var connectionStringConfiguration = connectionStringsConfig.Items.FirstOrDefault(csc => csc.Name == cc.ConnectionName);
                    if (connectionStringConfiguration == null)
                    {
                        connectionStringConfiguration = new ConnectionStringConfiguration
                        {
                            Name = cc.ConnectionName
                        };
                        connectionStringsConfig.Items.Add(connectionStringConfiguration);
                    }
                    connectionStringConfiguration.ConnectionString = cc.ConnectionString;

                    // Add client configuration
                    var clientConfiguration = serverConfig.Clients.FirstOrDefault(c => c.Name == cc.ClientName);
                    if (clientConfiguration == null)
                    {
                        clientConfiguration = new ClientConfiguration
                        {
                            Name = cc.ClientName
                        };
                        serverConfig.Clients.Add(clientConfiguration);
                    }
                    clientConfiguration.AuthenticationToken = cc.AuthenticationToken;
                    clientConfiguration.ConnectionStringName = cc.ConnectionName;
                    clientConfiguration.ServerDataPath = cc.ServerDataPath;
                }
            });
        }

        /// <summary>
        /// Cleanups web.config modifications
        /// </summary>
        public static void Cleanup()
        {
            // Configure test clients for web process
            Common.ModifyWebConfiguration(configuration =>
            {
                var serverConfig = configuration.GetSection<ApplicationServerConfiguration>("applicationServer");
                var connectionStringsConfig = configuration.GetSection<ConnectionStringConfigurationCollection>("customConnectionStrings");

                foreach (var client in serverConfig.Clients
                    .Where(c => c.Name != Common.DefaultApplicationServerClientName).ToList())
                {
                    serverConfig.Clients.Remove(client);

                    var connectionString = connectionStringsConfig.Items.FirstOrDefault(csc => csc.Name == client.ConnectionStringName);
                    connectionStringsConfig.Items.Remove(connectionString);
                }
            });
        }

        [TestInitialize]
        public void OnTestInitializeBase()
        {
            if (Thread.CurrentThread.ManagedThreadId != _threadId)
                throw new InvalidOperationException("Thread has changed unexpectedly.");

            if (ServiceProvider.GetService<IUnitOfWorkProvider>().Current != null) throw new InvalidOperationException("UnitOfWork already exists. This is unexpected.");

            // Identify running test
            TestClass = Type
                .GetType(TestContext.FullyQualifiedTestClassName)
                .EnsureNotDefault("Could not find TestClass.");

            TestMethod = TestClass.GetMember(TestContext.TestName)
                .OfType<MethodInfo>()
                .SingleOrDefault(m => m.HasAttribute<TestMethodAttribute>())
                .EnsureNotDefault("Could not find a TestMethod.");

            // Check how test should execute
            RunningModeAttribute = TestMethod.GetAttribute<TestRunModeAttribute>()
                ?? TestClass.GetAttribute<TestRunModeAttribute>();

            try
            {
                // Lock execution
                if (!AcquireTestLocks())
                {
                    throw new InvalidOperationException("Couldn't acquire all needed locks to run test safely. Failing");
                }

                // Record the test is running now
                CurrentlyRunningTests[TestMethod] = true;

                // Setup connection to dedicated or default database (using principal)
                PrincipalContext.Current.Principal = (RunningModeAttribute.UseDedicatedTestDatabase 
                    ? AcquireAndInitializeDatabaseConnection()
                    : Common.Principal)
                    .ToXml().FromXml<UserPrincipal>(); // Clone to avoid test modifying original principal

                // Setup transaction scope if configured
                if (RunningModeAttribute.UseTransactionScope)
                {
                    Assert.IsNull(Transaction.Current);
                    var ts = CreateTransactionScope();
                    TransactionScope = ts;
                }

                // anything with a scoped lifestyle will be single instance per TestContext
                Scope = ServiceProvider.GetService<IScope>();

                // Ensure server data path exists
                FileSystem.EnsureDirectory(ConfigurationManager.ServerDataPath);

                // Record test start
                _testExecutionStart = DateTime.UtcNow;

                OnAfterTestInitialize();
            }
            catch (Exception ex)
            {
                try
                {
                    // TestCleanup is not called if error occurs upon test initialization, so call manually from here
                    OnTestCleanupBase();
                }
                catch (Exception cleanupEx)
                {
                    throw new AggregateException("An error occurred initializing and cleaning up the test.", ex, cleanupEx);
                }
                throw;
            }
        }

        [TestCleanup]
        public void OnTestCleanupBase()
        {
            var executionTime = DateTime.UtcNow - _testExecutionStart;
            var testOutcome = TestContext.CurrentTestOutcome;
            var cleanupErrorOccured = false;

            try
            {
                try
                {
                    OnBeforeTestCleanup();
                }
                catch (Exception ex)
                {
                    cleanupErrorOccured = true;
                    Trace.TraceError("OnBeforeTestCleanup failed. Error: {0}", ex);
                }

                try
                {
                    // Clear file manager
                    FileManager.Instance.ClearCache();
                    FileSystem.DeleteDirectory(ConfigurationManager.ServerDataPath);
                }
                catch (Exception ex)
                {
                    cleanupErrorOccured = true;
                    Trace.TraceError("Failed to wait for incomplete file manager operations. Error: {0}", ex);
                }

                try
                {
                    // Cleanup scope if set
                    if (TransactionScope != null)
                    {
                        TransactionScope.Dispose();
                        Assert.IsNull(Transaction.Current);
                    }
                }
                catch (Exception ex)
                {
                    cleanupErrorOccured = true;
                    Trace.TraceError("Failed to dispose transaction scope. Error: {0}", ex);
                }

                try
                {
                    // Release database connection
                    ReleaseDatabaseConnection();
                }
                catch (Exception ex)
                {
                    cleanupErrorOccured = true;
                    Trace.TraceError("Failed to release database connection. Error: {0}", ex);
                }

                // Cleanup unit of work
                var unitOfWorkProvider = ServiceProvider.GetService<IUnitOfWorkProvider>();
                var uow = unitOfWorkProvider.Current;
                while (uow != null)
                {
                    uow.Dispose();
                    uow = unitOfWorkProvider.Current;
                }

                try
                {
                    OnAfterTestCleanup();
                }
                catch (Exception ex)
                {
                    cleanupErrorOccured = true;
                    Trace.TraceError("OnAfterTestCleanup failed. Error: {0}", ex);
                }

                // Cleanup scope and stored context data
                if (Scope != null)
                {
                    Scope.Dispose();
                }

                // Limit execution time if configured
                if (RunningModeAttribute.MaxRunTime != null 
                    && executionTime.TotalSeconds > RunningModeAttribute.MaxRunTime)
                {
                    Assert.Inconclusive("Test ran longer than anticipated (over {0} seconds): {1}", RunningModeAttribute.MaxRunTime, executionTime);
                }

                // Record pure test execution time
                Trace.TraceInformation("Test execution time: {0}".FormatWith(executionTime));

                // Any errors upon cleanup? -> fail
                if (cleanupErrorOccured)
                {
                    throw new InvalidOperationException("Test failed to cleanup properly");
                }

                // Validate test finished on the same thread
                if (Thread.CurrentThread.ManagedThreadId != _threadId)
                    throw new InvalidOperationException("Thread has changed unexpectedly.");
            }
            catch
            {
                // Change test outcome and re-throw
                testOutcome = UnitTestOutcome.Error;
                throw;
            }
            finally
            {
                // set the outcome for this current test
                TestRuns[TestMethod] = new UnitTestRun { Outcome = testOutcome, Duration = executionTime, TestContext = TestContext };

                // remove this test from the list of currently running tests
                bool result;
                CurrentlyRunningTests.TryRemove(TestMethod, out result);

                if (testOutcome != UnitTestOutcome.Passed)
                {
                    var runningTests = CurrentlyRunningTests.Keys.Select(test => string.Format("{0}.{1}", test.DeclaringType.EnsureNotDefault().FullName, test.Name)).Join();
                    Trace.TraceError("{0}.{1} failed. The following tests were currently running at the same time: {2}".FormatWith(TestMethod.DeclaringType.EnsureNotDefault().FullName, TestMethod.Name, runningTests));
                }

                // Release execution lock
                ReleaseTestLocks();
            }
        }

        /// <summary>
        /// Returns a lock which allows to synchronize test operations globally
        /// </summary>
        /// <returns></returns>
        protected static ReaderWriterLockSlim GetGlobalLock()
        {
            return TestExecutionLocks.GetValue(GlobalTestExecutionLockKey, Locks.GetLockInstance);
        }

        /// <summary>
        /// Called after TestInitialize code executes in TestBase.
        /// </summary>
        protected virtual void OnAfterTestInitialize()
        {

        }

        /// <summary>
        /// Called before TestCleanup code executes in TestBase.
        /// </summary>
        protected virtual void OnBeforeTestCleanup()
        {

        }

        /// <summary>
        /// Called after TestCleanup code executes in TestBase.
        /// No DB calls in here
        /// </summary>
        protected virtual void OnAfterTestCleanup()
        {

        }


        /// <summary>
        /// Initializes the test database. Allows for the test class to apply re-usable scripts that other methods in the test class can use.
        /// </summary>
        protected virtual void InitializeTestDatabase()
        {

        }

        /// <summary>
        /// Creates the transaction scope for the test.
        /// </summary>
        /// <returns></returns>
        protected virtual TransactionScope CreateTransactionScope()
        {
            var ts = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted,
                Timeout = TimeSpan.FromMinutes(10)
            });
            return ts;
        }

        /// <summary>
        /// Creates the dedicated test database.
        /// </summary>
        private void CreateTestDatabase(string connectionString, string databaseName, Action initializeNonDefaultDatabase)
        {
            // only create 1 base line DB if the class needs to initialize it
            if (DatabasePoolName != TestDatabasePoolName.Default)
            {
                bool added = false;
                var bakPath = DatabasePoolBakFiles
                         .GetValue(DatabasePoolName, () =>
                         {
                             var backupPath = Common.GetDatabaseBackupPath(connectionString);
#if BlindUseOfLocalBackup
                             if (!System.IO.File.Exists(backupPath))
#endif
                             {
                                 // create the db, initialize it, back it up, and use it for all tests in this pool
                                 Common.CreateDatabaseFromBase(databaseName);
                                 initializeNonDefaultDatabase();
                                 Common.BackupDatabase(connectionString);
                             }

                             added = true;
                             return backupPath;
                         });

                // someone else already made the backup of the initialized db...so use that
                if (!added) Common.RestoreDatabase(bakPath, databaseName);
            }
            else
            {
                Common.CreateDatabaseFromBase(databaseName);
            }
        }

        private UserPrincipal AcquireAndInitializeDatabaseConnection()
        {
            // Get pool for this database
            var pool = DatabasePools.GetValue(DatabasePoolName, () => new DatabasePool());

            // Acquire connection to dedicated instance of the database
            var poolDatabasePrincipal = pool.Request(i =>
            {
                // Ensure no pool requests more databases than configured number of parallel threads
                var runningTests = CurrentlyRunningTests.Keys.Select(test => string.Format("{0}.{1}", test.DeclaringType.EnsureNotDefault().FullName, test.Name)).Join();
                
                Assert.IsTrue(i <= ParallelThreadsSetInConfig, "Test {0}.{1} requested {2}th database from pool {3}. Find out why. Other tests running {4}"
                    .FormatWith(TestMethod.EnsureNotDefault().DeclaringType.EnsureNotDefault().FullName, TestMethod.Name,
                    i, DatabasePoolName, runningTests));

                var authenticationToken = string.Concat(DatabasePoolName, i).ToLower();
                var connectionString = PreConfiguredConnectionStrings[authenticationToken];
                SqlConnectionStringBuilder scb;
                DbConnections.TryGetConnectionStringBuilder(connectionString, out scb);
                UserPrincipal result = null;

                using (new TimedScope(s => Trace.TraceInformation("Created dedicated test database {0} in {1}.".FormatWith(scb.InitialCatalog, s))))
                {
                    CreateTestDatabase(connectionString, scb.InitialCatalog, () =>
                    {
                        // Login immediately for initialize test database to work properly
                        result = PrepAuthentication.Login(authenticationToken);
                        InitializeTestDatabase();
                    });
                }
                return result ?? PrepAuthentication.Login(authenticationToken);
            });

            // Record retrieved database connection principal
            DatabasePoolPrincipal = poolDatabasePrincipal;

            Trace.TraceInformation("Test {0}.{1} will run against pool database {2}.".FormatWith(TestMethod.EnsureNotDefault().DeclaringType.EnsureNotDefault().FullName, TestMethod.Name, poolDatabasePrincipal.Identity.Name));

            return poolDatabasePrincipal;
        }

        /// <summary>
        /// Releases database connection if acquired
        /// </summary>
        private void ReleaseDatabaseConnection()
        {
            if (DatabasePoolPrincipal == null) return;

            var pool = DatabasePools[DatabasePoolName];
            pool.Release(DatabasePoolPrincipal);
            DatabasePoolPrincipal = null;

            Trace.TraceInformation("Test {0}.{1} ran against pool database {2}."
                .FormatWith(TestMethod.EnsureNotDefault().DeclaringType.EnsureNotDefault().FullName, TestMethod.Name, ApplicationContext.Current.ClientKey));

            // Restore default principal
            PrincipalContext.Current.Principal = Common.Principal.ToXml().FromXml<UserPrincipal>();
        }

        /// <summary>
        /// Acquires needed locks to perform test execution
        /// </summary>
        /// <returns>Whether all locks were acquired</returns>
        private bool AcquireTestLocks()
        {
            var acquired = true;

            var acquireLockTimeout = TimeSpan.FromSeconds(ParallelThreadsSetInConfig * 60);
            var globalLock = GetGlobalLock();
            var classLock = TestExecutionLocks.GetValue(TestClass.FullName, Locks.GetLockInstance);

            switch (RunningModeAttribute.ParallelExecutionTolerance)
            {
                case TestParallelExecutionTolerance.RequiresLockOnCustomKey:
                    acquired &= globalLock.TryEnterReadLock(acquireLockTimeout);
                    acquired &= classLock.TryEnterReadLock(acquireLockTimeout);
                    acquired &= TestExecutionLocks.GetValue(RunningModeAttribute.CustomKeyToLockOn, Locks.GetLockInstance).TryEnterWriteLock(acquireLockTimeout);
                    break;
                case TestParallelExecutionTolerance.RequiresTestClassLock:
                    acquired &= globalLock.TryEnterReadLock(acquireLockTimeout);
                    acquired &= classLock.TryEnterWriteLock(acquireLockTimeout);
                    break;
                case TestParallelExecutionTolerance.RequiresGlobalLock:
                    acquired &= globalLock.TryEnterWriteLock(acquireLockTimeout);
                    acquired &= classLock.TryEnterReadLock(acquireLockTimeout);
                    break;
                default:
                    acquired &= globalLock.TryEnterReadLock(acquireLockTimeout);
                    acquired &= classLock.TryEnterReadLock(acquireLockTimeout);
                    break;
            }

            return acquired;
        }

        /// <summary>
        /// Releases locks held during test execution
        /// </summary>
        private void ReleaseTestLocks()
        {
            var globalLock = GetGlobalLock();
            var classLock = TestExecutionLocks[TestClass.FullName];

            switch (RunningModeAttribute.ParallelExecutionTolerance)
            {
                case TestParallelExecutionTolerance.RequiresLockOnCustomKey:
                    Locks.ReleaseReadOnlyLock(globalLock);
                    Locks.ReleaseReadOnlyLock(classLock);
                    Locks.ReleaseWriteLock(TestExecutionLocks[RunningModeAttribute.CustomKeyToLockOn]);
                    break;
                case TestParallelExecutionTolerance.RequiresTestClassLock:
                    Locks.ReleaseReadOnlyLock(globalLock);
                    Locks.ReleaseWriteLock(classLock);
                    break;
                case TestParallelExecutionTolerance.RequiresGlobalLock:
                    Locks.ReleaseWriteLock(globalLock);
                    Locks.ReleaseReadOnlyLock(classLock);
                    break;
                default:
                    Locks.ReleaseReadOnlyLock(globalLock);
                    Locks.ReleaseReadOnlyLock(classLock);
                    break;
            }
        }

        /// <summary>
        /// A simple request/release pool for databases.
        /// </summary>
        private class DatabasePool
        {
            private readonly ConcurrentBag<UserPrincipal> _availablePrincipals = new ConcurrentBag<UserPrincipal>();
            private readonly SemaphoreSlim _semaphore;
            private int _allConnectionStringsCount;

            public DatabasePool()
            {
                // Evaluate max size as +1 to throw exceptions when more databases are requested than expected
                const int maxSize = ParallelThreadsSetInConfig + 1;

                _semaphore = new SemaphoreSlim(maxSize, maxSize);
            }

            public UserPrincipal Request(Func<int, UserPrincipal> preparePoolDatabasePrincipal)
            {
                _semaphore.Wait();

                lock (_availablePrincipals)
                {
                    UserPrincipal result;
                    if (!_availablePrincipals.TryTake(out result))
                    {
                        try
                        {
                            result = preparePoolDatabasePrincipal(Interlocked.Increment(ref _allConnectionStringsCount));
                        }
                        catch
                        {
                            // If producing new connection string fails -> don't count it as released from pool
                            Interlocked.Decrement(ref _allConnectionStringsCount);
                            throw;
                        }
                    }
                    return result;
                }
            }

            public void Release(UserPrincipal poolDatabasePrincipal)
            {
                lock (_availablePrincipals)
                {
                    _semaphore.Release();
                    _availablePrincipals.Add(poolDatabasePrincipal);
                }
            }
        }
    }

    /// <summary>
    /// Allows to configure test execution
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true)]
    public class TestRunModeAttribute : Attribute
    {
        /// <summary>
        /// Denotes how well a test executes parallel execution
        /// </summary>
        public TestParallelExecutionTolerance ParallelExecutionTolerance { get; set; }

        /// <summary>
        /// To be used with TestParallelExecutionTolerance.RequiresLockOnCustomKey
        /// </summary>
        public string CustomKeyToLockOn { get; set; }

        /// <summary>
        /// Indicates that the class or method should have a dedicated test DB created.
        /// </summary>
        public bool UseDedicatedTestDatabase { get; set; }

        /// <summary>
        /// Denotes that a method or class should wrap itself in a transaction.
        /// </summary>
        public bool UseTransactionScope { get; set; }

        /// <summary>
        /// Specifies an individual maximum runtime for the test (in seconds). <c>null</c> for no limit
        /// </summary>
        public int? MaxRunTime { get; set; }

        public TestRunModeAttribute()
        {
            UseDedicatedTestDatabase = true;
            UseTransactionScope = true;
            MaxRunTime = null;
            ParallelExecutionTolerance = TestParallelExecutionTolerance.SupportsParallelExecution;
        }
    }

    public enum TestParallelExecutionTolerance
    {
        SupportsParallelExecution,
        RequiresLockOnCustomKey,
        RequiresTestClassLock,
        RequiresGlobalLock
    }

    public class UnitTestRun
    {
        public UnitTestOutcome Outcome { get; set; }

        public TimeSpan Duration { get; set; }

        public TestContext TestContext { get; set; }
    }
}