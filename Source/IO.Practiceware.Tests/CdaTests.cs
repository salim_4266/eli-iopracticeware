﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ExternalSystems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Constants = IO.Practiceware.Integration.Cda.Constants;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class CdaTests : TestBase
    {
        private static DateTime _currentDate = new DateTime(2013, 11, 19, 9, 0, 0);
        private static readonly DateTime DocumentDateTime = new DateTime(2013, 11, 19, 9, 0, 0);
        private static readonly Guid DocumentId = new Guid("6ac75b12-4682-4b4e-9caa-ff2fbdbf8677");

        protected override TestDatabasePoolName DatabasePoolName
        {
            get { return TestDatabasePoolName.CdaTests; }
        }

        protected override void InitializeTestDatabase()
        {
            base.InitializeTestDatabase();

            // Note: once a view supports CRUD operations -> remove from the list
            Common.RecreateViewAsTable<EncounterTransitionOfCareOrder>();
            Common.RecreateViewAsTable<EncounterPatientEducation>();
            Common.RecreateViewAsTable<EncounterClinicalInstruction>();
            Common.RecreateViewAsTable<EncounterDiagnosticTestOrder>();
            Common.RecreateViewAsTable<EncounterAdministeredMedication>();
            Common.RecreateViewAsTable<EncounterTreatmentGoalAndInstruction>();
            Common.RecreateViewAsTable<EncounterReasonForVisitComment>();

            Common.RecreateViewAsTable<Medication>();
            Common.RecreateViewAsTable<ClinicalQualifier>();
            Common.RecreateViewAsTable<ClinicalQualifierCategory>();
            Common.RecreateViewAsTable<DiagnosticTestOrder>();
            Common.RecreateViewAsTable<LaboratoryTestOrder>();

            Common.RecreateViewAsTable<PatientEducation>();
            Common.RecreateViewAsTable<PatientMedication>();
            Common.RecreateViewAsTable<PatientMedicationDetail>();
            Common.RecreateViewAsTable<PatientAllergen>();
            Common.RecreateViewAsTable<PatientSmokingStatus>();
            Common.RecreateViewAsTable<PatientAllergenAllergenReactionType>();
            Common.RecreateViewAsTable<PatientAllergenDetail>();
            Common.RecreateViewAsTable<PatientCognitiveStatusAssessment>();
            Common.RecreateViewAsTable<PatientLaboratoryTestResult>();
            Common.RecreateViewAsTable<PatientFunctionalStatusAssessment>();
            Common.RecreateViewAsTable<PatientBloodPressure>();
            Common.RecreateViewAsTable<PatientHeightAndWeight>();
            Common.RecreateViewAsTable<PatientProcedurePerformed>();
            Common.RecreateViewAsTable<TreatmentGoalAndInstruction>();
            Common.RecreateViewAsTable<PatientDiagnosis>();
            Common.RecreateViewAsTable<PatientDiagnosisDetailAxisQualifier>();

            using (var c = new SqlConnection(Configuration.ConfigurationManager.PracticeRepositoryConnectionString))
            {
                // Running outside of transaction
                c.RunScript(@"TRUNCATE TABLE dm.PrimaryDiagnosisTable", false);
            }

            InitializeMappings();
        }

        /// <summary>
        /// Tests the generation of transition of care documents.
        /// </summary>
        /// <remarks>
        /// The data used in this test is fully formed so that all parts of the document are generated.
        /// </remarks>
        [TestMethod]
        public void TestTransitionOfCareGeneration()
        {
            using (new DisposableScope<string>(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey, 
                k => ContextData.Set(k, "IoTestTcg_" + Guid.NewGuid()), 
                k => ContextData.Remove(k)))
            {
                var dischargedEncounterId = InitializeData();
                var messageProducer = Common.ServiceProvider.GetService<TransitionOfCareMessageProducer>();
                messageProducer.DocumentEffectiveTime = DocumentDateTime;
                messageProducer.DocumentId = DocumentId;

                var patientId = Common.PracticeRepository.Encounters.Where(e => e.Id == dischargedEncounterId).Select(e => e.PatientId).First();
                CdaMessageProducerUtilities.SynchronizePatientDiagnosisDetails(new List<int> { patientId }, null);
                var cdaMessage = messageProducer.ProduceMessage(patientId);

                Assert.AreEqual(NormalizeForComparison(CdaResources.TransitionOfCareDocument),
                    NormalizeForComparison(cdaMessage.ToString()));
            }
        }

        /// <summary>
        /// Tests the generation of visit summary documents.
        /// </summary>
        /// <remarks>
        /// The data used in this test is fully formed so that all parts of the document are generated.
        /// </remarks>
        [TestMethod]
        public void TestVisitSummaryGeneration()
        {
            using (new DisposableScope<string>(ExternalSystemServices.CustomImplicitCachingClientKeyContextDataKey,
                k => ContextData.Set(k, "IoTestVsg_" + Guid.NewGuid()),
                k => ContextData.Remove(k)))
            {
                var dischargedEncounterId = InitializeData();
                var messageProducer = Common.ServiceProvider.GetService<EncounterClinicalSummaryMessageProducer>();
                messageProducer.DocumentEffectiveTime = DocumentDateTime;
                messageProducer.DocumentId = DocumentId;

                CdaMessageProducerUtilities.SynchronizePatientDiagnosisDetails(null, new List<int> { dischargedEncounterId });
                var cdaMessage = messageProducer.ProduceMessage(dischargedEncounterId);
                Assert.AreEqual(NormalizeForComparison(CdaResources.VisitSummaryDocument),
                    NormalizeForComparison(cdaMessage.ToString()));
            }
        }

        private static string NormalizeForComparison(string cdaMessageXml)
        {
            // TODO: maybe come up with something smarter
            // Ensure timezone differences are not accounted, so removing time
            var result = Regex.Replace(cdaMessageXml,
                "(time|high|low) value=\".*?\"", string.Empty, RegexOptions.IgnoreCase);

            // Entity ids may vary. Removing them
            result = Regex.Replace(result,
                "extension=\".*?\"", string.Empty, RegexOptions.IgnoreCase);

            return result;
        }

        private static Patient CreateAndInitializePatient()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();

            var patient = Common.CreatePatient();
            Common.AddAddresses(patient);
            Common.AddPhoneNumbers(patient);
            Common.AddEmailAddresses(patient);

            patient.Races = new List<Race>();
            patient.Races.Add(practiceRepository.Races.First(pr => pr.Name == Race.White));
            patient.EthnicityId = practiceRepository.Ethnicities.First(pr => pr.Name == Ethnicity.HispLatino).Id;
            patient.LanguageId = practiceRepository.Languages.First(pr => pr.Name == Language.English).Id;
            patient.BillingOrganizationId = practiceRepository.BillingOrganizations.First(bo => bo.IsMain).Id;

            var patientExternalProvider = Common.CreatePatientExternalProvider();
            patientExternalProvider.Patient = patient;

            var clinicalCondition = practiceRepository.ClinicalConditions.First();
            var clinicalDataSourceType = practiceRepository.ClinicalDataSourceTypes.First();

            #region PatientLaboratoryTestResult

            patient.PatientLaboratoryTestResults.Add(new PatientLaboratoryTestResult
            {
                EnteredDateTime = _currentDate,
                ObservationDateTime = _currentDate,
                ClinicalCondition = clinicalCondition,
                LaboratoryNormalRange = new LaboratoryNormalRange { Name = "Test LaboratoryNormalRange" },
                LaboratoryTestAbnormalFlag = new LaboratoryTestAbnormalFlag { Abbreviation = "LaboratoryTestAbnormalFlag Test Abbrviation", Name = "TestLaboratoryTestAbnormalFlag" },
                LaboratoryTest = practiceRepository.LaboratoryTests.First(),
                ResultUnit = 100,
                UnitOfMeasurementId = 3,
                UserId = 1,
                ClinicalDataSourceType = clinicalDataSourceType
            });

            #endregion

            #region PatientDiagnosis

            var patientDiagnosis = new PatientDiagnosis
            {
                ClinicalCondition = clinicalCondition,
                ClinicalDataSourceType = clinicalDataSourceType
            };
            var patientDiagnosisDetail = new PatientDiagnosisDetail
            {
                EnteredDateTime = _currentDate,
                StartDateTime = _currentDate.AddDays(1),
                EndDateTime = _currentDate.AddDays(2),
                IsDeactivated = false,
                IsEncounterDiagnosis = true,
                UserId = 1,
                ClinicalConditionStatus = ClinicalConditionStatus.Active
            };
            patientDiagnosisDetail.PatientDiagnosisDetailAxisQualifiers.Add(new PatientDiagnosisDetailAxisQualifier
            {
                AxisQualifier = AxisQualifier.HistoryOf
            });
            patientDiagnosis.PatientDiagnosisDetails.Add(patientDiagnosisDetail);
            

            patientDiagnosis.PatientProblemConcernLevels.Add(new PatientProblemConcernLevel
            {
                ConcernLevel = ConcernLevel.Active,
                EnteredDateTime = _currentDate,
                ConcernStartDate = _currentDate.AddDays(1),
                ConcernEndDate = _currentDate.AddDays(2),
            });
            patient.PatientDiagnoses.Add(patientDiagnosis);

            #endregion

            #region Patient Status Assessments

            patient.PatientFunctionalStatusAssessments.Add(new PatientFunctionalStatusAssessment
            {
                IsDeactivated = false,
                ClinicalCondition = clinicalCondition,
                EnteredDateTime = _currentDate,
                ClinicalDataSourceType = clinicalDataSourceType,
                UserId = 1
            });
            patient.PatientCognitiveStatusAssessments.Add(new PatientCognitiveStatusAssessment
            {
                IsDeactivated = false,
                ClinicalCondition = clinicalCondition,
                EnteredDateTime = _currentDate,
                ClinicalDataSourceType = clinicalDataSourceType,
                UserId = 1
            });

            #endregion

            #region PatientMedication

            var medication = practiceRepository.Medications.First(m => m.Name == "CdaTestMedication");
            var routeOfAdministration = practiceRepository.RouteOfAdministrations.First();

            // 1st medication
            var patientMedication = new PatientMedication
            {
                Medication = medication,
                ClinicalDataSourceType = clinicalDataSourceType
            };
            patientMedication.PatientMedicationDetails.Add(new PatientMedicationDetail
            {
                IsDeactivated = false,
                MedicationEventDateTime = _currentDate,
                MedicationEventType = MedicationEventType.PatientStarted,
                AsPrescribedComment = "TestAsPrescribedComment",
                RouteOfAdministration = routeOfAdministration
            });
            patient.PatientMedications.Add(patientMedication);

            // 2nd medication (same as previous one, but with another date)
            patientMedication = new PatientMedication
            {
                Medication = medication,
                ClinicalDataSourceType = clinicalDataSourceType
            };
            patientMedication.PatientMedicationDetails.Add(new PatientMedicationDetail
            {
                IsDeactivated = false,
                MedicationEventDateTime = _currentDate.AddDays(-1),
                MedicationEventType = MedicationEventType.PatientStarted,
                AsPrescribedComment = "TestAsPrescribedComment_Medication2",
                RouteOfAdministration = routeOfAdministration
            });
            patient.PatientMedications.Add(patientMedication);

            #endregion

            #region PatientAllergen

            var allergen = practiceRepository.Allergens.First();
            var allergenReactionType = practiceRepository.AllergenReactionTypes.First();

            var patientAllergen = new PatientAllergen
            {
                Allergen = allergen,
                ClinicalDataSourceType = clinicalDataSourceType
            };
            patientAllergen.PatientAllergenAllergenReactionTypes.Add(new PatientAllergenAllergenReactionType
            {
                AllergenReactionType = allergenReactionType,
                PatientAllergen = patientAllergen,
            });
            patientAllergen.PatientAllergenDetails.Add(new PatientAllergenDetail
            {
                EnteredDateTime = _currentDate,
                StartDateTime = _currentDate.AddDays(1),
                EndDateTime = _currentDate.AddDays(2),
                ClinicalConditionStatus = ClinicalConditionStatus.Active,
            });
            patientAllergen.PatientProblemConcernLevels.Add(new PatientProblemConcernLevel
            {
                EnteredDateTime = _currentDate,
                ConcernStartDate = _currentDate.AddDays(1),
                ConcernEndDate = _currentDate.AddDays(2),
                ConcernLevel = ConcernLevel.Completed
            });

            patient.PatientAllergens.Add(patientAllergen);

            #endregion

            practiceRepository.Save(patient);
            return patient;
        }

        private static void AddEncounterTransitionOfCareOrder(Encounter encounter)
        {
            var transitionOfCareOrder = new EncounterTransitionOfCareOrder
            {
                TransitionOfCareReason = new TransitionOfCareReason
                {
                    Value = "TestTransitionOfCareReason"
                },
                ExternalProviderId = 2, // VICTOR ABDY, M.D.
                SentDateTime = _currentDate.Subtract(TimeSpan.FromDays(1)),
                TransitionOfCareOrderStatusId = (int)TransitionOfCareOrderStatusId.NotReviewed,
                TransitionOfCareOrder = new TransitionOfCareOrder
                {
                    Name = "TestTransitionOfCareOrder"
                },
                ExternalProviderCommunicationType = ExternalProviderCommunicationType.ClinicalDocumentArchitectureFile
            };
            encounter.EncounterTransitionOfCareOrders.Add(transitionOfCareOrder);
        }

        private static void AddEncounterTreatmentGoalAndInstruction(Encounter encounter, int treatmentGoalId)
        {
            var encounterTreatmentGoalAndInstruction = new EncounterTreatmentGoalAndInstruction
            {
                Value = "TestEncounterTreatmentGoalAndInstructionValue",
                TreatmentGoalAndInstruction = new TreatmentGoalAndInstruction
                {
                    Value = "TestInstructionValue",
                    ShortName = "TestInstructionShortName",
                    TreatmentGoalId = treatmentGoalId
                }
            };
            encounter.EncounterTreatmentGoalAndInstructions.Add(encounterTreatmentGoalAndInstruction);
        }

        private static void AddPatientBloodPressure(Encounter encounter, int patientId, int clinicalDataSourceTypeId)
        {
            encounter.PatientBloodPressures.Add(new PatientBloodPressure
            {
                ClinicalDataSourceTypeId = clinicalDataSourceTypeId,
                DiastolicPressure = 10,
                SystolicPressure = 10,
                PatientId = patientId,
                EnteredDateTime = _currentDate,
                MeasuredDateTime = _currentDate,
                UserId = 1
            });
        }

        private static void AddPatientHeightAndWeight(Encounter encounter, int patientId, int clinicalDataSourceTypeId)
        {
            encounter.PatientHeightAndWeights.Add(new PatientHeightAndWeight
            {
                ClinicalDataSourceTypeId = clinicalDataSourceTypeId,
                PatientId = patientId,
                EnteredDateTime = _currentDate,
                MeasuredDateTime = _currentDate,
                HeightUnit = 1524, // 5ft -> 1524mm
                WeightUnit = 68038, // 150lbs -> ~ 68038g
                UserId = 1
            });
        }

        private static IQueryable<ClinicalProcedure> GetMappedClinicalProcedures()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();

            var snomedClinicalProcedureEntityId = practiceRepository.ExternalSystemEntities
                .Where(e => e.ExternalSystemId == (int)ExternalSystemId.SnomedCt && e.Name == typeof(ClinicalProcedure).Name)
                .Select(e => e.Id)
                .First();
            var mappedClinicalProcedureIds = practiceRepository.ExternalSystemEntityMappings
                .Where(m => m.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.ClinicalProcedure
                            && m.ExternalSystemEntityId == snomedClinicalProcedureEntityId)
                .Select(m => m.PracticeRepositoryEntityKey);

            var clinicalProcedures = practiceRepository.ClinicalProcedures
                    .Where(i => mappedClinicalProcedureIds
                        .Contains(SqlFunctions.StringConvert((double)i.Id).Trim()))
                    .OrderBy(i => i.Id); // Force order by Id for improved match

            return clinicalProcedures;
        }

        private static void AddPatientProcedurePerformed(Encounter encounter, int patientId, int clinicalDataSourceTypeId)
        {
            var mappedProcedures = GetMappedClinicalProcedures();

            foreach (var category in Enums.GetValues<ClinicalProcedureCategory>())
            {
                // Add only for categories which have a mapped value
                var clinicalProcedure = mappedProcedures.FirstOrDefault(i => i.ClinicalProcedureCategory == category);
                if (clinicalProcedure == null) continue;

                encounter.PatientProcedurePerformeds.Add(new PatientProcedurePerformed
                {
                    ClinicalProcedureId = clinicalProcedure.Id,
                    ClinicalDataSourceTypeId = clinicalDataSourceTypeId,
                    PatientId = patientId,
                    PerformedDateTime = _currentDate,
                });
            }
        }

        private static void AddEncounterLaboratoryTestOrder(Encounter encounter, int patientLaboratoryTestResultId, int laboratoryTestOrderId)
        {
            encounter.EncounterLaboratoryTestOrders.Add(new EncounterLaboratoryTestOrder
            {
                PatientLaboratoryTestResultId = patientLaboratoryTestResultId,
                LaboratoryTestOrderId = laboratoryTestOrderId
            });
        }


        private static void InitializeMappings()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();

            // Medication (0 records in test database)
            var medication = new Medication
            {
                IsFollowUpRequired = false,
                Name = "CdaTestMedication",
            };
            practiceRepository.Save(medication);
            // Map it
            CreateNewMapping(typeof(Medication).Name, medication.Id,
                ExternalSystemId.RxNorm, Constants.RxNormMedicationsExternalEntityName, "3");

            // ClinicalQualifier (0 records in test database)
            // TODO: remove when AllergenReactionTypes are back in sync with ClinicalQualifiers table
            var clinicalQualifiersToCreate = practiceRepository.AllergenReactionTypes
                .Select(art => art.ClinicalQualifierId)
                .Distinct()
                .OrderBy(qId => qId)
                .ToArray();
            foreach (var clinicalQualifierId in clinicalQualifiersToCreate)
            {
                var clinicalQualifier = practiceRepository.ClinicalQualifiers.FirstOrDefault(cq => cq.Id == clinicalQualifierId);
                if (clinicalQualifier != null) throw new InvalidOperationException(string.Format("A clinical qualifier {0} is already present. Remove code in unit test which creates it", clinicalQualifierId));

                // Create clinical qualifier
                DbConnectionFactory.PracticeRepository.RunScriptAndDispose(string.Format(@"
SET IDENTITY_INSERT {0} ON
INSERT {0} ([Id], [Name], [OrdinalId], [IsDeactivated]) 
VALUES ({1}, N'{2}', 1, 0)
SET IDENTITY_INSERT {0} OFF"
                    , Model.PracticeRepository.GetDbObjectName(typeof(ClinicalQualifier))
                    ,clinicalQualifierId
                    ,"TestClinicalQualifier" + clinicalQualifierId));

                // Ensure category exists
                clinicalQualifier = practiceRepository.ClinicalQualifiers.WithId(clinicalQualifierId);
                clinicalQualifier.ClinicalQualifierCategory = new ClinicalQualifierCategory
                {
                    Name = "TestClinicalQualifierCategory" + clinicalQualifierId,
                    OrdinalId = 1
                };
                practiceRepository.Save(clinicalQualifier);

                // Map it to SnomedCt
                CreateNewMapping(typeof(ClinicalQualifier).Name, clinicalQualifierId,
                    ExternalSystemId.SnomedCt, Constants.SnomedClinicalQualifierExternalEntityName, clinicalQualifier.Name);
            }

            // ClinicalCondition (0 records in test database)
            var clincalCondition = new ClinicalCondition { Name = "TestClinicalCondition", };
            practiceRepository.Save(clincalCondition);
            CreateNewMapping(typeof(ClinicalCondition).Name, clincalCondition.Id,
                ExternalSystemId.SnomedCt, Constants.SnomedClinicalConditionExternalEntityName, "60204005");

            // LaboratoryTest (0 records in test database)
            var labTest = new LaboratoryTest { Name = "TestLaboratoryTest" };
            practiceRepository.Save(labTest);
            CreateNewMapping(typeof(LaboratoryTest).Name, labTest.Id,
                ExternalSystemId.Loinc, Constants.LoincLaboratoryTestExternalEntityName, "10014-9");

            // TODO: Temporary map Laterality to Snomed here until fixed
            var lateralityExternalEntity = practiceRepository.ExternalSystemEntities.FirstOrDefault(
                e => e.ExternalSystemId == (int)ExternalSystemId.SnomedCt
                    && e.Name == Constants.SnomedLateralityEntityName);
            if (lateralityExternalEntity != null)
            {
                throw new InvalidOperationException("Laterality is now registered as external entity for SnomedCt. Remove this fix in unit test.");
            }

            // Create and save
            lateralityExternalEntity = new ExternalSystemEntity
            {
                ExternalSystemId = (int)ExternalSystemId.SnomedCt,
                Name = Constants.SnomedLateralityEntityName
            };
            practiceRepository.Save(lateralityExternalEntity);

            foreach (var laterality in Enums.GetValues<Laterality>())
            {
                // Mapping to ClinicalAttribute, since we don't have Laterality entity for SnomedCt
                CreateNewMapping(typeof (Laterality).Name, (int) laterality,
                    ExternalSystemId.SnomedCt, Constants.SnomedLateralityEntityName, "Laterality_" + laterality);
            }
        }

        private static void CreateNewMapping(string practiceRepositoryEntityName, int practiceRepositoryEntityKey, ExternalSystemId externalSystemId, string externalSystemEntityName, string externalSystemEntityKey)
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(
            @"
            INSERT INTO model.ExternalSystemEntityMappings
            ([PracticeRepositoryEntityId], [PracticeRepositoryEntityKey], [ExternalSystemEntityId], [ExternalSystemEntityKey])
            SELECT TOP 1 pre.Id, {0}, ese.Id, '{1}'
            FROM model.PracticeRepositoryEntities pre, 
            model.ExternalSystemEntities ese
            WHERE pre.Name = '{2}'
            AND ese.ExternalSystemId = {3} AND ese.Name = '{4}'
            ".FormatWith(practiceRepositoryEntityKey, externalSystemEntityKey, practiceRepositoryEntityName, (int)externalSystemId, externalSystemEntityName));
        }

        /// <summary>
        /// Initializes data and returns a discharged encounter id.
        /// </summary>
        /// <returns></returns>
        private static int InitializeData()
        {
            var patient = CreateAndInitializePatient();

            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();

            var encounterType = practiceRepository.EncounterTypes.OrderBy(i => i.Id).First();
            var serviceLocation = practiceRepository.ServiceLocations.OrderBy(i => i.Id).First();
            var treatmentGoal = practiceRepository.TreatmentGoals.First();
            var clinicalDataSourceType = practiceRepository.ClinicalDataSourceTypes.First();
            var clinicalProcedure = GetMappedClinicalProcedures().First();

            var laboratoryTestOrder = new LaboratoryTestOrder
                {
                    LaboratoryTest = practiceRepository.LaboratoryTests.First(),
                    SimpleDescription = "test simple description",
                    TechnicalDescription = "test technical description"
                };

            practiceRepository.Save(laboratoryTestOrder);

            var patientId = patient.Id;
            var patientLaboratoryTestResult = practiceRepository.PatientLaboratoryTestResults
                .Include(i => i.LaboratoryTest.LaboratoryTestOrders)
                .First(i => i.PatientId == patientId);

            #region Pending Appointment

            var pendingAppointment = Common.CreateAppointment();
            pendingAppointment.Encounter.Patient = null;
            pendingAppointment.Encounter.PatientId = patient.Id;
            pendingAppointment.DateTime = _currentDate.AddDays(1);
            pendingAppointment.Encounter.StartDateTime = _currentDate.AddDays(1);
            pendingAppointment.Encounter.EncounterStatus = EncounterStatus.Pending;
            pendingAppointment.User = null;
            pendingAppointment.UserId = 1;
            pendingAppointment.Encounter.EncounterReasonForVisitComments.Add(new EncounterReasonForVisitComment { Value = "TestEncounterReasonForVisitComment", UserId = 1 });
            pendingAppointment.Encounter.EncounterPatientEducations.Add(new EncounterPatientEducation { PatientEducation = new PatientEducation { Name = "TestPatientEducationName" }, Comment = "TestEncounterPatientEducationComment" });
            pendingAppointment.Encounter.EncounterClinicalInstructions.Add(new EncounterClinicalInstruction { Value = "Test EncounterClinicalInstruction" });
            AddEncounterTransitionOfCareOrder(pendingAppointment.Encounter);
            pendingAppointment.Encounter.EncounterDiagnosticTestOrders.Add(new EncounterDiagnosticTestOrder { DiagnosticTestOrder = new DiagnosticTestOrder { ClinicalProcedure = clinicalProcedure } });
            AddPatientBloodPressure(pendingAppointment.Encounter, patient.Id, clinicalDataSourceType.Id);
            AddPatientHeightAndWeight(pendingAppointment.Encounter, patient.Id, clinicalDataSourceType.Id);
            AddEncounterLaboratoryTestOrder(pendingAppointment.Encounter, patientLaboratoryTestResult.Id, laboratoryTestOrder.Id);
            practiceRepository.Save(pendingAppointment);
            
            #endregion

            #region Pending Encounter with no Appointment

            var pendingEncounterNoAppt = new Encounter
            {
                PatientId = patient.Id,
                EncounterTypeId = encounterType.Id,
                ServiceLocationId = serviceLocation.Id,
                StartDateTime = _currentDate.AddDays(1),
                EncounterStatusId = (int)EncounterStatus.Pending,
            };
            pendingEncounterNoAppt.EncounterDiagnosticTestOrders.Add(new EncounterDiagnosticTestOrder { DiagnosticTestOrder = new DiagnosticTestOrder { ClinicalProcedureId = clinicalProcedure.Id } });
            AddEncounterLaboratoryTestOrder(pendingEncounterNoAppt, patientLaboratoryTestResult.Id, laboratoryTestOrder.Id);
            practiceRepository.Save(pendingEncounterNoAppt);

            #endregion

            #region Discharged Appointment

            var startDateTime = new DateTime(2010, 1, 1, 10, 15, 0);

            var dischargedAppointment = new UserAppointment
            {
                Encounter = new Encounter
                {
                    PatientId = patient.Id,
                    EncounterTypeId = encounterType.Id,
                    ServiceLocationId = serviceLocation.Id,
                    EncounterStatusId = (int)EncounterStatus.Discharged,
                    StartDateTime = startDateTime
                },
                DateTime = startDateTime,
                UserId = 1,
                Comment = "TestComment",
                AppointmentTypeId = practiceRepository.AppointmentTypes.First().Id
            };
            
            dischargedAppointment.Encounter.EncounterReasonForVisitComments.Add(new EncounterReasonForVisitComment { Value = "TestEncounterReasonForVisitComment", UserId = 1 });
            dischargedAppointment.Encounter.EncounterClinicalInstructions.Add(new EncounterClinicalInstruction { Value = "Test EncounterClinicalInstruction" });
            AddEncounterTreatmentGoalAndInstruction(dischargedAppointment.Encounter, treatmentGoal.Id);
            AddEncounterTransitionOfCareOrder(dischargedAppointment.Encounter);
            AddPatientBloodPressure(dischargedAppointment.Encounter, patient.Id, clinicalDataSourceType.Id);
            AddPatientHeightAndWeight(dischargedAppointment.Encounter, patient.Id, clinicalDataSourceType.Id);
            AddPatientProcedurePerformed(dischargedAppointment.Encounter, patient.Id, clinicalDataSourceType.Id);
            AddEncounterLaboratoryTestOrder(dischargedAppointment.Encounter, patientLaboratoryTestResult.Id, laboratoryTestOrder.Id);
            dischargedAppointment.Encounter.EncounterAdministeredMedications.Add(new EncounterAdministeredMedication { PatientMedicationId = patient.PatientMedications.First().Id });
            practiceRepository.Save(dischargedAppointment);
            
            #endregion

            practiceRepository.Save(new PatientSmokingStatus
            {
                EnteredDateTime = _currentDate,
                SmokingStartDateTime = _currentDate.Subtract(TimeSpan.FromDays(1)),
                SmokingConditionId = practiceRepository.SmokingConditions.First().Id,
                EncounterId = dischargedAppointment.Encounter.Id,
                ClinicalDataSourceTypeId = clinicalDataSourceType.Id,
                PatientId = patient.Id
            });

            return dischargedAppointment.Encounter.Id;
        }
    }
}
