﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using IO.Practiceware.Tests;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;

[assembly: AdoServiceForTestingConcern]

namespace IO.Practiceware.Tests
{
    /// <summary>
    /// An AdoService that logs additional information when running unit tests.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AdoServiceForTestingConcernAttribute : GlobalConcernAttribute
    {
        public AdoServiceForTestingConcernAttribute()
            : base(typeof(AdoServiceForTestingBehavior), 1000)
        {
        }

        public override void Visit(ComponentRegistration registration)
        {
            if (registration.For.Contains(typeof(IAdoService)))
            {
                // insert our concern for logging connection info first 
                registration.GetType().GetProperty("Concerns", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .SetValue(registration, new[] { new AdoServiceForTestingConcernAttribute() }.Concat(registration.Concerns).ToArray(), null);
            }

            base.Visit(registration);
        }

        public override bool IsApplicableFor(Type type)
        {
            return type.Is<IAdoService>();
        }
    }

    public class AdoServiceForTestingBehavior : IInterceptor
    {
        /// <summary>
        /// Intercepts ExecuteCommandsand logs the connection information used..
        /// </summary>
        /// <param name="invocation">The invocation.</param>
        public void Intercept(IInvocation invocation)
        {
            var arguments = invocation.Arguments.FirstOrDefault() as IEnumerable<AdoServiceCommandExecutionArgument>;

            if (arguments != null)
            {
                foreach (var a in arguments.ToArray())
                {
                    Trace.TraceInformation("Running AdoServiceCommandExecutionArgument {0}... against {1}.", a.CommandText.Truncate(100), a.ConnectionString);
                }
            }

            invocation.Proceed();
        }
    }
}
