﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Xml;

using IO.Practiceware.Configuration;
using IO.Practiceware.Monitoring.Service;
using IO.Practiceware.Monitoring.Service.UpdateMonitor;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using Soaf;
using Soaf.Threading;
using Soaf.Configuration;

namespace IO.Practiceware.Tests
{
    [TestClass]
    [Ignore] // todo reinstate once we get an ftp server on the build machine.
    public class UpdateMonitorTests
    {
        private const string FtpAddress = @"ftps://ftp.iopracticeware.com:990";
        private const string UserName = "Test";
        private const string Password = "kz>o#PF2";
        private const string FtpUserName = "Temp";
        private const string TestDirectory = @"C:\IOP\Pinpoint\";
        private const string LocalPath = TestDirectory + @"NewVersion\";
        private const string LocalDeletedPath = LocalPath + @"~Deleted\";
        private const string ServerInstallMsiName = "IO Practiceware Server Setup.msi";
        private const string OldServerInstallMsiName = "IO Practiceware Server Setup Old.msi";
        private const string UpdateServerInstallMsiName = "IO Practiceware Server Setup Update.msi";
        private const string IoServerApplicationName = "IO Practiceware Server";
        private const string IntegrationServiceName = "IO Practiceware Monitoring Service";
        private const string MonitoringServiceName = "IO Practiceware Monitoring Service";

        private FtpClient _ftpClient;
        private UpdateClientConfiguration _updateClientConfig;
        private UpdateMonitor _updateMonitor;


        [ClassInitialize]
        public static void OnClassInitialize(TestContext context)
        {
            new Func<bool>(() => ConfigurationManager.IsInitialized).Wait(TimeSpan.FromSeconds(10));
        }

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void OnTestInitialize()
        {
            TryUninstallCurrentIoServer();
            _updateMonitor = Common.ServiceProvider.GetService<UpdateMonitor>();
            _ftpClient = new FtpClient();

            _updateClientConfig = ConfigurationSection<UpdateMonitorConfiguration>.Named("updateMonitorConfiguration").UpdateClients.First();
            _updateClientConfig.FtpAddressString = FtpAddress;
            _updateClientConfig.LocalPath = LocalPath;
            _updateClientConfig.UserName = UserName;
            _updateClientConfig.Password = Password;

            if (Directory.Exists(LocalPath))
            {
                FileSystem.DeleteDirectory(LocalPath, true);
            }

            if (!Directory.Exists(LocalDeletedPath))
            {
                Directory.CreateDirectory(LocalDeletedPath);
            }

            var ftpItems = _ftpClient.GetFtpDirectoryAndFileManifest(_updateClientConfig.FtpAddress);
            foreach (var item in ftpItems.Where(i => !i.IsDirectory))
            {
                _ftpClient.DeleteFile(new Uri(item.AbsolutePath));
            }

            string installPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty, ".."));
            foreach (string filename in Directory.GetFiles(installPath, "*log"))
            {
                File.Delete(Path.Combine(installPath, filename));
            }
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            TryUninstallCurrentIoServer();
            if (Directory.Exists(LocalPath))
            {
                FileSystem.DeleteDirectory(LocalPath, true);
            }

            if (Directory.GetDirectories(TestDirectory).Length == 0 &&
                Directory.GetFiles(TestDirectory).Length == 0)
            {
                FileSystem.DeleteDirectory(TestDirectory, true);
            }

            var ftpItems = _ftpClient.GetFtpDirectoryAndFileManifest(_updateClientConfig.FtpAddress);
            foreach (var item in ftpItems.Where(i => !i.IsDirectory))
            {
                _ftpClient.DeleteFile(new Uri(item.AbsolutePath));
            }
            ConfigurationManager.Load();
        }

        [TestMethod]
        [Ignore]
        public void TestSyncDeleteLocalFiles()
        {
            const string fileName = "testFile1.txt";
            CreateLocalTestFile(fileName);

            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalDeletedPath).Length == 1);

            DeleteLocalTestFile(fileName);
        }

        [TestMethod]
        [Ignore]
        public void TestSyncDownloadLocalFiles()
        {
            const string fileName = "testFile1.txt";
            CreateRemoteTestFile(fileName);
            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalPath).Length == 1);
            DeleteRemoteTestFile(fileName);
        }

        [TestMethod]
        [Ignore]
        public void TestSyncDownloadLocalFilesFilterItemsSuccessfullyFilters()
        {
            // the monitoringService.ItemsToExcludeInDownload collection is a collections of file names that gets filtered out before download from the server.
            string fileName = UpdateMonitor.ItemsToExcludeInDownload.First();
            CreateRemoteTestFile(fileName);
            const string fileName2 = "testFile1.txt";
            CreateRemoteTestFile(fileName2);

            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalPath).Length == 1);
            Assert.IsFalse(Directory.GetFiles(LocalPath)[0] == fileName);
            DeleteRemoteTestFile(fileName);
            DeleteRemoteTestFile(fileName2);
        }

        [TestMethod]
        [Ignore]
        public void TestSyncDownloadAndOverwriteLocalFiles()
        {
            const string fileName = "testFile1.txt";
            CreateRemoteTestFile(fileName);
            CreateLocalTestFile(fileName);

            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalPath).Length == 1);
            Assert.IsTrue(Directory.GetFiles(LocalDeletedPath).Length == 0);

            DeleteLocalTestFile(fileName);
            DeleteRemoteTestFile(fileName);
        }

        [TestMethod]
        [Ignore]
        public void TestSyncNoChange()
        {
            const string fileName = "testFile1.txt";
            CreateRemoteTestFile(fileName);
            CreateLocalTestFile(fileName);

            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalPath).Length == 1);
            Assert.IsTrue(Directory.GetFiles(LocalDeletedPath).Length == 0);

            _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
            Assert.IsTrue(Directory.GetFiles(LocalPath).Length == 1);
            Assert.IsTrue(Directory.GetFiles(LocalDeletedPath).Length == 0);

            DeleteLocalTestFile(fileName);
            DeleteRemoteTestFile(fileName);
        }

        private static string GetLatestInstallLogFile(string logDirectory)
        {
            FileInfo logInfo = Directory.GetFiles(logDirectory, "serverInstall*.log").Select(i => new FileInfo(i)).OrderByDescending(i => i.LastWriteTimeUtc).FirstOrDefault();
            return logInfo != null ? logInfo.FullName : string.Empty;
        }

        private static bool IsInstallSuccessful(string installLogPath)
        {
            using (StreamReader sr = File.OpenText(installLogPath))
            {
                string msiLogLine = sr.ReadLine();
                if (msiLogLine != null) msiLogLine = msiLogLine.ToLowerInvariant();

                while (!string.IsNullOrEmpty(msiLogLine))
                {
                    if (msiLogLine.Contains("Configuration completed successfully".ToLowerInvariant()) ||
                        msiLogLine.Contains("Installation completed successfully".ToLowerInvariant()))
                    {
                        return true;
                    }
                    msiLogLine = sr.ReadLine();
                    if (msiLogLine != null) msiLogLine = msiLogLine.ToLowerInvariant();
                }
            }
            return false;
        }

        private static string GetInstallerCommandLineProperties()
        {
            string installPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty, ".."));

            var propertiesCommandLineBuilder = new StringBuilder();
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "IOFILESHAREDIR", @"C:\IOP");
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "INSTALLDIR", installPath);
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "WEBSITE", "1");
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "WEBSITE_ID", "1");
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "WEBSITE_PATH", @"C:\inetpub\wwwroot");
            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "FTPUSERNAME", FtpUserName);

            var connectionString = new SqlConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString);
            string dbServer = "server:{0},database:{1}".FormatWith(connectionString.DataSource, connectionString.InitialCatalog);

            propertiesCommandLineBuilder.AppendFormat("{0}=\"{1}\" ", "DBSERVER", dbServer);
            return propertiesCommandLineBuilder.ToString();
        }

        private static FileInfo GetServerInstaller(string installerName)
        {
            var serverMsi = new FileInfo(installerName);
            if (!serverMsi.Exists)
            {
                serverMsi = new FileInfo(Directory.GetCurrentDirectory() + "\\Resources\\" + installerName);
                if (!serverMsi.Exists)
                {
                    throw new Exception("Could not find server msi at {0}.".FormatWith(serverMsi.FullName));
                }
            }
            return serverMsi;
        }

        private static ServiceController GetServiceController(string serviceName)
        {
            ServiceController serviceController = ServiceController.GetServices().FirstOrDefault(i => i.ServiceName == serviceName);
            if (serviceController == null)
            {
                var exception = new Exception(string.Format("Could not find {0}", serviceName));
                Trace.TraceError(exception.ToString());
                throw exception;
            }
            return serviceController;
        }

        private static void StopService(string serviceName)
        {
            ServiceController monitoringServiceController = GetServiceController(serviceName);
            if (monitoringServiceController.Status == ServiceControllerStatus.Running)
            {
                monitoringServiceController.Stop();
                monitoringServiceController.WaitForCompleteOperation();
            }
        }

        [TestMethod]
        [Ignore] // interferes with other unit tests, ignoring for now...will become part of the nightly automated UI tests
        public void TestServerUpdate()
        {
            var configuration = ConfigurationManager.Configuration;
            var originalServerDataPath = configuration.AppSettings.Settings["ServerDataPath"].Value;
            configuration.AppSettings.Settings["ServerDataPath"].Value = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            configuration.Save();

            try
            {
                string installPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty, ".."));
                if (!Directory.Exists(installPath)) { Directory.CreateDirectory(installPath); }

                var serverMsi = GetServerInstaller(OldServerInstallMsiName);
                string installLogDirectory = Path.Combine(installPath, "~Logs");
                if (Directory.Exists(installLogDirectory)) { Directory.Delete(installLogDirectory, true); }
                Directory.CreateDirectory(installLogDirectory);

                string installLogPath = Path.Combine(installLogDirectory, string.Format("serverInstall_{0}.log", DateTime.Now.AddSeconds(1).ToString("yyyy.MM.dd_HH.mm.ss")));
                string commandLineString = string.Format("/i \"{0}\" /qn /norestart /L* \"{1}\" {2}", serverMsi.FullName, installLogPath, GetInstallerCommandLineProperties());

                TestContext.WriteLine("Installing server msi using command line: {0}.".FormatWith(commandLineString));

                var processStartInfo = new ProcessStartInfo("msiexec.exe", commandLineString);
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                var process = Process.Start(processStartInfo);
                process.WaitForExit();
                process.Close();
                process.Dispose();
                StopService(MonitoringServiceName);
                Assert.IsTrue(GetServiceStartMode(MonitoringServiceName) == ServiceStartMode.Automatic);
                Assert.IsTrue(GetServiceStartMode(IntegrationServiceName) == ServiceStartMode.Automatic);

                int installCount = 1;
                Assert.IsTrue(Directory.GetFiles(installLogDirectory, "*.log").Length == (installCount * 2), "Incorrect number of files in {0}.".FormatWith(installPath));
                Assert.IsTrue(File.Exists(installLogPath));
                Assert.IsTrue(IsInstallSuccessful(installLogPath));

                // Upload the server install msi, and run the update service (service will install new msi)
                var serverUpdateMsi = GetServerInstaller(UpdateServerInstallMsiName);
                var ftpUpdateServerPath = _updateClientConfig.FtpAddress.Combine(ServerInstallMsiName);
                _ftpClient.UploadFile(serverUpdateMsi.FullName, ftpUpdateServerPath);


                _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
                Assert.IsTrue(GetServiceStartMode(MonitoringServiceName) == ServiceStartMode.Automatic);
                Assert.IsTrue(GetServiceStartMode(IntegrationServiceName) == ServiceStartMode.Automatic);

                Assert.IsTrue(Directory.GetFiles(installLogDirectory).Length == (++installCount * 2));
                installLogPath = GetLatestInstallLogFile(installLogDirectory);
                Assert.IsTrue(!string.IsNullOrEmpty(installLogPath));
                Assert.IsTrue(IsInstallSuccessful(installLogPath));

                // Run the service again and sync up the install log file.  Check if uploads are okay.
                _updateMonitor.UpdateClient(_updateClientConfig, typeof(UpdateMonitor).Name, false, false);
                var ftpLogPath = _updateClientConfig.FtpAddress.Combine("~Logs");
                List<FtpManifestItem> ftpLogItems = _ftpClient.GetFtpDirectoryAndFileManifest(ftpLogPath).ToList();
                Assert.IsTrue(ftpLogItems.Count() == (installCount * 2));

                // Cleanup, uninstall
                TryUninstallCurrentIoServer();
                _ftpClient.DeleteFile(ftpUpdateServerPath);
                foreach (FtpManifestItem item in ftpLogItems)
                {
                    _ftpClient.DeleteFile(new Uri(item.AbsolutePath));
                }
                if (Directory.Exists(installLogDirectory)) { Directory.Delete(installLogDirectory, true); }

                var updateConfig = ConfigurationManager.Configuration;
                Assert.IsTrue(updateConfig.AreEqual(updateConfig));
            }
            finally
            {
                configuration.AppSettings.Settings["ServerDataPath"].Value = originalServerDataPath;
                configuration.Save();
            }
        }

        private static ServiceStartMode GetServiceStartMode(string serviceName)
        {
            string filter = String.Format("SELECT * FROM Win32_Service WHERE Name = '{0}'", serviceName);

            var query = new ManagementObjectSearcher(filter);
            ManagementObjectCollection services;
            try
            {
                services = query.Get();
                if (services.Count == 0)
                {
                    Trace.TraceInformation(string.Format("GetServiceStartMode::Could not find the service {0} using the query \"{1}\".", serviceName, filter));
                    return ServiceStartMode.Disabled;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation(string.Format("GetServiceStartMode::Could not find the service {0} using the query \"{1}\" due to the exception {2}.", serviceName, filter, ex));
                return ServiceStartMode.Disabled;
            }

            foreach (string startModeProperty in from ManagementObject service in services select service.GetPropertyValue("StartMode").ToString())
            {
                switch (startModeProperty)
                {
                    case "Auto": return ServiceStartMode.Automatic;
                    case "Disabled": return ServiceStartMode.Disabled;
                    case "Manual": return ServiceStartMode.Manual;
                }
            }

            Trace.TraceInformation(string.Format("GetServiceStartMode::Could not find the service {0} that has a property, \"StartMode\", using the query \"{1}\".", serviceName, filter));
            return ServiceStartMode.Disabled;
        }

        private static void CreateLocalTestFile(string fileName)
        {
            string filePath = Path.Combine(LocalPath, fileName);
            using (StreamWriter sw = File.CreateText(filePath))
            {
                sw.WriteLine("test");
            }
        }

        private static void DeleteLocalTestFile(string fileName)
        {
            string filePath = Path.GetFullPath(Path.Combine(LocalPath, fileName));
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        private void CreateRemoteTestFile(string fileName)
        {
            string localFilePath = Path.GetFullPath(Path.Combine(LocalPath, fileName));
            var remoteFilePath = _updateClientConfig.FtpAddress.Combine(fileName);
            CreateLocalTestFile(fileName);
            _ftpClient.UploadFile(localFilePath, remoteFilePath);
            DeleteLocalTestFile(fileName);
        }

        private void DeleteRemoteTestFile(string fileName)
        {
            var filePath = _updateClientConfig.FtpAddress.Combine(fileName);
            _ftpClient.DeleteFile(filePath);
        }

        private static string GetUnInstallServerMsiCommandLine()
        {
            string msiPath = GetUnInstallServerMsiCommandLine(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            if (string.IsNullOrEmpty(msiPath))
            {
                msiPath = GetUnInstallServerMsiCommandLine(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
            }
            return msiPath;
        }

        private static string GetUnInstallServerMsiCommandLine(string registryPath)
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(registryPath))
            {
                if (key != null)
                {
                    foreach (String keyName in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(keyName))
                        {
                            if (subkey != null)
                            {
                                var displayName = (string)subkey.GetValue("DisplayName");
                                if (IoServerApplicationName.Equals(displayName, StringComparison.OrdinalIgnoreCase))
                                {
                                    return (string)subkey.GetValue("ModifyPath");
                                }
                            }
                        }
                    }
                }
            }
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(registryPath))
            {
                if (key != null)
                {
                    foreach (String keyName in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(keyName))
                        {
                            if (subkey != null)
                            {
                                var displayName = (string)subkey.GetValue("DisplayName");
                                if (IoServerApplicationName.Equals(displayName, StringComparison.OrdinalIgnoreCase))
                                {
                                    return (string)subkey.GetValue("ModifyPath");
                                }
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }

        internal static void TryUninstallCurrentIoServer()
        {
            var unInstallServerMsiCommandLine = GetUnInstallServerMsiCommandLine();

            if (!unInstallServerMsiCommandLine.IsNullOrEmpty())
            {
                var processStartInfo = new ProcessStartInfo("msiexec.exe", unInstallServerMsiCommandLine.Remove(0, "msiexec.exe ".Length) + " /qn /norestart");
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processStartInfo.UseShellExecute = false;
                var process = Process.Start(processStartInfo);
                process.WaitForExit();
            }
        }
    }

    public static class ConfigurationExtensions
    {
        public static bool AreEqual(this System.Configuration.Configuration config, System.Configuration.Configuration otherConfig)
        {
            if (config == null && otherConfig == null) return true;
            if (config == null || otherConfig == null) return false;

            var xDoc = new XmlDocument();
            xDoc.Load(config.FilePath);

            var otherXDoc = new XmlDocument();
            otherXDoc.Load(otherConfig.FilePath);

            XmlNode connectionStrings = xDoc.SelectSingleNode("//configuration/connectionStrings");
            XmlNode otherConnectionStrings = otherXDoc.SelectSingleNode("//configuration/connectionStrings");
            if (!AreOuterXmlsEqual(connectionStrings, otherConnectionStrings)) return false;

            XmlNode migrationConnectionStrings = xDoc.SelectSingleNode("//configuration/dbMigrations/dbMigration/migrationConnectionStrings");
            XmlNode otherMigrationConnectionStrings = otherXDoc.SelectSingleNode("//configuration/dbMigrations/dbMigration/migrationConnectionStrings");
            if (!AreOuterXmlsEqual(migrationConnectionStrings, otherMigrationConnectionStrings)) return false;

            XmlNode dataServerNode = xDoc.SelectSingleNode("//configuration/appSettings");
            XmlNode otherDataServerNode = otherXDoc.SelectSingleNode("//configuration/appSettings");
            if (!AreOuterXmlsEqual(dataServerNode, otherDataServerNode)) return false;

            XmlNodeList updateClientNodes = xDoc.SelectNodes("//configuration/monitoringServiceConfiguration/updateClients/updateClient");
            XmlNodeList otherUpdateClientNodes = otherXDoc.SelectNodes("//configuration/monitoringServiceConfiguration/updateClients/updateClient");
            if (updateClientNodes != null && otherUpdateClientNodes != null && updateClientNodes.Count == otherUpdateClientNodes.Count)
            {
                for (int i = 0; i < updateClientNodes.Count; i++)
                {
                    if (!AreOuterXmlsEqual(updateClientNodes[i], otherUpdateClientNodes[i])) return false;
                }
            }
            else if ((updateClientNodes == null && otherUpdateClientNodes != null) || (updateClientNodes != null && otherUpdateClientNodes == null))
            {
                return false;
            }

            return true;
        }

        private static bool AreOuterXmlsEqual(XmlNode node, XmlNode otherNode)
        {
            if (node == null && otherNode == null) return true;
            if (node == null || otherNode == null) return false;

            return node.OuterXml.Equals(otherNode.OuterXml);
        }
    }
}
