﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Transactions;
using Aspose.Words;
using IO.Practiceware.Configuration;
using IO.Practiceware.Integration.Email;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Document = Aspose.Words.Document;

namespace IO.Practiceware.Tests
{
    [TestClass]
    [TestRunMode(ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresTestClassLock)] // Concurrency over folder contents
    public class EmailTests : TestBase
    {
        private MessageProcessor _messageProcessor;

        protected override void OnAfterTestInitialize()
        {
            var messenger = ServiceProvider.GetService<IMessenger>();
            _messageProcessor = new MessageProcessor(PracticeRepository, messenger);
            var messageAuditor = new MessageAuditor(PracticeRepository, _messageProcessor);

            messageAuditor.Transaction = Transaction.Current;
            _messageProcessor.Transaction = Transaction.Current;

            if (
                !Directory.Exists(
                   MessagingConfiguration.Current.TextMessageConfiguration.SpecifiedPickupDirectory.
                        PickupDirectoryLocation))
            {
                Directory.CreateDirectory(
                    MessagingConfiguration.Current.TextMessageConfiguration.SpecifiedPickupDirectory.
                        PickupDirectoryLocation);
            }
            if (
                !Directory.Exists(
                    MessagingConfiguration.Current.EmailConfiguration.SpecifiedPickupDirectory.PickupDirectoryLocation))
            {
                Directory.CreateDirectory(
                    MessagingConfiguration.Current.EmailConfiguration.SpecifiedPickupDirectory.PickupDirectoryLocation);
            }
        }

        protected override void OnBeforeTestCleanup()
        {
            if (!_messageProcessor.WaitForIdle(TimeSpan.FromSeconds(10))) throw new Exception("MessageProcessor never finished processing.");

            base.OnAfterTestCleanup();
        }

        [TestMethod]
        public void TestEmailUsingService()
        {
            DateTime start = DateTime.UtcNow;
            //Creating a Template in local Path
            string documentPath = ConfigurationManager.ServerDataPath + "\\Templates";
            if (!FileManager.Instance.DirectoryExists(documentPath))
            {
                FileManager.Instance.CreateDirectory(documentPath);
            }
            documentPath += "\\EmailAppointmentReminder.doc";
            if (!FileManager.Instance.FileExists(documentPath))
            {
                var document = new Document();
                FileManager.Instance.CommitWrite(documentPath, localPath => document.Save(localPath, SaveFormat.Doc));
            }
            FileManager.Instance.CommitContents(documentPath, EmailResources.EmailServiceTemplate);
            string content = new Document(new MemoryStream(EmailResources.EmailServiceResult)).GetText().Trim();
            //Creating a Patient and save it
            Patient patient = Common.CreatePatient();
            patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Test@aol.com" });
            Common.PracticeRepository.Save(patient);

            // Create a message and save it
            var messageType = new ExternalSystemExternalSystemMessageType
                                  {
                                      TemplateFile = "EmailAppointmentReminder.doc",
                                      ExternalSystemId = (int)ExternalSystemId.Email,
                                      ExternalSystemMessageTypeId =
                                          (int)ExternalSystemMessageTypeId.Confirmation_Outbound
                                  };

            Common.PracticeRepository.Save(messageType);

            var message = new ExternalSystemMessage
                              {
                                  CreatedBy = "TestEmail",
                                  CreatedDateTime = DateTime.UtcNow,
                                  UpdatedDateTime = DateTime.UtcNow,
                                  Description = string.Empty,
                                  ExternalSystemMessageProcessingStateId =
                                      (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                                  Value = string.Empty,
                                  ExternalSystemExternalSystemMessageTypeId = messageType.Id
                              };
            message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
                                                                            {
                                                                                PracticeRepositoryEntityId =
                                                                                    (int)
                                                                                    PracticeRepositoryEntityId.Patient,
                                                                                PracticeRepositoryEntityKey =
                                                                                    patient.Id.ToString()
                                                                            });
            Common.PracticeRepository.Save(message);

            // Start the integration monitor so it delivers the message
            _messageProcessor.ProcessUnprocessedMessages();
            message = WaitForMessageProcessed(start);

            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());
            Assert.IsTrue(CheckEmailWithRightContentGenerated(content, start));
        }

        [TestMethod]
        public void TestTextMessageUsingService()
        {
            DateTime start = DateTime.UtcNow;
            //Creating a Template in local Path
            string documentPath = ConfigurationManager.ServerDataPath + "\\Templates";
            if (!FileManager.Instance.DirectoryExists(documentPath))
            {
                FileManager.Instance.CreateDirectory(documentPath);
            }
            documentPath += "\\TextMessageAppointmentReminder.doc";
            if (!FileManager.Instance.FileExists(documentPath))
            {
                var document = new Document();
                FileManager.Instance.CommitWrite(documentPath, localPath => document.Save(localPath, SaveFormat.Doc));
            }
            FileManager.Instance.CommitContents(documentPath, EmailResources.TextMessageServiceTemplate);
            string content = new Document(new MemoryStream(EmailResources.TextMessageServiceResult)).GetText().Trim();
            //Creating a Patient and save it
            var patient = new Patient
                              {
                                  LastName = "TEST",
                                  FirstName = "1",
                                  DateOfBirth = new DateTime(2010, 6, 5)
                              };
            var phoneNumber = new PatientPhoneNumber
            {
                AreaCode = "212",
                ExchangeAndSuffix = "3334444",
                PatientPhoneNumberTypeId = PatientPhoneNumberType.Cell
            };
            patient.PatientPhoneNumbers.Add(phoneNumber);
            Common.PracticeRepository.Save(patient);

            // Create a message and save it        
            var messageType = new ExternalSystemExternalSystemMessageType
                                  {
                                      TemplateFile = "TextMessageAppointmentReminder.doc",
                                      ExternalSystemId = (int)ExternalSystemId.TextMessage,
                                      ExternalSystemMessageTypeId =
                                          (int)ExternalSystemMessageTypeId.Confirmation_Outbound
                                  };

            Common.PracticeRepository.Save(messageType);

            var message = new ExternalSystemMessage
                              {
                                  CreatedBy = "Test Text Message",
                                  CreatedDateTime = DateTime.UtcNow,
                                  UpdatedDateTime = DateTime.UtcNow,
                                  Description = string.Empty,
                                  ExternalSystemMessageProcessingStateId =
                                      (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                                  Value = string.Empty,
                                  ExternalSystemExternalSystemMessageTypeId = messageType.Id
                              };
            message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
                                                                            {
                                                                                PracticeRepositoryEntityId =
                                                                                    (int)
                                                                                    PracticeRepositoryEntityId.Patient,
                                                                                PracticeRepositoryEntityKey =
                                                                                    patient.Id.ToString()
                                                                            });
            Common.PracticeRepository.Save(message);

            // Start the integration monitor so it delivers the message
            _messageProcessor.ProcessUnprocessedMessages();
            message = WaitForMessageProcessed(start);

            Assert.IsNotNull(message);
            Assert.IsFalse(message.Value.IsNullOrEmpty());
            Assert.IsTrue(CheckEmailWithRightContentGenerated(content, start));
        }

        ///<summary>
        ///  A test for SendWebEmail
        ///</summary>
        [TestMethod]
        public void TestEmail()
        {
            var target = new EmailManager();
            Patient patient = Common.CreatePatient();
            patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Test@aol.com" });
            patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = "Test@gmail.com" });
            string content = new Document(new MemoryStream(EmailResources.EmailTemplate)).GetText().Trim();
            var start = DateTime.UtcNow;
            target.SendWebEmail(patient, content);

            Assert.IsTrue(CheckEmailWithRightContentGenerated(content, start));
        }

        ///<summary>
        ///  A test for SendTextMessage
        ///</summary>
        [TestMethod]
        public void TestTextMessage()
        {
            var target = new EmailManager();
            Patient patient = Common.CreatePatient();
            var patientNumber = new PatientPhoneNumber();
            patient.PatientPhoneNumbers.Add(patientNumber);
            patientNumber.SetValuesFromString("212-123-4567");
            patientNumber.PatientPhoneNumberTypeId = PatientPhoneNumberType.Cell;

            string content = new Document(new MemoryStream(EmailResources.TextMessageTemplate)).GetText().Trim();
            var start = DateTime.UtcNow;
            target.SendTextMessage(patient, content);
            Assert.IsTrue(CheckEmailWithRightContentGenerated(content, start));
        }

        private static bool CheckEmailWithRightContentGenerated(string content, DateTime messageDate)
        {
            SmtpConfiguration smtpMailSection = MessagingConfiguration.Current.TextMessageConfiguration;
            string pickupDirectoryLocation = smtpMailSection.SpecifiedPickupDirectory.PickupDirectoryLocation;
            var fileList = FileManager.Instance.ListFiles(Path.Combine(pickupDirectoryLocation, "*.eml"));
            var filesTimestamps = FileManager.Instance.GetFileInfos(fileList);
            var actualFileList = filesTimestamps
                    .Where(t => DateTime.Compare(t.CreationTimeUtc, messageDate) >= 0)
                    .Select(p => FileManager.Instance.ReadContents(p.FullName))
                    .ToList();

            bool foundMatch = false;
            foreach (var fileObj in actualFileList)
            {
                var outputFileContent = new Document(new MemoryStream(fileObj)).GetText();
                if (outputFileContent.Trim() == content)
                {
                    foundMatch = true;
                    break;
                }
            }
            return foundMatch;
        }

        private static ExternalSystemMessage WaitForMessageProcessed(DateTime after)
        {
            int count = 0;
            while (count < 40)
            {
                ExternalSystemMessage message =
                    Common.PracticeRepository.ExternalSystemMessages.FirstOrDefault(
                        m =>
                        m.CreatedDateTime >= after &&
                        m.ExternalSystemMessageProcessingState.Id ==
                        (int)ExternalSystemMessageProcessingStateId.Processed);
                if (message != null)
                {
                    Trace.WriteLine("Waited {0} seconds for message.".FormatWith(count));
                    return message;
                }
                Thread.Sleep(2000);
                count += 1;
            }
            Trace.WriteLine(
                "Failed to find processed message after {0}. ExternalSystemMessage table has {1} rows. Last row was created at {2}."
                    .FormatWith(after, Common.PracticeRepository.ExternalSystemMessages.Count(),
                                Common.PracticeRepository.ExternalSystemMessages.OrderByDescending(
                                    i => i.CreatedDateTime).Select(i => i.CreatedDateTime).FirstOrDefault()));
            return null;
        }
    }
}