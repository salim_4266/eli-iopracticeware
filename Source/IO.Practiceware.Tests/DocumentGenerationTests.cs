﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Aspose.Words.Fields;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Presentation;
using Document = Aspose.Words.Document;
using Encounter = IO.Practiceware.Services.Documents.Encounter;
using PatientSurgery = IO.Practiceware.Services.Documents.PatientSurgery;
using QuestionAnswer = IO.Practiceware.Services.Documents.QuestionAnswer;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class DocumentGenerationTests : TestBase
    {
        protected override TestDatabasePoolName DatabasePoolName
        {
            get { return TestDatabasePoolName.DocumentGenerationTests; }
        }

        protected override void InitializeTestDatabase()
        {
            base.InitializeTestDatabase();
            DbConnectionFactory.PracticeRepository.RunScript(X12Resources.SetupICD9TableForMappings, false);
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PaperClaimsResources.X12MetadataPaperClaims, false);
            Common.RecreateViewAsTable<PatientDiagnosis>();

            base.OnAfterTestInitialize();
        }

        /// <summary>
        ///     Tests mail merging with an object data source.
        /// </summary>
        [TestMethod]
        public void TestMailMergeObjectDataSource()
        {
            var dataSource = new
                                 {
                                     Name = new { FirstName = "John" },
                                     Addresses = new[]
                                                     {
                                                         new
                                                             {
                                                                 City = "New York"
                                                             },
                                                         new {City = "Boston"}
                                                     }
                                 }
                ;


            byte[] result = Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(DocumentGenerationResources.ObjectDataSourceTemplate, dataSource);

            AssertDocumentsEqual(result, DocumentGenerationResources.ObjectDataSourceTemplateResult);
        }

        [TestMethod]
        public void TestFormatting()
        {
            var document = new
                               {
                                   Patient = new
                                                 {
                                                     FirstName = "Steve joBs",
                                                     Email = "Test@aol.com"
                                                 },
                               };



            byte[] result = Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(DocumentGenerationResources.FormattingTemplate, document);
            string str = new Document(new MemoryStream(result)).GetText();
            string strResult = new Document(new MemoryStream(DocumentGenerationResources.FormattingResultTemplate)).GetText();
            Assert.IsTrue(str == strResult);
        }

        // The purpose of this test is to fill a form with data. No validations of services included is done on this test
        [TestMethod]
        public void PerformUb04PaperClaimMailMergeFromBillingServiceTransactions()
        {
            var queuedBillingTransactions = PaperClaimsTests.GetQueuedBillingServiceTransactions();

            var messageProducer = Common.ServiceProvider.GetService<PaperClaimsMessageProducer>();
            messageProducer.IsTesting = true;
            var bytes = PaperClaimsTests.GetDocumentBytes("UB-04.cshtml");
            var template = Encoding.UTF8.GetString(bytes);
            //Passing the first billing service transaction as resource has only one file data to compare.
            var mergedFile = (HtmlDocument)messageProducer.ProduceMessage(new[] { queuedBillingTransactions.OrderBy(x => x.BillingService.InvoiceId).Select(x => x.Id).FirstOrDefault() }, template, ContentType.RazorTemplate);
            var output = mergedFile.Content.ToList().First();
            var expectedOutput = PaperClaimsResources.Ub04ExpectedOutput.RemoveAllWhiteSpaces().Trim().ReplaceFirst("?", "");
            var actualOutput = output.RemoveAllWhiteSpaces().Trim().ReplaceFirst("?", "");
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        public void PerformCms1500PaperClaimMailMergeFromBillingServiceTransactions()
        {
            var queuedBillingTransactions = PaperClaimsTests.GetQueuedBillingServiceTransactions();

            var messageProducer = Common.ServiceProvider.GetService<PaperClaimsMessageProducer>();
            messageProducer.IsTesting = true;
            var bytes = PaperClaimsTests.GetDocumentBytes("CMS-1500.cshtml");
            var template = Encoding.UTF8.GetString(bytes);
            //Passing the first billing service transaction as resource has only one file data to compare.
            var mergedFile = (HtmlDocument)messageProducer.ProduceMessage(new[] { queuedBillingTransactions.OrderBy(x => x.BillingService.InvoiceId).Select(x => x.Id).FirstOrDefault() }, template, ContentType.RazorTemplate);
            var output = mergedFile.Content.ToList().First();
            var expectedOutput = PaperClaimsResources.Cms1500ExpectedOutput.RemoveAllWhiteSpaces().Trim().ReplaceFirst("?", "");
            var actualOutput = output.RemoveAllWhiteSpaces().Trim().ReplaceFirst("?", "");
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        public void PerformMailMergeTestforNewConsultComp()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.NewConsultCompdata));
            string signaturefile = FileManager.Instance.GetTempPathName();
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.NewConsultCompTemplate);
            FileManager.Instance.CommitContents(signaturefile, DocumentGenerationResources.KleinSig);
            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = signaturefile;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.NewConsultCompOutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(signaturefile);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestforOFlynnPrimaryLtr()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.OFlynnPrimaryLtrdata));
            string signaturefile = FileManager.Instance.GetTempPathName();
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.OFlynnPrimaryLtrtemplate);
            FileManager.Instance.CommitContents(signaturefile, DocumentGenerationResources.KleinSig);
            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = signaturefile;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.OFlynnPrimaryLtrOutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(signaturefile);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestforOflynnDilatedExam()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.OflynnDilatedExamdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.OflynnDilatedExamtemplate);
            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.OFlynnDilatedExamOutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestCheckinform()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.CheckInFormdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.CheckInFormtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Checkinformoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestClrxdata()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.CLRxdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.CLRXtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Clrxdataoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestDrugrx()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.DrugRxdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.DrugRxtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Drugrxoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestDrugrx2()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.DrugRx2data));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.DrugRx2template);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Drugrx2output);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestEyewarerx()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.EyeWearRxdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.EyewearRxtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Eyewarerxoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeTestEyewarerx2()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.EyeWearRx2data));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.EyewearRx_2template);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Eyewarerx2output);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeHcfa()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.HCFA1500Adata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.Hcfa1500Atemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.HCFA1500output);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeRecalltEnv()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.RecallTEnvdata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.RecallTEnvtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.RecallTEnvoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeRecalltlbl()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.RecallTLbldata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.RecallTLbltemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Recalltlbloutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeRecalltPostcard()
        {
            string templatetempfile = FileManager.Instance.GetTempPathName();
            string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.RecallTPostcarddata));
            string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
            FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.RecallTPostCardtemplate);

            string templateFilePath = templatetempfile;
            string mergeDataFilePath = mergedatatempfile;
            string mergeToFilePath = outputfile;
            string printSignature = string.Empty;
            Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

            AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Recalltpostcardoutput);
            FileManager.Instance.Delete(templateFilePath);
            FileManager.Instance.Delete(outputfile);
        }

        [TestMethod]
        public void PerformMailMergeRecallSpanish()
        {
            var oldCi = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-ES");

                string templatetempfile = FileManager.Instance.GetTempPathName();
                string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.RecallT_spanishdata));
                string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
                FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.RecallT_spanishtemplate);

                string templateFilePath = templatetempfile;
                string mergeDataFilePath = mergedatatempfile;
                string mergeToFilePath = outputfile;
                string printSignature = string.Empty;
                Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

                AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.RecalltSpanishoutput);
                FileManager.Instance.Delete(templateFilePath);
                FileManager.Instance.Delete(outputfile);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        [TestMethod]
        public void PerformMailMergeRiverroad()
        {
            int i = 0;
            EventHandler<InteractionEventArgs<PromptArguments, PromptResult>> promptHandler = (sender, e) =>
                                                                                                  {
                                                                                                      e.Handled = true;
                                                                                                      e.Result = new PromptResult(true, "xyz" + i++);
                                                                                                  };


            lock (InteractionManager.Current)
            {
                try
                {
                    InteractionManager.Current.Prompting += promptHandler;

                    string templatetempfile = FileManager.Instance.GetTempPathName();
                    string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.RiverRoadSxBookingShtdata));
                    string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
                    FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.RiverRoadSxBookingShttemplate);

                    string templateFilePath = templatetempfile;
                    string mergeDataFilePath = mergedatatempfile;
                    string mergeToFilePath = outputfile;
                    string printSignature = string.Empty;
                    Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

                    AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.Riverroadoutput);
                    FileManager.Instance.Delete(templateFilePath);
                    FileManager.Instance.Delete(outputfile);
                }
                finally
                {
                    InteractionManager.Current.Prompting -= promptHandler;
                }
            }
        }

        [TestMethod]
        public void PerformMailMergeAbn()
        {
            EventHandler<InteractionEventArgs<PromptArguments, PromptResult>> promptHandler = (sender, e) =>
                                                                                              {
                                                                                                  e.Handled = true;
                                                                                                  e.Result = new PromptResult(true, "O");
                                                                                              };

            lock (InteractionManager.Current)
            {
                InteractionManager.Current.Prompting += promptHandler;
                string templatetempfile = FileManager.Instance.GetTempPathName();
                string mergedatatempfile = (Encoding.UTF8.GetString(DocumentGenerationResources.ABNdata));
                string outputfile = FileManager.Instance.GetTempPathName().Replace(".tmp", ".doc");
                FileManager.Instance.CommitContents(templatetempfile, DocumentGenerationResources.ABNTemplate);

                string templateFilePath = templatetempfile;
                string mergeDataFilePath = mergedatatempfile;
                string mergeToFilePath = outputfile;
                string printSignature = string.Empty;
                Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(templateFilePath, mergeDataFilePath, mergeToFilePath, printSignature);

                AssertDocumentsEqual(FileManager.Instance.ReadContents(outputfile), DocumentGenerationResources.ABNOutput);
                FileManager.Instance.Delete(templateFilePath);
                FileManager.Instance.Delete(outputfile);
            }
        }

        public static void AssertDocumentsEqual(byte[] document1Bytes, byte[] document2Bytes)
        {
            using (var ms1 = new MemoryStream(document1Bytes))
            using (var ms2 = new MemoryStream(document2Bytes))
            {
                var document1 = new Document(ms1);
                document1.UpdatePageLayout();
                document1.UpdateFields();
                var document2 = new Document(ms2);
                document2.UpdatePageLayout();
                document2.UpdateFields();
                new[] { document1, document2 }.ToList().ForEach(d => d.ConvertFieldsToStaticText(FieldType.FieldPage)); // remove when this bug is fixed: http://www.aspose.com/community/forums/401608/wrong-page-number-when-convert-page-number-to-static-text/showthread.aspx#401608
                Assert.AreEqual(document2.GetText()
                    .Replace("25 June, 2012", DateTime.Now.ToString("d MMMM, yyyy"))
                    .Replace("June 25, 2012", DateTime.Now.ToString("MMMM d, yyyy"))
                    .Replace("1 May, 2011", new DateTime(2011, 1, 5).ToString("d MMMM, yyyy"))
                    .Replace("3 May, 2011", new DateTime(2011, 3, 5).ToString("d MMMM, yyyy"))
                    .Replace("11 December, 2010", new DateTime(2010, 11, 12).ToString("d MMMM, yyyy"))
                    .Replace("6 December, 2010", new DateTime(2010, 6, 12).ToString("d MMMM, yyyy"))
                    , document1.GetText());
                /* replace any dates in the expected output */
            }
        }

        /// <summary>
        ///     Tests mail merging with an object data source.
        /// </summary>
        [TestMethod]
        public void TestAscExamDocumentsUsingMailMergeWithDataSource()
        {
            Encounter encounterObject = GetEncounterViewModelData();

            byte[] result = Common.ServiceProvider.GetService<IDocumentGenerationService>().PerformMailMerge(DocumentGenerationResources.ASCTemplate, encounterObject);

            AssertDocumentsEqual(result, DocumentGenerationResources.ASCOutput);
        }

        private static Encounter GetEncounterViewModelData()
        {
            var encounter = new Encounter();
            encounter.PatientLastName = "John";
            encounter.PatientFirstName = "Smith";
            encounter.PatientDateOfBirth = new DateTime(1983, 06, 14, 11, 15, 0);
            encounter.AppointmentDateTime = new DateTime(2010, 1, 1, 10, 15, 0);
            encounter.DoctorName = "Mahima Chowdari, M.D";
            encounter.PatientSurgery = GetPatientSurgeryData();
            return encounter;
        }

        private static PatientSurgery GetPatientSurgeryData()
        {
            var patientSurgery = new PatientSurgery();
            patientSurgery.SurgeryType = "CATARACT SX";
            patientSurgery.Answers = GetAnswers();
            return patientSurgery;
        }

        private static QuestionAnswer[] GetAnswers()
        {
            var answers = new[]
                {
                    new QuestionAnswer
                        {
                            Question = "What is your name?", Value = "John, Smith", Note = "hi",
                            Choices = new[]
                                {
                                    new QuestionChoice
                                        {
                                            Value = "John, Smith",
                                            Note = "hi"
                                        }
                                }
                        },
                    new QuestionAnswer
                        {
                            Question = "How are you?", Value = "", Note = "",
                            Choices = new[]
                                {
                                    new QuestionChoice
                                        {
                                            Value = "Fine",
                                            Note = "fine"
                                        },
                                    new QuestionChoice
                                        {
                                            Value = "Thank you",
                                            Note = "thanks"
                                        }
                                }
                        },
                    new QuestionAnswer
                        {
                            Question = "Eye Context", Value = "", Note = "",
                            Choices = new[]
                                {
                                    new QuestionChoice
                                        {
                                            Value = "OD",
                                            Note = ""
                                        }
                                }
                        },
                    new QuestionAnswer
                        {
                            Question = "Note Box Top", Value = "", Note = "",
                            Choices = new[]
                                {
                                    new QuestionChoice
                                        {
                                            Value = "Top comments",
                                            Note = ""
                                        }
                                }
                        },
                    new QuestionAnswer
                        {
                            Question = "Note Box Bottom", Value = "", Note = "",
                            Choices = new[]
                                {
                                    new QuestionChoice
                                        {
                                            Value = "Bottom comments",
                                            Note = ""
                                        }
                                }
                        }
                };
            return answers;
        }
    }
}