﻿using IO.Practiceware.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class DatabaseValidityTests : TestBase
    {
        [TestMethod]
        public void TestForMissingForeignKeys()
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                Assert.AreEqual(connection.Execute<int>(@"SELECT
COUNT(*) AS [Count]
FROM sys.tables t
JOIN sys.columns c ON c.object_id = t.object_id
JOIN sys.tables t3 ON t3.name = SUBSTRING(c.Name, 1, LEN(c.Name) - 2) + 's'
	AND OBJECT_SCHEMA_NAME(t3.object_id) = 'model' 
WHERE EXISTS (SELECT * FROM sys.tables t2 WHERE c.name = SUBSTRING(t2.Name, 1, LEN(t2.Name) - 1) + 'Id')
	AND NOT EXISTS (SELECT * FROM sys.foreign_key_columns fkc WHERE fkc.parent_object_id = t.object_id AND fkc.parent_column_id = c.column_id)
	AND OBJECT_SCHEMA_NAME(t.object_id) = 'model'
	AND t.name NOT LIKE '%conflict%'"), 0);

            }
        }
    }
}
