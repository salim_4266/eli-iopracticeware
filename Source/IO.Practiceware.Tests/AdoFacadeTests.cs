﻿
// UNCOMMENT TO ENABLE DEBUGGING FOR VB6
//#define DEBUG_VBSCRIPT

using System;
using System.Diagnostics;
using System.IO;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Threading;


namespace IO.Practiceware.Tests
{


    [TestClass]
    public class AdoFacadeTests : TestBase
    {

#if DEBUG_VBSCRIPT

        private static string ArgumentsFormatString
        {
            get
            {
                return "//X //D //Nologo \"{0}\" \"" + Configuration.ConfigurationManager.PracticeRepositoryConnectionString + "\"";
            }
        }
#else

        private static string ArgumentsFormatString
        {
            get
            {
                return "//Nologo \"{0}\" \"" + ConfigurationManager.PracticeRepositoryConnectionString + "\"";
            }
        }
#endif

        private static readonly string FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cscript.exe");

        [ClassInitialize]
        public static void OnClassInitialize(TestContext context)
        {
            using (new WriteLock(GetGlobalLock()))
            {
                typeof (Interop.ComWrapper).EnsureNotDefault("ComWrapper not present.");

                File.WriteAllText("Delete_Registrations.bat", Resources.Delete_Registrations);
                var p = Process.Start("Delete_Registrations.bat");
                if (p != null) p.WaitForExit();

                var directoryWindows = Environment.GetFolderPath(Environment.SpecialFolder.Windows);
                var directory64Bit = Environment.GetFolderPath(Environment.SpecialFolder.SystemX86);
                var directory32Bit = Environment.GetFolderPath(Environment.SpecialFolder.System);
                var filePathComWrapperInterop = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"IO.Practiceware.Interop.dll");

                File.Copy(Environment.Is64BitOperatingSystem ? Path.Combine(directory64Bit, @"cscript.exe") : Path.Combine(directory32Bit, @"cscript.exe"), Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cscript.exe"), true);

                var vbsProcess = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                        FileName = Path.Combine(directoryWindows, @"Microsoft.NET\Framework\v4.0.30319\RegAsm.exe"),
                        Arguments = "\"" + filePathComWrapperInterop + "\"" + @" /codebase /tlb",
                        UseShellExecute = false
                    }
                };
                vbsProcess.Start();
                vbsProcess.WaitForExit();
            }
        }

        [TestMethod]
        public void TestVbScripts()
        {
            var name = "ADODBVbTestsScript.vbs";
            File.WriteAllText(name, ADODBVbScripts.ADODBVbTestsScript);

            string filePath = Path.GetFullPath(name);

            string query = ADODBVbScripts.GeneralMetadata;

            var p = new Process
                        {
                            StartInfo = new ProcessStartInfo
                                            {
                                                WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                                                FileName = FileName,
                                                Arguments = String.Format(ArgumentsFormatString, filePath) + " \"" + query + "\"",
                                                UseShellExecute = false,
                                                RedirectStandardOutput = true,
                                                RedirectStandardError = true
                                            }
                        };
            using (StringWriter outputWriter = new StringWriter(), errorWriter = new StringWriter())
            {
                p.OutputDataReceived += (sender, args) => outputWriter.WriteLine(args.Data);
                p.ErrorDataReceived += (sender, args) => errorWriter.WriteLine(args.Data);

                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                p.WaitForExit();

                var output = outputWriter.GetStringBuilder().ToString().Trim();

                Assert.AreEqual(0, p.ExitCode, "Invalid exit code. Output: " + output);
                Assert.IsTrue(output.EndsWith("Done"), "Output is not complete: " + output);
            }
        }
    }
}