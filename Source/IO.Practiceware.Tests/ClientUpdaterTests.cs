﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using IO.Practiceware.ClientUpdater;
using IO.Practiceware.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Collections;
using Soaf.Configuration;
using ConfigurationManager = IO.Practiceware.Configuration.ConfigurationManager;
using FileSystem = IO.Practiceware.Storage.FileSystem;
using KeyValueConfigurationCollection = Soaf.Configuration.KeyValueConfigurationCollection;

namespace IO.Practiceware.Tests
{
    [TestClass]
    [TestRunMode(
        UseDedicatedTestDatabase = false,
        ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresTestClassLock,
        UseTransactionScope = false)]
    public class ClientUpdaterTests : TestBase
    {
        private string _localApplicationTestFolder;
        private string _serverClientUpdateFolder;

        protected override void OnAfterTestInitialize()
        {
            base.OnAfterTestInitialize();

            _localApplicationTestFolder = Path.GetFullPath(Environment.ExpandEnvironmentVariables(@"%TEMP%\IOP\TestAppFolder"));
            FileSystem.EnsureDirectory(_localApplicationTestFolder);

            // Drop configs into test app folder we prepared
            FileSystem.ListFiles(Path.Combine(ConfigurationManager.ApplicationPath, "*.config"))
// ReSharper disable once AssignNullToNotNullAttribute
                .ForEach(f => FileSystem.TryCopyFile(f, Path.Combine(_localApplicationTestFolder, Path.GetFileName(f))));

            // Set server data path
            ModifyTestAppClientConfig(c =>
            {
                var appSettings = c.GetSection<KeyValueConfigurationCollection>("customAppSettings");
                appSettings.Items.Add(new KeyValueConfiguration("ServerDataPath", ConfigurationManager.ServerDataPath));
            });

            _serverClientUpdateFolder = Path.Combine(ConfigurationManager.ServerDataPath, @"SoftwareUpdates\Client");
            FileSystem.EnsureDirectory(_serverClientUpdateFolder);
        }

        protected override void OnBeforeTestCleanup()
        {
            base.OnBeforeTestCleanup();

            // Cleanup test folders
            FileSystem.DeleteDirectory(_localApplicationTestFolder, true);
            FileSystem.DeleteDirectory(_serverClientUpdateFolder, true);
        }

        [TestMethod]
        public void TestUpdaterCanUpdate()
        {
            // Init and validate configuration
            IOConfiguration.SetNewApplicationPath(_localApplicationTestFolder);
            Assert.AreEqual(Common.ConfigureWcfServices, IOConfiguration.UpdateSource == ClientUpdateSource.WebService);
            Assert.AreEqual(_serverClientUpdateFolder, IOConfiguration.ServerClientUpdateFolderPath);
            Assert.AreEqual(_localApplicationTestFolder, IOConfiguration.LocalApplicationPath);

            // Create new version test files
            var fileInRoot = PrepareNewVersionFileOnServer("File.txt");
            var fileInSubfolder = PrepareNewVersionFileOnServer(@"SubFolder\File.txt");
            var hiddenFile = PrepareNewVersionFileOnServer(@"Hidden.txt", s => File.SetAttributes(s, FileAttributes.Hidden));
            var skippedFolder = PrepareNewVersionFileOnServer(@"~SkippedFolder\File.txt");

            // Run Updater
            var updater = new Updater(new[] {"-v"});
            Assert.IsTrue(updater.CheckForUpdate());
            updater.PerformUpdate(null);

            // Verify local folder is up to date
            Assert.IsTrue(File.Exists(Path.Combine(_localApplicationTestFolder, fileInRoot)));
            Assert.IsTrue(File.Exists(Path.Combine(_localApplicationTestFolder, fileInSubfolder)));
            Assert.IsFalse(File.Exists(Path.Combine(_localApplicationTestFolder, hiddenFile)));
            Assert.IsFalse(File.Exists(Path.Combine(_localApplicationTestFolder, skippedFolder)));
        }

        [TestMethod]
        [TestRunMode(
            ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresGlobalLock, // Test modifies configuration of web app
            UseDedicatedTestDatabase = false, 
            UseTransactionScope = false)]
        public void TestUpdateForNewClient()
        {
            const string testAppClientToken = "testapp";

            // Ensure we are running in app server mode
            if (!Common.ConfigureWcfServices)
            {
                Assert.Inconclusive("TestUpdateForNewClient is expected to run in application server mode");
            }

            // Use test app token
            ModifyTestAppClientConfig(c =>
            {
                var section = c.GetSection<ApplicationServerClientConfiguration>("applicationServerClient");
                section.AuthenticationToken = testAppClientToken;
            });

            // Init and validate updater configuration
            IOConfiguration.SetNewApplicationPath(_localApplicationTestFolder);
            Assert.AreEqual(Common.ConfigureWcfServices, IOConfiguration.UpdateSource == ClientUpdateSource.WebService);
            Assert.AreEqual(testAppClientToken, IOConfiguration.AuthenticationToken);

            // Create new version test files
            var fileInRoot = PrepareNewVersionFileOnServer("File.txt");

            // Ensure update fails
            var updater = new Updater(new[] { "-v" });
            Assert.Throws(() => updater.CheckForUpdate(), "Client not recognized. Please check connection settings", ExceptionMessageCompareOptions.Contains);

            try
            {
                // Add new client
                Common.ModifyWebConfiguration(configuration =>
                {
                    var serverConfig = configuration.GetSection<ApplicationServerConfiguration>("applicationServer");
                    var defaultClientConfig = serverConfig.Clients.First(c => c.Name == Common.DefaultApplicationServerClientName);
                    serverConfig.Clients.Add(new ClientConfiguration
                    {
                        AuthenticationToken = testAppClientToken,
                        ConnectionStringName = defaultClientConfig.ConnectionStringName,
                        Name = testAppClientToken,
                        ServerDataPath = defaultClientConfig.ServerDataPath
                    });
                });

                // Run Updater
                Assert.IsTrue(updater.CheckForUpdate());
                updater.PerformUpdate(null);

                // Verify local folder is up to date
                Assert.IsTrue(File.Exists(Path.Combine(_localApplicationTestFolder, fileInRoot)));
            }
            finally
            {
                // Ensure test client is removed
                Common.ModifyWebConfiguration(configuration =>
                {
                    var serverConfig = configuration.GetSection<ApplicationServerConfiguration>("applicationServer");
                    serverConfig.Clients.RemoveAll(c => c.AuthenticationToken == testAppClientToken);
                });
            }
        }

        private void ModifyTestAppClientConfig(Action<System.Configuration.Configuration> makeChangesCallback)
        {
            var systemConfigPath = Path.Combine(_localApplicationTestFolder, ConfigurationManager.SystemConfigurationFileName);
            var configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap { ExeConfigFilename = systemConfigPath }, ConfigurationUserLevel.None);
            makeChangesCallback(configuration);
            configuration.Save();
        }

        private string PrepareNewVersionFileOnServer(string relativePath, Action<string> specialHandling = null)
        {
            var filePath = Path.Combine(_serverClientUpdateFolder, relativePath);
            FileSystem.TryWriteFile(filePath, Encoding.UTF8.GetBytes("Test file created at following path: " + relativePath));
            if (specialHandling != null)
            {
                specialHandling(filePath);
            }
            return relativePath;
        }
    }
}
