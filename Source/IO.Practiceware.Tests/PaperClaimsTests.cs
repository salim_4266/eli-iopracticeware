﻿using System.Linq;
using System.Reflection;
using IO.Practiceware.DbMigration;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf;
using Soaf.IO;
using Soaf.Linq;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class PaperClaimsTests:TestBase
    {

        // Used in replacing PatientIds and ItemIds for expected unit test files
        public static BillingServiceTransaction[] GetQueuedBillingServiceTransactions(string patientFirstName = null, string patientLastName = null)
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWorkScope())
            {
                var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
                practiceRepository.IsLazyLoadingEnabled = true;

                var billingServiceTransactions = practiceRepository.BillingServiceTransactions.Include(x => x.BillingService).Where(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued && bst.MethodSent == MethodSent.Paper);

                if (patientFirstName.IsNotNullOrEmpty())
                {
                    billingServiceTransactions = billingServiceTransactions.Where(bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.FirstName == patientFirstName);
                }
                if (patientLastName.IsNotNullOrEmpty())
                {
                    billingServiceTransactions = billingServiceTransactions.Where(bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.LastName == patientLastName);
                }

                return billingServiceTransactions.ToArray();
            }
        }

        private const string PaperClaimsDocumentPath = "IO.Practiceware.DbMigration.Documents.";

        public static byte[] GetDocumentBytes(string documentName)
        {
            var path = PaperClaimsDocumentPath + documentName;

            var assembly = Assembly.GetAssembly(typeof(Migrator));

            try
            {
                using (var stream = assembly.GetManifestResourceStream(path))
                {
                    return stream.ToArray();
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
