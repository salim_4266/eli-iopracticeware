﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using IO.Practiceware.Integration.HL7.Connections;
using IO.Practiceware.Integration.HL7.Producers;
using IO.Practiceware.Integration.Omedix;
using IO.Practiceware.Services.ExternalSystems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IO.Practiceware.Services.PatientSearch;
using IO.Practiceware.Model;
using IO.Practiceware.Data;
using IO.Practiceware.Integration.HL7;
using IO.Practiceware.Integration.Messaging;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Threading;
using Appointment = IO.Practiceware.Model.Appointment;
using V23ADT_A28 = NHapi.Model.V23.Message.ADT_A28;
using V23ADT_A31 = NHapi.Model.V23.Message.ADT_A31;

using System.Transactions;
using System.Data;
using AppointmentType = IO.Practiceware.Model.AppointmentType;
using NHapi.Model.V23.Message;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class OmedixTests : TestBase
    {
        #region [ ECS ]
        private const string Ecs = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<?xml-stylesheet type=""text/xsl"" href=""CDA.xsl""?>

<ClinicalDocument xmlns:xsi = ""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation = ""urn:hl7-org:v3 file:///C:/MDHT_CDATools/workspace-consol/org.openhealthtools.mdht.uml.cda.consol.model/CDA_Schema_Files/infrastructure/cda/CDA_SDTC.xsd"" xmlns = ""urn:hl7-org:v3"" xmlns:mif = ""urn:hl7-org:v3/mif"">
  <realmCode code = ""US""/>
  <typeId root = ""2.16.840.1.113883.1.3"" extension = ""POCD_HD000040""/>
  <templateId root = ""2.16.840.1.113883.10.20.22.1.1""/>
  <templateId root = ""2.16.840.1.113883.10.20.22.1.2""/>
  <id extension=""2.16.840.1.113883.19"" root=""N""/>
  <code code = ""34133-9"" displayName = ""Summarization of episode note"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC""/>
  <title>2014 VDT AMB: Consolidated CDA - Ambulatory</title>
  <effectiveTime value = ""20120920153010+0500""/>
  <confidentialityCode code = ""N"" codeSystem = ""2.16.840.1.113883.5.25""/>
  <languageCode code = ""en-US""/>
  <recordTarget>
    <patientRole>
      <id extension = ""281020131808"" root = ""2.16.840.1.113883.19""/>
      <addr use = ""HP"">
        <streetAddressLine>1357 Amber Dr.</streetAddressLine>
        <city>Beaverton</city>
        <state>OR</state>
        <postalCode>97867</postalCode>
        <country>US</country>
      </addr>
      <telecom use=""HP"" value =""tel:(516)551-3202"" />
      <telecom use=""WP"" value =""tel:(810)673-9492"" />
      <telecom use=""MC"" value =""tel:(516)551-3202"" />
      <telecom value=""mailto:Jeffnevins@mailinator.com"" />
      <patient>
        <name>
          <given>Jeff</given>
          <family>Nevins</family>
        </name>
        <administrativeGenderCode code = ""M"" displayName = ""male"" codeSystem = ""2.16.840.1.113883.5.1"" codeSystemName = ""HL7 AdministrativeGender""/>
        <birthTime value = ""19840228""/>
        <maritalStatusCode code = ""M"" displayName = ""Married"" codeSystem = ""2.16.840.1.113883.5.2"" codeSystemName = ""HL7 Marital status""/>
        <raceCode code = ""2106-3"" displayName = ""White"" codeSystem = ""2.16.840.1.113883.6.238"" codeSystemName = ""Race &amp; Ethnicity - CDC""/> 
        <ethnicGroupCode code = ""2186-5"" displayName = ""Not Hispanic or Latino"" codeSystem = ""2.16.840.1.113883.6.238"" codeSystemName = ""Race &amp; Ethnicity - CDC""/>
        <languageCommunication>
          <languageCode code = ""eng""/>
          <modeCode code = ""RWR"" displayName = ""Receive Written"" codeSystem = ""2.16.840.1.113883.5.60"" codeSystemName = ""LanguageAbilityMode""/>
          <proficiencyLevelCode code = ""G"" displayName = ""Good"" codeSystem = ""2.16.840.1.113883.5.61"" codeSystemName = ""LanguageAbilityProficiency""/>
          <preferenceInd value = ""true""/>
        </languageCommunication>
      </patient>
      <providerOrganization>
        <id root = ""2.16.840.1.113883.19""/>
        <id extension = ""1234567891"" root = ""2.16.840.1.113883.4.6""/>
        <name>Get Well Clinic</name>
        <telecom use = ""WP"" value = ""tel:(555)555-1002""/>
        <addr use = ""WP"">
          <streetAddressLine>1002 Healthcare Dr.</streetAddressLine>
          <city>Portland</city>
          <state>OR</state>
          <postalCode>99123</postalCode>
          <country>US</country>
        </addr>
        <standardIndustryClassCode/>
        <asOrganizationPartOf/>
      </providerOrganization>
    </patientRole>
  </recordTarget>
  <author>
    <time value = ""20120920153010+0500""/>
    <assignedAuthor>
      <id extension = ""657654389"" root = ""2.16.840.1.113883.4.6""/>
      <code code = ""207W00000X"" codeSystem = ""2.16.840.1.113883.6.101"" codeSystemName = ""NUCC""/>
      <addr use = ""WP"">
        <streetAddressLine>1002 Healthcare Dr.</streetAddressLine>
        <city>Portland</city>
        <state>OR</state>
        <postalCode>99123</postalCode>
        <country>US</country>
      </addr>
      <telecom use = ""WP"" value = ""tel:(555)555-1002""/>
	  <assignedAuthoringDevice>
          <manufacturerModelName>IO Practiceware, inc.</manufacturerModelName >
          <softwareName>Administrative</softwareName >
      </assignedAuthoringDevice >
	</assignedAuthor>
  </author>
  <custodian>
    <assignedCustodian>
      <representedCustodianOrganization>
        <id extension = ""1234567891"" root = ""2.16.840.1.113883.4.6""/>
        <name>Get Well Clinic</name>
        <telecom use = ""WP"" value = ""tel:(555)555-1002""/>
        <addr use = ""WP"">
          <streetAddressLine>1002 Healthcare Dr.</streetAddressLine>
          <city>Portland</city>
          <state>OR</state>
          <postalCode>99123</postalCode>
          <country>US</country>
        </addr>
      </representedCustodianOrganization>
    </assignedCustodian>
  </custodian>
  <documentationOf>
    <serviceEvent classCode = ""PCPR"">
      <effectiveTime>
        <low value=""20040528000000-0400"" />
        <high value=""20130506000000-0400"" />
      </effectiveTime>
      <performer typeCode = ""PRF"">
        <functionCode code=""PP"" codeSystem""2.16.840.1.113883.5.88""/>
        <assignedEntity>
          <id extension = ""657654389"" root = ""2.16.840.1.113883.4.6""/>
          <code code = ""207QA0505X"" codeSystem = ""2.16.840.1.113883.6.101"" codeSystemName = ""NUCC"" displayName = ""Adult Medicine""/>
          <addr>
            <streetAddressLine>1002 Healthcare Dr.</streetAddressLine>
            <city>Portland</city>
            <state>OR</state>
            <postalCode>99123</postalCode>
            <country>US</country>
          </addr>
          <telecom use = ""WP"" value = ""tel:(555)555-1002""  />
          <assignedPerson>
            <name>
			  <prefix></prefix>
			  <given>IOP</given>
			  <family>STAFF</family>
			  <suffix>M.D.</suffix>
            </name>
          </assignedPerson>
        </assignedEntity>
      </performer>
    </serviceEvent>
  </documentationOf>
  <component>
    <structuredBody>
      <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.6.1""/>
          <code code = ""48765-2"" codeSystem = ""2.16.840.1.113883.6.1""/>
          <title>ADVERSE REACTIONS, ALERTS, ALLERGIES</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Substance</th>
                  <th>Reaction</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr ID = ""ALGSUMMARY_1"">
                  <td ID = ""ALGTYPE_1"">Drug Allergy</td>
                  <td ID = ""ALGSUB_1"">Penicillin G benzathine (RxNorm: 7982)</td>
                  <td ID = ""ALGREACT_1"">Hives</td>
                  <td ID = ""ALGSTATUS_1"">Active</td>
                </tr>
              </tbody>
            </table>
          </text>
		  <!--Allergy Problem Act Start-->
          <entry typeCode = ""DRIV"">
            <act classCode = ""ACT"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.30""/>
              <id root = ""36e3e930-7b14-11db-9fe1-0800200c9a66""/>
              <code code = ""48765-2"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Allergies, adverse reactions, alerts""/>
              <statusCode code = ""active""/>
              <effectiveTime>
                <low value = ""20120920""/> 
              </effectiveTime>
              <entryRelationship typeCode = ""SUBJ"">
			  <!--Allergy Problem Act End, Allergy Intolerance Observation Start-->
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.7""/>
                  <id root = ""4adc1020-7b14-11db-9fe1-0800200c9a66""/>
                  <code code = ""ASSERTION"" codeSystem = ""2.16.840.1.113883.5.4""/>
                  <statusCode code = ""completed""/>
                  <effectiveTime>
                    <low value = ""20120920""/>
                  </effectiveTime>
                  <value xsi:type = ""CD"" code = ""419511003"" displayName = ""Propensity to adverse reactions to drug (disorder)"" codeSystem = ""2.16.840.1.113883.6.96"" codeSystemName = ""SNOMED CT"">
                    <originalText>
                      <reference value = ""#ALGREACT_1""/>
                    </originalText>
                  </value>
                  <participant typeCode = ""CSM"">
                    <participantRole classCode = ""MANU"">
                      <playingEntity classCode = ""MMAT"">
                        <code code = ""7982"" codeSystem = ""2.16.840.1.113883.6.88"" displayName = ""Penicillin G benzathine"" codeSystemName = ""RxNorm"">
                          <originalText>
                            <reference value = ""#ALGSUB_1""/>
                          </originalText>
                        </code>
                        <name>Penicillin G benzathine</name>
                      </playingEntity>
                    </participantRole>
                  </participant>
                </observation>
              </entryRelationship>
			  		  <!--Start of Reaction Observation-->
                <entryRelationship typeCode = ""MFST"" inversionInd = ""true"">
                 <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.9""/>
                  <id root = ""7e85d3a0-2a9e-4cb6-b651-f5dc2607f619""/>
                  <code code = ""ASSERTION"" codeSystem = ""2.16.840.1.113883.5.4""/>
                  <text>
                    <reference value = ""#ALGREACT_1""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <value xsi:type = ""CD"" code = ""247472004"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Hives""/>
                </observation>
                </entryRelationship>	
			  		  <!--Start of Severity Observation-->
                <entryRelationship typeCode = ""SUBJ"" inversionInd = ""true"">
                  <observation classCode = ""OBS"" moodCode = ""EVN"">
                    <templateId root = ""2.16.840.1.113883.10.20.22.4.8""/>
                    <code code = ""SEV"" codeSystem = ""2.16.840.1.113883.5.4"" codeSystemName = ""HL7ActCode"" displayName = ""SeverityObservation""/>
                    <text>
                      <reference value = ""#ALGREACT_1""/>
                    </text>
                    <statusCode code = ""completed""/>
                    <value xsi:type = ""CD"" code = ""371924009"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Moderate to severe""/>
                  </observation>
                </entryRelationship>
            </act>
          </entry>
        </section>
      </component>
      <component>
	    <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.1.1""/>
          <code code = ""10160-0"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""HISTORY OF MEDICATION USE""/>
          <title>MEDICATIONS</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Medication</th>
                  <th>Start Date</th>
                  <th>Route and Frequency</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <content ID = ""Med1"">Proventil 0.09 MG/ACTUAT, 573621</content>
                  </td>
                  <td>9/20/2012</td>
                  <td>Inhalation, 2 puffs every 6 hours PRN wheezing</td>
                  <td>Active</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry typeCode = ""DRIV"">
            <substanceAdministration classCode = ""SBADM"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.16""/>
              <id root = ""cdbd33f0-6cde-11db-9fe1-0800200c9a66""/>
              <text>
                <reference value = ""#Med1""/>
                Proventil 0.09 MG/ACTUAT, 573621
              </text>
              <statusCode code = ""active""/>
              <effectiveTime xsi:type = ""IVL_TS"">
                <low value = ""20120920""/>
                <high nullFlavor = ""NI""/>
              </effectiveTime>
              <effectiveTime xsi:type = ""PIVL_TS"" institutionSpecified = ""false"" operator = ""A"">
                <period value = ""6"" unit = ""h""/>
              </effectiveTime>             
              <routeCode code = ""C38216"" displayName = ""RESPIRATORY (INHALATION)"" codeSystem = ""2.16.840.1.113883.3.26.1.1"" codeSystemName = ""NCI Thesaurus""/>
              <doseQuantity value = ""90"" unit = ""ml/min""/>
              <consumable>
                <manufacturedProduct classCode = ""MANU"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.23""/>
                  <manufacturedMaterial>
                    <code code = ""573621"" codeSystem = ""2.16.840.1.113883.6.88"" displayName = ""albuterol 0.09 MG/ ACTUAT metered dose"" codeSystemName = ""RxNorm"">
                      <originalText>
                        <reference value = ""#Med1""/>
						Proventil 0.09 MG/ACTUAT, 573621
                      </originalText>
                    </code>
                  </manufacturedMaterial>
                </manufacturedProduct>
              </consumable>
            </substanceAdministration>
          </entry>
        </section>
      </component>
      <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.5""/>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.5.1""/>
          <code code = ""11450-4"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""PROBLEM LIST""/>
          <title>PROBLEMS</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Name and Code</th>
                  <th>Date Range</th>
                </tr>
              </thead>
              <tbody>
                <tr ID = ""ProbSumm_1"">
                  <td>Diagnosis</td>
                  <td>Sinusitis,36971009</td>
                  <td>20120901</td>
                </tr>
				 <tr ID = ""ProbSumm_2"">
                  <td>Problem</td>
                  <td>Asthma,195967001</td>
                  <td>20130101</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry typeCode = ""DRIV"">
            <act classCode = ""ACT"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.3""/>
              <id root = ""ec8a6ff8-ed4b-4f7e-82c3-e98e58b45de7""/>
              <code code = ""CONC"" codeSystem = ""2.16.840.1.113883.5.6"" displayName = ""Concern""/>
              <statusCode code = ""active""/>
              <effectiveTime>
                <low value = ""20120901""/>
              </effectiveTime>
              <entryRelationship typeCode = ""SUBJ"">
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.4""/>
                  <id root = ""ab1791b0-5c71-11db-b0de-0800200c9a66""/>
                  <code code = ""282291009"" displayName = ""Diagnosis"" codeSystem = ""2.16.840.1.113883.6.96"" codeSystemName = ""SNOMED-CT""/>
                  <text>
                    <reference value = ""#ProbSumm_1""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <effectiveTime>
                    <low value = ""20120901""/>
                  </effectiveTime>
                  <value xsi:type = ""CD"" code = ""36971009"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Sinusitis""/>
                  <entryRelationship typeCode = ""REFR"">
                    <observation classCode = ""OBS"" moodCode = ""EVN"">
                      <templateId root = ""2.16.840.1.113883.10.20.22.4.6""/>
                      <code xsi:type=""CE"" code = ""33999-4"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Status""/>
                      <text>
                        <reference value = ""#ProbSumm_1""/>
                        <!--References Problem/Diagnosis from above table-->
                      </text>
                      <statusCode code = ""completed""/>
                      <!--static-->
                      <value xsi:type = ""CD"" code = ""55561003"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Active""/>
                    </observation>
                  </entryRelationship>
                </observation>
              </entryRelationship>
            </act>
          </entry> 
		     <entry typeCode = ""DRIV"">
             <act classCode = ""ACT"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.3""/>
              <id root = ""ec8a6ff8-ed4b-4f7e-82c3-e98e58b45de7""/>
              <code code = ""CONC"" codeSystem = ""2.16.840.1.113883.5.6"" displayName = ""Concern""/>
              <statusCode code = ""active""/>
              <effectiveTime>
                <low value = ""20130101""/>
              </effectiveTime>
              <entryRelationship typeCode = ""SUBJ"">
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.4""/>
                  <id root = ""ab1791b0-5c71-11db-b0de-0800200c9a66""/>
                  <code code = ""55607006"" displayName = ""Problem"" codeSystem = ""2.16.840.1.113883.6.96"" codeSystemName = ""SNOMED-CT""/>
                  <text>
                    <reference value = ""#ProbSumm_2""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <effectiveTime>
                    <low value = ""20130101""/>
                  </effectiveTime>
                  <value xsi:type = ""CD"" code = ""195967001"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Asthma""/>
                  <entryRelationship typeCode = ""REFR"">
                    <observation classCode = ""OBS"" moodCode = ""EVN"">
                      <templateId root = ""2.16.840.1.113883.10.20.22.4.6""/>
                      <code xsi:type=""CE"" code = ""33999-4"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Status""/>
                      <text>
                        <reference value = ""#ProbSumm_2""/>
                      </text>
                      <statusCode code = ""completed""/>
                      <value xsi:type = ""CD"" code = ""55561003"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Active""/>
                    </observation>
                  </entryRelationship>
                </observation>
             </entryRelationship>
            </act>  
          </entry>
        </section>
      </component>
      <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.7""/>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.7.1""/>
          <code code = ""47519-4"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""PROCEDURES""/>
          <title>PROCEDURES</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ID = ""procedure_1"">Chest X-Ray, PA and Lateral Views, 168731009</td>
                  <td>9/20/2012</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry>
            <procedure classCode = ""PROC"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.14""/>
              <id root = ""e401f340-7be2-11db-9fe1-0800200c9a66""/>
              <code xsi:type = ""CE"" code = ""168731009"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Chest X-Ray, PA and Lateral Views "">
                <originalText>
                  Chest X-Ray, PA and Lateral Views, [SNOMED CT: 168731009]
                  <reference value = ""#procedure_1""/>
                </originalText>
              </code>
              <statusCode code = ""completed""/>
              <effectiveTime value = ""20120920""/>
            </procedure>
          </entry>
        </section>
      </component>
      <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.3.1""/>
          <code code = ""30954-2"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""RESULTS""/>
          <title>RESULTS</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Result Values</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <content ID = ""result1"">HGB, [LOINC: 30313-1]</content>
                  </td>
                  <td>12.8 g/dl</td>
                  <td>9/20/2012</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry typeCode = ""DRIV"">
            <organizer classCode = ""BATTERY"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.1""/>
              <id root = ""7d5a02b0-67a4-11db-bd13-0800200c9a66""/>
              <code xsi:type = ""CE"" code = ""57021-8"" displayName = ""CBC W Auto Differential panel in Blood"" codeSystemName = ""LOINC"" codeSystem = ""2.16.840.1.113883.6.1""/>
              <statusCode code = ""completed""/>
              <component>
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.2""/>
                  <id root = ""107c2dc0-67a5-11db-bd13-0800200c9a66""/>
                  <code xsi:type = ""CE"" code = ""30313-1"" displayName = ""Hemoglobin [Mass/volume] in Arterial blood""  codeSystem = ""2.16.840.1.113883.6.1""/>
                  <text>
                    <reference value = ""#result1""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <effectiveTime value = ""20120920""/>
                  <value xsi:type = ""PQ"" unit = ""g/dl "" value = ""12.8""/>
                  <interpretationCode code = ""N"" codeSystem = ""2.16.840.1.113883.5.83""/>
                  <referenceRange>
                    <observationRange>
                      <text>M 13-18 g/dl; F 12-16 g/dl</text>
                    </observationRange>
                  </referenceRange>
                </observation>
              </component>
            </organizer>
          </entry>
        </section>
      </component>
	  <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.14""/>
          <code code=""47420-5"" codeSystem""2.16.840.1.113883.6.1""/>
          <title>Functional Status</title>
          <text>
            <table border=""1"" width=""100%"">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ID = ""functional_status_1"">No impairment, 66577003</td>
                  <td>2013</td>
                  <td>Active</td>
                </tr>
              </tbody>
            </table>
          </text>
		  <entry typeCode = ""DRIV""> 
            <observation classCode = ""OBS"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.68""/>
              <id root = ""2.16.840.1.113883.1.3""/>
              <code code = ""404684003"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Finding of Functional Performance and activity""/>
              <text>
                <reference value = ""#functional_status_1""/>
              </text>
              <statusCode code = ""completed""/>
              <effectiveTime>
                <low value = ""20131001""/>
              </effectiveTime>
              <value xsi:type = ""CD"" code = ""66577003"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""No impairment""/>
            </observation>
          </entry>  
        </section>
      </component>
      <component>
        <section>
          <templateId root=""2.16.840.1.113883.10.20.22.2.2.1""/>
          <code code=""11369-6"" codeSystem""2.16.840.1.113883.6.1"" codeSystemName=""LOINC"" displayName=""History of immunizations""/>
          <title>Immunizations</title>
          <text>
            <table border=""1"" width=""100%"">
              <thead>
                <tr>
                  <th>Vaccine</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <content ID=""immun1"">Influenza virus vaccine, IM content</content>
                  </td>
                  <td>Nov 1999</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry>
            <substanceAdministration classCode=""SBADM"" moodCode=""EVN"" negationInd=""false"">
              <templateId root=""2.16.840.1.113883.10.20.22.4.52""/>
              <id root=""e6f1ba43-c0ed-4b9b-9f12-f435d8ad8f92""/>
              <text>
                <reference value=""#immun1""/>
              </text>
              <statusCode code=""completed""/>
              <effectiveTime xsi:type=""IVL_TS"" value=""19981215""/>
              <consumable >
                <manufacturedProduct classCode=""MANU"">
                  <templateId root=""2.16.840.1.113883.10.20.22.4.54""/>
                  <manufacturedMaterial>
                    <code code = ""88"" codeSystem = ""2.16.840.1.113883.12.292"" displayName = ""Influenza virus vaccine"" codeSystemName = ""CVX"">
                      <originalText>
                        <reference value=""#immun1""/>
                      </originalText>
                    </code>
                  </manufacturedMaterial>
                </manufacturedProduct>
              </consumable>
            </substanceAdministration>
          </entry>
        </section>
      </component>
	  <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.10""/>
          <code code = ""18776-5"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Treatment plan""/>
          <title>PLAN OF CARE</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ID = ""Goal_1"">Weight loss, 289169006</td>
                  <td>9/20/2012</td>
                </tr>
				<tr>
                  <td ID = ""Instructions_1""> diet and exercise counseling provided during visit</td>
                  <td>9/20/2012</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry>
            <observation classCode=""OBS"" moodCode=""GOL"" >
              <templateId root=""2.16.840.1.113883.10.20.22.4.44""/>
              <id root=""9a6d1bac-17d3-4195-89a4-1121bc809b4d""/>
              <code code=""289169006e"" codeSystem""2.16.840.1.113883.6.96"" displayName=""Weight loss""/>
              <statusCode code = ""new""/>
              <effectiveTime>
                <center value = ""20120920""/>
              </effectiveTime>
            </observation>
          </entry>
          <entry>
            <act classCode=""ACT"" moodCode=""INT"">
              <templateId root=""2.16.840.1.113883.10.20.22.4.20""/>
              <id root=""9a6d1bac-17d3-4195-89a4-1121bc809b4d""/>
              <code xsi:type = ""CE"" code = ""311401005"" codeSystem = ""2.16.840.1.113883.6.96"" displayName = ""Patient Education""/>
              <text>
                diet and exercise counseling provided during visit
              </text>
              <statusCode code = ""completed""/>
            </act>
          </entry>
        </section>
      </component>
	  <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.17""/>
          <code code = ""29762-2"" codeSystem = ""2.16.840.1.113883.6.1"" displayName = ""Social History""/>
          <title>SOCIAL HISTORY</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Smoking Status</th>
                  <th>Effective Dates</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Never smoker, 266919005</td>
                  <td>1/1/2012</td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry typeCode = ""DRIV"">
            <observation classCode = ""OBS"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.78""/>
              <id root = ""bb4029d5-cd10-437e-8985-6db6db5beb12""/>
              <code code = ""ASSERTION"" codeSystem = ""2.16.840.1.113883.5.4""/>
              <statusCode code = ""completed""/>
              <effectiveTime>
                <low value = ""20120101""/>
              </effectiveTime>
              <value xsi:type = ""CD"" code = ""266919005"" displayName = ""Never smoker"" codeSystem = ""2.16.840.1.113883.6.96""/>
            </observation>
          </entry>
        </section>
      </component>	  
	  <component>
        <section>
          <templateId root = ""2.16.840.1.113883.10.20.22.2.4.1""/>
          <code code = ""8716-3"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""VITAL SIGNS""/>
          <title>VITAL SIGNS</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th align = ""right"">Date / Time:</th>
                  <th ID = ""date_1"">9/20/2012</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th align = ""left"">Height</th>
                  <td>
                    <content ID = ""date_1_height"">64 in</content>
                  </td>
                </tr>
                <tr>
                  <th align = ""left"">Weight</th>
                  <td>
                    <content ID = ""date_2_height"">65 in</content>
                  </td>
                </tr>
              </tbody>
            </table>
          </text>
          <entry typeCode = ""DRIV"">
            <organizer classCode = ""CLUSTER"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.26""/>
              <id root = ""d1ad5613-3339-4248-9b75-35b8a6104c3c""/>
              <code code = ""46680005"" codeSystem = ""2.16.840.1.113883.6.96"" codeSystemName = ""SNOMED -CT"" displayName = ""Vital signs""/>
              <statusCode code = ""completed""/>
              <effectiveTime value = ""20120920""/>
              <component>
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.27""/>
                  <id root = ""5d8c2260-9c32-4bc7-8053-79dc510fdcd9""/>
                  <code code = ""8302-2"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Height""/>
                  <text>
                    <reference value = ""#date_1_height""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <effectiveTime value = ""20120920""/>
                  <value xsi:type = ""PQ"" value = ""64"" unit = ""[in_us]""/>
                </observation>
              </component>
            </organizer>
          </entry>
		  <entry typeCode = ""DRIV"">
            <organizer classCode = ""CLUSTER"" moodCode = ""EVN"">
              <templateId root = ""2.16.840.1.113883.10.20.22.4.26""/>
              <id root = ""d1ad5613-3339-4248-9b75-35b8a6104c3c""/>
              <code code = ""46680005"" codeSystem = ""2.16.840.1.113883.6.96"" codeSystemName = ""SNOMED -CT"" displayName = ""Vital signs""/>
              <statusCode code = ""completed""/>
              <effectiveTime value = ""20121001""/>
              <component>
                <observation classCode = ""OBS"" moodCode = ""EVN"">
                  <templateId root = ""2.16.840.1.113883.10.20.22.4.27""/>
                  <id root = ""5d8c2260-9c32-4bc7-8053-79dc510fdcd9""/>
                  <code code = ""8302-2"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""Height""/>
                  <text>
                    <reference value = ""#date_2_height""/>
                  </text>
                  <statusCode code = ""completed""/>
                  <effectiveTime value = ""20120920""/>
                  <value xsi:type = ""PQ"" value = ""65"" unit = ""[in_us]""/>
                </observation>
              </component>
            </organizer>
          </entry>
        </section>
      </component>
      <component>
        <section>
          <templateId root = ""1.3.6.1.4.1.19376.1.5.3.1.3.1""/>
          <code code = ""42349-1"" codeSystem = ""2.16.840.1.113883.6.1"" codeSystemName = ""LOINC"" displayName = ""CHIEF COMPLAINT AND REASON FOR VISIT""/>
          <title>REASON FOR REFERRAL</title>
          <text>
            <table border = ""1"" width = ""100%"">
              <thead>
                <tr>
                  <th>Reason for Referral</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Two week history of nasal congestion, nasal discharge, facial pain, and intermittent fevers.</td>
                </tr>
              </tbody>
            </table>
          </text>
        </section>
      </component>
    </structuredBody>
  </component>
</ClinicalDocument>";
        #endregion [ ECS ]


        private const string OmedixExternalSystemName = "Omedix";


        private IExternalSystemService _externalSystemService;
        private IOmedixIntegrationService _patientManager;
        private WebServiceMessageConnector _webConnector;
        private MessageProcessor _messageProcessor;
        private MessageAuditor _messageAuditor;

        private static void CreateMapping()
        {

            var apptype = Common.CreateAppointmentType();
            Common.PracticeRepository.Save(apptype);

            var users = Common.CreateUser();
            Common.PracticeRepository.Save(users);

            var location = Common.CreateServiceLocation();
            Common.PracticeRepository.Save(location);

            var entitiList = Common.PracticeRepository.ExternalSystemEntities;


            // AppointmentType
            ExternalSystemEntityMapping mapping = Common.CreateExternalSystemEntityMapping();
            mapping.ExternalSystemEntityKey = "12348";
            //mapping.ExternalSystemEntityId = 7;
            mapping.ExternalSystemEntityId = entitiList.First(u => u.PracticeRepositoryEntityId == (int) PracticeRepositoryEntityId.AppointmentType).Id;
            mapping.PracticeRepositoryEntityKey = "1";
            mapping.PracticeRepositoryEntityId = 2;
            Common.PracticeRepository.Save(mapping);

            // Doctor
            mapping = Common.CreateExternalSystemEntityMapping();
            mapping.ExternalSystemEntityKey = "1";
            //mapping.ExternalSystemEntityId = 8;
            mapping.ExternalSystemEntityId = entitiList.First(u => u.PracticeRepositoryEntityId == (int) PracticeRepositoryEntityId.User).Id;
            mapping.PracticeRepositoryEntityKey = "1";
            mapping.PracticeRepositoryEntityId = 6;
            Common.PracticeRepository.Save(mapping);


            // Locations
            mapping = Common.CreateExternalSystemEntityMapping();
            mapping.ExternalSystemEntityKey = "Fishman Center For Total Eye Care";
            //mapping.ExternalSystemEntityId = 14;
            mapping.ExternalSystemEntityId = entitiList.First(u => u.PracticeRepositoryEntityId == (int) PracticeRepositoryEntityId.ServiceLocation).Id;
            mapping.PracticeRepositoryEntityKey = "1";
            mapping.PracticeRepositoryEntityId = 8;
            Common.PracticeRepository.Save(mapping);
        }

        protected override void OnAfterTestInitialize()
        {
            _externalSystemService = Common.ServiceProvider.GetService<IExternalSystemService>();

            var messenger = ServiceProvider.GetService<IMessenger>();
            var authenticator = ServiceProvider.GetService<WebServiceAuthenticator>();
            _messageProcessor = new MessageProcessor(PracticeRepository, messenger);
            _messageAuditor = new MessageAuditor(PracticeRepository, _messageProcessor);
            _webConnector = new WebServiceMessageConnector(() => _messageAuditor, messenger, authenticator);
            _patientManager = new OmedixIntegrationService(PracticeRepository, ServiceProvider.GetService<IPatientSearchService>(), _messageAuditor, authenticator);

            _messageAuditor.Transaction = Transaction.Current;
            _messageProcessor.Transaction = Transaction.Current;

            // Register systems, messages types and entities.

            // Omedix
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.ADT_A28_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.ADT_A31_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S12_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S13_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S15_V23_Inbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S12_V23_Outbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S14_V23_Outbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.SIU_S15_V23_Outbound);
            _externalSystemService.EnsureExternalSystemMessageType(OmedixExternalSystemName, ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound);

            //Omedix
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Patient", PracticeRepositoryEntityId.Patient);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Insurer", PracticeRepositoryEntityId.Insurer);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Visit", PracticeRepositoryEntityId.Encounter);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "User", PracticeRepositoryEntityId.User);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Contact");
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Appointment", PracticeRepositoryEntityId.Appointment);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "AppointmentType", PracticeRepositoryEntityId.AppointmentType);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "AttendingDoctor", PracticeRepositoryEntityId.User);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "ReferringDoctor", PracticeRepositoryEntityId.ExternalContact);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "LocationResource", PracticeRepositoryEntityId.ServiceLocation);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Ethnicity", PracticeRepositoryEntityId.Ethnicity);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Language", PracticeRepositoryEntityId.Language);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "PatientInsurance");
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "ServiceLocation", PracticeRepositoryEntityId.ServiceLocation);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "MaritalStatus", PracticeRepositoryEntityId.MaritalStatus);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Gender", PracticeRepositoryEntityId.Gender);
            _externalSystemService.EnsureExternalSystemEntity(OmedixExternalSystemName, "Race", PracticeRepositoryEntityId.Race);

        }

        protected override void OnBeforeTestCleanup()
        {
            Func<bool> hasUnprocessedMessages = () => PracticeRepository.ExternalSystemMessages.All(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed);
            hasUnprocessedMessages.Wait(TimeSpan.FromSeconds(5));

            _messageProcessor.WaitForIdle(TimeSpan.FromSeconds(10));
            _messageAuditor.WaitForIdle();
            _webConnector.Stop();

        }

        #region OmedixGeneralMethods

        [TestMethod]
        public void TestSearchWithFirstName()
        {
            // Create patient and save
            Patient patient1 = Common.CreatePatient();
            Common.PracticeRepository.Save(patient1);
            Assert.IsTrue(patient1.Id > 0);

            var patient = new PatientSearchModel();
            patient.FirstName = patient1.FirstName;
            List<PatientSearchResult> patientsList = _patientManager.SearchPatient(patient);

            Assert.IsNotNull(patientsList, "No records found.");
            Assert.IsTrue(patientsList.Count > 0, "No records found.");
            Assert.IsTrue(patientsList.Select(p => p.FirstName == patient.FirstName).Any(), "Firstname not found.");

        }

        [TestMethod]
        public void TestSearchWithLastName()
        {

            Patient patient1 = Common.CreatePatient();
            Common.PracticeRepository.Save(patient1);
            Assert.IsTrue(patient1.Id > 0);

            var patients = new PatientSearchModel();
            patients.LastName = patient1.LastName;
            List<PatientSearchResult> patientsList = _patientManager.SearchPatient(patients);
            Assert.IsNotNull(patientsList, "No records found.");
            Assert.IsTrue(patientsList.Count > 0, "No records found.");
            Assert.IsTrue(patientsList.Select(p => p.LastName == patients.LastName).Any(), "Lastname not found.");
        }

        [TestMethod]
        public void TestSearchWithAll()
        {

            Patient patient1 = Common.CreatePatient();
            Common.PracticeRepository.Save(patient1);
            Common.AddPhoneNumbers(patient1);

            Assert.IsTrue(patient1.Id > 0);
            Assert.IsNotNull(patient1.DateOfBirth);

            var patients = new PatientSearchModel();
            patients.LastName = patient1.LastName;
            patients.FirstName = patient1.FirstName;
            patients.DateOfBirth = patient1.DateOfBirth == null ? string.Empty : patient1.DateOfBirth.ToMMDDYYYYString();
            List<PatientSearchResult> patientsList = _patientManager.SearchPatient(patients);

            Assert.IsNotNull(patientsList, "No records found.");
            Assert.IsTrue(patientsList.Select(p => p.LastName == patients.LastName && p.FirstName == patients.FirstName && p.CellPhone == patients.CellPhone && p.DateOfBirth == Convert.ToDateTime(patients.DateOfBirth)).Any(), "No records found.");
        }

        [TestMethod]
        public void TestGetAppointmentTypes()
        {
            AppointmentType apptype = Common.CreateAppointmentType();
            Common.PracticeRepository.Save(apptype);
            Assert.IsTrue(apptype.Id > 0);

            List<AppointmentTypeModel> appointmentTypes = _patientManager.GetAppointmentTypes();
            Assert.IsNotNull(appointmentTypes, "No records found.");
        }

        [TestMethod]
        public void TestGetLocations()
        {
            ServiceLocation location = Common.CreateServiceLocation();
            Common.PracticeRepository.Save(location);
            Assert.IsTrue(location.Id > 0);

            List<LocationsModel> locations = _patientManager.GetLocations();
            Assert.IsNotNull(locations, "No records found.");
        }

        [TestMethod]
        public void TestGetPhysicians()
        {
            User users = Common.CreateUser();
            Common.PracticeRepository.Save(users);
            Assert.IsTrue(users.Id > 0);

            List<PhysicianModel> physician = _patientManager.GetPhysicians();
            Assert.IsNotNull(physician, "No records found.");
        }

        [TestMethod]
        public void TestGetAppointments()
        {
            Appointment apps = Common.CreateAppointment();
            Common.PracticeRepository.Save(apps);
            Assert.IsTrue(apps.Id > 0);

            var appointments = new AppointmentSearchModel();
            appointments.StartDate = "01/01/2010";
            appointments.EndDate = "03/01/2013";
            List<AppointmentModel> appointmentList = _patientManager.SearchAppointments(appointments).ToList();
            Assert.IsNotNull(appointmentList, "No records found.");
            Assert.IsTrue(appointmentList.Select(p => p.AppointmentDate >= Convert.ToDateTime(appointments.StartDate) && p.AppointmentDate <= Convert.ToDateTime(appointments.EndDate)).Any(), "No records found between date");
        }

        [TestMethod]
        public void TestGetPatientAppointments()
        {
            Appointment apps = Common.CreateAppointment();
            Common.PracticeRepository.Save(apps);
            Assert.IsTrue(apps.Id > 0);

            var appointments = new AppointmentSearchModel();
            appointments.StartDate = "01/01/2010";
            appointments.EndDate = "03/01/2013";
            appointments.PatientId = apps.Encounter.PatientId.ToString();

            List<AppointmentModel> appointmentList = _patientManager.SearchAppointments(appointments).ToList();
            Assert.IsNotNull(appointmentList, "No records found.");
            Assert.IsTrue(appointmentList.Select(p => p.AppointmentDate >= Convert.ToDateTime(appointments.StartDate) && p.AppointmentDate <= Convert.ToDateTime(appointments.EndDate) && p.PatientId == Convert.ToInt32(appointments.PatientId)).Any(), "No appointment found for patient.");
        }

        #endregion OmedixGeneralMethods

        #region HL7Message

        [TestMethod]
        public void TestAdt28InboundMessage()
        {
            CreateMapping();

            DateTime start = DateTime.UtcNow;

            int lastPatientId = PracticeRepository.Patients.OrderByDescending(i => i.Id).Select(i => i.Id).FirstOrDefault();

            _messageProcessor.ProcessUnprocessedMessages();

            string messageText = HL7Resources.ADT_A28MessageOmedix;
            var messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            var message = (V23ADT_A28)HL7Messages.GetPipeParser().Parse(messageText);

            WaitForMessageProcessed(start);

            Patient newPatient = PracticeRepository.Patients.FirstOrDefault(i => i.Id > lastPatientId);
            Assert.IsNotNull(newPatient);


            ExternalSystemEntityMapping mapping = PracticeRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity.ExternalSystem,
                i => i.PracticeRepositoryEntity).FirstOrDefault(i => i.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient &&
                    i.PracticeRepositoryEntityKey == newPatient.Id.ToString() && i.ExternalSystemEntity.Name == "Patient" &&
                    i.ExternalSystemEntity.ExternalSystem.Name == message.MSH.SendingApplication.NamespaceID.Value);

            Assert.IsNotNull(mapping);

            Assert.IsTrue(newPatient != null && newPatient.LastName == message.PID.PatientName.FamilyName.Value.Truncate(35));
            Assert.IsTrue(newPatient.FirstName == message.PID.PatientName.GivenName.Value.Truncate(35));
        }

        [TestMethod]
        public void TestAdt31InboundMessage()
        {
            CreateMapping();

            DateTime start = DateTime.UtcNow;

            _messageProcessor.ProcessUnprocessedMessages();

            string messageText = HL7Resources.ADT_A31MessageOmedix;
            var messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            var message = (V23ADT_A31)HL7Messages.GetPipeParser().Parse(messageText);
            WaitForMessageProcessed(start, new HL7Message(message).Id.ToString());
            Patient patient = PracticeRepository.Patients.FirstOrDefault(i => i.LastName == message.PID.PatientName.FamilyName.Value.Truncate(35) && i.FirstName == message.PID.PatientName.GivenName.Value.Truncate(35));

            ExternalSystemEntityMapping mapping = PracticeRepository.ExternalSystemEntityMappings.Include(i => i.ExternalSystemEntity.ExternalSystem,
                i => i.PracticeRepositoryEntity).FirstOrDefault(i => i.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient &&
                    i.PracticeRepositoryEntityKey == patient.Id.ToString() && i.ExternalSystemEntity.Name == "Patient" &&
                     i.ExternalSystemEntity.ExternalSystem.Name == message.MSH.SendingApplication.NamespaceID.Value);

            Assert.IsNotNull(mapping);

            Assert.IsTrue(patient != null && patient.LastName == message.PID.PatientName.FamilyName.Value.Truncate(35));
            Assert.IsTrue(patient.FirstName == message.PID.PatientName.GivenName.Value.Truncate(35));
        }

        /// <summary>
        ///   Sets up the data for SIU messages.
        /// </summary>
        private static void SetUpSiu()
        {
            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Open();
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    foreach (string sqlStatement in HL7Resources.SetUpSIUOmedix.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        cmd.CommandText = sqlStatement;
                        cmd.ExecuteNonQuery();
                    }
                }
                connection.Close();
            }
        }

        [TestMethod]
        public void TestSiu12InboundMessage()
        {
            SetUpSiu();

            // Schedule the appointment
            string messageText = HL7Resources.SIU_S12MessageOmedix;
            var s12Message = (SIU_S12)HL7Messages.GetPipeParser().Parse(messageText);
            DateTime start = DateTime.UtcNow;

            _messageProcessor.ProcessUnprocessedMessages();

            var messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);

            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.GetMapping(OmedixExternalSystemName, PracticeRepositoryEntityId.Appointment, s12Message.SCH.FillerAppointmentID.EntityIdentifier.Value);
            Assert.IsTrue(appointmentMapping != null && appointmentMapping.PracticeRepositoryEntityKey != "0");

            var appointment = (Appointment)_externalSystemService.GetMappedEntity(appointmentMapping);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(appointment.DateTime == s12Message.SCH.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.GetAsDate());
        }

        [TestMethod]
        public void TestSiu13InboundMessage()
        {
            SetUpSiu();

            DateTime start = DateTime.UtcNow;

            _messageProcessor.ProcessUnprocessedMessages();

            string messageText = HL7Resources.SIU_S12MessageOmedix;
            var s12Message = (SIU_S12)HL7Messages.GetPipeParser().Parse(messageText);
            var messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);

            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.GetMapping(OmedixExternalSystemName,PracticeRepositoryEntityId.Appointment, s12Message.SCH.FillerAppointmentID.EntityIdentifier.Value);
            Assert.IsTrue(appointmentMapping != null && appointmentMapping.PracticeRepositoryEntityKey != "0");

            var appointment = (Appointment)_externalSystemService.GetMappedEntity(appointmentMapping);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(appointment.DateTime == s12Message.SCH.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.GetAsDate());

            // Cancel the appointment
            DateTime start3 = DateTime.UtcNow;
            messageText = HL7Resources.SIU_S13MessageOmedix;
            messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            externalSystemMessage = WaitForMessageProcessed(start3);
            Assert.IsNotNull(externalSystemMessage);
            appointment = Common.PracticeRepository.Appointments.Include(i => i.Encounter).FirstOrDefault(i => i.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(IsPendingStatus(appointment.Encounter.EncounterStatusId));
        }

        [TestMethod]
        public void TestSiu15InboundMessage()
        {
            SetUpSiu();
            DateTime start = DateTime.UtcNow;
            _messageProcessor.ProcessUnprocessedMessages();

            string messageText = HL7Resources.SIU_S12MessageOmedix;
            var s12Message = (SIU_S12)HL7Messages.GetPipeParser().Parse(messageText);
            var messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            ExternalSystemMessage externalSystemMessage = WaitForMessageProcessed(start);
            Assert.IsNotNull(externalSystemMessage);
            ExternalSystemEntityMapping appointmentMapping = _externalSystemService.GetMapping(OmedixExternalSystemName, PracticeRepositoryEntityId.Appointment, s12Message.SCH.FillerAppointmentID.EntityIdentifier.Value);
            Assert.IsTrue(appointmentMapping != null && appointmentMapping.PracticeRepositoryEntityKey != "0");

            var appointment = (Appointment)_externalSystemService.GetMappedEntity(appointmentMapping);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(appointment.DateTime == s12Message.SCH.GetAppointmentTimingQuantity(0).StartDateTime.TimeOfAnEvent.GetAsDate());

            // Cancel the appointment
            DateTime start3 = DateTime.UtcNow;
            messageText = HL7Resources.SIU_S15MessageOmedix;
            messageModel = new MessageIntegrationModel();
            messageModel.Message = messageText;
            _patientManager.ReceiveHL7Message(messageModel);

            externalSystemMessage = WaitForMessageProcessed(start3);
            Assert.IsNotNull(externalSystemMessage);
            appointment = Common.PracticeRepository.Appointments.Include(i => i.Encounter).FirstOrDefault(i => i.Id == appointmentMapping.PracticeRepositoryEntityKey.ToInt().Value);
            Assert.IsNotNull(appointment);
            Assert.IsTrue(IsCancelledStatus(appointment.Encounter.EncounterStatusId));
        }

        [TestMethod]
        public void TestSiu12OutboundMessage()
        {
            var appointment = Common.CreateAppointment();
            Common.PracticeRepository.Save(appointment);

            var s12Message = (SIU_S12)Common.ServiceProvider.GetService<AppointmentMessageProducer>().CreateSiuMessage(appointment, OmedixExternalSystemName, (int)ExternalSystemMessageTypeId.SIU_S12_V23_Outbound, "16caf61ada564f44aede743cd207ea14");
            Assert.IsNotNull(s12Message);

            string messageString = HL7Messages.GetPipeParser().Encode(s12Message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            Assert.IsTrue(s12Message.SCH.FillerStatusCode.Identifier.Value == "Booked");
        }
        [TestMethod]
        public void TestSIUACK()
        {
            var messageString = @"MSH|^~\&|ATHENANET|1615^AZ - Eye Physicians Surgeons of Arizona|IOPRACTICEWARE::1615||201512171130||SIU^S14|1219078|P|2.3|||AL|AL|AL|||

SCH|1295873|1295873||||BRIEF|f/u before neurology visit on 12/30 -P;CAN NOT REACH THIS PATIENT NUMBER NOT IN SERVICE//ALTERNATE NUMBER ON PPW GOES TO WORK PLACE OF MOTHER//CAN NOT VERIFY WHAT INSURANCE PATIENT NOW HAS//IF SHE SHOWS UP AND WE ARE NOT CONTRACTED EITHER SELF PAY OR RESCHEDULE//TM 12/14/15;PLEASE UPDATE DEMOGRAPHIC INFO|BRF^BRIEF|5|minutes|^^^201512151050|||||pverdoza|||||||||COMPLETED
PID||20116|20116||SAPP^CARLY^^||19980216|F||941^Patient Declined|7155 W. WILLOW AVE^^PEORIA^AZ^85381^UNITED STATES||(623)878-0683^||124^English|S||||||61^Patient Declined||||||||
PV1|||21^GLENDALE OFFICE^^GLENDALE OFFICE||||21^Patel_S|^^|^^||||||||21^Patel_S|||||||||||||||||||||||||||||||||||
DG1||ICD10|G43109|Migraine with aura, not intractable, w/o status migrainosus|||||||||||||||
RGS|||
AIG|||Patel_S|||||201512151050|||5|minutes||
AIL|||21^GLENDALE OFFICE|||201512151050|||5|minutes||";
            var hl7Message = new HL7Message(messageString);
            //Trace.TraceInformation("Message received of type {0}: {1}", hl7Message.MessageType, messageString);
            //_auditor.Audit(hl7Message, false, ProcessingState.Unprocessed);
            if (hl7Message.RequestsAcknowledgement)
            {
                IMessage acknowledgement = hl7Message.GetAcknowledgement();
                if (acknowledgement != null) 
                { 
                  //  SendMessage(acknowledgement); 
                }
            }
        }

        [TestMethod]
        public void TestSiu14OutboundMessage()
        {
            var appointment = Common.CreateAppointment();
            appointment.Encounter.EncounterStatus = EncounterStatus.Questions;
            Common.PracticeRepository.Save(appointment);

            var s14Message = (SIU_S14)Common.ServiceProvider.GetService<AppointmentMessageProducer>().CreateSiuMessage(appointment, OmedixExternalSystemName, (int)ExternalSystemMessageTypeId.SIU_S14_V23_Outbound, "16caf61ada564f44aede743cd207ea14");
            Assert.IsNotNull(s14Message);

            string messageString = HL7Messages.GetPipeParser().Encode(s14Message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            Assert.IsTrue(s14Message.SCH.FillerStatusCode.Identifier.Value == "CheckIn");


            var checkoutAppointment = appointment;
            checkoutAppointment.Encounter.EncounterStatus = EncounterStatus.Checkout;
            Common.PracticeRepository.Save(checkoutAppointment);

            s14Message = (SIU_S14)Common.ServiceProvider.GetService<AppointmentMessageProducer>().CreateSiuMessage(checkoutAppointment, OmedixExternalSystemName, (int)ExternalSystemMessageTypeId.SIU_S14_V23_Outbound, "16caf61ada564f44aede743cd207ea14");
            Assert.IsNotNull(s14Message);

            messageString = HL7Messages.GetPipeParser().Encode(s14Message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            Assert.IsTrue(s14Message.SCH.FillerStatusCode.Identifier.Value == "CheckOut");
        }

        [TestMethod]
        public void TestSiu15OutboundMessage()
        {
            var appointment = Common.CreateAppointment();
            appointment.Encounter.EncounterStatus = EncounterStatus.CancelPatient;
            Common.PracticeRepository.Save(appointment);

            var s15Message = (SIU_S15)Common.ServiceProvider.GetService<AppointmentMessageProducer>().CreateSiuMessage(appointment, OmedixExternalSystemName, (int)ExternalSystemMessageTypeId.SIU_S15_V23_Outbound, "16caf61ada564f44aede743cd207ea14");
            Assert.IsNotNull(s15Message);

            string messageString = HL7Messages.GetPipeParser().Encode(s15Message);
            Assert.IsFalse(messageString.IsNullOrEmpty());

            Assert.IsTrue(s15Message.SCH.FillerStatusCode.Identifier.Value == "Cancel");
        }

        public bool IsCancelledStatus(int status)
        {
            return status == (int)EncounterStatus.CancelLeft || status == (int)EncounterStatus.CancelNoShow || status == (int)EncounterStatus.CancelOffice ||
                   status == (int)EncounterStatus.CancelOther || status == (int)EncounterStatus.CancelPatient || status == (int)EncounterStatus.CancelSameDay ||
                   status == (int)EncounterStatus.CancelSpecial;
        }

        public bool IsPendingStatus(int status)
        {

            return status == (int)EncounterStatus.Discharged || status == (int)EncounterStatus.Checkout || status == (int)EncounterStatus.CheckoutExpress ||
                   status == (int)EncounterStatus.ExamRoom || status == (int)EncounterStatus.WaitingRoom || status == (int)EncounterStatus.Pending ||
                   status == (int)EncounterStatus.Questions;
        }

        #endregion

        #region CCD Msg

        //[TestMethod]
        public void TestCcdOutboundEncounterClinicalSummaryCda()
        {
            var patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);

            // Create a message and save it
            var messageTypeId = PracticeRepository.ExternalSystemExternalSystemMessageTypes.First(i => i.ExternalSystem.Name == OmedixExternalSystemName &&
                                                            i.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound);

            var message = new ExternalSystemMessage
            {
                CreatedBy = "Test",
                CreatedDateTime = DateTime.UtcNow,
                UpdatedDateTime = DateTime.UtcNow,
                Description = "EncounterClinicalSummaryCDA",
                ExternalSystemExternalSystemMessageTypeId = messageTypeId.Id,
                ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                Value = Ecs
                // CorrelationId = patient.Id.ToString()
            };

            message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
            {
                PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.Patient,
                PracticeRepositoryEntityKey = patient.Id.ToString()
            });
            PracticeRepository.Save(message);

            StartProcessorAndConnectors();

            var processedMessages = new List<ExternalSystemMessage>();

            // Process external message From PatientInsert trigger
            ExternalSystemMessage message1 = WaitForMessageProcessed();
            Assert.IsNotNull(message1);
            Assert.IsFalse(message1.Value.IsNullOrEmpty());
            processedMessages.Add(message1);

            Assert.IsNotNull(message1);
            Assert.IsFalse(message1.Value.IsNullOrEmpty());
        }

        private void StartProcessorAndConnectors()
        {
            _webConnector.Start();
            _messageProcessor.ProcessUnprocessedMessages();
        }

        /// <summary>
        ///   Waits for the first message to show as processed created after the specified time.
        /// </summary>
        /// <param name="after"> The after. </param>
        /// <param name="correlationId"> The correlation id. </param>
        /// <returns> </returns>
        /// <exception cref="System.Exception"></exception>
        private ExternalSystemMessage WaitForMessageProcessed(DateTime? after = null, string correlationId = null)
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                int count = 0;
                while (count < 150)
                {
                    IQueryable<ExternalSystemMessage> query =
                        PracticeRepository.ExternalSystemMessages.OrderByDescending(m => m.UpdatedDateTime).Where(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed);

                    if (after.HasValue) query = query.Where(m => m.UpdatedDateTime >= after);

                    if (correlationId != null) query = query.Where(i => i.CorrelationId == correlationId);

                    ExternalSystemMessage message = query.FirstOrDefault();

                    if (message != null)
                    {
                        Func<bool> hasUnprocessedMessages = () => PracticeRepository.ExternalSystemMessages.All(m => m.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed);
                        hasUnprocessedMessages.Wait(TimeSpan.FromSeconds(5));

                        Trace.WriteLine("Waited {0} seconds for message to be processed.".FormatWith(count * 0.3));
                        return message;
                    }
                    Thread.Sleep(300);
                    count += 1;
                }
            }
            throw new Exception("Failed to find processed message after {0}. ExternalSystemMessage table has {1} rows. Last row was created at {2}.".FormatWith(after, PracticeRepository.ExternalSystemMessages.Count(), PracticeRepository.ExternalSystemMessages.OrderByDescending(i => i.CreatedDateTime).Select(i => i.CreatedDateTime).FirstOrDefault()));
        }

        #endregion
    }
}

