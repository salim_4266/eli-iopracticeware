IF NOT EXISTS(SELECT * FROM PracticeInterfaceConfiguration WHERE FieldReference = '5010')
INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', '5010', 'T'


INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'ALLOWINTERNAL', 'T'

INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'GLASSON', 'T'


INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'NPIALL', 'T'

INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'NPIONLY', 'T'

INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'RPTPATPAY', 'F'

IF NOT EXISTS(SELECT * FROM PracticeInterfaceConfiguration WHERE FieldReference = 'ASC')
INSERT INTO dbo.PracticeInterfaceConfiguration
           ([Interface]
           ,[FieldReference]
           ,[FieldValue])
SELECT '', 'ASC', 'T'


------------------------------LOTS OF PRACTICECODETABLE ENTRIES--------------------------------------------------
--PracticeCode table
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'A Arrived'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'D Discharged'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,2
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'N No Show'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,4
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'O Office Cancel'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,5
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'P Pending'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,8
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'S Same Day Cancel'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,9
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'X Cancel'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,10
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'Y Patient Cancel'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,11
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'P Pending'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,6
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'F Left'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,3
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'APPOINTMENTSTATUS'
	,N'Q Special Cancel'
	,NULL
	,N'F'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,7
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'PS First Visit'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'Follow Up Visit - Contacts'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'First Visit'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'Post Op Visit'
	,N''
	,NULL
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'LASIK Consult'
	,N''
	,NULL
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'Lasik PO'
	,N''
	,NULL
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'Subsequent Visit'
	,N''
	,NULL
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'No Questions'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'QUESTIONSETS'
	,N'Follow Up Visit'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Chinese'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'English'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'French'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'German'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Italian'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Japanese'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Korean'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Other'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Polish'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Spanish'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'LANGUAGE'
	,N'Russian'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

--Ethnicity and Race
INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'Native Amer'
	,''
	,'F'
	,'American Indian or Alaska Native'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'Asian'
	,''
	,'F'
	,'Asian'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'Pacific Isl'
	,''
	,'F'
	,'Native Hawaiian or Other Pacific Islander'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'White'
	,''
	,'F'
	,'White'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'Other'
	,''
	,'F'
	,'Other'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'2 or More'
	,''
	,'F'
	,'Two or More'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'RACE'
	,'Black'
	,''
	,'F'
	,'Black or African American'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'ETHNICITY'
	,'Not Hisp'
	,''
	,'F'
	,'Not Hispanic or Latino'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)

INSERT INTO PRACTICECODETABLE (
	ReferenceType
	,Code
	,AlternateCode
	,Exclusion
	,LetterTranslation
	,OtherLtrTrans
	,FormatMask
	,Signature
	,TestOrder
	,FollowUp
	,Rank
	)
VALUES (
	'ETHNICITY'
	,'Hisp'
	,''
	,'F'
	,'Hispanic or Latino'
	,''
	,''
	,'F'
	,''
	,''
	,'1'
	)


--INSURERREFERENCECODES
IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'A')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'A - Self Pay', 'A', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'C')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'C - Medicaid', 'D', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'F')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'F - Federal Employees Program', 'J', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'I')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'I - HMO', 'I', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'K')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'K - Central Certification', 'K', 'F', '', '', '', 'F', '', '', '1')
	
IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'L')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'L - Self Administered', 'L', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'M')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'M - Medicare', 'C', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'N')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'N - Medicare NJ', 'C', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'P')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'P - Blue Cross', 'P', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'Q')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'Q - Managed Care', 'N', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'S')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'S - Champva', 'H', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'T')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'T - Title V', 'T', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'V')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'V - Veterans Administration Program', 'V', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'W')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'W - Workers Comp', 'B', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'X')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'X - Family or Friends', 'M', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'Y')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'Y - Other Federal Program', 'E', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'Z')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'Z - Other', 'Z', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = '[')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', '[ - Medicare PA', 'C', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'INSURERREF' and Substring(Code, 1, 1) = 'O')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERREF', 'O - Commercial Insurance', 'F', 'F', '', '', '', 'F', '', '', '1')


--TRANSMITTYPES
IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'TransmitType' and Substring(Code, 1, 1) = 'E')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('TRANSMITTYPE', 'E - MEDICARE : MEDICARE', '12902-BBB888', 'F', '', '', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'TransmitType' and Substring(Code, 1, 1) = 'I')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('TRANSMITTYPE', 'Y - GATEWAY : GATEWAYEDI', 'iRECEIVERID-iSUBMITTERID', 'F', 'gRECEIVERID-gSUBMITTERID', 'LoopRECEIVERID-LoopSUBMITTERID', '', 'F', '', '', '1')

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'TransmitType' and Substring(Code, 1, 1) = 'V')
INSERT [dbo].[PracticeCodeTable] ([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
	VALUES ('TRANSMITTYPE', 'V - WEBMD : WebMD','wRECEIVERID-wSUBMITTERID','F','','T','','F','','',0)

IF NOT EXISTS(SELECT * FROM PracticeCodeTable WHERE ReferenceType = 'TransmitType' and Substring(Code, 1, 1) = 'L')
INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('TRANSMITTYPE', 'L - MD OnLine', 'MDOL-123456123', 'F', '', '', '', 'F', '', '', '1')



-- PracticeCodeTable PaymentTypes
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Retraction Insurance - O'
	,N'D'
	,N'F'
	,N'INSRETRACT'
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Payment - P'
	,N'C'
	,N'F'
	,N'PYMT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Denial - D'
	,N''
	,N'F'
	,N'DENL'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Contractual Adjustment - X'
	,N'C'
	,N'F'
	,N'CTRWO'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Proffl Courtesy - 9'
	,N'C'
	,N'F'
	,N'PROFCT'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pymt Incl w Other Svc - 7'
	,N'C'
	,N'F'
	,N'PDOTHSVC'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Void - V'
	,NULL
	,N'F'
	,N'VOID'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Ins Billing Err - I'
	,N'D'
	,N'F'
	,N'REFINSERR'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Ins Pat Cov Termd - L'
	,N'D'
	,N'F'
	,N'REFINSTERM'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Duplicate Clm - *'
	,N''
	,N'F'
	,N'DUPECLM'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Appeal Closed - %'
	,N''
	,N'F'
	,N'APCLSD'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Appeal - @'
	,N''
	,N'F'
	,N'APPL'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Denial Closed - &'
	,N''
	,N'F'
	,N'DNLCLSD'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Sent to Pt - 0'
	,N''
	,N'F'
	,N'SENTPT'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Bad Debt - #'
	,N'C'
	,N'F'
	,N'BADDEBT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Bankruptcy Ins - $'
	,N'C'
	,N'F'
	,N'BKRPTINS'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Bankruptcy Pat - ^'
	,N'C'
	,N'F'
	,N'BKRPTPAT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Erroneous Charge - ('
	,N'C'
	,N'F'
	,N'ERROR'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'General Writeoff - +'
	,N'C'
	,N'F'
	,N'GENWO'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ins Settlement - 1'
	,N'C'
	,N'F'
	,N'INSSET'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Coinsurance - )'
	,N''
	,N'F'
	,N'COINS'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pat Courtesy - 4'
	,N'C'
	,N'F'
	,N'PATCT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'MD Request - 2'
	,N'C'
	,N'F'
	,N'MDREQ'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pat Deceased - 5'
	,N'C'
	,N'F'
	,N'DECEAS'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pat Finan Difficulty - 6'
	,N'C'
	,N'F'
	,N'FINDIF'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pt Resp Other - ?'
	,N''
	,N'F'
	,N'PTRESP'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Pt Copay - |'
	,N''
	,N'F'
	,N'COINS'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Ins Recoupmnt - J'
	,N'D'
	,N'F'
	,N'REFINSREC'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Ins Ovrpymnt - K'
	,N'D'
	,N'F'
	,N'REFINSOVPY'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Medicare Audit - M'
	,N'D'
	,N'F'
	,N'REFAUDIT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Ref Pat Overpymnt - N'
	,N'D'
	,N'F'
	,N'REFPAT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Refund Unspec - R'
	,N'D'
	,N'F'
	,N'REFUND'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Retd Check - S'
	,N'D'
	,N'F'
	,N'RTDCK'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Retd Ck Fee - T'
	,N'D'
	,N'F'
	,N'RETCK'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Reversal - U'
	,N'D'
	,N'F'
	,N'RVSL'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Deductible - !'
	,NULL
	,N'F'
	,N'DEDUCT'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTTYPE'
	,N'Adj Up; Wrong Svc - H'
	,N'D'
	,N'F'
	,N'ADJUP'
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Cash - C'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Check - K'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Debit Card - Z'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Visa Card - V'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'American Express - A'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Discover - D'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Endorsed Ins Check - I'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'MasterCard - M'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Money Order - O'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYABLETYPE'
	,N'Other Credit Card - R'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,NULL
	,0
	)

-- Type of service, used for the PatientReceivableServices table
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'01 - Medical Care'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'02 - Surgery'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'03 - Consultation'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'04 - Diagnostic X-Ray'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'05 - Diagnostic Lab'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'06 - Radiation Therapy'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'07 - Anesthesia'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'08 - Assist at Surgery'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'09 - Other Medical Services'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'10 - Pathology'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'11 - Used DME'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'12 - DME purchase'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'13 - Amb Surg Ctr/ASC'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'14 - Renal supplies in the home'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'15 - Alt Pmt-Dialysis'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'16 - CRD equipment'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'17 - Pre admission testing'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'18 - DME rental'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'19 - Pneumococcal Vaccine'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'20 - 2nd Opin-Elec Srgy'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'21 - 3rd Opin-Elec Srgy'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'22 - Whl Bld/Pkd Red Cell'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'TYPEOFSERVICE'
	,N'99 - Other'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

-- PaymentReason used for servicepayments
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTREASON'
	,N'1'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PAYMENTREASON'
	,N'2'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

-- ServiceModifiers
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICEMODS'
	,N'RT - Right Eye'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICEMODS'
	,N'LT - Left Eye'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICEMODS'
	,N'59 - Distinct Procedural Service'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)



-- ServiceCategories
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICECATEGORY'
	,N'MISC'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICECATEGORY'
	,N'DRUGS AND SUPPLIES'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICECATEGORY'
	,N'DIAGNOSTIC TESTS'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICECATEGORY'
	,N'SURGERY'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SERVICECATEGORY'
	,N'OFFICE VISITS'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)


-- Specialty types
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Optometrist'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Pediatric Ophthalmologist'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Cardiologist'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Family Practitioner'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Internist'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Other MD'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Otolaryngologist'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Technical Consultant'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Ophthalmologist'
	,N'1'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Pediatrician'
	,N'1'
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Retinal Specialist'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'SPECIALTY'
	,N'Glaucoma Specialist'
	,N''
	,N'F'
	,N''
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

-- Businesss classes
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'BLUES'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'MCAIDNJ'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'COMM'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'MCARENJ'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'AMERIH'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'MCAIDNY'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'BUSINESSCLASS'
	,N'MCARENY'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,0
	)

-- Plan Types
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLANTYPE'
	,N'HMO'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLANTYPE'
	,N'COMMERC'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLANTYPE'
	,N'Other'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLANTYPE'
	,N'POS'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLANTYPE'
	,N'PPO'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

-- Employment types
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'EMPLOYEESTATUS'
	,N'Employed'
	,N''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'EMPLOYEESTATUS'
	,N'Full Time Student'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'EMPLOYEESTATUS'
	,N'Other'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'EMPLOYEESTATUS'
	,N'Part Time Student'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'EMPLOYEESTATUS'
	,N'Retired'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

-- Patient Referalls
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Hospital'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Internet'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Newspaper'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Optometrist'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Patient'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'PCP'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'REFERBY'
	,N'Phone Book'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

-- patient types
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PATIENTTYPE'
	,N'E - Electronic Chart'
	,N''
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,NULL
	,NULL
	,0
	)

-- Phone Type
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Home Alt Add 1'
	,N'HP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Home Alt Add 2'
	,N'HP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Work Alt Add 1'
	,N'WP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Work Alt Add 2'
	,N'WP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Cell Alt Add 1'
	,N'CP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Home'
	,N'HP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Work'
	,N'WP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Cell'
	,N'CP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Fax'
	,N'FX'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Beeper'
	,N'BN'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Night'
	,N'NP'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPE'
	,N'Primary Tel'
	,N'TE'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)


INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPEUSER'
	,N'Primary'
	,N'TE'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)
	

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PHONETYPEUSER'
	,N'Fax'
	,N'TE'
	,N'F'
	,N''
	,N''
	,N''
	,N'F'
	,N''
	,N''
	,1
	)	

-- ServiceLocationCodes
INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'31-Skilled Nursing Facility'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'32-Nursing Facility'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'33-Custodial Care Facility'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'34-Hospice'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'41-Ambulance - land'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'42-Ambulance Air or Water'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'50-Federal Qualified Health Ctr'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'51-Inpatient Psychiatric Fac'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'52-Psychiatric Facility'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'53-Community Med Hlth Ctr'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'54-Int Care Fac/Mental Retarded'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'55-Res Substance Abuse Ctr'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'56-Psychiatric Res Trmt Ctr'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'61-Comp Rehan Inpatient Fac'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'62-Comp Rehab Outpatient Fac'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'65-End Stage Renal Treatment Ctr'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'71-State or Local Health Clinic'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'72-Rural Health Clinic'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'81-Independent Lab'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'99-Other Unlisted Facility'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'11-Office'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'12-Home'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'21-Hospital Inpatient'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'22-Hospital Outpatient'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'23-Hospital Emergency Room'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'24-Ambulatory Surgical Center'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'25-Birthing Center'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)

INSERT [dbo].[PracticeCodeTable] (
	[ReferenceType]
	,[Code]
	,[AlternateCode]
	,[Exclusion]
	,[LetterTranslation]
	,[OtherLtrTrans]
	,[FormatMask]
	,[Signature]
	,[TestOrder]
	,[FollowUp]
	,[Rank]
	)
VALUES (
	N'PLACEOFSERVICE'
	,N'26-Military Treatment Center'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,0
	)


INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MARITAL', 'Single', '297', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('GENDER', 'Female', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('RELATIONSHIP', 'Yourself', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('RELATIONSHIP', 'Spouse', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('INSURERPTYPE', 'IP - Individual Policy', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('CHIEFCOMPLAINTS', 'Flashes', '', 'F', '', '', '', 'F', '', '', '1')


INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '12 - Working Aged', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '13 - ESRD', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '14 - Auto/No Fault', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '15 - Workers� comp', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '41 - Black Lung', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '42 - VA', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '43 - Disability', '', 'F', '', '', '', 'F', '', '', '1')

INSERT INTO [PracticeCodeTable]([ReferenceType],[Code],[AlternateCode],[Exclusion],[LetterTranslation],[OtherLtrTrans],[FormatMask],[Signature],[TestOrder],[FollowUp],[Rank])
     VALUES ('MEDICARESECONDARY', '47 - Liability', '', 'F', '', '', '', 'F', '', '', '1')



---------------------END PRACTICECODETABLE------------------------

--add PracticeServices

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC])
VALUES (
	'66984'
	,'P'
	,'SURGERY'
	,'CATARACT SX'
	,'1000.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,'')



INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC])
VALUES (
	'66982'
	,'P'
	,'SURGERY'
	,'CATARACT SURG COMPLEX'
	,'1000.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,'')

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'67036'
	,'P'
	,'SURGERY'
	,'VITRECT'
	,'1200.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'67028'
	,'P'
	,'SURGERY'
	,'Intravitreal Injection'
	,'1000.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92133'
	,'P'
	,'DIAGNOSTIC TESTS'
	,'OCT of Optic Nerve'
	,'500.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'T'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92250'
	,'P'
	,'DIAGNOSTIC TESTS'
	,'Fundus Photos'
	,'175.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'T'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92235'
	,'P'
	,'DIAGNOSTIC TESTS'
	,'Fluorescein Angio'
	,'100.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'T'
	,'F'
	,''
	,''
	,''
	)
	
INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92083'
	,'P'
	,'DIAGNOSTIC TESTS'
	,'Visual Field'
	,'120.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'T'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'99214'
	,'P'
	,'OFFICE VISITS'
	,'Comprehensive Office Visit'
	,'155.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'99204'
	,'P'
	,'OFFICE VISITS'
	,'Comprehensive Office Visit'
	,'255.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'99203'
	,'P'
	,'OFFICE VISITS'
	,'Mid Office Visit'
	,'100.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'99244'
	,'P'
	,'CONSULTATION'
	,'Comprehensive Office Visit'
	,'155.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'T'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92014'
	,'P'
	,'OFFICE VISITS'
	,'Comprehensive Office Visit'
	,'155.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'92015'
	,'P'
	,'DIAGNOSTIC TESTS'
	,'REFRACTION'
	,'100.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'J3490'
	,'P'
	,'DRUGS AND SUPPLIES'
	,'Lucentis'
	,'2800.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,'HI VINESH THIS IS A DRUG NOTE'
	,''
	,'50242006001'
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'J2778'
	,'P'
	,'DRUGS AND SUPPLIES'
	,'Lucentis'
	,'1800.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,'61755000502'
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'25999'
	,'P'
	,'MYSTERY PROC'
	,'WHO KNOWS'
	,'2800.00'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,'HI VINESH THIS IS AN NOS NOTE'
	,''
	,''
	)

	
INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'V2784'
	,'P'
	,'DISPENSING'
	,'High Index 1.6'
	,'40'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'V2715A'
	,'P'
	,'DISPENSING'
	,'PRISM, PER LENS'
	,'65'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'V2750F'
	,'P'
	,'DISPENSING'
	,'ANTIREFLECT COATING'
	,'25'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)

INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'V2781AG'
	,'P'
	,'DISPENSING'
	,'THREE RIVERS DISCOVERY'
	,'225'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'V2020'
	,'P'
	,'DISPENSING'
	,'FRAMES'
	,'250'
	,''
	,''
	,'20000101'
	,''
	,''
	,'F'
	,'F'
	,''
	,''
	,''
	)


	
--add CustomDiagnosis PracticeServices
INSERT INTO [PracticeServices] (
	[Code]
	,[CodeType]
	,[CodeCategory]
	,[Description]
	,[Fee]
	,[TOS]
	,[POS]
	,[StartDate]
	,[EndDate]
	,[RelativeValueUnit]
	,[OrderDoc]
	,[ConsultOn]
	,[ClaimNote]
	,[ServiceFilter]
	,[NDC]
	)
VALUES (
	'001'
	,'D'
	,'NO DIAGNOSIS'
	,'NO DIAGNOSIS'
	,'0'
	,''
	,''
	,'20000101'
	,''
	,''
	,'T'
	,'F'
	,''
	,''
	,''
	)


INSERT INTO [dbo].[ModifierRules]
           ([BusinessClassId]
           ,[CPT]
           ,[Rule1]
           ,[Description1]
           ,[Rule2]
           ,[Description2]
           ,[Rule3]
           ,[Description3]
           ,[Rule4]
           ,[Description4]
           ,[Rule5]
           ,[Description5]
           ,[Rule6]
           ,[Description6]
           ,[Rule7]
           ,[Description7]
           ,[Rule8]
           ,[Description8]
           ,[Rule9]
           ,[Description9]
           ,[Rule10]
           ,[Description10]
           ,[Rule11]
           ,[Description11]
           ,[Rule12]
           ,[Description12]
           ,[Rule13]
           ,[Description13]
           ,[Rule14]
           ,[Description14]
           ,[Rule15]
           ,[Description15]
           ,[Status])
VALUES ('COMM', '66984', 'RT', 'OD', 'LT', 'OS', '', 'OU', '3', 'OU', '79', 'T90', 0, 'Wait', 9, 'Max', '', 'UL', '', 'LL', '', 'UR', '', 'LR', '', 'LIDSAGG', '', 'SAMEDAYSX', '', 'LIFELIMIT', '', '', 'A')


INSERT INTO PCInventory
           ([SKU]
           ,[Type_]
           ,[Manufacturer]
           ,[Model]
           ,[Color]
           ,[PCSize]
           ,[LensCoating]
           ,[LensTint]
           ,[PDD]
           ,[PDN]
           ,[CEGHeight]
           ,[Qty]
           ,[Cost]
           ,[WCost])
SELECT '805289111849', 'Ray-Ban RX', 'Luxottica', 'RX 5082' , 'Black', '0', '', '', '', '', '', 0, 220, 165

---------------BEGIN PRACTICE SPECIFIC DATA------------------



IF NOT EXISTS (SELECT * FROM PracticeName WHERE PracticeType = 'P' and LocationReference = '')
--Add Location 1 - MAIN OFFICE
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
VALUES	(
	'P'
	,'George R. Reiss, M.D., PC'
	,'6677 W. Thunderbird Rd'
	,''
	,'Glendale'
	,'AZ'
	,'100010003'
	,'2123235623'
	,''
	,'2124567890'
	,'PracticeEmail@Email.com'
	,'132334567'
	,'SITEID'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'1005'
	,1
	,''
	,'T'
	,'CLIACERTNUMBER'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'1992702377'
	,'207W00000X'
	)


--2D LOCATION
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
VALUES (
	'P'
	,'McCormick Eye Center'
	,'10619 N. Hayden Rd.'
	,''
	,'Scottsdale'
	,'AZ'
	,'100011458'
	,'2123235623'
	,''
	,'2124567890'
	,'Practice2Email@Email.com'
	,'132334567'
	,''
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'1225'
	,1
	,'MCCORMICK'
	,'T'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'1366455339'
	,'207W00000X'
	)

--3d Location, No email		   
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
VALUES (
	'P'
	,'VISION WELLNESS CENTER'
	,'10750 W. MCDOWELL RD.'
	,''
	,'AVONDALE'
	,'AZ'
	,'782589874'
	,'2103480265'
	,''
	,'2124567890'
	,''
	,'132334567'
	,''
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'1225'
	,1
	,'TLC'
	,'T'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'2589632589'
	,'207W00000X'
	)



--PayTo Address
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
VALUES (
	'P'
	,'GEORGE R. REISS, M.D., PC'
	,'PO BOX 15639'
	,''
	,'SCOTTSDALE'
	,'AZ'
	,'100010005'
	,'2123235623'
	,''
	,'2124567890'
	,'PracticeEmail@Email.com'
	,'132334567'
	,''
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,''
	,1
	,'PAYTO'
	,'T'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'1962427955'
	,'207W00000X'
	)



--4th practice LINKED TO PAYTO ADDRESS
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
SELECT
	'P'
	,'ROBERT M. MORGENSTERN, M.D.'
	,'6677 W. THUNDERBIRD ROAD'
	,''
	,'GLENDALE'
	,'AZ'
	,'100010003'
	,'2123235623'
	,''
	,'2124567890'
	,'PracticeEmail@Email.com'
	,'132334568'
	,'SITEID2'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,''
	,1
	,'MORGENSTERN'
	,'T'
	,''
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,'1962427955'
	,'207W00000X'
FROM PracticeName 
WHERE LocationReference = 'PAYTO'




--5TH practice OPTICAL SHOP
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
SELECT
	'P'
	,'SUZANNE BARTELS OPTICAL SHOP'
	,'6677 W. THUNDERBIRD ROAD'
	,''
	,'GLENDALE'
	,'AZ'
	,'100010003'
	,'2123235623'
	,''
	,'2124567890'
	,'PracticeEmail@Email.com'
	,'333-66-5555'
	,''
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,''
	,1
	,'OPTICAL'
	,'T'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'1093844532'
	,'332H00000X'



--6TH practice INTERNAL FACILITY
INSERT INTO [PracticeName] (
	[PracticeType]
	,[PracticeName]
	,[PracticeAddress]
	,[PracticeSuite]
	,[PracticeCity]
	,[PracticeState]
	,[PracticeZip]
	,[PracticePhone]
	,[PracticeOtherPhone]
	,[PracticeFax]
	,[PracticeEmail]
	,[PracticeTaxId]
	,[PracticeOtherId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[LoginType]
	,[LocationReference]
	,[BillingOffice]
	,[CatArray1]
	,[CatArray2]
	,[CatArray3]
	,[CatArray4]
	,[CatArray5]
	,[CatArray6]
	,[CatArray7]
	,[CatArray8]
	,[PracticeNPI]
	,[PracticeTaxonomy]
	)
VALUES (
	'P'
	,'INSIDE AMBULATORY SURGERY CENTER'
	,'6677 W. THUNDERBIRD ROAD'
	,'LOWER LOBBY'
	,'GLENDALE'
	,'AZ'
	,'100010003'
	,'2123235625'
	,''
	,'2124567899'
	,''
	,'987654321'
	,''
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,'07:00 AM'
	,'09:00 PM'
	,''
	,1
	,'ASC'
	,'T'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'1841341039'
	,'261QA1903X'
	)


--Add Doctors
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'REISS'
	,'D'
	,'GEORGE R. REISS, M.D.'
	,'9911'
	,'11111111111111101111111110000000111111111111111111111110000000111111100000000000000000000000000000011111111111000000000000000000'
	,12615680
	,4
	,0
	,'OPHTHALMOLOGIST'
	,''
	,'345 FIFTH AVENUE'
	,''
	,'NEW YORK'
	,'NY'
	,'11201'
	,'8012992584'
	,'Doc1Email@Email.com'
	,''
	,''
	,'AM8971940'
	,'345245452'
	,''
	,15
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'LP'
	,'207W00000X'
	,'A'
	,'REISS'
	,'GEORGE'
	,''
	,''
	,'Y'
	,'N'
	,'1982679841'
	,'R'
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = ''

--Add UserSchedule 1
INSERT INTO [PracticeCalendar] ([CalendarResourceId],[CalendarDate],[CalendarStartTime],[CalendarEndTime],[CalendarLoc1],[CalendarLoc2],[CalendarLoc3],[CalendarLoc4] ,[CalendarLoc5],[CalendarLoc6],[CalendarLoc7],[CalendarPurpose],[CalendarComment])
      SELECT IDENT_CURRENT('Resources'), convert(varchar(8),getdate(),112), '08:00 AM01:00 PM', '12:00 PM05:00 PM',  PracticeId,  PracticeId, '-1', '-1',  '-1',  '-1', '-1', '', '' 
	  FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = ''

--2nd dr: No email, phone
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'BATISTE'
	,'D'
	,'COREY BATISTE, M.D.'
	,'1111'
	,'11111111111111101111111110000000111111111111111111111110000000111111100000000000000000000000000000011111111111000000000000000000'
	,12615680
	,4
	,0
	,'OPHTHALMOLOGIST'
	,'111-22-3333'
	,'345 FIFTH AVENUE'
	,''
	,'NEW YORK'
	,'NY'
	,'112010123'
	,''
	,''
	,''
	,''
	,'AM8971940'
	,'345245452'
	,''
	,15
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'LP'
	,'207W00000X'
	,'A'
	,'BATISTE'
	,'COREY'
	,''
	,''
	,'Y'
	,'N'
	,'1235180696'
	,'Y'
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = ''


--Add UserSchedule 2
INSERT INTO [PracticeCalendar] ([CalendarResourceId],[CalendarDate],[CalendarStartTime],[CalendarEndTime],[CalendarLoc1],[CalendarLoc2],[CalendarLoc3],[CalendarLoc4] ,[CalendarLoc5],[CalendarLoc6],[CalendarLoc7],[CalendarPurpose],[CalendarComment])
SELECT IDENT_CURRENT('Resources'), convert(varchar(8),getdate(),112), '08:00 AM01:00 PM', '12:00 PM05:00 PM',  PracticeId,  PracticeId, '-1', '-1',  '-1',  '-1', '-1', '', ''
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = ''



----3rd doctor
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'SOO LIN'
	,'D'
	,'GEORGE SOO LIN, M.D.'
	,'3333'
	,'11111111111111101111111110000000111111111111111111111110000000111111100000000000000000000000000000011111111111000000000000000000'
	,12615680
	,4
	,0
	,'OPHTHALMOLOGIST'
	,''
	,'5200 BURLINGTON AVENUE'
	,''
	,'NORTHFIELD'
	,'NJ'
	,'07040'
	,''
	,''
	,''
	,''
	,'ZZSS1940'
	,'567245452'
	,''
	,15
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'LP'
	,'207W00000X'
	,'A'
	,'SOO LIN'
	,'GEORGE'
	,''
	,''
	,'Y'
	,'N'
	,'1629374921'
	,''
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = 'TLC'



----4TH doctor for PayTo address
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'MORGENSTERN'
	,'D'
	,'ROBERT M. MORGENSTERN, M.D.'
	,'4444'
	,'11111111111111101111111110000000111111111111111111111110000000111111100000000000000000000000000000011111111111000000000000000000'
	,12615680
	,4
	,0
	,'OPHTHALMOLOGIST'
	,''
	,'5200 BURLINGTON AVENUE'
	,''
	,'NORTHFIELD'
	,'NJ'
	,'07040'
	,''
	,''
	,''
	,''
	,'ZZSS1940'
	,'567245452'
	,''
	,15
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'LP'
	,'207W00000X'
	,'A'
	,'MORGENSTERN'
	,'ROBERT'
	,''
	,''
	,'Y'
	,'N'
	,'1588728158'
	,'M'
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = 'MORGENSTERN'



----5th doctor - Q resourcetype - optical supplier
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'BARTELS'
	,'Q'
	,'SUZANNE BARTELS'
	,'5555'
	,'11111111111111101111111110000000111111111111111111111110000000111111100000000000000000000000000000011111111111000000000000000000'
	,12615680
	,4
	,0
	,'OPTICIAN'
	,''
	,'1456 ROUTE 17'
	,''
	,'REGO PARK'
	,'NJ'
	,'07040'
	,''
	,''
	,''
	,''
	,'ZZSS1940'
	,'567245466'
	,''
	,15
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'LP'
	,'332H00000X'
	,'A'
	,'BARTELS'
	,'SUZANNE'
	,''
	,''
	,'Y'
	,'N'
	,'1093844532'
	,''
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
	AND LocationReference = 'OPTICAL'



--External Facility
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'BROOK'
	,'R'
	,'BROOK PLAZA SURGERY CENTER'
	,''
	,''
	,0
	,0
	,0
	,''
	,''
	,'345 FIFTH AVENUE'
	,''
	,'NEW YORK'
	,'NY'
	,'112011112'
	,'2125896325'
	,''
	,'02'
	,''
	,''
	,''
	,''
	,0
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,''
	,''
	,'24'
	,'Y'
	,'Y'
	,'1841341039'
	,''
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
AND LocationReference = ''


--Internal Facility
INSERT INTO [Resources] (
	[ResourceName]
	,[ResourceType]
	,[ResourceDescription]
	,[ResourcePid]
	,[ResourcePermissions]
	,[ResourceColor]
	,[ResourceOverLoad]
	,[ResourceCost]
	,[ResourceSpecialty]
	,[ResourceTaxId]
	,[ResourceAddress]
	,[ResourceSuite]
	,[ResourceCity]
	,[ResourceState]
	,[ResourceZip]
	,[ResourcePhone]
	,[ResourceEmail]
	,[ServiceCode]
	,[ResourceSSN]
	,[ResourceDEA]
	,[ResourceLicence]
	,[ResourceUPIN]
	,[ResourceScheduleTemplate1]
	,[ResourceScheduleTemplate2]
	,[ResourceScheduleTemplate3]
	,[PracticeId]
	,[StartTime1]
	,[EndTime1]
	,[StartTime2]
	,[EndTime2]
	,[StartTime3]
	,[EndTime3]
	,[StartTime4]
	,[EndTime4]
	,[StartTime5]
	,[EndTime5]
	,[StartTime6]
	,[EndTime6]
	,[StartTime7]
	,[EndTime7]
	,[Vacation]
	,[Status]
	,[ResourceLastName]
	,[ResourceFirstName]
	,[SignatureFile]
	,[PlaceOfService]
	,[Billable]
	,[GoDirect]
	,[NPI]
	,[ResourceMI]
	,[ResourceSuffix]
	,[ResourcePrefix]
	,[IsLoggedIn]
	)
SELECT 'SXCTR'
	,'R'
	,'INSIDE ASC'
	,''
	,''
	,0
	,0
	,0
	,''
	,''
	,'6677 W. THUNDERBIRD ROAD'
	,'LOWR LOBBY'
	,'GLENDALE'
	,'AZ'
	,'100010003'
	,'2123235625'
	,''
	,'02'
	,''
	,''
	,''
	,''
	,0
	,0
	,0
	,PracticeId
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'261QA1903X'
	,'A'
	,''
	,''
	,''
	,'24'
	,'Y'
	,'Y'
	,'1841341039'
	,''
	,''
	,''
	,0
FROM PracticeName
WHERE PracticeType = 'P'
AND LocationReference = 'ASC'

--add referring doctor
INSERT INTO [PracticeVendors] (
	[VendorName]
	,[VendorType]
	,[VendorAddress]
	,[VendorSuite]
	,[VendorCity]
	,[VendorState]
	,[VendorZip]
	,[VendorPhone]
	,[VendorFax]
	,[VendorEmail]
	,[VendorTaxId]
	,[VendorUPIN]
	,[VendorSpecialty]
	,[VendorOtherOffice1]
	,[VendorOtherOffice2]
	,[VendorLastName]
	,[VendorFirstName]
	,[VendorMI]
	,[VendorTitle]
	,[VendorSalutation]
	,[VendorFirmName]
	,[VendorTaxonomy]
	,[VendorExcRefDr]
	,[VendorNPI]
	,[VendorCellPhone]
	,[VendorSpecOther]
	)
VALUES (
	'STEPHEN COX, M.D.'
	,'D'
	,'10345 WATERSIDE PLAZA'
	,'SUITE 4G'
	,'BROOKLYN'
	,'NY'
	,'11201'
	,'2124467890'
	,'7188969635'
	,'kbradford@iopracticeware.com'
	,''
	,''
	,'INTERNIST'
	,''
	,''
	,'COX'
	,'STEPHEN'
	,''
	,''
	,'STEVE'
	,''
	,''
	,'F'
	,'1831408194'
	,''
	,''
	)

-- PracticeVendors without first name
INSERT [dbo].[PracticeVendors] ([VendorName], [VendorType], [VendorAddress], [VendorSuite], [VendorCity], [VendorState], [VendorZip], [VendorPhone], [VendorFax], [VendorEmail], [VendorTaxId], [VendorUPIN], [VendorSpecialty], [VendorOtherOffice1], [VendorOtherOffice2], [VendorLastName], [VendorFirstName], [VendorMI], [VendorTitle], [VendorSalutation], [VendorFirmName], [VendorTaxonomy], [VendorExcRefDr], [VendorNPI], [VendorCellPhone], [VendorSpecOther]) 
VALUES (N'VICTOR ABDY, M.D.', N'D', N'153 UNION BOULEVARD', N'', N'TOTOWA', N'NJ', N'07511'
	, N'(973)790-3232', N'(973)942-8848', N'', N'', N'C55114', N'', N'', N''
	, N'ABDY', N'VICTOR', N'A', N'', N'DR. ABDY', N'Test your firm', N'', N'F'
	, N'1164446795', NULL, NULL)



	-- PracticeVendors External Organization
INSERT [dbo].[PracticeVendors] ([VendorName], [VendorType], [VendorAddress], [VendorSuite], [VendorCity], [VendorState], [VendorZip], [VendorPhone], [VendorFax], [VendorEmail], [VendorTaxId], [VendorUPIN], [VendorSpecialty], [VendorOtherOffice1], [VendorOtherOffice2], [VendorLastName], [VendorFirstName], [VendorMI], [VendorTitle], [VendorSalutation], [VendorFirmName], [VendorTaxonomy], [VendorExcRefDr], [VendorNPI], [VendorCellPhone], [VendorSpecOther]) 
VALUES (N'', N'L', N'153 UNION BOULEVARD', N'', N'TOTOWA', N'NJ', N'07511'
	, N'(973)790-3232', N'(973)942-8848', N'', N'', N'', N'', N'', N''
	, N'', N'', N'', N'', N'', N'Test your firm', N'', N'F'
	, N'', NULL, NULL)



--add referring doctor for vision appointment
INSERT INTO [PracticeVendors] (
	[VendorName]
	,[VendorType]
	,[VendorAddress]
	,[VendorSuite]
	,[VendorCity]
	,[VendorState]
	,[VendorZip]
	,[VendorPhone]
	,[VendorFax]
	,[VendorEmail]
	,[VendorTaxId]
	,[VendorUPIN]
	,[VendorSpecialty]
	,[VendorOtherOffice1]
	,[VendorOtherOffice2]
	,[VendorLastName]
	,[VendorFirstName]
	,[VendorMI]
	,[VendorTitle]
	,[VendorSalutation]
	,[VendorFirmName]
	,[VendorTaxonomy]
	,[VendorExcRefDr]
	,[VendorNPI]
	,[VendorCellPhone]
	,[VendorSpecOther]
	)
VALUES (
	'SUZANNE BARTELS, O.D.'
	,'D'
	,'1456 ROUTE 17'
	,''
	,'REGO PARK'
	,'NJ'
	,'07040'
	,''
	,''
	,''
	,''
	,''
	,'OPTOMETRIST'
	,''
	,''
	,'BARTELS'
	,'SUZANNE'
	,''
	,''
	,'SUZIE'
	,''
	,''
	,'F'
	,'1093844532'
	,''
	,''
	)


--add referring doctor that is Excluded From Claims
INSERT INTO [PracticeVendors] (
	[VendorName]
	,[VendorType]
	,[VendorAddress]
	,[VendorSuite]
	,[VendorCity]
	,[VendorState]
	,[VendorZip]
	,[VendorPhone]
	,[VendorFax]
	,[VendorEmail]
	,[VendorTaxId]
	,[VendorUPIN]
	,[VendorSpecialty]
	,[VendorOtherOffice1]
	,[VendorOtherOffice2]
	,[VendorLastName]
	,[VendorFirstName]
	,[VendorMI]
	,[VendorTitle]
	,[VendorSalutation]
	,[VendorFirmName]
	,[VendorTaxonomy]
	,[VendorExcRefDr]
	,[VendorNPI]
	,[VendorCellPhone]
	,[VendorSpecOther]
	)
VALUES (
	'SELF REFERRAL'
	,'D'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'REFERRAL'
	,'SELF'
	,''
	,''
	,''
	,''
	,''
	,'T'
	,'1111111111'
	,''
	,''
	)

-- AppointmentType		   
INSERT INTO [AppointmentType] (
	[AppointmentType]
	,[Question]
	,[Duration]
	,[DurationOther]
	,[Recursion]
	,[ResourceId1]
	,[ResourceId2]
	,[ResourceId3]
	,[ResourceId4]
	,[ResourceId5]
	,[ResourceId6]
	,[ResourceId7]
	,[ResourceId8]
	,[DrRequired]
	,[PcRequired]
	,[Status]
	,[Period]
	,[Rank]
	,[OtherRank]
	,[RecallFreq]
	,[RecallMax]
	,[Category]
	,[FirstVisitIndicator]
	,[AvailableInPlan]
	,[EMBasis]
	)
VALUES (
	'BRF'
	,'Follow Up Visit'
	,5
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,8454143
	,0
	,0
	,'Y'
	,'N'
	,'A'
	,''
	,'1'
	,'1'
	,'1'
	,'3'
	,'S'
	,'N'
	,'T'
	,'99201'
	)

--add 2d appointment type
INSERT INTO [AppointmentType] (
	[AppointmentType]
	,[Question]
	,[Duration]
	,[DurationOther]
	,[Recursion]
	,[ResourceId1]
	,[ResourceId2]
	,[ResourceId3]
	,[ResourceId4]
	,[ResourceId5]
	,[ResourceId6]
	,[ResourceId7]
	,[ResourceId8]
	,[DrRequired]
	,[PcRequired]
	,[Status]
	,[Period]
	,[Rank]
	,[OtherRank]
	,[RecallFreq]
	,[RecallMax]
	,[Category]
	,[FirstVisitIndicator]
	,[AvailableInPlan]
	,[EMBasis]
	)
VALUES (
	'SRG'
	,'Follow Up Visit'
	,5
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,8454143
	,0
	,0
	,'Y'
	,'N'
	,'A'
	,''
	,'1'
	,'1'
	,'1'
	,'3'
	,'S'
	,'N'
	,'T'
	,'99201'
	)

--add 3d appointment type
INSERT INTO [AppointmentType] (
	[AppointmentType]
	,[Question]
	,[Duration]
	,[DurationOther]
	,[Recursion]
	,[ResourceId1]
	,[ResourceId2]
	,[ResourceId3]
	,[ResourceId4]
	,[ResourceId5]
	,[ResourceId6]
	,[ResourceId7]
	,[ResourceId8]
	,[DrRequired]
	,[PcRequired]
	,[Status]
	,[Period]
	,[Rank]
	,[OtherRank]
	,[RecallFreq]
	,[RecallMax]
	,[Category]
	,[FirstVisitIndicator]
	,[AvailableInPlan]
	,[EMBasis]
	)
VALUES (
	'LONG'
	,'Follow Up Visit'
	,5
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,8454143
	,0
	,0
	,'Y'
	,'N'
	,'A'
	,''
	,'1'
	,'1'
	,'1'
	,'3'
	,'S'
	,'N'
	,'T'
	,'99201'
	)

--add appointment type 4
INSERT INTO [AppointmentType] (
	[AppointmentType]
	,[Question]
	,[Duration]
	,[DurationOther]
	,[Recursion]
	,[ResourceId1]
	,[ResourceId2]
	,[ResourceId3]
	,[ResourceId4]
	,[ResourceId5]
	,[ResourceId6]
	,[ResourceId7]
	,[ResourceId8]
	,[DrRequired]
	,[PcRequired]
	,[Status]
	,[Period]
	,[Rank]
	,[OtherRank]
	,[RecallFreq]
	,[RecallMax]
	,[Category]
	,[FirstVisitIndicator]
	,[AvailableInPlan]
	,[EMBasis]
	)
VALUES (
	'NPG'
	,'Follow Up Visit'
	,5
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,8454143
	,0
	,0
	,'Y'
	,'N'
	,'A'
	,''
	,'1'
	,'1'
	,'1'
	,'3'
	,'S'
	,'N'
	,'T'
	,'99201'
	)

INSERT INTO [AppointmentType] (
	[AppointmentType]
	,[Question]
	,[Duration]
	,[DurationOther]
	,[Recursion]
	,[ResourceId1]
	,[ResourceId2]
	,[ResourceId3]
	,[ResourceId4]
	,[ResourceId5]
	,[ResourceId6]
	,[ResourceId7]
	,[ResourceId8]
	,[DrRequired]
	,[PcRequired]
	,[Status]
	,[Period]
	,[Rank]
	,[OtherRank]
	,[RecallFreq]
	,[RecallMax]
	,[Category]
	,[FirstVisitIndicator]
	,[AvailableInPlan]
	,[EMBasis]
	)
VALUES (
	'SURGERY'
	,'No Questions'
	,5
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,8454143
	,0
	,1
	,'Y'
	,'N'
	,'A'
	,''
	,'1'
	,'1'
	,'0'
	,'0'
	,'E'
	,'N'
	,'F'
	,''
	)

--add Insurers
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'CIGNA'
	,N'P.O. BOX 5710'
	,N'SCRANTON'
	,N'PA'
	,N'18505'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'O'
	,N'N'
	,0
	,0
	,N'A'
	,N''
	,N''
	,N''
	,N'62308'
	,N''
	,N'H'
	,N'V'
	,N''
	,N''
	,N''
	,N''
	,N'N'
	,N''
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'COMM'
	,N'N'
	,N''
	,N'N'
	,N''
	)


--Insurer #3 MEDICARE
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'MEDICARE'
	,N'P.O. BOX 889999'
	,N'CAMP HILL'
	,N'PA'
	,N'17089'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'['
	,N'N'
	,0
	,1
	,N'-'
	,N''
	,N''
	,N''
	,N'12502'
	,N''
	,N'H'
	,N'Y'
	,N''
	,N''
	,N''
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N'NNNNNNNNNAO'
	,N''
	,N'MCARENJ'
	,N'Y'
	,N''
	,N'Y'
	,N'PA'
	)

-- Insurer #4 BLUE SHIELD
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'BLUE SHIELD'
	,N'82349 EAST BOULEVARD'
	,N'CHICAGO'
	,N'IL'
	,N'60606'
	,N''
	,N''
	,N'BLUE GROUP'
	,N''
	,N''
	,N''
	,N'P'
	,N'N'
	,0
	,0
	,N'A'
	,N''
	,N''
	,N''
	,N'00803'
	,N''
	,N'H'
	,N'Y'
	,N''
	,N''
	,N''
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N'GAP03'
	,N''
	,N''
	,N''
	,N'BLUES'
	,N'Y'
	,N''
	,N'N'
	,N''
	)


--Insurer #5 MEDICAID
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'MEDICAID'
	,N'105 ABBY LANE'
	,N'ROCHESTER'
	,N'NY'
	,N'13205'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'C'
	,N'N'
	,0
	,1
	,N'-'
	,N''
	,N''
	,N''
	,N'SMKNY1'
	,N''
	,N'H'
	,N'Y'
	,N''
	,N''
	,N''
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'MCAIDNY'
	,N'Y'
	,N''
	,N'F'
	,N'PA'
	)

INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'AETNA'
	,N'P.O. BOX 3946'
	,N'ALLENTOWN'
	,N'PA'
	,N'18106'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'O'
	,N'N'
	,0
	,0
	,N'A'
	,N''
	,N''
	,N''
	,N'60054'
	,N''
	,N'H'
	,N'V'
	,N''
	,N''
	,N'1AU'
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N'1'
	,N'COMM'
	,N'Y'
	,NULL
	,NULL
	,NULL
	)

--Insurer #6 VSP
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'VSP'
	,N'ON A LONELY STREET'
	,N'DETROIT'
	,N'IL'
	,N'48243-1403'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'O'
	,N'N'
	,0
	,0
	,N'A'
	,N''
	,N''
	,N''
	,N'HIVSP'
	,N''
	,N'H'
	,N'Y'
	,N''
	,N''
	,N''
	,N''
	,N'N'
	,N''
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'COMM'
	,N'N'
	,N''
	,N'N'
	,N''
	)



--Insurer #7 SECURE HORIZONS (MEDICAID)
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'SECURE HORIZONS'
	,N'1224 ROUTE 46'
	,N'NEWARK'
	,N'NJ'
	,N'07105'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'C'
	,N'N'
	,0
	,1
	,N'-'
	,N''
	,N''
	,N''
	,N'SECHO'
	,N''
	,N'H'
	,N'Y'
	,N''
	,N''
	,N''
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'MCAIDNJ'
	,N'Y'
	,N''
	,N'F'
	,N'NJ'
	)



--Insurer #8 MEDICARE RAILROAD
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'MEDICARE RAILROAD'
	,N'P.O. BOX 889999'
	,N'CAMP HILL'
	,N'PA'
	,N'17089'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'['
	,N'N'
	,0
	,1
	,N'-'
	,N''
	,N''
	,N''
	,N'SRRGA'
	,N''
	,N'H'
	,N'E'
	,N''
	,N''
	,N''
	,N''
	,N'Y'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'MCARENJ'
	,N'Y'
	,N''
	,N'Y'
	,N'PA'
	)


--Insurer #9 Paper Claim Insurer
INSERT [dbo].[PracticeInsurers] (
	[InsurerName]
	,[InsurerAddress]
	,[InsurerCity]
	,[InsurerState]
	,[InsurerZip]
	,[InsurerPhone]
	,[InsurerPlanId]
	,[InsurerPlanName]
	,[InsurerPlanType]
	,[InsurerGroupId]
	,[InsurerGroupName]
	,[InsurerReferenceCode]
	,[ReferralRequired]
	,[Copay]
	,[OutOfPocket]
	,[Availability]
	,[InsurerFeeSchedule]
	,[InsurerTosTbl]
	,[InsurerPosTbl]
	,[NEICNumber]
	,[InsurerENumber]
	,[InsurerEFormat]
	,[InsurerTFormat]
	,[InsurerPrecPhone]
	,[InsurerEligPhone]
	,[InsurerProvPhone]
	,[InsurerClaimPhone]
	,[InsurerCrossOver]
	,[InsurerPayType]
	,[InsurerAdjustmentDefault]
	,[PrecertRequired]
	,[MedicareCrossOverId]
	,[InsurerComment]
	,[InsurerPlanFormat]
	,[InsurerClaimMap]
	,[InsurerBusinessClass]
	,[OCode]
	,[InsurerCPTClass]
	,[InsurerServiceFilter]
	,[StateJurisdiction]
	)
VALUES (
	N'LOCAL 104'
	,N'456 ROUTE 77'
	,N'BILOXI'
	,N'MS'
	,N'39530'
	,N'2165558888'
	,N''
	,N'BAKERS UNION'
	,N''
	,N''
	,N''
	,N'O'
	,N'N'
	,0
	,0
	,N'A'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'H'
	,N''
	,N''
	,N''
	,N''
	,N''
	,N'N'
	,N'IP'
	,N''
	,N'N'
	,N''
	,N''
	,N''
	,N''
	,N'COMM'
	,N'Y'
	,N''
	,N'N'
	,N''
	)


-- PersonBillingOrganization
INSERT INTO PracticeAffiliations
           ([ResourceId]
           ,[ResourceType]
           ,[PlanId]
           ,[Pin]
           ,[Override]
           ,[LocationId]
           ,[AlternatePin]
           ,[OrgOverride])
SELECT ResourceId, 'R', InsurerId, 'OVERRIDEAFF', 'Y', 0, '', 'Y'
FROM Resources 
INNER JOIN PracticeInsurers ON InsurerName = 'SECURE HORIZONS'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


INSERT INTO PracticeAffiliations
           ([ResourceId]
           ,[ResourceType]
           ,[PlanId]
           ,[Pin]
           ,[Override]
           ,[LocationId]
           ,[AlternatePin]
           ,[OrgOverride])
SELECT ResourceId, 'R', InsurerId, 'AETNAPROVIDER', 'N', 0, '', 'N'
FROM Resources 
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



