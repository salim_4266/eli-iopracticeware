WScript.Echo "Beginning ADODB tests."

Dim myConnection, myCommand, myRecordset, myRecordCount

Dim adAsyncExecute 
adAsyncExecute = 4096

Dim progress
progress = 0

Dim result

Dim connectionString

Dim generalMetaData

If WScript.Arguments.Count <> 2 Then
    WScript.Echo "Missing parameters"
    WScript.Quit(-1)
End If

connectionString = WScript.Arguments(0)

generalMetaData = WScript.Arguments(1)

Dim comWrapper
Set comWrapper = CreateObject("IO.Practiceware.Interop.ComWrapper")
Dim emptyArgs()
ReDim emptyArgs(-1)

WScript.Echo "Logging in."

Dim userContext
set userContext = comWrapper.Evaluate( "UserContext.Current")
userContext.Login "9997"

WScript.Echo "Running RunStoredProcedureAndGetRowsUsingCommand."
result = RunStoredProcedureAndGetRowsUsingCommand
If result <> 1 Then
    WScript.Echo "RunStoredProcedureAndGetRowsUsingCommand returned " & result & ". 1 was expected."
End If

WScript.Echo "Running RunStoredProcedureAndGetRowsUsingRecordset."
result = RunStoredProcedureAndGetRowsUsingRecordset
If result <> 1 Then
    WScript.Echo "RunStoredProcedureAndGetRowsUsingRecordset returned " & result & ". 1 was expected."
End If

WScript.Echo "Running GetRecordsetSelectTop10RowsAsync."
GetRecordsetSelectTop10RowsAsync
If myRecordCount <> 10 Then
    WScript.Echo "GetRecordsetSelectTop10RowsAsync returned " & myRecordCount & ". 10 was expected."
End If

WScript.Echo "Running GetRecordsetSelectTop10Rows."
result = GetRecordsetSelectTop10Rows 
If result <> 10 Then
    WScript.Echo "GetRecordsetSelectTop10Rows returned " & result & ". 10 was expected."
End If

WScript.Echo "Running AddNewAndSelectRowUsingRecordset."
result = AddNewAndSelectRowUsingRecordset
If result <> 2 Then
    WScript.Echo "AddNewAndSelectRowUsingRecordset returned " & result & ". 2 was expected."
End If

WScript.Echo "Running DeleteRowUsingRecordset."
result = DeleteRowUsingRecordset 
If result <> -1 Then
    WScript.Echo "DeleteRowUsingRecordset returned " & result & ". -1 was expected."
End If

WScript.Echo "Running UpdateAndSelectRowUsingRecordset."
result = UpdateAndSelectRowUsingRecordset
If result <> "Test3 Again" Then
    WScript.Echo "UpdateAndSelectRowUsingRecordset returned " & result & ". Test3 Again was expected."
End If

WScript.Echo "Running SelectTop10RowsUsingCommand."
result = SelectTop10RowsUsingCommand
If result <> 10 Then
    WScript.Echo "SelectTop10RowsUsingCommand returned " & result & ". 10 was expected."
End If

WScript.Echo "Running InsertAndSelectRowsUsingCommand."
result = InsertAndSelectRowsUsingCommand
If result <= 0 Then
    WScript.Echo "InsertAndSelectRowsUsingCommand returned " & result & ". > 1 was expected."
End If

WScript.Echo "Running DeleteRowUsingCommand."
result = DeleteRowUsingCommand
If result <> -1 Then
    WScript.Echo "DeleteRowUsingCommand returned " & result & ". -1 was expected."
End If

WScript.Echo "Running UpdateAndSelectRowUsingCommand."
result = UpdateAndSelectRowUsingCommand
If result <> "Test Again" Then
    WScript.Echo "UpdateAndSelectRowUsingCommand returned " & result & ".  Test Again was expected."
End If

WScript.Echo "Running SelectRowsAndMoveUsingRecordset."
result = SelectRowsAndMoveUsingRecordset
If result <> 10 Then
    WScript.Echo "SelectRowsAndMoveUsingRecordset returned " & result & ". 10 was expected."
End If

WScript.Echo "Running SetActiveConnectionAndSourceOnRecordset."
result = SetActiveConnectionAndSourceOnRecordset
If result <> 10 Then
    WScript.Echo "SetActiveConnectionAndSourceOnRecordset returned " & result & ". 10 was expected."
End If

WScript.Echo "Running SetCommandOnRecordset."
result = SetCommandOnRecordset
If result <> 10 Then
    WScript.Echo "SetCommandOnRecordset returned " & result & ". 10 was expected."
End If

WScript.Echo "Running SetConnectionOnRecordset."
result = SetConnectionOnRecordset
If result <> 10 Then
    WScript.Echo "SetConnectionOnRecordset returned " & result & ". 10 was expected."
End If

WScript.Echo "Running ResetCommandParameters."
result = ResetCommandParameters
If result <> 1 Then
    WScript.Echo "ResetCommandParameters returned " & result & ". 1 was expected."
End If

WScript.Echo "Done"
WScript.Quit(0)

Sub GetRecordsetSelectTop10RowsAsync()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordset = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    WScript.ConnectObject myRecordset, "myRecordset_"

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 
    myCommand.CommandText="SELECT Top 10 * FROM PracticeCodeTable order by 1" 
    Call myRecordset.Open(myCommand, , 3, 3, adAsyncExecute)   
    
    myRecordset.WaitAll
End Sub 

Sub myRecordset_FetchProgress(min, max, status, rs)
   progress = 1
End Sub

Sub myRecordset_FetchComplete(error, status, rs)
    If Not myRecordset.BOF Then myRecordset.MoveFirst
    If Not myRecordset.EOF Then myRecordset.MoveLast
    If Not myRecordset.EOF Then myRecordset.MoveNext
    If Not myRecordset.BOF Then myRecordset.MovePrevious
    If Not myRecordset.EOF And progress = 1 then 
        myRecordCount = myRecordset.RecordCount
    else
        myRecordCount = 0
    end if
End Sub

Function GetRecordsetSelectTop10Rows()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordset = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 
    myCommand.CommandText="SELECT Top 10 * FROM PracticeCodeTable order by 1" 
    Call myRecordset.Open(myCommand, , 3, 3, 1)
   
    If Not myRecordset.BOF Then myRecordset.MoveFirst
    If Not myRecordset.EOF Then myRecordset.MoveLast
    If Not myRecordset.EOF Then myRecordset.MoveNext
    If Not myRecordset.BOF Then myRecordset.MovePrevious
    If Not myRecordset.EOF then 
        GetRecordsetSelectTop10Rows = myRecordset.RecordCount
    else
        GetRecordsetSelectTop10Rows = 0
    end if
End Function 

Function AddNewAndSelectRowUsingRecordset()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordset = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
 
    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString 
    myCommand.ActiveConnection=myConnection 
    ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('Test2', 'Test2', 1, 1)" 
    myCommand.Execute()
    'Select records
    myCommand.CommandText="SELECT * FROM model.Patients where LastName = 'Test2' and FirstName = 'Test2' order by 1 desc"
    myCommand.ConvertToParameterizedSql
    Call myRecordset.Open(myCommand, , 3, 3, 1)
    While myCommand.Parameters().Count > 0
    myCommand.Parameters().Delete (0)
    Wend    
    Dim PatientId
    PatientId = 0
    If Not myRecordset.EOF then 
      PatientId = myRecordset(0)
    End If
    
    myRecordset.AddNew
    myRecordset("LastName") = "Test2 New"
    myRecordset("FirstName") = "Test2 New"
    myRecordset("PatientStatusId") = 1
    myRecordset("IsClinical") = 1
    myRecordset.Update()

    Dim recordCount 
    recordCount = myRecordset.RecordCount

    myConnection.RollbackTrans

    AddNewAndSelectRowUsingRecordset = recordCount
End Function 

Function DeleteRowUsingRecordset()
   
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordset = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString
    myCommand.ActiveConnection=myConnection 
      ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('Test', 'Test', 1, 1)" 
    myCommand.Execute()
    ' Get PatientId from PatientDemographics
    myCommand.CommandText="SELECT Id FROM model.Patients where LastName = 'Test' and FirstName = 'Test' order by 1 desc"
    Call myRecordset.Open(myCommand, , 3, 4, 1)
        
    Dim PatientId
    'Get the first record results
    If Not myRecordset.EOF then 
        PatientId = myRecordset(0)
        myRecordset.MoveFirst
        While Not (myRecordset.EOF)
            myRecordset.Delete
            myRecordset.MoveNext
        Wend
        If Not myRecordset.EOF then 
            PatientId = myRecordset(0)
        Else 
            PatientId = -1
        End If
    End If
    myConnection.RollbackTrans
    DeleteRowUsingRecordset = PatientId
End Function 

Function UpdateAndSelectRowUsingRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString
    myCommand.ActiveConnection=myConnection 
      ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('Test3', 'Test3', 1, 1)" 
    myCommand.Execute()
    ' Get PatientId from PatientDemographics
    myCommand.CommandText="SELECT Id, LastName, FirstName FROM model.Patients where LastName = 'Test3' and FirstName = 'Test3' order by 1 desc"
    Call myRecordSet.Open(myCommand, , 3, 3, 1)
        
    Dim PatientId
    'Get the first record results
    If Not myRecordSet.EOF then 
      PatientId = myRecordSet(0)
    End If
    IF Cint(PatientId) > 0 then
      ' update PatientDemographics
        myRecordSet("LastName") = "Test3 Again"
        myRecordSet("FirstName") = "Test3 Again"
        myRecordSet.Update()
        ' select record from PatientDemographics
        Set myRecordSet = Nothing
        'Set myRecordSet = CreateObject("ADODB.RecordSet") 
        Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
        myCommand.CommandText="SELECT Id, LastName, FirstName FROM model.Patients WHERE Id = " & PatientId 
        Call myRecordSet.Open(myCommand, , 3, 3, 1)
        If Not myRecordSet.EOF then 
          PatientId = myRecordSet(0)
          UpdateAndSelectRowUsingRecordset =  myRecordSet(1)
        Else 
          PatientId = -1
          UpdateAndSelectRowUsingRecordset =  ""
        End If
     End If
    
    myConnection.RollbackTrans
End Function 

Function RunStoredProcedureAndGetRowsUsingRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
   
    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString 
    myCommand.ActiveConnection=myConnection 
    RunStoredProcedureAndGetRowsUsingRecordset = -1
     ' Insert data into DB
    myCommand.CommandText= generalMetaData
    myCommand.Execute()

     'Select records
    myCommand.CommandText="SELECT Top 1 Id FROM model.Patients where LastName ='AETNA' and FirstName ='ONLY' order by 1"
    set adoRecSet = myCommand.Execute()
    Dim PatientId
    PatientId = 0
    If Not adoRecSet.EOF then 
      PatientId = adoRecSet(0)
    End If
    If (Cint(PatientId) > 0) Then
        myCommand.CommandType = 4 ' stored procedure
        myCommand.CommandText = "PatientDocumentList"
        Set myParameter = Nothing
        Set myParameter = myCommand.CreateParameter("@PatId")
        myParameter.Type = 3 ' integer
        myParameter.Direction = 1
        myParameter.Size = Len(Trim(cstr(PatientId)))
        myParameter.Value = Trim(cstr(PatientId))

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' myCommand.Parameters(1) = myParameter
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' NOTE: Three usage cases for working with parameters
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (1)
        myCommand.Parameters.Append myParameter
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (2)
        Dim parameters
        Set parameters = myCommand.Parameters
        parameters(0) = myParameter
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (3)
        myCommand.Parameters()(0) = myParameter ' NOTE: This is how the VB6 code should work with parameters
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        set adoRecSet = myCommand.Execute()
        Dim count
        count = 0
        'Iterate through the results 
        While Not adoRecSet.EOF 
          count = count + 1
          adoRecSet.MoveNext 
        Wend    
        RunStoredProcedureAndGetRowsUsingRecordset = count
    End If

    myConnection.RollbackTrans
End Function

Function SelectTop10RowsUsingCommand()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 
    myCommand.CommandText="SELECT Top 10 * FROM PracticeCodeTable order by 1"
    set adoRec = myCommand.Execute()
    Dim count
    count = 0
    'Iterate through the results 
    While Not adoRec.EOF 
      count = count + 1
      adoRec.MoveNext 
    Wend    
    SelectTop10RowsUsingCommand = count    
End Function 

Function InsertAndSelectRowsUsingCommand()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    
    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString 
    myCommand.ActiveConnection=myConnection 
    ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('Test', 'Test', 1, 1)" 
    myCommand.Execute()
    'Select records

    myCommand.CommandText="SELECT Id FROM model.Patients where LastName = 'Test' and FirstName = 'Test' order by 1 desc"
    set adoRec = myCommand.Execute()
    Dim PatientId
    PatientId = 0
    If Not adoRec.EOF then 
      PatientId = adoRec(0)
    End If

    myConnection.RollbackTrans

    InsertAndSelectRowsUsingCommand = PatientId
End Function 

Function DeleteRowUsingCommand()
    
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
     
    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString
    myCommand.ActiveConnection=myConnection 
      ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('TestC', 'TestC', 1, 1)" 
    myCommand.Execute()
    ' Get PatientId from PatientDemographics
    myCommand.CommandText="SELECT Id FROM model.Patients where LastName = 'TestC' and FirstName = 'TestC' order by 1 desc"
    set adoRec = myCommand.Execute()
        
    Dim PatientId
    'Get the first record results
    If Not adoRec.EOF then 
      PatientId = adoRec(0)
    End If
    IF Cint(PatientId) > 0 then
        ' Delete record from PatientDemographics
        myCommand.CommandText="If Exists(Select Id from model.Patients where Id = " & PatientId & " ) Delete from model.Patients where Id = " & PatientId  
        myCommand.Execute()
        myCommand.CommandText="SELECT Id FROM model.Patients WHERE Id = " & PatientId 
        set adoRec = myCommand.Execute()   
        If Not adoRec.EOF then 
          PatientId = adoRec(0)
        Else 
          PatientId = -1
        End If
     End If
     
    myConnection.RollbackTrans
    DeleteRowUsingCommand = PatientId
End Function 

Function UpdateAndSelectRowUsingCommand()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    
    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString
    myCommand.ActiveConnection=myConnection 
      ' Insert record into PatientDemographics
    myCommand.CommandText="insert into model.Patients (LastName, FirstName, PatientStatusId, IsClinical) values('Test', 'Test', 1, 1)" 
    myCommand.Execute()
    ' Get PatientId from PatientDemographics
    myCommand.CommandText="SELECT IDENT_CURRENT('model.Patients')"
    set adoRec = myCommand.Execute()
        
    Dim PatientId
    'Get the first record results
    If Not adoRec.EOF then 
      PatientId = adoRec(0)
    End If
    IF Cint(PatientId) > 0 then
      ' update PatientDemographics
        myCommand.CommandText="update model.Patients set LastName = 'Test Again', FirstName = 'Test Again' WHERE Id = " & PatientId 
        myCommand.Execute()
        ' select record from PatientDemographics
        myCommand.CommandText="SELECT Id, LastName, FirstName FROM model.Patients WHERE Id = " & PatientId 
        set adoRec = myCommand.Execute()   
        If Not adoRec.EOF then 
          PatientId = adoRec(0)
          UpdateAndSelectRowUsingCommand =  adoRec(1)
        Else 
          PatientId = -1
          UpdateAndSelectRowUsingCommand =  ""
        End If
     End If
    
     myCommand.CommandText="delete from model.Patients WHERE Id = " & PatientId 
     myCommand.Execute()

     myConnection.RollbackTrans
End Function 

Function RunStoredProcedureAndGetRowsUsingCommand()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance

    WScript.Echo "Beginning Transaction."    
    myConnection.BeginTrans
    myConnection.Open connectionString 
    myCommand.ActiveConnection=myConnection 
    GetRecordList = -1
     ' Insert data into DB
    myCommand.CommandText= generalMetaData
    myCommand.Execute()

     'Select records
    myCommand.CommandText="SELECT Top 1 Id FROM model.Patients where LastName ='AETNA' and FirstName ='ONLY' order by 1"
    set adoRecSet = myCommand.Execute()
    While myCommand.Parameters().Count > 0
    myCommand.Parameters().Delete (0)
    Wend  
    Dim PatientId
    PatientId = 0
    If Not adoRecSet.EOF then 
      PatientId = adoRecSet(0)
    End If
    If (Cint(PatientId) > 0) Then
        myCommand.CommandType = 4 ' stored procedure
        myCommand.CommandText = "PatientDocumentList"
        Set myParameter = Nothing
        Set myParameter = myCommand.CreateParameter("@PatId")
        myParameter.Type = 3 ' integer
        myParameter.Direction = 1
        myParameter.Size = Len(Trim(cstr(PatientId)))
        myParameter.Value = Trim(cstr(PatientId))

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' myCommand.Parameters(1) = myParameter
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' NOTE: Three usage cases for working with parameters
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (1)
        myCommand.Parameters.Append myParameter
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (2)
        Dim parameters
        Set parameters = myCommand.Parameters
        parameters(0) = myParameter
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' (3)
        myCommand.Parameters()(0) = myParameter ' NOTE: This is how the VB6 code should work with parameters
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        set adoRecSet = myCommand.Execute()
        Dim count
        count = 0
        'Iterate through the results 
        While Not adoRecSet.EOF 
          count = count + 1
          adoRecSet.MoveNext 
        Wend    
        RunStoredProcedureAndGetRowsUsingCommand = count
    End If

    myConnection.RollbackTrans
End Function

Function SelectRowsAndMoveUsingRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 
    myCommand.CommandText="SELECT Top 10 * FROM PracticeCodeTable order by 1" 
    Call myRecordSet.Open(myCommand, , 3, 3, 1)
   
    If Not myRecordSet.EOF Then 
     myRecordSet.MoveFirst
     myRecordSet.Move 0
    End if 
    If Not myRecordSet.EOF Then myRecordSet.MoveLast
    If Not myRecordSet.EOF Then myRecordSet.MoveNext
    If Not myRecordSet.BOF Then myRecordSet.MovePrevious
    If Not myRecordSet.EOF then 
        SelectRowsAndMoveUsingRecordset = myRecordSet.RecordCount
    else
        SelectRowsAndMoveUsingRecordset = 0
    end if
End Function 

Function GetRecordSet(SQL)
    Dim myConnection, myCommand, myRecordSet 
    'Set myConnection = CreateObject("ADODB.Connection") 
    'Set myCommand = CreateObject("ADODB.Command") 
    'Set myRecordSet = CreateObject("ADODB.RecordSet")

    Dim comWrapper
    Set comWrapper = CreateObject("IO.Practiceware.Interop.ComWrapper")
    Dim emptyArgs()
    ReDim emptyArgs(-1)
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    Dim userContext
    set userContext = comWrapper.Evaluate( "UserContext.Current")
    userContext.Login "9997"

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 

    myRecordSet.ActiveConnection = myCommand.ActiveConnection
    myRecordSet.CursorType = 3
    myRecordSet.LockType = 3
    myRecordSet.Source = SQL
    myRecordSet.Open
    Set GetRecordSet = myRecordSet
    Set myRecordSet = Nothing
End Function

Function SetActiveConnectionAndSourceOnRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 

    myRecordSet.ActiveConnection = myCommand.ActiveConnection
    myRecordSet.CursorType = 3
    myRecordSet.LockType = 3
    myRecordSet.Source = "SELECT Top 10 * FROM PracticeCodeTable order by 1" 
    myRecordSet.Open
      
    If Not myRecordSet.EOF Then 
     myRecordSet.MoveFirst
     myRecordSet.Move 1
    End if 
    If Not myRecordSet.EOF Then myRecordSet.MoveLast
    If Not myRecordSet.EOF Then myRecordSet.MoveNext
    If Not myRecordSet.BOF Then myRecordSet.MovePrevious
    If Not myRecordSet.EOF then 
        SetActiveConnectionAndSourceOnRecordset = myRecordSet.RecordCount
    else
        SetActiveConnectionAndSourceOnRecordset = 0
    end if
End Function 

Function SetCommandOnRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    Dim userContext
    set userContext = comWrapper.Evaluate( "UserContext.Current")
    userContext.Login "9997"

    myConnection.Open ConnectionString 
    myCommand.ActiveConnection=myConnection 
    myCommand.CommandText = "SELECT Top 10 * FROM PracticeCodeTable order by 1" 
      
    Call myRecordSet.Open(myCommand, , 3, 3)
    
    If Not myRecordSet.EOF Then 
     myRecordSet.MoveFirst
     myRecordSet.Move 1
    End if 
    If Not myRecordSet.EOF Then myRecordSet.MoveLast
    If Not myRecordSet.EOF Then myRecordSet.MoveNext
    If Not myRecordSet.BOF Then myRecordSet.MovePrevious
    If Not myRecordSet.EOF then 
        SetCommandOnRecordset = myRecordSet.RecordCount
    else
        SetCommandOnRecordset = 0
    end if
End Function

Function SetConnectionOnRecordset()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    myConnection.Open ConnectionString 
    
    myRecordSet.ActiveConnection = myConnection
    myRecordSet.CursorType = 3
    myRecordSet.LockType = 3
    myRecordSet.Source = "SELECT Top 10 * FROM PracticeCodeTable order by 1" 
    myRecordSet.Open
    
    If Not myRecordSet.EOF Then 
     myRecordSet.MoveFirst
     myRecordSet.Move 1
    End if 
    If Not myRecordSet.EOF Then myRecordSet.MoveLast
    If Not myRecordSet.EOF Then myRecordSet.MoveNext
    If Not myRecordSet.BOF Then myRecordSet.MovePrevious
    If Not myRecordSet.EOF then 
        SetConnectionOnRecordset = myRecordSet.RecordCount
    else
        SetConnectionOnRecordset = 0
    end if
End Function 

Function ResetCommandParameters()
    Set myConnection = comWrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
    Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
    Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance

    WScript.Echo "Beginning Transaction."
    myConnection.BeginTrans
    myConnection.Open connectionString 
    myCommand.ActiveConnection=myConnection 
    ResetCommandParameters = -1
     ' Insert record into PatientDemographics
    myCommand.CommandText= generalMetaData
    myCommand.Execute()
    'Select records
    myCommand.CommandText="SELECT Top 1 Id FROM model.Patients where LastName ='AETNA' and FirstName ='ONLY' order by 1"
    Call myRecordSet.Open(myCommand, , 3, 3, 1)
    Dim PatientId
    PatientId = 0
    If Not myRecordSet.EOF then 
      PatientId = myRecordSet(0)
    End If
    
    If (Cint(PatientId) > 0) Then
        'Set myCommand = Nothing
        'Set  myCommand = CreateObject("ADODB.Command") 
        'Set myCommand = comWrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
        Set myRecordSet = Nothing
        'Set myRecordSet = CreateObject("ADODB.RecordSet") 
        Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
        'myCommand.ActiveConnection=myConnection 

        myCommand.CommandType = 4 ' stored procedure
        myCommand.CommandText = "PatientDocumentList"
        Set myParameter = Nothing
        Set myParameter = myCommand.CreateParameter("@PatId")
        myParameter.Type = 3 ' integer
        myParameter.Direction = 1
        myParameter.Size = Len(Trim(cstr(PatientId)))
        myParameter.Value = Trim(cstr(PatientId))

        myCommand.Parameters()(0) = myParameter ' NOTE: This is how the VB6 code should work with parameters

        Call myRecordSet.Open(myCommand, , 3, 3, 4)
        Dim count
        count = 0
        'Iterate through the results 
        While Not myRecordSet.EOF 
          count = count + 1
          myRecordSet.MoveNext 
        Wend  

        Set myRecordSet = Nothing
        'Set myRecordSet = CreateObject("ADODB.RecordSet") 
         Set myRecordSet = comWrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
        myCommand.CommandType = 1 'text command
        myCommand.CommandText="SELECT Top 1 Id FROM model.Patients where LastName ='AETNA' and FirstName ='ONLY' order by 1"
        Call myRecordSet.Open(myCommand, , 3, 3, 1)       
        PatientId = 0
        If Not myRecordSet.EOF then 
          PatientId = myRecordSet(0)
        End If
        
        ResetCommandParameters = count
    End If

    myConnection.RollbackTrans
End Function