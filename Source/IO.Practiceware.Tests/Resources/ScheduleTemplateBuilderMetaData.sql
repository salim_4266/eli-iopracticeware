
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'InsteadOfInsertIntoPatientFinancial' AND type = 'TR')
 BEGIN
     DROP TRIGGER InsteadOfInsertIntoPatientFinancial
 END

EXEC('CREATE TRIGGER InsteadOfInsertIntoPatientFinancial ON dbo.PatientFinancial
INSTEAD OF INSERT
AS 
BEGIN
	INSERT INTO model.InsurancePolicies
	(PolicyCode
	,GroupCode
	,Copay
	,Deductible
	,StartDateTime
	,PolicyHolderPatientId
	,InsurerId
	,MedicareSecondaryReasonCodeId)
	SELECT
	i.FinancialPerson
	,i.FinancialGroupId
	,i.FinancialCopay
	,0
	,CONVERT(DATETIME,i.FinancialStartDate)
	,i.PatientId
	,i.FinancialInsurerId
	,NULL
	FROM Inserted i

	DECLARE @CurrentPatientId INT
	SET @CurrentPatientId = (SELECT PatientId FROM Inserted)

	INSERT INTO model.PatientInsurances
	(InsuranceTypeId
	,PolicyHolderRelationshipTypeId
	,OrdinalId
	,InsuredPatientId
	,InsurancePolicyId
	,EndDateTime
	,IsDeleted)
	SELECT
	CASE i.FinancialInsType
		WHEN ''V''
			THEN 2
		WHEN ''A''
			THEN 3			
		WHEN ''W''
			THEN 4
		ELSE 1
	END
	,CASE (
		CASE 
			WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
				THEN ISNULL(dbo.GetPolicyHolderRelationshipType(p.Id),''Y'')
			ELSE ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p.Id),'''')
			END
		)
		WHEN ''Y''
			THEN 1
		WHEN ''S''
			THEN 2
		WHEN ''C''
			THEN 3
		WHEN ''E''
			THEN 4
		WHEN ''L''
			THEN 6
		WHEN ''O''
			THEN 7
		WHEN ''D''
			THEN 7
		WHEN ''P''
			THEN 7
		ELSE CASE 
				WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
					THEN 1
				WHEN ISNULL(dbo.GetSecondPolicyHolderPatientId(p.Id),0) = i.PatientId
					THEN 1
			ELSE 5
			END
	END
	,CASE i.FinancialInsType
			WHEN ''V''
				THEN 200
			WHEN ''A''
				THEN 3000			
			WHEN ''W''
				THEN 40000
			ELSE 10
			END + CASE 
					WHEN i.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id)
						THEN 0
					ELSE 3
				  END + CONVERT(INT, i.FinancialPIndicator)
	,i.PatientId
	,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = @CurrentPatientId ORDER BY Id DESC)
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(DATETIME,i.FinancialEndDate)
					END
		ELSE NULL
	END
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(BIT, 1)
					END
		ELSE CONVERT(BIT, 0)
	END
	FROM Inserted i
	JOIN model.Patients p ON p.Id = i.PatientId
END
	')


INSERT INTO model.ScheduleTemplateBuilderConfigurations(SpanSize,AreaStart,AreaEnd,DefaultLocationId) VALUES('2000-01-01 00:30:00','2000-01-01 09:00:00','2000-01-01 17:00:00',1)
GO

--AppointmentCategories
INSERT INTO model.AppointmentCategories (Name, HexColor)
VALUES ('Long Appointment', 112233)

INSERT INTO model.AppointmentCategories (Name, HexColor)
VALUES ('Short Appointment', 332211)


--Update appointment types to have appointent categories
DECLARE @AppointmentLongCategory int
SET @AppointmentLongCategory = (SELECT TOP 1 Id FROM model.AppointmentCategories WHERE Name = 'Long Appointment')

DECLARE @AppointmentShortCategory int
SET @AppointmentShortCategory = (SELECT TOP 1 Id FROM model.AppointmentCategories WHERE Name = 'Short Appointment')

UPDATE dbo.AppointmentType
SET AppointmentCategoryId = @AppointmentLongCategory
WHERE ApptypeId IN (SELECT TOP 2 AppTypeId FROM dbo.AppointmentType WHERE ApptypeId <> 0)

UPDATE dbo.AppointmentType
SET AppointmentCategoryId = @AppointmentShortCategory
WHERE ApptypeId IN (SELECT TOP 2 AppTypeId FROM dbo.AppointmentType WHERE ApptypeId <> 0 ORDER BY AppTypeId DESC)

DECLARE @AppTypeLongCat int
SET  @AppTypeLongCat  = (SELECT TOP 1 Id FROM Model.AppointmentTypes WHERE AppointmentCategoryId = @AppointmentLongCategory)

DECLARE @AppTypeShortCat int
SET  @AppTypeShortCat = (SELECT TOP 1 Id FROM Model.AppointmentTypes WHERE AppointmentCategoryId = @AppointmentShortCategory)



---PATIENT #2 1 long scheduled
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'AETNA'
	,'ONLY'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)



--PatientFinancial #1 AETNA ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '12345', ''
, 25, 1, '20110201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'AETNA'

--ADD APPOINTMENT PATIENT #1
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeLongCat, 
	20120305,
	735, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' AND ResourceLastName = 'Reiss'


--PATIENT #2 Block #3, App-1 -short
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'BLUESHIELD'
	,'AETNA'
	,'SPOUSE'
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,IDENT_CURRENT('PracticeVendors')
	,IDENT_CURRENT('PracticeVendors')-1
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,IDENT_CURRENT('model.Patients') - 1
	,'S'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MRS'
	,'T'
	,''
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #2
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'YLK098765', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'BLUE SHIELD'

--ADD APPOINTMENT PATIENT #2
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeShortCat, 
	20120305,
	750, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' AND ResourceLastName = 'REISS'


--patient #2 Block #3, App2-short
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'ONLY'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #3 MEDICARE ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, '666999888A', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD APPOINTMENT PATIENT #3
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeShortCat, 
	20120305,
	750, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SRG'
WHERE dr.ResourceType = 'D' AND dr.ResourceLastName = 'REISS'


--Patient #4 Block #3, App3-short
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAID'
	,'ONLY'
	,'G'
	,''
	,''
	,'46699 EASTERN BOULEVARD'
	,''
	,'PHILADELPHIA'
	,'PA'
	,'19019'
	,'2153335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19280105'
	,''
	,'SPANISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #4 medicaid only
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'DEU99008', ''
, 0, 1, '20100711', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICAID'


--ADD APPOINTMENT PATIENT #4
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeShortCat, 
	20120305,
	750, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	fac.ResourceId, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111122
FROM Resources dr
INNER JOIN Resources fac ON fac.ResourceType = 'R' and fac.ServiceCode = '02' and fac.ResourceName = 'BROOK'
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE dr.ResourceType = 'D' AND dr.ResourceLastName = 'REISS'



--Patient #5 Block #3, App4-short
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'MEDICAID'
	,'C'
	,''
	,''
	,'85 RED ROSE STREET'
	,''
	,'ATLANTA'
	,'GA'
	,'10022'
	,'3033335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19151123'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #5 MEDICARE/MEDICAID
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '999663333A', ''
, 0, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD 2d INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '345TYU', ''
, 0, 2, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICAID'


--ADD APPOINTMENT #1 PATIENT #5
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeShortCat, 
	20120305,
	750, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = 'TLC'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'


----------PATIENT #6 ----
--Patient #6 Block #5 Long
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'AETNASPOUSEPRIMARY'
	,'CIGNASELFSECONDARY'
	,''
	,''
	,''
	,'448 STATE STREET'
	,''
	,'BROOKLYN'
	,'NY'
	,'12117'
	,'7188750653'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19550715'
	,''
	,'FRENCH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,pdSpouse.PatientId
	,'S'
	,'N'
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'HISP'
FROM PatientDemographics pdSpouse
WHERE pdSpouse.LastName = 'AETNA' AND pdSpouse.FirstName = 'ONLY'

UPDATE PatientDemographics
SET SecondPolicyPatientId = PatientId
WHERE LastName = 'AETNASPOUSEPRIMARY' and FirstName = 'CIGNASELFSECONDARY'

--PatientFinancial #6 AETNA SPOUSE IS PRIMARY; THIS POLICY IS THE CIGNA SECONDARY SELF
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGNAPOLICY', ''
, 0, 1, '20101201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'


--Add referral
INSERT INTO model.PatientInsuranceReferrals
           ([PatientId]
           ,[ReferralCode]
           ,[ExternalProviderId]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[TotalEncountersCovered]
           ,[Comment]
           ,[DoctorId]
           ,[PatientInsuranceId]
           ,[IsArchived])
SELECT IDENT_CURRENT('model.Patients'),
	'referral4567',
	VendorId,
	CONVERT(datetime,'20111201',112),
	CONVERT(datetime,'20121201',112),
	6,
	'',
	ResourceId,
	pi.Id,
	CONVERT(bit, 0)
FROM PracticeVendors pv 
INNER JOIN Resources re ON ResourceType = 'D' and ResourceLastName = 'Reiss'
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = IDENT_CURRENT('model.Patients')
	AND pi.IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.Insurers pri ON Name = 'AETNA'
WHERE VendorLastName = 'COX'

--ADD APPOINTMENT PATIENT #6
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeLongCat, 
	20120305,
	780, 10, IDENT_CURRENT('model.PatientInsuranceReferrals'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120201
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'NPG'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

INSERT INTO model.EncounterPatientInsuranceReferral (Encounters_Id, PatientInsuranceReferrals_Id)
SELECT IDENT_CURRENT('dbo.Appointments') AS Encounters_Id, IDENT_CURRENT('model.PatientInsuranceReferrals')

--patient #7 Block #5 app-2 Long
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAREPRI'
	,'CIGNASEC'
	,'W'
	,''
	,''
	,'85 LIME STREET'
	,''
	,'BOSTON'
	,'MA'
	,'10022'
	,'6173335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'M'
	,'19450905'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
	)

--PatientFinancial #7 MEDICARE PRIMARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '866663333A', ''
, 0, 1, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--PatientFinancial #7 CIGNA  SECONDARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGNAPOLICY2', ''
, 0, 2, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'

DECLARE @nextPatientId int
SET @nextPatientId = IDENT_CURRENT('model.Patients') + 1



--ADD APPOINTMENT #1 PATIENT #8 /Test #7 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeLongCat, 
	20120305,
	780, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'



--PATIENT #8 Block #6 Long
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty]
	,[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'BLUESHIELDSELF'
	,'CIGNASPOUSE'
	,'WILLIAM'
	,''
	,''
	,'85 LIME STREET','','BOSTON','MA','10022'
	,'6173335544'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19441123',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,@nextPatientId,'Y','N'
	,PatientId,'S','N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'MEDICAREPRI' AND FirstName = 'CIGNASEC'

--PATIENT #8 /Test #7  BLUESHIELD SELF/CIGNA SPOUSE
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'YLK9663333', ''
, 0, 1, '20110302', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'BLUE SHIELD'


--ADD APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	@AppTypeLongCat, 
	20120305,
	780, 15, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	sl.ResourceId, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
INNER JOIN Resources sl on sl.ResourceType = 'R' and sl.ResourceName = 'BROOK'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'Reiss'



--This data is for appointments for users. 

--UserScheduleBlock open slots
DECLARE @ServiceLocationMain int
SET @ServiceLocationMain = (SELECT TOP 1 Id FROM model.ServiceLocations WHERE IsExternal = 0 AND ShortName = 'Office')

DECLARE @ServiceLocationSatalite int
SET @ServiceLocationSatalite = (SELECT TOP 1 Id FROM model.ServiceLocations WHERE  ShortName = 'BROOK')

DECLARE @user int
SET @user = (SELECT TOP 1 Id FROM model.users WHERE IsSchedulable = 1 AND IsArchived = 0 AND __EntityType__ = 'Doctor')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 12:00:00.000', NULL, @ServiceLocationMain, CONVERT(bit, 1), @user, 'UserScheduleBlock')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 12:15:00.000', NULL, @ServiceLocationMain, CONVERT(bit, 1), @user, 'UserScheduleBlock')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 12:30:00.000', NULL, @ServiceLocationMain, CONVERT(bit, 1), @user, 'UserScheduleBlock')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 12:45:00.000', NULL, @ServiceLocationMain, CONVERT(bit, 1), @user, 'UserScheduleBlock')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 13:00:00.000', NULL, @ServiceLocationMain, CONVERT(bit, 1), @user, 'UserScheduleBlock')

--In satalite office
INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 13:15:00.000', NULL, @ServiceLocationSatalite, CONVERT(bit, 1), @user, 'UserScheduleBlock')

INSERT INTO model.ScheduleBlocks (StartDateTime, ScheduleTemplateId, ServiceLocationId, IsUnavailable, Userid, __EntityType__)
VALUES ('2012-03-05 13:30:00.000', NULL, @ServiceLocationSatalite, CONVERT(bit, 1), @user, 'UserScheduleBlock')

--ScheudleBlockAppoitnmentCategory
DECLARE @ScheduleBlock int
DECLARE @ScheduledAppointment int

--#1 block 2 longs and 1 short no appointments scheduled
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 12:00:00.000')

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentLongCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentLongCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

--#2 block 2 longs 1 short, 1 long scheduled 
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 12:15:00.000')
SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentLongCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentLongCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

--#3 block 4 shorts, all scheduled
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 12:30:00.000')
SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentShortCategory, @ScheduleBlock)

SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentShortCategory, @ScheduleBlock)

SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentShortCategory, @ScheduleBlock)

SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentShortCategory, @ScheduleBlock)

--#4 Block 4 shorts all open
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 12:45:00.000')

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

--#5 3 shorts 2 longs, 2 longs scheduled
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 13:00:00.000')

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), NULL, @AppointmentShortCategory, @ScheduleBlock)

SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentLongCategory, @ScheduleBlock)

SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentLongCategory, @ScheduleBlock)

--#6 1 long, scheduled
SET @ScheduleBlock = (SELECT TOP 1 Id FROM model.ScheduleBlocks WHERE StartDateTime = '2012-03-05 13:15:00.000')
SET @ScheduledAppointment = (SELECT TOP 1 Id FROM model.Encounters WHERE Id > @ScheduledAppointment)
INSERT INTO model.ScheduleBlockAppointmentCategories (Id, AppointmentId, AppointmentCategoryId, ScheduleBlockId)
VALUES (NEWID(), @ScheduledAppointment, @AppointmentLongCategory, @ScheduleBlock)

--#7 No types 