SET NOCOUNT ON
GO
SET ANSI_NULLS ON 
GO

USE [master]

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'MVERepositoryTest')
BEGIN
ALTER DATABASE [MVERepositoryTest] SET OFFLINE WITH ROLLBACK IMMEDIATE
ALTER DATABASE [MVERepositoryTest] SET ONLINE
DROP DATABASE [MVERepositoryTest]
END
GO

CREATE DATABASE [MVERepositoryTest]
GO

USE [MVERepositoryTest]

--Add data type Time to sql 2005

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'time')
BEGIN
	
	EXEC('CREATE TYPE time FROM dateTime')

	EXEC('CREATE RULE TimeOnlyRule AS DATEDIFF(dd,0,@DateTime) = 0')
		
	EXEC sp_bindrule 'TimeOnlyRule', 'time'
END

GO

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'date')
BEGIN

	EXEC('CREATE TYPE date FROM dateTime')
	EXEC('CREATE RULE DateOnlyRule AS DATEADD(dd,DATEDIFF(dd,0,@DateTime),0) = @DateTime')

	EXEC sp_bindrule 'DateOnlyRule', 'date'

END

GO

/****** Object:  Table [dbo].[web_link]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'Time')
BEGIN
	
	EXEC('CREATE TYPE Time FROM dateTime')

	EXEC('CREATE RULE TimeOnlyRule AS
	datediff(dd,0,@DateTime) = 0')
		
	EXEC sp_bindrule 'TimeOnlyRule', 'Time'
END

GO

IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'Date')
BEGIN
	EXEC('CREATE TYPE Date FROM dateTime')
	EXEC('CREATE RULE DateOnlyRule AS
  dateAdd(dd,datediff(dd,0,@DateTime),0) = @DateTime')

EXEC sp_bindrule 'DateOnlyRule', 'Date'

END

GO

CREATE TABLE [dbo].[web_link](
	[web_link_id] [int] IDENTITY(1,1) NOT NULL,
	[web_link_name] [varchar](500) NULL,
	[web_link_address] [varchar](500) NULL,
	[web_link_active_flag] [varchar](3) NULL,
	[sort_order] [int] NULL,
	[location_id] [int] NULL,
 CONSTRAINT [PK_web_link] PRIMARY KEY CLUSTERED 
(
	[web_link_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[vsp_lensoptions]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[vsp_lensoptions](
	[lensoption_id] [int] IDENTITY(1,1) NOT NULL,
	[lsid] [varchar](500) NOT NULL,
	[lnsname] [varchar](500) NULL,
	[option_type] [varchar](500) NULL,
 CONSTRAINT [PK_vsp_lensoptions_1] PRIMARY KEY CLUSTERED 
(
	[lensoption_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[vsp_lenslist]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[vsp_lenslist](
	[lsid] [varchar](20) NOT NULL,
	[lnsname] [varchar](255) NULL,
	[styleid] [int] NULL,
	[materialid] [int] NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[vsp_labs]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[vsp_labs](
	[lsid] [varchar](20) NULL,
	[lsname] [varchar](255) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[vsp_carrier]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[vsp_carrier](
	[vsp_carrier_id] [int] IDENTITY(1,1) NOT NULL,
	[ZZid] [varchar](50) NULL,
	[ZZnm] [varchar](500) NULL,
	[ZZgrp] [varchar](500) NULL,
	[ZZapr] [varchar](500) NULL,
	[ZZsts] [varchar](500) NULL,
 CONSTRAINT [PK_vsp_carrier] PRIMARY KEY CLUSTERED 
(
	[vsp_carrier_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[vsp_authorizations]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[vsp_authorizations](
	[auth_id] [int] IDENTITY(1,1) NOT NULL,
	[contactid] [int] NULL,
	[auth_aid] [int] NULL,
	[auth_aefd_date] [datetime] NULL,
	[auth_aexd_date] [datetime] NULL,
	[auth_mid] [varchar](300) NULL,
	[auth_prd] [varchar](200) NULL,
	[auth_bnm] [varchar](500) NULL,
	[auth_snm] [varchar](500) NULL,
	[auth_issue_date] [datetime] NULL,
	[auth_response_text] [text] NULL,
	[valid] [varchar](3) NULL,
	[locationid] [int] NULL,
	[vsp_bfra] [smallmoney] NULL,
	[vsp_bflc] [varchar](2) NULL,
	[vsp_manm] [varchar](2) NULL,
	[vsp_mat] [varchar](10) NULL,
	[vsp_maa] [smallmoney] NULL,
	[vsp_denm] [varchar](10) NULL,
	[vsp_dea] [smallmoney] NULL,
	[vsp_pfrl] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[copay_exam] [money] NULL,
	[copay_material] [money] NULL,
	[copay_total] [money] NULL,
	[copay_lens] [money] NULL,
	[copay_frame] [money] NULL,
	[insurance_id] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[TimeZoneList]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[TimeZoneList](
	[TimeZoneid] [int] NOT NULL,
	[TimeZoneBais] [decimal](4, 2) NULL,
	[TimeZoneDesc] [varchar](100) NULL,
	[Begin_DayLightSavings] [datetime] NULL,
	[End_DayLightSavings] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TimeZoneid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[timeclockHistory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[timeclockHistory](
	[timeclockHistoryid] [int] IDENTITY(1,1) NOT NULL,
	[timeclockid] [int] NOT NULL,
	[employeeid] [int] NOT NULL,
	[timedate] [datetime] NULL,
	[timein] [datetime] NULL,
	[timeout] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lunchout] [datetime] NULL,
	[lunchin] [datetime] NULL,
	[notes] [varchar](300) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[dinner_in] [datetime] NULL,
	[dinner_out] [datetime] NULL,
	[lunch_in_2] [datetime] NULL,
	[lunch_out_2] [datetime] NULL,
	[txnid] [varchar](50) NULL,
	[transfer_flag] [int] NULL,
	[locationid] [int] NULL,
	[remarks] [varchar](100) NULL,
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
 CONSTRAINT [PK_timeclockhistory] PRIMARY KEY CLUSTERED 
(
	[timeclockHistoryid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[taxonomy_codes]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[taxonomy_codes](
	[CMS] [int] NULL,
	[SPECIALTY] [varchar](255) NULL,
	[taxonomy_code] [varchar](20) NULL,
	[DESCRIPTION_TYPE] [varchar](255) NULL,
	[CLASSIFICATION_SPECIALIZATION] [varchar](255) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[tax]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[tax](
	[taxid] [int] NOT NULL,
	[taxdesc] [varchar](50) NOT NULL,
	[taxrate] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_tax] PRIMARY KEY CLUSTERED 
(
	[taxid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[systemsettings]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[systemsettings](
	[systemsettingsid] [int] IDENTITY(1,1) NOT NULL,
	[systemdefined] [char](1) NULL,
	[date_format] [varchar](20) NULL,
	[phone_format] [varchar](20) NULL,
	[country] [char](1) NULL,
	[installationdt] [datetime] NULL,
	[country_name] [varchar](50) NULL,
	[backupreminddt] [datetime] NULL,
	[taxinclusive] [varchar](3) NULL,
	[settings1] [varchar](50) NULL,
	[settings2] [varchar](50) NULL,
	[settings3] [varchar](50) NULL,
	[settings4] [varchar](50) NULL,
	[settings5] [varchar](50) NULL,
	[add_inventory_all_location] [char](1) NULL,
	[clearinghouse] [varchar](35) NULL,
	[interchangesenderid] [varchar](15) NULL,
	[interchangereceiverid] [varchar](15) NULL,
	[appsendercode] [varchar](15) NULL,
	[appreceivercode] [varchar](15) NULL,
	[claimdefault] [char](1) NULL,
	[ansi837_isa07] [char](2) NULL,
	[ansi837_isa14] [char](1) NULL,
	[setting_01] [varchar](100) NULL,
	[setting_02] [varchar](100) NULL,
	[setting_03] [varchar](100) NULL,
	[setting_04] [varchar](100) NULL,
	[setting_05] [varchar](100) NULL,
	[setting_06] [varchar](100) NULL,
	[setting_07] [varchar](100) NULL,
	[setting_08] [varchar](100) NULL,
	[setting_09] [varchar](100) NULL,
	[setting_10] [varchar](100) NULL,
	[setting_11] [varchar](100) NULL,
	[setting_12] [varchar](100) NULL,
	[setting_13] [varchar](100) NULL,
	[setting_14] [varchar](100) NULL,
	[setting_15] [varchar](100) NULL,
	[setting_16] [varchar](100) NULL,
	[setting_17] [varchar](100) NULL,
	[setting_18] [varchar](100) NULL,
	[setting_19] [varchar](100) NULL,
	[setting_20] [varchar](100) NULL,
	[encryption_conversion] [varchar](10) NULL,
	[FPCFolder] [varchar](5000) NULL,
	[FPCExportTime] [int] NULL,
	[FPCAppointments] [varchar](3) NULL,
	[FPCRecalls] [varchar](3) NULL,
	[FPCNotifications] [varchar](3) NULL,
	[FPCOrderStatus] [int] NULL,
	[FPCRecallsExportTime] [datetime] NULL,
 CONSTRAINT [PK_systemsettings] PRIMARY KEY CLUSTERED 
(
	[systemsettingsid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[systempreferences]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING OFF

CREATE TABLE [dbo].[systempreferences](
	[objectname] [varchar](128) NOT NULL,
	[attributename] [varchar](128) NOT NULL,
	[attributeid] [decimal](10, 0) NOT NULL,
	[attributevalue] [varchar](128) NULL,
	[attributexpos] [decimal](10, 0) NULL,
	[attributewidth] [decimal](10, 0) NULL,
	[attributevisible] [int] NULL,
	[attributekey] [int] NULL,
 CONSTRAINT [systempreferences_pk] PRIMARY KEY CLUSTERED 
(
	[objectname] ASC,
	[attributename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[suppliers]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[suppliers](
	[supplierid] [int] NOT NULL,
	[suppliertype] [char](1) NULL,
	[suppliername] [varchar](50) NULL,
	[contactname] [varchar](50) NULL,
	[contacttitle] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](30) NULL,
	[postalcode] [varchar](10) NULL,
	[stateorprovince] [varchar](20) NULL,
	[phonenumber] [varchar](20) NULL,
	[faxnumber] [varchar](20) NULL,
	[paymentterms] [varchar](50) NULL,
	[accountno] [varchar](30) NULL,
	[emailaddress] [varchar](100) NULL,
	[notes] [varchar](500) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[frames] [char](1) NULL,
	[lenses] [char](1) NULL,
	[contacts] [char](1) NULL,
	[other] [char](1) NULL,
	[lab] [char](1) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[vsp_code] [varchar](20) NULL,
	[locationid] [int] NULL,
	[bill_account] [varchar](50) NULL,
	[ship_account] [varchar](50) NULL,
	[vw_login] [varchar](50) NULL,
	[vw_password] [varchar](50) NULL,
	[vw_flag] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
 CONSTRAINT [PK_SUPPLIERS] PRIMARY KEY CLUSTERED 
(
	[supplierid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[status_tracking]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[status_tracking](
	[status_tracking_id] [int] NOT NULL,
	[status_type] [varchar](100) NULL,
	[status_id] [int] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
 CONSTRAINT [PK_status_tracking] PRIMARY KEY CLUSTERED 
(
	[status_tracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[stamp]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[stamp](
	[stampid] [int] IDENTITY(1,1) NOT NULL,
	[stamp_name] [varchar](100) NULL,
	[stamp_type] [varchar](50) NULL,
	[stamp_text] [varchar](1000) NULL,
	[stamp_image_file] [image] NULL,
	[sort_order] [smallint] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[font_name] [varchar](50) NULL,
	[font_bold] [char](1) NULL,
	[font_size] [smallint] NULL,
	[font_strikeout] [char](1) NULL,
	[font_underline] [char](1) NULL,
	[font_color] [int] NULL,
	[font_italic] [char](1) NULL,
	[otherid_1] [int] NULL,
	[otherid_2] [int] NULL,
	[otherid_3] [int] NULL,
	[diagnosis_cd_1] [varchar](50) NULL,
	[diagnosis_cd_2] [varchar](50) NULL,
	[diagnosis_cd_3] [varchar](50) NULL,
 CONSTRAINT [PK_stamp] PRIMARY KEY CLUSTERED 
(
	[stampid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectaclelens_pricing_prism_add]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectaclelens_pricing_prism_add](
	[spectaclelens_pricing_prism_add_id] [int] IDENTITY(1,1) NOT NULL,
	[pricing_type] [varchar](50) NOT NULL,
	[sort_order] [int] NULL,
	[range_start] [decimal](5, 3) NULL,
	[range_end] [decimal](5, 3) NULL,
	[price] [money] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_spectaclelens_pricing_prism_add] PRIMARY KEY CLUSTERED 
(
	[spectaclelens_pricing_prism_add_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[physician]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[physician](
	[physicianid] [int] NOT NULL,
	[physicianname] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[phone] [varchar](20) NULL,
	[fax] [varchar](20) NULL,
	[emailname] [varchar](45) NULL,
	[upinnumber] [varchar](10) NULL,
	[medicarenumber] [varchar](10) NULL,
	[medicaidnumber] [varchar](10) NULL,
	[vspnumber] [varchar](10) NULL,
	[groupnumber] [varchar](10) NULL,
	[notes] [varchar](1000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[inhouse] [varchar](3) NULL,
	[colorcode] [int] NULL,
	[active_flag] [varchar](3) NULL,
	[firstname] [varchar](50) NULL,
	[degree] [varchar](50) NULL,
	[locationid] [int] NULL,
	[singlelocation] [varchar](3) NULL,
	[bluecrossnumber] [varchar](50) NULL,
	[deanumber] [varchar](50) NULL,
	[licensenumber] [varchar](50) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[npi_number] [varchar](50) NULL,
	[taxonomy_code] [varchar](20) NULL,
	[tax_number] [varchar](20) NULL,
	[tax_ein_or_ssn] [varchar](10) NULL,
	[champus_1H] [varchar](50) NULL,
	[ein_ei] [varchar](50) NULL,
	[commercial_number_g2] [varchar](50) NULL,
	[location_number_lu] [varchar](50) NULL,
	[plan_network_identification_n5] [varchar](50) NULL,
	[ssn_sy] [varchar](50) NULL,
	[state_industrial_accident_number] [varchar](50) NULL,
	[provider_id_default] [int] NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[sort_order] [int] NULL,
	[signature_image] [image] NULL,
	[preappoint_flag] [varchar](3) NULL,
	[default_group_provider] [int] NULL,
 CONSTRAINT [PK_Physician] PRIMARY KEY CLUSTERED 
(
	[physicianid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pbcatvld]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pbcatvld](
	[pbv_name] [varchar](30) NOT NULL,
	[pbv_vald] [varchar](254) NULL,
	[pbv_type] [smallint] NULL,
	[pbv_cntr] [int] NULL,
	[pbv_msg] [varchar](254) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pbcattbl]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pbcattbl](
	[pbt_tnam] [varchar](129) NOT NULL,
	[pbt_tid] [int] NULL,
	[pbt_ownr] [varchar](129) NOT NULL,
	[pbd_fhgt] [smallint] NULL,
	[pbd_fwgt] [smallint] NULL,
	[pbd_fitl] [varchar](1) NULL,
	[pbd_funl] [varchar](1) NULL,
	[pbd_fchr] [smallint] NULL,
	[pbd_fptc] [smallint] NULL,
	[pbd_ffce] [varchar](18) NULL,
	[pbh_fhgt] [smallint] NULL,
	[pbh_fwgt] [smallint] NULL,
	[pbh_fitl] [varchar](1) NULL,
	[pbh_funl] [varchar](1) NULL,
	[pbh_fchr] [smallint] NULL,
	[pbh_fptc] [smallint] NULL,
	[pbh_ffce] [varchar](18) NULL,
	[pbl_fhgt] [smallint] NULL,
	[pbl_fwgt] [smallint] NULL,
	[pbl_fitl] [varchar](1) NULL,
	[pbl_funl] [varchar](1) NULL,
	[pbl_fchr] [smallint] NULL,
	[pbl_fptc] [smallint] NULL,
	[pbl_ffce] [varchar](18) NULL,
	[pbt_cmnt] [varchar](254) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pbcatfmt]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pbcatfmt](
	[pbf_name] [varchar](30) NOT NULL,
	[pbf_frmt] [varchar](254) NULL,
	[pbf_type] [smallint] NULL,
	[pbf_cntr] [int] NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pbcatedt]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pbcatedt](
	[pbe_name] [varchar](30) NOT NULL,
	[pbe_edit] [varchar](254) NULL,
	[pbe_type] [smallint] NULL,
	[pbe_cntr] [int] NULL,
	[pbe_seqn] [smallint] NOT NULL,
	[pbe_flag] [int] NULL,
	[pbe_work] [varchar](32) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pbcatcol]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pbcatcol](
	[pbc_tnam] [varchar](129) NOT NULL,
	[pbc_tid] [int] NULL,
	[pbc_ownr] [varchar](129) NOT NULL,
	[pbc_cnam] [varchar](129) NOT NULL,
	[pbc_cid] [smallint] NULL,
	[pbc_labl] [varchar](254) NULL,
	[pbc_lpos] [smallint] NULL,
	[pbc_hdr] [varchar](254) NULL,
	[pbc_hpos] [smallint] NULL,
	[pbc_jtfy] [smallint] NULL,
	[pbc_mask] [varchar](31) NULL,
	[pbc_case] [smallint] NULL,
	[pbc_hght] [smallint] NULL,
	[pbc_wdth] [smallint] NULL,
	[pbc_ptrn] [varchar](31) NULL,
	[pbc_bmap] [varchar](1) NULL,
	[pbc_init] [varchar](254) NULL,
	[pbc_cmnt] [varchar](254) NULL,
	[pbc_edit] [varchar](31) NULL,
	[pbc_tag] [varchar](254) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[paymentsHistory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[paymentsHistory](
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
	[audit_paymentsid] [int] IDENTITY(1,1) NOT NULL,
	[paymentid] [int] NOT NULL,
	[orderid] [int] NOT NULL,
	[employeeid] [int] NOT NULL,
	[paymentdate] [datetime] NOT NULL,
	[paymentmethodid] [int] NULL,
	[creditcardnumber] [varchar](30) NULL,
	[creditcardexpdate] [datetime] NULL,
	[referencenum] [varchar](300) NULL,
	[paymentamount] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[paymenttype] [varchar](50) NOT NULL,
	[locationid] [int] NULL,
	[adjustmenttype] [varchar](50) NULL,
	[printonstatement] [char](1) NULL,
	[insuranceid] [int] NULL,
	[paymentdescription] [varchar](100) NULL,
	[paymentmethod] [varchar](100) NULL,
	[employee] [varchar](50) NULL,
	[balanceeffect] [varchar](30) NULL,
	[insuranceid_from] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[transfer_flag] [int] NULL,
	[approvalcode] [varchar](50) NULL,
	[ccname] [varchar](100) NULL,
	[cctype] [varchar](100) NULL,
	[ccswiped] [varchar](50) NULL,
	[txnid] [varchar](36) NULL,
	[check_number] [varchar](100) NULL,
	[refund_reason] [varchar](100) NULL,
	[payment_batch_id] [int] NULL,
	[payment_location] [varchar](200) NULL,
	[user_defined_13] [varchar](100) NULL,
	[user_defined_12] [varchar](100) NULL,
	[user_defined_11] [varchar](100) NULL,
	[user_defined_10] [varchar](100) NULL,
	[user_defined_9] [varchar](100) NULL,
	[user_defined_8] [varchar](100) NULL,
	[user_defined_7] [varchar](100) NULL,
	[user_defined_6] [varchar](100) NULL,
	[user_defined_5] [varchar](100) NULL,
	[user_defined_4] [varchar](100) NULL,
	[user_defined_3] [varchar](100) NULL,
	[user_defined_2] [varchar](100) NULL,
	[user_defined_1] [varchar](100) NULL,
	[user_defined_20] [varchar](100) NULL,
	[user_defined_19] [varchar](100) NULL,
	[user_defined_18] [varchar](100) NULL,
	[user_defined_17] [varchar](100) NULL,
	[user_defined_16] [varchar](100) NULL,
	[user_defined_15] [varchar](100) NULL,
	[user_defined_14] [varchar](100) NULL,
	[cash_tendered] [money] NULL,
	[change_due] [money] NULL,
	[payments_header_id] [int] NULL,
	[computer_name] [varchar](500) NULL,
	[signature_blob] [int] NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[paymentsDetailHistory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[paymentsDetailHistory](
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
	[audit_paymentsdetailid] [int] IDENTITY(1,1) NOT NULL,
	[paymentsdetailid] [int] NULL,
	[paymentid] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[OrderItemsID] [int] NULL,
	[OrderItems_Detail_ID] [int] NULL,
	[procedure_code] [varchar](50) NULL,
	[orderid] [int] NULL,
	[paymentamount] [money] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastlocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[audit_paymentsdetailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[paymentsDetail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[paymentsDetail](
	[paymentsdetailid] [int] IDENTITY(1,1) NOT NULL,
	[paymentid] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[OrderItemsID] [int] NULL,
	[OrderItems_Detail_ID] [int] NULL,
	[procedure_code] [varchar](50) NULL,
	[orderid] [int] NULL,
	[paymentamount] [money] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastlocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[paymentsdetailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[payments_header_history]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[payments_header_history](
	[payments_header_history_id] [int] NOT NULL,
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
	[payments_header_id] [int] NOT NULL,
	[batch_payments_id] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [date] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[created_via_script] [varchar](3) NULL,
	[employeeid] [int] NULL,
	[paymentdate] [datetime] NULL,
	[paymentmethodid] [int] NULL,
	[creditcardnumber] [varchar](30) NULL,
	[creditcardexpdate] [datetime] NULL,
	[referencenum] [varchar](300) NULL,
	[paymentamount] [money] NULL,
	[paymenttype] [varchar](50) NULL,
	[locationid] [int] NULL,
	[adjustmenttype] [varchar](50) NULL,
	[printonstatement] [char](1) NULL,
	[insuranceid] [int] NULL,
	[paymentdescription] [varchar](100) NULL,
	[paymentmethod] [varchar](100) NULL,
	[employee] [varchar](50) NULL,
	[balanceeffect] [varchar](30) NULL,
	[transfer_flag] [int] NULL,
	[approvalcode] [varchar](50) NULL,
	[ccname] [varchar](100) NULL,
	[cctype] [varchar](100) NULL,
	[ccswiped] [varchar](50) NULL,
	[txnid] [varchar](36) NULL,
	[check_number] [varchar](100) NULL,
	[refund_reason] [varchar](100) NULL,
	[payment_location] [varchar](200) NULL,
	[cash_tendered] [money] NULL,
	[change_due] [money] NULL,
 CONSTRAINT [PK_payments_header_history] PRIMARY KEY CLUSTERED 
(
	[payments_header_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[payments_header]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[payments_header](
	[payments_header_id] [int] NOT NULL,
	[posting_date] [datetime] NULL,
	[batch_payments_id] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[created_via_script] [varchar](3) NULL,
	[employeeid] [int] NULL,
	[paymentdate] [datetime] NULL,
	[paymentmethodid] [int] NULL,
	[creditcardnumber] [varchar](30) NULL,
	[creditcardexpdate] [datetime] NULL,
 CONSTRAINT [PK_payments_header] PRIMARY KEY CLUSTERED 
(
	[payments_header_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[otherinventory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[otherinventory](
	[otherid] [int] NOT NULL,
	[detailed_domain_id] [int] NULL,
	[categoryid] [int] NULL,
	[name] [varchar](300) NULL,
	[quantitysold] [int] NULL,
	[quantitypurchased] [int] NULL,
	[retailprice] [money] NULL,
	[cost] [money] NULL,
	[taxable] [varchar](3) NULL,
	[notes] [varchar](2000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[upccode] [varchar](12) NULL,
	[structure_id] [int] NULL,
	[structure_type] [varchar](1) NULL,
	[gross_percentage] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[calendarappointment] [varchar](3) NULL,
	[calendarduration] [int] NULL,
	[deleted] [varchar](3) NULL,
	[inventory] [varchar](3) NULL,
	[colorcode] [int] NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[vsp_code] [varchar](10) NULL,
	[recallreasonid] [int] NULL,
	[recallreasonid2] [int] NULL,
	[sort_order] [int] NULL,
	[send_to_lab_flag] [varchar](3) NULL,
	[placeofservice] [varchar](50) NULL,
 CONSTRAINT [PK_OtherInventory] PRIMARY KEY CLUSTERED 
(
	[otherid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orderslink]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orderslink](
	[orderslinkid] [int] IDENTITY(1,1) NOT NULL,
	[feeslipid] [int] NOT NULL,
	[orderid] [int] NOT NULL,
	[primaryorder] [varchar](3) NULL,
 CONSTRAINT [PK_orderslink] PRIMARY KEY CLUSTERED 
(
	[orderslinkid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orders_print]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orders_print](
	[orders_print_id] [int] IDENTITY(1,1) NOT NULL,
	[orderid] [int] NOT NULL,
	[locationname] [varchar](100) NULL,
 CONSTRAINT [PK_orders_print] PRIMARY KEY CLUSTERED 
(
	[orders_print_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orderitemsHistory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orderitemsHistory](
	[orderitemsid] [int] NOT NULL,
	[orderitemsdate] [datetime] NOT NULL,
	[orderid] [int] NOT NULL,
	[itemtype] [char](1) NULL,
	[itemid] [int] NULL,
	[description] [varchar](200) NULL,
	[retailprice] [money] NULL,
	[quantity] [int] NULL,
	[taxable] [varchar](3) NULL,
	[tax] [money] NULL,
	[tax2] [money] NULL,
	[notes] [varchar](255) NULL,
	[lenstreatmentind] [varchar](1) NULL,
	[orderitemslink] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[commission_amt] [money] NULL,
	[commission_type] [varchar](1) NULL,
	[commission_spiff] [money] NULL,
	[employeeid] [int] NOT NULL,
	[fulldescription] [varchar](120) NULL,
	[supplierid] [int] NULL,
	[ownframe] [varchar](3) NULL,
	[itemcost] [money] NULL,
	[discount] [money] NULL,
	[discount_reasonid] [int] NULL,
	[insurancedue] [money] NULL,
	[insurancepaid] [money] NULL,
	[discount_rate] [money] NULL,
	[discount_type] [varchar](1) NULL,
	[insurance_rate] [money] NULL,
	[taxrate] [money] NULL,
	[taxrate2] [money] NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[print_receipt_flag] [varchar](3) NULL,
	[print_hcfa_flag] [varchar](3) NULL,
	[diagnosis_code] [varchar](10) NULL,
	[copay] [money] NULL,
	[trans_close_dt] [datetime] NULL,
	[insuranceid] [int] NULL,
	[insuranceplanid] [int] NULL,
	[original_retailprice] [money] NULL,
	[procedure_code] [varchar](20) NULL,
	[patient_pays] [money] NULL,
	[vcode_1] [int] NULL,
	[vcode_2] [int] NULL,
	[vcode_3] [int] NULL,
	[vcode_4] [int] NULL,
	[vcode_1_patient] [money] NULL,
	[vcode_2_patient] [money] NULL,
	[vcode_3_patient] [money] NULL,
	[vcode_4_patient] [money] NULL,
	[vcode_1_covered] [varchar](1) NULL,
	[vcode_2_covered] [varchar](1) NULL,
	[vcode_3_covered] [varchar](1) NULL,
	[vcode_4_covered] [varchar](1) NULL,
	[vcode_1_patient_orig] [money] NULL,
	[vcode_2_patient_orig] [money] NULL,
	[vcode_3_patient_orig] [money] NULL,
	[vcode_4_patient_orig] [money] NULL,
	[transfer_flag] [int] NULL,
	[days_units] [smallint] NULL,
	[modifier] [varchar](50) NULL,
	[exam_item_id] [int] NULL,
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
	[insurance_supplied] [varchar](1) NULL,
	[write_off_amount] [money] NULL,
	[overage_to_v20205] [varchar](1) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[patient_recalls]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[patient_recalls](
	[patient_recalls_id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NULL,
	[recall_reason_id] [int] NULL,
	[recall_dt] [datetime] NULL,
	[recall_type] [varchar](50) NULL,
	[recall_letter_name] [varchar](50) NULL,
	[letter_status] [varchar](50) NULL,
	[recall_method] [varchar](50) NULL,
	[updated_dt] [datetime] NULL,
	[letter_id] [int] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_patient_recalls] PRIMARY KEY CLUSTERED 
(
	[patient_recalls_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[pqri]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[pqri](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pqri_code] [varchar](50) NOT NULL,
	[diagnosis_code] [varchar](50) NOT NULL,
 CONSTRAINT [PK_pqri] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[place0fservice]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[place0fservice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](10) NOT NULL,
	[name] [varchar](100) NULL,
	[description] [varchar](1000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
 CONSTRAINT [PK_place0fservice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[settings_internet]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[settings_internet](
	[settings_internet_id] [int] NOT NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastlocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[mycompanyinformationid] [int] NULL,
	[smtp_server] [varchar](100) NULL,
	[smtp_port] [int] NULL,
	[smtp_timeout] [int] NULL,
	[smtp_require_ssa] [int] NULL,
	[smtp_encryption_type] [varchar](10) NULL,
	[smtp_use_emp_email_as_from] [varchar](10) NULL,
	[smtp_reply_email] [varchar](100) NULL,
	[smtp_email_signature_text] [text] NULL,
	[default_from_address] [varchar](100) NULL,
	[default_from_password] [varchar](100) NULL,
	[proxy_use_global_proxy_config] [varchar](1) NULL,
	[proxy_use_personal_proxy_server] [varchar](1) NULL,
	[proxy_type] [varchar](10) NULL,
	[proxy_address] [varchar](100) NULL,
	[proxy_port] [int] NULL,
	[proxy_socks_proxy_type] [varchar](10) NULL,
	[proxy_username] [varchar](100) NULL,
	[proxy_password] [varchar](100) NULL,
	[ftp_ch_host_name] [varchar](100) NULL,
	[ftp_ch_port] [int] NULL,
	[ftp_ch_account] [varchar](100) NULL,
	[ftp_ch_user_name] [varchar](100) NULL,
	[ftp_ch_password] [varchar](100) NULL,
	[ftp_ch_connection_type] [varchar](10) NULL,
	[ftp_ch_initial_remote_dir] [varchar](100) NULL,
	[ftp_ch_server_type] [varchar](100) NULL,
	[custom_field_01] [varchar](50) NULL,
	[custom_field_02] [varchar](50) NULL,
	[custom_field_03] [varchar](50) NULL,
	[custom_field_04] [varchar](50) NULL,
	[custom_field_05] [varchar](50) NULL,
	[custom_field_06] [varchar](50) NULL,
	[custom_field_07] [varchar](50) NULL,
	[custom_field_08] [varchar](50) NULL,
	[custom_field_09] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[send_claims_via_ftp] [varchar](10) NULL,
	[email_use_ssl] [varchar](10) NULL,
	[patient_eligibility_username] [varchar](100) NULL,
	[patient_eligibility_password] [varchar](100) NULL,
 CONSTRAINT [PK_dbo_settings_internet_1] PRIMARY KEY NONCLUSTERED 
(
	[settings_internet_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[settings]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[settings](
	[settingid] [int] IDENTITY(1,1) NOT NULL,
	[setting_name] [varchar](200) NULL,
	[setting_value] [varchar](500) NULL,
	[setting_type] [varchar](50) NULL,
 CONSTRAINT [PK_settings] PRIMARY KEY CLUSTERED 
(
	[settingid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[session_parameters]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[session_parameters](
	[session_parametersID] [int] IDENTITY(1,1) NOT NULL,
	[spid] [smallint] NOT NULL,
	[employeeid] [int] NOT NULL,
	[employeename] [varchar](50) NULL,
	[computer_name] [varchar](500) NULL,
	[locationid] [int] NULL,
	[computer_user] [varchar](100) NULL,
 CONSTRAINT [PK_session_parameters] PRIMARY KEY CLUSTERED 
(
	[session_parametersID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_session_parameters] UNIQUE NONCLUSTERED 
(
	[spid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[serviceagreement]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[serviceagreement](
	[serviceagreementid] [int] NOT NULL,
	[planname] [varchar](50) NULL,
	[coveragedays] [smallint] NULL,
	[renewalnoticeday] [smallint] NULL,
	[cost] [money] NULL,
	[lensreplacementcostleft] [money] NULL,
	[lensreplacementcostright] [money] NULL,
	[lensreplacementcostdeductible] [money] NULL,
	[discountgalssesamount] [money] NULL,
	[discountgalssespercentage] [money] NULL,
	[discountexaminationamount] [money] NULL,
	[discountexaminationpercent] [money] NULL,
	[discountprogresschecksamount] [money] NULL,
	[discountprogresscheckspercent] [money] NULL,
	[discountprogresschecksfree] [smallint] NULL,
	[lenstyperight] [varchar](30) NULL,
	[lenstypeleft] [varchar](30) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[sequences]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[sequences](
	[table_name] [varchar](50) NOT NULL,
	[sequence] [int] NOT NULL,
 CONSTRAINT [PK_Sequences] PRIMARY KEY CLUSTERED 
(
	[table_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[security_template]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[security_template](
	[application] [varchar](40) NOT NULL,
	[window] [varchar](64) NOT NULL,
	[control] [varchar](128) NOT NULL,
	[description] [varchar](254) NULL,
	[object_type] [varchar](24) NULL,
 CONSTRAINT [PK_security_template] PRIMARY KEY CLUSTERED 
(
	[application] ASC,
	[window] ASC,
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[security_function_grouping]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[security_function_grouping](
	[security_function_grouping_id] [int] IDENTITY(1,1) NOT NULL,
	[application] [varchar](40) NULL,
	[window] [varchar](64) NULL,
	[control] [varchar](128) NULL,
	[function_id] [int] NULL,
 CONSTRAINT [PK_security_function_grouping] PRIMARY KEY CLUSTERED 
(
	[security_function_grouping_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[security_function]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[security_function](
	[function_id] [int] NOT NULL,
	[description] [varchar](244) NULL,
	[sortorder] [int] NULL,
	[country] [varchar](1) NULL,
	[parentid] [int] NULL,
	[application] [varchar](40) NULL,
	[window] [varchar](64) NULL,
	[control] [varchar](128) NULL,
	[OBJECT_TYPE] [varchar](40) NULL,
	[window_name] [varchar](200) NULL,
 CONSTRAINT [PK_security_function] PRIMARY KEY CLUSTERED 
(
	[function_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectaclelens_pricing_add]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectaclelens_pricing_add](
	[spectaclelens_pricing_add_id] [int] IDENTITY(1,1) NOT NULL,
	[pricing_type] [varchar](50) NOT NULL,
	[sort_order] [int] NULL,
	[range_start] [decimal](5, 3) NULL,
	[range_end] [decimal](5, 3) NULL,
	[price] [money] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_spectaclelens_pricing_add] PRIMARY KEY CLUSTERED 
(
	[spectaclelens_pricing_add_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectaclelens_pricing]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectaclelens_pricing](
	[spectaclelens_pricing_id] [int] IDENTITY(1,1) NOT NULL,
	[grid_name] [varchar](100) NULL,
	[active_flag] [varchar](3) NULL,
	[default_grid] [varchar](3) NULL,
 CONSTRAINT [PK_spectaclelens_pricing] PRIMARY KEY CLUSTERED 
(
	[spectaclelens_pricing_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[recallreasons]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[recallreasons](
	[recallreasonid] [int] NOT NULL,
	[recallreason] [varchar](100) NULL,
	[recallmonths] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_recallreasons] PRIMARY KEY CLUSTERED 
(
	[recallreasonid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[provider]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[provider](
	[providerid] [int] NOT NULL,
	[providername] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](30) NULL,
	[state] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[phone] [varchar](20) NULL,
	[fax] [varchar](20) NULL,
	[federaltaxid] [varchar](9) NULL,
	[notes] [varchar](1000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[ssn] [varchar](9) NULL,
	[hcfa_31] [varchar](3) NULL,
	[hcfa_20_outsidelab] [varchar](3) NULL,
	[actuve_flag] [varchar](3) NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[taxonomy_code] [varchar](20) NULL,
	[npi_number] [varchar](50) NULL,
	[vsp_rid] [varchar](50) NULL,
	[vsp_rpw] [varchar](50) NULL,
	[vsp_oid] [varchar](50) NULL,
	[champus_1H] [varchar](50) NULL,
	[commercial_number_g2] [varchar](50) NULL,
	[location_number_lu] [varchar](50) NULL,
	[plan_network_identification_n5] [varchar](50) NULL,
	[state_industrial_accident_number] [varchar](50) NULL,
	[state_license_number_ob] [varchar](50) NULL,
	[blue_shield_1b] [varchar](50) NULL,
	[medicare_1c] [varchar](50) NULL,
	[medicaid_1d] [varchar](50) NULL,
	[provider_upin] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[default_facility_id] [int] NULL,
	[federal_tax_type] [varchar](3) NULL,
	[signature_on_file_text] [char](40) NULL,
 CONSTRAINT [PK_Provider] PRIMARY KEY CLUSTERED 
(
	[providerid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[qb_summaryaccountgroupdetails]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[qb_summaryaccountgroupdetails](
	[groupid] [int] NOT NULL,
	[accountid] [int] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [qb_summaryaccountgroupdetails_pk] PRIMARY KEY CLUSTERED 
(
	[groupid] ASC,
	[accountid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[qb_summaryaccountgroup]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[qb_summaryaccountgroup](
	[groupid] [int] IDENTITY(1,1) NOT NULL,
	[groupname] [varchar](50) NOT NULL,
	[groupmemo] [varchar](100) NULL,
	[depositto] [varchar](36) NULL,
	[accountfrom] [varchar](36) NULL,
	[paymethod] [varchar](36) NULL,
	[status] [int] NOT NULL,
	[locationid] [int] NULL,
 CONSTRAINT [qb_summaryaccountgroup_pk] PRIMARY KEY CLUSTERED 
(
	[groupid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[qb_summaryaccount]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[qb_summaryaccount](
	[accountid] [int] NOT NULL,
	[accountname] [varchar](30) NOT NULL,
	[accounttype] [int] NOT NULL,
	[sqlstatement] [varchar](1024) NULL,
	[status] [int] NULL,
 CONSTRAINT [qb_summaryofaccount_pk] PRIMARY KEY CLUSTERED 
(
	[accountid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[qb_locationclassmap]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[qb_locationclassmap](
	[locationid] [int] NOT NULL,
	[classid] [varchar](36) NOT NULL,
	[status] [int] NULL,
 CONSTRAINT [qb_locationclassmap_pk] PRIMARY KEY CLUSTERED 
(
	[locationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[qb_dailysheet]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[qb_dailysheet](
	[dailysheet_transid] [int] IDENTITY(1,1) NOT NULL,
	[dailysheet_accountid] [int] NOT NULL,
	[dailysheet_fromdt] [datetime] NOT NULL,
	[dailysheet_transdt] [datetime] NOT NULL,
	[dailysheet_amount] [decimal](20, 2) NOT NULL,
	[txnid] [varchar](36) NULL,
	[dailysheet_status] [int] NULL,
	[dailysheet_depositto] [varchar](36) NULL,
	[dailysheet_accountfrom] [varchar](36) NULL,
	[dailysheet_paymethod] [varchar](36) NULL,
	[dailysheet_groupid] [int] NULL,
	[dailysheet_classid] [varchar](36) NULL,
	[dailysheet_locationid] [int] NULL,
 CONSTRAINT [PK__qb_dailysheet__0BD1B136] PRIMARY KEY CLUSTERED 
(
	[dailysheet_transid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[security_apps]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[security_apps](
	[application] [varchar](40) NOT NULL,
	[description] [varchar](64) NULL,
	[version] [varchar](100) NULL,
 CONSTRAINT [PK_security_apps] PRIMARY KEY CLUSTERED 
(
	[application] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[required_fields]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[required_fields](
	[required_fields_id] [int] IDENTITY(1,1) NOT NULL,
	[field_source] [varchar](50) NULL,
	[field_column_name] [varchar](50) NULL,
	[field_display_name] [varchar](50) NULL,
	[field_required_flag] [varchar](3) NULL,
	[field_sort_order] [int] NULL,
	[system_defined] [varchar](3) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_required_fields] PRIMARY KEY CLUSTERED 
(
	[required_fields_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[reports]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[reports](
	[reportid] [int] IDENTITY(1,1) NOT NULL,
	[report_cd] [varchar](50) NULL,
	[report_name] [varchar](80) NOT NULL,
	[dataobject_search] [varchar](50) NULL,
	[dataobject_results] [varchar](50) NULL,
	[labelid] [int] NULL,
	[whereclausetable] [varchar](50) NULL,
	[whereclauseappend] [varchar](1) NULL,
	[groupby] [varchar](2000) NULL,
	[dataobject_composite] [varchar](50) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[printer_name] [varchar](200) NULL,
	[report_description] [varchar](500) NULL,
	[location_required_flag] [varchar](3) NULL,
	[search_criteria] [image] NULL,
	[sort_criteria] [varchar](200) NULL,
	[menu_name] [varchar](50) NULL,
	[is_emailable] [varchar](10) NULL,
 CONSTRAINT [PK_reports] PRIMARY KEY CLUSTERED 
(
	[reportid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[report_group_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[report_group_detail](
	[report_group_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[report_group_id] [int] NOT NULL,
	[report_cd] [varchar](50) NOT NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_report_group_detail] PRIMARY KEY CLUSTERED 
(
	[report_group_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[report_group]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[report_group](
	[report_group_id] [int] IDENTITY(1,1) NOT NULL,
	[group_name] [varchar](50) NOT NULL,
	[group_description] [varchar](1000) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[trigger_start] [varchar](25) NULL,
	[email_start_date_time] [datetime] NULL,
	[auto_email] [varchar](10) NULL,
	[last_emailed] [datetime] NULL,
 CONSTRAINT [PK_report_group] PRIMARY KEY CLUSTERED 
(
	[report_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[report_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[report_detail](
	[report_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[dataobject] [varchar](500) NOT NULL,
	[display_name] [varchar](500) NULL,
	[retrieval1] [varchar](500) NULL,
	[retrieval2] [varchar](500) NULL,
	[retrieval3] [varchar](500) NULL,
	[retrieval4] [varchar](500) NULL,
	[retrieval5] [varchar](500) NULL,
	[retrieval6] [varchar](500) NULL,
	[retrieval7] [varchar](500) NULL,
	[retrieval8] [varchar](500) NULL,
	[retrieval9] [varchar](500) NULL,
	[retrieval10] [varchar](500) NULL,
	[retrieval11] [varchar](500) NULL,
	[retrieval12] [varchar](500) NULL,
	[retrieval13] [varchar](500) NULL,
	[retrieval14] [varchar](500) NULL,
	[retrieval15] [varchar](500) NULL,
	[retrieval16] [varchar](500) NULL,
	[retrieval17] [varchar](500) NULL,
	[retrieval18] [varchar](500) NULL,
	[retrieval19] [varchar](500) NULL,
	[retrieval20] [varchar](500) NULL,
	[notes] [varchar](500) NULL,
 CONSTRAINT [PK_report_detail] PRIMARY KEY CLUSTERED 
(
	[report_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_field_mappings]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_field_mappings](
	[exam_field_mapping_id] [int] IDENTITY(1,1) NOT NULL,
	[column_name] [varchar](100) NULL,
	[display_name] [varchar](100) NULL,
	[data_type] [varchar](50) NULL,
	[format] [varchar](50) NULL,
 CONSTRAINT [PK_exam_field_mappings] PRIMARY KEY CLUSTERED 
(
	[exam_field_mapping_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_field_mapping_details]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_field_mapping_details](
	[exam_field_mapping_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[column_name] [varchar](100) NULL,
	[list_value] [varchar](100) NULL,
	[sort_order] [smallint] NULL,
 CONSTRAINT [PK_exam_field_mapping_details] PRIMARY KEY CLUSTERED 
(
	[exam_field_mapping_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_eyemaginations]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_eyemaginations](
	[eyemagination_id] [int] IDENTITY(1,1) NOT NULL,
	[animation_name] [varchar](500) NULL,
	[file_name] [varchar](500) NULL,
	[cpt] [varchar](500) NULL,
	[icd9] [varchar](500) NULL,
	[time] [smallint] NULL,
	[custom_field] [varchar](500) NULL,
 CONSTRAINT [PK_exam_eyemaginations] PRIMARY KEY CLUSTERED 
(
	[eyemagination_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_equipment]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_equipment](
	[equipment_id] [int] IDENTITY(1,1) NOT NULL,
	[active_flag] [varchar](3) NULL,
	[manufacturer] [varchar](500) NULL,
	[device_name] [varchar](500) NULL,
	[path_to_equipment_data] [varchar](500) NULL,
	[write_data_to_file_flag] [varchar](3) NULL,
	[verify_patient_flag] [varchar](3) NULL,
	[datasource] [varchar](500) NULL,
	[comm_port] [varchar](500) NULL,
	[baud_rate] [varchar](500) NULL,
	[store_image_file_in_mve_flag] [varchar](3) NULL,
	[connector] [varchar](50) NULL,
	[synchronous] [varchar](50) NULL,
	[line] [varchar](50) NULL,
	[bit_length] [varchar](50) NULL,
	[parity_check] [varchar](50) NULL,
	[stop_bit] [varchar](50) NULL,
	[datacode] [varchar](50) NULL,
	[CR_code] [varchar](50) NULL,
	[computer_name] [varchar](500) NULL,
	[device_type] [varchar](50) NULL,
	[instructions] [varchar](500) NULL,
	[notes] [varchar](500) NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
	[custom_field_6] [varchar](500) NULL,
	[custom_field_7] [varchar](500) NULL,
	[custom_field_8] [varchar](500) NULL,
	[custom_field_9] [varchar](500) NULL,
	[custom_field_10] [varchar](500) NULL,
	[ID_number] [varchar](50) NULL,
	[equipment_program_path] [varchar](500) NULL,
	[location_id] [int] NULL,
	[flow_control] [varchar](500) NULL,
	[tx_end_byte] [varchar](50) NULL,
	[config_1] [varchar](500) NULL,
	[config_2] [varchar](500) NULL,
	[icon_img_file] [varchar](500) NULL,
	[config_3] [varchar](500) NULL,
	[capture_txt_flag] [bit] NOT NULL,
	[config_4] [varchar](500) NULL,
	[read_file_flag] [bit] NOT NULL,
 CONSTRAINT [PK_exam_equipment] PRIMARY KEY CLUSTERED 
(
	[equipment_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_education_selected]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_education_selected](
	[exam_education_selected_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NULL,
	[date_added] [datetime] NULL,
	[education_name] [varchar](500) NULL,
	[education_id] [int] NULL,
	[patient_id] [int] NULL,
	[source] [varchar](500) NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
	[custom_field_6] [varchar](500) NULL,
	[custom_field_7] [varchar](500) NULL,
	[custom_field_8] [varchar](500) NULL,
	[custom_field_9] [varchar](500) NULL,
	[custom_field_10] [varchar](500) NULL,
 CONSTRAINT [PK_exam_education_selected] PRIMARY KEY CLUSTERED 
(
	[exam_education_selected_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_education]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_education](
	[exam_education_id] [int] IDENTITY(1,1) NOT NULL,
	[education_name] [varchar](300) NULL,
	[file_link] [varchar](1000) NULL,
	[education_type] [varchar](300) NULL,
	[sort_order] [int] NULL,
	[source] [varchar](500) NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
	[custom_field_6] [varchar](500) NULL,
	[custom_field_7] [varchar](500) NULL,
	[custom_field_8] [varchar](500) NULL,
	[custom_field_9] [varchar](500) NULL,
	[custom_field_10] [varchar](500) NULL,
 CONSTRAINT [PK_exam_education] PRIMARY KEY CLUSTERED 
(
	[exam_education_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_low_vision_custom_question_details]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_low_vision_custom_question_details](
	[low_vision_custom_question_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[low_vision_custom_question_id] [int] NULL,
	[low_vision_custom_question_answer] [varchar](500) NULL,
	[sort_order] [int] NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
 CONSTRAINT [PK_exam_low_vision_custom_question_details] PRIMARY KEY CLUSTERED 
(
	[low_vision_custom_question_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_low_vision_custom_question]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_low_vision_custom_question](
	[low_vision_custom_question_id] [int] IDENTITY(1,1) NOT NULL,
	[low_vision_question_category] [varchar](500) NOT NULL,
	[low_vision_question_sectionname] [varchar](500) NULL,
	[low_vision_question_description] [varchar](500) NOT NULL,
	[sort_order] [int] NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
 CONSTRAINT [PK_exam_low_vision_custom_question] PRIMARY KEY CLUSTERED 
(
	[low_vision_custom_question_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[facility]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[facility](
	[facilityid] [int] NOT NULL,
	[facilityname] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[addressline3] [varchar](30) NULL,
	[addressline4] [varchar](30) NULL,
	[addressline5] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[phonenumber] [varchar](20) NULL,
	[faxnumber] [varchar](20) NULL,
	[emailname] [varchar](50) NULL,
	[website] [varchar](50) NULL,
	[taxnumber] [varchar](50) NULL,
	[locationid] [int] NULL,
	[NPI] [varchar](15) NULL,
	[POS] [varchar](50) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[customfield1] [varchar](150) NULL,
	[customfield2] [varchar](150) NULL,
	[customfield3] [varchar](150) NULL,
	[customfield4] [varchar](150) NULL,
	[customfield5] [varchar](150) NULL,
	[customfield6] [varchar](150) NULL,
	[customfield7] [varchar](150) NULL,
	[customfield8] [varchar](150) NULL,
	[customfield9] [varchar](150) NULL,
	[customfield10] [varchar](150) NULL,
	[all_locations] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[facilityid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[employeemessages]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[employeemessages](
	[employeemessageid] [int] NOT NULL,
	[employeeid_from] [int] NULL,
	[employeeid_to] [int] NULL,
	[message] [varchar](2000) NULL,
	[warn] [varchar](3) NOT NULL,
	[messagedt] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[contactid] [int] NULL,
	[orderid] [int] NULL,
	[hcfaid] [int] NULL,
	[examid] [int] NULL,
	[calendarid] [int] NULL,
	[completed_flag] [varchar](3) NULL,
	[completed_dt] [smalldatetime] NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[due_dt] [smalldatetime] NULL,
	[status] [varchar](50) NULL,
	[priority] [varchar](50) NULL,
	[status_dt] [smalldatetime] NULL,
 CONSTRAINT [PK_employeeMessages] PRIMARY KEY CLUSTERED 
(
	[employeemessageid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[contactinteraction]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[contactinteraction](
	[contactinteractionid] [int] IDENTITY(1,1) NOT NULL,
	[patientid] [int] NOT NULL,
	[interactiondate] [datetime] NOT NULL,
	[employeename] [varchar](100) NULL,
	[notes] [varchar](1000) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[interaction_type] [varchar](50) NULL,
	[category_domainid] [int] NULL,
	[category] [varchar](100) NULL,
	[custom_field_1] [varchar](100) NULL,
	[custom_field_2] [varchar](100) NULL,
	[custom_field_3] [varchar](100) NULL,
	[custom_field_4] [varchar](100) NULL,
	[custom_field_5] [varchar](100) NULL,
	[custom_field_6] [varchar](100) NULL,
	[custom_field_7] [varchar](100) NULL,
	[custom_field_8] [varchar](100) NULL,
	[custom_field_9] [varchar](100) NULL,
	[custom_field_10] [varchar](100) NULL,
 CONSTRAINT [PK_contactinteraction] PRIMARY KEY CLUSTERED 
(
	[contactinteractionid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[commission_structure]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[commission_structure](
	[structure_id] [int] NOT NULL,
	[structure_name] [varchar](50) NOT NULL,
	[structure_type] [char](1) NULL,
	[gross_percentage] [money] NULL,
	[margin_percentage_before] [money] NULL,
	[margin_percentage_after] [money] NULL,
	[margin_percentage_amount] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_commission_structure] PRIMARY KEY CLUSTERED 
(
	[structure_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[email_column_mapping]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[email_column_mapping](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](50) NULL,
	[column_name] [varchar](50) NULL,
	[column_description] [varchar](50) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
 CONSTRAINT [PK_email_column_mapping] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[domaintype]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[domaintype](
	[domaintypeid] [int] NOT NULL,
	[domaintype] [varchar](50) NOT NULL,
	[systemdefined] [varchar](3) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[category] [varchar](50) NULL,
	[exam_category] [varchar](50) NULL,
	[sort_order] [smallint] NULL,
	[subjective_text] [varchar](500) NULL,
 CONSTRAINT [PK_domainType] PRIMARY KEY CLUSTERED 
(
	[domaintypeid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[color]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[color](
	[color_cd] [varchar](30) NOT NULL,
	[rgb_value] [varchar](30) NULL,
 CONSTRAINT [PK_color] PRIMARY KEY CLUSTERED 
(
	[color_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[citystatezip_empty]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[citystatezip_empty](
	[citystatezip_id] [int] NOT NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[country] [varchar](2) NULL,
 CONSTRAINT [PK_citystatezip_empty] PRIMARY KEY CLUSTERED 
(
	[citystatezip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[citystatezip_canada]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[citystatezip_canada](
	[citystatezip_id] [int] NOT NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[country] [varchar](2) NULL,
 CONSTRAINT [PK_citystatezip_canada] PRIMARY KEY CLUSTERED 
(
	[citystatezip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[citystatezip]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[citystatezip](
	[citystatezip_id] [int] NOT NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[country] [varchar](2) NULL,
 CONSTRAINT [PK_citystatezip] PRIMARY KEY CLUSTERED 
(
	[citystatezip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[calendarmaster]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[calendarmaster](
	[calendarmasterid] [int] NOT NULL,
	[calendar_date] [datetime] NOT NULL,
	[notes] [varchar](5000) NULL,
	[locationid] [int] NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_calendarmaster] PRIMARY KEY CLUSTERED 
(
	[calendarmasterid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_calendarmaster] UNIQUE NONCLUSTERED 
(
	[calendar_date] ASC,
	[locationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[calendar_recurrence]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[calendar_recurrence](
	[calendar_recurrence_id] [int] IDENTITY(1,1) NOT NULL,
	[starttime] [datetime] NOT NULL,
	[endtime] [datetime] NOT NULL,
	[duration] [int] NOT NULL,
	[pattern] [varchar](10) NOT NULL,
	[daily_type] [varchar](10) NULL,
	[monthly_type] [varchar](10) NULL,
	[day] [smallint] NULL,
	[week] [smallint] NULL,
	[month] [smallint] NULL,
	[frequency] [varchar](10) NULL,
	[dayname] [varchar](10) NULL,
	[startdt] [datetime] NOT NULL,
	[end_type] [varchar](10) NOT NULL,
	[enddt] [datetime] NULL,
	[number_occurrence] [int] NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[detailed_domain]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[detailed_domain](
	[detailed_domain_id] [int] NOT NULL,
	[domain_type_ind] [char](2) NOT NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](300) NULL,
	[type] [varchar](30) NULL,
	[active] [varchar](3) NULL,
	[linkid] [int] NULL,
	[parentid] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[diagnosis_code_1] [varchar](10) NULL,
	[diagnosis_code_2] [varchar](10) NULL,
	[diagnosis_code_3] [varchar](10) NULL,
	[diagnosis_code_4] [varchar](10) NULL,
	[retail_price] [money] NULL,
	[modifier] [varchar](10) NULL,
	[sort_order] [int] NULL,
 CONSTRAINT [PK_Detailed_domain] PRIMARY KEY CLUSTERED 
(
	[detailed_domain_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[data_products_groups]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[data_products_groups](
	[grp_id] [nvarchar](50) NULL,
	[grp_active_fl] [bit] NOT NULL,
	[grp_nm] [nvarchar](50) NULL,
	[category_id] [nvarchar](50) NULL,
	[manu_id] [nvarchar](50) NULL,
	[RCLGID_old] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[ProdNote] [nvarchar](max) NULL,
	[ManuProcess] [nvarchar](100) NULL,
	[StabMeth] [nvarchar](50) NULL,
	[dk] [nvarchar](50) NULL,
	[DisMeth1] [nvarchar](50) NULL,
	[DisMeth2] [nvarchar](50) NULL,
	[DisMeth3] [nvarchar](50) NULL,
	[DisMeth4] [nvarchar](50) NULL,
	[CtrThick] [nvarchar](75) NULL,
	[CtrThickMeasuredAt] [nvarchar](50) NULL,
	[OpZone] [nvarchar](75) NULL,
	[Material] [nvarchar](50) NULL,
	[FDA Group] [nvarchar](50) NULL,
	[H2O Percent] [float] NULL
) ON [PRIMARY]

/****** Object:  Table [dbo].[data_manufacturers]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[data_manufacturers](
	[manu_id] [nvarchar](50) NULL,
	[manu_nm] [nvarchar](150) NULL,
	[RCLGManuID] [int] NULL,
	[manu_active_fl] [bit] NOT NULL
) ON [PRIMARY]

/****** Object:  Table [dbo].[custom_fields]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[custom_fields](
	[custom_fields_id] [float] NULL,
	[field_source] [char](50) NULL,
	[display_name] [char](50) NULL,
	[column_name] [char](50) NULL,
	[display_flag] [char](3) NULL,
	[sort_order] [float] NULL,
	[field_type] [char](50) NULL,
	[format_mask] [char](50) NULL,
	[appointments_flag] [char](3) NULL,
	[order_flag] [char](3) NULL,
	[insurance_flag] [char](3) NULL,
	[lastupdateduser] [char](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [float] NULL,
	[lastlocation] [char](150) NULL,
	[lastprogramname] [char](150) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[country]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[country](
	[country] [varchar](50) NULL,
	[id] [int] NOT NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[contacts]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[contacts](
	[contactid] [int] NOT NULL,
	[employeeid] [int] NULL,
	[title] [varchar](30) NULL,
	[firstname] [varchar](30) NULL,
	[middlename] [varchar](1) NULL,
	[lastname] [varchar](30) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[stateorprovince] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[workphone] [varchar](20) NULL,
	[homephone] [varchar](20) NULL,
	[faxnumber] [varchar](20) NULL,
	[emailname] [varchar](45) NULL,
	[birthdate] [datetime] NULL,
	[ssn] [varchar](1000) NULL,
	[sex] [varchar](6) NULL,
	[referredby] [varchar](50) NULL,
	[dateentered] [datetime] NULL,
	[notes] [varchar](5000) NULL,
	[grelationship] [varchar](50) NULL,
	[gtitle] [varchar](4) NULL,
	[gfirstname] [varchar](30) NULL,
	[gmiddlename] [varchar](1) NULL,
	[glastname] [varchar](30) NULL,
	[gaddressline1] [varchar](30) NULL,
	[gaddressline2] [varchar](30) NULL,
	[gcity] [varchar](50) NULL,
	[gstateorprovince] [varchar](20) NULL,
	[gzipcode] [varchar](10) NULL,
	[gworkphone] [varchar](20) NULL,
	[ghomephone] [varchar](20) NULL,
	[gbirthdate] [datetime] NULL,
	[gssn] [varchar](1000) NULL,
	[gsex] [varchar](6) NULL,
	[employername] [varchar](50) NULL,
	[eaddressline1] [varchar](30) NULL,
	[eaddressline2] [varchar](30) NULL,
	[ecity] [varchar](50) NULL,
	[estateorprovince] [varchar](20) NULL,
	[ezipcode] [varchar](10) NULL,
	[ephone] [varchar](20) NULL,
	[insprimaryinsuranceid] [varchar](50) NULL,
	[inspolicygroup] [varchar](50) NULL,
	[inscompany] [int] NULL,
	[secondhealthplan] [varchar](3) NULL,
	[inssecondaryinsuranceid] [varchar](50) NULL,
	[inssecondarypolicygroup] [varchar](50) NULL,
	[inssecondarycompany] [int] NULL,
	[inssecondaryfirstname] [varchar](30) NULL,
	[inssecondarylastname] [varchar](30) NULL,
	[inssecondarybirthdate] [datetime] NULL,
	[inssecondaryssn] [varchar](20) NULL,
	[inssecondarysex] [varchar](6) NULL,
	[inssecondaryemployername] [varchar](50) NULL,
	[patientemploymentid] [int] NULL,
	[patientmaritalstatusid] [int] NULL,
	[sendmail] [varchar](3) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[calendarpatient] [varchar](3) NULL,
	[workphoneextension] [varchar](10) NULL,
	[cellularphone] [varchar](20) NULL,
	[hipaaconsent] [varchar](3) NULL,
	[hipaabyemployeeid] [int] NULL,
	[hipaadate] [datetime] NULL,
	[referredbypatientid] [int] NULL,
	[patientactive] [varchar](3) NULL,
	[familyid] [int] NULL,
	[recalls] [tinyint] NULL,
	[suffix] [varchar](30) NULL,
	[languageid] [int] NULL,
	[reference_num] [varchar](50) NULL,
	[badaddress] [varchar](3) NULL,
	[occupation] [varchar](30) NULL,
	[chartnumber] [varchar](50) NULL,
	[alert] [varchar](1000) NULL,
	[ethnicity] [varchar](50) NULL,
	[lastexamdate] [datetime] NULL,
	[recallreasonid] [int] NULL,
	[recallmonths] [int] NULL,
	[recalldate] [datetime] NULL,
	[locationid] [int] NULL,
	[recallreasonid2] [int] NULL,
	[recallmonths2] [int] NULL,
	[recalldate2] [datetime] NULL,
	[employer] [varchar](50) NULL,
	[physicianid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[guarantorpatientnum] [int] NULL,
	[relationshiptoinsured] [varchar](50) NULL,
	[insuredpatientnum] [int] NULL,
	[insurance_is_guarantor] [varchar](3) NULL,
	[patient_type] [varchar](50) NULL,
	[guarantor_title] [varchar](10) NULL,
	[guarantor_party] [varchar](30) NULL,
	[guarantor_celluarphone] [varchar](20) NULL,
	[link_address_to_guarantor_flag] [varchar](3) NULL,
	[patient_nickname] [varchar](30) NULL,
	[preferred_contact_method] [varchar](30) NULL,
	[alternate_phone] [varchar](20) NULL,
	[patient_image] [image] NULL,
	[patient_tracker] [int] NULL,
	[deceased_flag] [varchar](3) NULL,
	[deceased_dt] [smalldatetime] NULL,
	[primary_physician] [varchar](50) NULL,
	[primary_physician_phone] [varchar](20) NULL,
	[signature_on_file] [varchar](3) NULL,
	[signature_dt] [smalldatetime] NULL,
	[signature_source] [varchar](50) NULL,
	[guardian_name] [varchar](50) NULL,
	[guardian_relation] [varchar](50) NULL,
	[guardian_phone] [varchar](20) NULL,
	[guardian_same_as_guarantor_flag] [varchar](3) NULL,
	[head_household_flag] [varchar](3) NULL,
	[collection_flag] [varchar](3) NULL,
	[recallreasonid3] [int] NULL,
	[recallmonths3] [int] NULL,
	[recalldate3] [datetime] NULL,
	[emergency_contact] [varchar](50) NULL,
	[emergency_contactrelationship] [varchar](30) NULL,
	[emergency_phone] [varchar](20) NULL,
	[emergency_phone_2] [varchar](20) NULL,
	[referral_type] [varchar](30) NULL,
	[referred_by_professional_id] [int] NULL,
	[referral_name] [varchar](50) NULL,
	[referral_address] [varchar](50) NULL,
	[referral_city] [varchar](50) NULL,
	[referral_state] [varchar](20) NULL,
	[referral_zip] [varchar](10) NULL,
	[referral_phone] [varchar](20) NULL,
	[referral_sent_flag] [varchar](3) NULL,
	[referral_sent_dt] [smalldatetime] NULL,
	[diagnosis_id_1] [int] NULL,
	[diagnosis_id_2] [int] NULL,
	[diagnosis_id_3] [int] NULL,
	[diagnosis_id_4] [int] NULL,
	[preferred_payment_method] [int] NULL,
	[accept_checks] [varchar](3) NULL,
	[discount_percent] [money] NULL,
	[discount_material_flag] [varchar](3) NULL,
	[discount_services_flag] [varchar](3) NULL,
	[creditcard_no] [varchar](30) NULL,
	[creditcard_expiry] [varchar](10) NULL,
	[creditcard_cvv] [varchar](10) NULL,
	[creditcard_type] [int] NULL,
	[creditcard_holder_firstname] [varchar](30) NULL,
	[creditcard_holder_lastname] [varchar](30) NULL,
	[driverlicence_no] [varchar](50) NULL,
	[driverlicence_expiry] [smalldatetime] NULL,
	[driverlicence_state] [varchar](20) NULL,
	[shipping_address_same] [varchar](3) NULL,
	[shipping_addresline1] [varchar](30) NULL,
	[shipping_addressline2] [varchar](30) NULL,
	[shipping_city] [varchar](50) NULL,
	[shipping_state] [varchar](20) NULL,
	[shipping_zip] [varchar](10) NULL,
	[shipping_country] [varchar](30) NULL,
	[shipping_phone] [varchar](20) NULL,
	[shipping_phone2] [varchar](20) NULL,
	[shipping_badaddress] [varchar](3) NULL,
	[billing_address_same] [varchar](3) NULL,
	[billing_addresline1] [varchar](30) NULL,
	[billing_addressline2] [varchar](30) NULL,
	[billing_city] [varchar](50) NULL,
	[billing_state] [varchar](20) NULL,
	[billing_country] [varchar](30) NULL,
	[billing_phone] [varchar](20) NULL,
	[billing_phone2] [varchar](20) NULL,
	[billing_badaddress] [varchar](3) NULL,
	[insurance_primary_plan_id] [int] NULL,
	[insurance_primary_relation_to_insured] [varchar](50) NULL,
	[insurance_primary_insured_party] [varchar](50) NULL,
	[insurance_primary_patient_id] [int] NULL,
	[insurance_primary_status] [varchar](10) NULL,
	[insurance_primary_eligible] [varchar](10) NULL,
	[insurance_primary_secondary_hcfa] [varchar](3) NULL,
	[insurance_primary_authorization_dt] [smalldatetime] NULL,
	[insurance_primary_authorization_num] [varchar](20) NULL,
	[insurance_primary_authorization_expire_dt] [smalldatetime] NULL,
	[insurance_primary_copay] [smallmoney] NULL,
	[insurance_primary_copay_type] [varchar](20) NULL,
	[insurance_primary_vsp_allow_wfa] [smallmoney] NULL,
	[insurance_primary_vsp_allow_frameretail] [smallmoney] NULL,
	[insurance_primary_vsp_allow_contacts] [smallmoney] NULL,
	[insurance_primary_vsp_copay_exam] [smallmoney] NULL,
	[insurance_primary_vsp_copay_frame] [smallmoney] NULL,
	[insurance_primary_vsp_copay_material] [smallmoney] NULL,
	[insurance_primary_vsp_copay_contacts] [smallmoney] NULL,
	[insurance_primary_vsp_copay_total] [smallmoney] NULL,
	[insurance_primary_insured_name] [varchar](50) NULL,
	[insurance_primary_insured_sex] [varchar](10) NULL,
	[insurance_primary_insured_address] [varchar](50) NULL,
	[insurance_primary_insured_city] [varchar](50) NULL,
	[insurance_primary_insured_state] [varchar](10) NULL,
	[insurance_primary_insured_zip] [varchar](10) NULL,
	[insurance_primary_insured_phone] [varchar](20) NULL,
	[insurance_primary_insured_birthdate] [smalldatetime] NULL,
	[insurance_primary_insured_sig_dt] [smalldatetime] NULL,
	[insurance_primary_insured_employer_school] [varchar](50) NULL,
	[inssecondary_plan_id] [int] NULL,
	[inssecondary_relation_to_insured] [varchar](50) NULL,
	[inssecondary_insured_party] [varchar](50) NULL,
	[inssecondary_patient_id] [int] NULL,
	[inssecondary_status] [varchar](10) NULL,
	[inssecondary_eligible] [varchar](10) NULL,
	[inssecondary_authorization_dt] [smalldatetime] NULL,
	[inssecondary_authorization_num] [varchar](20) NULL,
	[inssecondary_authorization_expire_dt] [smalldatetime] NULL,
	[inssecondary_copay] [smallmoney] NULL,
	[inssecondary_copay_type] [varchar](20) NULL,
	[inssecondary_vsp_allow_wfa] [smallmoney] NULL,
	[inssecondary_vsp_allow_frameretail] [smallmoney] NULL,
	[inssecondary_vsp_allow_contacts] [smallmoney] NULL,
	[inssecondary_vsp_copay_exam] [smallmoney] NULL,
	[inssecondary_vsp_copay_frame] [smallmoney] NULL,
	[inssecondary_vsp_copay_material] [smallmoney] NULL,
	[inssecondary_vsp_copay_contacts] [smallmoney] NULL,
	[inssecondary_vsp_copay_total] [smallmoney] NULL,
	[inssecondary_insured_address] [varchar](50) NULL,
	[inssecondary_insured_city] [varchar](50) NULL,
	[inssecondary_insured_state] [varchar](10) NULL,
	[inssecondary_insured_zip] [varchar](10) NULL,
	[inssecondary_insured_phone] [varchar](20) NULL,
	[inssecondary_insured_sig_dt] [smalldatetime] NULL,
	[insurance_third_company_id] [int] NULL,
	[insurance_third_plan_id] [int] NULL,
	[insurance_third_insured_id] [varchar](50) NULL,
	[insurance_third_policy_group] [varchar](50) NULL,
	[insurance_third_relation_to_insured] [varchar](50) NULL,
	[insurance_third_insured_party] [varchar](50) NULL,
	[insurance_third_patient_id] [int] NULL,
	[insurance_third_status] [varchar](10) NULL,
	[insurance_third_eligible] [varchar](10) NULL,
	[insurance_third_secondary_hcfa] [varchar](3) NULL,
	[insurance_third_authorization_dt] [smalldatetime] NULL,
	[insurance_third_authorization_num] [varchar](20) NULL,
	[insurance_third_authorization_expire_dt] [smalldatetime] NULL,
	[insurance_third_copay] [smallmoney] NULL,
	[insurance_third_copay_type] [varchar](20) NULL,
	[insurance_third_vsp_allow_wfa] [smallmoney] NULL,
	[insurance_third_vsp_allow_frameretail] [smallmoney] NULL,
	[insurance_third_vsp_allow_contacts] [smallmoney] NULL,
	[insurance_third_vsp_copay_exam] [smallmoney] NULL,
	[insurance_third_vsp_copay_frame] [smallmoney] NULL,
	[insurance_third_vsp_copay_material] [smallmoney] NULL,
	[insurance_third_vsp_copay_contacts] [smallmoney] NULL,
	[insurance_third_vsp_copay_total] [smallmoney] NULL,
	[insurance_third_insured_name] [varchar](50) NULL,
	[insurance_third_insured_sex] [varchar](10) NULL,
	[insurance_third_insured_address] [varchar](50) NULL,
	[insurance_third_insured_city] [varchar](50) NULL,
	[insurance_third_insured_state] [varchar](10) NULL,
	[insurance_third_insured_zip] [varchar](10) NULL,
	[insurance_third_insured_phone] [varchar](20) NULL,
	[insurance_third_insured_birthdate] [smalldatetime] NULL,
	[insurance_third_insured_sig_dt] [smalldatetime] NULL,
	[insurance_third_insured_employer_school] [varchar](50) NULL,
	[custom_field_1] [varchar](5000) NULL,
	[custom_field_2] [varchar](5000) NULL,
	[custom_field_3] [varchar](5000) NULL,
	[custom_field_4] [varchar](5000) NULL,
	[custom_field_5] [varchar](5000) NULL,
	[custom_field_6] [varchar](5000) NULL,
	[custom_field_7] [varchar](5000) NULL,
	[custom_field_8] [varchar](5000) NULL,
	[custom_field_9] [varchar](5000) NULL,
	[custom_field_10] [varchar](5000) NULL,
	[custom_field_11] [varchar](5000) NULL,
	[custom_field_12] [varchar](5000) NULL,
	[custom_field_13] [varchar](5000) NULL,
	[custom_field_14] [varchar](5000) NULL,
	[custom_field_15] [varchar](5000) NULL,
	[custom_field_16] [varchar](5000) NULL,
	[custom_field_17] [varchar](5000) NULL,
	[custom_field_18] [varchar](5000) NULL,
	[custom_field_19] [varchar](5000) NULL,
	[custom_field_20] [varchar](5000) NULL,
	[custom_field_21] [varchar](5000) NULL,
	[custom_field_22] [varchar](5000) NULL,
	[custom_field_23] [varchar](5000) NULL,
	[custom_field_24] [varchar](5000) NULL,
	[custom_field_25] [varchar](5000) NULL,
	[custom_field_26] [varchar](5000) NULL,
	[custom_field_27] [varchar](5000) NULL,
	[custom_field_28] [varchar](5000) NULL,
	[custom_field_29] [varchar](5000) NULL,
	[custom_field_30] [varchar](5000) NULL,
	[custom_field_31] [varchar](5000) NULL,
	[custom_field_32] [varchar](5000) NULL,
	[custom_field_33] [varchar](5000) NULL,
	[custom_field_34] [varchar](5000) NULL,
	[custom_field_35] [varchar](5000) NULL,
	[custom_field_36] [varchar](5000) NULL,
	[custom_field_37] [varchar](5000) NULL,
	[custom_field_38] [varchar](5000) NULL,
	[custom_field_39] [varchar](5000) NULL,
	[custom_field_40] [varchar](5000) NULL,
	[custom_field_41] [varchar](5000) NULL,
	[custom_field_42] [varchar](5000) NULL,
	[custom_field_43] [varchar](5000) NULL,
	[custom_field_44] [varchar](5000) NULL,
	[custom_field_45] [varchar](5000) NULL,
	[custom_field_46] [varchar](5000) NULL,
	[custom_field_47] [varchar](5000) NULL,
	[custom_field_48] [varchar](5000) NULL,
	[custom_field_49] [varchar](5000) NULL,
	[custom_field_50] [varchar](5000) NULL,
	[billing_zip] [varchar](50) NULL,
	[selected_address] [char](1) NULL,
	[listid] [varchar](36) NULL,
	[hobbies] [varchar](200) NULL,
	[allergies] [varchar](200) NULL,
	[country] [varchar](100) NULL,
	[bill_to_guarantor_flag] [varchar](3) NULL,
	[userid] [varchar](100) NULL,
	[userpassword] [varchar](100) NULL,
	[last_login_dt] [smalldatetime] NULL,
	[account_status] [varchar](100) NULL,
	[transfer_flag] [int] NULL,
	[erx_status] [int] NULL,
	[pharmacyid] [varchar](10) NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[contactid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[insurance]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[insurance](
	[id] [int] NOT NULL,
	[companyname] [varchar](50) NOT NULL,
	[submitto] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[stateorprovince] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[phone] [varchar](20) NULL,
	[fax] [varchar](20) NULL,
	[electronicsubmissionno] [varchar](10) NULL,
	[active] [varchar](3) NULL,
	[notes] [varchar](5000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[insurancetypeid] [int] NULL,
	[hcfaprintname] [varchar](3) NULL,
	[hcfa24kprint] [varchar](3) NULL,
	[hcfa47ind] [varchar](1) NULL,
	[hcfaindent] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[payer_id] [varchar](50) NULL,
	[payer_sub_id] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[insurance] ADD [modifier_lens_default_option] [varchar](50) NULL
ALTER TABLE [dbo].[insurance] ADD  CONSTRAINT [PK_INSURANCE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[id_units]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[id_units](
	[lens_qty_id] [varchar](50) NULL,
	[lens_qty_nm] [varchar](50) NULL,
	[lens_qty_val] [int] NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[history]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[history](
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [smalldatetime] NOT NULL,
	[table_type] [varchar](50) NOT NULL,
	[table_key] [int] NOT NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[report_cd] [varchar](50) NULL,
 CONSTRAINT [PK_history] PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[framesdata_qi]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING OFF

CREATE TABLE [dbo].[framesdata_qi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[manufacturername] [varchar](50) NULL,
	[collectionname] [varchar](50) NULL,
	[stylename] [varchar](50) NULL,
	[transaction_number] [int] NULL,
UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[framesdata]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[framesdata](
	[framesdata_id] [int] IDENTITY(1,1) NOT NULL,
	[edition] [smalldatetime] NULL,
	[companyid] [char](4) NULL,
	[fpc] [varchar](20) NULL,
	[upc] [varchar](20) NULL,
	[styleid] [varchar](50) NULL,
	[stylename] [varchar](50) NULL,
	[colordescription] [varchar](50) NULL,
	[colorcode] [varchar](50) NULL,
	[lenscolor] [varchar](50) NULL,
	[lenscolorcode] [smallmoney] NULL,
	[eye] [tinyint] NULL,
	[bridge] [tinyint] NULL,
	[temple] [tinyint] NULL,
	[dbl] [decimal](5, 2) NULL,
	[a] [decimal](5, 2) NULL,
	[b] [decimal](5, 2) NULL,
	[ed] [decimal](5, 2) NULL,
	[circumference] [decimal](7, 2) NULL,
	[edangle] [decimal](5, 2) NULL,
	[frontprice] [smallmoney] NULL,
	[halftemplesprice] [smallmoney] NULL,
	[templesprice] [smallmoney] NULL,
	[completeprice] [smallmoney] NULL,
	[manufacturername] [varchar](50) NULL,
	[brandname] [varchar](50) NULL,
	[collectionname] [varchar](50) NULL,
	[gendertype] [varchar](50) NULL,
	[agegroup] [varchar](50) NULL,
	[activestatus] [varchar](50) NULL,
	[productgroupname] [varchar](50) NULL,
	[rimtype] [varchar](50) NULL,
	[material] [varchar](50) NULL,
	[frameshape] [varchar](50) NULL,
	[stylenew] [varchar](3) NULL,
	[changedprice] [varchar](3) NULL,
	[country] [varchar](50) NULL,
	[sku] [varchar](50) NULL,
	[yearintroduced] [char](4) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[framemarkup]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[framemarkup](
	[framemarkup_id] [int] IDENTITY(1,1) NOT NULL,
	[start_price] [money] NULL,
	[end_price] [money] NULL,
	[multiplier] [decimal](5, 2) NULL,
	[amount] [money] NULL,
	[ends_with] [money] NULL,
	[ceiling] [varchar](50) NULL,
	[locationid] [int] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[manufacturername] [varchar](50) NULL,
	[collectionname] [varchar](50) NULL,
	[activestatus] [varchar](50) NULL,
	[rimtype] [varchar](50) NULL,
	[material] [varchar](50) NULL,
	[frameshape] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[framemarkup] ADD [nearest_tenth] [varchar](10) NULL
ALTER TABLE [dbo].[framemarkup] ADD  CONSTRAINT [PK_framemarkup] PRIMARY KEY CLUSTERED 
(
	[framemarkup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[frameinventory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[frameinventory](
	[frameid] [int] NOT NULL,
	[detailed_domain_id] [int] NULL,
	[manufacturerid] [int] NULL,
	[framenumber] [varchar](20) NULL,
	[framename] [varchar](50) NULL,
	[frametypeid] [int] NULL,
	[colorid] [int] NULL,
	[colornumber] [varchar](30) NULL,
	[materialid] [int] NULL,
	[a] [decimal](5, 2) NULL,
	[dbl] [decimal](5, 2) NULL,
	[templelength] [decimal](5, 2) NULL,
	[templestyleid] [int] NULL,
	[b] [decimal](5, 2) NULL,
	[ed] [decimal](5, 2) NULL,
	[safety] [varchar](3) NULL,
	[quantitysold] [int] NULL,
	[quantitypurchased] [int] NULL,
	[cost] [money] NULL,
	[retailprice] [money] NULL,
	[taxable] [varchar](3) NULL,
	[notes] [varchar](5000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[upccode] [varchar](20) NULL,
	[structure_id] [int] NULL,
	[structure_type] [varchar](1) NULL,
	[gross_percentage] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[deleted] [varchar](3) NULL,
	[inventory] [varchar](3) NULL,
	[collectionid] [int] NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[eyesize] [tinyint] NULL,
	[bridgesize] [tinyint] NULL,
	[locationid] [int] NULL,
	[groupcost] [money] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[styleid] [varchar](50) NULL,
	[colordescription] [varchar](50) NULL,
	[lenscolor] [varchar](50) NULL,
	[lenscolorcode] [smallmoney] NULL,
	[circumference] [decimal](7, 2) NULL,
	[edangle] [decimal](5, 2) NULL,
	[frontprice] [smallmoney] NULL,
	[halftemplesprice] [smallmoney] NULL,
	[templesprice] [smallmoney] NULL,
	[manufacturername] [varchar](50) NULL,
	[brandname] [varchar](50) NULL,
	[collectionname] [varchar](50) NULL,
	[gendertype] [varchar](50) NULL,
	[agegroup] [varchar](50) NULL,
	[activestatus] [varchar](50) NULL,
	[productgroupname] [varchar](50) NULL,
	[rimtype] [varchar](50) NULL,
	[material] [varchar](50) NULL,
	[frameshape] [varchar](50) NULL,
	[country] [varchar](50) NULL,
	[sku] [varchar](50) NULL,
	[yearintroduced] [char](4) NULL,
	[upccode_type] [varchar](50) NULL,
	[default_supplierid] [int] NULL,
	[price_group_id] [int] NULL,
	[discontinue_dt] [smalldatetime] NULL,
	[created_dt] [smalldatetime] NULL,
	[commission_flag] [varchar](3) NULL,
	[frame_usage] [varchar](50) NULL,
	[user_defined] [varchar](100) NULL,
	[user_defined_2] [varchar](100) NULL,
	[bridge] [varchar](50) NULL,
	[dbm] [decimal](5, 2) NULL,
	[frame_lens_range_id] [int] NULL,
	[trace_file] [varchar](200) NULL,
	[msrp] [money] NULL,
	[rimless_flag] [varchar](3) NULL,
	[placeofservice] [varchar](10) NULL,
	[source] [varchar](20) NULL,
 CONSTRAINT [PK_FrameInventory] PRIMARY KEY CLUSTERED 
(
	[frameid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[blob_table]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[blob_table](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](250) NULL,
	[column_name] [varchar](250) NULL,
	[blob_key] [varchar](50) NOT NULL,
	[blob_index] [int] NULL,
	[created_date] [datetime] NULL,
	[created_by] [varchar](50) NULL,
	[item] [varbinary](max) NULL,
	[filename] [varchar](500) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[adjustment]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[adjustment](
	[adjustmentid] [int] NOT NULL,
	[adjustmentdate] [datetime] NOT NULL,
	[adjustmentamount] [money] NULL,
	[depositovershort] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[notes] [varchar](5000) NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_adjustment] PRIMARY KEY CLUSTERED 
(
	[adjustmentid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[AA WARNING: DO NOT CHANGE ANYTHING HERE OR THE APPLICATION MAY BE CORRUPTED]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[AA WARNING: DO NOT CHANGE ANYTHING HERE OR THE APPLICATION MAY BE CORRUPTED](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[warning] [nvarchar](255) NULL
) ON [PRIMARY]

/****** Object:  Table [dbo].[FLXR_PARAM]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_PARAM](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[value] [varchar](500) NOT NULL,
 CONSTRAINT [PK_FLXR_PARAM] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_FLXR_PARAM] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_GROUP]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_GROUP](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_CUSTOMIZATION_MACHINES]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_CUSTOMIZATION_MACHINES](
	[Machine_Name] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_CUSTOMIZATION]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_CUSTOMIZATION](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[dataobject] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[ref_syntax] [ntext] NULL,
	[cur_syntax] [ntext] NULL,
	[was_applied] [varchar](10) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastupdateddt_ref] [datetime] NULL,
	[created_dt] [datetime] NULL,
 CONSTRAINT [PK_FLXR_CUSTOMIZATION] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_FLXR_CUSTOMIZATION] UNIQUE NONCLUSTERED 
(
	[dataobject] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[frame_lens_range]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[frame_lens_range](
	[frame_lens_range_id] [int] IDENTITY(1,1) NOT NULL,
	[lens_range_name] [varchar](100) NULL,
	[lens_style_id] [int] NULL,
	[notes] [varchar](500) NULL,
	[sort_order] [int] NULL,
	[sphere_min] [decimal](5, 3) NULL,
	[sphere_max] [decimal](5, 3) NULL,
	[cylinder_min] [decimal](5, 3) NULL,
	[cylinder_max] [decimal](5, 3) NULL,
	[min_pd] [decimal](5, 3) NULL,
	[max_pd] [decimal](5, 3) NULL,
	[min_add] [decimal](5, 3) NULL,
	[max_add] [decimal](5, 3) NULL,
	[use_sphere_equivalent] [varchar](50) NULL,
	[item_type_id] [char](1) NULL,
	[max_prism] [decimal](3, 2) NULL,
 CONSTRAINT [PK_frame_lens_range] PRIMARY KEY CLUSTERED 
(
	[frame_lens_range_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[frame_labels_settings]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[frame_labels_settings](
	[manufacturername] [int] NULL,
	[collectionname] [int] NULL,
	[framename] [int] NULL,
	[colordescription] [int] NULL,
	[colornumber] [int] NULL,
	[material] [int] NULL,
	[eyesize] [int] NULL,
	[a] [int] NULL,
	[b] [int] NULL,
	[ed] [int] NULL,
	[bridge] [int] NULL,
	[templelength] [int] NULL,
	[productgroupname] [int] NULL,
	[rimtype] [int] NULL,
	[frameshape] [int] NULL,
	[cost] [int] NULL,
	[retailprice] [int] NULL,
	[upccode] [int] NULL,
	[gendertype] [int] NULL,
	[agegroup] [int] NULL
) ON [PRIMARY]

/****** Object:  Table [dbo].[letter]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[letter](
	[letterid] [int] NOT NULL,
	[type] [varchar](20) NULL,
	[description] [varchar](50) NULL,
	[lettertext] [text] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_letter] PRIMARY KEY CLUSTERED 
(
	[letterid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[labels]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[labels](
	[labelid] [int] NOT NULL,
	[label_name] [varchar](100) NOT NULL,
	[units] [smallint] NULL,
	[sheet] [smallint] NULL,
	[columns] [smallint] NULL,
	[rows] [smallint] NULL,
	[columns_spacing] [smallint] NULL,
	[rows_spacing] [smallint] NULL,
	[top_margin] [smallint] NULL,
	[left_margin] [smallint] NULL,
	[right_margin] [smallint] NULL,
	[bottom_margin] [smallint] NULL,
	[height] [smallint] NULL,
	[width] [smallint] NULL,
	[shape] [varchar](100) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[dataobject] [varchar](50) NULL,
	[inventory_type] [varchar](1) NULL,
	[units_original] [smallint] NULL,
	[sheet_original] [smallint] NULL,
	[columns_original] [smallint] NULL,
	[rows_original] [smallint] NULL,
	[columns_spacing_original] [smallint] NULL,
	[rows_spacing_original] [smallint] NULL,
	[top_margin_original] [smallint] NULL,
	[left_margin_original] [smallint] NULL,
	[right_margin_original] [smallint] NULL,
	[bottom_margin_original] [smallint] NULL,
	[height_original] [smallint] NULL,
	[width_original] [smallint] NULL,
	[shape_original] [varchar](100) NULL,
	[printer_name] [varchar](200) NULL,
 CONSTRAINT [PK_labels] PRIMARY KEY CLUSTERED 
(
	[labelid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[inventorytransactions]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[inventorytransactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[producttypeind] [varchar](1) NOT NULL,
	[productid] [int] NOT NULL,
	[daterecieved] [datetime] NULL,
	[quantity] [int] NULL,
	[expirationdate] [datetime] NULL,
	[lotno] [varchar](20) NULL,
	[supplierid] [int] NULL,
	[invoiceno] [varchar](20) NULL,
	[employeeid] [int] NULL,
	[notes] [varchar](2000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[cost] [money] NULL,
	[paymenttermsid] [int] NULL,
	[returndt] [datetime] NULL,
	[transactiontypeind] [char](1) NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_InventoryTransactions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[inventorylocation]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[inventorylocation](
	[inventorylocationid] [int] IDENTITY(1,1) NOT NULL,
	[locationid] [int] NOT NULL,
	[producttypeind] [char](1) NOT NULL,
	[productid] [int] NOT NULL,
	[quantitysold] [int] NOT NULL,
	[quantitypurchased] [int] NOT NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[reorder_point] [int] NULL,
 CONSTRAINT [PK_inventorylocation] PRIMARY KEY CLUSTERED 
(
	[inventorylocationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[interface_audit]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[interface_audit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [varchar](250) NULL,
	[column_name] [varchar](250) NULL,
	[ia_key] [varchar](50) NOT NULL,
	[ia_index] [int] NOT NULL,
	[created_date] [datetime] NULL,
	[created_by] [varchar](50) NULL,
	[item] [nvarchar](max) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[marketing]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[marketing](
	[marketingid] [int] NOT NULL,
	[code] [varchar](50) NULL,
	[source] [varchar](50) NULL,
	[description] [varchar](80) NULL,
	[detail] [varchar](1000) NULL,
	[package] [varchar](3) NULL,
	[packageamount] [money] NULL,
	[percentagediscount] [varchar](3) NULL,
	[discount] [money] NULL,
	[amountdiscount] [varchar](3) NULL,
	[amount] [money] NULL,
	[andor] [varchar](3) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_Marketing] PRIMARY KEY CLUSTERED 
(
	[marketingid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[manufacturer]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[manufacturer](
	[manufacturerid] [int] NOT NULL,
	[manufacturername] [varchar](150) NULL,
	[manufacturertype] [varchar](50) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_Manufacturer] PRIMARY KEY CLUSTERED 
(
	[manufacturerid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[licence]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[licence](
	[licenceid] [int] NOT NULL,
	[installationdate] [datetime] NULL,
	[licencenumber] [varchar](30) NULL,
	[evaluationdaysleft] [int] NULL,
	[registeredto] [varchar](60) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[country] [varchar](1) NULL,
	[edition] [varchar](1) NULL,
 CONSTRAINT [PK_licence] PRIMARY KEY CLUSTERED 
(
	[licenceid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[nc_patientallergydetails]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[nc_patientallergydetails](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[CompositeID] [varchar](100) NULL,
	[ConceptID] [varchar](100) NULL,
	[Source] [varchar](100) NULL,
	[ConceptTypeID] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[SeverityTypeID] [varchar](100) NULL,
	[SeverityName] [varchar](500) NULL,
	[Notes] [varchar](2000) NULL,
	[contactid] [int] NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[date_entered] [smalldatetime] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [pk_nc_patientallergydetails] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[mytable]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[mytable](
	[id] [int] NOT NULL,
	[something] [image] NULL,
 CONSTRAINT [PK_mytable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[mycompanylogo]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[mycompanylogo](
	[logoid] [int] IDENTITY(1,1) NOT NULL,
	[locationid] [int] NOT NULL,
	[logoimage] [image] NULL,
	[imagename] [varchar](100) NULL,
	[imagetype] [varchar](100) NULL,
	[imagewidth] [int] NULL,
	[imageheight] [int] NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_mycompanylogo] PRIMARY KEY CLUSTERED 
(
	[logoid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[mycompanyinformation]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[mycompanyinformation](
	[setupid] [int] IDENTITY(1,1) NOT NULL,
	[companyname] [varchar](50) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](20) NULL,
	[zipcode] [varchar](10) NULL,
	[phonenumber] [varchar](20) NULL,
	[faxnumber] [varchar](20) NULL,
	[emailname] [varchar](50) NULL,
	[website] [varchar](50) NULL,
	[taxnumber] [varchar](50) NULL,
	[orderreceiptnote] [varchar](2000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[autoaddcalendarpatient] [varchar](3) NULL,
	[calendarstarttime] [datetime] NULL,
	[calendarendtime] [datetime] NULL,
	[calendarduration] [int] NULL,
	[calendarcolorind] [varchar](1) NULL,
	[calendarsuncls] [varchar](3) NULL,
	[calendarmoncls] [varchar](3) NULL,
	[calendartuecls] [varchar](3) NULL,
	[calendarwedcls] [varchar](3) NULL,
	[calendarthucls] [varchar](3) NULL,
	[calendarfricls] [varchar](3) NULL,
	[calendarsatcls] [varchar](3) NULL,
	[calendarsunfrom] [datetime] NULL,
	[calendarsunto] [datetime] NULL,
	[calendarmonfrom] [datetime] NULL,
	[calendarmonto] [datetime] NULL,
	[calendartuefrom] [datetime] NULL,
	[calendartueto] [datetime] NULL,
	[calendarwedfrom] [datetime] NULL,
	[calendarwedto] [datetime] NULL,
	[calendarthufrom] [datetime] NULL,
	[calendarthuto] [datetime] NULL,
	[calendarfrifrom] [datetime] NULL,
	[calendarfrito] [datetime] NULL,
	[calendarsatfrom] [datetime] NULL,
	[calendarsatto] [datetime] NULL,
	[autolocksec] [int] NULL,
	[date_format] [varchar](20) NULL,
	[phone_format] [varchar](20) NULL,
	[country] [char](1) NULL,
	[installationdt] [datetime] NULL,
	[receipt_original] [image] NULL,
	[receipt_new] [image] NULL,
	[country_name] [varchar](50) NULL,
	[backupreminddt] [datetime] NULL,
	[receipt_printcompany] [varchar](3) NULL,
	[default_rxexpiry_lens] [int] NULL,
	[default_rxexpiry_contacts] [int] NULL,
	[taxinclusive] [varchar](3) NULL,
	[colorcodeclosed] [int] NULL,
	[colorcodenotavail] [int] NULL,
	[deleted] [varchar](3) NULL,
	[commisionable_frame] [char](1) NULL,
	[commisionable_lens] [char](1) NULL,
	[commisionable_lenstreatment] [char](1) NULL,
	[commisionable_other] [char](1) NULL,
	[commisionable_services] [char](1) NULL,
	[commisionable_contacts] [char](1) NULL,
	[default_tax1_frame] [int] NULL,
	[default_tax2_frame] [int] NULL,
	[default_tax1_lens] [int] NULL,
	[default_tax2_lens] [int] NULL,
	[default_tax1_lenstreatment] [int] NULL,
	[default_tax2_lenstreatment] [int] NULL,
	[default_tax1_services] [int] NULL,
	[default_tax2_services] [int] NULL,
	[default_tax1_other] [int] NULL,
	[default_tax2_other] [int] NULL,
	[default_tax1_contacts] [int] NULL,
	[default_tax2_contacts] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[default_rxexpiry_medication] [smallint] NULL,
	[copy_recalldate_expiry_flag] [varchar](3) NULL,
	[company_code] [varchar](50) NULL,
	[vsp_vid] [varchar](15) NULL,
	[vsp_vpw] [varchar](15) NULL,
	[vsp_rid] [varchar](10) NULL,
	[vsp_rpw] [varchar](8) NULL,
	[vsp_oid] [varchar](14) NULL,
	[vsp_sv_frame] [smallmoney] NULL,
	[vsp_sv] [smallmoney] NULL,
	[vsp_bifocal_frame] [smallmoney] NULL,
	[vsp_bifcoal] [smallmoney] NULL,
	[vsp_trifocal_frame] [smallmoney] NULL,
	[vsp_trifocal] [smallmoney] NULL,
	[vsp_lenticular_frame] [smallmoney] NULL,
	[vsp_lenticular] [smallmoney] NULL,
	[vsp_progressive_frame] [smallmoney] NULL,
	[vsp_progressive] [smallmoney] NULL,
	[vsp_frame_only] [smallmoney] NULL,
	[vsp_fa_calculation_type] [varchar](10) NULL,
	[vsp_fa_round_higher] [varchar](3) NULL,
	[vsp_quad_frame] [smallmoney] NULL,
	[vsp_quad] [smallmoney] NULL,
	[vsp_dual_frame] [smallmoney] NULL,
	[vsp_dual] [smallmoney] NULL,
	[group_practice_flag] [varchar](3) NULL,
	[TimeZone] [int] NULL,
	[AutoDayLightSavings] [char](1) NULL,
	[default_tax1_frame_rx] [int] NULL,
	[default_tax2_frame_rx] [int] NULL,
	[nc_siteid] [varchar](50) NULL,
	[xcharge_transaction_folder] [varchar](500) NULL,
	[default_facility] [int] NULL,
	[x_charge_merchant_id] [varchar](50) NULL,
	[x_charge_enable_trans_folder] [int] NULL,
	[x_charge_ip_address] [varchar](255) NULL,
	[x_charge_ip_port] [varchar](10) NULL,
 CONSTRAINT [PK_MyCompanyInformation] PRIMARY KEY CLUSTERED 
(
	[setupid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[messages]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[messages](
	[msgid] [varchar](40) NOT NULL,
	[msgtitle] [varchar](255) NULL,
	[msgtext] [varchar](255) NULL,
	[msgicon] [varchar](12) NULL,
	[msgbutton] [varchar](17) NULL,
	[msgdefaultbutton] [int] NULL,
	[msgseverity] [int] NULL,
	[msgprint] [varchar](1) NULL,
	[msguserinput] [varchar](1) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
 CONSTRAINT [PK_messages] PRIMARY KEY CLUSTERED 
(
	[msgid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[insurance_plan_fee_num_lens_vcode]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[insurance_plan_fee_num_lens_vcode](
	[num_vcode] [smallint] NOT NULL,
 CONSTRAINT [PK_insurance_plan_fee_num_lens_vcode] PRIMARY KEY CLUSTERED 
(
	[num_vcode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[insurance_plan]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[insurance_plan](
	[insurance_plan_id] [int] IDENTITY(1,1) NOT NULL,
	[insurance_id] [int] NULL,
	[plan_name] [varchar](50) NULL,
	[group_name] [varchar](50) NULL,
	[group_number] [varchar](20) NULL,
	[active_flag] [varchar](3) NULL,
	[frame_allowance_ws_retail_flag] [char](1) NULL,
	[frame_allowance_amt] [money] NULL,
	[frame_allowance_corresp_retail] [money] NULL,
	[frame_dispensing_fee] [money] NULL,
	[ecl_allowance_amt] [money] NULL,
	[frame_ins_pays] [money] NULL,
	[frame_discount] [money] NULL,
	[frame_patient_pays] [money] NULL,
	[frame_overage_discount] [money] NULL,
	[frame_overage_multiplier] [decimal](5, 2) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[location_id] [int] NULL,
	[vsp_sv_frame] [money] NULL,
	[vsp_sv] [money] NULL,
	[vsp_bifocal_frame] [money] NULL,
	[vsp_bifcoal] [money] NULL,
	[vsp_trifocal_frame] [money] NULL,
	[vsp_trifocal] [money] NULL,
	[vsp_lenticular_frame] [money] NULL,
	[vsp_lenticular] [money] NULL,
	[vsp_progressive_frame] [money] NULL,
	[vsp_progressive] [money] NULL,
	[vsp_frame_only] [money] NULL,
	[vsp_fa_calculation_type] [varchar](10) NULL,
	[vsp_fa_round_higher] [varchar](3) NULL,
	[vsp_quad_frame] [money] NULL,
	[vsp_quad] [money] NULL,
	[vsp_dual_frame] [money] NULL,
	[vsp_dual] [money] NULL,
	[insurance_supplied] [varchar](1) NULL,
	[overage_to_v20205] [varchar](1) NULL,
	[alert] [varchar](2000) NULL,
	[cl_allowance] [money] NULL,
	[supplied_frames] [varchar](1) NULL,
	[frame_overage_type] [varchar](10) NULL,
	[exam_copay] [money] NULL,
	[frame_copay] [money] NULL,
	[material_copay] [money] NULL,
	[combine_lens_codes] [varchar](1) NULL,
 CONSTRAINT [PK_insurance_plan] PRIMARY KEY CLUSTERED 
(
	[insurance_plan_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[marketingdetail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[marketingdetail](
	[marketingdetailid] [int] NOT NULL,
	[itemtype] [char](1) NOT NULL,
	[maximumprice] [money] NULL,
	[quantity] [int] NULL,
	[marketingid] [int] NOT NULL,
	[fr_manufacturerid] [int] NULL,
	[ot_otherid] [int] NULL,
	[co_lensnameid] [int] NULL,
	[co_manufacturerid] [int] NULL,
	[sp_typeid] [int] NULL,
	[sp_styleid] [int] NULL,
	[sp_materialid] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[ot_categoryid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[frame_manufacturername] [varchar](100) NULL,
	[frame_activestatus] [varchar](50) NULL,
 CONSTRAINT [PK_MarketingDetail] PRIMARY KEY CLUSTERED 
(
	[marketingdetailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[marketingdates]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[marketingdates](
	[marketingdatesid] [int] NOT NULL,
	[marketingid] [int] NOT NULL,
	[fromdt] [datetime] NULL,
	[todt] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_MarketingDates] PRIMARY KEY CLUSTERED 
(
	[marketingdatesid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[letter_history]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[letter_history](
	[letter_history_id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NULL,
	[letter_date] [datetime] NULL,
	[type] [varchar](100) NULL,
	[description] [varchar](500) NULL,
	[labels] [varchar](3) NULL,
	[listing] [varchar](3) NULL,
	[document] [varchar](3) NULL,
	[envelopes] [varchar](3) NULL,
	[custom] [varchar](50) NULL,
	[lettertext] [text] NULL,
	[from_dt] [datetime] NULL,
	[to_dt] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[notes] [varchar](1000) NULL,
	[type_id] [int] NULL,
 CONSTRAINT [PK_letter_history] PRIMARY KEY CLUSTERED 
(
	[letter_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_USER]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_USER](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[id_group] [numeric](9, 0) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_AFF_GROUP]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_AFF_GROUP](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[dataobject] [varchar](50) NOT NULL,
	[id_group] [numeric](9, 0) NOT NULL,
	[id_customization] [numeric](9, 0) NULL,
 CONSTRAINT [PK_FLXR_AFF_GROUP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_FLXR_AFF_GROUP] UNIQUE NONCLUSTERED 
(
	[dataobject] ASC,
	[id_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FLXR_AFF_GLOBAL]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_AFF_GLOBAL](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[dataobject] [varchar](50) NOT NULL,
	[id_customization] [numeric](9, 0) NULL,
 CONSTRAINT [PK_FLXR_AFF_GLOBAL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_FLXR_AFF_GLOBAL] UNIQUE NONCLUSTERED 
(
	[dataobject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[facility_provider]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[facility_provider](
	[facility_prov_id] [int] IDENTITY(1,1) NOT NULL,
	[facility_id] [int] NULL,
	[provider_id] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
 CONSTRAINT [PK_facility__provider] PRIMARY KEY CLUSTERED 
(
	[facility_prov_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[facility_location]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[facility_location](
	[facility_location_id] [int] IDENTITY(1,1) NOT NULL,
	[facility_id] [int] NULL,
	[location_id] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[facility_location_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[hcfa]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[hcfa](
	[hcfaid] [int] NOT NULL,
	[patientid] [int] NULL,
	[orderid] [int] NULL,
	[insurancetypeid] [int] NULL,
	[insuredid] [varchar](50) NULL,
	[patientlname] [varchar](30) NULL,
	[patientfname] [varchar](30) NULL,
	[patientminitial] [varchar](1) NULL,
	[patientbirthdate] [datetime] NULL,
	[patientsex] [varchar](6) NULL,
	[insuredlname] [varchar](30) NULL,
	[insuredminitial] [varchar](1) NULL,
	[insuredfname] [varchar](30) NULL,
	[patienttelephone] [varchar](20) NULL,
	[patientzipcode] [varchar](10) NULL,
	[patientstate] [varchar](2) NULL,
	[patientcity] [varchar](50) NULL,
	[patientaddress] [varchar](70) NULL,
	[patientrelationship] [varchar](10) NULL,
	[insuredaddress] [varchar](70) NULL,
	[insuredcity] [varchar](50) NULL,
	[insuredstate] [varchar](2) NULL,
	[insuredzip] [varchar](10) NULL,
	[insuredtelephone] [varchar](20) NULL,
	[patientemploymentid] [int] NULL,
	[patientmaritalstatusid] [int] NULL,
	[otherinsuredlname] [varchar](30) NULL,
	[otherinsuredfname] [varchar](30) NULL,
	[otherinsuredminitial] [varchar](1) NULL,
	[otherinsuredpolicy] [varchar](50) NULL,
	[otherinsuredbirthdate] [datetime] NULL,
	[otherinsuredsex] [varchar](6) NULL,
	[otherinsuredemployername] [varchar](50) NULL,
	[otherinsuredinsuranceplan] [varchar](50) NULL,
	[patientconditionrelatedemployment] [varchar](3) NULL,
	[patientconditionrelatedauto] [varchar](3) NULL,
	[patientconditionrelatedautoplace] [varchar](2) NULL,
	[patientconditionrelatedotheracc] [varchar](3) NULL,
	[insuredpolicy] [varchar](50) NULL,
	[insuredbirthdate] [datetime] NULL,
	[insuredsex] [varchar](6) NULL,
	[insuredemployer] [varchar](50) NULL,
	[insuredplan] [varchar](50) NULL,
	[secondhealthplan] [varchar](3) NULL,
	[datecurrentillness] [datetime] NULL,
	[datefirstillness] [datetime] NULL,
	[dateunabletoworkfrom] [datetime] NULL,
	[dateunabletoworkto] [datetime] NULL,
	[referringphysicianid] [int] NULL,
	[referringphysiciannum] [varchar](20) NULL,
	[hospitalizationdatefrom] [datetime] NULL,
	[hospitalizationdateto] [datetime] NULL,
	[reservedforlocaluse] [varchar](50) NULL,
	[outsidelab] [varchar](3) NULL,
	[outsidelabcharges] [money] NULL,
	[diagnosiscode1] [int] NULL,
	[diagnosiscode2] [int] NULL,
	[diagnosiscode3] [int] NULL,
	[diagnosiscode4] [int] NULL,
	[medresubmissioncode] [varchar](10) NULL,
	[originalreference] [varchar](10) NULL,
	[priorauthorization] [varchar](30) NULL,
	[federaltaxtype] [varchar](3) NULL,
	[federaltaxnum] [varchar](10) NULL,
	[patientaccount] [varchar](10) NULL,
	[acceptassignment] [varchar](3) NULL,
	[amountpaid] [money] NULL,
	[facilityaddress] [varchar](300) NULL,
	[physicianaddress] [varchar](300) NULL,
	[providerid] [int] NULL,
	[providerpin] [varchar](15) NULL,
	[providergroup] [varchar](15) NULL,
	[signature12] [varchar](40) NULL,
	[signaturedate12] [datetime] NULL,
	[reservedforlocaluse10] [varchar](40) NULL,
	[signature13] [varchar](40) NULL,
	[signature31] [varchar](40) NULL,
	[signaturedate31] [datetime] NULL,
	[totalcharge] [money] NULL,
	[balancedue] [money] NULL,
	[claimstatusid] [int] NULL,
	[balanceduedoctor] [money] NULL,
	[paidbyinsured] [money] NULL,
	[owedbyinsured] [money] NULL,
	[insuranceamountsubmitted] [money] NULL,
	[insuranceamountpaid] [money] NULL,
	[insurancewrittenoff] [money] NULL,
	[dateofservice] [datetime] NULL,
	[datesubmitted] [datetime] NULL,
	[datepaid] [datetime] NULL,
	[notes] [varchar](5000) NULL,
	[string_1] [varchar](30) NULL,
	[string_2] [varchar](30) NULL,
	[number_1] [money] NULL,
	[number_2] [money] NULL,
	[date_1] [datetime] NULL,
	[date_2] [datetime] NULL,
	[long_1] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[insurancecompanyid] [int] NULL,
	[insurancecompanyidsec] [int] NULL,
	[cv_12_generalstandard] [varchar](3) NULL,
	[cv_12_lensreplacementreason] [int] NULL,
	[cv_13_a] [varchar](3) NULL,
	[cv_13_b] [varchar](3) NULL,
	[cv_13_c] [varchar](3) NULL,
	[cv_14_framereplacementreason] [int] NULL,
	[cv_15_prescriptiondate] [datetime] NULL,
	[cv_20_feepaid] [varchar](3) NULL,
	[cv_12_text] [varchar](50) NULL,
	[cv_13_text] [varchar](50) NULL,
	[cv_25_providernumber] [varchar](50) NULL,
	[cv_27_providerphone] [varchar](20) NULL,
	[holddt] [datetime] NULL,
	[locationid] [int] NULL,
	[facilityaddress_1] [varchar](100) NULL,
	[facilityaddress_2] [varchar](100) NULL,
	[facilityaddress_3] [varchar](100) NULL,
	[facilityaddress_4] [varchar](100) NULL,
	[facilityaddress_5] [varchar](100) NULL,
	[physicianaddress_1] [varchar](100) NULL,
	[physicianaddress_2] [varchar](100) NULL,
	[physicianaddress_3] [varchar](100) NULL,
	[physicianaddress_4] [varchar](100) NULL,
	[physicianaddress_5] [varchar](100) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[vsp_orderstatus] [varchar](50) NULL,
	[vsp_auth_id] [int] NULL,
	[vsp_response_text] [text] NULL,
	[vsp_status_dt] [datetime] NULL,
	[npi_32a] [varchar](50) NULL,
	[id_32b] [varchar](50) NULL,
	[npi_33a] [varchar](50) NULL,
	[id_33b] [varchar](50) NULL,
	[phone_33] [varchar](50) NULL,
	[npi_17b] [varchar](50) NULL,
	[idtype_17a] [varchar](50) NULL,
	[id_17a] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[facility_id] [int] NULL,
	[otherpatientrelationship] [varchar](50) NULL,
 CONSTRAINT [PK_HCFA] PRIMARY KEY CLUSTERED 
(
	[hcfaid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[hipaa]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[hipaa](
	[hipaaid] [int] NOT NULL,
	[patientid] [int] NOT NULL,
	[formtypeid] [int] NOT NULL,
	[expirydt] [datetime] NULL,
	[notes] [varchar](500) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_hipaa] PRIMARY KEY CLUSTERED 
(
	[hipaaid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[contactlensname]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[contactlensname](
	[nameid] [int] NOT NULL,
	[manufacturerid] [int] NOT NULL,
	[lensname] [varchar](50) NOT NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[active_flag] [varchar](3) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_ContactLensName] PRIMARY KEY CLUSTERED 
(
	[nameid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[calendar]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[calendar](
	[calendarid] [int] NOT NULL,
	[calendarmasterid] [int] NOT NULL,
	[start_time] [datetime] NOT NULL,
	[duration] [int] NULL,
	[patientid] [int] NULL,
	[subjectid] [int] NULL,
	[locationid] [int] NULL,
	[confirmationid] [int] NULL,
	[confirmationdt] [datetime] NULL,
	[authorizationnum] [varchar](50) NULL,
	[notes] [varchar](5000) NULL,
	[physicianid] [int] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[calendar_date] [datetime] NULL,
	[cancellationreasonid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[calendar_recurrence_id] [int] NULL,
	[waitlist] [varchar](3) NULL,
	[waitlist_priority] [smallint] NULL,
	[show_status] [varchar](50) NULL,
	[orderid] [int] NULL,
	[appt_status] [varchar](50) NULL,
	[cancel_reason] [varchar](50) NULL,
	[created_by] [varchar](50) NULL,
	[created_date] [datetime] NULL,
 CONSTRAINT [PK_calendar] PRIMARY KEY CLUSTERED 
(
	[calendarid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[domain]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[domain](
	[domainid] [int] IDENTITY(1,1) NOT NULL,
	[domaintypeid] [int] NOT NULL,
	[domain] [varchar](500) NULL,
	[systemdesc] [varchar](500) NULL,
	[isdefault] [varchar](3) NULL,
	[systemdefined] [varchar](3) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[colorcode] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[sort_order] [smallint] NULL,
	[vsp_code] [varchar](10) NULL,
	[vsp_description] [varchar](500) NULL,
	[lens_style_type] [varchar](50) NULL,
	[lens_progessive_flag] [varchar](3) NULL,
	[lens_progressive_amount] [money] NULL,
	[lens_material_vcode_id] [int] NULL,
 CONSTRAINT [PK_domain] PRIMARY KEY CLUSTERED 
(
	[domainid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam](
	[exam_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_type] [varchar](100) NULL,
	[template_description] [varchar](500) NULL,
	[template_systemdefined] [varchar](3) NULL,
	[template_name] [varchar](100) NULL,
	[template_id] [int] NULL,
	[exam_dt] [datetime] NULL,
	[exam_room] [varchar](50) NULL,
	[exam_status] [varchar](50) NULL,
	[locked_dt] [datetime] NULL,
	[locked_by] [varchar](50) NULL,
	[locationid] [int] NULL,
	[patient_id] [int] NULL,
	[patient_type_flag] [varchar](50) NULL,
	[exam_priority] [varchar](50) NULL,
	[waitingroom_time] [smalldatetime] NULL,
	[checkedin_time] [smalldatetime] NULL,
	[checkedout_time] [smalldatetime] NULL,
	[physician_id] [int] NULL,
	[insurance_id] [int] NULL,
	[insurance_plan_id] [int] NULL,
	[last_exam_when] [varchar](100) NULL,
	[last_exam_location] [varchar](100) NULL,
	[last_exam_dt] [datetime] NULL,
	[last_exam_records] [varchar](100) NULL,
	[primary_care_physician] [varchar](100) NULL,
	[accompanied_by] [varchar](100) NULL,
	[accompanied_by_relationship] [varchar](100) NULL,
	[chief_complaint_patient] [varchar](500) NULL,
	[location] [varchar](500) NULL,
	[quality] [varchar](500) NULL,
	[severity] [varchar](500) NULL,
	[duration] [varchar](500) NULL,
	[timing] [varchar](500) NULL,
	[context] [varchar](500) NULL,
	[modifying_factors] [varchar](500) NULL,
	[symptoms] [varchar](500) NULL,
	[chief_complaint] [varchar](2000) NULL,
	[chief_complaint_other] [varchar](1000) NULL,
	[chief_complaint_notes] [varchar](1000) NULL,
	[exam_alert] [varchar](1000) NULL,
	[ros_note] [varchar](1000) NULL,
	[ros_note_additional] [varchar](1000) NULL,
	[ros_activate_flag] [varchar](3) NULL,
	[ros_status] [varchar](100) NULL,
	[ros_copy_exam_dt] [smalldatetime] NULL,
	[ros_mental_orientation] [varchar](100) NULL,
	[ros_mental_mood] [varchar](100) NULL,
	[ros_mental_headface] [varchar](100) NULL,
	[computer_flag] [varchar](10) NULL,
	[computer_hours] [varchar](100) NULL,
	[computer_usage] [varchar](100) NULL,
	[computer_ergonomic] [varchar](100) NULL,
	[computer_cl_worn] [varchar](10) NULL,
	[ocular_history] [varchar](5000) NULL,
	[medical_history] [varchar](5000) NULL,
	[medications_ocular] [varchar](5000) NULL,
	[medications_systemic] [varchar](5000) NULL,
	[allergies] [varchar](5000) NULL,
	[family_history] [varchar](5000) NULL,
	[social_history_tobacco] [varchar](100) NULL,
	[social_history_drugs] [varchar](100) NULL,
	[social_history_alcohol] [varchar](100) NULL,
	[social_history_other] [varchar](100) NULL,
	[social_history] [varchar](1000) NULL,
	[pfsh_note] [varchar](5000) NULL,
	[diabetic_glucosetolerancetest] [varchar](100) NULL,
	[diabetic] [varchar](10) NULL,
	[dilation_flag] [varchar](10) NULL,
	[dilation_start] [datetime] NULL,
	[dilation] [varchar](500) NULL,
	[dilation_pt_sign] [varchar](100) NULL,
	[dilation_witness] [varchar](100) NULL,
	[exam_method] [varchar](500) NULL,
	[bloodpressure_systolic] [varchar](20) NULL,
	[bloodpressure_diastolic] [varchar](20) NULL,
	[bloodpressure_arm] [varchar](20) NULL,
	[bloodpressure_pulse] [varchar](20) NULL,
	[cvf] [varchar](100) NULL,
	[perrla] [varchar](100) NULL,
	[apd] [varchar](100) NULL,
	[versions] [varchar](100) NULL,
	[cnp] [varchar](100) NULL,
	[cover_test_far_lat_prism] [varchar](100) NULL,
	[cover_test_far_lat_eyetype] [varchar](100) NULL,
	[cover_test_far_vert_prism] [varchar](100) NULL,
	[cover_test_far_vert_eyetype] [varchar](100) NULL,
	[cover_test_near_lat_prism] [varchar](100) NULL,
	[cover_test_near_lat_eyetype] [varchar](100) NULL,
	[cover_test_near_vert_prism] [varchar](100) NULL,
	[cover_test_near_vert_eyetype] [varchar](100) NULL,
	[entrance_notes] [varchar](500) NULL,
	[dominant_eye] [varchar](50) NULL,
	[dominant_hand] [varchar](50) NULL,
	[visual_acuity_unaided_od_dist] [varchar](20) NULL,
	[visual_acuity_unaided_od_near] [varchar](20) NULL,
	[visual_acuity_unaided_od_pinhole] [varchar](20) NULL,
	[visual_acuity_unaided_os_dist] [varchar](20) NULL,
	[visual_acuity_unaided_os_near] [varchar](20) NULL,
	[visual_acuity_unaided_os_pinhole] [varchar](20) NULL,
	[visual_acuity_unaided_ou_dist] [varchar](20) NULL,
	[visual_acuity_unaided_ou_near] [varchar](20) NULL,
	[visual_acuity_unaided_ou_pinhole] [varchar](20) NULL,
	[vergence_bi_dist_blur] [varchar](20) NULL,
	[vergence_bi_dist_break] [varchar](20) NULL,
	[vergence_bi_dist_recv] [varchar](20) NULL,
	[vergence_bi_near_blur] [varchar](20) NULL,
	[vergence_bi_near_break] [varchar](20) NULL,
	[vergence_bi_near_recv] [varchar](20) NULL,
	[vergence_bo_dist_blur] [varchar](20) NULL,
	[vergence_bo_dist_break] [varchar](20) NULL,
	[vergence_bo_dist_recv] [varchar](20) NULL,
	[vergence_bo_near_blur] [varchar](20) NULL,
	[vergence_bo_near_break] [varchar](20) NULL,
	[vergence_bo_near_recv] [varchar](20) NULL,
	[vergence_bd_dist_break] [varchar](20) NULL,
	[vergence_bd_dist_recv] [varchar](20) NULL,
	[vergence_bd_near_break] [varchar](20) NULL,
	[vergence_bd_near_recv] [varchar](20) NULL,
	[vergence_bu_dist_break] [varchar](20) NULL,
	[vergence_bu_dist_recv] [varchar](20) NULL,
	[vergence_bu_near_break] [varchar](20) NULL,
	[vergence_bu_near_recv] [varchar](20) NULL,
	[phoria_dist_horiz] [varchar](20) NULL,
	[phoria_dist_vert] [varchar](20) NULL,
	[phoria_near_horiz] [varchar](20) NULL,
	[phoria_near_vert] [varchar](20) NULL,
	[npa_od] [varchar](10) NULL,
	[npa_os] [varchar](10) NULL,
	[npa_ou] [varchar](10) NULL,
	[aca] [varchar](20) NULL,
	[b14] [varchar](20) NULL,
	[nra_blur] [varchar](20) NULL,
	[nra_recv] [varchar](20) NULL,
	[pra_blur] [varchar](20) NULL,
	[pra_recv] [varchar](20) NULL,
	[npc_test] [varchar](50) NULL,
	[npc_blur] [varchar](20) NULL,
	[npc_break] [varchar](20) NULL,
	[npc_recv] [varchar](20) NULL,
	[fusion_test] [varchar](20) NULL,
	[fusion_dist] [varchar](20) NULL,
	[fusion_near] [varchar](20) NULL,
	[fusion_impression] [varchar](20) NULL,
	[steropsis_test] [varchar](20) NULL,
	[steropsis_dist] [varchar](20) NULL,
	[steropsis_near] [varchar](20) NULL,
	[binolcular_note] [varchar](1000) NULL,
	[prism_tests_note] [varchar](500) NULL,
	[internal_image] [image] NULL,
	[external_image] [image] NULL,
	[internal_note] [varchar](2000) NULL,
	[external_note] [varchar](2000) NULL,
	[av_ratio_od] [varchar](20) NULL,
	[av_ratio_os] [varchar](20) NULL,
	[cd_ratio_hor_od] [varchar](20) NULL,
	[cd_ratio_hor_os] [varchar](20) NULL,
	[cd_ratio_vert_od] [varchar](20) NULL,
	[cd_ratio_vert_os] [varchar](20) NULL,
	[diagnosis_1] [varchar](10) NULL,
	[diagnosis_2] [varchar](10) NULL,
	[diagnosis_3] [varchar](10) NULL,
	[diagnosis_4] [varchar](10) NULL,
	[diagnosis_5] [varchar](10) NULL,
	[diagnosis_6] [varchar](10) NULL,
	[diagnosis_7] [varchar](10) NULL,
	[diagnosis_8] [varchar](10) NULL,
	[diagnosis_id_1] [int] NULL,
	[diagnosis_id_2] [int] NULL,
	[diagnosis_id_3] [int] NULL,
	[diagnosis_id_4] [int] NULL,
	[diagnosis_id_5] [int] NULL,
	[diagnosis_id_6] [int] NULL,
	[diagnosis_id_7] [int] NULL,
	[diagnosis_id_8] [int] NULL,
	[signed_flag] [varchar](20) NULL,
	[signature_image] [image] NULL,
	[signature_dt] [smalldatetime] NULL,
	[assessment] [varchar](5000) NULL,
	[plan_exam] [varchar](5000) NULL,
	[referred] [varchar](100) NULL,
	[medical_decision_making] [varchar](100) NULL,
	[recall_dt] [smalldatetime] NULL,
	[recallreason_id] [int] NULL,
	[recall_dt2] [smalldatetime] NULL,
	[recallreason_id2] [int] NULL,
	[recallmonths] [int] NULL,
	[recallmonths2] [int] NULL,
	[sooner_if_symptoms_worsen] [varchar](20) NULL,
	[notes_additional] [varchar](1000) NULL,
	[visual_fields_type] [varchar](50) NULL,
	[visual_fields_od] [varchar](500) NULL,
	[visual_fields_os] [varchar](500) NULL,
	[visual_fields_result] [varchar](200) NULL,
	[gonioscopy_od] [varchar](500) NULL,
	[gonioscopy_os] [varchar](500) NULL,
	[amsler_image] [image] NULL,
	[gonioscopy_image] [image] NULL,
	[pachymetry_od] [varchar](20) NULL,
	[pachymetry_os] [varchar](20) NULL,
	[pachymetry_type] [varchar](20) NULL,
	[pachymetry_findings_applied] [varchar](50) NULL,
	[fundus_photo_taken] [varchar](50) NULL,
	[corneal_topography_taken] [varchar](50) NULL,
	[ancillary_notes] [varchar](1000) NULL,
	[custom_field_20] [varchar](5000) NULL,
	[custom_field_19] [varchar](5000) NULL,
	[custom_field_18] [varchar](5000) NULL,
	[custom_field_17] [varchar](5000) NULL,
	[custom_field_16] [varchar](5000) NULL,
	[custom_field_15] [varchar](5000) NULL,
	[custom_field_14] [varchar](5000) NULL,
	[custom_field_13] [varchar](5000) NULL,
	[custom_field_12] [varchar](5000) NULL,
	[custom_field_11] [varchar](5000) NULL,
	[custom_field_10] [varchar](5000) NULL,
	[custom_field_9] [varchar](5000) NULL,
	[custom_field_8] [varchar](5000) NULL,
	[custom_field_7] [varchar](5000) NULL,
	[custom_field_6] [varchar](5000) NULL,
	[custom_field_5] [varchar](5000) NULL,
	[custom_field_4] [varchar](5000) NULL,
	[custom_field_3] [varchar](5000) NULL,
	[custom_field_1] [varchar](5000) NULL,
	[custom_field_2] [varchar](5000) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[referring_physician_id] [int] NULL,
	[referred_to_physician_id] [int] NULL,
	[keratometry_method] [varchar](50) NULL,
	[keratometry_datetime] [smalldatetime] NULL,
	[keratometry_od_horiz_diopter] [varchar](50) NULL,
	[keratometry_od_horiz_axis] [varchar](50) NULL,
	[keratometry_od_vert_diopter] [varchar](50) NULL,
	[keratometry_od_vert_axis] [varchar](50) NULL,
	[keratometry_od_mires] [varchar](50) NULL,
	[keratometry_od_horiz_mm] [varchar](50) NULL,
	[keratometry_od_vert_mm] [varchar](50) NULL,
	[keratometry_os_horiz_diopter] [varchar](50) NULL,
	[keratometry_os_horiz_axis] [varchar](50) NULL,
	[keratometry_os_vert_diopter] [varchar](50) NULL,
	[keratometry_os_vert_axis] [varchar](50) NULL,
	[keratometry_os_mires] [varchar](50) NULL,
	[keratometry_os_horiz_mm] [varchar](50) NULL,
	[keratometry_os_vert_mm] [varchar](50) NULL,
	[vectographic_horiz_prism] [varchar](50) NULL,
	[vectographic_horiz_type] [varchar](50) NULL,
	[vectographic_horiz_prism_range_from] [varchar](50) NULL,
	[vectographic_horiz_prism_range_from_type] [varchar](50) NULL,
	[vectographic_horiz_prism_range_to] [varchar](50) NULL,
	[vectographic_horiz_prism_range_to_type] [varchar](50) NULL,
	[vectographic_vert_prism] [varchar](50) NULL,
	[vectographic_vert_type] [varchar](50) NULL,
	[vectographic_vert_prism_range_from] [varchar](50) NULL,
	[vectographic_vert_prism_range_from_type] [varchar](50) NULL,
	[vectographic_vert_prism_range_to] [varchar](50) NULL,
	[vectographic_vert_prism_range_to_type] [varchar](50) NULL,
	[disc_size] [varchar](50) NULL,
	[disc_type] [varchar](50) NULL,
	[b14_od] [varchar](50) NULL,
	[b14_os] [varchar](50) NULL,
	[b14_ou] [varchar](50) NULL,
	[eye_color] [varchar](50) NULL,
	[internal_note_doctor] [varchar](1000) NULL,
	[external_note_doctor] [varchar](1000) NULL,
	[internal_edit_flag] [varchar](20) NULL,
	[external_edit_flag] [varchar](20) NULL,
	[ros_edit_flag] [varchar](20) NULL,
	[phoria_dist_horiz_result] [varchar](20) NULL,
	[phoria_near_horiz_result] [varchar](20) NULL,
	[phoria_dist_vert_result] [varchar](20) NULL,
	[phoria_dist_vert_result_eye] [varchar](20) NULL,
	[phoria_near_vert_result] [varchar](20) NULL,
	[phoria_near_vert_result_eye] [varchar](20) NULL,
	[glare_tinted_glasses] [varchar](50) NULL,
	[glare_indoors] [varchar](50) NULL,
	[glare_outdoors] [varchar](50) NULL,
	[glare_computer] [varchar](50) NULL,
	[glare_notes] [varchar](100) NULL,
	[lowvision_near_head_borne] [varchar](50) NULL,
	[lowvision_near_hand] [varchar](50) NULL,
	[lowvision_near_stand] [varchar](50) NULL,
	[lowvision_near_electronic_systems] [varchar](50) NULL,
	[lowvision_far_custom_TS] [varchar](50) NULL,
	[lowvision_far_Premade_TS] [varchar](50) NULL,
	[lowvision_far_electronic_systems] [varchar](50) NULL,
	[lowvision_read_lp] [varchar](50) NULL,
	[lowvision_fieldexpansion] [varchar](500) NULL,
	[lowvision_computer_modification] [varchar](50) NULL,
	[lowvision_monitor_size] [varchar](50) NULL,
	[lowvision_special_keyboard] [varchar](50) NULL,
	[lowvision_software_speech_or_lp] [varchar](50) NULL,
	[lowvision_telemicroscope_1] [varchar](50) NULL,
	[lowvision_telemicroscope_2] [varchar](50) NULL,
	[lowvision_notes] [varchar](500) NULL,
	[lowvision_treatment_plan_notes] [varchar](500) NULL,
	[binocular_test_name] [varchar](50) NULL,
	[binocular_test_note] [varchar](100) NULL,
	[custom_field_21] [varchar](5000) NULL,
	[custom_field_22] [varchar](5000) NULL,
	[custom_field_23] [varchar](5000) NULL,
	[custom_field_24] [varchar](5000) NULL,
	[custom_field_25] [varchar](5000) NULL,
	[custom_field_26] [varchar](5000) NULL,
	[custom_field_27] [varchar](5000) NULL,
	[custom_field_28] [varchar](5000) NULL,
	[custom_field_29] [varchar](5000) NULL,
	[custom_field_30] [varchar](5000) NULL,
	[custom_field_31] [varchar](5000) NULL,
	[custom_field_32] [varchar](5000) NULL,
	[custom_field_33] [varchar](5000) NULL,
	[custom_field_34] [varchar](5000) NULL,
	[custom_field_35] [varchar](5000) NULL,
	[custom_field_36] [varchar](5000) NULL,
	[custom_field_37] [varchar](5000) NULL,
	[custom_field_38] [varchar](5000) NULL,
	[custom_field_39] [varchar](5000) NULL,
	[custom_field_40] [varchar](5000) NULL,
	[custom_field_41] [varchar](5000) NULL,
	[custom_field_42] [varchar](5000) NULL,
	[custom_field_43] [varchar](5000) NULL,
	[custom_field_44] [varchar](5000) NULL,
	[custom_field_45] [varchar](5000) NULL,
	[custom_field_46] [varchar](5000) NULL,
	[custom_field_47] [varchar](5000) NULL,
	[custom_field_48] [varchar](5000) NULL,
	[custom_field_49] [varchar](5000) NULL,
	[custom_field_50] [varchar](5000) NULL,
	[bloodpressure_datetime] [smalldatetime] NULL,
	[time_spent_on_exam] [smallint] NULL,
	[time_spent_with_doctor] [smallint] NULL,
	[time_spent_counselling] [smallint] NULL,
	[exam_physical_flag] [varchar](50) NULL,
	[mdm_sx_or_dx] [varchar](50) NULL,
	[mdm_data_review] [varchar](50) NULL,
	[mdm_risk] [varchar](50) NULL,
	[history_outcome] [varchar](50) NULL,
	[physical_outcome] [nchar](10) NULL,
	[type_of_exam] [varchar](50) NULL,
	[type_of_exam_overridden_flag] [varchar](50) NULL,
	[letter_id] [int] NULL,
	[lock_dt] [smalldatetime] NULL,
	[referred_dt] [smalldatetime] NULL,
	[referred_expected_return_dt] [smalldatetime] NULL,
	[referred_actual_return_dt] [smalldatetime] NULL,
	[ros_reviewed_flag] [varchar](3) NULL,
	[ros_reviewed_dt] [smalldatetime] NULL,
	[ros_reviewed_by] [varchar](50) NULL,
	[internal_picture_were_taken_flag] [varchar](3) NULL,
	[height_feet] [tinyint] NULL,
	[height_inches] [tinyint] NULL,
	[weight] [smallmoney] NULL,
	[bmi] [smallmoney] NULL,
 CONSTRAINT [PK_EXAM] PRIMARY KEY CLUSTERED 
(
	[exam_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New or Established ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'exam', @level2type=N'COLUMN',@level2name=N'patient_type_flag'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'priority of this exam - as opposed to others' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'exam', @level2type=N'COLUMN',@level2name=N'exam_priority'

/****** Object:  Table [dbo].[commission_products]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[commission_products](
	[product_type] [varchar](10) NOT NULL,
	[structure_id] [int] NULL,
	[structure_type] [char](10) NULL,
	[gross_percentage] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[locationid] [int] NOT NULL,
	[commission_products_id] [int] IDENTITY(1,1) NOT NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_commission_products] PRIMARY KEY CLUSTERED 
(
	[commission_products_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[report_affections]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[report_affections](
	[affection_id] [int] IDENTITY(1,1) NOT NULL,
	[available_flag] [char](1) NULL,
	[default_flag] [char](1) NULL,
	[dataobject] [varchar](500) NOT NULL,
	[id_customization] [numeric](9, 0) NOT NULL,
	[security_group_id] [int] NOT NULL,
 CONSTRAINT [PK_report_affections] PRIMARY KEY CLUSTERED 
(
	[affection_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[receipt_note]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[receipt_note](
	[receipt_note_id] [int] IDENTITY(1,1) NOT NULL,
	[locationid] [int] NULL,
	[receipt_name] [varchar](100) NULL,
	[receipt_message] [varchar](2000) NULL,
	[isdefault] [varchar](3) NULL,
	[from_days] [int] NULL,
	[to_days] [int] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[providerinsurance]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[providerinsurance](
	[providerinsuranceid] [int] NOT NULL,
	[providerid] [int] NOT NULL,
	[domainid] [int] NULL,
	[providerinsurancenumber] [varchar](20) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_ProviderInsurance] PRIMARY KEY CLUSTERED 
(
	[providerinsuranceid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectacleinventory]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectacleinventory](
	[specid] [int] NOT NULL,
	[detailed_domain_id] [int] NULL,
	[speclensstyle] [int] NULL,
	[materialid] [int] NULL,
	[od] [varchar](3) NULL,
	[sphere] [decimal](5, 2) NULL,
	[cylinder] [money] NULL,
	[ssize] [decimal](5, 2) NULL,
	[sadd] [decimal](5, 2) NULL,
	[basecurve] [decimal](5, 2) NULL,
	[quantitysold] [int] NULL,
	[quantitypurchased] [int] NULL,
	[cost] [money] NULL,
	[retailprice] [money] NULL,
	[taxable] [varchar](3) NULL,
	[notes] [varchar](500) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[upccode] [varchar](12) NULL,
	[structure_id] [int] NULL,
	[structure_type] [varchar](1) NULL,
	[gross_percentage] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[deleted] [varchar](3) NULL,
	[inventory] [varchar](3) NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[unitmeasure] [varchar](50) NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[procedure_price_1] [smallmoney] NULL,
	[procedure_id_2] [int] NULL,
	[procedure_price_2] [smallmoney] NULL,
	[procedure_id_3] [int] NULL,
	[procedure_price_3] [smallmoney] NULL,
	[procedure_id_4] [int] NULL,
	[procedure_price_4] [smallmoney] NULL,
	[mfg_name] [varchar](20) NULL,
	[class_name] [varchar](20) NULL,
	[product_description] [varchar](100) NULL,
	[material_name] [varchar](50) NULL,
	[material_brand] [varchar](20) NULL,
	[product_name] [varchar](255) NULL,
	[style_name] [varchar](50) NULL,
	[filter] [varchar](50) NULL,
	[center_thick] [decimal](5, 1) NULL,
	[edge_thick] [decimal](5, 1) NULL,
	[sphere_high] [decimal](5, 2) NULL,
	[cylinder_high] [decimal](5, 2) NULL,
	[min_ht] [decimal](5, 0) NULL,
	[effective_diam] [decimal](5, 0) NULL,
	[pricing_option] [varchar](10) NULL,
	[vsp_code] [varchar](10) NULL,
	[color_code] [varchar](50) NULL,
	[returnable_flag] [varchar](3) NULL,
	[supplierid] [int] NULL,
	[pricing_type] [varchar](50) NULL,
	[spectaclelens_pricing_id] [int] NULL,
	[max_add_size] [decimal](5, 3) NULL,
	[max_diameter] [decimal](5, 3) NULL,
	[oversize_diameter_charge] [decimal](5, 3) NULL,
	[spectacle_lens_range_id] [int] NULL,
 CONSTRAINT [PK_SpectacleInventory] PRIMARY KEY CLUSTERED 
(
	[specid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'whether lens is retail only or retail and grid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'spectacleinventory', @level2type=N'COLUMN',@level2name=N'pricing_type'

/****** Object:  Table [dbo].[recall_schedule]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[recall_schedule](
	[recall_schedule_id] [int] IDENTITY(1,1) NOT NULL,
	[recall_reason_id] [int] NULL,
	[recall_interval] [int] NULL,
	[recall_period_type] [varchar](50) NULL,
	[recall_when] [varchar](50) NULL,
	[recall_date_type] [varchar](50) NULL,
	[letter_id] [int] NULL,
	[recall_type] [varchar](50) NULL,
	[sort_order] [int] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_recall_schedule] PRIMARY KEY CLUSTERED 
(
	[recall_schedule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[physicianavailexcept_audit]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[physicianavailexcept_audit](
	[audit_program_name] [varchar](150) NULL,
	[audit_last_location] [varchar](150) NULL,
	[audit_updated_dt] [datetime] NULL,
	[audit_updated_by] [varchar](150) NULL,
	[audit_operation_type] [varchar](25) NULL,
	[physicianavailexceptid] [int] NOT NULL,
	[physicianid] [int] NULL,
	[startdt] [datetime] NOT NULL,
	[enddt] [datetime] NULL,
	[noenddt] [varchar](3) NULL,
	[starttime] [datetime] NOT NULL,
	[endtime] [datetime] NOT NULL,
	[duration] [int] NULL,
	[recurrencetype] [varchar](50) NULL,
	[daynumber] [tinyint] NULL,
	[dayname] [varchar](50) NULL,
	[dayfrequency] [varchar](50) NULL,
	[monthsfrequency] [tinyint] NULL,
	[available] [varchar](3) NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[reason] [varchar](100) NULL,
	[locationid] [int] NULL,
	[week_number] [tinyint] NULL,
	[week_sunday] [varchar](3) NULL,
	[week_monday] [varchar](3) NULL,
	[week_tuesday] [varchar](3) NULL,
	[week_wednesday] [varchar](3) NULL,
	[week_thursday] [varchar](3) NULL,
	[week_friday] [varchar](3) NULL,
	[week_saturday] [varchar](3) NULL,
 CONSTRAINT [PK_physicianavailexcept_audit] PRIMARY KEY CLUSTERED 
(
	[physicianavailexceptid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[physicianavailexcept]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[physicianavailexcept](
	[physicianavailexceptid] [int] NOT NULL,
	[physicianid] [int] NULL,
	[startdt] [datetime] NOT NULL,
	[enddt] [datetime] NULL,
	[noenddt] [varchar](3) NULL,
	[starttime] [datetime] NOT NULL,
	[endtime] [datetime] NOT NULL,
	[duration] [int] NULL,
	[recurrencetype] [varchar](50) NOT NULL,
	[daynumber] [tinyint] NULL,
	[dayname] [varchar](50) NULL,
	[dayfrequency] [varchar](50) NULL,
	[monthsfrequency] [tinyint] NULL,
	[available] [varchar](3) NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[reason] [varchar](100) NULL,
	[locationid] [int] NULL,
	[week_number] [tinyint] NULL,
	[week_sunday] [varchar](3) NULL,
	[week_monday] [varchar](3) NULL,
	[week_tuesday] [varchar](3) NULL,
	[week_wednesday] [varchar](3) NULL,
	[week_thursday] [varchar](3) NULL,
	[week_friday] [varchar](3) NULL,
	[week_saturday] [varchar](3) NULL,
 CONSTRAINT [PK_physicianavailexcept] PRIMARY KEY CLUSTERED 
(
	[physicianavailexceptid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[physicianavailability]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[physicianavailability](
	[physavailid] [int] NOT NULL,
	[physicianid] [int] NOT NULL,
	[dayname] [varchar](10) NULL,
	[fromtime] [datetime] NULL,
	[totime] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[availserviceid] [int] NULL,
 CONSTRAINT [PK_physicianavailability] PRIMARY KEY CLUSTERED 
(
	[physavailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orders]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orders](
	[orderid] [int] NOT NULL,
	[parentorderid] [int] NULL,
	[ordertypeid] [int] NOT NULL,
	[orderdate] [datetime] NULL,
	[employeeid] [int] NOT NULL,
	[patientid] [int] NULL,
	[prescriptionid] [int] NULL,
	[physicianid] [int] NULL,
	[marketingid] [int] NULL,
	[layaway] [varchar](3) NULL,
	[amountpatientdue] [money] NULL,
	[amountinsurancedue] [money] NULL,
	[amountinsurancepaid] [money] NULL,
	[diagnisisid_1] [int] NULL,
	[diagnisisid_2] [int] NULL,
	[diagnisisid_3] [int] NULL,
	[diagnisisid_4] [int] NULL,
	[recalldate] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[duplication] [varchar](3) NULL,
	[amountinspatuncollectedpd] [money] NULL,
	[orderstatusid] [int] NULL,
	[supplierid] [int] NULL,
	[trainingverifiedemployeeid] [int] NULL,
	[traininggivenemployeeid] [int] NULL,
	[promisedate] [datetime] NULL,
	[shippingstatusid] [int] NULL,
	[traynumber] [varchar](20) NULL,
	[labreference] [varchar](30) NULL,
	[fitbyemployeeid] [int] NULL,
	[fitdate] [datetime] NULL,
	[orderedbyemployeeid] [int] NULL,
	[ordereddate] [datetime] NULL,
	[receivedbyemployeeid] [int] NULL,
	[receiveddate] [datetime] NULL,
	[notifiedbyemployeeid] [int] NULL,
	[notifieddate] [datetime] NULL,
	[deliveredbyemployeeid] [int] NULL,
	[delivereddate] [datetime] NULL,
	[orderactive] [varchar](3) NULL,
	[remake_orderid] [int] NULL,
	[remakereasonid] [int] NULL,
	[labid] [int] NULL,
	[labamount] [money] NULL,
	[labsentdt] [datetime] NULL,
	[recalls] [tinyint] NULL,
	[canceledreasonid] [int] NULL,
	[canceledbyemployeeid] [int] NULL,
	[canceleddate] [int] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[insuranceid] [int] NULL,
	[insurance_plan_id] [int] NULL,
	[receipt_note_id] [int] NULL,
	[auth_id] [int] NULL,
	[vsp_order_no] [varchar](50) NULL,
	[vsp_response_text] [text] NULL,
	[vsp_status] [varchar](50) NULL,
	[status_dt] [smalldatetime] NULL,
	[submitted_dt] [smalldatetime] NULL,
	[submitted_by_employeeid] [int] NULL,
	[bfla] [money] NULL,
	[bfra] [money] NULL,
	[bflc] [varchar](2) NULL,
	[manm] [varchar](2) NULL,
	[mat] [varchar](10) NULL,
	[maa] [money] NULL,
	[copay_exam] [money] NULL,
	[copay_material] [money] NULL,
	[copay_total] [money] NULL,
	[copay_lens] [money] NULL,
	[copay_frame] [money] NULL,
	[pfrl] [varchar](10) NULL,
	[refraction_flag] [varchar](3) NULL,
	[dilation_performed_flag] [varchar](3) NULL,
	[lens_flag] [varchar](3) NULL,
	[frame_flag] [varchar](3) NULL,
	[contacts_flag] [varchar](3) NULL,
	[exam_flag] [varchar](3) NULL,
	[new_patient_flag] [varchar](3) NULL,
	[contact_lens_reason_id] [int] NULL,
	[contact_lens_annual_supply_id] [int] NULL,
	[contact_lens_option] [varchar](10) NULL,
	[transfer_flag] [int] NULL,
	[txnid] [varchar](36) NULL,
	[uncut_lens_flag] [varchar](3) NULL,
	[vsp_cl_covered_units] [smallint] NULL,
	[custom_field_1] [varchar](5000) NULL,
	[custom_field_2] [varchar](5000) NULL,
	[custom_field_3] [varchar](5000) NULL,
	[custom_field_4] [varchar](5000) NULL,
	[custom_field_5] [varchar](5000) NULL,
	[custom_field_6] [varchar](5000) NULL,
	[custom_field_7] [varchar](5000) NULL,
	[custom_field_8] [varchar](5000) NULL,
	[custom_field_9] [varchar](5000) NULL,
	[custom_field_10] [varchar](5000) NULL,
	[custom_field_11] [varchar](5000) NULL,
	[custom_field_12] [varchar](5000) NULL,
	[custom_field_13] [varchar](5000) NULL,
	[custom_field_14] [varchar](5000) NULL,
	[custom_field_15] [varchar](5000) NULL,
	[custom_field_16] [varchar](5000) NULL,
	[custom_field_17] [varchar](5000) NULL,
	[custom_field_18] [varchar](5000) NULL,
	[custom_field_19] [varchar](5000) NULL,
	[custom_field_20] [varchar](5000) NULL,
	[exam_id] [int] NULL,
	[order_type] [varchar](100) NULL,
	[employee_name] [varchar](100) NULL,
	[physian_name] [varchar](100) NULL,
	[marketing_name] [varchar](100) NULL,
	[diagnosis_code_1] [varchar](50) NULL,
	[diagnosis_code_2] [varchar](50) NULL,
	[diagnosis_code_3] [varchar](50) NULL,
	[diagnosis_code_4] [varchar](50) NULL,
	[order_status] [varchar](100) NULL,
	[training_verified_employee_name] [varchar](100) NULL,
	[training_given_employee_name] [varchar](100) NULL,
	[shipping_status] [varchar](100) NULL,
	[shipping_carrier] [varchar](100) NULL,
	[shipping_tracking] [varchar](100) NULL,
	[fit_by_employee_name] [varchar](100) NULL,
	[ordered_by_employee_name] [varchar](100) NULL,
	[received_by_employee_name] [varchar](100) NULL,
	[notified_by_employee_name] [varchar](100) NULL,
	[delivered_by_employee_name] [varchar](100) NULL,
	[remake_reason] [varchar](100) NULL,
	[cancel_reason] [varchar](100) NULL,
	[cancel_by_employee_name] [varchar](100) NULL,
	[sms_message] [varchar](100) NULL,
	[sms_send_dt] [smalldatetime] NULL,
	[second_pair_flag] [varchar](3) NULL,
	[second_pair_original_orderid] [int] NULL,
	[secondary_insurance_id] [int] NULL,
	[secondary_insurance_plan_id] [int] NULL,
	[close_dt] [smalldatetime] NULL,
	[preferred_contact_method] [varchar](50) NULL,
	[preferred_contact_method_detail] [varchar](100) NULL,
	[secondary_insurance_due] [smallmoney] NULL,
	[secondary_insurance_paid] [smallmoney] NULL,
	[secondary_insurance_balance] [smallmoney] NULL,
	[shipping_status_dt] [smalldatetime] NULL,
	[user_defined_13] [varchar](100) NULL,
	[user_defined_12] [varchar](100) NULL,
	[user_defined_11] [varchar](100) NULL,
	[user_defined_10] [varchar](100) NULL,
	[user_defined_9] [varchar](100) NULL,
	[user_defined_8] [varchar](100) NULL,
	[user_defined_7] [varchar](100) NULL,
	[user_defined_6] [varchar](100) NULL,
	[user_defined_5] [varchar](100) NULL,
	[user_defined_4] [varchar](100) NULL,
	[user_defined_3] [varchar](100) NULL,
	[user_defined_2] [varchar](100) NULL,
	[user_defined_1] [varchar](100) NULL,
	[user_defined_20] [varchar](100) NULL,
	[user_defined_19] [varchar](100) NULL,
	[user_defined_18] [varchar](100) NULL,
	[user_defined_17] [varchar](100) NULL,
	[user_defined_16] [varchar](100) NULL,
	[user_defined_15] [varchar](100) NULL,
	[user_defined_14] [varchar](100) NULL,
	[material_allowance] [money] NULL,
 CONSTRAINT [PK_ORDERs] PRIMARY KEY CLUSTERED 
(
	[orderid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the Order is Active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'orders', @level2type=N'COLUMN',@level2name=N'orderactive'

/****** Object:  Table [dbo].[patient_insurance]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[patient_insurance](
	[patient_insurance_id] [int] IDENTITY(1,1) NOT NULL,
	[contactid] [int] NOT NULL,
	[entered_dt] [smalldatetime] NULL,
	[insurance_type] [varchar](50) NULL,
	[insurance_sequence] [varchar](50) NULL,
	[status_flag] [varchar](10) NULL,
	[insurance_company_id] [int] NULL,
	[insurance_plan_id] [int] NULL,
	[insurance_number] [varchar](50) NULL,
	[policy_group] [varchar](50) NULL,
	[relation_to_insured] [varchar](50) NULL,
	[insured_party] [varchar](50) NULL,
	[insured_patient_id] [int] NULL,
	[insured_lastname] [varchar](50) NULL,
	[insured_middlename] [varchar](50) NULL,
	[insured_firstname] [varchar](50) NULL,
	[insured_sex] [varchar](10) NULL,
	[insured_address] [varchar](50) NULL,
	[insured_address_2] [varchar](50) NULL,
	[insured_city] [varchar](50) NULL,
	[insured_state] [varchar](10) NULL,
	[insured_zip] [varchar](10) NULL,
	[insured_phone] [varchar](20) NULL,
	[insured_birthdate] [smalldatetime] NULL,
	[insured_sig_dt] [smalldatetime] NULL,
	[insured_employer_school] [varchar](50) NULL,
	[insured_ssn] [varchar](50) NULL,
	[family_medical_doctor] [varchar](50) NULL,
	[cms_secondary_health_flag] [varchar](10) NULL,
	[eligible_flag] [varchar](10) NULL,
	[authorization_dt] [smalldatetime] NULL,
	[authorization_num] [varchar](20) NULL,
	[authorization_expire_dt] [smalldatetime] NULL,
	[effective_dt] [smalldatetime] NULL,
	[end_dt] [smalldatetime] NULL,
	[image_front] [image] NULL,
	[image_back] [image] NULL,
	[max_yearly_visits] [smallint] NULL,
	[deductible] [smallmoney] NULL,
	[copay_office] [smallmoney] NULL,
	[copay_type] [varchar](20) NULL,
	[copay_er] [smallmoney] NULL,
	[copay_pharmacy] [smallmoney] NULL,
	[vsp_allow_wfa] [smallmoney] NULL,
	[vsp_allow_frameretail] [smallmoney] NULL,
	[vsp_allow_contacts] [smallmoney] NULL,
	[vsp_copay_exam] [smallmoney] NULL,
	[vsp_copay_frame] [smallmoney] NULL,
	[vsp_copay_material] [smallmoney] NULL,
	[vsp_copay_contacts] [smallmoney] NULL,
	[vsp_copay_total] [smallmoney] NULL,
	[notes] [varchar](5000) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_11] [varchar](50) NULL,
	[custom_field_12] [varchar](50) NULL,
	[custom_field_13] [varchar](50) NULL,
	[custom_field_14] [varchar](50) NULL,
	[custom_field_15] [varchar](50) NULL,
	[custom_field_16] [varchar](50) NULL,
	[custom_field_17] [varchar](50) NULL,
	[custom_field_18] [varchar](50) NULL,
	[custom_field_19] [varchar](50) NULL,
	[custom_field_20] [varchar](50) NULL,
	[custom_field_21] [varchar](50) NULL,
	[custom_field_22] [varchar](50) NULL,
	[custom_field_23] [varchar](50) NULL,
	[custom_field_24] [varchar](50) NULL,
	[custom_field_25] [varchar](50) NULL,
	[custom_field_26] [varchar](50) NULL,
	[custom_field_27] [varchar](50) NULL,
	[custom_field_28] [varchar](50) NULL,
	[custom_field_29] [varchar](50) NULL,
	[custom_field_30] [varchar](50) NULL,
	[custom_field_31] [varchar](50) NULL,
	[custom_field_32] [varchar](50) NULL,
	[custom_field_33] [varchar](50) NULL,
	[custom_field_34] [varchar](50) NULL,
	[custom_field_35] [varchar](50) NULL,
	[custom_field_36] [varchar](50) NULL,
	[custom_field_37] [varchar](50) NULL,
	[custom_field_38] [varchar](50) NULL,
	[custom_field_39] [varchar](50) NULL,
	[custom_field_40] [varchar](50) NULL,
	[custom_field_41] [varchar](50) NULL,
	[custom_field_42] [varchar](50) NULL,
	[custom_field_43] [varchar](50) NULL,
	[custom_field_44] [varchar](50) NULL,
	[custom_field_45] [varchar](50) NULL,
	[custom_field_46] [varchar](50) NULL,
	[custom_field_47] [varchar](50) NULL,
	[custom_field_48] [varchar](50) NULL,
	[custom_field_49] [varchar](50) NULL,
	[custom_field_50] [varchar](50) NULL,
	[material_allowance] [money] NULL,
 CONSTRAINT [PK_patient_insurance] PRIMARY KEY CLUSTERED 
(
	[patient_insurance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectacleinventory_opc]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectacleinventory_opc](
	[spectacle_opc_id] [int] IDENTITY(1,1) NOT NULL,
	[specid] [int] NOT NULL,
	[opc] [varchar](50) NULL,
 CONSTRAINT [PK_spectacleinventory_opc] PRIMARY KEY CLUSTERED 
(
	[spectacle_opc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectacleinventory_codes]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectacleinventory_codes](
	[spec_code_id] [int] IDENTITY(1,1) NOT NULL,
	[specid] [int] NOT NULL,
	[procedure_code_id] [int] NULL,
	[procedure_code] [varchar](50) NULL,
	[sort_order] [int] NULL,
	[retailprice] [money] NULL,
	[code_type] [varchar](50) NULL,
	[overwrite_flag] [varchar](3) NULL,
	[cost] [money] NULL,
 CONSTRAINT [PK_spectacleinventory_codes] PRIMARY KEY CLUSTERED 
(
	[spec_code_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[security_control_group]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[security_control_group](
	[security_control_group_id] [int] IDENTITY(1,1) NOT NULL,
	[status] [varchar](1) NULL,
	[user_name] [varchar](500) NULL,
	[function_id] [int] NULL,
	[security_group_id] [int] NULL,
	[lastUpdatedUser] [varchar](50) NULL,
	[lastUpdatedDt] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
 CONSTRAINT [PK_security_control_group] PRIMARY KEY CLUSTERED 
(
	[security_control_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[prescriptions]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[prescriptions](
	[prescriptionid] [int] IDENTITY(1,1) NOT NULL,
	[prescriptiontypeid] [int] NOT NULL,
	[patientid] [int] NOT NULL,
	[rxdate] [datetime] NULL,
	[expirydate] [datetime] NULL,
	[odaxis] [decimal](5, 2) NULL,
	[osaxis] [decimal](5, 2) NULL,
	[odadd] [decimal](5, 2) NULL,
	[osadd] [decimal](5, 2) NULL,
	[odflatk] [decimal](5, 2) NULL,
	[osflatk] [decimal](5, 2) NULL,
	[odaxisk] [decimal](5, 2) NULL,
	[osaxisk] [decimal](5, 2) NULL,
	[odsteepk] [decimal](5, 2) NULL,
	[ossteepk] [decimal](5, 2) NULL,
	[odkdiam] [decimal](5, 2) NULL,
	[oskdiam] [decimal](5, 2) NULL,
	[odaperture] [decimal](5, 2) NULL,
	[osaperture] [decimal](5, 2) NULL,
	[odpupdiam] [decimal](5, 2) NULL,
	[ospupdiam] [decimal](5, 2) NULL,
	[odthickness] [decimal](5, 2) NULL,
	[osthickness] [decimal](5, 2) NULL,
	[odprism] [decimal](5, 2) NULL,
	[osprism] [decimal](5, 2) NULL,
	[odbase] [decimal](5, 2) NULL,
	[osbase] [decimal](5, 2) NULL,
	[odbc] [decimal](5, 2) NULL,
	[osbc] [decimal](5, 2) NULL,
	[clensodmfg] [int] NULL,
	[clensosmfg] [int] NULL,
	[clensodname] [int] NULL,
	[clensosname] [int] NULL,
	[clensodbasecrv1] [decimal](5, 2) NULL,
	[clensosbasecrv1] [decimal](5, 2) NULL,
	[clensoddiam1] [decimal](5, 2) NULL,
	[clensosdiam1] [decimal](5, 2) NULL,
	[clensodcolor] [int] NULL,
	[clensoscolor] [int] NULL,
	[clensodbasecrv2] [decimal](5, 2) NULL,
	[clensosbasecrv2] [decimal](5, 2) NULL,
	[clensoddiam2] [decimal](5, 2) NULL,
	[clensosdiam2] [decimal](5, 2) NULL,
	[differentodos] [varchar](3) NULL,
	[physicianid] [int] NULL,
	[speclensmaterial] [int] NULL,
	[speclensstyle] [int] NULL,
	[comments] [varchar](1000) NULL,
	[odpdfar] [decimal](5, 2) NULL,
	[odpdnear] [decimal](5, 2) NULL,
	[ospdfar] [decimal](5, 2) NULL,
	[ospdnear] [decimal](5, 2) NULL,
	[oupdfar] [decimal](5, 2) NULL,
	[oupdnear] [decimal](5, 2) NULL,
	[seghtos] [decimal](5, 2) NULL,
	[seghtod] [decimal](5, 2) NULL,
	[segdios] [decimal](5, 2) NULL,
	[segdiod] [decimal](5, 2) NULL,
	[osaspheric] [varchar](10) NULL,
	[odaspheric] [varchar](10) NULL,
	[ozos] [decimal](5, 2) NULL,
	[ozod] [decimal](5, 2) NULL,
	[dotos] [varchar](3) NULL,
	[dotod] [varchar](3) NULL,
	[blendidos] [int] NULL,
	[blendidod] [int] NULL,
	[refills] [smallint] NULL,
	[refillsfilled] [smallint] NULL,
	[prescription_ind] [char](1) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[duplication] [varchar](3) NULL,
	[ossphere2] [decimal](5, 2) NULL,
	[odsphere2] [decimal](5, 2) NULL,
	[odpcr2] [varchar](10) NULL,
	[odpcw2] [varchar](10) NULL,
	[odpcr3] [varchar](10) NULL,
	[odpcw3] [varchar](10) NULL,
	[ospcr2] [varchar](10) NULL,
	[ospcw2] [varchar](10) NULL,
	[ospcw3] [varchar](10) NULL,
	[ospcr3] [varchar](10) NULL,
	[odbalance] [varchar](3) NULL,
	[osbalance] [varchar](3) NULL,
	[recalls] [tinyint] NULL,
	[locationid] [int] NULL,
	[odprismud] [decimal](5, 2) NULL,
	[osprismud] [decimal](5, 2) NULL,
	[odbaseudtext] [varchar](10) NULL,
	[osbaseudtext] [varchar](10) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[prescription_type] [varchar](50) NULL,
	[os_sphere] [varchar](20) NULL,
	[od_sphere] [varchar](20) NULL,
	[os_cylinder] [varchar](20) NULL,
	[od_cylinder] [varchar](20) NULL,
	[medication_group] [varchar](100) NULL,
	[medication] [varchar](100) NULL,
	[quantity] [varchar](100) NULL,
	[frequency] [varchar](100) NULL,
	[dosage] [varchar](100) NULL,
	[application] [varchar](100) NULL,
	[dispense_as_written_flag] [varchar](3) NULL,
	[released_flag] [varchar](3) NULL,
	[release_date] [datetime] NULL,
	[label_flag] [varchar](3) NULL,
	[locked_flag] [varchar](3) NULL,
	[lock_date] [datetime] NULL,
	[exam_id] [int] NULL,
	[discontinue_dt] [smalldatetime] NULL,
	[system_category] [varchar](50) NULL,
	[diagnosis_code_4] [varchar](50) NULL,
	[diagnosis_code_3] [varchar](50) NULL,
	[diagnosis_code_2] [varchar](50) NULL,
	[diagnosis_code_1] [varchar](50) NULL,
	[pharmacy_name] [varchar](500) NULL,
	[pharmacy_id] [int] NULL,
	[pharmacy_sent_dt] [smalldatetime] NULL,
	[pharmacy_sent_method] [varchar](50) NULL,
	[duration_4] [varchar](100) NULL,
	[duration_3] [varchar](100) NULL,
	[duration_2] [varchar](100) NULL,
	[duration_1] [varchar](100) NULL,
	[duration_type_3] [varchar](50) NULL,
	[duration_type_2] [varchar](50) NULL,
	[duration_type_4] [varchar](50) NULL,
	[duration_type_1] [varchar](50) NULL,
	[frequency_4] [varchar](100) NULL,
	[frequency_3] [varchar](100) NULL,
	[frequency_2] [varchar](100) NULL,
	[drops] [smallint] NULL,
	[specid_od] [int] NULL,
	[specid_os] [int] NULL,
	[contactid_od] [int] NULL,
	[contactid_os] [int] NULL,
	[generic] [varchar](100) NULL,
	[indications] [varchar](100) NULL,
	[group_name] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[rx_usage] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_11] [varchar](50) NULL,
	[custom_field_12] [varchar](50) NULL,
	[custom_field_13] [varchar](50) NULL,
	[custom_field_14] [varchar](50) NULL,
	[custom_field_15] [varchar](50) NULL,
	[custom_field_16] [varchar](50) NULL,
	[custom_field_17] [varchar](50) NULL,
	[custom_field_18] [varchar](50) NULL,
	[custom_field_19] [varchar](50) NULL,
	[custom_field_20] [varchar](50) NULL,
	[custom_field_21] [varchar](50) NULL,
	[custom_field_22] [varchar](50) NULL,
	[custom_field_23] [varchar](50) NULL,
	[custom_field_24] [varchar](50) NULL,
	[custom_field_25] [varchar](50) NULL,
	[custom_field_26] [varchar](50) NULL,
	[custom_field_27] [varchar](50) NULL,
	[custom_field_28] [varchar](50) NULL,
	[custom_field_29] [varchar](50) NULL,
	[custom_field_30] [varchar](50) NULL,
	[dominant] [varchar](50) NULL,
 CONSTRAINT [PK_Prescriptions] PRIMARY KEY CLUSTERED 
(
	[prescriptionid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[spectaclelens_pricing_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[spectaclelens_pricing_detail](
	[spectaclelens_pricing_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[spectaclelens_pricing_id] [int] NOT NULL,
	[pricing_type] [varchar](50) NOT NULL,
	[sort_order] [int] NULL,
	[range_start] [decimal](5, 3) NULL,
	[range_end] [decimal](5, 3) NULL,
	[cylinder_range_start] [decimal](5, 3) NULL,
	[cylinder_range_end] [decimal](5, 3) NULL,
	[material_id] [int] NULL,
	[lens_style_id] [int] NULL,
	[price] [money] NULL,
	[material] [varchar](100) NULL,
	[lens_style] [varchar](100) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_spectaclelens_pricing_detail] PRIMARY KEY CLUSTERED 
(
	[spectaclelens_pricing_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_template_tabs]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_template_tabs](
	[exam_tab_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[summary] [varchar](50) NULL,
	[patient] [varchar](50) NULL,
	[cheif_compaint] [varchar](50) NULL,
	[ros] [varchar](50) NULL,
	[pfsh] [varchar](50) NULL,
	[entrance] [varchar](50) NULL,
	[refractive] [varchar](50) NULL,
	[binocular] [varchar](50) NULL,
	[external_tab] [varchar](50) NULL,
	[internal] [varchar](50) NULL,
	[contact_lens_rx] [varchar](50) NULL,
	[ancillary] [varchar](50) NULL,
	[plans_and_assessment] [varchar](50) NULL,
	[files] [varchar](50) NULL,
	[interactions] [varchar](50) NULL,
	[custom_fields] [varchar](50) NULL,
	[summary_order] [int] NULL,
	[patient_order] [int] NULL,
	[cheif_compaint_order] [int] NULL,
	[ros_order] [int] NULL,
	[pfsh_order] [int] NULL,
	[entrance_order] [int] NULL,
	[refractive_order] [int] NULL,
	[binocular_order] [int] NULL,
	[external_tab_order] [int] NULL,
	[internal_order] [int] NULL,
	[contact_lens_rx_order] [int] NULL,
	[ancillary_order] [int] NULL,
	[plans_and_assessment_order] [int] NULL,
	[files_order] [int] NULL,
	[interactions_order] [int] NULL,
	[custom_fields_order] [int] NULL,
	[reserved_field_20] [varchar](50) NULL,
	[reserved_field_19] [varchar](50) NULL,
	[reserved_field_18] [varchar](50) NULL,
	[reserved_field_17] [varchar](50) NULL,
	[reserved_field_16] [varchar](50) NULL,
	[reserved_field_15] [varchar](50) NULL,
	[reserved_field_14] [varchar](50) NULL,
	[reserved_field_13] [varchar](50) NULL,
	[reserved_field_12] [varchar](50) NULL,
	[reserved_field_11] [varchar](50) NULL,
	[reserved_field_10] [varchar](50) NULL,
	[reserved_field_9] [varchar](50) NULL,
	[reserved_field_8] [varchar](50) NULL,
	[reserved_field_7] [varchar](50) NULL,
	[reserved_field_6] [varchar](50) NULL,
	[reserved_field_5] [varchar](50) NULL,
	[reserved_field_4] [varchar](50) NULL,
	[reserved_field_3] [varchar](50) NULL,
	[reserved_field_1] [varchar](50) NULL,
	[reserved_field_2] [varchar](50) NULL,
 CONSTRAINT [PK_exam_template_tabs] PRIMARY KEY CLUSTERED 
(
	[exam_tab_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_lowvision_prescribed]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_lowvision_prescribed](
	[exam_lowvision_prescribed_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[patient_id] [int] NULL,
	[created_dt] [smalldatetime] NULL,
	[lowvision_prescribed_aids] [varchar](500) NULL,
	[lowvision_training_time] [int] NULL,
	[notes] [varchar](500) NULL,
	[user_name] [varchar](100) NULL,
	[custom_field_1] [varchar](100) NULL,
	[custom_field_2] [varchar](100) NULL,
	[custom_field_3] [varchar](100) NULL,
	[custom_field_4] [varchar](100) NULL,
	[custom_field_5] [varchar](100) NULL,
	[custom_field_6] [varchar](100) NULL,
	[custom_field_7] [varchar](100) NULL,
	[custom_field_8] [varchar](100) NULL,
	[custom_field_9] [varchar](100) NULL,
	[custom_field_10] [varchar](100) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_lowvision_prescribed] PRIMARY KEY CLUSTERED 
(
	[exam_lowvision_prescribed_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_lowvision_items]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_lowvision_items](
	[exam_lowvision_item_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[patient_id] [int] NULL,
	[date_created] [smalldatetime] NULL,
	[which_eye] [varchar](50) NULL,
	[power] [varchar](50) NULL,
	[device_name] [varchar](200) NULL,
	[otherid] [int] NULL,
	[vision] [varchar](50) NULL,
	[usage] [varchar](100) NULL,
	[transferred_to_treatment_plan_flag] [varchar](3) NULL,
	[notes] [varchar](500) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_11] [varchar](50) NULL,
	[custom_field_12] [varchar](50) NULL,
	[custom_field_13] [varchar](50) NULL,
	[custom_field_14] [varchar](50) NULL,
	[custom_field_15] [varchar](50) NULL,
	[custom_field_16] [varchar](50) NULL,
	[custom_field_17] [varchar](50) NULL,
	[custom_field_18] [varchar](50) NULL,
	[custom_field_19] [varchar](50) NULL,
	[custom_field_20] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_lowvision_items] PRIMARY KEY CLUSTERED 
(
	[exam_lowvision_item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_items]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_items](
	[exam_item_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[otherid] [int] NOT NULL,
	[exam_item_dt] [datetime] NULL,
	[diagnosis_code] [varchar](50) NULL,
	[days_units] [smallint] NULL,
	[modifier] [varchar](50) NULL,
	[retailprice] [money] NULL,
	[procedure_code] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[insurance_id] [int] NULL,
	[insurance_plan_id] [int] NULL,
 CONSTRAINT [PK_exam_items] PRIMARY KEY CLUSTERED 
(
	[exam_item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_iop]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_iop](
	[exam_iop_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[patient_id] [int] NOT NULL,
	[od_reading] [varchar](50) NULL,
	[os_reading] [varchar](50) NULL,
	[test_name] [varchar](50) NULL,
	[date_taken] [smalldatetime] NULL,
	[time_taken] [smalldatetime] NULL,
	[category] [varchar](500) NULL,
	[notes] [varchar](1000) NULL,
	[sort_order] [smallint] NULL,
	[corrected_od] [varchar](50) NULL,
	[corrected_os] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_iop] PRIMARY KEY CLUSTERED 
(
	[exam_iop_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_detail]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_detail](
	[exam_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[exam_category] [varchar](100) NOT NULL,
	[sort_order] [int] NULL,
	[category] [varchar](100) NULL,
	[description] [varchar](1000) NULL,
	[diagnosed_dt] [datetime] NULL,
	[notes] [varchar](1000) NULL,
	[category_status] [varchar](100) NULL,
	[eye] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_detail] PRIMARY KEY CLUSTERED 
(
	[exam_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_customization]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_customization](
	[exam_customization_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[exam_field_mapping_id] [int] NOT NULL,
	[required_status] [varchar](50) NULL,
	[display_flag] [varchar](3) NULL,
 CONSTRAINT [PK_exam_customization] PRIMARY KEY CLUSTERED 
(
	[exam_customization_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_contactrx]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_contactrx](
	[exam_contactrx_id] [int] IDENTITY(1,1) NOT NULL,
	[physician_id] [int] NULL,
	[exam_id] [int] NOT NULL,
	[patient_id] [int] NOT NULL,
	[prescription_id] [int] NULL,
	[copy_to_patient] [varchar](3) NULL,
	[copy_dt] [smalldatetime] NULL,
	[rx_type] [varchar](50) NULL,
	[expiry_dt] [smalldatetime] NULL,
	[created_dt] [smalldatetime] NOT NULL,
	[usage] [varchar](50) NULL,
	[equipment_source] [varchar](50) NULL,
	[drawing] [image] NULL,
	[drawing_library_id] [int] NULL,
	[today_wear_time] [varchar](50) NULL,
	[wear_time_average] [varchar](50) NULL,
	[wear_time_type] [varchar](50) NULL,
	[wearing_plan] [varchar](50) NULL,
	[solution_system] [varchar](50) NULL,
	[notes] [varchar](500) NULL,
	[sphere_od] [varchar](50) NULL,
	[cyl_od] [varchar](50) NULL,
	[axis_od] [varchar](50) NULL,
	[add_od] [varchar](50) NULL,
	[prism_od] [varchar](50) NULL,
	[va_od] [varchar](50) NULL,
	[basecurve_od] [varchar](50) NULL,
	[far_pd_od] [varchar](50) NULL,
	[near_pd_od] [varchar](50) NULL,
	[vd_od] [varchar](50) NULL,
	[dva_ou] [varchar](50) NULL,
	[nva_ou] [varchar](50) NULL,
	[monovision_ou] [varchar](50) NULL,
	[sphere_os] [varchar](50) NULL,
	[cyl_os] [varchar](50) NULL,
	[axis_os] [varchar](50) NULL,
	[add_os] [varchar](50) NULL,
	[prism_os] [varchar](50) NULL,
	[va_os] [varchar](50) NULL,
	[basecurve_os] [varchar](50) NULL,
	[vd_os] [varchar](50) NULL,
	[far_pd_os] [varchar](50) NULL,
	[near_pd_os] [varchar](50) NULL,
	[upc_od] [varchar](50) NULL,
	[upc_os] [varchar](50) NULL,
	[manufacturerid_od] [int] NULL,
	[lensnameid_od] [int] NULL,
	[colorid_od] [int] NULL,
	[materialid_od] [int] NULL,
	[lens_qty_id_od] [int] NULL,
	[ct_od] [varchar](50) NULL,
	[ozd_od] [varchar](50) NULL,
	[r2_od] [varchar](50) NULL,
	[r2w_od] [varchar](50) NULL,
	[pc_od] [varchar](50) NULL,
	[pcw_od] [varchar](50) NULL,
	[dva_od] [varchar](50) NULL,
	[nva_od] [varchar](50) NULL,
	[centration_od] [varchar](50) NULL,
	[movement_od] [varchar](50) NULL,
	[fl_pattern_od] [varchar](50) NULL,
	[surface_od] [varchar](50) NULL,
	[rotation_od] [varchar](50) NULL,
	[rotation_type_od] [varchar](50) NULL,
	[or_sphcyl_sph_od] [varchar](50) NULL,
	[or_sphcyl_cyl_od] [varchar](50) NULL,
	[or_sphcyl_axis_od] [varchar](50) NULL,
	[or_sphcyl_va_od] [varchar](50) NULL,
	[or_sph_sph_od] [varchar](50) NULL,
	[or_sph_va_od] [varchar](50) NULL,
	[dia_od] [varchar](50) NULL,
	[manufacturerid_os] [int] NULL,
	[lensnameid_os] [int] NULL,
	[colorid_os] [int] NULL,
	[materialid_os] [int] NULL,
	[lens_qty_id_os] [int] NULL,
	[ct_os] [varchar](50) NULL,
	[ozd_os] [varchar](50) NULL,
	[r2_os] [varchar](50) NULL,
	[r2w_os] [varchar](50) NULL,
	[pc_os] [varchar](50) NULL,
	[pcw_os] [varchar](50) NULL,
	[dva_os] [varchar](50) NULL,
	[nva_os] [varchar](50) NULL,
	[centration_os] [varchar](50) NULL,
	[movement_os] [varchar](50) NULL,
	[fl_pattern_os] [varchar](50) NULL,
	[surface_os] [varchar](50) NULL,
	[rotation_os] [varchar](50) NULL,
	[rotation_type_os] [varchar](50) NULL,
	[or_sphcyl_sph_os] [varchar](50) NULL,
	[or_sphcyl_cyl_os] [varchar](50) NULL,
	[or_sphcyl_axis_os] [varchar](50) NULL,
	[or_sphcyl_va_os] [varchar](50) NULL,
	[or_sph_sph_os] [varchar](50) NULL,
	[or_sph_va_os] [varchar](50) NULL,
	[dia_os] [varchar](50) NULL,
	[dot_od] [varchar](50) NULL,
	[dot_os] [varchar](50) NULL,
	[blend_od] [int] NULL,
	[blend_os] [int] NULL,
	[seght_od] [varchar](50) NULL,
	[bc2_od] [varchar](50) NULL,
	[edge_od] [varchar](50) NULL,
	[warranty_od] [varchar](50) NULL,
	[seght_os] [varchar](50) NULL,
	[bc2_os] [varchar](50) NULL,
	[edge_os] [varchar](50) NULL,
	[warranty_os] [varchar](50) NULL,
	[reserved_field_20] [varchar](50) NULL,
	[reserved_field_19] [varchar](50) NULL,
	[reserved_field_18] [varchar](50) NULL,
	[reserved_field_17] [varchar](50) NULL,
	[reserved_field_16] [varchar](50) NULL,
	[reserved_field_15] [varchar](50) NULL,
	[reserved_field_14] [varchar](50) NULL,
	[reserved_field_13] [varchar](50) NULL,
	[reserved_field_12] [varchar](50) NULL,
	[reserved_field_11] [varchar](50) NULL,
	[reserved_field_10] [varchar](50) NULL,
	[reserved_field_9] [varchar](50) NULL,
	[reserved_field_8] [varchar](50) NULL,
	[reserved_field_7] [varchar](50) NULL,
	[reserved_field_6] [varchar](50) NULL,
	[reserved_field_5] [varchar](50) NULL,
	[reserved_field_4] [varchar](50) NULL,
	[reserved_field_3] [varchar](50) NULL,
	[reserved_field_1] [varchar](50) NULL,
	[reserved_field_2] [varchar](50) NULL,
	[custom_field_20] [varchar](50) NULL,
	[custom_field_19] [varchar](50) NULL,
	[custom_field_18] [varchar](50) NULL,
	[custom_field_17] [varchar](50) NULL,
	[custom_field_16] [varchar](50) NULL,
	[custom_field_15] [varchar](50) NULL,
	[custom_field_14] [varchar](50) NULL,
	[custom_field_13] [varchar](50) NULL,
	[custom_field_12] [varchar](50) NULL,
	[custom_field_11] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[contactid_od] [int] NULL,
	[contactid_os] [int] NULL,
	[prescription_type] [varchar](50) NULL,
	[spectacle_lens_style_id] [int] NULL,
	[prism_base_od] [varchar](50) NULL,
	[prism_base_os] [varchar](50) NULL,
	[prism_od_2] [varchar](50) NULL,
	[prism_os_2] [varchar](50) NULL,
	[prism_base_od_2] [varchar](50) NULL,
	[prism_base_os_2] [varchar](50) NULL,
	[good_comfort_od] [varchar](50) NULL,
	[good_comfort_os] [varchar](50) NULL,
	[good_vision_od] [varchar](50) NULL,
	[good_vision_os] [varchar](50) NULL,
	[good_coverage_od] [varchar](50) NULL,
	[good_coverage_os] [varchar](50) NULL,
	[good_movement_od] [varchar](50) NULL,
	[good_movement_os] [varchar](50) NULL,
	[good_centration_od] [varchar](50) NULL,
	[good_centration_os] [varchar](50) NULL,
	[personal_notes] [varchar](500) NULL,
	[replenishment_schedule] [varchar](50) NULL,
	[far_pd_ou] [varchar](50) NULL,
	[near_pd_ou] [varchar](50) NULL,
 CONSTRAINT [PK_EXAM_CONTACTRX] PRIMARY KEY NONCLUSTERED 
(
	[exam_contactrx_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_KEY_1_EXAM_CON] UNIQUE CLUSTERED 
(
	[exam_contactrx_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_chief_complaint]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_chief_complaint](
	[exam_chief_complaint_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[sort_order] [smallint] NULL,
	[chief_complaint] [varchar](5000) NULL,
	[chief_complaint_patient] [varchar](500) NULL,
	[location] [varchar](500) NULL,
	[quality] [varchar](500) NULL,
	[severity] [varchar](500) NULL,
	[duration] [varchar](500) NULL,
	[timing] [varchar](500) NULL,
	[context] [varchar](500) NULL,
	[modifying_factors] [varchar](500) NULL,
	[symptoms] [varchar](500) NULL,
	[chief_complaint_notes] [varchar](5000) NULL,
	[custom_field_20] [varchar](50) NULL,
	[custom_field_19] [varchar](50) NULL,
	[custom_field_18] [varchar](50) NULL,
	[custom_field_17] [varchar](50) NULL,
	[custom_field_16] [varchar](50) NULL,
	[custom_field_15] [varchar](50) NULL,
	[custom_field_14] [varchar](50) NULL,
	[custom_field_13] [varchar](50) NULL,
	[custom_field_12] [varchar](50) NULL,
	[custom_field_11] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_chief_complaint] PRIMARY KEY CLUSTERED 
(
	[exam_chief_complaint_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[exam_addendum]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[exam_addendum](
	[exam_addendum_id] [int] IDENTITY(1,1) NOT NULL,
	[exam_id] [int] NOT NULL,
	[addendum_date] [smalldatetime] NOT NULL,
	[user_name] [varchar](100) NULL,
	[notes] [varchar](5000) NULL,
	[custom_field_1] [varchar](100) NULL,
	[custom_field_2] [varchar](100) NULL,
	[custom_field_3] [varchar](100) NULL,
	[custom_field_4] [varchar](100) NULL,
	[custom_field_5] [varchar](100) NULL,
	[custom_field_6] [varchar](100) NULL,
	[custom_field_7] [varchar](100) NULL,
	[custom_field_8] [varchar](100) NULL,
	[custom_field_9] [varchar](100) NULL,
	[custom_field_10] [varchar](100) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
 CONSTRAINT [PK_exam_addendum] PRIMARY KEY CLUSTERED 
(
	[exam_addendum_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[employee]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[employee](
	[employeeid] [int] NOT NULL,
	[firstname] [varchar](30) NOT NULL,
	[lastname] [varchar](30) NOT NULL,
	[ssn] [varchar](20) NULL,
	[addressline1] [varchar](30) NULL,
	[addressline2] [varchar](30) NULL,
	[city] [varchar](50) NULL,
	[stateorprovince] [varchar](20) NULL,
	[postalcode] [varchar](10) NULL,
	[title] [varchar](50) NULL,
	[workphone] [varchar](20) NULL,
	[homephone] [varchar](20) NULL,
	[emailname] [varchar](45) NULL,
	[birthdate] [datetime] NULL,
	[notes] [varchar](5000) NULL,
	[maritalstatus] [varchar](20) NULL,
	[userid] [varchar](20) NULL,
	[password] [varchar](20) NULL,
	[active] [varchar](3) NOT NULL,
	[position] [int] NULL,
	[priority] [smallint] NULL,
	[user_type] [smallint] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[structure_id] [int] NULL,
	[structure_type] [varchar](1) NULL,
	[gross_percentage] [money] NULL,
	[margin_percentage_before] [money] NULL,
	[margin_percentage_after] [money] NULL,
	[margin_percentage_amount] [money] NULL,
	[wageperhour] [money] NULL,
	[employeeid_string] [varchar](6) NULL,
	[price_adjust] [varchar](3) NULL,
	[locationid] [int] NULL,
	[singlelocation] [varchar](3) NULL,
	[security_group_id] [int] NULL,
	[admin_flag] [char](1) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[listid] [varchar](36) NULL,
	[transfer_flag] [int] NOT NULL,
	[newcrop_usertype] [varchar](100) NULL,
	[newcrop_role] [varchar](100) NULL,
	[physicianid] [int] NULL,
	[providerid] [int] NULL,
	[session_domain] [varchar](50) NULL,
	[session_username] [varchar](50) NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[employeeid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[documents]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[documents](
	[documentid] [int] IDENTITY(1,1) NOT NULL,
	[domainid] [int] NOT NULL,
	[documentname] [varchar](300) NULL,
	[documenttype] [varchar](50) NULL,
	[documentimage] [image] NULL,
	[documentpath] [varchar](300) NULL,
	[documentfilename] [varchar](80) NULL,
	[dateentered] [datetime] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[zoom] [varchar](50) NULL,
	[notes] [varchar](500) NULL,
	[systemdefined] [varchar](50) NULL,
	[reserved_field_20] [varchar](50) NULL,
	[reserved_field_19] [varchar](50) NULL,
	[reserved_field_18] [varchar](50) NULL,
	[reserved_field_17] [varchar](50) NULL,
	[reserved_field_16] [varchar](50) NULL,
	[reserved_field_15] [varchar](50) NULL,
	[reserved_field_14] [varchar](50) NULL,
	[reserved_field_13] [varchar](50) NULL,
	[reserved_field_12] [varchar](50) NULL,
	[reserved_field_11] [varchar](50) NULL,
	[reserved_field_10] [varchar](50) NULL,
	[reserved_field_9] [varchar](50) NULL,
	[reserved_field_8] [varchar](50) NULL,
	[reserved_field_7] [varchar](50) NULL,
	[reserved_field_6] [varchar](50) NULL,
	[reserved_field_5] [varchar](50) NULL,
	[reserved_field_4] [varchar](50) NULL,
	[reserved_field_3] [varchar](50) NULL,
	[reserved_field_1] [varchar](50) NULL,
	[reserved_field_2] [varchar](50) NULL,
 CONSTRAINT [PK_documents] PRIMARY KEY CLUSTERED 
(
	[documentid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[contactinventory]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[contactinventory](
	[contactid] [int] NOT NULL,
	[detailed_domain_id] [int] NULL,
	[manufacturerid] [int] NOT NULL,
	[lensnameid] [int] NULL,
	[colorid] [int] NULL,
	[spherepower] [decimal](5, 2) NULL,
	[bc1] [decimal](5, 2) NULL,
	[bc2] [decimal](5, 2) NULL,
	[di1] [decimal](5, 2) NULL,
	[di2] [decimal](5, 2) NULL,
	[axis] [decimal](5, 2) NULL,
	[addd] [decimal](5, 2) NULL,
	[cylinder] [decimal](5, 2) NULL,
	[quantitysold] [int] NULL,
	[quantitypurchased] [int] NULL,
	[retailprice] [money] NULL,
	[cost] [money] NULL,
	[taxable] [varchar](3) NULL,
	[notes] [varchar](5000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[upccode] [varchar](12) NULL,
	[structure_id] [int] NULL,
	[structure_type] [varchar](1) NULL,
	[gross_percentage] [money] NULL,
	[amount] [money] NULL,
	[spiff] [money] NULL,
	[deleted] [varchar](3) NULL,
	[inventory] [varchar](3) NULL,
	[materialid] [int] NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lens_qty_id] [varchar](50) NULL,
 CONSTRAINT [PK_ContactInventory] PRIMARY KEY CLUSTERED 
(
	[contactid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[image]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[image](
	[imageid] [int] NOT NULL,
	[patientid] [int] NOT NULL,
	[imagetype] [char](1) NULL,
	[description] [varchar](250) NULL,
	[link] [varchar](250) NULL,
	[dateentered] [datetime] NULL,
	[imagename] [varchar](250) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[image] [image] NULL,
	[imagetypeid] [int] NULL,
	[lockimage] [varchar](3) NULL,
	[imagetypenew] [varchar](50) NULL,
	[lockdate] [datetime] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[zoom] [varchar](50) NULL,
 CONSTRAINT [PK_image] PRIMARY KEY CLUSTERED 
(
	[imageid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[hcfa_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[hcfa_detail](
	[hcfadetailid] [int] NOT NULL,
	[hcfaid] [int] NOT NULL,
	[servicefrom] [datetime] NULL,
	[serviceto] [datetime] NULL,
	[placeofservice] [varchar](10) NULL,
	[typeofservice] [varchar](3) NULL,
	[cpt_hcpcs] [varchar](10) NULL,
	[modifier] [varchar](10) NULL,
	[diagnosiscode] [varchar](10) NULL,
	[charges] [money] NULL,
	[days_units] [int] NULL,
	[epsdt] [varchar](3) NULL,
	[emg] [varchar](3) NULL,
	[cob] [varchar](3) NULL,
	[reservedforlocaluse] [varchar](30) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[description] [varchar](50) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[qualifier_type] [varchar](50) NULL,
	[qualifier] [varchar](200) NULL,
	[id_type] [varchar](50) NULL,
	[id_number] [varchar](50) NULL,
	[npi] [varchar](50) NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
 CONSTRAINT [PK_HCFA_Detail] PRIMARY KEY CLUSTERED 
(
	[hcfadetailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[frame_lens_style]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[frame_lens_style](
	[frame_lens_style_id] [int] IDENTITY(1,1) NOT NULL,
	[frameid] [int] NULL,
	[lens_style_id] [int] NULL,
 CONSTRAINT [PK_frame_lens_style] PRIMARY KEY CLUSTERED 
(
	[frame_lens_style_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[FLXR_AFF_USER]    Script Date: 12/16/2011 00:48:45 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FLXR_AFF_USER](
	[id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[dataobject] [varchar](50) NOT NULL,
	[id_user] [numeric](9, 0) NOT NULL,
	[id_customization] [numeric](9, 0) NULL,
 CONSTRAINT [PK_FLXR_AFF_USER] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_FLXR_AFF_USER] UNIQUE NONCLUSTERED 
(
	[dataobject] ASC,
	[id_user] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[insurance_schedule]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[insurance_schedule](
	[ins_schedule_id] [int] IDENTITY(1,1) NOT NULL,
	[insurance_id] [int] NOT NULL,
	[insurance_plan_id] [int] NOT NULL,
	[item_type] [char](1) NOT NULL,
	[item_id] [int] NOT NULL,
	[pat_pays] [smallmoney] NULL,
	[ins_pays] [smallmoney] NULL,
	[ins_discount] [smallmoney] NULL,
	[discount_type] [varchar](10) NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[vcode_num] [smallint] NULL,
	[insurance_allowance] [money] NULL,
	[discount_rate] [money] NULL,
	[copay] [money] NULL,
	[modifier] [varchar](50) NULL,
	[alternateCPT] [varchar](50) NULL,
	[not_on_claim] [smallint] NULL,
 CONSTRAINT [PK_insurance_schedule] PRIMARY KEY CLUSTERED 
(
	[ins_schedule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[medication]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[medication](
	[medicationid] [int] IDENTITY(1,1) NOT NULL,
	[domainid] [int] NULL,
	[medication] [varchar](200) NULL,
	[sort_order] [smallint] NULL,
	[lastupdateduser] [varchar](100) NULL,
	[lastupdateddt] [datetime] NULL,
	[lastversionnum] [smallint] NULL,
	[lastlocation] [varchar](150) NULL,
	[lastprogramname] [varchar](150) NULL,
	[system_category] [varchar](50) NULL,
	[application] [varchar](200) NULL,
	[medication_group] [varchar](100) NULL,
	[quantity] [varchar](100) NULL,
	[frequency] [varchar](100) NULL,
	[dosage] [varchar](100) NULL,
	[dispense_as_written_flag] [varchar](3) NULL,
	[label_flag] [varchar](3) NULL,
	[duration_4] [varchar](100) NULL,
	[duration_3] [varchar](100) NULL,
	[duration_2] [varchar](100) NULL,
	[duration_1] [varchar](100) NULL,
	[duration_type_3] [varchar](50) NULL,
	[duration_type_2] [varchar](50) NULL,
	[duration_type_4] [varchar](50) NULL,
	[duration_type_1] [varchar](50) NULL,
	[frequency_4] [varchar](100) NULL,
	[frequency_3] [varchar](100) NULL,
	[frequency_2] [varchar](100) NULL,
	[generic] [varchar](100) NULL,
	[indications] [varchar](100) NULL,
	[group_name] [varchar](50) NULL,
	[refills] [smallint] NULL,
	[custom_field_1] [varchar](50) NULL,
	[custom_field_2] [varchar](50) NULL,
	[custom_field_3] [varchar](50) NULL,
	[custom_field_4] [varchar](50) NULL,
	[custom_field_5] [varchar](50) NULL,
	[custom_field_6] [varchar](50) NULL,
	[custom_field_7] [varchar](50) NULL,
	[custom_field_8] [varchar](50) NULL,
	[custom_field_9] [varchar](50) NULL,
	[custom_field_10] [varchar](50) NULL,
 CONSTRAINT [PK_medication] PRIMARY KEY CLUSTERED 
(
	[medicationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orderitems]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orderitems](
	[orderitemsid] [int] IDENTITY(1,1) NOT NULL,
	[orderitemsdate] [datetime] NOT NULL,
	[orderid] [int] NOT NULL,
	[itemtype] [char](1) NULL,
	[itemid] [int] NULL,
	[description] [varchar](200) NULL,
	[retailprice] [money] NULL,
	[quantity] [int] NULL,
	[taxable] [varchar](3) NULL,
	[tax] [money] NULL,
	[tax2] [money] NULL,
	[notes] [varchar](255) NULL,
	[lenstreatmentind] [varchar](1) NULL,
	[orderitemslink] [varchar](10) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[commission_amt] [money] NULL,
	[commission_type] [varchar](1) NULL,
	[commission_spiff] [money] NULL,
	[employeeid] [int] NOT NULL,
	[fulldescription] [varchar](120) NULL,
	[supplierid] [int] NULL,
	[ownframe] [varchar](3) NULL,
	[itemcost] [money] NULL,
	[discount] [money] NULL,
	[discount_reasonid] [int] NULL,
	[insurancedue] [money] NULL,
	[insurancepaid] [money] NULL,
	[discount_rate] [money] NULL,
	[discount_type] [varchar](1) NULL,
	[insurance_rate] [money] NULL,
	[taxrate] [money] NULL,
	[taxrate2] [money] NULL,
	[taxid] [int] NULL,
	[taxid2] [int] NULL,
	[locationid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[print_receipt_flag] [varchar](3) NULL,
	[print_hcfa_flag] [varchar](3) NULL,
	[diagnosis_code] [varchar](10) NULL,
	[copay] [money] NULL,
	[trans_close_dt] [datetime] NULL,
	[insuranceid] [int] NULL,
	[insuranceplanid] [int] NULL,
	[original_retailprice] [money] NULL,
	[procedure_code] [varchar](20) NULL,
	[patient_pays] [money] NULL,
	[vcode_1] [int] NULL,
	[vcode_2] [int] NULL,
	[vcode_3] [int] NULL,
	[vcode_4] [int] NULL,
	[vcode_1_patient] [money] NULL,
	[vcode_2_patient] [money] NULL,
	[vcode_3_patient] [money] NULL,
	[vcode_4_patient] [money] NULL,
	[vcode_1_covered] [varchar](1) NULL,
	[vcode_2_covered] [varchar](1) NULL,
	[vcode_3_covered] [varchar](1) NULL,
	[vcode_4_covered] [varchar](1) NULL,
	[vcode_1_patient_orig] [money] NULL,
	[vcode_2_patient_orig] [money] NULL,
	[vcode_3_patient_orig] [money] NULL,
	[vcode_4_patient_orig] [money] NULL,
	[transfer_flag] [int] NULL,
	[days_units] [smallint] NULL,
	[modifier] [varchar](50) NULL,
	[exam_item_id] [int] NULL,
	[insurance_supplied] [varchar](1) NULL,
	[write_off_amount] [money] NULL,
	[overage_to_v20205] [varchar](1) NULL,
	[combine_lens_codes] [varchar](1) NULL,
 CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED 
(
	[orderitemsid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[order_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[order_detail](
	[order_detailid] [int] IDENTITY(1,1) NOT NULL,
	[orderid] [int] NOT NULL,
	[frameid] [int] NULL,
	[framenumber] [varchar](20) NULL,
	[ownframe] [varchar](3) NULL,
	[manufacturerid] [int] NULL,
	[framename] [varchar](50) NULL,
	[frametypeid] [int] NULL,
	[shape] [varchar](30) NULL,
	[colorid] [int] NULL,
	[colornumber] [varchar](30) NULL,
	[a] [decimal](5, 2) NULL,
	[b] [decimal](5, 2) NULL,
	[ed] [decimal](5, 2) NULL,
	[dbl] [decimal](5, 2) NULL,
	[templestyleid] [int] NULL,
	[templelength] [decimal](5, 2) NULL,
	[safety] [varchar](3) NULL,
	[framenotes] [varchar](500) NULL,
	[odspecid] [int] NULL,
	[osspecid] [int] NULL,
	[speclensmaterialid] [int] NULL,
	[odpdfar] [decimal](5, 2) NULL,
	[odpdnear] [decimal](5, 2) NULL,
	[ospdfar] [decimal](5, 2) NULL,
	[ospdnear] [decimal](5, 2) NULL,
	[oupdfar] [decimal](5, 2) NULL,
	[oupdnear] [decimal](5, 2) NULL,
	[odsphere] [decimal](5, 2) NULL,
	[ossphere] [decimal](5, 2) NULL,
	[odcylinder] [decimal](5, 2) NULL,
	[oscylinder] [decimal](5, 2) NULL,
	[odaxis] [decimal](5, 2) NULL,
	[osaxis] [decimal](5, 2) NULL,
	[odprism] [decimal](5, 2) NULL,
	[osprism] [decimal](5, 2) NULL,
	[odbase] [decimal](5, 2) NULL,
	[osbase] [decimal](5, 2) NULL,
	[oddec] [decimal](5, 2) NULL,
	[osdec] [decimal](5, 2) NULL,
	[odadd] [decimal](5, 2) NULL,
	[osadd] [decimal](5, 2) NULL,
	[odseght] [decimal](5, 2) NULL,
	[osseght] [decimal](5, 2) NULL,
	[odspeclensstyle] [int] NULL,
	[osspeclensstyle] [int] NULL,
	[odinset] [decimal](5, 2) NULL,
	[osinset] [decimal](5, 2) NULL,
	[odtotaldec] [decimal](5, 2) NULL,
	[ostotaldec] [decimal](5, 2) NULL,
	[odbc] [decimal](5, 2) NULL,
	[osbc] [decimal](5, 2) NULL,
	[odocht] [decimal](5, 2) NULL,
	[osocht] [decimal](5, 2) NULL,
	[oscontactid] [int] NULL,
	[odcontactid] [int] NULL,
	[differentodos] [varchar](3) NULL,
	[clensodmfg] [int] NULL,
	[clensosmfg] [int] NULL,
	[clensodname] [int] NULL,
	[clensosname] [int] NULL,
	[clensodcolor] [int] NULL,
	[clensoscolor] [int] NULL,
	[clensodbasecrv1] [decimal](5, 2) NULL,
	[clensosbasecrv1] [decimal](5, 2) NULL,
	[clensodbasecrv2] [decimal](5, 2) NULL,
	[clensosbasecrv2] [decimal](5, 2) NULL,
	[clensoddiam1] [decimal](5, 2) NULL,
	[clensosdiam1] [decimal](5, 2) NULL,
	[clensoddiam2] [decimal](5, 2) NULL,
	[clensosdiam2] [decimal](5, 2) NULL,
	[odsegdi] [decimal](5, 2) NULL,
	[ossegdi] [decimal](5, 2) NULL,
	[odaspheric] [varchar](10) NULL,
	[osaspheric] [varchar](10) NULL,
	[odthickness] [decimal](5, 2) NULL,
	[osthickness] [decimal](5, 2) NULL,
	[odoz] [decimal](5, 2) NULL,
	[osoz] [decimal](5, 2) NULL,
	[oddot] [varchar](3) NULL,
	[osdot] [varchar](3) NULL,
	[odblendid] [int] NULL,
	[osblendid] [int] NULL,
	[notes] [varchar](1000) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[upc_frame] [varchar](20) NULL,
	[upc1] [varchar](20) NULL,
	[upc2] [varchar](20) NULL,
	[ossphere2] [decimal](5, 2) NULL,
	[odsphere2] [decimal](5, 2) NULL,
	[odpcr2] [varchar](10) NULL,
	[odpcw2] [varchar](10) NULL,
	[odpcr3] [varchar](10) NULL,
	[odpcw3] [varchar](10) NULL,
	[ospcr2] [varchar](10) NULL,
	[ospcw2] [varchar](10) NULL,
	[ospcw3] [varchar](10) NULL,
	[ospcr3] [varchar](10) NULL,
	[odbalance] [varchar](3) NULL,
	[osbalance] [varchar](3) NULL,
	[eyesize] [tinyint] NULL,
	[bridgesize] [tinyint] NULL,
	[clensodmaterialid] [int] NULL,
	[clensosmaterialid] [int] NULL,
	[framecollectionid] [int] NULL,
	[odprismud] [decimal](5, 2) NULL,
	[osprismud] [decimal](5, 2) NULL,
	[odbaseudtext] [varchar](10) NULL,
	[osbaseudtext] [varchar](10) NULL,
	[odspheretext] [varchar](20) NULL,
	[osspheretext] [varchar](20) NULL,
	[odcylindertext] [varchar](20) NULL,
	[oscylindertext] [varchar](20) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[styleid] [varchar](50) NULL,
	[colordescription] [varchar](50) NULL,
	[lenscolor] [varchar](50) NULL,
	[lenscolorcode] [smallmoney] NULL,
	[circumference] [decimal](7, 2) NULL,
	[manufacturername] [varchar](50) NULL,
	[brandname] [varchar](50) NULL,
	[collectionname] [varchar](50) NULL,
	[gendertype] [varchar](50) NULL,
	[agegroup] [varchar](50) NULL,
	[productgroupname] [varchar](50) NULL,
	[rimtype] [varchar](50) NULL,
	[material] [varchar](50) NULL,
	[country] [varchar](50) NULL,
	[sku] [varchar](50) NULL,
	[yearintroduced] [char](4) NULL,
	[frame_usage] [varchar](50) NULL,
	[user_defined] [varchar](100) NULL,
	[user_defined_2] [varchar](100) NULL,
	[bridge] [varchar](50) NULL,
	[dbm] [decimal](5, 2) NULL,
	[frame_source] [varchar](50) NULL,
	[frame_shape_change] [varchar](50) NULL,
	[frame_order_date] [smalldatetime] NULL,
	[frame_received_date] [smalldatetime] NULL,
	[frame_order_notes] [varchar](500) NULL,
	[frame_po_reference] [varchar](100) NULL,
	[frame_supplier_id] [int] NULL,
	[frame_ship_to_lab_id] [int] NULL,
	[frame_cost] [money] NULL,
	[lens_tint_color_id] [int] NULL,
	[lens_tint_type_id] [int] NULL,
	[lens_tint_type_sample_id] [int] NULL,
	[lens_tint_type_sample_range] [smallmoney] NULL,
	[lens_bevel_id] [int] NULL,
	[lens_edge_id] [int] NULL,
	[lens_thickness_id] [int] NULL,
	[odlensname] [varchar](100) NULL,
	[oslensname] [varchar](100) NULL,
	[frame_activestatus] [varchar](50) NULL,
	[c] [decimal](5, 1) NULL,
	[lens_replenish_orsli] [varchar](50) NULL,
	[lens_diameter_orind] [smallint] NULL,
	[which_eye_lenses] [varchar](50) NULL,
	[frame_trace_file] [varchar](500) NULL,
	[custom_field_1] [varchar](500) NULL,
	[custom_field_2] [varchar](500) NULL,
	[custom_field_3] [varchar](500) NULL,
	[custom_field_4] [varchar](500) NULL,
	[custom_field_5] [varchar](500) NULL,
	[wrap] [varchar](50) NULL,
	[tilt] [varchar](50) NULL,
	[vertex] [varchar](50) NULL,
	[lens_shape_collection] [varchar](100) NULL,
	[lens_shape_framename] [varchar](100) NULL,
	[lens_shape_eyesize] [tinyint] NULL,
	[lens_shape_frameid] [int] NULL,
	[transpose_flag] [varchar](3) NULL,
	[reading_flag] [varchar](3) NULL,
	[group_cost] [money] NULL,
 CONSTRAINT [PK_ORDER_DETAIL] PRIMARY KEY CLUSTERED 
(
	[order_detailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[timeclock]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[timeclock](
	[timeclockid] [int] NOT NULL,
	[employeeid] [int] NOT NULL,
	[timedate] [datetime] NULL,
	[timein] [datetime] NULL,
	[timeout] [datetime] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[lunchout] [datetime] NULL,
	[lunchin] [datetime] NULL,
	[notes] [varchar](300) NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[dinner_in] [datetime] NULL,
	[dinner_out] [datetime] NULL,
	[lunch_in_2] [datetime] NULL,
	[lunch_out_2] [datetime] NULL,
	[txnid] [varchar](50) NULL,
	[transfer_flag] [int] NULL,
	[locationid] [int] NULL,
	[remarks] [varchar](100) NULL,
 CONSTRAINT [PK_timeclock] PRIMARY KEY CLUSTERED 
(
	[timeclockid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_timeclock_empltime] UNIQUE NONCLUSTERED 
(
	[employeeid] ASC,
	[timedate] ASC,
	[locationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[report_group_email_list]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[report_group_email_list](
	[group_email_id] [int] IDENTITY(1,1) NOT NULL,
	[groupid] [int] NULL,
	[employeeid] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[group_email_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[payments]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[payments](
	[paymentid] [int] IDENTITY(1,1) NOT NULL,
	[orderid] [int] NOT NULL,
	[employeeid] [int] NOT NULL,
	[paymentdate] [datetime] NOT NULL,
	[paymentmethodid] [int] NULL,
	[creditcardnumber] [varchar](30) NULL,
	[creditcardexpdate] [datetime] NULL,
	[referencenum] [varchar](300) NULL,
	[paymentamount] [money] NULL,
	[lastupdateduser] [varchar](50) NULL,
	[lastupdateddt] [datetime] NULL,
	[paymenttype] [varchar](50) NOT NULL,
	[locationid] [int] NULL,
	[adjustmenttype] [varchar](50) NULL,
	[printonstatement] [char](1) NULL,
	[insuranceid] [int] NULL,
	[paymentdescription] [varchar](100) NULL,
	[paymentmethod] [varchar](100) NULL,
	[employee] [varchar](50) NULL,
	[balanceeffect] [varchar](30) NULL,
	[insuranceid_from] [int] NULL,
	[lastLocation] [varchar](150) NULL,
	[lastVersionNum] [smallint] NULL,
	[lastProgramName] [varchar](150) NULL,
	[transfer_flag] [int] NULL,
	[approvalcode] [varchar](50) NULL,
	[ccname] [varchar](100) NULL,
	[cctype] [varchar](100) NULL,
	[ccswiped] [varchar](50) NULL,
	[txnid] [varchar](36) NULL,
	[check_number] [varchar](100) NULL,
	[refund_reason] [varchar](100) NULL,
	[payment_batch_id] [int] NULL,
	[payment_location] [varchar](200) NULL,
	[user_defined_13] [varchar](100) NULL,
	[user_defined_12] [varchar](100) NULL,
	[user_defined_11] [varchar](100) NULL,
	[user_defined_10] [varchar](100) NULL,
	[user_defined_9] [varchar](100) NULL,
	[user_defined_8] [varchar](100) NULL,
	[user_defined_7] [varchar](100) NULL,
	[user_defined_6] [varchar](100) NULL,
	[user_defined_5] [varchar](100) NULL,
	[user_defined_4] [varchar](100) NULL,
	[user_defined_3] [varchar](100) NULL,
	[user_defined_2] [varchar](100) NULL,
	[user_defined_1] [varchar](100) NULL,
	[user_defined_20] [varchar](100) NULL,
	[user_defined_19] [varchar](100) NULL,
	[user_defined_18] [varchar](100) NULL,
	[user_defined_17] [varchar](100) NULL,
	[user_defined_16] [varchar](100) NULL,
	[user_defined_15] [varchar](100) NULL,
	[user_defined_14] [varchar](100) NULL,
	[cash_tendered] [money] NULL,
	[change_due] [money] NULL,
	[payments_header_id] [int] NULL,
	[computer_name] [varchar](500) NULL,
	[signature_blob] [int] NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[paymentid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[orderitems_detail]    Script Date: 12/16/2011 00:48:46 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[orderitems_detail](
	[orderitems_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[orderitemsid] [int] NULL,
	[procedure_code] [varchar](50) NULL,
	[retail_amount] [money] NULL,
	[patient_pays] [money] NULL,
	[insurance_pays] [money] NULL,
	[insurance_pays_secondary] [money] NULL,
	[copay] [money] NULL,
	[insurance_id] [int] NULL,
	[insurance_id_secondary] [int] NULL,
	[tax_1_amount] [money] NULL,
	[tax_2_amount] [money] NULL,
	[discount] [money] NULL,
	[discount_reason] [varchar](50) NULL,
	[cost] [money] NULL,
	[tax_1_rate] [money] NULL,
	[tax_2_rate] [money] NULL,
	[original_amount] [money] NULL,
	[code_type] [varchar](50) NULL,
	[print_receipt_flag] [varchar](3) NULL,
	[print_hcfa_flag] [varchar](3) NULL,
	[covered_flag] [varchar](3) NULL,
	[sort_order] [smallint] NULL,
	[orderid] [int] NULL,
	[series_code] [varchar](50) NULL,
	[itemid] [int] NULL,
	[orderitemslink] [varchar](50) NULL,
	[patient_pays_original] [money] NULL,
	[ins_discount] [money] NULL,
	[insurance_pays_original] [money] NULL,
	[discount_type] [varchar](1) NULL,
	[discount_rate] [money] NULL,
	[write_off_ins_cvg_pay_diff] [varchar](1) NULL,
	[insurance_coverage_gap] [money] NULL,
	[covered_service] [money] NULL,
	[procedure_code_original] [varchar](50) NULL,
	[overage_was_applied] [varchar](1) NULL,
	[created_from_overage] [varchar](1) NULL,
	[frame_group_cost] [money] NULL,
	[modifier] [varchar](50) NULL,
	[alternateCPT] [varchar](50) NULL,
	[not_eligible] [varchar](1) NULL,
	[description] [varchar](300) NULL,
 CONSTRAINT [PK_orderitems_detail] PRIMARY KEY CLUSTERED 
(
	[orderitems_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

/****** Object:  Default [DF_contactlensname_active_flag]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contactlensname] ADD  CONSTRAINT [DF_contactlensname_active_flag]  DEFAULT ('Yes') FOR [active_flag]

/****** Object:  Default [DF_contacts_sendmail]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contacts] ADD  CONSTRAINT [DF_contacts_sendmail]  DEFAULT ('Yes') FOR [sendmail]

/****** Object:  Default [DF_Contacts_PatientActive]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contacts] ADD  CONSTRAINT [DF_Contacts_PatientActive]  DEFAULT ('Yes') FOR [patientactive]

/****** Object:  Default [DF_detailed_domain_domain_type_ind]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[detailed_domain] ADD  CONSTRAINT [DF_detailed_domain_domain_type_ind]  DEFAULT ('PR') FOR [domain_type_ind]

/****** Object:  Default [DF_detailed_domain_active]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[detailed_domain] ADD  CONSTRAINT [DF_detailed_domain_active]  DEFAULT ('Yes') FOR [active]

/****** Object:  Default [DF_detailed_domain_lastupdateduser]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[detailed_domain] ADD  CONSTRAINT [DF_detailed_domain_lastupdateduser]  DEFAULT ((1)) FOR [lastupdateduser]

/****** Object:  Default [DF_detailed_domain_lastupdateddt]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[detailed_domain] ADD  CONSTRAINT [DF_detailed_domain_lastupdateddt]  DEFAULT (getdate()) FOR [lastupdateddt]

/****** Object:  Default [DF_employee_price_adjust]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[employee] ADD  CONSTRAINT [DF_employee_price_adjust]  DEFAULT ('Yes') FOR [price_adjust]

/****** Object:  Default [DF__employee__transf__7814D14C]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[employee] ADD  DEFAULT ((0)) FOR [transfer_flag]

/****** Object:  Default [DF_employeeMessages_warn]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[employeemessages] ADD  CONSTRAINT [DF_employeeMessages_warn]  DEFAULT ('No') FOR [warn]

/****** Object:  Default [DF__exam_equi__captu__79FD19BE]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_equipment] ADD  DEFAULT ((0)) FOR [capture_txt_flag]

/****** Object:  Default [DF__exam_equi__read___7AF13DF7]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_equipment] ADD  DEFAULT ((0)) FOR [read_file_flag]

/****** Object:  Default [DF_frame_lens_range_Seq]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[frame_lens_range] ADD  CONSTRAINT [DF_frame_lens_range_Seq]  DEFAULT ((0)) FOR [sort_order]

/****** Object:  Default [DF_hcfa_cv_12_generalstandard]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa] ADD  CONSTRAINT [DF_hcfa_cv_12_generalstandard]  DEFAULT ('No') FOR [cv_12_generalstandard]

/****** Object:  Default [DF_hcfa_cv_13_a]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa] ADD  CONSTRAINT [DF_hcfa_cv_13_a]  DEFAULT ('No') FOR [cv_13_a]

/****** Object:  Default [DF_hcfa_cv_13_b]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa] ADD  CONSTRAINT [DF_hcfa_cv_13_b]  DEFAULT ('No') FOR [cv_13_b]

/****** Object:  Default [DF_hcfa_cv_13_c]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa] ADD  CONSTRAINT [DF_hcfa_cv_13_c]  DEFAULT ('No') FOR [cv_13_c]

/****** Object:  Default [DF_hcfa_cv_20_feepaid]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa] ADD  CONSTRAINT [DF_hcfa_cv_20_feepaid]  DEFAULT ('No') FOR [cv_20_feepaid]

/****** Object:  Default [DF_inventorylocation_quantitysold]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[inventorylocation] ADD  CONSTRAINT [DF_inventorylocation_quantitysold]  DEFAULT ((0)) FOR [quantitysold]

/****** Object:  Default [DF_inventorylocation_quantitypurchased]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[inventorylocation] ADD  CONSTRAINT [DF_inventorylocation_quantitypurchased]  DEFAULT ((0)) FOR [quantitypurchased]

/****** Object:  Default [DF_orderitems_discount]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems] ADD  CONSTRAINT [DF_orderitems_discount]  DEFAULT ((0)) FOR [discount]

/****** Object:  Default [DF_orderitems_insurancedue]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems] ADD  CONSTRAINT [DF_orderitems_insurancedue]  DEFAULT ((0)) FOR [insurancedue]

/****** Object:  Default [DF_orderitems_insurancepaid]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems] ADD  CONSTRAINT [DF_orderitems_insurancepaid]  DEFAULT ((0)) FOR [insurancepaid]

/****** Object:  Default [DF__orderitem__disco__0662F0A3]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitemsHistory] ADD  DEFAULT ((0)) FOR [discount]

/****** Object:  Default [DF__orderitem__insur__075714DC]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitemsHistory] ADD  DEFAULT ((0)) FOR [insurancedue]

/****** Object:  Default [DF__orderitem__insur__084B3915]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitemsHistory] ADD  DEFAULT ((0)) FOR [insurancepaid]

/****** Object:  Default [DF_orders_orderactive]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orders] ADD  CONSTRAINT [DF_orders_orderactive]  DEFAULT ('Yes') FOR [orderactive]

/****** Object:  Default [DF_physician_active_flag]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[physician] ADD  CONSTRAINT [DF_physician_active_flag]  DEFAULT ('Yes') FOR [active_flag]

/****** Object:  Default [DF_provider_actuve_flag]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[provider] ADD  CONSTRAINT [DF_provider_actuve_flag]  DEFAULT ('Yes') FOR [actuve_flag]

/****** Object:  Default [DF_security_control_group_status]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[security_control_group] ADD  CONSTRAINT [DF_security_control_group_status]  DEFAULT ('E') FOR [status]

/****** Object:  Default [DF_security_function_grouping_application]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[security_function_grouping] ADD  CONSTRAINT [DF_security_function_grouping_application]  DEFAULT ('vision_express') FOR [application]

/****** Object:  Default [DF_security_function_grouping_window]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[security_function_grouping] ADD  CONSTRAINT [DF_security_function_grouping_window]  DEFAULT ('w_opto_frame') FOR [window]

/****** Object:  Default [DF_security_template_application]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[security_template] ADD  CONSTRAINT [DF_security_template_application]  DEFAULT ('vision_express') FOR [application]

/****** Object:  Default [DF_suppliers_frames]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[suppliers] ADD  CONSTRAINT [DF_suppliers_frames]  DEFAULT ('N') FOR [frames]

/****** Object:  Default [DF_suppliers_lenses]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[suppliers] ADD  CONSTRAINT [DF_suppliers_lenses]  DEFAULT ('N') FOR [lenses]

/****** Object:  Default [DF_suppliers_contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[suppliers] ADD  CONSTRAINT [DF_suppliers_contacts]  DEFAULT ('N') FOR [contacts]

/****** Object:  Default [DF_suppliers_other]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[suppliers] ADD  CONSTRAINT [DF_suppliers_other]  DEFAULT ('N') FOR [other]

/****** Object:  Default [DF_suppliers_lab]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[suppliers] ADD  CONSTRAINT [DF_suppliers_lab]  DEFAULT ('N') FOR [lab]

/****** Object:  Default [DF_systemsettings_systemdefined]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[systemsettings] ADD  CONSTRAINT [DF_systemsettings_systemdefined]  DEFAULT ('N') FOR [systemdefined]

/****** Object:  Default [DF_tax_taxrate]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[tax] ADD  CONSTRAINT [DF_tax_taxrate]  DEFAULT ((0)) FOR [taxrate]

/****** Object:  ForeignKey [FK_calendar_calendarmaster]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[calendar]  WITH NOCHECK ADD  CONSTRAINT [FK_calendar_calendarmaster] FOREIGN KEY([calendarmasterid])
REFERENCES [dbo].[calendarmaster] ([calendarmasterid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[calendar] NOCHECK CONSTRAINT [FK_calendar_calendarmaster]

/****** Object:  ForeignKey [FK_calendar_physician]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[calendar]  WITH CHECK ADD  CONSTRAINT [FK_calendar_physician] FOREIGN KEY([physicianid])
REFERENCES [dbo].[physician] ([physicianid])

ALTER TABLE [dbo].[calendar] CHECK CONSTRAINT [FK_calendar_physician]

/****** Object:  ForeignKey [FK_commission_products_commission_structure]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[commission_products]  WITH NOCHECK ADD  CONSTRAINT [FK_commission_products_commission_structure] FOREIGN KEY([structure_id])
REFERENCES [dbo].[commission_structure] ([structure_id])

ALTER TABLE [dbo].[commission_products] CHECK CONSTRAINT [FK_commission_products_commission_structure]

/****** Object:  ForeignKey [FK_ContactInventory_commission_structure]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contactinventory]  WITH CHECK ADD  CONSTRAINT [FK_ContactInventory_commission_structure] FOREIGN KEY([structure_id])
REFERENCES [dbo].[commission_structure] ([structure_id])

ALTER TABLE [dbo].[contactinventory] CHECK CONSTRAINT [FK_ContactInventory_commission_structure]

/****** Object:  ForeignKey [FK_contactinventory_contactlensname]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contactinventory]  WITH NOCHECK ADD  CONSTRAINT [FK_contactinventory_contactlensname] FOREIGN KEY([lensnameid])
REFERENCES [dbo].[contactlensname] ([nameid])

ALTER TABLE [dbo].[contactinventory] NOCHECK CONSTRAINT [FK_contactinventory_contactlensname]

/****** Object:  ForeignKey [FK_contactlensname_manufacturer]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[contactlensname]  WITH NOCHECK ADD  CONSTRAINT [FK_contactlensname_manufacturer] FOREIGN KEY([manufacturerid])
REFERENCES [dbo].[manufacturer] ([manufacturerid])

ALTER TABLE [dbo].[contactlensname] NOCHECK CONSTRAINT [FK_contactlensname_manufacturer]

/****** Object:  ForeignKey [FK_documents_domain]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[documents]  WITH NOCHECK ADD  CONSTRAINT [FK_documents_domain] FOREIGN KEY([domainid])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[documents] CHECK CONSTRAINT [FK_documents_domain]

/****** Object:  ForeignKey [FK_domain_domainType]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[domain]  WITH NOCHECK ADD  CONSTRAINT [FK_domain_domainType] FOREIGN KEY([domaintypeid])
REFERENCES [dbo].[domaintype] ([domaintypeid])
ON DELETE CASCADE

ALTER TABLE [dbo].[domain] CHECK CONSTRAINT [FK_domain_domainType]

/****** Object:  ForeignKey [FK_employee_domain]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[employee]  WITH NOCHECK ADD  CONSTRAINT [FK_employee_domain] FOREIGN KEY([security_group_id])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [FK_employee_domain]

/****** Object:  ForeignKey [FK_exam_letter]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam]  WITH CHECK ADD  CONSTRAINT [FK_exam_letter] FOREIGN KEY([letter_id])
REFERENCES [dbo].[letter] ([letterid])

ALTER TABLE [dbo].[exam] CHECK CONSTRAINT [FK_exam_letter]

/****** Object:  ForeignKey [FK_EXAM_REFERENCE_CONTACTS]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam]  WITH NOCHECK ADD  CONSTRAINT [FK_EXAM_REFERENCE_CONTACTS] FOREIGN KEY([patient_id])
REFERENCES [dbo].[contacts] ([contactid])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam] NOCHECK CONSTRAINT [FK_EXAM_REFERENCE_CONTACTS]

/****** Object:  ForeignKey [FK_exam_addendum_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_addendum]  WITH CHECK ADD  CONSTRAINT [FK_exam_addendum_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_addendum] CHECK CONSTRAINT [FK_exam_addendum_exam]

/****** Object:  ForeignKey [FK_exam_chief_complaint_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_chief_complaint]  WITH CHECK ADD  CONSTRAINT [FK_exam_chief_complaint_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_chief_complaint] CHECK CONSTRAINT [FK_exam_chief_complaint_exam]

/****** Object:  ForeignKey [FK_exam_contactrx_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_contactrx]  WITH CHECK ADD  CONSTRAINT [FK_exam_contactrx_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_contactrx] CHECK CONSTRAINT [FK_exam_contactrx_exam]

/****** Object:  ForeignKey [FK_exam_customization_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_customization]  WITH CHECK ADD  CONSTRAINT [FK_exam_customization_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_customization] CHECK CONSTRAINT [FK_exam_customization_exam]

/****** Object:  ForeignKey [FK_exam_detail_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_detail]  WITH NOCHECK ADD  CONSTRAINT [FK_exam_detail_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_detail] NOCHECK CONSTRAINT [FK_exam_detail_exam]

/****** Object:  ForeignKey [FK_exam_iop_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_iop]  WITH NOCHECK ADD  CONSTRAINT [FK_exam_iop_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_iop] NOCHECK CONSTRAINT [FK_exam_iop_exam]

/****** Object:  ForeignKey [FK_exam_items_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_items]  WITH NOCHECK ADD  CONSTRAINT [FK_exam_items_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])

ALTER TABLE [dbo].[exam_items] NOCHECK CONSTRAINT [FK_exam_items_exam]

/****** Object:  ForeignKey [FK_exam_items_insurance]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_items]  WITH CHECK ADD  CONSTRAINT [FK_exam_items_insurance] FOREIGN KEY([insurance_id])
REFERENCES [dbo].[insurance] ([id])
ON DELETE SET NULL

ALTER TABLE [dbo].[exam_items] CHECK CONSTRAINT [FK_exam_items_insurance]

/****** Object:  ForeignKey [FK_exam_items_insurance_plan]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_items]  WITH CHECK ADD  CONSTRAINT [FK_exam_items_insurance_plan] FOREIGN KEY([insurance_plan_id])
REFERENCES [dbo].[insurance_plan] ([insurance_plan_id])

ALTER TABLE [dbo].[exam_items] CHECK CONSTRAINT [FK_exam_items_insurance_plan]

/****** Object:  ForeignKey [FK_exam_items_otherinventory]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_items]  WITH NOCHECK ADD  CONSTRAINT [FK_exam_items_otherinventory] FOREIGN KEY([otherid])
REFERENCES [dbo].[otherinventory] ([otherid])

ALTER TABLE [dbo].[exam_items] NOCHECK CONSTRAINT [FK_exam_items_otherinventory]

/****** Object:  ForeignKey [FK_exam_low_vision_custom_question_exam_low_vision_custom_question]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_low_vision_custom_question]  WITH CHECK ADD  CONSTRAINT [FK_exam_low_vision_custom_question_exam_low_vision_custom_question] FOREIGN KEY([low_vision_custom_question_id])
REFERENCES [dbo].[exam_low_vision_custom_question] ([low_vision_custom_question_id])

ALTER TABLE [dbo].[exam_low_vision_custom_question] CHECK CONSTRAINT [FK_exam_low_vision_custom_question_exam_low_vision_custom_question]

/****** Object:  ForeignKey [FK_exam_lowvision_items_contacts]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_lowvision_items]  WITH CHECK ADD  CONSTRAINT [FK_exam_lowvision_items_contacts] FOREIGN KEY([patient_id])
REFERENCES [dbo].[contacts] ([contactid])

ALTER TABLE [dbo].[exam_lowvision_items] CHECK CONSTRAINT [FK_exam_lowvision_items_contacts]

/****** Object:  ForeignKey [FK_exam_lowvision_items_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_lowvision_items]  WITH CHECK ADD  CONSTRAINT [FK_exam_lowvision_items_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_lowvision_items] CHECK CONSTRAINT [FK_exam_lowvision_items_exam]

/****** Object:  ForeignKey [FK_exam_lowvision_prescribed_contacts]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_lowvision_prescribed]  WITH CHECK ADD  CONSTRAINT [FK_exam_lowvision_prescribed_contacts] FOREIGN KEY([patient_id])
REFERENCES [dbo].[contacts] ([contactid])

ALTER TABLE [dbo].[exam_lowvision_prescribed] CHECK CONSTRAINT [FK_exam_lowvision_prescribed_contacts]

/****** Object:  ForeignKey [FK_exam_lowvision_prescribed_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_lowvision_prescribed]  WITH CHECK ADD  CONSTRAINT [FK_exam_lowvision_prescribed_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_lowvision_prescribed] CHECK CONSTRAINT [FK_exam_lowvision_prescribed_exam]

/****** Object:  ForeignKey [FK_exam_template_tabs_exam]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[exam_template_tabs]  WITH CHECK ADD  CONSTRAINT [FK_exam_template_tabs_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[exam_template_tabs] CHECK CONSTRAINT [FK_exam_template_tabs_exam]

/****** Object:  ForeignKey [FK__facility___facil__30592A6F]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[facility_location]  WITH CHECK ADD FOREIGN KEY([facility_id])
REFERENCES [dbo].[facility] ([facilityid])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK__facility___locat__314D4EA8]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[facility_location]  WITH CHECK ADD FOREIGN KEY([location_id])
REFERENCES [dbo].[mycompanyinformation] ([setupid])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK__facility___facil__324172E1]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[facility_provider]  WITH CHECK ADD FOREIGN KEY([facility_id])
REFERENCES [dbo].[facility] ([facilityid])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK__facility___provi__3335971A]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[facility_provider]  WITH CHECK ADD FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([providerid])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK_FLXR_AFFGLOBAL_REF_CUSTOM]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[FLXR_AFF_GLOBAL]  WITH CHECK ADD  CONSTRAINT [FK_FLXR_AFFGLOBAL_REF_CUSTOM] FOREIGN KEY([id_customization])
REFERENCES [dbo].[FLXR_CUSTOMIZATION] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[FLXR_AFF_GLOBAL] CHECK CONSTRAINT [FK_FLXR_AFFGLOBAL_REF_CUSTOM]

/****** Object:  ForeignKey [FK_FLXR_AFFGROUP_REF_CUSTOM]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[FLXR_AFF_GROUP]  WITH CHECK ADD  CONSTRAINT [FK_FLXR_AFFGROUP_REF_CUSTOM] FOREIGN KEY([id_customization])
REFERENCES [dbo].[FLXR_CUSTOMIZATION] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[FLXR_AFF_GROUP] CHECK CONSTRAINT [FK_FLXR_AFFGROUP_REF_CUSTOM]

/****** Object:  ForeignKey [FK_FLXR_AFFGROUP_REF_GROUP]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[FLXR_AFF_GROUP]  WITH CHECK ADD  CONSTRAINT [FK_FLXR_AFFGROUP_REF_GROUP] FOREIGN KEY([id_group])
REFERENCES [dbo].[FLXR_GROUP] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[FLXR_AFF_GROUP] CHECK CONSTRAINT [FK_FLXR_AFFGROUP_REF_GROUP]

/****** Object:  ForeignKey [FK_AFFUSER_REF_CUSTOM]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[FLXR_AFF_USER]  WITH CHECK ADD  CONSTRAINT [FK_AFFUSER_REF_CUSTOM] FOREIGN KEY([id_customization])
REFERENCES [dbo].[FLXR_CUSTOMIZATION] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[FLXR_AFF_USER] CHECK CONSTRAINT [FK_AFFUSER_REF_CUSTOM]

/****** Object:  ForeignKey [FK_FLXR_AFFGROUP_REF_USER]    Script Date: 12/16/2011 00:48:45 ******/
ALTER TABLE [dbo].[FLXR_AFF_USER]  WITH CHECK ADD  CONSTRAINT [FK_FLXR_AFFGROUP_REF_USER] FOREIGN KEY([id_user])
REFERENCES [dbo].[FLXR_USER] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[FLXR_AFF_USER] CHECK CONSTRAINT [FK_FLXR_AFFGROUP_REF_USER]

/****** Object:  ForeignKey [FK_FLXR_USER_REF_GROUP]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[FLXR_USER]  WITH CHECK ADD  CONSTRAINT [FK_FLXR_USER_REF_GROUP] FOREIGN KEY([id_group])
REFERENCES [dbo].[FLXR_GROUP] ([id])

ALTER TABLE [dbo].[FLXR_USER] CHECK CONSTRAINT [FK_FLXR_USER_REF_GROUP]

/****** Object:  ForeignKey [FK_frame_lens_style_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[frame_lens_style]  WITH CHECK ADD  CONSTRAINT [FK_frame_lens_style_domain] FOREIGN KEY([lens_style_id])
REFERENCES [dbo].[domain] ([domainid])
ON DELETE CASCADE

ALTER TABLE [dbo].[frame_lens_style] CHECK CONSTRAINT [FK_frame_lens_style_domain]

/****** Object:  ForeignKey [FK_frame_lens_style_frameinventory]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[frame_lens_style]  WITH CHECK ADD  CONSTRAINT [FK_frame_lens_style_frameinventory] FOREIGN KEY([frameid])
REFERENCES [dbo].[frameinventory] ([frameid])
ON DELETE CASCADE

ALTER TABLE [dbo].[frame_lens_style] CHECK CONSTRAINT [FK_frame_lens_style_frameinventory]

/****** Object:  ForeignKey [FK_hcfa_provider]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa]  WITH NOCHECK ADD  CONSTRAINT [FK_hcfa_provider] FOREIGN KEY([providerid])
REFERENCES [dbo].[provider] ([providerid])

ALTER TABLE [dbo].[hcfa] CHECK CONSTRAINT [FK_hcfa_provider]

/****** Object:  ForeignKey [FK_HCFA_Detail_HCFA]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hcfa_detail]  WITH NOCHECK ADD  CONSTRAINT [FK_HCFA_Detail_HCFA] FOREIGN KEY([hcfaid])
REFERENCES [dbo].[hcfa] ([hcfaid])

ALTER TABLE [dbo].[hcfa_detail] CHECK CONSTRAINT [FK_HCFA_Detail_HCFA]

/****** Object:  ForeignKey [FK_hipaa_Contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[hipaa]  WITH NOCHECK ADD  CONSTRAINT [FK_hipaa_Contacts] FOREIGN KEY([patientid])
REFERENCES [dbo].[contacts] ([contactid])

ALTER TABLE [dbo].[hipaa] CHECK CONSTRAINT [FK_hipaa_Contacts]

/****** Object:  ForeignKey [FK_image_Contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[image]  WITH NOCHECK ADD  CONSTRAINT [FK_image_Contacts] FOREIGN KEY([patientid])
REFERENCES [dbo].[contacts] ([contactid])

ALTER TABLE [dbo].[image] CHECK CONSTRAINT [FK_image_Contacts]

/****** Object:  ForeignKey [FK_image_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[image]  WITH NOCHECK ADD  CONSTRAINT [FK_image_domain] FOREIGN KEY([imagetypeid])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[image] CHECK CONSTRAINT [FK_image_domain]

/****** Object:  ForeignKey [FK_insurance_plan_insurance1]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[insurance_plan]  WITH NOCHECK ADD  CONSTRAINT [FK_insurance_plan_insurance1] FOREIGN KEY([insurance_id])
REFERENCES [dbo].[insurance] ([id])
ON DELETE CASCADE
NOT FOR REPLICATION

ALTER TABLE [dbo].[insurance_plan] CHECK CONSTRAINT [FK_insurance_plan_insurance1]

/****** Object:  ForeignKey [FK_insurance_schedule_insurance_plan]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[insurance_schedule]  WITH NOCHECK ADD  CONSTRAINT [FK_insurance_schedule_insurance_plan] FOREIGN KEY([insurance_plan_id])
REFERENCES [dbo].[insurance_plan] ([insurance_plan_id])
ON DELETE CASCADE
NOT FOR REPLICATION

ALTER TABLE [dbo].[insurance_schedule] CHECK CONSTRAINT [FK_insurance_schedule_insurance_plan]

/****** Object:  ForeignKey [FK_letter_history_contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[letter_history]  WITH NOCHECK ADD  CONSTRAINT [FK_letter_history_contacts] FOREIGN KEY([patient_id])
REFERENCES [dbo].[contacts] ([contactid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[letter_history] CHECK CONSTRAINT [FK_letter_history_contacts]

/****** Object:  ForeignKey [FK_MarketingDates_Marketing]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[marketingdates]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketingDates_Marketing] FOREIGN KEY([marketingid])
REFERENCES [dbo].[marketing] ([marketingid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[marketingdates] NOCHECK CONSTRAINT [FK_MarketingDates_Marketing]

/****** Object:  ForeignKey [FK_MarketingDetail_Marketing]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[marketingdetail]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketingDetail_Marketing] FOREIGN KEY([marketingid])
REFERENCES [dbo].[marketing] ([marketingid])
ON DELETE CASCADE

ALTER TABLE [dbo].[marketingdetail] CHECK CONSTRAINT [FK_MarketingDetail_Marketing]

/****** Object:  ForeignKey [FK_medication_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[medication]  WITH NOCHECK ADD  CONSTRAINT [FK_medication_domain] FOREIGN KEY([domainid])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[medication] CHECK CONSTRAINT [FK_medication_domain]

/****** Object:  ForeignKey [FK_ORDER_DETAIL_ORDERs]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[order_detail]  WITH NOCHECK ADD  CONSTRAINT [FK_ORDER_DETAIL_ORDERs] FOREIGN KEY([orderid])
REFERENCES [dbo].[orders] ([orderid])

ALTER TABLE [dbo].[order_detail] CHECK CONSTRAINT [FK_ORDER_DETAIL_ORDERs]

/****** Object:  ForeignKey [FK_orderitems_exam_items]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems]  WITH CHECK ADD  CONSTRAINT [FK_orderitems_exam_items] FOREIGN KEY([exam_item_id])
REFERENCES [dbo].[exam_items] ([exam_item_id])

ALTER TABLE [dbo].[orderitems] CHECK CONSTRAINT [FK_orderitems_exam_items]

/****** Object:  ForeignKey [FK_OrderItems_ORDERs]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_ORDERs] FOREIGN KEY([orderid])
REFERENCES [dbo].[orders] ([orderid])

ALTER TABLE [dbo].[orderitems] CHECK CONSTRAINT [FK_OrderItems_ORDERs]

/****** Object:  ForeignKey [FK_orderitems_detail_orderitems]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems_detail]  WITH CHECK ADD  CONSTRAINT [FK_orderitems_detail_orderitems] FOREIGN KEY([orderitemsid])
REFERENCES [dbo].[orderitems] ([orderitemsid])
ON DELETE CASCADE

ALTER TABLE [dbo].[orderitems_detail] CHECK CONSTRAINT [FK_orderitems_detail_orderitems]

/****** Object:  ForeignKey [FK_orderitems_detail_orders]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orderitems_detail]  WITH CHECK ADD  CONSTRAINT [FK_orderitems_detail_orders] FOREIGN KEY([orderid])
REFERENCES [dbo].[orders] ([orderid])
ON DELETE CASCADE

ALTER TABLE [dbo].[orderitems_detail] CHECK CONSTRAINT [FK_orderitems_detail_orders]

/****** Object:  ForeignKey [FK_orders_insurance_plan2]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orders]  WITH NOCHECK ADD  CONSTRAINT [FK_orders_insurance_plan2] FOREIGN KEY([insurance_plan_id])
REFERENCES [dbo].[insurance_plan] ([insurance_plan_id])
NOT FOR REPLICATION

ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_insurance_plan2]

/****** Object:  ForeignKey [FK_orders_marketing]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orders]  WITH NOCHECK ADD  CONSTRAINT [FK_orders_marketing] FOREIGN KEY([marketingid])
REFERENCES [dbo].[marketing] ([marketingid])

ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_marketing]

/****** Object:  ForeignKey [FK_orders_physician]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [FK_orders_physician] FOREIGN KEY([physicianid])
REFERENCES [dbo].[physician] ([physicianid])

ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_physician]

/****** Object:  ForeignKey [FK_patient_contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[patient_insurance]  WITH CHECK ADD  CONSTRAINT [FK_patient_contacts] FOREIGN KEY([contactid])
REFERENCES [dbo].[contacts] ([contactid])
ON DELETE CASCADE

ALTER TABLE [dbo].[patient_insurance] CHECK CONSTRAINT [FK_patient_contacts]

/****** Object:  ForeignKey [FK_patient_insurance_insurance]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[patient_insurance]  WITH CHECK ADD  CONSTRAINT [FK_patient_insurance_insurance] FOREIGN KEY([insurance_company_id])
REFERENCES [dbo].[insurance] ([id])

ALTER TABLE [dbo].[patient_insurance] CHECK CONSTRAINT [FK_patient_insurance_insurance]

/****** Object:  ForeignKey [FK_patient_insurance_insurance_plan]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[patient_insurance]  WITH CHECK ADD  CONSTRAINT [FK_patient_insurance_insurance_plan] FOREIGN KEY([insurance_plan_id])
REFERENCES [dbo].[insurance_plan] ([insurance_plan_id])

ALTER TABLE [dbo].[patient_insurance] CHECK CONSTRAINT [FK_patient_insurance_insurance_plan]

/****** Object:  ForeignKey [FK_Payments_ORDERs]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[payments]  WITH NOCHECK ADD  CONSTRAINT [FK_Payments_ORDERs] FOREIGN KEY([orderid])
REFERENCES [dbo].[orders] ([orderid])

ALTER TABLE [dbo].[payments] CHECK CONSTRAINT [FK_Payments_ORDERs]

/****** Object:  ForeignKey [FK_physicianavailability_Physician]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[physicianavailability]  WITH NOCHECK ADD  CONSTRAINT [FK_physicianavailability_Physician] FOREIGN KEY([physicianid])
REFERENCES [dbo].[physician] ([physicianid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[physicianavailability] NOCHECK CONSTRAINT [FK_physicianavailability_Physician]

/****** Object:  ForeignKey [FK_physicianavailexcept_physician]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[physicianavailexcept]  WITH NOCHECK ADD  CONSTRAINT [FK_physicianavailexcept_physician] FOREIGN KEY([physicianid])
REFERENCES [dbo].[physician] ([physicianid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[physicianavailexcept] NOCHECK CONSTRAINT [FK_physicianavailexcept_physician]

/****** Object:  ForeignKey [FK_physicianavailexcept_audit_mycompanyinformation]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[physicianavailexcept_audit]  WITH NOCHECK ADD  CONSTRAINT [FK_physicianavailexcept_audit_mycompanyinformation] FOREIGN KEY([locationid])
REFERENCES [dbo].[mycompanyinformation] ([setupid])
ON DELETE CASCADE
NOT FOR REPLICATION

ALTER TABLE [dbo].[physicianavailexcept_audit] NOCHECK CONSTRAINT [FK_physicianavailexcept_audit_mycompanyinformation]

/****** Object:  ForeignKey [FK_physicianavailexcept_audit_physician]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[physicianavailexcept_audit]  WITH NOCHECK ADD  CONSTRAINT [FK_physicianavailexcept_audit_physician] FOREIGN KEY([physicianid])
REFERENCES [dbo].[physician] ([physicianid])
ON DELETE CASCADE
NOT FOR REPLICATION

ALTER TABLE [dbo].[physicianavailexcept_audit] NOCHECK CONSTRAINT [FK_physicianavailexcept_audit_physician]

/****** Object:  ForeignKey [FK_prescriptions_contactlensname]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_contactlensname] FOREIGN KEY([clensodname])
REFERENCES [dbo].[contactlensname] ([nameid])

ALTER TABLE [dbo].[prescriptions] NOCHECK CONSTRAINT [FK_prescriptions_contactlensname]

/****** Object:  ForeignKey [FK_prescriptions_contactlensname1]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_contactlensname1] FOREIGN KEY([clensosname])
REFERENCES [dbo].[contactlensname] ([nameid])

ALTER TABLE [dbo].[prescriptions] NOCHECK CONSTRAINT [FK_prescriptions_contactlensname1]

/****** Object:  ForeignKey [FK_Prescriptions_Contacts]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_Prescriptions_Contacts] FOREIGN KEY([patientid])
REFERENCES [dbo].[contacts] ([contactid])

ALTER TABLE [dbo].[prescriptions] CHECK CONSTRAINT [FK_Prescriptions_Contacts]

/****** Object:  ForeignKey [FK_prescriptions_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_domain] FOREIGN KEY([speclensmaterial])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[prescriptions] CHECK CONSTRAINT [FK_prescriptions_domain]

/****** Object:  ForeignKey [FK_prescriptions_domain1]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_domain1] FOREIGN KEY([speclensstyle])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[prescriptions] CHECK CONSTRAINT [FK_prescriptions_domain1]

/****** Object:  ForeignKey [FK_prescriptions_domain2]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_domain2] FOREIGN KEY([blendidos])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[prescriptions] CHECK CONSTRAINT [FK_prescriptions_domain2]

/****** Object:  ForeignKey [FK_prescriptions_domain3]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_domain3] FOREIGN KEY([blendidod])
REFERENCES [dbo].[domain] ([domainid])

ALTER TABLE [dbo].[prescriptions] CHECK CONSTRAINT [FK_prescriptions_domain3]

/****** Object:  ForeignKey [FK_prescriptions_exam]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[exam] ([exam_id])

ALTER TABLE [dbo].[prescriptions] NOCHECK CONSTRAINT [FK_prescriptions_exam]

/****** Object:  ForeignKey [FK_prescriptions_manufacturer]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_manufacturer] FOREIGN KEY([clensodmfg])
REFERENCES [dbo].[manufacturer] ([manufacturerid])

ALTER TABLE [dbo].[prescriptions] NOCHECK CONSTRAINT [FK_prescriptions_manufacturer]

/****** Object:  ForeignKey [FK_prescriptions_manufacturer1]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[prescriptions]  WITH NOCHECK ADD  CONSTRAINT [FK_prescriptions_manufacturer1] FOREIGN KEY([clensosmfg])
REFERENCES [dbo].[manufacturer] ([manufacturerid])

ALTER TABLE [dbo].[prescriptions] NOCHECK CONSTRAINT [FK_prescriptions_manufacturer1]

/****** Object:  ForeignKey [FK_ProviderInsurance_Provider]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[providerinsurance]  WITH NOCHECK ADD  CONSTRAINT [FK_ProviderInsurance_Provider] FOREIGN KEY([providerid])
REFERENCES [dbo].[provider] ([providerid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[providerinsurance] NOCHECK CONSTRAINT [FK_ProviderInsurance_Provider]

/****** Object:  ForeignKey [FK_recall_schedule_letter]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[recall_schedule]  WITH CHECK ADD  CONSTRAINT [FK_recall_schedule_letter] FOREIGN KEY([letter_id])
REFERENCES [dbo].[letter] ([letterid])

ALTER TABLE [dbo].[recall_schedule] CHECK CONSTRAINT [FK_recall_schedule_letter]

/****** Object:  ForeignKey [FK_recall_schedule_recallreasons]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[recall_schedule]  WITH NOCHECK ADD  CONSTRAINT [FK_recall_schedule_recallreasons] FOREIGN KEY([recall_reason_id])
REFERENCES [dbo].[recallreasons] ([recallreasonid])
NOT FOR REPLICATION

ALTER TABLE [dbo].[recall_schedule] NOCHECK CONSTRAINT [FK_recall_schedule_recallreasons]

/****** Object:  ForeignKey [FK_receipt_note_mycompanyinformation]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[receipt_note]  WITH CHECK ADD  CONSTRAINT [FK_receipt_note_mycompanyinformation] FOREIGN KEY([locationid])
REFERENCES [dbo].[mycompanyinformation] ([setupid])

ALTER TABLE [dbo].[receipt_note] CHECK CONSTRAINT [FK_receipt_note_mycompanyinformation]

/****** Object:  ForeignKey [FK_report_affections_report_affections]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[report_affections]  WITH CHECK ADD  CONSTRAINT [FK_report_affections_report_affections] FOREIGN KEY([id_customization])
REFERENCES [dbo].[FLXR_CUSTOMIZATION] ([id])
ON DELETE CASCADE

ALTER TABLE [dbo].[report_affections] CHECK CONSTRAINT [FK_report_affections_report_affections]

/****** Object:  ForeignKey [FK__report_gr__emplo__63D8CE75]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[report_group_email_list]  WITH CHECK ADD FOREIGN KEY([employeeid])
REFERENCES [dbo].[employee] ([employeeid])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK__report_gr__group__64CCF2AE]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[report_group_email_list]  WITH CHECK ADD FOREIGN KEY([groupid])
REFERENCES [dbo].[report_group] ([report_group_id])
ON DELETE CASCADE

/****** Object:  ForeignKey [FK_security_control_group_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[security_control_group]  WITH NOCHECK ADD  CONSTRAINT [FK_security_control_group_domain] FOREIGN KEY([security_group_id])
REFERENCES [dbo].[domain] ([domainid])
ON DELETE CASCADE

ALTER TABLE [dbo].[security_control_group] CHECK CONSTRAINT [FK_security_control_group_domain]

/****** Object:  ForeignKey [FK_spectacleinventory_spectaclelens_pricing]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[spectacleinventory]  WITH CHECK ADD  CONSTRAINT [FK_spectacleinventory_spectaclelens_pricing] FOREIGN KEY([spectaclelens_pricing_id])
REFERENCES [dbo].[spectaclelens_pricing] ([spectaclelens_pricing_id])
ON DELETE SET NULL

ALTER TABLE [dbo].[spectacleinventory] CHECK CONSTRAINT [FK_spectacleinventory_spectaclelens_pricing]

/****** Object:  ForeignKey [FK_spectacleinventory_suppliers]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[spectacleinventory]  WITH CHECK ADD  CONSTRAINT [FK_spectacleinventory_suppliers] FOREIGN KEY([supplierid])
REFERENCES [dbo].[suppliers] ([supplierid])
ON DELETE SET NULL

ALTER TABLE [dbo].[spectacleinventory] CHECK CONSTRAINT [FK_spectacleinventory_suppliers]

/****** Object:  ForeignKey [FK_spectacleinventory_codes_spectacleinventory]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[spectacleinventory_codes]  WITH NOCHECK ADD  CONSTRAINT [FK_spectacleinventory_codes_spectacleinventory] FOREIGN KEY([specid])
REFERENCES [dbo].[spectacleinventory] ([specid])
ON DELETE CASCADE
NOT FOR REPLICATION

ALTER TABLE [dbo].[spectacleinventory_codes] NOCHECK CONSTRAINT [FK_spectacleinventory_codes_spectacleinventory]

/****** Object:  ForeignKey [FK_spectacleinventory_opc_spectacleinventory]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[spectacleinventory_opc]  WITH CHECK ADD  CONSTRAINT [FK_spectacleinventory_opc_spectacleinventory] FOREIGN KEY([specid])
REFERENCES [dbo].[spectacleinventory] ([specid])
ON DELETE CASCADE

ALTER TABLE [dbo].[spectacleinventory_opc] CHECK CONSTRAINT [FK_spectacleinventory_opc_spectacleinventory]

/****** Object:  ForeignKey [FK_spectaclelens_pricing_detail_domain]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[spectaclelens_pricing_detail]  WITH CHECK ADD  CONSTRAINT [FK_spectaclelens_pricing_detail_domain] FOREIGN KEY([material_id])
REFERENCES [dbo].[domain] ([domainid])
ON DELETE SET NULL

ALTER TABLE [dbo].[spectaclelens_pricing_detail] CHECK CONSTRAINT [FK_spectaclelens_pricing_detail_domain]

/****** Object:  ForeignKey [FK_timeclock_employee]    Script Date: 12/16/2011 00:48:46 ******/
ALTER TABLE [dbo].[timeclock]  WITH NOCHECK ADD  CONSTRAINT [FK_timeclock_employee] FOREIGN KEY([employeeid])
REFERENCES [dbo].[employee] ([employeeid])

ALTER TABLE [dbo].[timeclock] CHECK CONSTRAINT [FK_timeclock_employee]

GO

CREATE PROCEDURE [dbo].[sp_get_sequence]
@sequence varchar(100),
@sequence_id INT OUTPUT
AS
set @sequence_id = -1

UPDATE sequences
SET @sequence_id = sequence = sequence + 1
WHERE table_name = @sequence

RETURN @sequence_id
