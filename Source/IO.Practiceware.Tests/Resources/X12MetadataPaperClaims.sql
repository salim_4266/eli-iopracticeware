SELECT 1 AS Value INTO #NoAudit 
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'66984'
	,1
	,'CATARACT SX'
	,NULL
	,0
	,0
	,CONVERT(DATETIME, '20000101')
	,1000.00
	,0
	,0
	,'SURGERY'
	,2
	,1
	)
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'67028'
	,1
	,'SURGERY'
	,NULL
	,0
	,0
	,CONVERT(DateTime, '20000101')
	,1000.00
	,0
	,0
	,'Intravitreal Injection'
	,2
	,1
	)
IF OBJECT_ID('dbo.PadLeft') IS NULL
BEGIN
EXEC ('

CREATE FUNCTION dbo.PadLeft(
      @String int
     ,@NumChars int
     )
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @PadChar char(1) 
	SET @PadChar = ''0''
    RETURN STUFF(@String, 1, 0, REPLICATE(@PadChar, @NumChars - LEN(@String)))
END
')

END

INSERT INTO model.ServiceModifiers (BillingOrdinalId, Code, [Description], ClinicalOrdinalId, IsArchived)
SELECT 
99 AS BillingOrdinalId
,'24' AS Code
,'Unrelated E M Service by the Same Physician During PostOp' AS [Description]
,99 AS ClinicalOrdinalId
,CONVERT(BIT,0) AS IsArchived

--*1st Test Medicare Primary Claim*
--         Patient: Medicare Medicaid  
--         Billed to Mcare
--         Payment from Mcare (Should not be included in balance)
--         Payment from 2y
--         Payment from Patient (Should not be included in balance)
--         Auth Code
--         Service Note 
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'MEDICAID'
	,'C'
	,''
	,''
	,'85 RED ROSE STREET'
	,''
	,'ATLANTA'
	,'GA'
	,'10022'
	,'3033335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19151123'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--ADD Primary Plan
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'999663333A' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'345TYU' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICAID'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,12/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/
---- 

--ADD APPOINTMENT PATIENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20111115,
	900, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE ResourceType = 'D' and ResourceLastName = 'REISS'



--ADD ACTIVITY RECORD PATIENT
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20111115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'


--Add Diagnoses Patient
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', ''
, 'A', '', '', '', '', 0, '', '')

DECLARE @primaryPatientReceivableId int

-- PatientReceivables  (Medicare primary)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '4.50'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,CASE
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) <= 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) <= 5)
		THEN REPLACE(STR(CAST(ap.PatientId AS NVARCHAR), 5, 0), ' ', '0')+'-'+REPLACE(STR(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR) , 5, 0), ' ', '0')
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) > 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) <= 5)
		THEN CAST(ap.PatientId AS NVARCHAR)+'-'+REPLACE(STR(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR) , 5, 0), ' ', '0')
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) <= 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) > 5)
		THEN REPLACE(STR(CAST(ap.PatientId AS NVARCHAR), 5, 0), ' ', '0')+'-'+CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)
	ELSE CAST(ap.PatientId AS NVARCHAR)+'-'+CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)
	END AS Invoice
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,'ICN NUMBER'
	,''
	,0
	,''
	,''
	,'100.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SELECT @primaryPatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

--1st service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(PatientId,5) + '-' + dbo.PadLeft(IDENT_CURRENT('Appointments'),5) AS Invoice
        ,'J3490', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')

-- BILLING TRANSACTIONS Balance Billed to Medicare
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20111115
	,654
	,'I'
	,'//Amt/4.5'
	,'P'
	)

-- PatientReceivables (Medicaid Secondary)

INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '4.50'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,CASE
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) <= 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) <= 5)
		THEN REPLACE(STR(CAST(ap.PatientId AS NVARCHAR), 5, 0), ' ', '0')+'-'+REPLACE(STR(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR) , 5, 0), ' ', '0')
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) > 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) <= 5)
		THEN CAST(ap.PatientId AS NVARCHAR)+'-'+REPLACE(STR(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR) , 5, 0), ' ', '0')
	WHEN (LEN(CAST(ap.PatientId AS NVARCHAR)) <= 5 AND LEN(CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)) > 5)
		THEN REPLACE(STR(CAST(ap.PatientId AS NVARCHAR), 5, 0), ' ', '0')+'-'+CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)
	ELSE CAST(ap.PatientId AS NVARCHAR)+'-'+CAST(IDENT_CURRENT('Appointments') AS NVARCHAR)
	END AS Invoice
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,'ICN NUMBER'
	,'3'
	,0
	,''
	,''
	,'100.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

--Transaction to Medicaid, now closed.
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20111115
	,654
	,'I'
	,'//Amt/9.00'
	,'B'
	)
	
--NOTE for Drug Code
INSERT INTO PatientNotes
           ([PatientId]
           ,[AppointmentId]
           ,[NoteType]
           ,[Note1]
           ,[Note2]
           ,[Note3]
           ,[Note4]
           ,[UserName]
           ,[NoteDate]
           ,[NoteSystem]
           ,[NoteEye]
           ,[NoteCommentOn]
           ,[NoteCategory]
           ,[NoteOffDate]
           ,[NoteHighlight]
           ,[NoteILPNRef]
           ,[NoteAlertType]
           ,[NoteAudioOn]
           ,[NoteClaimOn])
SELECT IDENT_CURRENT('model.Patients'), 
	IDENT_CURRENT('Appointments'), 
	'B',
	'This is a Service Note that is just a little bit too long',
	'',
	'',
	'',
	'IOP',
	20120120,
	'R000' +  CONVERT(nvarchar, IDENT_CURRENT('model.Patients')) + '-000' + CONVERT(nvarchar, IDENT_CURRENT('Appointments')) + 'CJ3490',
	'',
	'F',
	'',
	'',
	'',
	'',
	'00000000000000000000000000000000',
	'F',
	'T'


-- 1st ServiceTransaction Billed To MEDICARE PRIMARY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,'4.50'
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICARE'


-- PatientReceivablePayments 
--Medicare batch
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5125
	,N'20111215'
	,N'7892'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111207'
FROM PracticeInsurers WHERE InsurerName = 'MEDICARE'

DECLARE @firstPaymentId int
SET @firstPaymentId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- Medicare Payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@primaryPatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,50
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Medicare Contractual Writeoff
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@primaryPatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,25
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@firstPaymentId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Medicare Coinsurance
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@primaryPatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,15.00
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@firstPaymentId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-- 2ND ServiceTransaction (Mcaid Closed)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20111215
	,'9.00'
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICAID'
AND ptj.TransactionRemark = '//Amt/9.00'

-- PAYMENT FROM MEDICAID  
--Medicaid Batch
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--Medicaid payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,5
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,PaymentId
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PatientReceivablePayments ON PaymentAmount = 5 
	AND PaymentCheck = '33BB'
	AND PaymentType = '='
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-- Patient Payment 
--Patient batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,'.50'
	,N'20120101'
	,N'105289455'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''

	INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,'.50'
	,N'20120101'
	,N'105289455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N'20110101'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--*2nd Test Medicaid Primary/Anesthesia Claim*
-- Patient: Medicaid Anesth
-- Precert
-- Refferal/ReferringDoc (should get doc from referral)
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'ANESTH'
	,'MEDICAID'
	,'C'
	,''
	,''
	,'45 White ROSE STREET'
	,''
	,'ATLANTA'
	,'GA'
	,'10022'
	,'3033335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19151123'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--ADD Primary Plan
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'123' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICAID'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--Add referral
INSERT INTO model.PatientInsuranceReferrals
           ([PatientId]
           ,[ReferralCode]
           ,[ExternalProviderId]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[TotalEncountersCovered]
           ,[Comment]
           ,[DoctorId]
           ,[PatientInsuranceId]
           ,[IsArchived])
SELECT IDENT_CURRENT('model.Patients'),
	'referral4567',
	VendorId,
	CONVERT(datetime,'20111201',112),
	CONVERT(datetime,'20121201',112),
	6,
	'',
	ResourceId,
	pi.Id,
	CONVERT(bit, 0)
FROM PracticeVendors pv 
INNER JOIN Resources re ON ResourceType = 'D' and ResourceLastName = 'REISS'
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = IDENT_CURRENT('model.Patients')
	AND pi.IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.Insurers pri ON Name = 'MEDICAID'
WHERE VendorLastName = 'COX'

---- 
--ADD APPOINTMENT PATIENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20111115,
	900, 10, IDENT_CURRENT('model.PatientInsuranceReferrals'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'ANESTH' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE ResourceType = 'D' and ResourceLastName = 'REISS'

INSERT INTO model.EncounterPatientInsuranceReferral (Encounters_Id, PatientInsuranceReferrals_Id)
SELECT IDENT_CURRENT('dbo.Appointments') AS Encounters_Id, IDENT_CURRENT('model.PatientInsuranceReferrals')

--ADD ACTIVITY RECORD 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20111115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'ANESTH' AND FirstName = 'MEDICAID'


--Add Diagnoses
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', ''
, 'A', '', '', '', '', 0, '', '')

--Add Authorization
INSERT INTO model.PatientInsuranceAuthorizations (
	AuthorizationCode
	,AuthorizationDateTime
	,IsArchived
	,PatientInsuranceId
	,EncounterId
	,Comment
	)
SELECT '9080ABDC'
	,CONVERT(datetime,'20111122', 112)
	,CONVERT(bit, 0)
	,pins.Id
	,IDENT_CURRENT('Appointments')
	,''
FROM model.PatientInsurances pins
INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId
WHERE ins.Name = 'MEDICAID'
	AND pins.InsuredPatientId = IDENT_CURRENT('model.Patients')

UPDATE Appointments 
SET PreCertId = IDENT_CURRENT('model.PatientInsuranceAuthorizations')
WHERE AppointmentId = IDENT_CURRENT('Appointments')

-- PatientReceivable
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '300'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( ap.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111115
	,'300.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'300.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN PracticeVendors ON VendorLastName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

-- BILLING TRANSACTIONS Claim Billed to Medicaid
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20111115
	,654
	,'I'
	,'//Amt/300.00'
	,'P'
	)
	
--1st service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(PatientId,5) +'-'+ dbo.PadLeft(AppointmentId,5)
		,'99203', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')


-- 1st ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,'100'
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICAID'

--2nd service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
        ,'92014', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')


-- 2nd ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,'100'
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICAID'

--3rd service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
        ,'99999', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')


-- 3rd ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,'100'
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICAID'

--*3rd Test DME Claim*
--Patient: DME Medicare
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'DME'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'444352785A' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120120,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE dr.ResourceType = 'Q' and dr.ResourceLastName = 'BARTELS'

--ADD ACTIVITY RECORD
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120120, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--ADD CLINICAL DATA AND PRESCRIPTIONS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition], [PostOpPeriod], [Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Y', '', '1', 'COMPREHENSIVE', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'H', '', '/CHIEFCOMPLAINTS', 'CC:FLASHES', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', 'TIME=01:07 PM (SMITH)', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', '*CURRENTEVERYDAY=T14854 <CURRENTEVERYDAY>', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OD', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OS', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OD', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OS', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '5', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '4', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Z', '', '1', '92012', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/LENTICULAR SINGLE VISION-2/ PLANO ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '2', 'DISPENSE CL RX-9/-1/-2/ +2.00 -0.50 X180 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-3/ +0.25 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-4/-5/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-6/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-7/', '^', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120120
	,'500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'500'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS 

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20120121
	,654
	,'I'
	,'//Amt/500'
	,'P'
	)


--SERVICES 
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'67028', 'RT', '1', '250.00', '04', '11'
		,20120120
        , '2', 'A', 'F', 'F', '156.39', '1', 0,
convert(varchar(8),getdate(),112), 'INTRAVITREAL I')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,250
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'J3490', '', '5', '100.00', '12', '11'
		,20120120
        , '1234', 'A', 'F', 'F', '5126.78', '2', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')




-- 2d ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,100
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--3rd service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'99214', '', '1', '150.00', '01', '11'
		,20120120
        , '', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')

--3rd service Transaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,150
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--4th Test DME w/Referral
-- Patient: DME Referral
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'REFERRAL'
	,'DME'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'555352785A' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--Add referral
INSERT INTO model.PatientInsuranceReferrals
           ([PatientId]
           ,[ReferralCode]
           ,[ExternalProviderId]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[TotalEncountersCovered]
           ,[Comment]
           ,[DoctorId]
           ,[PatientInsuranceId]
           ,[IsArchived])
SELECT IDENT_CURRENT('model.Patients'),
	'referral25846',
	VendorId,
	CONVERT(datetime,'20111201',112),
	CONVERT(datetime,'20121201',112),
	6,
	'',
	ResourceId,
	pi.Id,
	CONVERT(bit, 0)
FROM PracticeVendors pv 
INNER JOIN Resources re ON ResourceType = 'D' and ResourceLastName = 'REISS'
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = IDENT_CURRENT('model.Patients')
	AND pi.IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.Insurers pri ON Name = 'MEDICARE'
WHERE VendorLastName = 'COX'

--ADD APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120120,
	600, 10, IDENT_CURRENT('model.PatientInsuranceReferrals'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE dr.ResourceType = 'Q' and dr.ResourceLastName = 'BARTELS'

INSERT INTO model.EncounterPatientInsuranceReferral (Encounters_Id, PatientInsuranceReferrals_Id)
SELECT IDENT_CURRENT('dbo.Appointments') AS Encounters_Id, IDENT_CURRENT('model.PatientInsuranceReferrals')

--ADD ACTIVITY RECORD
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120120, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--ADD CLINICAL DATA AND PRESCRIPTIONS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition], [PostOpPeriod], [Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Y', '', '1', 'COMPREHENSIVE', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'H', '', '/CHIEFCOMPLAINTS', 'CC:FLASHES', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', 'TIME=01:07 PM (SMITH)', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', '*CURRENTEVERYDAY=T14854 <CURRENTEVERYDAY>', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OD', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OS', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OD', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OS', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '5', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '4', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Z', '', '1', '92012', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/LENTICULAR SINGLE VISION-2/ PLANO ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '2', 'DISPENSE CL RX-9/-1/-2/ +2.00 -0.50 X180 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-3/ +0.25 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-4/-5/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-6/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-7/', '^', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120120
	,'500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'500'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS 

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20120121
	,654
	,'I'
	,'//Amt/500'
	,'P'
	)


--SERVICES 
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'67028', 'RT', '1', '250.00', '04', '11'
		,20120120
        , '2', 'A', 'F', 'F', '156.39', '1', 0,
convert(varchar(8),getdate(),112), 'INTRAVITREAL I')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,250
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'J3490', '', '5', '100.00', '12', '11'
		,20120120
        , '1234', 'A', 'F', 'F', '5126.78', '2', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')




-- 2d ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,100
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--3rd service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'99214', '', '1', '150.00', '01', '11'
		,20120120
        , '', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')

--3rd service Transaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,150
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--*5th Test* 
--   Patient: AETNAPARENT, CIGNAPARENT
--   2 diff policy holders
--   pri pol holder: Aetna Only, Sec pol holder: Cigna Blue Shield
--   Refer DOC
--   1 Service Status "Wait" (should not be in claim)
--   Additional Info (dates,disability etc.)
--   8 services
--   NOS Note

--Add Primary Policy Holder: AETNA ONLY
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'AETNA'
	,'ONLY'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,'BRICK LAYER'
	,'BRICK LAYERS R US'
	,'5524 YELLOW BRICK ROAD'
	,'4D'
	,'Brooklyn'
	,'NY'
	,'11238'
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

	--PatientFinancial AETNA Only
--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'12345' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20090201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'AETNA'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--Add Secondary Policy Holder
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'BLUE SHIELD'
	,'CIGNA'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

	--PatientFinancial 
--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'546546' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20090201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'CIGNA'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'32323' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20090201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'BLUE SHIELD'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,12/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--PatientFinancial 
--ADD INSURANCE PLAN

--Add Patient
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'CIGNAPARENT'
	,'AETNAPARENT'
	,''
	,''
	,''
	,'448 STATE STREET'
	,''
	,'BROOKLYN'
	,'NY'
	,'12117'
	,'7188750653'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19550715'
	,''
	,'FRENCH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'HISP'
FROM PatientDemographics pdSpouse
WHERE pdSpouse.LastName = 'AETNA' AND pdSpouse.FirstName = 'ONLY'

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,11
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'))
,NULL
,0

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'BLUE SHIELD' AND FirstName = 'CIGNA') ORDER BY Id ASC)
,NULL
,0

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,15
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'BLUE SHIELD' AND FirstName = 'CIGNA') ORDER BY Id DESC)
,NULL
,0
--make second patientid blue shield, cigna relationship c.

--ADD APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120107,
	700, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120201
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'NPG'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120107, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '365.10', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120107
	,'885.00'
	,ResourceId1
	,'A'
	,'NC'
	,'01/07/2012'
	,'01/07/2012'
	,'T'
	,'S'
	,'01/07/2012'
	,'01/07/2012'
	,'Y'
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'775.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON LastName = 'AETNA' and FirstName = 'ONLY'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN PracticeVendors ON VendorLastName = 'COX'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20120107
	,654
	,'I'
	,'//Amt/575'
	,'P'
	)


--1st service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'67028', 'RT', '1', '375', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 1st ServiceTransaction 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,375
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'99214', '2459RT', '1', '100.00', '01', '11'
		,20120107
        , '213', 'A', 'F', 'F', '100', '2', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')

-- 2ND ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,100
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- 3rD Service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'25999', '', '1', '100', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '100', '3', 0,
convert(varchar(8),getdate(),112), 'NOS DRUG')

--NOTE for NOS Code
INSERT INTO PatientNotes
           ([PatientId]
           ,[AppointmentId]
           ,[NoteType]
           ,[Note1]
           ,[Note2]
           ,[Note3]
           ,[Note4]
           ,[UserName]
           ,[NoteDate]
           ,[NoteSystem]
           ,[NoteEye]
           ,[NoteCommentOn]
           ,[NoteCategory]
           ,[NoteOffDate]
           ,[NoteHighlight]
           ,[NoteILPNRef]
           ,[NoteAlertType]
           ,[NoteAudioOn]
           ,[NoteClaimOn])
SELECT IDENT_CURRENT('model.Patients'), 
	IDENT_CURRENT('Appointments'), 
	'B',
	'THIS IS AN NOS NOTE',
	'',
	'',
	'',
	'IOP',
	20120107,
	'R000' +  CONVERT(nvarchar, IDENT_CURRENT('model.Patients')) + '-000' + CONVERT(nvarchar, IDENT_CURRENT('Appointments')) + 'C25999',
	'',
	'F',
	'',
	'',
	'',
	'',
	'00000000000000000000000000000000',
	'F',
	'T'

	-- 3rd ServiceTransaction 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,100
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--4th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'92133', 'RT', '1', '150', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 4th ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,150
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--5th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'99204', 'RT', '1', '100', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 5th ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,100
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--6th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'92038', 'RT', '1', '150', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 6th ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,150
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--7th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'92250', 'RT', '1', '50', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 1st ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,50
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--7th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'92250', 'LT', '1', '50', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 8th ServiceTransaction - WAIT STATUS
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,50
	,'V'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--6th Test Professional Facility Claim
-- Patient: Medicare Only
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'ONLY'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial 
--ADD INSURANCE PLAN

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'111999888A' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

---DOCTOR'S CLAIM
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110504, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 1007, '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 



--Diagnoses for Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '660.22', '1', 0,
			20110506, 'CATARACT SX'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'P'
	)


--SERVICES  Surgery Claim for Facility
--1st service 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '703.96', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--7th Test Instituional Claim (UB04) 
--Patient: Insurance Complicated
--Two diff policy holders
--  Pri pol holder: Cigna Blue Shield (2 policies), Sec pol hol: Aetna Only (1 policy)
--  Payment/Coins/Deduct for each payer
--  Billed to  3rd
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'INSURANCE'
	,'COMPLICATED'
	,''
	,''
	,''
	,'448 STATE STREET'
	,''
	,'BROOKLYN'
	,'NY'
	,'12117'
	,'7188750653'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19550715'
	,''
	,'FRENCH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'HISP'
FROM PatientDemographics pdSpouse
WHERE pdSpouse.LastName = 'BLUE SHIELD' AND pdSpouse.FirstName = 'CIGNA'

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,11
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'BLUE SHIELD' AND FirstName = 'CIGNA') ORDER BY Id ASC)
,NULL
,0

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,12
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'BLUE SHIELD' AND FirstName = 'CIGNA') ORDER BY Id DESC)
,NULL
,0

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,3
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY') ORDER BY Id DESC)
,NULL
,0

--Surgery Appointment WITH DOCTOR 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100503,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20100207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20100503, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 1007, '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 


--Diagnoses for  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 

-- Primary CIGNA Receivable (dr claim)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '225.64'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20100503
	,'5000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'2665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20100506
	,654
	,'I'
	,'//Amt/5000'
	,'B'
	)

	
--SERVICES  Surgery Claim for Doctor
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'RT', '1', '2300.00', '02', '24',
			20100503, '123', 'A', 'F', 'F', '1900', '2', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 

-- 1st ServiceTransaction Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100506
	,2300
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100503,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


--Primary (CIGNA) Receivable (facility receivable)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pdPatient.PatientId
	,pdInsured.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pdPatient.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20100503
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics PdInsured ON pdInsured.LastName = 'BLUE SHIELD' and pdInsured.FirstName = 'CIGNA'
INNER JOIN PatientDemographics PdPatient on PdPatient.LastName = 'INSURANCE' and pdPatient.FirstName = 'COMPLICATED'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- Primary BILLING TRANSACTIONS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'LT', '1', '1000.00', '02', '24',
			20100503, '1', 'A', 'F', 'F', '665.87', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'INSURANCE' AND pd.FirstName = 'COMPLICATED' 

-- ServiceTransaction (CIGNA)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--CIGNA BATCH
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,100
	,N'20110715'
	,N'111'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers 
WHERE InsurerName = 'CIGNA'

DECLARE @CignaPRPId INT
SET @CignaPRPId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--CIGNA Coinsurance
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'|'
	,10
	,N'20110715'
	,N'111'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'CIGNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '111'

--CIGNA Deductible
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'!'
	,10
	,N'20110715'
	,N'111'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@firstPaymentId 
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'CIGNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '111'

--CIGNA Payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'P'
	,100
	,N'20110715'
	,N'111'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@CignaPRPId
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'CIGNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '111'


--Secondary Receivable (BLUE SHIELD)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pdPatient.PatientId
	,pdInsured.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pdPatient.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20100503
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdInsured ON pdInsured.LastName = 'BLUE SHIELD' and pdInsured.FirstName = 'CIGNA'
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'INSURANCE' and pdPatient.FirstName = 'COMPLICATED'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- Secondary BILLING TRANSACTIONS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


-- ServiceTransaction (BLUE SHIELD)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--BLUE SHIELD BATCH
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,100
	,N'20110715'
	,N'222'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'BLUE SHIELD'

DECLARE @BlueShieldBatchPymntId INT
SET @BlueShieldBatchPymntId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--BLUE SHIELD Coinsurance
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'|'
	,20
	,N'20110715'
	,N'222'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'BLUE SHIELD'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '222'

--BLUE SHIELD Deductible
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'!'
	,20
	,N'20110715'
	,N'222'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@BlueShieldBatchPymntId 
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'BLUE SHIELD'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '222'

INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'P'
	,100
	,N'20110715'
	,N'222'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@BlueShieldBatchPymntId
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'BLUE SHIELD'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '222'


--AETNA Receivable
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pdPatient.PatientId
	,pdInsured.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pdPatient.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20100503
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdInsured ON pdInsured.LastName = 'AETNA' and pdInsured.FirstName = 'ONLY'
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'INSURANCE' and pdPatient.FirstName = 'COMPLICATED'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- Aetna BILLING TRANSACTIONS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'P'
	)


--ServiceTransaction (Aetna)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--AETNA BATCH
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,100
	,N'20110715'
	,N'333'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'AETNA'

DECLARE @AetnaBatchPrpId INT
SET @AetnaBatchPrpId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--AETNA Coinsurance
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1 
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'|'
	,30
	,N'20110715'
	,N'333'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'AETNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '333'


--AETNA Deductible
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'!'
	,30
	,N'20110715'
	,N'333'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@AetnaBatchPrpId 
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'AETNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '333'

--AETNA Payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'P'
	,100
	,N'20110715'
	,N'333'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@AetnaBatchPrpId
	,N''
	,N''
	FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'AETNA'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 100
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
And InvoiceDate = '20100503'
And PaymentCheck = '333'