IF OBJECT_ID('dbo.PadLeft') IS NULL
BEGIN
EXEC ('

CREATE FUNCTION dbo.PadLeft(
      @String int
     ,@NumChars int
     )
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @PadChar char(1) 
	SET @PadChar = ''0''
    RETURN STUFF(@String, 1, 0, REPLICATE(@PadChar, @NumChars - LEN(@String)))
END
')

END

--metadata for hl7 tests

--metadata for hl7 tests

--ADD PATIENT
INSERT INTO [PatientDemographics]
([LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
,[ProfileComment1],[ProfileComment2],[ContactType]
,[PolicyPatientId],[Relationship],[BillParty],[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty]
,[ReferralCatagory],[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone],[Religion],[Race],[Ethnicity])
SELECT 'TRACY', 'SPENCER', 'B', '', '', '450 EAST 50TH ST', 'APT 2A', 'NEW YORK', 'NY', '10012'
, '2123334444', '7186665555', ''
, '', '', '', ''
, '', '', '', '', '', '', '', ''
, 'F', 'M', '19610105', '', 'RUSSIAN', '0', VendorId, '0'
, '', '', ''
, 0, '', 'N', 0, '', 'N'
, '', 'Y', 'Y', 'Y', '20110501', 'A'
, 'MRS', 'T', '', '0', 'Y', 'Y', ''
, 'N', '', 0, 0, '', '', 'BLACK', 'NOT HISP'
FROM PracticeVendors
WHERE VendorLastName = 'COX'

UPDATE PatientDemographics SET PolicyPatientId = PatientId, Relationship = 'Y'
where PatientId = IDENT_CURRENT('model.Patients')


--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT PatientId, InsurerId, '12345', '', 25, 1, '20110501', '', 'C', 'M'
FROM PatientDemographics 
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



--ADD APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId, PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments, ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5, ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus, SetDate, VisitReason)
SELECT PatientId, AppTypeId, convert(varchar(8),getdate(),112), 600, 10, 0, 0, ResourceId, 'D', 'D', '', ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', '20110507', ''
FROM AppointmentType 
INNER JOIN PatientDemographics ON LastName = 'TRACY' AND FirstName = 'SPENCER'
INNER JOIN Resources ON ResourceLastName = 'REISS' AND ResourceFirstName = 'GEORGE'
WHERE AppointmentType = 'LONG'



--ADD ACTIVITY RECORD
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, convert(varchar(8),getdate(),112), IDENT_CURRENT('Appointments'), 'No Questions', 'U', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

--ADD CLINICAL DATA AND PRESCRIPTIONS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition], [PostOpPeriod], [Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Y', '', '1', 'COMPREHENSIVE', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'H', '', '/CHIEFCOMPLAINTS', 'CC:FLASHES', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/SMOKING', 'TIME=01:07 PM (SMITH)', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/SMOKING', '*CURRENTEVERYDAY=T14854 &lt;CURRENTEVERYDAY&gt;', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'




INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
select IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OD', '1', '375.15', 'E', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
select IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OD', '2', '365.11', 'L', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '1', '375.15', 'E', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '2', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '3', '379.21.1', 'K', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '4', '377.2.2', 'L', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '5', '365.11', 'L', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Q', 'OS', '6', '362.52', 'L', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'




INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OD', '1', '365.11', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OD', '2', '375.15', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OS', '1', '365.11', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OS', '2', '362.52', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OS', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'U', 'OS', '4', '375.15', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '1', '365.11:92133', '1', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '2', '362.52:92235', '2', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '3', '362.52:67028', '2', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '4', '?:J3490', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '5', '362.52:92250', '1', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'P', '', '6', '365.11:92083', '1', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'




INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'Z', '', '1', '99214', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'





INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OU', '1', '365.11', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OS', '2', '362.52', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OU', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OU', '4', '375.15', '^', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', 'TIME=11:14 AM (HARRIS)', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', '*=T0 &lt;*&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', '*OD-FP=T5543 &lt;*OD-FP&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', '*FP-FP=T5559 &lt;NO DIABETIC RETINOPATHY TEMPORAL TO THE MACULA&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', '*OS-FP=T5544 &lt;*OS-FP&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FUNDUS PHOTO', '*FP-FP=T5559 &lt;TORTUOUS VESSELS NASAL TO THE MACULA&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', 'TIME=03:20 PM (JUNIPER)', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', '*=T0 &lt;*&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', '*OD-VF=T5321 &lt;*OD-VF&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', '*PROGRESSIONOFDEFECTS-300=T5319 &lt;&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', '*OS-VF=T5322 &lt;*OS-VF&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/VF (30,2, 24,2,10,2, ESTERMAN)', '*PROGRESSIONOFDEFECTS-300=T5319 &lt;&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', 'TIME=03:20 PM (JUNIPER)', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', '*=T0 &lt;*&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', '*OD-FADR=T4555 &lt;*OD-FADR&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', '*CONFIRMOU-FADR=T14217 &lt;&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', '*OS-FADR=T4556 &lt;*OS-FADR&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/FLUORESCEIN ANGIOGRAPHY', '*CONFIRMOU-FADR=T14217 &lt;&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', 'TIME=12:24 PM (JUMP)', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '/INTRAVIT INJEC, LUCENTIS	*=T0 &lt;*&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*OD-INJEC-LUCENTIS=T8000 &lt;*OD-INJEC-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*CC-LUCENTIS=T8002 &lt;; &gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*SUPERT-LUCENTIS=T13178 &lt;*SUPERT-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*ANEST-LUCENTIS=T8004 &lt;; &gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*LIDOCAINE-LUCENTIS=T9644 &lt;*LIDOCAINE-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*IOP1-LUCENTIS=T8005 &lt;12&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*COMMENTS-LUCENTIS=T8007 &lt;; &gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*VITREOUSHEME-LUCENTIS=T8011 &lt;*VITREOUSHEME-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*EYEPATCH-LUCENTIS=T8008 &lt;; &gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*PATCH4HOURS-LUCENTIS=T8019 &lt;*PATCH4HOURS-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*LOTN-LUCENTIS=T8009 &lt;RTY654&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/INTRAVIT INJEC, LUCENTIS', '*OS-INJEC-LUCENTIS=T8001 &lt;*OS-INJEC-LUCENTIS&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', 'TIME=03:20 PM (JUNIPER)', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*=T0 &lt;*&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*OD-OCTON=T6512 &lt;*OD-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*RNFLGENDEPRSS-OCTON=T7643 &lt;*RNFLGENDEPRSS-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*DISCRIMUNCHANGED-OCTON=T7657 &lt;*DISCRIMUNCHANGED-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*OS-OCTON=T6513 &lt;*OS-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*RNFLINFTEMP-OCTON=T7646 &lt;*RNFLINFTEMP-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*PROGRESSION-OCTON=T7652 &lt;*PROGRESSION-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'F', '', '/OCT, OPTIC NERVE', '*DISCRIMTHINNED-OCTON=T7656 &lt;*DISCRIMTHINNED-OCTON&gt;', '', '', 'A', '', 'T', '', '', 0, '', ''
FROM PatientDemographics 
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'




--FINANCIAL ROWS
INSERT INTO [PatientReceivables]
           ([OpenBalance],[ReceivableType],[PatientId],[InsuredId],[InsurerId],[AppointmentId],[Invoice],[InvoiceDate],[Charge],[BillToDr],[AccType],[AccState],[HspAdmDate],[HspDisDate],[Disability],[RsnType],[RsnDate],[FirstConsDate],[PrevCond],[Status],[ReferDr],[UnallocatedBalance],[ExternalRefInfo],[Over90],[BillingOffice],[PatientBillDate],[LastPayDate],[ChargesByFee],[InsurerDesignation],[WriteOff])
Select  '6385.00', 'I', pd.PatientId, pd.PatientId
, InsurerId
, AppointmentId
, dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
, convert(varchar(8),getdate(),112)
, '6385.00', ap.ResourceId1
, '' , '', '', '', '', '', '', '', '', 'A', 0 
, '6385.00', '', '', 0, '', '', '5940.82', 'T', ''
FROM PatientDemographics pd
INNER JOIN Appointments ap ON ap.PatientId = pd.PatientId
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'


INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'67028', 'RT', '1', '250.00', '04', '11', convert(varchar(8),getdate(),112)
           , '2', 'A', 'F', 'F', '156.39', '1', 0, convert(varchar(8),getdate(),112), 'INTRAVITREAL I'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'J3490', '', '5', '1050.00', '12', '11', convert(varchar(8),getdate(),112)
           , '1234', 'A', 'F', 'F', '5126.78', '2', 0, convert(varchar(8),getdate(),112), 'LUCENTIS'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'99214', '', '1', '225.00', '01', '11', convert(varchar(8),getdate(),112)
           , '1234', 'A', 'F', 'F', '132.56', '3', 0, convert(varchar(8),getdate(),112), 'OFFICE VISIT'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'92235', 'RT', '1', '170.00', '02', '11', convert(varchar(8),getdate(),112)
           , '2', 'A', 'F', 'F', '112.45', '4', 0, convert(varchar(8),getdate(),112), 'FLUORESCEIN'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'




INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'92235', 'LT', '1', '170.00', '02', '11', convert(varchar(8),getdate(),112)
           , '2', 'A', 'F', 'F', '112.45', '5', 0, convert(varchar(8),getdate(),112), 'FLUORESCEIN'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'92133', '', '1', '120.00', '02', '11', convert(varchar(8),getdate(),112)
           , '1', 'A', 'F', 'F', '123.22', '6', 0, convert(varchar(8),getdate(),112), 'OCT'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'92250', '', '1', '110.00', '02', '11', convert(varchar(8),getdate(),112)
           , '1', 'A', 'F', 'F', '98.16', '7', 0, convert(varchar(8),getdate(),112), 'FUNDUS PHOTOS'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'



INSERT INTO [PatientReceivableServices]
           ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) + '-' + dbo.PadLeft(AppointmentId,5)
           ,'92083', '', '1', '90.00', '02', '11', convert(varchar(8),getdate(),112)
           , '1', 'A', 'F', 'F', '78.81', '8', 0, convert(varchar(8),getdate(),112), 'VISUAL FIELD'
FROM PatientDemographics pd
INNER JOIN Appointments ap on ap.PatientId = pd.PatientId
WHERE LastName = 'TRACY' AND FirstName = 'SPENCER'

-- Mappings
INSERT INTO model.ExternalSystemEntityMappings
           ([PracticeRepositoryEntityId], [PracticeRepositoryEntityKey], [ExternalSystemEntityId], [ExternalSystemEntityKey])
SELECT pre.Id, CONVERT(nvarchar, IDENT_CURRENT('Appointments')), ese.Id, '12345'
FROM model.PracticeRepositoryEntities pre, model.ExternalSystemEntities ese 
JOIN model.ExternalSystems es on ese.ExternalSystemId = es.Id
WHERE pre.Name = 'Appointment'
AND es.Name = 'ATHENANET' AND ese.Name = 'Appointment'

INSERT INTO model.ExternalSystemEntityMappings
           ([PracticeRepositoryEntityId], [PracticeRepositoryEntityKey], [ExternalSystemEntityId], [ExternalSystemEntityKey])
SELECT TOP 1 pre.Id, CONVERT(nvarchar, PatientId), ese.Id, '123456'
FROM model.PracticeRepositoryEntities pre, 
model.ExternalSystemEntities ese 
JOIN model.ExternalSystems es on ese.ExternalSystemId = es.Id
INNER JOIN PatientDemographics pd ON LastName = 'TRACY' AND FirstName = 'SPENCER'
WHERE pre.Name = 'Patient'
AND es.Name = 'ATHENANET' AND ese.Name = 'Patient'

INSERT INTO model.ExternalSystemEntityMappings
         ([PracticeRepositoryEntityId], [PracticeRepositoryEntityKey], [ExternalSystemEntityId], [ExternalSystemEntityKey])
SELECT pre.Id, CONVERT(nvarchar, re.ResourceId), ese.Id, '1234567'
FROM model.PracticeRepositoryEntities pre, model.ExternalSystemEntities ese 
JOIN model.ExternalSystems es on ese.ExternalSystemId = es.Id
INNER JOIN dbo.Resources re ON ResourceLastName = 'REISS' AND ResourceFirstName = 'GEORGE'
WHERE pre.Name = 'User'
AND es.Name = 'ATHENANET' AND ese.Name = 'AttendingDoctor'

