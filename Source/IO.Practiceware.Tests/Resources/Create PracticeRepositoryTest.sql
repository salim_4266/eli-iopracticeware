EXEC('SET NOCOUNT ON')
EXEC('
SET ANSI_NULLS ON ')


USE [master]

--------------------------------------PracticeRepositoryTest
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'PracticeRepositoryTest')
BEGIN
ALTER DATABASE [PracticeRepositoryTest] SET OFFLINE WITH ROLLBACK IMMEDIATE
ALTER DATABASE [PracticeRepositoryTest] SET ONLINE
DROP DATABASE [PracticeRepositoryTest]
END

CREATE DATABASE [PracticeRepositoryTest]
GO

ALTER DATABASE [PracticeRepositoryTest] SET RECOVERY SIMPLE
GO

USE [PracticeRepositoryTest]
EXEC('
/****** Object:  Table [dbo].[HealthplanDetail]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

IF SCHEMA_ID(''fdb'') IS NULL EXEC(''CREATE SCHEMA fdb'')')
EXEC('

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[HealthplanDetail]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[HealthplanDetail](
	[HealthplanDetailID] [int] NOT NULL,
	[Name] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Address1] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address2] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Zip] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip4] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FormularyType] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrganizationID] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FormularyID] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[HealthplanSummary]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[HealthplanSummary]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[HealthplanSummary](
	[HealthplanSummaryID] [int] NOT NULL,
	[ConsolidatedName] [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HealthplanDetailID] [int] NULL,
	[State] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Country] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SummaryCount] [bit] NOT NULL,
	[Source] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[RFMLINM0]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[RFMLINM0]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[RFMLINM0](
	[ICD9CM] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ICD9CM_HCFADESC] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ICD9CM_DESC100] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ICD9CM_TYPE_CODE] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ICD9CM_SOURCE_CODE] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[tblCompositeAllergy]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[tblCompositeAllergy]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[tblCompositeAllergy](
	[CompositeAllergyID] [int] NOT NULL,
	[Source] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ConceptID] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ConceptType] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TouchDate] [datetime] NOT NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[tblCompositeDrug]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[tblCompositeDrug]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[tblCompositeDrug](
	[MEDID] [int] NOT NULL,
	[GCN_SEQNO] [int] NOT NULL,
	[MED_NAME_ID] [int] NOT NULL,
	[MED_NAME] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_ROUTED_MED_ID_DESC] [nvarchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_ROUTED_DF_MED_ID_DESC] [nvarchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_MEDID_DESC] [nvarchar](70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_STATUS_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_ROUTE_ID] [int] NOT NULL,
	[ROUTED_MED_ID] [int] NOT NULL,
	[ROUTED_DOSAGE_FORM_MED_ID] [int] NOT NULL,
	[MED_STRENGTH] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_STRENGTH_UOM] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_ROUTE_ABBR] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_ROUTE_DESC] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_DOSAGE_FORM_ABBR] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_DOSAGE_FORM_DESC] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GenericDrugName] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DosageFormOverride] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_REF_DEA_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_REF_DEA_CD_DESC] [nvarchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_REF_MULTI_SOURCE_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_REF_MULTI_SOURCE_CD_DESC] [nvarchar](90) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_REF_GEN_DRUG_NAME_CD_DESC] [nvarchar](90) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MED_REF_FED_LEGEND_IND] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_REF_FED_LEGEND_IND_DESC] [nvarchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GENERIC_MEDID] [int] NULL,
	[MED_NAME_TYPE_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GENERIC_MED_REF_GEN_DRUG_NAME_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MED_NAME_SOURCE_CD] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[etc] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DrugInfo] [nvarchar](70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GenericDrugNameOverride] [nvarchar](70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FormularyDrugID] [int] NULL,
	[Manufacturer] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TouchDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DrugTypeID] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[tblPharmacy]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[tblPharmacy]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[tblPharmacy](
	[PharmacyGuid] [uniqueidentifier] NOT NULL,
	[Pharmacy_ID] [int] NULL,
	[Pharmacy_Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_NCPDP] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Pharmacy_Store] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_StoreName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_Address1] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_Address2] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_Telephone1] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_Telephone2] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_Fax] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_PhoneAlt1] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_PhoneAlt2] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_PhoneAlt3] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_PhoneAlt4] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_PhoneAlt5] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_City] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_StateID] [int] NULL,
	[Pharmacy_Zip] [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_TouchDate] [datetime] NULL,
	[Pharmacy_StateAbbr] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_TypeID] [int] NULL,
	[Pharmacy_DetailTypeID] [bit] NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[viewBlackBoxFDB]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[viewBlackBoxFDB]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[viewBlackBoxFDB](
	[GCN_SEQNO] [int] NOT NULL
) ON [PRIMARY]
END')
EXEC('
/****** Object:  Table [dbo].[viewWSDrug]    Script Date: 10/3/2012 4:15:07 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[fdb].[viewWSDrug]'') AND type in (N''U''))
BEGIN
CREATE TABLE [fdb].[viewWSDrug](
	[dataProvider] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[drug] [nvarchar](70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DrugID] [int] NOT NULL,
	[DrugSubID1] [int] NULL,
	[DrugName] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DrugNameID] [int] NULL,
	[GenericName] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DeaClassCode] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[dosage] [nvarchar](31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DosageForm] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Route] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TheraputicCategory] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TouchDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END')
EXEC('


/****** Object:  StoredProcedure [dbo].[CheckOut]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[CheckOut] 
@AptDate varchar(8),
@LocId Int,
@ResId Int,
@MType Int
AS
SELECT DISTINCT Appointments.AppointmentId, Appointments.AppDate,
     Appointments.AppTime, Appointments.ScheduleStatus,
     Appointments.TechApptTypeId, Appointments.ApptInsType,
     PatientDemographics.Salutation, PatientDemographics.LastName,
     PatientDemographics.FirstName, PatientDemographics.MiddleInitial,
     PatientDemographics.NameReference, PatientDemographics.BirthDate,
     PatientDemographics.PatientId, PatientDemographics.HomePhone,
     PatientDemographics.BusinessPhone, PracticeActivity.ActivityId,
     PatientDemographics.PolicyPatientId, PracticeActivity.Status,
     Appointments.ResourceId2, AppointmentType.AppointmentType,
     Resources.ResourceName
FROM Resources
INNER JOIN (PatientDemographics
INNER JOIN (AppointmentType
RIGHT JOIN (Appointments
INNER JOIN PracticeActivity
ON Appointments.AppointmentId = PracticeActivity.AppointmentId)
ON AppointmentType.AppTypeId = Appointments.AppTypeId)
ON PatientDemographics.PatientId = Appointments.PatientId)
ON Resources.ResourceId = Appointments.ResourceId1
WHERE ((Appointments.AppDate = @AptDate) OR ((Appointments.AppDate >= @AptDate) And (@MType = 1))) AND
((PracticeActivity.Status = ''H'') OR (PracticeActivity.Status = ''G'')) AND
((Appointments.ResourceId1 = @ResId) OR (Appointments.TechApptTypeId = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY Appointments.AppDate, Appointments.AppTime
')
EXEC('
/****** Object:  StoredProcedure [dbo].[ClaimsList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[ClaimsList] 
@AptDate varchar(8),
@RscId Int,
@LocId Int,
@PatId Int,
@RType varchar(1),
@BalOn Int
AS
SELECT DISTINCT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime,
	PatientDemographics.PatientId, PatientDemographics.LastName, PatientDemographics.FirstName,
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, PatientDemographics.Salutation,
	PatientDemographics.BirthDate, Appointments.ScheduleStatus, Resources.ResourceName,
	Resources.ResourceId, PatientReceivables.ReceivableId, PatientReceivables.ReceivableType,
	PatientReceivables.Charge, PatientReceivables.OpenBalance, PatientReceivables.UnallocatedBalance,
	PatientReceivables.BillToDr, PatientReceivables.Invoice, AppointmentType.AppointmentType,
	PracticeInsurers.InsurerName
FROM ((AppointmentType
RIGHT JOIN ((Resources
INNER JOIN (PatientDemographics
INNER JOIN Appointments
ON PatientDemographics.PatientId = Appointments.PatientId)
ON Resources.ResourceId = Appointments.ResourceId1)
INNER JOIN PatientReceivables
ON (Appointments.AppointmentId = PatientReceivables.AppointmentId)
AND (PatientDemographics.PatientId = PatientReceivables.PatientId))
ON AppointmentType.AppTypeId = Appointments.AppTypeId)
LEFT JOIN Resources AS Resources_1
ON Appointments.ResourceId2 = Resources_1.ResourceId)
LEFT JOIN PracticeInsurers
ON PatientReceivables.InsurerId = PracticeInsurers.InsurerId
WHERE ((PatientDemographics.PatientId = @PatId) OR (@PatId = -1))
AND ((Appointments.AppDate <= @AptDate))
AND ((Appointments.ResourceId1 = @RscId) OR (@RscId = -1))
AND ((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
AND ((PatientReceivables.ReceivableType = @RType) OR (@RType = ''0''))
AND ((PatientReceivables.UnallocatedBalance > 0) OR (@BalOn = -1))
AND ((PatientReceivables.OpenBalance > 0) OR (@BalOn = -1))
ORDER BY Appointments.AppDate ASC, Appointments.AppTime ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[ClaimsListReverse]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[ClaimsListReverse]
@AptDate varchar(8),
@RscId Int,
@LocId Int,
@RType varchar(1)
AS
SELECT DISTINCT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime,
	PatientDemographics.PatientId, PatientDemographics.LastName, PatientDemographics.FirstName,
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, PatientDemographics.Salutation,
	PatientDemographics.BirthDate, Appointments.ScheduleStatus, Resources.ResourceName,
	Resources.ResourceId, PatientReceivables.ReceivableId, PatientReceivables.ReceivableType,
	PatientReceivables.Charge, PatientReceivables.OpenBalance, PatientReceivables.UnallocatedBalance,
	PatientReceivables.BillToDr, PatientReceivables.Invoice, AppointmentType.AppointmentType,
	PracticeInsurers.InsurerName
FROM PatientReceivables
INNER JOIN Appointments
ON PatientReceivables.AppointmentId = Appointments.AppointmentId
INNER JOIN PatientDemographics
ON PatientDemographics.PatientId = Appointments.PatientId
LEFT JOIN Resources
ON Resources.ResourceId = Appointments.ResourceId1
LEFT JOIN Resources AS Resources_1
ON Appointments.ResourceId2 = Resources_1.ResourceId
LEFT JOIN PracticeInsurers
ON PatientReceivables.InsurerId = PracticeInsurers.InsurerId
LEFT JOIN AppointmentType
ON AppointmentType.AppTypeId = Appointments.AppTypeId
WHERE ((Appointments.AppDate <= @AptDate))
AND ((Appointments.ResourceId1 = @RscId) OR (@RscId = -1))
AND ((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
AND ((PatientReceivables.ReceivableType = @RType) OR (@RType = ''0''))
AND ((PatientReceivables.UnallocatedBalance > 0))
AND ((PatientReceivables.OpenBalance > 0))
ORDER BY Appointments.AppDate ASC, Appointments.AppTime ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[DailyPayments]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

Create procedure [dbo].[DailyPayments]
as

select
       prp.PaymentRefType, prp.paymenttype, prp.payertype, prp.paymentamount, prp.paymentdate, prp.paymentassignby, prp.paymentcheck,
       pd.firstname, pd.middleinitial, pd.lastname, pd.patientid,
       pi.insurername,
       isnull(reasg.resourcename, ''ERA'') as AsgResourceName,
       isnull(pn.practicename, ''none'') as practicename, isnull(pn.practicecity, ''none'') as practicecity, isnull(pn.practiceaddress, ''none'') as practiceaddress, isnull(pn.practicestate, ''none'') as practicestate, isnull(pn.practicezip, ''none'') as practicezip,
       isnull(re.resourcename, ''none'') as resoursename, isnull(re.resourcedescription, ''none'') as resourcedescription,
       pr.billingoffice, pr.billtodr
from patientreceivablepayments prp
       inner join patientreceivables pr on prp.receivableid = pr.receivableid
       inner join patientdemographics pd on pd.patientid = pr.patientid
       left outer join practiceinsurers pi on pi.insurerid = prp.payerid
       left outer join resources reasg on reasg.resourceid = prp.paymentassignby
       left outer join resources re on re.resourceid = pr.billtodr
       left outer join practicename pn on pn.practiceid = re.practiceid
where
       paymenttype = ''p''
order by paymentdate, paymentassignby')
EXEC('
/****** Object:  StoredProcedure [dbo].[DI_ApptDrList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[DI_ApptDrList]
@AptDate varchar(8),
@PatId int
AS
SET NOCOUNT ON
SELECT Appointments.AppointmentId, 
	 Appointments.AppDate, 
	 PatientDemographics.LastName,
	 PatientDemographics.FirstName,
	 PatientDemographics.MiddleInitial,
	 Resources.ResourceName,
	 PracticeVendors.VendorFirstName + '' '' + PracticeVendors.VendorLastName RefName,
	 PracticeVendors.VendorId RefDr,
	 Appointments.ResourceId8
FROM	Appointments 
INNER JOIN PracticeActivity
ON PracticeActivity.AppointmentId = Appointments.AppointmentId
INNER JOIN PatientDemographics
ON PatientDemographics.PatientId = Appointments.PatientId
INNER JOIN Resources 
ON Resources.ResourceId = Appointments.ResourceId1 
LEFT JOIN PracticeVendors 
ON PracticeVendors.VendorId = PatientDemographics.ReferringPhysician 
WHERE	Appointments.AppDate <= @AptDate And
	Appointments.PatientId = @PatId And
	Appointments.Comments <> ''ADD VIA BILLING'' and
	(Appointments.ScheduleStatus = ''D'' Or 
	 PracticeActivity.Status = ''H'' Or 
	 PracticeActivity.Status = ''D'' Or
	 PracticeActivity.Status = ''G'')
ORDER BY Appointments.AppDate DESC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[DI_ImpressionsList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[DI_ImpressionsList]
@AptDate varchar (8),
@PatId Int
AS
SELECT PatientClinical.ClinicalId, PatientClinical.Symptom, 
	 PatientClinical.FindingDetail, PatientClinical.AppointmentId, 
	 PatientClinical.EyeContext, PatientClinical.PatientId, 
	 Appointments.AppDate, Appointments.AppointmentId
FROM Appointments 
INNER JOIN PatientClinical
ON PatientClinical.AppointmentId = Appointments.AppointmentId
Where PatientClinical.PatientId = @PatId And
Appointments.AppDate < @AptDate And
PatientClinical.ClinicalType = ''U'' And
PatientClinical.Status = ''A'' And
Appointments.ScheduleStatus = ''D''
ORDER BY Appointments.AppDate DESC, PatientClinical.Symptom ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[DI_LinkNotesList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[DI_LinkNotesList]
@AptDate varchar (8),
@PatId Int
AS
SELECT PatientClinical.ClinicalId, PatientClinical.Symptom, PatientNotes.NoteId, 
	 PatientClinical.FindingDetail, PatientClinical.AppointmentId, 
	 PatientClinical.PatientId, PatientNotes.NoteEye, 
	 PatientNotes.Note1, PatientNotes.Note2, 
	 PatientNotes.Note3, PatientNotes.Note4, PatientNotes.NoteILPNRef,
	 PatientNotes.NoteSystem, Appointments.AppDate
FROM PatientClinical 
INNER JOIN PatientNotes 
ON PatientClinical.AppointmentId = PatientNotes.AppointmentId
INNER JOIN Appointments
ON PatientClinical.AppointmentId = Appointments.AppointmentId
Where PatientClinical.PatientId = @PatId And
PatientClinical.Status = ''A'' And
PatientClinical.ClinicalType = ''U'' And
PatientNotes.NoteType = ''C'' And
(Left(PatientNotes.NoteSystem,5) = ''IMPR-'' or Left(PatientNotes.NoteSystem,5) = ''IMDN-'')
ORDER BY Appointments.AppDate DESC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[FinancialSummary]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[FinancialSummary]
@PatId Int,
@InvDate varchar(8),
@BalOn Int
AS
SELECT 
	PatientReceivables.InsurerDesignation, PatientReceivables.ReceivableId, PatientReceivables.Invoice, 
	PatientReceivables.InvoiceDate, PatientReceivables.Charge AS TotalCharge, 
	PatientReceivables.OpenBalance, PatientReceivables.UnallocatedBalance, 
	PracticeInsurers.InsurerName, Resources.ResourceName, PracticeInsurers.InsurerPlanName, 
	PatientReceivables.PatientId, PatientReceivables.InsuredId, PatientReceivables.InsurerId, 
	Appointments.AppointmentId, Appointments.ResourceId2, Resources_1.ResourceName AS R2, 
	PatientClinical.ClinicalType, PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.Status AS CStatus, PatientClinical.ClinicalId, PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, PatientReceivableServices.Quantity, 
	PatientReceivableServices.Charge, PatientReceivableServices.ItemId, 
	PatientReceivableServices.Status, PracticeServices.Description,
	PatientNotes.Note1 as ANote, PatientNotes_1.Note1 as SNote,
	PracticeName.PracticeName as LocName, PracticeName.LocationReference,
	Resources_2.ResourceName as AltLocName
FROM (((((PatientReceivables 
LEFT JOIN PracticeInsurers 
ON PatientReceivables.InsurerId = PracticeInsurers.InsurerId) 
LEFT JOIN Resources 
ON PatientReceivables.BillToDr = Resources.ResourceId) 
LEFT JOIN (Appointments 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId1 = Resources_1.ResourceId) 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
LEFT JOIN PatientClinical 
ON Appointments.AppointmentId = PatientClinical.AppointmentId) 
LEFT JOIN PracticeServices 
ON PatientReceivableServices.Service = PracticeServices.Code
LEFT JOIN PatientNotes 
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice = PatientNotes.NoteSystem))
LEFT JOIN PatientNotes As PatientNotes_1
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice+''C''+PatientReceivableServices.Service = PatientNotes_1.NoteSystem))
LEFT JOIN PracticeName
ON (Appointments.ResourceId2-1000) = PracticeName.PracticeId 
LEFT JOIN Resources as Resources_2
ON Appointments.ResourceId2 = Resources_2.ResourceId
WHERE ((PatientReceivables.PatientId = @PatId) OR (@PatId = -1))
AND ((PatientReceivables.InvoiceDate <= @InvDate) OR (@InvDate = ''-1'')) 
AND (((PatientReceivables.UnallocatedBalance <> 0) OR (@BalOn = -1)) 
OR ((PatientReceivables.OpenBalance <> 0) OR (@BalOn = -1)))
AND ((PatientClinical.ClinicalType in (''K'', ''B'')))
ORDER BY PatientReceivables.InvoiceDate DESC , PatientReceivables.Invoice DESC, PatientReceivables.InsurerDesignation DESC, 
PatientReceivables.ReceivableId, PatientClinical.Symptom ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[FinancialSummaryMedical]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[FinancialSummaryMedical]
@PatId Int,
@InvDate varchar(8),
@BalOn Int
AS
SELECT 
	PatientReceivables.InsurerDesignation, PatientReceivables.ReceivableId, PatientReceivables.Invoice, 
	PatientReceivables.InvoiceDate, PatientReceivables.Charge AS TotalCharge, 
	PatientReceivables.OpenBalance, PatientReceivables.UnallocatedBalance, 
	PracticeInsurers.InsurerName, Resources.ResourceName, PracticeInsurers.InsurerPlanName, 
	PatientReceivables.PatientId, PatientReceivables.InsuredId, PatientReceivables.InsurerId, 
	Appointments.AppointmentId, Appointments.ResourceId2, Resources_1.ResourceName AS R2, 
	PatientClinical.ClinicalType, PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.Status AS CStatus, PatientClinical.ClinicalId, PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, PatientReceivableServices.Quantity, 
	PatientReceivableServices.Charge, PatientReceivableServices.ItemId, 
	PatientReceivableServices.Status, PracticeServices.Description,
	PatientNotes.Note1 as ANote, PatientNotes_1.Note1 as SNote,
	PracticeName.PracticeName as LocName, PracticeName.LocationReference,
	Resources_2.ResourceName as AltLocName
FROM (((((PatientReceivables 
LEFT JOIN PracticeInsurers 
ON PatientReceivables.InsurerId = PracticeInsurers.InsurerId) 
LEFT JOIN Resources 
ON PatientReceivables.BillToDr = Resources.ResourceId) 
LEFT JOIN (Appointments 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId1 = Resources_1.ResourceId) 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
LEFT JOIN PatientClinical 
ON Appointments.AppointmentId = PatientClinical.AppointmentId) 
LEFT JOIN PracticeServices 
ON PatientReceivableServices.Service = PracticeServices.Code
LEFT JOIN PatientNotes 
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice = PatientNotes.NoteSystem))
LEFT JOIN PatientNotes As PatientNotes_1
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice+''C''+PatientReceivableServices.Service = PatientNotes_1.NoteSystem))
LEFT JOIN PracticeName
ON (Appointments.ResourceId2-1000) = PracticeName.PracticeId 
LEFT JOIN Resources as Resources_2
ON Appointments.ResourceId2 = Resources_2.ResourceId
WHERE ((PatientReceivables.PatientId = @PatId) OR (@PatId = -1))
AND ((PatientReceivables.InvoiceDate <= @InvDate) OR (@InvDate = ''-1'')) 
AND (((PatientReceivables.UnallocatedBalance <> 0) OR (@BalOn = -1)) 
OR ((PatientReceivables.OpenBalance <> 0) OR (@BalOn = -1)))
AND ((PatientClinical.ClinicalType in (''K'', ''B'')))
AND (Resources.ResourceType <> ''Q'')
ORDER BY PatientReceivables.InvoiceDate DESC , PatientReceivables.Invoice DESC, PatientReceivables.InsurerDesignation DESC, 
PatientReceivables.ReceivableId, PatientClinical.Symptom ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[FinancialSummaryOptical]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[FinancialSummaryOptical]
@PatId Int,
@InvDate varchar(8),
@BalOn Int
AS
SELECT 
	PatientReceivables.InsurerDesignation, PatientReceivables.ReceivableId, PatientReceivables.Invoice, 
	PatientReceivables.InvoiceDate, PatientReceivables.Charge AS TotalCharge, 
	PatientReceivables.OpenBalance, PatientReceivables.UnallocatedBalance, 
	PracticeInsurers.InsurerName, Resources.ResourceName, PracticeInsurers.InsurerPlanName, 
	PatientReceivables.PatientId, PatientReceivables.InsuredId, PatientReceivables.InsurerId, 
	Appointments.AppointmentId, Appointments.ResourceId2, Resources_1.ResourceName AS R2, 
	PatientClinical.ClinicalType, PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.Status AS CStatus, PatientClinical.ClinicalId, PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, PatientReceivableServices.Quantity, 
	PatientReceivableServices.Charge, PatientReceivableServices.ItemId, 
	PatientReceivableServices.Status, PracticeServices.Description,
	PatientNotes.Note1 as ANote, PatientNotes_1.Note1 as SNote,
	PracticeName.PracticeName as LocName, PracticeName.LocationReference,
	Resources_2.ResourceName as AltLocName
FROM (((((PatientReceivables 
LEFT JOIN PracticeInsurers 
ON PatientReceivables.InsurerId = PracticeInsurers.InsurerId) 
LEFT JOIN Resources 
ON PatientReceivables.BillToDr = Resources.ResourceId) 
LEFT JOIN (Appointments 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId1 = Resources_1.ResourceId) 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
LEFT JOIN PatientClinical 
ON Appointments.AppointmentId = PatientClinical.AppointmentId) 
LEFT JOIN PracticeServices 
ON PatientReceivableServices.Service = PracticeServices.Code
LEFT JOIN PatientNotes 
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice = PatientNotes.NoteSystem))
LEFT JOIN PatientNotes As PatientNotes_1
ON ((PatientReceivables.PatientId = PatientNotes.PatientId) And (''R''+PatientReceivables.Invoice+''C''+PatientReceivableServices.Service = PatientNotes_1.NoteSystem))
LEFT JOIN PracticeName
ON (Appointments.ResourceId2-1000) = PracticeName.PracticeId 
LEFT JOIN Resources as Resources_2
ON Appointments.ResourceId2 = Resources_2.ResourceId
WHERE ((PatientReceivables.PatientId = @PatId) OR (@PatId = -1))
AND ((PatientReceivables.InvoiceDate <= @InvDate) OR (@InvDate = ''-1'')) 
AND (((PatientReceivables.UnallocatedBalance <> 0) OR (@BalOn = -1)) 
OR ((PatientReceivables.OpenBalance <> 0) OR (@BalOn = -1)))
AND ((PatientClinical.ClinicalType in (''K'', ''B'')))
AND (Resources.ResourceType = ''Q'')
ORDER BY PatientReceivables.InvoiceDate DESC , PatientReceivables.Invoice DESC, PatientReceivables.InsurerDesignation DESC, 
PatientReceivables.ReceivableId, PatientClinical.Symptom ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[InsuranceAgingDetail]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec InsuranceAgingDetail

CREATE  Procedure [dbo].[InsuranceAgingDetail]

as

SET NOCOUNT ON

select GETDATE () as Today, GETDATE () -30 as Today_30, GETDATE () -60 as Today_60, GETDATE () -90 as Today_90, GETDATE () -120 as Today_120
into #DateTable

select 
cast(datepart(YYYY,Today) as nvarchar(4)) + case when len(cast(datepart(MM,Today) as nvarchar(2))) = 2 then cast(datepart(MM,Today) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today) as nvarchar(2)) end + case when len(cast(datepart(DD,Today) as nvarchar(2))) = 2 then cast(datepart(DD,Today) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today) as nvarchar(2)) end as IOToday,
cast(datepart(YYYY,Today_30) as nvarchar(4)) + case when len(cast(datepart(MM,Today_30) as nvarchar(2))) = 2 then cast(datepart(MM,Today_30) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_30) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_30) as nvarchar(2))) = 2 then cast(datepart(DD,Today_30) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_30) as nvarchar(2)) end as IOToday_30,
cast(datepart(YYYY,Today_60) as nvarchar(4)) + case when len(cast(datepart(MM,Today_60) as nvarchar(2))) = 2 then cast(datepart(MM,Today_60) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_60) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_60) as nvarchar(2))) = 2 then cast(datepart(DD,Today_60) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_60) as nvarchar(2)) end as IOToday_60,
cast(datepart(YYYY,Today_90) as nvarchar(4)) + case when len(cast(datepart(MM,Today_90) as nvarchar(2))) = 2 then cast(datepart(MM,Today_90) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_90) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_90) as nvarchar(2))) = 2 then cast(datepart(DD,Today_90) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_90) as nvarchar(2)) end as IOToday_90,
cast(datepart(YYYY,Today_120) as nvarchar(4)) + case when len(cast(datepart(MM,Today_120) as nvarchar(2))) = 2 then cast(datepart(MM,Today_120) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_120) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_120) as nvarchar(2))) = 2 then cast(datepart(DD,Today_120) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_120) as nvarchar(2)) end as IOToday_120
into #IODateTable
from #DateTable	

--drop table #datetable
--drop table #IOdatetable
--select * from #DateTable
--select * from #IODateTable


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service, prs.Charge, prs.Quantity,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount0_30,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket0_30
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId 
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''I'' and
	pr.InvoiceDate <= (
		select IOToday
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_30
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket0_30
--select * from #Bucket0_30


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount30_60,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket30_60
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''I'' and
	pr.InvoiceDate < (
		select IOToday_30
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_60
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket30_60
--select * from #Bucket30_60

select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount60_90,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket60_90
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''I'' and
	pr.InvoiceDate < (
		select IOToday_60
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_90
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket60_90
--select * from #Bucket60_90

select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount90_120,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket90_120
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''I'' and
	pr.InvoiceDate < (
		select IOToday_90
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_120
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket90_120
--select * from #Bucket90_120


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount120,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket_120
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''I'' and
	pr.InvoiceDate < (
		select IOToday_120
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket_120
--select * from #Bucket_120

CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, (practicename + '' '' + locationreference)as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, convert(nvarchar(10),''00_30'') as Bucket,
		Amount0_30,
		convert(Decimal(38,2),0) as Amount30_60,
		convert(Decimal(38,2),0) as Amount60_90,
		convert(Decimal(38,2),0) as Amount90_120,
		convert(Decimal(38,2),0) as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
into #FinalTable
from #Bucket0_30

--drop table #FinalTable
--select * from #FinalTable

insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''31_60'' as Bucket,
		0 as Amount0_30,
		Amount30_60,
		0 as Amount60_90,
		0 as Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket30_60


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''61_90'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		Amount60_90,
		0 as Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket60_90


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''91_120'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		0 as Amount60_90,
		Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket90_120


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''120_'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		0 as Amount60_90,
		0 as Amount90_120,
		Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket_120


update #FinalTable 
set  #FinalTable.LocationDescription = #Locations.LocationDescription
from #Locations ,#FinalTable
where #FinalTable.billingoffice = #Locations.LocationId


select * from #FinalTable
order by billtodr, billingoffice, insurername, insurerplanname, ordername, invoicedate, Service, description')
EXEC('
/****** Object:  StoredProcedure [dbo].[MonthlyPatientStatements]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[MonthlyPatientStatements]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int
AS
SELECT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.ResourceId2, Appointments.ScheduleStatus, PatientDemographics.PatientId, 
	PatientDemographics.Salutation, PatientDemographics.LastName, PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, PatientDemographics.BirthDate, 
	Resources.ResourceName, PracticeTransactionJournal.TransactionId, 
	PracticeTransactionJournal.TransactionType, PracticeTransactionJournal.TransactionStatus, 
	PracticeTransactionJournal.TransactionDate, PracticeTransactionJournal.TransactionRef, 
	PracticeTransactionJournal.TransactionRemark, PracticeTransactionJournal.TransactionBatch, 
	PracticeTransactionJournal.TransactionAction, Resources.ResourceName, 
	PracticeTransactionJournal.TransactionRcvrId
FROM ((((PatientDemographics 
INNER JOIN PatientReceivables 
ON PatientDemographics.PatientId = PatientReceivables.PatientId) 
INNER JOIN Appointments 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PracticeTransactionJournal 
ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId) 
INNER JOIN Resources 
ON Appointments.ResourceId1 = Resources.ResourceId) 
INNER JOIN PracticeName ON Resources.PracticeId = PracticeName.PracticeId
WHERE (PracticeTransactionJournal.TransactionType = ''R'') AND 
(PracticeTransactionJournal.TransactionStatus <> ''Z'') AND 
(PatientReceivables.OpenBalance > 0) AND 
(PatientDemographics.SendStatements <> ''N'') AND
(PracticeTransactionJournal.TransactionRef = ''P'') AND
((PracticeTransactionJournal.TransactionDate <= @SDate) OR (@SDate = ''0'')) AND
((PracticeTransactionJournal.TransactionDate >= @EDate) OR (@EDate = ''0'')) AND
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY PatientDemographics.LastName, PatientDemographics.FirstName
')
EXEC('
/****** Object:  StoredProcedure [dbo].[NewPendingClaims]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[NewPendingClaims] 
@SDate varchar(8),
@EDate varchar(8),
@RscId Int,
@LocId Int,
@RType varchar(1)
AS
SELECT 
	PatientReceivableServices.ServiceDate, PatientReceivables.PatientId, PatientReceivables.InsuredId, 
	Appointments.ResourceId2, Appointments.ResourceId1, Appointments.ReferralId, PatientReceivables.InsurerId, 
	PatientReceivableServices.LinkedDiag, PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, PatientReceivableServices.Quantity, 
	PatientReceivableServices.Charge, PatientReceivables.ReferDr, PatientReceivables.ReceivableId, 
	PatientReceivables.Invoice, PatientReceivables.InvoiceDate, PatientReceivableServices.ItemId, 
	Appointments.AppointmentId, PatientReceivableServices.Status, PracticeInsurers.InsurerName, 
	PracticeInsurers.InsurerAddress
FROM PracticeInsurers 
RIGHT JOIN (Appointments 
INNER JOIN (PatientReceivables 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
ON Appointments.AppointmentId = PatientReceivables.AppointmentId) 
ON PracticeInsurers.InsurerId = PatientReceivables.InsurerId
WHERE (Appointments.AppDate <= @SDate) AND
(Appointments.AppDate >= @EDate) AND
((Appointments.ResourceId1 = @RscId) OR (@RscId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) AND
((PatientReceivables.ReceivableType = @RType) OR (@RType = ''0''))
ORDER BY PatientReceivables.InvoiceDate, PatientReceivables.PatientId, Appointments.AppointmentId
')
EXEC('
/****** Object:  StoredProcedure [dbo].[NewPendingPayClaims]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[NewPendingPayClaims] 
@SDate varchar(8),
@EDate varchar(8),
@RscId Int,
@LocId Int,
@PType varchar(1)
AS
SELECT 
	PatientReceivableServices.ServiceDate, PatientReceivables.PatientId, PatientReceivables.InsuredId, 
	Appointments.ResourceId2, Appointments.ResourceId1, Appointments.ReferralId, PatientReceivables.InsurerId, 
	PatientReceivableServices.LinkedDiag, PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, PatientReceivableServices.Quantity, 
	PatientReceivables.InvoiceDate, PatientReceivableServices.Charge, PatientReceivables.ReferDr, 
	PatientReceivables.Invoice, PatientReceivableServices.ItemId, Appointments.AppointmentId, 
	PatientReceivableServices.Status, PracticeInsurers.InsurerName, PracticeInsurers.InsurerAddress, 
	PatientReceivablePayments.PaymentType, PatientReceivablePayments.PaymentDate, 
	PatientReceivablePayments.ReceivableId, PatientReceivablePayments.Status, 
	PatientReceivablePayments.PaymentId, PatientReceivablePayments.PaymentCommentOn, 
	PatientReceivablePayments.PaymentRefId, PatientReceivablePayments.PaymentReason
FROM PracticeInsurers 
RIGHT JOIN (((Appointments 
INNER JOIN PatientReceivables 
ON Appointments.AppointmentId = PatientReceivables.AppointmentId) 
INNER JOIN PatientReceivablePayments 
ON PatientReceivables.ReceivableId = PatientReceivablePayments.ReceivableId) 
LEFT JOIN PatientReceivableServices 
ON PatientReceivablePayments.PaymentServiceItem = PatientReceivableServices.ItemId) 
ON PracticeInsurers.InsurerId = PatientReceivables.InsurerId
WHERE (PatientReceivablePayments.Status <> ''X'') AND 
(PatientReceivablePayments.PaymentDate <= @SDate) AND 
(PatientReceivablePayments.PaymentDate >= @EDate) AND
((PatientReceivablePayments.PaymentType = @PType) OR (@PType = ''0'')) AND
((Appointments.ResourceId1 = @RscId) OR (@RscId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY PatientReceivables.InvoiceDate, PatientReceivables.PatientId, Appointments.AppointmentId
')
EXEC('
/****** Object:  StoredProcedure [dbo].[PatientAgingDetail]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec PatientAgingDetail

CREATE   Procedure [dbo].[PatientAgingDetail]

as

SET NOCOUNT ON

select GETDATE () as Today, GETDATE () -30 as Today_30, GETDATE () -60 as Today_60, GETDATE () -90 as Today_90, GETDATE () -120 as Today_120
into #DateTable

select 
cast(datepart(YYYY,Today) as nvarchar(4)) + case when len(cast(datepart(MM,Today) as nvarchar(2))) = 2 then cast(datepart(MM,Today) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today) as nvarchar(2)) end + case when len(cast(datepart(DD,Today) as nvarchar(2))) = 2 then cast(datepart(DD,Today) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today) as nvarchar(2)) end as IOToday,
cast(datepart(YYYY,Today_30) as nvarchar(4)) + case when len(cast(datepart(MM,Today_30) as nvarchar(2))) = 2 then cast(datepart(MM,Today_30) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_30) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_30) as nvarchar(2))) = 2 then cast(datepart(DD,Today_30) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_30) as nvarchar(2)) end as IOToday_30,
cast(datepart(YYYY,Today_60) as nvarchar(4)) + case when len(cast(datepart(MM,Today_60) as nvarchar(2))) = 2 then cast(datepart(MM,Today_60) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_60) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_60) as nvarchar(2))) = 2 then cast(datepart(DD,Today_60) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_60) as nvarchar(2)) end as IOToday_60,
cast(datepart(YYYY,Today_90) as nvarchar(4)) + case when len(cast(datepart(MM,Today_90) as nvarchar(2))) = 2 then cast(datepart(MM,Today_90) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_90) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_90) as nvarchar(2))) = 2 then cast(datepart(DD,Today_90) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_90) as nvarchar(2)) end as IOToday_90,
cast(datepart(YYYY,Today_120) as nvarchar(4)) + case when len(cast(datepart(MM,Today_120) as nvarchar(2))) = 2 then cast(datepart(MM,Today_120) as nvarchar(2)) else ''0'' + cast(datepart(MM,Today_120) as nvarchar(2)) end + case when len(cast(datepart(DD,Today_120) as nvarchar(2))) = 2 then cast(datepart(DD,Today_120) as nvarchar(2)) else ''0'' + cast(datepart(DD,Today_120) as nvarchar(2)) end as IOToday_120
into #IODateTable
from #DateTable	

--drop table #datetable
--drop table #IOdatetable
--select * from #DateTable
--select * from #IODateTable


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service, prs.Charge, prs.Quantity,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount0_30,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket0_30
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId 
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId and pr.InvoiceDate >= pf.FinancialStartDate and (pr.invoicedate <= pf.FinancialEndDate or pf.FinancialEndDate = '''')
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''P'' and
	pr.InvoiceDate <= (
		select IOToday
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_30
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket0_30
--select * from #Bucket0_30


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount30_60,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket30_60
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''P'' and
	pr.InvoiceDate < (
		select IOToday_30
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_60
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket30_60
--select * from #Bucket30_60

select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount60_90,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket60_90
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''P'' and
	pr.InvoiceDate < (
		select IOToday_60
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_90
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket60_90
--select * from #Bucket60_90

select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount90_120,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket90_120
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''P'' and
	pr.InvoiceDate < (
		select IOToday_90
		from #IODateTable
		) and
	pr.InvoiceDate >= (
		select IOToday_120
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket90_120
--select * from #Bucket90_120


select	distinct pr.BillingOffice, pr.BillToDr, pr.InvoiceDate, pr.InsurerDesignation,
		re.ResourceDescription,
		pi.InsurerName, pi.InsurerPlanName, pi.InsurerAddress, pi.InsurerCity, pi.InsurerState, pi.InsurerZip, pi.InsurerPhone,
		ptj.TransactionDate,
		pd.PatientId, pd.LastName, pd.FirstName, pd.BirthDate,
		pf.FinancialPerson, pf.FinancialGroupId,
		prs.ItemId, prs.service,
		ps.Description, ps.CodeCategory,
		st.Amount as Amount120,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip
into #Bucket_120
from PracticeTransactionJournal ptj
	left outer join ServiceTransactions st on st.TransactionId = ptj.TransactionId
	left outer join PatientReceivables pr on pr.ReceivableId = ptj.TransactionTypeId
	left outer join PatientReceivableServices prs on prs.ItemId = st.ServiceId
	left outer join Resources re on re.ResourceId = pr.BillToDr
	left outer join PracticeName pn on re.PracticeId = pn.PracticeId
	left outer join PracticeServices ps on ps.Code = prs.Service
	left outer join PracticeInsurers pi on pi.InsurerId = pr.InsurerId
	left outer join PatientDemographics pd on pd.PatientId = pr.PatientId
	left outer join PatientFinancial pf on pf.PatientId = pd.PatientId and pf.FinancialInsurerId = pr.InsurerId
where
	prs.itemid > 0 and
	st.Amount <> 0 and
	ptj.Transactiontype = ''R'' and
	ptj.Transactionstatus in (''P'', ''S'') and
	ptj.TransactionRef = ''P'' and
	pr.InvoiceDate < (
		select IOToday_120
		from #IODateTable
		)
order by pr.BillToDr, pr.BillingOffice, pi.InsurerName

--drop table #Bucket_120
--select * from #Bucket_120

CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, (practicename + '' '' + locationreference)as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, convert(nvarchar(10),''00_30'') as Bucket,
		Amount0_30,
		convert(Decimal(38,2),0) as Amount30_60,
		convert(Decimal(38,2),0) as Amount60_90,
		convert(Decimal(38,2),0) as Amount90_120,
		convert(Decimal(38,2),0) as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
into #FinalTable
from #Bucket0_30

--drop table #FinalTable
--select * from #FinalTable

insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''31_60'' as Bucket,
		0 as Amount0_30,
		Amount30_60,
		0 as Amount60_90,
		0 as Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket30_60


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''61_90'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		Amount60_90,
		0 as Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket60_90


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''91_120'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		0 as Amount60_90,
		Amount90_120,
		0 as Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket90_120


insert into #FinalTable
select BillingOffice, convert(nvarchar(250),0) as LocationDescription,
		BillToDr, ResourceDescription, InsurerDesignation,
		InsurerName, InsurerPlanName, InsurerAddress, InsurerCity, InsurerState, InsurerZip, InsurerPhone,
		TransactionDate, InvoiceDate,
		PatientId, LastName, FirstName, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, BirthDate,
		FinancialPerson, FinancialGroupId,
		ItemId, service, CodeCategory,
		Description, ''120_'' as Bucket,
		0 as Amount0_30,
		0 as Amount30_60,
		0 as Amount60_90,
		0 as Amount90_120,
		Amount120,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip
from #Bucket_120


update #FinalTable 
set  #FinalTable.LocationDescription = #Locations.LocationDescription
from #Locations ,#FinalTable
where #FinalTable.billingoffice = #Locations.LocationId


select * from #FinalTable
order by billtodr, billingoffice, insurername, insurerplanname, ordername, invoicedate, Service, description')
EXEC('
/****** Object:  StoredProcedure [dbo].[PatientDocumentList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[PatientDocumentList] 
@PatId Int
AS
SELECT	PracticeTransactionJournal.TransactionType,
 	Appointments.PatientId,
	PracticeTransactionJournal.TransactionId,
 	PracticeTransactionJournal.TransactionDate,
	AppointmentType.AppointmentType,
	Appointments.AppDate,
	PracticeTransactionJournal.TransactionRef,
 	PracticeTransactionJournal.TransactionRemark
FROM PracticeTransactionJournal
INNER JOIN (Appointments
LEFT JOIN AppointmentType
ON Appointments.AppTypeId = AppointmentType.AppTypeId)
ON PracticeTransactionJournal.TransactionTypeId = Appointments.AppointmentId
WHERE	Appointments.PatientId = @PatId And
	(PracticeTransactionJournal.TransactionType = ''S'' or
	 PracticeTransactionJournal.TransactionType = ''F'' or
	 PracticeTransactionJournal.TransactionType = ''Y'' or
	 PracticeTransactionJournal.TransactionType = ''X'')
ORDER BY PracticeTransactionJournal.TransactionDate ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[PatientLocator]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[PatientLocator] 
@AptDate varchar(8), 
@LocId Int, 
@ResId Int 
AS
SELECT DISTINCT Appointments.AppointmentId, Appointments.AppDate, 
    Appointments.AppTime, Appointments.ScheduleStatus, 
    Appointments.TechApptTypeId, Appointments.ApptInsType, 
    Appointments.ResourceId2, PatientDemographics.Salutation, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName, 
    PatientDemographics.MiddleInitial, 
    PatientDemographics.NameReference, 
    PatientDemographics.BirthDate, 
    PatientDemographics.PatientId, 
    PatientDemographics.HomePhone, 
    PatientDemographics.BusinessPhone, 
    PatientDemographics.PolicyPatientId, 
    PatientDemographics.PatType, 
    PatientDemographics.ReferralRequired,
    AppointmentType.AppointmentType, 
    Resources.ResourceName, Resources_1.ServiceCode, 
    Resources_1.ResourceName AS Resource2,
    PracticeActivity.Status
FROM ((Resources 
INNER JOIN (PatientDemographics 
INNER JOIN (AppointmentType 
RIGHT JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1) 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId2 = Resources_1.ResourceId) 
LEFT JOIN PracticeActivity 
ON Appointments.AppointmentId = PracticeActivity.AppointmentId
WHERE (Appointments.AppDate = @AptDate) AND 
    ((Appointments.ResourceId2 = @LocId) OR (@LocId = - 1)) AND
    ((Appointments.ResourceId1 = @ResId) OR (@ResId = - 1))
ORDER BY Appointments.AppDate, Appointments.AppTime
')
EXEC('
/****** Object:  StoredProcedure [dbo].[PatientStatements]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[PatientStatements]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int
AS
SELECT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.ResourceId2, Appointments.ScheduleStatus, PatientDemographics.PatientId, 
	PatientDemographics.Salutation, PatientDemographics.LastName, PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, PatientDemographics.BirthDate, 
	Resources.ResourceName, PracticeTransactionJournal.TransactionId, 
	PracticeTransactionJournal.TransactionType, PracticeTransactionJournal.TransactionStatus, 
	PracticeTransactionJournal.TransactionDate, PracticeTransactionJournal.TransactionRef, 
	PracticeTransactionJournal.TransactionRemark, PracticeTransactionJournal.TransactionBatch, 
	PracticeTransactionJournal.TransactionAction, Resources.ResourceName, 
	PracticeTransactionJournal.TransactionRcvrId
FROM ((((PatientDemographics 
INNER JOIN PatientReceivables 
ON PatientDemographics.PatientId = PatientReceivables.PatientId) 
INNER JOIN Appointments 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PracticeTransactionJournal 
ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId) 
INNER JOIN Resources 
ON Appointments.ResourceId1 = Resources.ResourceId) 
INNER JOIN PracticeName ON Resources.PracticeId = PracticeName.PracticeId
WHERE ((PracticeTransactionJournal.TransactionRef = ''P'')) AND 
((PracticeTransactionJournal.TransactionType = ''R'')) AND
((PracticeTransactionJournal.TransactionStatus = ''P'')) AND
((PracticeTransactionJournal.TransactionAction = ''P'')) AND
((PracticeTransactionJournal.TransactionDate <= @SDate)) AND
((PracticeTransactionJournal.TransactionDate >= @EDate) OR (@EDate = ''0'')) AND
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY PatientDemographics.LastName, PatientDemographics.FirstName
')
EXEC('
/****** Object:  StoredProcedure [dbo].[PrecertsList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[PrecertsList] 
@AptDate varchar(8),
@ResId Int
AS
SELECT
	PatientPreCerts.PreCert, PatientPreCerts.PreCertDate,
	PatientPreCerts.PreCertId, PatientDemographics.LastName,
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial,
	PatientDemographics.NameReference, PatientDemographics.Salutation,
	PatientDemographics.PatientId, PatientDemographics.BirthDate,
	Appointments.AppDate, Appointments.AppointmentId,
	Appointments.ScheduleStatus, AppointmentType.PcRequired,
	AppointmentType.AppointmentType, Appointments.AppTime,
	Resources.ResourceName
FROM (Resources
INNER JOIN ((PatientDemographics 
INNER JOIN (AppointmentType 
INNER JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
LEFT JOIN PatientPreCerts 
ON Appointments.PreCertId = PatientPreCerts.PreCertId) 
ON Resources.ResourceId = Appointments.ResourceId1) 
INNER JOIN (PracticeInsurers 
INNER JOIN PatientFinancial 
ON PracticeInsurers.InsurerId = PatientFinancial.FinancialInsurerId) 
ON PatientDemographics.PolicyPatientId = PatientFinancial.PatientId
WHERE ((Appointments.AppDate<=@AptDate) OR (@AptDate=''''))
AND ((Appointments.ScheduleStatus=''P'') OR (Appointments.ScheduleStatus=''R'') OR (Appointments.ScheduleStatus=''A''))
AND ((Appointments.ResourceId1=@ResId) OR (@ResId = -1))
AND (AppointmentType.PcRequired=''Y'')
AND (PatientFinancial.FinancialPIndicator=1)
AND (PracticeInsurers.PrecertRequired=''Y'')
ORDER BY Appointments.AppDate DESC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcCharges]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec ProcCharges ''20100931'',''20100901''

Create        Procedure [dbo].[ProcCharges]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.billingoffice, pr.billtodr, pr.invoice,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype,
		re.resourcedescription
into #Table1
from patientreceivableservices prs
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientreceivablepayments prp on prp.paymentserviceitem = prs.itemid
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join resources re on re.resourceid = pr.billtodr
where
prs.status = ''a''
and prs.servicedate between @enddate and @startdate
and pr.insurerdesignation = ''t''


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename + '' - '' + LocationReference as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate,
		billingoffice, billtodr, invoice,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,2),0) as InsPaid,
		convert(Decimal(38,2),0) as InsAdj,
		convert(Decimal(38,2),0) as AdjDebit,
		convert(Decimal(38,2),0) as PatPaid
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, billingoffice, billtodr, invoice, servicedate,
		resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid
into #InsPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''I''
Group by itemid

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid

--Patient Payment
select sum(paymentamount) as PatPaid, itemid
into #PatPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''P''
Group by itemid

update #Table2 
set  #Table2.PatPaid = #PatPaid.PatPaid
from #PatPaid ,#Table2
where #Table2.itemid = #PatPaid.itemid

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid
into #InsAdj 
from  #Table1 
where PaymentFinancialType = ''C'' And PaymentType <> ''P''
Group by itemid

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = ''D''
Group by itemid

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid




--drop table #FinalTable


select  lastname, (firstname + '' - '' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName,
		itemid, ps.codecategory, service, Modifier, ps.Description, quantity, sum(charge*quantity) as TotalCharge, #Table2.billingoffice, billtodr, invoice,
		servicedate, 
		resourcedescription, Locationdescription,
		InsPaid, InsAdj, AdjDebit, PatPaid, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip
into #FinalTable
from #Table2
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, ps.CodeCategory, service, Description, modifier, quantity, #Table2.billingoffice, billtodr, invoice,
		servicedate, resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, PatPaid,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip



select * from #FinalTable
order by ResourceDescription, LocationDescription, codecategory, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcChargesIns]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec ProcChargesIns ''20090131'',''20090101''

Create        Procedure [dbo].[ProcChargesIns]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.insurerid, pr.billingoffice, pr.billtodr, pr.invoice,
		pi.insurername,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip,
		--pi2.insurername as payername,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype, prp.payerid,
		re1.resourcedescription
into #Table1
from patientreceivableservices prs
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientreceivablepayments prp on prp.paymentserviceitem = prs.itemid
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join practiceinsurers pi on pi.insurerid = pr.insurerid
left outer join resources re1 on re1.resourceid = pr.billtodr
left outer join practicename pn on pn.practiceid = re1.practiceid
where
prs.status = ''a'' and
prs.servicedate between @enddate and @startdate
 and pr.insurerdesignation = ''t''
order by insurername, billtodr, lastname, service



select	prs.itemid,
		sum(prp.paymentamount) as OtherPay
into #OtherPayments
from patientreceivableservices prs
left outer join patientreceivablepayments prp on prp.paymentserviceitem = prs.itemid
left outer join patientreceivables pr on pr.ReceivableId = prp.ReceivableId
where
prs.status = ''a'' and
prs.servicedate between @enddate and @startdate
and pr.insurerdesignation <> ''t''
and PaymentType = ''P''
group by ItemId


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate,
		insurerid, billingoffice, billtodr, invoice,
		insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		--payername,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,8),0) as InsPaid,
		convert(Decimal(38,8),0) as InsAdj,
		convert(Decimal(38,8),0) as AdjDebit,
		convert(Decimal(38,8),0) as OtherPay
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, insurerid, billingoffice, billtodr, invoice, servicedate,
		insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		--payername,
		resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid, payerId
into #InsPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''I''
Group by itemid, payerId

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid and
#Table2.insurerid = #InsPaid.PayerId

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid, payerId
into #InsAdj 
from  #Table1 
where PaymentFinancialType = ''C'' And PaymentType <> ''P'' And PayerType= ''I''
Group by itemid, payerId

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid and
#Table2.insurerid = #InsAdj.PayerId



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid, payerId
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = ''D''
Group by itemid, payerId

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid and
#Table2.insurerid = #AdjustmentDebit.PayerId



--Other Payments
update #Table2 
set  #Table2.OtherPay = #OtherPayments.OtherPay
from #OtherPayments ,#Table2
where #Table2.itemid = #OtherPayments.itemid


--drop table #FinalTable


select  lastname, (firstname + '' - '' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, itemid, service + '' '' + Modifier as service, Description, modifier, quantity, sum(charge*quantity) as TotalCharge, insurerid, billingoffice, billtodr, invoice,
		servicedate, isnull(#table2.insurername,''NO INSURER'') as insurername,
		resourcedescription, Locationdescription,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		InsPaid, InsAdj, AdjDebit, OtherPay, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance
into #FinalTable
from #Table2
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, service, Description, modifier, quantity, insurerid, billingoffice, billtodr, invoice,
		servicedate, insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, OtherPay



select * from #FinalTable
order by insurername, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcPayments]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec ProcPayments ''20110931'',''20110901''

Create        Procedure [dbo].[ProcPayments]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.billingoffice, pr.billtodr, pr.invoice,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype,
		re.resourcedescription
into #Table1
from PatientReceivablePayments prp
left outer join PatientReceivableServices prs on prs.itemid = prp.PaymentServiceItem
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join resources re on re.resourceid = pr.billtodr
where
prs.status = ''a''
and prp.PaymentDate between @enddate and @startdate
and pr.insurerdesignation = ''t''


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename + '' - '' + LocationReference as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate,
		billingoffice, billtodr, invoice,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,2),0) as InsPaid,
		convert(Decimal(38,2),0) as InsAdj,
		convert(Decimal(38,2),0) as AdjDebit,
		convert(Decimal(38,2),0) as PatPaid
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, billingoffice, billtodr, invoice, servicedate,
		resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid
into #InsPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''I''
Group by itemid

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid

--Patient Payment
select sum(paymentamount) as PatPaid, itemid
into #PatPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''P''
Group by itemid

update #Table2 
set  #Table2.PatPaid = #PatPaid.PatPaid
from #PatPaid ,#Table2
where #Table2.itemid = #PatPaid.itemid

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid
into #InsAdj 
from  #Table1 
where PaymentFinancialType = ''C'' And PaymentType <> ''P''
Group by itemid

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = ''D''
Group by itemid

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid




--drop table #FinalTable


select  lastname, (firstname + '' - '' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName,
		itemid, ps.codecategory, service, Modifier, ps.Description, quantity, sum(charge*quantity) as TotalCharge, #Table2.billingoffice, billtodr, invoice,
		servicedate, 
		resourcedescription, Locationdescription,
		InsPaid, InsAdj, AdjDebit, PatPaid, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip
into #FinalTable
from #Table2
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, ps.CodeCategory, service, Description, modifier, quantity, #Table2.billingoffice, billtodr, invoice,
		servicedate, resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, PatPaid,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip



select * from #FinalTable
order by ResourceDescription, LocationDescription, codecategory, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcPaymentsIns]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('





--exec ProcPaymentsIns ''20090131'',''20090101''

Create         Procedure [dbo].[ProcPaymentsIns]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.service, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.insurerid, pr.billingoffice, pr.billtodr, pr.invoice,
		pi.insurername,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip,
		--pi2.insurername as payername,
		prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype, prp.payerid,
		re1.resourcedescription
into #Table1
from PatientReceivablePayments prp
left outer join PatientReceivableServices prs on prs.itemid = prp.PaymentServiceItem
left outer join patientreceivables pr on pr.invoice = prs.invoice
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join practiceinsurers pi on pi.insurerid = pr.insurerid
left outer join resources re1 on re1.resourceid = pr.billtodr
left outer join practicename pn on pn.practiceid = re1.practiceid
where
prs.status = ''a'' and
prp.PaymentDate between @enddate and @startdate
and pr.insurerdesignation = ''t''
order by insurername, billtodr, lastname, service



select	prs.itemid,
		sum(prp.paymentamount) as OtherPay
into #OtherPayments
from PatientReceivablePayments prp
left outer join PatientReceivableServices prs on prs.itemid = prp.PaymentServiceItem
left outer join patientreceivables pr on pr.ReceivableId = prp.ReceivableId
where
prs.status = ''a'' and
prp.PaymentDate between @enddate and @startdate
and pr.insurerdesignation <> ''t''
and PaymentType = ''P''
group by ItemId


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate,
		insurerid, billingoffice, billtodr, invoice,
		insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		--payername,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,2),0) as InsPaid,
		convert(Decimal(38,2),0) as InsAdj,
		convert(Decimal(38,2),0) as AdjDebit,
		convert(Decimal(38,2),0) as OtherPay
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, insurerid, billingoffice, billtodr, invoice, servicedate,
		insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		--payername,
		resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid, payerId
into #InsPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''I''
Group by itemid, payerId

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid and
#Table2.insurerid = #InsPaid.PayerId

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid, payerId
into #InsAdj 
from  #Table1 
where PaymentFinancialType = ''C'' And PaymentType <> ''P'' And PayerType= ''I''
Group by itemid, payerId

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid and
#Table2.insurerid = #InsAdj.PayerId



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid, payerId
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = ''D''
Group by itemid, payerId

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid and
#Table2.insurerid = #AdjustmentDebit.PayerId



--Other Payments
update #Table2 
set  #Table2.OtherPay = #OtherPayments.OtherPay
from #OtherPayments ,#Table2
where #Table2.itemid = #OtherPayments.itemid


--drop table #FinalTable


select  lastname, (firstname + '' - '' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName, itemid, service + '' '' + Modifier as service, Description, modifier, quantity, sum(charge*quantity) as TotalCharge, insurerid, billingoffice, billtodr, invoice,
		servicedate, isnull(#table2.insurername,''NO INSURER'') as insurername,
		resourcedescription, Locationdescription,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		InsPaid, InsAdj, AdjDebit, OtherPay, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance
into #FinalTable
from #Table2
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, service, Description, modifier, quantity, insurerid, billingoffice, billtodr, invoice,
		servicedate, insurername,
		practicename, practiceaddress, practicecity, practicestate, practicezip,
		resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, OtherPay



select * from #FinalTable
order by insurername, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcPaymentsmonth]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

--exec ProcPaymentsmonth ''20110631'',''20110130''
Create     Procedure [dbo].[ProcPaymentsmonth]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select	distinct pd.lastname, pd.firstname, pd.patientid,
		prs.itemid, prs.modifier, prs.charge, prs.quantity, prs.servicedate,
		pr.billingoffice, pr.billtodr, pr.invoice,
		case when prp.paymentservice in (''0'', '''') then ''UNASSIGNED'' else paymentservice end as service,
		prp.paymentid, prp.paymentamount, prp.paymenttype, prp.payertype, prp.paymentfinancialtype, prp.paymentdate,
		re.resourcedescription
into #Table1
from PatientReceivablePayments prp
left outer join PatientReceivableServices prs on prs.itemid = prp.PaymentServiceItem
left outer join patientreceivables pr on pr.ReceivableId = prp.ReceivableId
left outer join patientdemographics pd on pd.patientid = pr.patientid
left outer join resources re on re.resourceid = pr.billtodr
where
--prs.status = ''a'' and 
prp.PaymentDate between @enddate and @startdate


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, practicename + '' - '' + LocationReference as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02
and resourceid > 0
and resourceid < 100

Insert into #Locations
select 0 as LocationId, practicename as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

--select * from #Locations
--drop table #Locations


select	distinct lastname, firstname, patientid,
		itemid, service, modifier, quantity, charge, servicedate, paymentdate,
		billingoffice, billtodr, invoice,
		resourcedescription,
		convert(nvarchar(250),0) as LocationDescription,
		convert(Decimal(38,2),0) as InsPaid,
		convert(Decimal(38,2),0) as InsAdj,
		convert(Decimal(38,2),0) as AdjDebit,
		convert(Decimal(38,2),0) as PatPaid
into #Table2
from #Table1
group by lastname, firstname, patientid, itemid, service, modifier, quantity, charge, billingoffice, billtodr, 
		invoice, servicedate, paymentdate, resourcedescription


update #Table2 
set  #Table2.LocationDescription = #Locations.LocationDescription
from #Locations ,#Table2
where #Table2.billingoffice = #Locations.LocationId

--select * from #Table2

--drop table #Table1
--drop table #Table2

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, itemid
into #InsPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''I''
Group by itemid

update #Table2 
set  #Table2.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Table2
where #Table2.itemid = #InsPaid.itemid

--Patient Payment
select sum(paymentamount) as PatPaid, itemid
into #PatPaid 
from  #Table1 
where PaymentType = ''P'' and PayerType = ''P''
Group by itemid

update #Table2 
set  #Table2.PatPaid = #PatPaid.PatPaid
from #PatPaid ,#Table2
where #Table2.itemid = #PatPaid.itemid

--drop table #InsAdj

--Insurance Adjustments
select sum(paymentamount) as InsAdj,itemid
into #InsAdj 
from  #Table1 
where PaymentFinancialType = ''C'' And PaymentType <> ''P''
Group by itemid

update #Table2 
set  #Table2.InsAdj = #InsAdj.InsAdj
from #InsAdj ,#Table2
where #Table2.itemid = #InsAdj.itemid



--drop table #AdjustmentDebit

---Adjustments for Debit
select sum(paymentamount) as AdjDebit,itemid
into #AdjustmentDebit 
from  #Table1
where PaymentFinancialType = ''D''
Group by itemid

update #Table2 
set  #Table2.AdjDebit = #AdjustmentDebit.AdjDebit
from #AdjustmentDebit ,#Table2
where #Table2.itemid = #AdjustmentDebit.itemid




--drop table #FinalTable


select  lastname, (firstname + '' - '' + convert(nvarchar,PatientId)) as firstname , patientid, (LastName + FirstName + convert(nvarchar,PatientId)) as OrderName,
		itemid, ps.codecategory, service, Modifier, ps.Description, quantity, sum(charge*quantity) as TotalCharge, #Table2.billingoffice, billtodr, invoice,
		servicedate, paymentdate, left(paymentdate, 6) as paymentmonth,
		resourcedescription, Locationdescription,
		InsPaid, InsAdj, AdjDebit, PatPaid, sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance,
		InsPaid + PatPaid as TotalPaid,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip
into #FinalTable
from #Table2
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
left outer join PracticeServices ps on ps.Code = #Table2.service
group by lastname, firstname, patientid, itemid, ps.CodeCategory, service, Description, modifier, quantity, #Table2.billingoffice, billtodr, invoice,
		servicedate, paymentdate, resourcedescription, locationdescription, InsPaid, InsAdj, AdjDebit, PatPaid,
		pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip


--Unassigned Payments No BillToDr

Insert into #FinalTable
select	pd.lastname, (pd.firstname + '' - '' + convert(nvarchar,pd.PatientId)) as firstname , pd.patientid,
		(pd.LastName + pd.FirstName + convert(nvarchar,pd.PatientId)) as OrderName,
		0, --itemid, 
		''UNASSIGNED PAYMENTS'', --ps.codecategory, 
		''None'', --service, 
		'''', --Modifier, 
		'''',--ps.Description, 
		0, --quantity, 
		0, --sum(charge*quantity) as TotalCharge, 
		0, --#Table2.billingoffice, 
		pd.SchedulePrimaryDoctor, -- billtodr, 
		''None'',
		prp.paymentdate, 
		prp.paymentdate,
		left(prp.paymentdate, 6),
		re.resourcedescription, 
		lo.Locationdescription,
		0, --InsPaid,
		0, --InsAdj,
		0, --AdjDebit,
		prp.paymentamount, --PatPaid,
		0 - prp.paymentamount, -- sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance,
		prp.paymentamount,
		pn.practicename,
		pn.practiceaddress,
		pn.practicecity,
		pn.practicestate,
		pn.practicezip
From patientreceivablepayments prp
left outer join PatientDemographics pd on pd.PatientId = prp.PayerId
left outer join #Locations lo on lo.LocationId = 0
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
left outer join Resources re on re.ResourceId = pd.SchedulePrimaryDoctor
left outer join PatientReceivables pr on pr.ReceivableId = prp.ReceivableId
where prp.PaymentDate between @EndDate and @StartDate
and PaymentType = ''P''
and PayerType = ''P''
and PaymentServiceItem = 0
and PaymentAmount <> 0
and BillToDr = 0



--Unassigned Payments with BillToDr

Insert into #FinalTable
select	pd.lastname, (pd.firstname + '' - '' + convert(nvarchar,pd.PatientId)) as firstname , pd.patientid,
		(pd.LastName + pd.FirstName + convert(nvarchar,pd.PatientId)) as OrderName,
		0, --itemid, 
		''UNASSIGNED PAYMENTS'', --ps.codecategory, 
		''None'', --service, 
		'''', --Modifier, 
		'''',--ps.Description, 
		0, --quantity, 
		0, --sum(charge*quantity) as TotalCharge, 
		0, --#Table2.billingoffice, 
		pr.BillToDr, 
		''None'',
		prp.paymentdate, 
		prp.paymentdate,
		left(prp.paymentdate, 6),
		re.resourcedescription, 
		lo.Locationdescription,
		0, --InsPaid,
		0, --InsAdj,
		0, --AdjDebit,
		prp.paymentamount, --PatPaid,
		0 - prp.paymentamount, -- sum((charge*quantity) - (InsPaid + InsAdj - AdjDebit)) as Balance,
		prp.paymentamount,
		pn.practicename,
		pn.practiceaddress,
		pn.practicecity,
		pn.practicestate,
		pn.practicezip
From patientreceivablepayments prp
left outer join PatientDemographics pd on pd.PatientId = prp.PayerId
left outer join #Locations lo on lo.LocationId = 0
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
left outer join PatientReceivables pr on pr.ReceivableId = prp.ReceivableId
left outer join Resources re on re.ResourceId = pr.BillToDr
where prp.PaymentDate between @EndDate and @StartDate
and PaymentType = ''P''
and PayerType = ''P''
and PaymentServiceItem = 0
and PaymentAmount <> 0
and BillToDr <> 0


CREATE TABLE #Months
(
	Number nvarchar(10), 
	MonthDescription nvarchar(250)
)

Insert into #Months
select ''01'', ''January'' 
Insert into #Months
select ''02'', ''February'' 
Insert into #Months
select ''03'', ''March'' 
Insert into #Months
select ''04'', ''April'' 
Insert into #Months
select ''05'', ''May'' 
Insert into #Months
select ''06'', ''June'' 
Insert into #Months
select ''07'', ''July'' 
Insert into #Months
select ''08'', ''August'' 
Insert into #Months
select ''09'', ''September'' 
Insert into #Months
select ''10'', ''October'' 
Insert into #Months
select ''11'', ''November'' 
Insert into #Months
select ''12'', ''December'' 

select * from #FinalTable 
left outer join #Months on #Months.Number = right(#FinalTable.paymentmonth, 2)
order by ResourceDescription, LocationDescription, codecategory, service

Drop table #Table2

Drop table #Table1

SET NOCOUNT OFF



')
EXEC('
/****** Object:  StoredProcedure [dbo].[ProcTypeColl]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



--exec ProcTypeColl ''20110431'',''20110401''
Create     Procedure [dbo].[ProcTypeColl]
			(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

as

SET NOCOUNT ON

select prp.paymentamount, prp.payerid, prp.payertype, prp.paymenttype, prp.paymentfinancialtype,
	prp.paymentdate, case when prp.paymentservice in (''0'', '''') then ''UNASSIGNED'' else paymentservice end as paymentservice,
	isnull(billtodr, 9999) as billtodr, ISNULL(billingoffice, 9999) as billingoffice
into #PaymentTable
from patientreceivablepayments prp
left outer join patientreceivableservices prs on prs.itemid = prp.paymentserviceitem
left outer join patientreceivables pr on pr.receivableid = prp.receivableid
where prp.paymentdate between @Enddate and @StartDate
--and prs.Status <> ''x'' 
and PaymentType not in (''='', '''')
order by prp.paymentservice

--drop table #paymenttable
--drop table #servicetable
--select * from #paymenttable

--select * from #tempservice
--drop table #tempservice
--drop table #charges

select distinct itemid, prs.quantity, prs.charge, prs.service, pr.billtodr, pr.billingoffice
into #ChargesTable
from  PatientReceivableServices prs
left outer join PatientReceivables pr on pr.Invoice = prs.Invoice
where prs.ServiceDate between @EndDate and @StartDate
and prs.Status <> ''X''
Group by itemid, service, prs.Charge, Quantity, billtodr, billingoffice


--select * from #charges
--drop table #temppayment
--drop table #charges

select *
into #Temp
from(
select distinct case when prp.paymentservice in (''0'', '''') then ''UNASSIGNED'' else prp.paymentservice end as cpt, 
		isnull(pr.billtodr, 9999) as billtodr, ISNULL(pr.billingoffice, 9999) as billingoffice
from PatientReceivablePayments prp
left outer join PatientReceivables pr on pr.ReceivableId = prp.ReceivableId
where
PaymentDate between @EndDate and @StartDate
and PaymentType not in (''='', '''')

union

select distinct prs.service as cpt, pr.billtodr, pr.billingoffice
from PatientReceivableServices prs
left outer join PatientReceivables pr on pr.Invoice = prs.Invoice
where
prs.ServiceDate between @EndDate and @StartDate
) A
order by cpt




select ps.codecategory, #Temp.cpt, isnull(#Temp.billtodr, 9999) as billtodr, ISNULL(#Temp.billingoffice, 9999) as billingoffice,
	convert(Decimal(38,8),0) as quantity,
	convert(Decimal(38,8),0) as charges,
	convert(Decimal(38,8),0) as InsPaid,
	convert(Decimal(38,8),0) as InsPaidNeg,
	convert(Decimal(38,8),0) as PatientPaid,
	convert(Decimal(38,8),0) as PatientPaidNeg,
	convert(Decimal(38,8),0) as Adj,
	convert(Decimal(38,8),0) as Refund
into #Master
From #Temp
left outer join practiceservices ps on ps.code = #Temp.cpt
group by codecategory, cpt, billtodr, billingoffice


--select * from #Master

--drop table #Master

--Charges
select sum(quantity * charge) as Charges, service, billtodr, billingoffice
into #Charges
from  #ChargesTable 
Group by service, billtodr, billingoffice

update #Master
set  #Master.Charges = #Charges.Charges
from #Charges, #Master
where #Master.cpt = #Charges.service
and #Master.billtodr = #Charges.billtodr
and #Master.billingoffice = #Charges.billingoffice


--Quantity
select sum(Quantity) as quantity, service, billtodr, billingoffice
into #Quantity
from  #ChargesTable 
Group by service, billtodr, billingoffice

update #Master
set  #Master.Quantity = #Quantity.Quantity
from #Quantity, #Master
where #Master.cpt = #Quantity.service
and #Master.billtodr = #Quantity.billtodr
and #Master.billingoffice = #Quantity.billingoffice

--drop table #InsPaid

--Insurance Payment
select sum(paymentamount) as InsPaid, paymentservice, billtodr, billingoffice
into #InsPaid 
from  #PaymentTable 
where PaymentType = ''P'' and PayerType = ''I''
Group by paymentservice, billtodr, billingoffice

update #Master
set  #Master.InsPaid = #InsPaid.InsPaid
from #InsPaid ,#Master
where #Master.cpt = #InsPaid.paymentservice
and #Master.billtodr = #InsPaid.billtodr
and #Master.billingoffice = #InsPaid.billingoffice


--select * from #master

--Insurance Payment Negative
--select sum(paymentamount) as InsPaidNeg, paymentservice, billtodr, billingoffice
--into #InsPaidNegative 
--from  #PaymentTable
--where PaymentFinancialType = ''D'' and Paymenttype in (''S'', ''U'', ''B'', ''1'') and payertype = ''I''
--Group by paymentservice, billtodr, billingoffice

--update #finaltable
--set  #finaltable.InsPaidNeg = #InsPaidNegative.InsPaidNeg
--from #InsPaidNegative, #finaltable
--where #finaltable.paymentservice = #InsPaidNegative.paymentservice
--and #finaltable.billtodr = #InsPaidNegative .billtodr
--and #finaltable.billingoffice = #InsPaidNegative .billingoffice


--drop table #PatientPaid

select sum(paymentamount) as PatientPaid, paymentservice, billtodr, billingoffice
into #PatientPaid
from  #PaymentTable 
where PaymentType = ''P'' and PayerType = ''P''
Group by paymentservice, billtodr, billingoffice

update #Master
set  #Master.PatientPaid = #PatientPaid.PatientPaid
from #PatientPaid ,#Master
where #Master.cpt = #PatientPaid.paymentservice
and #Master.billtodr = #PatientPaid.billtodr
and #Master.billingoffice = #PatientPaid.billingoffice


--Patient Payment Negative
--select sum(paymentamount) as PatientPaidNeg, paymentservice, billtodr, billingoffice
--into #PatientPaidNegative 
--from  #PaymentTable
--where PaymentFinancialType = ''D'' and Paymenttype in (''S'', ''U'', ''B'', ''1'') and payertype = ''P''
--or (PaymentFinancialType = ''D'' and Paymenttype = ''B'')
--Group by paymentservice, billtodr, billingoffice

--update #finaltable
--set  #finaltable.PatientPaidNeg = #PatientPaidNegative .PatientPaidNeg
--from #PatientPaidNegative  ,#finaltable
--where #finaltable.paymentservice = #PatientPaidNegative.paymentservice
--and #finaltable.billtodr = #PatientPaidNegative .billtodr
--and #finaltable.billingoffice = #PatientPaidNegative .billingoffice



--drop table #Adj

--Insurance Adjustments
select sum(paymentamount) as Adj, paymentservice, billtodr, billingoffice
into #Adj 
from  #PaymentTable
where PaymentFinancialType = ''C'' And PaymentType <> ''P''
Group by paymentservice, billtodr, billingoffice

update #Master
set  #Master.Adj = #Adj.Adj
from #Adj ,#Master
where #Master.cpt = #Adj.paymentservice
and #Master.billtodr = #Adj.billtodr
and #Master.billingoffice = #Adj.billingoffice


--drop table #Refund

---Adjustments for Debit
select sum(paymentamount) as Refund, paymentservice, billtodr, billingoffice
into #Refund 
from  #PaymentTable
where PaymentFinancialType = ''D''
Group by paymentservice, billtodr, billingoffice

update #Master
set  #Master.Refund = #Refund.Refund
from #Refund, #Master
where #Master.cpt = #Refund.paymentservice
and #Master.billtodr = #Refund.billtodr
and #Master.billingoffice = #Refund.billingoffice


--drop table #FinalTable


CREATE TABLE #Locations
(
	LocationId Int, 
	LocationDescription nvarchar(250)
)

Insert into #Locations
select practiceid + 1000 as LocationId, LocationReference as LocationDescription
from practicename 
where practicetype = ''P''
and practiceid > 1

Insert into #Locations
select resourceid as LocationId, resourcedescription as LocationDescription
from resources where resourcetype = ''R''
and servicecode = 02

Insert into #Locations
select 0 as LocationId, ''MAIN OFFICE'' as LocationDescription
from practicename
where LocationReference = ''''
and BillingOffice = ''T''

Insert into #Locations
select 9999 as LocationId, ''NO OFFICE'' as LocationDescription

--select * from #Locations
--drop table #Locations




select isnull(#Master.codecategory,''NO CATEGORY'') as codecategory, #Master.cpt, #Master.billtodr, #Master.billingoffice, #Master.quantity,
	#Master.Charges, isnull(re.resourcedescription, ''Z-OFFICE TRANSACTIONS'') as resourcedescription, lo.locationdescription, sum(InsPaid) as InsPaid, sum(PatientPaid) as PatientPaid, Adj, Refund,
	pn.practicename, pn.practiceaddress, pn.practicecity, pn.practicestate, pn.practicezip
into #FinalTable
from #Master
left outer join resources re on re.resourceid = #master.billtodr
left outer join #locations lo on lo.locationid = #master.billingoffice
left outer join PracticeName pn on pn.PracticeId > 0 and pn.LocationReference = '''' and pn.BillingOffice = ''T''
group by #Master.codecategory, #Master.cpt, #Master.quantity, #Master.Charges, #Master.billtodr, #Master.billingoffice,
	re.resourcedescription, lo.locationdescription,	InsPaid, PatientPaid, Adj, Refund,
	practicename, practiceaddress, practicecity, practicestate, practicezip



select * from #FinalTable
order by resourcedescription, locationdescription, codecategory, cpt



SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveClnTransactions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[RetrieveClnTransactions]
@SDate varchar(8),
@LocId Int,
@ResId Int,
@StaId varchar(1)
AS
SELECT
	Appointments.AppDate, Appointments.ResourceId1,
	Resources.ResourceName, Appointments.ResourceId2, 
	Appointments.AppointmentId, PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.HomePhone, PatientDemographics.PatientId, 
	PatientClinical.ClinicalId, PatientClinical.ClinicalType, 
	PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 
	PatientClinical.Status, PatientClinical.FollowUp
FROM Resources 
INNER JOIN (PatientDemographics 
INNER JOIN (Appointments 
INNER JOIN PatientClinical 
ON Appointments.AppointmentId = PatientClinical.AppointmentId) 
ON PatientDemographics.PatientId = PatientClinical.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1
WHERE ((PatientClinical.ClinicalType = ''A'') Or (PatientClinical.ClinicalType =''F'')) And
((PatientClinical.Status) = ''A'') And
((PatientClinical.Followup) > '' '') And
((Appointments.AppDate) <= @SDate) And  
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((PatientClinical.FollowUp = @StaId) OR (@StaId = ''^''))
ORDER BY PatientDemographics.LastName ASC , PatientDemographics.FirstName ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveCollectionsList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[RetrieveCollectionsList]
@SDate varchar(8),
@LocId Int,
@ResId Int
AS
SELECT DISTINCT 
	PatientDemographics.LastName, 
	PatientDemographics.FirstName, 
	PatientReceivables.OpenBalance, 
	PatientReceivables.Invoice, 
	PatientReceivables.InvoiceDate, 
	Resources.ResourceName, 
	Resources.ResourceId, 
	PatientDemographics.PatientId, 
	PracticeTransactionJournal.TransactionType, 
	PracticeTransactionJournal.TransactionStatus, 
	Appointments.ResourceId2,
	Appointments.AppointmentId
FROM (((PracticeTransactionJournal 
INNER JOIN PatientReceivables 
ON PracticeTransactionJournal.TransactionTypeId = PatientReceivables.ReceivableId) 
INNER JOIN PatientDemographics 
ON PatientReceivables.PatientId = PatientDemographics.PatientId) 
LEFT JOIN Resources 
ON PatientReceivables.BillToDr = Resources.ResourceId) 
INNER JOIN Appointments 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId
WHERE (((PatientReceivables.OpenBalance)>0.01) And 
((PracticeTransactionJournal.TransactionType)=''R'') And
((PracticeTransactionJournal.TransactionStatus)<>''Z'')) And
((PatientReceivables.InvoiceDate) <= @SDate) And
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1))
ORDER BY PatientReceivables.InvoiceDate
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveInsTransactions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveInsTransactions] 
(
@SAptDate varchar(8),
@EAptDate varchar(8),
@LocId Int,
@ResId Int,
@PatId Int,
@TStat varchar(1)
)
AS
SET NOCOUNT ON
SELECT DISTINCT
	Appointments.AppointmentId, Appointments.AppDate,
	Appointments.AppTime, Appointments.Comments,
	Appointments.ScheduleStatus, Appointments.ReferralId,
	AppointmentType.AppointmentType, AppointmentType.Question,
	Appointments.ActivityStatus, Appointments.Comments, PatientDemographics.Salutation,
	Appointments.ConfirmStatus, PatientDemographics.LastName,
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial,
	PatientDemographics.NameReference, PatientDemographics.Gender,
	PatientDemographics.BirthDate, PatientDemographics.HomePhone,
	PatientDemographics.BusinessPhone, PatientDemographics.ReferralRequired, 
	Resources.ResourceName, PatientFinancial.FinancialPIndicator, 
	PracticeInsurers.InsurerName, PatientDemographics.PatientId
FROM PracticeInsurers 
RIGHT JOIN ((Resources 
INNER JOIN (PatientDemographics 
INNER JOIN (AppointmentType 
INNER JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1) 
LEFT JOIN PatientFinancial 
ON PatientDemographics.PolicyPatientId = PatientFinancial.PatientId) 
ON PracticeInsurers.InsurerId = PatientFinancial.FinancialInsurerId
WHERE ((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
And ((Appointments.ResourceId1 = @ResId) OR (@ResId = -1))
And ((PatientDemographics.PatientId = @PatId) Or (@PatId < 1))
And ((Appointments.ScheduleStatus = ''P'') Or (Appointments.ScheduleStatus = ''R'') Or (Appointments.ScheduleStatus = ''A''))
And ((Appointments.AppDate <= @SAptDate) Or (@SAptDate = ''0''))
And ((Appointments.AppDate >= @EAptDate) Or (@EAptDate = ''0''))
ORDER BY Appointments.AppDate DESC , Appointments.AppTime
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTransactions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveTransactions] 
(
@SAptDate varchar(8),
@EAptDate varchar(8),
@LocId Int,
@ResId Int,
@PatId Int,
@TStat varchar(1)
)
AS
SET NOCOUNT ON
SELECT 
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.Comments, Appointments.ScheduleStatus, Appointments.ReferralId, 
	AppointmentType.AppointmentType, AppointmentType.Question, Appointments.ActivityStatus, 
	Appointments.Comments, PatientDemographics.Salutation, 	Appointments.ConfirmStatus, 
	PatientDemographics.LastName, PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.NameReference, PatientDemographics.Gender, PatientDemographics.BirthDate, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, 
	PatientDemographics.ReferralRequired, Resources.ResourceName, PatientDemographics.PatientId
FROM Resources 
INNER JOIN (PatientDemographics 
LEFT JOIN (AppointmentType 
INNER JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON Resources.ResourceId = Appointments.ResourceId1
WHERE ((PatientDemographics.PatientId = @PatId) Or (@PatId < 1)) And
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((Appointments.ScheduleStatus = @TStat) Or (@TStat = ''0'')) And
((Appointments.AppDate <= @SAptDate) Or (@SAptDate = ''0'')) And
((Appointments.AppDate >= @EAptDate) Or (@EAptDate = ''0''))
ORDER BY Appointments.AppDate DESC , Appointments.AppTime
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTransactionType1]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveTransactionType1]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int,
@TransRef varchar(1),
@TransType varchar(1),
@AStat1 varchar(1),
@AStat2 varchar(1),
@AAct varchar(1)
AS
SELECT
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.ResourceId2, Appointments.ScheduleStatus, PatientDemographics.PatientId, 
	PatientDemographics.Salutation, PatientDemographics.LastName, PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, 
	PatientDemographics.BirthDate, Resources.ResourceName, AppointmentType.AppointmentType, 
	PracticeTransactionJournal.TransactionId, PracticeTransactionJournal.TransactionType, 
	PracticeTransactionJournal.TransactionStatus, PracticeTransactionJournal.TransactionDate, 
	PracticeTransactionJournal.TransactionRef, PracticeTransactionJournal.TransactionRemark, 
	PracticeTransactionJournal.TransactionBatch, PracticeTransactionJournal.TransactionAction, 
	Resources_1.ResourceName, PracticeInsurers.InsurerName, 
	PracticeTransactionJournal.TransactionRcvrId
FROM (PracticeInsurers 
INNER JOIN ((Resources 
INNER JOIN (Appointments 
INNER JOIN (PracticeTransactionJournal 
INNER JOIN (PatientDemographics 
INNER JOIN PatientReceivables 
ON PatientDemographics.PatientId = PatientReceivables.PatientId) 
ON PracticeTransactionJournal.TransactionTypeId = PatientReceivables.ReceivableId) 
ON (Appointments.PatientId = PatientDemographics.PatientId) AND 
(Appointments.AppointmentId = PatientReceivables.AppointmentId)) 
ON Resources.ResourceId = Appointments.ResourceId1) 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId2 = Resources_1.ResourceId) 
ON PracticeInsurers.InsurerId = PatientReceivables.InsurerId) 
LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId
WHERE ((PracticeTransactionJournal.TransactionRef = @TransRef) OR (@TransRef = ''0'')) AND
((PracticeTransactionJournal.TransactionType = @TransType) OR (@TransType = ''0'')) AND
((PracticeTransactionJournal.TransactionDate <= @SDate)) AND
((PracticeTransactionJournal.TransactionDate >= @EDate) OR (@EDate = ''0'')) AND
((PatientDemographics.Status = ''A'') OR (PatientDemographics.Status = ''I'')) AND
((PracticeTransactionJournal.TransactionAction = @AAct) OR (@AAct = ''0'')) AND
(((PracticeTransactionJournal.TransactionStatus = @AStat1) OR (@AStat1 = ''0'')) OR
 ((PracticeTransactionJournal.TransactionStatus = @AStat2) OR (@AStat2 = ''0''))) AND
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY PracticeTransactionJournal.TransactionDate
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTransactionType2]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveTransactionType2]
@SDate varchar(8),
@EDate varchar(8),
@TransType varchar(1),
@AStat1 varchar(1),
@AStat2 varchar(1),
@AAct varchar(1),
@ResName varchar(16)
AS
SELECT
	PatientDemographics.PatientId, PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.NameReference, PatientDemographics.Salutation, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, 
	PatientDemographics.BirthDate, PracticeTransactionJournal.TransactionId, 
	PracticeTransactionJournal.TransactionType, PracticeTransactionJournal.TransactionStatus, 
	PracticeTransactionJournal.TransactionDate, PracticeTransactionJournal.TransactionRef, 
	PracticeTransactionJournal.TransactionRemark, PracticeTransactionJournal.TransactionBatch, 
	PracticeTransactionJournal.TransactionAction, PracticeTransactionJournal.TransactionRcvrId
FROM PracticeTransactionJournal 
INNER JOIN PatientDemographics 
ON PracticeTransactionJournal.TransactionTypeId = PatientDemographics.PatientId
WHERE ((PracticeTransactionJournal.TransactionType = @TransType)) AND
((PracticeTransactionJournal.TransactionDate <= @SDate)) AND
((PracticeTransactionJournal.TransactionDate >= @EDate)) AND
((PatientDemographics.Status = ''A'') OR (PatientDemographics.Status = ''I'')) AND
(((PracticeTransactionJournal.TransactionStatus = @AStat1) OR (@AStat1 = ''0'')) OR
 ((PracticeTransactionJournal.TransactionStatus = @AStat2) OR (@AStat2 = ''0''))) AND
((PracticeTransactionJournal.TransactionAction = @AAct)) AND
((PracticeTransactionJournal.TransactionRef Like ''%''+@ResName) OR (@ResName = ''''))
ORDER BY PatientDemographics.LastName, PatientDemographics.FirstName


/****** Object:  StoredProcedure [dbo].[usp_FinancialDataDetails6]    Script Date: 08/31/2009 15:28:26 ******/
SET ANSI_NULLS ON
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTransactionType3]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveTransactionType3]
@LocId Int,
@ResId Int,
@TransType varchar(1),
@AStat1 varchar(1)
AS
SELECT
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.ScheduleStatus, PatientDemographics.PatientId, 
	PatientDemographics.LastName, PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, 
	PatientDemographics.Salutation, PatientDemographics.HomePhone, 
	PatientDemographics.BusinessPhone, PatientDemographics.BirthDate, 
	AppointmentType.AppointmentType, Resources.ResourceName, 
	PracticeTransactionJournal.TransactionType, PracticeTransactionJournal.TransactionId, 
	PracticeTransactionJournal.TransactionStatus, PracticeTransactionJournal.TransactionDate, 
	PracticeTransactionJournal.TransactionAction, PracticeTransactionJournal.TransactionRef, 
	PracticeTransactionJournal.TransactionRemark, PracticeTransactionJournal.TransactionBatch, 
	Appointments.ResourceId2, PracticeTransactionJournal.TransactionRcvrId
FROM (PracticeTransactionJournal 
INNER JOIN (PatientDemographics 
INNER JOIN (Resources 
INNER JOIN (AppointmentType 
RIGHT JOIN Appointments 
ON AppointmentType.AppTypeId = Appointments.AppTypeId) 
ON Resources.ResourceId = Appointments.ResourceId1) 
ON PatientDemographics.PatientId = Appointments.PatientId) 
ON PracticeTransactionJournal.TransactionTypeId = Appointments.AppointmentId) 
LEFT JOIN Resources AS Resources_1 ON Appointments.ResourceId2 = Resources_1.ResourceId
WHERE ((PracticeTransactionJournal.TransactionStatus = @AStat1) OR (@AStat1 = ''0'')) AND
((PracticeTransactionJournal.TransactionType = @TransType)) AND
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
--Uncommenting the next line will make ASCENDING date order
ORDER BY PracticeTransactionJournal.TransactionDate ASC,
--Uncommenting the next line will make DESCENDING date order
--ORDER BY PracticeTransactionJournal.TransactionDate DESC,
PatientDemographics.LastName, PatientDemographics.FirstName
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTransactionTypeR1]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[RetrieveTransactionTypeR1]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int,
@TransRef varchar(1),
@TransType varchar(1),
@AStat1 varchar(1),
@AStat2 varchar(1),
@AAct varchar(1)
AS
SELECT
	Appointments.AppointmentId, Appointments.AppDate, Appointments.AppTime, 
	Appointments.ResourceId2, Appointments.ScheduleStatus,
	PatientDemographics.PatientId, 
	PatientDemographics.Salutation, PatientDemographics.LastName,
	PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial, PatientDemographics.NameReference, 
	PatientDemographics.HomePhone, PatientDemographics.BusinessPhone, 
	PatientDemographics.BirthDate, Resources.ResourceName,
	AppointmentType.AppointmentType, 		PracticeTransactionJournal.TransactionId,
	PracticeTransactionJournal.TransactionType, 
	PracticeTransactionJournal.TransactionStatus,
	PracticeTransactionJournal.TransactionDate, 
	PracticeTransactionJournal.TransactionRef,
	PracticeTransactionJournal.TransactionRemark, 
	PracticeTransactionJournal.TransactionBatch, 
	PracticeTransactionJournal.TransactionAction, 
	Resources_1.ResourceName, PracticeInsurers.InsurerName, 
	PracticeTransactionJournal.TransactionRcvrId
FROM (PracticeInsurers 
INNER JOIN ((Resources 
INNER JOIN (Appointments 
INNER JOIN (PracticeTransactionJournal 
INNER JOIN (PatientDemographics 
INNER JOIN PatientReceivables 
ON PatientDemographics.PatientId = PatientReceivables.PatientId) 
ON PracticeTransactionJournal.TransactionTypeId = PatientReceivables.ReceivableId) 
ON (Appointments.PatientId = PatientDemographics.PatientId) AND 
(Appointments.AppointmentId = PatientReceivables.AppointmentId)) 
ON Resources.ResourceId = Appointments.ResourceId1) 
LEFT JOIN Resources AS Resources_1 
ON Appointments.ResourceId2 = Resources_1.ResourceId) 
ON PracticeInsurers.InsurerId = PatientReceivables.InsurerId) 
LEFT JOIN AppointmentType ON Appointments.AppTypeId = AppointmentType.AppTypeId
WHERE ((PracticeTransactionJournal.TransactionRef = @TransRef) OR (@TransRef = ''0'')) AND
((PracticeTransactionJournal.TransactionType = @TransType) OR (@TransType = ''0'')) AND
((PracticeTransactionJournal.TransactionDate <= @SDate)) AND
((PracticeTransactionJournal.TransactionDate >= @EDate) OR (@EDate = ''0'')) AND
((PracticeTransactionJournal.TransactionAction = @AAct) OR (@AAct = ''0'')) AND
(((PracticeTransactionJournal.TransactionStatus = @AStat1) OR (@AStat1 = ''0'')) OR
 ((PracticeTransactionJournal.TransactionStatus = @AStat2) OR (@AStat2 = ''0''))) AND
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1))
ORDER BY PracticeTransactionJournal.TransactionDate DESC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[RetrieveTranscriptions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[RetrieveTranscriptions] 
@SDate varchar (8),
@LocId Int,
@ResId Int
AS
SELECT
	Appointments.AppDate, 
	Appointments.ResourceId1, 
	Resources.ResourceName, 
	Appointments.ResourceId2, 
	Appointments.AppointmentId, 
	PatientDemographics.LastName, 
	PatientDemographics.FirstName, 
	PatientDemographics.MiddleInitial,
	PatientDemographics.HomePhone, 
	PatientDemographics.PatientId,
	PatientNotes.NoteId, 
	PatientNotes.NoteSystem,
	PatientNotes.NoteEye, 
	PatientNotes.NoteILPNRef
FROM ((Resources 
INNER JOIN Appointments 
ON Resources.ResourceId = Appointments.ResourceId1) 
INNER JOIN PatientNotes 
ON Appointments.AppointmentId = PatientNotes.AppointmentId)
INNER JOIN PatientDemographics
ON Appointments.PatientId = PatientDemographics.PatientId
WHERE (PatientNotes.NoteAudioOn = ''T'') And
(Appointments.AppDate <= @SDate) AND
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) AND 
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1))
ORDER BY PatientDemographics.LastName, PatientDemographics.FirstName
')
EXEC('
/****** Object:  StoredProcedure [dbo].[SurgerySchedule]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

--exec SurgerySchedule ''20100431'',''20100317''

CREATE PROCEDURE [dbo].[SurgerySchedule]
		(
			@StartDate Varchar(200),
			@EndDate   Varchar(200))

AS

select  pd.PatientId, pd.LastName, pd.firstname, pd.BirthDate, ap.AppDate, ap.AppTime, ap.ResourceId2, ap.ResourceId1,
		pd.Address, pd.City, pd.State, pd.Zip, pd.HomePhone, pd.CellPhone, pd.BusinessPhone,
		pi.InsurerName, pf.FinancialPIndicator, pf.FinancialPerson, at.AppointmentType, ap.Comments, re.ResourceName,
		pn.PracticeName, pn.PracticeAddress, pn.PracticeCity, pn.PracticeState, pn.PracticeZip

into #temp
from
appointments ap
left outer join patientdemographics pd on pd.patientid = ap.patientid
left outer join patientfinancial pf on pf.patientid = pd.patientid
left outer join practiceinsurers pi on pi.insurerid = pf.financialinsurerid
left outer join AppointmentType at on at.AppTypeId = ap.AppTypeId
left outer join Resources re on re.ResourceId = ap.ResourceId1
left outer join PracticeName pn on pn.PracticeId = re.PracticeId
where
ap.schedulestatus in (''P'', ''D'', ''R'') and
appdate between @enddate and @startdate and
ap.ResourceId2 = 15 and
ap.AppTypeId = 34

--select * from #temp


CREATE TABLE #FinalTable
(
	PatientId int,
	LastName nvarchar(250),
	firstname nvarchar(250),
	BirthDate nvarchar(250),
	AppDate nvarchar(250),
	AppTime int,
	ResourceId1 int,
	resourceid2 int,
	Address nvarchar(250),
	City nvarchar(250),
	State nvarchar(250),
	Zip nvarchar(250),
	HomePhone nvarchar(250),
	CellPhone nvarchar(250),
	BusinessPhone nvarchar(250),
	AppointmentType nvarchar(250),
	Comments nvarchar(250),
	ResourceName nvarchar(250),
	PracticeName nvarchar(250),
	PracticeAddress nvarchar(250),
	PracticeCity nvarchar(250),
	PracticeState nvarchar(250),
	PracticeZip nvarchar(250),
	PrimaryIns nvarchar(250),
	PrimaryNum nvarchar(250),
	SecondaryIns nvarchar(250),
	SecondaryNum nvarchar(250)
)


Insert into #FinalTable
select distinct PatientId, LastName, firstname, BirthDate, AppDate, AppTime, resourceid1, resourceid2,
		Address, City, State, Zip, HomePhone, CellPhone, BusinessPhone,
		AppointmentType, Comments, ResourceName,
		PracticeName, PracticeAddress, PracticeCity, PracticeState, PracticeZip,
		'''' as PrimaryIns, '''' as PrimaryNum, '''' as SecondaryIns, '''' as SecondaryNum

from #temp


update #FinalTable
set #FinalTable.PrimaryIns = #Temp.Insurername,
#FinalTable.PrimaryNum = #Temp.FinancialPerson
from #Temp, #FinalTable
where #FinalTable.PatientId = #Temp.PatientId and
#Temp.FinancialPIndicator = 1


update #FinalTable
set #FinalTable.SecondaryIns = #Temp.Insurername,
#FinalTable.SecondaryNum = #Temp.FinancialPerson
from #Temp, #FinalTable
where #FinalTable.PatientId = #Temp.PatientId and
#Temp.FinancialPIndicator = 2


select * from #FinalTable
order by AppDate, AppTime, lastname')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_BillingNotes]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_BillingNotes]
as

SET NOCOUNT ON

select pn.PatientId, pn.NoteType, pn.Note1
	, pn.Note2, pn.Note3, pn.Note4
	, pn.NoteSystem, pn.NoteCommentOn
	, pr.OpenBalance, pr.Invoice
	, ptj.TransactionStatus, ptj.TransactionRef
into #Temp
from PatientNotes pn 
	LEFT JOIN dbo.PatientReceivables pr ON pn.AppointmentId = pr.AppointmentId 
		and pr.InsurerDesignation = ''T'' 
    LEFT JOIN dbo.PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
		and ptj.TransactionType = ''R'' and ptj.TransactionStatus <> ''Z''
where pn.NoteType = ''B''
	and pn.NoteCommentOn = ''T''
	and (pn.appointmentid = 0 or pr.OpenBalance > 0)
	and (pn.appointmentid = 0 or ptj.TransactionRef = ''P'')

--Create FinalTable temp table
CREATE TABLE #FinalTable
(
	PatientId int, 
	Invoice nvarchar(32), 
	Note nvarchar(100) default '''',
	NoteType nvarchar(10)
)

Insert into #FinalTable
	Select PatientId, Invoice, left((Note1 + Note2 + Note3 + Note4),100), ''Account''
	From #Temp
	where NoteSystem = ''''

Insert into #FinalTable
	Select PatientId, Invoice, left((Note1 + Note2 + Note3 + Note4),100), ''Invoice''
	From #Temp
	where Left(NoteSystem,1) = ''R'' 
		and NoteSystem not like ''%C%''

Insert into #FinalTable
	Select PatientId, Invoice, left((Note1 + Note2 + Note3 + Note4),100), ''Service''
	From #Temp
	where Left(NoteSystem,1) = ''R'' 
		and NoteSystem like ''%C%''

select distinct PatientId, IsNull(Invoice, '''') as Invoice, Note, NoteType
from #FinalTable

Drop table #Temp

SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_BMI_GetPatientDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_BMI_GetPatientDetails]     
   @PATIENT_ID INTEGER=NULL,       
   @RACE VARCHAR(100)=NULL,    
   @Ethnicity VARCHAR(100)=NULL,    
   @MODE VARCHAR(50)=NULL,    
   @STATE VARCHAR(20)=NULL    
AS        
BEGIN        
 IF(@MODE=''PATIENTDETAILS'' )    
    SELECT ISNULL(FIRSTNAME,'''') AS FIRSTNAME,ISNULL(MiddleInitial,'''') AS MI,ISNULL(LASTNAME,'''') AS LASTNAME, ISNULL(BirthDate,'''') AS DOB,      
    ISNULL(GENDER,'''') AS GENDER,ISNULL(ADDRESS,'''') AS ADDRESS,ISNULL(CITY,'''') AS CITY,ISNULL(STATE,'''') AS STATE,      
    ISNULL(ZIP,'''') AS ZIP,ISNULL(HomePhone,'''') AS HOMEPH,ISNULL(BusinessPhone,'''') AS WORKPH,ISNULL('''','''') AS EXTERNALID,      
    ISNULL(Marital,'''') AS MARITALSTATUS,ISNULL('''','''') AS RACE,ISNULL(FirstName+ ''  '' + LastName,'''') AS PATIENTNAME,      
    ISNULL('''','''') AS ETHNICITY,ISNULL('''','''') AS NATIONALITY,ISNULL('''','''') AS RELIGION      
  ,''GENDERVALUE''= ISNULL(CASE GENDER WHEN ''M'' THEN ''MALE'' ELSE ''FEMALE'' END,'''')    
  ,''AGEYEARS''= ISNULL(CONVERT(VARCHAR(4),DATEDIFF(YEAR,BirthDate,CONVERT(NVARCHAR,GETDATE(),112))),'''')    
  ,CONVERT(VARCHAR(20),ISNULL(BirthDate,''''),101) AS PATIENTDOB    
    FROM PatientDemoGraphics WHERE PATIENTID=@PATIENT_ID      
 ELSE IF(@MODE=''AGE'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETTYPE=''age''and VALUESETNAME=''year''    
 ELSE IF(@MODE=''RACE'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETNAME like ''''+ @RACE+ ''%''    
 ELSE IF(@MODE=''ETHNICITY'')    
   SELECT ISNULL(VALUESETCODE,'''') AS VALUESETCODE,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETNAME LIKE ''''+@Ethnicity+''%''    
 ELSE IF(@MODE=''STATE'')    
   SELECT ISNULL(VALUESETNAME,'''') AS VALUESETNAME,ISNULL(CODESYSTEMOID,'''') AS CODESYSTEMOID    
   FROM TBLVALUESETS where VALUESETTYPE=''State'' and VALUESETCODE=''+@State+''    
END 
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_BMI_GROWTHCHARTDETAILS]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_BMI_GROWTHCHARTDETAILS]    
@MODE AS VARCHAR(20),    
@ID AS INTEGER,
@PatientID as INTEGER    
AS    
 BEGIN    
  IF (@MODE = ''GCDETAIL'')    
   BEGIN     
    SELECT GCD.GRAPHID,GCD.GROWTHCHARTID,ISNULL(GCD.XAXIS,''0.0'') AS XAXIS,ISNULL(GCD.Y1AXIS,''0.0'')     
    AS Y1AXIS,ISNULL(GCD.Y2AXIS,''0.0'') AS Y2AXIS,ISNULL(GCD.HCBMI,''0.0'') AS HCBMI    
    FROM TBLGROWTHCHARTDETAIL GCD WHERE GCD.GROWTHCHARTID = @ID ORDER BY GCD.XAXIS    
   END    
  ELSE IF (@MODE = ''GCMASTER'')    
   BEGIN     
    SELECT GROWTHCHARTID,CHARTID,CHARTNAME,DEVIATION FROM TBLGROWTHCHARTMASTER    
    WHERE CHARTID = @ID    
   END    
  ELSE IF (@MODE = ''PATDETAILS'')    
   BEGIN   
	select pc.PatientId,ap.AppDate,pd.birthdate,ap.AppointmentId,
	ROUND(CAST((DATEDIFF(MM,(SUBSTRING(pd.BirthDate,1,4)+''-'' + SUBSTRING(pd.BirthDate,5,2) + ''-'' + SUBSTRING(pd.BirthDate,7,2)),(SUBSTRING(ap.AppDate,1,4)+''-'' + SUBSTRING(ap.AppDate,5,2) + ''-'' + SUBSTRING(ap.AppDate,7,2)))) AS DECIMAL)/12,1) AS AGE,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%BMI%'' 
	THEN (REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))
	ELSE ''0'' END AS BMI,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%HEIGHT%'' 
	THEN 
		CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%FEET%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*12)) 
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%METERS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*39.370113))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%CM%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.393700787))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%INCHES%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*1))
		ELSE 0 END 
	ELSE 0 END As HEIGHT,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%WEIGHT%'' 
	THEN 
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%OUNCES%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.0625))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%POUNDS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*1))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%KGS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*2.20462262))
		WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%GRAMS%''
		THEN CEILING((CONVERT(NUMERIC(13,2),REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'',''''))*0.00220462262))
		ELSE 0 END 
	ELSE 0 END AS WEIGHT,
	SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) as TEXT,
	REPLACE(REPLACE(SUBSTRING(pc.FindingDetail,CHARINDEX(''<'',pc.FindingDetail,0),LEN(pc.FindingDetail)),''<'',''''),''>'','''') as VALUE,
	CASE WHEN SUBSTRING(pc.FindingDetail,0,CHARINDEX(''='',pc.FindingDetail,0)) LIKE ''%TIME%'' THEN 0
	ELSE 1 END AS TimeCol
	,pc.FindingDetail
	from PatientClinical pc
	inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
	INNER JOIN PatientDemographics pd on pd.PatientId=pc.PatientId
	where ClinicalType in (''F'') 
	and Symptom = ''/HEIGHT AND WEIGHT'' 
	and pc.Status = ''A'' ANd ap.ScheduleStatus in (''D'', ''A'')
	AND pc.PatientId=@PatientID AND ap.AppDate <> ''0'' ORDER BY AGE   
   END    
 END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CCD_GetPatientDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CCD_GetPatientDetails]
(
@PatientId NVARCHAR(200)
)
AS
BEGIN
select pd.LastName, pd.FirstName, pd.MiddleInitial, pd.NameReference,pd.PatientId, pd.Address, pd.Suite, pd.City, pd.State, pd.Zip
, pd.HomePhone, pd.BirthDate, pd.Gender,ap.AppDate, apt.AppointmentType
,pd.BusinessPhone,pd.CellPhone,pd.Email,pd.Salutation,convert(varchar,getdate(),112) as CurrDate
from PatientDemographics pd
inner join Appointments ap on ap.PatientId = pd.PatientId
inner join AppointmentType apt on apt.AppTypeId = ap.AppTypeId
where pd.PatientId = @PatientId
and ap.ScheduleStatus = ''D''
order by AppDate
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CCD_GetPatientMedsDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

-- Batch submitted through debugger: SQLQuery5.sql|8|0|C:\Documents and Settings\administrator\Local Settings\Temp\~vs201.sql
CREATE PROCEDURE [dbo].[USP_CCD_GetPatientMedsDetails]
(
@PatientId NVARCHAR(200)
)
AS
BEGIN
--DECLARE @ClinicalIdList NVARCHAR(200);
--SET @ClinicalIdList=''68489 68490 68493 68494'';
--DECLARE @PatientId NVARCHAR(200);
--SET @PatientId=''20073'';
CREATE TABLE #RxSegments
(
	ClinicalId int,
	SegTwo int,
	SegThree int,
	SegFour int,
	SegFive int,
	SegSix int,
	SegSeven int,
	SegNine int,
	LegacyDrug bit
)

INSERT INTO #RxSegments
SELECT pc.clinicalid
	, CHARINDEX(''-2/'', pc.FindingDetail) + 3
	, CHARINDEX(''-3/'', pc.FindingDetail) + 3
	, CHARINDEX(''-4/'', pc.FindingDetail) + 3
	, CHARINDEX(''-5/'', pc.FindingDetail) + 3
	, CHARINDEX(''-6/'', pc.FindingDetail) + 3
	, CHARINDEX(''-7/'', pc.FindingDetail) + 3
	, CHARINDEX(''9-9/'', pc.FindingDetail)
	, CASE WHEN pc.FindingDetail like ''%()%'' THEN 0 ELSE 1 END 
FROM PatientClinical pc
--pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
WHERE pc.ClinicalType = ''A'' and SUBSTRING(pc.Findingdetail, 1, 2) = ''Rx'' and pc.Status = ''A'' and substring(pc.ImageDescriptor, 1, 1) not in (''P'', ''C'', ''D'') and  pc.PatientId = @PatientID
	
select pc.ClinicalId as externalId,PC.PatientId,
	CASE WHEN pc.FindingDetail like ''RX-8/%-2/%'' 
		THEN SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6) 
	ELSE '''' END AS DrugName,
	CASE WHEN LEN(pc.ImageDescriptor) > 0
		THEN RIGHT(pc.ImageDescriptor, 4) + SUBSTRING(pc.ImageDescriptor, LEN(pc.ImageDescriptor) - 9, 2) 
		+ SUBSTRING(pc.ImageDescriptor, len(pc.ImageDescriptor) - 6, 2)
	ELSE '''' END as Date,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pc.FindingDetail like ''%(PILLS-PACKAGE %''
			THEN SUBSTRING(pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '', pc.FindingDetail)+15, CHARINDEX('' '', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15)-(CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15))
		ELSE '''' 
		END	
	END AS dispenseNumber,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pn1.Note1 IS NULL 
			THEN CASE WHEN pc.FindingDetail like ''rx-8%-2/(%'' THEN Substring(pc.findingdetail, CHARINDEX(''-2/'',pc.findingdetail)+4, charindex('')'', pc.findingdetail)-CHARINDEX(''-2/'',pc.findingdetail)-4)
			ELSE '''' END + '' '' + CASE WHEN pc.FindingDetail like ''rx-8%-2/(%(O%-3/%'' THEN Substring(pc.findingdetail, CHARINDEX('')-3/'',pc.findingdetail)-2,  2) 
			ELSE ''''	END	 + '' '' + CASE WHEN pc.FindingDetail NOT like ''rx-8%-3/-4/%''  
			THEN SUBSTRING(pc.findingdetail ,CHARINDEX(''-3/('',pc.findingdetail)+4,CHARINDEX('')'',pc.findingdetail, charindex(''-3/('',pc.findingdetail))-CHARINDEX(''-3/('',pc.findingdetail)-4 )
			ELSE '''' END	+ '' '' + CASE WHEN pc.FindingDetail like ''rx-8/%-3/(%)%(%)-4/%'' THEN SUBSTRING(pc.findingdetail, CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)+1, charindex('')-4/'', pc.findingdetail)-CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)-1)
		ELSE '''' END	ELSE ''AS PRESCRIBED'' END 
	ELSE '''' END AS sig, 
	CASE rx.LegacyDrug WHEN 1 THEN
		case WHEN pc.FindingDetail like ''%(PRN)%'' THEN '''' 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS%'' and pc.FindingDetail not like ''rx-8%REFILLS %(%-5/%'' 
				THEN SUBSTRING	(pc.findingdetail, charindex(''-4/REFILLS '',pc.findingdetail)+11, charindex(''-5/'', pc.findingdetail)- charindex(''-4/REFILLS '',pc.findingdetail)-11) 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS %(%)-5/%'' THEN SUBSTRING(pc.findingdetail, charindex(''-4/REFILLS'',pc.findingdetail)+11, CHARINDEX(''('',pc.findingdetail, charindex(''-4/REFILLS'', pc.FindingDetail))-charindex(''-4/REFILLS'',pc.findingdetail)-11) 
			 ELSE '''' END	
	ELSE '''' END AS refillCount,
	pc.FindingDetail,pc.DrawFileName
	FROM PatientClinical pc 
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
			AND Substring(PC.FindingDetail, 6, CHARINDEX(''-2/'',pc.findingdetail)-6) = pn1.NoteSystem 
			AND pn1.NoteType = ''R''
			
-- pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
WHERE pc.ClinicalType = ''A'' and SUBSTRING(pc.Findingdetail, 1, 2) = ''Rx'' and pc.Status = ''A'' and substring(pc.ImageDescriptor, 1, 1) not in (''P'', ''C'', ''D'') and pc.PatientId = @PatientID 
SET NOCOUNT OFF

DROP Table #RXsegments
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CCD_GetPatientProblemDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CCD_GetPatientProblemDetails]
(
@PatientId NVARCHAR(200)
)
As
BEGIN

select pc.Symptom, CASE RTRIM(LTRIM(SUBSTRING(pc.Symptom,2,LEN(pc.Symptom))))
WHEN ''%'' THEN ''History of''
WHEN ''!'' THEN ''Confirmed negative finding''
WHEN ''#'' THEN ''Rule out''
WHEN ''%#'' THEN ''History of/Rule out''
WHEN ''%!'' THEN ''History of/Confirmed negative finding''
ELSE ''Active''  
END AS Status
, pc.EyeContext,pc.FindingDetail,
LEFT(pc.findingDetail,CASE WHEN CHARINDEX(''.'',pc.FindingDetail,CHARINDEX(''.'',pc.FindingDetail,0)+1)>0 THEN CHARINDEX(''.'',pc.FindingDetail,CHARINDEX(''.'',pc.FindingDetail,0)+1)-1 
ELSE LEN(pc.FindingDetail) END) As ICD9Code
 from PatientClinical pc 
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where ap.AppDate in
(select MAX(AppDate) from Appointments ap
where ap.ScheduleStatus = ''d'' and ap.PatientId = @PatientId
group by ap.PatientId)
and ClinicalType = ''U'' and pc.PatientId= @PatientId and pc.Status = ''A''
AND SUBSTRING(pc.FindingDetail,0,2) NOT IN( ''L'', ''M'', ''P'', ''Q'', ''S'', ''X'', ''Y'')
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CCD_GetPatientProcedureDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CCD_GetPatientProcedureDetails]
(
@PatientId NVARCHAR(200)
)
AS
BEGIN
select distinct substring(pc.Symptom, 2, 127) AS ProcedureName, appdate 
from PatientClinical pc
inner join Appointments ap on ap.AppointmentId = pc.AppointmentId
where pc.ClinicalType = ''F'' and pc.Status = ''A''
and pc.PatientId = @PatientId
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_Drug_Interactions_Enabled]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER OFF')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_Drug_Interactions_Enabled]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @Parm1 INT;
DECLARE @Parm2 INT;
DECLARE @Parm3 INT;
DECLARE @Parm4 INT;
DECLARE @Parm5 INT;
SET @TotParm=0;
SET @CountParm=0;
SET @Parm1=0;
SET @Parm2=0;
SET @Parm3=0;
SET @Parm4=0;
SET @Parm5=0;

SELECT @Parm1= COUNT(*) FROM PracticeInterfaceConfiguration where FieldReference = ''ERX'' and FieldValue = ''T''

SELECT @Parm2=COUNT(*) FROM EPrescription_Credentials where AccountId > 0

SELECT @Parm3=COUNT(*) FROM  EPrescription_Credentials WHERE CONVERT(NVARCHAR,ModifiedDate,112) <= @StartDate

SET @Parm4=@Parm2 * @Parm1

SET @Parm5=@Parm3 * @Parm4

IF @Parm5=1
BEGIN
	SET @CountParm = 1
	SET @TotParm = 1
END

SELECT @TotParm as Denominator,@CountParm as Numerator

END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetActiveMedicationAllergyList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetActiveMedicationAllergyList]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @Status NVARCHAR(50); 
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

SELECT @Status=FieldValue FROM  PracticeInterfaceConfiguration WHERE FieldReference = ''MUON''

--maintain active med allergies list - 80% - need way to say "NO ALLERGIES"
----denominator (anyone with an appt)
select @TotParm=COUNT(DISTINCT ap.PatientId) from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0 


--numerator
IF @Status=''T''
BEGIN
	SELECT @CountParm=COUNT(DISTINCT ap.PatientId)
	FROM Appointments ap
	INNER JOIN PatientClinical pc on pc.PatientId = ap.PatientId
	INNER JOIN Resources re on re.ResourceId = ap.ResourceId1
	AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
	WHERE AppDate between @StartDate and @EndDate and
	ScheduleStatus = ''D'' and AppTime > 0 and
	pc.ClinicalType = ''Q'' and pc.Status = ''A''
END
ELSE
BEGIN
	SELECT @CountParm=COUNT(DISTINCT ap.PatientId)
	FROM Appointments ap
	INNER JOIN Resources re on re.ResourceId = ap.ResourceId1
	LEFT OUTER  JOIN PatientClinical PC on ap.PatientId=PC.PatientId AND  PC.ClinicalType IN (''C'',''H'') AND PC.Status=''A''
	LEFT OUTER JOIN PatientNotes PN on PN.PatientId=ap.PatientId 
	AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
	WHERE AppDate between @StartDate and @EndDate and
	ScheduleStatus = ''D'' and AppTime > 0
	and ((Symptom like ''%ALLERG%'') OR (PN.NoteType=''C'' AND PN.NoteSystem=''9''))
END

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetActiveMedicationList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetActiveMedicationList]  
(  
@StartDate NVARCHAR(200),  
@EndDate NVARCHAR(200),  
@ResourceIds NVARCHAR(1000)  
)  
AS  
BEGIN  
DECLARE @TotParm INT;  
DECLARE @CountParm INT;
DECLARE @Status NVARCHAR(50);  
SET @TotParm=0;  
SET @CountParm=0;
SELECT @Status='''';  
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))  
IF @ResourceIds=''''
BEGIN  
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources   
WHERE ResourceType IN(''D'',''Q'')  
END  
ELSE  
BEGIN  
DECLARE @TmpResourceIds VARCHAR(100)  
DECLARE @ResId VARCHAR(10)  
SELECT @TmpResourceIds =@ResourceIds  
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0  
 BEGIN  
  SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)  
  SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')  
  INSERT INTO #TMPResources values(@ResId)  
 END  
 INSERT INTO #TMPResources values(@TmpResourceIds)  
END  


SELECT @Status=FieldValue FROM  PracticeInterfaceConfiguration WHERE FieldReference = ''MUON''
  

----denominator (anyone with an appt)
  
SELECT @TotParm=COUNT(DISTINCT ap.PatientId) from Appointments ap  
inner join Resources re on re.ResourceId = ap.ResourceId1  
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)  
where AppDate between @StartDate and @EndDate and   
ScheduleStatus = ''D'' and AppTime > 0  
  
--numerator  
IF @Status =''T'' 
BEGIN
	SELECT @CountParm=COUNT(DISTINCT ap.PatientId)
	from Appointments ap
	inner join PatientClinical pc on pc.PatientId = ap.PatientId
	inner join Resources re on re.ResourceId = ap.ResourceId1
	AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
	where AppDate between @StartDate and @EndDate and
	ScheduleStatus =''D'' and AppTime > 0 and
	pc.ClinicalType = ''Q'' and pc.Status = ''A''
END
ELSE
BEGIN
	SELECT @CountParm=COUNT(DISTINCT ap.PatientId)
	FROM Appointments ap
	INNER JOIN Resources re on re.ResourceId = ap.ResourceId1
	LEFT OUTER  JOIN PatientClinical PC on ap.PatientId=PC.PatientId AND  PC.ClinicalType = ''A'' AND PC.Status=''A''
	LEFT OUTER JOIN PatientNotes PN on PN.PatientId=ap.PatientId
	AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
	WHERE AppDate between @StartDate and @EndDate and
	ScheduleStatus = ''D'' and AppTime > 0
	and ((substring(PC.FindingDetail, 1, 3) = ''RX-'') OR (PN.NoteType=''R''))
END
 
  
DROP TABLE #TMPResources  
  
SELECT @TotParm as Denominator,@CountParm as Numerator  
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetClinicalSummaryStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetClinicalSummaryStatus]  
(  
@StartDate NVARCHAR(200),  
@EndDate NVARCHAR(200),  
@ResourceIds NVARCHAR(1000)  
)  
AS  
BEGIN  
---#10-Clin Summ provided to patients win 3 bus days - > 50% of ofc visits--------------  
DECLARE @TotParm INT;  
DECLARE @CountParm INT;  
DECLARE @Parm1 INT;  
DECLARE @Parm2 INT;  
DECLARE @Status NVARCHAR(10);  
SET @TotParm=0;  
SET @CountParm=0;  
SET @Parm1=0;  
SET @Parm2=0;  
SET @Status=''''
  
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))  
IF @ResourceIds=''''
BEGIN  
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources   
WHERE ResourceType IN(''D'',''Q'')  
END  
ELSE  
BEGIN  
DECLARE @TmpResourceIds VARCHAR(100)  
DECLARE @ResId VARCHAR(10)  
SELECT @TmpResourceIds =@ResourceIds  
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0  
 BEGIN  
  SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)  
  SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')  
  INSERT INTO #TMPResources values(@ResId)  
 END  
 INSERT INTO #TMPResources values(@TmpResourceIds)  
END  
  
--denominator  
select @TotParm=COUNT(DISTINCT ap.PatientId)  
from Appointments ap  
inner join Resources re on re.ResourceId = ap.ResourceId1  
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId   
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)  
where AppDate between @StartDate and @EndDate and   
ScheduleStatus in (''D'') and AppTime > 0  
and pc.ClinicalType = ''Z'' and pc.Status = ''A''  
  
  
--numerator  
select @Status=FieldValue from PracticeInterfaceConfiguration where FieldReference = ''PRINT''
IF @Status=''T''  
   BEGIN
	SET @CountParm=@TotParm;  
   END  
ELSE  
   BEGIN  
  SELECT @CountParm=COUNT(DISTINCT ap.PatientId)  
  from Appointments ap  
  inner join LoginUsers_Audit LUA on LUA.EncounterId = ap.AppointmentId   
  inner join Resources re on re.ResourceId = ap.ResourceId1 
		AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources) 
  where AppDate between @StartDate and @EndDate and   
  ScheduleStatus in (''D'', ''G'', ''H'', ''U'', ''A'') and AppTime > 0  
  and substring(ActionTaken, 0, 17) in (''Printed Patient'', ''Printed Misc Ltr'')  
  and  Convert(NVARCHAR,LUA.TimeStamp,112) between AppDate and   
  CONVERT(NVARCHAR,DATEADD(D,3+  
  (CASE WHEN DATEPART(DW, CAST(SUBSTRING(AppDate,0,5)+ ''/'' + SUBSTRING(AppDate,5,2) + ''/'' + SUBSTRING(AppDate,7,2)   
  AS DATETIME))>3 THEN 2 ELSE 0 END) ,  
  CAST(SUBSTRING(AppDate,0,5)+ ''/'' + SUBSTRING(AppDate,5,2) + ''/'' + SUBSTRING(AppDate,7,2)   
  AS DATETIME)),112)  
 END  
  
DROP TABLE #TMPResources  
  
SELECT @TotParm as Denominator,@CountParm as Numerator  
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetElectronicCopyStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetElectronicCopyStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @TempEndDate NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator
SELECT @TotParm=COUNT(DISTINCT PTJ.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId =ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''P''
and PTJ.TransactionDate between @StartDate and @EndDate

--numerator
SELECT @TempEndDate=CONVERT(NVARCHAR,DATEADD(D,(CASE WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=4 THEN -1
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=5 THEN -2
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=6 THEN -3
WHEN DATEPART(DW, CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME))=7 THEN -4
 ELSE 0 END),
CAST(SUBSTRING(@EndDate,0,5)+ ''/'' + SUBSTRING(@EndDate,5,2) + ''/'' + SUBSTRING(@EndDate,7,2) 
AS DATETIME)),112)


SELECT @CountParm=COUNT(DISTINCT PTJ.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''P'' and PTJ.TransactionStatus = ''S''
and PTJ.TransactionDate between @StartDate and @TempEndDate
and PTJ.TransactionRemark <= 
CONVERT(NVARCHAR,DATEADD(D,3+(CASE WHEN DATEPART(DW, CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME))>3 THEN 2 ELSE 0 END),
CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME)),112)


DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator

END')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetEPrescStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetEPrescStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--#3 ErX - 40%
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator
select @TotParm=COUNT(DISTINCT pc.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
where AppDate between @StartDate and @EndDate and
pc.ClinicalType = ''A'' and pc.Status = ''A''
and pc.FindingDetail like ''RX-%/P-7/%''

--numerator
select @CountParm=COUNT(DISTINCT pc.patientid)
from Appointments ap
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join Resources re on re.ResourceId = ap.ResourceId1
And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and
pc.ClinicalType = ''A'' and pc.Status = ''A''
and pc.FindingDetail like ''RX-%/P-7/%''
and len(pc.ImageInstructions) > 10

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetMedicationList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetMedicationList]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN

DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator
select @TotParm=COUNT(DISTINCT ap.PatientId) from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0 
and ap.PatientId in
(select pc.PatientId 
from PatientClinical pc
where (ClinicalType = ''A'' and pc.Status = ''A'' and substring(FindingDetail, 1, 3) = ''rx-'')
or ap.PatientId in
(select PatientId from PatientNotes 
where NoteType = ''R''))

--numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate  
AND ScheduleStatus = ''D'' and AppTime > 0 
AND ap.PatientId in
(SELECT PatientId FROM PatientClinical 
WHERE ClinicalType = ''A'' and Status = ''A'' and substring(FindingDetail, 1, 3) = ''rx-'')
DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetMedicationReconStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetMedicationReconStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#15-Medication Reconclication--------------------------------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''F'' and pc.Symptom = ''/MEDS RECONCILIATION''
and pc.FindingDetail = ''*TRANSITIONOFCARE-MEDREC=T14911 <TRANSITIONOFCARE-MEDREC>''
and pc.Status = ''A''

--numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join PatientClinical pcTrans on pcTrans.AppointmentId = pc.AppointmentId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''F'' and pc.Symptom = ''/MEDS RECONCILIATION''
and pc.FindingDetail = ''*MEDRECPERFORMED-MEDREC=T14913 <MEDRECPERFORMED-MEDREC>''
and pc.Status = ''A''
and pcTrans.ClinicalType = ''F'' and pcTrans.Symptom = ''/MEDS RECONCILIATION''
and pcTrans.FindingDetail = ''*TRANSITIONOFCARE-MEDREC=T14911 <TRANSITIONOFCARE-MEDREC>''
and pcTrans.Status = ''A''

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetPatEducationMaterial]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetPatEducationMaterial]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#14---Education material > 10%------------------------------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
INNER JOIN Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0

--Numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate
and (pc.ClinicalType = ''A''
and pc.Status = ''A''
and (FindingDetail like ''%inform%''
or FindingDetail like ''%Instructions%''))
or ap.AppointmentId in (Select distinct encounterId from LoginUsers_Audit where ActionTaken like ''%Writ%'')

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetPatientEAccess]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetPatientEAccess]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#5---Timely e-Access to Patient Record------------------------------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @CountParm1 INT;
DECLARE @CountParm2 INT;
DECLARE @CountParm3 INT;
DECLARE @TotCountParm INT;
SET @TotParm=0;
SET @CountParm=0;
SET @CountParm1=0;
SET @CountParm2=0;
SET @CountParm3=0;
SET @TotCountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
INNER JOIN Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0

--Numerator
SELECT @CountParm1=COUNT(DISTINCT ap.PatientId) from PracticeTransactionJournal ptj
inner join Appointments ap on ap.AppointmentId = ptj.TransactionTypeId
inner join Resources re on re.ResourceId = ap.ResourceId1
WHERE TransactionType = ''P'' and TransactionStatus = ''Z''
and ap.AppDate between @StartDate and @EndDate and ap.AppTime > 0
and re.ResourceType in (''Q'', ''D'') and ap.ScheduleStatus = ''D''

SELECT @CountParm2=COUNT(DISTINCT ptj.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType =''P'' and PTJ.TransactionStatus = ''S''
and PTJ.TransactionDate between @StartDate and @EndDate

SELECT @CountParm3=COUNT(DISTINCT ptj.TransactionId)
from PracticeTransactionJournal PTJ
INNER JOIN Appointments ap on ap.AppointmentId=PTJ.TransactionTypeId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType =''P'' and PTJ.TransactionStatus = ''S''
and PTJ.TransactionDate between @StartDate and @EndDate 
and PTJ.TransactionRemark <= 
CONVERT(NVARCHAR,DATEADD(D,4+(CASE WHEN DATEPART(DW, CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME))>3 THEN 2 ELSE 0 END),
CAST(SUBSTRING(PTJ.TransactionDate,0,5)+ ''/'' + SUBSTRING(PTJ.TransactionDate,5,2) + ''/'' + SUBSTRING(PTJ.TransactionDate,7,2) 
AS DATETIME)),112)

SET @TotCountParm=@CountParm2 - @CountParm3

SET @CountParm=@TotParm -(@CountParm1 + @TotCountParm) 


DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetPatVitalSigns]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetPatVitalSigns]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END
--Vital signs - 50%
--denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0


--numerator
select @CountParm=COUNT(DISTINCT PC.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join PatientClinical pcBP on pcBP.AppointmentId = ap.AppointmentId
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''F'' and pc.Status = ''A''
and pc.Symptom =  ''/HEIGHT AND WEIGHT''
and pcBP.ClinicalType = ''F'' and pcBP.Status = ''A''
and pcBP.Symptom = ''/BLOOD PRESSURE''

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetProblemList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_CMS_GetProblemList]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--denominator
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1 
And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and
 ScheduleStatus = ''D'' and AppTime > 0


--numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
left outer join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''Q''
--GROUP BY re.resourceid, re.ResourceName
DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetRecordedDemographics]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetRecordedDemographics]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Record demographics - 80%
--denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0

--numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join PatientDemographics pd on pd.PatientId = ap.PatientId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and
ScheduleStatus = ''D'' and AppTime > 0
and (BirthDate <> '''' and Language <> '''' and Gender <> '''' and Race <> '''' and Ethnicity <> '''')


DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_GetReminderStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_GetReminderStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#12-Sent approp reminders to pats >= 65 or <=5-----------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
SELECT @TotParm=COUNT(DISTINCT PTJ.TransactionTypeId) from PracticeTransactionJournal PTJ
inner join PatientDemographics PD on PD.PatientId = PTJ.TransactionTypeId
inner join Resources re on re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef),0,CHARINDEX(''/'',REVERSE(PTJ.TransactionRef),0)))
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''L'' and PTJ.TransactionStatus in (''P'', ''S'')
and 
(PD.BirthDate <= PTJ.TransactionDate - 650000
or PD.BirthDate >= PTJ.TransactionDate - 50000)
and TransactionDate between @StartDate and @EndDate

--Numerator
select @CountParm=COUNT(DISTINCT PTJ.TransactionTypeId) from PracticeTransactionJournal PTJ
INNER JOIN PatientDemographics PD on PD.PatientId = PTJ.TransactionTypeId
inner join Resources re on re.ResourceName = REVERSE(SUBSTRING(REVERSE(PTJ.TransactionRef),0,CHARINDEX(''/'',REVERSE(PTJ.TransactionRef),0)))
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where PTJ.TransactionType = ''L'' and PTJ.TransactionStatus in (''S'')
and 
(PD.BirthDate <= PTJ.TransactionDate - 650000
or PD.BirthDate >= PTJ.TransactionDate - 50000)
and TransactionDate between @StartDate and @EndDate

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_HL7LabOrdersStats]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



CREATE PROCEDURE [dbo].[USP_CMS_HL7LabOrdersStats]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#11--Clinical lab results entered as structured data > 40%--------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Denominator
select @TotParm=COUNT(DISTINCT lr.ReportId)
from HL7_LabReports LR 
inner join Resources re on re.ResourceId = lr.providerid
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where CONVERT(NVARCHAR,LR.ReportDate,112) between @StartDate and @EndDate 

--Numerator
select @CountParm=COUNT(DISTINCT lr.ReportId)
from HL7_LabReports LR 
inner join HL7_Observation OBR on LR.ReportId = OBR.ReportId
inner join HL7_Observation_Details DET on OBR.OBRID = DET.OBRID 
inner join Resources re on re.ResourceId = lr.ProviderId
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where CONVERT(NVARCHAR,LR.ReportDate,112) between @StartDate and @EndDate 

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_PatientReferralStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_PatientReferralStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---#16--Summary of Care to referred dr > 50%-------------------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--denominator ***FindingDetail like %refer% might need to be changed to accomidate letter with different names
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''A'' and 
(substring(FindingDetail, 1, 34) = ''REFER PATIENT TO ANOTHER PHYSICIAN'' or
(substring(FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER'' AND FindingDetail LIKE ''%refer%'')or
substring(FindingDetail, 1, 38) = ''CLIN SUMMARY, REFER OR TRANSITION CARE'' or
substring(FindingDetail, 1, 38) = ''NO CLIN SUMM, REFER OR TRANSITION CARE'' or
substring(FindingDetail, 1, 33) = ''IN SUMM, REFER OR TRANSITION CARE'' or
substring(FindingDetail, 1, 33) = ''SUMMARY, REFER OR TRANSITION CARE'')
and pc.Status = ''A''



--numerator
select @CountParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''A'' and
(substring(FindingDetail, 1, 34) = ''REFER PATIENT TO ANOTHER PHYSICIAN'' or
(substring(FindingDetail, 1, 26) = ''SEND A CONSULTATION LETTER'' AND FindingDetail LIKE ''%refer%'')or
substring(FindingDetail, 1, 38) = ''CLIN SUMMARY, REFER OR TRANSITION CARE'' or
substring(FindingDetail, 1, 33) = ''SUMMARY, REFER OR TRANSITION CARE'' )
and pc.Status = ''A''

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_CMS_SmokeStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_CMS_SmokeStatus]
(
@StartDate NVARCHAR(200),
@EndDate NVARCHAR(200),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

--Smoking Status - 50% patients >= 13 yrs
--denominator
select @TotParm=COUNT(DISTINCT ap.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PatientDemographics pd on pd.PatientId=ap.PatientId AND pd.BirthDate < ap.appdate - 130000
where AppDate between @StartDate and @EndDate and
ScheduleStatus = ''D'' and AppTime > 0


--numerator
select @CountParm=COUNT(DISTINCT pc.PatientId)
from Appointments ap
inner join Resources re on re.ResourceId = ap.ResourceId1
AND re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PatientDemographics pd on pd.PatientId=ap.PatientId AND pd.BirthDate < ap.AppDate - 130000
inner join PatientClinical pc on pc.AppointmentId = ap.AppointmentId
where AppDate between @StartDate and @EndDate and 
ScheduleStatus = ''D'' and AppTime > 0
and pc.ClinicalType = ''F'' and pc.Status = ''A''
and pc.Symptom = ''/SMOKING''

DROP TABLE #TMPResources

SELECT @TotParm as Denominator,@CountParm as Numerator
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_Del_Immunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

Create procedure [dbo].[USP_Del_Immunizations](  
	@Patientid    varchar(50),  
	@Encounterid varchar(50),  
	@Immid   varchar(50))  
as  
begin   
 delete from patient_immunizations where patientid=@Patientid and Encounterid=@Encounterid and Immid=@Immid  
end

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ActivityList]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_DI_ActivityList]
	(
	@PatId int,
	@CurDate nchar(8),
	@Status nchar(15)
	)
AS

SET NOCOUNT ON

select pa.ActivityId, pa.AppointmentId
	, app.AppDate, app.AppTime, app.AppTypeId, app.Comments
	, apt.AppointmentType
from PracticeActivity pa
	inner join Appointments app on pa.AppointmentId = app.AppointmentId
	inner join AppointmentType apt on app.AppTypeId = apt.AppTypeId
where pa.PatientId = @PatId
	and pa.Status in (select nstr from CharTable(@Status, '','')) 
	and app.Comments <> ''ADD VIA BILLING''
order by pa.ActivityDate desc


-- Turn NOCOUNT back OFF
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_DocumentImages]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_DocumentImages]
(
@PatId int
)
AS
SET NOCOUNT ON
SELECT pc.clinicalid, pc.symptom, pc.drawfilename, pc.AppointmentId, ap.AppDate 
FROM PatientClinical pc 
INNER JOIN Appointments ap
ON pc.AppointmentId = ap.AppointmentId And pc.PatientId = ap.PatientId
WHERE 
	pc.PatientId = @PatId And
	pc.ClinicalType = ''I'' And
	pc.Status = ''A''
ORDER BY ap.AppDate DESC, pc.ClinicalId ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamContent]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ExamContent]
(
@PatId int,
@AptId int,
@AptDate varchar(8)
)
AS
SET NOCOUNT ON
SELECT pc.*, ap.AppDate, rs.ResourceName FROM PatientClinical pc 
INNER JOIN Appointments ap
ON pc.AppointmentId = ap.AppointmentId And pc.PatientId = ap.PatientId
INNER JOIN Resources rs
ON ap.ResourceId1 = rs.ResourceId
WHERE 
	ap.AppDate <= @AptDate And 
	pc.PatientId = @PatId And
	((pc.ClinicalType In (''C'', ''H'')) Or
	((pc.ClinicalType in (''F'', ''I'', ''Q'')) And (pc.AppointmentId = @AptId)) Or
	((pc.ClinicalType = ''A'') And (Left(FindingDetail,2) = ''RX'')))
ORDER BY pc.ClinicalType ASC, ap.AppDate DESC, pc.ClinicalId ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamHighlightImprs]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ExamHighlightImprs]
(
@AptId int
)
AS
SET NOCOUNT ON
SELECT pc.*, ap.AppDate, rs.ResourceName  FROM PatientClinical pc 
INNER JOIN Appointments ap 
ON pc.AppointmentId = ap.AppointmentId And pc.PatientId = ap.PatientId
INNER JOIN Resources rs
ON ap.ResourceId1 = rs.ResourceId
WHERE  (pc.AppointmentId = @AptId) And (pc.ClinicalType = ''U'')
ORDER BY pc.Symptom ASC, pc.EyeContext ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamHighlights]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ExamHighlights]
(
@PatId int,
@AptDate varchar(8)
)
AS
SET NOCOUNT ON
SELECT pc.*, ap.AppDate, rs.ResourceName FROM PatientClinical pc 
INNER JOIN Appointments ap
ON pc.AppointmentId = ap.AppointmentId And pc.PatientId = ap.PatientId
INNER JOIN Resources rs
ON ap.ResourceId1 = rs.ResourceId
WHERE 
	(ap.AppDate <= @AptDate And 
	 pc.PatientId = @PatId And
	 pc.ClinicalType In (''A'',''Q'',''P'',''C'',''H'',''F'') And
	 pc.Highlights In (''A'',''Q'',''P'',''C'',''H'',''T'',''R'') And
	 pc.Status = ''A'') OR
	((pc.ClinicalType = ''A'') And (Left(FindingDetail,2) = ''RX'') And
	  ap.AppDate <= @AptDate And 
	  pc.PatientId = @PatId)
ORDER BY pc.ClinicalType ASC, pc.Highlights ASC, ap.AppDate DESC, pc.ClinicalId ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamNoteContent]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ExamNoteContent]
(
@PatId int,
@AptId int,
@AptDate varchar(8)
)
AS
SET NOCOUNT ON
SELECT pn.*, ap.AppDate FROM PatientNotes pn
INNER JOIN Appointments ap
ON ap.AppointmentId = pn.AppointmentId
WHERE
	ap.AppDate <= @AptDate And 
	pn.PatientId = @PatId And
	pn.NoteType in (''C'',''R'')  And
	(((pn.NoteSystem >= ''1'') And (pn.NoteSystem <=''9'') Or (pn.NoteSystem = ''10'')) Or
	 ((Left(pn.NoteSystem,1) = ''T'') And (pn.AppointmentId = @AptId)) Or
	 ((LEN(LTRIM(pn.NoteSystem)) > 2) And (pn.AppointmentId = @AptId)) Or
	 (pn.NoteType = ''R''))
	ORDER BY ap.AppDate DESC, pn.NoteSystem ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamPatInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_DI_ExamPatInfo]
	(
	 @ActId int
	)
AS
SET NOCOUNT ON
select top 1 * from (
select top 2 app1.apptinstype, patientinsurance.instypeone,
pa1.activitydate, pa1.patientid, pa1.questionset, pa1.appointmentid
, pd.firstname, pd.middleinitial, pd.lastname, pd.namereference, pd.policypatientid, pd.secondpolicypatientid, pd.birthdate, pd.gender, pd.nationalorigin, pd.pattype, pd.referringphysician
, app1.visitreason, app1.appdate, app1.apptime, app1.apptypeid
, case when pv.vendorname is null then '''' else pv.vendorname end as VendorName
, apt.appointmenttype, apt.drrequired, apt.embasis, apt.question
, case when exists(select * from patientnotes where notetype = ''P'' and patientid = pa1.patientid) then ''Yes'' else ''No'' end as RedNotes
, case when exists(select * from practiceactivity where activitydate <= pa1.activitydate and patientid = pa1.patientid) then case pa2.activityid when @ActId then 0 else pa2.appointmentid end else 0 end as PreviousAppointmentId
, case when PatientInsurance.primaryinsurerone is null then '''' else patientinsurance.primaryinsurerone end as PrimaryInsurer
, case when PatientInsurance.PrimaryBusinessClass is null then '''' else PatientInsurance.PrimaryBusinessClass end as PrimaryBusinessClass
, case when PatientInsurance.InsurerCPTClass is null then '''' else PatientInsurance.InsurerCPTClass end as InsurerCPTClass
, case when PatientInsurance.PrimaryInsurerTwo is not null then PatientInsurance.PrimaryInsurerTwo when PatientInsurance.PrimaryInsurerTwo is null and PatientInsurance.SecondaryInsurerOne is not null then PatientInsurance.SecondaryInsurerOne else '''' end as SecondaryInsurer
from practiceactivity pa1
inner join patientdemographics pd on pa1.patientid = pd.patientid
inner join appointments app1 on pa1.appointmentid = app1.appointmentid
left outer join practicevendors pv on pd.referringphysician = pv.vendorid
inner join appointmenttype apt on app1.apptypeid = apt.apptypeid
left outer join practiceactivity pa2 on pa1.patientid = pa2.patientid
left outer join appointments app2 on pa2.appointmentid = app2.appointmentid
left outer join (select pd1.patientid as PatientId, pi1.insurername as PrimaryInsurerOne, pf1.FinancialInsType as InsTypeOne, pi1.insurercptclass as InsurerCPTClass, pi1.insurerbusinessclass as PrimaryBusinessClass, pi2.insurername as PrimaryInsurerTwo, pf2.FinancialInsType as InsTypeTwo, pi3.insurername as SecondaryInsurerOne, pf3.FinancialInsType as InsTypeThree
						from patientdemographics pd1
						left outer join patientfinancial pf1 on pd1.policypatientid = pf1.patientid and pf1.financialpindicator = 1
						left outer join patientfinancial pf2 on pd1.policypatientid = pf2.patientid and pf2.financialpindicator = 2
						left outer join patientfinancial pf3 on pd1.secondpolicypatientid = pf3.patientid and pf3.financialpindicator = 1
						left outer join practiceinsurers pi1 on pf1.financialinsurerid = pi1.insurerid
						left outer join practiceinsurers pi2 on pf2.financialinsurerid = pi2.insurerid
						left outer join practiceinsurers pi3 on pf3.financialinsurerid = pi3.insurerid
						where (pf1.status = ''C'' or pf1.status is null)
						and (pf2.status = ''C'' or pf2.status is null)
						and (pf3.status = ''C'' or pf3.status is null)
					) AS PatientInsurance on pd.patientid = patientinsurance.patientid and app1.ApptInsType = PatientInsurance.InsTypeOne
where pa1.activityid = @ActId and (pa2.activitydate <= pa1.activitydate or pa2.activitydate is null) 
and (app2.comments <> ''ADD VIA BILLING'' or app2.comments is null)
and (pa2.status in (''D'', ''G'', ''H'', ''W'', ''M'', ''E'') or pa2.status is null)
order by pa2.activitydate desc, app2.apptime desc
) as Top2
order by PreviousAppointmentId desc

-- Turn NOCOUNT back OFF
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ExamPrevDiags]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ExamPrevDiags]
@AptId int,
@Eye varchar(2)
AS
SET NOCOUNT ON
SELECT * FROM PatientClinical pc 
WHERE 
	pc.AppointmentId = @AptId And 
	pc.ClinicalType in (''Q'', ''I'') And
	((pc.EyeContext = @Eye) Or (@Eye = ''0''))
ORDER BY pc.ClinicalType DESC, pc.ClinicalId ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_PrevImprPlan]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_PrevImprPlan]
(
@AptId int
)
AS
SET NOCOUNT ON
SELECT * FROM PatientClinical pc 
WHERE 
	pc.AppointmentId = @AptId And 
	pc.Status = ''A'' And
	(pc.ClinicalType in (''Q'', ''U'', ''A''))
ORDER BY pc.ClinicalType ASC, pc.EyeContext, pc.ClinicalId ASC
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ProcsBilled]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ProcsBilled]
	(
	@PatId int,
	@CurDate nchar(8),
	@TstId nchar(10)
	)
AS
SET NOCOUNT ON
select distinct pr.patientid, ps.service, ap.appointmentid 
From PatientReceivableServices ps
inner join PatientReceivables pr
On ps.Invoice = pr.Invoice
inner join Appointments ap 
On ap.AppointmentId = pr.AppointmentId 
where ps.service = @TstId and ps.Status = ''A'' and 
pr.patientid = @PatId and  ap.AppDate >= @CurDate
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_DI_ProcsPerformed]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_DI_ProcsPerformed]
	(
	@PatId int,
	@CurDate nchar(8),
	@TstId nchar(64)
	)
AS
SET NOCOUNT ON
select distinct ap.appointmentid, pc.patientid, pc.symptom 
From PatientClinical pc
inner join Appointments ap 
On ap.AppointmentId = pc.AppointmentId 
where pc.clinicaltype = ''F'' and pc.symptom = @TstId and pc.Status = ''A'' and 
pc.patientid = @PatId and  ap.AppDate >= @CurDate
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_EPgetRenewalRequest]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

Create Procedure [dbo].[usp_EPgetRenewalRequest]  
as   
BEGIN
select renewalRequestId  from Patient_EPrescription_RenewalRequest where ProcessStatus is null or ProcessStatus<>''OK''
END
     
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_EPInsUpd_PatEPrescription_RenewalRequest]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE Procedure [dbo].[usp_EPInsUpd_PatEPrescription_RenewalRequest]    
(
@PatientID as varchar(10),    
@RenewalRequestGuid varchar(100),    
@ReceivedTimestamp varchar(100),    
@LocationName varchar(100),    
@DoctorFullName varchar(100),    
@PharmacyInfo varchar(100),    
@PharmacyFullInfo varchar(100),    
@PharmacyStoreName varchar(100),    
@PatientFirstName varchar(100),    
@PatientMiddleName varchar(100),    
@PatientLastName varchar(100),    
@PatientDOB varchar(100),    
@PatientGender varchar(100),    
@DrugInfo varchar(100),    
@NumberOfRefills varchar(100),    
@ExternalLocationId varchar(100),    
@ExternalDoctorId varchar(100),    
@ExternalPatientId varchar(100),    
@ExternalPrescriptionId varchar(100),    
@Quantity varchar(100),    
@Sig varchar(100),    
@NcpdpId varchar(100),    
@Spare1 varchar(100)    
)    
As 
BEGIN   
if not exists (Select * from Patient_EPrescription_RenewalRequest Where renewalRequestId=@RenewalRequestGuid)    
BEGIN
     INSERT INTO Patient_EPrescription_RenewalRequest(renewalRequestId, PatientID,DrugName,PharmacyName,Location,DoctorName,PatientName,CreatedDate)     
     Values(@RenewalRequestGuid, @PatientID,@DrugInfo,@PharmacyStoreName,@LocationName,@DoctorFullName,@PatientLastName + '' '' + @PatientFirstName,GETDATE())      
     END
--ELSE    
--     Update Patient_EPrescription_RenewalRequest set PatientID=@PatientID,  
END     
     
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_EPrescription_Patient_RenewalRequest]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE Procedure [dbo].[usp_EPrescription_Patient_RenewalRequest]  
(
@IsRenewalReq bit
)
  As  
  Declare @Str Varchar(1000)
  begin 
  SET @Str=''Select renewalRequestId,PatientID,PatientName,DrugName,PharmacyName,Location,DoctorName,Isnull(AcceptDenyStatus,'''''''') as AcceptDenyStatus,ISNULL(ProcessStatus,'''''''') as ProcessStatus from Patient_EPrescription_RenewalRequest ''
  if @IsRenewalReq <> 0 
  SET @Str= @Str + ''where ProcessStatus = ''''OK''''''
  else
  SET @Str= @Str + ''where ProcessStatus <> ''''OK'''' OR ProcessStatus is null''
 -- Print @Str 
  Exec (@Str)
    end 


')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_EPUpdateRenewalRequest]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

     
CREATE Procedure [dbo].[usp_EPUpdateRenewalRequest]    
(
@renewalRequestId varchar(100),    
@PharmacyNCPDP varchar(100),    
@ResponseCode varchar(100),    
@ResponseCodeDetail varchar(100),    
@ResponseFreeText varchar(100),    
@ResponseTimestamp varchar(100),    
@ResponseNumberOfRefills varchar(100),    
@ResponseDrugId varchar(100),    
@ResponseDrugTypeId varchar(100),    
@SentTimestamp varchar(100),    
@SentStatus varchar(100),    
@SentStatusMessage varchar(100),    
@PrescriptionGuid varchar(100)    
)    
As    
Declare @Str Varchar(1000)  
Begin     
    
 set @Str= ''Update Patient_EPrescription_RenewalRequest Set ProcessStatus='''''' + @SentStatus + '''''',''   
  if Upper(@ResponseCode) = ''D''    
  set @Str = @Str + ''AcceptDenyStatus = ''''Deny'''',''  
  else if Upper(@ResponseCode)= ''A''    
  set @Str = @Str + ''AcceptDenyStatus =''''Accept'''',''  
  else if Upper(@ResponseCode)= ''N''    
  set @Str = @Str + ''AcceptDenyStatus =''''New Rx'''',''  
      
 if @ResponseCodeDetail<>''''    
  set @Str = @Str + ''Comments='''''' + @ResponseCodeDetail + ''''  
  else     
  set @Str = @Str + ''Comments='''''' + @ResponseFreeText + ''''  
      
  set @Str = @Str + '' where renewalRequestId='' + @renewalRequestId    
End     
  
  
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_FinancialData]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_FinancialData]
(
@PatId int,
@InvId nvarchar(16),
@Bal int
)
AS
SELECT
pr.ReceivableId, pr.Invoice, 
pr.InvoiceDate, pr.AppointmentId,
pr.OpenBalance, pr.Charge,
pf.Referral, 
pr.BillToDr as BillDrId, 
bd.ResourceName as BillDrName,
pr.BillingOffice as SchAttId,
ap.ResourceId1 as SchDrId,
sd.ResourceName as SchDrName,
ap.ResourceId2 as SchLocId,
pr.ReferDr as RefDrId,
rf.VendorLastName as RefDrName,
pr.InsurerId, pl.InsurerName
FROM PatientReceivables pr 
INNER JOIN PatientDemographics pd ON pd.PatientId=pr.PatientId 
INNER JOIN Appointments ap ON pr.AppointmentId=ap.AppointmentId 
LEFT JOIN PatientReferral pf ON ap.ReferralId=pf.ReferralId
LEFT JOIN PracticeInsurers pl ON pr.InsurerId=pl.InsurerId
LEFT JOIN Resources as bd ON pr.BillToDr=bd.ResourceId 
LEFT JOIN Resources as sd ON ap.ResourceId1=sd.ResourceId 
LEFT JOIN PracticeVendors as rf ON pr.ReferDr=rf.VendorId 
WHERE ((pr.OpenBalance <> @bal)  OR (@bal = -1))
AND ((pr.Invoice = @InvId) OR (@InvId = ''-1''))
AND (pr.PatientId = @PatId) 
ORDER BY pr.InvoiceDate DESC, pr.Invoice DESC, pr.InsurerDesignation DESC


')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_FinancialData6]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_FinancialData6]
(
@PatId int,
@InvId nvarchar(16),
@Bal int
)
AS
SELECT distinct
pr.Invoice, 
pr.InvoiceDate, pr.AppointmentId,
pf.Referral, 
pr.BillToDr as BillDrId, 
bd.ResourceName as BillDrName,
pr.BillingOffice as SchAttId,
ap.ResourceId1 as SchDrId,
sd.ResourceName as SchDrName,
ap.ResourceId2 as SchLocId,
bd.ResourceType,
pr.ReferDr as RefDrId,
rf.VendorLastName as RefDrName
FROM PatientReceivables pr 
INNER JOIN PatientDemographics pd ON pd.PatientId=pr.PatientId 
INNER JOIN Appointments ap ON pr.AppointmentId=ap.AppointmentId 
LEFT JOIN PatientReferral pf ON ap.ReferralId=pf.ReferralId
LEFT JOIN PracticeInsurers pl ON pr.InsurerId=pl.InsurerId
LEFT JOIN Resources as bd ON pr.BillToDr=bd.ResourceId 
LEFT JOIN Resources as sd ON ap.ResourceId1=sd.ResourceId 
LEFT JOIN PracticeVendors as rf ON pr.ReferDr=rf.VendorId 
WHERE ((pr.OpenBalance <> @bal)  OR (@bal = -1))
AND ((pr.Invoice = @InvId) OR (@InvId = ''-1''))
AND (pr.PatientId = @PatId) 
ORDER BY pr.InvoiceDate DESC, pr.Invoice DESC

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_FinancialDataDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_FinancialDataDetails]
	(
	@InvId nvarchar(32)
	)
AS

SELECT * , ps.charge as ps_charge FROM PatientReceivables pr 
LEFT JOIN PatientReceivableServices ps ON pr.Invoice=ps.Invoice
LEFT JOIN PatientReceivablePayments pp ON pr.ReceivableId=pp.ReceivableId
WHERE pr.Invoice=@InvId
AND (ps.Status <> ''X'')
ORDER BY ps.ItemOrder ASC, pr.Invoice DESC, pr.InsurerDesignation DESC

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_FinancialDataDetails6]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE 
PROCEDURE [dbo].[usp_FinancialDataDetails6]
	(
	@InvId nvarchar(32)
	)
AS

SELECT ps.* , ps.charge as ps_charge FROM PatientReceivables pr 
LEFT JOIN PatientReceivableServices ps ON pr.Invoice=ps.Invoice
LEFT JOIN PatientReceivablePayments pp ON pr.ReceivableId=pp.ReceivableId
WHERE pr.Invoice=@InvId
AND (ps.Status <> ''X'')
ORDER BY ps.ItemOrder ASC, pr.Invoice DESC, pr.InsurerDesignation DESC

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_Get_Pat_Immunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_Get_Pat_Immunizations](        
@Encounterid  varchar(50))        
as        
declare        
@Sqlstr  varchar(1000)        
  begin        
 set @Sqlstr=''select dategiven Dategiven,Immid,I.IMMNAME,initials,site,dosage,        
 manuname,reaction,lotid,         
 IsNull(Convert(Varchar(10),lotexpdate,101),'''''''') ExpDate,vis,isnull(Dos_units,'''''''') Dos_Units,Manufacturer_Code,ImmCode from patient_immunizations PI,        
 IMMUNIZATIONS I where PI.EncounterID =''+ @Encounterid +''  and dategiven is not null AND IMMNO=IMMID''           
    
  exec (@Sqlstr)          
end   
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPAccountInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPAccountInfo]
AS
BEGIN

select AccountID  as AccountID, rtrim(ltrim(replace(PracticeName,''&'',''''))) as AccountName,1 as AccountSiteID, 
rtrim(ltrim(replace(PracticeAddress,''&'',''''))) as Address1, rtrim(ltrim(replace(PracticeSuite,''&'',''''))) as Address2,
UPPER(PracticeState) as State, rtrim(ltrim(PracticeCity)) as City,replace(substring(PracticeZip,1,5),''&'','''') as Zip,
replace(PracticePhone,''&'','''') as ''Phone'',replace(PracticeFax,''&'','''') as ''Fax'',''US'' as Country 
from PracticeName 
cross join EPrescription_Credentials
where PracticeType = ''P'' and LocationReference = ''''

--select 1 as AccountID, rtrim(ltrim(PracticeName)) as AccountName,1 as AccountSiteID, 
--rtrim(ltrim(PracticeAddress)) as Address1, rtrim(ltrim(PracticeSuite)) as Address2,
--UPPER(PracticeState) as State, rtrim(ltrim(PracticeCity)) as City,substring(PracticeZip,1,5) as Zip,
--PracticePhone as ''Phone'',PracticeFax as ''Fax'',''US'' as Country from PracticeName where PracticeId=2
END


')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPCredentials]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPCredentials]
AS
BEGIN
select Partnername,Username,password,siteid,url,''IO PRACTICEWARE'' as ProductName, ''V7.0'' as productVersion ,Update1URL from EPrescription_Credentials
END



')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPLocationInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPLocationInfo]
(
@Filter Nvarchar(100)
)
AS
BEGIN
select PracticeId as ''LocationID'',rtrim(ltrim(replace(PracticeName,''&'',''''))) as ''LocationName'', 
rtrim(ltrim(replace(Practiceaddress,''&'',''''))) as ''Address1'',rtrim(ltrim(replace(Practicesuite,''&'',''''))) as ''Address2'',
rtrim(ltrim(Practicecity)) as ''City'',UPPER(Practicestate) as ''State'',substring(Practicezip,1,5) as ''Zip'',
replace(Practicephone,''&'','''') as ''phone'',replace(Practicefax,''&'','''') as ''fax'',replace(Practicephone,''&'','''') as ''PrimaryContactNumber'', ''US'' as ''Country'' 
from PracticeName where PracticeId=@Filter 
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPPatientInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPPatientInfo]
(
@Filter NVarchar(100)
)
AS
BEGIN
select patientid, rtrim(ltrim(replace(replace(LastName,''"'',''''),''&'',''''))) as Lastname,rtrim(ltrim(replace(replace(firstname,''"'',''''),''&'',''''))) as Firstname,replace(MiddleInitial,''&'','''') as MI, 
replace(NameReference,''.'','''')as Suffix, '''' as prefix, 
patientid as ''EMRNumber'',replace(SocialSecurity,''-'','''') as ''SSN'', 
replace(address,''&'','''') as ''address1'','''' as ''address2'',replace(city,''&'','''') as city,upper(state) as state,REPLACE(substring(zip,1,5),''&'','''') as Zip,''US'' as country,
replace(HomePhone,''&'','''') as ''HomePhoneNumber'',Gender, 
convert(varchar(16),BirthDate,112)as DOB,'''' as ''memo'' from patientdemographics  where patientid=@Filter
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPPrescriberInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPPrescriberInfo]
(
@Filter int
)
AS
BEGIN
select ResourceId as ''LicensedPrescriberId'',
rtrim(ltrim(ResourceDEA)) as DEA,rtrim(ltrim(ResourceUPIN)) as upin,
UPPER(ResourceState) as State,rtrim(ltrim(ResourceLicence)) as License,
rtrim(ltrim(replace(replace(ResourceLastName,''"'',''''),''&'',''''))) as LastName,rtrim(ltrim(replace(replace(ResourceFirstName,''"'',''''),''&'',''''))) as FirstName,rtrim(ltrim(replace(replace(ResourceMI,''"'',''''),''&'',''''))) as MI,
'''' as prefix,rtrim(ltrim(ResourceSuffix)) as suffix,rtrim(ltrim(NPI)) as NPI ,'''' as freeformCredentials from Resources where ResourceID=@Filter
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetEPStaffInfo]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetEPStaffInfo]
(
@Filter NVARCHAR(100)
)
AS
BEGIN
select ResourceId as ''StaffId'',
rtrim(ltrim(ResourceLicence)) as StaffLicense,
rtrim(ltrim(replace(ResourceLastName,''"'',''''))) as LastName,rtrim(ltrim(replace(ResourceFirstName,''"'',''''))) 
as FirstName,isnull(rtrim(ltrim(replace(ResourceMI,''"'',''''))),'''') as MI,
isnull(rtrim(ltrim(ResourceSuffix)),'''') as suffix,'''' as prefix,ResourceType
from Resources where ResourceID=@Filter
END


')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetMeds]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_GetMeds]
(
@ClinicalIdList NVARCHAR(200),
@PatientID NVARCHAR(200)
)
AS
SET NOCOUNT ON

CREATE TABLE #RxSegments
(
	ClinicalId int,
	SegTwo int,
	SegThree int,
	SegFour int,
	SegFive int,
	SegSix int,
	SegSeven int,
	SegNine int,
	LegacyDrug bit
)

INSERT INTO #RxSegments
SELECT pc.clinicalid
	, CHARINDEX(''-2/'', pc.FindingDetail) + 3
	, CHARINDEX(''-3/'', pc.FindingDetail) + 3
	, CHARINDEX(''-4/'', pc.FindingDetail) + 3
	, CHARINDEX(''-5/'', pc.FindingDetail) + 3
	, CHARINDEX(''-6/'', pc.FindingDetail) + 3
	, CHARINDEX(''-7/'', pc.FindingDetail) + 3
	, CHARINDEX(''9-9/'', pc.FindingDetail)
	, CASE WHEN pc.FindingDetail like ''%()%'' THEN 0 ELSE 1 END 
FROM PatientClinical pc
WHERE pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
	and pc.PatientId = @PatientID
	
select pc.ClinicalId as externalId,

	CASE WHEN pc.FindingDetail like ''RX-8/%-2/%'' 
		THEN Replace (Replace(SUBSTRING(pc.findingdetail, 6, charindex(''-2/'', pc.findingdetail) - 6),''&'',''and''), ''+'',''Plus'') 
	ELSE '''' END AS DrugName,
	CASE WHEN LEN(pc.ImageDescriptor) > 0
		THEN RIGHT(pc.ImageDescriptor, 4) + SUBSTRING(pc.ImageDescriptor, LEN(pc.ImageDescriptor) - 9, 2) 
		+ SUBSTRING(pc.ImageDescriptor, len(pc.ImageDescriptor) - 6, 2)
	ELSE '''' END as Date,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegTwo + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegTwo) - rx.SegTwo - 1)
	ELSE ''''
		END as Dosage,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1) - 1) 
	ELSE ''''
		END as Strength,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1))) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1)))) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegTwo+1))) - 1)
	ELSE ''''
		END as Eye,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, rx.SegThree + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegThree) - rx.SegThree - 1)
	ELSE ''''
		END as Frequency,
	CASE rx.LegacyDrug WHEN 0 THEN 
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegThree+1) - 1)
	ELSE ''''
		END as PRN,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)) - 1)
	ELSE ''''
		END as PO,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegThree+1)+1)+1)) - 1)
	ELSE ''''
		END as OTC,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFour + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegFour) - rx.SegFour - 1)
	ELSE ''''
		END as Refills,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegFour+1) - 1)
	ELSE ''''
		END as DAW,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)) - 1)
	ELSE ''''
		END as PillsPackage,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour)+1)+1)) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)+1)) - CHARINDEX(''('', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, CHARINDEX('')'', pc.FindingDetail, rx.SegFour+1)+1)+1)) - 1)
	ELSE ''''
		END as LastTaken,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, rx.SegFive + 1, CHARINDEX('')'', pc.FindingDetail, rx.SegFive) - rx.SegFive - 1)
	ELSE ''''
		END as Duration,
	CASE rx.LegacyDrug WHEN 0 THEN
		SUBSTRING(pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) + 1, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1)) - CHARINDEX(''('', pc.FindingDetail, rx.SegFive+1) - 1)
	ELSE ''''
		END as DaysSupply,
	CONVERT(NVARCHAR(100),pc.DrawFileName) as FormulationID, 		
	CASE WHEN rx.SegNine > 0
			THEN ''Pending''
		ELSE '''' END as PrescriptionStatus,
	replace(pn1.note1+pn1.note2+pn1.note3+pn1.note4, ''&'', '','') as SigNote,
	replace(pn2.note1+pn2.note2+pn2.note3+pn2.note4, ''&'', '','') as PharmNote,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pc.FindingDetail like ''%(PILLS-PACKAGE %''
			THEN CASE WHEN CHARINDEX('' '', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15) > 0
				THEN SUBSTRING(pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '', pc.FindingDetail)+15, CHARINDEX('' '', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15)-(CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15))
				ELSE SUBSTRING(pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '', pc.FindingDetail)+15, CHARINDEX('')'', pc.FindingDetail, CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15)-(CHARINDEX(''(PILLS-PACKAGE '',pc.FindingDetail)+15))
				END
		ELSE '''' 
		END	
	END AS dispenseNumber,
	CASE rx.LegacyDrug WHEN 1 THEN
		CASE WHEN pn1.Note1 IS NULL 
			THEN CASE WHEN pc.FindingDetail like ''rx-8%-2/(%'' THEN Substring(pc.findingdetail, CHARINDEX(''-2/'',pc.findingdetail)+4, charindex('')'', pc.findingdetail, CHARINDEX(''-2/'',pc.findingdetail)+4)-CHARINDEX(''-2/'',pc.findingdetail)-4)
			ELSE '''' END + '' '' + CASE WHEN pc.FindingDetail like ''rx-8%-2/(%(O%-3/%'' THEN Substring(pc.findingdetail, CHARINDEX('')-3/'',pc.findingdetail)-2,  2) 
			ELSE ''''	END	 + '' '' + CASE WHEN pc.FindingDetail NOT like ''rx-8%-3/-4/%''  
			THEN SUBSTRING(pc.findingdetail ,CHARINDEX(''-3/('',pc.findingdetail)+4,CHARINDEX('')'',pc.findingdetail, charindex(''-3/('',pc.findingdetail))-CHARINDEX(''-3/('',pc.findingdetail)-4 )
			ELSE '''' END	+ '' '' + CASE WHEN pc.FindingDetail like ''rx-8/%-3/(%)%(%)-4/%'' THEN SUBSTRING(pc.findingdetail, CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)+1, charindex('')-4/'', pc.findingdetail)-CHARINDEX(''('',pc.findingdetail, CHARINDEX(''-3/('',pc.findingdetail)+4)-1)
		ELSE '''' END	ELSE ''AS PRESCRIBED'' END 
	ELSE '''' END AS sig, 
	CASE rx.LegacyDrug WHEN 1 THEN
		case WHEN pc.FindingDetail like ''%(PRN)%'' THEN '''' 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS%'' and pc.FindingDetail not like ''rx-8%REFILLS %(%-5/%'' 
				THEN SUBSTRING	(pc.findingdetail, charindex(''-4/REFILLS '',pc.findingdetail)+11, charindex(''-5/'', pc.findingdetail)- charindex(''-4/REFILLS '',pc.findingdetail)-11) 
			 WHEN pc.FindingDetail like ''rx-8%REFILLS %(%)-5/%'' THEN SUBSTRING(pc.findingdetail, charindex(''-4/REFILLS'',pc.findingdetail)+11, CHARINDEX(''('',pc.findingdetail, charindex(''-4/REFILLS'', pc.FindingDetail))-charindex(''-4/REFILLS'',pc.findingdetail)-11) 
			 ELSE '''' END	
	ELSE '''' END AS refillCount,
	rx.LegacyDrug as LegacyDrug
	
	 ,pc.FindingDetail
	 ,pc.ImageInstructions as PrescriptionGuid
	 ,pc.ImageDescriptor
	FROM PatientClinical pc 
		INNER JOIN #RxSegments rx on pc.ClinicalId = rx.ClinicalId
		LEFT OUTER JOIN PatientNotes pn1 on pn1.AppointmentId = pc.AppointmentId 
			AND Substring(PC.FindingDetail, 6, CHARINDEX(''-2/'',pc.findingdetail)-6) = pn1.NoteSystem 
			AND pn1.NoteType = ''R''
		LEFT OUTER JOIN PatientNotes pn2 on pn2.AppointmentId = pc.AppointmentId
			AND SUBSTRING(pc.FindingDetail, 6, CHARINDEX(''-2/'', pc.findingdetail)-6) = pn2.NoteSystem
			AND pn2.NoteType = ''X''
			
WHERE pc.ClinicalId in (select number from IntTable(@ClinicalIdList))
	and pc.PatientId = @PatientID
SET NOCOUNT OFF

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetPatientAllergyDet]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_GetPatientAllergyDet]
(
@PatientID nvarchar(100)
)
AS
BEGIN
	select CASE SUBSTRING(findingdetail, 1, 1)
			WHEN ''*''
				THEN CASE
					WHEN FindingDetail like ''%=%''
						THEN SUBSTRING(findingdetail, 2, CHARINDEX(''='', findingdetail) - 2)
					ELSE
						SUBSTRING(findingdetail, 2, LEN(findingdetail)-1)
					END
			ELSE CASE
					WHEN FindingDetail like ''%=%''
						THEN SUBSTRING(findingdetail, 1, CHARINDEX(''='', findingdetail) - 1)
					ELSE
						SUBSTRING(findingdetail, 1, LEN(findingdetail))
					END
			END AS AllergyName
		, DrawFileName AS AllergyId
		, CASE drawfilename
			WHEN ''''
				THEN ''''
			ELSE
				''FDB''
		END AS AllergySource
	From PatientClinical 
	Where ClinicalType in (''h'', ''c'')
		and Symptom like ''%allerg%''
		and PatientId = @PatientID
		and Status = ''a''

	UNION

	Select substring(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Note1 + Note2 + Note3 + Note4,'';'','',''),''+'',''PLUS''),''?'','' ''),''%'','',''),''-'',''''),CHAR(10), '',''),CHAR(13) , ''''), 1, 70) AS AllergyName, '''' AS AllergyId, '''' AS AllergySource
	From PatientNotes 
	Where PatientId = @PatientID
		and NoteType = ''C''
		and NoteSystem = ''9'' 
END

/**11**** Object:  StoredProcedure [dbo].[usp_GetPatientDet]    Script Date: 12/15/2010 18:19:22 ******/
SET ANSI_NULLS ON
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetPatientDet]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_GetPatientDet]
(
@Filter NVARCHAR(100)
)
AS
BEGIN
DECLARE @SQLSTR NVARCHAR(200)
if @Filter <> ''''
BEGIN
SET @SQLSTR=''SELECT * FROM PatientDemoGraphics WHERE ''+@Filter+''''
END
ELSE
BEGIN
SET @SQLSTR=''SELECT * FROM PatientDemoGraphics''
END
EXEC (@SQLSTR)
END


')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_GetSigMapData]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_GetSigMapData]
(
@ReferenceType NVARCHAR(200),
@Filter NVARCHAR(200)
)
AS
BEGIN
if @ReferenceType=''DOSAGEFORMTYPE'' 
BEGIN
SELECT * from PracticeCodeTable where ReferenceType=@ReferenceType AND Code LIKE ''''+@Filter+''%''
END
ELSE
BEGIN
SELECT * from PracticeCodeTable where ReferenceType=@ReferenceType AND Code=@Filter
END
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_Insert_Immunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE procedure [dbo].[USP_Insert_Immunizations](      
 @Patientid   varchar(20),      
 @Encounterid  varchar(20),      
 @immid   varchar(10),      
 @Dategiven   Datetime,      
 @Expdate     datetime,      
 @Site        varchar(100),      
 @dosage   varchar(30),      
 @Reaction    varchar(1000),      
 @Initials    Varchar(100),      
 @ManuName    varchar(100),      
 @Dos_Units   varchar(100),      
 @Vis         varchar(50),      
 @LotNo       varchar(100),      
 @ImmName  varchar(100),  
 @Manufact_Code  varchar(10))      
as       
begin      
 if  not exists(select patientid from patient_immunizations where patientid=@Patientid and EncounterID=@Encounterid and ImmID=@immid )       
  begin      
   insert into patient_immunizations(PatientID,EncounterID,ImmID,DateGiven,LotExpDate,Site,      
   Reaction,ManuName,Dosage,Dos_Units,Vis,Initials,Lotid,ImmName,Manufacturer_Code)     
   values( @Patientid , @Encounterid , @immid , @Dategiven ,      
   @Expdate, @Site , @Reaction , @ManuName , @dosage ,@Dos_Units, @Vis,@Initials,@LotNo,@ImmName,@Manufact_Code)      
  end      
 else      
  begin      
   update Patient_immunizations set DateGiven=@Dategiven ,LotExpDate= @Expdate,Site=@Site ,      
   Reaction= @Reaction ,ManuName=@ManuName,Dosage=@dosage,Dos_Units=@Dos_Units,Vis=@Vis,Initials=@Initials,Lotid=@LotNo,ImmName=@ImmName,Manufacturer_Code=@Manufact_Code      
   where patientid=@Patientid and EncounterID=@Encounterid and ImmID=@immid       
  end      
 end   
  
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_InsertPatientMedXMl]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_InsertPatientMedXMl]
(
@MedXML Text,
@PatientID int
)
As
BEGIN
DECLARE @PatID int
SELECT @PatID=COUNT(PatientID) From Patient_MedicationsXMl Where PatientID=@PatientID
IF(@PatID=0)
BEGIN
INSERT INTO 
Patient_MedicationsXMl(PatientID,GeneratedXml,LastModifiedDate) 
VALUES(@PatientID,@MedXML,GetDate())
END
ELSE
BEGIN
UPDATE Patient_MedicationsXMl 
SET 
GeneratedXml=@MedXML,
LastModifiedDate=GetDate()
WHERE PatientID=@PatientID
END
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_InsertRenewalReqXML]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_InsertRenewalReqXML]
(
@RenewalReqXML TEXT,
@RenewalReqID NVARCHAR(1000)
)
AS
BEGIN
UPDATE Patient_EPrescription_RenewalRequest
SET RenewalReqXMl=@RenewalReqXML  
WHERE renewalRequestId=@RenewalReqID
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USp_Lab_GetPatientIds]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USp_Lab_GetPatientIds](@ExternalID Varchar(15))    
as    
Declare     
 @Cnt int,    
 @PatId int    
BEGIN    
 select @Cnt = Count(PatientID), @PatId = PatientID from PatientDemographics where PatientId = @ExternalID     
 group by PatientId    
       
 If @cnt >1     
  Set @PatId = -2    
    If @Cnt <1    
  Set @PatId = -1      
    
   Select @PatId    
      
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_Lab_GetProviderIds]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_Lab_GetProviderIds] (@UPIN Varchar(20))    
as     
Declare    
 @ResourceId int,    
 @Count int    
BEGIN    
    select @Count = count(ResourceId) ,@ResourceId = ResourceId from Resources where ResourceUPIN = @UPIN     
 group by ResourceId   
    If @Count =0    
  Set @ResourceId = -1    
    
    Select @ResourceId   
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_Lab_GetScheduledProvId]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_Lab_GetScheduledProvId] (@PatientId int)    
AS    
Declare    
 @ResourceId1 int    
BEGIN    
 select top 1 @ResourceId1= isnull(ResourceId1,-1) from Appointments where Patientid = @Patientid and ResourceId1 in     
 (select ResourceId from Resources)    
 --and encounterid in (select encounterid from encounter)     
 --order by appointmentdate desc    
    
 If @ResourceId1 is NULL    
 set @ResourceId1 = -1    
    
 select @ResourceId1   
END 



')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_ListLocs]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_ListLocs]

AS

SET NOCOUNT ON

SELECT 
CASE
	WHEN r.resourceid IS NULL THEN cast(left(pn.PracticeCity, 20) as nchar(20))
	WHEN pn.practiceid IS NULL THEN cast(left(r.ResourceName, 20) as nchar(20))
END AS ResourceName
, CASE
	WHEN r.resourceid IS NULL THEN ''P''
	WHEN pn.practiceid IS NULL THEN ''R''
END AS TableId
, CASE
	WHEN r.resourceid IS NULL THEN pn.PracticeId
	WHEN pn.practiceid IS NULL THEN r.ResourceId
END AS LocId
, CASE
	WHEN r.resourceid IS NULL THEN pn.LocationReference
	WHEN pn.practiceid IS NULL THEN ''RES''
END AS LocRef
FROM Resources r FULL OUTER JOIN PracticeName pn ON str(r.Resourceid) = pn.PracticeState
WHERE (r.ResourceType = ''R'' and r.ServiceCode = ''02'') or (pn.PracticeType = ''P'')
ORDER BY r.resourceid, pn.practiceid

SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_ListStaff]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_ListStaff]
	(
	 @ResType nchar(1)
	)
AS

SET NOCOUNT ON

SELECT cast(left(ResourceName, 20) as nchar(20)) as ResourceName , ResourceId
FROM Resources
WHERE ResourceType = @ResType 
ORDER BY ResourceName

SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_ListStatus]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_ListStatus]
	(
	 @RefType nvarchar(64)
	)

AS

SET NOCOUNT ON

SELECT Code
FROM PracticeCodeTable
WHERE ReferenceType = @RefType
ORDER BY Rank, Code

SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_MatchPtnDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE PROCEDURE [dbo].[usp_MatchPtnDetails]
(
@FirstName NVARCHAR(200),
@LastName NVARCHAR(200),
@MiddleName NVARCHAR(200),
@DOB NVARCHAR(200),
@Gender NVARCHAR(200)
)
AS
BEGIN
DECLARE @Filter NVARCHAR(1000)
DECLARE @SQLSTR NVARCHAR(1000)
SET @Filter=''''
if @FirstName <> ''''
BEGIN
SET @Filter ='' FirstName=''''''+@FirstName+'''''' ''
END
if @LastName <> ''''
BEGIN
SET @Filter =@Filter+'' AND LastName=''''''+@LastName+'''''' '' 
END
if @MiddleName <> ''''
BEGIN
SET @Filter =@Filter+'' AND MiddleInitial=''''''+@MiddleName+'''''' ''
END
if @DOB <> ''''
BEGIN
SET @Filter =@Filter+'' AND BirthDate=''''''+@DOB+'''''' ''
END
if @Gender <> ''''
BEGIN
SET @Filter =@Filter+'' AND Gender=''''''+@Gender+'''''' ''
END
PRINT @Filter
IF @Filter <> ''''
BEGIN
SET @SQLSTR='' SELECT COUNT(*) FROM PatientDemoGraphics WHERE  ''+@Filter+'' ''
PRINT @SQLSTR
END
EXEC (@SQLSTR)
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_NewCrop_InsertNewRx]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_NewCrop_InsertNewRx]
(
@PatientId int,
@DrugId int,
@PrescriptionDate nvarchar(10),
--Third
@DosageNumber int,
@DosageForm int,
@Strength nvarchar(8),
@StrengthUOM nvarchar (16),
@Route int, 
--Fourth
@DosageFrequency int,
--Fifth
@RefillNo int,
@DAW nchar(1),
@DispenseNo NVARCHAR(20),
@DispenseNumberQualifier nvarchar(2),
--Sixth
@DaysSupply int,

@ResourceId int,
@PrescriptionGuid uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(64)
DECLARE @Third as nvarchar(64)
DECLARE @Fourth as nvarchar(64)
DECLARE @Fifth as nvarchar(64)
DECLARE @Sixth as nvarchar(64)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

CREATE TABLE #LeftJoin
(
	TempId int
)

Insert #LeftJoin Select 1

Select @DrugName = MED_ROUTED_DF_MED_ID_DESC from fdb.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''''
						THEN ''()''
					ELSE
						''('' + isnull(pctNumber.Code, '''') + '' '' + isnull(pctForm.Code, '''') + '')''
					END
	From #LeftJoin  
		Left Join PracticeCodeTable pctNumber ON pctNumber.ReferenceType = ''DOSAGENUMBERTYPE''
			and pctNumber.AlternateCode = @DosageNumber
		Left Join PracticeCodeTable pctForm ON pctForm.ReferenceType = ''DOSAGEFORMTYPE''
			and pctForm.AlternateCode = @DosageForm
		
Select @Third = @Third + CASE @Strength
							WHEN ''''
								THEN ''()''
							ELSE
								''('' + isnull(@Strength + '' '' + @StrengthUOM, '''') + '')''
							END
		
Select @Third = @Third + CASE
							WHEN @Route in (35, 34, 5)
								THEN ''('' + isnull(Code, '''') + '')''
							ELSE
								''()''
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DOSAGEROUTETYPE''
		and AlternateCode = @Route

Select @Fourth = CASE 
					WHEN @DosageFrequency > 1
						THEN ''('' + isnull(Code, '''') + '')()''
					ELSE
						''()()''
					END
	From #LeftJoin LEFT JOIN PracticeCodeTable ON ReferenceType = ''DOSAGEFREQUENCYTYPE'' and AlternateCode = @DosageFrequency

Select @Fourth = @Fourth + CASE @Route
							WHEN 2
								THEN ''('' + isnull(Code, '''') + '')()()()''
							ELSE
								''()()()()''
							END
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DOSAGEROUTETYPE''
		and AlternateCode = @Route

Select @Fifth = CASE @RefillNo
					WHEN ''''
						THEN ''()''
					ELSE
						''('' + cast(@RefillNo as nvarchar(5)) + '' Refills)''
					END
					+
					CASE @DAW
						WHEN ''Y'' 
							THEN ''(DAW)''
						ELSE
							''()''
					END
					+
					CASE @DispenseNo
						WHEN ''''
							THEN ''()''
						ELSE
							''('' + cast(@DispenseNo as nvarchar(5)) + '' '' + isnull(Code, '''') + '')''
					END
					+ ''()''
	From #LeftJoin Left Join PracticeCodeTable ON ReferenceType = ''DISPENSENUMBERQUALIFIER''
		and AlternateCode = @DispenseNumberQualifier

Select @Sixth = CASE @DaysSupply
					WHEN ''''
						THEN ''()()''
					ELSE	
						''()('' + CAST(@DaysSupply as nvarchar(4)) + '' DAYS SUPPLY)''
					END

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in (''H'', ''G'', ''D'', ''U'')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

select @Symptom = cast(Symptom as int) + 1
From PatientClinical
Where AppointmentId = @AppointmentId
	and ClinicalType = ''A''
Order By Symptom asc

Insert PatientClinical([AppointmentId]
           ,[PatientId]
           ,[ClinicalType]
           ,[EyeContext]
           ,[Symptom]
           ,[FindingDetail]
           ,[ImageDescriptor]
           ,[ImageInstructions]
           ,[Status]
           ,[DrawFileName]
           ,[Highlights]
           ,[FollowUp]
           ,[PermanentCondition]
           ,[PostOpPeriod]
           ,[Surgery]
           ,[Activity]) 
Select @AppointmentId as AppointmentId
, @PatientId as PatientId
, ''A'' as ClinicalType
, '''' as EyeContext
, @Symptom as Symptom
, ''RX-8/'' + @DrugName + ''-2/'' + @Third + ''-3/'' + @Fourth + ''-4/'' + @Fifth + ''-5/'' + @Sixth + ''-6/P-7/'' as FindingDetail
, ''!'' + SUBSTRING(@PrescriptionDate, 5, 2) + ''/'' + SUBSTRING(@PrescriptionDate, 7, 2) + ''/'' + SUBSTRING(@PrescriptionDate, 1, 4) as ImageDescriptor
, @PrescriptionGuid as ImageInstructions
, ''A'' as Status
, cast(@DrugId as nvarchar(16)) as DrawFileName
, ''A'' as Highlights
, '''' as FollowUp
, '''' as PermanentCondition
, 0 as PostOpPeriod
, '''' as Surgery
, '''' as Activity

END


--RX-8/BETAGAN EYE DROPS-2/(1 DROPS)(0.5 %)(OS)-3/(BID)()()()()()-4/(3 REFILLS)()(1 BOTTLE)()-5/(- ONGOING)(90 DAYS SUPPLY)-6/P-7/

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_NewCrop_UpdateRx]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



CREATE procedure [dbo].[usp_NewCrop_UpdateRx]
(
@PatientId int, 
@DrugId int, 
@PrescriptionDate nvarchar(10), 
@DispenseNo int,
@DosageNumber int,
@DosageForm nvarchar(20),
@Route nvarchar(20),
@DosageFrequency nvarchar(20),
@DAW bit,
@RefillNo int,
@ResourceId int,
@ClinicalId int,
@PrescriptionGuId uniqueidentifier
)

AS
BEGIN
DECLARE @DrugName as nvarchar(32)
DECLARE @Third as nvarchar(32)
DECLARE @Fourth as nvarchar(32)
DECLARE @Fifth as nvarchar(32)
DECLARE @Sixth as nvarchar(32)
DECLARE @AppointmentId as int
DECLARE @Symptom as int

Select @DrugName = MED_NAME from fdb.tblCompositeDrug where MEDID = @DrugId
Select @Third = CASE @DosageNumber
					WHEN ''''
						THEN ''()()()''
					ELSE
						''('' + cast(@DosageNumber as nvarchar(8)) + '' '' + @DosageForm + '')()()''
					END
					
Select @Fourth = CASE @DosageFrequency
						WHEN ''''
							THEN ''()()()()()()''
						ELSE
							''('' + @DosageFrequency + '')()()()()()''
						END

Select @Fifth = CASE @RefillNo
					WHEN 0
						THEN ''()''
					ELSE
						''('' + cast(@RefillNo as nvarchar(5)) + ''REFILLS)''
					END
					+
					CASE @DAW
						WHEN 1 
							THEN ''(DAW)''
						ELSE
							''()''
					END
					+
					CASE @DispenseNo
						WHEN 0
							THEN ''()''
						ELSE
							''('' + cast(@DispenseNo as nvarchar(5)) + '')''
					END
					+ ''()''

Select @Sixth = ''()()''

Select @AppointmentId = AppointmentId 
From PracticeActivity 
Where PatientId = @PatientId
	and Status in (''H'', ''G'', ''D'', ''U'')
	and ActivityDate <= @PrescriptionDate
Order By ActivityDate asc

Update PatientClinical
	Set FindingDetail = ''RX-8/'' + @DrugName + ''-2/'' + @Third + ''-3/'' + @Fourth + ''-4/'' + @Fifth + ''-5/'' + @Sixth + ''-6/-7/''
		, ImageDescriptor = ''!'' + Convert(nvarchar(10), getdate(), 101) + ImageDescriptor
		,ImageInstructions=@PrescriptionGuId
Where ClinicalId = @ClinicalId

END


')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_NQM_55_DM_with_DilatedExam]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('




CREATE PROCEDURE [dbo].[USP_NQM_55_DM_with_DilatedExam]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--Measure #12

--SET @ResourceIds=''''
DECLARE @TotParm INT;
DECLARE @CountParmDE INT;
DECLARE @CountParmROCNF INT;
DECLARE @CountParmNIDR INT;
SET @TotParm=0;
SET @CountParmDE=0;
SET @CountParmROCNF=0;
SET @CountParmNIDR=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
--PRINT ''IF''
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
--PRINT ''ELSE''
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END



Select pd.PatientId, COUNT(ap.AppointmentId) as totapps
INTO #TBLPATDET  
FROM PatientDemographics pd
	INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate between @enddate -750000 AND @enddate - 180000 
	INNER JOIN PatientReceivables pr ON pr.AppointmentId=ap.AppointmentId
	INNER JOIN PatientReceivableServices prs ON prs.invoice = pr.invoice and prs.Service in(
		''92002'',''92004'',''92012'',''92014'',''97802'',''97803'',''97804'',''99201'',''99202'',''99203'',''99204'',
		''99205'',''99212'',''99213'',''99214'',''99215'',''99304'',''99305'',''99306'',''99307'',''99308'',''99309'',
		''99310'',''99324'',''99325'',''99326'',''99327'',''99328'',''99334'',''99335'',''99336'',''99337'',''99341'',
		''99342'',''99343'',''99344'',''99345'',''99347'',''99348'',''99349'',''99350'',''G0270'',''G0271'') AND prs.Status=''A''
	INNER JOIN Appointments ap2Y on ap2Y.patientid = pd.patientid
		AND ap2Y.appdate >= @enddate - 20000	
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap2Y.AppointmentId
		AND pc.ClinicalType = ''Q'' AND pc.Status=''A''
		AND SUBSTRING(pc.FindingDetail,1,6)  IN(''250.00'', ''250.01'', ''250.02'', ''250.03'', ''250.10'', ''250.11'', 
		''250.12'', ''250.13'', ''250.20'', ''250.21'', ''250.22'', ''250.23'', ''250.30'', ''250.31'', ''250.32'', ''250.33'',
		''250.40'', ''250.41'', ''250.42'', ''250.43'', ''250.50'', ''250.51'', ''250.52'', ''250.53'', ''250.60'', ''250.61'',
		''250.62'', ''250.63'', ''250.70'', ''250.71'', ''250.72'', ''250.73'', ''250.80'', ''250.81'', ''250.82'', ''250.83'',
		''250.90'', ''250.91'', ''250.92'', ''250.93'', ''357.2'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'',
		''362.06'', ''362.07'', ''366.41'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
	INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
group by pd.PatientId


--Denominator
Select @TotParm=COUNT(distinct patientid)
from #tblpatdet
where totapps >= 2


--Eye Exam 

select distinct #tblpatdet.patientid
into #EyeExam
from #tblpatdet
	INNER JOIN Appointments ap on ap.PatientId = #tblpatdet.patientid and ap.AppDate <= @enddate and #tblpatdet.totapps >= 2
	LEFT OUTER JOIN PatientClinical pcD on pcD.AppointmentId = ap.AppointmentId 
		AND pcD.ClinicalType = ''F'' and pcD.Status = ''A''
		AND pcD.Symptom =''/DILATION''
	LEFT OUTER JOIN PatientClinical pcRE on pcRE.AppointmentId = ap.AppointmentId 
		AND pcRE.ClinicalType = ''F'' and pcRE.Status = ''A''
		AND pcRE.Symptom IN (''/FUNDUS PHOTO'', ''/VF (30,2, 24,2,10,2, ESTERMAN)'', ''/VF   (Superior 36)'',
		    ''/VF   (Suprathreshold, 120)'', ''/FLUORESCEIN ANGIOGRAPHY'', ''/GDX'', ''/HRT'',	''/HRT, MACULA'',
		     ''/OCT'', ''/OCT, CENTRAL MACULAR THICKNESS'', ''/SCANNING LASER POLARIMETRY'')


Select @CountParmDE=COUNT(distinct patientid)
From #EyeExam

--CNF or Rule Out DR in last year

select distinct #tblpatdet.patientid
into #ROCNF
from #tblpatdet
	INNER JOIN Appointments ap1Y on ap1Y.PatientId = #tblpatdet.PatientId and ap1Y.AppDate >= @startdate - 10000 
		AND #tblpatdet.totapps >= 2
		AND #tblpatdet.patientid not in (select patientid from #EyeExam)
	INNER JOIN PatientClinical pc on pc.AppointmentId = ap1Y.AppointmentId 
		AND pc.ClinicalType = ''Q'' and pc.Status = ''A'' 
		AND substring(pc.findingdetail, 1, 6) in (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'',''362.07'')
		AND (pc.Symptom like ''%!%'' or pc.Symptom like ''%#%'')

Select @CountParmROCNF=COUNT(distinct patientid)
From #ROCNF

--Not In DR in last year

select distinct #tblpatdet.patientid
into #NIDR
from #tblpatdet
	INNER JOIN Appointments ap1Y on ap1Y.PatientId = #tblpatdet.PatientId and ap1Y.AppDate >= @startdate - 10000
		AND #tblpatdet.totapps >= 2
		AND #tblpatdet.patientid not in (select patientid from #EyeExam)
		AND #tblpatdet.patientid not in (select patientid from #ROCNF)
	INNER JOIN PatientClinical pc on pc.AppointmentId = ap1Y.AppointmentId 
		AND pc.ClinicalType = ''Q'' and pc.Status = ''A''
		AND pc.PatientId not in
			(
			select pc1.patientid from PatientClinical pc1
			inner join Appointments ap on ap.AppointmentId = pc1.AppointmentId and ap.AppDate >= @startdate - 10000
			where
			SUBSTRING(FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'',''362.07'')
			)

Select @CountParmNIDR=COUNT(distinct patientid)
From #NIDR


DROP TABLE #TMPResources
DROP TABLE #TBLPATDET
DROP TABLE #EyeExam
DROP TABLE #ROCNF
DROP TABLE #NIDR

SELECT @TotParm as Denominator, sum(@CountParmDE + @CountParmROCNF + @CountParmNIDR) as Numerator

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_NQM_86_POAG]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('




CREATE PROCEDURE [dbo].[USP_NQM_86_POAG]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--NQM #86

--SET @ResourceIds=''''
DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
--PRINT ''IF''
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
--PRINT ''ELSE''
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END



Select DISTINCT pd.PatientId, count(ap.AppointmentId) as totapps
INTO #TBLPATDET  
FROM PatientDemographics pd
	INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=ap.AppDate -180000
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
		AND pc.ClinicalType = ''Q'' AND pc.Status=''A''
		AND SUBSTRING(pc.FindingDetail,1,6)  IN(''365.10'', ''365.11'', ''365.12'', ''365.15'')
	INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'', ''99202'', 
		''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215'', ''99304'', ''99305'', ''99306'',
		''99307'', ''99308'', ''99309'', ''99310'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', 
		''99335'', ''99336'', ''99337'') AND prs.Status=''A''
	INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
	INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
GROUP BY pd.patientid

--Denominator
Select @TotParm=COUNT(distinct patientid)
from #tblpatdet
where totapps > 1


--Numerator
Select @CountParm=COUNT(distinct #TBLPATDET.patientid)
FROM #TBLPATDET
	INNER JOIN Appointments ap ON ap.PatientId=#TBLPATDET.patientid and #TBLPATDET.totapps >= 2
		AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000
	LEFT OUTER JOIN PatientClinical pcF On pcF.AppointmentId=ap.AppointmentId
		AND pcF.ClinicalType=''F'' AND pcF.Status=''A''
		AND pcF.Symptom in (''/CUP/DISC'', ''/OCT, OPTIC NERVE'')
	LEFT OUTER JOIN PatientClinical pcQ On pcQ.AppointmentId=ap.AppointmentId 
		AND pcQ.ClinicalType=''Q'' AND pcQ.Status=''A''
		AND pcQ.ImageDescriptor =''L'' and pcQ.FindingDetail <> ''LN''
		AND	pcQ.FindingDetail not in (''365.10'', ''365.11'', ''365.12'', ''365.15'')


DROP TABLE #TMPResources
DROP TABLE #TBLPATDET

SELECT @TotParm as Denominator,@CountParm as Numerator

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_NQM_88_GetDiabetic_Retinopathy_MacularEdema]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



Create  PROCEDURE [dbo].[USP_NQM_88_GetDiabetic_Retinopathy_MacularEdema]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--NQM #88
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END


Select distinct pd.PatientId, count(ap.appointmentid) as totapps
INTO #TBLPATDET
FROM PatientDemographics pd
	INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid 
		AND pd.BirthDate <=(ap.AppDate - 180000)
	INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId
		AND pc.ClinicalType = ''Q'' AND pc.Status=''A''
		AND SUBSTRING(pc.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
	INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
		, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
	INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
	INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
group by pd.patientid

--Denominator
Select @TotParm=COUNT(distinct patientid)
from #tblpatdet
where totapps >= 2

Select distinct #TBLPATDET.patientid, pc0.Status as ''0'', pc1.Status as ''1'', pc2.Status as ''2'' 
into #MacEdema
FROM #TBLPATDET
	INNER JOIN Appointments ap ON #TBLPATDET.PatientId=ap.Patientid 
		AND #TBLPATDET.totapps >=2
		AND ap.AppDate between (@StartDate - 10000) and (@EndDate + 10000)	
	INNER JOIN PatientClinical pc0 ON pc0.AppointmentId = ap.AppointmentId
		AND pc0.ClinicalType = ''Q'' AND pc0.Status=''A''
		AND SUBSTRING(pc0.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
	INNER JOIN PatientClinical pc1 On pc1.ClinicalType=''F'' AND pc1.Status=''A''
		AND	(pc1.Symptom = ''/DILATION'' or pc1.Symptom LIKE ''%&'')
	INNER JOIN PatientClinical pc2 On pc2.AppointmentId = ap.AppointmentId 
		AND	pc2.ClinicalType=''Q'' AND pc2.Status=''A''	
		AND 
		(
		(pc2.findingdetail in (''Q008'', ''Q009'', ''362.50.22'', ''P405'', ''P423'', ''P427'', ''P648''
		, ''P656'', ''P657'', ''P658'', ''P659'', ''P660'', ''P661'', ''P662'', ''P663'', ''P664'', ''P665'', ''P666''
		, ''P667'', ''P668'', ''Q026'', ''Q027'', ''Q028'', ''Q029'', ''Q151'', ''Q173''))
		or
		(SUBSTRING(pc2.FindingDetail, 1, 6) in (''362.07'', ''362.53'', ''362.81'', ''362.83''))
		)

--Numerator		
Select @CountParm=COUNT(distinct patientid)
From #MacEdema
		
DROP TABLE #TMPResources
DROP TABLE #TBLPATDET
DROP TABLE #MacEdema

SELECT @TotParm as Denominator,@CountParm as Numerator


END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_NQM_89_DR_Communication_Physician]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_NQM_89_DR_Communication_Physician]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
As
BEGIN
--NQM #89
DECLARE @TotParm INT;
DECLARE @CountParm1 INT;
DECLARE @CountParm2 INT;
DECLARE @PtnId NVARCHAR(100);

SET @TotParm=0;
SET @CountParm1=0;
SET @CountParm2=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId, count(ap.AppointmentId) as totapps
INTO #TBLPATDET  
FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 180000) 
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType = ''Q'' AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'',
 ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99324'',
 ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
GROUP BY pd.patientid

--Denominator
Select @TotParm=COUNT(distinct patientid)
from #tblpatdet
where totapps >= 2

SELECT distinct #tblpatdet.patientid, pc0.Status as ''0'', pc1.Status as ''1'', pc2.Status as ''2'', pc3.Status as ''3''
into #MacEdema
FROM #tblpatdet 
	INNER JOIN Appointments ap ON ap.PatientId=#TBLPATDET.patientid
		AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000 
		AND #TBLPATDET.totapps >= 2
	INNER JOIN PatientClinical pc0 ON pc0.AppointmentId = ap.AppointmentId
		AND pc0.ClinicalType = ''Q'' AND pc0.Status=''A''
		AND SUBSTRING(pc0.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
	INNER JOIN PatientClinical pc1 On pc1.ClinicalType=''F'' AND pc1.Status=''A'' and 
		(	pc1.Symptom = ''/DILATION'' or pc1.Symptom LIKE ''%&''	)
	INNER JOIN PatientClinical pc2 On pc2.AppointmentId = ap.AppointmentId AND
		pc2.ClinicalType=''Q'' AND pc2.Status=''A''	and 
		(
		(pc2.findingdetail in (''Q008'', ''Q009'', ''362.50.22'', ''P405'', ''P423'', ''P427'', ''P648''
		, ''P656'', ''P657'', ''P658'', ''P659'', ''P660'', ''P661'', ''P662'', ''P663'', ''P664'', ''P665'', ''P666''
		, ''P667'', ''P668'', ''Q026'', ''Q027'', ''Q028'', ''Q029'', ''Q151'', ''Q173''))
		or
		(SUBSTRING(pc2.FindingDetail, 1, 6) in (''362.07'', ''362.53'', ''362.81'', ''362.83''))
		)
	INNER JOIN PatientClinical pc3 on pc3.AppointmentId = ap.AppointmentId AND
		pc3.ClinicalType=''A'' AND pc3.Status=''A'' and SUBSTRING(pc3.FindingDetail, 1, 14) = ''SEND A CONSULT''


Select @CountParm1=COUNT(distinct patientid)
From #MacEdema


SELECT distinct #tblpatdet.patientid, pc0.Status as ''0'', pc1.Status as ''1'', pc2.Status as ''2'', pc3.Status as ''3''
into #MacFindings
FROM #tblpatdet 
	INNER JOIN Appointments ap ON ap.PatientId=#TBLPATDET.patientid
		AND #TBLPATDET.patientid not in (select patientid from #MacEdema)
		AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000
		AND #TBLPATDET.totapps >= 2
	INNER JOIN PatientClinical pc0 ON pc0.AppointmentId = ap.AppointmentId
		AND pc0.ClinicalType = ''Q'' AND pc0.Status=''A''
		AND SUBSTRING(pc0.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
	INNER JOIN PatientClinical pc1 On  pc1.ClinicalType=''F'' AND pc1.Status=''A'' and 
		(pc1.Symptom = ''/DILATION'' or pc1.Symptom LIKE ''%&'')
	INNER JOIN PatientClinical pc2 On pc2.AppointmentId = ap.AppointmentId
		AND	pc2.ClinicalType=''Q'' AND pc2.Status=''A'' AND pc2.imagedescriptor=''N''
	INNER JOIN PatientClinical pc3  on pc3.AppointmentId = ap.AppointmentId
		AND	pc3.ClinicalType=''A'' AND pc3.Status=''A'' and SUBSTRING(pc3.FindingDetail, 1, 14) = ''SEND A CONSULT''


Select @CountParm2=COUNT(distinct patientid)
From #MacFindings

DROP TABLE #TBLPATDET
DROP TABLE #TMPResources
DROP TABLE #MacEdema
DROP TABLE #MacFindings

SELECT @TotParm as Denominator,sum(@CountParm1 + @CountParm2) as Numerator

END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_PagedItems]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_PagedItems]
	(
	 @Page int,
	 @RecsPerPage int,
	 @TransType nvarchar(5),
	 @TransStatus nvarchar(5),
	 @Status nvarchar(50),
	 @Staff nvarchar(1000),
	 @Loc nvarchar(500),
	 @SDate nvarchar(8),
	 @EDate nvarchar(8)
	)
AS

SET NOCOUNT ON

-- Create a temporary table
CREATE TABLE #TempItems
(
	ID int IDENTITY,
	Status nchar(1),
	TranDate nchar(10),
	PatName nchar(34),
	TranType nchar(64),
	Doctor nchar(16),
	RefDoc nchar(64),
	TranId int,
	PatId int,
	TransTypeId int
)

-- Insert the rows into temp table
INSERT INTO #TempItems (Status, TranDate, PatName, TranType, Doctor, RefDoc, TranId, PatId, TransTypeId)
SELECT case ptj.transactionbatch when '''' then ''N'' else ptj.transactionbatch end
, substring(app.appdate,5,2)+''/''+substring(app.appdate,7,2)+''/''+substring(app.appdate,1,4)
, case pd.middleinitial when '''' then LTrim(pd.firstname) + '' '' + LTrim(pd.lastname) else LTrim(pd.firstname) + '' '' + pd.middleinitial + '' '' + LTrim(pd.lastname) end
, ptj.transactionremark
, r.resourcelastname
, pv.vendorname
, ptj.transactionid
, pd.patientid
, ptj.transactiontypeid
FROM practicetransactionjournal ptj 
	inner join appointments app on ptj.transactiontypeid = app.appointmentid
	inner join patientdemographics pd on app.patientid = pd.patientid
	left join practicevendors pv on ptj.transactionrcvrid = pv.vendorid
	left join resources r on app.resourceid1 = r.resourceid
WHERE ptj.transactiontype = @TransType and ptj.transactionstatus = @TransStatus
and ptj.transactionbatch in (select nstr from CharTable(@Status, '',''))
and ((app.appdate <= @SDate) or (@SDate = ''-''))
and ((app.appdate >= @EDate) or (@EDate = ''-'')) 
and app.resourceid1 in (select number from IntTable(@Staff))
and app.resourceid2 in (select number from IntTable(@Loc))
ORDER BY app.appdate, pd.lastname, ptj.transactionid

-- Calculate record range for return.
DECLARE @FirstRec int, @LastRec int
SELECT @FirstRec = (@Page - 1) * @RecsPerPage
SELECT @LastRec = (@Page * @RecsPerPage + 1)

-- Add AllRecords on to the end so that we have a total rec count.
SELECT *,
       AllRecords = 
	(
	 SELECT COUNT(*) 
	 FROM #TempItems TI
	) 
FROM #TempItems
WHERE ID > @FirstRec AND ID < @LastRec

-- Turn NOCOUNT back OFF
SET NOCOUNT OFF
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PatientDemographics]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



CREATE procedure [dbo].[USP_PatientDemographics](      
@Patient_Id varchar(20))      
as      
declare      
@Sqlstr varchar(2000)         
begin      
 set @Sqlstr=''Select isnull(Patientid,'''''''') Patientid,isnull(LastName,'''''''') LastName,     
 isnull(FirstName,'''''''') FirstName, isnull(MiddleInitial,'''''''') MI,     
 isnull(BirthDate,'''''''') DOB, Gender as Gender,isnull(Address,'''''''') Address,isnull(Race,'''''''') Race,    
 isnull(City,'''''''') City,isnull(State,'''''''') State,isnull(Ethnicity,'''''''') Ethnicity,    
 isnull(zip,'''''''') zip,isnull(HomePhone,'''''''') HomePh,    
 isnull(LastName,'''''''') + '''', '''' +     
 isnull(FirstName,'''''''') + '''' ''''+ isnull(MiddleInitial,'''''''') PatientName      
 from patientdemographics where patientid ='' + @Patient_Id +''''      
 exec (@Sqlstr)      
end 
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PatientImmunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE procedure [dbo].[USP_PatientImmunizations](    
@Patient_Id Varchar(20))    
as    
declare    
@Sqlstr varchar(2000)       
begin    
 set @Sqlstr=''select isnull(I.ImmName,'''''''') ImmName,isnull(LotId,'''''''') LotId,isnull(DateGiven,'''''''') DateGiven,isnull(Dosage,'''''''') Dosage, isnull(Dos_Units,'''''''') Dos_Units, isnull(ManuName,'''''''') ManuName,   
isnull(Manufacturer_Code,'''''''') ManuCode, isnull(Immcode,'''''''') Immcode     
from patient_immunizations p,Immunizations i where p.immid=i.immno and patientid='''''' + @Patient_Id +''''''''      
  
exec (@Sqlstr)    
end 

')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_PaymentToDeleted]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_PaymentToDeleted]
(@PatId int)
AS
SELECT 
	PatientReceivablePayments.PaymentAmount,
	PatientReceivablePayments.PaymentType, 
	PatientReceivables.Invoice, PatientReceivables.InvoiceDate,
	PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientReceivables.ReceivableId, 
	PatientReceivablePayments.PaymentId, PatientReceivables.AppointmentId, 
	PatientReceivables.PatientId, PatientReceivablePayments.PaymentServiceItem, 
	PatientReceivablePayments.PaymentDate, PatientReceivableServices.Status
FROM (((PatientReceivablePayments 
INNER JOIN PatientReceivables 
ON PatientReceivablePayments.ReceivableId = PatientReceivables.ReceivableId) 
INNER JOIN PatientDemographics 
ON PatientReceivables.PatientId = PatientDemographics.PatientId) 
INNER JOIN Appointments 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
LEFT JOIN PatientReceivableServices 
ON PatientReceivablePayments.PaymentServiceItem = PatientReceivableServices.ItemId
WHERE (PatientReceivablePayments.PaymentType = ''P'') AND
(PatientReceivableServices.Status = ''X'') And
((PatientReceivables.PatientId = @PatId) Or (@PatId = ''0''))
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_PendingSurgery]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_PendingSurgery]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int,
@StaId varchar(1)
AS
SELECT DISTINCT
	Appointments.AppDate, Appointments.ResourceId1,
	Resources.ResourceName, Appointments.ResourceId2, 
	Appointments.AppointmentId, PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.HomePhone, PatientDemographics.PatientId, 
	PatientClinical.ClinicalId, PatientClinical.ClinicalType, 
	PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 
	PatientClinical.Status, PatientClinical.Surgery
FROM Appointments
INNER JOIN PatientClinical
ON Appointments.AppointmentId = PatientClinical.AppointmentId
INNER JOIN PatientDemographics 
ON PatientDemographics.PatientId = PatientClinical.PatientId
LEFT JOIN Resources 
ON Resources.ResourceId = Appointments.ResourceId1
LEFT JOIN PatientClinicalSurgeryPlan
ON PatientClinical.ClinicalId = PatientClinicalSurgeryPlan.SurgeryClnId
WHERE (PatientClinical.ClinicalType = ''A'') And (PatientClinical.Surgery <> ''Z'') And
(Left(PatientClinical.FindingDetail,16) = ''SCHEDULE SURGERY'') 
--And
--(((PatientClinicalSurgeryPlan.SurgeryRefType) = '''') Or
--((PatientClinicalSurgeryPlan.SurgeryRefType) <> ''F'') Or
--((PatientClinicalSurgeryPlan.SurgeryRefType = ''F'') And (PatientClinicalSurgeryPlan.SurgeryStatus = ''D''))) And
--(((PatientClinicalSurgeryPlan.SurgeryStatus) = '''') Or
--  (PatientClinicalSurgeryPlan.SurgeryStatus <> ''D'') Or
-- ((PatientClinicalSurgeryPlan.SurgeryRefType = ''F'') And (PatientClinicalSurgeryPlan.SurgeryStatus = ''D''))) And
--(((Appointments.AppDate) <= @SDate) Or (@SDate = ''0'')) And  
--(((Appointments.AppDate) >= @EDate) Or (@EDate = ''0'')) And 
--((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
--((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
--((PatientClinical.Surgery = @StaId) OR (@StaId = ''0''))
ORDER BY Appointments.AppDate DESC, PatientDemographics.LastName ASC , PatientDemographics.FirstName ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_PendingSurgeryAged]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_PendingSurgeryAged]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int,
@StaId varchar(1)
AS
SELECT DISTINCT
	Appointments.AppDate, Appointments.ResourceId1,
	Resources.ResourceName, Appointments.ResourceId2, 
	Appointments.AppointmentId, PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.HomePhone, PatientDemographics.PatientId, 
	PatientClinical.ClinicalId, PatientClinical.ClinicalType, 
	PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 
	PatientClinical.Status, PatientClinical.Surgery
--	PatientClinicalSurgeryPlan.SurgeryRefType,
--	PatientClinicalSurgeryPlan.SurgeryStatus
FROM PatientClinical
INNER JOIN Appointments
ON Appointments.AppointmentId = PatientClinical.AppointmentId
INNER JOIN PatientDemographics 
ON PatientDemographics.PatientId = PatientClinical.PatientId
INNER JOIN Resources 
ON Resources.ResourceId = Appointments.ResourceId1
LEFT JOIN PatientClinicalSurgeryPlan
ON PatientClinical.ClinicalId = PatientClinicalSurgeryPlan.SurgeryClnId
WHERE (PatientClinical.ClinicalType = ''A'') And
(PatientClinical.Surgery = ''Z'') And
(Left(PatientClinical.FindingDetail,16) = ''SCHEDULE SURGERY'') And
(((PatientClinicalSurgeryPlan.SurgeryRefType) IS NULL) Or
((PatientClinicalSurgeryPlan.SurgeryRefType) <> ''F'') Or
((PatientClinicalSurgeryPlan.SurgeryRefType = ''F'') And (PatientClinicalSurgeryPlan.SurgeryStatus <> ''A''))) And
(((PatientClinicalSurgeryPlan.SurgeryStatus) IS NULL) Or
(PatientClinicalSurgeryPlan.SurgeryStatus <> ''D'')) And
(((Appointments.AppDate) <= @SDate) Or (@SDate = ''0'')) And  
(((Appointments.AppDate) >= @EDate) Or (@EDate = ''0'')) And 
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((PatientClinical.Surgery = @StaId) OR (@StaId = ''0''))
ORDER BY Appointments.AppDate DESC, PatientDemographics.LastName ASC , PatientDemographics.FirstName ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_Diabetes_GetMellitus_Dilated_Eye]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_Diabetes_GetMellitus_Dilated_Eye]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--------------MEASURE #117 DM DILATED EYE EXAM   PQRS ----------------------------------

DECLARE @TotParm INT;
DECLARE @CountParm INT;
SET @TotParm=0;
SET @CountParm=0;
DECLARE @PtnId NVARCHAR(100);
SET @PtnId = ''0'';


CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,''NONE'' TMPStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate between ap.AppDate -750000 AND ap.AppDate - 180000 
and ap.AppDate between @StartDate and @EndDate 
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType in(''b'',''k'') AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''250.00'', ''250.01'', ''250.02'', ''250.03'', ''250.10'', ''250.11'', 
''250.12'', ''250.13'', ''250.20'', ''250.21'', ''250.22'', ''250.23'', ''250.30'', ''250.31'', ''250.32'', ''250.33'',
 ''250.40'', ''250.41'', ''250.42'', ''250.43'', ''250.50'', ''250.51'', ''250.52'', ''250.53'', ''250.60'', ''250.61'',
  ''250.62'', ''250.63'', ''250.70'', ''250.71'', ''250.72'', ''250.73'', ''250.80'', ''250.81'', ''250.82'', ''250.83'',
   ''250.90'', ''250.91'', ''250.92'', ''250.93'', ''357.2'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'',
    ''362.06'', ''362.07'', ''366.41'', ''648.00'', ''648.01'', ''648.02'', ''648.03'', ''648.04'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(
''92002'',''92004'',''92012'',''92014'',''97802'',''97803'',''97804'',''99201'',''99202'',''99203'',''99204'',
''99205'',''99212'',''99213'',''99214'',''99215'',''99304'',''99305'',''99306'',''99307'',''99308'',''99309'',
''99310'',''99324'',''99325'',''99326'',''99327'',''99328'',''99334'',''99335'',''99336'',''99337'',''99341'',
''99342'',''99343'',''99344'',''99345'',''99347'',''99348'',''99349'',''99350'',''G0270'',''G0271'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId --AND pr.invoicedate between ''20100101'' and ''20101231'' --AND pr.AppointmentId=ap.AppointmentId
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER Join PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')



DECLARE PATCURSOR CURSOR 
FOR SELECT DISTINCT PatientId FROM #TBLPATDET
OPEN PATCURSOR
FETCH NEXT FROM PATCURSOR INTO @PtnId
WHILE @@FETCH_STATUS=0
BEGIN
--PRINT ''''+@PtnId+''''
--DILATION
  IF ((SELECT  COUNT(*) FROM PatientClinical WHERE ClinicalType=''F'' and Symptom =''/DILATION'' 
  and status = ''A'' AND PatientId=@PtnId)>0)
	BEGIN
	--PRINT ''For DILATION''
--FUNDUS PHOTO
		IF((SELECT  COUNT(*) FROM PatientClinical WHERE ClinicalType=''F'' and Symptom =''/FUNDUS PHOTO'' 
		and status = ''A'' AND PatientId=@PtnId)>0)
			BEGIN
			--PRINT ''FUNDUS PHOTO''		
--OTHER IMAGING
				IF((select COUNT(*) from PatientClinical where ClinicalType = ''f'' and Symptom IN (''/VF (30,2, 24,2,10,2, ESTERMAN)'',
				''/VF   (Superior 36)'', ''/VF   (Suprathreshold, 120)'', ''/FLUORESCEIN ANGIOGRAPHY'', ''/GDX'', ''/HRT'',
				''/HRT, MACULA'', ''/OCT'', ''/OCT, CENTRAL MACULAR THICKNESS'', ''/SCANNING LASER POLARIMETRY'') 
				and Status = ''A'' AND PatientId=@PtnId)>0)
					BEGIN
					--PRINT ''OTHER IMAGING''		
						UPDATE #TBLPATDET SET TMPStatus=''3'' WHERE PatientId=@PtnId
					END
				ELSE
					BEGIN
						UPDATE #TBLPATDET SET TMPStatus=''2'' WHERE PatientId=@PtnId
					END
			END
			ELSE
				BEGIN
						UPDATE #TBLPATDET SET TMPStatus=''1'' WHERE PatientId=@PtnId
				END
	END
	ELSE
		BEGIN
			IF((select COUNT(*) from PatientClinical where ClinicalType = ''Q'' AND PatientId=@PtnId and substring(findingdetail, 1, 6) in (''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'',''362.07''))>0)
				BEGIN
					--OTHER IMAGING
					IF((select COUNT(*) from PatientClinical where ClinicalType = ''f'' and Symptom IN (''/VF (30,2, 24,2,10,2, ESTERMAN)'',
						''/VF   (Superior 36)'', ''/VF   (Suprathreshold, 120)'', ''/FLUORESCEIN ANGIOGRAPHY'', ''/GDX'', ''/HRT'',
						''/HRT, MACULA'', ''/OCT'', ''/OCT, CENTRAL MACULAR THICKNESS'', ''/SCANNING LASER POLARIMETRY'') 
						and Status = ''A'' AND PatientId=@PtnId)>0)
						BEGIN
						--FUDUS PHOTOS
								IF((SELECT  COUNT(*) FROM PatientClinical WHERE ClinicalType=''F'' and Symptom =''/FUNDUS PHOTO'' 
									and status = ''A'' AND PatientId=@PtnId)>0)
										BEGIN
											UPDATE #TBLPATDET SET TMPStatus=''5'' WHERE PatientId=@PtnId
										END
								ELSE
										BEGIN
											UPDATE #TBLPATDET SET TMPStatus=''5'' WHERE PatientId=@PtnId
										END
						END
					ELSE
						BEGIN
							UPDATE #TBLPATDET SET TMPStatus=''5'' WHERE PatientId=@PtnId
						END
				END
			ELSE
			BEGIN
				UPDATE #TBLPATDET SET TMPStatus=''4'' WHERE PatientId=@PtnId
			END
		END
	FETCH NEXT FROM PATCURSOR INTO @PtnId
END
CLOSE PATCURSOR
DEALLOCATE PATCURSOR


Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_dilate2
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPStatus
ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetAMD_Dilated_Macular_Examination]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE  PROCEDURE [dbo].[USP_PQRI_GetAMD_Dilated_Macular_Examination]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS 
BEGIN
--------------------------------------MEASURE #14 AMD  PQRS--------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,0 TMPStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 500000) 
and ap.AppDate between @StartDate and @EndDate 
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType in(''b'',''k'') AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''362.50'', ''362.51'', ''362.52'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', 
''92014'', ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99324'',
''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER Join PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')


DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
			IF ((Select COUNT(*) FROM #TBLPATDET 
				LEFT OUTER JOIN Appointments ap ON ap.PatientId=@PTNID 
				AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000
				LEFT OUTER JOIN PatientClinical pcF On  pcF.ClinicalType=''F'' AND pcF.Status=''A'' 
				AND #TBLPATDET.PatientId=pcF.PatientId AND ap.AppointmentID=pcF.AppointmentId
				AND pcF.PatientId=@PTNID
				LEFT OUTER JOIN PatientClinical pcQ On  pcQ.ClinicalType=''Q'' 
				AND pcQ.Status=''A'' AND #TBLPATDET.PatientId=pcQ.PatientId AND ap.AppointmentID=pcQ.AppointmentId
				AND pcQ.PatientId=@PTNID
				WHERE 
					((pcF.Symptom =''/DILATION'' OR  pcQ.Symptom like ''%&'')
					AND
					((pcQ.FindingDetail in (''Q008'', ''Q009'', ''362.50.22'', ''P405'', ''P423'', ''P427'', ''P648''
					, ''P656'', ''P657'', ''P658'', ''P659'', ''P660'', ''P661'', ''P662'', ''P663'', ''P664'', ''P665'', ''P666''
					, ''P667'', ''P668'', ''Q026'', ''Q027'', ''Q028'', ''Q029'', ''Q151'', ''Q173''))
					OR 
					(substring(pcQ.FindingDetail, 1, 6) in (''362.07'', ''362.53'', ''362.81'', ''362.83'')))
					OR 
					(pcF.Symptom = ''/PQRI (ARMD DILATED MACULAR EXAM)'')))>0)	
						BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
						END
			ELSE
				BEGIN
						UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''4'' WHERE #TBLPATDET.PatientId=@PTNID
				END	
FETCH NEXT FROM PTNCURSOR INTO @PTNID
END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR

DROP TABLE #TMPResources

Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_dilatedmac3
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPStatus
ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetCataracts_Complications]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetCataracts_Complications]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
As
BEGIN
-----------------------------MEASURE #192 Cataract Surgery Complications PQRS ---------------------------
DECLARE @TotalParm as int;
DECLARE @CountParam as int;
DECLARE @PTNID NVARCHAR(200);

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,prs.Service,0 TMPSurgStatus,0 TMPTamStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 180000)
 and ap.AppDate between @StartDate and @EndDate 
INNER JOIN PatientReceivableServices prs ON prs.Service in (''66840'', ''66850'', ''66852'', ''66920'', ''66930'',
				 ''66940'', ''66982'', ''66983'', ''66984'') 
				 AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')
WHERE pd.PatientId NOT IN(
SELECT  PdFlomax.PatientId FROM PatientDemographics PdFlomax
INNER JOIN Appointments apFlomax on apFlomax.AppDate < ap.AppDate
INNER JOIN PatientClinical pcFlomax ON PdFlomax.PatientId=pcFlomax.PatientId 
AND pcFlomax.AppointmentId=apFlomax.AppointmentId 
AND pcFlomax.ClinicalType=''A'' 
AND pcFlomax.Status=''A'' AND pcFlomax.FindingDetail LIKE ''Rx%flomax%''
					UNION 
SELECT  PdHist.PatientId FROM PatientDemographics PdHist
INNER JOIN PatientReceivableServices prsHist ON prsHist.Service IN(''67036'', ''67038'', ''67039'', ''67040'') 
INNER JOIN PatientReceivables prHist ON prsHist.Invoice=prHist.Invoice 
AND prHist.PatientId=PdHist.PatientId AND prHist.InvoiceDate < pr.InvoiceDate
					UNION 
SELECT  PdICD.PatientId FROM PatientDemographics PdICD
INNER JOIN Appointments apICD on apICD.AppDate <ap.AppDate
INNER JOIN PatientClinical pcICD ON pcICD.PatientId=PdICD.PatientId
AND pcICD.ClinicalType in(''b'',''k'') AND pcICD.Status=''A'' AND pcICD.AppointmentId=apICD.AppointmentId
AND ((substring(pcICD.findingdetail, 1, 6)  IN (''360.11'',''360.12'',''360.20'',''360.21'',''360.30'',
	''360.31'',''360.33'',''360.34'',''362.21'',''364.00'',''364.01'',''364.02'',''364.03'',''364.04'',
	''364.05'',''364.10'',''364.11'',''364.21'',''364.22'',''364.23'',''364.24'',''364.42'',''364.60'',
	''364.61'',''364.62'',''364.63'',''364.64'',''364.70'',''364.71'',''364.72'',''364.73'',''364.74'',
	''364.75'',''364.76'',''364.77'',''364.81'',''365.20'',''365.21'',''365.23'',''365.24'',''365.51'',
	''365.52'',''365.59'',''365.60'',''365.61'',''365.62'',''365.63'',''365.64'',''365.65'',''365.81'',
	''365.82'',''365.83'',''365.89'',''366.11'',''366.20'',''366.21'',''366.22'',''366.23'',''366.32'',
	''366.33'',''370.03'',''371.00'',''371.01'',''371.02'',''371.03'',''371.04'',''371.20'',''371.21'',
	''371.22'',''371.23'',''371.43'',''371.44'',''371.50'',''371.51'',''371.52'',''371.53'',''371.54'',
	''371.55'',''371.56'',''371.57'',''371.58'',''376.50'',''376.51'',''376.52'',''379.32'', ''379.33'',
	''379.34'',''379.42'',''743.30'',''743.31'',''743.36''))
	or (substring(pcICD.findingdetail, 1, 5) IN (''364.3'',''366.9'',''367.0'',''871.0'',''871.1'',
	''871.2'',''871.3'',''871.4'',''871.5'',''871.6'',''871.7'',''871.9'',''921.3'',''940.0'',''940.1'',
	''940.2'',''940.3'',''940.4'',''940.5'',''940.9'',''950.0'',''950.1'',''950.2'',''950.3'',''950.9'')))
)

DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
		IF((Select COUNT(*) FROM #TBLPATDET
		INNER JOIN PatientReceivableServices prs ON prs.Service in (''65235'', ''65800'', ''65810'', ''65815'', ''65860'', ''65880'', ''65900'',
		''65920'', ''65930'', ''66030'', ''66250'', ''66820'', ''66825'', ''66830'', ''66852'', ''66986'',
		''67005'', ''67010'', ''67015'', ''67025'', ''67028'', ''67030'', ''67031'', ''67036'', ''67038'',
		''67039'', ''67101'', ''67105'', ''67107'', ''67108'', ''67110'', ''67112'', ''67141'', ''67145'',
		''67250'', ''67255'') AND prs.Status=''A''
		INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice  AND pr.PatientId=#TBLPATDET.PatientID AND pr.PatientId=@PTNID 
		AND pr.invoicedate between @StartDate AND @EndDate
		WHERE 
		Appdate <= prs.ServiceDate + 300)>0)
			BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSurgStatus=''1'' WHERE #TBLPATDET.PatientId=@PTNID
					END
				ELSE
					BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSurgStatus=''2'' WHERE #TBLPATDET.PatientId=@PTNID
					END
		FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR

Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,#TBLPATDET.Service as tq_cpt
,#TBLPATDET.TMPSurgStatus as tq_surgcomplications
,#TBLPATDET.TMPTamStatus as tq_tamsulosin
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPSurgStatus,
	TMPTamStatus
ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources

END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetCataracts_Visual_Acuity]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetCataracts_Visual_Acuity]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
----------------------------Measure #191  Cataract Surgery VA PQRS -----------------------
DECLARE @TotalParm as int;
DECLARE @CountParam as int;
DECLARE @PTNID AS NVARCHAR(200);

SET @TotalParm=''0'';
SET @CountParam=''0'';


CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PCIcd.FindingDetail,prs.Service,0 TMPStatus,case 
		when substring(prs.Modifier , 1, 2) = ''LT'' THEN ''OS'' 
		when substring(prs.Modifier , 3, 4) = ''LT'' THEN ''OS'' 
		when substring(prs.Modifier , 1, 2) = ''RT'' THEN ''OD'' 
		when substring(prs.Modifier , 3, 4) = ''RT'' THEN ''OD'' 
		else ''other'' 
		end As Modifier
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 180000)
 and ap.AppDate between @StartDate and @EndDate 
INNER JOIN PatientClinical pc ON pc.AppointmentId=ap.AppointmentId and pd.PatientId=pc.PatientId and pc.status = ''A''
and pc.clinicaltype in (''B'', ''K'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''66840'', ''66850'', ''66852'',
			 ''66920'', ''66930'', ''66940'', ''66982'', ''66983'', ''66984'')  AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
AND pr.invoicedate between Convert(Varchar(10),@StartDate,112) and Convert(Varchar(10),@EndDate,112) 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')
INNER JOIN PatientClinical pcICD ON pcICD.AppointmentId = ap.AppointmentId AND pcICD.PatientId=Pd.PatientId
AND pcICD.ClinicalType in(''b'',''k'') AND pcICD.Status=''A''
WHERE pd.PatientId NOT IN
(
SELECT PdICD.PatientID FROM PatientDemographics PdICD
INNER JOIN Appointments apICD ON PdICD.PatientId=apICD.PatientId and apICD.AppDate <ap.AppDate
INNER JOIN PatientClinical pcICD ON pcICD.AppointmentId = apICD.AppointmentId AND pcICD.PatientId=PdICD.PatientId
AND pcICD.ClinicalType in(''b'',''k'') AND pcICD.Status=''A''
AND (SUBSTRING(pcICD.FindingDetail,1,6)  IN(''360.00'',''360.01'',''360.02'',''360.03'',''360.04'',
	''360.11'',''360.12'',''360.13'', ''360.14'', ''360.19'', ''360.20'',''360.21'',''360.23'', ''360.24'', 
	''360.29'', ''360.32'', ''361.00'', ''361.01'', ''361.02'', ''361.03'', ''361.04'', ''361.05'', 
	''361.06'', ''361.07'', ''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'', ''362.07'',
	''362.12'', ''362.16'', ''362.18'', ''362.21'', ''362.31'', ''362.32'', ''362.35'', ''362.36'', 
	''362.41'', ''362.42'', ''362.43'', ''362.50'', ''362.51'', ''362.52'', ''362.53'', ''362.54'', 
	''362.55'', ''362.56'', ''362.57'', ''362.70'', ''362.71'', ''362.72'', ''362.73'', ''362.74'', 
	''362.75'', ''362.76'', ''362.81'', ''362.82'', ''362.83'', ''362.84'', ''362.85'', ''362.89'', 
	''363.00'', ''363.01'', ''363.03'', ''363.04'', ''363.05'', ''363.06'', ''363.07'', ''363.08'', 
	''363.10'', ''363.11'', ''363.12'', ''363.14'', ''363.15'', ''363.20'', ''363.21'', ''363.22'', 
	''363.30'', ''363.31'', ''363.32'', ''363.33'', ''363.35'', ''363.43'', ''363.50'', ''363.51'', 
	''363.52'', ''363.53'', ''363.54'', ''363.55'', ''363.56'', ''363.57'', ''363.61'', ''363.62'', 
	''363.63'', ''363.72'', ''364.00'', ''364.01'', ''364.02'', ''364.03'', ''364.04'', ''364.05'',
	''364.10'', ''364.11'', ''364.21'', ''364.22'', ''364.23'', ''364.24'', ''365.20'', ''365.21'',
	''365.23'', ''365.24'', ''365.41'', ''365.42'', ''365.43'', ''365.44'', ''365.51'',
	''365.52'', ''365.59'', ''365.60'', ''365.61'', ''365.62'', ''365.63'', ''365.64'', ''365.65'', 
	''365.81'', ''365.82'', ''365.83'', ''365.89'', ''366.32'', ''366.33'', ''368.01'', ''368.02'', 
	''368.03'', ''368.41'', ''369.00'', ''369.01'', ''369.02'', ''369.03'', ''369.04'', ''369.05'', 
	''369.06'', ''369.07'', ''369.08'', ''369.10'', ''369.11'', ''369.12'', ''369.13'', ''369.14'', 
	''369.15'', ''369.16'', ''369.17'', ''369.18'', ''370.03'', ''371.00'', ''371.01'', ''371.02'',
	''371.03'', ''371.04'', ''371.20'', ''371.21'', ''371.22'', ''371.23'', ''371.43'', ''371.44'',
	''371.50'', ''371.51'', ''371.52'', ''371.53'', ''371.54'',
	''371.55'', ''371.56'', ''371.57'', ''371.58'', ''371.60'', ''371.61'', ''371.62'', 
	''371.70'', ''371.71'', ''371.72'', ''371.73'', ''377.10'', ''377.11'', ''377.12'', ''377.13'', 
	''377.14'', ''377.15'', ''377.16'', ''377.30'', ''377.31'', ''377.32'', ''377.33'', ''377.34'', 
	''377.39'', ''377.41'', ''377.51'', ''377.52'', ''377.53'', ''377.54'', ''377.75'', 
	''379.04'', ''379.05'', ''379.06'', ''379.07'', ''379.09'', ''379.11'', ''379.12'', ''379.51'') 
	or (SUBSTRING(pcICD.FindingDetail, 1, 5)   in (''364.3'',''365.9'',''871.0'',''871.1'',
	''871.2'',''871.3'',''871.4'',''871.5'',''871.6'',''871.7'',''871.9'',''921.3'',''940.0'',''940.1'',
	''940.2'',''940.3'',''940.4'',''940.5'',''940.9'',''950.0'',''950.1'',''950.2'',''950.3'',''950.9'')))
)

DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
			IF((Select COUNT(*) FROM #TBLPATDET
			LEFT OUTER  JOIN PatientClinical pcVision ON #TBLPATDET.PatientId=pcVision.PatientId 
			LEFT OUTER JOIN Appointments apvision ON #TBLPATDET.PatientId=apvision.PatientId 
			And apvision.AppDate < #TBLPATDET.AppDate and pcVision.AppointmentId=apvision.AppointmentId
			LEFT OUTER JOIN PatientClinical pcEYE ON #TBLPATDET.PatientId=pcEYE.PatientId AND pcEye.ClinicalType=''F''
			And apvision.AppDate < #TBLPATDET.AppDate and pcEYE.AppointmentId=apvision.AppointmentId
			AND pcVision.PatientId=@PTNID AND pcEYE.PatientId=@PTNID
			Where ((Substring(pcVision.FindingDetail, CHARINDEX(''=T'', pcVision.FindingDetail)+2, 4) between ''1080'' and ''1084'' )
			OR (Substring(pcVision.FindingDetail, CHARINDEX(''=T'', pcVision.FindingDetail)+2, 4) between ''1051'' and ''1055'')
			AND substring(pcEye.Symptom, 2, 7) =''vision '' AND substring(pcEye.FindingDetail, 2, 2) = #TBLPATDET.Modifier 
			AND apvision.AppDate >= #TBLPATDET.AppDate +300))>0)
					BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
					END
			ELSE
					BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''4'' WHERE #TBLPATDET.PatientId=@PTNID
					END
	FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR



Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9
,#TBLPATDET.Service as tq_cpt
,#TBLPATDET.TMPStatus as tq_visualacuity
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPStatus
ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetDiabetic_Retinopathy_Communication_Physician]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
As
BEGIN
---------------------------MEASURE #19 DR COMMUNICATE WITH DOCTOR   PQRS----------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PtnId NVARCHAR(100);

SET @TotParm=0;
SET @CountParm=0;
SET @PtnId=''0'';

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,0 TMPFStatus,0 TMPFCStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 180000) 
and ap.AppDate between @StartDate and @EndDate   
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType in(''b'',''k'') AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'',
 ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99324'',
 ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER Join PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')


DECLARE PTNCURSOR CURSOR
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PtnId
WHILE @@FETCH_STATUS=0
BEGIN
--PRINT ''''+@PtnId+''''
IF((SELECT COUNT(*) FROM PatientClinical pcF,PatientClinical pcQ 
	WHERE pcF.patientID=@PtnId AND pcQ.patientId=@PtnId AND
	pcF.ClinicalType=''F'' AND pcF.Status=''A'' AND pcQ.ClinicalType=''Q'' And pcQ.Status=''A''
	AND (((pcF.Symptom = ''/DILATION'') OR (pcQ.Symptom LIKE ''%&''))
						AND 
		((pcQ.FindingDetail IN (''Q008'', ''Q009'', ''362.50.22'', ''P405'', ''P423'', ''P427'', ''P648''
			, ''P656'', ''P657'', ''P658'', ''P659'', ''P660'', ''P661'', ''P662'', ''P663'', ''P664'', ''P665'', ''P666''
			, ''P667'', ''P668'', ''Q026'', ''Q027'', ''Q028'', ''Q029'', ''Q151'', ''Q173'')) 
			  OR (SUBSTRING(pcQ.FindingDetail, 1, 6) in (''362.07'', ''362.53'', ''362.81'', ''362.83'')))))>0)
	BEGIN
	--PRINT ''IN IF FOR ''+@PtnID+''''=
		UPDATE #TBLPATDET SET TMPFStatus=''1'' WHERE PatientID=@PtnId;
		IF ((SELECT COUNT(*) FROM PatientClinical pcA WHERE pcA.ClinicalType=''A''
		AND pcA.Status=''A'' AND pcA.FindingDetail like ''SEND A CONSULTATION LETTER%'' AND pcA.PatientId=@PtnId)>0)
			BEGIN
	--PRINT ''IN ELSE FOR ''+@PtnID+''''
				UPDATE #TBLPATDET SET TMPFCStatus=''1'' WHERE PatientID=@PtnId;
			END	
		ELSE
			BEGIN
				UPDATE #TBLPATDET SET TMPFCStatus=''4'' WHERE PatientID=@PtnId;
			END
	END
ELSE 
	BEGIN
	--PRINT ''IN ELSE FOR ''+@PtnID+''''
		UPDATE #TBLPATDET SET TMPFSTATUS=''4'',TMPFCStatus=''4'' WHERE PatientID=@PtnId;
	END
FETCH NEXT FROM PTNCURSOR INTO @PtnId
END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR

Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPFStatus as tq_fundus
,#TBLPATDET.TMPFCSTATUS AS tq_funduscom
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPFStatus,
	TMPFCSTATUS
	

ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources


END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetDiabetic_Retinopathy_MacularEdema]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



CREATE  PROCEDURE [dbo].[USP_PQRI_GetDiabetic_Retinopathy_MacularEdema]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
-------------------------Measure #18 DR pres/absence macular edema/severity DR  PQRS ----------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END


Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,0 TMPStatus
 INTO #TBLPATDET  from PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=(ap.AppDate - 180000)
AND ap.AppDate between @StartDate and @EndDate  
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType in(''b'',''k'') AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''362.01'', ''362.02'', ''362.03'', ''362.04'', ''362.05'', ''362.06'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215''
	, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')

DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
			IF((Select  COUNT(*) FROM #TBLPATDET 
				LEFT OUTER JOIN Appointments ap ON ap.PatientId=@PTNID 
				AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000
				LEFT OUTER JOIN PatientClinical pcF On  pcF.ClinicalType=''F'' AND pcF.Status=''A'' 
				AND #TBLPATDET.PatientId=pcF.PatientId AND ap.AppointmentId =pcF.AppointmentId
				AND pcF.PatientId=@PTNID
				LEFT OUTER JOIN PatientClinical pcQ On  pcQ.ClinicalType=''Q'' AND pcQ.Status=''A'' 
				AND #TBLPATDET.PatientId=pcQ.PatientId AND ap.AppointmentId =pcQ.AppointmentId
				AND pcQ.PatientId=@PTNID
				WHERE 
				(
					(pcF.Symptom =''/DILATION'' OR  pcQ.Symptom like ''%&'') 
					and
					((pcQ.FindingDetail in (''Q008'', ''Q009'', ''362.50.22'', ''P405'', ''P423'', ''P427'', ''P648''
					, ''P656'', ''P657'', ''P658'', ''P659'', ''P660'', ''P661'', ''P662'', ''P663'', ''P664'', ''P665'', ''P666''
					, ''P667'', ''P668'', ''Q026'', ''Q027'', ''Q028'', ''Q029'', ''Q151'', ''Q173'')) 
					OR 
					(substring(pcQ.FindingDetail, 1, 6) in (''362.07'', ''362.53'', ''362.81'', ''362.83'')))
				))>0)
				BEGIN	
						UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
				END
			ELSE
				BEGIN
						UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''4'' WHERE #TBLPATDET.PatientId=@PTNID
				END
	FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR


Select PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9,
 #TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_fundus
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPStatus
	
ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources

END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetHIT_eRX]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetHIT_eRX]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
----------------------MEASURE #125 eRx ok for 2011  PQRS----------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(200);
SET @TotParm=0;
SET @CountParm=0;


CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,prs.Service,0 TMPStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid 
and ap.AppDate between @StartDate and @EndDate 
--INNER JOIN PatientClinical pc ON pc.AppointmentId=ap.AppointmentId And pd.PatientId=ap.PatientId
INNER JOIN PatientReceivableServices prs ON prs.Service in (''92002'', ''92004'', ''92012'', ''92014''
, ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99211'', ''99212'', ''99213'', ''99214'', ''99215''
, ''90801'', ''90802'', ''90804'', ''90805'', ''90806'', ''90807'', ''90808'', ''90809'', ''90862''
, ''96150'', ''96151'', ''96152''
, ''99304'', ''99305'', ''99306'', ''99307'', ''99308'', ''99309'', ''99310'', ''99315'', ''99316''
, ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', ''99335'', ''99336'', ''99337''
, ''99341'', ''99342'', ''99343'', ''99344'', ''99345'', ''99346'', ''99347'', ''99348'', ''99349''
, ''99350'', ''G0101'', ''G0108'', ''G0109'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER Join PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')

DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
			IF((Select COUNT(*) FROM #TBLPATDET 
				INNER JOIN PatientClinical pcA ON pcA.ClinicalType = ''A'' 
				AND SUBSTRING(pcA.FindingDetail, 1, 2) = ''RX'' AND pcA.Status=''A'' AND LEN(pcA.ImageInstructions) > 20
				AND #TBLPATDET.PatientID=pcA.PatientID AND pcA.AppointmentId=#TBLPATDET.AppointmentId AND pcA.PatientId=@PTNID)>0)
			BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
					END
				ELSE
					BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''2'' WHERE #TBLPATDET.PatientId=@PTNID
					END
	FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR


Select DISTINCT PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_erx
FROM #TBLPATDET ORDER BY PatientID DESC
DROP TABLE #TBLPATDET

DROP TABLE #TMPResources


END


')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetHIT_USE_EHR]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetHIT_USE_EHR]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
---------------------MEASURE #124 EHR PQRS---------------------------------
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(100)
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
--PRINT ''IF''
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
--PRINT ''ELSE''
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,0 TMPStatus INTO #TBLPATDET
from PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid 
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId 
and ap.AppDate between @StartDate and @EndDate 
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014''
, ''99201'', ''99202'', ''99203'', ''99204'', ''99205'', ''99211'', ''99212'', ''99213'', ''99214'', ''99215''
, ''90801'', ''90802'', ''90804'', ''90805'', ''90806'', ''90807'', ''90808'', ''90809''
, ''92506'', ''92507'', ''92526''
, ''92541'', ''92542'', ''92543'', ''92544'', ''92548'', ''92552'', ''92553'', ''92555''
, ''92557'', ''92561'', ''92562'', ''92563'', ''92564'', ''92565'', ''92567'', ''92568'', ''92570'', ''92571'', ''92572'', ''92575'', ''92576'', ''92577'', ''92579''
, ''92582'', ''92584'', ''92585'', ''92586'', ''92587'', ''92588'', ''92601'', ''92602'', ''92603'', ''92604'', ''92610'', ''92611'', ''92612''
, ''92620'', ''92621'', ''92625'', ''92626'', ''92627''
, ''92640'', ''95920'', ''96150'', ''96151'', ''96152'', ''97001'', ''97002'', ''97003'', ''97004'', ''97750''
, ''97802'', ''97803'', ''97804'', ''98940'', ''98941'', ''98942''
, ''D7140'', ''D7210'', ''G0101'', ''G0108'', ''G0109'', ''G0270'', ''G0271'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.PatientId=pd.PatientId AND pr.AppointmentId=ap.AppointmentId
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')

DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
		IF((Select COUNT(*) FROM #TBLPATDET 
			LEFT OUTER JOIN PatientClinical pc ON #TBLPATDET.PatientId=pc.PatientId  
			AND #TBLPATDET.AppointmentId=pc.AppointmentId AND pc.PatientId=@PTNID
			WHERE ClinicalType In(''Q'', ''I'') )>0)
				BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
					END
		ELSE
				BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''2'' WHERE #TBLPATDET.PatientId=@PTNID
				END
	FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR


Select DISTINCT PATIENTID as patient_os_id ,FirstName tq_firstname,
#TBLPATDET.LastName as tq_lastname, 
substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi,
substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_emr3
FROM #TBLPATDET ORDER BY PatientID DESC

DROP TABLE #TBLPATDET

DROP TABLE #TMPResources

END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_PQRI_GetPOAGDetails]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE PROCEDURE [dbo].[USP_PQRI_GetPOAGDetails]
(
@StartDate NVARCHAR(100),
@EndDate NVARCHAR(100),
@ResourceIds NVARCHAR(1000)
)
AS
BEGIN
--------------------Measure #12 POAG PQRS----------------------

--SET @ResourceIds=''''
DECLARE @TotParm INT;
DECLARE @CountParm INT;
DECLARE @PTNID NVARCHAR(100)
SET @TotParm=0;
SET @CountParm=0;

CREATE TABLE #TMPResources(ResourceId NVARCHAR(1000))
IF @ResourceIds=''''
BEGIN
--PRINT ''IF''
INSERT INTO #TMPResources SELECT Resources.ResourceId FROM Resources 
WHERE ResourceType IN(''D'',''Q'')
END
ELSE
BEGIN
--PRINT ''ELSE''
DECLARE @TmpResourceIds VARCHAR(100)
DECLARE @ResId VARCHAR(10)
SELECT @TmpResourceIds =@ResourceIds
WHILE PATINDEX(''%,%'',@TmpResourceIds)<>0
	BEGIN
		SELECT @ResId=LEFT(@TmpResourceIds,PATINDEX(''%,%'',@TmpResourceIds)-1)
		SELECT @TmpResourceIds=REPLACE(@TmpResourceIds,@ResId+'','','''')
		INSERT INTO #TMPResources values(@ResId)
	END
	INSERT INTO #TMPResources values(@TmpResourceIds)
END

Select DISTINCT pd.PatientId,ap.AppointmentId,pd.FirstName,pd.LastName,ap.AppDate,re.NPI,pd.BirthDate
,PC.FindingDetail,prs.Service,0 TMPStatus
INTO #TBLPATDET  FROM PatientDemographics pd
INNER JOIN Appointments ap ON pd.PatientId=ap.Patientid AND pd.BirthDate <=ap.AppDate -180000 
 and ap.AppDate between @StartDate AND @EndDate
INNER JOIN PatientClinical pc ON pc.AppointmentId = ap.AppointmentId AND pc.PatientId=pd.PatientId
AND pc.ClinicalType in(''b'',''k'') AND pc.Status=''A''
AND SUBSTRING(pc.FindingDetail,1,6)  IN(''365.10'', ''365.11'', ''365.12'', ''365.15'')
INNER JOIN PatientReceivableServices prs ON prs.Service in(''92002'', ''92004'', ''92012'', ''92014'', ''99201'', ''99202'', 
''99203'', ''99204'', ''99205'', ''99212'', ''99213'', ''99214'', ''99215'', ''99304'', ''99305'', ''99306'',
 ''99307'', ''99308'', ''99309'', ''99310'', ''99324'', ''99325'', ''99326'', ''99327'', ''99328'', ''99334'', 
 ''99335'', ''99336'', ''99337'') AND prs.Status=''A''
INNER JOIN PatientReceivables pr ON prs.Invoice=pr.Invoice AND pr.AppointmentId=ap.AppointmentId 
INNER JOIN Resources re on re.ResourceId = pr.BillToDr And re.ResourceId IN(Select #TMPResources.ResourceId from #TMPResources)
INNER Join PracticeInsurers pri on pri.InsurerId = pr.InsurerId and InsurerReferenceCode in (''m'', ''n'', ''['', '']'')


--DECLARE @PTNID NVARCHAR(100)
DECLARE PTNCURSOR CURSOR 
FOR SELECT DISTINCT PATIENTID FROM #TBLPATDET
OPEN PTNCURSOR
FETCH NEXT FROM PTNCURSOR INTO @PTNID
WHILE @@FETCH_STATUS=0
	BEGIN
			IF((Select COUNT(*) FROM #TBLPATDET
				LEFT OUTER JOIN Appointments ap ON ap.PatientId=@PTNID 
				AND ap.AppDate BETWEEN @StartDate-10000 AND @EndDate+10000
				LEFT OUTER JOIN PatientClinical pcF On  pcF.ClinicalType=''F'' AND pcF.Status=''A''
				AND #TBLPATDET.PatientId=pcF.PatientId AND pcF.AppointmentId=ap.AppointmentId
				AND pcF.PatientId=@PTNID
				LEFT OUTER JOIN PatientClinical pcQ On  pcQ.ClinicalType=''Q'' AND pcQ.Status=''A''
				AND #TBLPATDET.PatientId=pcQ.PatientId AND  pcQ.AppointmentId=ap.AppointmentId 
				AND pcQ.PatientId=@PTNID
				WHERE
				((pcF.Symptom =''/CUP/DISC'') 
				OR ((pcQ.ImageDescriptor =  ''L'' and pcQ.FindingDetail <> ''LN'')
				and (pcQ.FindingDetail not in (''365.10'', ''365.11'', ''365.12'', ''365.15'')))
				OR (pcF.Symptom = ''/PQRI (POAG, OPTIC NERVE EVAL)'')))>0)
					BEGIN
					PRINT ''''+@PTNID+''''
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''1'' WHERE #TBLPATDET.PatientId=@PTNID
					END
			ELSE
				
				BEGIN
							UPDATE #TBLPATDET SET  #TBLPATDET.TMPSTATUS=''3'' WHERE #TBLPATDET.PatientId=@PTNID
				END
				
	FETCH NEXT FROM PTNCURSOR INTO @PTNID
	END
CLOSE PTNCURSOR
DEALLOCATE PTNCURSOR

DROP TABLE #TMPResources


Select PATIENTID as patient_os_id 
,FirstName tq_firstname
,#TBLPATDET.LastName as tq_lastname
,substring(#TBLPATDET.appdate, 5, 2) + ''/'' + substring(#TBLPATDET.appdate, 7, 2) + ''/'' + substring(#TBLPATDET.appdate, 1, 4) as tq_encounterdt
,#TBLPATDET.npi as tq_physiciannpi
,substring(#TBLPATDET.BirthDate, 5, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 7, 2) + ''/'' + substring(#TBLPATDET.BirthDate, 1, 4) as tq_dob
,''1'' as tq_medicare2
,max (substring(#TBLPATDET.FindingDetail, 1, 6)) as tq_icd9
,#TBLPATDET.Service as tq_em
,#TBLPATDET.TMPStatus as tq_opticnerve3
FROM #TBLPATDET 
GROUP BY PatientId,
	LastName,
	FirstName,
	AppDate,
	NPI,
	BirthDate,
	Service,
	TMPStatus
	

ORDER BY PatientID DESC

DROP TABLE #TBLPATDET
END

')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetFieldParams]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_RulesEngine_GetFieldParams]      
 -- Add the parameters for the stored procedure here      
AS      
BEGIN   
Select FldArea,FldName,FldType from IE_FieldParams   
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetRulePhrases]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_RulesEngine_GetRulePhrases]      
 -- Add the parameters for the stored procedure here      
AS      
BEGIN   
Select RuleId,Slno,FieldArea,FieldName,Operation,FieldValue,AndOrFlag from IE_RulesPhrases Order By 1,2       
end 
')
EXEC('
/****** Object:  StoredProcedure [dbo].[USP_RulesEngine_GetRules]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[USP_RulesEngine_GetRules]              
AS              
BEGIN       
Select IE.* from IE_Rules IE
LEFT OUTER  JOIN encounter_ie_overrides IER 
ON IE.RULEID=IER.RULEID 
WHERE CONVERT(NVARCHAR,IER.Comments)='''' 
OR CONVERT(NVARCHAR,IER.Comments) IS NULL
END  
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_Spectacle_Rx]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_Spectacle_Rx]
(@aptDate varchar(8), @patid int)
AS
SELECT dm.*, pc.*, ap.appdate, ap.apptime 
FROM patientClinical pc 
INNER JOIN appointments ap ON pc.appointmentid = ap.appointmentid 
INNER JOIN patientdemographics dm ON ap.patientid = dm.patientid
WHERE ((ap.appdate = @aptDate) OR (@aptDate = ''0'')) And
((ap.patientid = @patid) OR (@patid = 0)) And
(Left(pc.FindingDetail,13) = ''DISPENSE SPEC'') And
(pc.status = ''A'') And (pc.clinicalType = ''A'')
ORDER BY ap.appdate DESC, dm.lastname ASC, dm.firstname ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_SurgeryTransactions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_SurgeryTransactions]
@SDate varchar(8),
@EDate varchar(8),
@LocId Int,
@ResId Int,
@StaId varchar(1)
AS
SELECT DISTINCT
	ap.AppDate as OrderDate, ap.AppointmentId as SurgeryApptId,
	Appointments.AppDate, Appointments.ResourceId1,
	Resources.ResourceName, Appointments.ResourceId2, 
	Appointments.AppointmentId, PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientDemographics.MiddleInitial, 
	PatientDemographics.HomePhone, PatientDemographics.PatientId, 
	PatientClinical.ClinicalId, PatientClinical.ClinicalType, 
	PatientClinical.Symptom, PatientClinical.FindingDetail, 
	PatientClinical.ImageDescriptor, PatientClinical.ImageInstructions, 
	PatientClinical.Status, PatientClinical.Surgery
FROM PatientClinicalSurgeryPlan
INNER JOIN PatientClinical
ON PatientClinicalSurgeryPlan.SurgeryClnId = PatientClinical.ClinicalId
INNER JOIN Appointments
ON Appointments.AppointmentId = CAST(PatientClinicalSurgeryPlan.SurgeryValue AS INT)
INNER JOIN PatientDemographics 
ON PatientDemographics.PatientId = PatientClinical.PatientId
INNER JOIN Appointments ap
ON PatientClinical.AppointmentId = ap.AppointmentId
INNER JOIN Resources 
ON Resources.ResourceId = Appointments.ResourceId1
WHERE (PatientClinicalSurgeryPlan.SurgeryRefType = ''F'') And
(PatientClinicalSurgeryPlan.SurgeryStatus = ''A'') And
(((Appointments.AppDate) <= @SDate) Or (@SDate = ''0'')) And  
(((Appointments.AppDate) >= @EDate) Or (@EDate = ''0'')) And 
((Appointments.ResourceId2 = @LocId) OR (@LocId = -1)) And
((Appointments.ResourceId1 = @ResId) OR (@ResId = -1)) And
((PatientClinical.Surgery = @StaId) OR (@StaId = ''0''))
ORDER BY Appointments.AppDate DESC, PatientDemographics.LastName ASC , PatientDemographics.FirstName ASC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_UnassignPayments]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_UnassignPayments]
(@PatId int)
AS
SELECT 
	PatientReceivablePayments.PaymentAmount,
	PatientReceivablePayments.PaymentType, 
	PatientReceivables.Invoice, PatientReceivables.InvoiceDate,
	PatientDemographics.LastName, 
	PatientDemographics.FirstName, PatientReceivables.ReceivableId,
	PatientReceivablePayments.PaymentId, 
	PatientReceivables.AppointmentId, PatientReceivables.PatientId,
	PatientReceivablePayments.PaymentServiceItem, 
	PatientReceivablePayments.PaymentDate
FROM ((PatientReceivablePayments 
INNER JOIN PatientReceivables 
ON PatientReceivablePayments.ReceivableId = PatientReceivables.ReceivableId) 
INNER JOIN PatientDemographics 
ON PatientReceivables.PatientId = PatientDemographics.PatientId) 
INNER JOIN Appointments 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId
WHERE (PatientReceivablePayments.PaymentType = ''P'') AND 
(PatientReceivablePayments.PaymentServiceItem <= 0) AND
((PatientReceivables.PatientId = @PatId) Or (@PatId = ''0''))
ORDER BY PatientReceivables.InvoiceDate DESC
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_UpdateImgDesc]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE proc [dbo].[usp_UpdateImgDesc] 
(
@ClinicalId NVARCHAR(200),
@ImageDescriptor NVARCHAR(200)
)
AS
BEGIN
DECLARE @ImgDesc NVARCHAR(1000)
SELECT @ImgDesc=ImageDescriptor from PatientClinical WHERE ClinicalId=@ClinicalId
IF @ImgDesc = NULL
BEGIN
UPDATE PatientClinical 
SET ImageDescriptor=@ImageDescriptor
WHERE ClinicalId=@ClinicalId
END
ELSE
BEGIN
SET @ImgDesc =@ImageDescriptor + @ImgDesc
UPDATE PatientClinical 
SET ImageDescriptor=@ImgDesc
WHERE ClinicalId=@ClinicalId
AND ImageDescriptor <> ''D%''
END
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_UpdateImgInstructions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_UpdateImgInstructions]
(
@PrescriptionGuId uniqueidentifier,
@ClinicalId NVARCHAR(200)
)
AS
BEGIN

UPDATE PatientClinical SET ImageInstructions=@PrescriptionGuId
WHERE ClinicalId=@ClinicalId
END
')
EXEC('
/****** Object:  StoredProcedure [dbo].[usp_UpdatePatientImgDet]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE PROCEDURE [dbo].[usp_UpdatePatientImgDet]
(
@ClinicalId NVARCHAR(200),
@PrescriptionGuId UNIQUEIDENTIFIER,
@PatientId NVARCHAR(200),
@ImageDescriptor NVARCHAR(200)
)
AS
BEGIN
DECLARE @ImgDesc NVARCHAR(500)
SELECT @ImgDesc=ImageDescriptor FROM PatientClinical WHERE PatientId=@PatientId AND ClinicalId=@ClinicalId;
--PRINT  (''''+@ImgDesc+'''')
SET @ImgDesc=REPLACE(@ImgDesc,''C''+@ImageDescriptor+'''','''')
--PRINT  (''''+@ImgDesc+'''')
SET @ImgDesc=RTRIM(LTRIM(@ImgDesc))
--PRINT  (''''+@ImgDesc+'''')
UPDATE PatientClinical 
SET ImageInstructions=@PrescriptionGuId,
ImageDescriptor=@ImgDesc
WHERE ClinicalId=@ClinicalId AND PatientId=@PatientId
END

')
EXEC('
/****** Object:  UserDefinedFunction [dbo].[CharTable]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE FUNCTION [dbo].[CharTable]
                    (@list      nvarchar(2000),
                     @delimiter nchar(1) = N'','')
         RETURNS @tbl TABLE (listpos int IDENTITY(1, 1) NOT NULL,
                             nstr    nvarchar(2000)) AS

   BEGIN
      DECLARE @pos      int,
              @textpos  int,
              @chunklen smallint,
              @tmpstr   nvarchar(2000),
              @leftover nvarchar(2000),
              @tmpval   nvarchar(2000)

      SET @textpos = 1
      SET @leftover = ''''
      WHILE @textpos <= datalength(@list) / 2
      BEGIN
         SET @chunklen = 2000 - datalength(@leftover) / 2
         SET @tmpstr = @leftover + substring(@list, @textpos, @chunklen)
         SET @textpos = @textpos + @chunklen

         SET @pos = charindex(@delimiter, @tmpstr)

         WHILE @pos > 0
         BEGIN
            SET @tmpval = ltrim(rtrim(left(@tmpstr, @pos - 1)))
            INSERT @tbl (nstr) VALUES(@tmpval)
            SET @tmpstr = substring(@tmpstr, @pos + 1, len(@tmpstr))
            SET @pos = charindex(@delimiter, @tmpstr)
         END

         SET @leftover = @tmpstr
      END

      INSERT @tbl(nstr) VALUES (ltrim(rtrim(@leftover)))
   RETURN
   END
')
EXEC('
/****** Object:  UserDefinedFunction [dbo].[IntTable]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE FUNCTION [dbo].[IntTable] (@list nvarchar(2000))
      RETURNS @tbl TABLE (listpos int IDENTITY(1, 1) NOT NULL,
                          number  int NOT NULL) AS
   BEGIN
      DECLARE @pos      int,
              @textpos  int,
              @chunklen smallint,
              @str      nvarchar(2000),
              @tmpstr   nvarchar(2000),
              @leftover nvarchar(2000)

      SET @textpos = 1
      SET @leftover = ''''
      WHILE @textpos <= datalength(@list) / 2
      BEGIN
         SET @chunklen = 2000 - datalength(@leftover) / 2
         SET @tmpstr = ltrim(@leftover + substring(@list, @textpos, @chunklen))
         SET @textpos = @textpos + @chunklen

         SET @pos = charindex('' '', @tmpstr)
         WHILE @pos > 0
         BEGIN
            SET @str = substring(@tmpstr, 1, @pos - 1)
            INSERT @tbl (number) VALUES(convert(int, @str))
            SET @tmpstr = ltrim(substring(@tmpstr, @pos + 1, len(@tmpstr)))
            SET @pos = charindex('' '', @tmpstr)
         END

         SET @leftover = @tmpstr
      END

      IF ltrim(rtrim(@leftover)) <> ''''
         INSERT @tbl (number) VALUES(convert(int, @leftover))

      RETURN
   END
')
EXEC('
/****** Object:  Table [dbo].[Appointments]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[Appointments](
	[AppointmentId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[AppTypeId] [int] NULL,
	[AppDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AppTime] [int] NULL,
	[Duration] [int] NULL,
	[ReferralId] [int] NULL,
	[PreCertId] [int] NULL,
	[TechApptTypeId] [int] NULL,
	[ScheduleStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ActivityStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Comments] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceId1] [int] NULL,
	[ResourceId2] [int] NULL,
	[ResourceId3] [int] NULL,
	[ResourceId4] [int] NULL,
	[ResourceId5] [int] NULL,
	[ResourceId6] [int] NULL,
	[ResourceId7] [int] NULL,
	[ResourceId8] [int] NULL,
	[ApptInsType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfirmStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptTypeCat] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SetDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VisitReason] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Appointments] PRIMARY KEY CLUSTERED 
(
	[AppointmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[AppointmentTrack]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[AppointmentTrack](
	[ApptTrackId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentId] [int] NULL,
	[UserId] [int] NULL,
	[ComputerName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptAction] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptTrackDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptTrackTime] [int] NULL,
	[Comment] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApptTime] [int] NULL,
	[ApptDoc] [int] NULL,
	[ApptLoc] [int] NULL,
	[ApptType] [int] NULL,
 CONSTRAINT [PK_AppointmentTrack] PRIMARY KEY CLUSTERED 
(
	[ApptTrackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[AppointmentType]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[AppointmentType](
	[AppTypeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentType] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Question] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Duration] [int] NULL,
	[DurationOther] [int] NULL,
	[Recursion] [int] NULL,
	[ResourceId1] [int] NULL,
	[ResourceId2] [int] NULL,
	[ResourceId3] [int] NULL,
	[ResourceId4] [int] NULL,
	[ResourceId5] [int] NULL,
	[ResourceId6] [int] NULL,
	[ResourceId7] [int] NULL,
	[ResourceId8] [int] NULL,
	[DrRequired] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PcRequired] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Period] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rank] [int] NULL,
	[OtherRank] [int] NULL,
	[RecallFreq] [int] NULL,
	[RecallMax] [int] NULL,
	[Category] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstVisitIndicator] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvailableInPlan] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EMBasis] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_AppointmentType] PRIMARY KEY CLUSTERED 
(
	[AppTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[CLInventory]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[CLInventory](
	[InventoryId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SKU] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type_] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Manufacturer] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Series] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Material] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Disposable] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WearTime] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Tint] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BaseCurve] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Diameter] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpherePower] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CylinderPower] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Axis] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddReading] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PeriphCurve] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Qty] [int] NULL,
	[Cost] [real] NULL,
	[WCost] [real] NULL,
 CONSTRAINT [PK_CLInventory] PRIMARY KEY CLUSTERED 
(
	[InventoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[CLOrders]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[CLOrders](
	[OrderId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[InventoryId] [int] NULL,
	[PatientId] [int] NULL,
	[AppointmentId] [int] NULL,
	[ShipTo] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipAddress1] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipAddress2] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipCity] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipZip] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Eye] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Qty] [int] NULL,
	[Cost] [real] NULL,
	[OrderDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderComplete] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[OrderType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReceivableId] [int] NULL,
	[Reference] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PONumber] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AlternateReference] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rx] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_CLOrders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[CMSReports]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[CMSReports](
	[CMSRepId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CMSRepCode] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CMSRepName] [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CMSQuery] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CMSSubHeading] [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsSP] [bit] NOT NULL,
	[Numerator] [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Denominator] [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ReportType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Active_YN] [bit] NULL,
 CONSTRAINT [PK_CMSReports] PRIMARY KEY CLUSTERED 
(
	[CMSRepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[CMSReports_Params]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[CMSReports_Params](
	[CMSParamId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CMSRepId] [int] NOT NULL,
	[CMSParamCode] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CMSParamDesc] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CMSParamValue] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CMSParamUnits] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_CMSReports_Params] PRIMARY KEY CLUSTERED 
(
	[CMSParamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Devices]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[Devices](
	[DeviceID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Make] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Model] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DeviceAlias] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Baud] [int] NULL,
	[Parity] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DataBits] [int] NULL,
	[StopBits] [int] NULL,
 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
(
	[DeviceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Drug]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[Drug](
	[DrugId] [int] IDENTITY(1,1) NOT NULL,
	[Drugcode] [nvarchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DisplayName] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Family1] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Family2] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Focus] [bit] NULL,
 CONSTRAINT [PK_Drug] PRIMARY KEY CLUSTERED 
(
	[DrugId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Encounter_IE_Overrides]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Encounter_IE_Overrides](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ReferenceId] [int] NOT NULL,
	[Type] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RuleId] [int] NOT NULL,
	[Comments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[EPrescription_Credentials]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[EPrescription_Credentials](
	[PARTNERName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SiteID] [int] NOT NULL,
	[URL] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Update1URL] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RowCount] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[AccountID] [int] NULL,
 CONSTRAINT [PK_EPrescription_Credentials] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[EventCodes]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[EventCodes](
	[EventId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDesc] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EventMechanism] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsInfectious] [bit] NULL,
 CONSTRAINT [PK_EventCodes] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[HL7_LabInterface]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[HL7_LabInterface](
	[InterfaceID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EncounterID] [int] NULL,
	[PatientID] [int] NULL,
	[ProviderID] [int] NULL,
	[LabName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderFileName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderDate] [datetime] NULL,
	[GeneratedOrderNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderFileNo] [int] NULL,
	[ResultFileName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResultReportDate] [datetime] NULL,
	[ResultReceivedDate] [datetime] NULL,
	[ResultOrderNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderStatus] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Error] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Comments] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReportID] [int] NULL,
	[Posted] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageID] [int] NULL,
 CONSTRAINT [PK_HL7_LabInterface] PRIMARY KEY CLUSTERED 
(
	[InterfaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[HL7_LabReports]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[HL7_LabReports](
	[ReportID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientID] [int] NOT NULL,
	[ProviderID] [int] NOT NULL,
	[ReportDate] [datetime] NULL,
	[SendingFacility] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[XMLReport] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_HL7_LabReports] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[HL7_Observation]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[HL7_Observation](
	[OBRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ReportID] [int] NOT NULL,
	[SlNo] [int] NULL,
	[OrderCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderDesc] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RequestedDate] [datetime] NULL,
	[ReceivedDate] [datetime] NULL,
	[ReportDate] [datetime] NULL,
	[NTEComments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PerformingLab] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_HL7_Observation] PRIMARY KEY CLUSTERED 
(
	[OBRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[HL7_Observation_Details]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[HL7_Observation_Details](
	[OBXID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[OBRID] [int] NOT NULL,
	[SlNo] [int] NULL,
	[TestCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TestDesc] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Result] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Units] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RefRange] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AbnormalFlag] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResultStatus] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NTEComments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_HL7_Observation_Details] PRIMARY KEY CLUSTERED 
(
	[OBXID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[IE_Events]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[IE_Events](
	[EventId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EventName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_IE_Events] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[IE_FieldParams]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[IE_FieldParams](
	[FldId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FldArea] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FldName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FldDesc] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Fldtype] [int] NOT NULL,
	[LookupId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[IE_Rules]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[IE_Rules](
	[RuleId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RuleName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EventId] [int] NOT NULL,
	[Result] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Priority] [int] NOT NULL,
	[ShowAlert_YN] [bit] NULL,
	[PatientRule_YN] [bit] NULL,
	[CreateBy] [int] NULL,
	[CreateDt] [datetime] NULL,
	[ModifyBy] [int] NULL,
	[ModifyDt] [datetime] NULL,
	[VersionNo] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PatientId] [int] NULL,
	[Emcoder_Result] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_IE_RuleMast] PRIMARY KEY CLUSTERED 
(
	[RuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_IE_RuleMast] UNIQUE NONCLUSTERED 
(
	[RuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[IE_RulesPhrases]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[IE_RulesPhrases](
	[FldPhraseId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RuleId] [int] NOT NULL,
	[SlNo] [int] NOT NULL,
	[FieldArea] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FieldName] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FieldDesp] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Operation] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FieldValue] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AndOrFlag] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FldPhraseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Immunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Immunizations](
	[ImmNo] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ImmName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ImmDuration] [int] NULL,
	[ImmCPT] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortNo] [int] NULL,
	[ImmCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Immunizations] PRIMARY KEY CLUSTERED 
(
	[ImmNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[LoginUsers]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[LoginUsers](
	[LoginId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserID] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WrkName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LoginTime] [datetime] NOT NULL,
	[LogoutTime] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_LoginUsers] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[LoginUsers_Audit]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[LoginUsers_Audit](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LoginId] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[ActionTaken] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EncounterId] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_LoginUsers_Audit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Lookups]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Lookups](
	[LookupID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ICDCPTExists] [bit] NULL,
	[ImagesExists] [bit] NULL,
 CONSTRAINT [PK_Lookups] PRIMARY KEY CLUSTERED 
(
	[LookupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[LookupValues]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[LookupValues](
	[LookupValueID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LookupID] [int] NOT NULL,
	[LookupValue] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [bit] NULL,
	[Code1] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Code2] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ISDefault] [int] NULL,
 CONSTRAINT [PK_LookupValues] PRIMARY KEY CLUSTERED 
(
	[LookupValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[MedicaidLocationCodes]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[MedicaidLocationCodes](
	[MedId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DoctorId] [int] NULL,
	[LocationId] [int] NULL,
	[LocationCode] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_MedicaidLocationCodes] PRIMARY KEY CLUSTERED 
(
	[MedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Messaging]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Messaging](
	[ID] [uniqueidentifier] NOT NULL,
	[DteTme] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MsgStatus] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Header_MessageId] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Header_RelatesToMessageID] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Header_SentTime] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Response_Note] [varchar](210) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy_NCPDPID] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_SPI] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_PrescriberAgent_LastName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_PrescriberAgent_FirstName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_PrescriberAgent_MiddleName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_PrescriberAgent_Suffix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Prescriber_PrescriberAgent_Prefix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_SPI] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_PrescriberAgent_LastName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_PrescriberAgent_FirstName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_PrescriberAgent_MiddleName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_PrescriberAgent_Suffix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Supervisor_PrescriberAgent_Prefix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_MutuallyDefined] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_LastName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_FirstName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_MiddleName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Suffix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Prefix] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Gender] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_DateOfBirth] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_AddressLine1] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_AddressLine2] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_City] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_State] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_ZipCode] [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Email] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Number] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Patient_Qualifier] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_DrugDescription] [varchar](105) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_ProductCode] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_DosageForm] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Strength] [varchar](70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_StrengthUnits] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_DrugDBCode] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_DrugDBCodeQualifier] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Quantity_Qualifier] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Quantity_Value] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_DaysSupply] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Directions] [varchar](140) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Note] [varchar](210) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Refills_Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Refills_Quantity] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_Substitutions] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_WrittenDate] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_LastFillDate] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicationPrescribed_PriorAuthorization_Value] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Messaging] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Messaging2]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Messaging2](
	[ID] [uniqueidentifier] NOT NULL,
	[MessagingID] [uniqueidentifier] NULL,
	[MSGPartNum] [tinyint] NULL,
	[MSG] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Messaging2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[ModifierRules]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[ModifierRules](
	[RefId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BusinessClassId] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CPT] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule1] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description1] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule2] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description2] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule3] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description3] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule4] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description4] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule5] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description5] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule6] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description6] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule7] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description7] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule8] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description8] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule9] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description9] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule10] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description10] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule11] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description11] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule12] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description12] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule13] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description13] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule14] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description14] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rule15] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description15] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_ModifierRules] PRIMARY KEY CLUSTERED 
(
	[RefId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Patient_EPrescription_RenewalRequest]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Patient_EPrescription_RenewalRequest](
	[renewalRequestId] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PatientID] [int] NULL,
	[DrugName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PharmacyName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Location] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DoctorName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PatientName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AcceptDenyStatus] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ProcessStatus] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransmissionStatus] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[Status] [bit] NOT NULL,
	[Comments] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RenewalRequestDate] [datetime] NULL,
	[ResponseCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResponseCodeDetail] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResponseFreeText] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResponseTime] [datetime] NULL,
	[SentStatus] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SentTime] [datetime] NULL,
	[SentStatusMessage] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrescriptionGuid] [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RenewalReqXML] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Patient_EPrescription_RenewalRequest] PRIMARY KEY CLUSTERED 
(
	[renewalRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Patient_Images]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Patient_Images](
	[ImageID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientID] [int] NOT NULL,
	[ProviderID] [int] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[FolderPath] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FileName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EncounterID] [int] NULL,
	[ScanDate] [datetime] NOT NULL,
	[UserID] [int] NULL,
	[Comments] [varchar](6500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageDelete_YN] [bit] NULL,
	[LocationId] [int] NULL,
	[Abnormal] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Timestamp] [datetime] NULL,
	[Priority] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsChecked] [bit] NULL,
 CONSTRAINT [PK_Patient_Images] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Patient_Immunizations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Patient_Immunizations](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientID] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EncounterID] [int] NOT NULL,
	[ImmID] [int] NOT NULL,
	[DateOrdered] [datetime] NULL,
	[DateDue] [datetime] NULL,
	[DateGiven] [datetime] NULL,
	[Comments] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LotExpDate] [datetime] NULL,
	[ManufactDate] [datetime] NULL,
	[Site] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Reaction] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Initials] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ParentSign] [image] NULL,
	[Lotid] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ManuName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Dosage] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VisDate] [datetime] NULL,
	[NurseSign] [image] NULL,
	[ICDCodes] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CPTCodes] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserID] [int] NULL,
	[ImmTimeStamp] [datetime] NULL,
	[ProviderInitials] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Dos_units] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImmName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vis] [bit] NULL,
	[Manufacturer_Code] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[Patient_MedicationsXMl]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[Patient_MedicationsXMl](
	[PatientID] [int] NULL,
	[GeneratedXml] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientClinical]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientClinical](
	[ClinicalId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentId] [int] NULL,
	[PatientId] [int] NULL,
	[ClinicalType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EyeContext] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Symptom] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FindingDetail] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageDescriptor] [nvarchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageInstructions] [nvarchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DrawFileName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Highlights] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FollowUp] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PermanentCondition] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostOpPeriod] [int] NULL,
	[Surgery] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Activity] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientClinical] PRIMARY KEY CLUSTERED 
(
	[ClinicalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientClinicalSurgeryPlan]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientClinicalSurgeryPlan](
	[SurgeryId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SurgeryClnId] [int] NULL,
	[SurgeryRefType] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SurgeryValue] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SurgeryStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientClinicalSurgeryPlan] PRIMARY KEY CLUSTERED 
(
	[SurgeryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientDemoAlt]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientDemoAlt](
	[PatientId] [int] NOT NULL,
	[AltId] [int] NOT NULL,
	[Selected] [int] NOT NULL,
	[Address] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Suite] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HomePhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WorkPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CellPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pharmacy] [int] NULL,
	[PhoneType1] [int] NULL,
	[PhoneType2] [int] NULL,
	[PhoneType3] [int] NULL,
 CONSTRAINT [PK_PatientDemoAlt] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC,
	[AltId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientDemographics]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientDemographics](
	[PatientId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LastName] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstName] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleInitial] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NameReference] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SocialSecurity] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Suite] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HomePhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CellPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmergencyName] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmergencyPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmergencyRel] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Occupation] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessName] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessAddress] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessSuite] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessCity] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessZip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Gender] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Marital] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BirthDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NationalOrigin] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Language] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SchedulePrimaryDoctor] [int] NULL,
	[PrimaryCarePhysician] [int] NULL,
	[ReferringPhysician] [int] NULL,
	[ProfileComment1] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ProfileComment2] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PolicyPatientId] [int] NULL,
	[Relationship] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BillParty] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SecondPolicyPatientId] [int] NULL,
	[SecondRelationship] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SecondBillParty] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferralCatagory] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BulkMailing] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialAssignment] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialSignature] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialSignatureDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Salutation] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Hipaa] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PatType] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BillingOffice] [int] NULL,
	[SendStatements] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SendConsults] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OldPatient] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferralRequired] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicareSecondary] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostOpExpireDate] [int] NULL,
	[PostOpService] [int] NULL,
	[EmployerPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Religion] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Race] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Ethnicity] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientDemographics] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientFinancial]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientFinancial](
	[FinancialId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[FinancialInsurerId] [int] NULL,
	[FinancialPerson] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialGroupId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialCopay] [real] NULL,
	[FinancialPIndicator] [int] NULL,
	[FinancialStartDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialEndDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinancialInsType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientFinancial] PRIMARY KEY CLUSTERED 
(
	[FinancialId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientFinancialDependents]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientFinancialDependents](
	[DependentId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[PolicyHolderId] [int] NULL,
	[InsurerId] [int] NULL,
	[MemberId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubscriberId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientFinancialDependents] PRIMARY KEY CLUSTERED 
(
	[DependentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientNotes]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientNotes](
	[NoteId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[AppointmentId] [int] NULL,
	[NoteType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note1] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note2] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note3] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note4] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserName] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteSystem] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteEye] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteCommentOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteCategory] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteOffDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteHighlight] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteILPNRef] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteAlertType] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteAudioOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteClaimOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientNotes] PRIMARY KEY CLUSTERED 
(
	[NoteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientPreCerts]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientPreCerts](
	[PreCertId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[PreCert] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PreCertInsId] [int] NULL,
	[PreCertDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PreCertApptId] [int] NULL,
	[PreCertComment] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PreCertLocationId] [int] NULL,
	[PreCertStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientPreCerts] PRIMARY KEY CLUSTERED 
(
	[PreCertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientReceivablePayments]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientReceivablePayments](
	[PaymentId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ReceivableId] [int] NULL,
	[PayerId] [int] NULL,
	[PayerType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentAmount] [real] NULL,
	[PaymentDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentCheck] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentRefId] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentRefType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentService] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentCommentOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentFinancialType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentServiceItem] [int] NULL,
	[PaymentAssignBy] [int] NULL,
	[PaymentReason] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentBatchCheckId] [int] NULL,
	[PaymentAppealNumber] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PaymentEOBDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientReceivablePayments] PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientReceivables]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientReceivables](
	[ReceivableId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[OpenBalance] [real] NULL,
	[ReceivableType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PatientId] [int] NULL,
	[InsuredId] [int] NULL,
	[InsurerId] [int] NULL,
	[AppointmentId] [int] NULL,
	[Invoice] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InvoiceDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Charge] [real] NULL,
	[BillToDr] [int] NULL,
	[AccType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AccState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HspAdmDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HspDisDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Disability] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RsnType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RsnDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstConsDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrevCond] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferDr] [int] NULL,
	[UnallocatedBalance] [real] NULL,
	[ExternalRefInfo] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Over90] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BillingOffice] [int] NULL,
	[PatientBillDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastPayDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ChargesByFee] [real] NULL,
	[InsurerDesignation] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WriteOff] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientReceivables] PRIMARY KEY CLUSTERED 
(
	[ReceivableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientReceivableServices]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientReceivableServices](
	[ItemId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Invoice] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Service] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Modifier] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Quantity] [real] NULL,
	[Charge] [real] NULL,
	[TypeOfService] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PlaceOfService] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ServiceDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LinkedDiag] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrderDoc] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConsultOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FeeCharge] [real] NULL,
	[ItemOrder] [int] NULL,
	[ECode] [int] NULL,
	[PostDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExternalRefInfo] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientReceivableServices] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PatientReferral]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PatientReferral](
	[ReferralId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[Referral] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferredFrom] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferredFromId] [int] NULL,
	[ReferralDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferralExpireDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferralVisits] [int] NULL,
	[ReferralVisitsLeft] [int] NULL,
	[Reason] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferredTo] [int] NULL,
	[ReferredInsurer] [int] NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PatientReferral] PRIMARY KEY CLUSTERED 
(
	[ReferralId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PayerMapping]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PayerMapping](
	[ID] [uniqueidentifier] NOT NULL,
	[PayerId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ClrHouseId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PayerMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PCInventory]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PCInventory](
	[InventoryId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SKU] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type_] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Manufacturer] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Model] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Color] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PCSize] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LensCoating] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LensTint] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PDD] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PDN] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CEGHeight] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Qty] [int] NULL,
	[Cost] [real] NULL,
	[WCost] [real] NULL,
 CONSTRAINT [PK_PCInventory] PRIMARY KEY CLUSTERED 
(
	[InventoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Pharmacies]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Pharmacies](
	[NCPDPID] [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StoreNumber] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferenceNumberAlt1] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferenceNumberAlt1Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddressLine1] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AddressLine2] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Zip] [varchar](11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhonePrimary] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Fax] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt1] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt1Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt2] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt2Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt3] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt3Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt4] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt4Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt5] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneAlt5Qualifier] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ActiveStartTime] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ActiveEndTime] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ServiceLevel] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PartnerAccount] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [varchar](22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TwentyFourHourFlag] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CrossStreet] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RecordChange] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OldServiceLevel] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TextServiceLevel] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TextServiceLevelChange] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Version] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NPI] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Pharmacy] PRIMARY KEY CLUSTERED 
(
	[NCPDPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[PhysicianSpi]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PhysicianSpi](
	[PhysicianSpiId] [int] IDENTITY(1,1) NOT NULL,
	[PhysicianId] [int] NOT NULL,
	[LocationId] [int] NULL,
	[Spi] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneType] [int] NULL,
 CONSTRAINT [PK_PhysicianSpi] PRIMARY KEY CLUSTERED 
(
	[PhysicianSpiId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeActivity]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeActivity](
	[ActivityId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NULL,
	[StationId] [int] NULL,
	[ActivityDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AppointmentId] [int] NULL,
	[QuestionSet] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ActivityStatusTime] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceId] [int] NULL,
	[TechConfirmed] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrentRId] [int] NULL,
	[LocationId] [int] NULL,
	[PayAmount] [real] NULL,
	[PayMethod] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayReference] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ActivityStatusTRef] [real] NULL,
 CONSTRAINT [PK_PracticeActivity] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeAffiliations]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeAffiliations](
	[AffiliationId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ResourceId] [int] NULL,
	[ResourceType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PlanId] [int] NULL,
	[Pin] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Override] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LocationId] [int] NULL,
	[AlternatePin] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrgOverride] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeAffiliations] PRIMARY KEY CLUSTERED 
(
	[AffiliationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeAudit]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeAudit](
	[AuditId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Date] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Time] [int] NULL,
	[UserId] [int] NULL,
	[TableId] [smallint] NULL,
	[RecordId] [int] NULL,
	[PrevRec] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrentRec] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Action] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeAudit] PRIMARY KEY CLUSTERED 
(
	[AuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeCalendar]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeCalendar](
	[CalendarId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CalendarResourceId] [int] NULL,
	[CalendarDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CalendarPurpose] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CalendarColor] [int] NULL,
	[CalendarStartTime] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CalendarEndTime] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CalendarLoc1] [int] NULL,
	[CalendarLoc2] [int] NULL,
	[CalendarLoc3] [int] NULL,
	[CalendarLoc4] [int] NULL,
	[CalendarLoc5] [int] NULL,
	[CalendarLoc6] [int] NULL,
	[CalendarLoc7] [int] NULL,
	[CatArray1] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray2] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray3] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray4] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray5] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray6] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray7] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray8] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CalendarComment] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeCalendar] PRIMARY KEY CLUSTERED 
(
	[CalendarId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeCodeTable]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeCodeTable](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ReferenceType] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Code] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AlternateCode] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Exclusion] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LetterTranslation] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OtherLtrTrans] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FormatMask] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Signature] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TestOrder] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FollowUp] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_PracticeCodeTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeFavorites]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeFavorites](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[System] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Diagnosis] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Rank] [int] NULL,
	[Alternate] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeFavorites] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeFeeSchedules]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeFeeSchedules](
	[ServiceId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CPT] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DoctorId] [int] NULL,
	[PlanId] [int] NULL,
	[Fee] [real] NULL,
	[StartDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AdjustmentRate] [real] NULL,
	[AdjustmentAllowed] [real] NULL,
	[VRU] [real] NULL,
	[LocId] [int] NULL,
	[FacilityFee] [real] NULL,
 CONSTRAINT [PK_PracticeFeeSchedules] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeInsurers]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeInsurers](
	[InsurerId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[InsurerName] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerAddress] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerCity] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerZip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPlanId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPlanName] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPlanType] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerGroupId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerGroupName] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerReferenceCode] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReferralRequired] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Copay] [real] NULL,
	[OutOfPocket] [real] NULL,
	[Availability] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerFeeSchedule] [nvarchar](53) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerTosTbl] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPosTbl] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NEICNumber] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerENumber] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerEFormat] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerTFormat] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPrecPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerEligPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerProvPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerClaimPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerCrossOver] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPayType] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerAdjustmentDefault] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrecertRequired] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MedicareCrossOverId] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerComment] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerPlanFormat] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerClaimMap] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerBusinessClass] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OCode] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerCPTClass] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InsurerServiceFilter] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateJurisdiction] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeInsurers] PRIMARY KEY CLUSTERED 
(
	[InsurerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeInterfaceConfiguration]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeInterfaceConfiguration](
	[InterfaceId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Interface] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FieldReference] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FieldValue] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeInterfaceConfiguration] PRIMARY KEY CLUSTERED 
(
	[InterfaceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeMessages]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeMessages](
	[MessageId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SenderResourceId] [int] NULL,
	[ReceiverResourceId] [int] NULL,
	[MessageDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MessageTime] [int] NULL,
	[Message] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeMessages] PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeName]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeName](
	[PracticeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PracticeType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeAddress] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeSuite] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeCity] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeZip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticePhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeOtherPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeFax] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeEmail] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeTaxId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeOtherId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime1] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime1] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime2] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime2] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime3] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime3] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime4] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime4] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime5] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime5] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime6] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime6] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime7] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime7] [nvarchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Vacation] [nvarchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LoginType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LocationReference] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BillingOffice] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray1] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray2] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray3] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray4] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray5] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray6] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray7] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CatArray8] [nvarchar](192) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeNPI] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PracticeTaxonomy] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeName] PRIMARY KEY CLUSTERED 
(
	[PracticeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticePayables]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticePayables](
	[PayableId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PayableInvoice] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayableType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayablePayerType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayableDescription] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayableAmount] [real] NULL,
	[PayableDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayableVendorId] [int] NULL,
	[APAcct] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PayablePaidAmount] [real] NULL,
	[PayablePaidDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticePayables] PRIMARY KEY CLUSTERED 
(
	[PayableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeReports]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeReports](
	[ReportId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ReportName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReportOrder] [int] NULL,
	[QueryName] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CriteriaMap] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EntryScreenMap] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateName] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AllowDate] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DocBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LocBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PatBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DocLocBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DocPatBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LocPatBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DocLocPatBreak] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Active] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Daily] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CriteriaMapDefaults] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AdminOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeReports] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeServices]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeServices](
	[CodeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CodeType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CodeCategory] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Fee] [real] NULL,
	[TOS] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[POS] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RelativeValueUnit] [real] NULL,
	[OrderDoc] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConsultOn] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ClaimNote] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ServiceFilter] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NDC] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeServices] PRIMARY KEY CLUSTERED 
(
	[CodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeTransactionJournal]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeTransactionJournal](
	[TransactionId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TransactionType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionTypeId] [int] NULL,
	[TransactionStatus] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionDate] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionTime] [int] NULL,
	[TransactionRef] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionRemark] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionAction] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionBatch] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransactionServiceItem] [int] NULL,
	[TransactionRcvrId] [int] NULL,
	[TransactionAssignment] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeTransactionJournal] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[PracticeVendors]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[PracticeVendors](
	[VendorId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VendorName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorAddress] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorSuite] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorCity] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorZip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorPhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorFax] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorEmail] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorTaxId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorUPIN] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorSpecialty] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorOtherOffice1] [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorOtherOffice2] [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorLastName] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorFirstName] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorMI] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorTitle] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorSalutation] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorFirmName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorTaxonomy] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorExcRefDr] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorNPI] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorCellPhone] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VendorSpecOther] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PracticeVendors] PRIMARY KEY CLUSTERED 
(
	[VendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[REPORTFILTERS]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[REPORTFILTERS](
	[RepFilterID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RepSelection] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RepFilterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Resources]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[Resources](
	[ResourceId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ResourceName] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceType] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceDescription] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourcePid] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourcePermissions] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceColor] [int] NULL,
	[ResourceOverLoad] [int] NULL,
	[ResourceCost] [real] NULL,
	[ResourceSpecialty] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceTaxId] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceAddress] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceSuite] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceCity] [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceState] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceZip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourcePhone] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceEmail] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ServiceCode] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceSSN] [nvarchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceDEA] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceLicence] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceUPIN] [nvarchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceScheduleTemplate1] [int] NULL,
	[ResourceScheduleTemplate2] [int] NULL,
	[ResourceScheduleTemplate3] [int] NULL,
	[PracticeId] [int] NULL,
	[StartTime1] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime1] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime2] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime2] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime3] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime3] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime4] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime4] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime5] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime5] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime6] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime6] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartTime7] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EndTime7] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Vacation] [nvarchar](120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceLastName] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceFirstName] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SignatureFile] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PlaceOfService] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Billable] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GoDirect] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NPI] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceMI] [nvarchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourceSuffix] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResourcePrefix] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsLoggedIn] [bit] NOT NULL,
 CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[ServiceTransactions]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[ServiceTransactions](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TransactionId] [int] NULL,
	[ServiceId] [int] NULL,
	[TransactionDate] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Amount] [smallmoney] NULL,
	[TransportAction] [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModTime] [datetime] NULL,
 CONSTRAINT [PK_ServiceTransactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[tblGrowthChartDetail]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[tblGrowthChartDetail](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GraphID] [int] NOT NULL,
	[GrowthChartID] [int] NOT NULL,
	[XAxis] [numeric](18, 2) NULL,
	[Y1Axis] [numeric](18, 2) NULL,
	[Y2Axis] [numeric](18, 2) NULL,
	[HCBMI] [numeric](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[tblGrowthChartMaster]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[tblGrowthChartMaster](
	[GrowthChartID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ChartID] [int] NULL,
	[ChartName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Deviation] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[GrowthChartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[tblMsg]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[tblMsg](
	[MsgId] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MsgType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Msg] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ProcessStatus] [int] NOT NULL,
	[ProcessStatusMsg] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblMsg] PRIMARY KEY CLUSTERED 
(
	[MsgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[UpdateConfiguration]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[UpdateConfiguration](
	[Id] [int] NOT NULL,
	[DatabaseType] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DatabaseLocation] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[QueryAction] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[QueryTable] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[QueryClause] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_UpdateConfiguration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[User_Patients_Restrict]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[User_Patients_Restrict](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PatientId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[NoOfRuns] [int] NOT NULL,
	[RunsCompleted] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TimeStamp] [datetime] NOT NULL,
	[IsVisited] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
/****** Object:  Table [dbo].[Valuesets]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
SET ANSI_PADDING ON')
EXEC('
CREATE TABLE [dbo].[Valuesets](
	[ValuesetId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ValueSetCode] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ValueSetName] [varchar](800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ValueSetType] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CodeSystemOID] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CodeSystemName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Valuesets] PRIMARY KEY CLUSTERED 
(
	[ValuesetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
')
EXEC('
SET ANSI_PADDING OFF')
EXEC('
/****** Object:  Table [dbo].[ZIPCodes]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE TABLE [dbo].[ZIPCodes](
	[ZIPCode] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CityName] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateAbbr] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
')
EXEC('
/****** Object:  View [dbo].[AllergyView]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE VIEW [dbo].[AllergyView]
AS
SELECT     CompositeAllergyID, Source, ConceptID, ConceptType, Description, Status, TouchDate
FROM         fdb.tblCompositeAllergy

')
EXEC('
/****** Object:  View [dbo].[collectionprocs]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[collectionprocs]
AS
SELECT DISTINCT 
	PatientReceivableServices.Quantity, 
	PatientReceivableServices.Invoice, 
	PatientReceivableServices.Status, 
	Resources.ResourceDescription, 
	PatientReceivables.InvoiceDate, 
	PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, 
	Appointments.ResourceId2, 
	PatientReceivables.BillingOffice, 
	Resources_1.ResourceDescription AS DocName,
	PatientReceivableServices.ItemId
FROM (PatientReceivableServices 
INNER JOIN (PatientReceivables 
INNER JOIN (Appointments 
INNER JOIN Resources 
ON Appointments.ResourceId1 = Resources.ResourceId) 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
ON PatientReceivableServices.Invoice = PatientReceivables.Invoice) 
LEFT JOIN Resources AS Resources_1 
ON PatientReceivables.BillToDr = Resources_1.ResourceId
WHERE (((PatientReceivableServices.Status)<>''X''))
')
EXEC('
/****** Object:  View [dbo].[collectionprocsPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE  VIEW [dbo].[collectionprocsPostDate]
AS
SELECT DISTINCT 
	PatientReceivableServices.Quantity, 
	PatientReceivableServices.Invoice , 
	PatientReceivableServices.Status, 
	Resources.ResourceDescription, 
	PatientReceivableServices.PostDate  as InvoiceDate, 
	PatientReceivableServices.Service, 
	PatientReceivableServices.Modifier, 
	Appointments.ResourceId2, 
	PatientReceivables.BillingOffice, 
	Resources_1.ResourceDescription AS DocName,
	PatientReceivableServices.ItemId
FROM (PatientReceivableServices 
INNER JOIN (PatientReceivables 
INNER JOIN (Appointments 
INNER JOIN Resources 
ON Appointments.ResourceId1 = Resources.ResourceId) 
ON PatientReceivables.AppointmentId = Appointments.AppointmentId) 
ON PatientReceivableServices.Invoice = PatientReceivables.Invoice) 
LEFT JOIN Resources AS Resources_1 
ON PatientReceivables.BillToDr = Resources_1.ResourceId
WHERE (((PatientReceivableServices.Status)<>''X''))


')
EXEC('
/****** Object:  View [dbo].[collections]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[collections]
AS 
SELECT
    Appointments.ResourceId2 AS Location, 
    ''Invoice'' AS Type, 
    Left(PatientReceivables.InvoiceDate,6) AS YYYYMM, 
    PatientReceivables.Charge AS Amount, 
    PatientReceivables.ChargesByFee,
    '''' AS PaymentType, 
    '''' AS PayerType, 
    '''' AS FinType,
    Resources.ResourceId,
    Resources.ResourceDescription AS Doctor,
    PatientDemographics.LastName, 
    PatientDemographics.FirstName, 
    PatientReceivables.BillingOffice,
    Appointments.PatientId, 
    PatientReceivables.ReceivableId, 
    PracticeTransactionJournal.TransactionRef AS TransactionRef, 
    PracticeTransactionJournal.TransactionRemark AS TransactionRemark, 
    PracticeTransactionJournal.TransactionStatus AS TransactionStatus, 
    PatientReceivables.Invoice, 
    PracticeTransactionJournal.TransactionDate AS TransactionDate, 
    PatientReceivables.InvoiceDate AS YYYYMMDD, 
    Resources_2.ResourceDescription AS BillToDr,
    PatientReceivables.BillToDr as BillToDrId
FROM (PatientDemographics
    INNER JOIN ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON  Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN Resources AS Resources_2 ON PatientReceivables.BillToDr = Resources_2.ResourceId)
    LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
    ON  PatientDemographics.PatientId = PatientReceivables.PatientId)
UNION ALL SELECT
    Appointments.ResourceId2 AS Location, 
    ''Payment'' AS Type, 
    LEFT(PatientReceivablePayments.PaymentDate, 6)  AS YYYYMM, 
    PatientReceivablePayments.PaymentAmount AS Amount, 
    PatientReceivables.ChargesByFee,
    PatientReceivablePayments.PaymentType AS PaymentType, 
    PatientReceivablePayments.PayerType AS PayerType, 
    PatientReceivablePayments.PaymentFinancialType As FinType,
    Resources.ResourceId,
    Resources.ResourceDescription AS Doctor, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    PatientReceivables.BillingOffice,
    Appointments.PatientId, 
    PatientReceivables.ReceivableId, '''' AS TransactionRef, 
    '''' AS TransactionRemark, '''' AS TransactionStatus, 
    PatientReceivables.Invoice, '''' AS TransactionDate, 
    PatientReceivablePayments.PaymentDate AS YYYYMMDD,
    Resources_2.ResourceDescription AS BillToDr,
    PatientReceivables.BillToDr as BillToDrId
FROM PatientDemographics
    INNER JOIN  ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN PatientReceivablePayments ON  PatientReceivables.ReceivableId = PatientReceivablePayments.ReceivableId)
    INNER JOIN Resources ON  Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN Resources AS Resources_2 ON  PatientReceivables.BillToDr = Resources_2.ResourceId)
    ON  PatientDemographics.PatientId = Appointments.PatientId
WHERE ((PatientReceivablePayments.PaymentFinancialType) = ''C'') OR 
      ((PatientReceivablePayments.PaymentFinancialType) = ''D'') AND
      ((PatientReceivablePayments.Status) <> ''X'')
')
EXEC('
/****** Object:  View [dbo].[collectionsPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('


CREATE        VIEW [dbo].[collectionsPostDate]
AS 
SELECT
    Appointments.ResourceId2 AS Location, 
    ''Invoice'' AS Type, 
    Left(PatientReceivableServices.PostDate,6) AS YYYYMM, 
    PatientReceivableServices.Charge * PatientReceivableServices.Quantity AS Amount, 
    PatientReceivableServices.Service,
    PatientReceivableServices.ItemID as ItemID,
    PatientReceivables.ChargesByFee,
    '''' AS PaymentType, 
    '''' AS PayerType, 
    '''' AS FinType,
    Resources.ResourceId,
    Resources.ResourceDescription AS Doctor,
    PatientDemographics.LastName, 
    PatientDemographics.FirstName, 
    PatientReceivables.BillingOffice,
    Appointments.PatientId, 
    PatientReceivables.ReceivableId, 
    PracticeTransactionJournal.TransactionRef AS TransactionRef, 
    PracticeTransactionJournal.TransactionRemark AS TransactionRemark, 
    PracticeTransactionJournal.TransactionStatus AS TransactionStatus, 
    PatientReceivables.Invoice, 
    PracticeTransactionJournal.TransactionDate AS TransactionDate, 
    PatientReceivableServices.PostDate AS YYYYMMDD, 
    Resources_2.ResourceDescription AS BillToDr,
    PatientReceivables.BillToDr as BillToDrId
FROM (PatientDemographics
    INNER JOIN ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON  Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN Resources AS Resources_2 ON PatientReceivables.BillToDr = Resources_2.ResourceId)
    LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
    ON  PatientDemographics.PatientId = PatientReceivables.PatientId
   inner join  PatientReceivableServices ON PatientReceivableServices.Invoice = PatientReceivables.Invoice)
  where PatientReceivableServices.Status <> ''X''
UNION ALL SELECT
    Appointments.ResourceId2 AS Location, 
    ''Payment'' AS Type, 
    LEFT(PatientReceivablePayments.PaymentDate, 6)  AS YYYYMM, 
    PatientReceivablePayments.PaymentAmount AS Amount, 
    NULL as Service,
    PatientReceivablePayments.PaymentServiceItem as ItemID,
    PatientReceivables.ChargesByFee,
    PatientReceivablePayments.PaymentType AS PaymentType, 
    PatientReceivablePayments.PayerType AS PayerType, 
    PatientReceivablePayments.PaymentFinancialType As FinType,
    Resources.ResourceId,
    Resources.ResourceDescription AS Doctor, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    PatientReceivables.BillingOffice,
    Appointments.PatientId, 
    PatientReceivables.ReceivableId, '''' AS TransactionRef, 
    '''' AS TransactionRemark, '''' AS TransactionStatus, 
    PatientReceivables.Invoice, '''' AS TransactionDate, 
    PatientReceivablePayments.PaymentDate AS YYYYMMDD,
    Resources_2.ResourceDescription AS BillToDr,
    PatientReceivables.BillToDr as BillToDrId
FROM PatientDemographics
    INNER JOIN  ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN PatientReceivablePayments ON  PatientReceivables.ReceivableId = PatientReceivablePayments.ReceivableId)
    INNER JOIN Resources ON  Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN Resources AS Resources_2 ON  PatientReceivables.BillToDr = Resources_2.ResourceId)
    ON  PatientDemographics.PatientId = Appointments.PatientId
WHERE ((PatientReceivablePayments.PaymentFinancialType) = ''C'') OR 
      ((PatientReceivablePayments.PaymentFinancialType) = ''D'') AND
      ((PatientReceivablePayments.Status) <> ''X'')



')
EXEC('
/****** Object:  View [dbo].[FDBView]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE VIEW [dbo].[FDBView]
AS
SELECT     TOP 100 PERCENT MEDID AS MED_ID, MED_ROUTED_DF_MED_ID_DESC AS MED_DESC, 
                      MED_STRENGTH + '' '' + MED_STRENGTH_UOM AS MED_STRENGTH_UOM, MED_DOSAGE_FORM_DESC AS MED_DOSAGE, MED_REF_DEA_CD AS MED_REF
FROM         fdb.tblCompositeDrug
WHERE     (Status in (''P'', ''A''))
ORDER BY MED_ID

')
EXEC('
/****** Object:  View [dbo].[InsurerAging]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[InsurerAging]
AS
SELECT DISTINCT 
	dbo.PatientReceivables.PatientId, 
	dbo.PatientReceivables.InsurerId, 
	dbo.PatientReceivables.OpenBalance,
	dbo.PatientReceivables.UnallocatedBalance, 
	dbo.PatientReceivables.InsurerDesignation, 
	dbo.PatientReceivables.Invoice, 
	dbo.PatientReceivables.InvoiceDate, 
	dbo.PatientReceivables.Charge, 
	dbo.PatientReceivables.BillToDr, 
	dbo.PatientReceivables.BillingOffice, 
	dbo.PracticeTransactionJournal.TransactionRemark, 
	dbo.PatientReceivables.AppointmentId
FROM dbo.PatientReceivables
INNER JOIN dbo.PatientDemographics
ON dbo.PatientReceivables.PatientId = dbo.PatientDemographics.PatientId 
LEFT OUTER JOIN dbo.PracticeTransactionJournal 
ON dbo.PatientReceivables.ReceivableId = dbo.PracticeTransactionJournal.TransactionTypeId
WHERE (dbo.PracticeTransactionJournal.TransactionStatus <> ''Z'') AND 
	(dbo.PracticeTransactionJournal.TransactionType = ''R'') AND 
	(dbo.PracticeTransactionJournal.TransactionRef = ''I'')
')
EXEC('
/****** Object:  View [dbo].[InsurerAgingPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER OFF')
EXEC('



CREATE    VIEW [dbo].[InsurerAgingPostDate]
AS
SELECT DISTINCT 
	dbo.PatientReceivables.PatientId, 
	dbo.PatientReceivables.InsurerId, 
	dbo.PatientReceivables.OpenBalance,
	dbo.PatientReceivables.UnallocatedBalance, 
	dbo.PatientReceivables.InsurerDesignation, 
	dbo.PatientReceivables.Invoice, 
	dbo.PatientReceivableservices.service,
	dbo.PatientReceivableservices.PostDate as InvoiceDate, 
	dbo.PatientReceivables.Charge, 
	dbo.PatientReceivables.BillToDr, 
	dbo.PatientReceivables.BillingOffice, 
	dbo.PracticeTransactionJournal.TransactionRemark, 
	dbo.PatientReceivables.AppointmentId
FROM dbo.PatientReceivables
INNER JOIN dbo.PatientDemographics
ON dbo.PatientReceivables.PatientId = dbo.PatientDemographics.PatientId 
inner  JOIN dbo.PracticeTransactionJournal 
ON dbo.PatientReceivables.ReceivableId = dbo.PracticeTransactionJournal.TransactionTypeId
--Left outer join dbo.PatientReceivablePayments on dbo.PatientReceivables.receivableid=dbo.PatientReceivablePayments.ReceivableId
Left outer join PatientReceivableservices on PatientReceivableservices.invoice=dbo.PatientReceivables.invoice
WHERE (dbo.PracticeTransactionJournal.TransactionStatus <> ''Z'') AND 
	(dbo.PracticeTransactionJournal.TransactionType = ''R'') AND 
	(dbo.PracticeTransactionJournal.TransactionRef = ''I'')

')
EXEC('
/****** Object:  View [dbo].[PatAging]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[PatAging]
AS 
SELECT PatientDemographics.PatientId, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    Appointments.ResourceId2,
    ''Charge'' AS Type, 
    PatientReceivables.InvoiceDate, 
    PatientReceivables.PatientBillDate, 
    PatientReceivables.LastPayDate,
    PatientReceivables.ReceivableId,
    PatientReceivables.Invoice, 
    PatientReceivables.Charge AS Amount,
    PatientReceivables.ChargesByFee,
    '''' AS PaymentType, '''' AS PayerType, '''' AS FinType,
    Resources.ResourceDescription AS Doctor,
    PracticeTransactionJournal.TransactionStatus AS TransStat, 
    PracticeTransactionJournal.TransactionRef AS TransRef, 
    PracticeTransactionJournal.TransactionRemark AS TransRemark,
    Resources_1.ResourceDescription AS BillToDr
FROM ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
    INNER JOIN PatientDemographics ON Appointments.PatientId = PatientDemographics.PatientId)
    LEFT JOIN Resources AS Resources_1 ON PatientReceivables.BillToDr = Resources_1.ResourceId
UNION ALL SELECT PatientDemographics.PatientId, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    Appointments.ResourceId2,
    ''Payment'' AS Type, 
    PatientReceivables.InvoiceDate, 
    PatientReceivables.PatientBillDate,
    PatientReceivables.LastPayDate, 
    PatientReceivables.ReceivableId, PatientReceivables.Invoice, 
    PatientReceivablePayments.PaymentAmount AS Amount, 
    PatientReceivables.ChargesByFee,
    PatientReceivablePayments.PaymentType AS PaymentType, 
    PatientReceivablePayments.PayerType AS PayerType, 
    PatientReceivablePayments.PaymentFinancialType AS FinType,
    Resources.ResourceDescription AS Doctor,
    '''' AS TransStat, '''' AS TransRef, '''' AS TransRemark, 
    Resources_1.ResourceDescription AS BillToDr
FROM ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN PatientReceivablePayments ON PatientReceivables.ReceivableId = PatientReceivablePayments.ReceivableId)
    INNER JOIN PatientDemographics ON Appointments.PatientId = PatientDemographics.PatientId)
    LEFT JOIN Resources AS Resources_1 ON PatientReceivables.BillToDr = Resources_1.ResourceId
WHERE (((PatientReceivablePayments.Status) = ''A'') And 
((PatientReceivablePayments.PaymentFinancialType = ''C'') OR 
(PatientReceivablePayments.PaymentFinancialType = ''D'')))
')
EXEC('
/****** Object:  View [dbo].[PatAgingPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('

CREATE   VIEW [dbo].[PatAgingPostDate]
AS 
SELECT PatientDemographics.PatientId, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    Appointments.ResourceId2,
    ''Charge'' AS Type, 
    PatientReceivableServices.PostDate as InvoiceDate, 
    PatientReceivableServices.ItemID,
    PatientReceivables.PatientBillDate, 
    PatientReceivables.LastPayDate,
    PatientReceivables.ReceivableId,
    PatientReceivables.Invoice, 
    PatientReceivables.Charge AS Amount,
    PatientReceivables.ChargesByFee,
    '''' AS PaymentType, '''' AS PayerType, '''' AS FinType,
    Resources.ResourceDescription AS Doctor,
    PracticeTransactionJournal.TransactionStatus AS TransStat, 
    PracticeTransactionJournal.TransactionRef AS TransRef, 
    PracticeTransactionJournal.TransactionRemark AS TransRemark,
    Resources_1.ResourceDescription AS BillToDr
FROM ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
    INNER JOIN PatientDemographics ON Appointments.PatientId = PatientDemographics.PatientId)
    LEFT JOIN Resources AS Resources_1 ON PatientReceivables.BillToDr = Resources_1.ResourceId
   inner join  PatientReceivableServices ON PatientReceivableServices.Invoice = PatientReceivables.Invoice
UNION ALL SELECT PatientDemographics.PatientId, 
    PatientDemographics.LastName, 
    PatientDemographics.FirstName,
    Appointments.ResourceId2,
    ''Payment'' AS Type, 
    PatientReceivableServices.PostDate as InvoiceDate, 
    PatientReceivableServices.ItemID,
    PatientReceivables.PatientBillDate,
    PatientReceivables.LastPayDate, 
    PatientReceivables.ReceivableId, PatientReceivables.Invoice, 
    PatientReceivablePayments.PaymentAmount AS Amount, 
    PatientReceivables.ChargesByFee,
    PatientReceivablePayments.PaymentType AS PaymentType, 
    PatientReceivablePayments.PayerType AS PayerType, 
    PatientReceivablePayments.PaymentFinancialType AS FinType,
    Resources.ResourceDescription AS Doctor,
    '''' AS TransStat, '''' AS TransRef, '''' AS TransRemark, 
    Resources_1.ResourceDescription AS BillToDr
FROM ((((Appointments
    INNER JOIN PatientReceivables ON Appointments.AppointmentId = PatientReceivables.AppointmentId)
    INNER JOIN Resources ON Appointments.ResourceId1 = Resources.ResourceId)
    LEFT JOIN PatientReceivablePayments ON PatientReceivables.ReceivableId = PatientReceivablePayments.ReceivableId)
    INNER JOIN PatientDemographics ON Appointments.PatientId = PatientDemographics.PatientId)
    LEFT JOIN Resources AS Resources_1 ON PatientReceivables.BillToDr = Resources_1.ResourceId
   inner join  PatientReceivableServices ON PatientReceivableServices.Invoice = PatientReceivables.Invoice
WHERE (((PatientReceivablePayments.Status) = ''A'') And 
((PatientReceivablePayments.PaymentFinancialType = ''C'') OR 
(PatientReceivablePayments.PaymentFinancialType = ''D'')))




')
EXEC('
/****** Object:  View [dbo].[production]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[production]
AS 
SELECT DISTINCT
	PatientReceivables.InvoiceDate, 
	PatientDemographics.LastName,
	PatientDemographics.FirstName,
	PatientReceivableServices.Service, 
	PatientReceivableServices.Charge, 
	PatientReceivableServices.Modifier, 
	PatientReceivableServices.Status, 
	PatientReceivables.Invoice, 
	PracticeVendors.VendorName, 
	PracticeServices.Description, 
	Appointments.AppointmentId, 
	Appointments.ResourceId1 AS DoctorSch, 
	Appointments.ResourceId2 AS LocationSch, 
	PatientReceivables.BillToDr AS DoctorRec, 
	PatientReceivables.ReceivableId, 
	PatientReceivables.InsurerId, 
	PatientReceivables.InsurerDesignation,
	PatientReceivableServices.Quantity, 
	PatientReceivableServices.ItemId AS RecServID, 
	PatientReceivables.BillingOffice, 
	PatientDemographics.PatientId
FROM PracticeServices 
RIGHT JOIN ((Appointments 
RIGHT JOIN ((PatientReceivables 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
INNER JOIN PatientDemographics 
ON PatientReceivables.PatientId = PatientDemographics.PatientId) 
ON Appointments.AppointmentId = PatientReceivables.AppointmentId) 
LEFT JOIN PracticeVendors 
ON PatientDemographics.ReferringPhysician = PracticeVendors.VendorId) 
ON PracticeServices.Code = PatientReceivableServices.Service
WHERE (((PatientReceivableServices.Status)<>''X'')) and
((PatientReceivables.InsurerDesignation <> ''F'') Or (PatientReceivables.InsurerId < 1))
')
EXEC('
/****** Object:  View [dbo].[productionPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('



CREATE  VIEW [dbo].[productionPostDate]
AS 
SELECT DISTINCT
	PatientReceivableServices.PostDate as InvoiceDate, 
	PatientDemographics.LastName,
	PatientDemographics.FirstName,
	PatientReceivableServices.Service, 
	PatientReceivableServices.Charge, 
	PatientReceivableServices.Modifier, 
	PatientReceivableServices.Status, 
	PatientReceivables.Invoice, 
	PracticeVendors.VendorName, 
	PracticeServices.Description, 
	Appointments.AppointmentId, 
	Appointments.ResourceId1 AS DoctorSch, 
	Appointments.ResourceId2 AS LocationSch, 
	PatientReceivables.BillToDr AS DoctorRec, 
	PatientReceivables.ReceivableId, 
	PatientReceivables.InsurerId, 
	PatientReceivables.InsurerDesignation,
	PatientReceivableServices.Quantity, 
	PatientReceivableServices.ItemId AS RecServID, 
	PatientReceivables.BillingOffice, 
	PatientDemographics.PatientId
FROM PracticeServices 
RIGHT JOIN ((Appointments 
RIGHT JOIN ((PatientReceivables 
LEFT JOIN PatientReceivableServices 
ON PatientReceivables.Invoice = PatientReceivableServices.Invoice) 
INNER JOIN PatientDemographics 
ON PatientReceivables.PatientId = PatientDemographics.PatientId) 
ON Appointments.AppointmentId = PatientReceivables.AppointmentId) 
LEFT JOIN PracticeVendors 
ON PatientDemographics.ReferringPhysician = PracticeVendors.VendorId) 
ON PracticeServices.Code = PatientReceivableServices.Service
WHERE (((PatientReceivableServices.Status)<>''X'')) and
((PatientReceivables.InsurerDesignation <> ''F'') Or (PatientReceivables.InsurerId < 1))


')
EXEC('
/****** Object:  View [dbo].[PtTransJrnlToPtFinancial]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[PtTransJrnlToPtFinancial]
AS 
SELECT DISTINCT
  PatientFinancial.FinancialInsurerId, PatientFinancial.FinancialGroupId, PatientReceivables.PatientId,
  PatientReceivables.InsuredId, PatientFinancial.FinancialId, PatientReceivables.InsurerDesignation, PatientFinancial.FinancialPerson,
  PatientFinancial.FinancialPIndicator, PatientReceivables.ReceivableId,
  PatientReceivables.AppointmentId, PatientReceivables.Invoice,
  PatientReceivables.InvoiceDate, PatientReceivables.Charge,
  PatientReceivables.BillToDr, PracticeTransactionJournal.TransactionId,
  PracticeTransactionJournal.TransactionDate, PracticeTransactionJournal.TransactionStatus,
  PracticeTransactionJournal.TransactionType, PracticeTransactionJournal.TransactionRef,
  PracticeTransactionJournal.TransactionRemark
  FROM PatientDemographics
  INNER JOIN ((PatientReceivables
  LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
  INNER JOIN PatientFinancial ON (PatientReceivables.InsuredId = PatientFinancial.PatientId) AND (PatientReceivables.InsurerId = PatientFinancial.FinancialInsurerId)) ON PatientDemographics.PolicyPatientId = PatientFinancial.PatientId
  WHERE (((PracticeTransactionJournal.TransactionStatus)<>''Z'' And (PracticeTransactionJournal.TransactionStatus)<>''G'') AND ((PracticeTransactionJournal.TransactionType)=''R''))
UNION ALL SELECT DISTINCT
  PatientFinancial.FinancialInsurerId, PatientFinancial.FinancialGroupId, PatientReceivables.PatientId,
PatientReceivables.InsuredId, PatientFinancial.FinancialId, PatientReceivables.InsurerDesignation, PatientFinancial.FinancialPerson, 
PatientFinancial.FinancialPIndicator, PatientReceivables.ReceivableId, PatientReceivables.AppointmentId, 
PatientReceivables.Invoice,PatientReceivables.InvoiceDate, PatientReceivables.Charge, 
PatientReceivables.BillToDr,PracticeTransactionJournal.TransactionId, PracticeTransactionJournal.TransactionDate,
PracticeTransactionJournal.TransactionStatus, PracticeTransactionJournal.TransactionType,
PracticeTransactionJournal.TransactionRef, PracticeTransactionJournal.TransactionRemark
FROM PatientDemographics
INNER JOIN ((PatientReceivables
LEFT JOIN PracticeTransactionJournal ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
INNER JOIN PatientFinancial ON (PatientReceivables.InsuredId = PatientFinancial.PatientId) AND (PatientReceivables.InsurerId = PatientFinancial.FinancialInsurerId)) ON PatientDemographics.SecondPolicyPatientId = PatientFinancial.PatientId
WHERE (((PracticeTransactionJournal.TransactionStatus)<>''Z'' And (PracticeTransactionJournal.TransactionStatus)<>''G'') AND ((PracticeTransactionJournal.TransactionType)=''R''))
')
EXEC('
/****** Object:  View [dbo].[PtTransJrnlToPtFinancialPostDate]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER OFF')
EXEC('



CREATE  VIEW [dbo].[PtTransJrnlToPtFinancialPostDate]
AS 
SELECT DISTINCT
  PatientFinancial.FinancialInsurerId, 
  PatientFinancial.FinancialGroupId, 
  PatientFinancial.FinancialStartDate,
  PatientFinancial.FinancialEndDate,
  PatientReceivables.PatientId,
  PatientReceivables.InsuredId, 
  PatientFinancial.FinancialId, 
  PatientReceivables.InsurerDesignation, 
  PatientFinancial.FinancialPerson,
  PatientFinancial.FinancialPIndicator, 
  PatientReceivables.ReceivableId,
  PatientReceivables.AppointmentId, 
  PatientReceivables.Invoice,
  PatientReceivableServices.PostDate as InvoiceDate, 
  PatientReceivables.Charge,
  PatientReceivables.BillToDr, 
  PracticeTransactionJournal.TransactionId,
  PracticeTransactionJournal.TransactionDate, 
  PracticeTransactionJournal.TransactionStatus,
  PracticeTransactionJournal.TransactionType, 
  PracticeTransactionJournal.TransactionRef,
  PracticeTransactionJournal.TransactionRemark
  FROM PatientDemographics
  INNER JOIN ((PatientReceivables
  LEFT JOIN PracticeTransactionJournal 
  ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
  INNER JOIN PatientFinancial 
  ON (PatientReceivables.InsuredId = PatientFinancial.PatientId) AND (PatientReceivables.InsurerId = PatientFinancial.FinancialInsurerId)) 
  ON PatientDemographics.PolicyPatientId = PatientFinancial.PatientId
  inner join  PatientReceivableServices ON PatientReceivableServices.Invoice = PatientReceivables.Invoice
  WHERE (((PracticeTransactionJournal.TransactionStatus)<>''Z'' And (PracticeTransactionJournal.TransactionStatus)<>''G'') AND ((PracticeTransactionJournal.TransactionType)=''R''))
UNION ALL SELECT DISTINCT
  PatientFinancial.FinancialInsurerId, 
  PatientFinancial.FinancialGroupId, 
  PatientFinancial.FinancialStartDate,
  PatientFinancial.FinancialEndDate,
  PatientReceivables.PatientId,
  PatientReceivables.InsuredId, 
  PatientFinancial.FinancialId, 
  PatientReceivables.InsurerDesignation, 
  PatientFinancial.FinancialPerson, 
  PatientFinancial.FinancialPIndicator, 
  PatientReceivables.ReceivableId, 
  PatientReceivables.AppointmentId, 
  PatientReceivables.Invoice,
  PatientReceivableServices.PostDate as  InvoiceDate, 
  PatientReceivables.Charge, 
  PatientReceivables.BillToDr,
  PracticeTransactionJournal.TransactionId, 
  PracticeTransactionJournal.TransactionDate,
  PracticeTransactionJournal.TransactionStatus, 
  PracticeTransactionJournal.TransactionType,
  PracticeTransactionJournal.TransactionRef, 
  PracticeTransactionJournal.TransactionRemark
FROM PatientDemographics
  INNER JOIN ((PatientReceivables
  LEFT JOIN PracticeTransactionJournal 
  ON PatientReceivables.ReceivableId = PracticeTransactionJournal.TransactionTypeId)
  INNER JOIN PatientFinancial 
  ON (PatientReceivables.InsuredId = PatientFinancial.PatientId) AND (PatientReceivables.InsurerId = PatientFinancial.FinancialInsurerId)) 
  ON PatientDemographics.SecondPolicyPatientId = PatientFinancial.PatientId
  inner join  PatientReceivableServices ON PatientReceivableServices.Invoice = PatientReceivables.Invoice
  WHERE (((PracticeTransactionJournal.TransactionStatus)<>''Z'' And (PracticeTransactionJournal.TransactionStatus)<>''G'') AND ((PracticeTransactionJournal.TransactionType)=''R''))



')
EXEC('
/****** Object:  View [dbo].[qryDxBreakdown]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[qryDxBreakdown]
AS
SELECT DISTINCT 
                      dbo.PatientReceivables.Invoice, dbo.PatientReceivables.PatientId, dbo.PatientReceivables.ReceivableId, dbo.Appointments.AppointmentId, 
                      dbo.PatientReceivables.InvoiceDate, dbo.PatientClinical.FindingDetail, dbo.PatientClinical.Symptom, dbo.PatientReceivables.BillToDr, 
                      dbo.Appointments.ResourceId1 AS SchDoc, dbo.Appointments.ResourceId2 AS Loc
FROM         dbo.PatientReceivables INNER JOIN
                      dbo.Appointments ON dbo.PatientReceivables.AppointmentId = dbo.Appointments.AppointmentId INNER JOIN
                      dbo.PatientClinical ON dbo.Appointments.AppointmentId = dbo.PatientClinical.AppointmentId
WHERE     (dbo.PatientClinical.ClinicalType = ''K'' OR
                      dbo.PatientClinical.ClinicalType = ''B'') AND (dbo.PatientClinical.Status = ''A'') AND (dbo.PatientReceivables.Status = ''A'')
')
EXEC('
/****** Object:  View [dbo].[qryMerge]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[qryMerge]
AS
SELECT DISTINCT 
                      dbo.PatientDemographics.PatientId, dbo.PatientDemographics.LastName, dbo.PatientDemographics.FirstName, 
                      dbo.PatientDemographics.MiddleInitial, dbo.PatientDemographics.ProfileComment1, dbo.PatientDemographics.SocialSecurity, 
                      dbo.PatientDemographics.Address, dbo.PatientDemographics.Suite, dbo.PatientDemographics.City, dbo.PatientDemographics.State, 
                      dbo.PatientDemographics.Zip, dbo.PatientDemographics.HomePhone, dbo.PatientDemographics.CellPhone, dbo.PatientDemographics.Email, 
                      dbo.PatientDemographics.EmergencyName, dbo.PatientDemographics.EmergencyPhone, dbo.PatientDemographics.Occupation, 
                      dbo.PatientDemographics.BusinessName, dbo.PatientDemographics.BusinessAddress, dbo.PatientDemographics.BusinessSuite, 
                      dbo.PatientDemographics.BusinessCity, dbo.PatientDemographics.BusinessState, dbo.PatientDemographics.BusinessZip, 
                      dbo.PatientDemographics.BusinessPhone, dbo.PatientDemographics.BusinessType, dbo.PatientDemographics.Gender, 
                      dbo.PatientDemographics.Marital, dbo.PatientDemographics.BirthDate, dbo.PatientDemographics.ProfileComment1 AS Expr1, 
                      Resources_SchDoc.ResourceDescription AS SchDocName, Resources_SchDoc.ResourceTaxId AS SchDocTaxID, 
                      Resources_SchDoc.ResourceAddress AS SchDocAddress, Resources_SchDoc.ResourceSuite AS SchDocSuite, 
                      Resources_SchDoc.ResourceCity AS SchDocCity, Resources_SchDoc.ResourceState AS SchDocState, Resources_SchDoc.ResourceZip AS SchDocZip, 
                      Resources_SchDoc.ResourcePhone AS SchDocPhone, Resources_SchDoc.ResourceEmail AS SchDocEmail, 
                      PracticeVendors_PCP.VendorName AS PCPDocName, PracticeVendors_PCP.VendorAddress AS PCPDocAddress, 
                      PracticeVendors_PCP.VendorSuite AS PCPDocSuite, PracticeVendors_PCP.VendorCity AS PCPDocCity, 
                      PracticeVendors_PCP.VendorState AS PCPDocState, PracticeVendors_PCP.VendorZip AS PCPDocZip, 
                      PracticeVendors_PCP.VendorPhone AS PCPDocPhone, PracticeVendors_PCP.VendorFax AS PCPDocFax, 
                      PracticeVendors_PCP.VendorEmail AS PCPDocEmail, PracticeVendors_PCP.VendorTaxId AS PCPDocTaxId, 
                      PracticeVendors_RefDoc.VendorName AS RefDocName, PracticeVendors_RefDoc.VendorAddress AS RefDocAddress, 
                      PracticeVendors_RefDoc.VendorSuite AS RefDocSuite, PracticeVendors_RefDoc.VendorCity AS RefDocCity, 
                      PracticeVendors_RefDoc.VendorState AS RefDocState, PracticeVendors_RefDoc.VendorZip AS RefDocZip, 
                      PracticeVendors_RefDoc.VendorPhone AS RefDocPhone, PracticeVendors_RefDoc.VendorFax AS RefDocFax, 
                      PracticeVendors_RefDoc.VendorEmail AS RefDocEmail, PracticeVendors_RefDoc.VendorTaxId AS RefDocTaxId
FROM         dbo.PatientDemographics LEFT OUTER JOIN
                      dbo.Resources Resources_SchDoc ON dbo.PatientDemographics.SchedulePrimaryDoctor = Resources_SchDoc.ResourceId LEFT OUTER JOIN
                      dbo.PracticeVendors PracticeVendors_PCP ON dbo.PatientDemographics.PrimaryCarePhysician = PracticeVendors_PCP.VendorId LEFT OUTER JOIN
                      dbo.PracticeVendors PracticeVendors_RefDoc ON dbo.PatientDemographics.ReferringPhysician = PracticeVendors_RefDoc.VendorId
')
EXEC('
/****** Object:  View [dbo].[qryPrimIns]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[qryPrimIns]
AS
SELECT     dbo.PatientFinancial.FinancialPerson AS PrimPolicyNum, dbo.PatientFinancial.FinancialGroupId AS PrimGroupNum, 
                      dbo.PatientFinancial.FinancialCopay AS PrimCopay, dbo.PatientFinancial.FinancialStartDate AS PrimStartDate, 
                      dbo.PatientFinancial.FinancialEndDate AS PrimEndDate, dbo.PracticeInsurers.InsurerName AS PrimInsName, 
                      dbo.PracticeInsurers.InsurerAddress AS PrimInsAddress, dbo.PracticeInsurers.InsurerCity AS PrimInsCity, 
                      dbo.PracticeInsurers.InsurerState AS PrimInsState, dbo.PracticeInsurers.InsurerZip AS PrimInsZip, 
                      dbo.PracticeInsurers.InsurerPhone AS PrimInsPhone
FROM         dbo.PatientFinancial LEFT OUTER JOIN
                      dbo.PracticeInsurers ON dbo.PatientFinancial.FinancialInsurerId = dbo.PracticeInsurers.InsurerId
WHERE     (dbo.PatientFinancial.Status = ''C'') AND (dbo.PatientFinancial.FinancialPIndicator = 1)
')
EXEC('
/****** Object:  View [dbo].[qryProctype]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[qryProctype]
AS
SELECT     ReferenceType, Code
FROM         dbo.PracticeCodeTable
WHERE     (ReferenceType = ''SERVICECATEGORY'')
')
EXEC('
/****** Object:  View [dbo].[TotalAR]    Script Date: 10/3/2012 3:52:31 PM ******/
SET ANSI_NULLS ON')
EXEC('
SET QUOTED_IDENTIFIER ON')
EXEC('
CREATE VIEW [dbo].[TotalAR]
AS
SELECT     dbo.PatientReceivables.OpenBalance, dbo.PatientReceivables.ReceivableId, dbo.PatientReceivables.InsurerId, dbo.PatientReceivables.Invoice, 
                      dbo.PatientReceivables.InvoiceDate, dbo.PatientReceivables.Charge, dbo.PatientReceivables.ChargesByFee, 
                      dbo.PatientReceivablePayments.PaymentId, dbo.PatientReceivablePayments.PaymentAmount, dbo.PatientReceivablePayments.PaymentType, 
                      dbo.PatientReceivablePayments.PayerType, dbo.PatientReceivablePayments.PaymentServiceItem, 
                      dbo.PatientReceivablePayments.PaymentFinancialType, dbo.PatientReceivables.BillingOffice, dbo.Resources.ResourceName, 
                      dbo.PatientDemographics.LastName, dbo.PatientDemographics.FirstName, dbo.PatientReceivables.PatientId, dbo.PatientDemographics.MiddleInitial, 
                      dbo.PatientReceivables.BillToDr, dbo.PatientReceivables.UnallocatedBalance
FROM         dbo.PatientReceivables LEFT OUTER JOIN
                      dbo.PatientReceivablePayments ON dbo.PatientReceivables.ReceivableId = dbo.PatientReceivablePayments.ReceivableId LEFT OUTER JOIN
                      dbo.Resources ON dbo.PatientReceivables.BillToDr = dbo.Resources.ResourceId INNER JOIN
                      dbo.PatientDemographics ON dbo.PatientReceivables.PatientId = dbo.PatientDemographics.PatientId
WHERE     (dbo.PatientReceivables.OpenBalance <= - 0.01) OR
                      (dbo.PatientReceivables.OpenBalance >= 0.01)
')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [PatientId]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [AppTypeId]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [AppDate]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [AppTime]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [Duration]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ReferralId]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [PreCertId]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [TechApptTypeId]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [ScheduleStatus]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [ActivityStatus]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [Comments]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId1]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId2]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId3]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId4]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId5]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId6]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId7]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ((0)) FOR [ResourceId8]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [ApptInsType]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [ConfirmStatus]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [ApptTypeCat]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [SetDate]')
EXEC('
ALTER TABLE [dbo].[Appointments] ADD  DEFAULT ('''') FOR [VisitReason]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [AppointmentType]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [Question]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [Duration]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [DurationOther]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [Recursion]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId1]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId2]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId3]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId4]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId5]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId6]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId7]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [ResourceId8]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [DrRequired]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [PcRequired]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [Period]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [Rank]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [OtherRank]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [RecallFreq]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ((0)) FOR [RecallMax]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [Category]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [FirstVisitIndicator]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [AvailableInPlan]')
EXEC('
ALTER TABLE [dbo].[AppointmentType] ADD  DEFAULT ('''') FOR [EMBasis]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [SKU]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Type_]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Manufacturer]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Series]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Material]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Disposable]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [WearTime]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Tint]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [BaseCurve]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Diameter]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [SpherePower]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [CylinderPower]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [Axis]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [AddReading]')
EXEC('
ALTER TABLE [dbo].[CLInventory] ADD  DEFAULT ('''') FOR [PeriphCurve]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipTo]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipAddress1]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipAddress2]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipCity]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipState]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [ShipZip]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [Eye]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [OrderDate]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [OrderComplete]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [OrderType]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [Reference]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [PONumber]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [AlternateReference]')
EXEC('
ALTER TABLE [dbo].[CLOrders] ADD  DEFAULT ('''') FOR [Rx]')
EXEC('
ALTER TABLE [dbo].[CMSReports] ADD  DEFAULT ((1)) FOR [IsSP]')
EXEC('
ALTER TABLE [dbo].[Encounter_IE_Overrides] ADD  DEFAULT (''E'') FOR [Type]')
EXEC('
ALTER TABLE [dbo].[IE_FieldParams] ADD  DEFAULT ((0)) FOR [LookupId]')
EXEC('
ALTER TABLE [dbo].[IE_Rules] ADD  DEFAULT ((0)) FOR [ShowAlert_YN]')
EXEC('
ALTER TABLE [dbo].[IE_Rules] ADD  DEFAULT ((0)) FOR [PatientRule_YN]')
EXEC('
ALTER TABLE [dbo].[IE_RulesPhrases] ADD  DEFAULT (''*'') FOR [AndOrFlag]')
EXEC('
ALTER TABLE [dbo].[LoginUsers_Audit] ADD  CONSTRAINT [DF_LoginUsers_Audit_TimeStamp]  DEFAULT (getdate()) FOR [TimeStamp]')
EXEC('
ALTER TABLE [dbo].[Lookups] ADD  DEFAULT ((0)) FOR [ICDCPTExists]')
EXEC('
ALTER TABLE [dbo].[LookupValues] ADD  DEFAULT ((0)) FOR [Type]')
EXEC('
ALTER TABLE [dbo].[MedicaidLocationCodes] ADD  DEFAULT ('''') FOR [LocationCode]')
EXEC('
ALTER TABLE [dbo].[Messaging] ADD  CONSTRAINT [DF_Messaging_ID]  DEFAULT (newid()) FOR [ID]')
EXEC('
ALTER TABLE [dbo].[Messaging] ADD  CONSTRAINT [DF_Messaging_MsgStatus]  DEFAULT (''U'') FOR [MsgStatus]')
EXEC('
ALTER TABLE [dbo].[Messaging2] ADD  CONSTRAINT [DF_Messaging2_ID]  DEFAULT (newid()) FOR [ID]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [BusinessClassId]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [CPT]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule1]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description1]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule2]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description2]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule3]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description3]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule4]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description4]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule5]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description5]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule6]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description6]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule7]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description7]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule8]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description8]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule9]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description9]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule10]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description10]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule11]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description11]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule12]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description12]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule13]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description13]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule14]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description14]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Rule15]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Description15]')
EXEC('
ALTER TABLE [dbo].[ModifierRules] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[Patient_EPrescription_RenewalRequest] ADD  CONSTRAINT [DF_Patient_EPrescription_RenewalRequest_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]')
EXEC('
ALTER TABLE [dbo].[Patient_EPrescription_RenewalRequest] ADD  CONSTRAINT [DF_Patient_EPrescription_RenewalRequest_Status]  DEFAULT ((0)) FOR [Status]')
EXEC('
ALTER TABLE [dbo].[Patient_Images] ADD  DEFAULT ((0)) FOR [Abnormal]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [ClinicalType]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [EyeContext]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [Symptom]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [FindingDetail]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [ImageDescriptor]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [ImageInstructions]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [DrawFileName]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [Highlights]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [FollowUp]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [PermanentCondition]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ((0)) FOR [PostOpPeriod]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [Surgery]')
EXEC('
ALTER TABLE [dbo].[PatientClinical] ADD  DEFAULT ('''') FOR [Activity]')
EXEC('
ALTER TABLE [dbo].[PatientClinicalSurgeryPlan] ADD  DEFAULT ((0)) FOR [SurgeryClnId]')
EXEC('
ALTER TABLE [dbo].[PatientClinicalSurgeryPlan] ADD  DEFAULT ('''') FOR [SurgeryRefType]')
EXEC('
ALTER TABLE [dbo].[PatientClinicalSurgeryPlan] ADD  DEFAULT ('''') FOR [SurgeryValue]')
EXEC('
ALTER TABLE [dbo].[PatientClinicalSurgeryPlan] ADD  DEFAULT ('''') FOR [SurgeryStatus]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [Address]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [Suite]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [City]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [State]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [Zip]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [HomePhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [WorkPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [CellPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [Email]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [Pharmacy]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [PhoneType1]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [PhoneType2]')
EXEC('
ALTER TABLE [dbo].[PatientDemoAlt] ADD  DEFAULT ('''') FOR [PhoneType3]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [LastName]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [FirstName]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [MiddleInitial]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [NameReference]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [SocialSecurity]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Address]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Suite]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [City]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [State]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Zip]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [HomePhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [CellPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Email]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [EmergencyName]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [EmergencyPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [EmergencyRel]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Occupation]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessName]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessAddress]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessSuite]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessCity]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessState]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessZip]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BusinessType]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Gender]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Marital]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BirthDate]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [NationalOrigin]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Language]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ((0)) FOR [SchedulePrimaryDoctor]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [ProfileComment1]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [ProfileComment2]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [ContactType]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Relationship]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BillParty]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [SecondRelationship]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [SecondBillParty]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [ReferralCatagory]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [BulkMailing]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [FinancialAssignment]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [FinancialSignature]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [FinancialSignatureDate]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Salutation]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Hipaa]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [PatType]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [SendStatements]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [SendConsults]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [OldPatient]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [ReferralRequired]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [MedicareSecondary]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ((0)) FOR [PostOpExpireDate]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ((0)) FOR [PostOpService]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [EmployerPhone]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Religion]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Race]')
EXEC('
ALTER TABLE [dbo].[PatientDemographics] ADD  DEFAULT ('''') FOR [Ethnicity]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [FinancialPerson]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [FinancialGroupId]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [FinancialStartDate]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [FinancialEndDate]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientFinancial] ADD  DEFAULT ('''') FOR [FinancialInsType]')
EXEC('
ALTER TABLE [dbo].[PatientFinancialDependents] ADD  DEFAULT ('''') FOR [MemberId]')
EXEC('
ALTER TABLE [dbo].[PatientFinancialDependents] ADD  DEFAULT ('''') FOR [SubscriberId]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteType]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [Note1]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [Note2]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [Note3]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [Note4]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [UserName]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteDate]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteSystem]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteEye]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteCommentOn]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteCategory]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteOffDate]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteHighlight]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteILPNRef]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteAlertType]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteAudioOn]')
EXEC('
ALTER TABLE [dbo].[PatientNotes] ADD  DEFAULT ('''') FOR [NoteClaimOn]')
EXEC('
ALTER TABLE [dbo].[PatientPreCerts] ADD  DEFAULT ('''') FOR [PreCert]')
EXEC('
ALTER TABLE [dbo].[PatientPreCerts] ADD  DEFAULT ('''') FOR [PreCertDate]')
EXEC('
ALTER TABLE [dbo].[PatientPreCerts] ADD  DEFAULT ('''') FOR [PreCertComment]')
EXEC('
ALTER TABLE [dbo].[PatientPreCerts] ADD  DEFAULT ('''') FOR [PreCertStatus]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PayerType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentCheck]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentRefId]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentRefType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentService]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentCommentOn]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentFinancialType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentReason]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentAppealNumber]')
EXEC('
ALTER TABLE [dbo].[PatientReceivablePayments] ADD  DEFAULT ('''') FOR [PaymentEOBDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [ReceivableType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [Invoice]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [InvoiceDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [AccType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [AccState]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [HspAdmDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [HspDisDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [Disability]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [RsnType]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [RsnDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [FirstConsDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [PrevCond]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [ExternalRefInfo]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [Over90]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [PatientBillDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [LastPayDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [InsurerDesignation]')
EXEC('
ALTER TABLE [dbo].[PatientReceivables] ADD  DEFAULT ('''') FOR [WriteOff]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [Invoice]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [Service]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [Modifier]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [TypeOfService]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [PlaceOfService]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [ServiceDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [LinkedDiag]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [OrderDoc]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [ConsultOn]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [PostDate]')
EXEC('
ALTER TABLE [dbo].[PatientReceivableServices] ADD  DEFAULT ('''') FOR [ExternalRefInfo]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [Referral]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [ReferredFrom]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [ReferralDate]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [ReferralExpireDate]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [Reason]')
EXEC('
ALTER TABLE [dbo].[PatientReferral] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PayerMapping] ADD  DEFAULT (newid()) FOR [ID]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [SKU]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [Type_]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [Manufacturer]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [Model]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [Color]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [PCSize]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [LensCoating]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [LensTint]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [PDD]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [PDN]')
EXEC('
ALTER TABLE [dbo].[PCInventory] ADD  DEFAULT ('''') FOR [CEGHeight]')
EXEC('
ALTER TABLE [dbo].[PhysicianSpi] ADD  DEFAULT ('''') FOR [PhoneType]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [StationId]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [ActivityDate]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [Status]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [ActivityStatusTime]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [ResourceId]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [TechConfirmed]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [CurrentRId]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [LocationId]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [PayAmount]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [PayMethod]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ('''') FOR [PayReference]')
EXEC('
ALTER TABLE [dbo].[PracticeActivity] ADD  DEFAULT ((0)) FOR [ActivityStatusTRef]')
EXEC('
ALTER TABLE [dbo].[PracticeAffiliations] ADD  DEFAULT ('''') FOR [ResourceType]')
EXEC('
ALTER TABLE [dbo].[PracticeAffiliations] ADD  DEFAULT ('''') FOR [Pin]')
EXEC('
ALTER TABLE [dbo].[PracticeAffiliations] ADD  DEFAULT ('''') FOR [Override]')
EXEC('
ALTER TABLE [dbo].[PracticeAffiliations] ADD  DEFAULT ('''') FOR [AlternatePin]')
EXEC('
ALTER TABLE [dbo].[PracticeAffiliations] ADD  DEFAULT ('''') FOR [OrgOverride]')
EXEC('
ALTER TABLE [dbo].[PracticeAudit] ADD  DEFAULT ('''') FOR [Date]')
EXEC('
ALTER TABLE [dbo].[PracticeAudit] ADD  DEFAULT ('''') FOR [PrevRec]')
EXEC('
ALTER TABLE [dbo].[PracticeAudit] ADD  DEFAULT ('''') FOR [CurrentRec]')
EXEC('
ALTER TABLE [dbo].[PracticeAudit] ADD  DEFAULT ('''') FOR [Action]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CalendarDate]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CalendarPurpose]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CalendarStartTime]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CalendarEndTime]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray1]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray2]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray3]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray4]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray5]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray6]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray7]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CatArray8]')
EXEC('
ALTER TABLE [dbo].[PracticeCalendar] ADD  DEFAULT ('''') FOR [CalendarComment]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [ReferenceType]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [Code]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [AlternateCode]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [Exclusion]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [LetterTranslation]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [OtherLtrTrans]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [FormatMask]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [Signature]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [TestOrder]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ('''') FOR [FollowUp]')
EXEC('
ALTER TABLE [dbo].[PracticeCodeTable] ADD  DEFAULT ((0)) FOR [Rank]')
EXEC('
ALTER TABLE [dbo].[PracticeFavorites] ADD  DEFAULT ('''') FOR [System]')
EXEC('
ALTER TABLE [dbo].[PracticeFavorites] ADD  DEFAULT ('''') FOR [Diagnosis]')
EXEC('
ALTER TABLE [dbo].[PracticeFavorites] ADD  DEFAULT ('''') FOR [Alternate]')
EXEC('
ALTER TABLE [dbo].[PracticeFeeSchedules] ADD  DEFAULT ('''') FOR [CPT]')
EXEC('
ALTER TABLE [dbo].[PracticeFeeSchedules] ADD  DEFAULT ('''') FOR [StartDate]')
EXEC('
ALTER TABLE [dbo].[PracticeFeeSchedules] ADD  DEFAULT ('''') FOR [EndDate]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerName]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerAddress]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerCity]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerState]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerZip]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPlanId]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPlanName]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPlanType]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerGroupId]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerGroupName]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerReferenceCode]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [ReferralRequired]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ((0)) FOR [Copay]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ((0)) FOR [OutOfPocket]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [Availability]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerFeeSchedule]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerTosTbl]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPosTbl]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [NEICNumber]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerENumber]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerEFormat]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerTFormat]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPrecPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerEligPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerProvPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerClaimPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerCrossOver]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPayType]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerAdjustmentDefault]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [PrecertRequired]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [MedicareCrossOverId]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerComment]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerPlanFormat]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerClaimMap]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerBusinessClass]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [OCode]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerCPTClass]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [InsurerServiceFilter]')
EXEC('
ALTER TABLE [dbo].[PracticeInsurers] ADD  DEFAULT ('''') FOR [StateJurisdiction]')
EXEC('
ALTER TABLE [dbo].[PracticeInterfaceConfiguration] ADD  DEFAULT ('''') FOR [Interface]')
EXEC('
ALTER TABLE [dbo].[PracticeInterfaceConfiguration] ADD  DEFAULT ('''') FOR [FieldReference]')
EXEC('
ALTER TABLE [dbo].[PracticeInterfaceConfiguration] ADD  DEFAULT ('''') FOR [FieldValue]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray1]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray2]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray3]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray4]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray5]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray6]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray7]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [CatArray8]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [PracticeNPI]')
EXEC('
ALTER TABLE [dbo].[PracticeName] ADD  DEFAULT ('''') FOR [PracticeTaxonomy]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [ReportName]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ((0)) FOR [ReportOrder]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [QueryName]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [CriteriaMap]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [EntryScreenMap]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [TemplateName]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [AllowDate]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [DocBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [LocBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [PatBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [DocLocBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [DocPatBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [LocPatBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [DocLocPatBreak]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [Active]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [Daily]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [CriteriaMapDefaults]')
EXEC('
ALTER TABLE [dbo].[PracticeReports] ADD  DEFAULT ('''') FOR [AdminOn]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [Code]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [CodeType]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [CodeCategory]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [Description]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ((0)) FOR [Fee]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [TOS]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [POS]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [StartDate]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [EndDate]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ((0)) FOR [RelativeValueUnit]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [OrderDoc]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [ConsultOn]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [ClaimNote]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [ServiceFilter]')
EXEC('
ALTER TABLE [dbo].[PracticeServices] ADD  DEFAULT ('''') FOR [NDC]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionType]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ((0)) FOR [TransactionTypeId]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionStatus]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionDate]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ((0)) FOR [TransactionTime]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionRef]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionRemark]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionAction]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionBatch]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ((0)) FOR [TransactionServiceItem]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ((0)) FOR [TransactionRcvrId]')
EXEC('
ALTER TABLE [dbo].[PracticeTransactionJournal] ADD  DEFAULT ('''') FOR [TransactionAssignment]')
EXEC('
ALTER TABLE [dbo].[PracticeVendors] ADD  DEFAULT ('''') FOR [VendorNPI]')
EXEC('
ALTER TABLE [dbo].[PracticeVendors] ADD  DEFAULT ('''') FOR [VendorCellPhone]')
EXEC('
ALTER TABLE [dbo].[PracticeVendors] ADD  DEFAULT ('''') FOR [VendorSpecOther]')
EXEC('
ALTER TABLE [dbo].[Resources] ADD  DEFAULT ('''') FOR [NPI]')
EXEC('
ALTER TABLE [dbo].[Resources] ADD  DEFAULT ('''') FOR [ResourceMI]')
EXEC('
ALTER TABLE [dbo].[Resources] ADD  DEFAULT ('''') FOR [ResourceSuffix]')
EXEC('
ALTER TABLE [dbo].[Resources] ADD  DEFAULT ('''') FOR [ResourcePrefix]')
EXEC('
ALTER TABLE [dbo].[Resources] ADD  CONSTRAINT [DF_Resources_IsLoggedIn]  DEFAULT ((0)) FOR [IsLoggedIn]')
EXEC('
ALTER TABLE [dbo].[ServiceTransactions] ADD  DEFAULT (newid()) FOR [Id]')
EXEC('
ALTER TABLE [dbo].[ServiceTransactions] ADD  DEFAULT (getdate()) FOR [ModTime]')
EXEC('
ALTER TABLE [dbo].[tblMsg] ADD  CONSTRAINT [DF_tblMsg_ProcessStatus]  DEFAULT ((0)) FOR [ProcessStatus]')
EXEC('
ALTER TABLE [dbo].[tblMsg] ADD  CONSTRAINT [DF_tblMsg_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]')
EXEC('
ALTER TABLE [dbo].[User_Patients_Restrict] ADD  DEFAULT ((0)) FOR [NoOfRuns]')
EXEC('
ALTER TABLE [dbo].[User_Patients_Restrict] ADD  DEFAULT ((0)) FOR [RunsCompleted]')
EXEC('
ALTER TABLE [dbo].[User_Patients_Restrict] ADD  DEFAULT ((0)) FOR [IsVisited]')
EXEC('
ALTER TABLE [dbo].[CMSReports_Params]  WITH CHECK ADD  CONSTRAINT [FK_CMSReports_Params_CMSReports] FOREIGN KEY([CMSRepId])
REFERENCES [dbo].[CMSReports] ([CMSRepId])')
EXEC('
ALTER TABLE [dbo].[CMSReports_Params] CHECK CONSTRAINT [FK_CMSReports_Params_CMSReports]')
EXEC('
ALTER TABLE [dbo].[Encounter_IE_Overrides]  WITH CHECK ADD  CONSTRAINT [FK_Encounter_IE_Overrides_IE_Rules] FOREIGN KEY([RuleId])
REFERENCES [dbo].[IE_Rules] ([RuleId])')
EXEC('
ALTER TABLE [dbo].[Encounter_IE_Overrides] CHECK CONSTRAINT [FK_Encounter_IE_Overrides_IE_Rules]')
EXEC('
ALTER TABLE [dbo].[IE_Rules]  WITH CHECK ADD  CONSTRAINT [FK_IE_Rules_IE_Events] FOREIGN KEY([EventId])
REFERENCES [dbo].[IE_Events] ([EventId])')
EXEC('
ALTER TABLE [dbo].[IE_Rules] CHECK CONSTRAINT [FK_IE_Rules_IE_Events]')
EXEC('
ALTER TABLE [dbo].[IE_RulesPhrases]  WITH CHECK ADD  CONSTRAINT [FK_IE_RulePhrases_IE_Rules] FOREIGN KEY([RuleId])
REFERENCES [dbo].[IE_Rules] ([RuleId])')
EXEC('
ALTER TABLE [dbo].[IE_RulesPhrases] CHECK CONSTRAINT [FK_IE_RulePhrases_IE_Rules]')
EXEC('
ALTER TABLE [dbo].[LoginUsers_Audit]  WITH CHECK ADD  CONSTRAINT [FK_LoginUsers_Audit_LoginUsers] FOREIGN KEY([LoginId])
REFERENCES [dbo].[LoginUsers] ([LoginId])')
EXEC('
ALTER TABLE [dbo].[LoginUsers_Audit] CHECK CONSTRAINT [FK_LoginUsers_Audit_LoginUsers]')
EXEC('
ALTER TABLE [dbo].[LookupValues]  WITH CHECK ADD  CONSTRAINT [FK_LookupValues_Lookups] FOREIGN KEY([LookupID])
REFERENCES [dbo].[Lookups] ([LookupID])')
EXEC('
ALTER TABLE [dbo].[LookupValues] CHECK CONSTRAINT [FK_LookupValues_Lookups]')
EXEC('
ALTER TABLE [dbo].[Messaging2]  WITH NOCHECK ADD  CONSTRAINT [FK_Messaging2_Messaging] FOREIGN KEY([MessagingID])
REFERENCES [dbo].[Messaging] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE')
EXEC('
ALTER TABLE [dbo].[Messaging2] CHECK CONSTRAINT [FK_Messaging2_Messaging]')
EXEC('
ALTER TABLE [dbo].[Patient_Immunizations]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Immunizations_Immunizations] FOREIGN KEY([ImmID])
REFERENCES [dbo].[Immunizations] ([ImmNo])')
EXEC('
ALTER TABLE [dbo].[Patient_Immunizations] CHECK CONSTRAINT [FK_Patient_Immunizations_Immunizations]')
EXEC('
')