IF OBJECT_ID('dbo.PadLeft') IS NULL
BEGIN
EXEC ('

CREATE FUNCTION dbo.PadLeft(
      @String int
     ,@NumChars int
     )
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @PadChar char(1) 
	SET @PadChar = ''0''
    RETURN STUFF(@String, 1, 0, REPLICATE(@PadChar, @NumChars - LEN(@String)))
END
')

END

---PATIENT #1 AETNA ONLY
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'AETNA'
	,'ONLY'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #1 AETNA ONLY
--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'12345' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'AETNA'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT PATIENT #1
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111207,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #1
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111207, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #1
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.15', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #1
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '983.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111207
	,'993.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'993'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN PracticeVendors ON VendorLastName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

-- BILLING TRANSACTIONS patient #1
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'S'
	,IDENT_CURRENT('dbo.Appointments')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/983'
	,'B'
	)

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'S'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/983'
	,'B'
	)

--SERVICES  patient #1
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'99244', '24', '1', '675.00', '01', '11',
			20111207, '', 'A', 'F', 'T', '675.00', '1', 0,
			20111207, 'CONSULTATION')

-- PatientReceivablePayments  patient #1
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''

--payment from Patient #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,665
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag]
		  ,[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'92250', '', '1', '318.00', '01', '11',
			20111207, '2', 'A', 'T', 'F', '318.00', '2', 0,
			20111207, 'FUNDUS PHOTOS')
			
-- 2d ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')