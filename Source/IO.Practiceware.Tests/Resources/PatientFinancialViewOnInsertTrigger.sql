
IF OBJECT_ID('[dbo].[InsteadOfInsertIntoPatientFinancial]', 'TR') IS NOT NULL
 BEGIN
    EXEC('DROP TRIGGER [dbo].[InsteadOfInsertIntoPatientFinancial]')
 END

 GO

CREATE TRIGGER InsteadOfInsertIntoPatientFinancial ON dbo.PatientFinancial
INSTEAD OF INSERT
AS 
BEGIN
	INSERT INTO model.InsurancePolicies
	(PolicyCode
	,GroupCode
	,Copay
	,Deductible
	,StartDateTime
	,PolicyHolderPatientId
	,InsurerId
	,MedicareSecondaryReasonCodeId)
	SELECT
	i.FinancialPerson
	,i.FinancialGroupId
	,i.FinancialCopay
	,0
	,CONVERT(DATETIME,i.FinancialStartDate)
	,i.PatientId
	,i.FinancialInsurerId
	,NULL
	FROM Inserted i

	DECLARE @CurrentPatientId INT
	SET @CurrentPatientId = (SELECT PatientId FROM Inserted)

	INSERT INTO model.PatientInsurances
	(InsuranceTypeId
	,PolicyHolderRelationshipTypeId
	,OrdinalId
	,InsuredPatientId
	,InsurancePolicyId
	,EndDateTime
	,IsDeleted)
	SELECT
	CASE i.FinancialInsType
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3			
		WHEN 'W'
			THEN 4
		ELSE 1
	END
	,CASE (
		CASE 
			WHEN pdt.PolicyPatientId = i.PatientId
				THEN pdt.Relationship
			ELSE pdt.SecondRelationship
			END
		)
		WHEN 'Y'
			THEN 1
		WHEN 'S'
			THEN 2
		WHEN 'C'
			THEN 3
		WHEN 'E'
			THEN 4
		WHEN 'L'
			THEN 6
		WHEN 'O'
			THEN 7
		WHEN 'D'
			THEN 7
		WHEN 'P'
			THEN 7
		ELSE CASE 
				WHEN pdt.PolicyPatientId = i.PatientId
					THEN 1
				WHEN pdt.SecondPolicyPatientId = i.PatientId
					THEN 1
			ELSE 5
			END
	END
	,CASE i.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
					WHEN i.PatientId = pdt.PolicyPatientId
						THEN 0
					ELSE 3
				  END + CONVERT(INT, i.FinancialPIndicator)
	,i.PatientId
	,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = @CurrentPatientId ORDER BY Id DESC)
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(DATETIME,i.FinancialEndDate)
					END
		ELSE NULL
	END
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(BIT, 1)
					END
		ELSE CONVERT(BIT, 0)
	END
	FROM Inserted i
	JOIN PatientDemographicsTable pdt ON pdt.PatientId = i.PatientId
END