﻿IF EXISTS (SELECT * FROM Model.ExternalSystems WHERE Name = 'MVE')
BEGIN
	DECLARE @mveExternalSystemId int
	SET @mveExternalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'MVE')

	-- Create mappings from our MaritalStatus values, to MVE MaritalStatus values
	IF EXISTS (SELECT AlternateCode FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND Code = 'Single' AND AlternateCode IS NOT NULL)
	BEGIN
		DECLARE @externalMaritalStatusEntityId int
		SET @externalMaritalStatusEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'MaritalStatus' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @maritalStatusEntityId int
		SET @maritalStatusEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'MaritalStatus')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT @maritalStatusEntityId,
				CASE Code
					WHEN 'Single' THEN 1
					WHEN 'Married' THEN 2
					WHEN 'Divorced' THEN 3
					WHEN 'Widowed' THEN 4
				END,
				@externalMaritalStatusEntityId,
				AlternateCode
		FROM dbo.PracticeCodeTable WHERE ReferenceType = 'MARITAL' AND AlternateCode IS NOT NULL
	END


	-- Create mappings from our PatientLanguage values to MVE PatientLanguage values
	IF EXISTS (SELECT AlternateCode FROM dbo.PracticeCodeTable WHERE ReferenceType = 'LANGUAGE' AND AlternateCode IS NOT NULL)
	BEGIN
		DECLARE @externalPatientLanguageEntityId int
		SET @externalPatientLanguageEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'Language' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @patientLanguageEntityId int
		SET @patientLanguageEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Language')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT @patientLanguageEntityId,
			   Id,
			   @externalPatientLanguageEntityId,
			   AlternateCode
		FROM dbo.PracticeCodeTable WHERE ReferenceType = 'LANGUAGE' AND AlternateCode IS NOT NULL
	END


	-- Create mappings from our ClaimFilingIndicatorCode values to MVE ClaimFilingIndicatorCode values
	IF EXISTS (SELECT LetterTranslation FROM dbo.PracticeCodeTable WHERE ReferenceType = 'INSURERREF' AND LetterTranslation IS NOT NULL)
	BEGIN
		DECLARE @externalClaimFilingIndicatorCodeEntityId int
		SET @externalClaimFilingIndicatorCodeEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'ClaimFilingIndicatorCode' AND ExternalSystemId = @mveExternalSystemId)

		DECLARE @claimFilingIndicatorCodeEntityId int
		SET @claimFilingIndicatorCodeEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'ClaimFilingIndicatorCode')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		-- ClaimFilingIndicatorCode(7)=HealthMaintenanceOrganizationMedicareRisk
		SELECT @claimFilingIndicatorCodeEntityId, 7, @externalClaimFilingIndicatorCodeEntityId, 907
		UNION ALL
		-- ClaimFilingIndicatorCode(10)=BlueCrossBlueShield
		SELECT @claimFilingIndicatorCodeEntityId, 10, @externalClaimFilingIndicatorCodeEntityId, 1536
		UNION ALL
		-- ClaimFilingIndicatorCode(11)=Champus
		SELECT @claimFilingIndicatorCodeEntityId, 11, @externalClaimFilingIndicatorCodeEntityId, 908
		UNION ALL
		-- ClaimFilingIndicatorCode(12)=CommercialInsurance
		SELECT @claimFilingIndicatorCodeEntityId, 12, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(14)=FederalEmployeesProgram
		SELECT @claimFilingIndicatorCodeEntityId, 14, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(18)=MedicarePartB
		SELECT @claimFilingIndicatorCodeEntityId, 18, @externalClaimFilingIndicatorCodeEntityId, 903
		UNION ALL
		-- ClaimFilingIndicatorCode(19)=Medicaid
		SELECT @claimFilingIndicatorCodeEntityId, 19, @externalClaimFilingIndicatorCodeEntityId, 907
		UNION ALL
		-- ClaimFilingIndicatorCode(20)=OtherFederalProgram
		SELECT @claimFilingIndicatorCodeEntityId, 20, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(21)=TitleV
		SELECT @claimFilingIndicatorCodeEntityId, 21, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(22)=VeteransAffairsPlan
		SELECT @claimFilingIndicatorCodeEntityId, 22, @externalClaimFilingIndicatorCodeEntityId, 909
		UNION ALL
		-- ClaimFilingIndicatorCode(23)=WorkersCompensation
		SELECT @claimFilingIndicatorCodeEntityId, 23, @externalClaimFilingIndicatorCodeEntityId, 908
		UNION ALL
		-- ClaimFilingIndicatorCode(24)=MutuallyDefined
		SELECT @claimFilingIndicatorCodeEntityId, 24, @externalClaimFilingIndicatorCodeEntityId, 909
	END
END

	-- Create mappings from Race values to NextGen Rosetta
	IF EXISTS (
			SELECT *
			FROM Model.ExternalSystems
			WHERE NAME = 'NextGen Rosetta'
			)
	BEGIN
		IF EXISTS (
				SELECT LetterTranslation
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'RACE'
					AND LetterTranslation IS NOT NULL
				)
		BEGIN
			DECLARE @ExternalSystemId INT;
			
			SET @ExternalSystemId = (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'NextGen Rosetta'
					)

		
			DECLARE @externalRaceEntityId INT

			SET @externalRaceEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'RACE'
						AND ExternalSystemId = @ExternalSystemId
					)

			DECLARE @raceEntityId INT

			SET @raceEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'RACE'
					)

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @raceEntityId
				,pc.Id
				,@externalRaceEntityId
				,pc.LetterTranslation
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.LetterTranslation
				AND esm.ExternalSystemEntityId = @externalRaceEntityId
			WHERE pc.ReferenceType = 'RACE'
				AND pc.LetterTranslation <> ''
				AND pc.LetterTranslation IS NOT NULL
				AND esm.Id IS NULL
		END
	END

	-- Create mappings from ETHNICITY values to NextGen Rosetta
	IF EXISTS (
			SELECT *
			FROM Model.ExternalSystems
			WHERE NAME = 'NextGen Rosetta'
			)
	BEGIN
		IF EXISTS (
				SELECT LetterTranslation
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'ETHNICITY'
					AND LetterTranslation IS NOT NULL
				)
		BEGIN
			SET @ExternalSystemId = (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'NextGen Rosetta'
					)
			
			DECLARE @externalEthnicityEntityId INT

			SET @externalEthnicityEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'ETHNICITY'
						AND ExternalSystemId = @ExternalSystemId
					)

			DECLARE @ethnicityEntityId INT

			SET @ethnicityEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'ETHNICITY'
					)

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @ethnicityEntityId
				,pc.Id
				,@externalEthnicityEntityId
				,pc.LetterTranslation
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.LetterTranslation
				AND esm.ExternalSystemEntityId = @externalEthnicityEntityId
			WHERE pc.ReferenceType = 'ETHNICITY'
				AND pc.LetterTranslation <> ''
				AND pc.LetterTranslation IS NOT NULL
				AND esm.Id IS NULL
		END
	END

	--Create mappings from Gender values to NextGen Rosetta
	IF EXISTS (
			SELECT *
			FROM Model.ExternalSystems
			WHERE NAME = 'NextGen Rosetta'
			)
	BEGIN
		IF EXISTS (
				SELECT Code
				FROM dbo.PracticeCodeTable
				WHERE ReferenceType = 'GENDER'
					AND Code IS NOT NULL
				)
		BEGIN
			SET @externalSystemId = (
					SELECT Id
					FROM model.ExternalSystems
					WHERE NAME = 'NextGen Rosetta'
					)

			
			DECLARE @externalgenderEntityId INT

			SET @externalgenderEntityId = (
					SELECT Id
					FROM model.ExternalSystemEntities
					WHERE NAME = 'GENDER'
						AND ExternalSystemId = @externalSystemId
					)

			DECLARE @genderEntityId INT

			SET @genderEntityId = (
					SELECT Id
					FROM model.PracticeRepositoryEntities
					WHERE NAME = 'GENDER'
					)

			INSERT INTO model.ExternalSystemEntityMappings (
				PracticeRepositoryEntityId
				,PracticeRepositoryEntityKey
				,ExternalSystemEntityId
				,ExternalSystemEntityKey
				)
			SELECT @genderEntityId
				,CASE Code
					WHEN 'Female'
						THEN 2
					WHEN 'Male'
						THEN 3
					END
				,@externalgenderEntityId
				,SubStringOneCode
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.SubStringOneCode
				AND esm.ExternalSystemEntityId = @externalgenderEntityId
			WHERE pc.ReferenceType = 'GENDER'
				AND pc.Code <> ''
				AND pc.Code IS NOT NULL
				AND esm.Id IS NULL
		END
	END

	--Create mappings for Marital Status for NextGen Rosetta
	IF EXISTS (SELECT * FROM Model.ExternalSystems WHERE Name  IN('NextGen Rosetta'))
BEGIN
	IF EXISTS (SELECT Code FROM dbo.PracticeCodeTable WHERE ReferenceType = 'LANGUAGE' AND Code IS NOT NULL)
	BEGIN
		SET @externalSystemId = (SELECT Id FROM model.ExternalSystems WHERE Name = 'NextGen Rosetta')

		SET @externalPatientLanguageEntityId = (SELECT Id FROM model.ExternalSystemEntities WHERE Name = 'Language' AND ExternalSystemId = @externalSystemId)

		SET @patientLanguageEntityId = (SELECT Id FROM model.PracticeRepositoryEntities WHERE Name = 'Language')

		INSERT INTO model.ExternalSystemEntityMappings (PracticeRepositoryEntityId, PracticeRepositoryEntityKey, ExternalSystemEntityId, ExternalSystemEntityKey)
		SELECT @patientLanguageEntityId
				,pc.Id
				,@externalPatientLanguageEntityId
				,pc.Code
			FROM dbo.PracticeCodeTable pc
			LEFT JOIN model.ExternalSystemEntityMappings esm ON esm.ExternalSystemEntityKey = pc.Code AND esm.ExternalSystemEntityId = @patientLanguageEntityId
			WHERE pc.ReferenceType = 'LANGUAGE'
				AND pc.Code <> ''
				AND pc.Code IS NOT NULL
				AND esm.Id IS NULL
	END
END

