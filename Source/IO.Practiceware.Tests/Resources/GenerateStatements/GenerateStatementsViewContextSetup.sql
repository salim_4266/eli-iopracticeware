DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints 'model.BillingServiceTransactions', @names = @constraintNames OUTPUT

DECLARE @triggerNames xml
EXEC dbo.DisableEnabledTriggers 'model.BillingServiceTransactions', @names = @triggerNames OUTPUT

SELECT b.Id
,b.BillingServiceId
,b.InvoiceReceivableId
,irPatient.Id AS NewInvoiceReceivableId
,b.AmountSent
INTO #FixupBillingServiceTransactions
FROM model.BillingServiceTransactions b
JOIN model.InvoiceReceivables ir ON ir.Id = b.InvoiceReceivableId
JOIN model.Invoices i ON i.Id = ir.InvoiceId
	and i.InvoiceTypeId = 1
JOIN model.InvoiceReceivables irPatient ON irPatient.InvoiceId = i.Id
	AND irPatient.PatientInsuranceId IS NULL
WHERE b.BillingServiceTransactionStatusId IN (1, 2, 3)

UPDATE b
SET b.BillingServiceTransactionStatusId = 5
,b.DateTime = DATEADD(dd,-10,GETDATE())
FROM model.BillingServiceTransactions b
WHERE b.Id IN (SELECT Id FROM #FixupBillingServiceTransactions)

INSERT INTO model.BillingServiceTransactions (DateTime, AmountSent, BillingServiceTransactionStatusId, MethodSentId, BillingServiceId, InvoiceReceivableId)
SELECT
GETDATE() AS DateTime
,SUM(AmountSent) AS AmountSent
,1 AS Status
,1 AS MethodSentId
,BillingServiceId
,NewInvoiceReceivableId
FROM #FixupBillingServiceTransactions
GROUP BY BillingServiceId
,NewInvoiceReceivableId

EXEC dbo.EnableCheckConstraints @constraintNames
EXEC dbo.EnableTriggers @triggerNames

UPDATE e
SET e.EncounterServiceTypeId = 1
FROM model.EncounterServices e
WHERE EncounterServiceTypeId IS NULL