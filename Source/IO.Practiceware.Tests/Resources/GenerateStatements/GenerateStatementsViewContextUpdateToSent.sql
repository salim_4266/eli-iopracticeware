UPDATE b
SET b.DateTime = DATEADD(dd,-1,GETDATE())
FROM model.BillingServiceTransactions b
JOIN model.InvoiceReceivables ir ON ir.Id = b.InvoiceReceivableId
	AND ir.PatientInsuranceId IS NULL
AND b.BillingServiceTransactionStatusId = 2