﻿-- Creates procedures and functions used across the entire system.
IF SCHEMA_ID('model') IS NULL
BEGIN
	EXEC('CREATE SCHEMA model')
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PatientDocumentList]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[PatientDocumentList]
GO

CREATE PROCEDURE [dbo].[PatientDocumentList](@PatId Int)
AS
BEGIN	
	SET NOCOUNT ON
			SELECT	PracticeTransactionJournal.TransactionType,
 			Appointments.PatientId,
			PracticeTransactionJournal.TransactionId,
 			PracticeTransactionJournal.TransactionDate,
			AppointmentType.AppointmentType,
			Appointments.AppDate,
			PracticeTransactionJournal.TransactionRef,
 			PracticeTransactionJournal.TransactionRemark
		FROM PracticeTransactionJournal
		INNER JOIN (Appointments
		LEFT JOIN AppointmentType
		ON Appointments.AppTypeId = AppointmentType.AppTypeId)
		ON PracticeTransactionJournal.TransactionTypeId = Appointments.AppointmentId
		WHERE	Appointments.PatientId = @PatId And
			(PracticeTransactionJournal.TransactionType = 'S' or
			 PracticeTransactionJournal.TransactionType = 'F' or
			 PracticeTransactionJournal.TransactionType = 'Y' or
			 PracticeTransactionJournal.TransactionType = 'X')
		ORDER BY PracticeTransactionJournal.TransactionDate ASC  	
END
GO
