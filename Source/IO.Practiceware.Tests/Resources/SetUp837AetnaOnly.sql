UPDATE  PracticeTransactionJournal 
SET TransactionStatus = 'S'
WHERE TransactionType = 'R'


UPDATE ptj SET TransactionStatus = 'P'
FROM PracticeTransactionJournal ptj
INNER JOIN PatientReceivables pr ON pr.ReceivableId = ptj.TransactionTypeId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
Where TransactionType = 'R'
AND LastName = 'AETNA' AND FirstName = 'ONLY'