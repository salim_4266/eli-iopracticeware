﻿--Runs all stored procedures with null parameters, thereby checking syntax validity.
DECLARE @ObjectId INT
DECLARE @SPName NVARCHAR(255)           
DECLARE @ParamCount INT
DECLARE @i INT

SET NOCOUNT ON

DECLARE abc CURSOR FOR
        SELECT 'model.' + NAME AS Name, object_id
        FROM sys.objects AS o 
        WHERE o.[type] = 'P'
		AND o.[schema_id] = SCHEMA_ID('model')
		AND o.[Name] <> 'DropModelViews' AND o.[Name] NOT LIKE 'RaiseError%' AND o.[Name] NOT LIKE 'USP_CMS%'
		AND o.[Name] NOT LIKE 'sp_MSsync%' AND o.[Name] NOT LIKE 'sp_MScft%'
        ORDER BY o.[name]

OPEN abc

FETCH NEXT FROM abc
INTO @SPName, @ObjectId
WHILE @@FETCH_STATUS = 0 
BEGIN       
	SET @ParamCount = (SELECT COUNT(p.Parameter_Id) FROM sys.parameters p WHERE p.object_id = @ObjectId)
	SET @i = 0

	IF @ParamCount > 0
		SET @SPName = @SPName + ' NULL'
	ELSE SET @SpName = @SPName
    --Makes all parameters null
    WHILE @i < @ParamCount-1 BEGIN
		SET @SPName = @SPName + ',NULL'
		SET @i = @i + 1
	END

    PRINT @SPName

    EXEC(@SPName)

    FETCH NEXT FROM abc
    INTO @SPName, @ObjectId
END
CLOSE abc
DEALLOCATE abc  

SET NOCOUNT OFF