SELECT 1 AS Value INTO #NoAudit 

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'InsteadOfInsertIntoPatientFinancial' AND type = 'TR')
 BEGIN
     DROP TRIGGER InsteadOfInsertIntoPatientFinancial
 END

EXEC('CREATE TRIGGER InsteadOfInsertIntoPatientFinancial ON dbo.PatientFinancial
INSTEAD OF INSERT
AS 
BEGIN
	INSERT INTO model.InsurancePolicies
	(PolicyCode
	,GroupCode
	,Copay
	,Deductible
	,StartDateTime
	,PolicyHolderPatientId
	,InsurerId
	,MedicareSecondaryReasonCodeId)
	SELECT
	i.FinancialPerson
	,i.FinancialGroupId
	,i.FinancialCopay
	,0
	,CONVERT(DATETIME,i.FinancialStartDate)
	,i.PatientId
	,i.FinancialInsurerId
	,NULL
	FROM Inserted i

	DECLARE @CurrentPatientId INT
	SET @CurrentPatientId = (SELECT PatientId FROM Inserted)

	INSERT INTO model.PatientInsurances
	(InsuranceTypeId
	,PolicyHolderRelationshipTypeId
	,OrdinalId
	,InsuredPatientId
	,InsurancePolicyId
	,EndDateTime
	,IsDeleted)
	SELECT
	CASE i.FinancialInsType
		WHEN ''V''
			THEN 2
		WHEN ''A''
			THEN 3			
		WHEN ''W''
			THEN 4
		ELSE 1
	END
	,CASE (
		CASE 
			WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
				THEN ISNULL(dbo.GetPolicyHolderRelationshipType(p.Id),''Y'')
			ELSE ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p.Id),'''')
			END
		)
		WHEN ''Y''
			THEN 1
		WHEN ''S''
			THEN 2
		WHEN ''C''
			THEN 3
		WHEN ''E''
			THEN 4
		WHEN ''L''
			THEN 6
		WHEN ''O''
			THEN 7
		WHEN ''D''
			THEN 7
		WHEN ''P''
			THEN 7
		ELSE CASE 
				WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
					THEN 1
				WHEN ISNULL(dbo.GetSecondPolicyHolderPatientId(p.Id),0) = i.PatientId
					THEN 1
			ELSE 5
			END
	END
	,CASE i.FinancialInsType
			WHEN ''V''
				THEN 200
			WHEN ''A''
				THEN 3000			
			WHEN ''W''
				THEN 40000
			ELSE 10
			END + CASE 
					WHEN i.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id)
						THEN 0
					ELSE 3
				  END + CONVERT(INT, i.FinancialPIndicator)
	,i.PatientId
	,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = @CurrentPatientId ORDER BY Id DESC)
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(DATETIME,i.FinancialEndDate)
					END
		ELSE NULL
	END
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(BIT, 1)
					END
		ELSE CONVERT(BIT, 0)
	END
	FROM Inserted i
	JOIN model.Patients p ON p.Id = i.PatientId
END
	')





--Post Migration data additions

--Add revenue codes to surgeries
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'66984'
	,1
	,'SURGERY'
	,NULL
	,0
	,0
	,CONVERT(DATETIME, '20000101')
	,1000.00
	,0
	,0
	,'CATARACT SX'
	,2
	,1
	)
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'66982'
	, 1
	,'SURGERY'
	,NULL
	,0
	,0
	,CONVERT(DateTime, '20000101')
	,1000.00
	,0
	,0
	,'CATARACT SURG COMPLEX'
	,2
	,1
	)
	
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'67036'
	,1
	,'SURGERY'
	,NULL
	,0
	,0
	,CONVERT(DateTime, '20000101')
	,1200.00
	,0
	,0
	,'VITRECT'
	,2
	,1
	)
	
INSERT INTO [model].[FacilityEncounterServices] (
	[Code]
	,[DefaultUnit]
	,[Description]
	,[EndDateTime]
	,[IsZeroChargeAllowedOnClaim]
	,[RelativeValueUnit]
	,[StartDateTime]
	,[UnitFee]
	,[IsArchived]
	,[IsCliaCertificateRequiredOnClaim]
	,[UnclassifiedServiceDescription]
	,[ServiceUnitOfMeasurementId]
	,[RevenueCodeId]
	)
VALUES (
	'67028'
	,1
	,'SURGERY'
	,NULL
	,0
	,0
	,CONVERT(DateTime, '20000101')
	,1000.00
	,0
	,0
	,'Intravitreal Injection'
	,2
	,1
	)


DECLARE @PatientReceivableId int
DECLARE @PatientReceivablePaymentBatchCheckId int
IF OBJECT_ID('dbo.PadLeft') IS NULL
BEGIN
EXEC ('

CREATE FUNCTION dbo.PadLeft(
      @String int
     ,@NumChars int
     )
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @PadChar char(1) 
	SET @PadChar = ''0''
    RETURN STUFF(@String, 1, 0, REPLICATE(@PadChar, @NumChars - LEN(@String)))
END
')

END

INSERT INTO model.ServiceModifiers (BillingOrdinalId, Code, [Description], ClinicalOrdinalId, IsArchived)
SELECT 
99 AS BillingOrdinalId
,'24' AS Code
,'Unrelated E M Service by the Same Physician During PostOp' AS [Description]
,99 AS ClinicalOrdinalId
,CONVERT(BIT,0) AS IsArchived

--patient #3 Medicare only
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'ONLY'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #3 MEDICARE ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, '666999888A', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD APPOINTMENT PATIENT #3
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120120,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SRG'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'SOO LIN'

--ADD ACTIVITY RECORD PATIENT #3
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120120, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--ADD CLINICAL DATA AND PRESCRIPTIONS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition], [PostOpPeriod], [Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Y', '', '1', 'COMPREHENSIVE', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'H', '', '/CHIEFCOMPLAINTS', 'CC:FLASHES', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', 'TIME=01:07 PM (SMITH)', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', '*CURRENTEVERYDAY=T14854 <CURRENTEVERYDAY>', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OD', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OS', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OD', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OS', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '5', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '4', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Z', '', '1', '92012', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/LENTICULAR SINGLE VISION-2/ PLANO ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '2', 'DISPENSE CL RX-9/-1/-2/ +2.00 -0.50 X180 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-3/ +0.25 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-4/-5/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-6/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-7/', '^', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #3
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '6374.75'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20120120
	,'6385'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,ec.Id
	,0
	,''
	,''
	,0
	,''
	,''
	,'5940.82'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN model.ExternalContacts ec ON ec.LastNameOrEntityName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')
-- BILLING TRANSACTIONS Patient #3

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/6374.75'
	,'B'
	)


--SERVICES Patient #3
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'67028', 'RT', '1', '250.00', '04', '11'
		,20120120
        , '2', 'A', 'F', 'F', '156.39', '1', 0,
convert(varchar(8),getdate(),112), 'INTRAVITREAL I')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
		
        ,'J3490', '', '5', '1050.00', '12', '11'
		,20120120
        , '1234', 'A', 'F', 'F', '5126.78', '2', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')


--NOTE for Drug Code
UPDATE model.BillingServices
SET UnclassifiedServiceDescription = 'HI VINESH THIS IS A DRUG NOTE'
WHERE Id = (SELECT IDENT_CURRENT('model.BillingServices'))

-- 2d ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--3rd service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '', '1', '225.00', '01', '11'
		,20120120
        , '', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- PatientReceivablePayments Patient #3
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,10.25
	,N'20120101'
	,N'105289455'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--payment for Patient #3
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,10.25
	,N'20120101'
	,N'105289455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20110101'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 3rd ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--4th Service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92235', 'RT', '1', '170.00', '02', '11'
		,20120120
        , '2', 'A', 'T', 'F', '112.45', '4', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 4th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--5th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
			
        ,'92235', 'LT', '1', '170.00', '02', '11'
		,20120120
        , '2', 'A', 'T', 'F', '112.45', '5', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 5th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--6th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92133', '', '1', '120.00', '02', '11'
		,20120120
        , '1', 'A', 'T', 'F', '123.22', '6', 0,
convert(varchar(8),getdate(),112), 'OCT')


-- 6th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--7th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92250', '', '1', '110.00', '02', '11'
		,20120120
        , '1', 'A', 'T', 'F', '98.16', '7', 0,
convert(varchar(8),getdate(),112), 'FUNDUS PHOTOS')


-- 7th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--8th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92083', '', '1', '90.00', '02', '11'
		,20120120
        , '5', 'A', 'T', 'F', '78.81', '8', 0,
convert(varchar(8),getdate(),112), 'VISUAL FIELD')

-- 8th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


---PATIENT #1 AETNA ONLY	
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'AETNA'
	,'ONLY'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)


--PatientFinancial #1 AETNA ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '12345', ''
, 25, 1, '20110201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'AETNA'

--ADD APPOINTMENT PATIENT #1
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111207,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #1
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111207, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #1

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.15', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #1
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '973.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111207
	,'993.00'
	,ResourceId1
	,''
	,''
	,'12/07/2011'
	,'12/07/2011'
	,''
	,''
	,''
	,''
	,''
	,'A'
	,ec.Id
	,0
	,''
	,''
	,0
	,''
	,''
	,'993'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN model.ExternalContacts ec ON ec.LastNameOrEntityName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #1
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/973'
	,'B'
	)


--SERVICES  patient #1
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'99244', '24', '1', '675.00', '01', '11',
			20111207, '', 'A', 'F', 'T', '675.00', '1', 0,
			20111207, 'CONSULTATION')


-- PatientReceivablePayments  patient #1
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--payment from Patient #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual adjustment from Patient #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'X'
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,665
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag]
		  ,[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'92250', '', '1', '318.00', '01', '11',
			20111207, '2', 'A', 'T', 'F', '318.00', '2', 0,
			20111207, 'FUNDUS PHOTOS')
			
-- 2d ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--PATIENT #2 Blue Shield And Spouse's Aetna
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'BLUESHIELD'
	,'AETNA'
	,'SPOUSE'
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,IDENT_CURRENT('model.ExternalContacts')
	,IDENT_CURRENT('model.ExternalContacts')-1
	,''
	,''
	,''
	,0
	,''
	,'N'
	,'N'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MRS'
	,'T'
	,''
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'

--PatientFinancial #2
--ADD INSURANCE PLAN
INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,2
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'))
,NULL
,0

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'YLK098765' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20100101',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'BLUE SHIELD'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT PATIENT #2
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120104,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #2
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120104, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Diagnoses for Patient #2
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '3', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '4', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '550'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20120104
	,'550'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'550'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/550'
	,'B'
	)


--SERVICES
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ( dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'92235', 'RTLT', '2', '200.00', '01', '11',
			20120104, '1', 'A', 'T', 'F', '200.00', '1', 0,
			20120104, 'FLUORESCEIN')

-- 1st ServiceTransaction  patient #2
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'92014', '', '1', '150.00', '01', '11',
			20120104, '', 'A', 'F', 'F', '150.00', '2', 0,
			20120104, 'OFFICE VISIT')

-- 2d ServiceTransaction  patient #2
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Patient #4 Medicaid only
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAID'
	,'ONLY'
	,'G'
	,''
	,''
	,'46699 EASTERN BOULEVARD'
	,''
	,'PHILADELPHIA'
	,'PA'
	,'19019'
	,'2153335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19280105'
	,''
	,'SPANISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #4 medicaid only
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'DEU99008', ''
, 0, 1, '20100711', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICAID'


--ADD APPOINTMENT PATIENT #4
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111122,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	fac.ResourceId, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111122
FROM Resources dr
INNER JOIN Resources fac ON fac.ResourceType = 'R' and fac.ServiceCode = '02' and fac.ResourceName = 'BROOK'
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #4
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111122, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #4
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')

--Precert
INSERT INTO model.PatientInsuranceAuthorizations (
	AuthorizationCode
	,AuthorizationDateTime
	,IsArchived
	,PatientInsuranceId
	,EncounterId
	,Comment
	)
SELECT '9080ABDC'
	,CONVERT(datetime,'20111122', 112)
	,CONVERT(bit, 0)
	,pins.Id
	,IDENT_CURRENT('Appointments')
	,''
FROM model.PatientInsurances pins
INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId
WHERE ins.Name = 'MEDICAID'
	AND pins.InsuredPatientId = IDENT_CURRENT('model.Patients')

UPDATE Appointments 
SET PreCertId = IDENT_CURRENT('model.PatientInsuranceAuthorizations')
WHERE AppointmentId = IDENT_CURRENT('Appointments')


-- PatientReceivables Patient #4
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111122
	,'1500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,ap.ResourceId2
	,''
	,''
	,623.63
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE ap.AppointmentId = IDENT_CURRENT('Appointments')

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #4
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/1500'
	,'B'
	)

--1st service  patient #4
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66984', 'RT', '1', '1500.00', '01', '24',
			20111122, '', 'A', 'F', 'F', '1500.00', '1', 0,
			20111122, 'CATARACT SURGERY')

-- 1st ServiceTransaction  patient #4
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--Patient #5 Medicare/Medicaid
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'MEDICAID'
	,'C'
	,''
	,''
	,'85 RED ROSE STREET'
	,''
	,'ATLANTA'
	,'GA'
	,'10022'
	,'3033335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19151123'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #5 MEDICARE/MEDICAID
--ADD INSURANCE PLAN
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'999663333A' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'345TYU' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110501',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICAID'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,12/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT #1 PATIENT #5
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20110919,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = 'TLC'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #5
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110919, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #5
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables #1 Patient #5/1ST CLAIM
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110919
	,'885.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'885.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

DELETE FROM model.InvoiceReceivables WHERE Id IN (SELECT 
ReceivableId 
FROM PatientReceivables 
WHERE Invoice = dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
AND InsurerId = (SELECT InsurerId FROM PracticeInsurers WHERE InsurerName = 'MEDICAID'))

-- BILLING TRANSACTIONS Patient #5/1ST CLAIM
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110919
	,654
	,'I'
	,'//Amt/885'
	,'B'
	)


--1st service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '', '1', '225.00', '01', '11'
		,20110919
        , '123', 'A', 'F', 'F', '132.56', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--2nd Service Patient #5
INSERT INTO PatientReceivableServices
	([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],
	[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT
dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
,'92235'
,'RT'
,'1'
,'170.00'
,'02'
,'11'
		,20110919
,'2'
,'A'
,'T'
,'F'
,'89.42'
,'2'
,0
,CONVERT(varchar(8),GETDATE(),112)
,'FLUORESCEIN'


-- 2nd ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--3rd service Patient #5/1ST CLAIM
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
			
        ,'92235', 'LT', '1', '170.00', '02', '11'
		,20110919
        , '2', 'A', 'T', 'F', '89.42', '3', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 3rd ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--4th service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
        ,'92133', '', '1', '120.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '123.22', '4', 0,
convert(varchar(8),getdate(),112), 'OCT')


-- 4th ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--5th service Patient #5/1ST CLAIM
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92250', '', '1', '110.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '98.16', '5', 0,
convert(varchar(8),getdate(),112), 'FUNDUS PHOTOS')

-- 5th ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--6th service Patient #5/1ST CLAIM
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92083', '', '1', '90.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '78.81', '6', 0,
convert(varchar(8),getdate(),112), 'VISUAL FIELD')

-- 6th ServiceTransaction Patient #5/1ST CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- Patient #5 2d claim MedicareMedicaid - Medicaid Secondary Test
--ADD APPOINTMENT PATIENT #5/2D CLAIM
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20110715,
	900, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--ADD ACTIVITY RECORD PATIENT #5/2D CLAIM test Medicaid Secondary
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110715, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #5/2D CLAIM Medicaid Secondary
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables  Patient #5/2D CLAIM - Primary Receivable to Medicare (Medicaid Secondary test)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '102.09'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110715
	,'500.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'480.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN model.ExternalContacts ec ON ec.LastNameOrEntityName = 'COX'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #5/2D CLAIM Medicare Primary CLOSED (Medicaid Secondary test)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20110715
	,654
	,'I'
	,'//Amt/500'
	,'B'
	)


--1st service Patient #5/2D CLAIM Medicaid Secondary
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99204', '', '1', '225.00', '01', '11'
		,20110715
        , '12', 'A', 'F', 'F', '125.51', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #5/2D CLAIM TO MEDICARE (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,225
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivablePayments  patient #5/2D CLAIM (Medicaid Secondary test)
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5236.36
	,N'20110821'
	,N'7989'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'MEDICARE'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for Patient #5/2D CLAIM from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,98.56
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5/2D CLAIM from Medicare service #1 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,99.49
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--coinsurance for Patient #5/2D CLAIM from Medicare service #1 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,26.95
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--2nd Service Patient #5/2D CLAIM from MEDICARE (Medicaid Secondary test)
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92235', 'RT', '1', '100.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '89.42', '2', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 2nd Service 1st Transaction Patient #5 /2D CLAIM TO MEDICARE (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivablePayments  patient #5/#6 2nd service
-- payment for Patient #5/2D CLAIM from Medicare service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,67.43
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5/2D CLAIM from Medicare Service #2 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--deductible for Patient #5/2D CLAIM from Medicare Service #2 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,3.58
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--coinsurance for Patient #5/2D CLAIM from Medicare Service #2 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,18.99
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--3rd service Patient #5/2D CLAIM (Medicaid Secondary test)
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
			
        ,'92235', 'LT', '1', '100.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '89.42', '3', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 3rd ServiceTransaction Patient #5/2D CLAIM TO MEDICARE (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivablePayments   3d service (Medicaid Secondary test)
-- payment from Medicare for 3rd Service Patient #5/2D CLAIM
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,57.43
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5/2D CLAIM from Medicare Service #3 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--deductible for Patient #5, 2d claim, from Medicare Service #3 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,13.58
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--coinsurance for Patient #5, second claim, from Medicare Service #3 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,18.99
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--4th service Patient #5/2D CLAIM (Medicaid Secondary test)
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
			
        ,'92015', '', '1', '25.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '0.00', '5', 0,
convert(varchar(8),getdate(),112), 'REFRACT')


-- 4th ServiceTransaction Patient #5/#6 test TO MEDICARE (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,25
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- DENIAL from Medicare for 4th Service Patient #5/Test #6 - MEDICAID 2Y TEST
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'D'
	,5
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'23'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5/Test #6 from Medicare Service #4 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,15
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--COINSURANCE for Patient #5/Test #6 from Medicare Service #4 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,5.00
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--5th service Patient #5/#6 test (Medicaid Secondary test)
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
			
        ,'92020', '', '1', '50.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '30.00', '4', 0,
convert(varchar(8),getdate(),112), 'GONIO')


-- 5th ServiceTransaction Patient #5/#6 test TO MEDICARE (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,50
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivablePayments   5th service (Medicaid Secondary test)
-- payment from Medicare for 5th Service Patient #5/Test #6 
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,20
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5/Test #6 from Medicare Service #5 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,20
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--deductible for Patient #5/Test #6 from Medicare Service #5 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,5.00
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--coinsurance for Patient #5/Test #6 from Medicare Service #5 (Medicaid Secondary test)
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,5
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--Patient Payment for Patient #5/Test #6 from Medicare Service #4 (Medicaid Secondary test) - should be excluded
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,PatientId
	,N'P'
	,N'='
	,5
	,N'20110715'
	,N'4567'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PatientDemographics WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,PatientId
	,N'P'
	,N'P'
	,5
	,N'20110715'
	,N'4567'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20110715'
FROM PatientReceivableServices prs
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
WHERE prs.ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivables Patient #5/6th test - Medicaid (Medicaid Secondary test)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '102.09'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110715
	,'500.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'480.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS TO MEDICAID Patient #5/Test #6 (medicaid secondary test)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110815
	,654
	,'I'
	,'//Amt/102.09'
	,'B'
	)
	
-- 1st Service, 2d Transaction Patient #5/#6 test - TO Medicaid (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110815
	,26.95
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '99204'
AND InsurerName = 'MEDICAID'

-- 2nd Service 2nd Transaction Patient #5 Test #6 TO MEDICAID (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,22.57
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92235' and Modifier = 'RT'
AND InsurerName = 'MEDICAID'


-- 3rd Service 2D Transaction Patient #5/#6 test TO MEDICAID (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,32.57
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92235' and Modifier = 'LT'
AND InsurerName = 'MEDICAID'


-- 4TH Service 2D Transaction Patient #5/#6 test TO MEDICAID (Medicaid Secondary test)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,10
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92015'
AND InsurerName = 'MEDICAID'

-- 5th Service 2D Transaction Patient #5/#6 test TO MEDICAID (Medicaid Secondary test) 
-- IN WAIT STATUS!!! SHOULD NOT APPEAR IN CLAIM FILE
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,10.00
	,'V'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92020' 
AND InsurerName = 'MEDICAID'

INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')-1
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments')-1,5)
	,20110919
	,'885.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'885.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments')-1 = ap.AppointmentId


----------PATIENT #6 ----
--Patient #6 Aetna spouse/cigna self
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'AETNASPOUSEPRIMARY'
	,'CIGNASELFSECONDARY'
	,''
	,''
	,''
	,'448 STATE STREET'
	,''
	,'BROOKLYN'
	,'NY'
	,'12117'
	,'7188750653'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19550715'
	,''
	,'FRENCH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'HISP'

--PatientFinancial #6 AETNA SPOUSE IS PRIMARY; THIS POLICY IS THE CIGNA SECONDARY SELF
--ADD INSURANCE PLAN

INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,2
,11
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'))
,NULL
,0

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'CIGNAPOLICY' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20101201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'CIGNA'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,14/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--Add referral
INSERT INTO model.PatientInsuranceReferrals
           ([PatientId]
           ,[ReferralCode]
           ,[ExternalProviderId]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[TotalEncountersCovered]
           ,[Comment]
           ,[DoctorId]
           ,[PatientInsuranceId]
           ,[IsArchived])
SELECT IDENT_CURRENT('model.Patients'),
	'referral4567',
	pv.Id,
	CONVERT(datetime,'20111201',112),
	CONVERT(datetime,'20121201',112),
	6,
	'',
	ResourceId,
	pi.Id,
	CONVERT(bit, 0)
FROM model.ExternalContacts pv 
INNER JOIN Resources re ON ResourceType = 'D' and ResourceLastName = 'Reiss'
INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = IDENT_CURRENT('model.Patients')
	AND pi.IsDeleted = 0
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.Insurers pri ON Name = 'AETNA'
WHERE pv.LastNameOrEntityName = 'COX'

--ADD APPOINTMENT PATIENT #6
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120107,
	700, 10, IDENT_CURRENT('model.PatientInsuranceReferrals'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120201
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'NPG'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

INSERT INTO model.EncounterPatientInsuranceReferral (Encounters_Id, PatientInsuranceReferrals_Id)
SELECT IDENT_CURRENT('dbo.Appointments') AS Encounters_Id, IDENT_CURRENT('model.PatientInsuranceReferrals')

--ADD ACTIVITY RECORD PATIENT #6
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120107, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #6
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '365.10', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #6
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20120107
	,'885.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,(SELECT TOP 1 Id FROM model.ExternalContacts ec WHERE ec.LastNameOrEntityName = 'REFERRAL' AND ec.FirstName = 'SELF')
	,0
	,''
	,''
	,0
	,''
	,''
	,'775.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON LastName = 'AETNA' and FirstName = 'ONLY'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #6
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120107
	,654
	,'I'
	,'//Amt/375'
	,'B'
	)


--1st service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'67028', 'RT', '1', '375', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 1st ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '2459RT', '1', '100.00', '01', '11'
		,20120107
        , '213', 'A', 'F', 'F', '100', '2', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')

-- 2ND ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- 3rD Service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'25999', '', '1', '100', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '100', '3', 0,
convert(varchar(8),getdate(),112), 'NOS DRUG')


--NOTE for NOS Code
UPDATE model.BillingServices
SET UnclassifiedServiceDescription = 'HI VINESH THIS IS AN NOS NOTE'
WHERE Id = (SELECT IDENT_CURRENT('model.BillingServices'))

-- 3rd ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-------Patient #6 Test Paper Claim
--ADD APPOINTMENT PATIENT #6
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110204,
	700, 10, IDENT_CURRENT('model.PatientInsuranceReferrals'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110204
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #6
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110204, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #6 Paper Claim Test
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #6 Paper Claim Test - primary (aetna)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '100'
	,'B'
	,pdPatient.PatientId
	,pdInsured.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110204
	,'150.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'150.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdInsured ON pdInsured.LastName = 'AETNA' and pdInsured.FirstName = 'ONLY'
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'AETNASPOUSEPRIMARY' and pdPatient.FirstName = 'CIGNASELFSECONDARY'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #6 Paper Claim Test (AETNA PRIMARY CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20110204
	,654
	,'I'
	,'//Amt/150'
	,'B'
	)


--1st service Patient #6 Paper Claim Test
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '', '1', '150.00', '01', '11'
		,20110204
        , '1', 'A', 'F', 'F', '150', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'1'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- PatientReceivablePayments   #6 Paper Claim Test
-- payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,40
	,N'20110304'
	,N'57689030405430'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,0
	,N''
	,N'20110227'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for   #6 Paper Claim Test
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110304'
	,N'57689030405430'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,0
	,N''
	,N'20110227'
	FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-- PatientReceivables Patient #6 Paper Claim Test - secondary
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '100'
	,'B'
	,pdPatient.PatientId
	,pdPatient.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110204
	,'150.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'150.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'AETNASPOUSEPRIMARY' and pdPatient.FirstName = 'CIGNASELFSECONDARY'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

--	SECONDARY (CIGNA) BILLING TRANSACTION Patient #6 Paper Claim Test
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110404
	,654
	,'I'
	,'//Amt/100'
	,'P'
	)


-- 2dy ServiceTransaction Patient #6 Paper Claim Test
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110404
	,100
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--------End Patient #6 Test Paper Claim

--patient #7 MedicarePri/CignaSec
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAREPRI'
	,'CIGNASEC'
	,'W'
	,''
	,''
	,'85 LIME STREET'
	,''
	,'BOSTON'
	,'MA'
	,'10022'
	,'6173335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'M'
	,'19450905'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
	)

--PatientFinancial #7 MEDICARE PRIMARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '866663333A', ''
, 0, 1, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--PatientFinancial #7 CIGNA  SECONDARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGNAPOLICY2', ''
, 0, 2, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'

--PATIENT #8 /Test #7 BlueShield Self, Medicare/Cigna Spouse 
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[BillParty]
	,[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'BLUESHIELDSELF'
	,'CIGNASPOUSE'
	,'WILLIAM'
	,''
	,''
	,'85 LIME STREET','','BOSTON','MA','10022'
	,'6173335544'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19441123',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,'N'
	,'N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'

--PATIENT #8 /Test #7  BLUESHIELD SELF/CIGNA SPOUSE
--ADD INSURANCE PLAN
INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,2
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'MEDICAREPRI' AND FirstName = 'CIGNASEC') ORDER BY Id DESC)
,NULL
,0

--INSERT INTO model.PatientInsurances
--(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
--SELECT 
--1
--,2
--,15
--,IDENT_CURRENT('model.Patients')
--,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'MEDICAREPRI' AND FirstName = 'CIGNASEC') ORDER BY Id ASC)
--,NULL
--,0

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'YLK9663333' /*policy number*/
,'BLUEGROUP444' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110302',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'BLUE SHIELD'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT #1 PATIENT #8 /Test #7 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20110302,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #8 /Test #7 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110302, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT #8 /Test #7 

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables PATIENT #8 /Test #7 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110302
	,'500.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'500.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS PATIENT #8 /Test #7 
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110302
	,654
	,'I'
	,'//Amt/500'
	,'B'
	)


--1st service PATIENT #8 /Test #7 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '', '1', '225.00', '01', '11'
		,20110302
        , '12', 'A', 'F', 'F', '132.56', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction PATIENT #8 /Test #7 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--2nd Service PATIENT #8 /Test #7 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92235', 'RT', '1', '175.00', '02', '11'
		,20110302
        , '2', 'A', 'T', 'F', '89.42', '2', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 2nd ServiceTransaction PATIENT #8 /Test #7 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


----PATIENT #9 VISIONPLAN ONLY USED FOR THE TEST #8, next
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'VISIONPLAN'
	,'ONLY'
	,''
	,''
	,''
	,'788 CRANBERRY STREET','','BURLINGTON','MA','01805'
	,'6173335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'M'
	,'19471015'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'N'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
	)

--PatientFinancial #9 VISIONPLAN ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'VIZ12345', ''
, 0, 1, '20050101', '', 'C', 'V'
FROM PracticeInsurers where InsurerName = 'VSP'

DECLARE @nextPatientId INT
SET @nextPatientId = IDENT_CURRENT('model.Patients') + 1

----PATIENT #10/Test #8 BlueShield self/Vision spouse
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[BillParty]
	,[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'BLUESHIELDSELF'
	,'VISIONPLANSPOUSE'
	,''
	,''
	,''
	,'788 CRANBERRY STREET','','BURLINGTON','MA','01803'
	,'6173335544'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19480116',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,'N'
	,'N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--PatientFinancial PATIENT #10/Test #8 BLUESHIELD SELF/Vision SPOUSE
--ADD INSURANCE PLAN
INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
2
,2
,201
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'))
,NULL
,0

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'YLK9663333' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20050302',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'BLUE SHIELD'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,14/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT #1 PATIENT #10/Test #8 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20101225,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	sl.ResourceId, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
INNER JOIN Resources sl on sl.ResourceType = 'R' and sl.ResourceName = 'BROOK'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #10/Test #8 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20101225, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT #10/Test #8 

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables PATIENT #10/Test #8 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1200.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20101225
	,'1200.00'
	,ResourceId
	,'A'
	,'CT'
	,'12/24/2010'
	,'12/31/2010'
	,''
	,''
	,''
	,'12/25/2010'
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'1200.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
INNER JOIN Resources re ON ResourceType = 'D' 
	AND ResourceLastName = 'Reiss'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS PATIENT #10/Test #8 
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20101225
	,654
	,'I'
	,'//Amt/1200'
	,'B'
	)


--1st service Patient #9
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (
dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
,'67036'--[Service]
,'RT'--[Modifier]
,'1'
,'1200.00'--[Charge]
,'02'
,'24'
		,20110302
,'12'
,'A'
,'F'
,'F'
,'1032.56'
,'1'
,0
,CONVERT(VARCHAR(8),GETDATE(),112)
,'OFFICE VISIT'
)

UPDATE PatientReceivableServices
SET PlaceOfService = '21'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st ServiceTransaction Patient #9
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-------Patient #11/Test 9 CignaPrimary/MedicarePrimaryArchived/AetnaSecondary--------
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[BillParty]
	,[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'CIGNAPRIMARY'
	,'MEDICAREARCHIVED'
	,'AETNASECONDARY'
	,''
	,''
	,'55669 ORANGE STREET','','GLOUCESTER','MA','01803'
	,'6173856644'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19390123',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,'N'
	,'N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'

--ADD INSURANCE PLAN - MEDICARE ARCHIVED Patient #11/Test 9
INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,2
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'))
,NULL
,0

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'012336655A' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20000101',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL
,0 /*is deleted*/

--ADD INSURANCE PLAN - VSP VISION CURRENT Patient #11/Test 9
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'VSPPOL567' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20100902',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'VSP'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
2/*vis*/
,1/*yourself*/
,201/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT Patient #11/Test 9
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20100902,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20090302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
WHERE ResourceType = 'D' and ResourceLastName = 'Morgenstern'

--ADD ACTIVITY RECORD Patient #11/Test 9
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20100902, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT Patient #11/Test 9
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.15', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #11/Test 9 - MEDICARE ARCHIVED
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTION Patient #11/Test 9 ARCHIVED MEDICARE
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20100902
	,654
	,'I'
	,'//Amt/3130'
	,'B'
	)


-- PatientReceivables Patient #11/Test 9 - AETNA 2D
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN PatientDemographics pd ON LastName = 'AETNA' AND FirstName = 'ONLY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #11/Test 9 AETNA SECOND
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20100902
	,654
	,'I'
	,'//Amt/0'
	,'B'
	)

UPDATE pi SET EndDateTime = CONVERT(DATETIME,'20100901',112)/*EndDateTime*/
FROM model.PatientInsurances pi
INNER JOIN PatientDemographics pd ON pd.PatientId = pi.InsuredPatientId
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
WHERE ip.PolicyCode = '012336655A'
	AND pd.FirstName = 'MEDICAREARCHIVED'

--Receivable is no longer primary
UPDATE PatientReceivables SET InsurerDesignation = 'F'
WHERE ReceivableId IN (
SELECT ReceivableId 
FROM PatientReceivables pr
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND pd.FirstName = 'MEDICAREARCHIVED'
	AND pri.InsurerName = 'MEDICARE'
)

--ADD INSURANCE PLAN - CIGNA CURRENT Patient #11/Test 9
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'CIGPOL123' /*policy number*/
,'' /*group code*/
,'0' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20100902',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'CIGNA'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*medical*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

-- PatientReceivables Patient #11/Test 9  -- CIGNA PRIMARY
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #11/Test 9 CIGNA PRIMARY
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'P'
	,20100903
	,654
	,'I'
	,'//Amt/1130'
	,'B'
	)


-- PatientReceivables Patient #11/Test 9 - vsp vision primary for drug service
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'VSP'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTION Patient #11/Test 9 vsp primary for drug
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'P'
	,20100903
	,654
	,'I'
	,'//Amt/2000'
	,'B'
	)


--1st service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99214', '', '1', '225.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'2'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2nd service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn]
		  ,[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'J2778', '', '5', '1800.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '1312.88', '1', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')


-- 2ND Service 1st ServiceTransaction Patient #11/Test 9 (medicare archived)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- 2nd Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'2'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName =  'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- 2d Service 3rd ServiceTransaction Patient #11/Test 9  (VSP pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'VSP'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-- 3RD service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'67028', 'RT', '1', '725.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '598.24', '2', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')



-- 3rd Service #1 ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 3rd Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'2'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 3rd Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE [Service] = '67028' 
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'



-- 4TH service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92235', 'RT', '1', '90.00', '01', '11'
		,20100902
        , '1', 'A', 'T', 'F', '89.5', '4', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')

-- 4th ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 4th Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'2'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 4th Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'


-- 5TH service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92235', 'LT', '1', '90.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '89.5', '5', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')

-- 5th ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 5th Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'2'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 5th Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'


-----patient #12 MedicareReportPatientPaid Test #11
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'REPORTPATIENTPAID'
	,'B'
	,''
	,''
	,'345 GROVE STREET'
	,''
	,'ALPINE'
	,'NY'
	,'10022'
	,'2129935544'
	,'7183269955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'S'
	,'19660503'
	,''
	,'KOREAN'
	,''
	,''
	,''
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'N'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #12 MEDICARE ONLY  Test #11
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, 'A223999888', ''
, 25, 1, '20100501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE RAILROAD'

--ADD APPOINTMENT #1 PATIENT #12  Test #11
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110616,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'REISS'

--ADD ACTIVITY RECORD #1 PATIENT #12  Test #11
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110616, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add diagnoses
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '3', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables #1 Patient #12  Test #11
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '0'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110616
	,'350'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'350'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE RAILROAD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #12  Test #11  1ST appt

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110616
	,654
	,'I'
	,'//Amt/0'
	,'B'
	)


--SERVICES Patient #12  Test #11  1ST appt
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92014', '', '1', '250.00', '01', '11'
		,20110616
        , '12', 'A', 'F', 'F', '250', '1', 0
		,convert(varchar(8),getdate(),112), 'OFFICE VIZ')


--payment from Patient #12 1ST APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,250
	,N'20110616'
	,N'887766'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110616'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- 1st ServiceTransactions   Patient #12, 1ST appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110616
	,0
	,'B'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2ND service   Patient #12, 1ST appt
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92015', '', '1', '100.00', '01', '11'
		,20110616
        , '', 'A', 'F', 'F', '100', '2', 0
		,convert(varchar(8),getdate(),112), 'REFRACT')


--payment from Patient #12 1ST APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,100
	,N'20110616'
	,N'887766'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110616'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 2ND ServiceTransactions  Patient #12, 1ST appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110616
	,0
	,'B'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--ADD APPOINTMENT #2 PATIENT #12 - Test #11  SMALL BALANCE AFTER PATIENT PAID
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110214,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'REISS'

--ADD ACTIVITY RECORD #1 PATIENT #12 2nd appt
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110214, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add diagnoses  Patient #12, Test #11 2nd appt
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #12  Test #11  2nd VISIT
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '0.05'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110214
	,'350'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'350'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE RAILROAD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- 2nd appt BILLING TRANSACTIONS Patient #12

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110214
	,654
	,'I'
	,'//Amt/0.05'
	,'B'
	)


--SERVICES 2nd appt Patient #12
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92014', '', '1', '250.03', '01', '11'
		,20110214
        , '12', 'A', 'F', 'F', '250.03', '1', 0
		,convert(varchar(8),getdate(),112), 'OFFICE VIS')

--payment from Patient #12 2ND APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,250
	,N'20110214'
	,N'554433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110214'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110214
	,0.03
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '92014' and ServiceDate = 20110214

--2ND service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'92015', '', '1', '100.02', '01', '11'
		,20110214
        , '', 'A', 'F', 'F', '100.02', '2', 0
		,convert(varchar(8),getdate(),112), 'REFRACTION')

--payment 2nd service  Patient #12, 2nd appt
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,100
	,N'20110214'
	,N'554433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110214'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 2ND ServiceTransactions Patient #12, 2nd appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110214
	,0.02
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '92015' and ServiceDate = 20110214


DECLARE @PatientIdOfPatientNumFive INT
SET @PatientIdOfPatientNumFive = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID' AND Address = '85 RED ROSE STREET')

---- Patient #5 Test #12 MedicareMedicaid - Medicaid Secondary Resubmit disability
--ADD APPOINTMENT PATIENT #5, test #12
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20111115,
	900, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE ResourceType = 'D' and ResourceLastName = 'REISS'



--ADD ACTIVITY RECORD PATIENT #5/test #12
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20111115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'


--Add Diagnoses Patient #5/test #12
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdOfPatientNumFive, 'B', 'OS', '1', '362.04', '', ''
, 'A', '', '', '', '', 0, '', '')


-- PatientReceivables  Patient #5/test #12  (Medicare primary)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '24.55'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(ap.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,'ICN NUMBER'
	,'3'
	,0
	,''
	,''
	,'100.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #5/Test #12 - Closed
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20111115
	,654
	,'I'
	,'//Amt/100'
	,'B'
	)

-- PatientReceivables Patient #5 for Medicaid Secondary
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '24.55'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(ap.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'100.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #5 MEDICAID SECONDARY (CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20111215
	,654
	,'I'
	,'//Amt/12.55'
	,'B'
	)
	
--1st service Patient #5 Test #12
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(@PatientIdOfPatientNumFive,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

        ,'99203', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')

-- 1st ServiceTransaction Patient #5/Test #12 TO MEDICARE PRIMARY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICARE'


-- PatientReceivablePayments  patient #5 Test #12
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5125
	,N'20111215'
	,N'7892'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111207'
FROM PracticeInsurers WHERE InsurerName = 'MEDICARE'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for Patient #5/Test #12
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId-1
	,InsurerId
	,N'I'
	,N'P'
	,50
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--contractual writeoff for Patient #5
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId-1
	,InsurerId
	,N'I'
	,N'X'
	,25
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--coinsurance for Patient #5/Test #12 from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId-1
	,InsurerId
	,N'I'
	,N'|'
	,24.55
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- 2ND ServiceTransaction Patient #5/Test #12 TO MEDICAID SECONDARY [CLOSED]
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20111215
	,20.00
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND InsurerName = 'MEDICAID'
AND ptj.TransactionRemark = '//Amt/12.55'

-- PAYMENT FROM MEDICAID  Patient #5/Test #12 from Medicare service #1
--Batch
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,20
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

--payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,20
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,PaymentId
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PatientReceivablePayments ON PaymentAmount = 20 
	AND PaymentCheck = '33BB'
	AND PaymentType = '='
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-- ANOTHER PAYMENT FROM MEDICARE - Patient #5/Test #12
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pri.InsurerId
	,N'I'
	,N'P'
	,.45
	,N'20120105'
	,N'care02'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20120101'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers pri ON InsurerName = 'MEDICARE'
INNER JOIN PatientDemographics pd ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN PatientReceivables pr ON pr.InsurerId = pri.InsurerId
	AND InvoiceDate = 20111115
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


-- SECOND BILLING TRANSACTIONS Patient #5 MEDICAID SECONDARY - RESUBMIT
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'P'
	,20120105
	,654
	,'I'
	,'//Amt/24.55'
	,'B'
	)
	
-- SERVICE TRANSACTION #3 TO MEDICAID -- RESUBMIT
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120105
	,4.55
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

----Patient #13 THREE INSURERS, override, MEDICARE SECONDARY
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory],[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'INSURERS'
	,'THREE'
	,''
	,''
	,''
	,'4556 BROADWAY'
	,'APT 26'
	,'NEW YORK'
	,'NY'
	,'10024'
	,'2123444488'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19800731'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,'N'
	,'N'
	,''
	,'Y'
	,'N'
	,'N'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,'12'
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
FROM PatientDemographics
WHERE LastName = 'AETNA' AND FirstName = 'ONLY'

--PatientFinancial 1st Test  Patient #13 THREE INSURERS
--ADD INSURANCE PLAN (MEDICARE SECONDARY)
INSERT INTO model.PatientInsurances
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1
,2
,14
,IDENT_CURRENT('model.Patients')
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = (SELECT PatientId FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'))
,NULL
,0

--ADD INSURANCE PLAN (SECURE HORIZONS PRIMARY)
INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'SECURE789' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20110301',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,NULL
FROM PracticeInsurers
WHERE InsurerName = 'SECURE HORIZONS'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*vis*/
,1/*yourself*/
,11/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

INSERT INTO model.InsurancePolicies
(PolicyCode,GroupCode,Copay,Deductible,StartDateTime,PolicyHolderPatientId,InsurerId,MedicareSecondaryReasonCodeId)
SELECT
'111223333A' /*policy number*/
,'' /*group code*/
,'25' /*copay */
,'0' /*deductible*/
,CONVERT(DATETIME,'20111201',112) /*startdate*/
,IDENT_CURRENT('model.Patients') /*policyholder patientid*/
,InsurerId
,1
FROM PracticeInsurers
WHERE InsurerName = 'MEDICARE'

INSERT INTO model.PatientInsurances 
(InsuranceTypeId, PolicyHolderRelationshipTypeId, OrdinalId, InsuredPatientId, InsurancePolicyId, EndDateTime, IsDeleted)
SELECT 
1/*vis*/
,1/*yourself*/
,12/*ordinalId*/
,IDENT_CURRENT('model.Patients')/*insured Patient Id*/
,(SELECT TOP 1 Id FROM model.InsurancePolicies 
WHERE PolicyHolderPatientId = IDENT_CURRENT('model.Patients') ORDER BY Id DESC)/*insurance policy id*/
,NULL/*EndDateTime*/
,0 /*is deleted*/

--ADD APPOINTMENT PATIENT #13 , 1st Test  THREE INSURERS
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111027,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111007
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #13, 1st Test   THREE INSURERS
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111027, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #13, Test #13  THREE INSURERS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #13, 1st Test   THREE INSURERS, SECURE HORIZONS
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '225.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111027
	,'225.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'225'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'SECURE HORIZONS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #13 THREE INSURERS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20111027
	,654
	,'I'
	,'//Amt/225'
	,'B'
	)


--SERVICES  patient #13, tEST #13 THREE INSURERS
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'92014', '', '1', '125.00', '02', '11',
			20111027, '', 'A', 'F', 'F', '125.00', '1', 0,
			20111027, 'OFFICE VISIT')

--1ST service ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111027
	,100
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'92083', '', '1', '100.00', '02', '11',
			20111027, '', 'A', 'T', 'F', '100.00', '2', 0,
			20111027, 'VISUAL FIELD')

--2nd service ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111027
	,125
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')





---Patient 13 THREE INSURERS 2nd appt MEDICARE SECONDARY
--ADD APPOINTMENT PATIENT #13 , 2nd test THREE INSURERS
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111230,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111230
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Morgenstern'

--ADD ACTIVITY RECORD PATIENT #13,  THREE INSURERS
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111230, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #13, Test #1  THREE INSURERS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #13, Test #1  THREE INSURERS, SECURE HORIZONS
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '16.44'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111230
	,'85.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'SECURE HORIZONS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #13 THREE INSURERS (SECURE HORIZONS, CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20111230
	,800
	,'I'
	,'//Amt/85'
	,'B'
	)


--SERVICES  patient #13, THREE INSURERS
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'92250', '', '1', '85.00', '02', '11',
			20111230, '', 'A', 'T', 'F', '85.00', '1', 0,
			20111230, 'FUNDUS PHOTOS')

--1ST service transaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111230
	,85
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



-- PatientReceivablePayments  patient #13 2d test
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,56.33
	,N'20120105'
	,N'8899'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'SECURE HORIZONS'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for Patient #13 2d test from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'P'
	,24.34
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230


--contractual writeoff for Patient #13/Test #14 from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'X'
	,44.22
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230

--coinsurance for Patient #13/Test #14 from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'|'
	,12.16
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230


--deductible for Patient #13/2nd test from secure horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'!'
	,4.28
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230

-- PatientReceivables Patient #13, 2nd appt  THREE INSURERS, MEDICARE SECONDARY
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '16.44'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20111230
	,'85'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #13 THREE INSURERS (MEDICARE SECONDARY)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'P'
	,20120110
	,800
	,'I'
	,'//Amt/16.44'
	,'B'
	)


--1ST service transaction Medicare Secondary
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120110
	,16.44
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

----Test #14 VISION CLAIM - USING PATIENT "ONLY VISIONPLAN"
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20120115,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId AS ResourceId1, pn.PracticeId + 1000 AS ResourceId2, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111130
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PatientDemographics ON LastName = 'VISIONPLAN' AND FirstName = 'ONLY'
INNER JOIN PracticeName pn ON LocationReference = 'OPTICAL' and PracticeType = 'P'
WHERE ResourceType = 'Q' and ResourceLastName = 'BARTELS'


--ADD ACTIVITY RECORD test #14,  VISION CLAIM
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20120115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:45 AM', 3, 'N', 0, 0, '', '', '', 938 
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--Add Diagnoses  Test #14 VISION CLAIM
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--Add glasses prescription  Test #14 VISION CLAIM
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/SINGLE VISION-2/ -5.25 ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

-- PatientReceivables Test #14 VISION CLAIM
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '620'
	,'B'
	,PatientId
	,PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(IDENT_CURRENT('model.Patients'),5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20120115
	,'620.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,ec.Id
	,0
	,''
	,''
	,0
	,''
	,''
	,'620'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'VSP'
INNER JOIN model.ExternalContacts ec ON ec.LastNameOrEntityName = 'BARTELS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

--Test #14 Vision Claim Add Glasses Order
INSERT INTO CLOrders
           ([InventoryId]
           ,[PatientId]
           ,[AppointmentId]
           ,[ShipTo]
           ,[ShipAddress1]
           ,[ShipAddress2]
           ,[ShipCity]
           ,[ShipState]
           ,[ShipZip]
           ,[Eye]
           ,[Qty]
           ,[Cost]
           ,[OrderDate]
           ,[OrderComplete]
           ,[Status]
           ,[OrderType]
           ,[ReceivableId]
           ,[Reference]
           ,[PONumber]
           ,[AlternateReference]
           ,[Rx])
SELECT IDENT_CURRENT('PCInventory'),PatientId, IDENT_CURRENT('Appointments'), 'O', '&ODC:V2750F,&ODM:V2784&ODP:V2715A&ODB:V2781AG&ODT:PROG&', '&OSC:V2750F,&OSM:V2784&OSP:V2715A&OSB:V2781AG&OST:PROG&'
	, 'OD:33.0&&18&OS:33.5&&18&' , '10', '&01&&&&54&17&130&', 'OU', '1', 500, 20120115, '', 10, 'G', @PatientReceivableId, '', '', '', ''
FROM PatientDemographics  WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'


-- BILLING TRANSACTIONS TEST #14 VISION CLAIM
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120115
	,790
	,'I'
	,'//Amt/620'
	,'B'
	)
	
--SERVICES  TEST #14 VISION CLAIM
--1st service
DECLARE @VISIONPLANONLYPatientId INT

SET @VISIONPLANONLYPatientId = (SELECT TOP 1 Id FROM model.Patients WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY')

INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2020', '', '1', '250.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '250.00', '1', 0,
			20120115, 'FRAMES')

--1ST service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,250
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2D service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2781AG', 'RTLT', '2', '75.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '150.00', '2', 0,
			20120115, 'THREE RIVERS')

--2D service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,150
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--3rd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2750F', 'RTLT', '2', '25.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '50.00', '4', 0,
			20120115, 'ANTIREFLECT COAT')

--3D service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,50
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--4th service DELETED
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2715A', 'RT', '1', '65.00', '12', '12',
			20120115, '', 'X', 'F', 'F', '65.00', '4', 0,
			20120115, 'TRANSITIONS BR')


--4th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2715A', 'RTLT', '2', '65.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '130.00', '3', 0,
			20120115, 'TRANSITIONS BR')


-- 4TH service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,130
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--5TH SERVICE
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     (dbo.PadLeft(@VISIONPLANONLYPatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
				
          ,'V2784', '', '1', '40.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '40.00', '5', 0,
			20120115, 'High Index 1.6')

--5TH service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,40
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--TEST #16
------Patient #14 - Cigna Pri, Medicare Sec
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[BillParty]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'CIGNAPRIMARY'
	,'MEDICARESECONDARY'
	,''
	,''
	,''
	,'98-15 EAST 23RD ROAD'
	,'GARDEN LEVEL'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'7186754433'
	,'7186669932'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19230819'
	,''
	,'ENGLISH'
	,''
	,''
	,ec.Id
	,''
	,''
	,''
	,'N'
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'NOT HISP'
FROM model.ExternalContacts ec WHERE ec.LastNameOrEntityName = 'REFERRAL' AND ec.FirstName = 'SELF'

--PatientFinancial #14 CIGNA PRI MEDICARE SEC
--ADD INSURANCE PLAN #1
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT PatientId, InsurerID, 'CIGNAPRI567', ''
, 25, 1, '20090501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'CIGNA'

INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, '666999888A', ''
, 25, 2, '20090501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'MEDICARE'

INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, 'bakersplan', ''
, 10, 3, '20110501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'LOCAL 104'

--ADD APPOINTMENT PATIENT #14 TEST #16
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110819,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'SOO LIN'

--ADD ACTIVITY RECORD PATIENT #14
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110819, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'

--ADD Diagnosis patient #14
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


--patient #14 TEST #16
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '150'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110819
	,'6385'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,ec.Id
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
INNER JOIN model.ExternalContacts ec ON ec.LastNameOrEntityName = 'REFERRAL' AND ec.FirstName = 'SELF'
INNER JOIN PatientDemographics pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS Patient #14

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20120403
	,654
	,'I'
	,'//Amt/250.00'
	,'B'
	)


--SERVICES Patient #14
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(AppointmentId,5)

        ,'67028', 'LT', '1', '250.00', '04', '11'
		,20110819
        , '1', 'A', 'F', 'F', '85', '1', 0,
		convert(varchar(8),getdate(),112), 'INTRAVITREAL I'
FROM PatientDemographics pd
INNER JOIN Appointments ap ON ap.PatientId = pd.PatientId AND AppDate = 20110819
WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'


-- 1st ServiceTransactions #14
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120403
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



----TEST #17 ASC CLAIMS FACILITY CLAIMS
-------INSTITUTIONAL------
--Surgery Appointment WITH DOCTOR for Patient #2 (THIS WILL BE THE ENCOUNTERID)
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

DECLARE @PatientIdForPatientTwo INT
SET @PatientIdForPatientTwo = (SELECT PatientId FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE')

--ADD ACTIVITY RECORD PATIENT #2  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110504, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, (SELECT ResourceId2 FROM dbo.Appointments WHERE AppointmentId = IDENT_CURRENT('Appointments')), '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'



--Diagnoses for Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OU', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66982', 'LT', '1', '1000.00', '02', '24',
			20110504, '123', 'A', 'F', 'F', '665.87', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Patient #2  Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForPatientTwo, 'B', 'OU', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2 Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66982', 'LT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '665.87', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

-----------TEST #17 FACILITY CLAIM PROFESSIONAL - MEDICARE
---DOCTOR'S CLAIM
DECLARE @PatientIdForMedicareOnly INT
SET @PatientIdForMedicareOnly = (SELECT PatientId FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY')

INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD PATIENT #1  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110504, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, (SELECT ResourceId2 FROM dbo.Appointments WHERE AppointmentId = IDENT_CURRENT('Appointments')), '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 



--Diagnoses for Patient #1  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForMedicareOnly, 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #1  Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '660.22', '1', 0,
			20110506, 'CATARACT SX'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')



--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Patient #1  Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), @PatientIdForMedicareOnly, 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #1 Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #1  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '703.96', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

----TEST #18 ASC FACILITY SECONDARY CLAIMS 
-------INSTITUTIONAL------
--1st example
--Surgery Appointment WITH DOCTOR for Patient #2 BLUESHIELD, AETNA SPOUSE (THIS WILL BE THE ENCOUNTERID)
SET @PatientIdForPatientTwo = (SELECT PatientId FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE')

INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100503,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20100207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD PATIENT #2  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20100503, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, (SELECT ResourceId2 FROM dbo.Appointments WHERE AppointmentId = IDENT_CURRENT('Appointments')), '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'



--Diagnoses for Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'


-- PatientReceivables Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '225.64'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100503
	,'5000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'2665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')


-- BILLING TRANSACTIONS patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20100506
	,654
	,'I'
	,'//Amt/5000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'66984', 'RT', '1', '2300.00', '02', '24',
			20100503, '123', 'A', 'F', 'F', '1900', '2', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100506
	,2300
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--SERVICES  Surgery Claim for Doctor (TEST #18 SECONDARY CLAIM)
--2ND service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
          ,'67036', 'RT', '1', '2700.00', '02', '24',
			20100503, '123', 'A', 'F', 'F', '765.87', '1', 0,
			20100506, 'VITRECT'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 2nd ServiceTransaction  patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--TEST #18- PRI PAYMENTS FOR DR CLAIM /surgery
-- PatientReceivablePayments - primary payment for TEST #18  
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,2430.23
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'BLUE SHIELD'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,1786.46
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503

--contractual writeoff for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,400
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503


--coinsurance for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,113.54
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503



-- payment for TEST #18 Doctor claim 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,643.77
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503



--contractual writeoff for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,1934.13
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503


--deductible for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,122.1
	,N'20100516'
	,N'BS334455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503

--Test #18 facility claims - RECEIVABLE FOR 2Y DR CLAIM
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '235.64'
	,'B'
	,pd.PatientId
	,pdInsurer.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100503
	,'5000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'2665.87'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PatientDemographics pdInsurer ON pdInsurer.Lastname = 'AETNA' AND pdInsurer.FirstName = 'ONLY'
INNER JOIN PracticeInsurers ON InsurerName = 'aetna'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20100516
	,654
	,'I'
	,'//Amt/235.64'
	,'B'
	)

-- TEST #18 1st ServiceTransaction for secondary claim (patient #2)  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100516
	,113.54
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '66984' and modifier = 'RT' and servicedate = '20100503'



--  TEST #18 2nd ServiceTransaction for secondary claim (patient #2)  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100516
	,122.1
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '67036' and modifier = 'RT' and servicedate = '20100503'



-- TEST #18 SECONDARY FACILITY CLAIMS - PatientReceivables Patient #2 Surgery Claim for Facility
--facility part of example 1
--SECONDARY FACILITY CLAIMS TEST #18 - Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO. 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100503,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20100207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Patient #2  Surgery Claim for Facility (TEST #18 SECONDARY FACILITY CLAIMS)
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'K', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'K', 'OS', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), pd.PatientId, 'K', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd
WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'


-- PatientReceivables Patient #2  Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '525.64'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100503
	,'5000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'2665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- patient payment for TEST #18 1st service -- shouldn't be included in claim file
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,PatientId
	,N'P'
	,N'='
	,10
	,N'20100503'
	,N'6969'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PatientDemographics WHERE LastName = 'BLUESHIELD' AND FirstName = 'AETNA' AND MiddleInitial = 'SPOUSE'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,PatientId
	,N'P'
	,N'P'
	,10
	,N'20100503'
	,N'6969'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100503'
FROM PatientReceivableServices 
INNER JOIN PatientDemographics ON LastName = 'BLUESHIELD' AND FirstName = 'AETNA' and MiddleInitial = 'SPOUSE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY PatientId, Service, Modifier, ServiceDate

-- BILLING TRANSACTIONS patient #2  Surgery Claim for ASC
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20100506
	,654
	,'I'
	,'//Amt/4990'
	,'B'
	)


--TEST #18 SECONDARY FACILITY CLAIMS - SERVICES  Surgery Claim for Facility
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66984', 'RT', '1', '2300.00', '02', '24',
			20100503, '1', 'A', 'F', 'F', '1900.00', '2', 0,
			20100506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')


--2ND service for Facility Test #18
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'67036', 'RT', '1', '2700.00', '02', '24',
			20100503, '123', 'A', 'F', 'F', '765.87', '1', 0,
			20100506, 'VITRECT'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 2nd ServiceTransaction  Test #18  Surgery Claim for Facility
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100506
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--payments from primary (Blue shield) for Facility claim TEST #18 Secondary Facility Claims ***********
--TEST #18- PRI PAYMENTS FOR FACILITY CLAIM /surgery
-- PatientReceivablePayments - primary payment for TEST #18  
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,2430.23
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'BLUE SHIELD'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT 
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,1586.46
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N''
	,@PatientReceivablePaymentBatchCheckId 
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate


--contractual writeoff for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,400
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'45'
	,@PatientReceivablePaymentBatchCheckId 
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate


--coinsurance for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,213.54
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'2'
	,@PatientReceivablePaymentBatchCheckId 
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate

--deductible for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,100.00
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '66984' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate



-- payment for TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,543.77
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate



--contractual writeoff for TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,1934.13
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate


--coinsurance for  TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,100.00
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate


--deductible for  TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,122.1
	,N'20100516'
	,N'BS334466'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100514'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE Service = '67036' 
	and Modifier = 'RT' 
	and ServiceDate = 20100503
GROUP BY InsurerId, Service, Modifier, ServiceDate



--Test #18 facility claims - RECEIVABLE FOR 2Y FACILITY CLAIM
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '525.64'
	,'B'
	,pd.PatientId
	,pdInsurer.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100503
	,'5000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'2665.87'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PatientDemographics pdInsurer ON pdInsurer.Lastname = 'AETNA' AND pdInsurer.FirstName = 'ONLY'
INNER JOIN PracticeInsurers ON InsurerName = 'aetna'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20100516
	,654
	,'I'
	,'//Amt/525.64'
	,'B'
	)

-- TEST #18 1st ServiceTransaction for secondary claim (patient #2)  Surgery Claim for FACILITY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,MAX(ItemId)
	,20100516
	,303.54
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '66984' and modifier = 'RT' and servicedate = '20100503'


--  TEST #18 2nd ServiceTransaction for secondary claim (patient #2)  Surgery Claim for FACILITY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,MAX(ItemId)
	,20100516
	,222.1
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '67036' and modifier = 'RT' and servicedate = '20100503'


----------TEST #18 - FACILITY SECONDARY CLAIM PROFESSIONAL - MEDICARE 2y
--example 2 CIGNAPRIMARY, MEDICARESECONDARY
---DOCTOR'S APPOINTMENT
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100406,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD PATIENT #1  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20100406, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, (SELECT ResourceId2 FROM dbo.Appointments WHERE AppointmentId = IDENT_CURRENT('Appointments')), '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 



--Diagnoses for Patient #1  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd WHERE pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 


--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20100406,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

INSERT INTO model.PatientInsuranceAuthorizations (
	AuthorizationCode
	,AuthorizationDateTime
	,IsArchived
	,PatientInsuranceId
	,EncounterId
	,Comment
	)
SELECT 'CAREPREAUTH'
	,CONVERT(datetime,'20100406', 112)
	,CONVERT(bit, 0)
	,pins.Id
	,IDENT_CURRENT('Appointments')
	,''
FROM model.PatientInsurances pins
INNER JOIN model.InsurancePolicies ip ON ip.Id = pins.InsurancePolicyId
INNER JOIN model.Insurers ins ON ins.Id = ip.InsurerId
INNER JOIN model.Patients pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY'
WHERE ins.Name = 'MEDICARE'
	AND pins.InsuredPatientId = pd.Id


--no activity record for facility appt

--Diagnoses for Patient #1  Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics pd 
WHERE pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 


-- PatientReceivables - 2d example - Surgery Claim for Facility TO CIGNA
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '280'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100406
	,'1500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'990.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS example 2 Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'Z'
	,20100406
	,654
	,'I'
	,'//Amt/1500'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'66984', 'LT', '1', '1000.00', '02', '24',
			20100406, '1', 'A', 'F', 'F', '665.87', '1', 0,
			20100406, 'CATARACT SUR'
FROM PatientDemographics pd 
WHERE  pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 

-- 1st ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100406
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--2d service  in example 2 of test #18
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)

          ,'67028', 'LT', '1', '500.00', '02', '24',
			20100406, '1', 'A', 'F', 'F', '325', '2', 0,
			20100406, 'INTRAVIT INJ'
FROM PatientDemographics pd 
WHERE  pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 

--[model.DoctorInsurerAssignments MUST BE CHANGED TO PROVIDERINSURERASSIGNMENTS FOR MEDICARE PATIENT PAYMENTS TO BE SUPPRESSED.]
---- patient payment for TEST #18 example 2, 2d service -- shouldn't be included in claim file
----batch file
--INSERT [dbo].[PatientReceivablePayments] (
--	[ReceivableId]
--	,[PayerId]
--	,[PayerType]
--	,[PaymentType]
--	,[PaymentAmount]
--	,[PaymentDate]
--	,[PaymentCheck]
--	,[PaymentRefId]
--	,[PaymentRefType]
--	,[Status]
--	,[PaymentService]
--	,[PaymentCommentOn]
--	,[PaymentFinancialType]
--	,[PaymentServiceItem]
--	,[PaymentAssignBy]
--	,[PaymentReason]
--	,[PaymentBatchCheckId]
--	,[PaymentAppealNumber]
--	,[PaymentEOBDate]
--	)
--SELECT top 1
--	0
--	,PatientId
--	,N'P'
--	,N'='
--	,20
--	,N'20100406'
--	,N'7777'
--	,N'0'
--	,N''
--	,N'A'
--	,''
--	,N'F'
--	,N''
--	,0
--	,0
--	,N''
--	,0
--	,N''
--	,N''
--FROM PatientDemographics WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'

--INSERT [dbo].[PatientReceivablePayments] (
--	[ReceivableId]
--	,[PayerId]
--	,[PayerType]
--	,[PaymentType]
--	,[PaymentAmount]
--	,[PaymentDate]
--	,[PaymentCheck]
--	,[PaymentRefId]
--	,[PaymentRefType]
--	,[Status]
--	,[PaymentService]
--	,[PaymentCommentOn]
--	,[PaymentFinancialType]
--	,[PaymentServiceItem]
--	,[PaymentAssignBy]
--	,[PaymentReason]
--	,[PaymentBatchCheckId]
--	,[PaymentAppealNumber]
--	,[PaymentEOBDate]
--	)
--SELECT top 1
--	@PatientReceivableId
--	,PatientId
--	,N'P'
--	,N'P'
--	,10
--	,N'20100406'
--	,N'7777'
--	,N'0'
--	,N'V'
--	,N'A'
--	,Service
--	,N'F'
--	,N'C'
--	,ItemId
--	,ResourceId
--	,N''
--	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
--	,N''
--	,N'20100503'
--FROM PatientReceivableServices 
--INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY' 
--INNER JOIN Resources ON ResourceType <> 'R'
--WHERE Service = '67028' 
--	and Modifier = 'LT' 
--	and ServiceDate = 20100406


-- 2d ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20100406
	,Charge*Quantity
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = (SELECT TOP 1 Value FROM dbo.ViewIdentities WHERE Name = 'dbo.PatientReceivableServices')

--TEST #18 2d example CIGNAPRIMARY, MEDICARE SECONDARY - payments from Cigna ****
-- PatientReceivablePayments - 
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5500.23
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'CIGNA'

SET @PatientReceivablePaymentBatchCheckId = dbo.GetIdentCurrent('dbo.PatientReceivablePayments')

-- payment for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT 
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,545.87
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '66984' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate


--contractual writeoff for TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,334.13
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '66984' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate


--coinsurance for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,20
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '66984' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate

--deductible for  TEST #18 1st service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,100.00
	,N'20100406'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '66984' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate



-- payment for TEST #18 2D EXAMPLE 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'P'
	,150.00
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N''
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '67028' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate



--contractual writeoff for TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'X'
	,175.00
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'45'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '67028' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate


--coinsurance for  TEST #18 2d example, 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'|'
	,30.00
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'2'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '67028' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate


--deductible for  TEST #18 2d service
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	@PatientReceivableId
	,InsurerId
	,N'I'
	,N'!'
	,145
	,N'20100413'
	,N'CG334433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,MAX(ItemId)
	,MAX(ResourceId)
	,N'1'
	,@PatientReceivablePaymentBatchCheckId
	,N''
	,N'20100410'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE Service = '67028' 
	and Modifier = 'LT' 
	and ServiceDate = 20100406
GROUP BY InsurerId, Service, Modifier, ServiceDate



--Test #18 facility claims - example 2 - RECEIVABLE FOR 2Y FACILITY CLAIM to medicare
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '280'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,dbo.PadLeft(pd.PatientId,5) +'-'+ dbo.PadLeft(IDENT_CURRENT('Appointments'),5)
	,20100406
	,'1500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'998.87'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'CIGNAPRIMARY' AND pd.FirstName = 'MEDICARESECONDARY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId

SET @PatientReceivableId = dbo.GetIdentCurrent('dbo.PatientReceivables')

-- BILLING TRANSACTIONS patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,@PatientReceivableId
	,'S'
	,20100413
	,654
	,'I'
	,'//Amt/240'
	,'B'
	)

-- TEST #18 1st ServiceTransaction for secondary claim (patient #2)  Surgery Claim for FACILITY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,MAX(ItemId)
	,20100413
	,120.00
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '66984' and modifier = 'LT' and servicedate = '20100406'

--  TEST #18 2nd ServiceTransaction for secondary claim (patient #2)  Surgery Claim for FACILITY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,MAX(ItemId)
	,20100413
	,120.00
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '67028' and modifier = 'LT' and servicedate = '20100406'

--Add Billing Diagnosis for all dxs. 
INSERT INTO model.BillingDiagnosis (InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId)
        
SELECT InvoiceId, OrdinalId, MIN(ExternalSystemEntityMappingId) AS ExternalSystemEntityMappingId, LegacyPatientClinicalId FROM (
SELECT inv.Id AS InvoiceId ,CASE Symptom WHEN '1' THEN 1 WHEN '2' THEN 2 WHEN '3' THEN 3 WHEN '4' THEN 4 WHEN '5' THEN 5 WHEN '6' THEN 6 WHEN '7'THEN 7 WHEN '8' THEN 8 WHEN '9' THEN 9 WHEN '10' THEN 10 WHEN '11' THEN 11 WHEN '12' THEN 12 END As OrdinalId,e.Id AS ExternalSystemEntityMappingId,pc.ClinicalId AS LegacyPatientClinicalId FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.AppointmentId AND ap.ScheduleStatus <> 'A' AND ap.Comments <> 'ASC CLAIM'
INNER JOIN model.Invoices inv on inv.EncounterId = pc.AppointmentId AND inv.InvoiceTypeId <> 2 
INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0 THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) ELSE LEN(FindingDetail) END) AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition') AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9') AND Name = 'ClinicalCondition')
WHERE ClinicalType IN ('B', 'K')
GROUP BY ap.AppointmentId, inv.Id, Case Symptom WHEN '1' THEN 1 WHEN '2'THEN 2 WHEN '3' THEN 3 WHEN '4' THEN 4 WHEN '5' THEN 5 WHEN '6' THEN 6 WHEN '7' THEN 7 WHEN '8'THEN 8 WHEN '9' THEN 9 WHEN '10' THEN 10 WHEN '11' THEN 11 WHEN '12' THEN 12 End ,e.Id,pc.ClinicalId)v GROUP BY v.InvoiceId, v.OrdinalId, v.LegacyPatientClinicalId
EXCEPT SELECT InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId FROM model.BillingDiagnosis

--Covers ASC claims
INSERT INTO model.BillingDiagnosis (InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId)
SELECT InvoiceId, OrdinalId, MIN(ExternalSystemEntityMappingId) AS ExternalSystemEntityMappingId, LegacyPatientClinicalId FROM
(SELECT inv.Id AS InvoiceId ,CASE Symptom WHEN '1' THEN 1 WHEN '2' THEN 2 WHEN '3' THEN 3 WHEN '4' THEN 4 WHEN '5' THEN 5 WHEN '6' THEN 6 WHEN '7'THEN 7 WHEN '8' THEN 8 WHEN '9' THEN 9 WHEN '10' THEN 10 WHEN '11' THEN 11 WHEN '12' THEN 12 END As OrdinalId,e.Id AS ExternalSystemEntityMappingId,pc.ClinicalId AS LegacyPatientClinicalId FROM dbo.PatientClinical pc
INNER JOIN dbo.Appointments ap ON ap.AppointmentId = pc.Appointmentid  AND ap.Comments = 'ASC CLAIM' INNER JOIN dbo.Appointments apEncounter ON apEncounter.PatientId = ap.PatientId AND apEncounter.AppTypeId = ap.AppTypeId AND apEncounter.AppDate = ap.AppDate AND apEncounter.AppTime > 0 AND ap.AppTime = 0     AND apEncounter.ScheduleStatus = ap.ScheduleStatus AND ap.ScheduleStatus = 'D' AND apEncounter.ResourceId1 = ap.ResourceId1 AND apEncounter.ResourceId2 = ap.ResourceId2
INNER JOIN model.Invoices inv on inv.EncounterId = apEncounter.AppointmentId AND inv.InvoiceTypeId = 2
INNER JOIN model.ExternalSystemEntityMappings e ON e.ExternalSystemEntityKey = SUBSTRING(FindingDetail, 1, CASE WHEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) > 0 THEN (dbo.GetMax(CHARINDEX('.', FindingDetail, CHARINDEX('.', FindingDetail, 0) + 1) - 1, 0)) ELSE LEN(FindingDetail) END) AND PracticeRepositoryEntityId = (SELECT TOP 1 Id FROM model.PracticeRepositoryEntities WHERE name = 'ClinicalCondition') AND ExternalSystemEntityId = (SELECT TOP 1 Id FROM model.ExternalSystemEntities WHERE ExternalSystemId = (SELECT TOP 1 Id FROM model.ExternalSystems WHERE name = 'Icd9') AND Name = 'ClinicalCondition')
WHERE ClinicalType IN ('B', 'K')
GROUP BY apEncounter.AppointmentId, inv.Id, Case Symptom WHEN '1' THEN 1 WHEN '2'THEN 2 WHEN '3' THEN 3 WHEN '4' THEN 4 WHEN '5' THEN 5 WHEN '6' THEN 6 WHEN '7' THEN 7 WHEN '8'THEN 8 WHEN '9' THEN 9 WHEN '10' THEN 10 WHEN '11' THEN 11 WHEN '12' THEN 12 End ,e.Id,pc.ClinicalId)v GROUP BY v.InvoiceId, v.OrdinalId, v.LegacyPatientClinicalId
EXCEPT SELECT InvoiceId, OrdinalId, ExternalSystemEntityMappingId, LegacyPatientClinicalId FROM model.BillingDiagnosis



-- Adding Ndc codes for billing services
UPDATE bs
SET bs.Ndc = CONVERT(NVARCHAR(MAX),es.Ndc)
FROM  model.BillingServices bs
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
WHERE es.Ndc IS NOT NULL