IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AlterIndexes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [AlterIndexes]

GO

CREATE PROCEDURE [AlterIndexes](@schemaName nvarchar(max), @objectType nvarchar(max), @action nvarchar(max))
AS
	SET NOCOUNT ON
	DECLARE @sql nvarchar(max)

	DECLARE c CURSOR FOR
	SELECT 'ALTER INDEX [' + sys.indexes.name + '] ON [' + sys.schemas.name + '].[' + sys.objects.name + '] ' + @action + ';' + char(13) + char(10) FROM
	sys.indexes
	JOIN 
		sys.objects 
		ON sys.indexes.object_id = sys.objects.object_id
	JOIN
		sys.schemas
		ON sys.objects.schema_id = sys.schemas.schema_id
	WHERE (sys.schemas.name = @schemaName OR @schemaName = '') AND (@objectType = '' OR OBJECTPROPERTY(sys.objects.object_id, @objectType) = 1)
	ORDER BY sys.objects.name, sys.indexes.type, sys.indexes.name
	OPEN c
	FETCH NEXT FROM c INTO @sql
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @start datetime 
		SET @start = GETDATE()	
		RAISERROR(@sql, 0, 1) WITH NOWAIT	
		EXEC(@sql)
		DECLARE @duration int 
		SET @duration = (SELECT DATEDIFF(millisecond, @start, GETDATE()))
		DECLARE @message nvarchar(max) 
		SET @message = 'Completed in ' + CONVERT(nvarchar, @duration) + 'ms.'
		RAISERROR(@message, 0, 1) WITH NOWAIT	
		
		FETCH NEXT FROM c INTO @sql
	END
	CLOSE c
	DEALLOCATE c	