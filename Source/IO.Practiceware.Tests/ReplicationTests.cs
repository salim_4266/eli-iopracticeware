﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using IO.Practiceware.DbMigration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Logging;

namespace IO.Practiceware.Tests
{
    [TestClass]
#if DISABLE_REPLICATION_TESTS
    [Ignore]
#endif
    public class ReplicationTests
    {
        [TestMethod]
        [TestCategory("Replication")]
        public void TestDbSyncAndReplicationWithoutSnapshot()
        {
            RestoreHub();
            RestoreSpoke();

            using (new TimedScope(s => Trace.TraceInformation("Ran first iteration in {0}.".FormatWith(s))))
                // once on fresh DBs
                ExecuteTestDbSyncAndReplicationWithoutSnapshot();

            using (new TimedScope(s => Trace.TraceInformation("Ran second iteration in {0}.".FormatWith(s))))
                // once on existing setup
                ExecuteTestDbSyncAndReplicationWithoutSnapshot();
        }

        private static void ExecuteTestDbSyncAndReplicationWithoutSnapshot()
        {
            Bootstrapper.Run<object>();

            //ADD COMMENTS ON WHERE TO GET DATABASE TO TEST ON

            var configuration = new DbMigrationConfiguration
            {
                UseSnapshotForReplication = false,
                UseDbSyncForReplication = true,
                ExecuteDbSyncScriptAutomatically = true
            };
            configuration.MigrationConnectionStrings.Add(String.Format("Data Source=dev-sql03;Initial Catalog=PracticeRepositoryReplicatingHub;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"));
            configuration.MigrationConnectionStrings.Add(String.Format("Data Source=dev-sql01;Initial Catalog=PracticeRepositoryReplicatingSpoke;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"));

            Migrator.ShowElapsedTime = true;
            Migrator.Run(new[] { configuration });

            using (var hubConnection = new SqlConnection(configuration.MigrationConnectionStrings.First()))
            {
                //TODO Thread.Sleep(5000) between all of these.

                var patientId = CreateNewTestPatient(hubConnection);

                //Verify the PatientId was created on the hub database
                //Indicates "INSERT" statements are working on the hub
                Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                //Verify the seed range on the hub database
                Assert.IsTrue(patientId < 1000000);

                //Verify for each spoke connection string provided in the "configuration" the patient was inserted
                //Indicates "INSERT" statements are working from the hub to spoke
                foreach (var connectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    Thread.Sleep(15000);
                    Assert.AreEqual(CheckIfPatientIdExists(new SqlConnection(connectionString), patientId), 1);
                }

                //Update the test patient's name on the hub database
                UpdatePatientName(hubConnection, patientId);

                //Verify the patient
                //Indicates "UPDATE" statements are working on the hub
                Assert.AreEqual(CheckForUpdatedPatientName(hubConnection, patientId), 1);

                //Verify for each spoke connection string provided in the "configuration" the patient was updated
                //Indicates "UPDATE" statements are working from the hub to spoke
                foreach (var connectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    Thread.Sleep(15000);
                    Assert.AreEqual(CheckForUpdatedPatientName(new SqlConnection(connectionString), patientId), 1);
                }

                foreach (var spokeConnectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    patientId = CreateNewTestPatient(new SqlConnection(spokeConnectionString));

                    Thread.Sleep(15000);

                    //Verify the PatientId was created on the spoke database
                    //Indicates "INSERT" statements are working on the spoke
                    Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                    //Verify the seed range on the spoke database
                    Assert.IsTrue(patientId > 1000000);

                    Thread.Sleep(15000);
                    //Verify spoke connection string provided in the "configuration" the patient was inserted
                    //Indicates "INSERT" statements are working from the spoke to hub
                    Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                    //Update the test patient's name on the spoke database
                    UpdatePatientName(new SqlConnection(spokeConnectionString), patientId);

                    //Verify the patient
                    //Indicates "UPDATE" statements are working on the spoke
                    Assert.AreEqual(CheckForUpdatedPatientName(new SqlConnection(spokeConnectionString), patientId), 1);

                    Thread.Sleep(15000);

                    //Verify for each spoke connection string provided in the "configuration" the patient was updated
                    //Indicates "UPDATE" statements are working from the spoke to hub
                    Assert.AreEqual(CheckForUpdatedPatientName(hubConnection, patientId), 1);
                }
            }
        }

        [TestMethod]
        [TestCategory("Replication")]
        public void TestReplicationWithSnapshot()
        {
            RestoreHubForSnapshot();
            RestoreSpokeForSnapshot();

            using (new TimedScope(s => Trace.TraceInformation("Ran first iteration in {0}.".FormatWith(s))))
                // once on fresh DBs
                ExecuteTestReplicationWithSnapshot();

            using (new TimedScope(s => Trace.TraceInformation("Ran second iteration in {0}.".FormatWith(s))))
                // once on existing setup
                ExecuteTestReplicationWithSnapshot();
        }

        private void ExecuteTestReplicationWithSnapshot()
        {
            Bootstrapper.Run<object>();

            //ADD COMMENTS ON WHERE TO GET DATABASE TO TEST ON

            var configuration = new DbMigrationConfiguration
            {
                UseSnapshotForReplication = true,
                UseDbSyncForReplication = false,
                ExecuteDbSyncScriptAutomatically = false
            };
            configuration.MigrationConnectionStrings.Add(String.Format("Data Source=dev-sql03;Initial Catalog=PracticeRepositoryReplicatingHub_Snapshot;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"));
            configuration.MigrationConnectionStrings.Add(String.Format("Data Source=dev-sql01;Initial Catalog=PracticeRepositoryReplicatingSpoke_Snapshot;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"));

            Migrator.ShowElapsedTime = true;
            Migrator.Run(new[] { configuration });

            using (var hubConnection = new SqlConnection(configuration.MigrationConnectionStrings.First()))
            {
                HoldUntilSnapshotApplied(hubConnection);

                //TODO Thread.Sleep(5000) between all of these.

                var patientId = CreateNewTestPatient(hubConnection);

                //Verify the PatientId was created on the hub database
                //Indicates "INSERT" statements are working on the hub
                Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                //Verify the seed range on the hub database
                Assert.IsTrue(patientId < 1000000);

                //Verify for each spoke connection string provided in the "configuration" the patient was inserted
                //Indicates "INSERT" statements are working from the hub to spoke
                foreach (var connectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    Thread.Sleep(15000);
                    Assert.AreEqual(CheckIfPatientIdExists(new SqlConnection(connectionString), patientId), 1);
                }

                //Update the test patient's name on the hub database
                UpdatePatientName(hubConnection, patientId);

                //Verify the patient
                //Indicates "UPDATE" statements are working on the hub
                Assert.AreEqual(CheckForUpdatedPatientName(hubConnection, patientId), 1);

                //Verify for each spoke connection string provided in the "configuration" the patient was updated
                //Indicates "UPDATE" statements are working from the hub to spoke
                foreach (var connectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    Thread.Sleep(15000);
                    Assert.AreEqual(CheckForUpdatedPatientName(new SqlConnection(connectionString), patientId), 1);
                }

                foreach (var spokeConnectionString in configuration.MigrationConnectionStrings.Skip(1))
                {
                    patientId = CreateNewTestPatient(new SqlConnection(spokeConnectionString));

                    Thread.Sleep(15000);

                    //Verify the PatientId was created on the spoke database
                    //Indicates "INSERT" statements are working on the spoke
                    Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                    //Verify the seed range on the spoke database
                    Assert.IsTrue(patientId > 1000000);

                    Thread.Sleep(15000);
                    //Verify spoke connection string provided in the "configuration" the patient was inserted
                    //Indicates "INSERT" statements are working from the spoke to hub
                    Assert.AreEqual(CheckIfPatientIdExists(hubConnection, patientId), 1);

                    //Update the test patient's name on the spoke database
                    UpdatePatientName(new SqlConnection(spokeConnectionString), patientId);

                    //Verify the patient
                    //Indicates "UPDATE" statements are working on the spoke
                    Assert.AreEqual(CheckForUpdatedPatientName(new SqlConnection(spokeConnectionString), patientId), 1);

                    Thread.Sleep(15000);

                    //Verify for each spoke connection string provided in the "configuration" the patient was updated
                    //Indicates "UPDATE" statements are working from the spoke to hub
                    Assert.AreEqual(CheckForUpdatedPatientName(hubConnection, patientId), 1);
                }
            }
        }

        private void HoldUntilSnapshotApplied(SqlConnection hubConnection)
        {
            var time = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            var query = "exec [distribution].sys.sp_replmonitorsubscriptionpendingcmds @publisher = N'DEV-SQL03', @publisher_db = N'PracticeRepositoryReplicatingHub_Snapshot', @publication = N'PracticeRepositoryReplicatingHub_Snapshot', @subscriber = N'DEV-SQL01', @subscriber_db = N'PracticeRepositoryReplicatingSpoke_Snapshot', @subscription_type = 0".FormatWith(time);

            while (true)
            {
                Thread.Sleep(15000);

                var dt = hubConnection.Execute<DataTable>(query);

                if (dt.Rows.OfType<DataRow>().All(r => ((int)r[0] == 0)))
                {
                    return;
                }
            }
        }

        #region HelperMethods

        private const string DevSql03SqlDirectoryPath = @"D:\SQLData\MSSQL11.MSSQLSERVER\MSSQL\";
        private const string DevSql01SqlDirectoryPath = @"D:\SQLData\MSSQL10_50.MSSQLSERVER\MSSQL\";

        private static string GetRestoreSql(string database)
        {
            var sql = @"
USE [master]
IF COALESCE(DATABASEPROPERTYEX ('{0}', 'Status'), '') = 'ONLINE'
    EXEC('ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE')

IF COALESCE(DATABASEPROPERTYEX ('{0}_Archive', 'Status'), '') = 'ONLINE'
    EXEC('ALTER DATABASE [{0}_Archive] SET SINGLE_USER WITH ROLLBACK IMMEDIATE; DROP DATABASE [{0}_Archive]')

RESTORE DATABASE [{0}] FROM  DISK = N'{1}Backup\Replicating\{2}.bak' WITH FILE = 1,  
MOVE N'PracticeRepository_Data' TO N'{1}DATA\{0}.mdf',
MOVE N'PracticeRepository_Log' TO N'{1}DATA\{0}.ldf',
REPLACE
ALTER DATABASE [{0}] SET MULTI_USER
".FormatWith(database, database.Contains("Hub") ? DevSql03SqlDirectoryPath : DevSql01SqlDirectoryPath, database.Split('_')[0]);

            return sql;
        }


        private static void RestoreHub()
        {

            using (var c = DevSql03MasterConnection)
            {
                c.Execute(GetRestoreSql("PracticeRepositoryReplicatingHub"));
            }

        }

        private static void RestoreSpoke()
        {
            using (var c = DevSql01MasterConnection)
            {
                c.Execute(GetRestoreSql("PracticeRepositoryReplicatingSpoke"));
            }

        }

        private static void RestoreHubForSnapshot()
        {

            using (var c = DevSql03MasterConnection)
            {
                c.Execute(GetRestoreSql("PracticeRepositoryReplicatingHub_Snapshot"));
            }
        }

        private static void RestoreSpokeForSnapshot()
        {

            using (var c = DevSql01MasterConnection)
            {
                c.Execute(GetRestoreSql("PracticeRepositoryReplicatingSpoke_Snapshot"));
            }

        }

        public static SqlConnection DevSql01MasterConnection
        {
            get
            {
                return new SqlConnection("Data Source=dev-sql01;Initial Catalog=master;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1800");
            }
        }

        public static SqlConnection DevSql03MasterConnection
        {
            get { return new SqlConnection("Data Source=dev-sql03;Initial Catalog=master;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1800"); }
        }

        private static int CreateNewTestPatient(SqlConnection connection)
        {
            return connection.Execute<int>(@"INSERT INTO [PatientDemographics] (
	[FirstName]
	,[LastName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT TOP 1 'REPLICATION'
	,'TEST'
	,'B'
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,VendorId
	,'0'
	,''
	,''
	,''
	,0
	,''
	,'N'
	,0
	,''
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MRS'
	,'T'
	,''
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
FROM PracticeVendors"
                );
        }
        private static void UpdatePatientName(SqlConnection conn, int patientId)
        {
            conn.Execute(
                    string.Format(@"UPDATE p
SET p.FirstName = 'REPLICATIONTWO'
FROM dbo.PatientDemographics p
WHERE p.PatientId = {0}
", patientId));
        }
        private static int CheckForUpdatedPatientName(SqlConnection conn, int patientId)
        {
            return conn.Execute<int>(String.Format(@"IF (SELECT p.FirstName FROM PatientDemographics p WHERE p.PatientId = {0}) = 'REPLICATIONTWO'
SELECT 1 ELSE SELECT 0 ", patientId));
        }
        private static int CheckIfPatientIdExists(SqlConnection conn, int patientId)
        {
            return conn.Execute<int>(
                string.Format(@"IF EXISTS (SELECT * FROM model.Patients WHERE Id = {0}) SELECT 1 ELSE SELECT 0",
                    patientId));
        }
        #endregion
    }
}