﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class QrdaTests : TestBase
    {
        [TestMethod]
        [Ignore] // Test runs against external service. Run occasionally
        public void TestCanProduceSummaryReport()
        {
            // Prepare messages
            var messages = new List<CdaMessage>();

            // Extract test QRDA Cat 1 files
            using (var zip = new ZipInputStream(new MemoryStream(CdaResources.Diabetes__qrda)))
            {
                ZipEntry zipEntry;
                while ((zipEntry = zip.GetNextEntry()) != null)
                {
                    // Extract
                    var contentStream = new MemoryStream();
                    zip.CopyTo(contentStream);
                    var content = Encoding.UTF8.GetString(contentStream.ToArray());
                    contentStream.Dispose();

                    // Load message
                    var message = new CdaMessage(Guid.NewGuid());
                    message.Load(content);
                    message.Destination = zipEntry.Name;
                    messages.Add(message);
                }
            }

            // Generate Cat 3 report over supplied Cat 1 files
            var target = Common.ServiceProvider.GetService<QrdaCategory3MessageProducer>();
            var result = target.ProduceMessage(
                UserContext.Current.UserDetails.Id,
                new DateTime(2012, 1, 1), // Start
                new DateTime(2012, 12, 31), // End
                new[] { ElectronicClinicalQualityMeasure.DiabetesEyeExam }, // Measures
                messages.ToArray());

            Assert.IsNotNull(result);

            // Verify produced report is exactly what was expected
            var expectedComponent = Regex.Match(CdaResources.Diabetes__qrda3, "<component.*/component>", RegexOptions.Singleline).Value;
            var actualComponent = Regex.Match(result.ToString(), "<component.*/component>", RegexOptions.Singleline).Value;
            Assert.AreEqual(expectedComponent, actualComponent);
        }

        [TestMethod]
        [Ignore] // Runs over 6 minutes and fails with error for now
        public void TestCanProduceMeasureReport()
        {
            var target = Common.ServiceProvider.GetService<QrdaCategory1MessageProducer>();
            var result = target.ProduceMessages(
                new[] { ElectronicClinicalQualityMeasure.DiabetesEyeExam },
                new[] { UserContext.Current.UserDetails.Id },
                new DateTime(2012, 1, 1), // Start
                new DateTime(2012, 12, 31) // End
                );

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());
        }
    }
}
