﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Presentation;
using Soaf;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PatientDemographicsSetupTests : TestBase
    {
       
        [TestMethod]
        public void PatientDemographicsSetupViewContextLoad()
        {
            PatientDemographicsSetupViewContext viewContext;
            SetupTestContext(out viewContext);

            Assert.IsTrue(viewContext.PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.Any());
            Assert.AreEqual(1, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.Count);
            Assert.AreEqual(2, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.Count);
            Assert.IsTrue(viewContext.PatientDemographicsManageListViewContext.PatientDemographicsLists.Any());
        }

        [TestMethod]
        public void TestPatientDemographicsManageListViewContextConfigureFieldsChangeAndSave()
        {
            PatientDemographicsSetupViewContext viewContext;
            SetupTestContext(out viewContext);
            Assert.IsTrue(viewContext.PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.Any());
            PatientDemographicsConfigureFieldGroupViewModel fieldsGroup1 = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.First();
            Assert.AreEqual(fieldsGroup1.GroupName, PatientDemographicsFieldConfigurationId.Title.GetAttribute<DisplayAttribute>().GroupName);

            PatientDemographicsConfigureFieldViewModel titleField = fieldsGroup1.ConfigureFields.First(f => f.Id == (int)PatientDemographicsFieldConfigurationId.Title);
            Assert.AreEqual(1, titleField.Id); 
            Assert.IsTrue(titleField.IsVisibleConfigurable);
            Assert.IsTrue(titleField.IsVisible);
            Assert.IsFalse(titleField.IsRequiredConfigurable);
            Assert.IsFalse(titleField.IsRequired);
            //update title field configuration and save
            titleField.IsVisible = true;
            titleField.IsRequired = true;

            PermissionId.AdministerPatientDemographicsFields.AddPermissionToCurrentUser(PracticeRepository);
            //Save changes
            viewContext.Save.Execute();

            //Reset viewContext and check for the saved changes
            SetupTestContext(out viewContext);

            Assert.IsTrue(viewContext.PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.Any());
            fieldsGroup1 = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.First();

            titleField = fieldsGroup1.ConfigureFields.First(f => f.Id == (int)PatientDemographicsFieldConfigurationId.Title);
            Assert.AreEqual(1,   titleField.Id);
            Assert.IsTrue(titleField.IsVisibleConfigurable);
            Assert.IsTrue(titleField.IsVisible);
            Assert.IsFalse(titleField.IsRequiredConfigurable);
            Assert.IsTrue(titleField.IsRequired);
        }

        [TestMethod]
        public void TestPatientDemographicsManageListViewContextDefaultValueChangeAndSave()
        {
            //HIPAA Content check box default value change
            PatientDemographicsSetupViewContext viewContext;
            SetupTestContext(out viewContext);
            Assert.AreEqual(1, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.Count);
            PatientDemographicsConfigureDefaultValueViewModel hipaaCheckBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.First();
            Assert.AreEqual(hipaaCheckBox.Name, PatientDemographicsFieldConfigurationId.HasHipaaConsent.GetDisplayName());
            Assert.IsTrue(Convert.ToBoolean(hipaaCheckBox.DefaultValue));

            // Change default value to False
            hipaaCheckBox.DefaultValue = (false).ToString();

            PermissionId.AdministerPatientDemographicsFields.AddPermissionToCurrentUser(PracticeRepository);
            //Save changes
            viewContext.Save.Execute();

            //Reset viewContext and check for the saved changes
            SetupTestContext(out viewContext);
            Assert.AreEqual(1, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.Count);
            hipaaCheckBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.First();
            Assert.IsFalse(Convert.ToBoolean(hipaaCheckBox.DefaultValue));
            //Combo box Default value changes
            Assert.AreEqual(2, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.Count);
            PatientDemographicsConfigureDefaultValueViewModel releaseMedicalRecordsComboBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.First(p => p.Id == (int)PatientDemographicsFieldConfigurationId.ReleaseMedicalInformationId);
            Assert.AreEqual(ReleaseOfInformationCode.Y, ((ReleaseOfInformationCode)(Convert.ToInt32(releaseMedicalRecordsComboBox.DefaultValue))));
            
            PatientDemographicsConfigureDefaultValueViewModel assignBenefitsToProviderComboBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.First(p => p.Id == (int)PatientDemographicsFieldConfigurationId.AssignBenefitsToProviderId);
            Assert.AreEqual(PaymentOfBenefitsToProvider.Y, (PaymentOfBenefitsToProvider)(Convert.ToInt32(assignBenefitsToProviderComboBox.DefaultValue)));

            // Change default values to No
            releaseMedicalRecordsComboBox.DefaultValue = ((int) ReleaseOfInformationCode.N).ToString();
            assignBenefitsToProviderComboBox.DefaultValue =((int) PaymentOfBenefitsToProvider.N).ToString();

            //Save changes
            viewContext.Save.Execute();

            //Reset viewContext and check for the saved changes
            SetupTestContext(out viewContext);
            Assert.AreEqual(2, viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.Count);
            releaseMedicalRecordsComboBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.First(p => p.Id == (int)PatientDemographicsFieldConfigurationId.ReleaseMedicalInformationId);
            Assert.AreEqual(ReleaseOfInformationCode.N, ((ReleaseOfInformationCode)(Convert.ToInt32(releaseMedicalRecordsComboBox.DefaultValue))));

            assignBenefitsToProviderComboBox = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.First(p => p.Id == (int)PatientDemographicsFieldConfigurationId.AssignBenefitsToProviderId);
            Assert.AreEqual(PaymentOfBenefitsToProvider.N, (PaymentOfBenefitsToProvider)(Convert.ToInt32(assignBenefitsToProviderComboBox.DefaultValue)));
        }

        [TestMethod]
        public void TestPatientDemographicsSetupViewContextManageListsOrderChangeAndSave()
        {
            List<Language> patientLanguages = PracticeRepository.Languages.ToList();
            if (patientLanguages.Any() & patientLanguages.Count < 5)
            {
                // create 5 new patient languages
                var language1 = new Language();
                language1.Name = "Language 1";
                PracticeRepository.Save(language1);

                var language2 = new Language();
                language2.Name = "Language 2";
                PracticeRepository.Save(language2);

                var language3 = new Language();
                language3.Name = "Language 3";
                PracticeRepository.Save(language3);

                var language4 = new Language();
                language4.Name = "Language 4";
                PracticeRepository.Save(language4);

                var language5 = new Language();
                language5.Name = "Language 5";
                PracticeRepository.Save(language5);

                var language6 = new Language();
                language6.Name = "Language 6";
                PracticeRepository.Save(language6);

                patientLanguages = PracticeRepository.Languages.ToList();
            }
            
            Assert.IsTrue(patientLanguages.Count > 5);
            Language topPatientLanguage = patientLanguages.OrderBy(p => p.OrdinalId).First();
            PatientDemographicsSetupViewContext viewContext;
            SetupTestContext(out viewContext);
            Assert.IsTrue(viewContext.PatientDemographicsManageListViewContext.PatientDemographicsLists.Any());
            viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem = viewContext.PatientDemographicsManageListViewContext.PatientDemographicsLists.FirstOrDefault(l => l.Name == PatientDemographicsList.Language.GetDisplayName());
            Assert.IsNotNull(viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem);
            if (viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.FavoriteItems.Count > 0)
            {
                NamedViewModel topItem = viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.FavoriteItems.FirstOrDefault();
                viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsFavoriteListItem = viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.FavoriteItems.First();
                Assert.IsNotNull(viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsFavoriteListItem);
                viewContext.PatientDemographicsManageListViewContext.MoveDown.Execute();
                Assert.AreNotEqual(topItem, viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.FavoriteItems.FirstOrDefault());
            }

            if (viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.NonFavoriteItems.Count > 0)
            {
                NamedViewModel topItem = viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.NonFavoriteItems.FirstOrDefault();
                viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsAllListItem = viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.NonFavoriteItems.First();
                Assert.IsNotNull(viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsAllListItem);
                viewContext.PatientDemographicsManageListViewContext.MoveUp.Execute();
                Assert.AreEqual(topItem, viewContext.PatientDemographicsManageListViewContext.SelectedPatientDemographicsListItem.FavoriteItems.FirstOrDefault());
            }

            PermissionId.AdministerPatientDemographicsFields.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.Save.Execute();

            //To make sure that now we have another item as top item
            patientLanguages = PracticeRepository.Languages.OrderBy(p => p.OrdinalId).ToList();
            Assert.IsTrue(patientLanguages.Any());
            Language refetchTopPatientLanguage = patientLanguages.First();
            Assert.AreNotEqual(topPatientLanguage.Id, refetchTopPatientLanguage.Id);

            //To make sure the previous top item is moved down by 2 at 3rd place
            Language fetchTopThirdPatientLanguage = patientLanguages.Skip(2).ToList().OrderBy(p => p.OrdinalId).First();
            Assert.AreEqual(topPatientLanguage.Id, fetchTopThirdPatientLanguage.Id);
        }

        [TestMethod]
        public void TestPatientDemographicsManagePatientTagsViewContextSave()
        {
            // Check in database
            List<Tag> dbTags = PracticeRepository.Tags.Where(t => !t.IsArchived & t.TagType == TagType.PatientCategory).ToList();
            int tagsCount = dbTags.Count;

            List<Tag> dbArchivedTags = PracticeRepository.Tags.Where(t => t.IsArchived & t.TagType == TagType.PatientCategory).ToList();
            int archivedTagsCount = dbArchivedTags.Count;

            PatientDemographicsManagePatientTagsViewContext viewContext;
            SetupPatientDemographicsManagePatientTagsViewContext(out viewContext);

            Assert.AreEqual(tagsCount, viewContext.ExistingPatientTags.Count);
            Assert.AreEqual(archivedTagsCount, viewContext.ArchivedPatientTags.Count);

            PermissionId.AdministerPatientTags.AddPermissionToCurrentUser(PracticeRepository);
            // Add one item and save
            viewContext.AddPatientTag.Execute();
            Assert.IsNotNull(viewContext.NewPatientTag);

            viewContext.NewPatientTag.Name = "A";
            viewContext.NewPatientTag.Description = "A new patient tag";
            viewContext.SaveNewPatientTag.ExecuteAndWait();

            // Check for existing unarchived tags in database
            dbTags = PracticeRepository.Tags.Where(t => !t.IsArchived & t.TagType == TagType.PatientCategory).ToList();
            Assert.AreEqual(tagsCount + 1, dbTags.Count);
            Assert.AreEqual(dbTags.Count, viewContext.ExistingPatientTags.Count);

            viewContext.PatientTagToArchive = viewContext.ExistingPatientTags.First(t => t.Name == "A");
            Assert.IsNotNull(viewContext.PatientTagToArchive);
            viewContext.ArchivePatientTag.Execute();

            // Check for Archived tags in database
            dbArchivedTags = PracticeRepository.Tags.Where(t => t.IsArchived & t.TagType == TagType.PatientCategory).ToList();
            Assert.AreEqual(archivedTagsCount + 1, dbArchivedTags.Count);
            Assert.AreEqual(archivedTagsCount + 1, viewContext.ArchivedPatientTags.Count);
            Assert.AreEqual(tagsCount, viewContext.ExistingPatientTags.Count);

            //Restore the tag
            viewContext.RestorePatientTag.Execute(viewContext.ArchivedPatientTags.First(a => a.Name == "A"));
            Assert.AreEqual(archivedTagsCount, viewContext.ArchivedPatientTags.Count);
            Assert.AreEqual(tagsCount + 1, viewContext.ExistingPatientTags.Count);

            //update the existing patient tag
            PatientDemographicsPatientTagViewModel patientTag = viewContext.ExistingPatientTags.First(t => t.Name == "A");
            patientTag.Name = "B";
            patientTag.Description = "A modified patient tag";
            viewContext.SavePatientTag.Execute(patientTag);

            Tag dbTag = PracticeRepository.Tags.First(t => t.Id == patientTag.Id);
            Assert.IsNotNull(dbTag);
            Assert.AreEqual(patientTag.Name, dbTag.DisplayName);
            Assert.AreEqual(dbTag.Name, patientTag.Description);
        }

        #region Helpers

        private static void SetupTestContext(out PatientDemographicsSetupViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PatientDemographicsSetupViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
        }

        private static void SetupPatientDemographicsManagePatientTagsViewContext(out PatientDemographicsManagePatientTagsViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PatientDemographicsManagePatientTagsViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
        }

        #endregion
    }
}