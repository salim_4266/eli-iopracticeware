﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.ClinicalDataFiles;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ClinicalDataFilesPresentationTests : TestBase
    {

        [TestMethod]
        public void TestInitClinicalDataFilesEditAndTransmitViewContext()
        {
            var clinicalDataFilesEditAndTransmitViewContext = Common.ServiceProvider.GetService<ClinicalDataFilesEditAndTransmitViewContext>();
            Assert.IsNotNull(clinicalDataFilesEditAndTransmitViewContext.SearchResults);
            Assert.IsNull(clinicalDataFilesEditAndTransmitViewContext.SelectedAppointment);
        }

        [TestMethod]
        public void TestLoadClinicalDataFilesEditAndTransmitViewContext()
        {
            // Create an appointment
            UserAppointment appointment = Common.CreateAppointment();
            appointment.Encounter.EncounterStatusId = (int)EncounterStatus.Discharged;
            Common.PracticeRepository.Save(appointment);
            Assert.IsTrue(appointment.Id > 0);

            //Get appointments 
            var appointments = Common.PracticeRepository.Appointments.ToArray();
            int count = appointments.Count();
            Assert.IsTrue(count > 0);

            var clinicalDataFilesEditAndTransmitViewContext = Common.ServiceProvider.GetService<ClinicalDataFilesEditAndTransmitViewContext>();
            clinicalDataFilesEditAndTransmitViewContext.LoadArguments = new ClinicalDataFilesEditAndTransmitLoadArguments { ListOfAppointmentId = appointments.Select(a => a.Id).ToList() };

            // Load UI data
            clinicalDataFilesEditAndTransmitViewContext.Load.Execute(clinicalDataFilesEditAndTransmitViewContext.InteractionContext);
            Func<bool> isLoaded = () => clinicalDataFilesEditAndTransmitViewContext.SearchResults.Count > 0;
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(clinicalDataFilesEditAndTransmitViewContext.SearchResults.Count == count);
            Assert.IsNull(clinicalDataFilesEditAndTransmitViewContext.SelectedAppointment);
        }
    }
}
