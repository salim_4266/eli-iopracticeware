﻿using System;
using System.IO;
using System.Linq;
using IO.Practiceware.Configuration;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PatientImageImporterPresentationTests : TestBase
    {
        //[TestMethod]
        //public void TestPatientImageImport()
        //{
            // TODO: Add test logic here
            // Extract PatientImageImporterResources.PatientImageImportData.csv to a temp file.
            // Extract PatientImageImporterResources.PatientImageImportDataFiles.zip to a temp directory
            // Instanatiate PatientImageImporterViewService and call Import with criteria and index options for flat file. 
            // Use the regex ^(.*?,){3}".*? on (?<Date>.+?)"(,|$)(.*?,){8}"(?<Path>.+?)"(,|$)(.*?,){2}"(?<PatientId>.+?)[^\d]*?(,|$)(.*?,){1}"(?<FileType>.+?)"(,|$)
            // Include FileType filter like FileType = "EXAMS" OR FileType = "Obsolete Chart"
            // which corresponds to the above csv
            // Wait until import complete. Check job status periodically.
            // Make sure all files processed successfully.
            // Clean up (delete) all temp directories/files created
        //}

        [TestMethod]
        public void TestPatientImageImport()
        {
            // Bootstrap patient
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);
            
            // Prepare target directory
            var destinationFolder = Path.Combine(ConfigurationManager.ServerDataPath, "MyScan\\TestPatientImageImport");

            var service = Common.ServiceProvider.GetService<IPatientImageImporterService>();
            service.Import(new PatientDocument
            {
                FileType = "ID Card",
                PatientId = patient.Id,
                FileExtension = ".jpg",
                DateTime = DateTime.UtcNow,
                Pages = { new PatientDocumentBinaryPage(new byte[] { 1, 2, 3 }) }
            }.CreateEnumerable(), true, false, destinationFolder);

            // Check that file is there now
            var actualStorageFolder = Path.Combine(destinationFolder, patient.Id.ToString());
            var files = FileManager.Instance.ListFiles(actualStorageFolder);
            var patientFile = files.FirstOrDefault(f => f.EndsWith(".jpg"));
            Assert.IsNotNull(patientFile);
            FileManager.Instance.Delete(destinationFolder);
            FileManager.Instance.Engine.WaitForIncompleteOperations();
        }
    }
}