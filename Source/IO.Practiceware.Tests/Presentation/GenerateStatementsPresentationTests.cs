﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.GenerateStatements;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using IO.Practiceware.Presentation.Views.GenerateStatements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Presentation;
using Soaf.Security;
using System.Collections;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class GenerateStatementsPresentationTests : X12MetadataTestsBase
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            AddPermissionToCurrentUser("Patient Statement");
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(GenerateStatementsResources.GenerateStatementsViewContextSetup);
        }
        private static void AddPermissionToCurrentUser(string permission)
        {
            var dbPermission = Common.PracticeRepository.Permissions.Single(x => x.Name == permission);
            var user = (PrincipalContext.Current.Principal as UserPrincipal).IfNotNull(u => u.Identity.User);
            user.Roles.Add(new Soaf.Security.Role { Name = dbPermission.Name, Tag = new UserPermission { Permission = dbPermission, UserId = UserContext.Current.UserDetails.Id, PermissionId = dbPermission.Id } });
        }

        [TestMethod]
        public void TestGenerateStatementsSingleStatement()
        {
            CreateApplicationSetting();

            var viewStatementsViewContext = LoadStatementsViewContext();

            var statementFilter = new GenerateStatementsFilter
            {
                Name = "Test filter",
                RenderingProvideIds = new List<int>(),
                ServiceLocationIds = new List<int>(),
                BillingOrganizationIds = new List<int>(),
                TransactionStatusIds = new List<int> { 1 },
                StatmentCommunicationPreferenceIds = new List<int>(),
                MinimumAmount = 2,
                BillingCycleDays = 0,
            };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 11);
            var medicareOnlyAggregateStatement = viewStatementsViewContext.FilteredAggregateStatements.FirstOrDefault(x => x.Patient.Name == @"MEDICARE, ONLY B");
            var selectedAggregateStatement = new ObservableCollection<GenerateStatementViewModel> { medicareOnlyAggregateStatement };
            viewStatementsViewContext.SelectedAggregateStatements = selectedAggregateStatement;

            var patientId = viewStatementsViewContext.SelectedAggregateStatements
                .GroupBy(s => s.Patient.Id)
                .Select(g => g.Key)
                .First();

            byte[] result = null;
            // Mimic Print functionality
            var transaction = Transaction.Current;
            viewStatementsViewContext.Dispatcher().Invoke(() =>
            {
                using (var ts = new TransactionScope(transaction.DependentClone(DependentCloneOption.BlockCommitUntilComplete), TransactionManager.MaximumTimeout))
                {
                    var printparameters = new Hashtable(new Hashtable { { "PatientId", patientId } });
                    result = viewStatementsViewContext.RenderReport(printparameters);
                    ts.Complete();
                }
            });

            Assert.AreEqual(Convert.ToBase64String(result).Substring(1, 300), GenerateStatementsResources.GenerateStatementsSingleStatementExpectedOutput.Substring(1, 300));
        }

        [TestMethod]
        public void TestGenerateStatementsMultipleStatements()
        {
            CreateApplicationSetting();

            // TODO Add to this unit test a check for OptedOut patients.
            var viewStatementsViewContext = LoadStatementsViewContext();

            var statementFilter = new GenerateStatementsFilter
            {
                Name = "Test filter",
                RenderingProvideIds = new List<int>(),
                ServiceLocationIds = new List<int>(),
                BillingOrganizationIds = new List<int>(),
                TransactionStatusIds = new List<int> { 1 },
                StatmentCommunicationPreferenceIds = new List<int>(),
                MinimumAmount = 2,
                BillingCycleDays = 0,
            };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 11);
            viewStatementsViewContext.SelectedAggregateStatements = viewStatementsViewContext.FilteredAggregateStatements;

            // Mimic Export functionality
            var patientIds = viewStatementsViewContext.SelectedAggregateStatements
                .ToArray()
                .GroupBy(s => s.Patient.Id)
                .Select(g => g.Key)
                .ToArray();

            var results = viewStatementsViewContext.GenerateExportContent(new Hashtable { { "PatientIds", patientIds } });
            var exportStatement = results.Values.Join("");

            // Count the number of characters in both strings, they should be equal.
            Assert.AreEqual(exportStatement, GenerateStatementsResources.GenerateStatementsMultipleStatementsExpectedOutput.FormatWith(DateTime.Now.ToClientTime().Date.ToString("yyyyMMdd")));

            var patientIdsToForward = results.Where(x => x.Value != null).Select(x => x.Key).ToArray();
            var billingServiceTransactionsBeforeForwarding = PracticeRepository.BillingServiceTransactions
                .Where(bst => bst.InvoiceReceivable.PatientInsuranceId == null
                              && bst.BillingServiceTransactionStatusId == (int)BillingServiceTransactionStatus.Queued
                              && patientIdsToForward.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                .ToArray()
                .Count();

            viewStatementsViewContext.ForwardBillingServiceTransactions.ExecuteAndWait(patientIdsToForward.Select(x => x).ToDictionary(x => x, x => null as byte[]));

            var billingServiceTransactionsAfterForwarding = PracticeRepository.BillingServiceTransactions
                .Where(bst => bst.InvoiceReceivable.PatientInsuranceId == null
                              && bst.BillingServiceTransactionStatusId == (int)BillingServiceTransactionStatus.Sent
                              && patientIdsToForward.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                .ToArray();

            // Check to see that ALL bsts were forwarded correctly.
            Assert.AreEqual(billingServiceTransactionsBeforeForwarding, billingServiceTransactionsAfterForwarding.Length);

            // Check to see each billingServiceTransactionsAfterForwarding has a corresponding ExternalSystemMessage.
            var billingServiceTransactionIds = billingServiceTransactionsAfterForwarding.Select(x => x.Id).ToArray();
            var billingServiceTransactionExternalSystemMessages =
                    (from bst in PracticeRepository.BillingServiceTransactions
                     join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on CommonQueries.ConvertGuidToString(bst.Id) equals esmpre.PracticeRepositoryEntityKey into g
                     where billingServiceTransactionIds.Contains(bst.Id)
                     from esmpre in g.DefaultIfEmpty()
                     select new { BillingServiceTransaction = bst, esmpre.ExternalSystemMessage }
                    ).ToArray();

            Assert.AreEqual(billingServiceTransactionExternalSystemMessages.Where(b => b.ExternalSystemMessage != null).Select(b => b.BillingServiceTransaction).Count(), billingServiceTransactionsAfterForwarding.Length);

            // Make sure nothing is selected.
            viewStatementsViewContext.DeselectAllAggregateStatements.ExecuteAndWait();

            // Ensure after Exporting the statements, there are no longer any statements on the screen with current filter.
            // statementFilter.TransactionStatusIds = new List<int> {1}; => check for Queued first.
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 0);

            // Ensure you can see the sent statements after exporting.
            statementFilter.TransactionStatusIds = new List<int> { 2 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 11);

            // Update BST dates, set as yesterday. Now one day has past since sending the statements.
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(GenerateStatementsResources.GenerateStatementsViewContextUpdateToSent);

            // One day has past since sending.
            // Queued, BillingCycleDays = 0
            statementFilter.TransactionStatusIds = new List<int> { 1 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 0);

            // Sent, BillingCycleDays = 0
            statementFilter.TransactionStatusIds = new List<int> { 2 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 11);

            // Queued, BillingCycleDays = 1
            statementFilter.BillingCycleDays = 1;
            statementFilter.TransactionStatusIds = new List<int> { 1 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 0);

            // Sent, BillingCycleDays = 1
            statementFilter.TransactionStatusIds = new List<int> { 2 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 11);

            // Queued, BillingCycleDays = 2
            statementFilter.BillingCycleDays = 2;
            statementFilter.TransactionStatusIds = new List<int> { 1 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 0);

            // Sent, BillingCycleDays = 2
            statementFilter.TransactionStatusIds = new List<int> { 2 };
            viewStatementsViewContext.SelectedSavedFilter = statementFilter;
            viewStatementsViewContext.DisplayResult.ExecuteAndWait();
            Assert.IsTrue(viewStatementsViewContext.FilteredAggregateStatements.Count == 0);
        }

        private static GenerateStatementsViewContext LoadStatementsViewContext()
        {
            var viewContext = Common.ServiceProvider.GetService<GenerateStatementsViewContext>();
            viewContext.Load.ExecuteAndWait();
            return viewContext;
        }

        private void CreateApplicationSetting()
        {
            var modelSetting = new ApplicationSetting();
            var filter = new NamedViewModel();
            filter.Name = OpenBalanceType.Patient.ToString();
            filter.Id = (int)OpenBalanceType.Patient;

            modelSetting.UserId = UserContext.Current.UserDetails.Id;
            modelSetting.Name = ApplicationSetting.OpenBalanceDeterminer;
            modelSetting.Value = filter.ToXml();

            PracticeRepository.Save(modelSetting);
        }
    }
}
