﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using BillingServiceTransactionStatus = IO.Practiceware.Model.BillingServiceTransactionStatus;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PostAdjustmentsTests : TestBase
    {
        [TestMethod]
        public void TestPostAdjustmentsViewContextLoadAndDuplicateCheck()
        {
            var testData = InitializeTestData();

            // Load view filtered by patient and invoice
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PostAdjustmentsViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id,
                InvoiceId = testData.PrimaryInvoice.Id
            };
            viewContext.Load.ExecuteAndWait();

            Assert.AreEqual(false, viewContext.PostingInProgress, "New financial batches load in pre-posting state");

            // Fill-in required information
            viewContext.PaymentInstrument.PaymentAmount = 20;
            viewContext.PaymentInstrument.ReferenceNumber = "234328790478407";
            
            viewContext.StartPosting.ExecuteAndWait();

            Assert.IsTrue(viewContext.Accounts.Count == 1 && viewContext.Accounts[0].PatientId == testData.Patient.Id,
                "Must load only 1 account for specified patient");
            Assert.AreEqual(testData.PrimaryInvoice.Id, viewContext.Accounts[0].BillingServices.Select(e => e.InvoiceId).Distinct().Single(),
                "Encounter services must be filtered to display only current invoice");
            Assert.IsNull(viewContext.PaymentInstrument.Id, "Must be new payment instrument");
            Assert.IsTrue(viewContext.CanEditPaymentInstrument, "Can edit new payment instrument");

            // Convert command, so it can be used in this test
            viewContext.CheckDuplicatePaymentInstrument = viewContext.CheckDuplicatePaymentInstrument.UnwrapDelayedCommand();

            // Update amount, check and date to match current financial batch
            viewContext.PaymentInstrument.PaymentAmount = testData.FinancialBatch.Amount;
            viewContext.PaymentInstrument.CheckDate = testData.FinancialBatch.ExplanationOfBenefitsDateTime;
            viewContext.PaymentInstrument.ReferenceNumber = testData.FinancialBatch.CheckCode;
            viewContext.PaymentInstrument.FinancialSourceType = (Practiceware.Presentation.ViewModels.Financials.Common.Transaction.FinancialSourceType) testData.FinancialBatch.FinancialSourceTypeId;
            viewContext.CheckDuplicatePaymentInstrument.ExecuteAndWait();

            Assert.AreEqual(testData.FinancialBatch.Id, viewContext.PaymentInstrument.Id,
                "Existing payment instrument must be loaded");
            Assert.IsFalse(viewContext.CanEditPaymentInstrument, "Existing payment instrument is not editable");

            // Load new view filtered by patient, invoice and transaction
            var transactionOfInterest = viewContext.Accounts[0].BillingServices.SelectMany(es => es.Transactions).FirstOrDefault();
            Assert.IsNotNull(transactionOfInterest, "Should have at least 1 transaction");

            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id,
                InvoiceId = testData.PrimaryInvoice.Id,
                TransactionOfInterest = transactionOfInterest
            };
            viewContext.Load.ExecuteAndWait();

            Assert.AreEqual(true, viewContext.PostingInProgress, "Existing financial batches load with posting already in progress");
            Assert.AreEqual(testData.FinancialBatch.Id, viewContext.PaymentInstrument.Id,
                "Payment instrument of transaction must be loaded");

            transactionOfInterest = viewContext.Accounts[0].OnAccountTransactions.First();
            Assert.IsNotNull(transactionOfInterest, "Should have at least 1 on-account transaction");

            // Load in context of patient
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id
            };
            viewContext.Load.ExecuteAndWait();
            viewContext.PostingInProgress = true;

            // Both invoices must be shown
            var shownInvoices = viewContext.Accounts[0].BillingServices
                .Select(e => e.InvoiceId)
                .Distinct()
                .ToList();
            Assert.IsTrue(shownInvoices.Contains(testData.PrimaryInvoice.Id), 
                "Must show billing services for primary invoice");
            Assert.IsTrue(shownInvoices.Contains(testData.SecondaryInvoice.Id), 
                "Must show billing services for secondary invoice");
            Assert.IsFalse(shownInvoices.Contains(testData.NonActiveInvoice.Id),
                "Non-active invoices must be hidden");
            Assert.IsNull(viewContext.PaymentInstrument.Id,
                "Must be new payment instrument for on-account");
        }

        [TestMethod]
        public void TestPostAdjustmentsViewContextPermissions()
        {
            var testData = InitializeTestData();

            // Prepare view
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PostAdjustmentsViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id,
                InvoiceId = testData.PrimaryInvoice.Id,
                PaymentInstrumentId = testData.FinancialBatch.Id
            };
            viewContext.Load.ExecuteAndWait();

            // Locate transaction for testing
            var transaction = viewContext.Accounts[0].BillingServices
                .SelectMany(es => es.Transactions)
                .First(t => t.FinancialTransactionType is AdjustmentTypeViewModel);
            var deleteTransactionArguments = new RemoveItemArguments
            {
                Item = transaction
            };

            // Check delete permission
            Assert.IsFalse(viewContext.DeleteTransaction.CanExecute(deleteTransactionArguments),
                "Can't delete transaction without permissions");
            PermissionId.DeleteFinancialTransactions.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.DeleteTransaction.CanExecute(deleteTransactionArguments),
                "Should be able to delete once permission is given");

            Assert.IsFalse((bool)viewContext.CheckCanEditTransaction(transaction),
                "Transaction should not be editable until permission is given");
            PermissionId.EditFinancialTransactions.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue((bool)viewContext.CheckCanEditTransaction(transaction),
                "Transaction should be editable");

            // Check close date
            viewContext.CloseDateTime = transaction.Date.AddDays(1);
            Assert.IsFalse(viewContext.DeleteTransaction.CanExecute(deleteTransactionArguments),
                "Can't delete transaction after CloseDateTime");
            Assert.IsFalse((bool)viewContext.CheckCanEditTransaction(transaction),
                "Transaction must not be editable after CloseDateTime");
        }

        [TestMethod]
        public void TestPostAdjustmentsViewContextSuggestAndPost()
        {
            var testData = InitializeTestData();

            // Prepare view
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PostAdjustmentsViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id,
                InvoiceId = testData.PrimaryInvoice.Id,
                PaymentInstrumentId = testData.FinancialBatch.Id
            };
            viewContext.Load.ExecuteAndWait();

            // Check we can't suggest yet
            Assert.IsFalse(viewContext.Suggest.CanExecute(), "Can't suggest until at least one billingservice is active");
            var targetBillingService = viewContext.Accounts[0].BillingServices.First(es => es.Transactions.Count > 1);
            Assert.IsTrue(targetBillingService.NextPayersSelection.Count == 2
                && targetBillingService.NextPayersSelection.Any(np => np.Payer is InsurerPayerViewModel)
                && targetBillingService.NextPayersSelection.Any(np => np.Payer is PatientPayerViewModel),
                "Billing service payers should be: 1 Insurer and 1 Patient");
            Assert.IsTrue(targetBillingService.BillingAction == null && targetBillingService.NextPayer == null,
                "No suggestion yet");

            var adjustmentTransaction = targetBillingService.Transactions.First(t => t.FinancialTransactionType is AdjustmentTypeViewModel);
            
            // Activate billing service and test
            adjustmentTransaction.Amount++;
            Assert.IsTrue(viewContext.Suggest.CanExecute(),
                "Should allow suggesting when billing service transaction is modified");
            viewContext.Suggest.ExecuteAndWait();
            Assert.IsTrue(targetBillingService.BillingAction == null && targetBillingService.NextPayer == null,
                "No suggestions without active billing transaction");

            // Create active billing transaction and try again
            var billingServiceTransaction = new BillingServiceTransaction
            {
                AmountSent = adjustmentTransaction.Amount - 1, // Old amount
                BillingServiceId = targetBillingService.Id.GetValueOrDefault(),
                InvoiceReceivableId = testData.PrimaryInvoiceReceivable.Id,

                BillingServiceTransactionStatus = BillingServiceTransactionStatus.Queued,
                DateTime = testData.CurrentDate,
                MethodSent = Model.MethodSent.Electronic
            };
            PracticeRepository.Save(billingServiceTransaction);
            viewContext.Suggest.ExecuteAndWait();

            var assertFailMessage = string.Format("Should suggest to bill current party (insurer). Instead got To:{0} Action:{1}. Target billing service IsChanged: {2}. PaymentInstrument.IsValid: {3}. AccountsSource.IsValid: {4}",
                targetBillingService.NextPayer.IfNotNull(p => p.DisplayText), 
                targetBillingService.BillingAction, 
                targetBillingService.CastTo<IEditableObjectExtended>().IsChanged,
                viewContext.PaymentInstrument.Validate().ToValidationMessageString(),
                viewContext.AccountsSource.ValidateItems().Select(k => string.Format("{0} {1}", k.Key.DisplayName, k.Value.ToValidationMessageString())).Join());

            Assert.IsTrue(targetBillingService.BillingAction != null 
                && targetBillingService.NextPayer != null
                && targetBillingService.NextPayer.Payer is InsurerPayerViewModel,
                assertFailMessage);

            Assert.IsTrue(viewContext.Post.CanExecute(),
                "Should be able to post");
            viewContext.Post.ExecuteAndWait();

            // Previous active transaction should be closed now
            billingServiceTransaction = PracticeRepository.BillingServiceTransactions.First(bst => bst.Id == billingServiceTransaction.Id);
            Assert.AreEqual(BillingServiceTransactionStatus.ClosedNotSent, billingServiceTransaction.BillingServiceTransactionStatus);
            // Check new active transaction
            billingServiceTransaction = Common.ServiceProvider.GetService<PostAdjustmentsUtilities>().GetActivePrimaryTransaction(targetBillingService.Id.GetValueOrDefault());
            Assert.AreEqual(targetBillingService.Balance, billingServiceTransaction.AmountSent);
            var adjustmentDb = PracticeRepository.Adjustments.First(a => a.Id == adjustmentTransaction.Id);
            Assert.AreEqual(adjustmentTransaction.Amount, adjustmentDb.Amount);

            // Prepare view context for another posting session
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = testData.Patient.Id,
                InvoiceId = testData.PrimaryInvoice.Id,
                PaymentInstrumentId = testData.FinancialBatch.Id
            };
            viewContext.Load.ExecuteAndWait();

            // Remove denial financial information
            targetBillingService = viewContext.Accounts[0].BillingServices.First(es => es.Transactions.Count > 1);
            var financialInformationTransaction = targetBillingService.Transactions.First(t => t.FinancialTransactionType is FinancialInformationTypeViewModel);

            PermissionId.DeleteFinancialTransactions.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.DeleteTransaction.Execute(new RemoveItemArguments
            {
                Item = financialInformationTransaction,
                Collection = targetBillingService.Transactions
            });
            viewContext.Suggest.ExecuteAndWait();
            Assert.IsTrue(targetBillingService.BillingAction != null
                && targetBillingService.NextPayer != null
                && targetBillingService.NextPayer.Payer is PatientPayerViewModel,
                "Should suggest to bill next party (patient)");

// ReSharper disable HeuristicUnreachableCode
            viewContext.Post.ExecuteAndWait();
            billingServiceTransaction = Common.ServiceProvider.GetService<PostAdjustmentsUtilities>().GetActivePrimaryTransaction(targetBillingService.Id.GetValueOrDefault());
            Assert.AreEqual(targetBillingService.Balance, billingServiceTransaction.AmountSent);
            Assert.IsNull(billingServiceTransaction.InvoiceReceivable.PatientInsuranceId,
                "Must be to patient");
// ReSharper enable HeuristicUnreachableCode
        }

        private TestDataSet InitializeTestData()
        {
            var testData = new TestDataSet();
            Common.RecreateViewAsTable<ClinicalInvoiceProvider>();

            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            testData.Patient = Common.CreatePatient();
            var insurer = Common.CreateInsurer();

            Common.AddAddresses(testData.Patient);
            Common.AddAddresses(insurer);
            insurer.IsReferralRequired = false;
            
            testData.CurrentDate = DateTime.Now.Date.AddDays(-1); // To ensure it's always a non-future date

            // Fetch existing
            var checkPaymentMethod = practiceRepository.PaymentMethods.First(pm => pm.Name == PaymentMethod.Check);
            var serviceLocationCode = practiceRepository.ServiceLocationCodes.First();
            var encounterType = practiceRepository.EncounterTypes.First();
            var serviceLocation = practiceRepository.ServiceLocations.First();
            var twoEncounterServices = practiceRepository.EncounterServices.Take(2).ToArray();
            var billingProvider = practiceRepository.Providers.First();

            var encounter = new Encounter
            {
                Patient = testData.Patient,
                EncounterType = encounterType,
                ServiceLocation = serviceLocation,
                StartDateTime = testData.CurrentDate,
                EncounterStatus = EncounterStatus.Pending,
            };

            var clinicalInvoiceProvider = new ClinicalInvoiceProvider
            {
                Encounter = encounter
            };

            testData.PrimaryInvoice = new Invoice
            {
                Encounter = encounter,
                InvoiceType = InvoiceType.Professional,
                ClinicalInvoiceProvider = clinicalInvoiceProvider,
                ServiceLocationCode = serviceLocationCode,
                BillingProvider = billingProvider
            };

            var patientInsurance = new PatientInsurance
            {
                InsuranceType = InsuranceType.MajorMedical,
                InsuredPatient = testData.Patient,
                PolicyholderRelationshipType = PolicyHolderRelationshipType.Self,
                InsurancePolicy = new InsurancePolicy
                {
                    Insurer = insurer,
                    StartDateTime = testData.CurrentDate.AddDays(-30),
                    PolicyCode = "W11564259502",
                    PolicyholderPatient = testData.Patient
                }
            };

            testData.FinancialBatch = new FinancialBatch
            {
                Amount = 80,
                CheckCode = "123",
                ExplanationOfBenefitsDateTime = testData.CurrentDate,
                FinancialSourceType = FinancialSourceType.Insurer,
                Insurer = insurer,
                PaymentMethod = checkPaymentMethod,
                PaymentDateTime = testData.CurrentDate
            };

            // On-account adjustment
            testData.Patient.Adjustments.Add(new Adjustment
            {
                AdjustmentTypeId = (int)AdjustmentTypeId.Payment,
                Amount = 20,
                PostedDateTime = testData.CurrentDate,
                FinancialSourceType = FinancialSourceType.Patient,
                FinancialBatch = testData.FinancialBatch
            });

            testData.PrimaryInvoiceReceivable = new InvoiceReceivable
            {
                Invoice = testData.PrimaryInvoice,
                OpenForReview = false,
                PatientInsurance = patientInsurance
            };

            var primaryInvoiceBillingService = new BillingService
            {
                EncounterService = twoEncounterServices[0],
                Unit = 1,
                UnitCharge = 100,
                UnitAllowableExpense = 80
            };
            primaryInvoiceBillingService.Adjustments.Add(new Adjustment
            {
                AdjustmentTypeId = (int) AdjustmentTypeId.Payment,
                Amount = 80,
                PostedDateTime = testData.CurrentDate,
                InvoiceReceivable = testData.PrimaryInvoiceReceivable,
                FinancialSourceType = FinancialSourceType.Insurer,
                FinancialBatch = testData.FinancialBatch
            });
            primaryInvoiceBillingService.FinancialInformations.Add(new FinancialInformation
            {
                FinancialInformationTypeId = (int)FinancialInformationTypeId.Denial,
                Amount = 20,
                PostedDateTime = testData.CurrentDate,
                InvoiceReceivable = testData.PrimaryInvoiceReceivable,
                FinancialSourceType = FinancialSourceType.Insurer,
                FinancialBatch = testData.FinancialBatch
            });
            testData.PrimaryInvoice.BillingServices.Add(primaryInvoiceBillingService);

            testData.PrimaryInvoice.BillingDiagnoses.Add(new BillingDiagnosis
            {
                OrdinalId = 1,
                ExternalSystemEntityMapping = PracticeRepository.ExternalSystemEntityMappings.First()
            });

            PracticeRepository.Save(testData.PrimaryInvoice);

            // Secondary invoice for tests
            testData.SecondaryInvoice = new Invoice
            {
                Encounter = encounter,
                InvoiceType = InvoiceType.Professional,
                ClinicalInvoiceProvider = clinicalInvoiceProvider,
                ServiceLocationCode = serviceLocationCode,
                BillingProvider = billingProvider,
            };
            var secondaryInvoiceBillingService = new BillingService
            {
                EncounterService = twoEncounterServices[1],
                Unit = 1,
                UnitCharge = 10, // Otherwise Invoice will not be considered open
                UnitAllowableExpense = 0
            };
            testData.SecondaryInvoice.BillingServices.Add(secondaryInvoiceBillingService);

            PracticeRepository.Save(testData.SecondaryInvoice);

            // Create non-active invoice to test they are properly filtered out
            testData.NonActiveInvoice = new Invoice
            {
                Encounter = encounter,
                InvoiceType = InvoiceType.Professional,
                ClinicalInvoiceProvider = clinicalInvoiceProvider,
                ServiceLocationCode = serviceLocationCode,
                BillingProvider = billingProvider,
            };
            var nonActiveInvoiceBillingService = new BillingService
            {
                EncounterService = twoEncounterServices[1],
                Unit = 1,
                UnitCharge = 0,
                UnitAllowableExpense = 0
            };
            testData.NonActiveInvoice.BillingServices.Add(nonActiveInvoiceBillingService);

            PracticeRepository.Save(testData.NonActiveInvoice);

            return testData;
        }

        private class TestDataSet
        {
            public DateTime CurrentDate { get; set; }

            public Invoice PrimaryInvoice { get; set; }
            public Invoice SecondaryInvoice { get; set; }
            public Invoice NonActiveInvoice { get; set; }

            public Patient Patient {get;set;}
            public FinancialBatch FinancialBatch { get; set; }
            public InvoiceReceivable PrimaryInvoiceReceivable { get; set; }
        }
    }
}
