﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.ConfigureCategories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ConfigureCategoriesPresentationTests : TestBase
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider = Common.ServiceProvider.GetService<IUnitOfWorkProvider>();

        protected override void OnAfterTestInitialize()
        {
            InitializeData();
        }

        private void InitializeData()
        {
            #region AppointmentCategories

            var category1 = new AppointmentCategory
                {
                    IsArchived = false,
                    Name = "category1",
                    HexColor = "FFFFFF"
                };
            var category2 = new AppointmentCategory
                {
                    IsArchived = false,
                    Name = "category2",
                    HexColor = "FFFFFF"
                };
            var archivedCategory = new AppointmentCategory
            {
                IsArchived = true,
                Name = "category3",
                HexColor = "FFFFFF"
            };

            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                PracticeRepository.Save(category1);
                PracticeRepository.Save(category2);
                PracticeRepository.Save(archivedCategory);
                unitOfWork.AcceptChanges();
            }

            #endregion

            #region AppointmentTypes

            int numAppointmentTypes = PracticeRepository.AppointmentTypes.Count();

            var appointmentTypes = PracticeRepository.AppointmentTypes.ToList();

            var appointmentType1 = appointmentTypes.First(at => at.Id == 1);
            appointmentType1.AppointmentCategory = category1;

            var appointmentType2 = appointmentTypes.First(at => at.Id == 2);
            appointmentType2.AppointmentCategory = category2;

            var appointmentType3 = appointmentTypes.First(at => at.Id == 3);
            appointmentType3.AppointmentCategory = archivedCategory;

            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                PracticeRepository.Save(appointmentType1);
                PracticeRepository.Save(appointmentType2);
                PracticeRepository.Save(appointmentType3);
                unitOfWork.AcceptChanges();
            }

            // Ensure same number of appointment types before and after changes
            var newAppointmentTypesCount = PracticeRepository.AppointmentTypes.Count();
            if (numAppointmentTypes != newAppointmentTypesCount)
            {
                var currentAppointmentTypes = PracticeRepository.AppointmentTypes.ToList();
                var beforeString = appointmentTypes
                    .Select(Soaf.Strings.ToJson)
                    .ToArray()
                    .Join(";");
                var afterString = currentAppointmentTypes
                    .Select(Soaf.Strings.ToJson)
                    .ToArray()
                    .Join(";");
                Assert.Fail("Appointment types count didn't match. Before -> ({0}), After -> ({1})", beforeString, afterString);
            }

            #endregion

        }

        [TestMethod]
        public void TestLoadConfigureCategoriesViewContext()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoad = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(configureCategoriesViewContext.ExistingCategories.Count == 2);
            Assert.IsTrue(configureCategoriesViewContext.AppointmentTypes.Count == 5);
        }

        [TestMethod]
        public void TestAddNewCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Add new category
            configureCategoriesViewContext.AddCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isAddCategoryComplete = () => configureCategoriesViewContext.AddCategory.CanExecute(null);
            isAddCategoryComplete.Wait(TimeSpan.FromSeconds(10));
            Assert.IsNotNull(configureCategoriesViewContext.NewAppointmentCategory);

            // Save new category
            configureCategoriesViewContext.NewAppointmentCategory.Name = "new category";
            configureCategoriesViewContext.SaveNewCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isSaveNewCategoryComplete = () => configureCategoriesViewContext.SaveNewCategory.CanExecute(null);
            isSaveNewCategoryComplete.Wait(TimeSpan.FromSeconds(10));
            Assert.IsNull(configureCategoriesViewContext.NewAppointmentCategory);
            Assert.IsTrue(configureCategoriesViewContext.ExistingCategories.Count == 3);
        }

        [TestMethod]
        public void TestRemoveNewCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Add new category
            configureCategoriesViewContext.AddCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isAddCategoryComplete = () => configureCategoriesViewContext.AddCategory.CanExecute(null);
            isAddCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            var newCategory = configureCategoriesViewContext.NewAppointmentCategory;

            configureCategoriesViewContext.RemoveNewCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isRemoveComplete = () => configureCategoriesViewContext.RemoveNewCategory.CanExecute(null);
            isRemoveComplete.Wait(TimeSpan.FromSeconds(10));
            Assert.IsNull(configureCategoriesViewContext.NewAppointmentCategory);
            Assert.IsTrue(configureCategoriesViewContext.ExistingCategories.Count == 2);
            Assert.IsFalse(configureCategoriesViewContext.AppointmentTypes.Any(at => at.AppointmentCategory == newCategory));
        }

        [TestMethod]
        public void TestAssociateAppointmentTypeToNewCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Add new category
            configureCategoriesViewContext.AddCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isAddCategoryComplete = () => configureCategoriesViewContext.AddCategory.CanExecute(null);
            isAddCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            // Create args to associate appointment type to new category
            var args = new AssociateAppointmentTypeArgs();
            args.AppointmentType = configureCategoriesViewContext.AppointmentTypes.First(at => at.Id == 4);
            args.Category = configureCategoriesViewContext.NewAppointmentCategory;

            // Associate appointment type to new category
            configureCategoriesViewContext.AssociateAppointmentTypeToNewCategory.Execute(new List<AssociateAppointmentTypeArgs> { args });
            Func<bool> isAssociateAppointmentTypeToNewCategoryComplete = () => configureCategoriesViewContext.AssociateAppointmentTypeToNewCategory.CanExecute(null);
            isAssociateAppointmentTypeToNewCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(args.AppointmentType.AppointmentCategory == args.Category);
        }

        [TestMethod]
        public void TestSaveCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Save existing category
            var categoryToSave = configureCategoriesViewContext.ExistingCategories.First();
            categoryToSave.Name = "Category for TestSaveCategory";
            configureCategoriesViewContext.SaveCategory.Execute(categoryToSave);
            Func<bool> isSaveCategoryComplete = () => configureCategoriesViewContext.SaveCategory.CanExecute(null);
            isSaveCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            var databaseCategory = PracticeRepository.AppointmentCategories.First(ac => ac.Id == categoryToSave.Id);
            Assert.IsTrue(databaseCategory.Name == "Category for TestSaveCategory");
        }

        [TestMethod]
        public void TestAssociateAppointmentType()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Create args to associate appointment type to category
            var args = new AssociateAppointmentTypeArgs();
            args.AppointmentType = configureCategoriesViewContext.AppointmentTypes.First(at => at.Id == 4);
            args.Category = configureCategoriesViewContext.ExistingCategories.First();

            // Associate appointment type to category
            configureCategoriesViewContext.AssociateAppointmentType.Execute(new List<AssociateAppointmentTypeArgs> { args });
            Func<bool> isAssociateAppointmentTypeComplete = () => configureCategoriesViewContext.AssociateAppointmentType.CanExecute(null);
            isAssociateAppointmentTypeComplete.Wait(TimeSpan.FromSeconds(10));

            var databaseAppointmentType = PracticeRepository.AppointmentTypes.WithId(args.AppointmentType.Id);
            Assert.IsTrue(databaseAppointmentType.AppointmentCategoryId == args.Category.Id);
        }

        [TestMethod]
        public void TestLoadArchivedCategories()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Load archived categories
            configureCategoriesViewContext.LoadArchivedCategories.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadArchivedCategoriesComplete = () => configureCategoriesViewContext.LoadArchivedCategories.CanExecute(null);
            isLoadArchivedCategoriesComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(configureCategoriesViewContext.ArchivedCategories.Count == 1);
        }

        [TestMethod]
        public void TestLoadAssociatedTemplates()
        {
            var databaseCategoryWithTemplates = PracticeRepository.AppointmentCategories.First();

            #region Initialize test template data

            var scheduleTemplate = new ScheduleTemplate
                {
                    Name = "ScheduleTemplate1",
                    StartTime = DateTime.Now.Date
                };

            PracticeRepository.Save(scheduleTemplate);

            var scheduleTemplateBlock = new ScheduleTemplateBlock
                {
                    Comment = "comment",
                    IsUnavailable = false,
                    ScheduleTemplateId = scheduleTemplate.Id,
                    ServiceLocationId = PracticeRepository.ServiceLocations.First().Id
                };

            PracticeRepository.Save(scheduleTemplateBlock);

            var scheduleTemplateBlockCategory = new ScheduleTemplateBlockAppointmentCategory
                {
                    AppointmentCategory = databaseCategoryWithTemplates,
                    ScheduleTemplateBlockOrdinalId = scheduleTemplateBlock.OrdinalId,
                    ScheduleTemplateBlockScheduleTemplateId = scheduleTemplate.Id,
                    ScheduleTemplateBlock = scheduleTemplateBlock,
                    Count = 1
                };

            PracticeRepository.Save(scheduleTemplateBlockCategory);

            #endregion

            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            var category = configureCategoriesViewContext.ExistingCategories.First(c => c.Id == databaseCategoryWithTemplates.Id);

            // Load associated templates with selected category
            configureCategoriesViewContext.LoadAssociatedTemplates.Execute(category);
            Func<bool> isLoadAssociatedTemplatesComplete = () => configureCategoriesViewContext.LoadAssociatedTemplates.CanExecute(null);
            isLoadAssociatedTemplatesComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(category.Templates.Count == 1);
        }

        [TestMethod]
        public void TestArchiveCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Archive selected category
            var category = configureCategoriesViewContext.ExistingCategories.First();
            configureCategoriesViewContext.CategoryToArchive = category;
            configureCategoriesViewContext.ArchiveCategory.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isArchiveCategoryComplete = () => configureCategoriesViewContext.ArchiveCategory.CanExecute(null);
            isArchiveCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(configureCategoriesViewContext.ExistingCategories.Count == 1);

            // Simulate displaying Archived Categories
            configureCategoriesViewContext.LoadArchivedCategories.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadArchivedCategoriesComplete = () => configureCategoriesViewContext.LoadArchivedCategories.CanExecute(null);
            isLoadArchivedCategoriesComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(configureCategoriesViewContext.ArchivedCategories.Count == 2);
        }

        [TestMethod]
        public void TestRestoreCategory()
        {
            // Load ViewContext
            var configureCategoriesViewContext = Common.ServiceProvider.GetService<ConfigureCategoriesViewContext>();
            configureCategoriesViewContext.Load.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadComplete = () => configureCategoriesViewContext.Load.CanExecute(null);
            isLoadComplete.Wait(TimeSpan.FromSeconds(10));

            // Simulate displaying Archived Categories
            configureCategoriesViewContext.LoadArchivedCategories.Execute(configureCategoriesViewContext.InteractionContext);
            Func<bool> isLoadArchivedCategoriesComplete = () => configureCategoriesViewContext.LoadArchivedCategories.CanExecute(null);
            isLoadArchivedCategoriesComplete.Wait(TimeSpan.FromSeconds(10));

            // Restore an archived category
            var archivedCategoryToRestore = configureCategoriesViewContext.ArchivedCategories.First();
            configureCategoriesViewContext.RestoreCategory.Execute(archivedCategoryToRestore);
            Func<bool> isRestoreArchivedCategoryComplete = () => configureCategoriesViewContext.RestoreCategory.CanExecute(null);
            isRestoreArchivedCategoryComplete.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(configureCategoriesViewContext.ExistingCategories.Count == 3);
            Assert.IsTrue(configureCategoriesViewContext.ArchivedCategories.Count == 0);
        }
    }
}
