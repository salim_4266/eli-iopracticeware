﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Presentation;
using Soaf.Security;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    public static class PresentationTestsCommon
    {
        /// <summary>
        /// Executes <see cref="ICommand"/> and waits till it completes (if it was async)
        /// </summary>
        /// <param name="target"></param>
        /// <param name="parameter"></param>
        public static void ExecuteAndWait(this ICommand target, object parameter = null)
        {
            // Run
            target.Execute(parameter);

            // Wait
            Func<bool> isLoaded = () => target.As<AsyncCommand>().IfNotNull(c => !c.IsBusy, () => target.CanExecute(null));
            isLoaded.Wait(TimeSpan.FromSeconds(15));
        }

        /// <summary>
        /// Adds permission to user runtime record (doesn't persist in DB)
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="practiceRepository"></param>
        public static void AddPermissionToCurrentUser(this PermissionId permission, IPracticeRepository practiceRepository)
        {
            var dbPermission = practiceRepository.Permissions.Single(x => x.Id == (int)permission);
            var user = (PrincipalContext.Current.Principal as UserPrincipal).IfNotNull(u => u.Identity.User);
            if (user == null)
            {
                throw new InvalidOperationException("Could not add permission to principal because principal is a {0}.".FormatWith(PrincipalContext.Current.Principal == null ? "null" : PrincipalContext.Current.Principal.GetType().FullName));
            }

            user.Roles.Add(new Soaf.Security.Role { Tag = new UserPermission { Permission = dbPermission, UserId = UserContext.Current.UserDetails.Id, PermissionId = dbPermission.Id } });
        }

        /// <summary>
        /// Converts delayed command back to underlying command
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static ICommand UnwrapDelayedCommand(this ICommand command)
        {
            var delayedCommand = command as DelayedCommand;
            if (delayedCommand == null) return command;

            var fieldInfo = delayedCommand.GetType().GetField("command", BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo == null)
            {
                throw new InvalidOperationException("DelayedCommand implementation changed. Update UnwrapDelayedCommand method");
            }

            var result = (ICommand)fieldInfo.GetValue(delayedCommand);
            return result;
        }
    }

    /// <summary>
    /// <see cref="IInteractionManager"/> which doesn't trigger any UI, but instead allow to override results using events
    /// </summary>
    public class StubInteractionManager : IInteractionManager
    {
        /// <summary>
        /// Creates instance which confirms all prompts
        /// </summary>
        /// <returns></returns>
        public static StubInteractionManager CreateAllApprovingInstance()
        {
            var stubInteractionManager = new StubInteractionManager();
            stubInteractionManager.Prompting += (sender, e) =>
            {
                e.Handled = true;
                e.Result = new PromptResult(true, string.Empty);
            };
            stubInteractionManager.Confirming += (sender, e) =>
            {
                e.Handled = true;
                e.Result = true;
            };
            return stubInteractionManager;
        }

        public void Alert(AlertArguments arguments)
        {
            if (Alerting != null)
                Alerting(this, new InteractionEventArgs<AlertArguments>(arguments));
        }

        public PromptResult Prompt(PromptArguments arguments)
        {
            var interactionEventArgs = new InteractionEventArgs<PromptArguments, PromptResult>(arguments);
            if (Prompting != null)
            {
                Prompting(this, interactionEventArgs);
            }

            return interactionEventArgs.Result;
        }

        public bool? Confirm(ConfirmationArguments arguments)
        {
            var interactionEventArgs = new InteractionEventArgs<ConfirmationArguments, bool?>(arguments);
            if (Confirming != null)
            {
                Confirming(this, interactionEventArgs);
            }

            return interactionEventArgs.Result;
        }

        public void DisplayException(ExceptionInteractionArguments arguments)
        {
            if (DisplayingException != null)
                DisplayingException(this, new HandledEventArgs<ExceptionInteractionArguments>(arguments));
        }

        public void Show(InteractionArguments arguments)
        {
            if (Showing != null)
                Showing(this, new InteractionEventArgs<InteractionArguments>(arguments));
        }

        public event EventHandler<InteractionEventArgs<AlertArguments>> Alerting;
        public event EventHandler<InteractionEventArgs<PromptArguments, PromptResult>> Prompting;
        public event EventHandler<InteractionEventArgs<ConfirmationArguments, bool?>> Confirming;
        public event EventHandler<HandledEventArgs<ExceptionInteractionArguments>> DisplayingException;
        public event EventHandler<InteractionEventArgs<InteractionArguments>> Showing;
    }
}
