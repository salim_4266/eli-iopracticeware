﻿using System;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Referrals;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ReferralTests : TestBase
    {
        protected override void OnAfterTestInitialize()
        {
            // Create a TransactionScope so any DB modifications can be rolled back.
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ModelTestsResources.ModelTestsMetadata);
        }

        [TestMethod]
        public void TestLoadReferralViewContextByReferralId()
        {
            var referral = CreateAndSaveAReferral();

            var referralsViewContext = Common.ServiceProvider.GetService<ReferralsViewContext>();
            referralsViewContext.ReferralId = referral.Id;
            referralsViewContext.AppointmentDate = referral.PatientInsurance.InsurancePolicy.StartDateTime.AddDays(1);

            // Load UI data
            referralsViewContext.Load.Execute(referralsViewContext.InteractionContext);
            Func<bool> isLoaded = () => referralsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(1500));

            Assert.IsNotNull(referralsViewContext.ReferralDetails);
        }

        [TestMethod]
        public void TestLoadReferralViewContextByPatientId()
        {
            var referral = CreateAndSaveAReferral();

            var referralsViewContext = Common.ServiceProvider.GetService<ReferralsViewContext>();
            referralsViewContext.PatientId = referral.PatientId;
            referralsViewContext.AppointmentDate = referral.PatientInsurance.InsurancePolicy.StartDateTime.AddDays(1);

            // Load UI data
            referralsViewContext.Load.Execute(referralsViewContext.InteractionContext);
            Func<bool> isLoaded = () => referralsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            Assert.IsNotNull(referralsViewContext.ReferralHistory);
            Assert.IsTrue(referralsViewContext.ReferralHistory.Any());
        }

        [TestMethod]
        public void TestViewContextIsInAcceptableSaveState()
        {
            var referral = CreateAndSaveAReferral();

            var referralViewContext = Common.ServiceProvider.GetService<ReferralsViewContext>();
            referralViewContext.ReferralId = referral.Id;
            referralViewContext.AppointmentDate = referral.PatientInsurance.InsurancePolicy.StartDateTime.AddDays(1);

            // Load UI data
            referralViewContext.Load.Execute(referralViewContext.InteractionContext);
            Func<bool> isLoaded = () => referralViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            Assert.IsFalse(referralViewContext.ButtonsCanExecute());

            // make a change
            referralViewContext.ReferralDetails.ReferralNumber = "test ref number";

            // should now be true
            Assert.IsTrue(referralViewContext.ButtonsCanExecute());
        }

        [TestMethod]
        public void TestViewContextSave()
        {
            var referral = CreateAndSaveAReferral();

            var referralViewContext = Common.ServiceProvider.GetService<ReferralsViewContext>();
            referralViewContext.ReferralId = referral.Id;
            referralViewContext.AppointmentDate = referral.PatientInsurance.InsurancePolicy.StartDateTime.AddDays(1);

            // Load UI data
            referralViewContext.Load.Execute(referralViewContext.InteractionContext);
            Func<bool> isLoaded = () => referralViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            const string referralNumber = "TESTCODE123";
            var testDateTime = new DateTime(2012, 1, 1);
            const int totalVisists = 99;

            referralViewContext.ReferralDetails.ReferralNumber = referralNumber;
            referralViewContext.ReferralDetails.StartDate = testDateTime;
            referralViewContext.ReferralDetails.TotalNumberOfVisits = totalVisists;
            referralViewContext.ReferralDetails.ExpiresDate = testDateTime;

            referralViewContext.Save.Execute(referralViewContext.InteractionContext);
            Func<bool> isSaved = () => referralViewContext.Save.CanExecute(null);
            isSaved.Wait(TimeSpan.FromSeconds(15));

            var persistedReferral = Common.PracticeRepository.PatientInsuranceReferrals.First(x => x.Id == referral.Id);
            Assert.AreEqual(referralNumber, persistedReferral.ReferralCode);
            Assert.AreEqual(testDateTime, persistedReferral.StartDateTime);
            Assert.AreEqual(testDateTime, persistedReferral.EndDateTime);
            Assert.AreEqual(totalVisists, persistedReferral.TotalEncountersCovered);
        }

        [TestMethod]
        public void TestViewContextClear()
        {
            var referral = CreateAndSaveAReferral();

            var referralsViewContext = Common.ServiceProvider.GetService<ReferralsViewContext>();
            referralsViewContext.ReferralId = referral.Id;
            referralsViewContext.AppointmentDate = referral.PatientInsurance.InsurancePolicy.StartDateTime.AddDays(1);

            // Load UI data
            referralsViewContext.Load.Execute(referralsViewContext.InteractionContext);
            Func<bool> isLoaded = () => referralsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            referralsViewContext.Clear.Execute(referralsViewContext.InteractionContext);

            Assert.IsTrue(referralsViewContext.ReferralDetails.Comments.IsNullOrEmpty());
            Assert.IsTrue(referralsViewContext.ReferralDetails.ExpiresDate == null);
            Assert.IsTrue(referralsViewContext.ReferralDetails.ExternalProviderId == null);
            Assert.IsTrue(referralsViewContext.ReferralDetails.InsuranceId == referralsViewContext.ReferralDetails.Insurances.First().Id);
            Assert.IsTrue(referralsViewContext.ReferralDetails.ReferralId == null);
            Assert.IsTrue(referralsViewContext.ReferralDetails.ReferralNumber.IsNullOrEmpty());
            Assert.IsTrue(referralsViewContext.ReferralDetails.ReferredToDoctorId == null);
            Assert.IsTrue(referralsViewContext.ReferralDetails.StartDate == null);

        }

        private PatientInsuranceReferral CreateAndSaveAReferral()
        {
            var patientInsuranceReferral = Common.CreatePatientInsuranceReferral();
            Common.PracticeRepository.Save(patientInsuranceReferral);

            var newPatientInsuranceReferral = PracticeRepository.PatientInsuranceReferrals
                .Include(x => x.PatientInsurance.InsurancePolicy)
                .Single(x => x.Id == patientInsuranceReferral.Id);


            return newPatientInsuranceReferral;
        }

    }
}
