﻿using System;
using System.Linq;
using IO.Practiceware.Presentation.Views.Utilities.Merge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class MergeEntitiesPresentationTests : X12MetadataTestsBase
    {
        [TestMethod]
        public void TestInsurerMerge()
        {
            var viewContextCreate = ServiceProvider.GetService<Func<IInteractionManager, MergeEntitiesViewContext>>();
            var viewContext = viewContextCreate(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new MergeEntitiesLoadArguments
            {
                EntityType = Practiceware.Presentation.ViewModels.Utilities.Merge.MergeEntityType.Insurer
            };

            viewContext.EntityToUseSearchText = "MEDICARE";
            viewContext.EntityToOverwriteSearchText = "MEDICARE RAILROAD";
            viewContext.SearchEntityToUse.ExecuteAndWait();
            viewContext.SearchEntityToOverwrite.ExecuteAndWait();

            viewContext.SelectedEntityToUse = viewContext.EntityToUseSelection.First();
            viewContext.SelectedEntityToOverwrite = viewContext.EntityToOverwriteSelection.First();
            int toUseId = viewContext.SelectedEntityToUse.Id;
            int toOverwriteId = viewContext.SelectedEntityToOverwrite.Id;

            Assert.IsFalse(PracticeRepository.Insurers.First(i => i.Id == toUseId).IsArchived);
            Assert.IsFalse(PracticeRepository.Insurers.First(i => i.Id == toOverwriteId).IsArchived);

            viewContext.Merge.ExecuteAndWait();
            Assert.IsFalse(PracticeRepository.Insurers.First(i => i.Id == toUseId).IsArchived);
            Assert.IsTrue(PracticeRepository.Insurers.First(i => i.Id == toOverwriteId).IsArchived);
        }

        [TestMethod]
        public void TestProviderMerge()
        {
            var viewContextCreate = ServiceProvider.GetService<Func<IInteractionManager, MergeEntitiesViewContext>>();
            var viewContext = viewContextCreate(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.LoadArguments = new MergeEntitiesLoadArguments
            {
                EntityType = Practiceware.Presentation.ViewModels.Utilities.Merge.MergeEntityType.Provider
            };

            viewContext.EntityToUseSearchText = "BARTELS";
            viewContext.EntityToOverwriteSearchText = "COX";
            viewContext.SearchEntityToUse.ExecuteAndWait();
            viewContext.SearchEntityToOverwrite.ExecuteAndWait();

            viewContext.SelectedEntityToUse = viewContext.EntityToUseSelection.First();
            viewContext.SelectedEntityToOverwrite = viewContext.EntityToOverwriteSelection.First();
            int toUseId = viewContext.SelectedEntityToUse.Id;
            int toOverwriteId = viewContext.SelectedEntityToOverwrite.Id;

            Assert.IsNotNull(PracticeRepository.ExternalContacts.FirstOrDefault(i => i.Id == toUseId));
            Assert.IsNotNull(PracticeRepository.ExternalContacts.FirstOrDefault(i => i.Id == toOverwriteId));

            viewContext.Merge.ExecuteAndWait();
            Assert.IsNotNull(PracticeRepository.ExternalContacts.FirstOrDefault(i => i.Id == toUseId));

            var providerToOverwrite = PracticeRepository.ExternalContacts.FirstOrDefault(i => i.Id == toOverwriteId);
            Assert.IsNotNull(providerToOverwrite);
            Assert.IsTrue(providerToOverwrite.IsArchived);
           
        }
    }
}
