﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Recalls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class RecallTests : TestBase
    {
     

        [TestMethod]
        public void TestLoadRecallViewContextByPatientId()
        {
            var recall = CreateAndSaveARecall();

            var recallsViewContext = Common.ServiceProvider.GetService<RecallsViewContext>();
            recallsViewContext.LoadArguments = new RecallLoadArguments
                                                   {
                                                       PatientId = recall.PatientId
                                                   };

            // Load UI data
            recallsViewContext.Load.Execute(recallsViewContext.InteractionContext);
            Func<bool> isLoaded = () => recallsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(1500));

            Assert.IsNotNull(recallsViewContext.FilteredPatientRecalls);
            Assert.IsTrue(recallsViewContext.FilteredPatientRecalls.Any());
            Assert.AreEqual(recall.RecallStatus.GetDescription(), recallsViewContext.FilteredPatientRecalls.First().Action);
            Assert.AreEqual(recall.User.UserName, recallsViewContext.FilteredPatientRecalls.First().Resource);
        } 


        [TestMethod]
        public void TestViewContextSave()
        {
            var recall = CreateAndSaveARecall();

            var recallsViewContext = Common.ServiceProvider.GetService<RecallsViewContext>();
            recallsViewContext.LoadArguments = new RecallLoadArguments
            {
                PatientId = recall.PatientId
            };

            // Load UI data
            recallsViewContext.Load.Execute(recallsViewContext.InteractionContext);
            Func<bool> isLoaded = () => recallsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(1500));
            
            var testDateTime = new DateTime(2012, 1, 1);

            recallsViewContext.PatientRecallDetail.RecallDate = testDateTime;
            
            recallsViewContext.Create.Execute(recallsViewContext.InteractionContext);
            Func<bool> isSaved = () => recallsViewContext.Create.CanExecute(null);
            isSaved.Wait(TimeSpan.FromSeconds(15));

            var persistedRecall = Common.PracticeRepository.PatientRecalls.Include(x => x.Patient).First(x => x.Id == recall.Id);

            Assert.AreEqual(testDateTime, persistedRecall.DueDateTime);
            Assert.IsTrue(recallsViewContext.FilteredPatientRecalls.Count(x => x.RecallId == recall.Id) == 1);
        }

        private PatientRecall CreateAndSaveARecall()
        {
            var patientRecall = Common.CreatePatientRecall();
            Common.PracticeRepository.Save(patientRecall);

            var newRecall = PracticeRepository.PatientRecalls
                .Include(x => x.User)
                .Include(x => x.AppointmentType)
                .Include(x => x.Patient)
                .Single(x => x.Id == patientRecall.Id);


            return newRecall;
        }


    }
}
