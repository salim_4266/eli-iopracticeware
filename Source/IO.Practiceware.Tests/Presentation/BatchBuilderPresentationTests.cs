﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using IO.Practiceware.Presentation.Views.BatchBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class BatchBuilderPresentationTests : TestBase
    {
        private readonly IInteractionContext _interactionContext = Common.ServiceProvider.GetService<IInteractionContext>();


        /// <summary>
        ///   Tests the Batch Builder Search View context..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderSearchViewContext()
        {
            var searchViewcontext = Common.ServiceProvider.GetService<BatchBuilderSearchViewContext>();
            Func<bool> loadSearchPane = () => searchViewcontext.Load.CanExecute(null);
            searchViewcontext.Load.Execute(searchViewcontext.InteractionContext);
            loadSearchPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadSearchPane());

            Assert.IsTrue(searchViewcontext.IsPatientSearchChecked);
            Assert.IsTrue(searchViewcontext.IsAppointmentSearchChecked == false);
        }

        /// <summary>
        ///   Tests the Batch Builder Template View context..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderTemplateViewContext()
        {
            var batchBuilderFiltersViewService = Common.ServiceProvider.GetService<IBatchBuilderFiltersViewService>();
            var batchBuilderResultsViewService = Common.ServiceProvider.GetService<IBatchBuilderResultsViewService>();
            var batchBuilderTemplatesViewService =
                Common.ServiceProvider.GetService<IBatchBuilderTemplatesViewService>();

            var filterViewcontext = new BatchBuilderFilterPaneViewContext(batchBuilderFiltersViewService,
                                                                          batchBuilderResultsViewService,
                                                                          batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            //Testing Load Command
            Func<bool> assertion =
                () =>
                filterViewcontext.AvailableFilters != null && filterViewcontext.DropDownFilters != null &&
                filterViewcontext.SelectedFilters.Count == 0;
            filterViewcontext.Load.Execute(filterViewcontext.InteractionContext);
            assertion.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(assertion());

            //Testing Add Filter Command
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            BatchBuilderResultsMessage resultsMessage = null;
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>(
                (t, o, m) => resultsMessage = m);
            Func<bool> resultsReceived = () => resultsMessage != null;
            //Testing Run Filters Command
            filterViewcontext.Run.Execute(filterViewcontext.InteractionContext);
            resultsReceived.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(resultsReceived());
            Assert.IsNotNull(resultsMessage);
            Assert.IsNotNull(resultsMessage.Results);

            //Testing Remove Filter Command
            filterViewcontext.RemoveFilter.Execute(filterViewcontext.SelectedFilters[1]);
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 1);

            //Testing Clear Filters Command
            filterViewcontext.Clear.Execute(null);
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 0);

            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Testing Save Batch template Command
            filterViewcontext.BatchBuilderTemplateName = "Test";

            var resultViewcontext = new BatchBuilderResultsViewContext(null) { InteractionContext = _interactionContext };

            //Testing Load Command
            Func<bool> loadResultsPane = () => resultViewcontext.Load.CanExecute(null);
            resultViewcontext.Load.Execute(_interactionContext);
            loadResultsPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadResultsPane());
            //Save templates
            int count = filterViewcontext.BatchBuilderTemplates.Count();
            Func<bool> savedTemplates =
                () =>
                filterViewcontext.BatchBuilderTemplates != null &&
                filterViewcontext.BatchBuilderTemplates.Count() > count;
            filterViewcontext.SaveBatchTemplate.Execute(filterViewcontext.InteractionContext);
            savedTemplates.Wait(TimeSpan.FromSeconds(100000));
            Assert.IsTrue(savedTemplates());
            Assert.IsNotNull(filterViewcontext.BatchBuilderTemplates);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplates.Count() == 1);

            //Load templates
            var templateViewcontext = new BatchBuilderTemplateViewContext(batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            Func<bool> loadTemplates = () => templateViewcontext.BatchBuilderTemplates != null && templateViewcontext.BatchBuilderTemplates.Count > 0;
            templateViewcontext.Load.Execute(templateViewcontext.InteractionContext);
            loadTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadTemplates());
            Assert.IsNotNull(templateViewcontext.BatchBuilderTemplates);
            Assert.IsNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(templateViewcontext.BatchBuilderTemplates.Count() == 1);
            //Select template
            templateViewcontext.SelectedBatchBuilderTemplate =
                templateViewcontext.BatchBuilderTemplates.FirstOrDefault();
            Assert.IsNotNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.Name == "Test");
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.SelectedFilters.Count > 0);

            BatchBuilderTemplateViewModel template = null;
            templateViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (t, o, m) => template = m.Template);
            Func<bool> templateChanged = () => template != null;
            //Testing Change Templates Command
            templateViewcontext.ChangeBatchTemplate.Execute(null);
            templateChanged.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(templateChanged());
            Assert.IsNotNull(template);
            Assert.IsTrue(template.Name == "Test");
            Assert.IsTrue(template.SelectedFilters.Count > 0);

            var busyCommandName = filterViewcontext.GetType().GetProperties()
                .Where(pi => pi.PropertyType.Is<ICommand>()).Select(p => new { p.Name, Value = p.GetValue(filterViewcontext, null) as AsyncCommand })
                .Where(i => i.Value != null && i.Value.IsBusy)
                .Select(i => i.Name).FirstOrDefault();

            if (busyCommandName != null) throw new Exception("Command {0} is busy.".FormatWith(busyCommandName));

            //Remove Templates
            Func<bool> removeTemplate = () => filterViewcontext.SelectedBatchBuilderTemplate == null && !templateViewcontext.BatchBuilderTemplates.Any();
            filterViewcontext.RemoveBatchTemplate.Execute(filterViewcontext.InteractionContext);
            removeTemplate.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(removeTemplate());
            Assert.IsNull(filterViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(!templateViewcontext.BatchBuilderTemplates.Any());
        }

        /// <summary>
        ///   Tests the Batch Builder Filter View context..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderFilterViewContext()
        {
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);
            var batchBuilderFiltersViewService = Common.ServiceProvider.GetService<IBatchBuilderFiltersViewService>();
            var batchBuilderResultsViewService = Common.ServiceProvider.GetService<IBatchBuilderResultsViewService>();
            var batchBuilderTemplatesViewService =
                Common.ServiceProvider.GetService<IBatchBuilderTemplatesViewService>();

            var filterViewcontext = new BatchBuilderFilterPaneViewContext(batchBuilderFiltersViewService,
                                                                          batchBuilderResultsViewService,
                                                                          batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            //Testing Load Command
            Func<bool> assertion =
                () =>
                filterViewcontext.AvailableFilters != null && filterViewcontext.DropDownFilters != null &&
                filterViewcontext.SelectedFilters.Count == 0;
            filterViewcontext.Load.Execute(filterViewcontext.InteractionContext);
            assertion.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(assertion());

            //Testing Add Filter Command
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Set user input for the filters
            foreach (BatchBuilderFilterViewModel filter in filterViewcontext.SelectedFilters)
            {
                if (filter is BatchBuilderTextFilterViewModel &&
                    filter.Name == BatchBuilderFilterNames.PatientFirstName)
                {
                    filter.CastTo<BatchBuilderTextFilterViewModel>().Text = patient.FirstName;
                }
            }

            BatchBuilderResultsMessage resultsMessage = null;
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>(
                (t, o, m) => resultsMessage = m);
            Func<bool> resultsReceived = () => resultsMessage != null;
            //Testing Run Filters Command
            filterViewcontext.Run.Execute(filterViewcontext.InteractionContext);
            resultsReceived.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(resultsReceived());
            Assert.IsNotNull(resultsMessage);
            Assert.IsNotNull(resultsMessage.Results);
            Assert.IsTrue(resultsMessage.Results.First().PatientId == patient.Id);

            //Testing Remove Filter Command
            filterViewcontext.RemoveFilter.Execute(filterViewcontext.SelectedFilters[1]);
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 1);

            //Testing Clear Filters Command
            var template = new BatchBuilderTemplateViewModel();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResetSelectedTemplateMessage>(
                (t, o, m) => template = m.Template);
            ObservableCollection<BatchBuilderResultViewModel> results = resultsMessage.Results.ToExtendedObservableCollection();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>(
                (t, o, m) => results = m.Results.ToExtendedObservableCollection());
            Func<bool> clearFilters = () => template == null;
            //Testing Change Templates Command
            filterViewcontext.Clear.Execute(null);
            clearFilters.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(clearFilters());
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 0);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplateName == "");
            Assert.IsTrue(results.Count == 0);
            Assert.IsNull(template);

            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Testing Save Batch template Command
            filterViewcontext.BatchBuilderTemplateName = "Test";
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));

            var resultViewcontext = new BatchBuilderResultsViewContext(null) { InteractionContext = _interactionContext };
            //Testing Load Command
            Func<bool> loadResultsPane = () => resultViewcontext.Load.CanExecute(null);
            resultViewcontext.Load.Execute(_interactionContext);
            loadResultsPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadResultsPane());

            int count = filterViewcontext.BatchBuilderTemplates.Count();
            Func<bool> savedTemplates =
                () =>
                filterViewcontext.BatchBuilderTemplates != null &&
                filterViewcontext.BatchBuilderTemplates.Count() > count;
            filterViewcontext.SaveBatchTemplate.Execute(filterViewcontext.InteractionContext);
            savedTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(savedTemplates());
            Assert.IsNotNull(filterViewcontext.BatchBuilderTemplates);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplates.Count() == 1);
        }

        /// <summary>
        ///   Tests the Batch Builder Result View context..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderResultViewContext()
        {
            var resultViewcontext = new BatchBuilderResultsViewContext(null) { InteractionContext = _interactionContext };

            //Testing Load Command
            Func<bool> loadResultsPane = () => resultViewcontext.Load.CanExecute(null);
            resultViewcontext.Load.Execute(resultViewcontext.InteractionContext);
            loadResultsPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadResultsPane());
            Assert.IsNotNull(resultViewcontext.Results);
            Assert.IsNotNull(resultViewcontext.SelectedResults);
            Assert.IsNotNull(resultViewcontext.CurrentBatchResults);
            Assert.IsNotNull(resultViewcontext.CurrentBatchSelectedResults);

            //var result = new BatchBuilderPatientResultViewModel { PatientId = 1, LastName = "Test", FirstName = "Abc", DateOfBirth = DateTime.Now };
            //resultViewcontext.Results.Add(result);
            //Func<bool> selectAllResults = () => resultViewcontext.SelectedResults != null;
            //resultViewcontext.SelectAll.Execute(null);
            //Assert.IsTrue(selectAllResults());
            //Assert.IsTrue(resultViewcontext.SelectedResults.Count > 0);

            ////Testing Add to Batch Command
            //resultViewcontext.AddToBatch.Execute(null);
            //Assert.IsTrue(resultViewcontext.CurrentBatchResults.Count > 0);
            //resultViewcontext.CurrentBatchSelectedResults.Add(resultViewcontext.CurrentBatchResults.First());
            //Assert.IsTrue(resultViewcontext.CurrentBatchSelectedResults.Count > 0);

            ////Testing Remove Batch Command
            //resultViewcontext.RemoveSelectedBatch.Execute(null);
            //Assert.IsTrue(resultViewcontext.CurrentBatchSelectedResults.Count == 0);

            //resultViewcontext.AddToBatch.Execute(null);
            //Assert.IsTrue(resultViewcontext.CurrentBatchResults.Count > 0);

            ////Testing clear Batch Command
            //resultViewcontext.ClearBatch.Execute(null);
            //Assert.IsTrue(resultViewcontext.CurrentBatchResults.Count == 0);
        }

        /// <summary>
        ///   Tests the Batch Builder Template Saving/ Loading / Deleting..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderTemplateViewServiceMethods()
        {
            //Load templates
            var batchBuilderTemplatesViewService =
                Common.ServiceProvider.GetService<IBatchBuilderTemplatesViewService>();
            ObservableCollection<BatchBuilderTemplateViewModel> batchBuilderTemplates =
                batchBuilderTemplatesViewService.GetBatchBuilderTemplates().ToExtendedObservableCollection();
            Assert.IsNotNull(batchBuilderTemplates);

            const string batchBuilderTemplateName = "Test";
            BatchBuilderTemplateViewModel template =
                batchBuilderTemplates.FirstOrDefault(t => t.Name == batchBuilderTemplateName) ??
                new BatchBuilderTemplateViewModel();
            template.Name = batchBuilderTemplateName;
            template.SelectedFilters = null;
            // Save template
            BatchBuilderTemplateViewModel savedTemplate =
                batchBuilderTemplatesViewService.SaveBatchBuilderTemplate(template);
            batchBuilderTemplates = batchBuilderTemplatesViewService.GetBatchBuilderTemplates().ToExtendedObservableCollection();

            Assert.IsTrue(batchBuilderTemplates.Count > 0);
            Assert.IsNotNull(savedTemplate);
            Assert.IsTrue(savedTemplate.Name == "Test");

            //Remove template
            batchBuilderTemplatesViewService.RemoveBatchBuilderTemplate(savedTemplate);
            batchBuilderTemplates = batchBuilderTemplatesViewService.GetBatchBuilderTemplates().ToExtendedObservableCollection();
            Assert.IsTrue(batchBuilderTemplates.Count == 0);
            template = batchBuilderTemplates.FirstOrDefault(t => t.Name == batchBuilderTemplateName);
            Assert.IsNull(template);
        }

        /// <summary>
        ///   Tests the Batch Builder Actions View context..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderActionsViewContext()
        {
            CreateExternalSystemExternalSystemMessageTypes();

            var batchBuilderActionsViewService = Common.ServiceProvider.GetService<IBatchBuilderActionsViewService>();
            var actionViewcontext = new BatchBuilderActionsViewContext(batchBuilderActionsViewService) { InteractionContext = _interactionContext };
            Func<bool> loadActions = () => actionViewcontext.ActionGroups != null;
            actionViewcontext.Load.Execute(actionViewcontext.InteractionContext);
            loadActions.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadActions());
            Assert.IsNotNull(actionViewcontext.ActionGroups);
            Assert.IsTrue(actionViewcontext.ActionGroups.First().Name == "Eligibility");
            actionViewcontext.InteractionContext.Messenger.Publish(new BatchBuilderSelectedSearchTypeMessage { SearchType = BatchBuilderSearchType.Appointment });
            Assert.IsNotNull(actionViewcontext.ActionGroups);
            BatchBuilderConfirmationsActionsGroupViewModel batchBuilderConfirmationActionsGroupViewModel =
                actionViewcontext.ActionGroups.OfType<BatchBuilderConfirmationsActionsGroupViewModel>().FirstOrDefault();
            Assert.IsNotNull(batchBuilderConfirmationActionsGroupViewModel);
            BatchBuilderConfirmationActionViewModel batchBuilderConfirmationActionViewModel =
                batchBuilderConfirmationActionsGroupViewModel.Actions.OfType<BatchBuilderConfirmationActionViewModel>().
                    FirstOrDefault();
            Assert.IsTrue(batchBuilderConfirmationActionViewModel != null &&
                          batchBuilderConfirmationActionViewModel.ConfirmationType == BatchBuilderConfirmationType.Email);
            Assert.IsTrue(batchBuilderConfirmationActionViewModel.IsSelected == false);
            Assert.IsTrue(batchBuilderConfirmationActionViewModel.Templates.Count > 0);
            Assert.IsNull(batchBuilderConfirmationActionViewModel.SelectedTemplate);
        }

        private static void CreateExternalSystemExternalSystemMessageTypes()
        {
            // Create  message Types and save it
            var emailMessageType = new ExternalSystemExternalSystemMessageType
                                       {
                                           TemplateFile = "EmailAppointmentReminder.doc",
                                           ExternalSystemId = (int)ExternalSystemId.Email,
                                           ExternalSystemMessageTypeId =
                                               (int)ExternalSystemMessageTypeId.Confirmation_Outbound
                                       };
            Common.PracticeRepository.Save(emailMessageType);

            var textMessageType = new ExternalSystemExternalSystemMessageType
                                      {
                                          TemplateFile = "TextMessageAppointmentReminder.doc",
                                          ExternalSystemId = (int)ExternalSystemId.TextMessage,
                                          ExternalSystemMessageTypeId =
                                              (int)ExternalSystemMessageTypeId.Confirmation_Outbound
                                      };
            Common.PracticeRepository.Save(textMessageType);
        }

        /// <summary>
        ///   Tests the Batch Builder Patient search using all View contexts..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderPatientSearch()
        {
            //Creating a patient
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);
            //Init All services
            var batchBuilderFiltersViewService = Common.ServiceProvider.GetService<IBatchBuilderFiltersViewService>();
            var batchBuilderResultsViewService = Common.ServiceProvider.GetService<IBatchBuilderResultsViewService>();
            var batchBuilderTemplatesViewService =
                Common.ServiceProvider.GetService<IBatchBuilderTemplatesViewService>();

            // Init All view Contexts
            var filterViewcontext = new BatchBuilderFilterPaneViewContext(batchBuilderFiltersViewService,
                                                                          batchBuilderResultsViewService,
                                                                          batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            var resultViewcontext = new BatchBuilderResultsViewContext(null) { InteractionContext = _interactionContext };
            var templateViewcontext = new BatchBuilderTemplateViewContext(batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };

            //Testing Load Command
            Func<bool> assertion = () =>
                                   filterViewcontext.AvailableFilters != null &&
                                   filterViewcontext.DropDownFilters != null &&
                                   filterViewcontext.SelectedFilters.Count == 0;
            filterViewcontext.Load.Execute(filterViewcontext.InteractionContext);
            assertion.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(assertion());

            //Testing Add Filter Command
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Set user input for the filters
            foreach (BatchBuilderFilterViewModel filter in filterViewcontext.SelectedFilters)
            {
                if (filter is BatchBuilderTextFilterViewModel &&
                    filter.Name == BatchBuilderFilterNames.PatientFirstName)
                {
                    filter.CastTo<BatchBuilderTextFilterViewModel>().Text = patient.FirstName;
                }
            }

            //Testing Load Command
            Func<bool> loadResultsPane = () => resultViewcontext.Load.CanExecute(null);
            resultViewcontext.Load.Execute(_interactionContext);
            loadResultsPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadResultsPane());

            Func<bool> resultsReceived = () => resultViewcontext.Results.Any();
            //Testing Run Filters Command
            filterViewcontext.Run.Execute(filterViewcontext.InteractionContext);
            resultsReceived.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(resultsReceived());
            Assert.IsTrue(resultViewcontext.Results.Any());
            Assert.IsTrue(resultViewcontext.Results.First().PatientId == patient.Id);

            //Testing Remove Filter Command
            filterViewcontext.RemoveFilter.Execute(filterViewcontext.SelectedFilters[1]);
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 1);

            //Testing Clear Filters Command
            var template1 = new BatchBuilderTemplateViewModel();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResetSelectedTemplateMessage>(
                (t, o, m) => template1 = m.Template);

            ObservableCollection<BatchBuilderResultViewModel> results =
                resultViewcontext.Results.ToExtendedObservableCollection();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>(
                (t, o, m) => results = m.Results.ToExtendedObservableCollection());
            Func<bool> clearFilters = () => template1 == null;
            //Testing Change Templates Command
            filterViewcontext.Clear.Execute(null);
            clearFilters.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(clearFilters());
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 0);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplateName == "");
            Assert.IsTrue(results.Count == 0);
            Assert.IsNull(template1);

            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Testing Save Batch template Command
            filterViewcontext.BatchBuilderTemplateName = "Test";
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));

            int count = filterViewcontext.BatchBuilderTemplates.Count();
            Func<bool> savedTemplates =
                () =>
                filterViewcontext.BatchBuilderTemplates != null &&
                filterViewcontext.BatchBuilderTemplates.Count() > count;
            filterViewcontext.SaveBatchTemplate.Execute(filterViewcontext.InteractionContext);
            savedTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(savedTemplates());
            Assert.IsNotNull(filterViewcontext.BatchBuilderTemplates);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplates.Count() == 1);

            Func<bool> loadTemplates = () => templateViewcontext.BatchBuilderTemplates != null;
            templateViewcontext.Load.Execute(templateViewcontext.InteractionContext);
            loadTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadTemplates());
            Assert.IsNotNull(templateViewcontext.BatchBuilderTemplates);
            Assert.IsNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(templateViewcontext.BatchBuilderTemplates.Count() == 1);
            //Select template
            templateViewcontext.SelectedBatchBuilderTemplate =
                templateViewcontext.BatchBuilderTemplates.FirstOrDefault();
            Assert.IsNotNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.Name == "Test");
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.SelectedFilters.Count > 0);

            BatchBuilderTemplateViewModel template2 = null;
            templateViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (t, o, m) => template2 = m.Template);
            Func<bool> templateChanged = () => template2 != null;
            //Testing Change Templates Command
            templateViewcontext.ChangeBatchTemplate.Execute(null);
            templateChanged.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(templateChanged());
            Assert.IsNotNull(template2);
            Assert.IsTrue(template2.Name == "Test");
            Assert.IsTrue(template2.SelectedFilters.Count > 0);
        }

        /// <summary>
        ///   Tests the Batch Builder Appointment search using all View contexts..
        /// </summary>
        [TestMethod]
        public void TestBatchBuilderAppointmentSearch()
        {
            //Creating an appointment
            UserAppointment appointment = Common.CreateAppointment();
            Common.PracticeRepository.Save(appointment);
            Assert.IsTrue(appointment.Id > 0);

            CreateExternalSystemExternalSystemMessageTypes();

            //Init All services
            var batchBuilderFiltersViewService = Common.ServiceProvider.GetService<IBatchBuilderFiltersViewService>();
            var batchBuilderResultsViewService = Common.ServiceProvider.GetService<IBatchBuilderResultsViewService>();
            var batchBuilderTemplatesViewService =
                Common.ServiceProvider.GetService<IBatchBuilderTemplatesViewService>();
            var batchBuilderActionsViewService = Common.ServiceProvider.GetService<IBatchBuilderActionsViewService>();

            // Init All view Contexts
            var filterViewcontext = new BatchBuilderFilterPaneViewContext(batchBuilderFiltersViewService,
                                                                          batchBuilderResultsViewService,
                                                                          batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            var resultViewcontext = new BatchBuilderResultsViewContext(null) { InteractionContext = _interactionContext };
            var searchViewcontext = new BatchBuilderSearchViewContext { InteractionContext = _interactionContext };
            var templateViewcontext = new BatchBuilderTemplateViewContext(batchBuilderTemplatesViewService) { InteractionContext = _interactionContext };
            var actionViewcontext = new BatchBuilderActionsViewContext(batchBuilderActionsViewService) { InteractionContext = _interactionContext };

            //Testing Filter view Load Command
            Func<bool> assertion =
                () =>
                filterViewcontext.AvailableFilters != null && filterViewcontext.DropDownFilters != null &&
                filterViewcontext.SelectedFilters.Count == 0;
            filterViewcontext.Load.Execute(filterViewcontext.InteractionContext);
            assertion.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(assertion());

            //Testing Result View Load Command
            Func<bool> loadResultsPane = () => resultViewcontext.Load.CanExecute(null);
            resultViewcontext.Load.Execute(_interactionContext);
            loadResultsPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadResultsPane());

            //Testing Search View Load Command
            Func<bool> loadSearchPane = () => searchViewcontext.Load.CanExecute(null);
            searchViewcontext.Load.Execute(searchViewcontext.InteractionContext);
            loadSearchPane.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadSearchPane());
            Assert.IsTrue(searchViewcontext.IsPatientSearchChecked);
            Assert.IsTrue(searchViewcontext.IsAppointmentSearchChecked == false);
            searchViewcontext.SelectAppointmentSearchType.Execute();

            Assert.IsTrue(searchViewcontext.IsAppointmentSearchChecked);

            //Testing Template View Load Command
            Func<bool> loadTemplates = () => templateViewcontext.BatchBuilderTemplates != null;
            templateViewcontext.Load.Execute(templateViewcontext.InteractionContext);
            loadTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadTemplates());
            Assert.IsNotNull(templateViewcontext.BatchBuilderTemplates);
            Assert.IsNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(!templateViewcontext.BatchBuilderTemplates.Any());

            //Testing Actions View Load Command
            Func<bool> loadActions = () => actionViewcontext.ActionGroups != null;
            actionViewcontext.Load.Execute(actionViewcontext.InteractionContext);
            loadActions.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadActions());
            Assert.IsNotNull(actionViewcontext.ActionGroups);
            actionViewcontext.InteractionContext.Messenger.Publish(new BatchBuilderSelectedSearchTypeMessage { SearchType = BatchBuilderSearchType.Appointment });
            BatchBuilderConfirmationsActionsGroupViewModel batchBuilderConfirmationActionsGroupViewModel =
                actionViewcontext.ActionGroups.OfType<BatchBuilderConfirmationsActionsGroupViewModel>().FirstOrDefault();
            Assert.IsNotNull(batchBuilderConfirmationActionsGroupViewModel);
            BatchBuilderConfirmationActionViewModel batchBuilderConfirmationActionViewModel =
                batchBuilderConfirmationActionsGroupViewModel.Actions.OfType<BatchBuilderConfirmationActionViewModel>().
                    FirstOrDefault();
            Assert.IsTrue(batchBuilderConfirmationActionViewModel != null &&
                          batchBuilderConfirmationActionViewModel.ConfirmationType == BatchBuilderConfirmationType.Email);
            Assert.IsTrue(batchBuilderConfirmationActionViewModel.IsSelected == false);
            Assert.IsTrue(batchBuilderConfirmationActionViewModel.Templates.Count > 0);
            Assert.IsNull(batchBuilderConfirmationActionViewModel.SelectedTemplate);
            batchBuilderConfirmationActionViewModel.SelectedTemplate =
                batchBuilderConfirmationActionViewModel.Templates.First();
            batchBuilderConfirmationActionsGroupViewModel.Actions.OfType<BatchBuilderConfirmationActionViewModel>().
                First().IsSelected = true;

            //Testing Add Filter Command
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Set user input for the filters
            foreach (BatchBuilderFilterViewModel filter in filterViewcontext.SelectedFilters)
            {
                if (filter is BatchBuilderTextFilterViewModel &&
                    filter.Name == BatchBuilderFilterNames.PatientFirstName)
                {
                    filter.CastTo<BatchBuilderTextFilterViewModel>().Text = appointment.Encounter.Patient.FirstName;
                }
            }

            Func<bool> resultsReceived = () => resultViewcontext.Results.Any();
            //Testing Run Filters Command
            filterViewcontext.Run.Execute(filterViewcontext.InteractionContext);
            resultsReceived.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(resultsReceived());
            Assert.IsTrue(resultViewcontext.Results.Any());

            Func<bool> selectAllResults = () => resultViewcontext.SelectedResults != null;
            resultViewcontext.SelectAll.Execute(null);
            Assert.IsTrue(selectAllResults());
            Assert.IsTrue(resultViewcontext.SelectedResults.Any());

            //Testing Add to Batch Command
            resultViewcontext.AddToBatch.Execute(null);
            Assert.IsTrue(resultViewcontext.CurrentBatchResults.Any());
            resultViewcontext.CurrentBatchSelectedResults.CastTo<IList>().Add(resultViewcontext.CurrentBatchResults.First());
            Assert.IsTrue(resultViewcontext.CurrentBatchSelectedResults.Any());

            Func<bool> transmit = () => actionViewcontext.TransmitConfirmations.CanExecute(null);
            actionViewcontext.TransmitConfirmations.Execute(actionViewcontext.InteractionContext);
            transmit.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(transmit());

            //Testing Remove Filter Command
            filterViewcontext.RemoveFilter.Execute(filterViewcontext.SelectedFilters[1]);
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 1);

            //Testing Clear Filters Command
            var template1 = new BatchBuilderTemplateViewModel();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResetSelectedTemplateMessage>(
                (t, o, m) => template1 = m.Template);

            ObservableCollection<BatchBuilderResultViewModel> results =
                resultViewcontext.Results.ToExtendedObservableCollection();
            filterViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>(
                (t, o, m) => results = m.Results.ToExtendedObservableCollection());
            Func<bool> clearFilters = () => template1 == null;
            //Testing Change Templates Command
            filterViewcontext.Clear.Execute(null);
            clearFilters.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(clearFilters());
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 0);
            Assert.IsTrue(filterViewcontext.BatchBuilderTemplateName == "");
            Assert.IsTrue(results.Count == 0);
            Assert.IsNull(template1);

            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));
            Assert.IsTrue(filterViewcontext.SelectedFilters.Count == 2);

            //Testing Save Batch template Command
            filterViewcontext.BatchBuilderTemplateName = "Test";
            filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient));
            //filterViewcontext.AddFilter.Execute(new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient));

            Func<bool> savedTemplates =
                () =>
                filterViewcontext.BatchBuilderTemplates != null &&
                filterViewcontext.BatchBuilderTemplates.Count() == 1;
            filterViewcontext.SaveBatchTemplate.Execute(filterViewcontext.InteractionContext);
            savedTemplates.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(savedTemplates());
            Assert.IsNotNull(filterViewcontext.BatchBuilderTemplates);

            Assert.IsTrue(templateViewcontext.BatchBuilderTemplates.Count() == 1);
            //Select template
            templateViewcontext.SelectedBatchBuilderTemplate =
                templateViewcontext.BatchBuilderTemplates.FirstOrDefault();
            Assert.IsNotNull(templateViewcontext.SelectedBatchBuilderTemplate);
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.Name == "Test");
            Assert.IsTrue(templateViewcontext.SelectedBatchBuilderTemplate.SelectedFilters.Count > 0);

            BatchBuilderTemplateViewModel template2 = null;
            templateViewcontext.InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (t, o, m) => template2 = m.Template);
            Func<bool> templateChanged = () => template2 != null;
            //Testing Change Templates Command
            templateViewcontext.ChangeBatchTemplate.Execute(null);
            templateChanged.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(templateChanged());
            Assert.IsNotNull(template2);
            Assert.IsTrue(template2.Name == "Test");
            Assert.IsTrue(template2.SelectedFilters.Count > 0);
        }
    }
}