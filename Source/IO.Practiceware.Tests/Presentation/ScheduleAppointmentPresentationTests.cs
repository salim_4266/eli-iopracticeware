﻿using System;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.ScheduleAppointment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Presentation;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ScheduleAppointmentPresentationTests : TestBase
    {
       
        [TestMethod]
        public void TestInitScheduleAppointmentViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleAppointment.ScheduleAppointmentMetaData);
            var scheduleAppointmentViewContext = Common.ServiceProvider.GetService<ScheduleAppointmentViewContext>();

            Assert.IsNull(scheduleAppointmentViewContext.LoadArguments);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedPatient);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedScheduleBlock);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedAppointment);

            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ExistingReferralInformation);
            Assert.IsFalse(scheduleAppointmentViewContext.Referral.IsReferralInformationRequired);

            Assert.IsNull(scheduleAppointmentViewContext.ListOfAppointmentType);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedAppointmentType);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfInsuranceType);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedInsuranceType);

            Assert.IsTrue(scheduleAppointmentViewContext.Referral.ReferralButtonMode == ReferralMode.None);
            Assert.IsTrue(scheduleAppointmentViewContext.Referral.ReferralButtonText == null);
            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ListOfAttachedReferral);
            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ListOfActiveReferral);
            Assert.IsNull(scheduleAppointmentViewContext.Referral.SelectedActiveReferral);

            Assert.IsNull(scheduleAppointmentViewContext.PatientInsuranceAuthorizationPopupViewContext);

            Assert.IsNull(scheduleAppointmentViewContext.ReasonForVisit);
            Assert.IsNull(scheduleAppointmentViewContext.Comment);
        }

        [TestMethod]
        public void TestLoadScheduleBlockScheduleAppointmentViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleAppointment.ScheduleAppointmentMetaData);
            var scheduleAppointmentViewContext = Common.ServiceProvider.GetService<ScheduleAppointmentViewContext>();

            // Simulate UI - Load Arguments
            var fromDatabaseResource = Common.PracticeRepository.Users.OfType<Doctor>().First();
            var fromDatabasePatient = Common.PracticeRepository.Patients.First();
            var fromDatabaseShortCategory = Common.PracticeRepository.AppointmentCategories.First(c => c.Name.Equals("Short Appointment", StringComparison.OrdinalIgnoreCase));
            var fromDatabaseSurgeryType = Common.PracticeRepository.AppointmentTypes.First(t => t.Name.Equals("SURGERY", StringComparison.OrdinalIgnoreCase));
            var fromDatabaseScheduleBlock = (from scheduleBlock in Common.PracticeRepository.ScheduleBlocks
                                             join scheduleBlockAppointmentCategory in Common.PracticeRepository.ScheduleBlockAppointmentCategories
                                                 on scheduleBlock.Id equals scheduleBlockAppointmentCategory.ScheduleBlockId
                                             let userScheduleBlock = scheduleBlock as UserScheduleBlock
                                             where userScheduleBlock.UserId == fromDatabaseResource.Id
                                                   && scheduleBlockAppointmentCategory.AppointmentCategoryId == fromDatabaseShortCategory.Id
                                             select userScheduleBlock).First();

            var loadArguments = new ScheduleAppointmentLoadArguments();
            loadArguments.PatientId = fromDatabasePatient.Id;
            loadArguments.PatientDisplayName = fromDatabasePatient.DisplayName;
            
            loadArguments.AppointmentCategoryId = fromDatabaseShortCategory.Id;
            loadArguments.AppointmentTypeId = fromDatabaseSurgeryType.Id;
            loadArguments.ScheduleBlockId = fromDatabaseScheduleBlock.Id;

            scheduleAppointmentViewContext.LoadArguments = loadArguments;
            scheduleAppointmentViewContext.SelectedInsuranceType = scheduleAppointmentViewContext.ListOfInsuranceType.Last();

            // Load UI data
            scheduleAppointmentViewContext.Load.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isLoad = () => !scheduleAppointmentViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(scheduleAppointmentViewContext.LoadArguments);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedPatient);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedScheduleBlock);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedAppointment);

            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ExistingReferralInformation);
            Assert.IsFalse(scheduleAppointmentViewContext.Referral.IsReferralInformationRequired);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfAppointmentType);
            Assert.IsTrue(scheduleAppointmentViewContext.SelectedAppointmentType.Id == fromDatabaseSurgeryType.Id);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfInsuranceType);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedInsuranceType);

            Assert.IsNull(scheduleAppointmentViewContext.ReasonForVisit);
            Assert.IsNull(scheduleAppointmentViewContext.Comment);
        }

        [TestMethod]
        public void TestSaveScheduleBlockScheduleAppointmentViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleAppointment.ScheduleAppointmentMetaData);
            var scheduleAppointmentViewContext = Common.ServiceProvider.GetService<ScheduleAppointmentViewContext>();

            // Simulate UI - Load Arguments
            var fromDatabaseResource = Common.PracticeRepository.Users.OfType<Doctor>().First();
            var fromDatabasePatient = Common.PracticeRepository.Patients.First();
            var fromDatabaseShortCategory = Common.PracticeRepository.AppointmentCategories.First(c => c.Name.Equals("Short Appointment", StringComparison.OrdinalIgnoreCase));
            var fromDatabaseSurgeryType = Common.PracticeRepository.AppointmentTypes.First(t => t.Name.Equals("SURGERY", StringComparison.OrdinalIgnoreCase));
            var fromDatabaseScheduleBlock = (from scheduleBlock in Common.PracticeRepository.ScheduleBlocks
                                             join scheduleBlockAppointmentCategory in Common.PracticeRepository.ScheduleBlockAppointmentCategories
                                                 on scheduleBlock.Id equals scheduleBlockAppointmentCategory.ScheduleBlockId
                                             let userScheduleBlock = scheduleBlock as UserScheduleBlock
                                             where userScheduleBlock.UserId == fromDatabaseResource.Id
                                                   && scheduleBlockAppointmentCategory.AppointmentCategoryId == fromDatabaseShortCategory.Id
                                             select userScheduleBlock).First();

            var loadArguments = new ScheduleAppointmentLoadArguments();
            loadArguments.PatientId = fromDatabasePatient.Id;
            loadArguments.PatientDisplayName = fromDatabasePatient.DisplayName;
            loadArguments.OrderAppointmentDate = fromDatabaseScheduleBlock.StartDateTime.Subtract(TimeSpan.FromDays(1));
            
            loadArguments.AppointmentCategoryId = fromDatabaseShortCategory.Id;
            loadArguments.AppointmentTypeId = fromDatabaseSurgeryType.Id;
            loadArguments.ScheduleBlockId = fromDatabaseScheduleBlock.Id;

            scheduleAppointmentViewContext.LoadArguments = loadArguments;
            scheduleAppointmentViewContext.SelectedInsuranceType = scheduleAppointmentViewContext.ListOfInsuranceType.Last();

            // Load UI data
            scheduleAppointmentViewContext.Load.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isLoad = () => !scheduleAppointmentViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoad.Wait(TimeSpan.FromSeconds(10));

            var originalNumberOfAppointments = Common.PracticeRepository.Appointments.Count();

            // Save UI data
            scheduleAppointmentViewContext.Save.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isSave = () => !scheduleAppointmentViewContext.Save.CastTo<AsyncCommand>().IsBusy;
            isSave.Wait(TimeSpan.FromSeconds(10));

            var currentNumberOfAppointments = Common.PracticeRepository.Appointments.Count();
            var maxAppointmentId = Common.PracticeRepository.Appointments.Select(a => a.Id).Max();
            // An extra appointment representing the 'OrderAppointment' was created.  (See notes below).
            // but Appointments View altered by excluding appointments with comments 'ADD VIA BILLING', 'ADD VIA OPTICAL', 'ADD VIA SURGERY ORDER'
            // We want the actual appointment we initially inserted.
            var fromDatabaseNewAppointment = Common.PracticeRepository.Appointments.OfType<UserAppointment>().First(a => a.Id == maxAppointmentId);
            var fromDatabaseNewEncounter = Common.PracticeRepository.Encounters.First(e => e.Id == fromDatabaseNewAppointment.EncounterId);

            // Scheduling an appointment whose appointment type requires an 'order' may create 2 appointments.
            // requirements:
            // 1. Insert New Appointment with AppointmentType.OrderRequired = true
            // 2. New Appointment is inserted
            // 3. No 'OrderAppointment' match is found for newly inserted Appointment
            // 3a.  OrderAppointment match is found by matching PatientId, AppointmentDate to OrderDate, and PatientClinical.FindingDetail = 'SCHEDULE SURGERY'

            // In previous code, the Appointment View was incorrectly filtering out any appointments whose time was Midnight, thus filtering this new OrderAppointment.
            // Appointments View altered by excluding appointments with comments 'ADD VIA BILLING', 'ADD VIA OPTICAL', 'ADD VIA SURGERY ORDER'
            Assert.IsTrue((originalNumberOfAppointments + 1) == currentNumberOfAppointments);
            Assert.IsTrue(fromDatabaseNewAppointment.DateTime == fromDatabaseScheduleBlock.StartDateTime);
            Assert.IsTrue(fromDatabaseNewAppointment.AppointmentTypeId == fromDatabaseSurgeryType.Id);
            Assert.IsTrue(fromDatabaseNewAppointment.UserId == fromDatabaseResource.Id);
            Assert.IsTrue(fromDatabaseNewEncounter.InsuranceTypeId == (int) scheduleAppointmentViewContext.SelectedInsuranceType.Item2);
            Assert.IsTrue(fromDatabaseNewEncounter.PatientId == fromDatabasePatient.Id);
            Assert.IsTrue(fromDatabaseNewEncounter.ServiceLocationId == fromDatabaseScheduleBlock.ServiceLocationId);
        }


        [TestMethod]
        public void TestLoadAppointmentScheduleAppointmentViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleAppointment.ScheduleAppointmentMetaData);
            var scheduleAppointmentViewContext = Common.ServiceProvider.GetService<ScheduleAppointmentViewContext>();

            // Simulate UI - Load Arguments
            var fromDatabaseAppointment = Common.PracticeRepository.Appointments.OfType<UserAppointment>().First();
            var fromDatabaseEncounter = Common.PracticeRepository.Encounters.First(e => e.Id == fromDatabaseAppointment.EncounterId);
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First(l => l.Id == fromDatabaseEncounter.ServiceLocationId);
            var fromDatabaseResource = Common.PracticeRepository.Users.OfType<Doctor>().First(d => d.Id == fromDatabaseAppointment.UserId);
            var fromDatabasePatient = Common.PracticeRepository.Patients.First(p => p.Id == fromDatabaseEncounter.PatientId);
            var fromDatabaseType = Common.PracticeRepository.AppointmentTypes.First(t => t.Id == fromDatabaseAppointment.AppointmentTypeId);

            var loadArguments = new ScheduleAppointmentLoadArguments();
            loadArguments.AppointmentId = fromDatabaseAppointment.Id;

            scheduleAppointmentViewContext.LoadArguments = loadArguments;

            // Load UI data
            scheduleAppointmentViewContext.Load.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isLoad = () => !scheduleAppointmentViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(scheduleAppointmentViewContext.LoadArguments);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedPatient);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedScheduleBlock);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedAppointment);

            Assert.IsTrue(scheduleAppointmentViewContext.SelectedPatient.Id == fromDatabasePatient.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentDetailLocationId == fromDatabaseLocation.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentDetailResourceId == fromDatabaseResource.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentStartDateTime == fromDatabaseAppointment.DateTime);

            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ExistingReferralInformation);
            Assert.IsFalse(scheduleAppointmentViewContext.Referral.IsReferralInformationRequired);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfAppointmentType);
            Assert.IsTrue(scheduleAppointmentViewContext.SelectedAppointmentType.Id == fromDatabaseType.Id);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfInsuranceType);
            Assert.IsTrue(((int) scheduleAppointmentViewContext.SelectedInsuranceType.Item2) == fromDatabaseEncounter.InsuranceTypeId);

            Assert.IsNull(scheduleAppointmentViewContext.ReasonForVisit);
            Assert.IsNull(scheduleAppointmentViewContext.Comment);
        }


        [TestMethod]
        public void TestSaveAppointmentScheduleAppointmentViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleAppointment.ScheduleAppointmentMetaData);
            var scheduleAppointmentViewContext = Common.ServiceProvider.GetService<ScheduleAppointmentViewContext>();

            // Simulate UI - Load Arguments
            var fromDatabaseAppointment = Common.PracticeRepository.Appointments.OfType<UserAppointment>().First();
            var fromDatabaseEncounter = Common.PracticeRepository.Encounters.First(e => e.Id == fromDatabaseAppointment.EncounterId);
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First(l => l.Id == fromDatabaseEncounter.ServiceLocationId);
            var fromDatabaseResource = Common.PracticeRepository.Users.OfType<Doctor>().First(d => d.Id == fromDatabaseAppointment.UserId);
            var fromDatabasePatient = Common.PracticeRepository.Patients.First(p => p.Id == fromDatabaseEncounter.PatientId);
            var fromDatabaseType = Common.PracticeRepository.AppointmentTypes.First(t => t.Id == fromDatabaseAppointment.AppointmentTypeId);

            var loadArguments = new ScheduleAppointmentLoadArguments();
            loadArguments.AppointmentId = fromDatabaseAppointment.Id;

            scheduleAppointmentViewContext.LoadArguments = loadArguments;

            // Load UI data
            scheduleAppointmentViewContext.Load.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isLoad = () => !scheduleAppointmentViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Change Insurance Information
            var insurance = scheduleAppointmentViewContext.ListOfInsuranceType.First(i => (int) i.Item2 != fromDatabaseEncounter.InsuranceTypeId);
            scheduleAppointmentViewContext.SelectedInsuranceType = insurance;

            // Save UI data
            scheduleAppointmentViewContext.Save.Execute(scheduleAppointmentViewContext.InteractionContext);
            Func<bool> isSave = () => !scheduleAppointmentViewContext.Save.CastTo<AsyncCommand>().IsBusy;
            isSave.Wait(TimeSpan.FromSeconds(10));

            var fromDatabaseUpdatedEncounter = Common.PracticeRepository.Encounters.First(e => e.Id == fromDatabaseAppointment.EncounterId);

            Assert.IsTrue(fromDatabaseEncounter.InsuranceTypeId != fromDatabaseUpdatedEncounter.InsuranceTypeId);

            Assert.IsNotNull(scheduleAppointmentViewContext.LoadArguments);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedPatient);
            Assert.IsNull(scheduleAppointmentViewContext.SelectedScheduleBlock);
            Assert.IsNotNull(scheduleAppointmentViewContext.SelectedAppointment);

            Assert.IsTrue(scheduleAppointmentViewContext.SelectedPatient.Id == fromDatabasePatient.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentDetailLocationId == fromDatabaseLocation.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentDetailResourceId == fromDatabaseResource.Id);
            Assert.IsTrue(scheduleAppointmentViewContext.AppointmentStartDateTime == fromDatabaseAppointment.DateTime);

            Assert.IsNotNull(scheduleAppointmentViewContext.Referral.ExistingReferralInformation);
            Assert.IsFalse(scheduleAppointmentViewContext.Referral.IsReferralInformationRequired);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfAppointmentType);
            Assert.IsTrue(scheduleAppointmentViewContext.SelectedAppointmentType.Id == fromDatabaseType.Id);

            Assert.IsNotNull(scheduleAppointmentViewContext.ListOfInsuranceType);
            Assert.IsTrue(((int)scheduleAppointmentViewContext.SelectedInsuranceType.Item2) == fromDatabaseUpdatedEncounter.InsuranceTypeId);

            Assert.IsNull(scheduleAppointmentViewContext.ReasonForVisit);
            Assert.IsNull(scheduleAppointmentViewContext.Comment);
        }
    }
}