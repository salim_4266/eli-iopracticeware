﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientInsurance;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Security;
using Soaf.Threading;
using PatientInsurance = IO.Practiceware.Model.PatientInsurance;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PatientInfoTests : TestBase
    {

        #region PatientInfo Main Screen Tests

        [TestMethod]
        public void TestLoadPatientInfoViewContextByPatientId()
        {
            var patient = CreateAndSavePatient();

            var patientAppointmentsViewContext = Common.ServiceProvider.GetService<PatientAppointmentsViewContext>();
            patientAppointmentsViewContext.SetPatientInfo(new PatientInfoViewModel { PatientId = patient.Id });

            AddPermissionToCurrentUser(PermissionId.ViewPatientAppointmentSummary);

            // Load UI data
            patientAppointmentsViewContext.Load.Execute(patientAppointmentsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientAppointmentsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(patientAppointmentsViewContext.PatientAppointments);
            Assert.IsTrue(patientAppointmentsViewContext.PatientAppointments.Any());
        }

        [TestMethod]
        public void TestPatientInfoSetSelectedAppointment()
        {
            var patient = CreateAndSavePatient();

            var patientInfoViewContext = Common.ServiceProvider.GetService<PatientAppointmentsViewContext>();
            patientInfoViewContext.SetPatientInfo(new PatientInfoViewModel { PatientId = patient.Id });

            AddPermissionToCurrentUser(PermissionId.ViewPatientAppointmentSummary);

            // Load UI data
            patientInfoViewContext.Load.Execute(patientInfoViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInfoViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            // set selected appointment
            patientInfoViewContext.SetSelectedAppointment.Execute(patientInfoViewContext.PatientAppointments.First());
            Func<bool> isSet = () => patientInfoViewContext.SetSelectedAppointment.CanExecute(patientInfoViewContext.PatientAppointments.First());
            isSet.Wait(TimeSpan.FromSeconds(10));

            // make sure our appointment has been set
            Assert.AreEqual(patientInfoViewContext.PatientAppointments.First(), patientInfoViewContext.SelectedAppointment);
            Assert.AreEqual(patientInfoViewContext.PatientAppointments.First().PatientAppointment.DateTime, patientInfoViewContext.SelectedAppointment.PatientAppointment.DateTime);
        }

        [TestMethod]
        public void TestPatientInfoDeleteAppointmentComment()
        {
            var patient = CreateAndSavePatient();

            var patientInfoViewContext = Common.ServiceProvider.GetService<PatientAppointmentsViewContext>();
            patientInfoViewContext.SetPatientInfo(new PatientInfoViewModel { PatientId = patient.Id });

            AddPermissionToCurrentUser(PermissionId.ViewPatientAppointmentSummary);

            // Load UI data
            patientInfoViewContext.Load.Execute(patientInfoViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInfoViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            patientInfoViewContext.DeleteComment.Execute(patientInfoViewContext.PatientAppointments.First());
            Func<bool> isDeleted = () => patientInfoViewContext.DeleteComment.CanExecute(patientInfoViewContext.PatientAppointments.First());
            isDeleted.Wait(TimeSpan.FromSeconds(10));

            // now make sure the selected appointment notes are empty(have been deleted)
            Assert.AreEqual(string.Empty, patientInfoViewContext.PatientAppointments.First().PatientAppointment.Comments);

        }

        [TestMethod]
        public void TestPatientInfoRemovedAttachedReferral()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ModelTestsResources.ModelTestsMetadata);

            var patient = CreateAndSavePatient();
            var referral = CreateAndSaveAReferral();
            referral.Patient = patient;
            referral.PatientInsurance.InsuredPatient = patient;
            referral.PatientInsurance.InsurancePolicy.PolicyholderPatient = patient;
            patient.PatientInsuranceReferrals.Add(referral);
            patient.PatientInsurances.First().InsuredPatient = patient;
            patient.PatientInsurances.First().InsurancePolicy.PolicyholderPatient = patient;
            patient.Encounters.First().PatientInsuranceReferrals.Add(referral);
            Common.PracticeRepository.Save(patient);

            var patientInfoViewContext = Common.ServiceProvider.GetService<PatientAppointmentsViewContext>();
            patientInfoViewContext.SetPatientInfo(new PatientInfoViewModel { PatientId = patient.Id });

            AddPermissionToCurrentUser(PermissionId.ViewPatientAppointmentSummary);

            // Load UI data
            patientInfoViewContext.Load.Execute(patientInfoViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInfoViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            // set selected appointment
            patientInfoViewContext.SelectedAppointment = patientInfoViewContext.PatientAppointments.First();

            // make sure ListOfAttachedReferrals is has something
            Assert.IsTrue(patientInfoViewContext.SelectedAppointment.Referral.ListOfAttachedReferral.Any());
            Assert.IsTrue(!patientInfoViewContext.SelectedAppointment.Referral.ListOfActiveReferral.Any());

            // this should take the selected attach referral and move it to the Existing list
            patientInfoViewContext.SelectedAppointment.Referral.RemoveAttachedReferral.Execute(patientInfoViewContext.SelectedAppointment.Referral.ListOfAttachedReferral.First());

            // make sure list of attached referrals is now empty      
            Assert.IsTrue(!patientInfoViewContext.SelectedAppointment.Referral.ListOfAttachedReferral.Any());
        }

        #endregion

        #region Insurance Tab Tests

        [TestMethod]
        public void TestLoadPatientInsuranceViewContextByPatientId()
        {
            var patient = CreateAndSavePatient();

            var insurance = CreateAndSaveInsurancePolicySelf(patient.Id);

            var patientInsuranceViewContext = SetupLoadViewContext(patient.Id);

            Assert.IsNotNull(patientInsuranceViewContext.PatientInsuranceViewViewModel);
            Assert.IsNotNull(patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies);
            Assert.IsTrue(patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.Any());
            Assert.IsTrue(patientInsuranceViewContext.PatientInsuranceViewViewModel.MedicareSecondaryReasonCodes.Any());
            Assert.IsTrue(patientInsuranceViewContext.PatientInsuranceViewViewModel.Relationships.Any());

            Assert.AreEqual(insurance.InsuranceType, patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().InsuranceType);
            Assert.AreEqual(insurance.InsurancePolicy.PolicyCode, patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().PolicyCode);
            Assert.AreEqual(insurance.PolicyholderRelationshipType, patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().Relationship);
            Assert.AreEqual(insurance.InsurancePolicy.StartDateTime, patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().StartDate);
            Assert.AreEqual(insurance.InsurancePolicyId, patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().InsurancePolicyId);
        }

        [TestMethod]
        public void TestSaveMedicareSecondaryReason()
        {
            var patient = CreateAndSavePatient();
            CreateAndSaveInsurancePolicySelf(patient.Id);

            var patientInsuranceViewContext = SetupLoadViewContext(patient.Id);

            patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().IsMedicarePolicy = true;
            patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().PolicyRank = 2;
            patientInsuranceViewContext.PatientInsuranceViewViewModel.SelectedMedicareSecondaryReasonCode = (int)MedicareSecondaryReasonCode.WorkersCompensation;

            patientInsuranceViewContext.MedicareSecondaryReasonChanged.Execute(patientInsuranceViewContext.InteractionContext);
            patientInsuranceViewContext.LastTaskCreated.WaitIfNotNullOrDone();

            var policy = Common.PracticeRepository.InsurancePolicies.Single(x => x.Id == patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First().InsurancePolicyId);

            Assert.AreEqual(patientInsuranceViewContext.PatientInsuranceViewViewModel.SelectedMedicareSecondaryReasonCode, (int)policy.MedicareSecondaryReasonCode.EnsureNotDefault());
        }

        [TestMethod]
        public void TestDeletePolicyWhenRelationshipIsSelfAndHasNoAdjustments()
        {
            var patient = CreateAndSavePatient();
            var insurance = CreateAndSaveInsurancePolicySelf(patient.Id);

            var patientInsuranceViewContext = SetupLoadViewContext(patient.Id);

            var policyViewModel = patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First();

            AddPermissionToCurrentUser(PermissionId.DeletePatientInsurance);

            patientInsuranceViewContext.DeletePolicy.Execute(policyViewModel);
            patientInsuranceViewContext.LastTaskCreated.WaitIfNotNullOrDone();

            var policy = Common.PracticeRepository.PatientInsurances.Single(x => x.Id == insurance.Id);

            Assert.IsTrue(policy.IsDeleted);
            Assert.IsTrue(policyViewModel.IsDeleted);
        }

        private static void AddPermissionToCurrentUser(PermissionId permission)
        {
            var dbPermission = Common.PracticeRepository.Permissions.Single(x => x.Id == (int)permission);
            var user = (PrincipalContext.Current.Principal as UserPrincipal).IfNotNull(u => u.Identity.User);
            user.Roles.Add(new Soaf.Security.Role { Tag = new UserPermission { Permission = dbPermission, UserId = UserContext.Current.UserDetails.Id, PermissionId = dbPermission.Id } });
        }

        [TestMethod]
        public void TestDeletePatientInsurancePhoto()
        {
            var patient = CreateAndSavePatient();
            var insurance = CreateAndSaveInsurancePolicySelf(patient.Id);

            var patientInsuranceViewContext = SetupLoadViewContext(patient.Id);
            var policyViewModel = patientInsuranceViewContext.PatientInsuranceViewViewModel.Policies.First();

            Assert.IsTrue(insurance.PatientInsuranceDocuments.Any());
            Assert.IsTrue(policyViewModel.PolicyDetails.AttachedImages.Any());

            AddPermissionToCurrentUser(PermissionId.DeletePatientInsuranceDocument);

            patientInsuranceViewContext.DeleteImage.Execute(policyViewModel.PolicyDetails.AttachedImages.First());
            patientInsuranceViewContext.LastTaskCreated.WaitIfNotNullOrDone();

            var imageExists = Common.PracticeRepository.PatientInsuranceDocuments.Any(x => x.Id == insurance.PatientInsuranceDocuments.First().Id);

            Assert.IsFalse(imageExists);
            Assert.IsFalse(policyViewModel.PolicyDetails.AttachedImages.Any());
        }

        private static PatientInsuranceViewContext SetupLoadViewContext(int patientId)
        {
            var patientInsuranceViewContext = Common.ServiceProvider.GetService<PatientInsuranceViewContext>();
            patientInsuranceViewContext.PatientInfoViewModel = new PatientInfoViewModel { PatientId = patientId };

            // Load UI data
            patientInsuranceViewContext.Load.Execute(patientInsuranceViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInsuranceViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            return patientInsuranceViewContext;
        }

        #endregion

        #region Communications Tab Tests

        [TestMethod]
        public void TestLoadPatientCommunicationPreferenceViewContextByPatientId()
        {
            var patient = CreateAndSavePatient();
            var patient2 = CreateAndSavePatient();

            CreateAndSavePatientRelationship(patient, patient2);
            var preference = CreateAndSavePatientCommunicationPreference(patient);

            var patientCommunicationPreferencesViewContext = SetupPatientCommunicationPreferencesLoadViewContext(patient.Id);

            Assert.IsNotNull(patientCommunicationPreferencesViewContext.Contacts);
            Assert.IsNotNull(patientCommunicationPreferencesViewContext.PatientCommunicationPreferences);
            Assert.IsTrue(patientCommunicationPreferencesViewContext.Contacts.Any());
            Assert.IsTrue(patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Any());

            // assert the ordering
            var expectedPreferencesOrdering = Enums.GetValues<PatientCommunicationType>().OrderByAttributeOrder();
            var actualPreferencesOrdering = patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Select(p => p.CommunicationType);

            Assert.IsTrue(expectedPreferencesOrdering.ToList().SequenceEqual(actualPreferencesOrdering), "The actual communication preferences ordering were not the same as the expected communication preferences");            
        }

        [TestMethod]
        public void TestUnlinkContactInPatientCommunicationPreferenceViewContext()
        {
            var patient = CreateAndSavePatient();
            var patient2 = CreateAndSavePatient();

            CreateAndSavePatientRelationship(patient, patient2);
            CreateAndSavePatientCommunicationPreference(patient);

            var patientCommunicationPreferencesViewContext = SetupPatientCommunicationPreferencesLoadViewContext(patient.Id);

            Assert.AreEqual(patientCommunicationPreferencesViewContext.Contacts.Count, 2);
            PermissionId.UnLinkContact.AddPermissionToCurrentUser(PracticeRepository);
            patientCommunicationPreferencesViewContext.UnlinkContact.Execute(patientCommunicationPreferencesViewContext.Contacts.First(x => x.PatientId != patient.Id));
            Assert.AreEqual(patientCommunicationPreferencesViewContext.Contacts.Count, 1);
        }

        [TestMethod]
        public void TestAddNewPreferenceInPatientCommunicationPreferenceViewContext()
        {
            var patient = CreateAndSavePatient();
            var patient2 = CreateAndSavePatient();

            CreateAndSavePatientRelationship(patient, patient2);
            CreateAndSavePatientCommunicationPreference(patient);

            var patientCommunicationPreferencesViewContext = SetupPatientCommunicationPreferencesLoadViewContext(patient.Id);

            var currentCount = patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Count;
            patientCommunicationPreferencesViewContext.AddContactPreferenceForCommunicationType.Execute(patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.First());
            Assert.AreEqual(currentCount + 1, patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Count);
        }

        [TestMethod]
        public void TestRemovePreferenceInPatientCommunicationPreferenceViewContext()
        {
            var patient = CreateAndSavePatient();
            var patient2 = CreateAndSavePatient();

            CreateAndSavePatientRelationship(patient, patient2);
            CreateAndSavePatientCommunicationPreference(patient);

            var patientCommunicationPreferencesViewContext = SetupPatientCommunicationPreferencesLoadViewContext(patient.Id);

            var currentCount = patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Count;
            patientCommunicationPreferencesViewContext.RemovePreference.Execute(patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.First());
            Assert.AreEqual(currentCount - 1, patientCommunicationPreferencesViewContext.PatientCommunicationPreferences.Count);
        }

        [TestMethod]
        public void TestSaveAllPreferencesInPatientCommunicationPreferenceViewContext()
        {
            var patient = CreateAndSavePatient();
            var patient2 = CreateAndSavePatient();

            var relationship = CreateAndSavePatientRelationship(patient, patient2);
            CreateAndSavePatientCommunicationPreference(patient);

            var patientCommunicationPreferencesViewContext = SetupPatientCommunicationPreferencesLoadViewContext(patient.Id);

            Assert.IsFalse(patientCommunicationPreferencesViewContext.Contacts.First().Hippa);
            patientCommunicationPreferencesViewContext.Contacts.First(x => x.PatientId == patient2.Id).Hippa = true;
            patientCommunicationPreferencesViewContext.SaveAllPreferences.Execute(patientCommunicationPreferencesViewContext.InteractionContext);

            var newRelationship = Common.PracticeRepository.PatientRelationships.Single(x => x.Id == relationship.Id);
            Assert.IsTrue(newRelationship.HasSignedHipaa);

        }

        private static PatientCommunicationPreferencesViewContext SetupPatientCommunicationPreferencesLoadViewContext(int patientId)
        {
            var patientCommunicationPreferencesViewContext = Common.ServiceProvider.GetService<PatientCommunicationPreferencesViewContext>();
            patientCommunicationPreferencesViewContext.PatientInfoViewModel = new PatientInfoViewModel { PatientId = patientId };

            // Load UI data
            patientCommunicationPreferencesViewContext.Load.Execute(patientCommunicationPreferencesViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientCommunicationPreferencesViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            return patientCommunicationPreferencesViewContext;
        }

        #endregion

        #region Demographics Tab Tests

        [TestMethod]        
        public void TestLoadPatientDemographicsViewContextByPatientId()
        {
            var patient = CreateAndSaveFullPatient();

            var patientDemographicsViewContext = SetupPatientDemographicsLoadViewContext(patient.Id);

            Assert.IsNotNull(patientDemographicsViewContext.PatientData);
            Assert.IsNotNull(patientDemographicsViewContext.PatientDemographicsViewViewModel);
            Assert.IsTrue(patientDemographicsViewContext.PatientData.EmailAddresses.Any());
            Assert.IsTrue(patientDemographicsViewContext.PatientData.Addresses.Any());
            Assert.IsTrue(patientDemographicsViewContext.PatientData.PhoneNumbers.Any());
        }

        public void TestSavePatientDemographicsPatientData()
        {
            var patient = CreateAndSaveFullPatient();

            var patientDemographicsViewContext = SetupPatientDemographicsLoadViewContext(patient.Id);

            const string firstName = "Test random first name here";
            patientDemographicsViewContext.PatientData.FirstName = firstName;

            patientDemographicsViewContext.Save.Execute(patientDemographicsViewContext.InteractionContext);

            var savedPatient = Common.PracticeRepository.Patients.SingleOrDefault(x => x.Id == patient.Id);

            Assert.AreEqual(firstName, savedPatient.EnsureNotDefault().FirstName);
        }

        private static PatientDemographicsViewContext SetupPatientDemographicsLoadViewContext(int patientId)
        {
            var patientDemographicsViewContext = Common.ServiceProvider.GetService<PatientDemographicsViewContext>();
            patientDemographicsViewContext.SetPatientInfo(new PatientInfoViewModel { PatientId = patientId });

            // Load UI data
            patientDemographicsViewContext.Load.Execute(patientDemographicsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientDemographicsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            return patientDemographicsViewContext;
        }

        #endregion

        #region Helper Methods

        private static int _pidIdentity = 1;

        private static Patient CreateAndSavePatient()
        {
            var app = Common.CreateAppointment();

            var userUniquePid = string.Format("-{0:D3}", Interlocked.Increment(ref _pidIdentity));
            app.User.UserName = "tst" + userUniquePid;
            app.User.Pid = userUniquePid;

            Common.PracticeRepository.Save(app.Encounter.Patient);

            var newPatient = Common.PracticeRepository.Patients
                .Include(x => x.Encounters.Select(z => z.Appointments))
                .Include(x => x.Encounters.Select(y => y.PatientInsuranceReferrals))
                .Single(x => x.Id == app.Encounter.Patient.Id);

            return newPatient;
        }

        /// <summary>
        /// Returns an almost fully fleshed patient (almost all test data filled in).  Good for testing patient demographics
        /// </summary>
        /// <returns></returns>
        private static Patient CreateAndSaveFullPatient()
        {
            var patient = Common.CreateFullPatient();

            Common.PracticeRepository.Save(patient);

            return patient;
        }

        private static PatientInsuranceReferral CreateAndSaveAReferral()
        {
            var patientInsuranceReferral = Common.CreatePatientInsuranceReferral();

            Common.PracticeRepository.Save(patientInsuranceReferral);

            var newPatientInsuranceReferral = Common.PracticeRepository.PatientInsuranceReferrals
                .Include(x => x.PatientInsurance.InsurancePolicy)
                .Include(x => x.Patient)
                .Single(x => x.Id == patientInsuranceReferral.Id);


            return newPatientInsuranceReferral;
        }

        private PatientInsurance CreateAndSaveInsurancePolicySelf(int patientId)
        {
            var patient = Common.PracticeRepository.Patients.Single(x => x.Id == patientId);

            var insurance = CreatePatientInsuranceSelf(patient);

            if (insurance.PatientInsuranceDocuments == null)
            {
                insurance.PatientInsuranceDocuments = new List<PatientInsuranceDocument>();
            }

            insurance.PatientInsuranceDocuments.Add(new PatientInsuranceDocument { PatientInsurance = insurance, Content = new byte[] { 2, 2, 255, 255 } });

            Common.PracticeRepository.Save(insurance);

            var savedInsurance = PracticeRepository
              .PatientInsurances
              .Include(x => x.InsurancePolicy.Insurer)              
              .Include(x => x.PatientInsuranceDocuments)
              .Single(x => x.Id == insurance.Id);

            return savedInsurance;
        }

        private static PatientInsurance CreatePatientInsuranceSelf(Patient patient)
        {
            patient = patient ?? Common.CreatePatient();

            var patientInsurance = new PatientInsurance();
            patientInsurance.EndDateTime = new DateTime(2012, 1, 1);
            patientInsurance.InsurancePolicy = new InsurancePolicy { StartDateTime = new DateTime(2009, 1, 1), Insurer = Common.CreateInsurer(), PolicyholderPatient = patient, PolicyCode = "TESTPOLICYCODE" };
            patientInsurance.InsuranceType = InsuranceType.MajorMedical;
            patientInsurance.InsuredPatient = patient;
            patientInsurance.PolicyholderRelationshipType = PolicyHolderRelationshipType.Self;

            return patientInsurance;
        }

        private static PatientRelationship CreateAndSavePatientRelationship(Patient patientFrom, Patient patientTo)
        {
            var relationship = new PatientRelationship();
            relationship.FromPatientId = patientFrom.Id;
            relationship.ToPatientId = patientTo.Id;

            Common.PracticeRepository.Save(relationship);

            return relationship;
        }

        private static PatientCommunicationPreference CreateAndSavePatientCommunicationPreference(Patient patient)
        {
            var pa = Common.CreatePatientFirstAddress(patient);

            Common.PracticeRepository.Save(pa);

            var preference = new PatientAddressCommunicationPreference();
            preference.PatientId = patient.Id;
            preference.PatientAddressId = pa.Id;
            preference.PatientCommunicationType = PatientCommunicationType.Recall;
            preference.CommunicationMethodType = CommunicationMethodType.Letter;

            Common.PracticeRepository.Save(preference);

            return preference;
        }

        #endregion
    }

    public static class PatientInfoTestExtensions
    {
        public static void WaitIfNotNullOrDone(this System.Threading.Tasks.Task task, int? milliSecondsToWait = null)
        {
            if (task == null || task.IsCanceled || task.IsCompleted || task.IsFaulted)
            {
                return;
            }

            if (milliSecondsToWait.HasValue) task.Wait(milliSecondsToWait.Value);
            else task.Wait();
        }
    }
}
