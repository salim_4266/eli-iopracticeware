﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Alerts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class AlertsViewTests : X12MetadataTestsBase
    {

        public void InitializeTestData()
        {
            //Creating new comments for alerts.
            var invoiceComment = new InvoiceComment
            {
                IncludeOnStatement = true,
                InvoiceId = 1,
                IsArchived = false,
                Value = "Test invoice Comment",
                DateTime = DateTime.Now.ToClientTime()
            };

            var billingServiceComment = new BillingServiceComment
            {
                IsIncludedOnStatement = true,
                BillingServiceId = 1,
                IsArchived = false,
                Value = "Test billing Comment",
                DateTime = DateTime.Now.ToClientTime()
            };

            var patientFinancialComment = new PatientFinancialComment
            {
                IsIncludedOnStatement = true,
                PatientId = 1,
                IsArchived = false,
                Value = "Test financial Comment"
            };

            PracticeRepository.Save(billingServiceComment);
            PracticeRepository.Save(invoiceComment);
            PracticeRepository.Save(patientFinancialComment);

            //Creating Alerts.
            var screens = PracticeRepository.Screens.Where(x => x.ScreenType == ScreenType.Financials);

            var alerts = new List<Alert>();
            var financialAlert = new Alert
            {
                PatientFinancialCommentId = patientFinancialComment.Id,
                ExpirationDateTime = DateTime.Now.AddDays(5),
                Screens = screens.ToArray()
            };
            alerts.Add(financialAlert);

            var screen = screens.First(x => x.Id == 9);
            var invoiceAlert = new Alert
            {
                InvoiceCommentId = invoiceComment.Id,
                ExpirationDateTime = DateTime.Now.AddDays(5),
            };
            invoiceAlert.Screens.Add(screen);
            alerts.Add(invoiceAlert);

            screen = screens.First(x => x.Id == 9);
            var editScreenAlert = new Alert
            {
                BillingServiceCommentId = billingServiceComment.Id,
                ExpirationDateTime = DateTime.Now.AddDays(5),
            };
            editScreenAlert.Screens.Add(screen);
            alerts.Add(editScreenAlert);

            alerts.ForEach(PracticeRepository.Save);

        }

        [TestMethod]
        public void TestAlertViewLoadSaveAndDeactivateForInvoice()
        {
            InitializeTestData();

            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, AlertViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

            //Loading alerts for Edit Invoice Screen
            viewContext.LoadArguments = new NoteLoadArguments();
            viewContext.LoadArguments.AlertScreen = AlertScreen.EditAndBill;
            viewContext.LoadArguments.NoteType = NoteType.InvoiceComment;
            viewContext.LoadArguments.EntityId = 1;

            viewContext.Load.Execute();
            Assert.AreEqual(viewContext.Alerts.Count, 3);

            //Edit Billing service comment from alert
            var billinServiceAlert = viewContext.Alerts.First(x => x.NoteType == NoteType.BillingServiceComment);
            billinServiceAlert.Value = "New Billing Service Comment";
            billinServiceAlert.IsIncludedOnStatement = false;

            viewContext.UpdateAlerts();

            var billingServiceComment = PracticeRepository.BillingServiceComments.First(x => x.Id == billinServiceAlert.Id);
            Assert.AreEqual(billinServiceAlert.Value, billingServiceComment.Value);
            Assert.AreEqual(billinServiceAlert.IsIncludedOnStatement, billingServiceComment.IsIncludedOnStatement);


            //Updating screen through invoice comment
            var invoiceServiceAlert = viewContext.Alerts.FirstOrDefault(x => x.NoteType == NoteType.InvoiceComment);
            Assert.IsNotNull(invoiceServiceAlert);
            invoiceServiceAlert.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 8));

            viewContext.Update.Execute();

            var screens = PracticeRepository.Alerts.Where(x => x.Id == invoiceServiceAlert.AlertId).SelectMany(x => x.Screens).ToArray();
            Assert.AreEqual(screens.Count(), invoiceServiceAlert.AlertableScreens.Count);

            //Deleting an alert by deselecting all the screens
            invoiceServiceAlert.AlertableScreens.Clear();
            viewContext.UpdateAlerts();

            var alert = PracticeRepository.Alerts.FirstOrDefault(x => x.Id == invoiceServiceAlert.AlertId);
            Assert.IsNull(alert);

            //Delete all the alerts
            viewContext.DeactivateAllAlerts();

            var alertIds = viewContext.Alerts.Select(a => a.AlertId).ToArray();
            var invoiceScreenAlert = PracticeRepository.Alerts.Where(x => alertIds.Contains(x.Id)).ToArray();

            Assert.AreEqual(invoiceScreenAlert.Count(), 0);
        }

        [TestMethod]
        public void TestAlertViewLoadSaveAndDeactivateForPatient()
        {

            InitializeTestData();

            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, AlertViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

            //Loading alerts for patient Screen
            viewContext.LoadArguments = new NoteLoadArguments();
            viewContext.LoadArguments.AlertScreen = AlertScreen.PatientFinancials;
            viewContext.LoadArguments.NoteType = NoteType.PatientFinancialComment;
            viewContext.LoadArguments.EntityId = 1;

            viewContext.Load.Execute();
            Assert.AreEqual(viewContext.Alerts.Count, 1);

            //Patient financial comment from alert
            var patientFinancialAlert = viewContext.Alerts.First(x => x.NoteType == NoteType.PatientFinancialComment);
            patientFinancialAlert.Value = "New patient Comment";
            patientFinancialAlert.IsIncludedOnStatement = false;

            viewContext.UpdateAlerts();

            var patientFinancialComment = PracticeRepository.PatientFinancialComments.First(x => x.Id == patientFinancialAlert.Id);
            Assert.AreEqual(patientFinancialAlert.Value, patientFinancialComment.Value);
            Assert.AreEqual(patientFinancialAlert.IsIncludedOnStatement, patientFinancialComment.IsIncludedOnStatement);

            //Deleting an alert by deselecting all the screens
            patientFinancialAlert.AlertableScreens.Clear();
            viewContext.UpdateAlerts();

            var alert = PracticeRepository.Alerts.FirstOrDefault(x => x.Id == patientFinancialAlert.AlertId);
            Assert.IsNull(alert);

            //Delete all the alerts
            viewContext.DeactivateAllAlerts();

            var alertIds = viewContext.Alerts.Select(a => a.AlertId).ToArray();
            var invoiceScreenAlert = PracticeRepository.Alerts.Where(x => alertIds.Contains(x.Id)).ToArray();

            Assert.AreEqual(invoiceScreenAlert.Count(), 0);
        }
    }
}


