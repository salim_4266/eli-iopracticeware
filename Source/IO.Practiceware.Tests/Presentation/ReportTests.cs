﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Reporting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Security;
using Telerik.Reporting;
using Telerik.Reporting.XmlSerialization;
using Soaf;
using Report = Telerik.Reporting.Report;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ReportTests : X12MetadataTestsBase
    {
        [TestMethod]
        public void TestRunReports()
        {
            AddPermissionToCurrentUser("Aging");
            AddPermissionToCurrentUser("Credit Balances");
            AddPermissionToCurrentUser("Daily Adjustments");
            AddPermissionToCurrentUser("Daily Deposits");
            AddPermissionToCurrentUser("Daily Patient Payments");
            AddPermissionToCurrentUser("Daily Schedule");
            AddPermissionToCurrentUser("Glasses Receipt");
            AddPermissionToCurrentUser("Patient Balances Grouped By Interval");
            AddPermissionToCurrentUser("Patient Receipt");
            AddPermissionToCurrentUser("Daily Payments");
            AddPermissionToCurrentUser("Adjustments with MTD and YTD Analysis");
            AddPermissionToCurrentUser("Appointments Forced");
            AddPermissionToCurrentUser("Charges, Payments, and Adjustments Summary");
            AddPermissionToCurrentUser("Daily Charges");
            AddPermissionToCurrentUser("On Account Payments");
            AddPermissionToCurrentUser("Open Unbilled Invoices");
            AddPermissionToCurrentUser("Patient Account History");
            AddPermissionToCurrentUser("Patient Statement");
            AddPermissionToCurrentUser("Patients by Insurer, Diagnosis, and Service");
            AddPermissionToCurrentUser("Payments with MTD and YTD Analysis");
            AddPermissionToCurrentUser("Adjustments (grid)");
            AddPermissionToCurrentUser("Appointments (grid)");
            AddPermissionToCurrentUser("Last Visit (grid)");
            AddPermissionToCurrentUser("TimeSheet");
            AddPermissionToCurrentUser("Patient Bill Hold");
            AddPermissionToCurrentUser("Charges (grid)");
            AddPermissionToCurrentUser("Payments (grid)");
            AddPermissionToCurrentUser("Charges, Payments, and Adjustments Detail");
            AddPermissionToCurrentUser("Charges, Payments, and Adjustments Detail By Transaction");
            AddPermissionToCurrentUser("Payments By Primary Insurer");
            AddPermissionToCurrentUser("Referring Doctor Detail");
            AddPermissionToCurrentUser("Patients Without Future Appointments Or Recalls");
            AddPermissionToCurrentUser("Charges with YTD Analysis");
            AddPermissionToCurrentUser("Glasses Inventory");
            AddPermissionToCurrentUser("Informational Postings (grid)");
            AddPermissionToCurrentUser("Unassigned Payments");

            var reportService = Common.ServiceProvider.GetService<IReportService>();
            var reports = reportService.GetReports().Select(r => reportService.GetReportContent(r.Name)).ToArray().OfType<TelerikReportContent>();
            reports.AsParallel().WithDegreeOfParallelism(8).ForAll(report =>
            {
                var parameters = new Hashtable();
                using (var preparedReport = PrepareReport(report, parameters))
                {
                    var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                    var result = reportProcessor.RenderReport("PDF", new InstanceReportSource { ReportDocument = preparedReport }, null);
                    Assert.AreEqual(0, result.Errors.Count());
                }
            });
            
        }

        [TestMethod]
        public void RunTestStoredProcedures()
        {
            using (var connection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
            {
                connection.RunScript(Properties.Resources.TestStoredProcedures);
            }
        }


        private static void AddPermissionToCurrentUser(string permission)
        {
            var dbPermission = Common.PracticeRepository.Permissions.Single(x => x.Name == permission);
            var user = (PrincipalContext.Current.Principal as UserPrincipal).IfNotNull(u => u.Identity.User);
            user.Roles.Add(new Soaf.Security.Role { Name = dbPermission.Name, Tag = new UserPermission { Permission = dbPermission, UserId = UserContext.Current.UserDetails.Id, PermissionId = dbPermission.Id } });
        }

        private static Report PrepareReport(TelerikReportContent reportContent, Hashtable parameters)
        {
            using (var ms = new MemoryStream(reportContent.CastTo<TelerikReportContent>().Content))
            {
                var report = (Report)(new ReportXmlSerializer().Deserialize(ms));
                // Assign passed in parameter values
                if (parameters != null)
                {
                    foreach (var reportParameter in report.ReportParameters.ToList())
                    {
                        if (reportParameter.Name == "StartDate")
                        {
                            reportParameter.Value = DateTime.Now;
                            //reportParameter.Value = "2010-01-01".ToDateTime();
                        }
                        else if (reportParameter.Name == "EndDate")
                        {
                            reportParameter.Value = DateTime.Now;
                        }
                        else if (reportParameter.Type == ReportParameterType.Boolean)
                        {
                            reportParameter.Value = true;
                        }
                        else if (reportParameter.Type == ReportParameterType.Integer)
                        {
                            reportParameter.Value = 1;
                        }

                        else reportParameter.Value = parameters[reportParameter.Name];
                        parameters.Add(reportParameter.Name, reportParameter.Value);
                    }
                }

                return report;
            }
        }
    }
}
