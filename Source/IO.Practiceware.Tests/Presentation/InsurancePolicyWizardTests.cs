﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.Views.InsurancePolicy;
using IO.Practiceware.Presentation.Views.NonClinicalPatient;
using IO.Practiceware.Services.PatientSearch;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class InsurancePolicyWizardTests : TestBase
    {
        
        #region EnterPolicyDetails

        [TestMethod]
        public void TestEnterPolicyViewContextLoad()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var policyholder = Common.CreatePatient2();
            PracticeRepository.Save(policyholder);

            var viewContext = Common.ServiceProvider.GetService<EnterPolicyDetailsViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.InsurerId = 1;
            interactionContext.Context.PatientId = patient.Id;
            interactionContext.Context.PolicyholderId = policyholder.Id;

            viewContext.InteractionContext = interactionContext;
            viewContext.Load.Execute();

            Assert.IsNotNull(viewContext.InsurancePolicy);
            Assert.IsNotNull(viewContext.Insurer);
            Assert.IsNotNull(viewContext.Policyholder);
        }

        [TestMethod]
        public void TestEnterPolicyViewContextSave()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var policyholder = Common.CreatePatient2();
            PracticeRepository.Save(policyholder);

            var viewContext = Common.ServiceProvider.GetService<EnterPolicyDetailsViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.InsurerId = 1;
            interactionContext.Context.PatientId = patient.Id;
            interactionContext.Context.PolicyholderId = policyholder.Id;

            viewContext.InteractionContext = interactionContext;
            viewContext.Load.Execute();

            var insurancePolicy = viewContext.InsurancePolicy;
            insurancePolicy.StartDate = DateTime.Now;
            insurancePolicy.PolicyCode = "test policy code";
            insurancePolicy.CoPay = 50;

            interactionContext.Context.RelationshipType = PolicyHolderRelationshipType.Child;
            interactionContext.Context.InsuranceType = InsuranceType.Vision;

            viewContext.Save.Execute();

            var savedPatientInsurance = PracticeRepository.PatientInsurances.Include(pi => pi.InsurancePolicy).FirstOrDefault(pi => pi.InsuredPatientId == patient.Id && pi.InsurancePolicy.PolicyholderPatientId == policyholder.Id);
            Assert.IsNotNull(savedPatientInsurance);
            Assert.AreEqual(InsuranceType.Vision, savedPatientInsurance.InsuranceType);
            Assert.AreEqual(PolicyHolderRelationshipType.Child, savedPatientInsurance.PolicyholderRelationshipType);
            Assert.AreEqual(50, savedPatientInsurance.InsurancePolicy.Copay);
        }

        #endregion

        #region PolicyholderDetails

        [TestMethod]
        public void TestPolicyholderDetailsViewContextLoad()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var viewContext = Common.ServiceProvider.GetService<NonClinicalPatientDetailsViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.PatientId = patient.Id;
            viewContext.InteractionContext = interactionContext;

            viewContext.Load.Execute();

            Assert.IsNotNull(viewContext.EmploymentStatuses);
            Assert.IsNotNull(viewContext.PhoneNumberTypes);
            Assert.IsNotNull(viewContext.NonClinicalPatient);

            Assert.IsTrue(viewContext.EmploymentStatuses.Count == 5);                        
        }

        [TestMethod]
        public void TestPolicyholderDetailsViewContextValidateSocialSecurityConflict()
        {
            const string socialSecurityNumber = "123121234";

            var patient = Common.CreatePatient();
            patient.SocialSecurityNumber = socialSecurityNumber;
            PracticeRepository.Save(patient);

            var viewContext = Common.ServiceProvider.GetService<NonClinicalPatientDetailsViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.PatientId = patient.Id;
            viewContext.InteractionContext = interactionContext;

            viewContext.Load.ExecuteAndWait();
            viewContext.NonClinicalPatient.SocialSecurity = socialSecurityNumber;

            viewContext.ValidateSocialSecurity.ExecuteAndWait();
            Assert.IsTrue(viewContext.NonClinicalPatient.HasSocialSecurityConflict);
        }

        [TestMethod]
        public void TestPolicyholderDetailsViewContextSave()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var viewContext = Common.ServiceProvider.GetService<NonClinicalPatientDetailsViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.PatientId = patient.Id;
            viewContext.InteractionContext = interactionContext;

            viewContext.Load.Execute();

            viewContext.NonClinicalPatient.FirstName = "TestFirst";
            viewContext.NonClinicalPatient.LastName = "TestLast";
            viewContext.NonClinicalPatient.DateOfBirth = DateTime.Today;

            viewContext.Save.Execute();

            var policyholder = PracticeRepository.Patients.FirstOrDefault(p => p.FirstName == "TestFirst" && p.LastName == "TestLast");
            Assert.IsNotNull(policyholder);
            Assert.IsTrue(policyholder.DateOfBirth == DateTime.Today);
        }

        #endregion

        #region SearchInsurancePlan

        [TestMethod]
        public void TestSearchInsurancePlanViewContextSearch()
        {
            var viewContext = Common.ServiceProvider.GetService<SearchInsurancePlanViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            viewContext.InteractionContext = interactionContext;

            viewContext.SearchText = "MED";
            viewContext.TextSearchModes.First(so => so.Id == (int)TextSearchMode.BeginsWith).IsSelected = true;
            viewContext.Search.Execute();

            Assert.IsNotNull(viewContext.SearchResults);
            Assert.IsTrue(viewContext.SearchResults.Count == 3);

            viewContext.SearchText = "CIGNA";
            viewContext.Search.Execute();
            Assert.IsNotNull(viewContext.SearchResults);
            Assert.IsTrue(viewContext.SearchResults.Count == 1);
            Assert.IsTrue(viewContext.SearchResults.First().Name == "CIGNA");
        }

        #endregion

        #region SelectPolicies

        [TestMethod]
        public void TestSelectPoliciesViewContextLoad()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var patientInsurance = Common.CreatePatientInsuranceForPolicyholder();
            PracticeRepository.Save(patientInsurance);
            var policyholder = PracticeRepository.PatientInsurances.Include(pi => pi.InsurancePolicy.PolicyholderPatient).WithId(patientInsurance.Id).InsurancePolicy.PolicyholderPatient;

            var viewContext = Common.ServiceProvider.GetService<SelectPoliciesViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.PatientId = patient.Id;
            interactionContext.Context.PolicyholderId = policyholder.Id;
            interactionContext.Context.InsuranceType = InsuranceType.Ambulatory;
            viewContext.InteractionContext = interactionContext;

            viewContext.Load.Execute();

            Assert.IsNotNull(viewContext.Policyholder);
            Assert.IsNotNull(viewContext.PatientInsurances);
            Assert.IsTrue(viewContext.PatientInsurances.Count == 1);
        }
        
        [TestMethod]
        public void TestSelectPoliciesViewContextSave()
        {
            var patient = Common.CreatePatient();
            PracticeRepository.Save(patient);

            var patientInsurance = Common.CreatePatientInsuranceForPolicyholder();
            PracticeRepository.Save(patientInsurance);
            var policyholder = PracticeRepository.PatientInsurances.Include(pi => pi.InsurancePolicy.PolicyholderPatient).WithId(patientInsurance.Id).InsurancePolicy.PolicyholderPatient;

            var viewContext = Common.ServiceProvider.GetService<SelectPoliciesViewContext>();
            var interactionContext = Common.ServiceProvider.GetService<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            interactionContext.Context.PatientId = patient.Id;
            interactionContext.Context.PolicyholderId = policyholder.Id;
            interactionContext.Context.InsuranceType = InsuranceType.Ambulatory;
            interactionContext.Context.RelationshipType = PolicyHolderRelationshipType.Child;
            viewContext.InteractionContext = interactionContext;

            viewContext.Load.Execute();

            Assert.IsNull(PracticeRepository.PatientInsurances.FirstOrDefault(pi => pi.InsuredPatientId == patient.Id));
            Assert.IsFalse(viewContext.PatientInsurances.First().IsLinked);
            viewContext.PatientInsurances.First().IsLinked = true;

            viewContext.Save.Execute();
            Assert.IsNotNull(PracticeRepository.PatientInsurances.FirstOrDefault(pi => pi.InsuredPatientId == patient.Id));
        }

        #endregion
    }
}
