﻿using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Exam;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Exam;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Presentation;
using PatientSurgery = IO.Practiceware.Model.PatientSurgery;

namespace IO.Practiceware.Tests.Presentation
{
    /// <summary>
    ///   Tests business logic code for the surgery screens. UI functionality is handled in separate coded UI tests.
    /// </summary>
    [TestClass]
    public class SurgeryPresentationTests : TestBase
    {
      
        /// <summary>
        ///   Tests that the surgery presenter loads data correctly..
        /// </summary>
        [TestMethod]
        public void TestSurgeryPresenterLoading()
        {
            // Create some dummy data first (this will all get rolled back at the end of the test)
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            CreateSurgeryData(practiceRepository);

            UserAppointment appointment = Common.CreateAppointment();
            practiceRepository.Save(appointment);
            UserAppointment fetchAppointment = practiceRepository.Appointments.OfType<UserAppointment>().WithId(appointment.Id);
            Assert.IsNotNull(fetchAppointment);
            // Create the presenter and load it up
            var presenter = Common.ServiceProvider.GetService<SurgeryViewPresenter>();
            presenter.AppointmentId = fetchAppointment.Id;
            presenter.Load();
            Assert.IsTrue(presenter.ActiveAppointment.DateTime == fetchAppointment.DateTime);
            Assert.IsTrue(presenter.Questions.Count() > 1);
            Assert.IsTrue(presenter.Choices.Count() > 1);
            Assert.IsTrue(presenter.SurgeryTypes.Count() > 1);
            Assert.IsNotNull(presenter.Patient);
        }

        [TestMethod]
        public void TestSurgerySavingWithoutUserInput()
        {
            // Create some dummy data first (this will all get rolled back at the end of the test)
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            CreateSurgeryData(practiceRepository);
            UserAppointment appointment = Common.CreateAppointment();
            practiceRepository.Save(appointment);
            UserAppointment fetchAppointment = practiceRepository.Appointments.OfType<UserAppointment>().WithId(appointment.Id);
            Assert.IsNotNull(fetchAppointment);

            // Create the presenter and load it up
            var presenter = Common.ServiceProvider.GetService<SurgeryViewPresenter>();
            presenter.AppointmentId = fetchAppointment.Id;
            presenter.Load();

            presenter.ActiveSurgeryType = practiceRepository.SurgeryTypes.Include(st => st.SurgeryTemplates).First(s => s.Name.ToUpper() == "CATARACT SX");
            presenter.SurgeryTypeId = presenter.ActiveSurgeryType.Id;
            //practiceRepository.SurgeryTypes.Where(Function(s) s.Name.ToUpper() = "CATARACT SX").First.Id
            presenter.Save();
            Assert.IsNotNull(presenter.PatientSurgery);
            presenter.HealthAssessmentViewPresenter.SurgeryTemplateId = presenter.SurgeryTemplateId;
            presenter.HealthAssessmentViewPresenter.Load();
            presenter.WorkUpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.WorkUpViewPresenter.Load();
            presenter.OpRoomViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.OpRoomViewPresenter.Load();
            presenter.PostOpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.PostOpViewPresenter.Load();

            Assert.IsTrue(presenter.ActiveAppointment.DateTime == fetchAppointment.DateTime);
            Assert.IsTrue(presenter.Questions.Count() > 1);
            Assert.IsTrue(presenter.HealthAssessmentViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.WorkUpViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.OpRoomViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.PostOpViewPresenter.ScreenQuestions.Any());

            PatientSurgery fetchPatientSurgery = practiceRepository.PatientSurgeries.WithId(presenter.PatientSurgery.Id);
            Assert.IsNotNull(fetchPatientSurgery);
            var comparison = new Objects.ObjectComparison
                                 {
                                     CompareChildren = false,
                                     MaxDifferences = 100
                                 };
            bool areSame = comparison.Compare(presenter.PatientSurgery, fetchPatientSurgery);
            Assert.IsTrue(areSame);
        }

        [TestMethod]
        public void TestSurgerySavingWithUserInput()
        {
            // Create some dummy data first (this will all get rolled back at the end of the test)
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            CreateSurgeryData(practiceRepository);
            UserAppointment appointment = Common.CreateAppointment();
            practiceRepository.Save(appointment);
            UserAppointment fetchAppointment = practiceRepository.Appointments.OfType<UserAppointment>().WithId(appointment.Id);
            Assert.IsNotNull(fetchAppointment);

            // Create the presenter and load it up
            var presenter = Common.ServiceProvider.GetService<SurgeryViewPresenter>();
            presenter.AppointmentId = fetchAppointment.Id;
            presenter.Load();

            presenter.ActiveSurgeryType = practiceRepository.SurgeryTypes.Include(st => st.SurgeryTemplates).First(s => s.Name.ToUpper() == "CATARACT SX");
            presenter.SurgeryTypeId = presenter.ActiveSurgeryType.Id;
            //practiceRepository.SurgeryTypes.Where(Function(s) s.Name.ToUpper() = "CATARACT SX").First.Id
            presenter.Save();
            Assert.IsNotNull(presenter.PatientSurgery);
            presenter.HealthAssessmentViewPresenter.SurgeryTemplateId = presenter.SurgeryTemplateId;
            presenter.HealthAssessmentViewPresenter.Load();
            presenter.HealthAssessmentViewPresenter.HealthAssessmentArgs = CreateHealthAssessmentUserInput(presenter.HealthAssessmentViewPresenter);
            Assert.IsNotNull(presenter.ActiveAppointment);
            presenter.Save();
            Assert.IsTrue(presenter.PatientSurgeryQuestionChoices.Count() == 1);
            presenter.WorkUpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.WorkUpViewPresenter.Load();
            presenter.OpRoomViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.OpRoomViewPresenter.Load();
            presenter.PostOpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.PostOpViewPresenter.Load();

            Assert.IsTrue(presenter.ActiveAppointment.DateTime == fetchAppointment.DateTime);
            Assert.IsTrue(presenter.Questions.Count() > 1);
            Assert.IsTrue(presenter.HealthAssessmentViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.WorkUpViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.OpRoomViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.PostOpViewPresenter.ScreenQuestions.Any());

            PatientSurgery fetchPatientSurgery = practiceRepository.PatientSurgeries.WithId(presenter.PatientSurgery.Id);
            Assert.IsNotNull(fetchPatientSurgery);
            var comparison = new Objects.ObjectComparison
                                 {
                                     CompareChildren = false,
                                     MaxDifferences = 100
                                 };
            bool areSame = comparison.Compare(presenter.PatientSurgery, fetchPatientSurgery);
            Assert.IsTrue(areSame);
        }

        private static IEnumerable<QuestionChoiceArguments> CreateHealthAssessmentUserInput(HealthAssessmentViewPresenter presenter)
        {
            if (presenter.ScreenQuestions.Any())
            {
                var arguments = new List<QuestionChoiceArguments>();
                foreach (SurgeryTemplateScreenQuestion sq in presenter.ScreenQuestions)
                {
                    SurgeryTemplateScreenQuestion sQuestion = sq;
                    if (sQuestion.Question.Choices.Any())
                    {
                        var argument = new QuestionChoiceArguments();
                        int choiceid1 = sQuestion.Question.Choices.ToArray()[0].Id;
                        int choiceid2 = sQuestion.Question.Choices.ToArray()[1].Id;
                        PatientSurgeryQuestionChoice choice = presenter.GetChoice(choiceid1);
                        if (choice != null)
                        {
                            argument.PatientSurgeryQuestionChoiceId = choice.Id;
                        }
                        else
                        {
                            choice = presenter.GetChoice(choiceid2);
                            if (choice != null)
                            {
                                argument.PatientSurgeryQuestionChoiceId = choice.Id;
                            }
                        }
                        argument.QuestionId = sQuestion.QuestionId;
                        argument.ChoiceId = sQuestion.Question.Choices.ToArray()[0].Id;
                        argument.Note = "Note";
                        arguments.Add(argument);
                    }
                }
                return arguments;
            }
            return null;
        }

        private static IQueryable<Screen> CreateScreens(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "HEALTH-ASSESSMENT"))
            {
                var screen = new Screen();
                screen.Id = 1;
                screen.Name = "Health-Assessment";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "HEIGHT-WEIGHT"))
            {
                var screen = new Screen();
                screen.Id = 2;
                screen.Name = "Height-Weight";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "CURRENT MEDICATIONS"))
            {
                var screen = new Screen();
                screen.Id = 3;
                screen.Name = "Current Medications";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "ALLERGIES"))
            {
                var screen = new Screen();
                screen.Id = 4;
                screen.Name = "Allergies";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "WORK-UP"))
            {
                var screen = new Screen();
                screen.Id = 5;
                screen.Name = "Work-Up";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "OPERATING-ROOM"))
            {
                var screen = new Screen();
                screen.Id = 6;
                screen.Name = "Operating-Room";
                practiceRepository.Save(screen);
            }
            if (!practiceRepository.Screens.Any(s => s.Name.ToUpper() == "POST-OP"))
            {
                var screen = new Screen();
                screen.Id = 7;
                screen.Name = "Post-OP";
                practiceRepository.Save(screen);
            }
            return practiceRepository.Screens;
        }

        private static IEnumerable<SurgeryTemplateScreenQuestion> CreateScreenQuestions(IPracticeRepository practiceRepository)
        {
            CreateHealthAssessmentScreenQuestions(practiceRepository);
            CreateWorkUpScreenQuestions(practiceRepository);
            CreateOrScreenQuestions(practiceRepository);
            CreatePostOpScreenQuestions(practiceRepository);
            return practiceRepository.SurgeryTemplateScreenQuestions;
        }

        private static void CreateHealthAssessmentScreenQuestions(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "HEALTH-ASSESSMENT").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "ID BAND").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "HEALTH-ASSESSMENT").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ID BAND").Id;
                practiceRepository.Save(screenQuestion);
            }
        }

        private static void CreateWorkUpScreenQuestions(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "WORK-UP").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "ID BAND").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "WORK-UP").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ID BAND").Id;
                practiceRepository.Save(screenQuestion);
            }
        }

        private static void CreateOrScreenQuestions(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "TIME IN").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "TIME IN").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "TIME OUT").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "TIME OUT").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "SURGERY START").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SURGERY START").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "SURGERY END").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SURGERY END").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "PUT TAPE ON FACE").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "PUT TAPE ON FACE").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "PAINT EYE").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "PAINT EYE").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "CLEANED AREA").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "CLEANED AREA").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "GET KNIVES OUT").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "GET KNIVES OUT").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "GET TRAY READY").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "GET TRAY READY").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "OPERATING ROOMS").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "OPERATING ROOMS").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "CALL DOCTOR").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "CALL DOCTOR").Id;
                practiceRepository.Save(screenQuestion);
            }
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "OPERATING-ROOM").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "OPENED TAPE").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "OPERATING-ROOM").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "OPENED TAPE").Id;
                practiceRepository.Save(screenQuestion);
            }
        }

        private static void CreatePostOpScreenQuestions(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTemplateScreenQuestions.Any(sq => sq.SurgeryTemplateId == practiceRepository.SurgeryTemplates.FirstOrDefault(s => s.Name.ToUpper() == "1. CATARACT SX").Id & sq.ScreenId == practiceRepository.Screens.FirstOrDefault(s => s.Name.ToUpper() == "POST-OP").Id & sq.QuestionId == practiceRepository.Questions.FirstOrDefault(q => q.Label.ToUpper() == "ID BAND").Id))
            {
                var screenQuestion = new SurgeryTemplateScreenQuestion();
                screenQuestion.SurgeryTemplateId = practiceRepository.SurgeryTemplates.First(s => s.Name.ToUpper() == "1. CATARACT SX").Id;
                screenQuestion.ScreenId = practiceRepository.Screens.First(s => s.Name.ToUpper() == "POST-OP").Id;
                screenQuestion.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ID BAND").Id;
                practiceRepository.Save(screenQuestion);
            }
        }

        /// <summary>
        ///   Creates surgery data for testing. This method should pre-suppose that no other data exists in the database at this time. In other words, it should not be tied to run against any specific database instance. That way the unit test may be autonomous (self contained).
        /// </summary>
        internal static void CreateSurgeryData(IPracticeRepository practiceRepository)
        {
            IEnumerable<Question> questions = CreateQuestions(practiceRepository);
            Assert.IsNotNull(questions);
            IEnumerable<Choice> choices = CreateQuestionChoices(practiceRepository);
            Assert.IsNotNull(choices);
            IEnumerable<SurgeryType> surgeryTypes = CreateSurgeryTypes(practiceRepository);
            Assert.IsNotNull(surgeryTypes);
            IEnumerable<SurgeryTemplate> surgeryTemplates = CreateSurgeryTemplates(practiceRepository);
            Assert.IsNotNull(surgeryTemplates);
            IQueryable<Screen> screens = CreateScreens(practiceRepository);
            Assert.IsNotNull(screens);
            IEnumerable<SurgeryTemplateScreenQuestion> sQuestions = CreateScreenQuestions(practiceRepository);
            Assert.IsNotNull(sQuestions);
            //Dim appointment = CreateAppointment(practiceRepository)
            //Assert.IsNotNull(appointment)
        }

        private static IEnumerable<Question> CreateQuestions(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "NOTE BOX TOP"))
            {
                var question = new Question();
                question.Label = "NOTE BOX TOP";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "NOTE BOX BOTTOM"))
            {
                var question = new Question();
                question.Label = "NOTE BOX BOTTOM";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "EYE CONTEXT"))
            {
                var question = new Question();
                question.Label = "EYE CONTEXT";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ID BAND"))
            {
                var question = new Question();
                question.Label = "ID BAND";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "HEIGHT-FEET"))
            {
                var question = new Question();
                question.Label = "HEIGHT-FEET";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "HEIGHT-INCHES"))
            {
                var question = new Question();
                question.Label = "HEIGHT-INCHES";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "HEIGHT-M"))
            {
                var question = new Question();
                question.Label = "HEIGHT-M";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "HEIGHT-CM"))
            {
                var question = new Question();
                question.Label = "HEIGHT-CM";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WEIGHT-LBS"))
            {
                var question = new Question();
                question.Label = "WEIGHT-LBS";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WEIGHT-OUNCES"))
            {
                var question = new Question();
                question.Label = "WEIGHT-OUNCES";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WEIGHT-KGS"))
            {
                var question = new Question();
                question.Label = "WEIGHT-KGS";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WEIGHT-GRAMS"))
            {
                var question = new Question();
                question.Label = "WEIGHT-GRAMS";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "IV IN"))
            {
                var question = new Question();
                question.Label = "IV IN";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "IV IN NOTES"))
            {
                var question = new Question();
                question.Label = "IV IN NOTES";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP BP SYSTOLIC"))
            {
                var question = new Question();
                question.Label = "WORK-UP BP SYSTOLIC";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP BP DIASTOLIC"))
            {
                var question = new Question();
                question.Label = "WORK-UP BP DIASTOLIC";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP BP TYPE"))
            {
                var question = new Question();
                question.Label = "WORK-UP BP TYPE";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP RESP"))
            {
                var question = new Question();
                question.Label = "WORK-UP RESP";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP PULSE"))
            {
                var question = new Question();
                question.Label = "WORK-UP PULSE";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP O2 SATURATION"))
            {
                var question = new Question();
                question.Label = "WORK-UP O2 SATURATION";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "TIME IN"))
            {
                var question = new Question();
                question.Label = "Time In";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SURGERY START"))
            {
                var question = new Question();
                question.Label = "Surgery start";
                question.OrdinalId = 2;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "PUT TAPE ON FACE"))
            {
                var question = new Question();
                question.Label = "Put Tape on Face";
                question.OrdinalId = 3;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "PAINT EYE"))
            {
                var question = new Question();
                question.Label = "Paint Eye";
                question.OrdinalId = 4;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "CLEANED AREA"))
            {
                var question = new Question();
                question.Label = "Cleaned Area";
                question.OrdinalId = 5;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "GET KNIVES OUT"))
            {
                var question = new Question();
                question.Label = "Get Knives Out";
                question.OrdinalId = 6;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "TIME OUT"))
            {
                var question = new Question();
                question.Label = "Time Out";
                question.OrdinalId = 7;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SURGERY END"))
            {
                var question = new Question();
                question.Label = "Surgery End";
                question.OrdinalId = 8;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "GET TRAY READY"))
            {
                var question = new Question();
                question.Label = "Get Tray ready";
                question.OrdinalId = 9;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "OPERATING ROOMS"))
            {
                var question = new Question();
                question.Label = "Operating Rooms";
                question.OrdinalId = 10;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "CALL DOCTOR"))
            {
                var question = new Question();
                question.Label = "Call Doctor";
                question.OrdinalId = 11;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "OPENED TAPE"))
            {
                var question = new Question();
                question.Label = "Opened Tape";
                question.OrdinalId = 12;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SURGEON"))
            {
                var question = new Question();
                question.Label = "SURGEON";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ASSISTANT SURGEON"))
            {
                var question = new Question();
                question.Label = "ASSISTANT SURGEON";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SCRUB TECH"))
            {
                var question = new Question();
                question.Label = "SCRUB TECH";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "CIRCULATOR"))
            {
                var question = new Question();
                question.Label = "CIRCULATOR";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "OTHER TEAM MEMBERS"))
            {
                var question = new Question();
                question.Label = "OTHER TEAM MEMBERS";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ANESTHESIA"))
            {
                var question = new Question();
                question.Label = "ANESTHESIA";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ANESTHESIOLOGIST"))
            {
                var question = new Question();
                question.Label = "ANESTHESIOLOGIST";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "EQUIPMENT"))
            {
                var question = new Question();
                question.Label = "EQUIPMENT";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "DRUGS"))
            {
                var question = new Question();
                question.Label = "DRUGS";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "IV OUT"))
            {
                var question = new Question();
                question.Label = "IV OUT";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "IV OUT NOTES"))
            {
                var question = new Question();
                question.Label = "IV OUT NOTES";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP1"))
            {
                var question = new Question();
                question.Label = "POST-OP BP1";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP2"))
            {
                var question = new Question();
                question.Label = "POST-OP BP2";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP3"))
            {
                var question = new Question();
                question.Label = "POST-OP BP3";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP4"))
            {
                var question = new Question();
                question.Label = "POST-OP BP4";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP5"))
            {
                var question = new Question();
                question.Label = "POST-OP BP5";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP BP6"))
            {
                var question = new Question();
                question.Label = "POST-OP BP6";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP RESP"))
            {
                var question = new Question();
                question.Label = "POST-OP RESP";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP PULSE"))
            {
                var question = new Question();
                question.Label = "POST-OP PULSE";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            if (!practiceRepository.Questions.Any(q => q.Label.ToUpper() == "POST-OP O2 SATURATION"))
            {
                var question = new Question();
                question.Label = "POST-OP O2 SATURATION";
                question.OrdinalId = 1;
                practiceRepository.Save(question);
            }
            return practiceRepository.Questions;
        }

        private static IEnumerable<Choice> CreateQuestionChoices(IPracticeRepository practiceRepository)
        {
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "EYE CONTEXT"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "OD" & ch.Question.Label.ToUpper() == "EYE CONTEXT"))
                {
                    var choice = new Choice();
                    choice.Name = "OD";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EYE CONTEXT").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "OS" & ch.Question.Label.ToUpper() == "EYE CONTEXT"))
                {
                    var choice = new Choice();
                    choice.Name = "OS";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EYE CONTEXT").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "OU" & ch.Question.Label.ToUpper() == "EYE CONTEXT"))
                {
                    var choice = new Choice();
                    choice.Name = "OU";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EYE CONTEXT").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ID BAND"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "YES" & ch.Question.Label.ToUpper() == "ID BAND"))
                {
                    var choice = new Choice();
                    choice.Name = "Yes";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ID BAND").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "NO" & ch.Question.Label.ToUpper() == "ID BAND"))
                {
                    var choice = new Choice();
                    choice.Name = "No";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ID BAND").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "WORK-UP BP TYPE"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SITTING" & ch.Question.Label.ToUpper() == "WORK-UP BP TYPE"))
                {
                    var choice = new Choice();
                    choice.Name = "SITTING";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "WORK-UP BP TYPE").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "STANDING" & ch.Question.Label.ToUpper() == "WORK-UP BP TYPE"))
                {
                    var choice = new Choice();
                    choice.Name = "STANDING";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "WORK-UP BP TYPE").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SUPINE" & ch.Question.Label.ToUpper() == "WORK-UP BP TYPE"))
                {
                    var choice = new Choice();
                    choice.Name = "SUPINE";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "WORK-UP BP TYPE").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SURGEON"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SURGEON1" & ch.Question.Label.ToUpper() == "SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Surgeon1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SURGEON").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SURGEON2" & ch.Question.Label.ToUpper() == "SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Surgeon2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SURGEON").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SURGEON3" & ch.Question.Label.ToUpper() == "SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Surgeon3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SURGEON").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ASSISTANT SURGEON"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ASSISTANT SURGEON1" & ch.Question.Label.ToUpper() == "ASSISTANT SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Assistant Surgeon1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ASSISTANT SURGEON").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ASSISTANT SURGEON2" & ch.Question.Label.ToUpper() == "ASSISTANT SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Assistant Surgeon2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ASSISTANT SURGEON").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ASSISTANT SURGEON3" & ch.Question.Label.ToUpper() == "ASSISTANT SURGEON"))
                {
                    var choice = new Choice();
                    choice.Name = "Assistant Surgeon3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ASSISTANT SURGEON").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "SCRUB TECH"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SCRUB TECH1" & ch.Question.Label.ToUpper() == "SCRUB TECH"))
                {
                    var choice = new Choice();
                    choice.Name = "Scrub Tech1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SCRUB TECH").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SCRUB TECH2" & ch.Question.Label.ToUpper() == "SCRUB TECH"))
                {
                    var choice = new Choice();
                    choice.Name = "Scrub Tech2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SCRUB TECH").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "SCRUB TECH3" & ch.Question.Label.ToUpper() == "SCRUB TECH"))
                {
                    var choice = new Choice();
                    choice.Name = "Scrub Tech3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "SCRUB TECH").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "CIRCULATOR"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "CIRCULATOR1" & ch.Question.Label.ToUpper() == "CIRCULATOR"))
                {
                    var choice = new Choice();
                    choice.Name = "Circulator1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "CIRCULATOR").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "CIRCULATOR2" & ch.Question.Label.ToUpper() == "CIRCULATOR"))
                {
                    var choice = new Choice();
                    choice.Name = "Circulator2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "CIRCULATOR").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "CIRCULATOR3" & ch.Question.Label.ToUpper() == "CIRCULATOR"))
                {
                    var choice = new Choice();
                    choice.Name = "Circulator3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "CIRCULATOR").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "OTHER TEAM MEMBERS"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "MEMBER1" & ch.Question.Label.ToUpper() == "OTHER TEAM MEMBERS"))
                {
                    var choice = new Choice();
                    choice.Name = "Member1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "OTHER TEAM MEMBERS").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "MEMBER2" & ch.Question.Label.ToUpper() == "OTHER TEAM MEMBERS"))
                {
                    var choice = new Choice();
                    choice.Name = "Member2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "OTHER TEAM MEMBERS").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "MEMBER3" & ch.Question.Label.ToUpper() == "OTHER TEAM MEMBERS"))
                {
                    var choice = new Choice();
                    choice.Name = "Member3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "OTHER TEAM MEMBERS").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ANESTHESIA"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIA1" & ch.Question.Label.ToUpper() == "ANESTHESIA"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesia1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIA").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIA2" & ch.Question.Label.ToUpper() == "ANESTHESIA"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesia2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIA").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIA3" & ch.Question.Label.ToUpper() == "ANESTHESIA"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesia3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIA").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "ANESTHESIOLOGIST"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIOLOGIST1" & ch.Question.Label.ToUpper() == "ANESTHESIOLOGIST"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesiologist1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIOLOGIST").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIOLOGIST2" & ch.Question.Label.ToUpper() == "ANESTHESIOLOGIST"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesiologist2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIOLOGIST").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "ANESTHESIOLOGIST3" & ch.Question.Label.ToUpper() == "ANESTHESIOLOGIST"))
                {
                    var choice = new Choice();
                    choice.Name = "Anesthesiologist3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "ANESTHESIOLOGIST").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "EQUIPMENT"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "EQUIPMENT1" & ch.Question.Label.ToUpper() == "EQUIPMENT"))
                {
                    var choice = new Choice();
                    choice.Name = "Equipment1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EQUIPMENT").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "EQUIPMENT2" & ch.Question.Label.ToUpper() == "EQUIPMENT"))
                {
                    var choice = new Choice();
                    choice.Name = "Equipment2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EQUIPMENT").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "EQUIPMENT3" & ch.Question.Label.ToUpper() == "EQUIPMENT"))
                {
                    var choice = new Choice();
                    choice.Name = "Equipment3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "EQUIPMENT").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            if (practiceRepository.Questions.Any(q => q.Label.ToUpper() == "DRUGS"))
            {
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "DRUG1" & ch.Question.Label.ToUpper() == "DRUGS"))
                {
                    var choice = new Choice();
                    choice.Name = "Drug1";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "DRUGS").Id;
                    choice.OrdinalId = 1;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "DRUG2" & ch.Question.Label.ToUpper() == "DRUGS"))
                {
                    var choice = new Choice();
                    choice.Name = "Drug2";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "DRUGS").Id;
                    choice.OrdinalId = 2;
                    practiceRepository.Save(choice);
                }
                if (!practiceRepository.Choices.Any(ch => ch.Name.ToUpper() == "DRUG3" & ch.Question.Label.ToUpper() == "DRUGS"))
                {
                    var choice = new Choice();
                    choice.Name = "Drug3";
                    choice.QuestionId = practiceRepository.Questions.First(q => q.Label.ToUpper() == "DRUGS").Id;
                    choice.OrdinalId = 3;
                    practiceRepository.Save(choice);
                }
            }
            return practiceRepository.Choices;
        }

        private static IEnumerable<SurgeryType> CreateSurgeryTypes(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTypes.Any(s => s.Name.ToUpper() == "CATARACT SX"))
            {
                var surgeryType = new SurgeryType();
                surgeryType.Name = "CATARACT SX";
                practiceRepository.Save(surgeryType);
            }
            if (!practiceRepository.SurgeryTypes.Any(q => q.Name.ToUpper() == "LASER EYE SURGERY"))
            {
                var surgeryType = new SurgeryType();
                surgeryType.Name = "LASER EYE SURGERY";
                practiceRepository.Save(surgeryType);
            }
            return practiceRepository.SurgeryTypes;
        }

        private static IEnumerable<SurgeryTemplate> CreateSurgeryTemplates(IPracticeRepository practiceRepository)
        {
            if (!practiceRepository.SurgeryTemplates.Any(s => s.Name.ToUpper() == "1. CATARACT SX"))
            {
                var surgeryTemplate = new SurgeryTemplate();
                surgeryTemplate.Name = "1. CATARACT SX";
                surgeryTemplate.SurgeryTypeId = practiceRepository.SurgeryTypes.First(s => s.Name.ToUpper() == "CATARACT SX").Id;
                practiceRepository.Save(surgeryTemplate);
            }
            if (!practiceRepository.SurgeryTemplates.Any(q => q.Name.ToUpper() == "2. LASER EYE SURGERY"))
            {
                var surgeryTemplate = new SurgeryTemplate();
                surgeryTemplate.Name = "2. LASER EYE SURGERY";
                surgeryTemplate.SurgeryTypeId = practiceRepository.SurgeryTypes.First(s => s.Name.ToUpper() == "LASER EYE SURGERY").Id;
                practiceRepository.Save(surgeryTemplate);
            }
            return practiceRepository.SurgeryTemplates;
        }

        [TestMethod]
        public void TestAscPrintingDocuments()
        {
            // Create some dummy data first (this will all get rolled back at the end of the test)
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            CreateSurgeryData(practiceRepository);
            UserAppointment appointment = Common.CreateAppointment();
            practiceRepository.Save(appointment);
            UserAppointment fetchAppointment = practiceRepository.Appointments.OfType<UserAppointment>().WithId(appointment.Id);
            Assert.IsNotNull(fetchAppointment);

            // Create the presenter and load it up
            var presenter = Common.ServiceProvider.GetService<SurgeryViewPresenter>();
            presenter.AppointmentId = fetchAppointment.Id;
            presenter.Load();

            presenter.ActiveSurgeryType = practiceRepository.SurgeryTypes.Include(st => st.SurgeryTemplates).First(s => s.Name.ToUpper() == "CATARACT SX");
            presenter.SurgeryTypeId = presenter.ActiveSurgeryType.Id;
            //practiceRepository.SurgeryTypes.Where(Function(s) s.Name.ToUpper() = "CATARACT SX").First.Id
            presenter.Save();
            Assert.IsNotNull(presenter.PatientSurgery);
            presenter.HealthAssessmentViewPresenter.SurgeryTemplateId = presenter.SurgeryTemplateId;
            presenter.HealthAssessmentViewPresenter.Load();
            presenter.HealthAssessmentViewPresenter.HealthAssessmentArgs = CreateHealthAssessmentUserInput(presenter.HealthAssessmentViewPresenter);
            Assert.IsNotNull(presenter.ActiveAppointment);
            presenter.Save();
            Assert.IsTrue(presenter.PatientSurgeryQuestionChoices.Count() == 1);
            presenter.WorkUpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.WorkUpViewPresenter.Load();
            presenter.OpRoomViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.OpRoomViewPresenter.Load();
            presenter.PostOpViewPresenter.SurgeryTemlateId = presenter.SurgeryTemplateId;
            presenter.PostOpViewPresenter.Load();

            Assert.IsTrue(presenter.ActiveAppointment.DateTime == fetchAppointment.DateTime);
            Assert.IsTrue(presenter.Questions.Count() > 1);
            Assert.IsTrue(presenter.HealthAssessmentViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.WorkUpViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.OpRoomViewPresenter.ScreenQuestions.Any());
            Assert.IsTrue(presenter.PostOpViewPresenter.ScreenQuestions.Any());

            IEnumerable<Services.Documents.BinaryDocument> documents = Common.ServiceProvider.GetService<IEncounterDocumentService>().CreateDocuments(presenter.ActiveAppointment.Id);
            Assert.IsNotNull(documents);
        }
    }
}