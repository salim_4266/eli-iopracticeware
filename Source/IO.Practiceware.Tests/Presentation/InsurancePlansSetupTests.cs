﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Presentation.Views.InsurancePlansSetup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class InsurancePlansSetupTests : TestBase
    {
        [TestMethod]
        public void TestManageInsurancePlansViewContextLoad()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            Assert.IsTrue(viewContext.InsurancePlans.Count > 0
                && viewContext.InsurancePlans.Any(p => p.Id == insurer.Id));
        }

        [TestMethod]
        public void TestRemittanceMappingViewContextSave()
        {
            RemittanceMappingViewContext viewContext;
            SetupRemittanceTestContext(out viewContext);

            Assert.AreEqual(0, viewContext.PayerMappings.Count);

            var insurer = Common.CreateInsurer();
            insurer.PayerCode = "ABC";
            PracticeRepository.Save(insurer);

            // Add one item and save
            var newItem = viewContext.NewPayerMappingFactory();
            newItem.OutgoingPayerCode = "ABC";
            newItem.IncomingPayerCode = "123";
            viewContext.PayerMappings.Add(newItem);
            viewContext.SaveMappings.ExecuteAndWait();

            // Check in database
            var dbMappings = PracticeRepository.IncomingPayerCodes.ToList();
            Assert.AreEqual(1, dbMappings.Count);

            // Reload -> Delete -> save -> check
            SetupRemittanceTestContext(out viewContext);
            Assert.AreEqual(1, viewContext.PayerMappings.Count);

            insurer.PayerCode = "";
            PracticeRepository.Save(insurer);

            viewContext.PayerMappings.Clear();
            viewContext.SaveMappings.ExecuteAndWait();
            dbMappings = PracticeRepository.IncomingPayerCodes.ToList();
            Assert.AreEqual(0, dbMappings.Count);
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextAddNew()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            // Allow following operations
            PermissionId.EditInsurancePlanInformation.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.CreateInsurancePlans.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.CanCreateNewPlan && viewContext.CanEditPlans, "Must be able to create plans");

            viewContext.SelectedInsurancePlans.Clear();
            viewContext.Filter = "InsurerAddTest";
            viewContext.NewPlan.ExecuteAndWait();

            var addedPlan = viewContext.InsurancePlans.FirstOrDefault(p => p.InsuranceName == "InsurerAddTest");
            Assert.IsNotNull(addedPlan, "Plan must be added");

            viewContext.ApplyAllChanges.ExecuteAndWait();

            var savedInsurer = PracticeRepository.Insurers.FirstOrDefault(pi => pi.Id == addedPlan.Id);
            Assert.IsNotNull(savedInsurer, "Saved insurer is not present in database");
// ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual(ClaimFilingIndicatorCode.CommercialInsurance, savedInsurer.ClaimFilingIndicatorCode);
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextApplyAllChanges()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            // Allow following operations
            PermissionId.EditInsurancePlanInformation.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.CanEditPlans, "Must be able to edit plans");

            selectedPlan.PlanName = "New Plan Name";
            viewContext.ApplyAllChanges.ExecuteAndWait();

            var savedInsurer = PracticeRepository.Insurers.FirstOrDefault(pi => pi.Id == selectedPlan.Id);
            Assert.IsNotNull(savedInsurer, "Saved insurer is not present in database");
// ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual(savedInsurer.PlanName, "New Plan Name");
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextCancelPlanChanges()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            selectedPlan.PlanName = "New Plan Name";

            viewContext.CancelPlanChanges.ExecuteAndWait(selectedPlan);
            Assert.IsTrue(selectedPlan.PlanName != "New Plan Name");
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextLoadChangeHistory()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            selectedPlan.ChangeHistory = null;
            viewContext.LoadChangeHistory.ExecuteAndWait(selectedPlan);
            Assert.IsNotNull(selectedPlan.ChangeHistory);
// ReSharper disable once PossibleNullReferenceException
            Assert.AreNotEqual(0, selectedPlan.ChangeHistory.Count);
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextLoadClaimFormSetup()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            Assert.IsFalse(selectedPlan.IsInsurerClaimFormsLoaded);
            viewContext.LoadClaimFormSetup.ExecuteAndWait(selectedPlan);
            Assert.IsTrue(selectedPlan.IsInsurerClaimFormsLoaded);
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextLoadClaimCrossovers()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            Assert.IsFalse(selectedPlan.IsClaimCrossoversLoaded);
            viewContext.LoadClaimCrossovers.ExecuteAndWait(selectedPlan);
            Assert.IsTrue(selectedPlan.IsClaimCrossoversLoaded);
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextReloadPlans()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            // Allow following operations
            viewContext.SelectedInsurancePlans.Add(selectedPlan);
            PermissionId.ArchiveInsurancePlans.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.CanArchivePlans);

            // Archive selected plan to test reload
            viewContext.ArchivePlans.ExecuteAndWait();

            // Now test reload
            viewContext.IsActiveChecked = false;
            viewContext.IsArchivedChecked = true;
            viewContext.ReloadPlans.ExecuteAndWait();

            // Check we have an archived plan in reloaded list
            Assert.IsTrue(viewContext.InsurancePlans.All(p => p.IsArchived)
                && viewContext.InsurancePlans.Any(p => p.Id == selectedPlan.Id));
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextArchivePlansReactivatePlan()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            // Check context has right switched by default
            Assert.IsTrue(viewContext.IsActiveChecked, "Active is not checked");
            Assert.IsFalse(viewContext.IsArchivedChecked, "Archived shouldn't be checked");

            viewContext.SelectedInsurancePlans.Add(selectedPlan);

            // Allow following operations
            PermissionId.ArchiveInsurancePlans.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.EditInsurancePlanInformation.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.ReactivateInsurancePlans.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.CanArchivePlans, "Must be able to archive plans");
            Assert.IsTrue(selectedPlan.CanEditPlan, "Must be able to edit plan");
            Assert.IsFalse(selectedPlan.CanReactivatePlan, "Must not be able to reactive plan");

            // Archive selected plans
            viewContext.ArchivePlans.ExecuteAndWait();
            Assert.IsTrue(selectedPlan.IsArchived, "Plan hasn't been archived");
            Assert.IsFalse(selectedPlan.CanEditPlan, "Plan shouldn't be editable at this point");
            Assert.IsTrue(selectedPlan.IsPlanReadOnly, "Plan must be readonly");
            Assert.IsTrue(PracticeRepository.Insurers.First(p => p.Id == selectedPlan.Id).IsArchived, "Plan be set as archived in database");
            Assert.IsFalse(viewContext.InsurancePlans.Any(p => p.Id == selectedPlan.Id), "Plan should not appear in plans list");

            // Reactivate plan
            viewContext.ReactivatePlan.ExecuteAndWait(selectedPlan);
            Assert.IsFalse(selectedPlan.IsArchived, "Plan should be reactivated");
            Assert.IsTrue(selectedPlan.CanEditPlan, "Plan should be back editable");
            Assert.IsFalse(PracticeRepository.Insurers.First(p => p.Id == selectedPlan.Id).IsArchived, "IsArchived should be cleared in database");
        }

        [TestMethod]
        public void TestManageInsurancePlansViewContextDeletePlans()
        {
            Insurer insurer;
            ManageInsurancePlansViewContext viewContext;
            InsurancePlanToManageViewModel selectedPlan;
            SetupTestContext(out insurer, out viewContext, out selectedPlan);

            // Allow following operations
            viewContext.SelectedInsurancePlans.Add(selectedPlan);
            PermissionId.DeleteInsurancePlans.AddPermissionToCurrentUser(PracticeRepository);
            Assert.IsTrue(viewContext.CanDeletePlans);

            // Archive selected plan to test reload
            viewContext.DeletePlans.ExecuteAndWait();

            // Checks it is not present anymore
            Assert.IsFalse(viewContext.InsurancePlans.Any(p => p.Id == selectedPlan.Id));
            Assert.IsFalse(PracticeRepository.Insurers.Any(p => p.Id == selectedPlan.Id));
        }

        #region Helpers

        void SetupTestContext(out Insurer insurer, out ManageInsurancePlansViewContext viewContext, out InsurancePlanToManageViewModel selectedPlan)
        {
            insurer = Common.CreateInsurer();
            PracticeRepository.Save(insurer);

            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, ManageInsurancePlansViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
            viewContext.ReloadPlans.ExecuteAndWait();
            var insurerParam = insurer;
            selectedPlan = viewContext.InsurancePlans.First(p => p.Id == insurerParam.Id);
        }

        void SetupRemittanceTestContext(out RemittanceMappingViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, RemittanceMappingViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
        }

        #endregion
    }
}
