﻿using System;
using System.Data.Entity;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Notes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class NoteViewTests : X12MetadataTestsBase
    {
        [TestMethod]
        public void TestNoteViewContextLoadAndSaveCommentWithAlert()
        {
            var invoiceComment = new InvoiceComment
            {
                IncludeOnStatement = true,
                InvoiceId = 1,
                IsArchived = false,
                Value = "Test invoice Comment",
                DateTime  = DateTime.Now.ToClientTime()
            };

            var billingServiceComment = new BillingServiceComment
            {
                IsIncludedOnStatement = true,
                BillingServiceId = 1,
                IsArchived = false,
                Value = "Test billing Comment",
                DateTime = DateTime.Now.ToClientTime()
            };
            PracticeRepository.Save(billingServiceComment);
            PracticeRepository.Save(invoiceComment);

            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, NoteViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

            viewContext.NoteLoadArguments = new NoteLoadArguments();
            //invoice comment context
            viewContext.NoteLoadArguments.NoteType = NoteType.InvoiceComment;
            viewContext.NoteLoadArguments.NoteId = invoiceComment.Id;

            viewContext.Load.Execute();

            Assert.AreEqual(invoiceComment.Value, viewContext.Note.Value);
            Assert.AreEqual(invoiceComment.IsArchived, viewContext.Note.IsArchived);
            Assert.AreEqual(invoiceComment.IncludeOnStatement, viewContext.Note.IsIncludedOnStatement);

            viewContext.Note.Value = "Test Comment";
            viewContext.Note.AlertableScreens = new System.Collections.ObjectModel.ObservableCollection<NamedViewModel>();
            viewContext.Note.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 9));

            //Saving comment and sending alert.
            viewContext.SaveComment();

            invoiceComment = PracticeRepository.InvoiceComments.WithId(invoiceComment.Id);
            var screens = PracticeRepository.Alerts.Include(x => x.Screens).Where(x => x.InvoiceCommentId == invoiceComment.Id).SelectMany(x => x.Screens).ToArray();
            Assert.AreEqual(invoiceComment.Value, viewContext.Note.Value);
            Assert.AreEqual(screens.Count(), viewContext.Note.AlertableScreens.Count);


            //Billing service comment context
            viewContext.NoteLoadArguments.NoteType = NoteType.BillingServiceComment;
            viewContext.NoteLoadArguments.EntityId = 1;

            viewContext.Load.Execute();

            Assert.IsNotNull(billingServiceComment);
            Assert.AreEqual(billingServiceComment.Value, viewContext.Note.Value);
            Assert.AreEqual(billingServiceComment.IsArchived, viewContext.Note.IsArchived);
            Assert.AreEqual(billingServiceComment.IsIncludedOnStatement, viewContext.Note.IsIncludedOnStatement);

            viewContext.Note.Value = "Test billing service Comment";
            viewContext.Note.AlertableScreens = new System.Collections.ObjectModel.ObservableCollection<NamedViewModel>();
            viewContext.Note.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 9));

            //Saving comment and sending alert.
            viewContext.SaveComment();

            billingServiceComment = PracticeRepository.BillingServiceComments.Include(x => x.Alerts.Select(s => s.Screens)).FirstOrDefault(x => x.Id == billingServiceComment.Id);
            screens = PracticeRepository.Alerts.Include(x => x.Screens).Where(x => x.BillingServiceCommentId == billingServiceComment.Id).SelectMany(x => x.Screens).ToArray();
            Assert.IsNotNull(billingServiceComment);
            Assert.AreEqual(billingServiceComment.Value, viewContext.Note.Value);
            Assert.AreEqual(screens.Count(), viewContext.Note.AlertableScreens.Count);


            //Sending alert only.
            viewContext.Note.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 10));
            viewContext.Note.AlertId = PracticeRepository.Alerts.Where(x => x.BillingServiceCommentId == billingServiceComment.Id).Select(x => (int?)x.Id).FirstOrDefault();
            viewContext.SaveComment();

            screens = PracticeRepository.Alerts.Include(x => x.Screens).Where(x => x.BillingServiceCommentId == billingServiceComment.Id).SelectMany(x => x.Screens).ToArray();
            Assert.AreEqual(screens.Count(), viewContext.Note.AlertableScreens.Count);

            //Saving standard comment
            viewContext.Note.Name = "Test Comment";

            viewContext.SaveStandardTemplate.Execute(viewContext.Note);

            var standardNote = PracticeRepository.NoteTemplates.FirstOrDefault(x => x.Name == viewContext.Note.Name);
            Assert.IsNotNull(standardNote);
            Assert.AreEqual(standardNote.Value, viewContext.Note.Value);
            Assert.AreEqual(standardNote.Name, viewContext.Note.Name);

            if (PermissionId.DeleteStandardComments.EnsurePermission(false))
            {
                viewContext.DeleteNoteTemplate.Execute(standardNote.Id);
                Assert.AreEqual(viewContext.StandardNotes.Count, 0);
            }

        }

    }
}

