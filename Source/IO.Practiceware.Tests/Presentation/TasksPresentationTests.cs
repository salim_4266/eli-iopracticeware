﻿using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Tasks;
using IO.Practiceware.Services.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class TasksPresentationTests : TestBase
    {

        [TestMethod]
        public void TestAssignTaskPresenter()
        {
            List<User> users = Common.CreateUsers().ToList();
            users.ForEach(u => Common.PracticeRepository.Save(u));

            User user = users.First();
            Common.PracticeRepository.Save(user);
            User fetchUser = Common.PracticeRepository.Users.WithId(user.Id);
            Assert.IsNotNull(fetchUser);

            List<Group> groups = Common.CreateGroups().ToList();
            Group @group = groups.FirstOrDefault();
            Common.PracticeRepository.Save(@group);

            TaskActivityType taskactivitytype = Common.CreateTaskActivityType();
            Common.PracticeRepository.Save(taskactivitytype);

            Task task = Common.CreateTask();

            var arguments = Common.ServiceProvider.GetService<AssignTaskArguments>();
            arguments.AssignedBy = user.Id;
            arguments.Task = task;
            arguments.Title = "left eye hurts";
            arguments.Description = "Problem with new lenses:";
            arguments.AssignToUserIds = users.Select(u => u.Id).ToList();
            arguments.AssignToGroupIds = groups.Select(g => g.Id).ToList();
            arguments.Alert = false;
            arguments.AssignIndivudally = true;
            arguments.PatientId = task.Patient.Id;
            arguments.TaskActivityTypeId = taskactivitytype.Id;


            var presenter = Common.ServiceProvider.GetService<AssignTaskPresenter>();
            presenter.AssignTask(arguments);
            ApplicationContext.Current.PatientId = task.Patient.Id;
            presenter.Load();

            Assert.IsTrue(presenter.Groups.Count() > 0);
            Assert.IsTrue(presenter.Users.Count() > 0);
            Assert.IsNotNull(presenter.ActivePatient);
            Assert.IsTrue(presenter.TaskActivityTypes.Count() > 0);
        }

        [TestMethod]
        public void TestReviewTaskPresenter()
        {
            List<User> users = Common.CreateUsers().ToList();
            users.ForEach(u => Common.PracticeRepository.Save(u));
            User user = users.First();
            Common.PracticeRepository.Save(user);
            User fetchUser = Common.PracticeRepository.Users.WithId(user.Id);
            Assert.IsNotNull(fetchUser);

            List<Group> groups = Common.CreateGroups().ToList();
            Group @group = groups.FirstOrDefault();
            Common.PracticeRepository.Save(@group);

            TaskActivityType taskactivitytype = Common.CreateTaskActivityType();
            Common.PracticeRepository.Save(taskactivitytype);

            Task task = Common.CreateTask();

            var arguments = Common.ServiceProvider.GetService<AssignTaskArguments>();
            arguments.AssignedBy = user.Id;
            arguments.Task = task;
            arguments.Title = "left eye hurts";
            arguments.Description = "Problem with new lenses:";
            arguments.AssignToUserIds = users.Select(u => u.Id).ToList();
            arguments.AssignToGroupIds = groups.Select(g => g.Id).ToList();
            arguments.Alert = false;
            arguments.AssignIndivudally = true;
            arguments.PatientId = task.Patient.Id;
            arguments.TaskActivityTypeId = taskactivitytype.Id;


            var presenter = Common.ServiceProvider.GetService<AssignTaskPresenter>();
            presenter.AssignTask(arguments);

            ApplicationContext.Current.PatientId = task.Patient.Id;
            var reviewTaskpresenter = Common.ServiceProvider.GetService<ReviewTasksPresenter>();
            reviewTaskpresenter.Arguments.AssignedToUserIds = arguments.AssignToUserIds.ToArray();
            reviewTaskpresenter.Load();

            Assert.IsNotNull(reviewTaskpresenter.ActiveUser);
            Assert.IsNotNull(reviewTaskpresenter.ActivePatient);
            Assert.IsTrue(reviewTaskpresenter.Users.Any());
            Assert.IsTrue(reviewTaskpresenter.Groups.Any());
            Assert.IsTrue(reviewTaskpresenter.TaskActivityTypes.Any());
            Assert.IsTrue(reviewTaskpresenter.Tasks.Any());
            Assert.IsTrue(reviewTaskpresenter.Tasks.First().TaskActivities.Any());
        }
    }
}