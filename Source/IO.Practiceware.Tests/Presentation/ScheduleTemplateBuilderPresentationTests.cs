﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Presentation;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ScheduleTemplateBuilderPresentationTests : TestBase
    {
      
        [TestMethod]
        public void TestInitScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            Assert.IsNotNull(scheduleTemplateBuilderViewContext.DefaultSpanLocation);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.SpanSize);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.AreaStart);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.AreaEnd);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.ScheduleBlocks);
            Assert.IsNull(scheduleTemplateBuilderViewContext.ListOfMinuteIncrementSelector);
            Assert.IsNull(scheduleTemplateBuilderViewContext.ListOfLocation);
            Assert.IsNull(scheduleTemplateBuilderViewContext.ListOfCategory);
            Assert.IsNull(scheduleTemplateBuilderViewContext.ListOfTemplate);
            Assert.IsNull(scheduleTemplateBuilderViewContext.SelectedTemplate);
            Assert.IsNull(scheduleTemplateBuilderViewContext.TemplateName);

            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier.DefaultSpanLocation);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier.SpanSize);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier.AreaStart);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier.AreaEnd);
            Assert.IsNotNull(scheduleTemplateBuilderViewContext.Applier.ScheduleBlocks);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ListOfTemplate.ToExtendedObservableCollection().Count == 0);
            Assert.IsNull(scheduleTemplateBuilderViewContext.Applier.SelectedTemplate);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ListOfResource.ToExtendedObservableCollection().Count == 0);
            Assert.IsNull(scheduleTemplateBuilderViewContext.Applier.SelectedResource);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.RadioDatesIsChecked);
            Assert.IsFalse(scheduleTemplateBuilderViewContext.Applier.RadioRepeatsIsChecked);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ListOfFrequency.ToExtendedObservableCollection().Count == 7);
            Assert.IsNull(scheduleTemplateBuilderViewContext.Applier.SelectedFrequency);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ApplyBeginDate == DateTime.Today);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ApplyEndDate == scheduleTemplateBuilderViewContext.Applier.ApplyBeginDate.AddYears(1));
            Assert.IsFalse(scheduleTemplateBuilderViewContext.Applier.HasScheduleBlockConflict);
        }

        [TestMethod]
        public void TestLoadScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(scheduleTemplateBuilderViewContext.ListOfLocation.ToExtendedObservableCollection().Count != 0);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.ListOfCategory.ToExtendedObservableCollection().Count != 0);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count != 0);

            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ListOfTemplate.ToExtendedObservableCollection().Count == 2);
            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.ListOfResource.ToExtendedObservableCollection().Count != 0);
        }

        [TestMethod]
        public void TestSaveScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Default template
            scheduleTemplateBuilderViewContext.SelectedTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First();
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDefaultTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isDefaultTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Create template
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First();
            var fromDatabaseCategory = Common.PracticeRepository.AppointmentCategories.First();
            var start = new TimeSpan(9, 0, 0);
            var end = new TimeSpan(9, 15, 0);
            var slot = scheduleTemplateBuilderViewContext.ScheduleBlocks.First(s => s.StartTime == start && s.EndTime == end);
            slot.IsSelected = true;
            var locationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorLocation = System.Windows.Media.Color.FromArgb(gdiColorLocation.A, gdiColorLocation.R, gdiColorLocation.G, gdiColorLocation.B);
            locationViewModel.Color = wpfColorLocation;
            slot.Location = locationViewModel;
            var categoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorCategory = System.Windows.Media.Color.FromArgb(gdiColorCategory.A, gdiColorCategory.R, gdiColorCategory.G, gdiColorCategory.B);
            categoryViewModel.Color = wpfColorCategory;
            slot.Categories.Add(categoryViewModel);

            var firstCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(firstCount == 2);

            // Perform save
            const string name = "TestSave";
            scheduleTemplateBuilderViewContext.TemplateName = name;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isCreateTemplateSave = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isCreateTemplateSave.Wait(TimeSpan.FromSeconds(10));

            scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Re-load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isReload = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isReload.Wait(TimeSpan.FromSeconds(10));

            var secoundCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(secoundCount == 3);
        }

        [TestMethod]
        public void TestSaveAsScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Default template
            scheduleTemplateBuilderViewContext.SelectedTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First();
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDefaultTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isDefaultTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Create template
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First();
            var fromDatabaseCategory = Common.PracticeRepository.AppointmentCategories.First();
            var start = new TimeSpan(9, 0, 0);
            var end = new TimeSpan(9, 15, 0);
            var slot = scheduleTemplateBuilderViewContext.ScheduleBlocks.First(s => s.StartTime == start && s.EndTime == end);
            slot.IsSelected = true;
            var locationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorLocation = System.Windows.Media.Color.FromArgb(gdiColorLocation.A, gdiColorLocation.R, gdiColorLocation.G, gdiColorLocation.B);
            locationViewModel.Color = wpfColorLocation;
            slot.Location = locationViewModel;
            var categoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorCategory = System.Windows.Media.Color.FromArgb(gdiColorCategory.A, gdiColorCategory.R, gdiColorCategory.G, gdiColorCategory.B);
            categoryViewModel.Color = wpfColorCategory;
            slot.Categories.Add(categoryViewModel);

            // Perform save
            const string name = "TestSave";
            scheduleTemplateBuilderViewContext.TemplateName = name;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isCreateTemplateSave = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isCreateTemplateSave.Wait(TimeSpan.FromSeconds(10));

            SetupTestContext(out scheduleTemplateBuilderViewContext);
            // Simulate UI - Pick newly created template
            var originalTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = originalTemplate;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            var firstCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(firstCount == 3);

            var originalSlotsCount = scheduleTemplateBuilderViewContext.ScheduleBlocks.Count();

            // Perform save as
            const string newName = "TestSaveAs";
            scheduleTemplateBuilderViewContext.TemplateName = newName;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isSaveAs = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isSaveAs.Wait(TimeSpan.FromSeconds(10));
            
            SetupTestContext(out scheduleTemplateBuilderViewContext);
            var secoundCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(secoundCount == 4);

            // Simulate UI - Pick save as created template
            var newTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(newName, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = newTemplate;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isNewTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isNewTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(scheduleTemplateBuilderViewContext.ScheduleBlocks.Count == originalSlotsCount);
        }

        [TestMethod]
        public void TestEditScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Default template
            scheduleTemplateBuilderViewContext.SelectedTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First();
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDefaultTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isDefaultTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Create template
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First();
            var fromDatabaseCategory = Common.PracticeRepository.AppointmentCategories.First();
            var start = new TimeSpan(9, 0, 0);
            var end = new TimeSpan(9, 15, 0);
            var slot = scheduleTemplateBuilderViewContext.ScheduleBlocks.First(s => s.StartTime == start && s.EndTime == end);
            slot.IsSelected = true;
            var locationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorLocation = System.Windows.Media.Color.FromArgb(gdiColorLocation.A, gdiColorLocation.R, gdiColorLocation.G, gdiColorLocation.B);
            locationViewModel.Color = wpfColorLocation;
            slot.Location = locationViewModel;
            var categoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorCategory = System.Windows.Media.Color.FromArgb(gdiColorCategory.A, gdiColorCategory.R, gdiColorCategory.G, gdiColorCategory.B);
            categoryViewModel.Color = wpfColorCategory;
            slot.Categories.Add(categoryViewModel);

            // Perform save
            const string name = "TestSave";
            scheduleTemplateBuilderViewContext.TemplateName = name;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isCreateTemplateSave = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isCreateTemplateSave.Wait(TimeSpan.FromSeconds(10));

            SetupTestContext(out scheduleTemplateBuilderViewContext);
            // Simulate UI - Pick newly created template
            var originalTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = originalTemplate;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            var firstCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(firstCount == 3);

            // Simulate UI - Add 
            var newStart = new TimeSpan(9, 0, 0);
            var newEnd = new TimeSpan(9, 15, 0);
            var newSlot = scheduleTemplateBuilderViewContext.SelectedTemplate.ScheduleBlocks.First(s => s.StartTime == newStart && s.EndTime == newEnd);
            newSlot.IsSelected = true;
            var newlocationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorNewLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorNewLocation = System.Windows.Media.Color.FromArgb(gdiColorNewLocation.A, gdiColorNewLocation.R, gdiColorNewLocation.G, gdiColorNewLocation.B);
            newlocationViewModel.Color = wpfColorNewLocation;
            newSlot.Location = newlocationViewModel;
            var newCategoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorNewCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorNewCategory = System.Windows.Media.Color.FromArgb(gdiColorNewCategory.A, gdiColorNewCategory.R, gdiColorNewCategory.G, gdiColorNewCategory.B);
            newCategoryViewModel.Color = wpfColorNewCategory;
            newSlot.Categories.Add(newCategoryViewModel);

            // Perform edit
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isEdit = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isEdit.Wait(TimeSpan.FromSeconds(10));

            SetupTestContext(out scheduleTemplateBuilderViewContext);
            var secoundCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(secoundCount == 3);

            // Simulate UI - Pick edited template
            var selected = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = selected;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isEditedTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isEditedTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            var listOfCategoryViewModel = new List<CategoryViewModel>();
            var listOfList = scheduleTemplateBuilderViewContext.SelectedTemplate.ScheduleBlocks.Select(s => s.Categories);
            foreach (var list in listOfList)
            {
                listOfCategoryViewModel.AddRange(list);
            }

            Assert.IsTrue(listOfCategoryViewModel.Count == 2);

            var first = listOfCategoryViewModel.First();
            var second = listOfCategoryViewModel.Last();
            Assert.IsTrue(first.Id == second.Id && first.Name == second.Name);
        }

        private static void SetupTestContext(out ScheduleTemplateBuilderViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, ScheduleTemplateBuilderViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
        }

        [TestMethod]
        public void TestDeleteScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Default template
            scheduleTemplateBuilderViewContext.SelectedTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First();
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDefaultTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isDefaultTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Create template
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First();
            var fromDatabaseCategory = Common.PracticeRepository.AppointmentCategories.First();
            var start = new TimeSpan(9, 0, 0);
            var end = new TimeSpan(9, 15, 0);
            var slot = scheduleTemplateBuilderViewContext.ScheduleBlocks.First(s => s.StartTime == start && s.EndTime == end);
            slot.IsSelected = true;
            var locationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorLocation = System.Windows.Media.Color.FromArgb(gdiColorLocation.A, gdiColorLocation.R, gdiColorLocation.G, gdiColorLocation.B);
            locationViewModel.Color = wpfColorLocation;
            slot.Location = locationViewModel;
            var categoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorCategory = System.Windows.Media.Color.FromArgb(gdiColorCategory.A, gdiColorCategory.R, gdiColorCategory.G, gdiColorCategory.B);
            categoryViewModel.Color = wpfColorCategory;
            slot.Categories.Add(categoryViewModel);

            // Perform save
            const string name = "TestSave";
            scheduleTemplateBuilderViewContext.TemplateName = name;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isCreateTemplateSave = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isCreateTemplateSave.Wait(TimeSpan.FromSeconds(10));

            scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();
            // Re-load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isReload = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isReload.Wait(TimeSpan.FromSeconds(10));

            var firstCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(firstCount == 3);

            // Simulate UI - Pick newly created template
            var originalTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = originalTemplate;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Perform delete
            scheduleTemplateBuilderViewContext.CreateTemplateDelete.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDelete = () => !scheduleTemplateBuilderViewContext.CreateTemplateDelete.CastTo<AsyncCommand>().IsBusy;
            isDelete.Wait(TimeSpan.FromSeconds(10));

            scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();
            // Re-load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isReloaded = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isReloaded.Wait(TimeSpan.FromSeconds(10));

            var secoundCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(secoundCount == 2);
        }

        [TestMethod]
        public void TestApplyScheduleTemplateBuilderViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ScheduleTemplateBuilder.ScheduleTemplateBuilderMetaData);
            var scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();

            // Load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isLoad = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Default template
            scheduleTemplateBuilderViewContext.SelectedTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First();
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isDefaultTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isDefaultTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Create template
            var fromDatabaseLocation = Common.PracticeRepository.ServiceLocations.First();
            var fromDatabaseCategory = Common.PracticeRepository.AppointmentCategories.First();
            var start = new TimeSpan(9, 0, 0);
            var end = new TimeSpan(9, 15, 0);

            var slot = scheduleTemplateBuilderViewContext.ScheduleBlocks.First(s => s.StartTime == start && s.EndTime == end);
            slot.IsSelected = true;

            var locationViewModel = new ColoredViewModel {Id = fromDatabaseLocation.Id, Name = fromDatabaseLocation.Name};
            System.Drawing.Color gdiColorLocation = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseLocation.HexColor);
            System.Windows.Media.Color wpfColorLocation = System.Windows.Media.Color.FromArgb(gdiColorLocation.A, gdiColorLocation.R, gdiColorLocation.G, gdiColorLocation.B);
            locationViewModel.Color = wpfColorLocation;
            slot.Location = locationViewModel;

            var newCategoryViewModel = new CategoryViewModel {Id = fromDatabaseCategory.Id, Name = fromDatabaseCategory.Name};
            System.Drawing.Color gdiColorNewCategory = System.Drawing.ColorTranslator.FromHtml("#" + fromDatabaseCategory.HexColor);
            System.Windows.Media.Color wpfColorNewCategory = System.Windows.Media.Color.FromArgb(gdiColorNewCategory.A, gdiColorNewCategory.R, gdiColorNewCategory.G, gdiColorNewCategory.B);
            newCategoryViewModel.Color = wpfColorNewCategory;
            slot.Categories.Add(newCategoryViewModel);

            // Perform save
            const string name = "TestSave";
            scheduleTemplateBuilderViewContext.TemplateName = name;
            scheduleTemplateBuilderViewContext.ConfirmCreateTemplateSave.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isCreateTemplateSave = () => !scheduleTemplateBuilderViewContext.SaveCommand.CastTo<AsyncCommand>().IsBusy;
            isCreateTemplateSave.Wait(TimeSpan.FromSeconds(10));

            scheduleTemplateBuilderViewContext = Common.ServiceProvider.GetService<ScheduleTemplateBuilderViewContext>();
            // Re-load UI data
            scheduleTemplateBuilderViewContext.Load.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isReload = () => scheduleTemplateBuilderViewContext.Load.CanExecute(null);
            isReload.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Pick newly created template
            var originalTemplate = scheduleTemplateBuilderViewContext.ListOfTemplate.First(t => t.Name.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.SelectedTemplate = originalTemplate;
            scheduleTemplateBuilderViewContext.TemplateChange.Execute(scheduleTemplateBuilderViewContext.InteractionContext);
            Func<bool> isTemplateChanged = () => scheduleTemplateBuilderViewContext.TemplateChange.CanExecute(null);
            isTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            var firstCount = scheduleTemplateBuilderViewContext.ListOfTemplate.ToExtendedObservableCollection().Count;
            Assert.IsTrue(firstCount == 3);

            // Simulate UI - Switch to Apply tab
            scheduleTemplateBuilderViewContext.Applier.Load.Execute(scheduleTemplateBuilderViewContext.Applier.InteractionContext);
            Func<bool> isApplierLoad = () => scheduleTemplateBuilderViewContext.Applier.Load.CanExecute(null);
            isApplierLoad.Wait(TimeSpan.FromSeconds(10));

            /////// Simulate UI - Pick options to apply template ///////

            Assert.IsNull(scheduleTemplateBuilderViewContext.Applier.GeneratedDates);
            scheduleTemplateBuilderViewContext.Applier.GeneratedDates = new System.Collections.ObjectModel.ObservableCollection<DateTime>();

            // Simulate UI - Select template to apply
            var templateToApply = scheduleTemplateBuilderViewContext.Applier.ListOfTemplate.First(t => t.Item1.Equals(name, StringComparison.InvariantCulture));
            scheduleTemplateBuilderViewContext.Applier.SelectedTemplate = templateToApply;
            scheduleTemplateBuilderViewContext.Applier.TemplateChanged.Execute(scheduleTemplateBuilderViewContext.Applier.InteractionContext);
            Func<bool> isApplierTemplateChanged = () => scheduleTemplateBuilderViewContext.Applier.TemplateChanged.CanExecute(null);
            isApplierTemplateChanged.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Select user
            var applierUser = scheduleTemplateBuilderViewContext.Applier.ListOfResource.First();
            scheduleTemplateBuilderViewContext.Applier.SelectedResource = applierUser;

            // Simulate UI - Select Monday
            scheduleTemplateBuilderViewContext.Applier.SetDayOfWeek.Execute(DayOfWeek.Monday);
            Func<bool> isApplierSetDayOfWeek = () => scheduleTemplateBuilderViewContext.Applier.SetDayOfWeek.CanExecute(null);
            isApplierSetDayOfWeek.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Select Every Week
            FrequencyPerMonthWrapper frequencyPerMonthWrapper = scheduleTemplateBuilderViewContext.Applier.ListOfFrequency.First(); // All
            scheduleTemplateBuilderViewContext.Applier.SelectedFrequency = frequencyPerMonthWrapper;
            scheduleTemplateBuilderViewContext.Applier.SetFrequencyPerMonth.Execute(scheduleTemplateBuilderViewContext.Applier.InteractionContext);
            Func<bool> isApplierSetFrequencyPerMonth = () => scheduleTemplateBuilderViewContext.Applier.SetFrequencyPerMonth.CanExecute(null);
            isApplierSetFrequencyPerMonth.Wait(TimeSpan.FromSeconds(10));

            // Simulate UI - Pick date options to apply template
            var startDateTime = new DateTime(2012, 8, 1);
            var endDateTime = new DateTime(2012, 8, 8);
            scheduleTemplateBuilderViewContext.Applier.ApplyBeginDate = startDateTime;
            scheduleTemplateBuilderViewContext.Applier.ApplyEndDate = endDateTime;

            Assert.IsTrue(scheduleTemplateBuilderViewContext.Applier.Dates.Count == 1);

            // Perform apply
            scheduleTemplateBuilderViewContext.Applier.Apply.Execute(scheduleTemplateBuilderViewContext.Applier.InteractionContext);
            Func<bool> isApply = () => scheduleTemplateBuilderViewContext.Applier.Apply.CanExecute(null);
            isApply.Wait(TimeSpan.FromSeconds(20));
        }


    }
}