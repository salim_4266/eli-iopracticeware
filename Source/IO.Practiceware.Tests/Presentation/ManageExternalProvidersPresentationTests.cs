﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Linq;
using IO.Practiceware.Presentation.Views.ExternalProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ManageExternalProvidersPresentationTests : X12MetadataTestsBase
    {
        #region public test methods

        [TestMethod]
        public void TestManageExternalProvidersViewContextLoadAndFilter()
        {
            if (PermissionId.ViewManageExternalProviders.EnsurePermission(false)) { 
                ExternalProvidersViewContext viewContext;
                SetupTestContext(out viewContext);
                var allExternalContacts = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().ToList();
                Assert.AreEqual(allExternalContacts.Count, viewContext.ExternalProviders.Count);

                viewContext.LoadProviders.ExecuteAndWait("S");
                var filteredExternalProviders = allExternalContacts.Where(x => x.LastNameOrEntityName.ContainsIgnoreCase("S") || x.FirstName.ContainsIgnoreCase("S")).ToList();
                Assert.AreEqual(filteredExternalProviders.Count, viewContext.ExternalProviders.Count);

                viewContext.Filter.ExecuteAndWait("Su");
                var filteredCount = filteredExternalProviders.Where(x => x.LastNameOrEntityName.ContainsIgnoreCase("Su") || x.FirstName.ContainsIgnoreCase("Su")).ToList().Count;
                Assert.AreEqual(filteredCount, viewContext.FilteredExternalProviders.Count);
            }
        }


        [TestMethod]
        public void TestManageExternalProvidersViewAddEditAndDeleteExternalProvider()
        {
            PermissionId.ViewManageExternalProviders.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.CreateExternalProviders.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.EditExternalProviders.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.DeleteExternalProvider.AddPermissionToCurrentUser(PracticeRepository);
            ExternalProvidersViewContext viewContext;
            SetupTestContext(out viewContext);

            viewContext.CreateNew.Execute();
            
            var newExternalProvider = viewContext.ExternalProviders.Last();
            viewContext.SelectedExternalProvider = newExternalProvider;
            var newExternalProviderInfo = Common.CreateExternalProvider();
            newExternalProvider.LastNameOrEntityName = newExternalProviderInfo.LastNameOrEntityName;
            newExternalProvider.FirstName = newExternalProviderInfo.FirstName;
            newExternalProvider.NickName = newExternalProviderInfo.NickName;

            var state = PracticeRepository.StateOrProvinces.Single(x => x.Abbreviation.Equals("NY"));
            newExternalProvider.Address = new AddressViewModel
            {
                Line1 = "123 Fake St",
                City = "Brooklyn",
                PostalCode = "11204",
                StateOrProvince = new StateOrProvinceViewModel
                {
                    Abbreviation = state.Abbreviation,
                    Name = state.Name,
                    Id = state.Id
                },
                AddressType = new NamedViewModel
                {
                    Id  = (int)ExternalContactAddressTypeId.MainOffice,
                    Name = ExternalContactAddressTypeId.MainOffice.GetDisplayName()
                }
            };

            newExternalProvider.Taxonomy = "123456";
            newExternalProvider.Npi = "8675309123";

            viewContext.AddNewEmailAddress.ExecuteAndWait();
            viewContext.AddNewPhoneNumber.ExecuteAndWait();

            var email = newExternalProvider.EmailAddresses.FirstOrDefault();

            Assert.IsNotNull(email);

            email.Value = "kbradford@iopracticeware.com";
            email.EmailAddressType = new NamedViewModel
            {
                Id = (int)EmailAddressType.Business,
                Name = EmailAddressType.Business.GetDisplayName()
            };

            var phoneNumberViewModel = newExternalProvider.PhoneNumbers.FirstOrDefault();

            Assert.IsNotNull(phoneNumberViewModel);

            phoneNumberViewModel.AreaCode = "718";
            phoneNumberViewModel.ExchangeAndSuffix = "232-9562";
            phoneNumberViewModel.PhoneType = new NamedViewModel
            {
                Id = (int)ExternalContactAddressTypeId.MainOffice,
                Name = ExternalContactAddressTypeId.MainOffice.GetDisplayName()
            };


            viewContext.Save.Execute();

            var savedNewExternalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId(newExternalProvider.Id);

            PracticeRepository.AsQueryableFactory().Load(savedNewExternalProvider,
                x => x.ExternalContactAddresses.Select(sop => new
                {
                    sop.StateOrProvince.Country,
                    sop.ExternalContactAddressType
                }),
               x => x.ExternalContactPhoneNumbers.Select(pn => pn.ExternalContactPhoneNumberType),
               x => x.ExternalContactEmailAddresses,
               x => x.IdentifierCodes
           );

            // Check name values
            Assert.AreEqual(savedNewExternalProvider.LastNameOrEntityName, newExternalProvider.LastNameOrEntityName);
            Assert.AreEqual(savedNewExternalProvider.FirstName, newExternalProvider.FirstName);
            Assert.AreEqual(savedNewExternalProvider.NickName, newExternalProvider.NickName);

            // Check Identifier Codes
            Assert.AreEqual(newExternalProvider.Taxonomy, savedNewExternalProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy));
            Assert.AreEqual(newExternalProvider.Npi, savedNewExternalProvider.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi));

            // Check address
            Assert.IsNotNull(savedNewExternalProvider.ExternalContactAddresses);
            Assert.AreEqual(savedNewExternalProvider.ExternalContactAddresses.Count, 1);
            var savedAddress = savedNewExternalProvider.ExternalContactAddresses.First();
            Assert.AreEqual(savedAddress.Line1, newExternalProvider.Address.Line1);
            Assert.AreEqual(savedAddress.City, newExternalProvider.Address.City);
            Assert.AreEqual(savedAddress.PostalCode, newExternalProvider.Address.PostalCode);
            Assert.AreEqual(savedAddress.StateOrProvinceId, newExternalProvider.Address.StateOrProvince.Id);

            // check email
            Assert.IsNotNull(savedNewExternalProvider.ExternalContactEmailAddresses);
            Assert.AreEqual(savedNewExternalProvider.ExternalContactEmailAddresses.Count, 1);
            var savedEmail = savedNewExternalProvider.ExternalContactEmailAddresses.First();
            Assert.AreEqual(savedEmail.Value, newExternalProvider.EmailAddresses.First().Value);

            // check phone number
            Assert.IsNotNull(savedNewExternalProvider.ExternalContactPhoneNumbers);
            Assert.AreEqual(savedNewExternalProvider.ExternalContactPhoneNumbers.Count, 1);
            var phoneNumber = savedNewExternalProvider.ExternalContactPhoneNumbers.First();
            Assert.AreEqual(phoneNumber.AreaCode, phoneNumberViewModel.AreaCode);
            Assert.AreEqual(phoneNumber.ExchangeAndSuffix, phoneNumberViewModel.ExchangeAndSuffix);

            //Delete Address, Phone Number and Email Address
            viewContext.DeleteAddress.ExecuteAndWait();
            viewContext.DeleteEmailAddress.ExecuteAndWait(newExternalProvider.EmailAddresses.First());
            viewContext.DeletePhoneNumber.ExecuteAndWait(phoneNumberViewModel);

            // Save
            viewContext.Save.ExecuteAndWait();
            savedNewExternalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId(newExternalProvider.Id);

            PracticeRepository.AsQueryableFactory().Load(savedNewExternalProvider,
                x => x.ExternalContactAddresses.Select(sop => new
                {
                    sop.StateOrProvince.Country,
                    sop.ExternalContactAddressType
                }),
               x => x.ExternalContactPhoneNumbers.Select(pn => pn.ExternalContactPhoneNumberType),
               x => x.ExternalContactEmailAddresses,
               x => x.IdentifierCodes
           );

            // Test that they are no longer loaded from the Database

            Assert.AreEqual(savedNewExternalProvider.ExternalContactAddresses.Count, 0);
            Assert.AreEqual(savedNewExternalProvider.ExternalContactEmailAddresses.Count, 0);
            Assert.AreEqual(savedNewExternalProvider.ExternalContactPhoneNumbers.Count, 0);

            viewContext.DeleteExternalProvider.Execute(newExternalProvider);

            viewContext.Save.ExecuteAndWait();

            savedNewExternalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId(newExternalProvider.Id);

            // Make sure it was deleted
            Assert.IsNull(savedNewExternalProvider);
        }
        
        #endregion

        #region private methods

        private static void SetupTestContext(out ExternalProvidersViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, ExternalProvidersViewContext>>(); 
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
            viewContext.LoadProviders.ExecuteAndWait(string.Empty);
        }
        #endregion
    }
}
