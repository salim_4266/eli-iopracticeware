﻿using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using IO.Practiceware.Presentation.Views.ClinicalInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ClinicalInfoTests : TestBase
    {
        protected override void OnAfterTestInitialize()
        {
            Common.RecreateViewAsTable<PatientDiagnosis>();

            base.OnAfterTestInitialize();
        }

        [TestMethod]
        public void TestReconcilePatientProblemsViewContextCanLoad()
        {
            var viewContext = SetupReconcilePatientProblemsViewContext();

            Assert.IsTrue(viewContext.Problems.Count == 0);
        }

        [TestMethod]
        public void TestReconcilePatientProblemsViewContextCanMerge()
        {
            var viewContext = SetupReconcilePatientProblemsViewContext();

            // Load problems from Cda
            if (viewContext.PatientInfoViewModel.PatientId != null)
                viewContext.SelectedCdaForImport = ImportCdaTestResource(viewContext.PatientInfoViewModel.PatientId.Value);
            viewContext.LoadImportCdaProblems.ExecuteAndWait();

            Assert.IsTrue(viewContext.ImportCdaProblems.Count == 1);

            // Merge them
            viewContext.SelectedImportCdaProblems.AddRangeWithSinglePropertyChangedNotification(viewContext.ImportCdaProblems);
            viewContext.Merge.ExecuteAndWait();

            Assert.IsTrue(viewContext.Problems.Count == 1);
        }

        [TestMethod]
        public void TestReconcileMedicationAllergiesViewContextCanLoad()
        {
            var viewContext = SetupReconcileMedicationAllergiesViewContext();

            Assert.IsTrue(viewContext.MedicationAllergies.Count == 0);
        }

        [TestMethod]
        public void TestReconcileMedicationAllergiesViewContextCanMerge()
        {
            var viewContext = SetupReconcileMedicationAllergiesViewContext();

            // Load problems from Cda
            if (viewContext.PatientInfoViewModel.PatientId != null) viewContext.SelectedCdaForImport = ImportCdaTestResource(viewContext.PatientInfoViewModel.PatientId.Value);
            viewContext.LoadImportCdaAllergies.ExecuteAndWait();

            Assert.IsTrue(viewContext.ImportCdaAllergies.Count == 1);

            // Merge them
            viewContext.SelectedImportCdaAllergies.AddRangeWithSinglePropertyChangedNotification(viewContext.ImportCdaAllergies);
            viewContext.Merge.ExecuteAndWait();

            Assert.IsTrue(viewContext.MedicationAllergies.Count == 1);
        }

        [TestMethod]
        public void TestEditProblemListViewContextCanLoad()
        {
            var viewContext = SetupEditProblemListViewContext();

            Assert.IsNotNull(viewContext.Values);
            Assert.IsTrue(viewContext.Values.LateralityValues.Count > 0);
            Assert.IsTrue(viewContext.Problems.Count == 0);
        }

        [TestMethod]
        public void TestEditProblemListViewContextCanAddProblem()
        {
            var viewContext = SetupEditProblemListViewContext();

            Assert.IsTrue(viewContext.Problems.Count == 0);
            Assert.IsTrue(viewContext.SuggestedClinicalProblemNames.Count > 0);
            Assert.IsTrue(viewContext.Values.StatusValues.Count > 0);

            // Add a new problem
            PermissionId.CanEditProblemList.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.AddProblem.ExecuteAndWait();

            Assert.IsTrue(viewContext.Problems.Count == 1);

            // Assign description and status
            viewContext.Problems[0].Description = viewContext.SuggestedClinicalProblemNames[0];
            viewContext.Problems[0].Status = viewContext.Values.StatusValues[0];

            // Save
            viewContext.Save.ExecuteAndWait();

            // Verify both in DB and context
            Assert.IsTrue(PracticeRepository.PatientDiagnoses.Count() == 1);
            Assert.IsTrue(viewContext.Problems.Count == 1);
        }

        #region Helpers

        EditProblemListViewContext SetupEditProblemListViewContext()
        {
            // Prepare required entities in database
            var testPatient = CreateAndSavePatient();
            var clinicalCondition = Common.CreateClinicalCondition();
            PracticeRepository.Save(new object[] { clinicalCondition, testPatient });

            // Setup view context
            var viewContext = Common.ServiceProvider.GetService<EditProblemListViewContext>();
            viewContext.SetPatientInfo(new PatientInfoViewModel
                {
                    PatientId = testPatient.Id
                });

            // Add a suggested clinical problem, so that we don't have to run suggest in tests
            viewContext.SuggestedClinicalProblemNames = new ExtendedObservableCollection<NamedViewModel>
                {
                    new NamedViewModel(clinicalCondition.Id, clinicalCondition.Name)
                };

            // Add permissions and load
            PermissionId.CanViewProblemList.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.Load.ExecuteAndWait();

            return viewContext;
        }

        ReconcilePatientProblemsViewContext SetupReconcilePatientProblemsViewContext()
        {
            // Prepare required entities in database
            var testPatient = CreateAndSavePatient();
            PracticeRepository.Save(testPatient);

            // Setup view context
            var viewContext = Common.ServiceProvider.GetService<ReconcilePatientProblemsViewContext>();
            viewContext.SetPatientInfo(new PatientInfoViewModel
            {
                PatientId = testPatient.Id
            });

            // Add permissions and load
            PermissionId.CanViewProblemList.AddPermissionToCurrentUser(PracticeRepository);
            PermissionId.CanReconcileClinical.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.Load.ExecuteAndWait();

            return viewContext;
        }

        ReconcileMedicationAllergiesViewContext SetupReconcileMedicationAllergiesViewContext()
        {
            // Prepare required entities in database
            var testPatient = CreateAndSavePatient();

            // Setup view context
            var viewContext = Common.ServiceProvider.GetService<ReconcileMedicationAllergiesViewContext>();
            viewContext.SetPatientInfo(new PatientInfoViewModel
            {
                PatientId = testPatient.Id
            });

            // Add permissions and load
            PermissionId.CanReconcileClinical.AddPermissionToCurrentUser(PracticeRepository);
            viewContext.Load.ExecuteAndWait();

            return viewContext;
        }

        private Patient CreateAndSavePatient()
        {
            var patient = Common.CreatePatient();

            // Appointment is required when inserting into dbo.PatientClinical (allergies would go there). Ensure one exists
            var app = Common.CreateAppointment(patient);
            app.Encounter.EncounterStatusId = (int)EncounterStatus.Discharged;
            PracticeRepository.Save(app);

            return patient;
        }

        ImportedCdaViewModel ImportCdaTestResource(int patientId)
        {
            // TODO: if some of this mappings is later included in migration -> remove the scripting of it here
            // Find external entity ids
            var allergenReactionTypeSnomedCt = PracticeRepository.ExternalSystemEntities
                .First(e => e.Name == "AllergenReactionType" && e.ExternalSystem.Name == "SnomedCt");
            var clinicalConditionSnomedCt = PracticeRepository.ExternalSystemEntities
                .First(e => e.Name == "ClinicalCondition" && e.ExternalSystem.Name == "SnomedCt");
            var clinicalQualifierSnomedCt = PracticeRepository.ExternalSystemEntities
                .First(e => e.Name == "ClinicalQualifier" && e.ExternalSystem.Name == "SnomedCt");
            var allergenRxNorm = PracticeRepository.ExternalSystemEntities
                .First(e => e.Name == "Allergen" && e.ExternalSystem.Name == "RxNorm");

            var clinicalCondition = new ClinicalCondition { Name = "Sinusitis", OrdinalId = 0 };

            // TODO: remove this when qualifier categories are scripted
            var allergen = new Allergen { Name = "Penicillin G benzathine", IsAdverseDrugReaction = true, OrdinalId = 0 };

            // Save so that we get Ids
            PracticeRepository.Save(new object[] { allergen, clinicalCondition });

            var clinicalQualifier = new ClinicalQualifier
            {
                Name = "Moderate to severe",
                OrdinalId = 0,
                ClinicalQualifierCategory = new ClinicalQualifierCategory
                {
                    Name = ClinicalQualifierCategory.Severity,
                    IsSingleUse = true,
                    OrdinalId = 0,
                }
            };
            PracticeRepository.Save(clinicalQualifier);

            // Create practice entities for mapping
            var allergenReactionType = new AllergenReactionType
            {
                Name = "Hives",
                OrdinalId = 0,
                ClinicalQualifier = new ClinicalQualifier
                {
                    OrdinalId = 0,
                    Name = "Test qualifier" ,
                    ClinicalQualifierCategory = new ClinicalQualifierCategory
                    {
                        Name = ClinicalQualifierCategory.Severity,
                        IsSingleUse = true,
                        OrdinalId = 0
                    }
                }
            };
            PracticeRepository.Save(allergenReactionType);

            // Create mappings
            PracticeRepository.Save(new[]
                {
                    new ExternalSystemEntityMapping
                        {
                            PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.AllergenReactionType,
                            PracticeRepositoryEntityKey = allergenReactionType.Id.ToString(),
                            ExternalSystemEntityId = allergenReactionTypeSnomedCt.Id,
                            ExternalSystemEntityKey = "247472004"
                        },
                    new ExternalSystemEntityMapping
                        {
                            PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.ClinicalCondition,
                            PracticeRepositoryEntityKey = clinicalCondition.Id.ToString(),
                            ExternalSystemEntityId = clinicalConditionSnomedCt.Id,
                            ExternalSystemEntityKey = "36971009"
                        },
                    new ExternalSystemEntityMapping
                        {
                            PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.ClinicalQualifier,
                            PracticeRepositoryEntityKey = clinicalQualifier.Id.ToString(),
                            ExternalSystemEntityId = clinicalQualifierSnomedCt.Id,
                            ExternalSystemEntityKey = "371924009"
                        },
                    new ExternalSystemEntityMapping
                        {
                            PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.Allergen,
                            PracticeRepositoryEntityKey = allergen.Id.ToString(),
                            ExternalSystemEntityId = allergenRxNorm.Id,
                            ExternalSystemEntityKey = "7982"
                        }
                });

            // Import CDA file (extra area testing, but the fastest way to get file in)
            var cdaImportService = Common.ServiceProvider.GetService<IClinicalDataFilesLinkFilesViewService>();
            cdaImportService.Save(new[] { new ImportAndLinkClinicalDataFileViewModel
                {
                    ClinicalDataFileType = ClinicalDataFileType.Ccda,
                    PatientId = patientId,
                    XmlContent = CdaResources.IO_TOC_CDA
                } });

            // Retrieve it through ImportClinicalSelectionViewContext (thus test it as well)
            var importClinicalViewContext = Common.ServiceProvider.GetService<ImportClinicalSelectionViewContext>();
            importClinicalViewContext.PatientId = patientId;
            importClinicalViewContext.Load.ExecuteAndWait();

            Assert.IsTrue(importClinicalViewContext.PatientImportedCdaFiles.Count == 1);
            return importClinicalViewContext.PatientImportedCdaFiles.First();
        }

        #endregion
    }
}
