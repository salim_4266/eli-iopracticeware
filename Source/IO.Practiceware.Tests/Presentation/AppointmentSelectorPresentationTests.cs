﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.Views.AppointmentSelector;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class AppointmentSelectorPresentationTests : TestBase
    {

        [TestMethod]
        public void TestInitAppointmentSelectorViewContext()
        {
            var appointmentSelectorViewContext = Common.ServiceProvider.GetService<AppointmentSelectorViewContext>();
            Assert.IsNull(appointmentSelectorViewContext.FilterViewModel);
            Assert.IsNull(appointmentSelectorViewContext.FilterSelectionViewModel);
            Assert.IsNull(appointmentSelectorViewContext.SelectedAppointments);
            Assert.IsNull(appointmentSelectorViewContext.SearchResults);
        }

        [TestMethod]
        public void TestLoadAppointmentSelectorViewContext()
        {
            var appointmentSelectorViewContext = Common.ServiceProvider.GetService<AppointmentSelectorViewContext>();

            // Load UI data
            appointmentSelectorViewContext.Load.Execute(appointmentSelectorViewContext.InteractionContext);
            Func<bool> isLoaded = () => appointmentSelectorViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(appointmentSelectorViewContext.FilterViewModel);
            Assert.IsNotNull(appointmentSelectorViewContext.FilterViewModel.ScheduledDoctors);
            Assert.IsNotNull(appointmentSelectorViewContext.FilterViewModel.ServiceLocations);
            Assert.IsNotNull(appointmentSelectorViewContext.FilterViewModel.AppointmentTypes);

            Assert.IsNotNull(appointmentSelectorViewContext.FilterSelectionViewModel);
            Assert.IsNotNull(appointmentSelectorViewContext.FilterSelectionViewModel.StartDate);
            Assert.IsNotNull(appointmentSelectorViewContext.FilterSelectionViewModel.EndDate);
        }

        [TestMethod]
        public void TestSearchResultsAppointmentSelectorViewContext()
        {
            //Create Scheduled Doctor
            User doctor = Common.CreateUser();
            Common.PracticeRepository.Save(doctor);
            Assert.IsTrue(doctor.Id > 0);

            //Create Service Location
            ServiceLocation location = Common.CreateServiceLocation();
            Common.PracticeRepository.Save(location);
            Assert.IsTrue(location.Id > 0);

            // Create an AppointmentCategory
            AppointmentCategory appointmentCategory = Common.CreateAppointmentCategory();
            appointmentCategory.AppointmentTypes.Add(Common.PracticeRepository.AppointmentTypes.OrderByDescending(at => at.Id).First());
            Common.PracticeRepository.Save(appointmentCategory);
            Assert.IsTrue(appointmentCategory.Id > 0);

            AppointmentType appointmentType = Common.PracticeRepository.AppointmentTypes.WithId(appointmentCategory.AppointmentTypes.Last().Id);
            appointmentType.AppointmentCategory = Common.PracticeRepository.AppointmentCategories.WithId(appointmentCategory.Id);
            Common.PracticeRepository.Save(appointmentType);
            Assert.IsTrue(appointmentType.Id > 0);

            // Create an appointment
            UserAppointment appointment = Common.CreateAppointment();
            appointment.AppointmentType = appointmentType;
            appointment.User = doctor;
            appointment.Encounter.ServiceLocation = location;
            Common.PracticeRepository.Save(appointment);
            Assert.IsTrue(appointment.Id > 0);

            var appointmentSelectorViewContext = Common.ServiceProvider.GetService<AppointmentSelectorViewContext>();

            // Load UI data
            appointmentSelectorViewContext.Load.Execute(appointmentSelectorViewContext.InteractionContext);
            Func<bool> isLoaded = () => appointmentSelectorViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(appointmentSelectorViewContext.FilterViewModel.ScheduledDoctors.Count > 0);
            Assert.IsTrue(appointmentSelectorViewContext.FilterViewModel.ServiceLocations.Count > 0);
            Assert.IsTrue(appointmentSelectorViewContext.FilterViewModel.AppointmentTypes.Count > 0);

            appointmentSelectorViewContext.FilterSelectionViewModel.SelectedScheduledDoctors = appointmentSelectorViewContext.FilterViewModel.ScheduledDoctors;
            appointmentSelectorViewContext.FilterSelectionViewModel.SelectedServiceLocations = appointmentSelectorViewContext.FilterViewModel.ServiceLocations;
            NamedViewModel selectedAppointmentType = appointmentSelectorViewContext.FilterViewModel.AppointmentTypes.First(at => at.Id == appointment.AppointmentTypeId);
            appointmentSelectorViewContext.FilterSelectionViewModel.SelectedAppointmentTypes.Add(selectedAppointmentType);

            var startDate = new DateTime(2010, 1, 1, 10, 15, 0);
            appointmentSelectorViewContext.FilterSelectionViewModel.StartDate = startDate;
            appointmentSelectorViewContext.FilterSelectionViewModel.EndDate = startDate;

            // Perform search
            appointmentSelectorViewContext.Search.Execute(appointmentSelectorViewContext.InteractionContext);
            Func<bool> isResultsFound = () => appointmentSelectorViewContext.SearchResults != null && appointmentSelectorViewContext.SearchResults.Count > 0;

            Assert.IsTrue(isResultsFound.Wait(TimeSpan.FromSeconds(15)));
        }
    }
}