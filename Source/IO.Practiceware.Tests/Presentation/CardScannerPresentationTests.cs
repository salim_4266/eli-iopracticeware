﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IO.Practiceware.Presentation.ViewServices.Imaging;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Linq;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class CardScannerPresentationTests : TestBase
    {
        /// <summary>
        /// Tests The Patient demographics record insert using card scanner procesesed Data structure
        /// </summary>
        [TestMethod]
        public void TestInsertPatientInformationUsingCardScanner()
        {
            var imageService = Common.ServiceProvider.GetService<IImageService>();
            var cardScannerViewService = Common.ServiceProvider.GetService<ICardScannerViewService>();
            Patient patient = Common.CreatePatient();
            const string mode = "Insert";
            IEnumerable<CardScannerDataModel> cardScannerProcessedData = CreateCardScannerProcessedData(patient, mode);
            if (cardScannerProcessedData != null)
            {
                CardScannerProcessingReponse response = cardScannerViewService.SaveProcessedData(0, imageService.GetPatientPhoto(0), CardType.Identification, cardScannerProcessedData);
                Assert.IsTrue(response.PatientId > 0);
                Assert.IsTrue(string.IsNullOrEmpty(response.ValidationMessage));
            }
            Patient getPatient = Common.PracticeRepository.Patients.FirstOrDefault(p => p.FirstName.Contains(mode));
            Assert.IsNotNull(getPatient);
            Assert.IsTrue(getPatient.FirstName == patient.FirstName + " " + mode);
            Assert.IsTrue(getPatient.LastName == patient.LastName + " " + mode);
        }

        private static IEnumerable<CardScannerDataModel> CreateCardScannerProcessedData(Patient patient, string addMode)
        {
            var newProcessedData = new List<CardScannerDataModel>();

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Prefix, patient.Prefix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.FirstName, patient.FirstName + " " + addMode));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.LastName, patient.LastName + " " + addMode));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MiddleName, patient.MiddleName));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Suffix, patient.Suffix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.DateOfBirth, patient.DateOfBirth.ToString()));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine1, "BroadWay 291"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.City, "XYZ"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.State, "NY"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PostalCode, "10971"));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Email, "sunitha@docassistant.net"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Telephone, "212-844-0105"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ContractCode, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayEmergencyRoom, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayOfficeVisit, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopaySpecialist, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayUrgentCare, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Coverage, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Deductible, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.EffectiveDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Employer, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ExpirationDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupName, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.IssuerNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MemberId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PayerId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanAdministrator, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanType, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanProvider, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxBin, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxGroup, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxPcn, ""));
            ICollection<CardScannerDataModel> processedData = new ObservableCollection<CardScannerDataModel>();
            processedData.Clear();
            newProcessedData.ForEach(processedData.Add);
            return processedData;
        }

        /// <summary>
        /// Tests The Patient demographics record update using card scanner procesesed Data structure
        /// </summary>
        [TestMethod]
        public void TestUpdatePatientInformationUsingCardScanner()
        {
            var imageService = Common.ServiceProvider.GetService<IImageService>();
            var cardScannerViewService = Common.ServiceProvider.GetService<ICardScannerViewService>();
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsNotNull(patient);
            Assert.IsTrue(patient.Id > 0);
            const string mode = "Update";
            IEnumerable<CardScannerDataModel> cardScannerProcessedData = CreateCardScannerProcessedData(patient, mode);
            if (cardScannerProcessedData != null)
            {
                CardScannerProcessingReponse response = cardScannerViewService.SaveProcessedData(patient.Id, imageService.GetPatientPhoto(patient.Id), CardType.Identification, cardScannerProcessedData);
                Assert.IsTrue(response.PatientId > 0);
                Assert.IsTrue(string.IsNullOrEmpty(response.ValidationMessage));
                Patient getPatient = Common.PracticeRepository.Patients.FirstOrDefault(p => p.FirstName.Contains(mode));
                Assert.IsNotNull(getPatient);
                Assert.IsTrue(getPatient.FirstName == patient.FirstName + " " + mode);
                Assert.IsTrue(getPatient.LastName == patient.LastName + " " + mode);
            }
        }

        /// <summary>
        /// Tests The Patient demographics record insert using card scanner procesesed Data structure
        /// </summary>
        [TestMethod]
        public void TestInsertPatientInformationUsingCardScannerFailedWithValidationMessage()
        {
            var imageService = Common.ServiceProvider.GetService<IImageService>();
            var cardScannerViewService = Common.ServiceProvider.GetService<ICardScannerViewService>();
            Patient patient = Common.CreatePatient();
            const string mode = "Insert";
            IEnumerable<CardScannerDataModel> cardScannerProcessedData = CreateCardScannerProcessedDataWithMissingFields(patient, mode);
            if (cardScannerProcessedData != null)
            {
                CardScannerProcessingReponse response = cardScannerViewService.SaveProcessedData(0, imageService.GetPatientPhoto(0), CardType.Identification, cardScannerProcessedData);
                Assert.IsTrue(response.PatientId == 0);
                Assert.IsTrue(!string.IsNullOrEmpty(response.ValidationMessage));
            }
            Patient getPatient = Common.PracticeRepository.Patients.FirstOrDefault(p => p.FirstName.Contains(mode));
            Assert.IsNull(getPatient);
        }

        private static IEnumerable<CardScannerDataModel> CreateCardScannerProcessedDataWithMissingFields(Patient patient, string addMode)
        {
            var newProcessedData = new List<CardScannerDataModel>();

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Prefix, patient.Prefix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.FirstName, patient.FirstName + " " + addMode));
            //newProcessedData.Add(KeyValueViewModel.Create(CardScannerDataField.LastName, patient.LastName + " " + addMode));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MiddleName, patient.MiddleName));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Suffix, patient.Suffix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.DateOfBirth, patient.DateOfBirth.ToString()));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine1, "BroadWay 291"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.City, "XYZ"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.State, "NY"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PostalCode, "10971"));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Email, "sunitha@docassistant.net"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Telephone, "212-844-0105"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ContractCode, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayEmergencyRoom, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayOfficeVisit, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopaySpecialist, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayUrgentCare, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Coverage, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Deductible, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.EffectiveDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Employer, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ExpirationDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupName, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.IssuerNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MemberId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PayerId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanAdministrator, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanType, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanProvider, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxBin, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxGroup, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxPcn, ""));
            ICollection<CardScannerDataModel> processedData = new ObservableCollection<CardScannerDataModel>();
            processedData.Clear();
            newProcessedData.ForEach(processedData.Add);
            return processedData;
        }

        [TestMethod]
        public void TestInsertPatientInformationUsingCardScannerIgnoreInvalidAddress()
        {
            var imageService = Common.ServiceProvider.GetService<IImageService>();
            var cardScannerViewService = Common.ServiceProvider.GetService<ICardScannerViewService>();
            Patient patient = Common.CreatePatient();
            const string mode = "Insert";
            IEnumerable<CardScannerDataModel> cardScannerProcessedData = CreateCardScannerProcessedDataInvalidState(patient, mode);
            if (cardScannerProcessedData != null)
            {
                CardScannerProcessingReponse response = cardScannerViewService.SaveProcessedData(0, imageService.GetPatientPhoto(0), CardType.Identification, cardScannerProcessedData);
                Assert.IsTrue(response.PatientId > 0);
                Assert.IsTrue(string.IsNullOrEmpty(response.ValidationMessage));
            }
            Patient getPatient = Common.PracticeRepository.Patients.Include(p => p.PatientAddresses).FirstOrDefault(p => p.FirstName.Contains(mode));
            Assert.IsNotNull(getPatient);
            Assert.IsFalse(getPatient.PatientAddresses.Any());
            Assert.IsTrue(getPatient.FirstName == patient.FirstName + " " + mode);
            Assert.IsTrue(getPatient.LastName == patient.LastName + " " + mode);
        }

        private static IEnumerable<CardScannerDataModel> CreateCardScannerProcessedDataInvalidState(Patient patient, string addMode)
        {
            var newProcessedData = new List<CardScannerDataModel>();

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Prefix, patient.Prefix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.FirstName, patient.FirstName + " " + addMode));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.LastName, patient.LastName + " " + addMode));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MiddleName, patient.MiddleName));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Suffix, patient.Suffix));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.DateOfBirth, patient.DateOfBirth.ToString()));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.AddressLine1, "BroadWay 291"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.City, "XYZ"));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.State, "BB")); // This state doesn't exists in DB
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PostalCode, "10971"));

            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Email, "sunitha@docassistant.net"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Telephone, "212-844-0105"));


            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ContractCode, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayEmergencyRoom, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayOfficeVisit, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopaySpecialist, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.CopayUrgentCare, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Coverage, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Deductible, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.EffectiveDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.Employer, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.ExpirationDate, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupName, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.GroupNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.IssuerNumber, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.MemberId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PayerId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanAdministrator, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanType, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.PlanProvider, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxBin, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxGroup, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxId, ""));
            newProcessedData.Add(CardScannerDataModel.Create(CardScannerDataField.RxPcn, ""));
            ICollection<CardScannerDataModel> processedData = new ObservableCollection<CardScannerDataModel>();
            processedData.Clear();
            newProcessedData.ForEach(processedData.Add);
            return processedData;
        }
    }
}