﻿using System.Linq;
using System.Transactions;
using IO.Practiceware.Data;
using IO.Practiceware.Presentation.Views.Home;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class HomeScreenTests : TestBase
    {


        [TestMethod]
        public void TestHomeScreenToolbarContextLoad()
        {
            DbConnectionFactory.PracticeRepository.Execute("DELETE FROM model.ApplicationSettings");

            var toolbarContext = Common.ServiceProvider.GetService<HomeScreenToolbarContext>();
            toolbarContext.Loaded.ExecuteAndWait();

            // More than one group
            Assert.IsTrue(toolbarContext.FeaturesSelection.Count > 1);
            // More than one feature in every group
            toolbarContext.FeaturesSelection.ForEachWithSinglePropertyChangedNotification(g => Assert.IsTrue(g.Features.Count > 1));

            // No pinned features
            Assert.AreEqual(0, toolbarContext.PinnedFeatures.Count);
        }

        [TestMethod]
        public void TestHomeScreenToolbarContextPinnedStatePersists()
        {
            // Load
            var toolbarContext = Common.ServiceProvider.GetService<HomeScreenToolbarContext>();
            toolbarContext.Loaded.ExecuteAndWait();

            // Mark first feature in every group as pinned
            toolbarContext.FeaturesSelection
                .Select(g => g.Features.First())
                .ForEachWithSinglePropertyChangedNotification(f =>
                    {
                        f.IsPinned = true;
                        toolbarContext.PinnedFeatures.Add(f);
                    });

            // Simulate closing
            toolbarContext.Closing.ExecuteAndWait();

            // Get new context and load it
            var refreshedContext = Common.ServiceProvider.GetService<HomeScreenToolbarContext>();
            refreshedContext.Loaded.ExecuteAndWait();

            // Ensure same count
            Assert.AreEqual(toolbarContext.PinnedFeatures.Count, refreshedContext.PinnedFeatures.Count);
        }

        [TestMethod]
        public void TestHomeScreenWidgetsPanelContextLoad()
        {
            var widgetsContext = Common.ServiceProvider.GetService<HomeScreenWidgetsPanelContext>();
            widgetsContext.Loaded.ExecuteAndWait();

            // At least 2 widgets in selection
            Assert.IsTrue(widgetsContext.WidgetsSelection.Count >= 2);

            // Center container has 2 widgets by default
            Assert.IsTrue(widgetsContext.CenterContainer.Widgets.Count == 3);
            // First is Patient Search and second is Feature access
            Assert.AreEqual(widgetsContext.CenterContainer.Widgets.ElementAt(0).WidgetTemplateKey, "PatientSearch");
            Assert.AreEqual(widgetsContext.CenterContainer.Widgets.ElementAt(1).WidgetTemplateKey, "FeatureAccess");
        }

        [TestMethod]
        public void TestHomeScreenWidgetsPanelContextPersists()
        {
            // Load
            var widgetsContext = Common.ServiceProvider.GetService<HomeScreenWidgetsPanelContext>();
            widgetsContext.Loaded.ExecuteAndWait();

            // Center container has 2 widgets by default
            Assert.IsTrue(widgetsContext.CenterContainer.Widgets.Count == 3);

            // Add a third widget
            var thirdWidget = widgetsContext.CloneWidgetViewModelDelegate(widgetsContext.WidgetsSelection[0]);
            widgetsContext.CenterContainer.Widgets.Add(thirdWidget);

            // Simulate closing
            var t = Transaction.Current;
            widgetsContext.Dispatcher().Invoke(() =>
            {
                using (new DependentTransactionScope(t, true)) widgetsContext.InteractionContext.Complete();
            });
            // Get new context and load it
            var refreshedContext = Common.ServiceProvider.GetService<HomeScreenWidgetsPanelContext>();
            refreshedContext.Loaded.ExecuteAndWait();

            // Ensure same count
            Assert.AreEqual(widgetsContext.CenterContainer.Widgets.Count, refreshedContext.CenterContainer.Widgets.Count);
        }

        #region Helpers

        #endregion
    }
}
