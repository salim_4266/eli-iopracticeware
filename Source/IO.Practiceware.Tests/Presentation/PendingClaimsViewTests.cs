﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Financials.ManageClaims.PendingClaims;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Presentation;
using System.Collections.Generic;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PendingClaimsViewTests : X12MetadataTestsBase
    {

        private static void SetupTestContext(out PendingClaimsViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, PendingClaimsViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());
            viewContext.Load.ExecuteAndWait();
        }

        [TestMethod]
        public void TestPendingClaimsScreenAndInvoiceLoad()
        {
            PendingClaimsViewContext viewContext;
            SetupTestContext(out viewContext);

            var renderingProvidersCount = PracticeRepository.Providers.Count(x => (x.User != null && !x.User.IsArchived) || x.ServiceLocation != null);
            var serviceLocationsCount = PracticeRepository.ServiceLocations.Count();
            var startDate = PracticeRepository.InvoiceReceivables
                .Where(x => x.OpenForReview && x.Invoice.Encounter != null)
                .OrderBy(x => x.Invoice.Encounter.StartDateTime)
                .Select(x => x.Invoice.Encounter.StartDateTime).FirstOrDefault();
            viewContext.Load.Execute();

            Assert.AreEqual(viewContext.PendingClaimsLoadInformation.DataLists.RenderingProviders.Count, renderingProvidersCount);
            Assert.AreEqual(viewContext.PendingClaimsLoadInformation.DataLists.ServiceLocations.Count, serviceLocationsCount);

            Assert.IsNotNull(viewContext.Filter.StartDate);
            Assert.AreEqual(viewContext.Filter.StartDate.Value.Date, startDate.Date);
            Assert.IsNotNull(viewContext.Filter.EndDate);
            Assert.AreEqual(viewContext.Filter.EndDate.Value.Date, DateTime.Now.Date);

            viewContext.Filter.StartDate = Convert.ToDateTime("2011-05-04 10:00:00.000");
            viewContext.Filter.EndDate = Convert.ToDateTime("2011-06-04 10:00:00.000");

            viewContext.Filter.RenderingProviders.Add(
                new ProviderViewModel
                {
                    Provider = new NamedViewModel
                    {
                        Id = 1
                    },
                    BillingOrganization = new NamedViewModel { Id = 1 },
                    Id = 1
                }
                );
            viewContext.Filter.ServiceLocations.Add(
                new NamedViewModel { Id = 1 }
                );
            var invoices = LoadInvoices(viewContext.Filter);
            viewContext.DisplayResult.Execute();
            Assert.AreEqual(viewContext.PendingClaimsData.Invoices.Count, invoices.Count());

            viewContext.ResetFilter.Execute();
            invoices = LoadInvoices(viewContext.Filter);
            viewContext.DisplayResult.Execute();
            Assert.AreEqual(viewContext.PendingClaimsData.Invoices.Count, invoices.Count());
        }

        [TestMethod]
        public void TestPendingClaimsValdiationAndProcess()
        {
            if (PermissionId.ProcessPendingClaims.EnsurePermission(false))
            {
                PendingClaimsViewContext viewContext;
                SetupTestContext(out viewContext);
                viewContext.ResetFilter.Execute();
                viewContext.DisplayResult.Execute();
                //Select all the invoices
                viewContext.PendingClaimsData.SelectedInvoices = viewContext.PendingClaimsData.Invoices;
                viewContext.ProcessValidate.Execute();
                var invoices = LoadInvoices(viewContext.Filter);
                Assert.AreEqual(viewContext.PendingClaimsData.SelectedInvoices.Count(x => x.Errors.Any(y => y.Id == (int)ValidationMessage.MissingDiagnosis)), invoices.Count(x => !x.BillingDiagnoses.Any()));
                viewContext.Process.Execute();
                invoices = LoadInvoices(viewContext.Filter);
                Assert.AreEqual(invoices.Count(), viewContext.PendingClaimsData.Invoices.Count() - viewContext.PendingClaimsData.Invoices.Count(x => x.Status.Id == (int)InvoiceStatus.Bill || x.Status.Id == (int)InvoiceStatus.Close));
            }
        }

        private IEnumerable<Invoice> LoadInvoices(FilterViewModel filter)
        {
            var locationIds = filter.ServiceLocations.Select(x => x.Id).ToArray();
            var billingProviderIds = filter.RenderingProviders.Select(x => x.Id).ToArray();
            var startDate = filter.StartDate.HasValue ? filter.StartDate.Value.Date : (DateTime?)null;
            var endDate = filter.EndDate.HasValue ? filter.EndDate.Value.Date : (DateTime?)null;
            var invoiceReceivables = PracticeRepository.InvoiceReceivables.Where(x => x.OpenForReview);
            var invoices = invoiceReceivables.Select(ir => ir.Invoice).Distinct()
                    .Where(x => (startDate == null || x.Encounter.StartDateTime >= startDate)
                                && (endDate == null || x.Encounter.EndDateTime <= endDate)
                                && (!locationIds.Any() || locationIds.Contains(x.AttributeToServiceLocationId.Value)) &&
                              (!billingProviderIds.Any() || billingProviderIds.Contains(x.BillingProviderId)))
                    .Select(x => x).ToArray();
            PracticeRepository.AsQueryableFactory().Load(invoices, inv => inv.BillingDiagnoses);

            return invoices.ToArray();
        }
    }
}
