﻿using System;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Services.PatientSearch;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PatientSearchPresentationTests : TestBase
    {


        [TestMethod]
        public void TestInitPatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            Assert.IsNull(patientSearchViewContext.LoadArguments);

            Assert.IsTrue(patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.BeginsWith).IsSelected);
            Assert.IsTrue(patientSearchViewContext.SearchFields.Count(so => so.IsSelected) == 5);

            Assert.IsNull(patientSearchViewContext.SearchText);
            Assert.IsNull(patientSearchViewContext.HistoryResults);

            Assert.IsNull(patientSearchViewContext.SearchResults);
            Assert.IsNull(patientSearchViewContext.TermsToHighlight);
            Assert.IsNull(patientSearchViewContext.SelectedSearchResult);

            Assert.IsTrue(patientSearchViewContext.ShowPreSearchBorder);
            Assert.IsFalse(patientSearchViewContext.ShowNoResultsBorder);
            Assert.IsFalse(patientSearchViewContext.ShowResultsGrid);
        }

        [TestMethod]
        public void TestLoadPatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            var loadArguments = new PatientSearchLoadArguments();
            loadArguments.SearchText = "aetna";
            patientSearchViewContext.LoadArguments = loadArguments;

            // Load UI data
            patientSearchViewContext.Load.Execute(patientSearchViewContext.InteractionContext);
            Func<bool> isLoad = () => patientSearchViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(patientSearchViewContext.LoadArguments);
            Assert.IsTrue(patientSearchViewContext.SearchText.Equals("aetna", StringComparison.OrdinalIgnoreCase));
        }

        [TestMethod]
        public void TestSearchBeginsWithFirstNamePatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            // Simulate UI
            patientSearchViewContext.SearchText = "only";

            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.BeginsWith).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.FirstName).IsSelected = true;

            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.Contains).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.LastName).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PatientId).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PriorPatientCode).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PhoneNumber).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.DateOfBirth).IsSelected = false;

            // Search
            patientSearchViewContext.Search.Execute(patientSearchViewContext.InteractionContext);
            Func<bool> isSave = () => patientSearchViewContext.Search.CanExecute(null);
            isSave.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(patientSearchViewContext.SearchResults.Count == 3);
            Assert.IsTrue(patientSearchViewContext.TermsToHighlight.Count == 1);
            Assert.IsNull(patientSearchViewContext.SelectedSearchResult);
        }

        [TestMethod]
        public void TestSearchBeginsWithFirstNameLastNamePatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            // Simulate UI
            patientSearchViewContext.SearchText = "aetna";
            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.BeginsWith).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.FirstName).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.LastName).IsSelected = true;

            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.Contains).IsSelected = false;

            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PatientId).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PriorPatientCode).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PhoneNumber).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.DateOfBirth).IsSelected = false;

            // Search
            patientSearchViewContext.Search.Execute(patientSearchViewContext.InteractionContext);
            Func<bool> isSave = () => patientSearchViewContext.Search.CanExecute(null);
            isSave.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(patientSearchViewContext.SearchResults.Count == 3);
            Assert.IsTrue(patientSearchViewContext.TermsToHighlight.Count == 1);
            Assert.IsNull(patientSearchViewContext.SelectedSearchResult);
        }

        [TestMethod]
        public void TestSearchContainsLastNameTwoTermsPatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            // Simulate UI
            patientSearchViewContext.SearchText = "shield care";
            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.Contains).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.LastName).IsSelected = true;

            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.BeginsWith).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.FirstName).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PatientId).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PriorPatientCode).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PhoneNumber).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.DateOfBirth).IsSelected = false;

            // Search
            patientSearchViewContext.Search.Execute(patientSearchViewContext.InteractionContext);
            Func<bool> isSave = () => patientSearchViewContext.Search.CanExecute(null);
            isSave.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(patientSearchViewContext.SearchResults.Count == 0); //When only one checkbox is checked, do not split search text into multiple terms (if there are any spaces)
            Assert.IsTrue(patientSearchViewContext.TermsToHighlight.Count == 2);
            Assert.IsNull(patientSearchViewContext.SelectedSearchResult);
        }

        [TestMethod]
        public void TestSearchContainsFirstNameLastNameTwoTermsPatientSearchViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PatientSearch.PatientSearchMetaData);
            var patientSearchViewContext = Common.ServiceProvider.GetService<PatientSearchViewContext>();

            // Simulate UI
            patientSearchViewContext.SearchText = "care only"; // NOTE: SEARCH SEPARATELY SHOWS OVERLAP IN RESULTS 
            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.Contains).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.LastName).IsSelected = true;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.FirstName).IsSelected = true;

            patientSearchViewContext.TextSearchModes.First(aso => aso.Id == (int)TextSearchMode.BeginsWith).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PatientId).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PriorPatientCode).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.PhoneNumber).IsSelected = false;
            patientSearchViewContext.SearchFields.First(so => so.Id == (int)SearchFields.DateOfBirth).IsSelected = false;

            // Search
            patientSearchViewContext.Search.Execute(patientSearchViewContext.InteractionContext);
            Func<bool> isSave = () => patientSearchViewContext.Search.CanExecute(null);
            isSave.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(patientSearchViewContext.SearchResults.Count == 5); // NOTE: NO DUPLICATES SHOULD BE PRESENT
            Assert.IsTrue(patientSearchViewContext.TermsToHighlight.Count == 2);
            Assert.IsNull(patientSearchViewContext.SelectedSearchResult);
        }
    }
}