﻿using System;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.SubmitTransactions;
using IO.Practiceware.Presentation.Views.SubmitTransactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Presentation;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class SubmitTransactionsTests : TestBase
    {
        protected override void OnAfterTestInitialize()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ModelTestsResources.ModelTestsMetadata);
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PaperClaimsResources.X12MetadataPaperClaims);
        }

        [TestMethod]
        public void TestLoadSubmitTransactionsViewContext()
        {
            CreateFilterTestData();

            var submitTransactionsViewContext = Common.ServiceProvider.GetService<SubmitTransactionsViewContext>();
            submitTransactionsViewContext.LoadArguments = new SubmitTransactionsLoadArguments
            {
                DefaultClaimType = ClaimType.PaperClaims
            };

            // Load UI data
            submitTransactionsViewContext.Load.Execute(submitTransactionsViewContext.InteractionContext);
            Func<bool> isLoaded = () => !submitTransactionsViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(submitTransactionsViewContext.FilterViewModel);
            Assert.IsNotNull(submitTransactionsViewContext.FilterSelectionViewModel);
            Assert.AreEqual(ClaimType.PaperClaims, submitTransactionsViewContext.FilterSelectionViewModel.ClaimType);

            // check saved filters has 2 entries.  one for the 'Create new Filter' default entry, and one for the 
            // injected filter.
            Assert.AreEqual(2, submitTransactionsViewContext.FilterViewModel.SavedFilters.Count);
            var savedFilter = submitTransactionsViewContext.FilterViewModel.SavedFilters[1];
            Assert.AreEqual(ApplicationSetting.SubmitTransactions, savedFilter.Name);
            Assert.AreEqual(FilterStartDate, savedFilter.StartDate);
            Assert.AreEqual(FilterEndDate, savedFilter.EndDate);
            Assert.AreEqual((int)ClaimType.PaperClaims, savedFilter.ClaimType);
        }

        [TestMethod]
        public void TestDeleteFilter()
        {
            CreateFilterTestData();

            var submitTransactionsViewContext = Common.ServiceProvider.GetService<SubmitTransactionsViewContext>();
            submitTransactionsViewContext.LoadArguments = new SubmitTransactionsLoadArguments
            {
                DefaultClaimType = ClaimType.PaperClaims
            };

            // Load UI data
            submitTransactionsViewContext.Load.Execute(submitTransactionsViewContext.InteractionContext);
            Func<bool> isLoaded = () => !submitTransactionsViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            // check filters has 2 entries
            Assert.AreEqual(2, submitTransactionsViewContext.FilterViewModel.SavedFilters.Count);

            // select the filter
            submitTransactionsViewContext.SelectedFilter = submitTransactionsViewContext.FilterViewModel.SavedFilters[1];

            // delete filter
            submitTransactionsViewContext.DeleteFilter.Execute(submitTransactionsViewContext.InteractionContext);

            // check filters has 1 entry
            Assert.AreEqual(1, submitTransactionsViewContext.FilterViewModel.SavedFilters.Count);
        }

        [TestMethod]
        public void TestSearchResults()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(PaperClaimsResources.SetUpTest1);

            var submitTransactionsViewContext = Common.ServiceProvider.GetService<SubmitTransactionsViewContext>();
            submitTransactionsViewContext.LoadArguments = new SubmitTransactionsLoadArguments
            {
                DefaultClaimType = ClaimType.PaperClaims
            };

            // Load UI data
            submitTransactionsViewContext.Load.Execute(submitTransactionsViewContext.InteractionContext);
            Func<bool> isLoaded = () => !submitTransactionsViewContext.Load.CastTo<AsyncCommand>().IsBusy;
            isLoaded.Wait(TimeSpan.FromSeconds(10));


            // filter the data. set to search for cm-1500.  expect to get back 1 result
            submitTransactionsViewContext.FilterSelectionViewModel.ClaimType = ClaimType.PaperClaims;
            submitTransactionsViewContext.FilterSelectionViewModel.PaperClaimFormId = (int)ClaimFormTypeId.Cms1500;
            submitTransactionsViewContext.FilterSelectionViewModel.StartDate = new DateTime(2009, 1, 1);
            submitTransactionsViewContext.FilterSelectionViewModel.EndDate = new DateTime(2013, 1, 1);
            submitTransactionsViewContext.Search.Execute(submitTransactionsViewContext.InteractionContext);
            Func<bool> isSearched = () => !submitTransactionsViewContext.Search.CastTo<AsyncCommand>().IsBusy;
            isSearched.Wait(TimeSpan.FromSeconds(10));
            Assert.AreEqual(1, submitTransactionsViewContext.SearchResults.Count);
            var result = submitTransactionsViewContext.SearchResults.First();
            Assert.AreEqual(4.50m, result.AmountBilled);
            Assert.AreEqual("MEDICARE", result.LastName);
            Assert.AreEqual("MEDICAID", result.FirstName);


            // filter the data. set to search for ub-04. expect to get back no results
            submitTransactionsViewContext.FilterSelectionViewModel.ClaimType = ClaimType.EClaims;
            submitTransactionsViewContext.FilterSelectionViewModel.SelectedElectronicClaims = new System.Collections.ObjectModel.ObservableCollection<ClaimSelectionViewModel> { submitTransactionsViewContext.FilterViewModel.ElectronicClaimForms[0] };
            submitTransactionsViewContext.FilterSelectionViewModel.StartDate = new DateTime(2009, 1, 1);
            submitTransactionsViewContext.FilterSelectionViewModel.EndDate = new DateTime(2013, 1, 1);


            // filter the data
            submitTransactionsViewContext.Search.Execute(submitTransactionsViewContext.InteractionContext);
            isSearched.Wait(TimeSpan.FromSeconds(10));

            Assert.AreEqual(0, submitTransactionsViewContext.SearchResults.Count);
        }


        private static readonly DateTime FilterStartDate = new DateTime(2011, 1, 1);
        private static readonly DateTime FilterEndDate = new DateTime(2014, 1, 1);

        private void CreateFilterTestData()
        {
            var modelSetting = new ApplicationSetting();
            var filter = new SubmitTransactionsApplicationSetting();
            filter.ClaimType = (int)ClaimType.PaperClaims;
            filter.PaperClaimFormId = (int)ClaimFormTypeId.Cms1500;
            filter.StartDate = FilterStartDate;
            filter.EndDate = FilterEndDate;
            filter.Name = "SubmitTransactions";

            modelSetting.UserId = UserContext.Current.UserDetails.Id;
            modelSetting.Name = ApplicationSetting.SubmitTransactions;
            modelSetting.Value = filter.ToXml();
            
            PracticeRepository.Save(modelSetting);


        }
    }
}
