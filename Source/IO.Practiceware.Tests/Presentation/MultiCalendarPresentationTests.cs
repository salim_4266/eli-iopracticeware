﻿using System;
using System.Linq;
using System.Transactions;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.Views.MultiCalendar;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    [TestRunMode(ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresTestClassLock)]
    public class MultiCalendarPresentationTests : TestBase
    {
        private static MultiCalendarViewContext _multiCalendarViewContext;

        #region PARENT

        [TestMethod]
        public void TestInitMultiCalendarViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            Assert.IsNotNull(multiCalendarViewContext.DocsViewContext);
            Assert.IsNotNull(multiCalendarViewContext.DaysViewContext);


        }

        [TestMethod]
        public void TestLoadMultiCalendarViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(multiCalendarViewContext.DocsViewContext);
            Assert.IsNotNull(multiCalendarViewContext.DaysViewContext);


        }

        #endregion

        #region DOCS

        [TestMethod]
        public void TestInitMultiCalendarDocsViewContext()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            var docsViewContext = multiCalendarViewContext.DocsViewContext;

            Assert.IsNotNull(docsViewContext.DefaultSpanLocation);
            Assert.IsNotNull(docsViewContext.SpanSize);
            Assert.IsNotNull(docsViewContext.AreaStart);
            Assert.IsNotNull(docsViewContext.AreaEnd);

            Assert.IsTrue(docsViewContext.ListOfResource.Count == 0);


        }

        [TestMethod]
        public void TestLoadMultiCalendarDocsViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            var docsViewContext = multiCalendarViewContext.DocsViewContext;

            Assert.IsTrue(docsViewContext.ListOfResource.Count != 0);


        }

        [TestMethod]
        public void TestPopulateBlocksMultiCalendarDocsViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            Assert.IsTrue(isLoad.Wait(TimeSpan.FromSeconds(10)));

            // Select options in UI
            var docsViewContext = multiCalendarViewContext.DocsViewContext;
            docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).IsSelected = true;
            docsViewContext.SelectedDateFilter = new DateTime(2012, 3, 5);

            docsViewContext.RefreshView();

            WaitUntilTaskQueueNotBusyAndDisable(multiCalendarViewContext);

            int blockCount = docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Count(b => b.Id.HasValue);

            Assert.IsTrue(blockCount == 7);
        }

        [TestMethod]
        public void TestDeletedScheduleBlockUnavailableCategoryMultiCalendarDocsViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            PermissionId.InsertDeleteCategoriesInSchedule.AddPermissionToCurrentUser(PracticeRepository);

            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Select options in UI
            var docsViewContext = multiCalendarViewContext.DocsViewContext;
            docsViewContext.SelectedDateFilter = new DateTime(2012, 3, 5);
            docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).IsSelected = true;

            docsViewContext.RefreshView();
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Create new schedule block - Unavailable
            var lastBlockForCreating = docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Last();
            var userScheduleBlock = new UserScheduleBlock();
            userScheduleBlock.StartDateTime = lastBlockForCreating.StartDateTime.AddMinutes(Common.ServiceProvider.GetService<ISchedulingConfigurationViewService>().GetSchedulingConfiguration().SpanSize.Minutes);
            userScheduleBlock.ServiceLocationId = lastBlockForCreating.Location.Id;
            userScheduleBlock.IsUnavailable = true;
            userScheduleBlock.UserId = lastBlockForCreating.Resource.Id;
            PracticeRepository.Save(userScheduleBlock);

            Assert.IsTrue(userScheduleBlock.Id > 0);

            docsViewContext.RefreshView();
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Make note
            var lastBlock = docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Last(b => b.Id.HasValue);
            var lastBlockCategoryCount = lastBlock.Categories.Count;
            var lastBlockCategory = lastBlock.Categories.First();

            Assert.IsTrue(lastBlock.Categories.Count == 1);
            Assert.IsTrue(lastBlockCategory is UnavailableBlockCategoryViewModel);

            // Delete block category
            docsViewContext.AppointmentEditor.DeleteScheduleBlockCategory.Execute(lastBlockCategory);

            WaitUntilTaskQueueNotBusyAndDisable(multiCalendarViewContext);

            Assert.AreEqual(lastBlockCategoryCount - 1, lastBlock.Categories.Count);


        }

        [TestMethod]
        public void TestDeletedScheduleBlockCategoryMultiCalendarDocsViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            PermissionId.InsertDeleteCategoriesInSchedule.AddPermissionToCurrentUser(PracticeRepository);

            var scheduleBlockOne = PracticeRepository.ScheduleBlocks.First();
            scheduleBlockOne.IsUnavailable = false;
            PracticeRepository.Save(scheduleBlockOne);
            var shortAppointment = PracticeRepository.AppointmentCategories.First(c => c.Name == "Short Appointment");
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Select options in UI
            var docsViewContext = multiCalendarViewContext.DocsViewContext;
            docsViewContext.SelectedDateFilter = new DateTime(2012, 3, 5);
            docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).IsSelected = true;

            docsViewContext.RefreshView();
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Make note
            var originalNumberOfBlocks = docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Count(b => b.Id.HasValue);
            var blockOne = docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.First(b => b.Id == scheduleBlockOne.Id);
            var originalNumberOfBlockOneCategories = blockOne.Categories.Count;
            var blockOneShortCategory = blockOne.Categories.First(c => c.Category.Id == shortAppointment.Id);

            Assert.IsTrue(originalNumberOfBlocks == 7);
            Assert.IsTrue(originalNumberOfBlockOneCategories == 3);

            // Delete block
            docsViewContext.AppointmentEditor.DeleteScheduleBlockCategory.Execute(blockOneShortCategory);
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            WaitUntilTaskQueueNotBusyAndDisable(multiCalendarViewContext);

            Assert.IsTrue(docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Count(b => b.Id.HasValue) == originalNumberOfBlocks);
            Assert.IsTrue(docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.Count(b => b.StartDateTime == blockOne.StartDateTime) == 1);
            Assert.IsTrue(docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.First(b => b.StartDateTime == blockOne.StartDateTime).Categories.Count == originalNumberOfBlockOneCategories - 1);
            Assert.IsTrue(docsViewContext.ListOfResource.First(r => r.Resource.Id == 1).Blocks.First(b => b.StartDateTime == blockOne.StartDateTime).Categories.All(c => c.Id != blockOneShortCategory.Id));


        }

        #endregion

        #region DAYS

        [TestMethod]
        public void TestInitMultiCalendarDaysViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();

            var daysViewContext = multiCalendarViewContext.DaysViewContext;

            Assert.IsNotNull(daysViewContext.DefaultSpanLocation);
            Assert.IsNotNull(daysViewContext.SpanSize);
            Assert.IsNotNull(daysViewContext.AreaStart);
            Assert.IsNotNull(daysViewContext.AreaEnd);

            Assert.IsTrue(daysViewContext.Dates.Count == 0);

        }

        [TestMethod]
        public void TestLoadMultiCalendarDaysViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            var daysViewContext = multiCalendarViewContext.DaysViewContext;

            Assert.IsTrue(daysViewContext.ListOfResource.Count != 0);



        }

        [TestMethod]
        public void TestPopulateBlocksMultiCalendarDaysViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Select options in UI
            var daysViewContext = multiCalendarViewContext.DaysViewContext;
            daysViewContext.GeneratedDates.Clear();
            daysViewContext.GeneratedDates.Add(new DateTime(2012, 3, 5));
            daysViewContext.SelectedResource = daysViewContext.ListOfResource.First(p => p.Id == 1);

            // Load blocks
            daysViewContext.RefreshView();
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            Assert.IsTrue(daysViewContext.Dates.Count == 1);
            Assert.IsTrue(daysViewContext.Dates.First().Blocks.Count(b => b.Id.HasValue) == 7);


        }

        [TestMethod]
        public void TestDeletedScheduleBlockUnavailableCategoryMultiCalendarDaysViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            PermissionId.InsertDeleteCategoriesInSchedule.AddPermissionToCurrentUser(PracticeRepository);
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Select options in UI
            var daysViewContext = multiCalendarViewContext.DaysViewContext;
            daysViewContext.GeneratedDates.Clear();
            daysViewContext.GeneratedDates.Add(new DateTime(2012, 3, 5));
            daysViewContext.SelectedResource = daysViewContext.ListOfResource.First(p => p.Id == 1);

            // Load blocks - One
            daysViewContext.RefreshView();
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Create new schedule block - Unavailable
            var lastBlockForCreating = daysViewContext.Dates.First().Blocks.Last(b => b.Id.HasValue);
            var userScheduleBlock = new UserScheduleBlock();
            userScheduleBlock.StartDateTime = lastBlockForCreating.StartDateTime.AddMinutes(Common.ServiceProvider.GetService<ISchedulingConfigurationViewService>().GetSchedulingConfiguration().SpanSize.Minutes);
            userScheduleBlock.ServiceLocationId = lastBlockForCreating.Location.Id;
            userScheduleBlock.IsUnavailable = true;
            userScheduleBlock.UserId = lastBlockForCreating.Resource.Id;
            PracticeRepository.Save(userScheduleBlock);

            // Load blocks - Two
            daysViewContext.RefreshView();
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Make note
            var lastBlock = daysViewContext.Dates.First().Blocks.Last(b => b.Id.HasValue);
            var lastBlockCategoryCount = lastBlock.Categories.Count;
            var lastBlockCategory = lastBlock.Categories.First();

            Assert.IsTrue(lastBlock.Categories.Count == 1);
            Assert.IsTrue(lastBlockCategory is UnavailableBlockCategoryViewModel);

            // Delete block category
            daysViewContext.AppointmentEditor.DeleteScheduleBlockCategory.Execute(lastBlockCategory);
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            Assert.IsTrue(lastBlock.Categories.Count == lastBlockCategoryCount - 1);


        }

        [TestMethod]
        public void TestDeletedScheduleBlockCategoryMultiCalendarDaysViewContext()
        {
            // Add sample data to database
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(MultiCalendar.MultiCalendarMetaData);
            PermissionId.InsertDeleteCategoriesInSchedule.AddPermissionToCurrentUser(PracticeRepository);
            var scheduleBlockOne = PracticeRepository.ScheduleBlocks.First();
            scheduleBlockOne.IsUnavailable = false;
            PracticeRepository.Save(scheduleBlockOne);
            var shortAppointment = PracticeRepository.AppointmentCategories.First(c => c.Name == "Short Appointment");
            var multiCalendarViewContext = CreateMultiCalendarViewContext();


            // Load UI data
            multiCalendarViewContext.Load.Execute(multiCalendarViewContext.InteractionContext);
            Func<bool> isLoad = () => multiCalendarViewContext.Load.CanExecute(null);
            isLoad.Wait(TimeSpan.FromSeconds(10));

            // Select options in UI
            var daysViewContext = multiCalendarViewContext.DaysViewContext;
            daysViewContext.GeneratedDates.Clear();
            daysViewContext.GeneratedDates.Add(new DateTime(2012, 3, 5));
            daysViewContext.SelectedResource = daysViewContext.ListOfResource.First(p => p.Id == 1);

            // Load blocks - One
            daysViewContext.RefreshView();
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            // Make note
            var originalNumberOfBlocks = daysViewContext.Dates.First().Blocks.Count(b => b.Id.HasValue);
            var blockOne = daysViewContext.Dates.First().Blocks.First(b => b.Id == scheduleBlockOne.Id);
            var originalNumberOfBlockOneCategories = blockOne.Categories.Count;
            var blockOneShortCategory = blockOne.Categories.First(c => c.Category.Id == shortAppointment.Id);

            Assert.IsTrue(originalNumberOfBlocks == 7);
            Assert.IsTrue(originalNumberOfBlockOneCategories == 3);

            // Delete block
            daysViewContext.AppointmentEditor.DeleteScheduleBlockCategory.Execute(blockOneShortCategory);
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));

            WaitUntilTaskQueueNotBusyAndDisable(multiCalendarViewContext);

            Assert.IsTrue(daysViewContext.Dates.First().Blocks.Count(b => b.Id.HasValue) == originalNumberOfBlocks);
            Assert.IsTrue(daysViewContext.Dates.First().Blocks.Count(b => b.StartDateTime == blockOne.StartDateTime) == 1);
            Assert.IsTrue(daysViewContext.Dates.First().Blocks.First(b => b.StartDateTime == blockOne.StartDateTime).Categories.Count == originalNumberOfBlockOneCategories - 1);
            Assert.IsTrue(daysViewContext.Dates.First().Blocks.First(b => b.StartDateTime == blockOne.StartDateTime).Categories.All(c => c.Id != blockOneShortCategory.Id));


        }


        #endregion

        protected override void OnBeforeTestCleanup()
        {
            WaitUntilTaskQueueNotBusyAndDisable(_multiCalendarViewContext);
            base.OnBeforeTestCleanup();
        }

        private static void WaitUntilTaskQueueNotBusyAndDisable(MultiCalendarViewContext multiCalendarViewContext)
        {
            multiCalendarViewContext.TaskQueue.IsDisabled = true;
            multiCalendarViewContext.PostOpTaskQueue.IsDisabled = true;
            Func<bool> isTaskQueueReady = () => !multiCalendarViewContext.TaskQueue.IsBusy && !multiCalendarViewContext.PostOpTaskQueue.IsBusy;
            Assert.IsTrue(isTaskQueueReady.Wait(TimeSpan.FromSeconds(20)));
        }

        private static MultiCalendarViewContext CreateMultiCalendarViewContext()
        {
            var multiCalendarViewContext = Common.ServiceProvider.GetService<MultiCalendarViewContext>();
            multiCalendarViewContext.TaskQueue.Transaction = Transaction.Current;
            multiCalendarViewContext.PostOpTaskQueue.Transaction = Transaction.Current;
            _multiCalendarViewContext = multiCalendarViewContext;
            return multiCalendarViewContext;
        }
    }
}