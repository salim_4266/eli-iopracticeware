﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Notes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class ManageNotesViewTests : X12MetadataTestsBase
    {

        [TestMethod]
        public void TestManageNoteViewContextLoadAndSaveCommentWithAlert()
        {
            var dbPatientFinancialsComment = new PatientFinancialComment
            {
                IsIncludedOnStatement = true,
                PatientId = 1,
                IsArchived = false,
                Value = "Test financial Comment"
            };
            PracticeRepository.Save(dbPatientFinancialsComment);

            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, ManageNotesViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

            viewContext.NoteLoadArguments = new NoteLoadArguments();
            viewContext.NoteLoadArguments.NoteType = NoteType.PatientFinancialComment;
            viewContext.NoteLoadArguments.EntityId = 1;

            viewContext.Load.Execute();
            Assert.AreEqual(viewContext.Notes.Count, 1);

            var patientFinancialsComment = viewContext.Notes[0];
            Assert.IsNotNull(dbPatientFinancialsComment);
            Assert.AreEqual(patientFinancialsComment.Value, dbPatientFinancialsComment.Value);
            Assert.AreEqual(patientFinancialsComment.IsArchived, dbPatientFinancialsComment.IsArchived);
            Assert.AreEqual(patientFinancialsComment.IsIncludedOnStatement, dbPatientFinancialsComment.IsIncludedOnStatement);

            //Edit patient financials comments.
            patientFinancialsComment.Id = dbPatientFinancialsComment.Id;
            patientFinancialsComment.Value = "Test financial Comment";
            patientFinancialsComment.AlertableScreens = new System.Collections.ObjectModel.ObservableCollection<NamedViewModel>();
            patientFinancialsComment.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 10));

            //Saving comment and sending alert.
            viewContext.SaveComments();

            dbPatientFinancialsComment = PracticeRepository.PatientFinancialComments.WithId(dbPatientFinancialsComment.Id);
            Assert.IsNotNull(patientFinancialsComment);
            Assert.AreEqual(patientFinancialsComment.Value, dbPatientFinancialsComment.Value);
            Assert.AreEqual(patientFinancialsComment.IsArchived, dbPatientFinancialsComment.IsArchived);
            Assert.AreEqual(patientFinancialsComment.IsIncludedOnStatement, dbPatientFinancialsComment.IsIncludedOnStatement);

            //Adding 2 more notes to save.
            viewContext.Note.IsIncludedOnStatement = false;
            viewContext.Note.IsArchived = false;
            viewContext.Note.Value = "Test financial Comment2";
            viewContext.Note.AlertableScreens = new System.Collections.ObjectModel.ObservableCollection<NamedViewModel>();
            viewContext.Note.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 9));
            viewContext.AddNote.Execute();

            viewContext.Note.IsIncludedOnStatement = false;
            viewContext.Note.IsArchived = false;
            viewContext.Note.Value = "Test financial Comment3";
            viewContext.Note.AlertableScreens = new System.Collections.ObjectModel.ObservableCollection<NamedViewModel>();
            viewContext.Note.AlertableScreens.Add(viewContext.AlertableScreens.FirstOrDefault(x => x.Id == 10));
            viewContext.AddNote.Execute();

            viewContext.SaveComments();

            var patientFinancialCommentsCount = PracticeRepository.PatientFinancialComments.Count(x => x.PatientId == 1);
            Assert.AreEqual(patientFinancialCommentsCount, viewContext.Notes.Count);

            viewContext.Load.Execute();
            foreach (var note in viewContext.Notes)
            {
                var screensCount = PracticeRepository.Alerts.Where(x => x.Id == note.AlertId).Select(x => x.Screens).Count();
                Assert.AreEqual(screensCount, note.AlertableScreens.Count);
            }

            //Saving standard comment
            viewContext.Note.Name = "Test Comment";
            viewContext.Note.Value = "Test Comment value";

            viewContext.SaveStandardTemplate.Execute(viewContext.Note);

            var standardNote = PracticeRepository.NoteTemplates.FirstOrDefault(x => x.Name == viewContext.Note.Name);
            Assert.IsNotNull(standardNote);
            Assert.AreEqual(standardNote.Value, viewContext.Note.Value);
            Assert.AreEqual(standardNote.Name, viewContext.Note.Name);

            if (PermissionId.DeleteStandardComments.EnsurePermission(false))
            {
                viewContext.DeleteNoteTemplate.Execute(standardNote.Id);
                Assert.AreEqual(viewContext.StandardNotes.Count, 0);
            }

        }
    }


}
