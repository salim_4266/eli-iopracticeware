﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.AppointmentSearch;
using IO.Practiceware.Presentation.Views.AppointmentSearch;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class AppointmentSearchPresentationTests : TestBase
    {

        [TestMethod]
        public void TestInitAppointmentSearchViewContext()
        {
            var appointmentSearchViewContext = Common.ServiceProvider.GetService<AppointmentSearchViewContext>();
            Assert.IsNull(appointmentSearchViewContext.AppointmentHistoryHelper);
            Assert.IsNull(appointmentSearchViewContext.PatientInfo);
            Assert.IsNull(appointmentSearchViewContext.PatientComments);
            Assert.IsNull(appointmentSearchViewContext.PatientReferrals);
            Assert.IsNull(appointmentSearchViewContext.SearchFilter);
            Assert.IsNotNull(appointmentSearchViewContext.SearchResults);
            Assert.IsNull(appointmentSearchViewContext.SearchFilter);
            Assert.IsTrue(appointmentSearchViewContext.TimeSelectionOptions.Count() == 7);
        }

        [TestMethod]
        public void TestLoadAppointmentSearchViewContext()
        {
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);

            var appointmentSearchViewContext = Common.ServiceProvider.GetService<AppointmentSearchViewContext>();
            appointmentSearchViewContext.DisableLoadOfPatientPostOp = true;
            appointmentSearchViewContext.LoadArguments = new AppointmentSearchLoadArguments { PatientId = PracticeRepository.Patients.First().Id };

            // Load UI data
            appointmentSearchViewContext.Load.Execute(appointmentSearchViewContext.InteractionContext);
            Func<bool> isLoaded = () => appointmentSearchViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsNotNull(appointmentSearchViewContext.PatientInfo);
            Assert.IsNotNull(appointmentSearchViewContext.AppointmentHistoryHelper);
            Assert.IsNotNull(appointmentSearchViewContext.PatientComments);
            Assert.IsNotNull(appointmentSearchViewContext.PatientReferrals);
            Assert.IsNotNull(appointmentSearchViewContext.SearchFilter);

            Assert.IsNotNull(appointmentSearchViewContext.SelectedDateTimeFilter);
            Assert.IsNotNull(appointmentSearchViewContext.SelectedDateTimeFilter.SelectedDateOption);
            Assert.IsNotNull(appointmentSearchViewContext.SelectedDateTimeFilter.SelectedTimeOption);
            Assert.IsTrue(appointmentSearchViewContext.SelectedDateTimeFilter.SelectedTimeOption.TimeSelectionType == TimeSelectionType.AllDay);
        }

        [TestMethod]
        public void TestFindAppointmentSearchViewContext()
        {
            // Create an AppointmentCategory
            var appointmentCategory = Common.CreateAppointmentCategory();
            appointmentCategory.AppointmentTypes.Add(Common.PracticeRepository.AppointmentTypes.OrderByDescending(at => at.Id).First());
            Common.PracticeRepository.Save(appointmentCategory);
            Assert.IsTrue(appointmentCategory.Id > 0);

            var appointmentType = Common.PracticeRepository.AppointmentTypes.WithId(appointmentCategory.AppointmentTypes.Last().Id);
            appointmentType.AppointmentCategory = Common.PracticeRepository.AppointmentCategories.WithId(appointmentCategory.Id);
            Common.PracticeRepository.Save(appointmentType);
            Assert.IsTrue(appointmentType.Id > 0);

            // Create an appointment
            var appointment = Common.CreateAppointment();
            //appointment.AppointmentType = appointmentType;
            Common.PracticeRepository.Save(appointment);
            Assert.IsTrue(appointment.Id > 0);

            var refetchedAppointmentType = Common.PracticeRepository.AppointmentTypes.WithId(appointmentType.Id);
            refetchedAppointmentType.Appointments.Add(appointment);
            Common.PracticeRepository.Save(refetchedAppointmentType);
            Assert.IsTrue(refetchedAppointmentType.Id > 0);

            // Create a UserScheduleBlock
            var userScheduleBlock = Common.CreateUserScheduleBlock();
            Common.PracticeRepository.Save(userScheduleBlock);
            Assert.IsTrue(userScheduleBlock.Id > 0);

            // Create a ScheduleBlockAppointmentCategory for the ScheduleBlock
            Common.AddScheduleBlockAppointmentCategory(userScheduleBlock);
            Common.PracticeRepository.Save(userScheduleBlock);
            Assert.IsTrue(userScheduleBlock.ScheduleBlockAppointmentCategories.Count == 1);

            // Create a ScheduleBlockAppointmentCategory for the ScheduleBlock that will have an Appointment
            Common.AddScheduleBlockAppointmentCategory(userScheduleBlock);
            Common.PracticeRepository.Save(userScheduleBlock);
            Assert.IsTrue(userScheduleBlock.ScheduleBlockAppointmentCategories.Count == 2);

            // Add an Appointment to the second ScheduleBlockAppointmentCategory
            // Create an appointment
            var secondAppointment = Common.CreateAppointment();
            secondAppointment.User.Pid = "9877";
            secondAppointment.User.UserName = "9877";
            Common.PracticeRepository.Save(secondAppointment);
            Assert.IsTrue(secondAppointment.Id > 0);

            var category = userScheduleBlock.ScheduleBlockAppointmentCategories.ElementAt(1);
            category.AppointmentId = secondAppointment.Id;
            Common.PracticeRepository.Save(category);

            var refetchedAppointment = PracticeRepository.Appointments.WithId(appointment.Id);
            refetchedAppointment.ScheduleBlockAppointmentCategories.Add(category);
            Common.PracticeRepository.Save(refetchedAppointment);
            Assert.IsTrue(refetchedAppointment.ScheduleBlockAppointmentCategories.Count == 1);

            var appointmentSearchViewContext = Common.ServiceProvider.GetService<AppointmentSearchViewContext>();
            appointmentSearchViewContext.DisableLoadOfPatientPostOp = true;
            appointmentSearchViewContext.LoadArguments = new AppointmentSearchLoadArguments { PatientId = PracticeRepository.Patients.First().Id };

            // Load UI data
            appointmentSearchViewContext.Load.Execute(appointmentSearchViewContext.InteractionContext);
            Func<bool> isLoaded = () => appointmentSearchViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(appointmentSearchViewContext.SearchFilter.AppointmentTypes.Count > 0);
            Assert.IsTrue(appointmentSearchViewContext.SearchFilter.Locations.Count > 0);
            Assert.IsTrue(appointmentSearchViewContext.SearchFilter.Resources.Count > 0);

            var selectedAppointmentType = appointmentSearchViewContext.SearchFilter.AppointmentTypes.First(at => at.Id == refetchedAppointment.AppointmentTypeId);
            appointmentSearchViewContext.SearchFilter.SelectedAppointmentType = selectedAppointmentType;
            appointmentSearchViewContext.SearchFilter.SelectedLocations = appointmentSearchViewContext.SearchFilter.Locations;
            appointmentSearchViewContext.SearchFilter.SelectedResources = appointmentSearchViewContext.SearchFilter.Resources;

            Assert.IsTrue(appointmentSearchViewContext.SelectedDateTimeFilter.SelectedTimeOption.TimeSelectionType == TimeSelectionType.AllDay);

            var beginDate = new DateTime(2010, 1, 1, 10, 15, 0);
            appointmentSearchViewContext.SelectedDateTimeFilter.SelectedDateOption = new DateSelectionViewModel { BeginDate = beginDate };
            appointmentSearchViewContext.SelectedDateTimeFilter.SelectedDateOption.SelectedDays.Add(beginDate.DayOfWeek);

            // Perform search
            appointmentSearchViewContext.Find.Execute(appointmentSearchViewContext.InteractionContext);
            Func<bool> isFound = () => appointmentSearchViewContext.SearchResults != null && appointmentSearchViewContext.SearchResults.Count > 0;

            Assert.IsTrue(isFound.Wait(TimeSpan.FromSeconds(15)));
        }
    }
}
