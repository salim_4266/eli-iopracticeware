﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class PatientInsuranceAuthorizationTests : TestBase
    {

        [TestMethod]
        public void TestLoadPatientInsuranceAuthorizationViewContext()
        {
            var patientInsuranceAuthorization = CreateAndSavePatientInsuranceAuthorization();            

            var patientInsuranceAuthorizationsViewContext = Common.ServiceProvider.GetService<PatientInsuranceAuthorizationsViewContext>();
            patientInsuranceAuthorizationsViewContext.LoadArgument = new PatientInsuranceAuthorizationsLoadArguments();
            patientInsuranceAuthorizationsViewContext.LoadArgument.PatientInsuranceAuthorizationId = patientInsuranceAuthorization.Id;

            // Load UI data
            patientInsuranceAuthorizationsViewContext.Load.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInsuranceAuthorizationsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            Assert.IsNotNull(patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization);
            Assert.IsNotNull(patientInsuranceAuthorizationsViewContext.PatientAppointmentInfo);           
        }

        [TestMethod]
        public void TestViewContextIsInAcceptableSaveState()
        {
            var patientInsuranceAuthorization = CreateAndSavePatientInsuranceAuthorization();

            var patientInsuranceAuthorizationsViewContext = Common.ServiceProvider.GetService<PatientInsuranceAuthorizationsViewContext>();
            patientInsuranceAuthorizationsViewContext.LoadArgument = new PatientInsuranceAuthorizationsLoadArguments();

            patientInsuranceAuthorizationsViewContext.LoadArgument.PatientInsuranceAuthorizationId = patientInsuranceAuthorization.Id;

            // Load UI data
            patientInsuranceAuthorizationsViewContext.Load.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInsuranceAuthorizationsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            Assert.IsFalse(patientInsuranceAuthorizationsViewContext.ButtonsCanExecute());

            // make a change
            patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization.Code = "test";

            // should now be true
            Assert.IsTrue(patientInsuranceAuthorizationsViewContext.ButtonsCanExecute());
        }

        [TestMethod]
        public void TestViewContextSave()
        {
            var patientInsuranceAuthorization = CreateAndSavePatientInsuranceAuthorization();

            var patientInsuranceAuthorizationsViewContext = Common.ServiceProvider.GetService<PatientInsuranceAuthorizationsViewContext>();
            patientInsuranceAuthorizationsViewContext.LoadArgument = new PatientInsuranceAuthorizationsLoadArguments();
            patientInsuranceAuthorizationsViewContext.LoadArgument.PatientInsuranceAuthorizationId = patientInsuranceAuthorization.Id;

            // Load UI data
            patientInsuranceAuthorizationsViewContext.Load.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInsuranceAuthorizationsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            const string authCode = "TESTCODE123";
            const string authComments = "TESTCOMMENT123";
            patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization.Code = authCode;
            patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization.Comments = authComments;

            patientInsuranceAuthorizationsViewContext.Save.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);
            Func<bool> isSaved = () => patientInsuranceAuthorizationsViewContext.Save.CanExecute(null);
            isSaved.Wait(TimeSpan.FromSeconds(15));

            var persistedPatientInsuranceAuthorization = Common.PracticeRepository.PatientInsuranceAuthorizations.First(x => x.Id == patientInsuranceAuthorization.Id);
            Assert.AreEqual(authCode, persistedPatientInsuranceAuthorization.AuthorizationCode);            
        }

        [TestMethod]
        public void TestViewContextClear()
        {
            var patientInsuranceAuthorization = CreateAndSavePatientInsuranceAuthorization();

            var patientInsuranceAuthorizationsViewContext = Common.ServiceProvider.GetService<PatientInsuranceAuthorizationsViewContext>();
            patientInsuranceAuthorizationsViewContext.LoadArgument = new PatientInsuranceAuthorizationsLoadArguments();
            patientInsuranceAuthorizationsViewContext.LoadArgument.PatientInsuranceAuthorizationId = patientInsuranceAuthorization.Id;

            // Load UI data
            patientInsuranceAuthorizationsViewContext.Load.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);
            Func<bool> isLoaded = () => patientInsuranceAuthorizationsViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(15));

            patientInsuranceAuthorizationsViewContext.Clear.Execute(patientInsuranceAuthorizationsViewContext.InteractionContext);

            Assert.IsTrue(patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization.Code.IsNullOrEmpty());
            Assert.IsTrue(patientInsuranceAuthorizationsViewContext.PatientInsuranceAuthorization.Comments.IsNullOrEmpty());
        }

        private static PatientInsuranceAuthorization CreateAndSavePatientInsuranceAuthorization()
        {
            var patientInsuranceAuthorization = Common.CreatePatientInsuranceAuthorization();
            Common.PracticeRepository.Save(patientInsuranceAuthorization);
            return patientInsuranceAuthorization;
        }
    }
}
