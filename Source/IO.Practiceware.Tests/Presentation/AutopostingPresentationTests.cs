﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.Views.Financials.Autoposting;
using IO.Practiceware.Presentation.ViewServices.Financials.Autoposting;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using IO.Practiceware.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OopFactory.X12.Parsing.Model;
using Soaf;
using Soaf.Collections;
using Soaf.Linq;
using Soaf.Presentation;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class AutopostingPresentationTests : X12MetadataTestsBase
    {
        private static readonly CultureInfo USCulture = CultureInfo.GetCultureInfo("en-US");
        private string _testSourcePath;

        protected override void OnAfterTestInitialize()
        {
            base.OnAfterTestInitialize();

            _testSourcePath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(@"%TEMP%\IOP\AutopostingTests\" + ApplicationContext.Current.ClientKey));
            FileSystem.EnsureDirectory(_testSourcePath);
        }

        protected override void OnBeforeTestCleanup()
        {
            base.OnBeforeTestCleanup();

            FileSystem.TryDeleteDirectory(_testSourcePath);
        }

        [TestMethod]
        public void TestPostingUnbilledAndBilledInvoice()
        {
            const int invalidPatientId = int.MaxValue;
            var testData = CreateTestData();

            // ReSharper disable ConvertToLambdaExpression
            var x12Message = CreateX12Message(DateTime.UtcNow, "check1", testData.SelectedInsurer.PayerCode, 100m, lx =>
            {
                AddPatient(lx, testData.NonBilledInvoice.Encounter.Patient.Id, RemittanceMessage.KnownClaimStatusCode.ProcessedPrimary, clp =>
                {
                    AddServicePayment(clp,
                        testData.Over5CharacterCodeBillingService.Id,
                        testData.NonBilledInvoice.Encounter.StartDateTime,
                        testData.Over5CharacterCodeBillingService.EncounterService.Code,
                        testData.Over5CharacterCodeBillingService.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Add $12 payment
                            svc.SetElement(3, (12m).ToString(USCulture));

                            // Adjustment Type: Contractual adjustment
                            AddPayment(svc, ClaimAdjustmentGroupCode.ContractualObligation, "45", 25m);

                            // Financial Information Type: Pt Resp
                            AddPayment(svc, ClaimAdjustmentGroupCode.PatientResponsibility, "96", 5m);
                        });

                    AddServicePayment(clp,
                        testData.BillingServiceWithModifiers.Id,
                        testData.NonBilledInvoice.Encounter.StartDateTime,
                        testData.BillingServiceWithModifiers.EncounterService.Code,
                        testData.BillingServiceWithModifiers.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Add $20 payment
                            svc.SetElement(3, (20m).ToString(USCulture));
                        });

                    AddServicePayment(clp,
                        testData.BillingService.Id,
                        testData.NonBilledInvoice.Encounter.StartDateTime,
                        testData.BillingService.EncounterService.Code,
                        testData.BillingService.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Erase date. This will create an error and block payments
                            svc.Segments.First(s => s.SegmentId == "DTM").SetElement(2, string.Empty);

                            // Add $21 payment that will not be posted
                            svc.SetElement(3, (21m).ToString(USCulture));
                        });
                });

                // Enter wrong patient id -> it should not post
                AddPatient(lx, invalidPatientId, RemittanceMessage.KnownClaimStatusCode.Denied, clp =>
                {
                    AddServicePayment(clp,
                        testData.BillingService.Id,
                        testData.NonBilledInvoice.Encounter.StartDateTime,
                        testData.BillingService.EncounterService.Code,
                        testData.BillingService.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Add $23 payment that will not be posted
                            svc.SetElement(3, (23m).ToString(USCulture));
                        });
                });

                // Add payment against second invoice
                AddPatient(lx, testData.SecondNonBilledInvoice.Encounter.PatientId, RemittanceMessage.KnownClaimStatusCode.ProcessedSecondary, clp =>
                {
                    AddServicePayment(clp,
                        testData.SecondBillingService.Id,
                        testData.SecondNonBilledInvoice.Encounter.StartDateTime,
                        testData.SecondBillingService.EncounterService.Code,
                        testData.SecondBillingService.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Add $24 payment that will be posted
                            svc.SetElement(3, (24m).ToString(USCulture));
                        });
                });
            });
            // ReSharper restore ConvertToLambdaExpression

            var viewContext = LoadAutopostingViewContext();

            // Post it
            const string postedFileName = "TestPostingAgainstUnbilledInvoice.835";
            FileManager.Instance.CommitContents(string.Format("{0}\\{1}", viewContext.SourcePath, postedFileName), x12Message);
            viewContext.Upload.ExecuteAndWait();
            Assert.IsTrue(viewContext.Remittances.Any(r => r.FileName == postedFileName), "Remittance file {0} must have been uploaded", postedFileName);
            viewContext.SelectedRemittanceMessage = viewContext.Remittances.First(r => r.FileName == postedFileName);
            viewContext.LoadRemittanceAdvices.ExecuteAndWait();
            viewContext.Post.ExecuteAndWait();

            // Verify message uploaded
            var messages = GetRemittanceMessages().Where(m => m.Description == postedFileName).ToList();
            Assert.IsTrue(messages.Count == 1 && messages.First().ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processed,
                "File {0} must have been processed", postedFileName);

            // Verify expected errors
            Assert.AreEqual(
                string.Format(@"Could not bill next party for patient {0} and date of service {1}.
Could not bill next party for patient {2} and date of service {3}.
Could not bill next party for patient {4} and date of service {5}.
Can't find a matching insurance policy for patient id {6}.
Unable to parse service date. Looked at loop 2100, segment DTM, element 232 & 233, then loop 2110, segment DTM, element 472, then segment DTM, element 150 & 152. CLP is CLP*8*1
PLB02 20150212 PLB03 plb_1 PLB04 10",
                    testData.Over5CharacterCodeBillingService.Invoice.Encounter.PatientId,
                    testData.Over5CharacterCodeBillingService.Invoice.Encounter.StartDateTime.ToMMDDYYString(),
                    testData.BillingServiceWithModifiers.Invoice.Encounter.PatientId,
                    testData.BillingServiceWithModifiers.Invoice.Encounter.StartDateTime.ToMMDDYYString(),
                    testData.SecondBillingService.Invoice.Encounter.PatientId,
                    testData.SecondBillingService.Invoice.Encounter.StartDateTime.ToMMDDYYString(),
                    invalidPatientId),
                viewContext.SelectedRemittanceMessage.AllMessages);

            // Verify posted adjustments & informations
            var billingServiceIds = new[] 
            {
                testData.BillingService.Id, 
                testData.Over5CharacterCodeBillingService.Id, 
                testData.BillingServiceWithModifiers.Id,
                testData.SecondBillingService.Id
            };
            var refreshedBillingServices = PracticeRepository.BillingServices
                .Include(
                    bs => bs.Adjustments.Select(a => a.InvoiceReceivable.PatientInsurance),
                    bs => bs.FinancialInformations.Select(f => f.InvoiceReceivable.PatientInsurance))
                .Where(bs => billingServiceIds.Contains(bs.Id))
                .ToList();

            var over5CharacterCodeBillingService = refreshedBillingServices.First(bs => bs.Id == testData.Over5CharacterCodeBillingService.Id);
            var billingServiceWithModifiers = refreshedBillingServices.First(bs => bs.Id == testData.BillingServiceWithModifiers.Id);
            var billingService = refreshedBillingServices.First(bs => bs.Id == testData.BillingService.Id);
            var secondBillingService = refreshedBillingServices.First(bs => bs.Id == testData.SecondBillingService.Id);

            Assert.AreEqual(2, over5CharacterCodeBillingService.Adjustments.Count, "2 adjustments should be posted for 'over 5 character code' billing service");
            Assert.AreEqual(1, over5CharacterCodeBillingService.FinancialInformations.Count, "1 financial information should be posted for 'over 5 character code' billing service");
            Assert.AreEqual(1, billingServiceWithModifiers.Adjustments.Count, "1 adjustment should be posted for 'with modifiers' billing service");
            Assert.AreEqual(0, billingService.Adjustments.Count + billingService.FinancialInformations.Count, "0 transactions should be posted for mentioned billing service");
            Assert.AreEqual(1, secondBillingService.Adjustments.Count, "1 adjustment should be posted for 2nd billing service");

            // All invoice receivables must be for our invoice and ids must match
            var distinctInvoiceReceivables = over5CharacterCodeBillingService.Adjustments
                .Select(a => a.InvoiceReceivable)
                .Union(billingServiceWithModifiers.Adjustments.Select(a => a.InvoiceReceivable))
                .Union(over5CharacterCodeBillingService.FinancialInformations.Select(f => f.InvoiceReceivable))
                .Distinct()
                .ToList();
            Assert.AreEqual(1, distinctInvoiceReceivables.Count, "Must be same invoice receivable");
            Assert.AreEqual(testData.NonBilledInvoice.Encounter.PatientId, distinctInvoiceReceivables.First().PatientInsurance.InsuredPatientId, "Patient id must match");
            Assert.AreEqual(testData.NonBilledInvoice.Id, distinctInvoiceReceivables.First().InvoiceId, "Invoice id must match");
            Assert.AreNotEqual(secondBillingService.Adjustments.First().InvoiceReceivableId, distinctInvoiceReceivables.First(), "Must be different invoice receivables");
            Assert.AreEqual(testData.SecondNonBilledInvoice.Encounter.PatientId, secondBillingService.Adjustments.First().InvoiceReceivable.PatientInsurance.InsuredPatientId, "Patient id must match for 2");

            //// Post 2nd remittance for same billing service

            // Setup billing service transaction
            var billingServiceTransaction = new BillingServiceTransaction
            {
                AmountSent = 1,
                BillingServiceId = secondBillingService.Id,
                BillingServiceTransactionStatus = BillingServiceTransactionStatus.Queued,
                DateTime = DateTime.UtcNow,
                InvoiceReceivableId = secondBillingService.Adjustments.First().InvoiceReceivable.Id,
                MethodSent = MethodSent.Paper
            };
            PracticeRepository.Save(billingServiceTransaction);

            // Setup X12 message
            var secondX12Message = CreateX12Message(DateTime.UtcNow, "check2", testData.SelectedInsurer.PayerCode, 9m, lx =>
            {
                AddPatient(lx, testData.SecondNonBilledInvoice.Encounter.PatientId, RemittanceMessage.KnownClaimStatusCode.ProcessedSecondary, clp =>
                {
                    AddServicePayment(clp,
                        testData.SecondBillingService.Id,
                        testData.SecondNonBilledInvoice.Encounter.StartDateTime,
                        testData.SecondBillingService.EncounterService.Code,
                        testData.SecondBillingService.BillingServiceModifiers.Select(m => m.ServiceModifier.Code).ToArray(),
                        svc =>
                        {
                            // Add $9 payment that will be posted
                            svc.SetElement(3, (9m).ToString(USCulture));
                        });
                });
            });

            viewContext = LoadAutopostingViewContext();

            // Post it
            const string secondPostedFileName = "TestPostingAgainstBilledInvoice.835";
            FileManager.Instance.CommitContents(string.Format("{0}\\{1}", viewContext.SourcePath, secondPostedFileName), secondX12Message);
            viewContext.Upload.ExecuteAndWait();
            Assert.IsTrue(viewContext.Remittances.Any(r => r.FileName == secondPostedFileName), "Remittance file {0} must have been uploaded", secondPostedFileName);
            viewContext.SelectedRemittanceMessage = viewContext.Remittances.First(r => r.FileName == secondPostedFileName);
            viewContext.LoadRemittanceAdvices.ExecuteAndWait();
            viewContext.Post.ExecuteAndWait();

            // Verify no errors
            Assert.AreEqual(@"PLB02 20150212 PLB03 plb_1 PLB04 10", viewContext.SelectedRemittanceMessage.AllMessages);

            var refreshedSecondBillingService = PracticeRepository.BillingServices
                .Include(
                    bs => bs.Adjustments.Select(a => a.InvoiceReceivable.PatientInsurance),
                    bs => bs.FinancialInformations.Select(f => f.InvoiceReceivable.PatientInsurance))
                .First(bs => bs.Id == secondBillingService.Id);

            // Verify posted data
            Assert.AreEqual(2, refreshedSecondBillingService.Adjustments.Count, "2 adjustment should be posted for 2nd billing service");
            Assert.IsTrue(refreshedSecondBillingService.Adjustments.Select(a => a.InvoiceReceivableId).Distinct().Count() == 1, "Same InvoiceReceivableId must be re-used");

            // Verify special auto posting user is used
            var adjustmentsAuditRecords = Common.ServiceProvider.GetService<IAuditRepository>()
                .AuditEntriesFor<Adjustment>(refreshedSecondBillingService.Adjustments.Select(a => a.Id).ToArray())
                .ToList();

            var userIds = adjustmentsAuditRecords.Select(a => a.UserId.GetValueOrDefault()).Distinct().ToList();
            Assert.AreEqual(1, userIds.Count, "Adjustments must be posted by same autoposting user");
            var user = PracticeRepository.Users.FirstOrDefault(u => u.Id == userIds[0]);
            Assert.IsNotNull(user, "User must exist in database");
            // ReSharper disable once PossibleNullReferenceException
            Assert.IsTrue(user.GetType() == typeof(User)
                && user.IsArchived
                && user.UserName.EndsWith(AutopostingUserContext.AutopostingUserNameSuffix), "Autoposting user must be archived, of user type and username must end with " + AutopostingUserContext.AutopostingUserNameSuffix);
        }

        [TestMethod]
        public void TestAutopostingViewContextPosting()
        {
            var viewContext = LoadAutopostingViewContext();

            // Copy example file to the ServerDataPath\Remit folder.
            FileManager.Instance.CommitContents(viewContext.SourcePath + @"\PostingERA_AutopostingText.835", AutopostingResources.AutopostingTest);

            // Uploading a remittance message to the database.
            viewContext.Upload.ExecuteAndWait();
            Assert.IsTrue(FileManager.Instance.ListFiles(viewContext.SourcePath + @"\Uploaded").Length == 1);

            var uploadedMessages = GetRemittanceMessages();
            Assert.IsTrue(uploadedMessages.Count == 1);
            Assert.IsTrue(viewContext.Remittances.Count == 1);

            // Loading remittanceAdvices with the remittance message.
            viewContext.SelectedRemittanceMessage = viewContext.Remittances.Single();
            viewContext.LoadRemittanceAdvices.ExecuteAndWait();
            var selectedPaymentDate = CommonQueries.GetDatePart(viewContext.SelectedPaymentDate);
            var selectedPaymentMethod = viewContext.SelectedPaymentMethod.Id;
            var orderedRemittanceAdvices = viewContext.SelectedRemittanceMessage.RemittanceAdvices.Select(ra => ra).OrderBy(ra => ra.RemittanceDate).ToList();

            var checkCodeMedicareOnly = orderedRemittanceAdvices.Select(ra => ra.RemittanceCode).Last();
            var checkDateMedicareOnly = orderedRemittanceAdvices.Select(ra => ra.RemittanceDate).Last();
            var amountMedicareOnly = orderedRemittanceAdvices.Select(ra => ra.TotalAmount).Last();

            var checkCodeMedicareMedicaid = orderedRemittanceAdvices.Select(ra => ra.RemittanceCode).First();
            var checkDateMedicareMedicaid = orderedRemittanceAdvices.Select(ra => ra.RemittanceDate).First();
            var amountMedicareMedicaid = orderedRemittanceAdvices.Select(ra => ra.TotalAmount).First();

            Assert.IsTrue(viewContext.SelectedRemittanceMessage.RemittanceAdvices.Count() == 2);
            Assert.IsTrue(viewContext.SelectedRemittanceMessage.RemittanceAdvices.Select(ra => ra.InsurerName).First() == "MEDICARE");
            Assert.IsTrue(checkCodeMedicareOnly == "889201162");
            Assert.IsTrue(checkCodeMedicareMedicaid == "889124195");
            Assert.IsTrue(checkDateMedicareOnly.ToMMDDYYString() == "07/16/14");
            Assert.IsTrue(checkDateMedicareMedicaid.ToMMDDYYString() == "06/26/14");
            Assert.IsTrue(amountMedicareOnly.ToString(CultureInfo.InvariantCulture) == "768.71");
            Assert.IsTrue(amountMedicareMedicaid.ToString(CultureInfo.InvariantCulture) == "685");
            Assert.IsFalse(viewContext.SelectedRemittanceMessage.HasErrors);

            // Verify only one BST exists before posting.
            var dateOfService = new DateTime(2011, 5, 4);
            var billingServiceTransaction = PracticeRepository.BillingServiceTransactions.Where(b => b.InvoiceReceivable.Invoice.Encounter.Patient.LastName == "MEDICARE"
                && b.InvoiceReceivable.Invoice.Encounter.Patient.FirstName == "ONLY"
                && CommonQueries.GetDatePart(b.InvoiceReceivable.Invoice.Encounter.StartDateTime) == dateOfService
                && b.InvoiceReceivable.Invoice.InvoiceTypeId != (int)InvoiceType.Facility)
                .ToArray();
            Assert.IsTrue(billingServiceTransaction.Count() == 1, "One billing transaction expected");
            var transaction = billingServiceTransaction.First();
            Assert.IsTrue(transaction.BillingServiceTransactionStatusId == (int)BillingServiceTransactionStatus.Sent);

            // Posting the remittance message.
            viewContext.Post.ExecuteAndWait();

            // Verify posting created new Adjustments and FinancialInformations for Medicare Only.
            var financialBatchIdMedicareOnly = PracticeRepository.FinancialBatches
                .Where(f => f.CheckCode == checkCodeMedicareOnly && f.ExplanationOfBenefitsDateTime == checkDateMedicareOnly && f.Amount == amountMedicareOnly)
                .Select(f => f.Id).FirstOrDefault();
            var adjustmentsTotalMedicareOnly = PracticeRepository.Adjustments.Where(adj => adj.FinancialBatchId == financialBatchIdMedicareOnly
                && CommonQueries.GetDatePart(adj.PostedDateTime) == selectedPaymentDate
                && adj.FinancialBatch.PaymentMethodId == selectedPaymentMethod)
                .Select(a => a.Amount)
                .ToArray();
            var financialInformationsTotalMedicareOnly = PracticeRepository.FinancialInformations.Where(fi => fi.FinancialBatchId == financialBatchIdMedicareOnly
                && CommonQueries.GetDatePart(fi.PostedDateTime) == selectedPaymentDate)
                .Select(fi => fi.Amount)
                .ToArray();
            Assert.IsNotNull(financialBatchIdMedicareOnly);
            Assert.AreEqual((decimal)800.00, adjustmentsTotalMedicareOnly.Sum());
            Assert.AreEqual((decimal)200.00, financialInformationsTotalMedicareOnly.Sum());

            // Verify two BST exists after posting, and balances forwarded correctly.
            var billingServiceTransactionAfterPosting = PracticeRepository.BillingServiceTransactions.Where(b => b.InvoiceReceivable.Invoice.Encounter.Patient.LastName == "MEDICARE"
                && b.InvoiceReceivable.Invoice.Encounter.Patient.FirstName == "ONLY"
                && CommonQueries.GetDatePart(b.InvoiceReceivable.Invoice.Encounter.StartDateTime) == dateOfService
                && b.InvoiceReceivable.Invoice.InvoiceTypeId != (int)InvoiceType.Facility)
                .ToArray();
            Assert.IsTrue(billingServiceTransaction.Count() + 1 == 2);
            var transactionsAfterPosting = billingServiceTransactionAfterPosting.OrderByDescending(b => b.BillingServiceTransactionStatusId);
            Assert.IsNotNull(transactionsAfterPosting);
            Assert.IsTrue(transactionsAfterPosting.Select(t => t).First().BillingServiceTransactionStatusId == (int)BillingServiceTransactionStatus.ClosedSent);
            var createdBillingServiceTransasction = transactionsAfterPosting.Select(t => t).Last();
            Assert.IsTrue(createdBillingServiceTransasction.BillingServiceTransactionStatusId == (int)BillingServiceTransactionStatus.Queued);
            Assert.IsTrue(createdBillingServiceTransasction.MethodSentId == (int)MethodSent.Paper);

            // Verify posting created new Adjustments and FinancialInformations for Medicare Medicaid.
            var financialBatchIdMedicareMedicaid = PracticeRepository.FinancialBatches
                .Where(f => f.CheckCode == checkCodeMedicareMedicaid && f.ExplanationOfBenefitsDateTime == checkDateMedicareMedicaid && f.Amount == amountMedicareMedicaid)
                .Select(f => f.Id)
                .FirstOrDefault();
            var adjustmentsTotalMedicareMedicaid = PracticeRepository.Adjustments.Where(adj => adj.FinancialBatchId == financialBatchIdMedicareMedicaid
                && CommonQueries.GetDatePart(adj.PostedDateTime) == selectedPaymentDate
                && adj.FinancialBatch.PaymentMethodId == selectedPaymentMethod)
                .Select(a => a.Amount)
                .ToArray();
            var financialInformationsTotalMedicareMedicaid = PracticeRepository.FinancialInformations.Where(fi => fi.FinancialBatchId == financialBatchIdMedicareMedicaid
                && CommonQueries.GetDatePart(fi.PostedDateTime) == selectedPaymentDate)
                .Select(fi => fi.Amount)
                .ToArray();
            Assert.IsNotNull(financialBatchIdMedicareMedicaid);
            Assert.AreEqual((decimal)705.00, adjustmentsTotalMedicareMedicaid.Sum());
            Assert.AreEqual((decimal)147.5, financialInformationsTotalMedicareMedicaid.Sum());
        }

        private List<ExternalSystemMessage> GetRemittanceMessages()
        {
            var remittanceMessageTypeId = PracticeRepository.ExternalSystemExternalSystemMessageTypes
                .Where(esesmt => esesmt.ExternalSystemId == (int)ExternalSystemId.Payer &&
                                 esesmt.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.X12_835_Inbound)
                .Select(esesmt => esesmt.Id).Single();

            var uploadedMessages = PracticeRepository.ExternalSystemMessages
                .Where(esm => esm.ExternalSystemExternalSystemMessageTypeId == remittanceMessageTypeId)
                .ToList();
            return uploadedMessages;
        }

        [TestMethod]
        public void TestAutopostingViewContextParseException()
        {
            var viewContext = LoadAutopostingViewContext();

            // Copy example file to the ServerDataPath\Remit folder.
            FileManager.Instance.CommitContents(viewContext.SourcePath + @"\ParseExceptionERA_AutopostingText.835",
                @"CRABS!@#$%^&*()KITTENS!@#$%^&*()DOGS!@#$%^&*()LAZERS!@#$%^&*()" + AutopostingResources.AutopostingTest);

            // Uploading a remittance message to the database.
            viewContext.Upload.ExecuteAndWait();

            // Loading remittanceAdvices with the remittance message.
            viewContext.SelectedRemittanceMessage = viewContext.Remittances.Single();
            viewContext.LoadRemittanceAdvices.ExecuteAndWait(); // This will populate proper error messages

            // Make sure this file is unparseable and already marked as processed
            Assert.IsTrue(viewContext.SelectedRemittanceMessage.IsProcessed, "Must be processed");
            Assert.IsTrue(viewContext.SelectedRemittanceMessage.Errors.Contains("Could not parse remittance file."), "Must be no parseable");
        }

        [TestMethod]
        public void TestAutopostingViewContextRemittanceParsingErrors()
        {
            var viewContext = LoadAutopostingViewContext();

            // Copy example file to the ServerDataPath\Remit folder.
            var file = AutopostingResources.AutopostingTest;

            RemoveSegment(file, viewContext, "BPR", @"BPR segment not found.
The payers listed in the file is not mapped to an insurance. Please do the following: 
- Open the Setup Insurers Screen
- Click Remittance Mapping
- Enter any one of the following values: ""LAZER, TAZER"" as the Incoming Payer Id 
- Enter the correct Outgoing Payer Id (See the PayerId column of the Setup Insurers Screen for the insurer)");

            RemoveSegment(file, viewContext, "TRN", @"TRN segment not found.
Unable to obtain TRN02 (Remittance Code (ex: Check #).");
        }

        private static void RemoveSegment(string file, AutopostingViewContext viewContext, string segment, string errorMessage)
        {
            var result = SplitRemittanceFile(file, segment);

            // Copy example file to the ServerDataPath\Remit folder.
            FileManager.Instance.CommitContents(viewContext.SourcePath + @"\ParsingErrorsERA_AutopostingText.835", result);

            // Uploading a remittance message to the database.
            viewContext.Upload.ExecuteAndWait();

            // Loading remittanceAdvices with the remittance message.
            viewContext.SelectedRemittanceMessage = viewContext.Remittances.Single();

            // Make sure this file is unparseable.
            viewContext.LoadRemittanceAdvices.ExecuteAndWait();

            ValidateErrors(viewContext, errorMessage);

            viewContext.SelectedRemittanceMessage = null;
            viewContext.Remittances.Clear();
        }

        // ReSharper disable once UnusedParameter.Local
        private static void ValidateErrors(AutopostingViewContext viewContext, string errorMessage)
        {
            var remittanceMessageViewModel = viewContext.Remittances.First();
            var remittanceAdviceViewModel = remittanceMessageViewModel.RemittanceAdvices.First();
            Assert.IsTrue(remittanceAdviceViewModel.Errors == errorMessage);
        }

        private static string SplitRemittanceFile(string file, string segmentToRemove)
        {
            var fileParts = file.Split('~').ToList();
            var validFileParts = fileParts.RemoveWhere(f => f.Contains(segmentToRemove)).Select(s => s + @"~").ToList();
            validFileParts.RemoveAt(validFileParts.LastIndexOf("~"));
            var result = string.Join(string.Empty, validFileParts);
            return result;
        }
        private AutopostingViewContext LoadAutopostingViewContext()
        {
            // Construct new Autoposting view context.
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, AutopostingViewContext>>();
            var viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

            // Loading the UI parameters for the screen.
            viewContext.Load.ExecuteAndWait();

            // Init source path
            viewContext.SourcePath = _testSourcePath;

            Assert.IsTrue(viewContext.PaymentMethods.Any());
            Assert.IsTrue(!viewContext.Remittances.Any());
            Assert.IsNull(viewContext.SelectedRemittanceMessage);
            return viewContext;
        }

        private TestDataSet CreateTestData()
        {
            const string blueShieldCode = "00803"; // Payer code for blueshield insurer
            const int patientId = 8; // Patients from X12 test data set
            const int patientId2 = 3;

            var result = new TestDataSet();

            var testPatients = PracticeRepository.Patients
                .Include(p => p.InsurancePolicies.Select(ip => ip.Insurer), p => p.Encounters.Select(e => e.ClinicalInvoiceProviders))
                .Where(p => new[] { patientId, patientId2 }.Contains(p.Id))
                .ToList();
            var testPatient = testPatients.First(p => p.Id == patientId);
            var testPatient2 = testPatients.First(p => p.Id == patientId2);

            var serviceLocationCode = PracticeRepository.ServiceLocationCodes.First();
            var twoServiceModifiers = PracticeRepository.ServiceModifiers.Take(2).ToArray();
            var encounterServicesSelection = PracticeRepository.EncounterServices.Take(50).ToArray();
            var billingProvider = PracticeRepository.Providers.First();
            var externalSystemEntityMapping = PracticeRepository.ExternalSystemEntityMappings.First();

            // Select insurer
            result.SelectedInsurer = testPatient.InsurancePolicies.Select(ip => ip.Insurer).First(ins => ins.PayerCode == blueShieldCode);

            // Prepare test invoice
            result.NonBilledInvoice = new Invoice
            {
                Encounter = testPatient.LastEncounter,
                InvoiceType = InvoiceType.Professional,
                ClinicalInvoiceProvider = testPatient.LastEncounter.ClinicalInvoiceProviders.First(),
                ServiceLocationCode = serviceLocationCode,
                BillingProvider = billingProvider
            };

            result.NonBilledInvoice.BillingDiagnoses.Add(new BillingDiagnosis
            {
                OrdinalId = 1,
                ExternalSystemEntityMapping = externalSystemEntityMapping
            });

            // Prepare test billing services to post adjustments against
            // 1
            result.Over5CharacterCodeBillingService = new BillingService
            {
                EncounterService = encounterServicesSelection.First(es => es.Code.Length > 5),
                Unit = 1,
                UnitCharge = 100,
                UnitAllowableExpense = 80
            };
            result.NonBilledInvoice.BillingServices.Add(result.Over5CharacterCodeBillingService);

            // 2
            result.BillingServiceWithModifiers = new BillingService
            {
                EncounterService = encounterServicesSelection.Skip(1).First(),
                Unit = 1,
                UnitCharge = 120,
                UnitAllowableExpense = 100
            };
            result.BillingServiceWithModifiers.BillingServiceModifiers.Add(new BillingServiceModifier
            {
                OrdinalId = 1,
                ServiceModifier = twoServiceModifiers[0]
            });
            result.BillingServiceWithModifiers.BillingServiceModifiers.Add(new BillingServiceModifier
            {
                OrdinalId = 2,
                ServiceModifier = twoServiceModifiers[1]
            });
            result.NonBilledInvoice.BillingServices.Add(result.BillingServiceWithModifiers);

            // 3
            result.BillingService = new BillingService
            {
                EncounterService = encounterServicesSelection.Skip(2).First(),
                Unit = 1,
                UnitCharge = 200,
                UnitAllowableExpense = 150
            };
            result.NonBilledInvoice.BillingServices.Add(result.BillingService);

            // Save first
            PracticeRepository.Save(result.NonBilledInvoice);

            // Second patient invoice and billing service
            result.SecondNonBilledInvoice = new Invoice
            {
                Encounter = testPatient2.LastEncounter,
                InvoiceType = InvoiceType.Professional,
                ClinicalInvoiceProvider = testPatient2.LastEncounter.ClinicalInvoiceProviders.First(),
                ServiceLocationCode = serviceLocationCode,
                BillingProvider = billingProvider
            };
            result.SecondNonBilledInvoice.BillingDiagnoses.Add(new BillingDiagnosis
            {
                OrdinalId = 1,
                ExternalSystemEntityMapping = externalSystemEntityMapping
            });
            result.SecondBillingService = new BillingService
            {
                EncounterService = encounterServicesSelection.Skip(3).First(),
                Unit = 1,
                UnitCharge = 200,
                UnitAllowableExpense = 150
            };
            result.SecondNonBilledInvoice.BillingServices.Add(result.SecondBillingService);

            // Save second
            PracticeRepository.Save(result.SecondNonBilledInvoice);

            return result;
        }

        private string CreateX12Message(DateTime date, string checkNumber, string payerIdentifier, decimal total, Action<Loop> modifyLxLoop)
        {
            // Init supported interchange
            var i = new Interchange(date, 1, true);
            var fGroup = i.AddFunctionGroup("HP", date, 1);
            var transaction = fGroup.AddTransaction("835", "000000001");
            var bprSegment = transaction.AddSegment("BPR");
            var trnSegment = transaction.AddSegment("TRN");
            var n1Segment = transaction.AddLoop("N1*PR*TESTPAYER");

            // Set check number
            trnSegment.SetElement(2, checkNumber);

            // Add payer identifiers
            n1Segment.AddSegment("REF*NF*").SetElement(2, payerIdentifier);

            // Set total amount and date
            bprSegment.SetElement(2, total.ToString(USCulture));
            bprSegment.SetElement(16, date.ToString("yyyyMMdd", USCulture));

            // Add provider level adjustment for $10
            var plbSegment = transaction.AddSegment("PLB");
            plbSegment.SetElement(2, "20150212");
            plbSegment.SetElement(3, "plb_1");
            plbSegment.SetElement(4, (10m).ToString(USCulture));

            var claimPaymentsLoop = transaction.AddLoop("LX");
            modifyLxLoop(claimPaymentsLoop);
            var x12Message = i.SerializeToX12(false);
            return x12Message;
        }

        private void AddPatient(Loop lxLoop, int patientId, RemittanceMessage.KnownClaimStatusCode statusCode, Action<Loop> modifyClpLoop)
        {
            var claimPayment = lxLoop.AddLoop("CLP");

            // Patient id, status and date
            claimPayment.SetElement(1, patientId.ToString(USCulture));
            claimPayment.SetElement(2, ((int)statusCode).ToString(USCulture));
            modifyClpLoop(claimPayment);
        }

        private void AddServicePayment(Loop clpLoop, int billingServiceId, DateTime billingServiceDate, string procedureIdentifier, string[] procedureModifiers, Action<Loop> modifySvcLoop)
        {
            var servicePayment = clpLoop.AddLoop("SVC");

            // Set billing service id, date, procedure codes
            servicePayment.AddSegment("REF*6R").SetElement(2, billingServiceId.ToString(USCulture));
            servicePayment.AddSegment("DTM*472").SetElement(2, billingServiceDate.ToString("yyyyMMdd", USCulture));
            servicePayment.SetElement(1, "HC".CreateEnumerable().Union(procedureIdentifier.CreateEnumerable()).Union(procedureModifiers).Join(":"));

            modifySvcLoop(servicePayment);
        }

        private void AddPayment(Loop svcLoop, ClaimAdjustmentGroupCode groupCode, string reasonCode, decimal amount)
        {
            var prAdjustments = svcLoop.AddSegment("CAS");
            prAdjustments.SetElement(1, PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMappings[groupCode]);
            prAdjustments.SetElement(2, reasonCode);
            prAdjustments.SetElement(3, amount.ToString(USCulture));
        }

        private class TestDataSet
        {
            public Insurer SelectedInsurer { get; set; }

            public Invoice NonBilledInvoice { get; set; }
            public BillingService Over5CharacterCodeBillingService { get; set; }
            public BillingService BillingServiceWithModifiers { get; set; }
            public BillingService BillingService { get; set; }

            public Invoice SecondNonBilledInvoice { get; set; }
            public BillingService SecondBillingService { get; set; }
        }
    }
}
