﻿using System;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Financials.EditInvoice;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Linq;
using Soaf.Presentation;
using Soaf;
using EncounterServiceViewModel = IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice.EncounterServiceViewModel;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class EditInvoicePresentationTests : X12MetadataTestsBase
    {
        [TestMethod]
        public void CreateAndEditInvoiceTest()
        {
            EditInvoiceViewContext viewContext;
            SetupTestContext(out viewContext);

            viewContext.LoadArguments = new EditInvoiceLoadArguments
            {
                PatientId = 1
            };
            viewContext.EditInvoice.Invoice.Date = DateTime.Now;
            viewContext.Load.ExecuteAndWait();
            if (PermissionId.EditInvoice.EnsurePermission(false))
            {
                //Create Manual Claim
                FillInvoice(viewContext);
                viewContext.SaveInvoice.Execute();
                Assert.IsNotNull(viewContext.EditInvoice.Invoice.Id);
                var invoice = LoadInvoice(viewContext.EditInvoice.Invoice.Id);
                Assert.AreEqual(invoice.ClaimComment, viewContext.EditInvoice.Invoice.ClaimNote);
                Assert.AreEqual(invoice.InvoiceTypeId, viewContext.EditInvoice.Invoice.InvoiceType.Id);
                Assert.AreEqual(invoice.Encounter.InsuranceTypeId, (int)InsuranceType.MajorMedical);
                Assert.AreEqual(invoice.AttributeToServiceLocationId, viewContext.EditInvoice.Invoice.AttributeTo.Id);
                Assert.AreEqual(invoice.ServiceLocationCodeId, viewContext.EditInvoice.Invoice.ServiceLocation.CodeId);
                Assert.AreEqual(invoice.Encounter.ServiceLocationId, viewContext.EditInvoice.Invoice.ServiceLocation.Id);
                Assert.AreEqual(invoice.Encounter.StartDateTime.Date, viewContext.EditInvoice.Invoice.Date.Date);
                Assert.AreEqual(invoice.BillingProvider.BillingOrganizationId, viewContext.EditInvoice.Invoice.CareTeam.RenderingProvider.BillingOrganization.Id);
                Assert.AreEqual(invoice.BillingProvider.UserId, viewContext.EditInvoice.Invoice.CareTeam.RenderingProvider.Provider.Id);
                var newDiagnosisIds = viewContext.EditInvoice.Invoice.EncounterDiagnoses.Select(x => x.ActiveDiagnosisCode.Id);
                Assert.IsNotNull(invoice.BillingDiagnoses.Where(x => newDiagnosisIds.Contains(x.ExternalSystemEntityMappingId)));

                var newServicesIds = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Id);
                Assert.IsNotNull(invoice.BillingServices.Where(x => newServicesIds.Contains(x.Id)));

                Assert.AreEqual(invoice.BillingServices.Select(x => x.UnitCharge).First(), viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Charge).First());
                Assert.AreEqual(invoice.BillingServices.Select(x => x.Unit).First(), viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Units).First());

                var modifiersIds = viewContext.EditInvoice.Invoice.EncounterServices.SelectMany(x => x.Modifiers.Select(y => y.Id));
                Assert.IsNotNull(invoice.BillingServices.Where(x => x.BillingServiceModifiers.All(y => modifiersIds.Contains(y.ServiceModifierId))));
                Assert.IsNotNull(invoice.BillingServices.Where(x => x.BillingServiceBillingDiagnoses.All(y => newDiagnosisIds.Contains(y.BillingDiagnosisId))));
                var patientInsurancesIds = PracticeRepository.PatientInsurances.Where(x => x.InsuredPatientId == viewContext.EditInvoice.Invoice.Patient.Id
                                                                                           && x.InsurancePolicy != null && viewContext.EditInvoice.Invoice.Date.Date >= x.InsurancePolicy.StartDateTime
                                                                                           && (viewContext.EditInvoice.Invoice.Date.Date <= x.EndDateTime || !x.EndDateTime.HasValue)
                                                                                           && InsuranceType.MajorMedical == x.InsuranceType).Select(x => x.Id).ToArray();

                Assert.IsNotNull(invoice.InvoiceReceivables.Any(x => patientInsurancesIds.Contains(x.PatientInsuranceId.GetValueOrDefault())));
                Assert.IsNotNull(invoice.InvoiceReceivables.Any(x => !x.PatientInsuranceId.HasValue));

                //Edit Claim
                viewContext.EditInvoice.Invoice.ClaimNote = "New Comment";
                viewContext.EditInvoice.Invoice.InvoiceType.Id = (int)InvoiceType.Vision;
                viewContext.EditInvoice.Invoice.AttributeTo.Id = 2;
                viewContext.EditInvoice.Invoice.ServiceLocation.CodeId = 21;
                viewContext.EditInvoice.Invoice.ServiceLocation.Id = 2;
                viewContext.EditInvoice.Invoice.CareTeam.RenderingProvider.Id = 7;
                viewContext.EditInvoice.Invoice.EncounterDiagnoses.Add(new DiagnosisViewModel
                {
                    ActiveDiagnosisCode = new DiagnosisCodeViewModel
                    {
                        Id = 2
                    },
                    DiagnosisLinkPointer = new DiagnosisLinkPointerViewModel
                    {
                        PointerLetter = "B"
                    }
                }
                    );
                viewContext.EditInvoice.Invoice.EncounterServices.Add(new EncounterServiceViewModel
                {
                    Service = new ServiceViewModel
                    {
                        Id = 1
                    },
                    Units = 10,
                    Charge = 12,
                    Allowed = 20,
                    Modifiers = new System.Collections.ObjectModel.ObservableCollection<NameAndAbbreviationAndDetailViewModel>
                    {
                        new NameAndAbbreviationAndDetailViewModel
                        {
                            Id = 2
                        }
                    },
                    LinkedDiagnoses = new System.Collections.ObjectModel.ObservableCollection<DiagnosisLinkPointerViewModel>
                    {
                        new DiagnosisLinkPointerViewModel
                        {
                            PointerLetter = "B"
                        }
                    },
                    PatientBillDate = new DateTime(2014, 03, 18),
                    NdcCode = "Ndc 1",
                    UnclassifiedServiceDescription = "USD 1",
                    FreehandNote = "Freehand 1"
                });

                viewContext.SaveInvoice.Execute();
                Assert.IsNotNull(viewContext.EditInvoice.Invoice.Id);
                invoice = LoadInvoice(viewContext.EditInvoice.Invoice.Id);
                Assert.AreEqual(invoice.ClaimComment, viewContext.EditInvoice.Invoice.ClaimNote);
                Assert.AreEqual(invoice.InvoiceTypeId, viewContext.EditInvoice.Invoice.InvoiceType.Id);
                Assert.AreEqual(invoice.Encounter.InsuranceTypeId, (int)InsuranceType.Vision);
                Assert.AreEqual(invoice.AttributeToServiceLocationId, viewContext.EditInvoice.Invoice.AttributeTo.Id);
                Assert.AreEqual(invoice.ServiceLocationCodeId, viewContext.EditInvoice.Invoice.ServiceLocation.CodeId);
                Assert.AreEqual(invoice.Encounter.ServiceLocationId, viewContext.EditInvoice.Invoice.ServiceLocation.Id);
                Assert.AreEqual(invoice.Encounter.StartDateTime.Date, viewContext.EditInvoice.Invoice.Date.Date);
                Assert.AreEqual(invoice.BillingProvider.BillingOrganizationId, viewContext.EditInvoice.Invoice.CareTeam.RenderingProvider.BillingOrganization.Id);
                Assert.AreEqual(invoice.BillingProvider.UserId, viewContext.EditInvoice.Invoice.CareTeam.RenderingProvider.Provider.Id);
                var editDiagnosisIds = viewContext.EditInvoice.Invoice.EncounterDiagnoses.Select(x => x.ActiveDiagnosisCode.Id);
                Assert.IsNotNull(invoice.BillingDiagnoses.Where(x => editDiagnosisIds.Contains(x.ExternalSystemEntityMappingId)));

                var editServicesIds = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Id);
                Assert.IsNotNull(invoice.BillingServices.Where(x => editServicesIds.Contains(x.Id)));
                Assert.AreEqual(invoice.BillingServices.Where(x => editServicesIds.Contains(x.Id)).Select(x => new
                {
                    x.PatientBillDate,
                    NdcCode = x.Ndc,
                    EmgCode = x.IsEmg,
                    EpsdtCode = x.IsEpsdt,
                    x.UnclassifiedServiceDescription,
                    FreehandNote = x.ClaimComment
                }), new
                {
                    PatientBillDate = new DateTime(2014, 03, 18),
                    NdcCode = "Ndc 1",
                    EmgCode = "Emg 1",
                    EpsdtCode = "Epsdt 1",
                    UnclassifiedServiceDescription = "USD 1",
                    FreehandNote = "Freehand 1"
                });
                var charges = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Charge).ToArray();
                Assert.IsNotNull(invoice.BillingServices.Any(x => charges.Contains(x.Charge.GetValueOrDefault())));

                var units = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Units).ToArray();
                Assert.IsNotNull(invoice.BillingServices.Any(x => units.Contains(x.Unit.GetValueOrDefault())));
                modifiersIds = viewContext.EditInvoice.Invoice.EncounterServices.SelectMany(x => x.Modifiers.Select(y => y.Id));
                Assert.IsNotNull(invoice.BillingServices.Where(x => x.BillingServiceModifiers.All(y => modifiersIds.Contains(y.ServiceModifierId))));
                Assert.IsNotNull(invoice.BillingServices.Where(x => x.BillingServiceBillingDiagnoses.All(y => editDiagnosisIds.Contains(y.BillingDiagnosisId))));

                Assert.IsNotNull(invoice.InvoiceReceivables.Any(x => patientInsurancesIds.Contains(x.PatientInsuranceId.GetValueOrDefault())));
                Assert.IsNotNull(invoice.InvoiceReceivables.Any(x => !x.PatientInsuranceId.HasValue));
            }
        }

        [TestMethod]
        public void BillInvoiceTest()
        {
            EditInvoiceViewContext viewContext;
            SetupTestContext(out viewContext);
            viewContext.LoadArguments = new EditInvoiceLoadArguments
            {
                InvoiceId = 1
            };
            viewContext.Load.ExecuteAndWait();
            var nextPayerViewModel = new NextPayerViewModel
            {
                Payer = new PatientPayerViewModel
                {
                    Id = 1,
                    Abbreviation = "P"
                }
            };

            if (PermissionId.EditInvoice.EnsurePermission(false))
            {
                viewContext.NextPayerRowSelected.Execute(nextPayerViewModel);
                viewContext.Billing.Execute();

                //Check if the validation part is not failing as dialog will be close after billing is done.
                if (viewContext.InteractionContext.DialogResult.GetValueOrDefault())
                {
                    var invoice = LoadInvoice(viewContext.EditInvoice.Invoice.Id);
                    var editServicesIds = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Id);
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.Any()));
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => editServicesIds.Contains(y.BillingServiceId))));

                    var balances = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => x.Balance).ToArray();
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => balances.Contains(y.AmountSent))));
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => y.DateTime.Date == DateTime.Now.Date)));

                    var billingServiceTransactionStatuses = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => MapBillingServiceTransaction(x.BillingAction.Id)).ToArray();
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => billingServiceTransactionStatuses.Contains(y.BillingServiceTransactionStatus))));

                    var methodSentIds = viewContext.EditInvoice.Invoice.EncounterServices.Select(x => MapBillingMethodSent(x.BillingAction.Id)).ToArray();
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => methodSentIds.Contains(y.MethodSent))));

                    var claimFileReceiverIDs = viewContext.EditInvoice.Invoice.EncounterServices.Where(x => (x.NextPayer.Payer as PatientInsurancePayerViewModel) != null).Select(x => ((PatientInsurancePayerViewModel)x.NextPayer.Payer).ClaimFileReceiverId).ToArray();
                    Assert.IsNotNull(invoice.BillingServices.Select(x => x.BillingServiceTransactions.All(y => claimFileReceiverIDs.Contains(y.ClaimFileReceiverId))));

                    Assert.IsTrue(!invoice.InvoiceReceivables.Any(x => x.OpenForReview));

                    var billingServiceDetails = viewContext.EditInvoice.Invoice.EncounterServices.Where(x => (x.NextPayer.Payer as InsurerPayerViewModel) != null).Select(x => new
                    {
                        x.PatientBillDate,
                        x.NdcCode,
                        x.IsEmg,
                        x.IsEpsdt,
                        x.UnclassifiedServiceDescription,
                        x.FreehandNote
                    });
                    Assert.AreEqual(new
                    {
                        PatientBillDate = new DateTime(2014, 03, 17),
                        NdcCode = "Ndc",
                        IsEmg = false,
                        IsEpsdt = false,
                        UnclassifiedServiceDescription = "USD",
                        FreehandNote = "Freehand"

                    }, billingServiceDetails);
                }
            }

        }

        private static void SetupTestContext(out EditInvoiceViewContext viewContext)
        {
            var viewContextConstruct = Common.ServiceProvider.GetService<Func<IInteractionManager, EditInvoiceViewContext>>();
            viewContext = viewContextConstruct(StubInteractionManager.CreateAllApprovingInstance());

        }
        private Invoice LoadInvoice(int? id)
        {
            var invoice = PracticeRepository.Invoices.Where(x => x.Id == id).Select(x => x).Select(inv => inv).FirstOrDefault();

            PracticeRepository.AsQueryableFactory().Load(invoice,
                inv => inv.ReferringExternalProvider.PatientExternalProviders.Select(x => x.ExternalProvider),
                inv => inv.BillingDiagnoses.Select(x => x.ExternalSystemEntityMapping.ExternalSystemEntity),
                inv => inv.InvoiceComments,
                inv => inv.InvoiceReceivables.Select(x => x.PatientInsurance),
                inv => inv.BillingServices.Select(x => x.BillingServiceBillingDiagnoses.Select(y => y.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntity)),
                inv => inv.BillingServices.Select(x => x.BillingServiceModifiers.Select(y => y.ServiceModifier)),
                inv => inv.BillingServices.Select(x => x.EncounterService),
                inv => inv.BillingServices.Select(x => x.FacilityEncounterService),
                inv => inv.BillingServices.Select(x => x.FacilityEncounterService.RevenueCode),
                inv => inv.BillingServices.Select(x => x.Adjustments.Select(y => y.AdjustmentType)),
                inv => inv.BillingServices.Select(x => x.BillingServiceTransactions),
                inv => inv.BillingProvider.BillingOrganization.IdentifierCodes,
                inv => inv.BillingProvider.User,
                inv => inv.Encounter.ServiceLocation,
                inv => inv.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.User),
                inv => inv.Encounter.Patient.PatientAddresses.Select(y => y.StateOrProvince),
                inv => inv.Encounter.PatientInsuranceAuthorizations,
                inv => inv.Encounter.Patient.PatientInsurances.Select(x => x.InsurancePolicy).Select(x => x.Insurer),
                inv => inv.Encounter.Patient.InsurancePolicies.Select(x => x.Insurer).Select(x => x.InsurerAddresses.Select(y => y.StateOrProvince)),
                inv => inv.Encounter.Patient.PatientInsurances.Select(x => x.InvoiceReceivables),
                inv => inv.AttributeToServiceLocation);

            return invoice;

        }

        private static void FillInvoice(EditInvoiceViewContext viewContext)
        {
            if (viewContext != null)
            {
                viewContext.EditInvoice.Invoice.ClaimNote = "Unit test comment";
                viewContext.EditInvoice.Invoice.InvoiceType = new NamedViewModel
                {
                    Id = (int)InvoiceType.Professional
                };
                viewContext.EditInvoice.Invoice.CareTeam = new Practiceware.Presentation.ViewModels.Financials.Common.Provider.CareTeamViewModel
                {
                    RenderingProvider = new Practiceware.Presentation.ViewModels.Financials.Common.Provider.ProviderViewModel
                    {
                        BillingOrganization = new NamedViewModel
                        {
                            Id = 1,
                        },
                        Provider = new NamedViewModel
                        {
                            Id = 1,
                        },
                        Id = 1
                    },
                    IsNotManualClaim = false
                };
                viewContext.EditInvoice.Invoice.ServiceLocation = new ServiceLocationViewModel
                {
                    CodeId = 21,
                    Id = 1
                };
                viewContext.EditInvoice.Invoice.AttributeTo = new ServiceLocationViewModel
                {
                    Id = 1
                };

                viewContext.EditInvoice.Invoice.ReferringProvider = new NameAndAbbreviationAndDetailViewModel
                {
                    Id = 7
                };
                var activeDiagnosis = new DiagnosisCodeViewModel();
                activeDiagnosis.Id = 1;
                viewContext.EditInvoice.Invoice.EncounterDiagnoses = new System.Collections.ObjectModel.ObservableCollection<DiagnosisViewModel>
                {
                    new DiagnosisViewModel
                    {
                        ActiveDiagnosisCode = activeDiagnosis,
                        DiagnosisLinkPointer = new DiagnosisLinkPointerViewModel
                        {
                            PointerLetter = "A"
                        }
                    }
                };
                viewContext.EditInvoice.Invoice.EncounterServices = new System.Collections.ObjectModel.ObservableCollection<EncounterServiceViewModel>
                {
                    new EncounterServiceViewModel
                    {
                        Service = new ServiceViewModel
                        {
                            Id = 1
                        },
                        Units = 5,
                        Charge = 12,
                        Allowed = 20,
                        Modifiers = new System.Collections.ObjectModel.ObservableCollection<NameAndAbbreviationAndDetailViewModel>
                        {
                            new NameAndAbbreviationAndDetailViewModel
                            {
                                Id = 1
                            }
                        },
                        LinkedDiagnoses = new System.Collections.ObjectModel.ObservableCollection<DiagnosisLinkPointerViewModel>
                        {
                            new DiagnosisLinkPointerViewModel
                            {
                                PointerLetter = "A"
                            }
                        },
                        PatientBillDate = new DateTime(2014, 03, 17),
                        NdcCode = "Ndc",
                        IsEmg = false,
                        IsEpsdt = false,
                        UnclassifiedServiceDescription = "USD",
                        FreehandNote = "Freehand"
                    }
                };
            }
        }
        private static BillingServiceTransactionStatus MapBillingServiceTransaction(int actionId)
        {
            var actionItem = (BillingActionType)actionId;
            switch (actionItem)
            {
                case BillingActionType.ClaimWait:
                case BillingActionType.StatementWait:
                    return BillingServiceTransactionStatus.Wait;
                case BillingActionType.CrossoverClaim:
                    return BillingServiceTransactionStatus.SentCrossOver;
                default:
                    return BillingServiceTransactionStatus.Queued;
            }

        }
        private static MethodSent MapBillingMethodSent(int actionId)
        {
            var actionItem = (BillingActionType)actionId;
            switch (actionItem)
            {
                case BillingActionType.QueueClaimPaper:
                case BillingActionType.QueueStatement:
                    return MethodSent.Paper;
                default:
                    return MethodSent.Electronic;
            }
        }

    }
}



