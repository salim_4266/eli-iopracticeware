﻿using System;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using IO.Practiceware.Presentation.Views.PrinterSetup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Presentation;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    [TestRunMode(ParallelExecutionTolerance = TestParallelExecutionTolerance.RequiresGlobalLock)] // saves Save on ConfigurationManager which will cause threading problems for tests that read at the same time.
    public class PrinterSetupPresentationTests : TestBase
    {
        private IInteractionContext _interactionContext;
        private bool _overridePrinterConfiguration;
        
        protected override void OnAfterTestInitialize()
        {
            _interactionContext = Common.ServiceProvider.GetService<IInteractionContext>();
            _overridePrinterConfiguration = PrinterConfiguration.OverridePrinterConfiguration;
        }

        protected override void OnBeforeTestCleanup()
        {
            _interactionContext.Dispose();
            PrinterConfiguration.OverridePrinterConfiguration = _overridePrinterConfiguration;
        }

     
        /// <summary>
        /// Tests the Printer setup Add Printer View context..
        /// </summary>
        [TestMethod]
        [Ignore] //Should be created as coded UI test.
        public void TestAddPrinterViewContext()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(
               "UPDATE [model].[Sites] SET ShortName = '" + UserContext.Current.ServiceLocation.Item2 + "' WHERE Id = (SELECT TOP 1 Id FROM [model].[Sites])");

            var addPrinterViewService = Common.ServiceProvider.GetService<IAddPrinterViewService>();
            var addPrinterViewContext = new AddPrinterViewContext(addPrinterViewService) { InteractionContext = _interactionContext };
            //Testing Load command
            Func<bool> loadPrinters =
                () =>
                addPrinterViewContext.Printers != null && addPrinterViewContext.Printers.Count > 0 &&
                addPrinterViewContext.Printers.Last().Name != "Searching..." &&
                addPrinterViewContext.ServiceLocations != null && addPrinterViewContext.ServiceLocations.Count > 0 &&
                addPrinterViewContext.SelectedServiceLocation != null;
            addPrinterViewContext.Load.Execute(addPrinterViewContext.InteractionContext);
            loadPrinters.Wait(TimeSpan.FromSeconds(180));
            Assert.IsTrue(loadPrinters());
            Assert.IsTrue(addPrinterViewContext.Printers.Count > 0);
            Assert.IsNotNull(addPrinterViewContext.ServiceLocations);
            Assert.IsTrue(addPrinterViewContext.ServiceLocations.Count > 0);

            addPrinterViewContext.Printers.Clear();
            Assert.IsTrue(addPrinterViewContext.Printers.Count == 0);

            ////Testing Refresh printers command
            Func<bool> refreshPrinters =
                () => addPrinterViewContext.Printers.Count > 0;
            addPrinterViewContext.RefreshPrinters.Execute(addPrinterViewContext.InteractionContext);
            refreshPrinters.Wait(TimeSpan.FromSeconds(180));
            Assert.IsTrue(refreshPrinters());
            Assert.IsTrue(addPrinterViewContext.Printers.Count > 0);
        }

        /// <summary>
        /// Tests the Printer setup Document Printer View context..
        /// </summary>
        [TestMethod]
        public void TestDocumentPrinterViewContext()
        {
            TemplateDocument document = CreateDocument();
            Common.PracticeRepository.Save(document);
            Assert.IsTrue(document.Id > 0);

            var documentPrinterViewService = Common.ServiceProvider.GetService<IDocumentPrinterViewService>();
            var documentPrinterViewContext = new DocumentPrinterViewContext(documentPrinterViewService) { InteractionContext = _interactionContext };
            //Testing Load command
            Func<bool> loadDocuments =
                () =>
                documentPrinterViewContext.DocumentTypes != null && documentPrinterViewContext.IoPrinters != null &&
                documentPrinterViewContext.DocumentTypes.Count > 0 && documentPrinterViewContext.IoPrinters.Count > 0 && !documentPrinterViewContext.Load.As<AsyncCommand>().IsBusy;
            documentPrinterViewContext.Load.Execute(documentPrinterViewContext.InteractionContext);
            loadDocuments.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadDocuments());
            Assert.IsNotNull(documentPrinterViewContext.DocumentTypes);
            Assert.IsTrue(documentPrinterViewContext.DocumentTypes.Count > 0);
            Assert.IsNotNull(documentPrinterViewContext.IoPrinters);
            Assert.IsNotNull(documentPrinterViewContext.Documents);
            Assert.IsFalse(documentPrinterViewContext.IsPrinterOptionsEnabled);

            //Testing Change DocumentType command
            documentPrinterViewContext.SelectedDocumentType =
                documentPrinterViewContext.DocumentTypes.FirstOrDefault(d => d.Id == document.TemplateDocumentCategoryId);
            Assert.IsNotNull(documentPrinterViewContext.SelectedDocumentType);
            Func<bool> loadTemplates = () => documentPrinterViewContext.Documents.Count > 1 && documentPrinterViewContext.Documents.WithId(document.Id) != null;
            loadTemplates.Wait(TimeSpan.FromSeconds(30));
            Assert.IsTrue(loadTemplates());
            Assert.IsTrue(documentPrinterViewContext.Documents.Count > 1);
            Assert.IsTrue(documentPrinterViewContext.Documents.First().Name == "Apply to All Templates");

            Assert.IsTrue(documentPrinterViewContext.IoPrinters.Count > 0);
            documentPrinterViewContext.IoPrinters.Clear();
            Assert.IsTrue(documentPrinterViewContext.IoPrinters.Count == 0);
        }

        private static TemplateDocument CreateDocument()
        {
            var document = new TemplateDocument
                               {
                                   TemplateDocumentCategoryId = 2,
                                   Name = "Test Document",
                                   DisplayName = "Test Document",
                               };
            return document;
        }

        [TestMethod]
        [Ignore]
        public void TestPrinterSetupViewContext()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(
                "UPDATE [model].[Sites] SET ShortName = '" + UserContext.Current.ServiceLocation + "' WHERE Id = (SELECT TOP 1 Id FROM [model].[Sites])");

            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(
                "INSERT INTO [model].[Documents] (DocumentTypeId, Name) values (1,'DrugRx.Doc')");

            var addPrinterViewService = Common.ServiceProvider.GetService<IAddPrinterViewService>();
            var documentPrinterViewService = Common.ServiceProvider.GetService<IDocumentPrinterViewService>();
            var documentSummaryViewService = Common.ServiceProvider.GetService<IDocumentSummaryViewService>();

            var addPrinterViewContext = new AddPrinterViewContext(addPrinterViewService) { InteractionContext = _interactionContext };
            var documentPrinterViewContext = new DocumentPrinterViewContext(documentPrinterViewService) { InteractionContext = _interactionContext };
            var documentSummaryViewContext = new DocumentSummaryViewContext(documentSummaryViewService) { InteractionContext = _interactionContext };

            //Testing Load commands
            addPrinterViewContext.Load.Execute(null);
            Func<bool> addPrinterIsLoaded = () => addPrinterViewContext.Load.CanExecute(null);
            addPrinterIsLoaded.Wait(TimeSpan.FromSeconds(10));

            documentPrinterViewContext.Load.Execute(null);
            Func<bool> documentPrinterIsLoaded = () => documentPrinterViewContext.Load.CanExecute(null);
            documentPrinterIsLoaded.Wait(TimeSpan.FromSeconds(10));

            documentSummaryViewContext.Load.Execute(null);
            Func<bool> documentSummaryIsLoaded = () => documentSummaryViewContext.Load.CanExecute(null);
            documentSummaryIsLoaded.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(addPrinterViewContext.UncPath == string.Empty);
            Assert.IsTrue(addPrinterViewContext.IoPrinterName == string.Empty);
            Assert.IsTrue(addPrinterViewContext.SelectedServiceLocation.Id == 1);

            Func<bool> loadDocuments =
                () =>
                documentPrinterViewContext.DocumentTypes != null && documentPrinterViewContext.IoPrinters != null &&
                documentPrinterViewContext.DocumentTypes.Count > 0 && documentPrinterViewContext.IoPrinters.Count > 0 && !documentPrinterViewContext.Load.As<AsyncCommand>().IsBusy;
            loadDocuments.Wait(TimeSpan.FromSeconds(10));
            Assert.IsTrue(loadDocuments());
            Assert.IsNotNull(documentPrinterViewContext.DocumentTypes);
            Assert.IsTrue(documentPrinterViewContext.DocumentTypes.Count > 0);
            Assert.IsNotNull(documentPrinterViewContext.IoPrinters);
            Assert.IsNotNull(documentPrinterViewContext.Documents);
            Assert.IsNotNull(documentPrinterViewContext.IoPrinters.Count == 1);

            Func<bool> loadDocumentSummary =
                () =>
                documentSummaryViewContext.DocumentPrinters != null &&
                documentSummaryViewContext.DocumentPrinters.Count == 0;

            loadDocumentSummary.Wait(TimeSpan.FromSeconds(100));
            Assert.IsTrue(loadDocumentSummary());
            Assert.IsNotNull(documentSummaryViewContext.DocumentPrinters);
            Assert.IsTrue(documentSummaryViewContext.DocumentPrinters.Count == 0);

            Func<bool> loadPrinters =
                () =>
                addPrinterViewContext.Printers != null && addPrinterViewContext.Printers.Count > 0 &&
                addPrinterViewContext.Printers.Last().Name != "Searching..." &&
                addPrinterViewContext.ServiceLocations != null && addPrinterViewContext.ServiceLocations.Count > 0 &&
                addPrinterViewContext.SelectedServiceLocation != null;
            loadPrinters.Wait(TimeSpan.FromSeconds(180));
            Assert.IsNotNull(addPrinterViewContext.Printers);
            Assert.IsTrue(addPrinterViewContext.Printers.Count > 1);
            Assert.IsNotNull(addPrinterViewContext.ServiceLocations);
            Assert.IsTrue(addPrinterViewContext.ServiceLocations.Count > 0);
            Assert.IsNotNull(addPrinterViewContext.SelectedServiceLocation);

            if (addPrinterViewContext.SelectedServiceLocation.Id > 0)
            {
                Assert.IsTrue(addPrinterViewContext.SelectedServiceLocation.Name == UserContext.Current.ServiceLocation.Item2);

                const string ioPrinterName = "Test Printer";
                addPrinterViewContext.IoPrinterName = ioPrinterName;
                Assert.IsNotNull(addPrinterViewContext.IoPrinterName);

                //Testing ServiceLocationChange command
                Func<bool> isValidDataSelected =
                    () =>
                    addPrinterViewContext.UncPath != null &&
                    addPrinterViewContext.SelectedServiceLocation != null &&
                    addPrinterViewContext.IoPrinterName != null && addPrinterViewContext.IsValid();
                addPrinterViewContext.SelectedPrinter = addPrinterViewContext.Printers.FirstOrDefault();

                isValidDataSelected.Wait(TimeSpan.FromSeconds(10));
                Assert.IsTrue(isValidDataSelected());
                Assert.IsNotNull(addPrinterViewContext.UncPath);
                Assert.IsNotNull(addPrinterViewContext.IoPrinterName);
                Assert.IsNotNull(addPrinterViewContext.SelectedServiceLocation);
                Assert.IsTrue(addPrinterViewContext.IsValid());

                Func<bool> isPrinterSaved =
                    () => addPrinterViewContext.SavedPrinters.Any();
                addPrinterViewContext.Apply.Execute(true);
                isPrinterSaved.Wait(TimeSpan.FromSeconds(40));
                Assert.IsTrue(isPrinterSaved());

               //Testing Refresh IO Printers Command
                documentPrinterViewContext.Load.Execute(null);
                Func<bool> hasReloadedPrinters = () => documentPrinterViewContext.IoPrinters.First().Name == "Remove associated printer" && documentPrinterViewContext.IoPrinters.Count == 2;
                Assert.IsTrue(hasReloadedPrinters.Wait(TimeSpan.FromSeconds(5)));

                //Testing Change DocumentType command
                documentPrinterViewContext.SelectedDocumentType =
                    documentPrinterViewContext.DocumentTypes.FirstOrDefault(d => d.Id == 1);
                Assert.IsNotNull(documentPrinterViewContext.SelectedDocumentType);
                Func<bool> loadTemplates = () => documentPrinterViewContext.Documents.Count > 0;
                loadTemplates.Wait(TimeSpan.FromSeconds(30));
                Assert.IsTrue(loadTemplates());
                Assert.IsTrue(documentPrinterViewContext.Documents.Count > 1);
                Assert.IsTrue(documentPrinterViewContext.Documents.First().Name == "Apply to All Templates");
                Assert.IsFalse(documentPrinterViewContext.IsPrinterOptionsEnabled);

                //Select Printer from IO printers list command
                documentPrinterViewContext.SelectedIoPrinter =
                    documentPrinterViewContext.IoPrinters.FirstOrDefault(i => i.Name == ioPrinterName);
                documentPrinterViewContext.IsPrinterOptionsEnabled = true;

                ////Testing Select document command
                documentPrinterViewContext.SelectedDocument =
                    documentPrinterViewContext.Documents.FirstOrDefault(t => t.Id > 0);

                Assert.IsNotNull(documentPrinterViewContext.SelectedDocumentType);
                Assert.IsNotNull(documentPrinterViewContext.SelectedDocument);
                Assert.IsTrue(documentPrinterViewContext.IsPrinterOptionsEnabled);
                Assert.IsNotNull(documentPrinterViewContext.SelectedIoPrinter);

                Func<bool> isDocumentPrinterSaved =
                    () => documentPrinterViewContext.SelectedDocumentType == null && documentPrinterViewContext.SelectedDocument ==null &&  documentPrinterViewContext.SelectedIoPrinter ==null;
                documentPrinterViewContext.Apply.Execute(true);
                isDocumentPrinterSaved.Wait(TimeSpan.FromSeconds(40));
                Assert.IsTrue(isDocumentPrinterSaved());

                //Assert.IsTrue(isDocumentPrinterSaved.Wait(TimeSpan.FromSeconds(40)));

                Assert.IsTrue(documentPrinterViewContext.IsPrinterOptionsEnabled);

                //Loading all documents which have printers
                Assert.IsTrue(documentSummaryViewContext.DocumentPrinters.Count == 0);
                documentSummaryViewContext.Load.Execute();
                Assert.IsTrue(new Func<bool>(() => documentSummaryViewContext.DocumentPrinters.Count == 1).Wait(TimeSpan.FromSeconds(5)));
                DocumentPrinterViewModel documentPrinter = documentSummaryViewContext.DocumentPrinters.FirstOrDefault();
                Assert.IsTrue(documentPrinter != null && documentPrinter.PrinterName == ioPrinterName);
            }
        }
    }
}
