﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.RescheduleAppointment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using Soaf;
using Soaf.Linq;
using Soaf.Threading;

namespace IO.Practiceware.Tests.Presentation
{
    [TestClass]
    public class RescheduleAppointmentTests : TestBase
    {
        protected override void OnAfterTestInitialize()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ModelTestsResources.ModelTestsMetadata);
        }


        [TestMethod]
        public void TestLoadAppointmentRescheduleViewContextByPatientId()
        {
            var appointment = CreateAndSaveAppointment();

            var rescheduleViewContext = Common.ServiceProvider.GetService<RescheduleAppointmentViewContext>();
            rescheduleViewContext.LoadArguments = new RescheduleAppointmentLoadArguments
            {
                AppointmentIds = new List<int> { appointment.Id }
            };

            // Load UI data
            rescheduleViewContext.Load.Execute(rescheduleViewContext.InteractionContext);
            Func<bool> isLoaded = () => rescheduleViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(1500));

            Assert.IsNotNull(rescheduleViewContext.SingleAppointmentDetail);
            Assert.IsNotNull(rescheduleViewContext.Appointments);
            Assert.IsTrue(rescheduleViewContext.Appointments.Any());
            Assert.AreEqual(appointment.Encounter.Patient.DisplayName, rescheduleViewContext.Appointments.First().PatientName);
            Assert.IsTrue(rescheduleViewContext.HasOnlySingleAppointment);
        }


        [TestMethod]
        public void TestViewContextSave()
        {
            var appointment = CreateAndSaveAppointment();

            var rescheduleViewContext = Common.ServiceProvider.GetService<RescheduleAppointmentViewContext>();
            rescheduleViewContext.LoadArguments = new RescheduleAppointmentLoadArguments
            {
                AppointmentIds = new List<int> { appointment.Id }
            };

            // Load UI data
            rescheduleViewContext.Load.Execute(rescheduleViewContext.InteractionContext);
            Func<bool> isLoaded = () => rescheduleViewContext.Load.CanExecute(null);
            isLoaded.Wait(TimeSpan.FromSeconds(1500));


            var rescheduleTypeId = rescheduleViewContext.ListOfRescheduledTypes.First().Id;
            const string comment = "my test comment 1234";

            // set the selected type and reason
            rescheduleViewContext.SelectedRescheduleType = rescheduleViewContext.ListOfRescheduledTypes.First();
            rescheduleViewContext.Comment = comment;
            var appDateTime = new DateTime(2014, 2, 17, 6, 27, 0);
            rescheduleViewContext.SelectedBlock = new RescheduleAppointmentViewModel
                                                      {
                                                          AppointmentTypeId = 1,
                                                          StartDateTime = appDateTime,
                                                          LocationId = 1,
                                                          ResourceId = 1,
                                                          ScheduleBlockCategoryId = Guid.Empty,
                                                      };

            rescheduleViewContext.RescheduleAppointments.Execute(rescheduleViewContext.InteractionContext);
            Func<bool> isSaved = () => rescheduleViewContext.RescheduleAppointments.CanExecute(null);
            isSaved.Wait(TimeSpan.FromSeconds(1500));

            // get appointment and its details
            var newApp = PracticeRepository.Appointments.OfType<UserAppointment>()
               .Include(x => x.User)
               .Include(x => x.Encounter.Patient)
               .Single(x => x.Id == appointment.Id);

            Assert.AreEqual(rescheduleTypeId, newApp.Encounter.EncounterStatusId);
            Assert.AreEqual(appDateTime, newApp.Encounter.StartDateTime);
            Assert.AreEqual(appDateTime, newApp.DateTime);
        }

        private UserAppointment CreateAndSaveAppointment()
        {
            var app = Common.CreateAppointment();
            app.Encounter.EncounterStatusId = (int)EncounterStatus.Pending;

            Common.PracticeRepository.Save(app);

            var newApp = PracticeRepository.Appointments.OfType<UserAppointment>()
                .Include(x => x.User)
                .Include(x => x.Encounter.Patient)
                .Single(x => x.Id == app.Id);


            return newApp;
        }


    }
}
