﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Appointment = IO.Practiceware.Model.Appointment;
using AppointmentType = IO.Practiceware.Model.AppointmentType;


namespace IO.Practiceware.Tests
{
    [TestClass]
    public class ModelTests : TestBase
    {

        [TestMethod]
        public void TestSpectaclePrescriptionParsing()
        {
            var pc = new PatientClinical
                {
                    ImageDescriptor = "^OD:every day glassesOS:",
                    FindingDetail = "DISPENSE SPECTACLE RX-9/-1/a type of glasses-2/+4.25 -0.75 X78 PRISM OF ANGLE 1:3 BI PRISM OF ANGLE 2:1 BU-3/+9.00 -4.25 X180 PRISM OF ANGLE 1:6 BO PRISM OF ANGLE 2:4 BD-4/-5/-6/-7/"
                };

            var prescription = new EncounterLensesPrescription();
            prescription.GetType().GetProperty("FindingDetail", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(prescription, pc.FindingDetail, null);
            prescription.GetType().GetProperty("ImageDescriptor", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(prescription, pc.ImageDescriptor, null);

            var spectaclePrescription = prescription.LensesPrescription as SpectaclePrescription;
            Assert.IsNotNull(spectaclePrescription);

            Assert.IsTrue(spectaclePrescription.Comment == "every day glasses");
            Assert.IsTrue(spectaclePrescription.GlassesType == "a type of glasses");

            Assert.IsTrue(spectaclePrescription.ODPrescription.Axis == "X78");
            Assert.IsTrue(spectaclePrescription.ODPrescription.Cylinder == "-0.75");
            Assert.IsTrue(spectaclePrescription.ODPrescription.Intermediate == null);
            Assert.IsTrue(spectaclePrescription.ODPrescription.PrismOfAngle1 == "3 BI");
            Assert.IsTrue(spectaclePrescription.ODPrescription.PrismOfAngle2 == "1 BU");
            Assert.IsTrue(spectaclePrescription.ODPrescription.Reading == null);
            Assert.IsTrue(spectaclePrescription.ODPrescription.Sphere == "+4.25");
            Assert.IsTrue(spectaclePrescription.ODPrescription.VertexDistance == null);

            Assert.IsTrue(spectaclePrescription.OSPrescription.Axis == "X180");
            Assert.IsTrue(spectaclePrescription.OSPrescription.Cylinder == "-4.25");
            Assert.IsTrue(spectaclePrescription.OSPrescription.Intermediate == null);
            Assert.IsTrue(spectaclePrescription.OSPrescription.PrismOfAngle1 == "6 BO");
            Assert.IsTrue(spectaclePrescription.OSPrescription.PrismOfAngle2 == "4 BD");
            Assert.IsTrue(spectaclePrescription.OSPrescription.Reading == null);
            Assert.IsTrue(spectaclePrescription.OSPrescription.Sphere == "+9.00");
            Assert.IsTrue(spectaclePrescription.OSPrescription.VertexDistance == null);
        }

        [TestMethod]
        public void TestContactLensesPrescriptionParsing()
        {
            var pc = new PatientClinical
                {
                    ImageDescriptor = "^OD:every day lensesOS:",
                    FindingDetail = "DISPENSE CL RX-9/-1/-2/+5.25 -2.50 X25 ADD-READING:+1.00 BASE CURVE:8.3 DIAMETER:8 PERIPH CURVE:SKIRT D OR N:D-3/+0.75 -0.75 X123 ADD-READING:+1.00 BASE CURVE:8.3 DIAMETER:8 PERIPH CURVE:SKIRT-4/-5/149-6/149-7/"
                };
            var prescription = new EncounterLensesPrescription();
            prescription.GetType().GetProperty("FindingDetail", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(prescription, pc.FindingDetail, null);
            prescription.GetType().GetProperty("ImageDescriptor", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(prescription, pc.ImageDescriptor, null);

            var contactLensesPrescription = prescription.LensesPrescription as ContactLensesPrescription;
            Assert.IsNotNull(contactLensesPrescription);

            Assert.IsTrue(contactLensesPrescription.ODPrescription.Axis == "X25");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.BaseCurve == "8.3");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Comment == "every day lenses");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Cylinder == "-2.50");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Diameter == "8");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Dominant.GetValueOrDefault());
            Assert.IsTrue(contactLensesPrescription.ODPrescription.InventoryId == 149);
            Assert.IsTrue(contactLensesPrescription.ODPrescription.PeripheralCurve == "SKIRT");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Reading == "+1.00");
            Assert.IsTrue(contactLensesPrescription.ODPrescription.Sphere == "+5.25");

            Assert.IsTrue(contactLensesPrescription.OSPrescription.Axis == "X123");
            Assert.IsTrue(contactLensesPrescription.OSPrescription.BaseCurve == "8.3");
            Assert.IsTrue(string.IsNullOrEmpty(contactLensesPrescription.OSPrescription.Comment));
            Assert.IsTrue(contactLensesPrescription.OSPrescription.Cylinder == "-0.75");
            Assert.IsTrue(contactLensesPrescription.OSPrescription.Diameter == "8");
            Assert.IsTrue(!contactLensesPrescription.OSPrescription.Dominant.HasValue);
            Assert.IsTrue(contactLensesPrescription.OSPrescription.InventoryId == 149);
            Assert.IsTrue(contactLensesPrescription.OSPrescription.PeripheralCurve == "SKIRT");
            Assert.IsTrue(contactLensesPrescription.OSPrescription.Reading == "+1.00");
            Assert.IsTrue(contactLensesPrescription.OSPrescription.Sphere == "+0.75");
        }

        /// <summary>
        ///   Tests the saving and retrieval of a patient.
        /// </summary>
        [TestMethod]
        public void TestPatient()
        {
            // Use the comparer to help compare field values. It will compare all non-child properties for us to assert equality.
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            // Create patient and save
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);


            // Refetch patient and compare
            Patient patientRefetched = Common.PracticeRepository.Patients.WithId(patient.Id);
            Assert.IsNotNull(patientRefetched);
            bool areSame = comparison.Compare(patient, patientRefetched);
            Assert.IsTrue(areSame);

            // Update patient and save
            patientRefetched.FirstName = "NewFirstName";
            patientRefetched.LastName = "NewLastName";
            patientRefetched.MiddleName = "N";
            patientRefetched.SocialSecurityNumber = "333-45-6789";
            patient.IsClinical = false;
            patientRefetched.Suffix = "A";
            patient.MaritalStatus = MaritalStatus.Married;
            Common.PracticeRepository.Save(patientRefetched);

            Patient patientUpdated = Common.PracticeRepository.Patients.WithId(patient.Id);
            Assert.IsNotNull(patientUpdated);
            areSame = comparison.Compare(patientRefetched, patientUpdated);
            Assert.IsTrue(areSame);

            // Delete
            patientUpdated.PatientAddresses.ToList().ForEach(Common.PracticeRepository.Delete);
            patientUpdated.PatientEmailAddresses.ToList().ForEach(Common.PracticeRepository.Delete);
            Common.PracticeRepository.Delete(patientUpdated);
            Patient patientDeleted = Common.PracticeRepository.Patients.WithId(patient.Id);
            Assert.IsNull(patientDeleted);
        }


        /// <summary>
        ///   Tests the saving and retrieval of a Insurance Policy.
        /// </summary>
        [TestMethod]
        public void TestInsurancePolicy()
        {
            // Use the comparer to help compare field values. It will compare all non-child properties for us to assert equality.
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            // Create patient and save
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);

            // Create Insurance Policy and save
            var insurancePolicy = new InsurancePolicy();
            insurancePolicy.PolicyholderPatientId = patient.Id;
            insurancePolicy.Copay = 15;
            insurancePolicy.StartDateTime = DateTime.Now.Date;
            insurancePolicy.PolicyCode = "12345";
            insurancePolicy.Deductible = 0;
            insurancePolicy.Insurer = Common.PracticeRepository.Insurers.First();

            Common.PracticeRepository.Save(insurancePolicy);

            Assert.IsTrue(insurancePolicy.Id > 0);

            // Refetch Insurance Policy and compare
            InsurancePolicy insuranceRefetched = Common.PracticeRepository.InsurancePolicies.WithId(insurancePolicy.Id);
            Assert.IsNotNull(insuranceRefetched);
            bool areSame = comparison.Compare(insurancePolicy, insuranceRefetched);
            Assert.IsTrue(areSame);

            // Update Insurance Policy and save
            insuranceRefetched.PolicyCode = "54321";
            insuranceRefetched.StartDateTime = DateTime.Now.Date.AddDays(1);
            insuranceRefetched.PolicyholderPatient = patient;

            Common.PracticeRepository.Save(insuranceRefetched);

            InsurancePolicy insuranceUpdated = Common.PracticeRepository.InsurancePolicies.WithId(insurancePolicy.Id);
            Assert.IsNotNull(insuranceUpdated);
            areSame = comparison.Compare(insuranceRefetched, insuranceUpdated);
            Assert.IsTrue(areSame);

            // Delete
            Common.PracticeRepository.Delete(insuranceUpdated);
            InsurancePolicy insuranceDeleted = Common.PracticeRepository.InsurancePolicies.WithId(insuranceUpdated.Id);
            Assert.IsNull(insuranceDeleted);
        }


        /// <summary>
        ///   Tests the saving and retrieval of a Patient Insurance.
        /// </summary>
        [TestMethod]
        public void TestPatientInsurance()
        {
            // Use the comparer to help compare field values. It will compare all non-child properties for us to assert equality.
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100,
                    ElementsToIgnore = new[] { "OrdinalId" }.ToList()
                };

            // Create patient and save
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);

            Patient patient2 = Common.CreatePatient2();
            Common.PracticeRepository.Save(patient2);
            Assert.IsTrue(patient2.Id > 0);

            var legacyPracticeRepository = Common.ServiceProvider.GetService<ILegacyPracticeRepository>();
            CreatePracticeCodeData(legacyPracticeRepository);

            //Create Insurer
            Insurer insurer = Common.CreateInsurer();
            insurer.ClaimFileReceiverId = Common.PracticeRepository.ClaimFileReceivers.First().Id;
            insurer.InsurerBusinessClassId = (int)InsurerBusinessClassId.BlueCrossBlueShield;
            insurer.InsurerPayTypeId = (int)InsurerPayType.IndividualPolicy;
            insurer.InsurerPlanTypeId = Common.PracticeRepository.InsurerPlanTypes.First().Id;
            Assert.IsNotNull(insurer);
            Common.PracticeRepository.Save(insurer);

            // Create Insurance Policy and save
            var insurancePolicy = new InsurancePolicy();
            insurancePolicy.PolicyholderPatientId = patient.Id;
            insurancePolicy.InsurerId = insurer.Id;
            insurancePolicy.StartDateTime = DateTime.Now.Date;
            insurancePolicy.PolicyCode = "12345";
            insurancePolicy.Deductible = 0;
            Common.PracticeRepository.Save(insurancePolicy);
            Assert.IsTrue(insurancePolicy.Id > 0);

            // Refetch Insurance Policy and compare
            InsurancePolicy insuranceRefetched = Common.PracticeRepository.InsurancePolicies.WithId(insurancePolicy.Id);
            Assert.IsNotNull(insuranceRefetched);
            bool areSame = comparison.Compare(insurancePolicy, insuranceRefetched);
            Assert.IsTrue(areSame);

            // Update Insurance Policy and save
            insuranceRefetched.PolicyCode = "54321";
            insuranceRefetched.StartDateTime = DateTime.Now.Date.AddDays(1);
            insuranceRefetched.PolicyholderPatient = patient;
            // TODO: Update more properties
            Common.PracticeRepository.Save(insuranceRefetched);

            InsurancePolicy insuranceUpdated = Common.PracticeRepository.InsurancePolicies.WithId(insurancePolicy.Id);
            Assert.IsNotNull(insuranceUpdated);
            areSame = comparison.Compare(insuranceRefetched, insuranceUpdated);
            Assert.IsTrue(areSame);

            //Policy Holder1 = Patient
            var patientInsurance = new PatientInsurance();
            patientInsurance.InsuredPatientId = patient.Id;
            patientInsurance.InsurancePolicyId = insurancePolicy.Id;
            patientInsurance.InsuranceTypeId = 1;
            patientInsurance.OrdinalId = 3001;
            patientInsurance.PolicyholderRelationshipTypeId = 1;
            patientInsurance.EndDateTime = DateTime.Now.Date.AddYears(2).AddDays(1);

            Common.PracticeRepository.Save(patientInsurance);

            Assert.IsTrue(patientInsurance.Id > 0);

            // Refetch Insurance Policy and compare
            PatientInsurance patInsuranceRefetched = Common.PracticeRepository.PatientInsurances.WithId(patientInsurance.Id);
            patInsuranceRefetched.EndDateTime = DateTime.Now.Date.AddYears(2).AddDays(1);
            Assert.IsNotNull(patInsuranceRefetched);
            areSame = comparison.Compare(patientInsurance, patInsuranceRefetched);
            Assert.IsTrue(areSame);

            // Create Insurance Policy2 for patient2 and save
            var insurancePolicy2 = new InsurancePolicy();
            insurancePolicy2.PolicyholderPatientId = patient2.Id;
            insurancePolicy2.InsurerId = insurer.Id;
            insurancePolicy2.StartDateTime = DateTime.Now.Date.AddDays(1);
            insurancePolicy2.PolicyCode = "010203";
            insurancePolicy2.Deductible = 0;
            Common.PracticeRepository.Save(insurancePolicy2);
            Assert.IsTrue(insurancePolicy2.Id > 0);

            //Policy Holder1 = Patient2
            var patient2Insurance = new PatientInsurance();
            patient2Insurance.InsuredPatientId = patient2.Id;
            patient2Insurance.InsurancePolicyId = insurancePolicy2.Id;
            patient2Insurance.InsuranceTypeId = 1;
            patient2Insurance.OrdinalId = 3001;
            patient2Insurance.PolicyholderRelationshipTypeId = 1;

            Common.PracticeRepository.Save(patient2Insurance);

            Assert.IsTrue(patient2Insurance.Id > 0);

            // Refetch Insurance Policy and compare
            PatientInsurance pat2InsuranceRefetched = Common.PracticeRepository.PatientInsurances.WithId(patient2Insurance.Id);
            Assert.IsNotNull(pat2InsuranceRefetched);
            areSame = comparison.Compare(patient2Insurance, pat2InsuranceRefetched);
            Assert.IsTrue(areSame);


            //Policy Holder2 = Patient
            var patientSecInsurance = new PatientInsurance();
            patientSecInsurance.InsuredPatientId = patient.Id;
            patientSecInsurance.InsurancePolicyId = insurancePolicy2.Id;
            patientSecInsurance.InsuranceTypeId = 1;
            patientSecInsurance.OrdinalId = 3001;
            patientSecInsurance.PolicyholderRelationshipTypeId = 2;

            Common.PracticeRepository.Save(patientSecInsurance);

            Assert.IsTrue(patientSecInsurance.Id > 0);

            // Refetch Patient Insurances compare
            PatientInsurance patSecInsuranceRefetched = Common.PracticeRepository.PatientInsurances.WithId(patientSecInsurance.Id);
            Assert.IsNotNull(patSecInsuranceRefetched);
            areSame = comparison.Compare(patientSecInsurance, patSecInsuranceRefetched);
            Assert.IsTrue(areSame);

            // Fetch Patient1 Insurances and check count
            IQueryable<PatientInsurance> pat1Insurances = Common.PracticeRepository.PatientInsurances.Where(p => p.InsuredPatientId == patient.Id);
            Assert.IsNotNull(pat1Insurances);
            Assert.IsTrue(pat1Insurances.Count() == 2);

            // Fetch Patient2 Insurances and check count
            IQueryable<PatientInsurance> pat2Insurances = Common.PracticeRepository.PatientInsurances.Where(p => p.InsuredPatientId == patient2.Id);
            Assert.IsNotNull(pat2Insurances);
            Assert.IsTrue(pat2Insurances.Count() == 1);
        }

        /// <summary>
        ///   Tests saving of email addresses
        /// </summary>
        [TestMethod]
        public void TestEmailAddress()
        {
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Update patient email address

            //Create new patient
            var patient = new Patient();
            patient.FirstName = "TestPatient";
            patient.LastName = "PatientEmail";
            patient.DateOfBirth = DateTime.Today;
            Common.PracticeRepository.Save(patient);

            //Give patient email address
            var patientEmailAddress = new PatientEmailAddress();
            patientEmailAddress.Value = "testEmailPatient@test.com";
            patientEmailAddress.PatientId = patient.Id;
            patientEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(patientEmailAddress);

            //Check to see if same
            PatientEmailAddress patientEmailAddressRefetched = Common.PracticeRepository.PatientEmailAddresses.WithId(patientEmailAddress.Id);
            bool areSame = comparison.Compare(patientEmailAddress, patientEmailAddressRefetched);
            Assert.IsTrue(areSame);

            //Update email address for users
            //Create new user
            var user = new User();
            user.FirstName = "Test";
            user.LastName = "UserEmail";
            Common.PracticeRepository.Save(user);

            //Give user email address
            var userEmailAddress = new EmailAddress();
            userEmailAddress.Value = "testEmail@Test.com";
            userEmailAddress.UserId = user.Id;
            userEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(userEmailAddress);

            //Check to see if same
            EmailAddress useremailAddressRefetched = Common.PracticeRepository.EmailAddresses.WithId(userEmailAddress.Id);
            areSame = comparison.Compare(userEmailAddress, useremailAddressRefetched);
            Assert.IsTrue(areSame);

            //Update email address for Billing Organization
            BillingOrganization billingOrganization = Common.CreateBillingOrganization();
            Common.PracticeRepository.Save(billingOrganization);

            //Give Billing Organization email address
            var billingEmailAddress = new EmailAddress();
            billingEmailAddress.Value = "billingOrganizationEmail@Test.com";
            billingEmailAddress.BillingOrganizationId = billingOrganization.Id;
            billingEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(billingEmailAddress);

            //Check to see if same
            EmailAddress billingEmailAddressRefetched = Common.PracticeRepository.EmailAddresses.WithId(billingEmailAddress.Id);
            areSame = comparison.Compare(billingEmailAddress, billingEmailAddressRefetched);
            Assert.IsTrue(areSame);

            //Update email address for Billing Organization
            ServiceLocation serviceLocation = Common.CreateServiceLocation();
            serviceLocation.ShortName = "new office";
            Common.PracticeRepository.Save(serviceLocation);

            //Give Billing Organization email address
            var serviceLocationEmailAddress = new EmailAddress();
            serviceLocationEmailAddress.Value = "serviceLocationEmail@Test.com";
            serviceLocationEmailAddress.ServiceLocationId = serviceLocation.Id;
            serviceLocationEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(serviceLocationEmailAddress);

            //Check to see if same
            EmailAddress serviceLocationEmailAddressRefetched = Common.PracticeRepository.EmailAddresses.WithId(serviceLocationEmailAddress.Id);
            areSame = comparison.Compare(serviceLocationEmailAddress, serviceLocationEmailAddressRefetched);
            Assert.IsTrue(areSame);

            //Create contact ExternalProvider
            ExternalProvider externalProvider = Common.CreateExternalProvider();
            Common.PracticeRepository.Save(externalProvider);

            //'Give contact ExternalProvider email address
            var externalProviderEmailAddress = new ExternalContactEmailAddress();
            externalProviderEmailAddress.Value = "contactrEmailPatient@test.com";
            externalProviderEmailAddress.ExternalContactId = externalProvider.Id;
            externalProviderEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(externalProviderEmailAddress);

            //Check to see if same
            ExternalContactEmailAddress externalProviderEmailAddressRefetched = Common.PracticeRepository.ExternalContactEmailAddresses.WithId(externalProviderEmailAddress.Id);
            areSame = comparison.Compare(externalProviderEmailAddress, externalProviderEmailAddressRefetched);
            Assert.IsTrue(areSame);


            //Create ExternalOrganization
            ExternalOrganization externalOrganization = Common.CreateExternalOrganization();
            Common.PracticeRepository.Save(externalOrganization);
            Assert.IsTrue(externalOrganization.Id > 0);

            //'Give ExternalOrganization email address
            var externalOrganizationEmailAddress = new ExternalOrganizationEmailAddress();
            externalOrganizationEmailAddress.Value = "contactrEmailPatient@test.com";
            externalOrganizationEmailAddress.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationEmailAddress.EmailAddressType = EmailAddressType.Personal;
            Common.PracticeRepository.Save(externalOrganizationEmailAddress);

            //Check to see if same
            ExternalOrganizationEmailAddress externalOrganizationEmailAddressRefetched = Common.PracticeRepository.ExternalOrganizationEmailAddresses.WithId(externalOrganizationEmailAddress.Id);
            areSame = comparison.Compare(externalOrganizationEmailAddress, externalOrganizationEmailAddressRefetched);
            Assert.IsTrue(areSame);
        }

        /// <summary>
        ///   Tests inserting and updating users
        /// </summary>
        [TestMethod]
        public void TestUser()
        {
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create new user and save
            User user = Common.CreateUser();
            Common.PracticeRepository.Save(user);
            Assert.IsTrue(user.Id > 0);

            // Refetch user and compare
            User userRefetched = Common.PracticeRepository.Users.Include(u => u.Roles).WithId(user.Id);
            Assert.IsNotNull(userRefetched);
            Assert.IsInstanceOfType(userRefetched, typeof(Doctor));
            comparison.Compare(user, userRefetched);
            // Only difference should be that refetched is a Doctor, but original was a User
            Assert.IsTrue(comparison.Differences.Count == 0);

            // Update user and save
            var emailAddress = new EmailAddress();
            emailAddress.Value = "test@test.com";
            emailAddress.UserId = userRefetched.Id;
            Common.PracticeRepository.Save(emailAddress);

            userRefetched.FirstName = "UnitLastName";
            userRefetched.LastName = "UnitLastName";
            userRefetched.MiddleName = "U";
            userRefetched.Color = Color.Aqua;
            userRefetched.DisplayName = "Dr. Unit Test for User";
            userRefetched.Honorific = "Doctor";
            userRefetched.IsArchived = false;
            userRefetched.IsLoggedIn = false;
            userRefetched.IsSchedulable = true;
            userRefetched.Pid = "9876";
            userRefetched.Prefix = "the first";
            userRefetched.UserName = "9876";
            Common.PracticeRepository.Save(userRefetched);

            User userUpdated = Common.PracticeRepository.Users.WithId(user.Id);
            Assert.IsNotNull(userUpdated);
            bool areSame = comparison.Compare(userRefetched, userUpdated);
            Assert.IsTrue(areSame);
        }

        /// <summary>
        ///   Tests Encounter is Updating/Inserting correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestEncounter()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create Data
            Encounter encounter = Common.CreateAppointment().Encounter;
            //Save Encounter
            practiceRepository.Save(encounter);
            Assert.IsTrue(encounter.Id > 0);

            //Getting back encounter details
            Encounter encounterRefechted = practiceRepository.Encounters.WithId(encounter.Id);
            Assert.IsNotNull(encounterRefechted);
            bool areSame = comparison.Compare(encounter, encounterRefechted);
            Assert.IsTrue(areSame);

            //Update Encounter
            encounterRefechted.EncounterStatusId = (int)EncounterStatus.CancelNoShow;
            practiceRepository.Save(encounterRefechted);
            Encounter encounterUpdated = practiceRepository.Encounters.WithId(encounterRefechted.Id);
            areSame = comparison.Compare(encounterRefechted, encounterUpdated);
            Assert.IsTrue(areSame);

            //Delete Encounter
            practiceRepository.Delete(encounterRefechted);
            Encounter encounterDeleted = practiceRepository.Encounters.WithId(encounterRefechted.Id);
            Assert.IsNull(encounterDeleted);
        }

        /// <summary>
        ///   Test Patient Ethnicity is Inserting/updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestPatientEthnicity()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };
            Patient patient = Common.CreatePatient();
            Assert.IsNotNull(patient);
            practiceRepository.Save(patient);

            //Create Patient Ethnicity
            Ethnicity patientEthnicity = Common.CreatePatientEthnicity();
            Assert.IsNotNull(patientEthnicity);

            //Save Patient Ethnicity
            practiceRepository.Save(patientEthnicity);
            Ethnicity patientEthnicityRefetched = practiceRepository.Ethnicities.WithId(patientEthnicity.Id);
            Assert.IsNotNull(patientEthnicityRefetched);
            bool areSame = comparison.Compare(patientEthnicity, patientEthnicityRefetched);
            Assert.IsTrue(areSame);

            //Link Patient with Ethnicity and save
            patient.EthnicityId = patientEthnicityRefetched.Id;
            patient.Ethnicity = patientEthnicityRefetched;
            practiceRepository.Save(patient);
            Patient patientRefetched = practiceRepository.Patients.WithId(patient.Id);
            Assert.IsNotNull(patientRefetched);
            areSame = comparison.Compare(patient, patientRefetched);
            Assert.IsTrue(areSame);

            //Update Ethnicity
            patientEthnicityRefetched.Name = "NewTestEthnicity";
            practiceRepository.Save(patientEthnicityRefetched);
            Ethnicity patientEthnicityUpdated = practiceRepository.Ethnicities.Include(p => p.Patients).WithId(patientEthnicityRefetched.Id);
            areSame = comparison.Compare(patientEthnicityRefetched, patientEthnicityUpdated);
            Assert.IsTrue(areSame);


            //Delete Ethnicity
            practiceRepository.Delete(patientEthnicityUpdated);
            Ethnicity patientEthnicityDeleted = practiceRepository.Ethnicities.WithId(patientEthnicityUpdated.Id);
            Assert.IsNull(patientEthnicityDeleted);
        }

        /// <summary>
        ///   Test's Patient Language is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestPatientLanguage()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            Patient patient = Common.CreatePatient();
            Assert.IsNotNull(patient);
            practiceRepository.Save(patient);

            //Create Patient Language
            Language patientLanguage = Common.CreatePatientLanguage();
            Assert.IsNotNull(patientLanguage);

            //Save Patient Language
            practiceRepository.Save(patientLanguage);
            Language patientLanguageRefetched = practiceRepository.Languages.WithId(patientLanguage.Id);
            Assert.IsNotNull(patientLanguageRefetched);
            bool aresame = comparsion.Compare(patientLanguage, patientLanguageRefetched);
            Assert.IsTrue(aresame);

            //Link Patient with Language and save
            patient.LanguageId = patientLanguageRefetched.Id;
            patient.Language = patientLanguageRefetched;
            practiceRepository.Save(patient);
            Patient patientRefetched = practiceRepository.Patients.WithId(patient.Id);
            Assert.IsNotNull(patientRefetched);
            aresame = comparsion.Compare(patient, patientRefetched);
            Assert.IsTrue(aresame);

            //Update Language
            patientLanguageRefetched.Name = "NewLang";
            practiceRepository.Save(patientLanguageRefetched);
            Language patientLanguageUpdated = practiceRepository.Languages.Include(p => p.Patients).WithId(patientLanguageRefetched.Id);
            Assert.IsNotNull(patientLanguageUpdated);
            aresame = comparsion.Compare(patientLanguageRefetched, patientLanguageUpdated);
            Assert.IsTrue(aresame);

            //Delete Language
            practiceRepository.Delete(patientLanguageUpdated);
            Language patientLanguageDeleted = practiceRepository.Languages.WithId(patientLanguageUpdated.Id);
            Assert.IsNull(patientLanguageDeleted);
        }


        /// <summary>
        ///   Test's Patient Race is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestPatientRace()
        {

            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var unitOfWorkProvider = Common.ServiceProvider.GetService<IUnitOfWorkProvider>();
            using (var work = unitOfWorkProvider.Create())
            {
                var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };
                Patient patient = Common.CreatePatient();
                Assert.IsNotNull(patient);
                practiceRepository.Save(patient);

                //Create Patient Race
                Race patientRace = Common.CreatePatientRace();
                Assert.IsNotNull(patientRace);

                practiceRepository.Save(patientRace);
                //Save Patient Race
                work.AcceptChanges();

                Race patientRaceRefetched = practiceRepository.Races.WithId(patientRace.Id);
                Assert.IsNotNull(patientRaceRefetched);
                bool areSame = comparsion.Compare(patientRace, patientRaceRefetched);
                Assert.IsTrue(areSame);

                //Link Patient with Race and save
                patient.Races = new List<Race> { patientRaceRefetched };
                practiceRepository.Save(patient);
                work.AcceptChanges();
                Patient patientRefetched = practiceRepository.Patients.WithId(patient.Id);
                Assert.IsNotNull(patientRefetched);
                areSame = comparsion.Compare(patient, patientRefetched);
                Assert.IsTrue(areSame);

                //Update Race
                patientRaceRefetched.Name = "NewTestRace";
                practiceRepository.Save(patientRaceRefetched);
                work.AcceptChanges();
                Race patientRaceUpdated = practiceRepository.Races.Include(p => p.Patients).WithId(patientRaceRefetched.Id);
                Assert.IsNotNull(patientRaceUpdated);
                areSame = comparsion.Compare(patientRaceRefetched, patientRaceUpdated);
                Assert.IsTrue(areSame);

                //Delete Race
                practiceRepository.Delete(patientRaceUpdated);
                work.AcceptChanges();
                Race patientRaceDeleted = practiceRepository.Races.WithId(patientRaceUpdated.Id);
                Assert.IsNull(patientRaceDeleted);
            }

        }

        /// <summary>
        ///   Tests the saving and retrieval of a Appointment.
        /// </summary>
        [TestMethod]
        public void TestAppointments()
        {
            //Create Patient
            Patient patient = Common.CreatePatient();
            Common.PracticeRepository.Save(patient);
            Assert.IsTrue(patient.Id > 0);


            //Save Appointment
            UserAppointment appointment = Common.CreateAppointment();
            appointment.AppointmentType = Common.PracticeRepository.AppointmentTypes.First();
            Common.PracticeRepository.Save(appointment);
            Assert.IsTrue(appointment.Id > 0);


            //Getting back Appointment details
            Appointment appointmentfetched = Common.PracticeRepository.Appointments.WithId(appointment.Id);
            Assert.IsNotNull(appointmentfetched);


            // Update Appointment 
            appointmentfetched.DateTime = DateTime.Now;
            appointmentfetched.AppointmentTypeId = 1;
            Common.PracticeRepository.Save(appointmentfetched);
            Appointment appointmentUpdated = Common.PracticeRepository.Appointments.WithId(appointment.Id);
            Assert.IsNotNull(appointmentUpdated);


            // Delete Appointment
            Common.PracticeRepository.Delete(appointmentUpdated);
            Appointment appointmentDeleted = Common.PracticeRepository.Appointments.WithId(appointment.Id);
            Assert.IsNull(appointmentDeleted);
        }

        /// <summary>
        ///   Tests the saving and retrieval of a ExternalProvider.
        /// </summary>
        [TestMethod]
        public void TestExternalProvider()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };


            //Save ExternalProvider
            ExternalProvider externalProvider = Common.CreateExternalProvider();
            Common.PracticeRepository.Save(externalProvider);
            Assert.IsTrue(externalProvider.Id > 0);


            //Getting back ExternalProvider details
            ExternalProvider externalproviderfetched = Common.PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId(externalProvider.Id);
            Assert.IsNotNull(externalproviderfetched);
            bool areSame = comparsion.Compare(externalproviderfetched, externalProvider);
            Assert.IsTrue(areSame);


            // Update ExternalProvider
            externalproviderfetched.FirstName = "STEPHEN1";
            externalproviderfetched.LastNameOrEntityName = "COX1";
            externalproviderfetched.MiddleName = "M";
            externalproviderfetched.DisplayName = "STEPHEN1 M COX1";
            externalproviderfetched.Salutation = "STEVE1";
            externalproviderfetched.NickName = "STEVE1";
            Common.PracticeRepository.Save(externalproviderfetched);
            ExternalProvider externalproviderUpdated = Common.PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId((externalproviderfetched.Id));
            //PracticeRepository.Contacts.WithId(externalproviderfetched.Id)
            Assert.IsNotNull(externalproviderUpdated);
            areSame = comparsion.Compare(externalproviderUpdated, externalproviderfetched);
            Assert.IsTrue(areSame);


            //Update ExternalProvider email address
            var emailAddress = new ExternalContactEmailAddress();
            emailAddress.Value = "testEmailprovider@test.com";
            emailAddress.OrdinalId = 1;
            emailAddress.ExternalContactId = externalproviderfetched.Id;
            Common.PracticeRepository.Save(emailAddress);
            Assert.IsNotNull(emailAddress);

            var externalContactAddressesFetched = Common.PracticeRepository.ExternalContactAddresses.Where(x => x.ExternalContact.Id == externalproviderfetched.Id).ToList();
            var externalPhoneNumbersFetched = Common.PracticeRepository.ExternalContactPhoneNumbers.Where(x => x.ExternalContact.Id == externalproviderfetched.Id).ToList();
            
            // Delete ExternalProvider
            Common.PracticeRepository.Delete(emailAddress);
            externalContactAddressesFetched.ForEach(x => Common.PracticeRepository.Delete(x));
            externalPhoneNumbersFetched.ForEach(x => Common.PracticeRepository.Delete(x));
            Common.PracticeRepository.Delete(externalproviderUpdated);
            ExternalContact externalproviderDeleted = Common.PracticeRepository.ExternalContacts.WithId(externalproviderUpdated.Id);
            Assert.IsNull(externalproviderDeleted);
        }

        /// <summary>
        ///   Tests the saving and retrieval of a PatientExternalProvider.
        /// </summary>
        [TestMethod]
        public void TestPatientExternalProvider()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Save PatientExternalProvider
            PatientExternalProvider patientExternalProvider = Common.CreatePatientExternalProvider();
            Common.PracticeRepository.Save(patientExternalProvider);
            Assert.IsTrue(patientExternalProvider.Id > 0);

            //Getting back PatientExternalProvider details
            PatientExternalProvider patientExternalProviderRefetched = Common.PracticeRepository.PatientExternalProviders.WithId(patientExternalProvider.Id);
            Assert.IsNotNull(patientExternalProviderRefetched);
            bool areSame = comparsion.Compare(patientExternalProviderRefetched, patientExternalProvider);
            Assert.IsTrue(areSame);


            //Update PatientExternalProvider
            patientExternalProviderRefetched.ExternalProvider = Common.CreateExternalProvider();
            patientExternalProviderRefetched.IsReferringPhysician = true;
            patientExternalProviderRefetched.IsPrimaryCarePhysician = false;
            Common.PracticeRepository.Save(patientExternalProviderRefetched);
            PatientExternalProvider externalpatientproviderUpdated = Common.PracticeRepository.PatientExternalProviders.WithId(patientExternalProviderRefetched.Id);
            Assert.IsNotNull(externalpatientproviderUpdated);
            areSame = comparsion.Compare(patientExternalProviderRefetched, externalpatientproviderUpdated);
            Assert.IsTrue(areSame);


            //Update PatientExternalProvider email address
            var emailAddress = new PatientEmailAddress();
            emailAddress.Value = "testEmailPatientExtrenalprovider@test.com";
            emailAddress.PatientId = patientExternalProviderRefetched.PatientId.IfNotDefault(i => i.Value, default(int));
            Common.PracticeRepository.Save(emailAddress);
            Assert.IsNotNull(emailAddress);
        }

        /// <summary>
        ///   Test's Insurer is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestInsurer()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var legacyPracticeRepository = Common.ServiceProvider.GetService<ILegacyPracticeRepository>();
            CreatePracticeCodeData(legacyPracticeRepository);

            //Create Insurer
            Insurer insurer = Common.CreateInsurer();
            insurer.ClaimFileReceiverId = practiceRepository.ClaimFileReceivers.First().Id; // legacyPracticeRepository.PracticeCodeTables.First(t => t.ReferenceType == "TRANSMITTYPE").Id;
            insurer.InsurerBusinessClassId = (int)InsurerBusinessClassId.BlueCrossBlueShield;
            insurer.InsurerPayTypeId = (int)InsurerPayType.IndividualPolicy; //legacyPracticeRepository.PracticeCodeTables.First(t => t.ReferenceType == "INSURERPTYPE").Id;
            insurer.InsurerPlanTypeId = practiceRepository.InsurerPlanTypes.First().Id; //legacyPracticeRepository.PracticeCodeTables.First(t => t.ReferenceType == "PLANTYPE").Id;
            Assert.IsNotNull(insurer);
            practiceRepository.Save(insurer);

            //Get Back Insurer
            Insurer insurerRefetched = practiceRepository.Insurers.WithId(insurer.Id);
            Assert.IsNotNull(insurerRefetched);
            bool areSame = comparsion.Compare(insurerRefetched, insurer);
            Assert.IsTrue(areSame);

            //Update the Insurer
            insurerRefetched.GroupName = "NewGroupName";
            insurerRefetched.Name = "NewTestName";
            insurerRefetched.PlanName = "NewPlanName";
            insurerRefetched.AllowDependents = false;
            insurerRefetched.MedigapCode = "0";
            insurerRefetched.IsSecondaryClaimPaper = true;
            insurerRefetched.IsReferralRequired = false;
            practiceRepository.Save(insurerRefetched);

            Insurer insurerUpdated = practiceRepository.Insurers.WithId(insurerRefetched.Id);
            Assert.IsNotNull(insurerUpdated);
            areSame = comparsion.Compare(insurerUpdated, insurerRefetched);
            Assert.IsTrue(areSame);

            //Delete the Insurer
            practiceRepository.Delete(insurerUpdated);
            Insurer insurerDeleted = practiceRepository.Insurers.WithId(insurerUpdated.Id);
            Assert.IsNull(insurerDeleted);
        }

        private static void CreatePracticeCodeData(ILegacyPracticeRepository legacyPracticeRepository)
        {
            PracticeCodeTable subFormat = Common.CreatePracticeCode("SUBFORMAT", "S");
            legacyPracticeRepository.Save(subFormat);
            PracticeCodeTable transmittype = Common.CreatePracticeCode("TRANSMITTYPE", "T");
            legacyPracticeRepository.Save(transmittype);
            PracticeCodeTable insurerptype = Common.CreatePracticeCode("INSURERPTYPE", "I");
            legacyPracticeRepository.Save(insurerptype);
            PracticeCodeTable plantype = Common.CreatePracticeCode("PLANTYPE", "P");
            legacyPracticeRepository.Save(plantype);
        }

        /// <summary>
        ///   Test's AppointmentType is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestAppointmentType()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();

            //Create AppointmentType
            AppointmentType appointmentType = Common.CreateAppointmentType();
            appointmentType.OleColor = 1;
            appointmentType.EncounterTypeId = 1;
            appointmentType.PatientQuestionSet = practiceRepository.PatientQuestionSets.WithId(20);
            practiceRepository.Save(appointmentType);

            //Get Back AppointmentType
            AppointmentType appointmentTypeRefetched = practiceRepository.AppointmentTypes.WithId(appointmentType.Id);
            Assert.IsNotNull(appointmentTypeRefetched);
            bool areSame = comparsion.Compare(appointmentType, appointmentTypeRefetched);
            Assert.IsTrue(areSame);
            //Update AppointmentType Details
            appointmentTypeRefetched.OleColor = 0;
            appointmentTypeRefetched.Name = "NewTestAppointmentType";
            practiceRepository.Save(appointmentTypeRefetched);

            AppointmentType appointmentTypeupdated = practiceRepository.AppointmentTypes.WithId(appointmentTypeRefetched.Id);
            Assert.IsNotNull(appointmentTypeupdated);
            areSame = comparsion.Compare(appointmentTypeRefetched, appointmentTypeupdated);
            Assert.IsTrue(areSame);

            //Delete AppointmentType
            practiceRepository.Delete(appointmentTypeupdated);
            AppointmentType appointmentTypeDeleted = practiceRepository.AppointmentTypes.WithId(appointmentTypeupdated.Id);
            Assert.IsNull(appointmentTypeDeleted);
        }

        /// <summary>
        ///   Test's PatientInsuranceReferral is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestPatientInsuranceReferral()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ModelTestsResources.ModelTestsMetadata);

            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create PatientInsuranceReferral Data
            PatientInsuranceReferral patientInsuranceReferral = Common.CreatePatientInsuranceReferral();
            Common.PracticeRepository.Save(patientInsuranceReferral);

            //Get Back PatientInsuranceReferral Data
            PatientInsuranceReferral patientInsuranceReferralRefetched = Common.PracticeRepository.PatientInsuranceReferrals.WithId(patientInsuranceReferral.Id);
            Assert.IsNotNull(patientInsuranceReferralRefetched);
            bool areSame = comparsion.Compare(patientInsuranceReferral, patientInsuranceReferralRefetched);
            Assert.IsTrue(areSame);

            //Update PatientInsuranceReferral Referral Details
            patientInsuranceReferralRefetched.StartDateTime = DateTime.Today.AddDays(50);
            patientInsuranceReferralRefetched.TotalEncountersCovered = 20;
            patientInsuranceReferralRefetched.ReferralCode = "Z";
            patientInsuranceReferralRefetched.IsArchived = false;
            patientInsuranceReferralRefetched.EndDateTime = DateTime.Today.AddDays(500);
            Common.PracticeRepository.Save(patientInsuranceReferralRefetched);

            PatientInsuranceReferral patientInsuranceReferralUpdated = Common.PracticeRepository.PatientInsuranceReferrals.WithId(patientInsuranceReferralRefetched.Id);
            Assert.IsNotNull(patientInsuranceReferralUpdated);
            areSame = comparsion.Compare(patientInsuranceReferralUpdated, patientInsuranceReferralRefetched);
            Assert.IsTrue(areSame);

            //Delete
            Common.PracticeRepository.Delete(patientInsuranceReferralUpdated);
            PatientInsuranceReferral patientInsuranceReferralDeleted = Common.PracticeRepository.PatientInsuranceReferrals.WithId(patientInsuranceReferralUpdated.Id);
            Assert.IsNull(patientInsuranceReferralDeleted);
        }

        /// <summary>
        ///   Test's InsurerPlanType is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestInsurerPlanType()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparsion = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create InsurerPlanType
            InsurerPlanType insurerPlanType = Common.CreateInsurerPlanType();
            Assert.IsNotNull(insurerPlanType);
            //Save InsurerPlanType 
            practiceRepository.Save(insurerPlanType);

            InsurerPlanType insurerPlanTypeRefetched = practiceRepository.InsurerPlanTypes.WithId(insurerPlanType.Id);
            Assert.IsNotNull(insurerPlanTypeRefetched);
            bool areSame = comparsion.Compare(insurerPlanType, insurerPlanTypeRefetched);
            Assert.IsTrue(areSame);

            //Update InsurerPlanType
            insurerPlanTypeRefetched.Name = "NewInsurerPlanType";
            practiceRepository.Save(insurerPlanTypeRefetched);
            InsurerPlanType insurerPlanTypeUpdated = practiceRepository.InsurerPlanTypes.WithId(insurerPlanTypeRefetched.Id);
            Assert.IsNotNull(insurerPlanTypeUpdated);
            areSame = comparsion.Compare(insurerPlanTypeRefetched, insurerPlanTypeUpdated);
            Assert.IsTrue(areSame);

            //Delete insurerPlanType
            practiceRepository.Delete(insurerPlanTypeUpdated);
            InsurerPlanType insurerPlanTypeDeleted = practiceRepository.InsurerPlanTypes.WithId(insurerPlanTypeUpdated.Id);
            Assert.IsNull(insurerPlanTypeDeleted);
        }

        /// <summary>
        ///   Test's BillingOrganization is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestBillingOrganization()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    MaxDifferences = 100,
                    CompareChildren = false
                };
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            BillingOrganization billingOrganization = Common.CreateBillingOrganization();
            Assert.IsNotNull(billingOrganization);
            practiceRepository.Save(billingOrganization);

            //Get Back BillingOrganization
            BillingOrganization billingOrganizationRefetched = practiceRepository.BillingOrganizations.WithId(billingOrganization.Id);
            Assert.IsNotNull(billingOrganizationRefetched);
            bool areSame = comparsion.Compare(billingOrganization, billingOrganizationRefetched);
            Assert.IsTrue(areSame);

            //Update
            billingOrganizationRefetched.Name = "NewTestBillingOrganizations";
            practiceRepository.Save(billingOrganizationRefetched);

            BillingOrganization billingOrganizationUpdated = practiceRepository.BillingOrganizations.WithId(billingOrganizationRefetched.Id);
            Assert.IsNotNull(billingOrganizationUpdated);
            areSame = comparsion.Compare(billingOrganizationRefetched, billingOrganizationUpdated);
            Assert.IsTrue(areSame);

            //Delete
            practiceRepository.Delete(billingOrganizationUpdated);
            BillingOrganization billingOrganizationDeleted = practiceRepository.BillingOrganizations.WithId(billingOrganizationUpdated.Id);
            Assert.IsNull(billingOrganizationDeleted);
        }

        /// <summary>
        ///   Test's ServiceLocation is creating and updating correctly
        /// </summary>
        /// <remarks>
        /// </remarks>
        [TestMethod]
        public void TestServiceLocation()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    MaxDifferences = 100,
                    CompareChildren = false
                };
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            ServiceLocation serviceLocation = Common.CreateServiceLocation();
            serviceLocation.ShortName = "new office";
            practiceRepository.Save(serviceLocation);

            //Get Back ServiceLocation
            ServiceLocation serviceLocationRefetched = practiceRepository.ServiceLocations.WithId(serviceLocation.Id);
            serviceLocationRefetched.ShortName = serviceLocation.ShortName;
            Assert.IsNotNull(serviceLocationRefetched);
            bool areSame = comparsion.Compare(serviceLocation, serviceLocationRefetched);
            Assert.IsTrue(areSame);

            //Update
            serviceLocationRefetched.ShortName = "new office 2";
            serviceLocationRefetched.Name = "NewServiceLocation";
            practiceRepository.Save(serviceLocationRefetched);

            ServiceLocation serviceLocationUpdated = practiceRepository.ServiceLocations.WithId(serviceLocationRefetched.Id);
            serviceLocationUpdated.ShortName = serviceLocationRefetched.ShortName;
            Assert.IsNotNull(serviceLocationUpdated);
            areSame = comparsion.Compare(serviceLocationRefetched, serviceLocationUpdated);
            Assert.IsTrue(areSame);

            //Delete
            practiceRepository.Delete(serviceLocationUpdated);
            ServiceLocation serviceLocationDeleted = practiceRepository.ServiceLocations.WithId(serviceLocationUpdated.Id);
            Assert.IsNull(serviceLocationDeleted);
        }

        /// <summary>
        ///   Tests the saving and retrieval of a ExternalOrganization.
        /// </summary>
        [TestMethod]
        public void TestExternalOrganization()
        {
            var comparsion = new Objects.ObjectComparison
                {
                    MaxDifferences = 100,
                    CompareChildren = false
                };
            //Save ExternalOrganization
            ExternalOrganization externalOrganization = Common.CreateExternalOrganization();
            Common.PracticeRepository.Save(externalOrganization);
            Assert.IsTrue(externalOrganization.Id > 0);


            //Getting back ExternalOrganization details
            ExternalOrganization externalOrganizationfetched = Common.PracticeRepository.ExternalOrganizations.WithId(externalOrganization.Id);
            Assert.IsNotNull(externalOrganizationfetched);
            bool areSame = comparsion.Compare(externalOrganizationfetched, externalOrganization);
            Assert.IsTrue(areSame);

            // Update ExternalOrganization

            externalOrganizationfetched.DisplayName = "Test your firm1";
            externalOrganizationfetched.ExternalOrganizationTypeId = 3;
            externalOrganizationfetched.Name = "Test your firm1";
            Common.PracticeRepository.Save(externalOrganizationfetched);
            ExternalOrganization externalOrganizationUpdated = Common.PracticeRepository.ExternalOrganizations.WithId(externalOrganizationfetched.Id);
            Assert.IsNotNull(externalOrganizationUpdated);
            areSame = comparsion.Compare(externalOrganizationfetched, externalOrganizationUpdated);
            Assert.IsTrue(areSame);


            // Delete ExternalOrganization
            Common.PracticeRepository.Delete(externalOrganizationUpdated);
            ExternalOrganization externalOrganizationDeleted = Common.PracticeRepository.ExternalOrganizations.WithId(externalOrganizationUpdated.Id);
            Assert.IsNull(externalOrganizationDeleted);
        }

        /// <summary>
        ///   Tests saving of addresses
        /// </summary>
        [TestMethod]
        public void TestAddress()
        {
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Update patient address

            //Create new patient
            Patient patient = Common.CreatePatient();
            patient.FirstName = "TestPatient";
            patient.LastName = "PatientAddress";
            patient.DateOfBirth = DateTime.Today;
            Common.PracticeRepository.Save(patient);

            //Give patient address
            var patientAddress = new PatientAddress();
            patientAddress.Line1 = "Testing Address Line 1";
            patientAddress.Line2 = "And this is line 2";
            patientAddress.PatientId = patient.Id;
            patientAddress.PostalCode = "84040";
            patientAddress.City = "New York";
            patientAddress.PatientAddressTypeId = (int)PatientAddressTypeId.Home;
            patientAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            Common.PracticeRepository.Save(patientAddress);

            //Check to see if same
            PatientAddress patientAddressRefetched = Common.PracticeRepository.PatientAddresses.WithId(patientAddress.Id);
            bool areSame = comparison.Compare(patientAddress, patientAddressRefetched);
            Assert.IsTrue(areSame);

            //Update patient address

            //Give patient Business address
            var patientAddressBusiness = new PatientAddress();
            patientAddressBusiness.Line1 = "Testing Address Line 1";
            patientAddressBusiness.Line2 = "line 2";
            patientAddressBusiness.PatientId = patient.Id;
            patientAddressBusiness.PostalCode = "84040";
            patientAddressBusiness.City = "New York";
            patientAddressBusiness.PatientAddressTypeId = (int)PatientAddressTypeId.Business;
            patientAddressBusiness.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            Common.PracticeRepository.Save(patientAddressBusiness);

            //Check to see if same
            PatientAddress patientAddressBusinessRefetched = Common.PracticeRepository.PatientAddresses.WithId(patientAddressBusiness.Id);
            areSame = comparison.Compare(patientAddressBusiness, patientAddressBusinessRefetched);
            Assert.IsTrue(areSame);

            //Give patient Other1 address
            var patientAddressOther1 = new PatientAddress();
            patientAddressOther1.Line1 = "Testing Address Line 1";
            patientAddressOther1.Line2 = "line 2";
            patientAddressOther1.PatientId = patient.Id;
            patientAddressOther1.PostalCode = "84040";
            patientAddressOther1.City = "New York";
            patientAddressOther1.PatientAddressTypeId = Common.PracticeRepository.PatientAddressTypes.First(at => at.Name == "Other1").Id;
            patientAddressOther1.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            Common.PracticeRepository.Save(patientAddressOther1);

            //Check to see if same
            PatientAddress patientAddressOther1Refetched = Common.PracticeRepository.PatientAddresses.WithId(patientAddressOther1.Id);
            areSame = comparison.Compare(patientAddressOther1, patientAddressOther1Refetched);
            Assert.IsTrue(areSame);

            //Give patient Other2 address
            var patientAddressOther2 = new PatientAddress();
            patientAddressOther2.Line1 = "Testing Address Line 1";
            patientAddressOther2.Line2 = "line 2";
            patientAddressOther2.PatientId = patient.Id;
            patientAddressOther2.PostalCode = "84040";
            patientAddressOther2.City = "New York";
            patientAddressOther2.PatientAddressTypeId = Common.PracticeRepository.PatientAddressTypes.First(at => at.Name == "Other2").Id;
            patientAddressOther2.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            Common.PracticeRepository.Save(patientAddressOther2);

            //Check to see if same
            PatientAddress patientAddressOther2Refetched = Common.PracticeRepository.PatientAddresses.WithId(patientAddressOther2.Id);
            areSame = comparison.Compare(patientAddressOther2, patientAddressOther2Refetched);
            Assert.IsTrue(areSame);

            //'Update address for users
            //'Create(New User)
            User user = Common.CreateUser();
            user.FirstName = "Test";
            user.LastName = "UserAddress";
            Common.PracticeRepository.Save(user);

            //'Give user address
            var userAddress = new UserAddress();
            userAddress.Line1 = "Testing Address Line 1";
            userAddress.Line2 = "line2";
            userAddress.UserId = user.Id;
            userAddress.PostalCode = "84040";
            userAddress.City = "New York";
            userAddress.UserAddressTypeId = Common.PracticeRepository.UserAddressTypes.First(at => at.Name == "User1").Id;
            userAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            Common.PracticeRepository.Save(userAddress);

            //'Check to see if same
            UserAddress userAddressRefetched = Common.PracticeRepository.UserAddresses.WithId(userAddress.Id);
            areSame = comparison.Compare(userAddress, userAddressRefetched);
            Assert.IsTrue(areSame);

            //'Test Insurers
            Insurer insurer = Common.CreateInsurer();
            Common.PracticeRepository.Save(insurer);

            var insurerClaimAddress = new InsurerAddress();
            insurerClaimAddress.Line1 = "Last call for Line1";
            insurerClaimAddress.City = "New York";
            insurerClaimAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            insurerClaimAddress.PostalCode = "10P5I";
            insurerClaimAddress.InsurerId = insurer.Id;
            insurerClaimAddress.InsurerAddressTypeId = 3;
            Common.PracticeRepository.Save(insurerClaimAddress);

            //'Check to see if same
            InsurerAddress insurerClaimAddressRefetched = Common.PracticeRepository.InsurerAddresses.WithId(insurerClaimAddress.Id);
            areSame = comparison.Compare(insurerClaimAddress, insurerClaimAddressRefetched);
            Assert.IsTrue(areSame);

            //'Test BillingOrganization
            BillingOrganization billingOrganization = Common.PracticeRepository.BillingOrganizations.First();

            var billingOrganizationAddress = new BillingOrganizationAddress();
            billingOrganizationAddress.BillingOrganizationAddressTypeId = (int)BillingOrganizationAddressTypeId.PhysicalLocation;
            billingOrganizationAddress.BillingOrganizationId = billingOrganization.Id;
            billingOrganizationAddress.City = "Layton";
            billingOrganizationAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            billingOrganizationAddress.Line1 = "Broadway";
            billingOrganizationAddress.Line2 = "2";
            billingOrganizationAddress.PostalCode = "12345";
            Common.PracticeRepository.Save(billingOrganizationAddress);

            //'Check to see if same
            BillingOrganizationAddress billingOrganizationAddressRefetched = Common.PracticeRepository.BillingOrganizationAddresses.WithId(billingOrganizationAddress.Id);
            areSame = comparison.Compare(billingOrganizationAddress, billingOrganizationAddressRefetched);
            Assert.IsTrue(areSame);

            //'BillingOrganization from Resource Table
            PersonBillingOrganization billingOrganizationRe = Common.PracticeRepository.BillingOrganizations.OfType<PersonBillingOrganization>().First();

            var billingOrganizationReAddress = new BillingOrganizationAddress();
            billingOrganizationReAddress.BillingOrganizationAddressTypeId = (int)BillingOrganizationAddressTypeId.PhysicalLocation;
            billingOrganizationReAddress.BillingOrganizationId = billingOrganizationRe.Id;
            billingOrganizationReAddress.City = "Nebraska";
            billingOrganizationReAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First().Id;
            billingOrganizationReAddress.Line1 = "Uintah";
            billingOrganizationReAddress.Line2 = "2";
            billingOrganizationReAddress.PostalCode = "12345";
            Common.PracticeRepository.Save(billingOrganizationReAddress);

            BillingOrganizationAddress billingOrganizationReAddressRefetched = Common.PracticeRepository.BillingOrganizationAddresses.WithId(billingOrganizationReAddress.Id);
            areSame = comparison.Compare(billingOrganizationReAddress, billingOrganizationReAddressRefetched);
            Assert.IsTrue(areSame);

            //Save ExternalOrganization
            ExternalOrganization externalOrganization = Common.CreateExternalOrganization();
            Common.PracticeRepository.Save(externalOrganization);
            Assert.IsTrue(externalOrganization.Id > 0);
            //'ExternalOrganizations
            externalOrganization = Common.PracticeRepository.ExternalOrganizations.First();

            var externalOrganizationMainAddress = new ExternalOrganizationAddress();
            externalOrganizationMainAddress.ExternalOrganizationAddressTypeId = (int)ExternalOrganizationAddressTypeId.MainOffice;
            externalOrganizationMainAddress.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationMainAddress.City = "Kaysville";
            externalOrganizationMainAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First(st => st.Name == "Utah").Id;
            externalOrganizationMainAddress.Line1 = "No I don't Want to";
            externalOrganizationMainAddress.Line2 = "Go Away";
            externalOrganizationMainAddress.PostalCode = "84040";
            Common.PracticeRepository.Save(externalOrganizationMainAddress);

            ExternalOrganizationAddress externalOrganizationMainAddressRefetched = Common.PracticeRepository.ExternalOrganizationAddresses.WithId(externalOrganizationMainAddress.Id);
            areSame = comparison.Compare(externalOrganizationMainAddress, externalOrganizationMainAddressRefetched);
            Assert.IsTrue(areSame);

            //Other 1 
            var externalOrganizationOther1Address = new ExternalOrganizationAddress();
            externalOrganizationOther1Address.ExternalOrganizationAddressTypeId = (int)ExternalOrganizationAddressTypeId.MainOffice;
            externalOrganizationOther1Address.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationOther1Address.City = "Kaysville";
            externalOrganizationOther1Address.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First(st => st.Name == "Utah").Id;
            externalOrganizationOther1Address.Line1 = "YesPlease";
            externalOrganizationOther1Address.Line2 = "Suite 2";
            externalOrganizationOther1Address.PostalCode = "84040";
            Common.PracticeRepository.Save(externalOrganizationOther1Address);

            ExternalOrganizationAddress externalOrganizationOther1AddressRefetched = Common.PracticeRepository.ExternalOrganizationAddresses.WithId(externalOrganizationOther1Address.Id);
            areSame = comparison.Compare(externalOrganizationOther1Address, externalOrganizationOther1AddressRefetched);
            Assert.IsTrue(areSame);

            //Other2
            var externalOrganizationOther2Address = new ExternalOrganizationAddress();
            externalOrganizationOther2Address.ExternalOrganizationAddressTypeId = (int)ExternalOrganizationAddressTypeId.MainOffice;
            externalOrganizationOther2Address.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationOther2Address.City = "Kaysville";
            externalOrganizationOther2Address.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First(st => st.Name == "Utah").Id;
            externalOrganizationOther2Address.Line1 = "YesPlease";
            externalOrganizationOther2Address.Line2 = "Suite 2";
            externalOrganizationOther2Address.PostalCode = "84040";
            Common.PracticeRepository.Save(externalOrganizationOther2Address);

            ExternalOrganizationAddress externalOrganizationOther2AddressRefetched = Common.PracticeRepository.ExternalOrganizationAddresses.WithId(externalOrganizationOther2Address.Id);
            areSame = comparison.Compare(externalOrganizationOther2Address, externalOrganizationOther2AddressRefetched);
            Assert.IsTrue(areSame);

            //'Service Location
            ServiceLocation serviceLocation = Common.PracticeRepository.ServiceLocations.First(s => s.ShortName == "Office");
            //Primary
            var serviceLocationMainAddress = new ServiceLocationAddress();
            serviceLocationMainAddress.ServiceLocationId = serviceLocation.Id;
            serviceLocationMainAddress.ServiceLocationAddressTypeId = (int)ServiceLocationAddressTypeId.MainOffice;
            serviceLocationMainAddress.Line1 = "I like Unit Tests";
            serviceLocationMainAddress.Line2 = "Fun";
            serviceLocationMainAddress.PostalCode = "52587";
            serviceLocationMainAddress.City = "boho";
            serviceLocationMainAddress.StateOrProvinceId = Common.PracticeRepository.StateOrProvinces.First(st => st.Name == "Utah").Id;
            Common.PracticeRepository.Save(serviceLocationMainAddress);

            ServiceLocationAddress serviceLocationMainAddressRefetched = Common.PracticeRepository.ServiceLocationAddresses.WithId(serviceLocationMainAddress.Id);
            areSame = comparison.Compare(serviceLocationMainAddress, serviceLocationMainAddressRefetched);
            Assert.IsTrue(areSame);
        }

        /// <summary>
        ///   Tests saving of phone numbers
        /// </summary>
        [TestMethod]
        public void TestPhoneNumber()
        {
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //BillingOrganization Practice Name Table
            BillingOrganization billingOrganizationPn = Common.CreateBillingOrganization();
            Common.PracticeRepository.Save(billingOrganizationPn);

            var billingOrganizationPnPhoneNumber = new BillingOrganizationPhoneNumber();
            billingOrganizationPnPhoneNumber.BillingOrganizationId = billingOrganizationPn.Id;
            billingOrganizationPnPhoneNumber.BillingOrganizationPhoneNumberTypeId = 9;
            billingOrganizationPnPhoneNumber.ExchangeAndSuffix = "5432109";

            Common.PracticeRepository.Save(billingOrganizationPnPhoneNumber);

            BillingOrganizationPhoneNumber billingOrganizationPnPhoneNumberRefetched = Common.PracticeRepository.BillingOrganizationPhoneNumbers.WithId(billingOrganizationPnPhoneNumber.Id);
            billingOrganizationPnPhoneNumber.OrdinalId = 1;
            bool areSame = comparison.Compare(billingOrganizationPnPhoneNumber, billingOrganizationPnPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Other Phone
            var billingOrganizationPnOpPhoneNumber = new BillingOrganizationPhoneNumber();
            billingOrganizationPnOpPhoneNumber.BillingOrganizationId = billingOrganizationPn.Id;
            billingOrganizationPnOpPhoneNumber.BillingOrganizationPhoneNumberTypeId = 1;
            billingOrganizationPnOpPhoneNumber.ExchangeAndSuffix = "5298725";
            billingOrganizationPnOpPhoneNumber.OrdinalId = 2;
            Common.PracticeRepository.Save(billingOrganizationPnOpPhoneNumber);

            BillingOrganizationPhoneNumber billingOrganizationPnOpPhoneNumberRefetched = Common.PracticeRepository.BillingOrganizationPhoneNumbers.WithId(billingOrganizationPnOpPhoneNumber.Id);
            areSame = comparison.Compare(billingOrganizationPnOpPhoneNumber, billingOrganizationPnOpPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Fax
            var billingOrganizationPnFxPhoneNumber = new BillingOrganizationPhoneNumber();
            billingOrganizationPnFxPhoneNumber.BillingOrganizationId = billingOrganizationPn.Id;
            billingOrganizationPnFxPhoneNumber.BillingOrganizationPhoneNumberTypeId = 6;
            billingOrganizationPnFxPhoneNumber.ExchangeAndSuffix = "5586981";
            billingOrganizationPnFxPhoneNumber.OrdinalId = 3;
            Common.PracticeRepository.Save(billingOrganizationPnFxPhoneNumber);

            BillingOrganizationPhoneNumber billingOrganizationPnFxPhoneNumberRefetched = Common.PracticeRepository.BillingOrganizationPhoneNumbers.WithId(billingOrganizationPnFxPhoneNumber.Id);
            areSame = comparison.Compare(billingOrganizationPnFxPhoneNumber, billingOrganizationPnFxPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //BillingOrganization Resource Table 
            PersonBillingOrganization billingOrganizationRe = Common.PracticeRepository.BillingOrganizations.OfType<PersonBillingOrganization>().First();

            var billingOrganizationRePhoneNumber = new BillingOrganizationPhoneNumber();
            billingOrganizationRePhoneNumber.BillingOrganizationId = billingOrganizationRe.Id;
            billingOrganizationRePhoneNumber.BillingOrganizationPhoneNumberTypeId = 9;
            billingOrganizationRePhoneNumber.ExchangeAndSuffix = "1245635";
            Common.PracticeRepository.Save(billingOrganizationRePhoneNumber);

            BillingOrganizationPhoneNumber billingOrganizationRePhoneNumberRefetched = Common.PracticeRepository.BillingOrganizationPhoneNumbers.WithId(billingOrganizationRePhoneNumber.Id);
            billingOrganizationRePhoneNumber.OrdinalId = 1;
            areSame = comparison.Compare(billingOrganizationRePhoneNumber, billingOrganizationRePhoneNumberRefetched);
            Assert.IsTrue(areSame);


            //Patient phone number
            //Create new patient
            Patient patient = Common.CreatePatient();
            patient.FirstName = "TestPatient";
            patient.LastName = "PatientPhone";
            patient.DateOfBirth = DateTime.Today;
            Common.PracticeRepository.Save(patient);

            //Give patient phone home number
            var patientHomePhoneNumber = new PatientPhoneNumber();
            patientHomePhoneNumber.ExchangeAndSuffix = "5462988";
            patientHomePhoneNumber.PatientPhoneNumberTypeId = PatientPhoneNumberType.Home;
            patientHomePhoneNumber.PatientId = patient.Id;
            patientHomePhoneNumber.OrdinalId = 1;
            Common.PracticeRepository.Save(patientHomePhoneNumber);

            PatientPhoneNumber patientHomePhoneNumberRefetched = Common.PracticeRepository.PatientPhoneNumbers.WithId(patientHomePhoneNumber.Id);
            areSame = comparison.Compare(patientHomePhoneNumber, patientHomePhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //BusinessPhone
            var patientBusinessPhoneNumber = new PatientPhoneNumber();
            patientBusinessPhoneNumber.ExchangeAndSuffix = "5462988";
            patientBusinessPhoneNumber.PatientPhoneNumberTypeId = PatientPhoneNumberType.Business;
            patientBusinessPhoneNumber.PatientId = patient.Id;
            patientBusinessPhoneNumber.OrdinalId = 2;
            Common.PracticeRepository.Save(patientBusinessPhoneNumber);

            PatientPhoneNumber patientBusinessPhoneNumberRefetched = Common.PracticeRepository.PatientPhoneNumbers.WithId(patientBusinessPhoneNumber.Id);
            areSame = comparison.Compare(patientBusinessPhoneNumber, patientBusinessPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Cell Phone
            var patientCellPhoneNumber = new PatientPhoneNumber();
            patientCellPhoneNumber.ExchangeAndSuffix = "5462988";
            patientCellPhoneNumber.PatientPhoneNumberTypeId = PatientPhoneNumberType.Cell;
            patientCellPhoneNumber.OrdinalId = 3;
            patientCellPhoneNumber.PatientId = patient.Id;
            Common.PracticeRepository.Save(patientCellPhoneNumber);

            PatientPhoneNumber patientCellPhoneNumberRefetched = Common.PracticeRepository.PatientPhoneNumbers.WithId(patientCellPhoneNumber.Id);
            areSame = comparison.Compare(patientCellPhoneNumber, patientCellPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //' Contacts
            //Main phone
            ExternalProvider contact = Common.CreateExternalProvider();
            Common.PracticeRepository.Save(contact);

            var contactMainPhoneNumber = new ExternalContactPhoneNumber();
            contactMainPhoneNumber.ExternalContactId = contact.Id;
            contactMainPhoneNumber.ExternalContactPhoneNumberTypeId = 5;
            contactMainPhoneNumber.ExchangeAndSuffix = "1234567";
            contactMainPhoneNumber.OrdinalId = 1;
            Common.PracticeRepository.Save(contactMainPhoneNumber);

            ExternalContactPhoneNumber contactMainPhoneNumberRefetched = Common.PracticeRepository.ExternalContactPhoneNumbers.WithId(contactMainPhoneNumber.Id);
            contactMainPhoneNumber.OrdinalId = 1;
            areSame = comparison.Compare(contactMainPhoneNumber, contactMainPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Fax phone
            var contactFaxPhoneNumber = new ExternalContactPhoneNumber();
            contactFaxPhoneNumber.ExternalContactId = contact.Id;
            contactFaxPhoneNumber.ExternalContactPhoneNumberTypeId = 4;
            contactFaxPhoneNumber.ExchangeAndSuffix = "1234567";
            contactFaxPhoneNumber.OrdinalId = 2;
            Common.PracticeRepository.Save(contactFaxPhoneNumber);

            ExternalContactPhoneNumber contactFaxPhoneNumberRefetched = Common.PracticeRepository.ExternalContactPhoneNumbers.WithId(contactFaxPhoneNumber.Id);
            areSame = comparison.Compare(contactFaxPhoneNumber, contactFaxPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Cell phone
            var contactCellPhoneNumber = new ExternalContactPhoneNumber();
            contactCellPhoneNumber.ExternalContactId = contact.Id;
            contactCellPhoneNumber.ExternalContactPhoneNumberTypeId = Common.PracticeRepository.ExternalContactPhoneNumberTypes.First(ct => ct.Name == "Cell").Id;
            contactCellPhoneNumber.ExchangeAndSuffix = "1234567";
            contactCellPhoneNumber.OrdinalId = 3;
            Common.PracticeRepository.Save(contactCellPhoneNumber);

            ExternalContactPhoneNumber contactCellPhoneNumberRefetched = Common.PracticeRepository.ExternalContactPhoneNumbers.WithId(contactCellPhoneNumber.Id);
            areSame = comparison.Compare(contactCellPhoneNumber, contactCellPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //'ExternalOrganization
            //Main
            ExternalOrganization externalOrganization = Common.CreateExternalOrganization();
            Common.PracticeRepository.Save(externalOrganization);

            var externalOrganizationMainPhoneNumber = new ExternalOrganizationPhoneNumber();
            externalOrganizationMainPhoneNumber.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationMainPhoneNumber.ExternalOrganizationPhoneNumberTypeId = Common.PracticeRepository.ExternalOrganizationPhoneNumberTypes.First(ct => ct.Name == "Main").Id;
            externalOrganizationMainPhoneNumber.ExchangeAndSuffix = "3456789";
            externalOrganizationMainPhoneNumber.OrdinalId = 1;
            Common.PracticeRepository.Save(externalOrganizationMainPhoneNumber);

            ExternalOrganizationPhoneNumber externalOrganizationMainPhoneNumberRefetched = Common.PracticeRepository.ExternalOrganizationPhoneNumbers.WithId(externalOrganizationMainPhoneNumber.Id);
            externalOrganizationMainPhoneNumber.OrdinalId = 1;
            areSame = comparison.Compare(externalOrganizationMainPhoneNumber, externalOrganizationMainPhoneNumberRefetched);
            Assert.IsTrue(areSame);
            //Fax
            var externalOrganizationFaxPhoneNumber = new ExternalOrganizationPhoneNumber();
            externalOrganizationFaxPhoneNumber.ExternalOrganizationId = externalOrganization.Id;
            externalOrganizationFaxPhoneNumber.ExternalOrganizationPhoneNumberTypeId = Common.PracticeRepository.ExternalOrganizationPhoneNumberTypes.First(ct => ct.Name == "Main").Id;
            externalOrganizationFaxPhoneNumber.ExchangeAndSuffix = "3456789";
            Common.PracticeRepository.Save(externalOrganizationFaxPhoneNumber);

            ExternalOrganizationPhoneNumber externalOrganizationFaxPhoneNumberRefetched = Common.PracticeRepository.ExternalOrganizationPhoneNumbers.WithId(externalOrganizationFaxPhoneNumber.Id);
            externalOrganizationFaxPhoneNumber.OrdinalId = 0;
            areSame = comparison.Compare(externalOrganizationFaxPhoneNumber, externalOrganizationFaxPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //'Insurers
            Insurer insurer = Common.CreateInsurer();
            Common.PracticeRepository.Save(insurer);

            //Authorization
            var insurerAuthorizationPhoneNumber = new InsurerPhoneNumber();
            insurerAuthorizationPhoneNumber.InsurerId = insurer.Id;
            insurerAuthorizationPhoneNumber.InsurerPhoneNumberTypeId = Common.PracticeRepository.InsurerPhoneNumberTypes.First(i => i.Name == "Authorization").Id;
            insurerAuthorizationPhoneNumber.ExchangeAndSuffix = "3463267";
            Common.PracticeRepository.Save(insurerAuthorizationPhoneNumber);

            InsurerPhoneNumber insurerAuthorizationPhoneNumberRefetched = Common.PracticeRepository.InsurerPhoneNumbers.WithId(insurerAuthorizationPhoneNumber.Id);
            areSame = comparison.Compare(insurerAuthorizationPhoneNumber, insurerAuthorizationPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Eligibility
            var insurerEligibilityPhoneNumber = new InsurerPhoneNumber();
            insurerEligibilityPhoneNumber.InsurerId = insurer.Id;
            insurerEligibilityPhoneNumber.InsurerPhoneNumberTypeId = Common.PracticeRepository.InsurerPhoneNumberTypes.First(i => i.Name == "Eligibility").Id;
            insurerEligibilityPhoneNumber.ExchangeAndSuffix = "8794561";
            Common.PracticeRepository.Save(insurerEligibilityPhoneNumber);

            InsurerPhoneNumber eligibilityPhoneNumberRefetched = Common.PracticeRepository.InsurerPhoneNumbers.WithId(insurerEligibilityPhoneNumber.Id);
            areSame = comparison.Compare(insurerEligibilityPhoneNumber, eligibilityPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Provider
            var insurerPhoneNumber = new InsurerPhoneNumber();
            insurerPhoneNumber.InsurerId = insurer.Id;
            insurerPhoneNumber.InsurerPhoneNumberTypeId = Common.PracticeRepository.InsurerPhoneNumberTypes.First(ip => ip.Name == "Provider").Id;
            insurerPhoneNumber.ExchangeAndSuffix = "8798415";
            Common.PracticeRepository.Save(insurerPhoneNumber);

            InsurerPhoneNumber insurerProviderPhoneNumberRefetched = Common.PracticeRepository.InsurerPhoneNumbers.WithId(insurerPhoneNumber.Id);
            areSame = comparison.Compare(insurerPhoneNumber, insurerProviderPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Claims
            var insurerClaimsPhoneNumber = new InsurerPhoneNumber();
            insurerClaimsPhoneNumber.InsurerId = insurer.Id;
            insurerClaimsPhoneNumber.InsurerPhoneNumberTypeId = Common.PracticeRepository.InsurerPhoneNumberTypes.First(i => i.Name == "Claims").Id;
            insurerClaimsPhoneNumber.ExchangeAndSuffix = "3487945";
            Common.PracticeRepository.Save(insurerClaimsPhoneNumber);

            InsurerPhoneNumber insurerClaimsPhoneNumberRefetched = Common.PracticeRepository.InsurerPhoneNumbers.WithId(insurerClaimsPhoneNumber.Id);
            areSame = comparison.Compare(insurerClaimsPhoneNumber, insurerClaimsPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //Submissions
            var insurerSubmissionsPhoneNumber = new InsurerPhoneNumber();
            insurerSubmissionsPhoneNumber.InsurerId = insurer.Id;
            insurerSubmissionsPhoneNumber.InsurerPhoneNumberTypeId = Common.PracticeRepository.InsurerPhoneNumberTypes.First(i => i.Name == "Submissions").Id;
            insurerSubmissionsPhoneNumber.ExchangeAndSuffix = "3463673";
            Common.PracticeRepository.Save(insurerSubmissionsPhoneNumber);

            InsurerPhoneNumber insurerSubmissionsPhoneNumberRefetched = Common.PracticeRepository.InsurerPhoneNumbers.WithId(insurerSubmissionsPhoneNumber.Id);
            areSame = comparison.Compare(insurerSubmissionsPhoneNumber, insurerSubmissionsPhoneNumberRefetched);
            Assert.IsTrue(areSame);

            //'Users
            User user = Common.CreateUser();
            user.FirstName = "Test";
            user.LastName = "UserPhone";
            Common.PracticeRepository.Save(user);

            //Main phone
            var userPhoneNumber = new UserPhoneNumber();
            userPhoneNumber.UserId = user.Id;
            userPhoneNumber.UserPhoneNumberTypeId = 10;
            userPhoneNumber.ExchangeAndSuffix = "1234567";
            Common.PracticeRepository.Save(userPhoneNumber);

            //'Check to see if same
            UserPhoneNumber userPhoneNumberRefetched = Common.PracticeRepository.UserPhoneNumbers.WithId(userPhoneNumber.Id);
            userPhoneNumber.OrdinalId = 1;
            areSame = comparison.Compare(userPhoneNumber, userPhoneNumberRefetched);
            Assert.IsTrue(areSame);
        }

        [TestMethod]
        public void TestEncounterServiceLocation()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create Data
            Encounter encounter = Common.CreateAppointment().Encounter;

            //Change the service Location to Main Office
            encounter.ServiceLocation = Common.PracticeRepository.ServiceLocations.FirstOrDefault(i => i.ShortName == "office");

            //Save Encounter
            practiceRepository.Save(encounter);
            Assert.IsTrue(encounter.Id > 0);

            //Getting back encounter details
            Encounter encounterRefechted = practiceRepository.Encounters.Include(s => s.ServiceLocation).WithId(encounter.Id);
            Assert.IsNotNull(encounterRefechted);
            bool areSame = comparison.Compare(encounter.ServiceLocation, encounterRefechted.ServiceLocation);
            Assert.IsTrue(areSame);

            //Update Encounter
            encounterRefechted.EncounterStatusId = (int)EncounterStatus.CancelNoShow;

            //Change the service Location other than Main Office
            encounterRefechted.ServiceLocation = Common.PracticeRepository.ServiceLocations.FirstOrDefault(i => i.ShortName != "office");

            practiceRepository.Save(encounterRefechted);
            Encounter encounterUpdated = practiceRepository.Encounters.Include(s => s.ServiceLocation).WithId(encounterRefechted.Id);
            areSame = comparison.Compare(encounterRefechted.ServiceLocation, encounterUpdated.ServiceLocation);
            Assert.IsTrue(areSame);

            //Delete Encounter
            practiceRepository.Delete(encounterRefechted);
            Encounter encounterDeleted = practiceRepository.Encounters.WithId(encounterRefechted.Id);
            Assert.IsNull(encounterDeleted);
        }

        [TestMethod]
        public void TestUserScheduleBlock()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create UserScheduleBlock
            UserScheduleBlock scheduleBlock = Common.CreateUserScheduleBlock();

            //Save UserScheduleBlock
            practiceRepository.Save(scheduleBlock);
            Assert.IsTrue(scheduleBlock.Id > 0);

            //Get Back UserScheduleBlock
            UserScheduleBlock scheduleBlockRefetched = practiceRepository.ScheduleBlocks.OfType<UserScheduleBlock>().WithId(scheduleBlock.Id);
            Assert.IsNotNull(scheduleBlockRefetched);
            bool areSame = comparison.Compare(scheduleBlock, scheduleBlockRefetched);
            Assert.IsTrue(areSame);

            //Update UserScheduleBlock details
            scheduleBlockRefetched.Comment = "Updating refetched UserScheduleBlock";
            practiceRepository.Save(scheduleBlockRefetched);

            UserScheduleBlock scheduleBlockUpdated = practiceRepository.ScheduleBlocks.OfType<UserScheduleBlock>().WithId(scheduleBlock.Id);
            areSame = comparison.Compare(scheduleBlockRefetched, scheduleBlockUpdated);
            Assert.IsTrue(areSame);

            //Delete UserScheduleBlock
            practiceRepository.Delete(scheduleBlockUpdated);
            UserScheduleBlock scheduleBlockDeleted = practiceRepository.ScheduleBlocks.OfType<UserScheduleBlock>().WithId(scheduleBlockUpdated.Id);
            Assert.IsNull(scheduleBlockDeleted);
        }

        [TestMethod]
        public void TestAppointmentCategory()
        {
            var practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            var comparison = new Objects.ObjectComparison
                {
                    CompareChildren = false,
                    MaxDifferences = 100
                };

            //Create AppointmentCategory
            AppointmentCategory appointmentCategory = Common.CreateAppointmentCategory();

            //Save AppointmentCategory
            practiceRepository.Save(appointmentCategory);
            Assert.IsTrue(appointmentCategory.Id > 0);

            //Get Back AppointmentCategory
            AppointmentCategory appointmentCategoryRefetched = practiceRepository.AppointmentCategories.WithId(appointmentCategory.Id);
            Assert.IsNotNull(appointmentCategoryRefetched);
            bool areSame = comparison.Compare(appointmentCategory, appointmentCategoryRefetched);
            Assert.IsTrue(areSame);

            //Update AppointmentCategory details
            appointmentCategoryRefetched.HexColor = "000000";
            appointmentCategoryRefetched.Name = "Updated AppointmentCategory";
            practiceRepository.Save(appointmentCategoryRefetched);

            AppointmentCategory appointmentCategoryUpdated = practiceRepository.AppointmentCategories.WithId(appointmentCategory.Id);
            areSame = comparison.Compare(appointmentCategoryRefetched, appointmentCategoryUpdated);
            Assert.IsTrue(areSame);

            //Delete AppointmentCategory
            practiceRepository.Delete(appointmentCategoryUpdated);
            AppointmentCategory scheduleBlockDeleted = practiceRepository.AppointmentCategories.WithId(appointmentCategoryUpdated.Id);
            Assert.IsNull(scheduleBlockDeleted);
        }

        /// <summary>
        ///   Tests saving of Employee hours
        /// </summary>
        [TestMethod]
        public void TestEmployeeHour()
        {
            var comparison = new Objects.ObjectComparison
            {
                CompareChildren = false,
                MaxDifferences = 100
            };

            //Create new user
            var user = new User();
            user.FirstName = "Test";
            user.LastName = "UserEmail";
            Common.PracticeRepository.Save(user);

            //create employee hour with Clock In time
            var employeeHour = new EmployeeHour();
            employeeHour.StartDateTime = new DateTime(2010, 1, 1, 10, 15, 0);
            employeeHour.UserId = user.Id;
            employeeHour.Comment = "Test Employee Clock In time";
            Common.PracticeRepository.Save(employeeHour);

            //Check to see if same
            EmployeeHour employeeHourRefetched = Common.PracticeRepository.EmployeeHours.WithId(employeeHour.Id);
            bool areSame = comparison.Compare(employeeHour, employeeHourRefetched);
            Assert.IsTrue(areSame);

            //update employee hour with Clock out Time
            employeeHour.EndDateTime = new DateTime(2010, 1, 1, 12, 15, 0);
            employeeHour.Comment = "Test Employee Clock out time";
            Common.PracticeRepository.Save(employeeHour);

            //Check to see if same
            employeeHourRefetched = Common.PracticeRepository.EmployeeHours.WithId(employeeHour.Id);
            areSame = comparison.Compare(employeeHour, employeeHourRefetched);
            Assert.IsTrue(areSame);
        }
    }
}