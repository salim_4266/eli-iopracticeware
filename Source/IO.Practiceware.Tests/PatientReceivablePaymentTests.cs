﻿using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model.Legacy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class PatientReceivablePaymentTests : TestBase
    {
        private readonly ILegacyPracticeRepository _legacyPracticeRepository = Common.ServiceProvider.GetService<ILegacyPracticeRepository>();

        protected override void OnAfterTestInitialize()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose(ADODBVbScripts.GeneralMetadata);
        }

        // Tests the insert and delete triggers on PatientReceivablePayments
        [TestMethod]
        public void TestDeletePatientReceivablePaymentNotBatch()
        {
            // get the first patient receivable payment that is not an adjustment
            PatientReceivablePayment payment = _legacyPracticeRepository.PatientReceivablePayments.First(a => a.PaymentType != "=");
            int id = payment.PaymentId;
            Assert.IsTrue(id > 0);

            // delete the patient receivable payment and check to see that it does not exist anymore
            _legacyPracticeRepository.Delete(payment);
            PatientReceivablePayment paymentDeleted = _legacyPracticeRepository.PatientReceivablePayments.FirstOrDefault(a => a.PaymentId == id);
            Assert.IsNull(paymentDeleted);
        }
    }
}