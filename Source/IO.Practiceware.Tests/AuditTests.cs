﻿using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Presentation;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class AuditTests : TestBase
    {
        private const string TaskActivityIdName = "Id";
        private const string TaskActivityDbName = "model.TaskActivityTypes";
        private int _taskActivityId;
        private IPracticeRepository _practiceRepository;
        private IAuditRepository _auditRepository;

        protected override void OnAfterTestInitialize()
        {
            _practiceRepository = Common.ServiceProvider.GetService<IPracticeRepository>();
            _auditRepository = Common.ServiceProvider.GetService<IAuditRepository>();

            DeleteAuditEntriesData();

            var taskActivityType = Common.CreateTaskActivityType();
            _practiceRepository.Save(taskActivityType);
            _taskActivityId = taskActivityType.Id;
        }

        protected override void OnAfterTestCleanup()
        {
            _practiceRepository = null;
            _auditRepository = null;
            _taskActivityId = 0;
        }

        private static void DeleteAuditEntriesData()
        {
            // Clear out AuditEntriesData so they don't interfere with unit tests
            using (IDbConnection connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Open();
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT 1 AS Value INTO #NoCheck; DELETE FROM AuditEntryChanges; DROP TABLE #NoCheck;";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "SELECT 1 AS Value INTO #NoCheck; DELETE FROM AuditEntries; DROP TABLE #NoCheck;";
                    cmd.ExecuteNonQuery();
                }
            }
        }
        [TestMethod]
        public void TestLogEntriesServerNameInsertTrigger()
        {
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose("INSERT INTO dbo.LogEntries (Id, Source, Message, SeverityId, DateTime) VALUES " +
                                                                                                  "(NEWID(), \'TestSource\', 'TestMessage\', 0, GETDATE())");

            string serverName = _auditRepository.LogEntries.First(i => i.Source == "TestSource" && i.Message == "TestMessage").ServerName;
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(DbConnectionFactory.PracticeRepository.ConnectionString);
            if (sqlConnectionStringBuilder.DataSource.Equals("localhost", StringComparison.OrdinalIgnoreCase)) sqlConnectionStringBuilder.DataSource = Environment.MachineName;
            Assert.IsTrue(serverName.ToUpper() == sqlConnectionStringBuilder.DataSource.ToUpper());
        }

        [TestMethod]
        public void TestUpdateTriggerNoChange()
        {
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            _practiceRepository.Save(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in auditEntryQuery
                                                                 where aec.AuditEntryId == ae.Id
                                                                 select aec;

            Assert.IsNull(auditEntryQuery.FirstOrDefault());
            Assert.IsNull(auditEntryChangeQuery.FirstOrDefault());
        }

        [TestMethod]
        public void TestServerNameUpdateTrigger()
        {
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            refetchedTaskActivityType.AlertDefault = !refetchedTaskActivityType.AlertDefault;
            _practiceRepository.Save(refetchedTaskActivityType);


            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(DbConnectionFactory.PracticeRepository.ConnectionString);
            if (sqlConnectionStringBuilder.DataSource.Equals("localhost", StringComparison.OrdinalIgnoreCase)) sqlConnectionStringBuilder.DataSource = Environment.MachineName;
            Assert.IsTrue(auditEntryQuery.First().ServerName.ToUpper() == sqlConnectionStringBuilder.DataSource.ToUpper());
        }

        [TestMethod]
        public void TestServerNameDeleteTrigger()
        {
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            _practiceRepository.Delete(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(DbConnectionFactory.PracticeRepository.ConnectionString);
            if (sqlConnectionStringBuilder.DataSource.Equals("localhost", StringComparison.OrdinalIgnoreCase)) sqlConnectionStringBuilder.DataSource = Environment.MachineName;
            Assert.IsTrue(auditEntryQuery.First().ServerName.ToUpper() == sqlConnectionStringBuilder.DataSource.ToUpper());
        }

        [TestMethod]
        public void TestInsertTrigger()
        {
            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == _taskActivityId &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;


            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().ChangeTypeId == 0);
            Assert.IsTrue(auditEntryQuery.First().ObjectName == "model.TaskActivityTypes");

            int numChanges = typeof(TaskActivityType).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Count() - 2;
            Assert.IsTrue(auditEntryChangeQuery.Count() == numChanges);

            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().KeyNames == "[" + TaskActivityIdName + "]");
            Assert.IsTrue(auditEntryQuery.First().KeyValueNumeric == _taskActivityId);
        }

        [TestMethod]
        public void TestUpdateTriggerOneColumnChange()
        {
            BackgroundThreadOnlyAttribute.IsEnabled = false;
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            int oldValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            refetchedTaskActivityType.AlertDefault = !refetchedTaskActivityType.AlertDefault;
            _practiceRepository.Save(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == _taskActivityId && ae.ChangeType == AuditEntryChangeType.Update &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;


            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().ChangeTypeId == 1);
            Assert.IsTrue(auditEntryQuery.First().ObjectName == "model.TaskActivityTypes");

            Assert.IsTrue(auditEntryChangeQuery.Count() == 1);
            Assert.IsTrue(auditEntryChangeQuery.First().ColumnName == "AlertDefault");
            Assert.IsTrue(auditEntryChangeQuery.First().OldValue == oldValue.ToString());

            Assert.IsTrue(auditEntryQuery.First().KeyNames == "[" + TaskActivityIdName + "]");
            Assert.IsTrue(auditEntryQuery.First().KeyValueNumeric == _taskActivityId);
        }

        [TestMethod]
        public void TestUpdateTriggerTwoColumnChange()
        {
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            int oldAlertDefaultValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            refetchedTaskActivityType.AlertDefault = !refetchedTaskActivityType.AlertDefault;
            int oldOrdinalIdValue = refetchedTaskActivityType.OrdinalId;
            refetchedTaskActivityType.OrdinalId += 1;
            _practiceRepository.Save(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;


            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == _taskActivityId &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;
            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().KeyNames == "[" + TaskActivityIdName + "]");
            Assert.IsTrue(auditEntryQuery.First().KeyValueNumeric == _taskActivityId);
            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "AlertDefault" &&
                                                             aec.OldValueNumeric == oldAlertDefaultValue)
                                                      == 1);
            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "OrdinalId" &&
                                                             aec.OldValueNumeric == oldOrdinalIdValue)
                                                      == 1);
        }

        [TestMethod]
        public void TestUpdateTriggerMultipleRowChange()
        {
            const int taskCount = 4;
            for (int i = 0; i < taskCount; i++)
            {
                var taskActivityType = new TaskActivityType { Name = "Contact Lens Queation", DefaultContent = "Problem with new lenses:", OrdinalId = 1, AlertDefault = true, AssignIndividuallyDefault = true, LinkToPatientDefault = true };
                _practiceRepository.Save(taskActivityType);
            }

            var refetchedTaskActivityTypes = _practiceRepository.TaskActivityTypes.ToList();
            // Assume task activity types have saved correctly.
            // ReSharper disable PossibleNullReferenceException
            int oldOrdinalIdValue = refetchedTaskActivityTypes.FirstOrDefault().OrdinalId;
            int oldAlertDefaultValue = (refetchedTaskActivityTypes.FirstOrDefault().AlertDefault) ? 1 : 0;
            // ReSharper restore PossibleNullReferenceException
            foreach (var taskActivityType in refetchedTaskActivityTypes)
            {
                taskActivityType.OrdinalId = oldOrdinalIdValue + 1;
                taskActivityType.AlertDefault = !taskActivityType.AlertDefault;
            }
            _practiceRepository.Save(refetchedTaskActivityTypes);
            // TaskCount + 1 because we created a task activity in TestInitialize.
            const int adjustedTaskCount = taskCount + 1;
            var taskActivityTypeIds = new List<string>(adjustedTaskCount);
            refetchedTaskActivityTypes.ToList().ForEach(tat => taskActivityTypeIds.Add(tat.Id.ToString()));

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]"
                                                     select ae;

            var keyValues = refetchedTaskActivityTypes.Select(i => (long)i.Id);

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" && ae.ChangeType == AuditEntryChangeType.Update &&
                                                                       aec.AuditEntryId == ae.Id && keyValues.Contains(ae.KeyValueNumeric.Value)
                                                                 select aec;

            Assert.IsTrue(auditEntryQuery.Count() == adjustedTaskCount);
            Assert.IsTrue(auditEntryChangeQuery.Count() == adjustedTaskCount * 2);

            var alertDefaultChanges = auditEntryChangeQuery.Where(aec => aec.ColumnName == "AlertDefault" && aec.OldValueNumeric == oldAlertDefaultValue);
            Assert.IsTrue(alertDefaultChanges.Count() == adjustedTaskCount);
            var ordinalIdChanges = auditEntryChangeQuery.Where(aec => aec.ColumnName == "OrdinalId" && aec.OldValueNumeric == oldOrdinalIdValue);
            Assert.IsTrue(ordinalIdChanges.Count() == adjustedTaskCount);
        }

        [TestMethod]
        public void TestDeleteTriggerOneRowDelete()
        {
            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(_taskActivityId);
            _practiceRepository.Delete(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 2 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == _taskActivityId
                                                     select ae;


            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == _taskActivityId &&
                                                                       aec.AuditEntryId == ae.Id && ae.ChangeType == AuditEntryChangeType.Delete
                                                                 select aec;

            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.Count() == 1);
            // "TaskActivities" property is not defined as a column for the TaskActivityType table.  So we decrement numChanges manually.
            int numChanges = typeof(TaskActivityType).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Count() - 2;
            Assert.IsTrue(auditEntryChangeQuery.Count() == numChanges);


            var auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "AlertDefault");
            int oldBitValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "AssignIndividuallyDefault");
            oldBitValue = (refetchedTaskActivityType.AssignIndividuallyDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "DefaultContent");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValue == refetchedTaskActivityType.DefaultContent);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "Id");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == refetchedTaskActivityType.Id);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "LinkToPatientDefault");
            oldBitValue = (refetchedTaskActivityType.LinkToPatientDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "Name");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValue == refetchedTaskActivityType.Name);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "OrdinalId");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == refetchedTaskActivityType.OrdinalId);
        }

        [TestMethod]
        public void TestDeleteTriggerTwoRowDelete()
        {
            var taskActivityType = new TaskActivityType { Name = "Contact Lens Queation", DefaultContent = "Problem with new lenses:", OrdinalId = 1, AlertDefault = true, AssignIndividuallyDefault = true, LinkToPatientDefault = true };
            _practiceRepository.Save(taskActivityType);

            var refetchedTaskActivityTypes = _practiceRepository.TaskActivityTypes.ToList();
            DbConnectionFactory.PracticeRepository.RunScriptAndDispose("DELETE FROM model.TaskActivityTypes WHERE Id IN ({0})".FormatWith(refetchedTaskActivityTypes.Select(t => t.Id).Join(",")));

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 2 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]"
                                                     select ae;

            var keyValues = refetchedTaskActivityTypes.Select(i => (long)i.Id).ToArray();

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       aec.AuditEntryId == ae.Id && keyValues.Contains(ae.KeyValueNumeric.Value) && ae.ChangeTypeId == 2
                                                                 select aec;

            int taskCount = refetchedTaskActivityTypes.Count;
            Assert.IsTrue(auditEntryQuery.Count() == taskCount);

            // "TaskActivities" property is not defined as a column for the TaskActivityType table.  So we decrement numChanges manually.
            int numChanges = typeof(TaskActivityType).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Count() - 2;
            Assert.IsTrue(auditEntryChangeQuery.Count() == numChanges * taskCount);

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "AlertDefault") == taskCount);
            int oldBitValue = (refetchedTaskActivityTypes.First().AlertDefault) ? 1 : 0;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "AlertDefault"))
            {
                Assert.IsTrue(change.OldValueNumeric == oldBitValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "AssignIndividuallyDefault") == taskCount);
            oldBitValue = (refetchedTaskActivityTypes.First().AssignIndividuallyDefault) ? 1 : 0;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "AssignIndividuallyDefault"))
            {
                Assert.IsTrue(change.OldValueNumeric == oldBitValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "DefaultContent") == taskCount);
            string oldStringValue = refetchedTaskActivityTypes.First().DefaultContent;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "DefaultContent"))
            {
                Assert.IsTrue(change.OldValue == oldStringValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "Id") == taskCount);
            var idList = new List<string>();
            refetchedTaskActivityTypes.ForEach(tat => idList.Add(tat.Id.ToString()));
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "Id"))
            {
                Assert.IsTrue(idList.Contains(change.OldValue));
                idList.Remove(change.OldValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "LinkToPatientDefault") == taskCount);
            oldBitValue = (refetchedTaskActivityTypes.First().LinkToPatientDefault) ? 1 : 0;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "LinkToPatientDefault"))
            {
                Assert.IsTrue(change.OldValueNumeric == oldBitValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "Name") == taskCount);
            oldStringValue = refetchedTaskActivityTypes.First().Name;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "Name"))
            {
                Assert.IsTrue(change.OldValue == oldStringValue);
            }

            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "OrdinalId") == taskCount);
            int oldIntValue = refetchedTaskActivityTypes.First().OrdinalId;
            foreach (var change in auditEntryChangeQuery.Where(aec => aec.ColumnName == "OrdinalId"))
            {
                Assert.IsTrue(change.OldValueNumeric == oldIntValue);
            }
        }

        [TestMethod]
        public void TestStoredProcedureToRestoreOneColumnUpdatedAuditEntries()
        {
            var taskActivityType = Common.CreateTaskActivityType();
            _practiceRepository.Save(taskActivityType);
            int taskActivityId = taskActivityType.Id;

            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);
            bool oldAlert = refetchedTaskActivityType.AlertDefault;
            int oldValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            refetchedTaskActivityType.AlertDefault = !refetchedTaskActivityType.AlertDefault;
            _practiceRepository.Save(refetchedTaskActivityType);

            Assert.IsTrue(oldAlert != refetchedTaskActivityType.AlertDefault);
            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == taskActivityId && ae.ChangeType == AuditEntryChangeType.Update &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;


            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().ChangeTypeId == 1);
            Assert.IsTrue(auditEntryQuery.First().ObjectName == "model.TaskActivityTypes");

            Assert.IsTrue(auditEntryChangeQuery.Count() == 1);
            Assert.IsTrue(auditEntryChangeQuery.First().ColumnName == "AlertDefault");
            Assert.IsTrue(auditEntryChangeQuery.First().OldValueNumeric == oldValue);

            Assert.IsTrue(auditEntryQuery.First().KeyNames == "[" + TaskActivityIdName + "]");
            Assert.IsTrue(auditEntryQuery.First().KeyValueNumeric == taskActivityId);

            DbConnectionFactory.PracticeRepository.RunScript(@"
                SELECT CONVERT(uniqueidentifier, '{0}') AuditEntryId INTO #AuditEntryIdsToRestore FROM AuditEntries
                EXEC RestoreAuditEntries
                ".FormatWith(auditEntryQuery.First().Id));

            refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);

            Assert.IsTrue(oldAlert == refetchedTaskActivityType.AlertDefault);
        }

        [TestMethod]
        public void TestStoredProcedureToRestoreTwoColumnUpdatedAuditEntries()
        {
            var taskActivityType = Common.CreateTaskActivityType();
            _practiceRepository.Save(taskActivityType);
            int taskActivityId = taskActivityType.Id;

            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);
            bool oldAlert = refetchedTaskActivityType.AlertDefault;
            int oldAlertDefaultValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            refetchedTaskActivityType.AlertDefault = !refetchedTaskActivityType.AlertDefault;
            int oldOrdinalIdValue = refetchedTaskActivityType.OrdinalId;
            refetchedTaskActivityType.OrdinalId += 1;
            _practiceRepository.Save(refetchedTaskActivityType);

            Assert.IsTrue(oldAlert != refetchedTaskActivityType.AlertDefault);
            Assert.IsTrue(oldOrdinalIdValue != refetchedTaskActivityType.OrdinalId);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == taskActivityId &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;
            Assert.IsTrue(auditEntryQuery.Count() == 1);
            Assert.IsTrue(auditEntryQuery.First().KeyNames == "[" + TaskActivityIdName + "]");
            Assert.IsTrue(auditEntryQuery.First().KeyValueNumeric == taskActivityId);
            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "AlertDefault" &&
                                                             aec.OldValueNumeric == oldAlertDefaultValue)
                                                      == 1);
            Assert.IsTrue(auditEntryChangeQuery.Count(aec => aec.ColumnName == "OrdinalId" &&
                                                             aec.OldValueNumeric == oldOrdinalIdValue)
                                                      == 1);

            DbConnectionFactory.PracticeRepository.RunScript(@"
                SELECT CONVERT(uniqueidentifier, '{0}') AuditEntryId INTO #AuditEntryIdsToRestore FROM AuditEntries
                EXEC RestoreAuditEntries
                ".FormatWith(auditEntryQuery.First().Id));

            refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);

            Assert.IsTrue(oldAlert == refetchedTaskActivityType.AlertDefault);
            Assert.IsTrue(oldOrdinalIdValue == refetchedTaskActivityType.OrdinalId);
        }

        [TestMethod]
        public void TestStoredProcedureToRestoreDeletedRowFromAuditEntries()
        {
            var taskActivityType = Common.CreateTaskActivityType();
            _practiceRepository.Save(taskActivityType);
            int taskActivityId = taskActivityType.Id;

            var refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);
            _practiceRepository.Delete(refetchedTaskActivityType);

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 2 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                           ae.KeyValueNumeric == taskActivityId
                                                     select ae;

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" &&
                                                                       ae.KeyValueNumeric == taskActivityId && ae.ChangeType == AuditEntryChangeType.Delete &&
                                                                       aec.AuditEntryId == ae.Id
                                                                 select aec;

            Assert.IsTrue(auditEntryQuery.Count() == 1);

            // "TaskActivities" property is not defined as a column for the TaskActivityType table.  So we decrement numChanges manually.
            int numChanges = typeof(TaskActivityType).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Count() - 2;
            Assert.IsTrue(auditEntryChangeQuery.Count() == numChanges);

            var auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "AlertDefault");
            int oldBitValue = (refetchedTaskActivityType.AlertDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "AssignIndividuallyDefault");
            oldBitValue = (refetchedTaskActivityType.AssignIndividuallyDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "DefaultContent");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValue == refetchedTaskActivityType.DefaultContent);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "Id");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == refetchedTaskActivityType.Id);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "LinkToPatientDefault");
            oldBitValue = (refetchedTaskActivityType.LinkToPatientDefault) ? 1 : 0;
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == oldBitValue);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "Name");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValue == refetchedTaskActivityType.Name);

            auditChange = auditEntryChangeQuery.FirstOrDefault(change => change.ColumnName == "OrdinalId");
            Assert.IsNotNull(auditChange);
            Assert.IsTrue(auditChange.OldValueNumeric == refetchedTaskActivityType.OrdinalId);

            //make sure that no row with taskActivityId in TaskActivityTypes
            refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);
            Assert.IsNull(refetchedTaskActivityType);

            DbConnectionFactory.PracticeRepository.RunScript(@"
                SELECT CONVERT(uniqueidentifier, '{0}') AuditEntryId INTO #AuditEntryIdsToRestore FROM AuditEntries
                EXEC RestoreAuditEntries
                ".FormatWith(auditEntryQuery.First().Id));

            //assert the row exists with taskActivityId which is res-inserted in TaskActivityTypes
            refetchedTaskActivityType = _practiceRepository.TaskActivityTypes.WithId(taskActivityId);
            Assert.IsNotNull(refetchedTaskActivityType);
        }

        [TestMethod]
        public void TestStoredProcedureToRestoreMultipleRowUpdatesFromAuditEntries()
        {
            const int taskCount = 4;
            for (int i = 0; i < taskCount; i++)
            {
                var taskActivityType = new TaskActivityType { Name = "Contact Lens Queation", DefaultContent = "Problem with new lenses:", OrdinalId = 1, AlertDefault = true, AssignIndividuallyDefault = true, LinkToPatientDefault = true };
                _practiceRepository.Save(taskActivityType);
            }
            var fetchedTaskActivityTypes = _practiceRepository.TaskActivityTypes.Include(t => t.TaskActivities).ToList();
            var refetchedTaskActivityTypes = _practiceRepository.TaskActivityTypes.Include(t => t.TaskActivities).ToList();
            // Assume task activity types have saved correctly.
            // ReSharper disable PossibleNullReferenceException
            int oldOrdinalIdValue = refetchedTaskActivityTypes.FirstOrDefault().OrdinalId;
            int oldAlertDefaultValue = (refetchedTaskActivityTypes.FirstOrDefault().AlertDefault) ? 1 : 0;
            // ReSharper restore PossibleNullReferenceException
            foreach (var taskActivityType in refetchedTaskActivityTypes)
            {
                taskActivityType.OrdinalId = oldOrdinalIdValue + 1;
                taskActivityType.AlertDefault = !taskActivityType.AlertDefault;
            }
            _practiceRepository.Save(refetchedTaskActivityTypes);

            Assert.AreNotEqual(fetchedTaskActivityTypes, refetchedTaskActivityTypes);
            // TaskCount + 1 because we created a task activity in TestInitialize.
            const int adjustedTaskCount = taskCount + 1;
            var taskActivityTypeIds = new List<string>(adjustedTaskCount);
            refetchedTaskActivityTypes.ToList().ForEach(tat => taskActivityTypeIds.Add(tat.Id.ToString()));

            IQueryable<AuditEntry> auditEntryQuery = from ae in _auditRepository.AuditEntries
                                                     where
                                                           ae.ObjectName == TaskActivityDbName &&
                                                           ae.ChangeTypeId == 1 &&
                                                           ae.KeyNames == "[" + TaskActivityIdName + "]"
                                                     select ae;

            var keyValues = refetchedTaskActivityTypes.Select(i => (long)i.Id).ToArray();

            IQueryable<AuditEntryChange> auditEntryChangeQuery = from aec in _auditRepository.AuditEntryChanges
                                                                 from ae in _auditRepository.AuditEntries
                                                                 where ae.KeyNames == "[" + TaskActivityIdName + "]" && ae.ChangeType == AuditEntryChangeType.Update &&
                                                                       aec.AuditEntryId == ae.Id && keyValues.Contains(ae.KeyValueNumeric.Value)
                                                                 select aec;

            Assert.IsTrue(auditEntryQuery.Count() == adjustedTaskCount);
            Assert.IsTrue(auditEntryChangeQuery.Count() == adjustedTaskCount * 2);

            var alertDefaultChanges = auditEntryChangeQuery.Where(aec => aec.ColumnName == "AlertDefault" && aec.OldValueNumeric == oldAlertDefaultValue);
            Assert.IsTrue(alertDefaultChanges.Count() == adjustedTaskCount);
            var ordinalIdChanges = auditEntryChangeQuery.Where(aec => aec.ColumnName == "OrdinalId" && aec.OldValueNumeric == oldOrdinalIdValue);
            Assert.IsTrue(ordinalIdChanges.Count() == adjustedTaskCount);

            string auditEntryIds = "";
            List<Guid> getAuditEntryIds = auditEntryQuery.Select(a => a.Id).ToList();
            foreach (Guid id in getAuditEntryIds)
            {
                if (auditEntryIds.IsNullOrEmpty())
                    auditEntryIds = " '" + id + "' ";
                else
                    auditEntryIds = auditEntryIds + ", '" + id + "' ";
            }

            DbConnectionFactory.PracticeRepository.RunScript(@"
                SELECT Id as AuditEntryId INTO #AuditEntryIdsToRestore FROM AuditEntries where Id in ({0})
                EXEC RestoreAuditEntries
                ".FormatWith(auditEntryIds));

            refetchedTaskActivityTypes = _practiceRepository.TaskActivityTypes.Include(t => t.TaskActivities).ToList();

            int tskCount = refetchedTaskActivityTypes.Count();
            for (int i = 0; i < tskCount; i++)
            {
                Assert.IsTrue(fetchedTaskActivityTypes[i].Id == refetchedTaskActivityTypes[i].Id);
                Assert.IsTrue(fetchedTaskActivityTypes[i].Name == refetchedTaskActivityTypes[i].Name);
                Assert.IsTrue(fetchedTaskActivityTypes[i].OrdinalId == refetchedTaskActivityTypes[i].OrdinalId);
                Assert.IsTrue(fetchedTaskActivityTypes[i].LinkToPatientDefault == refetchedTaskActivityTypes[i].LinkToPatientDefault);
                Assert.IsTrue(fetchedTaskActivityTypes[i].DefaultContent == refetchedTaskActivityTypes[i].DefaultContent);
                Assert.IsTrue(fetchedTaskActivityTypes[i].AssignIndividuallyDefault == refetchedTaskActivityTypes[i].AssignIndividuallyDefault);
                Assert.IsTrue(fetchedTaskActivityTypes[i].AlertDefault == refetchedTaskActivityTypes[i].AlertDefault);
            }
        }
    }
}
