﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using IO.Practiceware.DataConversion;
using IO.Practiceware.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;

namespace IO.Practiceware.Tests
{
    [TestClass]
    public class FinancialDataConverterTest : TestBase
    {
        private readonly IServiceProvider _serviceProvider;
        public FinancialDataConverterTest()
        {
            _serviceProvider = Bootstrapper.Run<IServiceProvider>();
        }

        [TestMethod]
        public void TestFinancialConverterRun()
        {
            if (DataConversionConfiguration.Current != null)
            {
                var arguments = DataConversion.DataConversionConfiguration.Current.Converters.Where(c => c.ConverterName.ToLower() == "financialsdataconverter" && c.IsEnabled == true).Select(c => c).FirstOrDefault();
                if (arguments != null)
                {
                    var instance = (IDataConverter) _serviceProvider.GetService(arguments.Converter);
                    instance.Run(arguments);

                    IList<PatientInsurance> cachedPatientInsurances = PracticeRepository.PatientInsurances.ToList();
                    IList<InsurancePolicy> cachedInsurancePolicies = PracticeRepository.InsurancePolicies.ToList();
                    IList<Patient> cachedPatients = PracticeRepository.Patients.ToList();


                    foreach (DataRow row in arguments.DataTable.Rows)
                    {

                        var oldPatientId = row.GetValueOrDefault<string>("OldPatientId");

                        if (string.IsNullOrEmpty(oldPatientId))
                        {
                            FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPatient++;
                            continue;
                        }

                        Patient patient = cachedPatients.Where(p => p.PriorPatientCode == oldPatientId).Select(p => p).FirstOrDefault();

                        if (patient == null)
                        {
                            continue;
                        }

                        for (int i = 1; i <= 3; i++)
                        {
                            var oldInsurerId = row.GetValueOrDefault<string>("OldInsurer" + i + "Id").ToUpper();

                            if (string.IsNullOrEmpty(oldInsurerId) || oldInsurerId == "0")
                            {
                                i = 4;
                                continue;
                            }


                            int insurerId = PracticeRepository.Insurers.Where(ci => ci.PriorInsurerCode == oldInsurerId).Select(ci => ci.Id).FirstOrDefault();

                            if (insurerId == 0)
                            {
                                continue;
                            }

                            var iOInsurerRelationship = row.GetValueOrDefault<string>("IOInsurer" + i + "Relationship");

                            if (string.IsNullOrEmpty(iOInsurerRelationship))
                            {
                                continue;
                            }

                            int policyHolderId;

                            if (iOInsurerRelationship == "Y")
                            {
                                policyHolderId = patient.Id;
                            }
                            else
                            {
                                if (!(new[] {"C", "S"}.Contains(iOInsurerRelationship)))
                                {
                                    i = 4;
                                    continue;
                                }

                                policyHolderId = PracticeRepository.GetPolicyHolder(row, i);

                                if (policyHolderId == -1)
                                {
                                    i = 4;
                                    continue;
                                }

                                if (policyHolderId == 0)
                                {
                                    var postPatients = PracticeRepository.PostPolicyHolder(row, i);
                                    policyHolderId = postPatients.Id;
                                    cachedPatients.Add(postPatients);
                                }
                            }

                            InsurancePolicy[] policyHolderPolicies = cachedInsurancePolicies.Where(ci => ci.PolicyholderPatientId == policyHolderId).Select(ci => ci).ToArray();

                            var insuredPatient = cachedPatientInsurances.Where(pi => pi.InsuredPatientId == policyHolderId).Select(pi => pi);

                            //As per John, There will be only one policy for parent policy holder.
                            if (policyHolderPolicies.Any() && insuredPatient.Any())
                            {
                                continue;
                            }

                            int polHoldInsCount = policyHolderPolicies.Length + 1;

                            string insurerType = row.GetValueOrDefault<string>("IOInsurer" + i + "Type");

                            /* PatientInsurance */

                            var patientInsurance = new PatientInsurance
                            {
                                InsuredPatientId = policyHolderId,
                                OrdinalId = polHoldInsCount,
                                IsDeleted = false, //Status = "C",
                                PolicyholderRelationshipType = PolicyHolderRelationshipType.Self
                            };

                            var policyCode = row.GetValueOrDefault<string>("Insurer" + i + "MemberNumber").ToAlphanumeric();
                            var matchingPolicy = cachedInsurancePolicies.Where(ip => ip.PolicyCode == policyCode).Select(ip => ip).FirstOrDefault();
                            if (matchingPolicy == null)
                            {
                                patientInsurance.InsurancePolicy = new InsurancePolicy
                                {
                                    InsurerId = insurerId,
                                    PolicyCode = policyCode,
                                    GroupCode = row.GetValueOrDefault<string>("Insurer" + i + "GroupNumber").ToAlphanumeric(),
                                    Copay = row.GetValueOrDefault<int>("Insurer" + i + "Copay"),
                                    StartDateTime = DateTime.ParseExact((DataConversion.Utilities.ValidateDate(row.GetValueOrDefault<string>("Insurer" + i + "StartDate")) == string.Empty ? row.GetValueOrDefault<string>("GoLiveDate") : row.GetValueOrDefault<string>("Insurer" + i + "StartDate")), "yyyyMMdd", CultureInfo.InvariantCulture,
                                        DateTimeStyles.None),
                                    PolicyholderPatientId = cachedPatients.Where(p => p.Id == policyHolderId).Select(p => p.Id).FirstOrDefault(),
                                };

                            }
                            else
                            {
                                patientInsurance.InsurancePolicyId = matchingPolicy.Id;
                            }

                            string endDateTime = DataConversion.Utilities.ValidateDate(row.GetValueOrDefault<string>("Insurer" + i + "EndDate"));
                            if (!string.IsNullOrEmpty(endDateTime))
                            {
                                patientInsurance.EndDateTime = DateTime.ParseExact(endDateTime, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            switch (insurerType)
                            {
                                case "V":
                                    patientInsurance.InsuranceType = InsuranceType.Vision;
                                    break;
                                case "A":
                                    patientInsurance.InsuranceType = InsuranceType.Ambulatory;
                                    break;
                                case "W":
                                    patientInsurance.InsuranceType = InsuranceType.WorkersComp;
                                    break;
                                default:
                                    patientInsurance.InsuranceType = InsuranceType.Vision;
                                    break;
                            }

                            patientInsurance.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                            var matchingPolicies = from pi in cachedPatientInsurances
                                where pi.InsuredPatientId == patientInsurance.InsuredPatientId &&
                                      pi.InsurancePolicyId == patientInsurance.InsurancePolicyId
                                select pi;

                            bool isDuplicateFinancialRecord = matchingPolicies.Any();

                            if (!isDuplicateFinancialRecord)
                            {
                                Assert.IsNotNull(cachedPatientInsurances.Where(pi => pi.InsuredPatientId == patientInsurance.InsuredPatientId &&
                                                                                     pi.InsurancePolicyId == patientInsurance.InsurancePolicyId &&
                                                                                     pi.OrdinalId == patientInsurance.OrdinalId &&
                                                                                     pi.InsuranceType == patientInsurance.InsuranceType).Select(pi => pi).FirstOrDefault(), "Could not validate Patient Insurances");
                                if (patientInsurance.InsurancePolicy != null)
                                {
                                    Assert.IsNotNull(cachedInsurancePolicies.Where(ip => ip.InsurerId == patientInsurance.InsurancePolicy.InsurerId &&
                                                                                         ip.PolicyCode == patientInsurance.InsurancePolicy.PolicyCode &&
                                                                                         ip.GroupCode == patientInsurance.InsurancePolicy.GroupCode &&
                                                                                         ip.Copay == patientInsurance.InsurancePolicy.Copay &&
                                                                                         ip.StartDateTime == patientInsurance.InsurancePolicy.StartDateTime &&
                                                                                         ip.PolicyholderPatientId == patientInsurance.InsurancePolicy.PolicyholderPatientId).Select(ip => ip).FirstOrDefault(), "Could not validate Insurance Policies");
                                }
                            }
                            else
                            {
                                continue;
                            }

                            int? primaryPatientPolicyHolderId = cachedInsurancePolicies.Where(ip => ip.Id == patientInsurance.InsurancePolicyId).Select(ci => ci.PolicyholderPatientId).FirstOrDefault();

                            if (policyHolderId == primaryPatientPolicyHolderId)
                                continue;

                            if (primaryPatientPolicyHolderId == 0)
                            {
                                var patientInsuranceWithoutPolicyHolder = cachedPatientInsurances.Where(ip => ip.InsurancePolicy != null && ip.InsurancePolicy.PolicyholderPatientId == 0).Select(pi => pi).FirstOrDefault();
                                if (patientInsuranceWithoutPolicyHolder != null)
                                {
                                    patientInsuranceWithoutPolicyHolder.InsurancePolicy.PolicyholderPatientId = policyHolderId;
                                    patientInsuranceWithoutPolicyHolder.PolicyholderRelationshipType = PolicyHolderRelationshipType.Self;
                                    PracticeRepository.Save(patientInsuranceWithoutPolicyHolder);
                                    Assert.IsNotNull(cachedPatientInsurances.Where(ip => ip.InsurancePolicy.PolicyholderPatientId == policyHolderId &&
                                                                                         ip.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self).Select(ip => ip).FirstOrDefault(), "Could not validate Insurance Policies after updating poliyholder partient for no Id");
                                }

                            }
                        }
                    }


                }

            }
        }
    }
}
