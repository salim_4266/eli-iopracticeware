﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace IO.Practiceware.ClientUpdater
{
    public static class Enums
    {
        /// <summary>
        /// Removes the specified value from the flags enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The type.</param>
        /// <param name="toRemove">To remove.</param>
        /// <returns></returns>
        public static T Remove<T>(this Enum value, T toRemove)
        {
            try
            {
                return (T)(((dynamic)value & ~(dynamic)toRemove));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not remove value {0} from enumerated value '{1}'.",
                        value, toRemove)
                        , ex);
            }
        }

        /// <summary>
        /// Adds the specified value from the flags enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The type.</param>
        /// <param name="toAdd">To remove.</param>
        /// <returns></returns>
        public static T Add<T>(this Enum value, T toAdd)
        {
            try
            {
                return (T)(((dynamic)value | (dynamic)toAdd));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not add value {0} from enumerated value '{1}'.",
                        value, toAdd)
                        , ex);
            }
        }

    }

    public static class Objects
    {
        /// <summary>
        /// Ensures the value is not null/default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static T EnsureNotDefault<T>(this T value, string message = null)
        {
            if (Equals(value, default(T)))
            {
                throw message == null ? new InvalidOperationException("Value should not be null") : new InvalidOperationException(message);
            }
            return value;
        }
    }

    public static class Enumerables
    {
        /// <summary>
        /// Joins the specified string property based on the specified selector, separated by the specified delimiter.
        /// </summary>
        /// <typeparam name="T">The type of the collection values</typeparam>
        /// <param name="source">The values to join</param>
        /// <param name="selector">Selector delegate to choose the property to join on</param>
        /// <param name="delimiter">The delimiter string used to separate each string value</param>
        /// <param name="excludeEmptyItems">if set to <c>true</c> [exclude empty items].</param>
        /// <returns></returns>
        public static string Join<T>(this IEnumerable<T> source, Func<T, string> selector, string delimiter = ", ", bool excludeEmptyItems = false)
        {
            return source.Select(selector).Join(delimiter, excludeEmptyItems);
        }

        /// <summary>
        ///   Joins the specified strings values, separated by the specified delimiter.
        /// </summary>
        /// <param name = "values">The values.</param>
        /// <param name = "delimiter">The delimiter.</param>
        /// <param name = "excludeEmptyItems">if set to <c>true</c> [exclude empty items].</param>
        /// <returns></returns>
        public static string Join(this IEnumerable values, string delimiter = ", ", bool excludeEmptyItems = false)
        {
            var sb = new StringBuilder();

            foreach (object value in values)
            {
                if (!excludeEmptyItems || (value != null && value.ToString().Trim() != string.Empty))
                {
                    sb.Append(value);
                    sb.Append(delimiter);
                }
            }
            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - delimiter.Length, delimiter.Length);
            }
            return sb.ToString();
        }

    }

    public static class Strings
    {
        /// <summary>
        ///   Formats the format string with the specified arguments.
        /// </summary>
        /// <param name = "format">The format.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        public static string FormatWith(this string format, params object[] arguments)
        {
            return string.Format(format, arguments);
        }
    }

    /// <summary>
    ///   Provides a mechanism for retrying delegates.
    /// </summary>
    public static class RetryUtility
    {
        /// <summary>
        ///   Executes the action with retry handling.
        /// </summary>
        /// <typeparam name="TResult"> The type of the result. </typeparam>
        /// <param name="action"> The action. </param>
        /// <param name="numberOfRetries"> The number of retries. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="shouldRetry"> The should retry. </param>
        /// <returns> </returns>
        public static TResult ExecuteWithRetry<TResult>(this Func<TResult> action, int numberOfRetries, TimeSpan retryInterval = default(TimeSpan), Func<Exception, bool> shouldRetry = null)
        {
            if (retryInterval == default(TimeSpan)) retryInterval = TimeSpan.FromMilliseconds(100);

            var exceptionsThrown = new List<Exception>();
            var retriesLeft = numberOfRetries;
            TResult result = default(TResult);
            do
            {
                try
                {
                    result = action();
                    return result;
                }
                catch (Exception ex)
                {
                    exceptionsThrown.Add(ex);
                    if ((shouldRetry != null && !shouldRetry(ex)) || retriesLeft <= 0)
                    {
                        throw new AggregateException("Operation failed to complete after {0}/{1} retries. See list of thrown exceptions for every attempt"
                            .FormatWith(exceptionsThrown.Count, numberOfRetries), exceptionsThrown);
                    }
                    Thread.Sleep(retryInterval);
                }
            } while (retriesLeft-- > 0);
            return result;
        }

        /// <summary>
        ///   Executes the action with retry handling.
        /// </summary>
        /// <param name="action"> The action. </param>
        /// <param name="numberOfRetries"> The number of retries. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="shouldRetry"> The should retry. </param>
        public static void ExecuteWithRetry(this Action action, int numberOfRetries, TimeSpan retryInterval, Func<Exception, bool> shouldRetry = null)
        {
            ExecuteWithRetry(new Func<object>(() =>
            {
                action();
                return null;
            }), numberOfRetries, retryInterval, shouldRetry);
        }
    }

}
