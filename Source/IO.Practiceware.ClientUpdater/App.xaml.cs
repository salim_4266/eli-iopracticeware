﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
#if !NO_RESTART_FROM_TEMP
using System.Diagnostics;
#endif

namespace IO.Practiceware.ClientUpdater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private bool _hasHandle;
        private Mutex _mutex;

        /// <summary>
        /// Updater instance
        /// </summary>
        public static Updater Updater { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Read current application name
            var currentUpdaterName = Path.GetFileName(IOConfiguration.CurrentUpdaterPath);

            // Evaluate temp copy name
            var updaterTempCopyName = string.Format("{0}.tmp{1}",
                // ReSharper disable once PossibleNullReferenceException
                Path.GetFileNameWithoutExtension(currentUpdaterName)
                    .Replace(".tmp", string.Empty), // Ensure we won't end up in infinite restarts to temp copy
                Path.GetExtension(currentUpdaterName));

            // unique id for mutex - Local prefix means it is scoped to terminal server session (we want to allow multiple users upgrading)
            _mutex = new Mutex(false, GetUpdaterMutexKey());
            _hasHandle = false;
            try
            {
                // Check if app instance is already running
                // Also give 1s to switch to temp copy
                _hasHandle = _mutex.WaitOne(1000, false);
                if (!_hasHandle)
                {
                    // Another instance running -> close
                    Shutdown();
                    return;
                }
            }
            catch (AbandonedMutexException)
            {
                // We have acquired mutex, but previous process abandoned it instead of releasing
                _hasHandle = true;
            }

            // --------
            // Mutex acquired, running single app instance
            // --------

            // Evaluate full path for temp copy
            // ReSharper disable once AssignNullToNotNullAttribute
            var updaterTempCopy = Path.Combine(IOConfiguration.LocalApplicationPath, updaterTempCopyName);

            // Cleanup a copy of Updater if it exists in temp
            if (!string.Equals(updaterTempCopy, IOConfiguration.CurrentUpdaterPath, StringComparison.OrdinalIgnoreCase))
            {
                FileSystem.TryDeleteFile(updaterTempCopy);
            }

            try
            {
                // Check for updates before UI shows up
                Updater = new Updater(e.Args);

                // Check if there is an update
                if (!Updater.CheckForUpdate()
                    || RestartFromTempLocation(IOConfiguration.CurrentUpdaterPath, updaterTempCopy, e.Args))
                {
                    Shutdown();
                }
            }
            catch (Exception ex)
            {
                Updater.LogMessage(ex.ToString());
                MessageBox.Show(string.Format("Update failed. Please, try again or contact support{0}{0}Error: {1}",
                    Environment.NewLine, ex.Message), "IO Practiceware", MessageBoxButton.OK, MessageBoxImage.Error);

                Shutdown(-1);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            if (_hasHandle)
                _mutex.ReleaseMutex();
        }

        /// <summary>
        /// Builds unique id of current updater instance
        /// </summary>
        /// <returns></returns>
        private static string GetUpdaterMutexKey()
        {
            var updaterKey = IOConfiguration.CurrentUpdaterPath
                .Replace(".tmp", string.Empty); // Make sure same key when restarted from temp location

            // Create Md5 from updater path
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(updaterKey)))
            {
                using (var md5 = MD5.Create())
                {
                    var hashBytes = md5.ComputeHash(ms);
                    updaterKey = BitConverter
                        .ToString(hashBytes)
                        .Replace("-", string.Empty)
                        .ToLower();
                }
            }

            return string.Format("Local\\UpdaterInstance_{0}", updaterKey);
        }

        /// <summary>
        /// Installer will shutdown Updater, since it updates it as well. So we ensure that we are running from temp location
        /// </summary>
        /// <param name="currentUpdaterLocation">The current updater location.</param>
        /// <param name="updaterTempCopy">The temporary updater location.</param>
        /// <param name="args"></param>
        /// <returns></returns>
// ReSharper disable UnusedParameter.Local
        private static bool RestartFromTempLocation(string currentUpdaterLocation, string updaterTempCopy, string[] args)
// ReSharper restore UnusedParameter.Local
        {
#if !NO_RESTART_FROM_TEMP
            try
            {
                if (!string.Equals(currentUpdaterLocation, updaterTempCopy, StringComparison.OrdinalIgnoreCase))
                {
                    File.Copy(currentUpdaterLocation, updaterTempCopy, true);
                    Process.Start(updaterTempCopy, string.Join(" ", args));
                    return true;
                }
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {}
#endif

            // Continue update from current location in case of errors or if already restarted from it
            return false;
        }
    }
}
