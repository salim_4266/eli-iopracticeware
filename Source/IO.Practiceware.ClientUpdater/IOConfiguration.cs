﻿using System;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using System.Xml;

namespace IO.Practiceware.ClientUpdater
{
    public enum ClientUpdateSource
    {
        WebService,
        ServerClientUpdateFolder,
        
        // TODO: make it obsolete in 9.0 or 10.0
        ServerNewVersionFolder
    }

    public class IOConfiguration
    {
        public const string ClientDataDirectory = "Pinpoint";

        private static Lazy<string> _localApplicationPath;
        private static Lazy<ClientUpdateSource> _updateSource;
        private static Lazy<string> _serverNewVersionFolderPath;
        private static Lazy<EndpointAddress> _clientUpdateServiceUrl;
        private static Lazy<string> _authenticationToken;
        private static Lazy<XmlElement> _configurationSource;
        private static Lazy<string> _currentUpdaterPath;
        private static Lazy<string> _serverClientUpdateFolderPath;
        private static Lazy<string> _serverDataPath;

        static IOConfiguration()
        {
            Initialize();
        }

        public static ClientUpdateSource UpdateSource
        {
            get { return _updateSource.Value; }
        }

        public static string ServerClientUpdateFolderPath
        {
            get { return _serverClientUpdateFolderPath.Value; }
        }

        public static string ServerNewVersionFolderPath
        {
            get { return _serverNewVersionFolderPath.Value; }
        }

        public static EndpointAddress ClientUpdateServiceUrl
        {
            get { return _clientUpdateServiceUrl.Value; }
        }

        public static string AuthenticationToken
        {
            get { return _authenticationToken.Value; }
        }

        /// <summary>
        /// Path where application and updater is installed
        /// </summary>
        public static string LocalApplicationPath
        {
            get { return _localApplicationPath.Value;  }
        }

        /// <summary>
        /// Full path to current IOPUpdater location
        /// </summary>
        public static string CurrentUpdaterPath
        {
            get { return _currentUpdaterPath.Value; }
        }

        /// <summary>
        /// Allows to override application paths (let's say for tests)
        /// </summary>
        /// <param name="customLocalApplicationPath">The custom local application path.</param>
        public static void SetNewApplicationPath(string customLocalApplicationPath)
        {
            Initialize();
            _localApplicationPath = new Lazy<string>(() => customLocalApplicationPath);
        }

        /// <summary>
        /// Evaluates xPath using IO and returns result
        /// </summary>
        /// <param name="xPath"></param>
        /// <returns></returns>
        private static string ReadFromConfig(string xPath)
        {
            string result = null;
            try
            {
                var selectedElement = _configurationSource.Value.SelectSingleNode(xPath);
                result = selectedElement != null ? selectedElement.Value : null;
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {}
            return result;
        }

        private static void Initialize()
        {
            _currentUpdaterPath = new Lazy<string>(() =>
            {
                var location = (Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly()).Location;
                return location;
            });

            _updateSource = new Lazy<ClientUpdateSource>(() =>
            {
                var result = ClientUpdateSource.ServerNewVersionFolder;
                try
                {
                    // Are files stored in the cloud?
                    string isEnabledString = ReadFromConfig("/configuration/applicationServerClient/@isEnabled");
                    var isEnabled = Convert.ToBoolean(isEnabledString);
                    if (isEnabled)
                    {
                        result = ClientUpdateSource.WebService;
                    }
                    else
                    {
                        result = Directory.Exists(ServerClientUpdateFolderPath)
                            ? ClientUpdateSource.ServerClientUpdateFolder
                            : ClientUpdateSource.ServerNewVersionFolder;
                    }
                }
// ReSharper disable once EmptyGeneralCatchClause
                catch
                {}

                return result;
            });

            _localApplicationPath = new Lazy<string>(() => Path.GetDirectoryName(CurrentUpdaterPath));

            _serverDataPath = new Lazy<string>(() =>
            {
                var result = ReadFromConfig("/configuration/appSettings/add[@key='ServerDataPath']/@value");
                return result;
            });

            _serverNewVersionFolderPath = new Lazy<string>(() =>
            {
                var result = Path.Combine(_serverDataPath.Value, "NewVersion");
                return result;
            });

            _serverClientUpdateFolderPath = new Lazy<string>(() =>
            {
                var result = Path.Combine(_serverDataPath.Value, @"SoftwareUpdates\Client");
                return result;
            });

            _clientUpdateServiceUrl = new Lazy<EndpointAddress>(() =>
            {
                var applicationServiceUrl = ReadFromConfig("/configuration/applicationServerClient/@servicesUriFormat");

                // Format and trim all after service name (so that it's basic binding)
                const string serviceName = "ClientUpdateService";
                applicationServiceUrl = string.Format(applicationServiceUrl, serviceName);
                var serviceNameEndIndex = applicationServiceUrl.LastIndexOf(serviceName, StringComparison.Ordinal) + serviceName.Length;
                if (applicationServiceUrl.Length > serviceNameEndIndex + 1)
                {
                    applicationServiceUrl = applicationServiceUrl.Substring(0, serviceNameEndIndex);
                }

                return new EndpointAddress(applicationServiceUrl);
            });

            _authenticationToken = new Lazy<string>(() =>
            {
                var authToken = ReadFromConfig("/configuration/applicationServerClient/@authenticationToken");
                return authToken;
            });

            _configurationSource = new Lazy<XmlElement>(() =>
            {
                var configPath = LocateCustomConfigurationFilePath();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(configPath);
                return xmlDoc.DocumentElement;
            });
        }

        /// <summary>
        /// Locates path to Custom.config
        /// </summary>
        /// <returns></returns>
        private static string LocateCustomConfigurationFilePath()
        {
            const string systemConfigurationFileName = "IO.Practiceware.System.config";
            const string customConfigurationFileName = "IO.Practiceware.Custom.config";

            // -- First find system config and then create relative path to Custom.config

            // Priority: app's folder -> LocalAppData -> ProgramData
            // Note: in live environment there should really be no config found in app's folder, but thats how ConfigurationManager searches

            // Search maximum 3 levels up from current updater location
            var systemConfigPath = FileSystem.SearchFileInFolderAndUp(LocalApplicationPath,
                systemConfigurationFileName, 3);
            
            if (!File.Exists(systemConfigPath))
            {
                throw new InvalidOperationException("Could not find application configuration. Please, ensure you run updater from application's directory");
            }

            // By now it should have been found, so create path to Custom.config
// ReSharper disable once AssignNullToNotNullAttribute
            var result = Path.Combine(Path.GetDirectoryName(systemConfigPath), customConfigurationFileName);
            return result;
        }
    }
}
