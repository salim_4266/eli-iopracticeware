﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.ClientUpdater.ClientUpdateService;

namespace IO.Practiceware.ClientUpdater
{
    public static class FileSystem
    {
        /// <summary>
        ///     Ensures that all file path parts exist.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="throwExceptionWhenInvalid">If the path is incorrectly formatted, will throw an exception if true.</param>
        /// <returns>Returns a flag indicating whether the path is valid and contains the corresponding directories (and file).</returns>
        public static bool EnsureFilePath(string path, bool throwExceptionWhenInvalid = false)
        {
            bool isValidPath = IsValidFilePath(path, throwExceptionWhenInvalid);
            if (isValidPath)
            {
                path = Path.GetFullPath(path);
                string directoryPath = Path.GetDirectoryName(path);
                if (directoryPath == null)
                {
                    if (throwExceptionWhenInvalid)
                    {
                        throw new Exception("Could not derive directory path from {0}".FormatWith(path));
                    }
                    return false;
                }

                // If directory doesn't exist -> create it
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }
            return true;
        }

        /// <summary>
        ///     Ensures that the directory exists at the given path.
        /// </summary>
        /// <param name="directory"></param>
        public static void EnsureDirectory(this DirectoryInfo directory)
        {
            EnsureDirectory(directory.FullName);
        }

        /// <summary>
        ///     Ensures that the directory exists at the given path.
        /// </summary>
        /// <param name="directoryPath"></param>
        public static void EnsureDirectory(string directoryPath)
        {
            directoryPath = Path.GetFullPath(directoryPath);
            // If directory doesn't exist -> create it
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        [DebuggerNonUserCode]
        public static bool IsValidFilePath(string path, bool throwExceptionWhenInvalid = false)
        {
            // Check if path contains any invalid path characters
            char[] invalidChars = Path.GetInvalidPathChars();
            bool containsInvalidChars = path.Any(invalidChars.Contains);
            if (containsInvalidChars)
            {
                if (throwExceptionWhenInvalid)
                {
                    throw new Exception("{0} contains an Invalid Path Character {1}".FormatWith(path, invalidChars));
                }
                return false;
            }

            // Also try leveraging FileInfo Constructor which checks for file path validity.
            try
            {
                // ReSharper disable UnusedVariable
                new FileInfo(path).EnsureNotDefault();
                // ReSharper restore UnusedVariable
            }
            catch (Exception ex)
            {
                if (throwExceptionWhenInvalid)
                {
                    var exception = new Exception("Provided path value of {0} is invalid.".FormatWith(path), ex);
                    throw exception;
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes the directory. Deletes directories and files recursively. Handles readonly files. Deletes one at a time
        /// because of known limitation in Directory.DeleteDirectory(path, true). See: [insert reference].
        /// Calls the deletionScope resolver before each file and directory to be deleted. Disposes the IDisposable upon
        /// completion of deleting that file/directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="deletionScope">The deletion scope.</param>
        /// <param name="silent">if set to <c>true</c> runs in silent mode and doesn't raise exceptions.</param>
        /// <exception cref="System.ApplicationException">Could not delete directory that contains the following content</exception>
        public static void DeleteDirectory(string directory, Func<string, IDisposable> deletionScope = null, bool silent = false)
        {
            if (deletionScope == null) deletionScope = s => null;

            var directoryInfo = new DirectoryInfo(directory);
            if (!directoryInfo.Exists) return;

            var files = new FileInfo[0];
            try
            {
                files = directoryInfo.GetFiles();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
            }

            var dirs = new DirectoryInfo[0];
            try
            {
                dirs = directoryInfo.GetDirectories();
            }
            catch (Exception ex)
            {
                if (!silent) throw;
                Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
            }


            // Delete all files in a directory
            foreach (var file in files)
            {
                using (deletionScope(file.FullName))
                {
                    try
                    {
                        file.Attributes = FileAttributes.Normal;
                        TryDeleteFile(file.FullName);
                    }
                    catch (Exception ex)
                    {
                        if (!silent) throw;
                        Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
                    }
                }
            }

            // Delete all subdirectories recursively
            foreach (var dir in dirs)
            {
                DeleteDirectory(dir.FullName, deletionScope, silent);
            }

            using (deletionScope(directoryInfo.FullName))
            {
                // Can't delete non-empty folders, so check beforehand
                var currentDirectoryContent = directoryInfo.EnumerateFileSystemInfos().ToArray();
                if (currentDirectoryContent.Any())
                {
                    if (!silent)
                    {
                        throw new ApplicationException("Could not delete directory {0} that contains the following content: {1}."
                            .FormatWith(directoryInfo.FullName, currentDirectoryContent.Select(s => s.FullName).Join()));
                    }
                }
                else
                {
                    try
                    {
                        // Try delete folder
                        directoryInfo.Attributes.Remove(FileAttributes.ReadOnly);
                        RetryUtility.ExecuteWithRetry(() => directoryInfo.Delete(false), 5, TimeSpan.FromMilliseconds(20));
                    }
                    catch (Exception ex)
                    {
                        if (!silent) throw;
                        Trace.TraceWarning("Silent clean of directory partially failed. Error: {0}", ex);
                    }
                }
            }
        }

        /// <summary>
        ///     Deletes the directory by calling FileSystem.DeleteDirectory and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        /// <param name="directory">The directory.</param>
        /// <param name="deletionScope">The deletion scope.</param>
        [DebuggerNonUserCode]
        public static void TryDeleteDirectory(string directory, Func<string, IDisposable> deletionScope = null)
        {
            RetryUtility.ExecuteWithRetry(() => DeleteDirectory(directory, deletionScope), 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        ///     Copies an existing file to a new file and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="overwrite">if set to <c>true</c> overwrites file at destination.</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <param name="inheritAccessControl">Use inherited access control in destination</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryCopyFile(string sourcePath, string destinationPath, bool overwrite = true, bool ensureDirectory = true, bool inheritAccessControl = true)
        {
            if (ensureDirectory) EnsureFilePath(destinationPath);

            RetryUtility.ExecuteWithRetry(() => File.Copy(sourcePath, destinationPath, overwrite), 5, TimeSpan.FromMilliseconds(20));

            // Now reset ACLs with retries
            if (inheritAccessControl)
            {
                RetryUtility.ExecuteWithRetry(() =>
                {
                    var fs = File.GetAccessControl(destinationPath);
                    fs.SetAccessRuleProtection(false, false);
                    File.SetAccessControl(destinationPath, fs);
                }, 5, TimeSpan.FromMilliseconds(20));
            }
        }

        /// <summary>
        /// Moves an existing file to a new location and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="overwrite">if set to <c>true</c> [overwrite].</param>
        /// <param name="ensureDirectory">if set to <c>true</c> ensures destination directory existence.</param>
        /// <param name="inheritAccessControl">if set to <c>true</c> [inherit access control].</param>
        /// <remarks>
        /// Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        /// use.
        /// Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryMoveFile(string sourcePath, string destinationPath, bool overwrite = true, bool ensureDirectory = true, bool inheritAccessControl = true)
        {
            if (ensureDirectory) EnsureFilePath(destinationPath);

            // First move file with retries
            RetryUtility.ExecuteWithRetry(() =>
            {
                if (overwrite) TryDeleteFile(destinationPath, true);
                File.Move(sourcePath, destinationPath);
            }, 5, TimeSpan.FromMilliseconds(20));

            // Now reset ACLs with retries
            if (inheritAccessControl)
            {
                RetryUtility.ExecuteWithRetry(() =>
                {
                    var fs = File.GetAccessControl(destinationPath);
                    fs.SetAccessRuleProtection(false, false);
                    File.SetAccessControl(destinationPath, fs);
                }, 5, TimeSpan.FromMilliseconds(20));
            }
        }

        /// <summary>
        ///     Deletes the specified file and retries up to 5 times if unsuccessful.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="checkExists">if set to <c>true</c> checks if the file exists before deleting.</param>
        /// <remarks>
        ///     Windows does not release it's file locks immediately, so we sometimes encounter an exception that the file is in
        ///     use.
        ///     Retrying is a workaround to this low level issue.
        /// </remarks>
        [DebuggerNonUserCode]
        public static void TryDeleteFile(string path, bool checkExists = false)
        {
            RetryUtility.ExecuteWithRetry(() => { if (!checkExists || File.Exists(path)) File.Delete(path); }, 5, TimeSpan.FromMilliseconds(20));
        }

        /// <summary>
        /// Returns file information
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static FileInfoRecord GetFileInfo(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            return GetFileInfo(fileInfo);
        }

        /// <summary>
        /// Returns file information
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public static FileInfoRecord GetFileInfo(FileInfo fileInfo)
        {
            var infoRecord = new FileInfoRecord
            {
                FullName = fileInfo.FullName,
                Exists = fileInfo.Exists
            };

            // Fill in other part of information if it exists
            if (infoRecord.Exists)
            {
                infoRecord.Size = fileInfo.Length;
                infoRecord.CreationTimeUtc = fileInfo.CreationTimeUtc;
                infoRecord.LastWriteTimeUtc = fileInfo.LastWriteTimeUtc;
                infoRecord.LastAccessTimeUtc = fileInfo.LastAccessTimeUtc;
            }

            return infoRecord;
        }

        /// <summary>
        /// Converts path to a full path. Unlike Path.GetFullName -> will not throw exception on path expressions
        /// </summary>
        /// <param name="path">Path or path expression (with wildcards)</param>
        /// <returns></returns>
        public static string GetFullPath(string path)
        {
            // Check if path represents at least something
            if (string.IsNullOrEmpty(path)) return path;

            // Check if file name contains any special characters
            var fileName = Path.GetFileName(path);
            if (string.IsNullOrEmpty(fileName) // No file name
                || !Path.GetInvalidFileNameChars().Any(fileName.Contains)) // No invalid characters
            {
                // Path is safe to convert to full path
                return Path.GetFullPath(path);
            }

            // Retrieve directory name
            var directoryName = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(directoryName)) return path;

            // Convert directory to full path and combine back with file name
            directoryName = Path.GetFullPath(directoryName);
            var resultPath = Path.Combine(directoryName, fileName);

            // Ensure trailing backslash is preserved
            if (path.EndsWith(@"\") && !resultPath.EndsWith(@"\"))
            {
                resultPath += @"\";
            }
            return resultPath;
        }

        /// <summary>
        /// Searches the file in folder and it's parents.
        /// </summary>
        /// <param name="folderToCheck">The folder to check.</param>
        /// <param name="fileToFind">The file to find.</param>
        /// <param name="maxLevelsToGoUp">The maximum levels to go up.</param>
        /// <returns></returns>
        public static string SearchFileInFolderAndUp(string folderToCheck, string fileToFind, int maxLevelsToGoUp = int.MaxValue)
        {
            var currentDirectory = new DirectoryInfo(folderToCheck);

            string result = null;
            int currentLevel = -1;
            while (currentDirectory != null
                && currentDirectory.Exists
                && ++currentLevel <= maxLevelsToGoUp)
            {
                var testFileLocation = Path.Combine(currentDirectory.FullName, fileToFind);
                if (File.Exists(testFileLocation))
                {
                    result = testFileLocation;
                    break;
                }

                currentDirectory = currentDirectory.Parent;
            }

            return result;
        }
    }
}
