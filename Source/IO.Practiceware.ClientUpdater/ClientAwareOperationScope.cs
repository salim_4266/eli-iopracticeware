﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace IO.Practiceware.ClientUpdater
{
    public class ClientAwareOperationScope : IDisposable
    {
        private readonly OperationContextScope _dependentScope;

        public ClientAwareOperationScope(IContextChannel channel)
        {
            _dependentScope = new OperationContextScope(channel);
            
            // TODO: write unit test, to check that it always works
            // Add token as message header
            var contextData = new Dictionary<string, object>();
            contextData.Add("__Authentication_Token__", IOConfiguration.AuthenticationToken);
            var messageHeader = MessageHeader.CreateHeader("ArrayOfKeyValueOfstringanyType", "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
                contextData);

            OperationContext.Current.OutgoingMessageHeaders.Add(messageHeader);
        }


        public void Dispose()
        {
            _dependentScope.Dispose();
        }
    }
}