﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;

namespace IO.Practiceware.ClientUpdater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly BackgroundWorker _updateWorker;

        public MainWindow()
        {
            InitializeComponent();

            _updateWorker = new BackgroundWorker();
            _updateWorker.DoWork += OnDoWork;
            _updateWorker.ProgressChanged += OnProgressChanged;
            _updateWorker.RunWorkerCompleted += OnRunWorkerCompleted;
            _updateWorker.WorkerReportsProgress = true;
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _updateWorker.RunWorkerAsync();
        }

        private void OnDoWork(object sender, DoWorkEventArgs args)
        {
            App.Updater.PerformUpdate(updateMessage => _updateWorker.ReportProgress(0, updateMessage));
        }

        private void OnProgressChanged(object sender, ProgressChangedEventArgs args)
        {
            ProgressMessageLabel.Content = args.UserState;
        }

        private static void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs args)
        {
            if (args.Error != null)
            {
                App.Updater.LogMessage(args.Error.ToString());
                MessageBox.Show(string.Format("Update failed. Please, try again or contact support{0}{0}Error: {1}",
                    Environment.NewLine, args.Error.Message), "IO Practiceware", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                Thread.Sleep(2000); // Wait 2 seconds before shutting down
            }
            Application.Current.Shutdown();
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            // Prevent attempts to close updater while update is in progress
            if (_updateWorker.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
