﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml;
using IO.Practiceware.ClientUpdater.ClientUpdateService;
using IO.Practiceware.Services.Update;
using Microsoft.Win32;

namespace IO.Practiceware.ClientUpdater
{
    public class Updater
    {
        private const string ClientInstallUpgradeCode = "{E93C26BA-DF46-45EB-87F5-05E03E895B56}";
        private const string UpdateCleanupBatch = "UpdateCleanUp.bat";
        private const string IoPracticewareClientSetupExe = "IO Practiceware Client Setup.exe";

        /// <summary>
        /// The key of application which triggered update
        /// </summary>
        private readonly string _callingProcessFileName;

        private ClientUpdateManifest _updateManifest;
        private FileInfoRecord[] _filesForUpdate;
        private readonly bool _logVerbose;
        private readonly string _updaterLogPath;

        private static readonly TimeSpan Timeout = TimeSpan.FromMinutes(30);

        public Updater(string[] args)
        {
            const string verboseSwitch = "-v";

            // Read application key parameter
            _callingProcessFileName = args.FirstOrDefault(s =>
                s != verboseSwitch);

            _logVerbose = args.Contains(verboseSwitch);

            // ReSharper disable once AssignNullToNotNullAttribute
            _updaterLogPath = Path.Combine(IOConfiguration.LocalApplicationPath, "IOPUpdater.log");
        }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="verbose">if set to <c>true</c> [verbose].</param>
        public void LogMessage(string message, bool verbose = false)
        {
            if (verbose && !_logVerbose) return;

            lock (_updaterLogPath)
            {
                File.AppendAllText(_updaterLogPath, string.Format("{0}: {1}{2}", DateTime.Now.ToString("G"), message, Environment.NewLine));
            }
        }

        /// <summary>
        /// Checks for update.
        /// </summary>
        /// <returns><c>true</c> if update found</returns>
        public bool CheckForUpdate()
        {
            _updateManifest = FetchLatestUpdateInformation();
            _filesForUpdate = CompareManifestWithLocal(_updateManifest);

            // We have an update if there is something else but cleanup file to update
            var hasUpdate = _filesForUpdate
                .Any(f => !string.Equals(f.FullName, UpdateCleanupBatch, StringComparison.OrdinalIgnoreCase));

            LogMessage(string.Format("Checked for update completed with result: {0}. Application: {1}, User: {2}, Machine: {3}, Local: {4}, Server: {5}, UpdateSource: {6}",
                hasUpdate, _callingProcessFileName, Environment.UserName, Environment.MachineName, IOConfiguration.LocalApplicationPath, IOConfiguration.ServerNewVersionFolderPath, IOConfiguration.UpdateSource), true);

            return hasUpdate;
        }

        /// <summary>
        /// Triggers actual update
        /// </summary>
        public void PerformUpdate(Action<string> activityNotify)
        {
            if (_updateManifest == null || _filesForUpdate == null || _filesForUpdate.Length <= 0)
            {
                throw new InvalidOperationException("First call CheckForUpdate and ensure there is an update");
            }

            // Override activity notify, so that it logs messages when verbose output requested
            var originalActivityNotify = activityNotify;
            activityNotify = s =>
            {
                LogMessage(s, true);
                if (originalActivityNotify != null)
                {
                    originalActivityNotify(s);
                }
            };

            LogMessage(string.Format("Performing update. Application data path: {0}. Update source: {1}",
                IOConfiguration.LocalApplicationPath, IOConfiguration.UpdateSource));

            // Terminate application, so that it doesn't hold files needed by update
            activityNotify("Closing application");
            TerminateApplicationProcesses();

            // Download update
            activityNotify("Preparing to download and extract update...");
            var updateSourceDirectory = DownloadAndExtractUpdate(activityNotify, _filesForUpdate);

            // Update files from source
            activityNotify("Updating files");
            UpdateFiles(updateSourceDirectory, _filesForUpdate);

            activityNotify("Cleaning up update download folder");
            FileSystem.DeleteDirectory(updateSourceDirectory, null, true);

            // Run every installer which was copied or updated
            var installersToRun = _filesForUpdate
                .Where(f => string.Equals(Path.GetExtension(f.FullName), ".msi")) // MSI
                .Select(f => Path.Combine(IOConfiguration.LocalApplicationPath, f.FullName))
                .Where(File.Exists)
                .ToList();

            if (installersToRun.Count > 0)
            {
                foreach (var installer in installersToRun)
                {
                    var installFileName = Path.GetFileNameWithoutExtension(installer);
                    activityNotify("Installing " + installFileName);
                    if (!InstallMsi(installer))
                    {
                        // Remove files so that we can later retry update
                        FileSystem.TryDeleteFile(installer, true);
                        CleanupAfterUpdate(_filesForUpdate, false);

                        // Fail update
                        throw new InvalidOperationException(string.Format(
                            "Installation of {0} failed. {1}Please, restart your computer and try again.",
                            installFileName, Environment.NewLine));
                    }
                }
            }

            // Run client setup if provided as "exe" file
            var clientSetup = _filesForUpdate
                .Where(f => string.Equals(Path.GetFileName(f.FullName), IoPracticewareClientSetupExe, StringComparison.OrdinalIgnoreCase)) // Client install
                .Select(f => Path.Combine(IOConfiguration.LocalApplicationPath, f.FullName))
                .Where(File.Exists)
                .SingleOrDefault();

            if (!string.IsNullOrEmpty(clientSetup))
            {
                var installFileName = Path.GetFileNameWithoutExtension(clientSetup);
                activityNotify("Installing " + installFileName);
                if (!InstallWithBurnExe(clientSetup))
                {
                    // Remove files so that we can later retry update
                    FileSystem.TryDeleteFile(clientSetup, true);
                    CleanupAfterUpdate(_filesForUpdate, false);

                    // Fail update
                    throw new InvalidOperationException(string.Format(
                        "Installation of {0} failed. {1}Please, restart your computer and try again.",
                        installFileName, Environment.NewLine));
                }
            }

            // Cleanup
            activityNotify("Cleaning after update");
            CleanupAfterUpdate(_filesForUpdate);

            // Restart application which triggered update
            if (!string.IsNullOrEmpty(_callingProcessFileName))
            {
                var appLocation = Path.Combine(IOConfiguration.LocalApplicationPath, _callingProcessFileName);
                if (File.Exists(appLocation))
                {
                    activityNotify("Restarting application");

                    // Skip update check on next run
                    Process.Start(appLocation, "--noupdate");
                }
                else
                {
                    LogMessage(string.Format("Could not restart application, since it wasn't found. Path: {0}", appLocation));
                }
            }

            activityNotify("Done");
        }

        private static void CleanupAfterUpdate(IEnumerable<FileInfoRecord> filesForUpdate, bool executeUpdateCleanupBatch = true)
        {
            // Find cleanup batch file
            var updateCleanupFile = filesForUpdate
                .Where(f => string.Equals(f.FullName, UpdateCleanupBatch, StringComparison.OrdinalIgnoreCase))
                .Select(f => Path.Combine(IOConfiguration.LocalApplicationPath, f.FullName))
                .FirstOrDefault(File.Exists);

            if (!string.IsNullOrEmpty(updateCleanupFile))
            {
                if (executeUpdateCleanupBatch)
                {
                    ExecuteAndWait(updateCleanupFile);
                }
                FileSystem.TryDeleteFile(updateCleanupFile, true);
            }
        }

        private static bool InstallMsi(string installerPath)
        {
            // ReSharper disable AssignNullToNotNullAttribute
            var logLocation = Path.Combine(Path.GetDirectoryName(installerPath), Path.GetFileNameWithoutExtension(installerPath)) + ".log";
            // ReSharper restore AssignNullToNotNullAttribute
            var arguments = string.Format("/c start /wait msiexec.exe /i \"{0}\" /passive /l* \"{1}\"", installerPath, logLocation);
            var exitCode = ExecuteAndWait("cmd", arguments);
            return exitCode == 0 || exitCode == 3010; // Success or Success_Restart_Required
        }

        private bool InstallWithBurnExe(string installerPath)
        {
            // ReSharper disable AssignNullToNotNullAttribute
            var baseLogPath = Path.Combine(Path.GetDirectoryName(installerPath), Path.GetFileNameWithoutExtension(installerPath));
            string errorLog;

            // 1. Uninstall (this enables downgrades with Burn)
            var uninstallPath = LocateUninstaller();
            if (!string.IsNullOrEmpty(uninstallPath))
            {
                errorLog = string.Format("{0}.uninstall.log", baseLogPath);
                var uninstallCommand = string.Format("-uninstall -passive -norestart -log \"{0}\"", errorLog);
                var uninstallExitCode = ExecuteAndWait(uninstallPath, uninstallCommand);
                if (uninstallExitCode != 0)
                {
                    LogMessage(string.Format("Failed to uninstall previous Client version. Return code: {0}, error log: {1}", uninstallExitCode, errorLog));
                    return false;
                }
            }

            // 2. Install new version
            errorLog = string.Format("{0}.log", baseLogPath);
            var arguments = string.Format("-passive -norestart -log \"{0}\"", errorLog);
            var exitCode = ExecuteAndWait(installerPath, arguments);
            if (exitCode != 0)
            {
                LogMessage(string.Format("Failed to install newer Client version. Return code: {0}, error log: {1}", exitCode, errorLog));
            }
            return exitCode == 0; // Success
        }

        /// <summary>
        /// Searches registry for previously installed client version using Burn. 
        /// If found -> returns path to install "exe" for that version
        /// </summary>
        /// <returns></returns>
        private static string LocateUninstaller()
        {
            using (var key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall"))
            {
                if (key != null)
                {
                    // Iterate over app guids
                    foreach (var keyName in key.GetSubKeyNames())
                    {
                        using (var subkey = key.OpenSubKey(keyName))
                        {
                            if (subkey != null)
                            {
                                // Match by static Bundle Upgrade Code we specified in Client Setup (Full).wix
                                var upgradeCode = subkey.GetValue("BundleUpgradeCode") as string[];
                                if (upgradeCode != null && upgradeCode[0] == ClientInstallUpgradeCode)
                                {
                                    var cachePath = subkey.GetValue("BundleCachePath") as string;
                                    return cachePath;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        private static int ExecuteAndWait(string command, string arguments = null)
        {
            var process = Process.Start(new ProcessStartInfo
            {
                FileName = command,
                Arguments = arguments,
                WindowStyle = ProcessWindowStyle.Hidden
            });
            // ReSharper disable once PossibleNullReferenceException
            process.WaitForExit();
            return process.ExitCode;
        }

        private void UpdateFiles(string updateSourceDirectory, IEnumerable<FileInfoRecord> filesForUpdate)
        {
            foreach (var updateFile in filesForUpdate)
            {
                var sourcePath = Path.Combine(updateSourceDirectory, updateFile.FullName);
                var destinationPath = Path.Combine(IOConfiguration.LocalApplicationPath, updateFile.FullName);
                var destinationDirectory = Path.GetDirectoryName(destinationPath);

                // Skip non-existing files
                if (!File.Exists(sourcePath)) continue;

                // Ensure destination folder exists
                if (!Directory.Exists(destinationDirectory))
                {
                    Directory.CreateDirectory(destinationDirectory);
                }

                LogMessage("Updating file from server: " + updateFile.FullName);

                FileSystem.TryMoveFile(sourcePath, destinationPath);

                // Ensure it will have proper date & time
                var file = updateFile;
                RetryUtility.ExecuteWithRetry(() => File.SetLastWriteTimeUtc(destinationPath, file.LastWriteTimeUtc), 
                    5, TimeSpan.FromMilliseconds(50));
            }
        }

        private string DownloadAndExtractUpdate(Action<string> activityNotify, IEnumerable<FileInfoRecord> filesForUpdate)
        {
            // Use temp file name as folder name
            var tempUpdateFolder = Path.GetTempFileName();
            // If it exists as file -> delete it
            FileSystem.TryDeleteFile(tempUpdateFolder, true);
            // Create it as folder
            Directory.CreateDirectory(tempUpdateFolder);

            if (IOConfiguration.UpdateSource == ClientUpdateSource.WebService)
            {
                ClientUpdateServiceClient client = null;
                try
                {
                    client = GetUpdateClient();
                    using (new ClientAwareOperationScope(client.InnerChannel))
                    {
                        // Download update parts
                        foreach (var updateFile in filesForUpdate)
                        {
                            var saveLocation = Path.Combine(tempUpdateFolder, updateFile.FullName);
                            FileSystem.EnsureFilePath(saveLocation);

                            var fileName = Path.GetFileName(updateFile.FullName);
                            string currentDownloadProgressText = string.Format("Initiating download of {0}...", fileName);
                            activityNotify(currentDownloadProgressText);
                            var updatePart = client.DownloadUpdatePart(updateFile.FullName, UpdateApiVersion.V2);
                            using (var saveFileStream = new FileStream(saveLocation, FileMode.Create))
                            {
                                // UI updates can slowdown the download, so we'll notify regularly instead
                                var notifyTimer = new Timer(p => activityNotify(currentDownloadProgressText), null, 0, 900);

                                Action<long> notifyDownloadProgress = l =>
                                {
                                    currentDownloadProgressText = string.Format("Downloaded {0}KB of {1} update.", l/1024, fileName);
                                };
                                CopyStream(updatePart, saveFileStream, notifyDownloadProgress);

                                // End notifying
                                notifyTimer.Dispose();
                            }

                            LogMessage(string.Format("Downloaded update {0}. Size: {1}", updateFile.FullName, new FileInfo(saveLocation).Length), true);
                        }
                    }
                }
                finally
                {
                    if (client != null)
                    {
                        client.Close();
                    }
                }
            }
            else
            {
                // Copy files from remote computer to temp location on this machine
                foreach (var updateFile in filesForUpdate)
                {
                    var saveLocation = Path.Combine(tempUpdateFolder, updateFile.FullName);
                    string sourceFilePath;
                    if (IOConfiguration.UpdateSource == ClientUpdateSource.ServerClientUpdateFolder)
                    {
                        sourceFilePath = Path.Combine(IOConfiguration.ServerClientUpdateFolderPath, updateFile.FullName);
                    }
                    else
                    {
                        var realPath = updateFile.FullName.Remove(0, IOConfiguration.ClientDataDirectory.Length + 1);
                        sourceFilePath = Path.Combine(IOConfiguration.ServerNewVersionFolderPath, realPath);
                    }

                    FileSystem.TryCopyFile(sourceFilePath, saveLocation);
                }
            }

            return tempUpdateFolder;
        }

        private static void CopyStream(Stream source, Stream destination, Action<long> lengthRead = null, int bufferSize = 4096)
        {
            int count;
            var buffer = new byte[bufferSize];
            long length = 0;

            while ((count = source.Read(buffer, 0, bufferSize)) != 0)
            {
                length += count;
                destination.Write(buffer, 0, count);

                if (lengthRead != null) lengthRead(length);
            }
        }

        private void TerminateApplicationProcesses()
        {
            // Build a list of known client process names
            var knownProcessNames = new[]
            {
                "Administrator",
                "DoctorInterface",
                "PatientInterface",
                "AdminUtilities",
                "IOClinImport"
            }
#if DEBUG
            .Union(new[] { "IO.Practiceware.TestClient" })
#endif
            .Distinct()
            .ToList();

            var currentUser = PInvoke.Wtsapi32.GetUsernameBySessionId(Process.GetCurrentProcess().SessionId);

            // Locate these processes
            var processesToKill = knownProcessNames
                .SelectMany(Process.GetProcessesByName)
                .Where(p => string.Equals(PInvoke.Wtsapi32.GetUsernameBySessionId(p.SessionId), currentUser, 
                    StringComparison.OrdinalIgnoreCase))
                .ToList();

            // Kill them
            processesToKill.AsParallel().ForAll(p =>
            {
                LogMessage("Closing process: " + p.ProcessName, true);

                p.Kill();
                p.WaitForExit(Convert.ToInt32(TimeSpan.FromSeconds(30).TotalMilliseconds));
            });
        }

        private static ClientUpdateManifest FetchLatestUpdateInformation()
        {
            // If cloud mode -> just use the service
            if (IOConfiguration.UpdateSource == ClientUpdateSource.WebService)
            {
                var client = GetUpdateClient();
                using (new ClientAwareOperationScope(client.InnerChannel))
                {
                    var updateManifest = client.GetUpdateManifest(UpdateApiVersion.V2);
                    return updateManifest;
                }
            }

            var manifest = new ClientUpdateManifest();

            // Otherwise -> try to fetch it from server application folder
            var serverUpdateFolderPath = IOConfiguration.UpdateSource == ClientUpdateSource.ServerClientUpdateFolder
                ? IOConfiguration.ServerClientUpdateFolderPath
                : IOConfiguration.ServerNewVersionFolderPath;

            var serverUpdateFolder = new DirectoryInfo(serverUpdateFolderPath);
            if (serverUpdateFolder.Exists)
            {
                manifest.AvailableUpdateFiles = new UpdateContentSearch()
                    .ListUpdateContent(serverUpdateFolder)
                    .ToArray();

                // Fix update paths for update from Server New Version folder
                if (IOConfiguration.UpdateSource == ClientUpdateSource.ServerNewVersionFolder)
                {
                    foreach (var updateFile in manifest.AvailableUpdateFiles)
                    {
                        updateFile.FullName = Path.Combine(IOConfiguration.ClientDataDirectory, updateFile.FullName);
                    }
                }
            }

            return manifest;
        }

        private static ClientUpdateServiceClient GetUpdateClient()
        {
            // Allow any certificate
            // TODO: Allow only *.iopracticeware.com certificate
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender, certificate, chain, sslPolicyErrors) => true);

            var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
                          {
                              ReaderQuotas = XmlDictionaryReaderQuotas.Max,
                              MaxBufferPoolSize = int.MaxValue,
                              MaxBufferSize = int.MaxValue,
                              MaxReceivedMessageSize = int.MaxValue,
                              CloseTimeout = Timeout,
                              OpenTimeout = Timeout,
                              ReceiveTimeout = Timeout,
                              SendTimeout = Timeout,
                              TransferMode = TransferMode.Streamed
                          };
            var client = new ClientUpdateServiceClient(binding, IOConfiguration.ClientUpdateServiceUrl);
            client.InnerChannel.OperationTimeout = Timeout;

            return client;
        }

        /// <summary>
        /// Compares manifest information with local
        /// </summary>
        /// <param name="manifest"></param>
        /// <returns>Files for update</returns>
        private FileInfoRecord[] CompareManifestWithLocal(ClientUpdateManifest manifest)
        {
            if (manifest.AvailableUpdateFiles == null || manifest.AvailableUpdateFiles.Length <= 0)
                return new FileInfoRecord[0];

            // Check files to see whether we have an update
            var filesForUpdate = new List<FileInfoRecord>();
            foreach (var updateFile in manifest.AvailableUpdateFiles)
            {
                var localFile = new FileInfo(Path.Combine(IOConfiguration.LocalApplicationPath, updateFile.FullName));
                if (!localFile.Exists // Not exists
                    || localFile.LastWriteTimeUtc != updateFile.LastWriteTimeUtc // Not latest
                    || localFile.Length != updateFile.Size) // Size doesn't match
                {
                    LogMessage("Server has newer version of file: " + updateFile.FullName, true);
                    filesForUpdate.Add(updateFile);
                }
            }

            return filesForUpdate.ToArray();
        }
    }
}
