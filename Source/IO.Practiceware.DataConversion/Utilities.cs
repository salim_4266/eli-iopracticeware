﻿
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Collections;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace IO.Practiceware.DataConversion
{
    public static class Utilities
    {
        private static class InvokerCache<T>
        {
            private static readonly IDictionary<string, Invoker> Cache = new Dictionary<string, Invoker>();

            public static Invoker GetPropertyGetter(string propertyName)
            {
                return Cache.GetValue(propertyName, () => typeof(T).GetProperty(propertyName).GetGetMethod().GetInvoker());
            }
        }

        public static object GetPropertyValue<T>(this T instance, string propertyName)
        {
            return InvokerCache<T>.GetPropertyGetter(propertyName)(instance);
        }

        /// <summary>
        /// Sets the property values on the instance from the named (= separated) arguments.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="args"></param>
        public static void SetFromArguments(object instance, string[] args)
        {
            foreach (var arg in args.Select(a => a.Split('=')).Where(a => a.Length > 1))
            {
                string propertyName = arg[0];
                string value = arg.Skip(1).Join("=");
                PropertyInfo property = instance.GetType().GetProperty(propertyName);
                if (property != null)
                {
                    property.SetValue(instance, Convert.ChangeType(value, property.PropertyType), null);
                }
            }
        }

        /// <summary>
        /// Parses a flat file with named headers into a datatable.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="delimiter"></param>
        /// <param name="hasHeader"> </param>
        /// <param name="removeQuotes"> </param>
        /// <returns></returns>
        public static DataTable ToDataTable(this string source, string delimiter = "~", bool hasHeader = true, bool removeQuotes = true)
        {
            if (removeQuotes)
            {
                source = source.Replace("\"", string.Empty);
            }

            string[] lines = source.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            int lineIndex = 0;

            var dataTable = new DataTable();
            string headerLine = lines[0];

            if (hasHeader)
            {
                foreach (string header in headerLine.Split(new[] { delimiter }, StringSplitOptions.None))
                {
                    dataTable.Columns.Add(header, typeof(string));
                }
                lineIndex++;
            }

            while (lineIndex < lines.Length)
            {
                var line = lines[lineIndex];
                if (!string.IsNullOrWhiteSpace(line))
                {
                    string[] lineValues = line.Split(new[] { delimiter }, StringSplitOptions.None);

                    while (lineValues.Length > dataTable.Columns.Count)
                    {
                        Trace.TraceInformation("A row {0} has been encountered that has more fields than header columns. Columns:{1}", line, headerLine);
                        dataTable.Columns.Add();
                    }

                    dataTable.Rows.Add(lineValues.OfType<object>().ToArray());

                    lineIndex++;
                }
            }

            return dataTable;
        }


        /// <summary>
        /// Filters the queryable to match the specified member values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="memberValues"></param>
        /// <returns></returns>
        public static IQueryable<T> WithMemberValues<T>(this IQueryable<T> source, IDictionary memberValues)
        {
            foreach (object key in memberValues.Keys)
            {
                source = source.Where("{0}=@0".FormatWith(key), memberValues[key]);
            }

            return source;
        }

        /// <summary>
        /// Returns a queryable with items that match all non id properties.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="match"></param>
        /// <param name="includedProperties"></param>
        /// <returns></returns>
        public static IEnumerable<T> WhereMatches<T>(this IEnumerable<T> source, T match, params string[] includedProperties)
        {
            Dictionary<string, object> memberValues =
                typeof(T).GetProperties().Where(
                    p =>
                    p.CanRead && p.GetIndexParameters().Length == 0 &&
                    !new[] { typeof(T).Name + "Id", "Id" }.Any(i => i.Equals(p.Name, StringComparison.OrdinalIgnoreCase)) && includedProperties.Contains(p.Name))
                    .ToDictionary(i => i.Name, i => match.GetPropertyValue(i.Name));


            return source.WithMemberValues(memberValues);
        }

        /// <summary>
        /// Filters the queryable to match the specified member values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="memberValues"></param>
        /// <returns></returns>
        public static IEnumerable<T> WithMemberValues<T>(this IEnumerable<T> source, IDictionary memberValues)
        {
            foreach (object k in memberValues.Keys)
            {
                object key = k;
                source = source.Where(i => Equals(i.GetPropertyValue((string)key), memberValues[key]));
            }

            return source;
        }

        /// <summary>
        /// Returns a queryable with items that match all non id properties.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="match"></param>
        /// <param name="includedProperties"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereMatches<T>(this IQueryable<T> source, T match, params string[] includedProperties)
        {
            Dictionary<string, object> memberValues =
                typeof(T).GetProperties().Where(
                    p =>
                    p.CanRead && p.GetIndexParameters().Length == 0 &&
                    !new[] { typeof(T).Name + "Id", "Id" }.Any(i => i.Equals(p.Name, StringComparison.OrdinalIgnoreCase)) && includedProperties.Contains(p.Name))
                    .ToDictionary(i => i.Name, i => match.GetPropertyValue(i.Name));


            return source.WithMemberValues(memberValues);
        }

        /// <summary>
        /// Attempts to get the policy holder using old patient ID, then social security number, and finally by last name, first name, and birth date.
        /// If it finds a policy holder, it returns its patient ID. If no policy holder is found, it returns 0 for the patient ID.
        /// If last name, first name, or birth date is not present and a policy holder is not found, the method returns -1, which indicates
        /// that there is insufficient data to post the policy holder.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="row">The row.</param>
        /// <param name="insurerCount">The insurer count (e.g. 1 if primary insurer, 2 if secondary insurer, etc.)</param>
        /// <returns></returns>
        public static int GetPolicyHolder(this IPracticeRepository practiceRepository, DataRow row, int insurerCount)
        {
            int policyHolderId = 0;
            var insurerOldPolicyHolderId = row.GetValueOrDefault<string>("Insurer" + insurerCount + "OldPolicyHolderId");

            if (!string.IsNullOrEmpty(insurerOldPolicyHolderId))
            {
                policyHolderId = practiceRepository.FindPatientIdByOldPatientId(insurerOldPolicyHolderId);
            }

            if (policyHolderId == 0)
            {
                var policyHolderSocialSecurityNumber = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderSSN");

                if (!string.IsNullOrEmpty(policyHolderSocialSecurityNumber))
                {
                    policyHolderId = practiceRepository.Patients.Where(p => p.SocialSecurityNumber == policyHolderSocialSecurityNumber).Select(p => p.Id).FirstOrDefault();//legacyPracticeRepository.FindPatientIdBySsn(policyHolderSocialSecurityNumber);
                }


                if (policyHolderId == 0)
                {
                    bool areDataRowColumnsNecessaryToLinkOrPostPresent = row.Table.Columns.Contains("Insurer" + insurerCount + "PolicyHolderLastName") && row.Table.Columns.Contains("Insurer" + insurerCount + "PolicyHolderFirstName") && row.Table.Columns.Contains("Insurer" + insurerCount + "PolicyHolderBirthdate");

                    if (areDataRowColumnsNecessaryToLinkOrPostPresent)
                    {
                        var policyHolderSearchObject = new Patient { LastName = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderLastName"), FirstName = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderFirstName"), DateOfBirth = ValidateDate(row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderBirthdate")).ToDateTime("yyyyMMdd") };

                        bool isPolicyHolderDataSufficientToLinkOrPost = string.IsNullOrEmpty(policyHolderSearchObject.LastName) && string.IsNullOrEmpty(policyHolderSearchObject.FirstName) && !policyHolderSearchObject.DateOfBirth.HasValue;

                        if (isPolicyHolderDataSufficientToLinkOrPost)
                            return -1;

                        policyHolderSearchObject.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                        var comparerDob = policyHolderSearchObject.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty); 
                        policyHolderId = (from ptn in practiceRepository.Patients
                                          where ptn.FirstName == policyHolderSearchObject.FirstName &&
                                                      ptn.LastName == policyHolderSearchObject.LastName &&
                                                      CommonQueries.ConvertToMMDDYYYYString(ptn.DateOfBirth) == comparerDob
                                          select ptn.Id).FirstOrDefault();

                        return policyHolderId;
                    }

                    return -1;

                }
            }

            return policyHolderId;
        }


        /// <summary>
        /// Posts the policy holder.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="row">The row.</param>
        /// <param name="insurerCount">The insurer count.</param>
        /// <returns>patient to post</returns>
        public static Patient PostPolicyHolder(this IPracticeRepository practiceRepository, DataRow row, int insurerCount)
        {
            var policyHolderToPost = new Patient
            {
                LastName = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderLastName"),
                FirstName = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderFirstName"),
                DateOfBirth = ValidateDate(row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderBirthdate")).ToDateTime("yyyyMMdd"),
                MiddleName = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderMiddleInitial"),
                PriorPatientCode = row.GetValueOrDefault<string>("Insurer" + insurerCount + "OldPolicyHolderId"),
                SocialSecurityNumber = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderSSN").ToAlphanumeric()
            };
            var maritalStatus = row.GetValueOrDefault<string>("IOInsurer" + insurerCount + "PolicyHolderMarital");
            if (maritalStatus.IsNotNullOrEmpty())
            {
                policyHolderToPost.MaritalStatus = Enums.ToEnumFromDisplayNameOrDefault<MaritalStatus>(maritalStatus);
            }
           
            var gender = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderGender");
            if (gender.IsNotNullOrEmpty())
            {
                policyHolderToPost.Gender = Enums.ToEnumFromDisplayNameOrDefault<Gender>(gender);
            }
            string abbreviation = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderState");
            if (!abbreviation.IsNotNullOrEmpty())
            {
                var stateOrProvince = practiceRepository.StateOrProvinces.Where(sop => sop.Abbreviation == abbreviation).Select(sop => sop).EnsureNotDefault();
                if (stateOrProvince.Any())
                {
                    policyHolderToPost.PatientAddresses.Add(new PatientAddress
                    {
                        Line1 = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderStreetAddress"),
                        Line2 = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderSuite"),
                        PatientAddressTypeId = (int) PatientAddressTypeId.Home,
                        City = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderCity"),
                        PostalCode = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderZip"),
                        StateOrProvinceId = stateOrProvince.First().Id
                    });
                }
            }

            var homeNumber = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderHomePhone");
            if (!homeNumber.IsNullOrEmpty() && homeNumber.Length == 10)
            {
                policyHolderToPost.PatientPhoneNumbers.Add(
                    new PatientPhoneNumber
                    {
                        AreaCode = homeNumber.Substring(0, 3),
                        ExchangeAndSuffix = homeNumber.Substring(3, homeNumber.Length - 3),
                        PatientPhoneNumberType = PatientPhoneNumberType.Home
                    });
            }

            var workNumber = row.GetValueOrDefault<string>("Insurer" + insurerCount + "PolicyHolderWorkPhone").ToAlphanumeric();
            if (!workNumber.IsNullOrEmpty() && workNumber.Length == 10)
            {
                policyHolderToPost.PatientPhoneNumbers.Add(
                    new PatientPhoneNumber
                    {
                        AreaCode = workNumber.Substring(0, 3),
                        ExchangeAndSuffix = workNumber.Substring(3, workNumber.Length - 3),
                        PatientPhoneNumberType = PatientPhoneNumberType.Business
                    });
            }

            policyHolderToPost.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

            var comparerDob = policyHolderToPost.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty); 
            var matchingPatients = from ptn in practiceRepository.Patients
                                   where ptn.FirstName == policyHolderToPost.FirstName &&
                                         ptn.LastName == policyHolderToPost.LastName &&
                                         CommonQueries.ConvertToMMDDYYYYString(ptn.DateOfBirth) == comparerDob
                                   select ptn;

            bool isDuplicatePatient = matchingPatients.Any();

            if (!isDuplicatePatient)
            {
                practiceRepository.Save(policyHolderToPost);
                int policyHolderId = policyHolderToPost.Id;
                var patientInsurances = practiceRepository.PatientInsurances.Where(pi => pi.InsuredPatientId == policyHolderId).Select(pi => pi).FirstOrDefault();
                if (patientInsurances != null && patientInsurances.PolicyholderRelationshipType != PolicyHolderRelationshipType.Self)
                {
                    patientInsurances.PolicyholderRelationshipType = PolicyHolderRelationshipType.Self;
                    practiceRepository.Save(policyHolderToPost);
                }
                return policyHolderToPost;
            }

            Trace.TraceInformation("A patient with name: {0}, {1}, and birthdate {2} already exists", policyHolderToPost.LastName, policyHolderToPost.FirstName, policyHolderToPost.DateOfBirth);
            // ReSharper disable PossibleNullReferenceException
            return matchingPatients.FirstOrDefault();
            // ReSharper restore PossibleNullReferenceException
        }

        public static Patient ToPatient(this DataRow row)
        {
            var patient = new Patient
            {
                PriorPatientCode = row.GetValueOrDefault<string>("OldPatientId")
                ,
                LastName = row.GetValueOrDefault<string>("PatientLastName")
                ,
                FirstName = row.GetValueOrDefault<string>("PatientFirstName")
                ,
                MiddleName = row.GetValueOrDefault<string>("PatientMiddleInitial")
                ,
                DateOfBirth = row.GetValueOrDefault<string>("PatientBirthDate").ToDateTime("yyyyMMdd")
            };

            var homeNumber = row.GetValueOrDefault<string>("PatientHomePhone");
            if (homeNumber.IsNotNullOrEmpty())
            {
                patient.PatientPhoneNumbers.Add(new PatientPhoneNumber
                {
                    AreaCode = homeNumber.Substring(0, 3),
                    ExchangeAndSuffix = homeNumber.Substring(3, homeNumber.Length - 3),
                    PatientPhoneNumberType = PatientPhoneNumberType.Home
                });
            }

            var businessPhone = row.GetValueOrDefault<string>("PatientWorkPhone");
            if (businessPhone.IsNotNullOrEmpty())
            {
                patient.PatientPhoneNumbers.Add(new PatientPhoneNumber
                {
                    AreaCode = businessPhone.Substring(0, 3),
                    ExchangeAndSuffix = businessPhone.Substring(3, businessPhone.Length - 3),
                    PatientPhoneNumberType = PatientPhoneNumberType.Business
                });
            }
            patient.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();
            return patient;
        }

        /// <summary>
        /// Find the patient by old patinet id.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="oldPatientId">Old patient id.</param>
        /// <returns></returns>
        public static Patient FindPatientByOldPatientId(this IPracticeRepository practiceRepository, string oldPatientId)
        {
            var matchingPatients = practiceRepository.Patients.Where(p => p.PriorPatientCode == oldPatientId).Select(p => p);

            if (matchingPatients.Count() > 1)
            {
                Trace.TraceInformation("Old patient ID {0} could not be uniquely linked.{1}", oldPatientId, Environment.NewLine);
                return null;
            }

            return matchingPatients.FirstOrDefault();
        }

        /// <summary>
        /// Finds the patient id by old patient id. If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository</param>
        /// <param name="oldPatientId">The old patient id.</param>
        /// <returns></returns>
        public static int FindPatientIdByOldPatientId(this IPracticeRepository practiceRepository, string oldPatientId)
        {
            var patient = FindPatientByOldPatientId(practiceRepository, oldPatientId);
            if (patient != null)
            {
                return patient.Id;
            }
            return 0;
        }

        /// <summary>
        /// Finds the patient id by old patient id. If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="patients">The patients.</param>
        /// <param name="oldPatientId">The old patient id.</param>
        /// <returns></returns>
        public static int FindPatientIdByOldPatientId(this IQueryable<Patient> patients, string oldPatientId)
        {
            IEnumerable<int> matchingPatientIds = patients
                .Where(p => p.PriorPatientCode == oldPatientId)
                .Select(p => p.Id);

            if (matchingPatientIds.Count() != 1)
            {
                Trace.TraceWarning(matchingPatientIds.Count() < 1
                                           ? string.Format("Patient with old patient ID {0} not found.{1}", oldPatientId, Environment.NewLine)
                                           : string.Format("Old patient ID {0} could not be uniquely linked.{1}", oldPatientId, Environment.NewLine));
                return 0;
            }

            return matchingPatientIds.FirstOrDefault();
        }

        /// <summary>
        /// Finds the patient id by name and birth date. If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="row">The data row.</param>
        /// <returns></returns>
        public static int FindPatientIdByNameAndBirthDate(this IPracticeRepository practiceRepository, DataRow row)
        {
            var patientSearchObject = new Patient
            {
                LastName = row.GetValueOrDefault<string>("PatientLastName")
                ,
                FirstName = row.GetValueOrDefault<string>("PatientFirstName")
                ,
                DateOfBirth = ValidateDate(row.GetValueOrDefault<string>("PatientBirthDate")).ToDateTime("yyyyMMdd"),
            };
            var comparerDob = patientSearchObject.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty); 
            var matchingPatientIds = from ptn in practiceRepository.Patients
                                     where ptn.FirstName == patientSearchObject.FirstName &&
                                                 ptn.LastName == patientSearchObject.LastName &&
                                                 CommonQueries.ConvertToMMDDYYYYString(ptn.DateOfBirth) == comparerDob
                                     select ptn.Id;
            if (matchingPatientIds.Count() != 1)
            {
                Trace.TraceWarning(matchingPatientIds.Count() < 1
                                           ? string.Format("A patient ID with name: {0}, {1}, and birthdate {2} already exists.{3}", patientSearchObject.LastName, patientSearchObject.FirstName, patientSearchObject.DateOfBirth, Environment.NewLine)
                                           : string.Format("Name: {0}, {1}, and birthdate {2} could not be uniquely linked to a patient ID.{3}", patientSearchObject.LastName, patientSearchObject.FirstName, patientSearchObject.DateOfBirth, Environment.NewLine));
                return 0;
            }

            return matchingPatientIds.FirstOrDefault();
        }

        /// <summary>
        /// Finds the patient id by SSN. If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="socialSecurityNumber">The social security number.</param>
        /// <returns></returns>
        public static int FindPatientIdBySsn(this IPracticeRepository practiceRepository, string socialSecurityNumber)
        {

            IEnumerable<int> matchingPatientIds = practiceRepository.Patients
                .Where(p => p.SocialSecurityNumber == socialSecurityNumber)
                .Select(p => p.Id);

            if (matchingPatientIds.Count() != 1)
            {
                Trace.TraceWarning(matchingPatientIds.Count() < 1
                                           ? string.Format("Patient with social security number {0} not found.{1}", socialSecurityNumber, Environment.NewLine)
                                           : string.Format("social security number {0} could not be uniquely linked.{1}", socialSecurityNumber, Environment.NewLine));
                return 0;
            }

            return matchingPatientIds.FirstOrDefault();
        }

        /// <summary>
        /// Find insurer by old patient id.If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="oldInsurerId">Old insurer id</param>
        /// <returns></returns>
        public static Patient FindInsurerByOldInsurerId(this IPracticeRepository practiceRepository, string oldInsurerId)
        {
            var matchingInsurers = practiceRepository.PatientInsurances.Where(pi => pi.InsuredPatient.PriorPatientCode == oldInsurerId).Select(pi => pi.InsuredPatient);

            if (matchingInsurers.Count() > 1)
            {
                Trace.TraceInformation("Insurer with old insurer ID {0} could not be uniquely linked.{1}", oldInsurerId, Environment.NewLine);
                return null;
            }

            return matchingInsurers.FirstOrDefault();
        }

        /// <summary>
        /// Validate the date.
        /// </summary>
        /// <param name="inputDate">Date to validate.</param>
        /// <returns></returns>
        public static string ValidateDate(string inputDate)
        {
            if (string.IsNullOrEmpty(inputDate) || (Convert.ToInt32(inputDate) < 19000101))
            {
                return string.Empty;
            }

            return inputDate;
        }

        /// <summary>
        /// Finds the insurer id by old insurer id.  If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="oldInsurerId">The old insurer id.</param>
        /// <returns></returns>
        public static int FindInsurerIdByOldInsurerId(this IPracticeRepository practiceRepository, string oldInsurerId)
        {
            IEnumerable<int> matchingInsurerIds = practiceRepository.Insurers
                .Where(ins => ins.PriorInsurerCode == oldInsurerId)
                .Select(ins => ins.Id);

            if (matchingInsurerIds.Count() != 1)
            {
                Trace.TraceInformation(matchingInsurerIds.Count() < 1
                                           ? string.Format("Insurer with old insurer ID {0} not found.{1}", oldInsurerId, Environment.NewLine)
                                           : string.Format("Old insurer ID {0} could not be uniquely linked.{1}", oldInsurerId, Environment.NewLine));

                return 0;
            }

            return matchingInsurerIds.FirstOrDefault();
        }

        /// <summary>
        /// Finds the referring doctor id by old referring doctor id.  If no IDs are found or more than 1 ID is found, logs an error and returns 0.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="oldReferringDoctorId">The old referring doctor id.</param>
        /// <returns></returns>
        public static int FindReferringDoctorIdByOldReferringDoctorId(this ILegacyPracticeRepository practiceRepository, string oldReferringDoctorId)
        {
            IEnumerable<int> matchingReferringDoctorIds = practiceRepository.PracticeVendors.Where(pv => pv.VendorOtherOffice2  == oldReferringDoctorId).Select(pv => pv.VendorId);

            if (matchingReferringDoctorIds.Count() != 1)
            {
                Trace.TraceInformation(matchingReferringDoctorIds.Count() < 1
                                           ? string.Format("Referring doctor with old referring doctor ID {0} not found.{1}", oldReferringDoctorId, Environment.NewLine)
                                           : string.Format("Old referring doctor ID {0} could not be uniquely linked.{1}", oldReferringDoctorId, Environment.NewLine));

                return 0;
            }

            return matchingReferringDoctorIds.FirstOrDefault();
        }

        public static PracticeVendor FindReferringDoctorByOldReferringDoctorId(this ILegacyPracticeRepository practiceRepository, string oldReferringDoctorId)
        {
            IEnumerable<PracticeVendor> matchingReferringDoctor = practiceRepository.PracticeVendors.Where(pv => pv.VendorOtherOffice2 == oldReferringDoctorId).Select(pv => pv);

            if (matchingReferringDoctor.Count() != 1)
            {
                Trace.TraceInformation(matchingReferringDoctor.Count() < 1
                                           ? string.Format("Referring doctor with old referring doctor ID {0} not found.{1}", oldReferringDoctorId, Environment.NewLine)
                                           : string.Format("Old referring doctor ID {0} could not be uniquely linked.{1}", matchingReferringDoctor.Select(mrd => mrd.VendorId).First(), Environment.NewLine));

                return null;
            }

            return matchingReferringDoctor.FirstOrDefault();
        }


        /// <summary>
        /// Find the matching patient based on patient information has been passed.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository.</param>
        /// <param name="patient">Patinet on the basis of which need to find one.</param>
        /// <returns></returns>
        public static Patient FindMatchingPatient(this IPracticeRepository practiceRepository, Patient patient)
        {
            Patient[] matchingPatients = practiceRepository.Patients.Where(r => r.PriorPatientCode == patient.PriorPatientCode).ToArray();

            if (matchingPatients.Length > 1)
            {
                Trace.TraceInformation("Old patient ID {0} could not be uniquely linked.{1}", patient.PriorPatientCode, Environment.NewLine);
                return null;
            }

            return practiceRepository.Patients.WhereMatches(patient, new[] { "PriorPatientCode" }).FirstOrDefault();
        }

        /// <summary>
        /// Find the patient , if not found create a new patient.
        /// </summary>
        /// <param name="practiceRepository">The new practice repository</param>
        /// <param name="row">Patient data row to search for.</param>
        /// <returns></returns>
        public static Patient FindOrCreatePatient(this IPracticeRepository practiceRepository, DataRow row)
        {
            Patient patient = row.ToPatient();
            Patient existingPatient = practiceRepository.FindMatchingPatient(patient);

            if (existingPatient == null)
            {
                practiceRepository.Save(patient);
                return patient;
            }

            return existingPatient;
        }

        #region Nested type: Address

        public class Address
        {

            private string _streetAddress;

            public string StreetAddress
            {
                get { return _streetAddress; }
                set { _streetAddress = value; }
            }

            private string _suite;

            public string Suite
            {
                get { return _suite; }
                set { _suite = value; }
            }

            // write constructor with validation logic

            public Address(string streetAddress, string suite)
            {
                if (streetAddress.Length == 1)
                    streetAddress = "";

                StreetAddress = streetAddress.ToUpper();

                if (suite.Length == 1)
                    suite = "";

                Suite = suite.ToUpper();
            }

            public void ParseAddress(bool isPatient = false)
            {

                if (!String.IsNullOrEmpty(_suite) || _streetAddress.Contains(" STE ") || _streetAddress.Contains(" SUITE") || _streetAddress.Contains(",STE ") || _streetAddress.Contains(",SUITE") || _streetAddress.Contains("APT"))
                {
                    string addressCombined;

                    if (!(String.IsNullOrEmpty(_streetAddress)))
                    {
                        if (!(String.IsNullOrEmpty(_suite)))
                            addressCombined = _streetAddress + ", " + _suite;
                        else
                            addressCombined = _streetAddress;
                    }
                    else
                    {
                        addressCombined = _suite;
                    }


                    if (addressCombined != null)
                    {
                        int suitePosition = addressCombined.LastIndexOf("STE", StringComparison.Ordinal);
                        suitePosition = suitePosition == -1 ? addressCombined.LastIndexOf("APT", StringComparison.Ordinal) : suitePosition;

                        string suiteTemp;
                        if (suitePosition != -1)
                        {
                            suiteTemp = addressCombined.Substring(suitePosition, addressCombined.Length - suitePosition).Trim();

                            if (suiteTemp.Length > 3)
                            {
                                _streetAddress = addressCombined.Substring(0, suitePosition).Trim();
                                _suite = suiteTemp;
                            }
                            else
                            {
                                _streetAddress = addressCombined.Substring(0, suitePosition).Trim();
                                _suite = String.Empty;
                                if (_streetAddress.LastIndexOf(",", StringComparison.Ordinal) == _streetAddress.Length - 1)
                                {
                                    _streetAddress = _streetAddress.Substring(0, _streetAddress.Length - 1).Trim();
                                }
                            }
                        }
                        else
                        {
                            suitePosition = addressCombined.LastIndexOf("SUITE", StringComparison.Ordinal);

                            if (suitePosition != -1)
                            {
                                suiteTemp = addressCombined.Substring(suitePosition, addressCombined.Length - suitePosition).Trim();

                                if (suiteTemp.Length > 5)
                                {
                                    _streetAddress = addressCombined.Substring(0, suitePosition).Trim();
                                    _suite = suiteTemp;
                                }
                            }
                            else
                            {
                                _streetAddress = addressCombined.Trim();
                                _suite = String.Empty;
                            }
                        }
                    }
                }

                if (!String.IsNullOrEmpty(_streetAddress))
                {
                    if (_streetAddress.LastIndexOf(",", StringComparison.Ordinal) == _streetAddress.Length - 1)
                    {
                        _streetAddress = _streetAddress.Substring(0, _streetAddress.Length - 2).Trim();
                    }


                    if (isPatient == false)
                    {
                        if (_streetAddress.Length > 50 && String.IsNullOrEmpty(_suite))
                        {
                            _suite = _streetAddress.Substring(50, _streetAddress.Length - 50).Trim();
                            _streetAddress = _streetAddress.Substring(0, 50).Trim();
                        }
                    }
                }

                StreetAddress = _streetAddress;
                Suite = _suite;

            }
        }

        #endregion

        public static void DisplayProgress(Int32 numberOfRowsInDataTable, Int32 counter)
        {
            try
            {
                double progress = Convert.ToDouble(counter) / numberOfRowsInDataTable;
                Console.Clear();
                Console.WriteLine("Progress: {0}", progress.ToString("#0.##%", CultureInfo.InvariantCulture));
            }
            catch { }
            
        }

    }
}