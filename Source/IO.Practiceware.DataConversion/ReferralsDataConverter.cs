﻿
using IO.Practiceware.Model.Legacy;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class ReferralsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId", "OldInsurerId", "IOLocationId", "IOPracticeDoctorId" };
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext);

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            return argumentValidationResults;

        }

    }

    public class ReferralsDataConverterMetrics : IConverterMetrics
    {
        private ReferralsDataConverterMetrics()
        {
        }


        private static ReferralsDataConverterMetrics _instance;

        public static ReferralsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new ReferralsDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }

        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Referrals Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records Converted: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);


            return report.ToString();

        }

    }

    /// <summary>
    /// Data Converter for Referrals
    /// </summary>
    public class ReferralsDataConverter : DataConverter
    {

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new ReferralsDataConverterArgumentsValidator();
            }
        }

        public override string[] AffectedTableNames
        {
            get { return new[] { "dbo.PatientReferral" }; }
        }

        public override IConverterMetrics Metrics
        {
            get
            {
                return ReferralsDataConverterMetrics.Instance;
            }
        }

        protected override void Run(DataConverterArguments arguments)
        {

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            foreach (DataRow row in arguments.DataTable.Rows)
            {

                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var oldInsurerId = row.GetValueOrDefault<string>("OldInsurerId");

                if (string.IsNullOrEmpty(oldInsurerId))
                    continue;


                int insurerId = PracticeRepository.FindInsurerIdByOldInsurerId(oldInsurerId);

                if (insurerId == 0)
                    continue;

                string oldPatientId = row.Get<string>("OldPatientId");

                if (string.IsNullOrEmpty(oldPatientId))
                    continue;

                int patientId = PracticeRepository.FindPatientIdByOldPatientId(oldPatientId);

                if (patientId == 0)
                    continue;
                
                var oldReferringDoctorId = row.GetValueOrDefault<string>("OldReferringDoctorId");

                if (string.IsNullOrEmpty(oldReferringDoctorId))
                    continue;


                int referringDoctorId = LegacyPracticeRepository.FindReferringDoctorIdByOldReferringDoctorId(oldReferringDoctorId);

                if (referringDoctorId == 0)
                    continue;
                
                var referral = new PatientReferral
                                   {
                                       PatientId = patientId,
                                       ReferredInsurer = insurerId,
                                       ReferralVisits = row.GetValueOrDefault<int>("TotalVisits"),
                                       ReferralVisitsLeft = row.GetValueOrDefault<int>("VisitsRemaining"),
                                       ReferralDate = Utilities.ValidateDate(row.GetValueOrDefault<string>("ReferralStartDate")),
                                       ReferralExpireDate = Utilities.ValidateDate(row.GetValueOrDefault<string>("ReferralEndDate")),
                                       Referral = row.GetValueOrDefault<string>("ReferralNumber"),
                                       Reason = row.GetValueOrDefault<string>("Reason"),
                                       ReferredTo = row.GetValueOrDefault<int>("IOPracticeDoctorId"),
                                       ReferredFromId = referringDoctorId,
                                   };

                referral.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                bool isDuplicateReferral = LegacyPracticeRepository.PatientReferrals.WhereMatches(referral, "PatientId", "Referral").ToArray().Length > 0;

                if (!isDuplicateReferral)
                {
                    LegacyPracticeRepository.Save(referral);
                    ReferralsDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                {
                    ReferralsDataConverterMetrics.Instance.TotalDuplicates++;
                }
            }
        }
    }

}