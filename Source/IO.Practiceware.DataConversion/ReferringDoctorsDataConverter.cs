﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model.Legacy;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;


namespace IO.Practiceware.DataConversion
{

    public class ReferringDoctorsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "ReferringDoctorFullName", "ReferringDoctorStreetAddress" };
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentValidationResults = ValidateColumnsInDataTable(validationContext);

            return argumentValidationResults;

        }

    }

    public class ReferringDoctorsDataConverterResultsValidator : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidZipQuery = validationConnection.Execute<DataTable>(@"SELECT  *
FROM    dbo.PracticeVendors
WHERE   LEN(VendorZip) < 3 AND (VendorZip <> '')
ORDER BY VendorLastName, VendorFirstName
");

                if (invalidZipQuery.Rows.Count > 0)
                {
                    throw new ArgumentException("Invalid referring doctor zip code.");
                }

                var invalidCityQuery = validationConnection.Execute<DataTable>(@"SELECT  *
FROM    dbo.PracticeVendors
WHERE   LEN(VendorCity) < 3 AND (VendorCity <> '')
ORDER BY VendorLastName, VendorFirstName");

                if (invalidCityQuery.Rows.Count > 0)
                {
                    throw new ArgumentException("Invalid referring doctor city.");
                }
            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;

        }
    }


    public class ReferringDoctorsDataConverterMetrics : IConverterMetrics
    {
        private ReferringDoctorsDataConverterMetrics()
        {
        }

        private static ReferringDoctorsDataConverterMetrics _instance;

        public static ReferringDoctorsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new ReferringDoctorsDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }

        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Referring Doctors Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records Converted: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            return report.ToString();

        }

    }


    /// <summary>
    /// Data converter for referring doctors.
    /// </summary>
    public class ReferringDoctorsDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[] { "dbo.PracticeVendors" }; }
        }

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new ReferringDoctorsDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
            get
            {
                return new ReferringDoctorsDataConverterResultsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {
            get
            {
                return ReferringDoctorsDataConverterMetrics.Instance;
            }
        }

        protected override void Run(DataConverterArguments arguments)
        {

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            foreach (DataRow row in arguments.DataTable.Rows)
            {

                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var refDocAddress = new Utilities.Address(row.GetValueOrDefault<string>("ReferringDoctorStreetAddress"), row.GetValueOrDefault<string>("ReferringDoctorSuite"));

                refDocAddress.ParseAddress();
    

                var refDoc = new PracticeVendor
                                 {
                                     VendorOtherOffice2 = row.GetValueOrDefault<string>("OldReferringDoctorId"),
                                     VendorName=row.Get<string>("ReferringDoctorFullName"),
                                     VendorLastName=row.Get<string>("ReferringDoctorLastName"),
                                     VendorFirstName=row.Get<string>("ReferringDoctorFirstName"),
                                     VendorMI=row.GetValueOrDefault<string>("ReferringDoctorMiddleInitial"),
                                     VendorTitle=row.GetValueOrDefault<string>("ReferringDoctorTitle"),
                                     VendorSalutation=row.GetValueOrDefault<string>("ReferringDoctorSalutation").ToAlphanumeric(),
                                     VendorAddress=refDocAddress.StreetAddress,
                                     VendorSuite=refDocAddress.Suite,
                                     VendorCity=row.Get<string>("ReferringDoctorCity").Remove(character => Strings.IsNotAlphanumeric(character) && character != ' '),
                                     VendorState=row.Get<string>("ReferringDoctorState"),
                                     VendorZip=row.Get<string>("ReferringDoctorZip"),
                                     VendorPhone=row.GetValueOrDefault<string>("ReferringDoctorOfficePhone"),
                                     VendorFax=row.GetValueOrDefault<string>("ReferringDoctorFax"),
                                     VendorCellPhone=row.GetValueOrDefault<string>("ReferringDoctorCell"),
                                     VendorEmail=row.GetValueOrDefault<string>("ReferringDoctorEmail"),
                                     VendorNPI=row.GetValueOrDefault<string>("ReferringDoctorNPI"),
                                     VendorTaxId=row.GetValueOrDefault<string>("ReferringDoctorTaxId"),
                                     VendorFirmName=row.GetValueOrDefault<string>("ReferringDoctorFirmName")
                                 };


                refDoc.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                var matchingRefDocs = LegacyPracticeRepository.PracticeVendors.WhereMatches(refDoc, new[]{"VendorName", "VendorFirstName", "VendorLastName", "VendorMI"}).ToArray();

                bool isDuplicateReferringDoctor = matchingRefDocs.Length > 0;

                if (!isDuplicateReferringDoctor)
                {
                    LegacyPracticeRepository.Save(refDoc);
                    ReferringDoctorsDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                {
                    ReferringDoctorsDataConverterMetrics.Instance.TotalDuplicates++;
                }

            }

            CleanUpReferringDoctors();

        }

        

        private void CleanUpReferringDoctors()
        {
            using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
            {
                cleanupConnection.Execute(
                    @"UPDATE PracticeVendors
SET VendorName = UPPER( VendorName ),
VendorFirstName = UPPER( VendorFirstName ),
VendorLastName = UPPER( VendorLastName ),
VendorAddress = UPPER( VendorAddress ),
VendorCity = UPPER( VendorCity )

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'D O', 'D.O.')
WHERE VendorName LIKE '% D O' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'DO', 'D.O.')
WHERE VendorName LIKE '% DO' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'M D', 'M.D.')
WHERE VendorName LIKE '% M D' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'MD', 'M.D.')
WHERE VendorName LIKE '% MD' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'O D', 'O.D.')
WHERE VendorName LIKE '% O D' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'OD', 'O.D.')
WHERE VendorName LIKE '% OD'

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, ' M.D.', ', M.D.')
WHERE VendorName LIKE '% M.D.' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, 'DR ', 'DR. ')
WHERE VendorName LIKE 'DR %' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, '  ', ' ')
WHERE VendorName LIKE '%  %'

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, '   ', ' ')
WHERE VendorName LIKE '%   %' 

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, ' _ ', ' _. ')
WHERE VendorName LIKE '% _ %' 

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' M D', '')
WHERE VendorLastName LIKE '% M D' 

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' MD', '')
WHERE VendorLastName LIKE '% MD' 

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' D O', '')
WHERE VendorLastName LIKE '% D O' 

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' OD', '')
WHERE VendorLastName LIKE '% OD'

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' O D', '')
WHERE VendorLastName LIKE '% O D' 

UPDATE PracticeVendors
SET VendorLastName = REPLACE (VendorLastName, ' DO', '')
WHERE VendorLastName LIKE '% DO' 

UPDATE PracticeVendors
SET VendorCity = REPLACE (VendorCity, 'NY', 'NEW YORK')
WHERE VendorCity LIKE 'NY'

UPDATE PracticeVendors
SET VendorName = REPLACE (VendorName, ',,', ',')
WHERE VendorName LIKE '%,,%'

UPDATE PracticeVendors
SET VendorName = VendorFirmName
WHERE VendorName IS NULL

UPDATE PracticeVendors
SET VendorName = VendorFirstName + VendorLastName
WHERE VendorName IS NULL

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'POB ', 'PO BOX ')
WHERE VendorAddress LIKE 'POB %'

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'P.O.BOX ', 'PO BOX ')
WHERE VendorAddress LIKE 'P.O.B%'

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'P.O. BOX ', 'PO BOX ')
WHERE VendorAddress LIKE 'P.O. BOX %'

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'POBOX ', 'PO BOX ')
WHERE VendorAddress LIKE 'POBOX %'

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'BOX ', 'PO BOX ')
WHERE VendorAddress LIKE 'BOX %'

UPDATE PracticeVendors
SET VendorAddress = REPLACE (VendorAddress, 'P O BOX', 'PO BOX ')
WHERE VendorAddress LIKE 'P O %'
");
            }
        }

    }

}