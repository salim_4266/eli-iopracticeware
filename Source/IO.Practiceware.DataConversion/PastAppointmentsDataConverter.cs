﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class PastAppointmentsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {

        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId", "IODoctorId", "IOLocationId", "IOAppointmentTypeId" };
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var argumentValidationResults = new List<ValidationResult>();
            var arguments = (DataConverterArguments)validationContext.Items[typeof(DataConverterArguments)];

            if (arguments != null && arguments.DataTable.Columns.Contains("IODoctorId"))
            {

                var listOfPracticeDoctorIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IODoctorId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfPracticeDoctorIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT ResourceId
FROM Resources
WHERE ResourceType = 'D'
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();



                    bool allArePresent = listOfPracticeDoctorIdsInData.Intersect(listOfPracticeDoctorIdsInDatabase).Count() == listOfPracticeDoctorIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A practice doctor ID present in the data table does not match any doctor Ids present in the database."));
                    }

                }
            }

            if (arguments != null && arguments.DataTable.Columns.Contains("IOLocationId"))
            {

                var listOfLocationIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOLocationId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfLocationIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT PracticeId + 1000
FROM PracticeName
WHERE LocationReference <> ''

UNION

SELECT ResourceId
FROM Resources
WHERE ResourceType = 'R'

UNION

SELECT 0
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();

                    bool allArePresent = listOfLocationIdsInData.Intersect(listOfLocationIdsInDatabase).Count() == listOfLocationIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A location ID present in the data table does not match any location Ids present in the database."));
                    }

                }
            }


            if (arguments != null && arguments.DataTable.Columns.Contains("IOAppointmentTypeId"))
            {

                var listOfAppointmentTypeIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOAppointmentTypeId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfAppointmentTypeIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT AppTypeId
FROM AppointmentType
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();

                    bool allArePresent = listOfAppointmentTypeIdsInData.Intersect(listOfAppointmentTypeIdsInDatabase).Count() == listOfAppointmentTypeIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("An appointment type ID present in the data table does not match any appointment type Ids present in the database."));
                    }

                }

            }

            return argumentValidationResults;


        }
    }


    public class PastAppointmentsDataConverterResultsValidator : IValidatableObject
    {
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            
            var validationResults = new List<ValidationResult>();

            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidResourceId1Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId1 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'D'
		)
");

                if (invalidResourceId1Query.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Validation requirements not met: resource ID not present in Resources table."));
                }

                var invalidResourceId2Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId2 <> 0
	AND ResourceId2 NOT IN (
		SELECT PracticeId + 1000
		FROM PracticeName
		WHERE PracticeAddress <> ''
		)
	AND ResourceId2 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'R'
		)
");

                if (invalidResourceId2Query.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Validation requirements not met: location ID not present in PracticeName or Resources tables."));
                }

                var invalidAppointmentTypeQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE AppTypeId NOT IN (
		SELECT AppTypeId
		FROM AppointmentType
		)
");

                if (invalidAppointmentTypeQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Validation requirements not met: appointment type ID not present in AppointmentType table."));
                }
            }

            return validationResults;

        }
    }

    public class PastAppointmentsDataConverterMetrics : IConverterMetrics
    {
        private PastAppointmentsDataConverterMetrics()
        {
        }

        private static PastAppointmentsDataConverterMetrics _instance;

        public static PastAppointmentsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new PastAppointmentsDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }
        public int TotalDuplicates { get; set; }

        public int TotalPatientsAdded { get; set; }
        public int TotalPatientsNotLinked { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Past Appointments Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            report.AppendFormat("Total Patients Not Linked: {0}" + Environment.NewLine + Environment.NewLine, TotalPatientsNotLinked);

            return report.ToString();

        }

    }


    /// <summary>
    /// Data Converter for Past Appointments. This converter posts an appointment record and an activity record for each appointment converted, so that information can be entered into these visits
    /// via Edit Previous Visit. This converter is similar to the clinical converter except that it doesn't post clinical rows.
    /// </summary>
    public class PastAppointmentsDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[] { "dbo.Appointments", "dbo.PracticeActivity" }; }
        }

        

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new PastAppointmentsDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
            get
            {
                return new PastAppointmentsDataConverterResultsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {
            get
            {
                return PastAppointmentsDataConverterMetrics.Instance;
            }
        }

        protected override void Run(DataConverterArguments arguments)
        {

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            DateTime todaysDate = DateTime.Now;
            string todaysDateString = todaysDate.ToString("yyyyMMdd");

            foreach (DataRow row in arguments.DataTable.Rows)
            {

                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                //link patient

                var oldPatientId = row.GetValueOrDefault<string>("OldPatientId");

                if (string.IsNullOrEmpty(oldPatientId))
                {
                    PastAppointmentsDataConverterMetrics.Instance.TotalPatientsNotLinked++;
                    continue;
                }

                int patientId = PracticeRepository.FindPatientIdByOldPatientId(oldPatientId);

                if (patientId == 0)
                {
                    PastAppointmentsDataConverterMetrics.Instance.TotalPatientsNotLinked++;
                    continue;
                }
                var dateOfService = Utilities.ValidateDate(row.GetValueOrDefault<string>("DateOfService"));

                if ((Convert.ToInt32(dateOfService) > Convert.ToInt32(todaysDateString)) || (Convert.ToInt32(dateOfService) <= 19750101))
                {
                    continue;
                }

                var doctorId = row.GetValueOrDefault<int>("IODoctorId");

                if (doctorId == 0)
                    continue;

                var locationId = row.GetValueOrDefault<int>("IOLocationId");

                var appointmentComments = row.GetValueOrDefault<string>("AppointmentComments");

                // Get appointment type ID for historical appointment type

                var appointmentTypeId = row.GetValueOrDefault<int>("IOAppointmentTypeId");

                // Check for historical appointment comment


                var pastAppointment = new Appointment
                {
                    AppDate = dateOfService,
                    AppTime = -1,
                    PatientId = patientId,
                    ApptInsType = "M",
                    PreCertId = 0,
                    ReferralId = 0,
                    AppTypeId = appointmentTypeId,
                    TechApptTypeId = 0,
                    ScheduleStatus = "D",
                    ActivityStatus = "D",
                    Comments = appointmentComments == string.Empty ? "HIGHLIGHTS" : appointmentComments,
                    Duration = 10,
                    ResourceId1 = doctorId,
                    ResourceId2 = locationId,
                    ResourceId3 = 0,
                    ResourceId4 = 0,
                    ResourceId5 = 0,
                    ResourceId6 = 0,
                    ResourceId7 = 0,
                    ResourceId8 = 0,
                    ConfirmStatus = string.Empty,
                    ApptTypeCat = string.Empty
                };


                Appointment[] matchingAppointments = LegacyPracticeRepository.Appointments.WhereMatches(pastAppointment, new[] { "PatientId", "AppDate", "AppTypeId", "ResourceId1", "ResourceId2" }).ToArray();

                int appointmentId;
                if (matchingAppointments.Length <= 1)
                {
                    if (matchingAppointments.Length == 0)
                    {
                        LegacyPracticeRepository.Save(pastAppointment);
                        appointmentId = pastAppointment.AppointmentId;
                    }
                    else
                    {
                        appointmentId = matchingAppointments[0].AppointmentId;
                    }
                }
                else
                {
                    throw new ArgumentException("Appointment is not uniquely specified.");
                }


                var clinicalActivityRecord = new PracticeActivity
                {
                    AppointmentId = appointmentId,
                    PatientId = patientId,
                    CurrentRId = 0,
                    ActivityDate = dateOfService,
                    LocationId = locationId,
                    QuestionSet = "No Questions",
                    ResourceId = doctorId,
                    StationId = 0,
                    Status = "D",
                    ActivityStatusTime = "",
                    PayMethod = "",
                    PayReference = "",
                    ActivityStatusTRef = 0
                };

                PracticeActivity[] matchingPracticeActivityRecords = LegacyPracticeRepository.PracticeActivities.WhereMatches(clinicalActivityRecord, new[] { "AppointmentId" }).ToArray();

                bool isDuplicateActivity = matchingPracticeActivityRecords.Length > 0;

                if (!isDuplicateActivity)
                {
                    LegacyPracticeRepository.Save(clinicalActivityRecord);
                }
            }

            
            ValidatePastAppointments();

        }

        private void ValidatePastAppointments()
        {
            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidResourceId1Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId1 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'D'
		)
");

                if (invalidResourceId1Query.Rows.Count > 0)
                {
                    throw new ArgumentException("Validation requirements not met: resource ID not present in Resources table.");
                }

                var invalidResourceId2Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId2 <> 0
	AND ResourceId2 NOT IN (
		SELECT PracticeId + 1000
		FROM PracticeName
		WHERE PracticeAddress <> ''
		)
	AND ResourceId2 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'R'
		)
");

                if (invalidResourceId2Query.Rows.Count > 0)
                {
                    throw new ArgumentException("Validation requirements not met: location ID not present in PracticeName or Resources tables.");
                }

                var invalidAppointmentTypeQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE AppTypeId NOT IN (
		SELECT AppTypeId
		FROM AppointmentType
		)
");

                if (invalidAppointmentTypeQuery.Rows.Count > 0)
                {
                    throw new ArgumentException("Validation requirements not met: appointment type ID not present in AppointmentType table.");
                }
            }
        }

    }

}