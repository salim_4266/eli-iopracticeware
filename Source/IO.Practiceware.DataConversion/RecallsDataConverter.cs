﻿
using IO.Practiceware.Model.Legacy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class RecallsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId" };
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext);

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            return argumentValidationResults;

        }
    }

    public class RecallsDataConverterMetrics : IConverterMetrics
    {
        private RecallsDataConverterMetrics()
        {
        }

        private static RecallsDataConverterMetrics _instance;

        public static RecallsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new RecallsDataConverterMetrics()); }
        }

        #region IConverterMetrics Members

        
        public int TotalRecordsConverted { get; set; }
        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Recalls Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            return report.ToString();
        }

        #endregion
    }

    public class RecallsDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[]{"dbo.PracticeTransactionJournal"};}
        }

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new RecallsDataConverterArgumentsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {
            get
            {
                return RecallsDataConverterMetrics.Instance;
            }
        }

        protected override void Run(DataConverterArguments arguments)
        {

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            // read all lines from the input file in a split the elements by ~

            foreach (DataRow row in arguments.DataTable.Rows)
            {

                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var oldPatientId = row.GetValueOrDefault<string>("OldPatientId");

                if (string.IsNullOrEmpty(oldPatientId))
                    continue;

                int patientId = PracticeRepository.FindPatientIdByOldPatientId(oldPatientId);

                if (patientId == 0)
                    continue;

                var recall = new PracticeTransactionJournal
                    {
                        TransactionTypeId = patientId,
                        TransactionDate = Utilities.ValidateDate(row.GetValueOrDefault<string>("Date")),
                        TransactionRef = row.GetValueOrDefault<string>("TransactionRef"),
                        TransactionType = "L",
                        TransactionStatus = "P",
                        TransactionTime = 0,
                        TransactionRemark = "",
                        TransactionAction = "-",
                        TransactionBatch = "",
                        TransactionServiceItem = 0,
                        TransactionRcvrId = 0,
                        TransactionAssignment = ""
                    };


                PracticeTransactionJournal[] matchingRecalls = LegacyPracticeRepository.PracticeTransactionJournals.WhereMatches(recall, new[] {"TransactionTypeId", "TransactionDate", "TransactionRef"}).ToArray();

                bool isDuplicateRecall = matchingRecalls.Length > 0;

                if (!isDuplicateRecall)
                {
                    LegacyPracticeRepository.Save(recall);
                    RecallsDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                {
                    RecallsDataConverterMetrics.Instance.TotalDuplicates++;
                }
            }
        }

    }
}