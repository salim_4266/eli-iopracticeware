﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using Appointment = IO.Practiceware.Model.Legacy.Appointment;
using AppointmentType = IO.Practiceware.Model.Legacy.AppointmentType;
using Question = IO.Practiceware.Model.Legacy.Question;

namespace IO.Practiceware.DataConversion
{

    public class ClinicalDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {

        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId", "IODoctorId", "IOLocationId" };
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext).ToList();

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            var arguments = (DataConverterArguments)validationContext.Items[typeof(DataConverterArguments)];

            if (arguments != null && arguments.DataTable.Columns.Contains("IODoctorId"))
            {

                var listOfPracticeDoctorIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IODoctorId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfPracticeDoctorIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT ResourceId
FROM Resources
WHERE ResourceType = 'D'
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();



                    bool allArePresent = listOfPracticeDoctorIdsInData.Intersect(listOfPracticeDoctorIdsInDatabase).Count() == listOfPracticeDoctorIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A practice doctor ID present in the data table does not match any doctor Ids present in the database."));
                    }

                }
            }

            if (arguments != null && arguments.DataTable.Columns.Contains("IOLocationId"))
            {

                var listOfLocationIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOLocationId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfLocationIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT PracticeId + 1000
FROM PracticeName
WHERE LocationReference <> ''

UNION

SELECT ResourceId
FROM Resources
WHERE ResourceType = 'R'

UNION

SELECT 0
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();

                    bool allArePresent = listOfLocationIdsInData.Intersect(listOfLocationIdsInDatabase).Count() == listOfLocationIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A location ID present in the data table does not match any location Ids present in the database."));
                    }

                }
            }





            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var appointmentTypeId = validationConnection.Execute<DataTable>(
                    @"SELECT AppTypeId FROM AppointmentType
WHERE AppointmentType LIKE 'HISTORICAL%'").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();


                if (appointmentTypeId.Length == 0)
                {
                    argumentValidationResults.Add(new ValidationResult("A historical appointment type is not present in the database."));
                }

            }



            return argumentValidationResults;


        }
    }

    public class ClinicalDataConverterMetrics : IConverterMetrics
    {

        private ClinicalDataConverterMetrics()
        {
        }

        private static ClinicalDataConverterMetrics _instance;

        public static ClinicalDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new ClinicalDataConverterMetrics()); }
        }

        public int TotalClinicalHistoryAppointmentsConverted { get; set; }
        public int TotalClinicalHistoryActivityRecordsConverted { get; set; }
        public int TotalClinicalHistoryPatientClinicalRowsConverted { get; set; }

        public int TotalDuplicateClinicalHistoryAppointments { get; set; }
        public int TotalDuplicateClinicalHistoryActivityRecords { get; set; }
        public int TotalDuplicatePatientClinicalRows { get; set; }

        public string EarliestClinicalHistoryAppointmentDate { get; set; }
        public string MostRecentClinicalHistoryAppointmentDate { get; set; }

        public int TotalRecordsConverted { get; set; }
        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Clinical History Converter Metrics" + Environment.NewLine + Environment.NewLine);

            using (IDbConnection metricsConnection = DbConnectionFactory.PracticeRepository)
            {
                report.AppendFormat("Total Clinical History Appointments Converted in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalClinicalHistoryAppointmentsConverted);

                report.AppendFormat("Total Clinical History Activity Records Converted in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalClinicalHistoryActivityRecordsConverted);

                report.AppendFormat("Total Clinical History Patient Clinical Rows Converted in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalClinicalHistoryPatientClinicalRowsConverted);

                report.AppendFormat("Total Duplicate Clinical History Appointments Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicateClinicalHistoryAppointments);

                report.AppendFormat("Total Duplicate Activity Records Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicateClinicalHistoryActivityRecords);

                report.AppendFormat("Total Duplicate PatientClinical Rows: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicatePatientClinicalRows);

                EarliestClinicalHistoryAppointmentDate = metricsConnection.Execute<string>(
                    @"SELECT MIN(AppDate)
FROM Appointments
WHERE AppTime = - 1
");

                report.AppendFormat("Earliest Clinical History Appointment Date: {0}" + Environment.NewLine + Environment.NewLine, EarliestClinicalHistoryAppointmentDate);

                MostRecentClinicalHistoryAppointmentDate = metricsConnection.Execute<string>(
                @"SELECT MAX(AppDate)
FROM Appointments
WHERE AppTime = - 1
");

                report.AppendFormat("Most Recent Clinical Appointment Date: {0}" + Environment.NewLine + Environment.NewLine, MostRecentClinicalHistoryAppointmentDate);


            }

            return report.ToString();
        }
    }


    /// <summary>
    /// Data converter for clinical history.
    /// </summary>
    public class ClinicalDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[] { "dbo.PracticeActivity", "dbo.Appointments", "dbo.PatientClinical" }; }
        }



        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new ClinicalDataConverterArgumentsValidator();
            }
        }


        public override IConverterMetrics Metrics
        {

            get
            {
                return ClinicalDataConverterMetrics.Instance;
            }
        }

        private class PatientClinicalKey : IEquatable<PatientClinicalKey>
        {
            public PatientClinicalKey(int patientId, int appointmentId, string clinicalType, string findingDetail)
            {
                FindingDetail = findingDetail;
                ClinicalType = clinicalType;
                AppointmentId = appointmentId;
                PatientId = patientId;
            }

            public int PatientId { get; set; }
            public int AppointmentId { get; set; }
            public string ClinicalType { get; set; }
            public string FindingDetail { get; set; }

            public bool Equals(PatientClinicalKey other)
            {
                return other != null &&
                       other.PatientId == PatientId &&
                       other.AppointmentId == AppointmentId &&
                       other.ClinicalType == ClinicalType &&
                       other.FindingDetail == FindingDetail;
            }

            public override bool Equals(object obj)
            {
                return Equals(obj as PatientClinicalKey);
            }

            public override int GetHashCode()
            {
                return PatientId.GetHashCode() ^ AppointmentId.GetHashCode() ^ ClinicalType.GetHashCode() ^ FindingDetail.GetHashCode();
            }
        }


        /// <summary>
        ///   Gets or sets the DynamicForms repository. Used to access data from the DynamicForms data source.
        /// </summary>
        /// <value> The practice repository. </value>
        [Dependency]
        public virtual IDynamicFormsRepository DynamicFormsRepository { get; set; }

        private IEnumerable<Form> _forms;
        private IEnumerable<FormsControl> _formsControls;
        private IEnumerable<Question> _questions;
        private IList<PatientClinical> _patientClinicals;
        private IList<PracticeActivity> _practiceActivities;
        private readonly IDictionary<int, HashSet<PracticeActivity>> _practiceActivitiesByAppointmentId = new Dictionary<int, HashSet<PracticeActivity>>();
        private readonly IDictionary<PatientClinicalKey, HashSet<PatientClinical>> _patientClinicalsByKey = new Dictionary<PatientClinicalKey, HashSet<PatientClinical>>();
        private readonly IDictionary<int, HashSet<PatientClinical>> _patientClinicalsByAppointmentId = new Dictionary<int, HashSet<PatientClinical>>();
        private readonly IDictionary<object, ObjectStateEntry> _objectStateEntries = new Dictionary<object, ObjectStateEntry>();
        private IList<Appointment> _appointments;
        private IDictionary<Tuple<int?, string, int?, int?, int?>, HashSet<Appointment>> _appointmentsByKey;


        private HashSet<PatientClinical> GetPatientClinicalsByKey(int patientId, int appointmentId, string clinicalType, string findingDetail)
        {
            var key = new PatientClinicalKey(patientId, appointmentId, clinicalType, findingDetail);
            HashSet<PatientClinical> result;
            if (!_patientClinicalsByKey.TryGetValue(key, out result))
            {
                _patientClinicalsByKey[key] = result = new HashSet<PatientClinical>();
            }
            return result;
        }

        private HashSet<PatientClinical> GetPatientClinicalsByKey(PatientClinical patientClinical)
        {
            return GetPatientClinicalsByKey(patientClinical.PatientId ?? 0, patientClinical.AppointmentId ?? 0, patientClinical.ClinicalType, patientClinical.FindingDetail);
        }

        private void SaveAppointment(Appointment appointment)
        {
            LegacyPracticeRepository.Save(appointment);

            _appointments.Add(appointment);
            _appointmentsByKey.GetValue(Tuple.Create(appointment.PatientId, appointment.AppDate, appointment.AppTypeId, appointment.ResourceId1, appointment.ResourceId2), () => new HashSet<Appointment>()).Add(appointment);

            _objectStateEntries[appointment] = new ObjectStateEntry(appointment) { State = ObjectState.Unchanged };
        }


        private void SavePracticeActivity(PracticeActivity practiceActivity)
        {
            _practiceActivities.Add(practiceActivity);
            _practiceActivitiesByAppointmentId.GetValue(practiceActivity.AppointmentId ?? 0, () => new HashSet<PracticeActivity>()).Add(practiceActivity);
            _objectStateEntries[practiceActivity] = new ObjectStateEntry(practiceActivity) { State = ObjectState.Added };
        }

        private void SavePatientClinical(PatientClinical patientClinical)
        {
            ObjectStateEntry objectStateEntry;
            if (!_objectStateEntries.TryGetValue(patientClinical, out objectStateEntry))
            {
                _objectStateEntries[patientClinical] = objectStateEntry = new ObjectStateEntry(patientClinical);
            }

            objectStateEntry.State = patientClinical.ClinicalId == 0 ? ObjectState.Added : ObjectState.Modified;

            _patientClinicals.Add(patientClinical);

            GetPatientClinicalsByKey(patientClinical.PatientId ?? 0, patientClinical.AppointmentId ?? 0, patientClinical.ClinicalType, patientClinical.FindingDetail).Add(patientClinical);
            _patientClinicalsByAppointmentId.GetValue(patientClinical.AppointmentId ?? 0, () => new HashSet<PatientClinical>()).Add(patientClinical);
        }

        protected override void Run(DataConverterArguments arguments)
        {
            using (var currScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _objectStateEntries.Clear();
                _forms = DynamicFormsRepository.Forms.ToArray();
                _formsControls = DynamicFormsRepository.FormsControls.ToArray();
                _questions = DynamicFormsRepository.Questions.ToArray();
            }
            _patientClinicals = LegacyPracticeRepository.PatientClinicals.ToList();
            _practiceActivities = LegacyPracticeRepository.PracticeActivities.ToList();
            IList<Patient> cachedPatients = PracticeRepository.Patients.ToList();

            var appointmentTypes = LegacyPracticeRepository.AppointmentTypes.ToArray();

            AppointmentType[] historicalAppointmentType = appointmentTypes.Where(r => r.AppointmentType1 == "HISTORICAL").ToArray();
            _appointments = LegacyPracticeRepository.Appointments.ToList();

            _appointmentsByKey = _appointments
                .GroupBy(i => Tuple.Create(i.PatientId, i.AppDate, i.AppTypeId, i.ResourceId1, i.ResourceId2))
                .ToDictionary(i => i.Key, i => new HashSet<Appointment>(i.ToArray()));

            var patientsByOldId = cachedPatients.Where(i => i.PriorPatientCode != null).GroupBy(i => i.PriorPatientCode).ToDictionary(i => i.Key, i => i.First().Id);

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            // Every 100 rows, update the progress of the converter.
            if (counter % 100 == 0)
            {
                Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
            }

            var procedures = File.ReadAllLines(arguments.Settings.First(s => s.Name == "ProceduresListPath").Value).Select(r => r.Split(new[] { "::" }, StringSplitOptions.RemoveEmptyEntries))
                        .ToDictionary(r => r[0], s => s.Length > 2 ? s[2] : string.Empty);

            int historicalAppointmentTypeId = 0;

            DateTime todaysDate = DateTime.Now;
            string todaysDateString = todaysDate.ToString("yyyyMMdd");

            foreach (DataRow row in arguments.DataTable.Rows)
            {
                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                //link patient

                var oldPatientId = row.GetValueOrDefault<string>("OldPatientId");

                if (string.IsNullOrEmpty(oldPatientId))
                    continue;

                int patientId = patientsByOldId.GetValue(oldPatientId);

                if (patientId == 0)
                    continue;

                var dateOfService = Utilities.ValidateDate(row.GetValueOrDefault<string>("DateOfService"));

                if ((Convert.ToInt32(dateOfService) > Convert.ToInt32(todaysDateString)) || (Convert.ToInt32(dateOfService) <= 19750101))
                {
                    continue;
                }

                var doctorId = row.GetValueOrDefault<int>("IODoctorId");

                if (doctorId == 0)
                    continue;

                var locationId = row.GetValueOrDefault<int>("IOLocationId");

                var appointmentComments = row.GetValueOrDefault<string>("HistoricalAppointmentComments");

                // Get appointment type ID for historical appointment type

                if (historicalAppointmentType.Length == 1)
                {
                    historicalAppointmentTypeId = historicalAppointmentType[0].AppTypeId;
                }

                // Check for historical appointment comment


                var clinicalAppointment = new Appointment
                                              {
                                                  AppDate = dateOfService,
                                                  AppTime = 0,
                                                  PatientId = patientId,
                                                  ApptInsType = "M",
                                                  PreCertId = 0,
                                                  ReferralId = 0,
                                                  AppTypeId = historicalAppointmentTypeId,
                                                  TechApptTypeId = 0,
                                                  ScheduleStatus = "D",
                                                  ActivityStatus = "D",
                                                  Comments = appointmentComments == string.Empty ? "HIGHLIGHTS" : appointmentComments,
                                                  Duration = 10,
                                                  ResourceId1 = doctorId,
                                                  ResourceId2 = locationId,
                                                  ResourceId3 = 0,
                                                  ResourceId4 = 0,
                                                  ResourceId5 = 0,
                                                  ResourceId6 = 0,
                                                  ResourceId7 = 0,
                                                  ResourceId8 = 0,
                                                  ConfirmStatus = string.Empty,
                                                  ApptTypeCat = string.Empty
                                              };


                Appointment[] matchingAppointments = _appointmentsByKey.GetValue(Tuple.Create(clinicalAppointment.PatientId, clinicalAppointment.AppDate, clinicalAppointment.AppTypeId, clinicalAppointment.ResourceId1, clinicalAppointment.ResourceId2), () => new HashSet<Appointment>()).ToArray();

                int appointmentId;
                if (matchingAppointments.Length <= 1)
                {
                    if (matchingAppointments.Length == 0)
                    {
                        SaveAppointment(clinicalAppointment);
                        appointmentId = clinicalAppointment.AppointmentId;
                        ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryAppointmentsConverted++;
                    }
                    else
                    {
                        appointmentId = matchingAppointments[0].AppointmentId;
                        ClinicalDataConverterMetrics.Instance.TotalDuplicateClinicalHistoryAppointments++;
                    }
                }
                else
                {
                    throw new ArgumentException("Appointment is not uniquely specified.");
                }


                var clinicalActivityRecord = new PracticeActivity
                                                 {
                                                     AppointmentId = appointmentId,
                                                     PatientId = patientId,
                                                     CurrentRId = 0,
                                                     ActivityDate = dateOfService,
                                                     LocationId = locationId,
                                                     QuestionSet = "No Questions",
                                                     ResourceId = doctorId,
                                                     StationId = 0,
                                                     Status = "D",
                                                     ActivityStatusTime = "",
                                                     PayMethod = "",
                                                     PayReference = "",
                                                     ActivityStatusTRef = 0
                                                 };

                PracticeActivity[] matchingPracticeActivityRecords = _practiceActivitiesByAppointmentId.GetValue(clinicalActivityRecord.AppointmentId ?? 0, () => new HashSet<PracticeActivity>()).ToArray();

                bool isDuplicateActivity = matchingPracticeActivityRecords.Length > 0;

                if (!isDuplicateActivity)
                {
                    SavePracticeActivity(clinicalActivityRecord);
                    ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryActivityRecordsConverted++;
                }
                else
                {
                    ClinicalDataConverterMetrics.Instance.TotalDuplicateClinicalHistoryActivityRecords++;
                }


                var procedureCode = row.GetValueOrDefault<string>("ProcedureCode");

                var procedureModifier1 = row.GetValueOrDefault<string>("ProcedureModifier1");
                var procedureModifier2 = row.GetValueOrDefault<string>("ProcedureModifier2");

                if (procedureCode.Substring(0, 1) != "J")
                {

                    if (procedureCode == "67028")
                    {
                        PostIntravitrealInjection(procedures, arguments.DataTable, oldPatientId, patientId, appointmentId, dateOfService, doctorId, locationId, procedureCode, procedureModifier1, procedureModifier2);
                    }
                    else
                    {
                        string procedureDescription = QueryDynamicFormsForProcedure(procedureCode, procedures);

                        if (!(string.IsNullOrEmpty(procedureDescription)))
                        {

                            EyeControls eyeControls = GetEyeControls(procedureDescription, procedureCode, procedureModifier1, procedureModifier2);

                            PostClinicalProcedure(patientId, appointmentId, procedureDescription, eyeControls, procedureModifier1, procedureModifier2);
                        }
                    }

                }


                var diagnosisString = row.GetValueOrDefault<string>("DiagnosisString");

                if (!(string.IsNullOrEmpty(diagnosisString)) && (diagnosisString.IndexOf(".", StringComparison.Ordinal) != -1))
                {

                    var diagnosisArray = diagnosisString.Split(';');

                    foreach (var currentDiagnosis in diagnosisArray)
                    {
                        PostDiagnosis(currentDiagnosis, patientId, appointmentId);
                    }
                }
            }

            RepositoryService.Persist(_objectStateEntries.Values, typeof(ILegacyPracticeRepository).FullName);
        }

        private void PostIntravitrealInjection(Dictionary<string, string> procedures, DataTable clinicalHistoryDataTable, string oldPatientId, int patientId, int appointmentId, string dos, int doctorId, int locationId, string procedureCode, string procedureModifier1, string procedureModifier2)
        {
            var jCodes = GetDrugCodesForProcedure(clinicalHistoryDataTable, oldPatientId, dos, locationId, doctorId);

            if (jCodes.Length == 0)
            {
                string procedureDescription = QueryDynamicFormsForProcedure(procedureCode, procedures);

                if (string.IsNullOrEmpty(procedureDescription))
                    throw new ArgumentException("No procedure description for 67028 (Intravitreal Injection).");

                EyeControls eyeControls = GetEyeControls(procedureDescription, procedureCode, procedureModifier1, procedureModifier2);

                PostClinicalProcedure(patientId, appointmentId, procedureDescription, eyeControls, procedureModifier1, procedureModifier2);
            }
            else
            {
                foreach (var drugCode in jCodes)
                {
                    string procedureDescription = QueryDynamicFormsForProcedure(drugCode, procedures);

                    if (String.IsNullOrEmpty(procedureDescription))
                        continue;

                    EyeControls drugEyeControls = GetEyeControls(procedureDescription, drugCode, procedureModifier1, procedureModifier2);
                    PostClinicalProcedure(patientId, appointmentId, procedureDescription, drugEyeControls, procedureModifier1, procedureModifier2);
                }
            }
        }

        private void PostEyeModifierRow(int patientId, int appointmentId, string procedureDescription, EyeControls procedureEyeControls, string procedureModifier1, string procedureModifier2)
        {
            if (procedureEyeControls.ModifierId == 0)
                return;

            var patientClinicalEyeModifierRow = new PatientClinical
                                                    {
                                                        PatientId = patientId,
                                                        AppointmentId = appointmentId,
                                                        ClinicalType = "F",
                                                        EyeContext = "",
                                                        Symptom = "/" + procedureDescription.ToUpper(),
                                                        FindingDetail = "*" + procedureEyeControls.ModifierName + "=T" + procedureEyeControls.ModifierId.ToString(CultureInfo.InvariantCulture).Trim() + " <HIGHLIGHT>",
                                                        ImageDescriptor = "",
                                                        ImageInstructions = "",
                                                        DrawFileName = "",
                                                        Status = "A",
                                                        Highlights = "T",
                                                        FollowUp = "",
                                                        PermanentCondition = "",
                                                        PostOpPeriod = 0,
                                                        Surgery = "",
                                                        Activity = ""
                                                    };


            var matchingPatientClinicalEyeModifierRows = GetPatientClinicalsByKey(patientClinicalEyeModifierRow).Where(c => c.Symptom == patientClinicalEyeModifierRow.Symptom).ToArray();

            // The eye modifier row itself is identical regardless of whether or not the modifier is LT or RT. What determines its significance is the row that
            // comes directly before it. If the modifier is LT, the eye modifier row is posted after the left eye row. If the modifier is RT, the eye modifier
            // row is posted after the right eye row. If the modifier is "50", a modifier row should be posted after both the left and right-eye procedure rows.
            // Therefore, to check for duplicate eye rows for modifier 50, check to ensure that 2 such rows aren't already present for the record.

            if (procedureModifier1 == "50" || procedureModifier2 == "50")
            {
                if (matchingPatientClinicalEyeModifierRows.Length < 2)
                {
                    SavePatientClinical(patientClinicalEyeModifierRow);

                    ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
                }
                else
                {
                    ClinicalDataConverterMetrics.Instance.TotalDuplicatePatientClinicalRows++;
                }
            }
            else
            {
                if (matchingPatientClinicalEyeModifierRows.Length == 0)
                {
                    SavePatientClinical(patientClinicalEyeModifierRow);

                    ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
                }
                else
                {
                    ClinicalDataConverterMetrics.Instance.TotalDuplicatePatientClinicalRows++;
                }
            }

        }



        private void PostClinicalProcedure(int patientId, int appointmentId, string procedureDescription, EyeControls procedureEyeControls, string procedureModifier1 = "", string procedureModifier2 = "")
        {


            var patientClinicalProcedureHeaderRow = new PatientClinical
                                                        {
                                                            PatientId = patientId,
                                                            AppointmentId = appointmentId,
                                                            ClinicalType = "F",
                                                            EyeContext = "",
                                                            Symptom = "/" + procedureDescription.ToUpper(),
                                                            FindingDetail = "TIME=12:00 AM (IOP)",
                                                            ImageDescriptor = "",
                                                            ImageInstructions = "",
                                                            DrawFileName = "",
                                                            Status = "A",
                                                            Highlights = "T",
                                                            FollowUp = "",
                                                            PermanentCondition = "",
                                                            PostOpPeriod = 0,
                                                            Surgery = "",
                                                            Activity = ""
                                                        };

            PatientClinical[] matchingPatientClinicalRecords =
                GetPatientClinicalsByKey(patientClinicalProcedureHeaderRow).Where(c => c.Symptom == patientClinicalProcedureHeaderRow.Symptom).ToArray();

            if (matchingPatientClinicalRecords.Length == 0)
            {
                SavePatientClinical(patientClinicalProcedureHeaderRow);

                ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
            }
            else
            {
                ClinicalDataConverterMetrics.Instance.TotalDuplicatePatientClinicalRows++;
            }

            var patientClinicalProcedureRightEyeRow = new PatientClinical
                                                          {
                                                              PatientId = patientId,
                                                              AppointmentId = appointmentId,
                                                              ClinicalType = "F",
                                                              EyeContext = "",
                                                              Symptom = "/" + procedureDescription.ToUpper(),
                                                              FindingDetail = "*" + procedureEyeControls.RightEyeName + "=T" + procedureEyeControls.RightEyeId.ToString(CultureInfo.InvariantCulture).Trim() + " <*" + procedureEyeControls.RightEyeName + ">",
                                                              ImageDescriptor = "",
                                                              ImageInstructions = "",
                                                              DrawFileName = "",
                                                              Status = "A",
                                                              Highlights = "T",
                                                              FollowUp = "",
                                                              PermanentCondition = "",
                                                              PostOpPeriod = 0,
                                                              Surgery = "",
                                                              Activity = ""
                                                          };



            PatientClinical[] matchingPatientClinicalRecords2 =
                GetPatientClinicalsByKey(patientClinicalProcedureRightEyeRow).ToArray();

            if (matchingPatientClinicalRecords2.Length == 0)
            {
                SavePatientClinical(patientClinicalProcedureRightEyeRow);

                ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
            }
            else
            {
                ClinicalDataConverterMetrics.Instance.TotalDuplicatePatientClinicalRows++;
            }

            // POST RIGHT EYE ROW IF RT OR 50 MODIFIER IS PRESENT!

            if ((new[] { "RT", "50" }.Contains(procedureModifier1) || new[] { "RT", "50" }.Contains(procedureModifier2)) && (procedureEyeControls.ModifierId != 0) && (procedureEyeControls.ModifierName != ""))
            {
                PostEyeModifierRow(patientId, appointmentId, procedureDescription, procedureEyeControls, procedureModifier1, procedureModifier2);
            }

            var patientClinicalProcedureLeftEyeRow = new PatientClinical
                                                         {
                                                             PatientId = patientId,
                                                             AppointmentId = appointmentId,
                                                             ClinicalType = "F",
                                                             EyeContext = "",
                                                             Symptom = "/" + procedureDescription.ToUpper(),
                                                             FindingDetail = "*" + procedureEyeControls.LeftEyeName + "=T" + procedureEyeControls.LeftEyeId.ToString(CultureInfo.InvariantCulture).Trim() + " <*" + procedureEyeControls.LeftEyeName + ">",
                                                             ImageDescriptor = "",
                                                             ImageInstructions = "",
                                                             DrawFileName = "",
                                                             Status = "A",
                                                             Highlights = "T",
                                                             FollowUp = "",
                                                             PermanentCondition = "",
                                                             PostOpPeriod = 0,
                                                             Surgery = "",
                                                             Activity = ""
                                                         };

            PatientClinical[] matchingPatientClinicalRecords3 =
                GetPatientClinicalsByKey(patientClinicalProcedureLeftEyeRow).ToArray();

            if (matchingPatientClinicalRecords3.Length == 0)
            {
                SavePatientClinical(patientClinicalProcedureLeftEyeRow);

                ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
            }
            else
            {
                ClinicalDataConverterMetrics.Instance.TotalDuplicatePatientClinicalRows++;
            }

            // POST LEFT EYE ROW IF LT OR 50 MODIFIER IS PRESENT!

            if ((new[] { "LT", "50" }.Contains(procedureModifier1) || new[] { "LT", "50" }.Contains(procedureModifier2)) && (procedureEyeControls.ModifierId != 0) && (procedureEyeControls.ModifierName != ""))
            {
                PostEyeModifierRow(patientId, appointmentId, procedureDescription, procedureEyeControls, procedureModifier1, procedureModifier2);

            }

        }

        public class EyeControls
        {

            public string Question { get; set; }

            public int RightEyeId { get; set; }

            public int LeftEyeId { get; set; }

            public int ModifierId { get; set; }

            public string RightEyeName { get; set; }

            public string LeftEyeName { get; set; }

            public string ModifierName { get; set; }

        }

        public string[] GetDrugCodesForProcedure(DataTable recordsTableForAppointment, string oldPatientId, string dateOfService, int ioLocationId, int ioDoctorId)
        {
            var jCodesFilter = recordsTableForAppointment.Select("OldPatientId = '" + oldPatientId + "' And DateOfService = '" + dateOfService + "' And IODoctorId = " + ioDoctorId + " And ProcedureCode like '%J%'");

            string[] jCodes = jCodesFilter.AsEnumerable().Select(r => r.Field<string>("ProcedureCode")).ToArray();

            return jCodes;

        }

        public string QueryDynamicFormsForProcedure(string procedureCode, Dictionary<string, string> procedures)
        {
            string procedureSpecification;
            string procedureDescription = "";

            if (!(procedures.TryGetValue(procedureCode, out procedureSpecification)))
            {
                return null;
            }

            int classIdInput = !(string.IsNullOrEmpty(procedureSpecification)) ? Convert.ToInt16(procedureSpecification) : 0;

            Question[] matchingQuestionsRows = classIdInput == 0 ? _questions.Where(r => r.QuestionParty == "T").OrderBy(r => r.QuestionOrder).Where(r => r.ControlName != null && r.ControlName.Contains(procedureCode)).ToArray() : _questions.Where(r => r.ClassId == classIdInput).ToArray();

            if (matchingQuestionsRows.Length >= 1)
            {
                procedureDescription = matchingQuestionsRows[0].Question1;
            }
            else
            {
                Trace.TraceInformation(string.Format("Procedure code {0} not found in the Questions table.{1}", procedureCode, Environment.NewLine));
            }

            return procedureDescription;

        }

        public EyeControls GetEyeControls(string question, string procedureCode, string procedureModifier1, string procedureModifier2)
        {
            var eyeControls = new EyeControls();

            var matchingFormsRows = _forms.Where(r => r.Question == question).ToArray();

            if (matchingFormsRows.Length >= 1)
            {
                int formId = matchingFormsRows[0].FormId;

                var matchingFormControlsRows = _formsControls.Where(r => r.FormId == formId).ToArray();

                if (matchingFormControlsRows.Length > 0)
                {

                    foreach (var row in matchingFormControlsRows)
                    {
                        if (eyeControls.RightEyeId != 0 && eyeControls.LeftEyeId != 0 && eyeControls.ModifierId != 0) break;

                        switch (row.TabIndex)
                        {
                            case 2:
                                eyeControls.RightEyeId = row.ControlId;
                                eyeControls.RightEyeName = row.ControlName.ToUpper();
                                break;

                            case 3:
                                eyeControls.LeftEyeId = row.ControlId;
                                eyeControls.LeftEyeName = row.ControlName.ToUpper();
                                break;

                            case 99:
                                eyeControls.ModifierId = row.ControlId;
                                eyeControls.ModifierName = row.ControlName.ToUpper();
                                break;

                        }
                    }
                }

            }

            if (((eyeControls.RightEyeId == 0) || (eyeControls.LeftEyeId == 0) || (eyeControls.ModifierId == 0)) && (new[] { "LT", "RT", "50" }.Contains(procedureModifier1) || new[] { "LT", "RT", "50" }.Contains(procedureModifier2)))
            {
                Trace.TraceInformation(string.Format("Uninitialized eye controls for procedure: {0}{1}", procedureCode, Environment.NewLine));
            }

            return eyeControls;


        }

        public bool PostDiagnosis(string currentDiagnosis, int patientId, int appointmentId)
        {

            var patientClinicalDiagnosisRow = new PatientClinical
                                                  {
                                                      PatientId = patientId,
                                                      AppointmentId = appointmentId,
                                                      ClinicalType = "U",
                                                      EyeContext = "",
                                                      ImageDescriptor = "",
                                                      ImageInstructions = "",
                                                      DrawFileName = "",
                                                      Status = "A",
                                                      Highlights = "",
                                                      FindingDetail = currentDiagnosis,
                                                      FollowUp = "",
                                                      PermanentCondition = "",
                                                      PostOpPeriod = 0,
                                                      Surgery = "",
                                                      Activity = ""
                                                  };

            PatientClinical matchingDiagnosisRecord = GetPatientClinicalsByKey(patientId, appointmentId, "U", currentDiagnosis).FirstOrDefault();

            if (matchingDiagnosisRecord == null)
            {
                PatientClinical[] diagnosisRowsForAppointment = _patientClinicalsByAppointmentId.GetValue(appointmentId, () => new HashSet<PatientClinical>()).Where(c => c.PatientId == patientId && c.ClinicalType == "U").ToArray();

                int diagnosisCounter = diagnosisRowsForAppointment.Length + 1;

                patientClinicalDiagnosisRow.Symptom = diagnosisCounter.ToString(CultureInfo.InvariantCulture);

                SavePatientClinical(patientClinicalDiagnosisRow);

                ClinicalDataConverterMetrics.Instance.TotalClinicalHistoryPatientClinicalRowsConverted++;
                return true;
            }

            return false;

        }
    }
}