﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class PatientDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {

        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId" };
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext).ToList();

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            var arguments = (DataConverterArguments)validationContext.Items[typeof(DataConverterArguments)];

            if (arguments != null && arguments.DataTable.Columns.Contains("IOPracticeDoctorId"))
            {

                var listOfPracticeDoctorIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOPracticeDoctorId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    var listOfPracticeDoctorIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT ResourceId
FROM Resources
WHERE ResourceType = 'D'
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();


                    bool allArePresent = listOfPracticeDoctorIdsInData.Intersect(listOfPracticeDoctorIdsInDatabase).Count() == listOfPracticeDoctorIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A practice doctor ID present in the data table does not match any doctor Ids present in the database."));
                    }

                }

            }

            return argumentValidationResults;

        }
    }


    public class PatientDataConverterResultsValidator : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var validationResults = new List<ValidationResult>();

            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidPatientRecordsQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM model.PatientCommunicationPreferences
WHERE PatientCommunicationTypeId = 3
	AND IsOptOut = 1;
");

                if (invalidPatientRecordsQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("SendStatements not set to 'Y'"));

                }
                invalidPatientRecordsQuery = validationConnection.Execute<DataTable>(
                  @"SELECT *
FROM model.PatientCommunicationPreferences
WHERE PatientCommunicationTypeId = 8
	AND IsOptOut = 1;
");


                if (invalidPatientRecordsQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("SendConsults not set to 'Y'"));

                }

                var invalidPolicyHolderRecordsQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
  FROM model.InsurancePolicies
  where PolicyHolderPatientId = 0
");

                if (invalidPolicyHolderRecordsQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("The PolicyPatientId field cannot be equal to 0."));
                }

            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;

        }


    }

    public class PatientDataConverterMetrics : IConverterMetrics
    {
        private PatientDataConverterMetrics()
        {
        }

        private static PatientDataConverterMetrics _instance;

        public static PatientDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new PatientDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }

        public int TotalDuplicates { get; set; }

        public int TotalPatientsWithoutZipCode { get; set; }
        public int TotalPatientsWithoutGender { get; set; }
        public int TotalPatientsWithoutHomePhone { get; set; }
        public int TotalPatientsWithoutBirthDate { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Patient Converter Metrics" + Environment.NewLine + Environment.NewLine);

            using (IDbConnection metricsConnection = DbConnectionFactory.PracticeRepository)
            {
                TotalRecordsConverted = metricsConnection.Execute<int>(@"SELECT COUNT(*) FROM model.Patients");

                report.AppendFormat("Total Records in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

                report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

                var patientsWithBlankZipQuery = metricsConnection.Execute<DataTable>(
                    @"SELECT *
FROM model.PatientAddresses
WHERE PostalCode is null or
PostalCode = ''
");

                TotalPatientsWithoutZipCode = patientsWithBlankZipQuery.Rows.Count;

                report.AppendFormat("Total Patients Without Zip Code: {0}" + Environment.NewLine + "Percentage:{1}" + Environment.NewLine + Environment.NewLine, TotalPatientsWithoutZipCode, (Convert.ToDouble(TotalPatientsWithoutZipCode) / TotalRecordsConverted).ToString("P"));

                var patientsWithBlankHomePhoneQuery = metricsConnection.Execute<DataTable>(
                @"SELECT *
FROM model.PatientPhoneNumbers
WHERE PatientPhoneNumberTypeId = 7 AND
(AreaCode is null or AreaCode = '')
");

                TotalPatientsWithoutHomePhone = patientsWithBlankHomePhoneQuery.Rows.Count;

                report.AppendFormat("Total Patients Without Home Phone: {0}{2}Percentage:{1}{2}{2}", TotalPatientsWithoutHomePhone, (Convert.ToDouble(TotalPatientsWithoutHomePhone) / TotalRecordsConverted).ToString("P"), Environment.NewLine);

                var patientsWithBlankGenderQuery = metricsConnection.Execute<DataTable>(
             @"SELECT *
FROM model.Patients
WHERE GenderId is null or GenderId = ''
");

                TotalPatientsWithoutGender = patientsWithBlankGenderQuery.Rows.Count;

                report.AppendFormat("Total Patients Without Gender: {0}" + Environment.NewLine + "Percentage:{1}" + Environment.NewLine + Environment.NewLine, TotalPatientsWithoutGender, (Convert.ToDouble(TotalPatientsWithoutGender) / TotalRecordsConverted).ToString("P"));

                var patientsWithBlankBirthDateQuery = metricsConnection.Execute<DataTable>(
               @"SELECT *
FROM model.Patients
WHERE DateOfBirth is null or DateOfBirth = ''
");

                TotalPatientsWithoutBirthDate = patientsWithBlankBirthDateQuery.Rows.Count;

                report.AppendFormat("Total Patients Without Birth Date: {0}" + Environment.NewLine + "Percentage:{1}" + Environment.NewLine + Environment.NewLine, TotalPatientsWithoutBirthDate, (Convert.ToDouble(TotalPatientsWithoutBirthDate) / TotalRecordsConverted).ToString("P"));

            }

            return report.ToString();
        }
    }

    /// <summary>
    ///   Data Converter for Patients.
    /// </summary>
    public class PatientDataConverter : DataConverter
    {
        private IDictionary<Tuple<int, string, string, string>, HashSet<Patient>> _patientByKey;
        private readonly IDictionary<object, ObjectStateEntry> _objectStateEntries = new Dictionary<object, ObjectStateEntry>();
        public override string[] AffectedTableNames
        {
            get { return new[] { "model.Patients", "model.PatientAddresses", "model.StateOrProvinces", "model.InsurancePolicies", "model.PatientPhoneNumbers", "model.PatientCommunicationPreferences", "model.PatientEmailAddresses", "model.PatientComments" }; }
        }

        

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new PatientDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
            get
            {
                return new PatientDataConverterResultsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {
            get
            {
                return PatientDataConverterMetrics.Instance;
            }
        }

        
        /// <summary>
        /// Runs the specified arguments.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        protected override void Run(DataConverterArguments arguments)
        {

            try
            {

                IList<Patient> cachedPatients = PracticeRepository.Patients.ToList();
                IList<PatientPhoneNumber> cachedPatientPhoneNumbers = PracticeRepository.PatientPhoneNumbers.ToList();
                IList<Race> cachedRaces = PracticeRepository.Races.ToList();
                IList<StateOrProvince> cachedStateOrProvinces = PracticeRepository.StateOrProvinces.ToList();
                IList<Ethnicity> cachedEthinicities = PracticeRepository.Ethnicities.ToList();
                IList<Language> cachedLanguages = PracticeRepository.Languages.ToList();
                IList<PatientAddress> cachedAddresses = PracticeRepository.PatientAddresses.ToList();
                //IList<PatientExternalProvider> cachedPatientExternalProviders = PracticeRepository.PatientExternalProviders.ToList();
                int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

                
                int counter = 0;
                _patientByKey = cachedPatients.GroupBy(i => Tuple.Create(i.Id,i.FirstName, i.LastName, i.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty)))
                .ToDictionary(i => i.Key, i => new HashSet<Patient>(i.ToArray()));
                foreach (DataRow row in arguments.DataTable.Rows)
                {
                    counter++;

                    // Every 100 rows, update the progress of the converter.
                    if (counter % 100 == 0)
                    {
                        Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                    }

                    var patientAddress = new Utilities.Address(row.GetValueOrDefault<string>("PatientStreetAddress"), row.GetValueOrDefault<string>("PatientSuite"));

                    if (!string.IsNullOrEmpty(patientAddress.StreetAddress) || !string.IsNullOrEmpty(patientAddress.Suite))
                    {
                        patientAddress.ParseAddress(true);
                    }

                    //string oldPrimaryDoctorId = row.GetValueOrDefault<string>("PatientPrimaryCarePhysician").ToUpper();
                    //string oldReferringDoctorId = row.GetValueOrDefault<string>("PatientReferringPhysician").ToUpper();

                    /*Patient*/
                    var gender = row.GetValueOrDefault<string>("IOPatientGender");
                    var maritalStatus = row.GetValueOrDefault<string>("IOPatientMarital");
                    var language = row.GetValueOrDefault<string>("Language");
                    var patient = new Patient
                    {
                        PriorPatientCode = row.GetValueOrDefault<string>("OldPatientId"),
                        LastName = row.GetValueOrDefault<string>("PatientLastName"),
                        FirstName = row.GetValueOrDefault<string>("PatientFirstName"),
                        MiddleName = row.GetValueOrDefault<string>("PatientMiddleInitial"),
                        Salutation = row.GetValueOrDefault<string>("PatientSalutation"),
                        Suffix = row.GetValueOrDefault<string>("PatientNameReference"),
                        SocialSecurityNumber = row.GetValueOrDefault<string>("PatientSSN").ToAlphanumeric(),
                        Gender = gender.IsNullOrEmpty() ? Gender.Male : Enums.ToEnumFromDisplayNameOrDefault<Gender>(gender),
                        DateOfBirth = row.GetValueOrDefault<string>("PatientBirthDate").IsNullOrEmpty() ? null : Utilities.ValidateDate(row.GetValueOrDefault<string>("PatientBirthDate")).ToDateTime("yyyyMMdd"),
                        MaritalStatus = maritalStatus.IsNullOrEmpty() ? MaritalStatus.Single : Enums.ToEnumFromDisplayNameOrDefault<MaritalStatus>(maritalStatus),
                        DefaultUserId = row.GetValueOrDefault<string>("IOPracticeDoctorId").ToInt() ?? 0
                    };
                    if (!language.IsNullOrEmpty())
                    {
                        patient.LanguageId = cachedLanguages.Where(l => l.Name == language).Select(l => l.Id).FirstOrDefault();
                    }
                    patient.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                    var matchingPatients = _patientByKey.GetValue(Tuple.Create(patient.Id, patient.FirstName, patient.LastName, patient.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty)), () => new HashSet<Patient>()).ToArray();

                    bool isDuplicatePatient = matchingPatients.Any();

                    //Moved the relative table inside the duplicate check as all the new entities are going as new table and having foreing key issue when duplicate has found.
                    if (!isDuplicatePatient)
                    {
                        /* Ethnicity */
                        string drEnthinicity = row.GetValueOrDefault<string>("Ethnicity");
                        if (!drEnthinicity.IsNullOrEmpty())
                        {
                            var enthnicity = cachedEthinicities.Where(e => e.Name == drEnthinicity).Select(e => e).FirstOrDefault();
                            if (enthnicity != null)
                            {
                                patient.EthnicityId = enthnicity.Id;
                            }
                        }

                        /* PatientPhoneNumber */
                        var homeNumber = row.GetValueOrDefault<string>("PatientHomePhone").ToAlphanumeric();
                        if (!homeNumber.IsNullOrEmpty() && homeNumber.Length == 10)
                        {
                            var homePhoneNumber = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Home,
                                AreaCode = homeNumber.ToAlphanumeric().Substring(0, 3),
                                ExchangeAndSuffix = homeNumber.ToAlphanumeric().Substring(3, homeNumber.Length - 3)
                            };
                            _objectStateEntries[homePhoneNumber] = new ObjectStateEntry(homePhoneNumber) { State = ObjectState.Added };
                            patient.PatientPhoneNumbers.Add(homePhoneNumber);
                        }
                        var workNumber = row.GetValueOrDefault<string>("PatientWorkPhone").ToAlphanumeric();
                        if (!workNumber.IsNullOrEmpty() && workNumber.Length == 10)
                        {
                            var businessPhoneNumber = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Business,
                                AreaCode = workNumber.Substring(0, 3),
                                ExchangeAndSuffix = workNumber.Substring(3, workNumber.Length - 3)
                            };
                            _objectStateEntries[businessPhoneNumber] = new ObjectStateEntry(businessPhoneNumber) { State = ObjectState.Added };
                            patient.PatientPhoneNumbers.Add(businessPhoneNumber);
                        }
                        var cellNumber = row.GetValueOrDefault<string>("PatientCellPhone").ToAlphanumeric();
                        if (!cellNumber.IsNullOrEmpty() && cellNumber.Length == 10)
                        {
                            var cellPhoneNumber = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Cell,
                                AreaCode = cellNumber.Substring(0, 3),
                                ExchangeAndSuffix = cellNumber.Substring(3, cellNumber.Length - 3)
                            };
                            _objectStateEntries[cellPhoneNumber] = new ObjectStateEntry(cellPhoneNumber) { State = ObjectState.Added };
                            patient.PatientPhoneNumbers.Add(cellPhoneNumber);
                        }

                        /* PatientAddress */
                        string abbreviation = row.GetValueOrDefault<string>("PatientState");
                        var stateOrProvince = cachedStateOrProvinces.Where(sop => sop.Abbreviation == abbreviation);
                        if (stateOrProvince.Any())
                        {
                            var homeAddress = new PatientAddress
                            {
                                Line1 = patientAddress.StreetAddress,
                                Line2 = patientAddress.Suite,
                                PatientAddressTypeId = (int)PatientAddressTypeId.Home,
                                City = row.GetValueOrDefault<string>("PatientCity"),
                                PostalCode = row.GetValueOrDefault<string>("PatientZip"),
                                StateOrProvinceId = stateOrProvince.First().Id
                            };
                            _objectStateEntries[homeAddress] = new ObjectStateEntry(homeAddress) { State = ObjectState.Added };
                            patient.PatientAddresses.Add(homeAddress);
                        }
                        /*PatientPhoneNumberCommunicationPreference*/
                        string emergencyPhone = row.GetValueOrDefault<string>("PatientEmergencyContactPhone").ToAlphanumeric();
                        if (!emergencyPhone.IsNullOrEmpty() && emergencyPhone.Length >= 10)
                        {
                            var areaCode = emergencyPhone.Substring(0, 3);
                            var exchangeAndSuffix = emergencyPhone.Substring(3, emergencyPhone.Length - 3);

                            var patientPreferredPhone = new PatientPhoneNumber
                            {
                                PhoneNumberType = PatientPhoneNumberType.Home,
                                AreaCode = areaCode,
                                ExchangeAndSuffix = exchangeAndSuffix
                            };

                            var matchingPreferredPhone = cachedPatientPhoneNumbers.Where(ppn => ppn.AreaCode == areaCode && ppn.ExchangeAndSuffix == exchangeAndSuffix).Select(ppn => ppn).FirstOrDefault();
                            if (matchingPreferredPhone == null)
                            {
                                _objectStateEntries[patientPreferredPhone] = new ObjectStateEntry(patientPreferredPhone) { State = ObjectState.Added };
                                patient.PatientPhoneNumbers.Add(patientPreferredPhone);
                                cachedPatientPhoneNumbers.Add(patientPreferredPhone);
                            }
                            var patientPhoneNumberCommunicationPreference = new PatientPhoneNumberCommunicationPreference
                            {
                                CommunicationMethodType = CommunicationMethodType.PhoneCall,
                                PatientCommunicationType = PatientCommunicationType.Emergency,
                                DestinationId = cachedAddresses.Select(pa => pa.Id).FirstOrDefault(),
                                PatientPhoneNumber = patientPreferredPhone,
                                IsOptOut = true //not sure if it is true always
                            };
                            _objectStateEntries[patientPhoneNumberCommunicationPreference] = new ObjectStateEntry(patientPhoneNumberCommunicationPreference) { State = ObjectState.Added };
                            patient.PatientCommunicationPreferences.Add(patientPhoneNumberCommunicationPreference);

                        }
                        /* PatientEmail */
                        var emailAddress = row.GetValueOrDefault<string>("PatientEmail");
                        if (!emailAddress.IsNullOrEmpty())
                        {
                            var personalEmailAddress = new PatientEmailAddress
                            {
                                EmailAddressType = EmailAddressType.Personal, //Defaulted to personal as no value are coming in DR.
                                Value = emailAddress,
                            };
                            _objectStateEntries[personalEmailAddress] = new ObjectStateEntry(personalEmailAddress) { State = ObjectState.Added };
                            patient.PatientEmailAddresses.Add(personalEmailAddress);
                        }

                        /* PatientComment */
                        var patientComment1 = row.GetValueOrDefault<string>("PatientComments");
                        if (!patientComment1.IsNullOrEmpty())
                        {
                            var comment = new PatientComment
                            {
                                PatientCommentType = PatientCommentType.PatientCommunicationPreferenceNotes, //Defaulted to PatientCommunicationPreferenceNotes as no value are coming in DR.
                                Value = patientComment1,
                            };
                            patient.PatientComments.Add(comment);
                            _objectStateEntries[comment] = new ObjectStateEntry(comment) { State = ObjectState.Added };
                        }
                        var patientComment2 = row.GetValueOrDefault<string>("OldPatientId2");
                        if (!patientComment2.IsNullOrEmpty())
                        {

                            var comment = new PatientComment
                            {
                                PatientCommentType = PatientCommentType.PatientCommunicationPreferenceNotes, //Defaulted to PatientCommunicationPreferenceNotes as no value are coming in DR.
                                Value = row.GetValueOrDefault<string>("OldPatientId2"),
                            };
                            patient.PatientComments.Add(comment);
                            _objectStateEntries[comment] = new ObjectStateEntry(comment) { State = ObjectState.Added };
                        }

                        var raceValue = row.GetValueOrDefault<string>("Race");
                        if (!raceValue.IsNullOrEmpty())
                        {
                            var race = cachedRaces.Where(r => r.Name == raceValue).Select(r => r).FirstOrDefault();
                            if (race != null)
                            {
                                if (patient.Races.All(r => r.Name != race.Name))
                                {
                                    _objectStateEntries[race] = new ObjectStateEntry(race) { State = ObjectState.Added };
                                    patient.Races.Add(race);
                                }
                            }
                        }

                        // Lookup referring doctor and primary care physician.
                        /*if (!String.IsNullOrEmpty(oldReferringDoctorId))
                        {
                            var referringPhysician = cachedPatientExternalProviders.Where(pep => pep.ExternalProviderId.ToString() == oldReferringDoctorId).Select(pep => pep).FirstOrDefault();
                            if (referringPhysician != null &&
                                !patient.ExternalProviders.Any(ep => ep.Id == referringPhysician.Id))
                            {
                                patient.ExternalProviders.Add(referringPhysician);
                            }
                        }

                        if (!String.IsNullOrEmpty(oldPrimaryDoctorId))
                        {
                            var primaryCarePhysician = cachedPatientExternalProviders.Where(pep => pep.ExternalProviderId.ToString() == oldPrimaryDoctorId).Select(pep => pep).FirstOrDefault();
                            if (primaryCarePhysician != null &&
                                !patient.ExternalProviders.Any(ep => ep.Id == primaryCarePhysician.Id))
                            {
                                patient.ExternalProviders.Add(primaryCarePhysician);
                            }

                        }*/
                        _patientByKey.GetValue(Tuple.Create(patient.Id, patient.FirstName, patient.LastName, patient.DateOfBirth.ToMMDDYYYYString().Replace("/", string.Empty)), () => new HashSet<Patient>()).Add(patient);
                        _objectStateEntries[patient] = new ObjectStateEntry(patient) { State = ObjectState.Added};
                    }
                    else
                    {
                        PatientDataConverterMetrics.Instance.TotalDuplicates++;
                    }

                }
                RepositoryService.Persist(_objectStateEntries.Values, typeof(IPracticeRepository).FullName);
            }
            finally
            {
               CleanUpPatients();
            }


        }

        private void CleanUpPatients()
        {
            using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
            {
                cleanupConnection.Execute(
                    @"
UPDATE model.Patients
SET FirstName = UPPER( FirstName ),
MiddleName = UPPER( MiddleName ),
LastName = UPPER( LastName )

Update model.PatientAddresses
SET
Line1 = UPPER( Line1 ),
Line2 = UPPER( Line2 ),
City = UPPER( City )

Update model.StateOrProvinces
SET
Name = UPPER( Name )

UPDATE model.InsurancePolicies
SET InsurancePolicies.PolicyHolderPatientId = pi.InsuredPatientId
FROM model.InsurancePolicies ip
INNER JOIN model.PatientInsurances pi ON ip.id = pi.InsurancePolicyId
WHERE PolicyHolderPatientId = 0
");
            }
        }

    }

}