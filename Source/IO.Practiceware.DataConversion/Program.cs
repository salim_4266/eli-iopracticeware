﻿using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace IO.Practiceware.DataConversion
{
    internal class Program
    {
        private static void Main()
        {
            var serviceProvider = Bootstrapper.Run<IServiceProvider>();

            if (!(Directory.Exists("logs")))
                Directory.CreateDirectory("logs");

            foreach (DataConverterArguments argument in DataConversionConfiguration.Current.Converters.Where(a => a.IsEnabled))
            {
                Trace.Listeners.Clear();

                Trace.Listeners.Add(new DefaultTraceListener());

#if DEBUG
                Trace.Listeners.Add(new ConsoleTraceListener());

#endif

                Trace.Listeners.Add(new TextWriterTraceListener("logs\\" + argument.ConverterName + ".log"));

                try
                {
                    var instance = (IDataConverter) serviceProvider.GetService(argument.Converter);

                    var argumentsValidator = instance.ArgumentsValidator;

                    if (argumentsValidator != null)
                    {
                        var argumentsValidationResults = new List<ValidationResult>();
                        if (!(Validator.TryValidateObject(argumentsValidator, new ValidationContext(argumentsValidator, null, new Dictionary<object, object> {{typeof (DataConverterArguments), argument}}), argumentsValidationResults)))
                        {
                            File.WriteAllText("logs\\" + argument.ConverterName + ".ArgumentsValidation.log", argumentsValidationResults.Select(r => r.ErrorMessage).Join(Environment.NewLine));
                            Console.WriteLine("Could not validate data specified in arguments!");
                            Console.ReadLine();
                            throw new ArgumentException("Could not validate data specified in arguments!");
                        }
                    }

                    instance.Run(argument);

                    var resultsValidator = instance.ResultsValidator;

                    if (resultsValidator != null)
                    {
                        var resultsValidationResults = new List<ValidationResult>();
                        if (!(Validator.TryValidateObject(resultsValidator, new ValidationContext(resultsValidator, null, null), resultsValidationResults)))
                        {
                            File.WriteAllText("logs\\" + argument.ConverterName + ".ResultsValidation.log", resultsValidationResults.Select(r => r.ErrorMessage).Join(Environment.NewLine));
                            Console.WriteLine("Could not validate converter!");
                            Console.ReadLine();
                            return;
                        }
                    }

                    if (instance.Finalizer != null)
                    {
                        var finalizer = instance.Finalizer;

                        finalizer.FinalizeData();
                    }

                    // The metrics classes create a report containing certain measurements of the conversion (e.g. how many records were converted, how many duplicates were encountered in the data).
                    if (instance.Metrics != null)
                    {
                        File.WriteAllText("logs\\" + argument.ConverterName + ".Metrics.log", instance.Metrics.OutputMetrics());
                    }

                    Trace.Flush();
                }
                catch (Exception ex)
                {
                    if (!(Directory.Exists("errors")))
                        Directory.CreateDirectory("errors");
                    File.WriteAllText("errors\\" + DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss") + ".txt", ex.ToString());
                }
                finally
                {
                    Trace.Flush();
                }
            }
        }
    }
}