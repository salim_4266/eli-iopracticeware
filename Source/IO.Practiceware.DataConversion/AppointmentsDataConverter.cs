﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using Appointment = IO.Practiceware.Model.Legacy.Appointment;

namespace IO.Practiceware.DataConversion
{
    public class AppointmentsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get { return new[] {"OldPatientId", "IODoctorId", "IOLocationId", "IOAppointmentTypeId"}; }
        }

        #region IValidatableObject Members

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext).ToList();

            List<ValidationResult> argumentValidationResults = argumentDataTableFieldValidationResults;

            var arguments = (DataConverterArguments) validationContext.Items[typeof (DataConverterArguments)];

            if (arguments != null && arguments.DataTable.Columns.Contains("IODoctorId"))
            {
                int[] listOfPracticeDoctorIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IODoctorId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    int[] listOfPracticeDoctorIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT ResourceId
FROM Resources
WHERE ResourceType = 'D'
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();


                    bool allArePresent = listOfPracticeDoctorIdsInData.Intersect(listOfPracticeDoctorIdsInDatabase).Count() == listOfPracticeDoctorIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A practice doctor Id present in the data table does not match any doctor Ids present in the database."));
                    }
                }
            }

            if (arguments != null && arguments.DataTable.Columns.Contains("IOLocationId"))
            {
                int[] listOfLocationIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOLocationId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    int[] listOfLocationIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT PracticeId + 1000
FROM PracticeName
WHERE LocationReference <> ''

UNION

SELECT ResourceId
FROM Resources
WHERE ResourceType = 'R'

UNION

SELECT 0
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();

                    bool allArePresent = listOfLocationIdsInData.Intersect(listOfLocationIdsInDatabase).Count() == listOfLocationIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("A location Id present in the data table does not match any location Ids present in the database."));
                    }
                }
            }


            if (arguments != null && arguments.DataTable.Columns.Contains("IOAppointmentTypeId"))
            {
                int[] listOfAppointmentTypeIdsInData = arguments.DataTable.AsEnumerable().Select(r => r["IOAppointmentTypeId"]).OfType<string>().Select(s => s.ToInt() ?? 0).Where(i => i != 0).Distinct().ToArray();

                using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
                {
                    int[] listOfAppointmentTypeIdsInDatabase = validationConnection.Execute<DataTable>(
                        @"SELECT DISTINCT AppTypeId
FROM AppointmentType
").Rows.OfType<DataRow>().Select(r => r[0]).OfType<int>().ToArray();

                    bool allArePresent = listOfAppointmentTypeIdsInData.Intersect(listOfAppointmentTypeIdsInDatabase).Count() == listOfAppointmentTypeIdsInData.Count();

                    if (!allArePresent)
                    {
                        argumentValidationResults.Add(new ValidationResult("An appointment type Id present in the data table does not match any appointment type Ids present in the database."));
                    }
                }
            }

            return argumentValidationResults;
        }

        #endregion
    }


    public class AppointmentsDataConverterResultsValidator : IValidatableObject
    {
        #region IValidatableObject Members

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidResourceId1Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId1 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'D'
		)
");

                if (invalidResourceId1Query.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Resource ID not present in Resources table."));
                }

                var invalidResourceId2Query = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE ResourceId2 <> 0
	AND ResourceId2 NOT IN (
		SELECT PracticeId + 1000
		FROM PracticeName
		WHERE PracticeAddress <> ''
		)
	AND ResourceId2 NOT IN (
		SELECT ResourceId
		FROM Resources
		WHERE ResourceType = 'R'
		)
");

                if (invalidResourceId2Query.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Location ID not present in PracticeName or Resources tables."));
                }
               
                var invalidAppointmentTypeQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM Appointments
WHERE AppTypeId NOT IN (
		SELECT AppTypeId
		FROM AppointmentType
		)
");

                if (invalidAppointmentTypeQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Appointment type ID not present in AppointmentType table."));
                }
            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;
        }

        #endregion
    }

    public class AppointmentsDataConverterMetrics : IConverterMetrics
    {
        private static AppointmentsDataConverterMetrics _instance;

        private AppointmentsDataConverterMetrics()
        {
        }

        public static AppointmentsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new AppointmentsDataConverterMetrics()); }
        }

        public int TotalPatientsAdded { get; set; }
        public int TotalPatientsNotLinked { get; set; }

        #region IConverterMetrics Members

        public int TotalRecordsConverted { get; set; }
        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Appointments Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            report.AppendFormat("Total Patients Added: {0}" + Environment.NewLine + Environment.NewLine, TotalPatientsAdded);

            report.AppendFormat("Total Patients Not Linked: {0}" + Environment.NewLine + Environment.NewLine, TotalPatientsNotLinked);

            return report.ToString();
        }

        #endregion
    }

    /// <summary>
    /// Data Converter for Appointments.
    /// </summary>
    public class AppointmentsDataConverter : DataConverter
    {
        public override string[] AffectedTableNames
        {
            get { return new[] {"dbo.Appointments", "dbo.PatientDemographics"}; }
        }


        public override IValidatableObject ArgumentsValidator
        {
            get { return new AppointmentsDataConverterArgumentsValidator(); }
        }

        public override IValidatableObject ResultsValidator
        {
            get { return new AppointmentsDataConverterResultsValidator(); }
        }

        public override IConverterMetrics Metrics
        {
            get { return AppointmentsDataConverterMetrics.Instance; }
        }

        protected override void Run(DataConverterArguments arguments)
        {

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            foreach (DataRow row in arguments.DataTable.Rows)
            {

                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var oldPatientId = row.Get<string>("OldPatientId");

                int patientId = !string.IsNullOrEmpty(oldPatientId) ? PracticeRepository.FindPatientIdByOldPatientId(oldPatientId) : 0;

                if (patientId == 0)
                {
                    if (!string.IsNullOrEmpty(row.GetValueOrDefault<string>("PatientLastName")))
                    {
                        Patient patientToPost = row.ToPatient();

                        patientId = PracticeRepository.FindPatientIdByNameAndBirthDate(row);
                        var homePhone = row.GetValueOrDefault<string>("PatientHomePhone").ToAlphanumeric();
                        if (patientId == 0 )
                        {
                            if (homePhone.IsNullOrEmpty() && homePhone.Length == 10)
                            {
                                patientToPost.PatientPhoneNumbers.Add(new PatientPhoneNumber
                                {
                                    PhoneNumberType = PatientPhoneNumberType.Home,
                                    AreaCode = homePhone.Substring(0, 3),
                                    ExchangeAndSuffix = homePhone.Substring(3, row.Get<string>("PatientHomePhone").Length - 3)
                                });
                            }
                            PracticeRepository.Save(patientToPost);
                            patientId = patientToPost.Id;

                            var patientInsurances = patientToPost.PatientInsurances.Select(ip => ip).FirstOrDefault();
                            if (patientInsurances != null)
                            {
                                patientInsurances.InsurancePolicy.PolicyholderPatientId = patientId;
                                patientInsurances.PolicyholderRelationshipType = PolicyHolderRelationshipType.Self;
                                PracticeRepository.Save(patientToPost);
                            }
                            
                            AppointmentsDataConverterMetrics.Instance.TotalPatientsAdded++;
                        }
                    }
                    else
                    {
                        AppointmentsDataConverterMetrics.Instance.TotalPatientsNotLinked++;
                        continue;
                    }
                }


                var appointment = new Appointment
                    {
                        PatientId = patientId,
                        AppTime = row.GetValueOrDefault<int>("IOAppointmentTime"),
                        Duration = 0,
                        ReferralId = 0,
                        PreCertId = 0,
                        TechApptTypeId = 2,
                        ScheduleStatus = "P",
                        ActivityStatus = "P",
                        Comments = row.GetValueOrDefault<string>("AppointmentComment"),
                        ResourceId1 = row.GetValueOrDefault<int>("IODoctorId"),
                        ResourceId2 = row.GetValueOrDefault<int>("IOLocationId"),
                        ResourceId3 = 0,
                        ResourceId4 = 0,
                        ResourceId5 = 0,
                        ResourceId6 = 0,
                        ResourceId7 = 0,
                        ResourceId8 = 0,
                        ApptInsType = "M",
                        ConfirmStatus = "",
                        ApptTypeCat = "",
                        SetDate = "",
                        VisitReason = "",
                        AppTypeId = row.GetValueOrDefault<int>("IOAppointmentTypeId"),
                        AppDate = Utilities.ValidateDate(row.GetValueOrDefault<string>("DateOfService")),
                    };

                appointment.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                bool isDuplicateAppointment = LegacyPracticeRepository.Appointments.WhereMatches(appointment, new[] {"PatientId", "AppDate", "AppTime", "AppTypeId", "ResourceId1", "ResourceId2"}).ToArray().Length > 0;

                if (!isDuplicateAppointment)
                {
                    LegacyPracticeRepository.Save(appointment);
                    AppointmentsDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                    AppointmentsDataConverterMetrics.Instance.TotalDuplicates++;
            }

            CleanupAppointments();
        }

        private void CleanupAppointments()
        {
            using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
            {
                cleanupConnection.Execute(
                    @"UPDATE Appointments
SET EncounterId = AppointmentId
WHERE EncounterId IS NULL"
                    );
            }
        }
    }
}