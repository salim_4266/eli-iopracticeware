﻿using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Xml;
using System.Xml.Serialization;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace IO.Practiceware.DataConversion
{
    /// <summary>
    /// An interface for tracking converter statistics and outcomes.
    /// </summary>
    public interface IArgumentsValidator
    {
        string[] FieldsRequiredInDataTable { get; }

        IEnumerable<ValidationResult> ValidateColumnsInDataTable(ValidationContext validationContext);
    }


    /// <summary>
    /// The interface implemented by the data converters' metrics classes.
    /// </summary>
    public interface IConverterMetrics
    {
        int TotalRecordsConverted { get; }
        int TotalDuplicates { get; set; }

        string OutputMetrics();
    }

    public interface IFinalizer
    {
        void FinalizeData();
    }


    /// <summary>
    ///   Definition for a type that runs data conversions.
    /// </summary>
    [SupportsUnitOfWork]
    public interface IDataConverter
    {
        IConverterMetrics Metrics { get; }
        IValidatableObject ArgumentsValidator { get; }
        IValidatableObject ResultsValidator { get; }
        IFinalizer Finalizer { get; }

        /// <summary>
        ///   Runs the converter using specified arguments.
        /// </summary>
        /// <param name="arguments"> The arguments. </param>
        void Run(DataConverterArguments arguments);
    }

    /// <summary>
    /// The arguments validator ensures that the data to be used to write rows to the database meets certain criteria
    /// (e.g. that all reference IDs such as doctor IDs present in the data are also present in the database).
    /// </summary>
    public class ArgumentsValidator : IArgumentsValidator
    {
        #region IArgumentsValidator Members

        public virtual string[] FieldsRequiredInDataTable
        {
            get { return new[] { "OldPatientId" }; }
        }

        public IEnumerable<ValidationResult> ValidateColumnsInDataTable(ValidationContext validationContext)
        {
            var arguments = (DataConverterArguments)validationContext.Items[typeof(DataConverterArguments)];

            var argumentValidationResults = new List<ValidationResult>();

            if (arguments != null)
            {
                foreach (string tableName in FieldsRequiredInDataTable)
                {
                    if (!arguments.DataTable.Columns.Contains(tableName))
                    {
                        argumentValidationResults.Add(new ValidationResult(string.Format("Required field {0} is missing from DataTable.", tableName)));
                    }
                }
            }

            return argumentValidationResults;
        }

        #endregion
    }

    /// <summary>
    ///   Base type for performing data conversions.
    /// </summary>
    public abstract class DataConverter : IDataConverter
    {
        /// <summary>
        ///   Gets or sets the practice repository. Used to access data from the new model.
        /// </summary>
        /// <value> The practice repository. </value>
        [Dependency]
        public virtual IPracticeRepository PracticeRepository { get; set; }

        /// <summary>
        /// An array containing the names of tables that will be converted to or modified by a conversion. This is to ensure that triggers and constraints are disabled while the converter is running, and re-enabled when it is finished.
        /// </summary>
        /// <value>
        /// The names of the tables affected by a converter.
        /// </value>
        public virtual string[] AffectedTableNames { get; set; }

        /// <summary>
        /// Gets or sets the unit of work provider.
        /// </summary>
        /// <value>
        /// The unit of work provider.
        /// </value>
        [Dependency]
        public virtual IUnitOfWorkProvider UnitOfWorkProvider { get; set; }

        /// <summary>
        ///   Gets or sets the legacy practice repository. Used to access data directly from tables.
        /// </summary>
        /// <value> The legacy practice repository. </value>
        [Dependency]
        public virtual ILegacyPracticeRepository LegacyPracticeRepository { get; set; }

        [Dependency]
        public virtual IRepositoryService RepositoryService { get; set; }

        #region IDataConverter Members

        /// <summary>
        /// Gets the arguments validator. A data converter's arguments validator contains checks to ensure that the database has corresponding elements for those present in the data being converted.
        /// </summary>
        /// <value>
        /// The arguments validator
        /// </value>
        public virtual IValidatableObject ArgumentsValidator
        {
            get { return null; }
        }

        public virtual IValidatableObject ResultsValidator
        {
            get { return null; }
        }

        public virtual IConverterMetrics Metrics
        {
            get { return null; }
        }

        public virtual IFinalizer Finalizer
        {
            get { return null; }
        }


        void IDataConverter.Run(DataConverterArguments arguments)
        {
            var tableConstraintsDictionary = new Dictionary<string, string>();
            var tableTriggersDictionary = new Dictionary<string, string>();

            bool is72Database;

            using (IDbConnection checkDatabaseVersionConnection = DbConnectionFactory.PracticeRepository)
            {
                is72Database = checkDatabaseVersionConnection.ExecuteAndDispose<int>(@"SELECT CASE WHEN OBJECT_ID('dbo.DisableEnabledCheckConstraints') IS NULL THEN 0 ELSE 1 END") != 0;
            }

            if (is72Database)
            {
                try
                {
                    using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { Timeout = TimeSpan.FromHours(20), IsolationLevel = IsolationLevel.ReadUncommitted }))
                    using (new SuppressAuditingScope())
                    {
                        if (AffectedTableNames != null)
                        {
                            foreach (string tableName in AffectedTableNames)
                            {
                                using (IDbConnection toggleConstraintsAndTriggersConnection = DbConnectionFactory.PracticeRepository)
                                {
                                    string queryToListConstraints = @"
DECLARE @constraintNames xml
EXEC dbo.DisableEnabledCheckConstraints '" + tableName + @"', @names = @constraintNames OUTPUT

SELECT @constraintNames
";

                                    var constraintNames = toggleConstraintsAndTriggersConnection.Execute<string>(queryToListConstraints);

                                    tableConstraintsDictionary.Add(tableName, constraintNames);

                                    string queryToListTriggers = @"
DECLARE @triggerNames xml
EXEC  dbo.DisableEnabledTriggers '" + tableName + @"', @names = @triggerNames OUTPUT

SELECT @triggerNames";

                                    var triggerNames = toggleConstraintsAndTriggersConnection.Execute<string>(queryToListTriggers);

                                    tableTriggersDictionary.Add(tableName, triggerNames);
                                }
                            }

                            Run(arguments);

                            if (AffectedTableNames != null)
                            {
                                foreach (var item in tableTriggersDictionary)
                                {
                                    string tableTriggers = item.Value;

                                    using (IDbConnection toggleConstraintsAndTriggersConnection = DbConnectionFactory.PracticeRepository)
                                    {
                                        if (!string.IsNullOrEmpty(tableTriggers))
                                        {
                                            toggleConstraintsAndTriggersConnection.Execute(@"
EXEC dbo.EnableTriggers @triggerNames
", new Dictionary<string, object> { { "triggerNames", tableTriggers } });
                                        }
                                    }
                                }


                                foreach (var item in tableConstraintsDictionary)
                                {
                                    string tableConstraints = item.Value;

                                    using (IDbConnection toggleConstraintsAndTriggersConnection = DbConnectionFactory.PracticeRepository)
                                    {
                                        if (!string.IsNullOrEmpty(tableConstraints))
                                        {
                                            toggleConstraintsAndTriggersConnection.Execute(@"
EXEC dbo.EnableCheckConstraints @constraintNames
", new Dictionary<string, object> { { "constraintNames", tableConstraints } });
                                        }
                                    }
                                }
                            }
                        }

                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(string.Format("Conversion failed with exception{0}", ex));
                }
            }
            else
            {
                Run(arguments);
            }
        }

        #endregion

        protected abstract void Run(DataConverterArguments arguments);
    }

    public class DataConverterArguments
    {
        private string _converterName;
        private string _inputFilePath;

        [XmlAttribute("converterName")]
        public string ConverterName
        {
            get { return _converterName; }
            set
            {
                _converterName = value;
                if (value.IsNotNullOrEmpty()) Converter = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(t => t.Name == value);
                else Converter = null;
            }
        }

        public Type Converter { get; set; }

        [XmlAttribute("isEnabled")]
        public bool IsEnabled { get; set; }

        [XmlAttribute("inputFilePath")]
        public string InputFilePath
        {
            get { return _inputFilePath; }
            set
            {
                _inputFilePath = value;
                if (value.IsNotNullOrEmpty()) DataTable = File.ReadAllText(value).ToDataTable();
                else DataTable = null;
            }
        }

        public DataTable DataTable { get; set; }

        [XmlArray("settings")]
        [XmlArrayItem("add")]
        public List<DataConverterArgumentSetting> Settings { get; set; }
    }

    public class DataConverterArgumentSetting
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }
    }

    [XmlRoot("dataConversionConfiguration")]
    public class DataConversionConfiguration : IConfigurationSectionHandler
    {
        [XmlArray("converters")]
        [XmlArrayItem("converter")]
        public List<DataConverterArguments> Converters { get; set; }

        private static readonly Lazy<System.Configuration.Configuration> Configuration = new Lazy<System.Configuration.Configuration>(() => System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap { ExeConfigFilename = "IO.Practiceware.Custom.config" }, ConfigurationUserLevel.None));

        public static DataConversionConfiguration Current
        {
            get
            {
                const string sectionName = "dataConversionConfiguration";
                ConfigurationSection section = Configuration.Value.GetSection(sectionName);
                if (section == null) throw new NullReferenceException("Could not find section {0}.".FormatWith(sectionName));
                return Create(section.SectionInformation.GetRawXml());
            }
        }

        #region IConfigurationSectionHandler Members

        public object Create(object parent, object configContext, XmlNode section)
        {
            return Create(section.OuterXml);
        }

        #endregion

        private static DataConversionConfiguration Create(string xml)
        {
            var serializer = Serialization.CreateCachedXmlSerializer(typeof(DataConversionConfiguration));

            using (var sr = new StringReader(xml))
            {
                var result = (DataConversionConfiguration)serializer.Deserialize(sr);
                return result;
            }
        }
    }
}