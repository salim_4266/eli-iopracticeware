﻿using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class ServicesDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            { 
                return new[] { "CPT", "Description", "Fee" };
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentValidationResults =  ValidateColumnsInDataTable(validationContext);

            return argumentValidationResults;

        }

    }

    public class ServicesDataConverterResultsValidator:IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            using (var validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var servicesValidationQuery = validationConnection.Execute<DataTable>(@"
SELECT *
FROM model.EncounterServices 
WHERE LEN(Code) > 7
    OR Code LIKE '% %'
");

                if (servicesValidationQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Service CPT code is longer than 7 characters or contains a space."));
                }
            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;
        }
    }


    public class ServicesDataConverterMetrics : IConverterMetrics
    {
        private ServicesDataConverterMetrics()
        {
        }

        private static ServicesDataConverterMetrics _instance;

        public static ServicesDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new ServicesDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }

        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Services Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records Converted: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);


            return report.ToString();

        }

    }
    

    public class ServicesDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[] { "model.EncounterServices" }; }
        }

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new ServicesDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
	        get 
	        { 
		        return new ServicesDataConverterResultsValidator();
	        }
        }
        

        public override IConverterMetrics Metrics
        {

            get
            {
                return ServicesDataConverterMetrics.Instance;
            }
        }

        protected override void Run(DataConverterArguments arguments)
        {
            var numberOfRowsInDataTable = arguments.DataTable.Rows.Count;
            var counter = 0;
            
            foreach (DataRow row in arguments.DataTable.Rows)
            {
                counter++;
                
                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var cpt = row["CPT"];

                if (cpt.ToString().IndexOf(" ", StringComparison.Ordinal) != -1)
                    continue;

                var encounterServiceTypeId = PracticeRepository.EncounterServiceTypes.Where(s => s.Name.Contains("Procedure")).Select(s => s.Id).FirstOrDefault();
                var encounterService = new EncounterService
                                              {
                                                  Code=row.GetValueOrDefault<string>("CPT"),
                                                  Description = row.GetValueOrDefault<string>("Description"),
                                                  DefaultUnit = 1,
                                                  UnitFee = row.GetValueOrDefault<decimal>("Fee"),
                                                  IsOrderingProviderRequiredOnClaim = false,
                                                  IsReferringDoctorRequiredOnClaim = false,
                                                  IsZeroChargeAllowedOnClaim = false,
                                                  IsArchived = false,
                                                  EncounterServiceTypeId = encounterServiceTypeId,
                                                  ServiceUnitOfMeasurement = ServiceUnitOfMeasurement.Unit,
                                                  IsCliaCertificateRequiredOnClaim = false
                                              };

                encounterService.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                var matchingServices = PracticeRepository.EncounterServices.WhereMatches(encounterService, new[] { "Code" }).ToArray();

                var isDuplicateService = matchingServices.Length > 0;

                if (!isDuplicateService)
                {
                    PracticeRepository.Save(encounterService);
                    ServicesDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                {
                    ServicesDataConverterMetrics.Instance.TotalDuplicates++;
                }
            }
            CleanUpServices();
        }


        private static void CleanUpServices()
        {
           using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
           {
               cleanupConnection.Execute(
                   @"
UDPATE es
SET es.Code = LTRIM(RTRIM(UPPER(es.Code)))
    ,es.[Description] = (UPPER(es.[Description])
FROM model.EncounterServices es
");
           }
        }

    }
}



