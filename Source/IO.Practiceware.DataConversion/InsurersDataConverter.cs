﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using Soaf.Linq;

namespace IO.Practiceware.DataConversion
{

    public class InsurersDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "InsurerName" };
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext);

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            return argumentValidationResults;

        }
    }

    /// <summary>
    /// Checks for bad data in the PracticeInsurers table.
    /// </summary>
    public class InsurersDataConverterResultsValidator : ArgumentsValidator, IValidatableObject
    {

        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "InsurerName" };
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidZipQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerZip <> ''
	AND LEN(InsurerZip) <> 5
	AND LEN(InsurerZip) <> 10
	AND InsurerZip NOT LIKE '%-%'");

                if (invalidZipQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Invalid insurer zip code."));
                }

                var insurerPlanIdBlankQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerPlanId <> ''
	OR InsurerGroupId <> ''");

                if (insurerPlanIdBlankQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Non-blank InsurerPlanId or InsurerGroupId."));
                }

                var insurerReferralRequiredQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE ReferralRequired <> 'N'");

                if (insurerReferralRequiredQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("ReferralRequired does not equal 'N'."));
                }

                var insurerCopayQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE Copay <> 0");

                if (insurerCopayQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Non-0 copay."));
                }

                var insurerOutOfPocketQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE OutOfPocket <> 0");

                if (insurerOutOfPocketQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Non-0 OutOfPocket."));
                }

                var insurerAvailabilityQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE Availability <> 'A'");

                if (insurerAvailabilityQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Availability is not equal to 'A'."));
                }

                var insurerEFormatQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerEFormat <> 'H'");

                if (insurerEFormatQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("InsurerEFormat does not equal 'H'."));
                }

                var insurerPayTypeQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerPayType <> 'IP'");

                if (insurerPayTypeQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("InsurerPayType does not equal 'IP'."));
                }

                var insurerAdjustmentDefaultQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerAdjustmentDefault <> ''");

                if (insurerAdjustmentDefaultQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Non-blank InsurerAdjustmentDefault."));
                }

                var insurerPrecertRequiredQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE PrecertRequired <> 'N'");

                if (insurerPrecertRequiredQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("PrecertRequired does not equal to 'N'."));
                }

                var insurerOCodeQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE OCode <> 'Y'
	AND OCode <> ''");

                if (insurerOCodeQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("OCode is not equal to Y or blank."));
                }

                var insurerServiceFilterQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM PracticeInsurers
WHERE InsurerServiceFilter <> 'N'");

                if (insurerServiceFilterQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("InsurerServiceFilterQuery does not equal 'N'."));
                }
            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;

        }
    }





    public class InsurersDataConverterFinalizer : IFinalizer
    {
        /// <summary>
        /// Sets insurer parameters based on content of InsurerName field.
        /// </summary>
        public void FinalizeData()
        {
            using (IDbConnection finalizationConnection = DbConnectionFactory.PracticeRepository)
            {
                finalizationConnection.Execute(
                    @"UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' PPO ', ' '), InsurerPlanType = 'PPO'
WHERE InsurerName LIKE '% PPO %'

UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' PPO', ''), InsurerPlanType = 'PPO'
WHERE InsurerName LIKE '% PPO'

UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' HMO ', ' '), InsurerPlanType = 'HMO'
WHERE InsurerName LIKE '% HMO %'

UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' HMO', ' '), InsurerPlanType = 'HMO'
WHERE InsurerName LIKE '% HMO'

UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' POS ', ' '), InsurerPlanType = 'POS'
WHERE InsurerName LIKE '% POS %'

UPDATE PracticeInsurers
SET InsurerName = REPLACE (InsurerName, ' POS', ' '), InsurerPlanType = 'POS'
WHERE InsurerName LIKE '% POS'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%ANTHEM %'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BCBS%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BLUE CROSS%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BLUE CROSS%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BLUE SHIELD%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BLUESHIELD%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%BLUE CROSS BLUE SHIELD%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%EMPIRE BLUE%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'P'
WHERE InsurerName LIKE '%EMPIRE BCBS%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'C'
WHERE InsurerName LIKE '%MEDICAID%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'M'
WHERE InsurerName LIKE '%MEDICARE%NEW YORK%'
OR InsurerName LIKE '%MEDICARE%NY%'
OR InsurerName LIKE '%NY%MEDICARE%'
OR InsurerName LIKE '%NEW YORK%MEDICARE%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'N'
WHERE InsurerName LIKE '%MEDICARE%NEW JERSEY%'
OR InsurerName LIKE '%MEDICARE%NJ%'
OR InsurerName LIKE '%NJ%MEDICARE%'
OR InsurerName LIKE '%NEW JERSEY%MEDICARE%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = ']'
WHERE InsurerName LIKE '%MEDICARE%PENNSYLVANIA%'
OR InsurerName LIKE '%MEDICARE%PA%'
OR InsurerName LIKE '%PA%MEDICARE%'
OR InsurerName LIKE '%PENNSYLVANIA%MEDICARE%'

UPDATE PracticeInsurers
SET InsurerReferenceCode = 'N'
WHERE InsurerName LIKE '%MEDICARE RAILROAD%'
OR InsurerName LIKE '%RAILROAD MEDICARE%'

UPDATE PracticeInsurers
SET OutOfPocket = 1

WHERE InsurerName LIKE '%MEDICAID%'
OR InsurerName LIKE '%MEDICARE%'

UPDATE PracticeInsurers
SET Availability = '-'

WHERE InsurerName LIKE '%MEDICAID%NEW YORK%'
OR InsurerName LIKE '%MEDICARE%NEW YORK%'
OR InsurerName LIKE '%MEDICAID%NY%'
OR InsurerName LIKE '%MEDICARE%NY%'
OR InsurerName LIKE '%NY%MEDICARE%'
OR InsurerName LIKE '%NY%MEDICAID%'
OR InsurerName LIKE '%NEW YORK%MEDICARE%'
OR InsurerName LIKE '%NEW YORK%MEDICAID%'

UPDATE PracticeInsurers
SET InsurerEFormat = 'H'

UPDATE PracticeInsurers
SET InsurerEFormat = 'Z'
WHERE InsurerName LIKE '%MEDICARE RAILROAD%'

UPDATE PracticeInsurers
SET InsurerPayType = 'MG'
WHERE InsurerName LIKE '%AARP%'
OR InsurerName LIKE '%MEDIGAP%'

--cms1500
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId) 
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 1 AS InvoiceTypeId   
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms


--cms1500
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId)
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 3 AS InvoiceTypeId
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms



--837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 1 AS InvoiceTypeId   
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms


--837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 3 AS InvoiceTypeId
FROM dbo.PracticeInsurers
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms


--facility cms1500
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId)
SELECT 1 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 2 AS InvoiceTypeId
FROM dbo.PracticeInsurers
WHERE InsurerReferenceCode <> 'P' AND InsurerName NOT LIKE '%OXFORD%'
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms


--facility 837p
INSERT INTO [model].[InsurerClaimForms] (ClaimFormTypeId, InsurerId, InvoiceTypeId)
SELECT 3 AS ClaimFormTypeId
    , InsurerId As InsurerId
    , 2 AS InvoiceTypeId
FROM dbo.PracticeInsurers
WHERE InsurerReferenceCode <> 'P' AND InsurerName NOT LIKE '%OXFORD%'
EXCEPT SELECT ClaimFormTypeId, InsurerId, InvoiceTypeId FROM model.InsurerClaimForms


");
            }
        }

    }

    public class InsurersDataConverterMetrics : IConverterMetrics
    {
        private InsurersDataConverterMetrics()
        {
        }

        private static InsurersDataConverterMetrics _instance;

        public static InsurersDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new InsurersDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }
        public int TotalDuplicates { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Insurers Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records in Table: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Duplicates Found in Input Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            return report.ToString();

        }

    }

    public class InsurersDataConverter : DataConverter
    {

        public override string[] AffectedTableNames
        {
            get { return new[] { "dbo.PracticeInsurers", "model.InsurerPhoneNumbers" }; }
        }


        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new InsurersDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
            get
            {
                return new InsurersDataConverterResultsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {

            get
            {
                return InsurersDataConverterMetrics.Instance;
            }
        }


        public override IFinalizer Finalizer
        {
            get
            {
                return new InsurersDataConverterFinalizer();
            }
        }



        protected override void Run(DataConverterArguments arguments)
        {
            IList<StateOrProvince> cachedStateOrProvinces = PracticeRepository.StateOrProvinces.ToList();

            int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

            int counter = 0;

            foreach (DataRow row in arguments.DataTable.Rows)
            {
                counter++;

                // Every 100 rows, update the progress of the converter.
                if (counter % 100 == 0)
                {
                    Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                }

                var insurer = new PracticeInsurer
                {
                    InsurerProvPhone = row.Get<string>("OldInsurerId"),
                    InsurerName = row.Get<string>("InsurerName"),
                    InsurerAddress = row.GetValueOrDefault<string>("InsurerAddress"),
                    InsurerCity = row.GetValueOrDefault<string>("InsurerCity"),
                    InsurerState = row.GetValueOrDefault<string>("InsurerState"),
                    InsurerZip = row.GetValueOrDefault<string>("InsurerZip"),
                    InsurerPhone = row.GetValueOrDefault<string>("InsurerPhone").ToAlphanumeric(),
                    InsurerPrecPhone = row.GetValueOrDefault<string>("InsurerPrecertPhone").ToAlphanumeric(),
                    InsurerClaimPhone = row.GetValueOrDefault<string>("InsurerClaimsPhone").ToAlphanumeric(),
                    InsurerComment = row.GetValueOrDefault<string>("InsurerComment"),
                    InsurerEligPhone = row.GetValueOrDefault<string>("InsurerFax").ToAlphanumeric(),
                    NEICNumber = row.GetValueOrDefault<string>("InsurerPayorId"),
                    InsurerEFormat = "H"
                };

                insurer.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                PracticeInsurer[] matchingInsurers = LegacyPracticeRepository.PracticeInsurers.WhereMatches(insurer, new[] { "InsurerName", "InsurerAddress", "InsurerCity", "InsurerState", "InsurerZip" }).ToArray();

                bool isDuplicateInsurer = matchingInsurers.Length > 0;

                if (!isDuplicateInsurer)
                {
                    LegacyPracticeRepository.Save(insurer);
                    InsurersDataConverterMetrics.Instance.TotalRecordsConverted++;
                }
                else
                {
                    InsurersDataConverterMetrics.Instance.TotalDuplicates++;
                    insurer.InsurerId = matchingInsurers[0].InsurerId;
                }

                CleanupInsurers();

                var modelInsurer = PracticeRepository.Insurers.WithId(insurer.InsurerId);

                string abbreviation = insurer.InsurerState;
                var stateOrProvince = cachedStateOrProvinces.Where(sop => sop.Abbreviation == abbreviation);
                if (stateOrProvince.Any())
                {
                    modelInsurer.InsurerAddresses.Add(new InsurerAddress
                    {
                        InsurerId = insurer.InsurerId,
                        InsurerAddressTypeId = (int)InsurerAddressTypeId.Claims,
                        Line1 = insurer.InsurerAddress,
                        City = insurer.InsurerCity,
                        StateOrProvinceId = stateOrProvince.First().Id,
                        PostalCode = insurer.InsurerZip
                    });
                }
                if (!insurer.InsurerPhone.IsNullOrEmpty() && insurer.InsurerPhone.Length == 10)
                {
                    modelInsurer.InsurerPhoneNumbers.Add(new InsurerPhoneNumber
                    {
                        InsurerId = insurer.InsurerId,
                        InsurerPhoneNumberTypeId = (int)InsurerPhoneNumberTypeId.Main,
                        ExchangeAndSuffix = insurer.InsurerPhone.Substring(3, insurer.InsurerPhone.Length - 3),
                        AreaCode = insurer.InsurerPhone.Substring(0, 3),
                        OrdinalId = (int)InsurerPhoneNumberTypeId.Main
                    });
                }

                if (!insurer.InsurerPrecPhone.IsNullOrEmpty() && insurer.InsurerPrecPhone.Length == 10)
                {
                    modelInsurer.InsurerPhoneNumbers.Add(new InsurerPhoneNumber
                    {
                        InsurerId = insurer.InsurerId,
                        InsurerPhoneNumberTypeId = (int)InsurerPhoneNumberTypeId.Authorization,
                        ExchangeAndSuffix = insurer.InsurerPrecPhone.Substring(3, insurer.InsurerPrecPhone.Length - 3),
                        AreaCode = insurer.InsurerPrecPhone.Substring(0, 3),
                        OrdinalId = (int)InsurerPhoneNumberTypeId.Authorization
                    });
                }

                if (!insurer.InsurerClaimPhone.IsNullOrEmpty() && insurer.InsurerClaimPhone.Length == 10)
                {
                    modelInsurer.InsurerPhoneNumbers.Add(new InsurerPhoneNumber
                    {
                        InsurerId = insurer.InsurerId,
                        InsurerPhoneNumberTypeId = (int)InsurerPhoneNumberTypeId.Claims,
                        ExchangeAndSuffix = insurer.InsurerClaimPhone.Substring(3, insurer.InsurerClaimPhone.Length - 3),
                        AreaCode = insurer.InsurerClaimPhone.Substring(0, 3),
                        OrdinalId = (int)InsurerPhoneNumberTypeId.Claims
                    });
                }

                if (!insurer.InsurerEligPhone.IsNullOrEmpty() && insurer.InsurerEligPhone.Length == 10)
                {
                    modelInsurer.InsurerPhoneNumbers.Add(new InsurerPhoneNumber
                    {
                        InsurerId = insurer.InsurerId,
                        InsurerPhoneNumberTypeId = (int)InsurerPhoneNumberTypeId.Eligibility,
                        ExchangeAndSuffix = insurer.InsurerEligPhone.Substring(3, insurer.InsurerEligPhone.Length - 3),
                        AreaCode = insurer.InsurerEligPhone.Substring(0, 3),
                        OrdinalId = (int)InsurerPhoneNumberTypeId.Eligibility
                    });
                }

                PracticeRepository.Save(modelInsurer);
            }

        }


        private void CleanupInsurers()
        {

            using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
            {
                cleanupConnection.Execute(
                    @"UPDATE PracticeInsurers
SET InsurerAddress = UPPER( InsurerAddress ),
InsurerCity = UPPER( InsurerCity ),
InsurerState = UPPER( InsurerState ),
InsurerPlanName = UPPER( InsurerPlanName ),
InsurerComment = UPPER( InsurerComment )

UPDATE PracticeInsurers
SET OutOfPocket =
    CASE WHEN InsurerName LIKE '%Medicare%' OR InsurerName LIKE '%Medicaid%'
        THEN CONVERT(BIT,1)
    ELSE CONVERT(BIT,0) END

UPDATE PracticeInsurers
SET AllowDependents =
    CASE WHEN InsurerName LIKE '%Medicare%' OR InsurerName LIKE '%Medicaid%'
        THEN CONVERT(BIT,0)
    ELSE CONVERT(BIT,1) END

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'POB ', 'PO BOX ')
WHERE InsurerAddress LIKE 'POB %'

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'P.O.BOX ', 'PO BOX ')
WHERE InsurerAddress LIKE 'P.O.B%'

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'P.O. BOX ', 'PO BOX ')
WHERE InsurerAddress LIKE 'P.O. BOX %'

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'POBOX ', 'PO BOX ')
WHERE InsurerAddress LIKE 'POBOX %'

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'BOX ', 'PO BOX ')
WHERE InsurerAddress LIKE 'BOX %'

UPDATE PracticeInsurers
SET InsurerAddress = REPLACE (InsurerAddress, 'P O BOX', 'PO BOX ')
WHERE InsurerAddress LIKE 'P O %'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'POB ', 'PO BOX ')
WHERE InsurerComment LIKE 'POB %'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'P.O. BOX ', 'PO BOX ')
WHERE InsurerComment LIKE 'P.O. BOX %'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'POBOX ', 'PO BOX ')
WHERE InsurerComment LIKE 'POBOX %'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'BOX ', 'PO BOX ')
WHERE InsurerComment LIKE 'BOX %'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'P.O.BOX ', 'PO BOX ')
WHERE InsurerComment LIKE 'P.O.B%'

UPDATE PracticeInsurers
SET InsurerComment = REPLACE (InsurerComment, 'P O BOX', 'PO BOX ')
WHERE InsurerComment LIKE 'P O %'

UPDATE PracticeInsurers
SET InsurerZip = '0' + InsurerZip
WHERE LEN(InsurerZip) = 4
");
            }
        }



    }
}