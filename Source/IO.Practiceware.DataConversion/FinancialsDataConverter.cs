﻿
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DataConversion
{

    public class FinancialsDataConverterArgumentsValidator : ArgumentsValidator, IValidatableObject
    {
        public override string[] FieldsRequiredInDataTable
        {
            get
            {
                return new[] { "OldPatientId", "OldInsurer1Id", "Insurer1MemberNumber" };
            }
        }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var argumentDataTableFieldValidationResults = ValidateColumnsInDataTable(validationContext);

            var argumentValidationResults = argumentDataTableFieldValidationResults;

            return argumentValidationResults;

        }
    }


    public class FinancialDataConverterResultsValidator : IValidatableObject
    {


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var validationResults = new List<ValidationResult>();
            using (IDbConnection validationConnection = DbConnectionFactory.PracticeRepository)
            {
                var invalidPatientIdQuery = validationConnection.Execute<DataTable>(
                    @"SELECT *
FROM model.PatientInsurances
WHERE InsuredPatientId = 0
");

                if (invalidPatientIdQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Policy cannot have PatientId equal to 0."));
                }


                var duplicatePIndicatorQuery = validationConnection.Execute<DataTable>(
                    @"SELECT pi.InsuredPatientId
	,pi.InsuranceTypeId
	,pi.OrdinalId
	,COUNT(*)
FROM model.PatientInsurances pi
INNER JOIN model.InsurancePolicies ip ON pi.InsurancePolicyId = ip.id
WHERE (
		pi.EndDateTime IS NULL
		OR pi.EndDateTime >= CONVERT(DATETIME, CONVERT(NVARCHAR, GETDATE(), 112), 112)
		)
	AND (
		ip.StartDateTime IS NULL
		OR ip.StartDateTime <= CONVERT(DATETIME, CONVERT(NVARCHAR, GETDATE(), 112), 112)
		)
	AND pi.IsDeleted <> 1
GROUP BY pi.InsuredPatientId
	,pi.InsuranceTypeId
	,pi.OrdinalId
HAVING COUNT(*) > 1
");

                if (duplicatePIndicatorQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Duplicate PIndicator."));
                }

                var invalidPolicyDatesQuery = validationConnection.Execute<DataTable>(
@"SELECT *
FROM model.InsurancePolicies
WHERE (
len(CONVERT(NVARCHAR(10),StartDateTime,112)) <> 8
AND CONVERT(NVARCHAR(10),StartDateTime,112) <> ''
)
OR CONVERT(NVARCHAR(10),StartDateTime,112) LIKE '%-%'
OR CONVERT(NVARCHAR(10),StartDateTime,112) LIKE '%/%'
");

                if (invalidPolicyDatesQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Invalid policy dates."));
                }

                invalidPolicyDatesQuery = validationConnection.Execute<DataTable>(
@"SELECT *
FROM model.PatientInsurances
WHERE (
len(CONVERT(NVARCHAR(10),EndDateTime,112)) <> 8
AND CONVERT(NVARCHAR(10),EndDateTime,112) <> ''
)
OR CONVERT(NVARCHAR(10),EndDateTime,112) LIKE '%-%'
OR CONVERT(NVARCHAR(10),EndDateTime,112) LIKE '%/%'
");

                if (invalidPolicyDatesQuery.Rows.Count > 0)
                {
                    validationResults.Add(new ValidationResult("Invalid policy dates."));
                }

                var incorrectlySetPolicyPatientIdQuery1 = validationConnection.Execute<DataTable>(
@"SELECT ip.PolicyHolderPatientId
FROM model.InsurancePolicies ip
LEFT JOIN model.PatientInsurances pi ON ip.Id = pi.InsurancePolicyId
WHERE PolicyHolderPatientId IS NULL
AND IsDeleted = 0
");

                if (incorrectlySetPolicyPatientIdQuery1.Rows.Count > 0)
                {
                    throw new ArgumentException("Policy exists with no corresponding policy holder set in PatientDemographics.");
                }

            }

            if (validationResults.Count == 0)
            {
                validationResults.Add(ValidationResult.Success);
            }

            return validationResults;

        }

    }

    public class FinancialsDataConverterMetrics : IConverterMetrics
    {
        private FinancialsDataConverterMetrics()
        {
        }

        private static FinancialsDataConverterMetrics _instance;

        public static FinancialsDataConverterMetrics Instance
        {
            get { return _instance ?? (_instance = new FinancialsDataConverterMetrics()); }
        }

        public int TotalRecordsConverted { get; set; }

        public int TotalDuplicates { get; set; }

        public int TotalPolicyHoldersAdded { get; set; }

        public int TotalFailuresDueToMissingPatient { get; set; }

        public int TotalFailuresDueToMissingInsurer { get; set; }

        public int TotalFailuresDueToMissingPolicyHolder { get; set; }

        public int TotalSecondaryInsurances { get; set; }

        public string OutputMetrics()
        {
            var report = new StringBuilder();

            report.AppendLine("Financials Converter Metrics" + Environment.NewLine + Environment.NewLine);

            report.AppendFormat("Total Records Converted: {0}" + Environment.NewLine + Environment.NewLine, TotalRecordsConverted);

            report.AppendFormat("Total Policy Holders Added: {0}" + Environment.NewLine + Environment.NewLine, TotalPolicyHoldersAdded);

            report.AppendFormat("Total Duplicate Financial Policies Found In Data: {0}" + Environment.NewLine + Environment.NewLine, TotalDuplicates);

            report.AppendFormat("Total Failures due to Missing Insurers: {0}" + Environment.NewLine + Environment.NewLine, TotalFailuresDueToMissingInsurer);

            report.AppendFormat("Total Failures due to Missing Patients: {0}" + Environment.NewLine + Environment.NewLine, TotalFailuresDueToMissingPatient);

            report.AppendFormat("Total Failures due to Missing Policy Holders: {0}" + Environment.NewLine + Environment.NewLine, TotalFailuresDueToMissingPolicyHolder);

            report.AppendFormat("Total secondary insurances added: {0}" + Environment.NewLine + Environment.NewLine, TotalSecondaryInsurances);

            return report.ToString();

        }

    }

    /// <summary>
    /// Data converter for patient financial policies
    /// </summary>
    public class FinancialsDataConverter : DataConverter
    {
        //private IDictionary<Tuple<long, int, int, InsuranceType>, HashSet<PatientInsurance>> _patientInuranceByKey;
        //private readonly IDictionary<object, ObjectStateEntry> _objectStateEntries = new Dictionary<object, ObjectStateEntry>();
        public override string[] AffectedTableNames
        {
            get { return new[] { "model.PatientInsurances", "model.InsurancePolicies", "model.Patients", "model.PatientAddresses", "model.StateOrProvinces", "model.PatientPhoneNumbers", "model.PatientCommunicationPreferences", "model.PatientEmailAddresses", "model.PatientComments" }; }
        }

        public override IValidatableObject ArgumentsValidator
        {
            get
            {
                return new FinancialsDataConverterArgumentsValidator();
            }
        }

        public override IValidatableObject ResultsValidator
        {
            get
            {
                return new FinancialDataConverterResultsValidator();
            }
        }

        public override IConverterMetrics Metrics
        {

            get
            {
                return FinancialsDataConverterMetrics.Instance;
            }
        }


        protected override void Run(DataConverterArguments arguments)
        {
            try
            {
                IList<PatientInsurance> cachedPatientInsurances = PracticeRepository.PatientInsurances.ToList();
                IList<InsurancePolicy> cachedInsurancePolicies = PracticeRepository.InsurancePolicies.ToList();
                IList<Patient> cachedPatients = PracticeRepository.Patients.ToList();
                IList<Insurer> cachedInsurers = PracticeRepository.Insurers.ToList(); //there was an error occured related to AllowDependent, will check later.
                //_patientInuranceByKey = cachedPatientInsurances.GroupBy(i => Tuple.Create(i.Id, i.InsuredPatientId, i.InsurancePolicyId, i.InsuranceType))
                //.ToDictionary(i => i.Key, i => new HashSet<PatientInsurance>(i.ToArray()));
                int numberOfRowsInDataTable = arguments.DataTable.Rows.Count;

                int counter = 0;

                foreach (DataRow row in arguments.DataTable.Rows)
                {

                    counter++;

                    // Every 100 rows, update the progress of the converter.
                    if (counter % 100 == 0)
                    {
                        Utilities.DisplayProgress(numberOfRowsInDataTable, counter);
                    }

                    var oldPatientId = row.GetValueOrDefault<string>("OldPatientId");

                    if (string.IsNullOrEmpty(oldPatientId))
                    {
                        FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPatient++;
                        continue;
                    }

                    Patient patient = cachedPatients.Where(p => p.PriorPatientCode == oldPatientId).Select(p => p).FirstOrDefault();

                    if (patient == null)
                    {
                        FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPatient++;
                        continue;
                    }

                    for (int i = 1; i <= 3; i++)
                    {
                        var oldInsurerId = row.GetValueOrDefault<string>("OldInsurer" + i + "Id").ToUpper();

                        if (string.IsNullOrEmpty(oldInsurerId) || oldInsurerId == "0")
                        {
                            // If an old insurer ID is blank, it indicates that the patient doesn't have an "n"th policy, and so this sets i = 4 so it won't search for any additional policies for the patient.
                            i = 4;
                            continue;
                        }


                        int insurerId = cachedInsurers.Where(ci => ci.PriorInsurerCode == oldInsurerId).Select(ci => ci.Id).FirstOrDefault();

                        if (insurerId == 0)
                        {
                            FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingInsurer++;
                            continue;
                        }

                        var iOInsurerRelationship = row.GetValueOrDefault<string>("IOInsurer" + i + "Relationship");

                        if (string.IsNullOrEmpty(iOInsurerRelationship))
                        {
                            FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPolicyHolder++;
                            continue;
                        }

                        int policyHolderId;

                        if (iOInsurerRelationship == "Y") //Mike suggestion: Make function or boolean variable to make condition clearer
                        {
                            policyHolderId = patient.Id;
                        }
                        else
                        {
                            if (!(new[] { "C", "S" }.Contains(iOInsurerRelationship)))
                            {
                                i = 4;
                                FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPolicyHolder++;
                                continue;
                            }

                            policyHolderId = PracticeRepository.GetPolicyHolder(row, i);

                            if (policyHolderId == -1)
                            {
                                i = 4;
                                FinancialsDataConverterMetrics.Instance.TotalFailuresDueToMissingPolicyHolder++;
                                continue;
                            }

                            if (policyHolderId == 0)
                            {
                                var postPatients = PracticeRepository.PostPolicyHolder(row, i);
                                policyHolderId = postPatients.Id;
                                cachedPatients.Add(postPatients);
                                FinancialsDataConverterMetrics.Instance.TotalPolicyHoldersAdded++;
                            }
                        }

                        InsurancePolicy[] policyHolderPolicies = cachedInsurancePolicies.Where(ci => ci.PolicyholderPatientId == policyHolderId).Select(ci => ci).ToArray();

                        var insuredPatient = cachedPatientInsurances.Where(pi => pi.InsuredPatientId == policyHolderId).Select(pi => pi);

                        string insurerType = row.GetValueOrDefault<string>("IOInsurer" + i + "Type");

                        //As per John, There will be only one policy for parent policy holder.
                        if (policyHolderPolicies.Any() && insuredPatient.Any() && (insurerType == "A" || insurerType == "W")) { continue; }
                        if (policyHolderPolicies.Any() && insuredPatient.Any())
                        {
                            FinancialsDataConverterMetrics.Instance.TotalSecondaryInsurances++;
                        }

                        int polHoldInsCount = policyHolderPolicies.Length + 1;

                        /* PatientInsurance */

                        var patientInsurance = new PatientInsurance
                        {
                            InsuredPatientId = policyHolderId,
                            OrdinalId = polHoldInsCount,
                            IsDeleted = false, //Status = "C",
                            PolicyholderRelationshipType = PolicyHolderRelationshipType.Self
                        };

                        var policyCode = row.GetValueOrDefault<string>("Insurer" + i + "MemberNumber").ToAlphanumeric();
                        var matchingPolicy = cachedInsurancePolicies.Where(ip => ip.PolicyCode == policyCode).Select(ip => ip).FirstOrDefault();
                        if (matchingPolicy == null)
                        {
                            var insurancePolicy = new InsurancePolicy
                            {
                                InsurerId = insurerId,
                                PolicyCode = policyCode,
                                GroupCode = row.GetValueOrDefault<string>("Insurer" + i + "GroupNumber").ToAlphanumeric(),
                                Copay = row.GetValueOrDefault<int>("Insurer" + i + "Copay"),
                                StartDateTime = DateTime.ParseExact((Utilities.ValidateDate(row.GetValueOrDefault<string>("Insurer" + i + "StartDate")) == string.Empty ? row.GetValueOrDefault<string>("GoLiveDate") : row.GetValueOrDefault<string>("Insurer" + i + "StartDate")), "yyyyMMdd", CultureInfo.InvariantCulture,
                                    DateTimeStyles.None),
                                PolicyholderPatientId = cachedPatients.Where(p => p.Id == policyHolderId).Select(p => p.Id).FirstOrDefault(),
                            };
                            patientInsurance.InsurancePolicy = insurancePolicy;
                            cachedInsurancePolicies.Add(insurancePolicy);
                        }
                        else
                        {
                            patientInsurance.InsurancePolicyId = matchingPolicy.Id;
                        }

                        string endDateTime = Utilities.ValidateDate(row.GetValueOrDefault<string>("Insurer" + i + "EndDate"));
                        if (!string.IsNullOrEmpty(endDateTime))
                        {
                            patientInsurance.EndDateTime = DateTime.ParseExact(endDateTime, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        switch (insurerType)
                        {
                            case "V":
                                patientInsurance.InsuranceType = InsuranceType.Vision;
                                break;
                            case "A":
                                patientInsurance.InsuranceType = InsuranceType.Ambulatory;
                                break;
                            case "W":
                                patientInsurance.InsuranceType = InsuranceType.WorkersComp;
                                break;
                            default:
                                patientInsurance.InsuranceType = InsuranceType.MajorMedical;
                                break;
                        }

                        patientInsurance.TruncateStringProperties().ToUpperStringProperties().TrimStringProperties();

                        var matchingPolicies = from pi in cachedPatientInsurances
                                               where pi.InsuredPatientId == patientInsurance.InsuredPatientId &&
                                                     pi.InsurancePolicyId == patientInsurance.InsurancePolicyId
                                               select pi;

                        bool isDuplicateFinancialRecord = matchingPolicies.Any();

                        if (!isDuplicateFinancialRecord)
                        {
                            cachedPatientInsurances.Add(patientInsurance);
                            FinancialsDataConverterMetrics.Instance.TotalRecordsConverted++;
                            PracticeRepository.Save(patientInsurance);
                          /*Commenting it for temprory purpose as key not found issue is there
                           * _patientInuranceByKey.GetValue(Tuple.Create(patientInsurance.Id, patientInsurance.InsuredPatientId, patientInsurance.InsurancePolicyId, patientInsurance.InsuranceType), () => new HashSet<PatientInsurance>()).Add(patientInsurance);
                            _objectStateEntries[counter] = new ObjectStateEntry(patientInsurance) { State = ObjectState.Added };
                           * */
                        }
                        else
                        {
                            FinancialsDataConverterMetrics.Instance.TotalDuplicates++;
                            continue;
                        }

                        // Check if patient has a primary policy holder, and that the patient's primary policy holder actually has a policy
                        int? primaryPatientPolicyHolderId = cachedInsurancePolicies.Where(ip => ip.Id == patientInsurance.InsurancePolicyId).Select(ci => ci.PolicyholderPatientId).FirstOrDefault();

                        if (policyHolderId == primaryPatientPolicyHolderId)
                            continue;

                        if (primaryPatientPolicyHolderId == 0)
                        {
                            var patientInsuranceWithoutPolicyHolder = cachedPatientInsurances.Where(ip => ip.InsurancePolicy != null && ip.InsurancePolicy.PolicyholderPatientId == 0).Select(pi => pi).FirstOrDefault();
                            if (patientInsuranceWithoutPolicyHolder != null)
                            {
                                patientInsuranceWithoutPolicyHolder.InsurancePolicy.PolicyholderPatientId = policyHolderId;
                                patientInsuranceWithoutPolicyHolder.PolicyholderRelationshipType = PolicyHolderRelationshipType.Self;
                                PracticeRepository.Save(patientInsuranceWithoutPolicyHolder);
                            }
                        }
                       
                    }
                }
                //RepositoryService.Persist(_objectStateEntries.Values, typeof(IPracticeRepository).FullName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CleanUpFinancialPolicies();
            }

        }



        private void CleanUpFinancialPolicies()
        {
            using (IDbConnection cleanupConnection = DbConnectionFactory.PracticeRepository)
            {
                cleanupConnection.Execute(
                    @"UPDATE model.PatientInsurances
SET OrdinalId = OrdinalId + 1
WHERE Id > (
		SELECT min(Id)
		FROM model.PatientInsurances pi
		WHERE pi.InsuredPatientId = PatientInsurances.InsuredPatientId
			AND pi.OrdinalId = PatientInsurances.OrdinalId
		)

UPDATE model.PatientInsurances
SET OrdinalId = OrdinalId - 1
WHERE OrdinalId > 1
	AND NOT EXISTS (
		SELECT pi.OrdinalId
		FROM model.PatientInsurances pi
		WHERE pi.OrdinalId = PatientInsurances.OrdinalId - 1
			AND pi.InsuredPatientId = PatientInsurances.InsuredPatientId
		)

UPDATE model.PatientInsurances
SET PolicyHolderRelationshipTypeId = 1
FROM model.PatientInsurances pi
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.patients p ON p.Id = pi.InsuredPatientId
WHERE model.GetAgeFromDob(p.DateOfBirth) < 15
	AND p.Id <> ip.PolicyHolderPatientId
	AND pi.PolicyHolderRelationshipTypeId in (5,7,8,9)

UPDATE model.PatientInsurances
SET PolicyHolderRelationshipTypeId = 3
FROM model.PatientInsurances pi
INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
INNER JOIN model.patients p ON p.Id = pi.InsuredPatientId
WHERE model.GetAgeFromDob(p.DateOfBirth) < 15
	AND p.Id <> ip.PolicyHolderPatientId
	AND pi.PolicyHolderRelationshipTypeId in (5,7,8,9)

UPDATE model.InsurancePolicies
SET InsurancePolicies.PolicyHolderPatientId = pi.InsuredPatientId
FROM model.InsurancePolicies ip
INNER JOIN model.PatientInsurances pi ON ip.id = pi.InsurancePolicyId
WHERE PolicyHolderPatientId = 0
"
                    );
            }
        }
    }

}