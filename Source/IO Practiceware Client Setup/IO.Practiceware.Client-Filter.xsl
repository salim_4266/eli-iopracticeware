<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wix="http://schemas.microsoft.com/wix/2006/wi" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
   
  <!-- Copy all attributes and elements to the output. -->
  <xsl:template match="@*|*">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="*" />
    </xsl:copy>
  </xsl:template>

  <xsl:output method="xml" indent="yes" />

  <!-- Exclude *.cmd, *.log, *.tlb and *.pdb files -->
  <xsl:key name="cmd-exclude-search" match="wix:Component[contains(wix:File/@Source, '.cmd')]" use="@Id" />
  <xsl:template match="wix:Component[key('cmd-exclude-search', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('cmd-exclude-search', @Id)]" />
  
  <xsl:key name="log-exclude-search" match="wix:Component[contains(wix:File/@Source, '.log')]" use="@Id" />
  <xsl:template match="wix:Component[key('log-exclude-search', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('log-exclude-search', @Id)]" />
  
  <xsl:key name="tlb-exclude-search" match="wix:Component[contains(wix:File/@Source, '.tlb')]" use="@Id" />
  <xsl:template match="wix:Component[key('tlb-exclude-search', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('tlb-exclude-search', @Id)]" />
  
  <xsl:key name="pdb-exclude-search" match="wix:Component[contains(wix:File/@Source, '.pdb')]" use="@Id" />
  <xsl:template match="wix:Component[key('pdb-exclude-search', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('pdb-exclude-search', @Id)]" />
  
  <!-- Exclude Readme.txt which is dropped by DbSync project -->
  <xsl:key name="readme-exclude-search" match="wix:Component[contains(wix:File/@Source, 'Readme.txt')]" use="@Id" />
  <xsl:template match="wix:Component[key('readme-exclude-search', @Id)]" />
  <xsl:template match="wix:ComponentRef[key('readme-exclude-search', @Id)]" />
  
  <!-- Use sticky Id for IO.Practiceware.Interop.dll file, since we'll use it's file version as installer version -->
  <xsl:template match="wix:File[@Source = '$(var.ClientBinDir)\IO.Practiceware.Interop.dll']/@Id">
    <xsl:attribute name="Id">filInteropDll</xsl:attribute>
  </xsl:template>
  
  <!-- Ensure Custom.config is permanent and does not get overwritten -->
  <xsl:template match="wix:Component[contains(wix:File/@Source, '$(var.ClientBinDir)\IO.Practiceware.Custom.config')]">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="Guid">{7AC2EA38-C577-5173-9D3F-50E58DC7D1E5}</xsl:attribute>
      <xsl:attribute name="NeverOverwrite">yes</xsl:attribute>
      <xsl:attribute name="Permanent">yes</xsl:attribute>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- Create shortcuts to executables -->
  <xsl:template match="wix:Component/wix:File[@Source = '$(var.ClientBinDir)\Administrator.exe']">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
      <xsl:element name="Shortcut" namespace="http://schemas.microsoft.com/wix/2006/wi">
        <xsl:attribute name="Id">AdministratorExeShortcut</xsl:attribute>
        <xsl:attribute name="Advertise">yes</xsl:attribute>
        <xsl:attribute name="Directory">DesktopFolder</xsl:attribute>
        <xsl:attribute name="Name">Admin</xsl:attribute>
        <xsl:attribute name="Description">Shortcut to the Scheduler Interface</xsl:attribute>
        <xsl:attribute name="Icon">icon.exe</xsl:attribute>
        <xsl:attribute name="WorkingDirectory">INSTALLDIR</xsl:attribute>
      </xsl:element>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="wix:Component/wix:File[@Source = '$(var.ClientBinDir)\DoctorInterface.exe']">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
      <xsl:element name="Shortcut" namespace="http://schemas.microsoft.com/wix/2006/wi">
        <xsl:attribute name="Id">DoctorInterfaceExeShortcut</xsl:attribute>
        <xsl:attribute name="Advertise">yes</xsl:attribute>
        <xsl:attribute name="Directory">DesktopFolder</xsl:attribute>
        <xsl:attribute name="Name">Exam</xsl:attribute>
        <xsl:attribute name="Description">Shortcut to the Doctor Interface</xsl:attribute>
        <xsl:attribute name="Icon">icon.exe</xsl:attribute>
        <xsl:attribute name="WorkingDirectory">INSTALLDIR</xsl:attribute>
      </xsl:element>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="wix:Component/wix:File[@Source = '$(var.ClientBinDir)\PatientInterface.exe']">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
      <xsl:element name="Shortcut" namespace="http://schemas.microsoft.com/wix/2006/wi">
        <xsl:attribute name="Id">PatientInterfaceExeShortcut</xsl:attribute>
        <xsl:attribute name="Advertise">yes</xsl:attribute>
        <xsl:attribute name="Directory">DesktopFolder</xsl:attribute>
        <xsl:attribute name="Name">Patient</xsl:attribute>
        <xsl:attribute name="Description">Shortcut to the Patient Interface</xsl:attribute>
        <xsl:attribute name="Icon">icon.exe</xsl:attribute>
        <xsl:attribute name="WorkingDirectory">INSTALLDIR</xsl:attribute>
      </xsl:element>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
