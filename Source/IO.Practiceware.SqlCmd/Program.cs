﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Soaf.Data;
using IO.Practiceware.Application;
using System.Data;
using IO.Practiceware.Data;
namespace IO.Practiceware.SqlCmd
{
    class Program
    {
        /// <summary>
        /// Main program, takes the arguements provided, parses them, logs in the user and then runs a query on the cloud
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                Bootstrapper.Run<IAdoService>();
                var options = new Options();
                if (CommandLine.Parser.Default.ParseArguments(args, options)) // parse the options
                {
                    using (var output = new Output(options.Output, options.IncludeHeader, options.Delimiter, options.TurnOffDelimiterArgument, options.FixedColumnLength))
                    {
                        if (!options.TurnOffDelimiterArgument && string.IsNullOrEmpty(options.Delimiter))
                        {
                            output.Delimiter = ",";
                        }

                        #region Login

                        if (!IsValid(options, output))
                        {
                            return;
                        }
                        if (string.IsNullOrEmpty(options.Pin))
                        {
                            return;
                        }
                        //login 
                        var ioUserId = UserContext.Current.Login(options.Pin);
                        if (ioUserId == -1)
                        {
                            throw new Exception("Your login failed. Please make sure you've entered the correct Persional Id and try again.");
                        }
                        #endregion
                        RunQuery(options.Query, output);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Checks the validity of the options provided, then prints any errors
        /// </summary>
        /// <param name="o"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static bool IsValid(object o, Output output)
        {
            var context = new ValidationContext(o, null, null);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(o, context, results);
            if (!isValid)
            {
                foreach (var result in results)
                {
                    output.WriteOutput(result.ErrorMessage);
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Run the query, then print the results
        /// </summary>
        /// <param name="query"></param>
        /// <param name="output"></param>
        private static void RunQuery(String query, Output output)
        {
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                try
                {
                    var dt = connection.Execute<DataTable>(query);
                    output.PrintResults(dt);
                }
                catch (Exception e)
                {
                    output.WriteError(e);
                }
            }
        }
    }
}
