﻿using System;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;

namespace IO.Practiceware.SqlCmd
{
    public class Output : IDisposable
    {
        private readonly StreamWriter _streamWriter;

        private string Extension { get; set; }

        public bool TurnOffDelimiter { get; set; }

        public String Delimiter { get; set; }

        public bool IncludeHeader { get; set; }

        public int FixedColumnLength { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputFile"></param>
        /// <param name="includeHeader"></param>
        /// <param name="delimiter"></param>
        /// <param name="fixedColumnLength"></param>
        /// <param name="turnOffDelimiter"></param>
        public Output(string outputFile, bool includeHeader, String delimiter = null, bool turnOffDelimiter = false, int fixedColumnLength = 0)
        {
            if (!string.IsNullOrEmpty(outputFile))
            {
                Extension = Path.GetExtension(outputFile);
                _streamWriter = new StreamWriter(outputFile);
            }
            IncludeHeader = includeHeader;
            Delimiter = delimiter;
            FixedColumnLength = fixedColumnLength;
            TurnOffDelimiter = turnOffDelimiter;
        }

        /// <summary>
        /// Writes whatever output
        /// </summary>
        /// <param name="output"></param>
        public void WriteOutput(string output)
        {
            if (_streamWriter != null)
            {
                _streamWriter.WriteLine(output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }

        /// <summary>
        /// Prints the results in the data table
        /// </summary>
        /// <param name="dt"></param>
        public void PrintResults(DataTable dt)
        {
            var line = new StringBuilder();
            int columnCount = dt.Columns.OfType<DataColumn>().Count();
            //Auto calculate Column lengths if not specified the FixedColumnLength
            var maxLengths = new int[columnCount];
            if (FixedColumnLength == 0)
            {
                // loop to get max length of each column to fix it for all rows of that column
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!row.IsNull(i))
                        {
                            int length = row[i].ToString().Length;
                            if (length > maxLengths[i])
                            {
                                maxLengths[i] = length;
                            }
                        }
                    }
                }
            }

            if (IncludeHeader)
            {
                // Print the column names
                int index = 0;
                foreach (var column in dt.Columns.OfType<DataColumn>())
                {
                    string columnName = column.ColumnName;
                    if (FixedColumnLength > 0)
                    {
                        if (columnName.Length > FixedColumnLength) columnName = columnName.Substring(0, FixedColumnLength);
                        if (index < columnCount - 1)
                        {
                            columnName = String.Concat(columnName, Delimiter);
                            line.Append(columnName.PadRight(FixedColumnLength + (!TurnOffDelimiter ? Delimiter.Length : 0)));
                        }
                        else
                        {
                            line.Append(columnName);
                        }
                    }
                    else
                    {
                            columnName = String.Concat(columnName, Delimiter);
                            line.Append(columnName);
                    }
                    index++;
                }
                line.Append(Environment.NewLine);
            }
            // print each row
            foreach (var row in dt.Rows.OfType<DataRow>())
            {
                for (int i = 0; i < columnCount; i++)
                {
                    string rowValue = row[i].ToString();
                    // Print each column of the row
                    if (!string.IsNullOrEmpty(Extension))
                    {
                        // if the column uses the delimiter or has double quotes in it, then provide surrounding quotes so the value appears as in the database
                        if ((!string.IsNullOrEmpty(Delimiter) && rowValue.Contains(Delimiter)) || rowValue.Contains("\"") || rowValue.Contains(Environment.NewLine))
                        {
                            var rowVal = rowValue.Replace("\"", "\"\"");
                            rowValue = string.Concat("\"", rowVal, "\"");
                        }
                    }
                    if (FixedColumnLength > 0)
                    {
                        if (rowValue.Length > FixedColumnLength) rowValue = rowValue.Substring(0, FixedColumnLength);
                        if (i < columnCount - 1)
                        {
                            rowValue = string.Concat(rowValue, Delimiter);
                            line.Append(rowValue.PadRight(FixedColumnLength + (!TurnOffDelimiter ? Delimiter.Length : 0)));
                        }
                        else
                        {
                            line.Append(rowValue);
                        }
                    }
                    else
                    {
                        if (i < columnCount - 1)
                        {
                            rowValue = string.Concat(rowValue, Delimiter);
                        }
                            line.Append(rowValue);
                    }
                }
                line.Append(Environment.NewLine);
            }
            // send the output to the console or the specified file
            WriteOutput(line.ToString());
        }

        /// <summary>
        /// Writes a new line to the output
        /// </summary>
        public void WriteOutput()
        {
            if (_streamWriter != null)
            {
                _streamWriter.WriteLine();
            }
            else
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Writes the provided error to the output
        /// </summary>
        /// <param name="e"></param>
        internal void WriteError(Exception e)
        {
            if (_streamWriter != null)
            {
                _streamWriter.WriteLine(e);
            }
            else
            {
                Console.WriteLine(e);
            }
        }

        public void Dispose()
        {
            if (_streamWriter != null)
            {
                _streamWriter.Dispose();
            }
        }
    }
}
