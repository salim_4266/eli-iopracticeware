﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using CommandLine;
using CommandLine.Text;

namespace IO.Practiceware.SqlCmd
{
    public class Options : IValidatableObject
    {
        [Option('p', "Pin", HelpText = "Pin Used to Login, is always a 4 digit number", Required=true)]
        public String Pin { get; set; }

        [Option('q', "Query", HelpText = "Query to be run", Required = true)]
        public String Query { get; set; }

        [Option('o', "Output", HelpText = "File where you want the output from the query to be saved")]
        public String Output { get; set; }

        [Option('d', "Delimiter", HelpText = "Delimiter you want used to separate columns in a csv file")]
        public String Delimiter { get; set; }

        [Option('t', "TurnOffDelimiter", HelpText = "Turns off delimiting")]
        public bool TurnOffDelimiterArgument { get; set; }

        [Option('h', "IncludeHeader", DefaultValue = "true", HelpText = "To include column headers")]
        public string IncludeHeaderArgument { get; set; }

        [Option('l', "ColumnLength", DefaultValue = "0", HelpText = "Fixed column length")]
        public string ColumnLength { get; set; }

        public bool IncludeHeader
        {
            get
            {
                bool result;
                if (bool.TryParse(IncludeHeaderArgument, out result))
                {
                    return result;
                }
                return true;
            }
        }

        public int FixedColumnLength
        {
            get
            {
                int result;
                if (int.TryParse(ColumnLength, out result))
                {
                    return result;
                }
                return 0;
            }
        }
        /// <summary>
        /// Get Usage prints out all of the help text for each of the options to display to the user
        /// </summary>
        /// <returns></returns>
        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              current => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        /// <summary>
        /// Validate checks that all the required fields are accounted for, and that the Pin number has 4 digits
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ex = new Regex("^\\d{4}");
            var results = new List<ValidationResult>();
            if (string.IsNullOrEmpty(Pin) || Pin.Length < 4 || Pin.Length > 4 || !ex.IsMatch(Pin))
            {
                results.Add(new ValidationResult("Your login failed. Please make sure you've entered the correct Persional Id and try again. For more information run the script with the -help option."));
            }
            if (string.IsNullOrEmpty(Query))
            {
                results.Add(new ValidationResult("There must be a query included in your parameters. For more information run the script with the -help option."));
            }
            return results;
        }
    }
}
