﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IO.Practiceware.Integration.MVE.Model;
using Soaf.ComponentModel;
using Soaf.Domain;
using System.Linq;

[assembly: Component(typeof(MVERepository), typeof(IMVERepository))]

namespace IO.Practiceware.Integration.MVE.Model
{
    [Repository(Name = "MVERepository")]
    public partial interface IMVERepository 
    {
      
        #region IQueryable Properties
    
        IQueryable<contact> contacts { get; }    
    
        IQueryable<insurance> insurances { get; }    
    
        IQueryable<insurance_plan> insurance_plan { get; }    
    
        IQueryable<patient_insurance> patient_insurance { get; }    
    
        IQueryable<prescription> prescriptions { get; }    
    
        IQueryable<domain> domains { get; }    
    
        IQueryable<domaintype> domaintypes { get; }    
    
        IQueryable<history> histories { get; }    

        #endregion
    
        #region Save
    
        void Save(contact value);
    
        void Save(insurance value);
    
        void Save(insurance_plan value);
    
        void Save(patient_insurance value);
    
        void Save(prescription value);
    
        void Save(domain value);
    
        void Save(domaintype value);
    
        void Save(history value);

        #endregion
    
        #region Delete
    
        void Delete(contact value);
    
        void Delete(insurance value);
    
        void Delete(insurance_plan value);
    
        void Delete(patient_insurance value);
    
        void Delete(prescription value);
    
        void Delete(domain value);
    
        void Delete(domaintype value);
    
        void Delete(history value);

        #endregion
    	
        #region Create Objects
    
        contact Createcontact();
    
        insurance Createinsurance();
    
        insurance_plan Createinsurance_plan();
    
        patient_insurance Createpatient_insurance();
    
        prescription Createprescription();
    
        domain Createdomain();
    
        domaintype Createdomaintype();
    
        history Createhistory();

        #endregion
    
    
    }
    
    public abstract partial class MVERepository : IMVERepository
    {
     
        #region IQueryable Properties
    
        public abstract IQueryable<contact> contacts { get; }    
    
        public abstract IQueryable<insurance> insurances { get; }    
    
        public abstract IQueryable<insurance_plan> insurance_plan { get; }    
    
        public abstract IQueryable<patient_insurance> patient_insurance { get; }    
    
        public abstract IQueryable<prescription> prescriptions { get; }    
    
        public abstract IQueryable<domain> domains { get; }    
    
        public abstract IQueryable<domaintype> domaintypes { get; }    
    
        public abstract IQueryable<history> histories { get; }    

        #endregion
    
        #region Save
    
        public abstract void Save(contact value);
    
        public abstract void Save(insurance value);
    
        public abstract void Save(insurance_plan value);
    
        public abstract void Save(patient_insurance value);
    
        public abstract void Save(prescription value);
    
        public abstract void Save(domain value);
    
        public abstract void Save(domaintype value);
    
        public abstract void Save(history value);

        #endregion
    
        #region Delete
    
        public abstract void Delete(contact value);
    
        public abstract void Delete(insurance value);
    
        public abstract void Delete(insurance_plan value);
    
        public abstract void Delete(patient_insurance value);
    
        public abstract void Delete(prescription value);
    
        public abstract void Delete(domain value);
    
        public abstract void Delete(domaintype value);
    
        public abstract void Delete(history value);

        #endregion
    
        #region Create Objects
    
        public abstract contact Createcontact();
    
        public abstract insurance Createinsurance();
    
        public abstract insurance_plan Createinsurance_plan();
    
        public abstract patient_insurance Createpatient_insurance();
    
        public abstract prescription Createprescription();
    
        public abstract domain Createdomain();
    
        public abstract domaintype Createdomaintype();
    
        public abstract history Createhistory();

        #endregion
    
    	
    }
}

