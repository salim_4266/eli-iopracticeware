﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Integration.HL7;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Integration.MVE.HL7;
using IO.Practiceware.Integration.MVE.Model;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Base.Model;
using NHapi.Model.V231.Datatype;
using NHapi.Model.V231.Group;
using NHapi.Model.V231.Message;
using NHapi.Model.V231.Segment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Transactions;
using IMessage = IO.Practiceware.Integration.Messaging.IMessage;

[assembly: Component(typeof(MVEMessageHandler.HandleMessageSubscriber), Initialize = true)]

namespace IO.Practiceware.Integration.MVE.HL7
{
    [SupportsUnitOfWork]
    public class MVEMessageHandler
    {
        public class HandleMessageSubscriber
        {
            public HandleMessageSubscriber(IMessenger messenger, Func<MVEMessageHandler> handler)
            {
                messenger.Subscribe<HandleMessageRequest>(
                    (s, t, m) => handler().HandleMessage(m.Message.CastTo<HL7Message>()), 
                    m => handler().HandlesMessage(m.Message));
            }
        }

        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<IMVERepository> _mveRepository;
        private readonly Lazy<IPracticeRepository> _practiceRepository;

        public MVEMessageHandler(Func<IPracticeRepository> practiceRepository, 
            Func<IExternalSystemService> externalSystemService, 
            Func<IMVERepository> mveRepository)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
            _mveRepository = Lazy.For(mveRepository);
        }

        protected void HandleMessage(HL7Message message)
        {
            if (message.Message.Is<ORM_O01>())
            {
                HandleMessage((ORM_O01)message.Message);
            }
        }

        [UnitOfWork(AcceptChanges = true, AutoAcceptChanges = true)]
        protected virtual void HandleMessage(ORM_O01 message)
        {
            contact contact = HandleContact(message);

            HandleInsurance(message, contact);

            HandleOrders(message, contact);

            var history = new history
                              {
                                  history_date = DateTime.Today,
                                  table_type = "Patient",
                                  table_key = contact.contactid
                              };

            // suppress to prevent escalation to DTC if MVE is on a different server
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _mveRepository.Value.Save(history);
                ts.Complete();
            }
        }

        /// <summary>
        ///   Handles the orders in the message.
        /// </summary>
        /// <param name="message"> The message. </param>
        /// <param name="contact"> The contact. </param>
        private void HandleOrders(ORM_O01 message, contact contact)
        {
            for (int index = 0; index < message.ORDERRepetitionsUsed; index++)
            {
                ORM_O01_ORDER order = message.GetORDER(index);
                HandleOrder(contact, order);
            }
            // suppress to prevent escalation to DTC if MVE is on a different server
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _mveRepository.Value.Save(contact);
                ts.Complete();
            }
        }

        /// <summary>
        ///   Handles the order. Creates a prescription in MVE for each order and adds it to the contact. Prescriptions are never mapped since they should never be sent over twice.
        /// </summary>
        /// <param name="contact"> The contact. </param>
        /// <param name="order"> The order. </param>
        private void HandleOrder(contact contact, ORM_O01_ORDER order)
        {
            var prescription = new prescription { refills = 0, duplication = "No", odbalance = "No", osbalance = "No", system_category = "Ocular", released_flag = "No", locked_flag = "No", lastupdateddt = DateTime.Now };
            contact.prescriptions.Add(prescription);
            prescription.rxdate = order.ORC.DateTimeOfTransaction.TimeOfAnEvent.GetAsDate();
            prescription.expirydate = prescription.rxdate.Value.AddYears(1);

            // find mapped provider or create a new one
            int userId;
            if (int.TryParse(order.ORC.GetOrderingProvider(0).IDNumber.Value, out userId))
            {
                ExternalSystemEntityMapping physicianMapping = _externalSystemService.Value.GetMappings<User>(userId.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Physician");
                if (physicianMapping != null)
                {
                    prescription.physicianid = physicianMapping.ExternalSystemEntityKey.ToInt();
                }
            }

            // find mapped office or create a new one
            int locationId;
            if (int.TryParse(order.ORC.EntererSLocation.Facility.NamespaceID.Value, out locationId))
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMappings<ServiceLocation>(locationId.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Location");
                if (locationMapping != null)
                {
                    prescription.locationid = locationMapping.ExternalSystemEntityKey.ToInt();
                }
            }

            // each prescription may have a ZOD and ZOS segment to describe the prescription details in IO
            if (order.ORDER_DETAIL.GetOBSERVATION().Names.Contains("ZOD") && order.ORDER_DETAIL.GetOBSERVATION().Names.Contains("ZOS"))
            {
                var zod = (GenericSegment)order.ORDER_DETAIL.GetOBSERVATION().GetStructure("ZOD");
                prescription.odaxis = zod.GetFieldPrimitive(4).IfNotNull(i => i.Replace("X", string.Empty).ToDecimal());
                prescription.odadd = zod.GetFieldPrimitive(5).ToDecimal();
                prescription.od_sphere = zod.GetFieldPrimitive(2);
                prescription.od_cylinder = zod.GetFieldPrimitive(3);
                var zos = (GenericSegment)order.ORDER_DETAIL.GetOBSERVATION().GetStructure("ZOS");
                prescription.osaxis = zos.GetFieldPrimitive(4).IfNotNull(i => i.Replace("X", string.Empty).ToDecimal());
                prescription.osadd = zos.GetFieldPrimitive(5).ToDecimal();
                prescription.os_sphere = zos.GetFieldPrimitive(2);
                prescription.os_cylinder = zos.GetFieldPrimitive(3);

                if (order.ORDER_DETAIL.RQD.ItemCodeInternal.Text.Value == PatientClinical.SpectaclePrescriptionIndicator)
                {
                    HandleSpectaclePrescription(order, zos, zod, order.ORDER_DETAIL.RQD, prescription);
                }
                else
                {
                    HandleContactLensesPrescription(zos, zod, order.ORDER_DETAIL.RQD, prescription);
                }
            }
        }

        /// <summary>
        ///   Handles a contact lenses prescription ZOD/ZOS segment.
        /// </summary>
        /// <param name="zos"> The zos. </param>
        /// <param name="zod"> The zod. </param>
        /// <param name="rqd"> The RQD. </param>
        /// <param name="prescription"> The prescription. </param>
        private static void HandleContactLensesPrescription(GenericSegment zos, GenericSegment zod, RQD rqd, prescription prescription)
        {
            if (rqd.ItemCodeInternal.AlternateText.Value == "SOFT")
            {
                prescription.prescriptiontypeid = 953;
                prescription.prescription_type = "Soft Contact Lens";
            }
            else
            {
                prescription.prescriptiontypeid = 950;
                prescription.prescription_type = "Hard Contact Lens";
            }

            prescription.clensodbasecrv1 = zod.GetFieldPrimitive(6).ToDecimal();
            prescription.clensoddiam1 = zod.GetFieldPrimitive(7).ToDecimal();
            prescription.odpcr2 = zod.GetFieldPrimitive(8);
            prescription.clensosbasecrv1 = zos.GetFieldPrimitive(6).ToDecimal();
            prescription.clensosdiam1 = zos.GetFieldPrimitive(7).ToDecimal();
            prescription.ospcr2 = zos.GetFieldPrimitive(8);

            var comments = new List<string>();
            if (zod.GetFieldPrimitive(10).IsNotNullOrEmpty())
            {
                comments.Add("OD Type: {0}".FormatWith(zod.GetFieldPrimitive(10)));
            }
            if (zos.GetFieldPrimitive(10).IsNotNullOrEmpty())
            {
                comments.Add("OS Type: {0}".FormatWith(zos.GetFieldPrimitive(10)));
            }
            if (zod.GetFieldPrimitive(11).IsNotNullOrEmpty())
            {
                comments.Add("OD: {0}".FormatWith(zod.GetFieldPrimitive(11)));
            }
            if (zos.GetFieldPrimitive(11).IsNotNullOrEmpty())
            {
                comments.Add("OS: {0}".FormatWith(zos.GetFieldPrimitive(11)));
            }
            prescription.comments = comments.Join();

            if (zod.GetFieldPrimitive(9) == "D" && new[] { "N", string.Empty }.Contains(zos.GetFieldPrimitive(9) ?? string.Empty))
            {
                prescription.dominant = "OD";
            }
            else if (new[] { "N", string.Empty }.Contains(zod.GetFieldPrimitive(9) ?? string.Empty) && zos.GetFieldPrimitive(9) == "D")
            {
                prescription.dominant = "OS";
            }
            else if (zod.GetFieldPrimitive(9) == "D" && zos.GetFieldPrimitive(9) == "D")
            {
                prescription.dominant = "OU";
            }
        }

        /// <summary>
        ///   Handles the poa. Does some nasty parsing due to the structure of the data coming from IO.
        /// </summary>
        /// <param name="prescription"> The prescription. </param>
        /// <param name="text"> The text. </param>
        /// <param name="isOD"> if set to <c>true</c> [is OD]. </param>
        /// <returns> </returns>
        private static bool HandlePoa(prescription prescription, string text, bool isOD)
        {
            if (text.IsNullOrEmpty())
            {
                return false;
            }
            decimal? value = text.Split(' ')[0].ToDecimal();

            decimal? direction = null;

            if (text.Contains("BI"))
            {
                direction = (decimal)990.00;
            }
            else if (text.Contains("BO"))
            {
                direction = (decimal)992.00;
            }

            if (direction.HasValue)
            {
                if (isOD)
                {
                    prescription.odprism = value;
                    prescription.odbase = direction;
                }
                else
                {
                    prescription.osprism = value;
                    prescription.osbase = direction;
                }

                return true;
            }

            string udText = null;

            if (text.Contains("BU"))
            {
                udText = "Up";
            }
            else if (text.Contains("BD"))
            {
                udText = "Down";
            }

            if (udText != null)
            {
                if (isOD)
                {
                    prescription.odprismud = value;
                    prescription.odbaseudtext = udText;
                }
                else
                {
                    prescription.osprismud = value;
                    prescription.osbaseudtext = udText;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///   Handles the poa. Does some nasty parsing due to the structure of the data coming from IO.
        /// </summary>
        /// <param name="prescription"> The prescription. </param>
        /// <param name="poa1"> The poa1. </param>
        /// <param name="poa2"> The poa2. </param>
        /// <param name="isOD"> if set to <c>true</c> [is OD]. </param>
        private static void HandlePoa(prescription prescription, string poa1, string poa2, bool isOD)
        {
            bool setPrism1 = HandlePoa(prescription, poa1, isOD);
            bool setPrism2 = HandlePoa(prescription, poa2, isOD);

            if (!setPrism1 && !poa1.IsNullOrEmpty())
            {
                HandleUnassignedPoa(prescription, poa1.Split(' ')[0].ToDecimal(), isOD);
            }
            if (!setPrism2 && !poa2.IsNullOrEmpty())
            {
                HandleUnassignedPoa(prescription, poa2.Split(' ')[0].ToDecimal(), isOD);
            }
        }

        private static void HandleUnassignedPoa(prescription prescription, decimal? value, bool isOD)
        {
            if (isOD)
            {
                if (prescription.odprism == null && prescription.odbase == null)
                {
                    prescription.odprism = value;
                }
                else
                {
                    prescription.odprismud = value;
                }
            }
            else
            {
                if (prescription.osprism == null && prescription.osbase == null)
                {
                    prescription.osprism = value;
                }
                else
                {
                    prescription.osprismud = value;
                }
            }
        }

        /// <summary>
        ///   Handles a spectacle lenses prescription.
        /// </summary>
        /// <param name="order"> The order. </param>
        /// <param name="zos"> The zos. </param>
        /// <param name="zod"> The zod. </param>
        /// <param name="rqd"> The RQD. </param>
        /// <param name="prescription"> The prescription. </param>
        private void HandleSpectaclePrescription(ORM_O01_ORDER order, GenericSegment zos, GenericSegment zod, RQD rqd, prescription prescription)
        {
            prescription.prescriptiontypeid = 1026;
            prescription.prescription_type = "Spectacle Lens";

            string odPrism1 = zod.GetFieldPrimitive(7);
            string odPrism2 = zod.GetFieldPrimitive(8);

            HandlePoa(prescription, odPrism1, odPrism2, true);

            string osPrism1 = zos.GetFieldPrimitive(7);
            string osPrism2 = zos.GetFieldPrimitive(8);

            HandlePoa(prescription, osPrism1, osPrism2, false);

            if (!rqd.ItemCodeInternal.AlternateText.Value.IsNullOrEmpty())
            {
                // suppress to prevent escalation to DTC if MVE is on a different server
                using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    int lensStyle = _mveRepository.Value.domains.Where(d => d.domaintype.domaintype1.ToLower() == "spectacle lens style" && d.domain1.ToLower() == rqd.ItemCodeInternal.AlternateText.Value.ToLower()).Select(i => i.domainid).FirstOrDefault();

                    if (lensStyle > 0)
                    {
                        prescription.speclensstyle = lensStyle;
                    }

                    ts.Complete();
                }
            }

            var comments = new List<string>();
            if (zod.GetFieldPrimitive(6).IsNotNullOrEmpty())
            {
                comments.Add("OD Intermediate Add: {0}".FormatWith(zod.GetFieldPrimitive(6)));
            }
            if (zos.GetFieldPrimitive(6).IsNotNullOrEmpty())
            {
                comments.Add("OS Intermediate Add: {0}".FormatWith(zos.GetFieldPrimitive(6)));
            }
            if (zod.GetFieldPrimitive(9).IsNotNullOrEmpty())
            {
                comments.Add("Vertex Dist: {0}".FormatWith(zod.GetFieldPrimitive(9)));
            }
            else if (zos.GetFieldPrimitive(9).IsNotNullOrEmpty())
            {
                comments.Add("Vertex Dist: {0}".FormatWith(zos.GetFieldPrimitive(9)));
            }
            if (order.ORDER_DETAIL.GetNTE(0).GetComment(0).Value.IsNotNullOrEmpty())
            {
                comments.Add(order.ORDER_DETAIL.GetNTE(0).GetComment(0).Value);
            }

            prescription.comments = comments.Join();
        }

        /// <summary>
        ///   Handles the insurance segment of the ORM message.
        /// </summary>
        /// <param name="message"> The message. </param>
        /// <param name="contact"> The contact. </param>
        private void HandleInsurance(ORM_O01 message, contact contact)
        {
            string insuranceSequence = "Primary";
            var insuranceMappings = new Dictionary<insurance, ExternalSystemEntityMapping>();
            var patientInsuranceMappings = new Dictionary<patient_insurance, ExternalSystemEntityMapping>();
            for (int index = 0; index < message.PATIENT.INSURANCERepetitionsUsed; index++)
            {
                ORM_O01_INSURANCE insuranceRepetition = message.PATIENT.GetINSURANCE(index);

                long insurancePlanId;
                if (insuranceRepetition.IN1.InsurancePlanID.Identifier.Value.IsNullOrEmpty() || !long.TryParse(insuranceRepetition.IN1.InsurancePlanID.Identifier.Value, out insurancePlanId))
                {
                    continue;
                }

                long[] patientInsuranceMappingIds = _externalSystemService.Value.GetMappings<PatientInsurance>(insurancePlanId.ToString(), Constants.MVEExternalSystemName).Where(i => i.ExternalSystemEntity.Name == "PatientInsurance").Select(i => i.ExternalSystemEntityKey.ConvertTo<long>()).ToArray();

                patient_insurance patientInsurance = null;

                if (patientInsuranceMappingIds.Length > 0)
                {
                    patientInsurance = contact.patient_insurance.FirstOrDefault(i => patientInsuranceMappingIds.Contains(i.patient_insurance_id));
                }
                if (patientInsurance == null)
                {
                    patientInsurance = new patient_insurance { entered_dt = DateTime.Now };
                    contact.patient_insurance.Add(patientInsurance);
                    var patientInsuranceMapping = new ExternalSystemEntityMapping
                                                      {
                                                          ExternalSystemEntityId = _practiceRepository.Value.ExternalSystemEntities.FirstOrDefault(i => i.ExternalSystem.Name == Constants.MVEExternalSystemName && i.Name == "PatientInsurance").EnsureNotDefault(new InvalidOperationException("Could not find PatientInsurance external system entity.")).Id,
                                                          PracticeRepositoryEntityId = PracticeRepositoryEntityId.PatientInsurance.CastTo<int>(),
                                                          PracticeRepositoryEntityKey = insurancePlanId.ToString()
                                                      };

                    patientInsuranceMappings[patientInsurance] = patientInsuranceMapping;
                }

                patientInsurance.insurance_type = "Vision";
                patientInsurance.status_flag = "Yes";
                patientInsurance.insurance_number = insuranceRepetition.IN1.PolicyNumber.Value;
                patientInsurance.policy_group = insuranceRepetition.IN1.GroupNumber.Value;

                var relationShipMap = Enum.GetValues(typeof(PolicyHolderRelationshipType))
                   .Cast<PolicyHolderRelationshipType>()
                   .ToDictionary( t => (int)t,  t => t.ToString());

                string relationship; 
                if (insuranceRepetition.IN1.InsuredSRelationshipToPatient.Identifier.Value == "Y")
                {
                    relationship = "Self";
                }
                else
                {
                    relationship = relationShipMap.Where(
                        i => i.Value.Substring(0,1) == insuranceRepetition.IN1.InsuredSRelationshipToPatient.Identifier.
                                            Value).Select(i => i.Value).FirstOrDefault();
                }

                if (insuranceRepetition.IN1.InsuredSRelationshipToPatient.Identifier.Value != null)
                {
                    bool isSelf = relationship == "Self";
                    const string self = "Self";
                    const string other = "Other";

                    if (relationship != null) patientInsurance.relation_to_insured = relationship;

                    patientInsurance.insured_party = isSelf ? self : other;
                    if (!isSelf)
                    {
                        patientInsurance.insured_lastname = insuranceRepetition.IN1.GetNameOfInsured(0).FamilyLastName.FamilyName.Value;
                        patientInsurance.insured_firstname = insuranceRepetition.IN1.GetNameOfInsured(0).GivenName.Value;
                        patientInsurance.insured_middlename = insuranceRepetition.IN1.GetNameOfInsured(0).MiddleInitialOrName.Value;
                        if (insuranceRepetition.IN1.InsuredSSex.Value == "M")
                        {
                            patientInsurance.insured_sex = "Male";
                        }
                        else if (insuranceRepetition.IN1.InsuredSSex.Value == "F")
                        {
                            patientInsurance.insured_sex = "Female";
                        }
                        patientInsurance.insured_address = insuranceRepetition.IN1.GetInsuredSAddress(0).StreetAddress.Value;
                        patientInsurance.insured_address_2 = insuranceRepetition.IN1.GetInsuredSAddress(0).OtherDesignation.Value;
                        patientInsurance.insured_city = insuranceRepetition.IN1.GetInsuredSAddress(0).City.Value;
                        patientInsurance.insured_state = insuranceRepetition.IN1.GetInsuredSAddress(0).StateOrProvince.Value;
                        patientInsurance.insured_zip = insuranceRepetition.IN1.GetInsuredSAddress(0).ZipOrPostalCode.Value;
                        patientInsurance.insured_birthdate = insuranceRepetition.IN1.InsuredSDateOfBirth.TimeOfAnEvent.GetAsDate();
                    }
                }

                patientInsurance.insurance_sequence = insuranceSequence;

                DateTime effectiveDate;
                if (insuranceRepetition.IN1.PlanEffectiveDate.Value != null && DateTime.TryParseExact(insuranceRepetition.IN1.PlanEffectiveDate.Value, "yyyyMMdd", null, DateTimeStyles.None, out effectiveDate))
                {
                    patientInsurance.effective_dt = effectiveDate;
                }

                if (insuranceSequence == "Primary")
                {
                    insuranceSequence = "Secondary";
                }
                else if (insuranceSequence == "Secondary")
                {
                    insuranceSequence = "Tertiary";
                }
                else if (insuranceSequence == "Tertiary")
                {
                    insuranceSequence = "Fourth";
                }

                int insurerId;
                if (insuranceRepetition.IN1.GetInsuranceCompanyID(0).ID.Value.IsNullOrEmpty() || !int.TryParse(insuranceRepetition.IN1.GetInsuranceCompanyID(0).ID.Value, out insurerId))
                {
                    continue;
                }

                ExternalSystemEntityMapping insuranceMapping = _externalSystemService.Value.GetMappings<Insurer>(insurerId.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Insurance");
                insurance insurance;
                if (insuranceMapping != null)
                {
                    // suppress to prevent escalation to DTC if MVE is on a different server
                    using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        insurance = _mveRepository.Value.insurances.FirstOrDefault(i => i.id == int.Parse(insuranceMapping.ExternalSystemEntityKey));
                        ts.Complete();
                    }
                    if (insurance == null)
                    {
                        throw new InvalidOperationException("Expected to find insurance {0}.".FormatWith(insuranceMapping.ExternalSystemEntityKey));
                    }
                }
                else
                {
                    insurance = new insurance { id = GetSequence("insurance"), hcfaprintname = "Yes", hcfa24kprint = "Yes", hcfa47ind = "N", hcfaindent = 0, active = "Yes" };
                    insuranceMapping = new ExternalSystemEntityMapping
                                           {
                                               ExternalSystemEntityId = _practiceRepository.Value.ExternalSystemEntities.First(i => i.ExternalSystem.Name == Constants.MVEExternalSystemName && i.Name == "Insurance").EnsureNotDefault(new InvalidOperationException("Could not find Insurance external system entity.")).Id,
                                               PracticeRepositoryEntityId = PracticeRepositoryEntityId.Insurer.CastTo<int>(),
                                               PracticeRepositoryEntityKey = insurerId.ToString()
                                           };

                    insuranceMappings[insurance] = insuranceMapping;
                }

                patientInsurance.insurance = insurance;

                insurance.insurancetypeid = insuranceRepetition.IN1.PlanType.Value.ToInt();
                insurance.companyname = insuranceRepetition.IN1.GetInsuranceCompanyName(0).OrganizationName.Value;
                insurance.submitto = insurance.companyname;
                insurance.addressline1 = insuranceRepetition.IN1.GetInsuranceCompanyAddress(0).StreetAddress.Value.Truncate(29);
                insurance.city = insuranceRepetition.IN1.GetInsuranceCompanyAddress(0).City.Value;
                insurance.stateorprovince = insuranceRepetition.IN1.GetInsuranceCompanyAddress(0).StateOrProvince.Value;
                insurance.zipcode = insuranceRepetition.IN1.GetInsuranceCompanyAddress(0).ZipOrPostalCode.Value;
                insurance.phone = insuranceRepetition.IN1.GetInsuranceCoPhoneNumber(0).Get9999999X99999CAnyText.Value;
            }
            // suppress to prevent escalation to DTC if MVE is on a different server
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _mveRepository.Value.Save(contact);
                ts.Complete();
            }
            foreach (KeyValuePair<insurance, ExternalSystemEntityMapping> insuranceMapping in insuranceMappings)
            {
                insuranceMapping.Value.ExternalSystemEntityKey = insuranceMapping.Key.id.ToString();
            }
            insuranceMappings.Values.ForEachWithSinglePropertyChangedNotification(_practiceRepository.Value.Save);

            foreach (KeyValuePair<patient_insurance, ExternalSystemEntityMapping> patientInsuranceMapping in patientInsuranceMappings)
            {
                patientInsuranceMapping.Value.ExternalSystemEntityKey = patientInsuranceMapping.Key.patient_insurance_id.ToString();
            }
            patientInsuranceMappings.Values.ForEachWithSinglePropertyChangedNotification(_practiceRepository.Value.Save);
        }

        /// <summary>
        ///   Handles the contact data in the ORM message.
        /// </summary>
        /// <param name="message"> The message. </param>
        /// <returns> </returns>
        private contact HandleContact(ORM_O01 message)
        {
            PID pid = message.PATIENT.PID;
            int patientId;
            if (pid.PatientID.ID.Value.IsNullOrEmpty() || !int.TryParse(pid.PatientID.ID.Value, out patientId))
            {
                throw new InvalidOperationException("Could not parse PatientId {0}.".FormatWith(pid.PatientID.ID.Value ?? "null"));
            }

            var contactMappings = _externalSystemService.Value.GetMappings<Patient>(patientId.ToString(), Constants.MVEExternalSystemName);
            var contactMapping = contactMappings.Where(i => i.PracticeRepositoryEntityKey== patientId.ToString()).FirstOrDefault();
            contact contact;
            if (contactMapping != null)
            {
                // suppress to prevent escalation to DTC if MVE is on a different server
                using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    contact = _mveRepository.Value.contacts.Include(c => c.patient_insurance.Select(i => i.insurance)).First(c => c.contactid == int.Parse(contactMapping.ExternalSystemEntityKey));
                    ts.Complete();
                }
                if (contact == null)
                {
                    throw new InvalidOperationException("Expected to find contact {0}.".FormatWith(contactMapping.ExternalSystemEntityKey));
                }
            }
            else
            {
                contact = new contact { contactid = GetSequence("contacts"), dateentered = DateTime.Now, patientactive = "Yes", bill_to_guarantor_flag = "No", head_household_flag = "No", collection_flag = "No", signature_on_file = "No", deceased_flag = "No", recallmonths = 0, badaddress = "No", discount_material_flag = "No", discount_services_flag = "No", accept_checks = "Yes", referral_sent_flag = "No", billing_address_same = "P" };
                contactMapping = new ExternalSystemEntityMapping
                                     {
                                         ExternalSystemEntityId = _practiceRepository.Value.ExternalSystemEntities.First(i => i.ExternalSystem.Name == Constants.MVEExternalSystemName && i.Name == "Contact").EnsureNotDefault(new InvalidOperationException("Could not find Contact external system entity.")).Id,
                                         PracticeRepositoryEntityId = PracticeRepositoryEntityId.Patient.CastTo<int>(),
                                         PracticeRepositoryEntityKey = patientId.ToString()
                                     };
            }
            XPN patientName = pid.GetPatientName(0);
            XAD patientAddress = pid.GetPatientAddress(0);
            contact.lastname = patientName.FamilyLastName.FamilyName.Value;
            contact.firstname = patientName.GivenName.Value;
            contact.middlename = patientName.MiddleInitialOrName.Value.Truncate(1);
            contact.addressline1 = patientAddress.StreetAddress.Value;
            contact.addressline2 = patientAddress.OtherDesignation.Value;
            contact.city = patientAddress.City.Value;
            contact.stateorprovince = patientAddress.StateOrProvince.Value;
            contact.zipcode = patientAddress.ZipOrPostalCode.Value;
            contact.workphone = pid.GetPhoneNumberBusiness(0).Get9999999X99999CAnyText.Value;
            contact.homephone = pid.GetPhoneNumberHome(0).Get9999999X99999CAnyText.Value;
            if (pid.GetPhoneNumberHome(0).ExtraComponents != null && pid.GetPhoneNumberHome(0).ExtraComponents.numComponents() > 2)
            {
                string emailAddress = pid.GetPhoneNumberHome(0).ExtraComponents.getComponent(2).Data.ToString();
                if (emailAddress.IsNotNullOrEmpty())
                {
                    contact.emailname = emailAddress;
                }
            }
            contact.birthdate = pid.DateTimeOfBirth.TimeOfAnEvent.GetAsDate();
            if (pid.Sex.Value == "M")
            {
                contact.sex = "Male";
            }
            else if (pid.Sex.Value == "F")
            {
                contact.sex = "Female";
            }
            contact.patientmaritalstatusid = pid.MaritalStatus.Identifier.Value.ToInt();
            contact.languageid = pid.PrimaryLanguage.Identifier.Value.ToInt();
            contact.reference_num = pid.PatientID.ID.Value;
            contact.lastupdateddt = DateTime.Now;
            contact.sendmail = "Yes";
            contact.calendarpatient = "Yes";
            contact.badaddress = "No";
            contact.guarantor_party = "Other";
            contact.deceased_flag = "No";
            contact.signature_on_file = "No";
            contact.head_household_flag = "No";
            contact.collection_flag = "No";
            contact.referral_type = "Other";
            contact.referral_sent_flag = "No";
            contact.accept_checks = "Yes";
            contact.discount_material_flag = "No";
            contact.discount_services_flag = "No";
            int locationId;
            if (int.TryParse(message.GetORDER(0).ORC.EntererSLocation.Facility.NamespaceID.Value, out locationId))
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMappings<ServiceLocation>(locationId.ToString(), Constants.MVEExternalSystemName).FirstOrDefault(i => i.ExternalSystemEntity.Name == "Location");
                if (locationMapping != null)
                {
                    contact.locationid = locationMapping.ExternalSystemEntityKey.ToInt();
                }
            }

            // suppress to prevent escalation to DTC if MVE is on a different server
            using (var ts = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _mveRepository.Value.Save(contact);
                ts.Complete();
            }

            contactMapping.ExternalSystemEntityKey = contact.contactid.ToString();
            _practiceRepository.Value.Save(contactMapping);

            return contact;
        }

        protected bool HandlesMessage(HL7Message message)
        {
            return message.Message.Is<ORM_O01>();
        }

        /// <summary>
        ///   Calls a SP in MVE to get the current PK id to use for certain entities because tables don't have identity columns.
        /// </summary>
        /// <param name="sequence"> The sequence. </param>
        /// <returns> </returns>
        private static int GetSequence(string sequence)
        {
            string connectionString = ConfigurationManager.Configuration.ConnectionStrings.ConnectionStrings["MVERepository"].IfNotNull(i => i.ConnectionString);
            if (connectionString == null)
            {
                throw new InvalidOperationException("Could not find MVE connection string.");
            }

            try
            {

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "sp_get_sequence";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@sequence", sequence));
                        var sequenceId = new SqlParameter("@sequence_id", SqlDbType.Int) { Direction = ParameterDirection.Output };
                        command.Parameters.Add(sequenceId);
                        command.ExecuteNonQuery();

                        return (int)sequenceId.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error getting MVE sequence id. Connection string: {0}.".FormatWith(connectionString), ex);
            }
        }

        private bool HandlesMessage(IMessage message)
        {
            return message is HL7Message && HandlesMessage(message.CastTo<HL7Message>());
        }
    }
}