IF OBJECT_ID('model.GetUnbilledAmount') IS NOT NULL
	DROP PROCEDURE model.GetUnbilledAmount
GO

CREATE PROCEDURE model.GetUnbilledAmount (@StartDate DATETIME,@EndDate DATETIME)
AS
BEGIN

SELECT  OpenBalance, Invoice, UnbilledAmount, PatientId, FirstName, LastName, DateofService
FROM (SELECT 
	 isnull((
			(bs.Unit * bs.UnitCharge) - isnull((	
				SELECT SUM(CASE adjtype.IsDebit WHEN 1 THEN adj.Amount * - 1 else adj.Amount END) AS AdjAmount
				FROM model.Adjustments adj
				Inner Join model.AdjustmentTypes adjtype ON adj.AdjustmentTypeId = adjtype.Id
				WHERE adj.BillingServiceId = bs.Id 
				),0)
	),0) AS [OpenBalance]
	,isnull((
			(bs.Unit * bs.UnitCharge) - isnull((	
				SELECT SUM(CASE adjtype.IsDebit WHEN 1 THEN adj.Amount * - 1 else adj.Amount END) AS AdjAmount
				FROM model.Adjustments adj
				Inner Join model.AdjustmentTypes adjtype ON adj.AdjustmentTypeId = adjtype.Id
				WHERE adj.BillingServiceId = bs.Id 
				),0) -
				isnull((	
				SELECT SUM(bst.AmountSent) AS bstAmount
				FROM model.BillingServiceTransactions bst
				WHERE bst.BillingServiceId = bs.Id AND bst.BillingServiceTransactionStatusId in (1,2,3,4)
				),0) 
	),0) AS [UnbilledAmount]
	,(convert(varchar(20),pat.Id) +'-'+ convert(varchar(20),enc.Id)) [Invoice]
	,pat.Id as PatientId  
	,pat.LastName 
	,pat.FirstName
	,CONVERT(NVARCHAR(10),enc.StartDateTime,101) as DateOfService
	,bs.Id as BillingServiceId
	FROM model.Patients pat
	JOIN model.Encounters enc ON pat.Id = enc.PatientId
	left outer JOIN model.Appointments appt ON enc.Id = appt.EncounterId
	JOIN model.Invoices inv ON enc.Id = inv.EncounterId
	JOIN model.BillingServices bs ON inv.Id = bs.InvoiceId
	JOIN model.EncounterServices es ON bs.EncounterServiceId = es.Id
	Left outer join model.BillingServiceTransactions bst ON bs.Id = bst.BillingServiceId
	WHERE
    (((bst.BillingServiceTransactionStatusId IS NULL)) or ((bst.BillingServiceTransactionStatusId IS NOT NULL) 
     and (bst.BillingServiceTransactionStatusId not in (1,2,3,4)))) AND 
	pat.LastName <> 'TEST'
	AND enc.StartDateTime BETWEEN @StartDate AND @EndDate
	) v
WHERE v.unbilledamount > 0 
GROUP BY OpenBalance , UnbilledAmount,Invoice,PatientId, FirstName,LastName,DateofService,BillingServiceId
ORDER BY PatientId, LastName, FirstName

END	
GO

-- exec model.GetUnbilledAmount '2013-01-01 00:00:00','2015-01-01 00:00:00'