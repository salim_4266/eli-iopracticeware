IF OBJECT_ID('model.GetDailyAdjustments') IS NOT NULL
	DROP PROCEDURE model.GetDailyAdjustments

GO

CREATE PROCEDURE [model].[GetDailyAdjustments](@StartDate datetime, 
										@EndDate datetime, 
										@BillingOrganizations nvarchar(max), 
										@Providers nvarchar(max), 
										@ServiceLocations nvarchar(max), 
										@Users nvarchar(max), 
										@PayerTypes nvarchar(max),
										@ShowBillingOrganizationBreakdown bit,
										@ShowProviderBreakdown bit,
										@ShowServiceLocationBreakdown bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

CREATE TABLE #Results(AdjustmentId int, AdjustmentType nvarchar(255), AdjustmentName nvarchar(255), PayerType nvarchar(20),  Amount decimal(36,2), PatientId int, InvoiceReceivableId bigint, DateTime datetime)

INSERT #Results
SELECT a.Id
	,at.ShortName
	,at.Name
	,(CASE 
	WHEN fb.FinancialSourceTypeId = 1
		THEN 'Insurer'
	WHEN fb.FinancialSourceTypeId = 2
		THEN 'Patient'
	ELSE 'Office'
	END) AS PayerType
	,CASE WHEN at.IsDebit = 1 THEN - a.Amount ELSE a.Amount END AS Amount
	,en.PatientID
	,a.InvoiceReceivableId
	,a.PostedDateTime
FROM model.Adjustments a
INNER MERGE JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
INNER JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
INNER JOIN model.InvoiceReceivables ir ON a.InvoiceReceivableId = ir.Id
INNER JOIN model.Invoices inv ON inv.Id = ir.InvoiceId
INNER JOIN model.Encounters en ON en.ID = inv.EncounterId
WHERE at.IsCash = 0
AND CONVERT(DATETIME,CONVERT(NVARCHAR(10),a.PostedDateTime,101)) BETWEEN @StartDate AND @EndDate
AND ((@PayerTypes IS NULL AND fb.FinancialSourceTypeId IN (1,2,3,4))
	 OR (@PayerTypes IS NOT NULL AND fb.FinancialSourceTypeId IN (SELECT CONVERT(int,nstr) FROM CharTable(@PayerTypes,','))))

CREATE INDEX IX_Results_PatientId ON #Results(PatientId)
CREATE INDEX IX_Results_AdjustmentId ON #Results(AdjustmentId)

CREATE TABLE #AuditUser(AdjustmentId int, UserId int, UserName nvarchar(255), ChangeTypeId int)

INSERT #AuditUser
SELECT a.Id, ae.UserId,u.UserName, MAX(ChangeTypeId) AS ChangeTypeId
FROM model.Adjustments a 
LEFT JOIN AuditEntries ae ON ae.KeyValueNumeric = a.Id
LEFT JOIN model.Users u ON u.Id = ae.UserId
WHERE ObjectName = 'model.Adjustments'
GROUP BY a.Id, ae.UserId, u.UserName


SELECT DISTINCT 
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN COALESCE(bo.ShortName,'No Billing Organization') ELSE NULL END AS BillingOrganizationShortName,
	CASE WHEN @ShowProviderBreakdown = 1 THEN COALESCE(u.UserName,'No Provider') ELSE NULL END AS Provider,
	CASE WHEN @ShowServiceLocationBreakdown = 1 THEN COALESCE(sl.ShortName,'No Service Location') ELSE NULL END AS ServiceLocation,
	r.AdjustmentId, r.PayerType, r.AdjustmentType, r.AdjustmentName, Amount, r.PatientId AS PatientId
	,p.LastName + ', ' + p.FirstName AS PatientName
	,COALESCE(au.UserName,'UNKNOWN') AS [User]
	,r.Datetime AS PostedDateTime
	,CONVERT(NVARCHAR(10), r.Datetime, 101) AS [Date]
 FROM #Results r
	INNER JOIN model.Patients p ON p.Id = r.PatientId
	LEFT JOIN model.InvoiceReceivables ir ON ir.Id = r.InvoiceReceivableId
	LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId
	LEFT JOIN model.ServiceLocations sl ON sl.Id = i.AttributeToServiceLocationId
	LEFT MERGE JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	LEFT MERGE JOIN model.BillingOrganizations bo ON bo.Id = pr.BillingOrganizationId
	LEFT MERGE JOIN model.Users u ON u.Id = pr.UserId
	LEFT JOIN #AuditUser au ON r.AdjustmentId = au.AdjustmentId
	WHERE 
	 ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations)))
		OR (@BillingOrganizations IS NOT NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))))
	AND ((@Providers IS NULL AND (u.Id IS NULL OR u.Id IN (SELECT Id FROM model.Users)))
		OR (@Providers IS NOT NULL AND (u.Id IS NULL OR u.ID IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,',')))))
	AND ((@ServiceLocations IS NULL AND sl.Id IN (SELECT Id FROM model.ServiceLocations))
		OR (@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Users IS NULL and (au.UserId IS NULL OR au.UserId IN (SELECT UserId FROM #AuditUser)))
		OR (@Users IS NOT NULL AND (au.UserId IS NULL OR au.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,',')))))
	ORDER BY PayerType

END
RETURN
END

GO
/*
exec model.GetDailyAdjustments '2013-02-01'
									,'2013-02-05'
									,NULL
									,NULL
									,NULL
									,NULL
									,NULL,0,0,0
									*/

