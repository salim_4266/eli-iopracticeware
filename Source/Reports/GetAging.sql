IF OBJECT_ID('model.GetAgingReport') IS NOT NULL
	DROP PROCEDURE model.GetAgingReport
GO

CREATE PROCEDURE model.GetAgingReport (@EndDate DATETIME
	,@BilledTo NVARCHAR(10)
	,@BillingOrganizations NVARCHAR(MAX)
	,@Doctors NVARCHAR(MAX)
	,@ServiceLocations NVARCHAR(MAX)
	,@Insurers NVARCHAR(MAX)
	,@Services NVARCHAR(MAX)
	,@BillStatus NVARCHAR(MAX)
	,@AgingBucket NVARCHAR(MAX)
	,@BreakdownByBilledTo bit
	,@BreakdownByBillingOrganization bit
	,@BreakdownByProvider bit
	,@BreakdownByServiceLocation bit
	,@BreakdownByInsurer bit
	,@BreakDownByPatient BIT
	,@BreakdownByService bit
	)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	DECLARE @Today DATETIME 
	DECLARE @Today_30 DATETIME
	DECLARE @Today_60 DATETIME
	DECLARE @Today_90 DATETIME
	DECLARE @Today_120 DATETIME
	SET @Today = CONVERT(DATETIME, CONVERT(NVARCHAR(10),@EndDate,101))
	SET @Today_30 = DATEADD(DD,-30,@EndDate)
	SET @Today_60 = DATEADD(DD,-60,@EndDate)
	SET @Today_90 = DATEADD(DD,-90,@EndDate)
	SET @Today_120 = DATEADD(DD,-120,@EndDate)
	
	SELECT Id, CAST((Unit * UnitCharge) AS DECIMAL(20,2)) AS Charge,EncounterServiceId,InvoiceId,PatientBillDate INTO #BillingServices FROM model.billingservices
	CREATE CLUSTERED INDEX IX_BillingServices ON #BillingServices(Id, InvoiceId)
	SELECT Id, ShortName INTO #ServiceLocations FROM model.ServiceLocations
	SELECT Id, Code, Description INTO #EncounterServices FROM model.EncounterServices 
	SELECT Id, StartDateTime, PatientId,ServiceLocationId,InsuranceTypeId INTO #Encounters FROM model.Encounters WHERE StartDateTime <= @Today
	SELECT Id, Name INTO #BillingOrganizations FROM model.BillingOrganizations
	SELECT Id, ISNULL(Name,'') + ',' + ISNULL(PlanName,'') As Name INTO #Insurers FROM model.Insurers

	SELECT DISTINCT p.Id
		,COALESCE(u.Id,p.Id) AS UserId
		,p.BillingOrganizationId
		,COALESCE(u.UserName, sl.ShortName) AS Provider
	INTO #Providers
	FROM model.providers p
	LEFT JOIN model.users u ON u.id = p.userid
	LEFT JOIN #ServiceLocations sl ON sl.id = p.ServiceLocationId

	SELECT CONVERT(INT, nstr) AS Id INTO #SelectedInsurers FROM CharTable(@Insurers,',') 
	SELECT CONVERT(INT, nstr) AS Id INTO #SelectedBillingOrganizations FROM CharTable(@BillingOrganizations,',') 
	SELECT CONVERT(INT, nstr) AS Id INTO #SelectedServiceLocations FROM CharTable(@ServiceLocations,',') 
	SELECT CONVERT(INT, nstr) AS Id INTO #SelectedDoctors FROM CharTable(@Doctors,',') 
	SELECT CONVERT(INT, nstr) AS Id INTO #SelectedServices FROM CharTable(@Services,',') 
	SELECT CONVERT(INT, nstr) AS Id INTO #BilledTo FROM CharTable(@BilledTo,',')
	SELECT CONVERT(INT, nstr) AS Id INTO #AgingBucket FROM CharTable(@AgingBucket,',')

	SELECT PatientId, MAX(LastPaidDate) AS LastPaidDate
	INTO #GetLastPaidDate
	FROM (
		SELECT COALESCE(adj.PatientId, e.PatientId) AS PatientId, MAX(PostedDateTime) AS LastPaidDate 
		FROM model.Adjustments adj
		LEFT JOIN #BillingServices bs ON bs.id = adj.BillingServiceId
		LEFT JOIN model.Invoices i ON i.id = bs.InvoiceId
		LEFT JOIN #Encounters e ON e.Id = i.EncounterId
		GROUP BY adj.PatientId, e.PatientId
	) v
	GROUP BY v.PatientId

	SELECT InvoiceId, PatientInsuranceId
	INTO #PrimaryInsurances
	FROM (
		SELECT ROW_NUMBER() OVER (PARTITION BY ir.InvoiceId, pi.InsuranceTypeId ORDER BY pi.OrdinalId) AS RANK,
		ir.InvoiceId, ir.PatientInsuranceId
	FROM model.Invoices i 
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id 
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		INNER JOIN #Encounters e ON e.ID = i.EncounterId
			AND e.InsuranceTypeId = pi.InsuranceTypeId
		WHERE pi.IsDeleted = 0
			AND (pi.EndDateTime IS NULL 
			OR pi.EndDateTime = '' 
			OR pi.EndDateTime >= e.StartDateTime)
			AND ip.StartDateTime <= e.StartDateTime
	) v WHERE v.Rank = 1


	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #AllBuckets 
	FROM (
		SELECT 
		*
		FROM (
			SELECT 
			CASE 
				WHEN ir.PatientInsuranceId IS NULL 
					THEN 'Patient'
				ELSE 
					CASE 
						WHEN pri.PatientInsuranceId = ir.PatientInsuranceId 
							THEN 'Primary Insurer' 
						ELSE 'Non Primary Insurer' 
					END 
			END AS BilledToType
			,CASE 
				WHEN ir.PatientInsuranceId IS NULL 
					THEN 3
				ELSE 
					CASE 
						WHEN pri.PatientInsuranceId = ir.PatientInsuranceId 
							THEN 1 
						ELSE 2 
					END 
			END AS BilledToId
			,bo.NAME  AS BillingOrganization
			,pr.Provider  AS Doctor
			,sl.ShortName  AS ServiceLocation
			,ISNULL(ins.Name,'') + ',' + ISNULL(ins.PlanName,'') AS Insurer
			,ip.PolicyCode
			,CASE 
				WHEN ipn.AreaCode IS NULL 
					THEN SUBSTRING(ipn.ExchangeAndSuffix,0,4) + '-' + SUBSTRING(ipn.ExchangeAndSuffix,5,4) + '-' + SUBSTRING(ipn.ExchangeAndSuffix,9,5) 
				ELSE ipn.AreaCode + '-' + SUBSTRING(ipn.ExchangeAndSuffix,0,4) + '-' + SUBSTRING(ipn.ExchangeAndSuffix,4,5) 
			END AS InsurerPhone
			,p.LastName
			,p.FirstName
			,p.Id AS PatientId
			,CONVERT(NVARCHAR(10),p.DateOfBirth,101) AS DOB
			,bs.Id AS BillingServiceId
			,inv.Id AS InvoiceId
			,es.Code AS ServiceCode
			,es.Description AS ServiceDescription
			,CONVERT(NVARCHAR(10),e.StartDateTime,101) AS NiceDate
			,CONVERT(DATETIME,CONVERT(NVARCHAR(10),e.StartDateTime,112)) AS Date
			,bst.BillingServiceTransactionStatusId
			,bst.AmountSent AS Amount
			,CASE 
				WHEN EXISTS (
					SELECT 1 FROM model.BillingServiceTransactions 
					WHERE BillingServiceId = bst.BillingServiceId 
					AND BillingServiceTransactionStatusId = 6 
					AND InvoiceReceivableId = bst.InvoiceReceivableId
				) 
					THEN (
					SELECT MIN(DateTime) 
					FROM model.BillingServiceTransactions 
					WHERE BillingServiceId = bst.BillingServiceId 
					AND BillingServiceTransactionStatusId = 6 
					AND InvoiceReceivableId = bst.InvoiceReceivableId
				) 
				ELSE bst.[DateTime] 
			END AS BilledDate
			,bo.Id AS BillingOrganizationID
			,sl.Id AS ServiceLocationId
			,pr.UserId AS DoctorId
			,ins.Id AS InsurerId
			,es.Id AS ServiceId
			,ISNULL((
				(bs.Unit * bs.UnitCharge) - ISNULL((	
					SELECT SUM(CASE adjtype.IsDebit WHEN 1 THEN adj.Amount * - 1 else adj.Amount END) AS AdjAmount
					FROM model.Adjustments adj
					INNER JOIN model.AdjustmentTypes adjtype ON adj.AdjustmentTypeId = adjtype.Id
					WHERE adj.BillingServiceId = bs.Id 
					), 0)
			), 0) AS OpenBalance
			FROM model.Patients p
			JOIN #Encounters enc ON p.Id = enc.PatientId
			JOIN model.Invoices inv ON enc.Id = inv.EncounterId
			JOIN model.BillingServices bs ON inv.Id = bs.InvoiceId
			JOIN model.EncounterServices es ON bs.EncounterServiceId = es.Id
			JOIN model.BillingServiceTransactions bst ON bs.Id = bst.BillingServiceId
			JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
			JOIN #Encounters e ON e.Id = inv.EncounterId
			JOIN #Providers pr ON pr.Id = inv.BillingProviderId
			JOIN #BillingOrganizations bo ON bo.Id = pr.BillingOrganizationId
			JOIN #ServiceLocations sl ON sl.Id = inv.AttributeToServiceLocationId
			LEFT JOIN #PrimaryInsurances pri ON pri.InvoiceId = ir.InvoiceId
			LEFT OUTER JOIN model.Appointments ap ON enc.Id = ap.EncounterId
			LEFT OUTER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
			LEFT OUTER JOIN model.InsurancePolicies ip on ip.Id = pi.InsurancePolicyId
			LEFT OUTER JOIN model.Insurers ins ON ins.Id = ip.InsurerId
			LEFT JOIN model.InsurerPhoneNumbers ipn ON ipn.InsurerId = ins.Id
				AND ipn.InsurerPhoneNumberTypeId = 1
			WHERE ((@BillStatus IS NULL AND bst.BillingServiceTransactionStatusId IN (2,3,4))
				OR (@BillStatus IS NOT NULL AND bst.BillingServiceTransactionStatusId IN (SELECT CONVERT(int,nstr) AS Id FROM CHARTABLE(@BillStatus,','))))
				AND p.LastName NOT LIKE '%TEST%'
		) v
		WHERE v.OpenBalance > 0 
		) v
		WHERE Amount <> 0
		AND Date <= @Today
		AND ((@BillingOrganizations IS NULL AND BillingOrganizationId IN (SELECT Id FROM #BillingOrganizations)
			OR (@BillingOrganizations IS NOT NULL AND BillingOrganizationId IN (SELECT Id FROM #SelectedBillingOrganizations)))
			OR BillingOrganizationId IS NULL)
		AND ((@ServiceLocations IS NULL AND ServiceLocationId IN (SELECT Id FROM #ServiceLocations)
			OR (@ServiceLocations IS NOT NULL AND ServiceLocationId IN (SELECT Id FROM #SelectedServiceLocations)))
			OR ServiceLocationId IS NULL)
		AND ((@Doctors IS NULL AND DoctorId IN (SELECT Id FROM model.Users)
			OR (@Doctors IS NOT NULL AND DoctorId IN (SELECT Id FROM #SelectedDoctors)))
			OR DoctorId IS NULL)
		AND ((@Services IS NULL AND ServiceId IN (SELECT Id FROM #EncounterServices)
			OR (@Services IS NOT NULL AND ServiceId IN (SELECT Id FROM #SelectedServices)))
			OR ServiceId IS NULL)
		AND ((@BilledTo IS NULL AND BilledToId IN (1,2,3))
		 OR (@BilledTo IS NOT NULL AND BilledToId IN (SELECT Id FROM #BilledTo)))
		AND ((@Insurers IS NULL AND ((InsurerId IN (SELECT Id FROM #Insurers)))
		 OR (@Insurers IS NOT NULL AND InsurerId IN (SELECT Id FROM #SelectedInsurers)))
		 OR (@BreakdownByInsurer= 0 AND InsurerId IS NULL))

	-- Creating Bucket 0-30
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #Bucket_30
	FROM #AllBuckets v
	WHERE Date BETWEEN @Today_30 + 1 AND @Today

	-- Creating Bucket 31-60
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #Bucket30_60
	FROM #AllBuckets v
	WHERE Date BETWEEN @Today_60 + 1 AND @Today_30

	-- Creating Bucket 61-90
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #Bucket60_90
	FROM #AllBuckets v
	WHERE Date BETWEEN @Today_90 + 1 AND @Today_60

	-- Creating Bucket 90-120
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #Bucket90_120
	FROM #AllBuckets v
	WHERE Date BETWEEN @Today_120 + 1 AND @Today_90

	-- Creating Bucket 120+
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.Amount,v.BilledDate
	INTO #Bucket_120
	FROM #AllBuckets v
	WHERE Date <= @Today_120


	-- Creating the final bucket
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,CASE WHEN (@AgingBucket IS NULL OR 1 IN (SELECT Id FROM #AgingBucket)) THEN Amount ELSE CONVERT(DECIMAL(38, 2), 0) END AS Amount_30
		,CONVERT(DECIMAL(38, 2), 0) AS Amount30_60
		,CONVERT(DECIMAL(38, 2), 0) AS Amount60_90
		,CONVERT(DECIMAL(38, 2), 0) AS Amount90_120
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_120
		,CONVERT(DECIMAL(38, 2), 0) AS AmountTotal
	INTO #FinalBucket
	FROM #Bucket_30 v

	INSERT INTO #FinalBucket
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_30
		,CASE WHEN (@AgingBucket IS NULL OR 2 IN (SELECT Id FROM #AgingBucket)) THEN Amount ELSE CONVERT(DECIMAL(38, 2), 0) END AS Amount30_60
		,CONVERT(DECIMAL(38, 2), 0) AS Amount60_90
		,CONVERT(DECIMAL(38, 2), 0) AS Amount90_120
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_120
		,CONVERT(DECIMAL(38, 2), 0) AS AmountTotal
	FROM #Bucket30_60 v

	INSERT INTO #FinalBucket
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_30
		,CONVERT(DECIMAL(38, 2), 0) AS Amount30_60
		,CASE WHEN (@AgingBucket IS NULL OR 3 IN (SELECT Id FROM #AgingBucket)) THEN Amount ELSE CONVERT(DECIMAL(38, 2), 0) END AS Amount60_90
		,CONVERT(DECIMAL(38, 2), 0) AS Amount90_120
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_120
		,CONVERT(DECIMAL(38, 2), 0) AS AmountTotal
	FROM #Bucket60_90 v

	INSERT INTO #FinalBucket
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_30
		,CONVERT(DECIMAL(38, 2), 0) AS Amount30_60
		,CONVERT(DECIMAL(38, 2), 0) AS Amount60_90
		,CASE WHEN (@AgingBucket IS NULL OR 4 IN (SELECT Id FROM #AgingBucket)) THEN Amount ELSE CONVERT(DECIMAL(38, 2), 0) END AS Amount90_120
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_120
		,CONVERT(DECIMAL(38, 2), 0) AS AmountTotal
	FROM #Bucket90_120 v

	INSERT INTO #FinalBucket
	SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,CONVERT(DECIMAL(38, 2), 0) AS Amount_30
		,CONVERT(DECIMAL(38, 2), 0) AS Amount30_60
		,CONVERT(DECIMAL(38, 2), 0) AS Amount60_90
		,CONVERT(DECIMAL(38, 2), 0) AS Amount90_120
		,CASE WHEN (@AgingBucket IS NULL OR 5 IN (SELECT Id FROM #AgingBucket)) THEN Amount ELSE CONVERT(DECIMAL(38, 2), 0) END AS Amount_120
		,CONVERT(DECIMAL(38, 2), 0) AS AmountTotal
	FROM #Bucket_120 v

SELECT v.BilledToType, v.BilledToId,v.BillingOrganization,v.Doctor,v.ServiceLocation,v.Insurer,v.InsurerId,v.PolicyCode
		,v.LastPaidDate,v.PatientPhone,v.InsurerPhone,v.Date,v.NiceDate
		,v.BillingServiceTransactionStatusId,v.PatientId,v.DOB
		,v.LastName,v.FirstName,v.BillingServiceId,v.InvoiceId, v.ServiceCode,v.ServiceDescription,v.BilledDate
		,v.Amount_30,v.Amount30_60,v.Amount60_90, v.Amount90_120,v.Amount_120,v.AmountTotal
FROM (
	SELECT DISTINCT
		CASE WHEN @BreakdownByBilledTo = 0 THEN NULL ELSE fb.BilledToType END AS BilledToType
		,CASE WHEN @BreakdownByBilledTo = 0 THEN NULL ELSE fb.BilledToId END AS BilledToId
		,CASE WHEN @BreakdownByBillingOrganization = 0 THEN NULL ELSE fb.BillingOrganization END AS BillingOrganization
		,CASE WHEN @BreakdownByProvider = 0 THEN NULL ELSE fb.Doctor END AS Doctor
		,CASE WHEN @BreakdownByServiceLocation = 0 THEN NULL ELSE fb.ServiceLocation END AS ServiceLocation
		,CASE WHEN @BreakdownByInsurer = 0 THEN NULL ELSE fb.Insurer  END AS Insurer
		,CASE WHEN @BreakdownByInsurer = 0 THEN NULL ELSE fb.InsurerId  END AS InsurerId
		,CASE WHEN @BreakdownByInsurer = 0 THEN NULL ELSE fb.InsurerPhone END AS InsurerPhone
		,fb.PatientId 
		,fb.LastName 
		,fb.FirstName 
		,fb.DOB
		,fb.PolicyCode
		,MAX(CONVERT(NVARCHAR(10),lpd.LastPaidDate,101)) AS LastPaidDate
		,CASE WHEN ppn.AreaCode IS NULL THEN ppn.ExchangeAndSuffix
			ELSE ppn.AreaCode + '-' + ppn.ExchangeAndSuffix END AS PatientPhone 
		,fb.Date
		,fb.NiceDate 
		,fb.BillingServiceTransactionStatusId
		,fb.BillingServiceId 
		,fb.InvoiceId
		,fb.ServiceCode 
		,fb.ServiceDescription
		,fb.BilledDate 
		,SUM(fb.Amount_30) AS Amount_30
		,SUM(fb.Amount30_60) AS Amount30_60
		,SUM(fb.Amount60_90) AS Amount60_90
		,SUM(fb.Amount90_120) AS Amount90_120
		,SUM(fb.Amount_120) AS Amount_120
		,SUM(fb.Amount_30) + SUM(fb.Amount30_60) + SUM(fb.Amount60_90) + SUM(fb.Amount90_120) + SUM(fb.Amount_120) AS AmountTotal
	FROM #FinalBucket fb
	LEFT JOIN #GetLastPaidDate lpd ON lpd.PatientId = fb.PatientId
	LEFT JOIN model.PatientPhoneNumbers ppn ON ppn.PatientId = fb.PatientId
		AND ppn.OrdinalId = 1
	GROUP BY 
		fb.BilledToType
		,fb.BilledToId 
		,fb.BillingOrganization 
		,fb.Doctor 
		,fb.ServiceLocation 
		,fb.Insurer
		,fb.InsurerId
		,fb.InsurerPhone
		,fb.PatientId 
		,fb.LastName 
		,fb.FirstName 
		,fb.DOB
		,fb.PolicyCode
		,lpd.LastPaidDate
		,CASE WHEN ppn.AreaCode IS NULL THEN ppn.ExchangeAndSuffix
			ELSE ppn.AreaCode + '-' + ppn.ExchangeAndSuffix END 		
			,fb.Date
		,fb.NiceDate 
		,fb.BillingServiceTransactionStatusId
		,fb.BillingServiceId 
		,fb.InvoiceId
		,fb.ServiceCode 
		,fb.ServiceDescription
		,fb.BilledDate 
) v WHERE AmountTotal <> 0 

SET NOCOUNT OFF
END
GO

-- exec model.GetAgingReport '2014-11-18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0  