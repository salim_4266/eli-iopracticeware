SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- Author:		Rawle A.
-- Create date: 20130409
-- Description:	Get's a patient's balance, grouped by intervals
--				MINIMUM of 2 dollars
--				*currently customized for Cetina
--				Current intervals are:
--				'30 To 60 Day Interval'
--				'61 Day To 7 Month Interval'
--				'Older Than 7 Month Interval'
-- ============================================================
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'model.GetPatientBalancesGroupedByInterval') AND type in (N'P', N'PC'))
	EXEC('DROP PROCEDURE model.GetPatientBalancesGroupedByInterval')
GO

CREATE PROCEDURE model.GetPatientBalancesGroupedByInterval
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON;

--get me all the invoicereceivables that are out to the patient
IF OBJECT_ID('tempdb..#InvoiceReceivablesOutToPatient', 'U') IS NOT NULL
  DROP TABLE #InvoiceReceivablesOutToPatient
SELECT 
ir.Id AS InvoiceReceivableId
INTO #InvoiceReceivablesOutToPatient
FROM model.InvoiceReceivables ir
WHERE PatientInsuranceId IS NULL

CREATE INDEX IX_InvoiceReceivablesOutToPatient_InvoiceReceivableId ON #InvoiceReceivablesOutToPatient(InvoiceReceivableId)

-- ok not get me the patientIds linked to those invoicereceivables
IF OBJECT_ID('tempdb..#InvoiceReceivablesOutToPatientWithPatientId', 'U') IS NOT NULL
  DROP TABLE #InvoiceReceivablesOutToPatientWithPatientId
SELECT 
irotp.InvoiceReceivableId
,e.PatientId
INTO #InvoiceReceivablesOutToPatientWithPatientId
FROM #InvoiceReceivablesOutToPatient irotp
INNER JOIN model.InvoiceReceivables ir ON ir.id = irotp.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.id = ir.InvoiceId
INNER JOIN model.Encounters e ON e.Id = i.EncounterId

CREATE INDEX IX_InvoiceReceivablesOutToPatientWithPatientId_InvoiceReceivableId ON #InvoiceReceivablesOutToPatientWithPatientId(InvoiceReceivableId)
IF OBJECT_ID('tempdb..#InvoiceReceivablesOutToPatient', 'U') IS NOT NULL
  DROP TABLE #InvoiceReceivablesOutToPatient

-- ok for those invoice receivables get me the BSTs linked to them in an effort to learn how much was "sent" to the patient
IF OBJECT_ID('tempdb..#AmountSentPerIntervalPerInvoiceReceivableId', 'U') IS NOT NULL
  DROP TABLE #AmountSentPerIntervalPerInvoiceReceivableId
SELECT 
InvoiceReceivableId
,AmountSent
,[DateTime]
INTO #AmountSentPerIntervalPerInvoiceReceivableId
FROM model.BillingServiceTransactions
WHERE BillingServiceTransactionStatusId = 2
AND InvoiceReceivableId IN (SELECT InvoiceReceivableId FROM #InvoiceReceivablesOutToPatientWithPatientId)

CREATE INDEX IX_AmountSentPerIntervalPerInvoiceReceivableId_InvoiceReceivableId ON #AmountSentPerIntervalPerInvoiceReceivableId(InvoiceReceivableId)

-- ok give me a total of the amount that was sent, per invoicereceivable
IF OBJECT_ID('tempdb..#TotalAmountSentPerIntervalPerInvoiceReceivableId', 'U') IS NOT NULL
  DROP TABLE #TotalAmountSentPerIntervalPerInvoiceReceivableId
SELECT 
InvoiceReceivableId
,SUM(AmountSent) AS TotalAmountSent
,CASE 
	WHEN [DateTime] BETWEEN DATEADD(DAY,-60,GETDATE()) AND DATEADD(DAY,-30,GETDATE()) THEN 1
	WHEN [DateTime] BETWEEN DATEADD(MONTH,-7,GETDATE()) AND DATEADD(DAY,-61,GETDATE()) THEN 2
	WHEN [DateTime] <= DATEADD(MONTH,-7,GETDATE()+1) THEN 3
	ELSE -1
END AS InvoiceReceivableDateTimeInterval
,MAX(DateTime) AS BSTDateTime
INTO #TotalAmountSentPerIntervalPerInvoiceReceivableId
FROM #AmountSentPerIntervalPerInvoiceReceivableId
GROUP BY InvoiceReceivableId
,[DateTime]

CREATE INDEX IX_TotalAmountSentPerIntervalPerInvoiceReceivableId_InvoiceReceivableId ON #TotalAmountSentPerIntervalPerInvoiceReceivableId(InvoiceReceivableId)
IF OBJECT_ID('tempdb..#AmountSentPerIntervalPerInvoiceReceivableId', 'U') IS NOT NULL
  DROP TABLE #AmountSentPerIntervalPerInvoiceReceivableId

-- for the invoicereceivables that are in question, get me all payments that are linked to them in an effort to learn when the last payment date on the invoicereceivables was made. 
-- this does not consider who the payment is from, just when the last payment towards this invoicereceivable was made.
IF OBJECT_ID('tempdb..#PaymentDates', 'U') IS NOT NULL
  DROP TABLE #PaymentDates
SELECT 
InvoiceReceivableId
,PostedDateTime
INTO #PaymentDates
FROM model.Adjustments
WHERE InvoiceReceivableId IN (SELECT InvoiceReceivableId FROM #InvoiceReceivablesOutToPatientWithPatientId)
AND AdjustmentTypeId = 1

CREATE INDEX IX_PaymentDates_InvoiceReceivableId ON #PaymentDates(InvoiceReceivableId)

--get me the last paymentdate, per invoicereceivable
IF OBJECT_ID('tempdb..#LastPaymentDate', 'U') IS NOT NULL
  DROP TABLE #LastPaymentDate
SELECT 
InvoiceReceivableId
,MAX(PostedDateTime) AS LastPaymentDate 
INTO #LastPaymentDate
FROM #PaymentDates
GROUP BY InvoiceReceivableId

CREATE INDEX IX_LastPaymentDate_InvoiceReceivableId ON #LastPaymentDate(InvoiceReceivableId)
IF OBJECT_ID('tempdb..#PaymentDates', 'U') IS NOT NULL
  DROP TABLE #PaymentDates

-- build me a mini-table with patient info that correlates to the invoicereceivables in question
-- first: name and phonenumber
IF OBJECT_ID('tempdb..#PatientInfo', 'U') IS NOT NULL
  DROP TABLE #PatientInfo
SELECT 
P.Id
,CASE
	WHEN MiddleName<>'' AND MiddleName IS NOT NULL
		THEN LastName + ', ' + FirstName + ', ' + MiddleName
	ELSE LastName + ', ' + FirstName
END AS PatientName
,P.SocialSecurityNumber
,y.PhoneNumber
INTO #PatientInfo
FROM model.Patients P
LEFT JOIN 
(
	SELECT 
	PatientId
	,CASE LEN(ExchangeAndSuffix)
		WHEN 10 THEN 
		CASE 
			WHEN AreaCode IS NULL 
				THEN SUBSTRING(ExchangeAndSuffix,1,3) + '-' + SUBSTRING(ExchangeAndSuffix,3,3) + '-' + SUBSTRING(ExchangeAndSuffix,6,4) 
			ELSE AreaCode + '-' + SUBSTRING(ExchangeAndSuffix,1,3) + '-' + SUBSTRING(ExchangeAndSuffix,3,4) 
		END
	END AS PhoneNumber
	FROM model.PatientPhoneNumbers pn 
	WHERE OrdinalId = 1
) y ON y.PatientId=P.Id	
WHERE PatientId IN (
SELECT 
PatientId
FROM 
#InvoiceReceivablesOutToPatientWithPatientId
)

CREATE INDEX IX_PatientInfo_Id ON #PatientInfo(Id)

-- second: address
IF OBJECT_ID('tempdb..#FullPatientInfo', 'U') IS NOT NULL
  DROP TABLE #FullPatientInfo
SELECT 
P.Id
,P.PatientName
,P.SocialSecurityNumber
,P.PhoneNumber
,A.Line1
,A.Line2
,CASE 
	WHEN A.Line3 IS NOT NULL
		THEN A.Line3 
	ELSE ''
END AS Line3
,A.City
,S.Abbreviation
,CASE LEN(A.PostalCode)
	WHEN 9 THEN SUBSTRING(A.PostalCode,1,5) + '-' + SUBSTRING(A.PostalCode,5,4)
	ELSE A.PostalCode
END AS PostalCode 
INTO #FullPatientInfo
FROM #PatientInfo P
JOIN model.PatientAddresses A ON A.PatientId=P.Id
JOIN model.StateOrProvinces S ON S.Id = A.StateOrProvinceId

CREATE INDEX IX_FullPatientInfo_Id ON #FullPatientInfo(Id)
IF OBJECT_ID('tempdb..#PatientInfo', 'U') IS NOT NULL
  DROP TABLE #PatientInfo

-- combine the table's I've collected to produce the data requested
IF OBJECT_ID('tempdb..#PresentStatementAmount', 'U') IS NOT NULL
  DROP TABLE #PresentStatementAmount
SELECT 
A.InvoiceReceivableId
,A.PatientId
,B.TotalAmountSent
,B.InvoiceReceivableDateTimeInterval
,C.LastPaymentDate
,B.BSTDateTime
INTO #PresentStatementAmount
FROM #InvoiceReceivablesOutToPatientWithPatientId A
JOIN #TotalAmountSentPerIntervalPerInvoiceReceivableId B ON A.InvoiceReceivableId=B.InvoiceReceivableId
LEFT JOIN #LastPaymentDate C ON A.InvoiceReceivableId=C.InvoiceReceivableId

-- total balance out to the patient, great than 2 bucks
SELECT
ptInfo.PatientName
,v.TotalBalanceDue
,ptInfo.Line1
,ptInfo.Line2
,ptInfo.Line3
,ptInfo.City
,ptInfo.Abbreviation
,ptInfo.PostalCode
,ptInfo.SocialSecurityNumber
,ptInfo.PhoneNumber
,CASE MIN(A.InvoiceReceivableDateTimeInterval)  --i want only the minimum invoicereceivable interval, because that is the most current balance
	WHEN 1 THEN '30 To 60 Day Interval'
	WHEN 2 THEN '61 Day To 7 Month Interval'
	WHEN 3 THEN 'Older Than 7 Month Interval'
	ELSE 'Irrelevant' -- shouldn't have any of these
END AS InvoiceReceivableDateTimeInterval
,CONVERT(NVARCHAR(10),MAX(A.LastPaymentDate),101)  AS LastPaymentDate --per interval
FROM #PresentStatementAmount A
INNER JOIN (
	SELECT 
	PatientId
	,SUM(TotalAmountSent) AS TotalBalanceDue
	FROM #PresentStatementAmount
	WHERE InvoiceReceivableDateTimeInterval <> -1
	GROUP BY PatientId
) v ON v.PatientId=A.PatientId
INNER JOIN #FullPatientInfo ptInfo ON ptInfo.Id=A.PatientId
WHERE InvoiceReceivableDateTimeInterval <> -1
AND TotalBalanceDue >= 2.00
GROUP BY A.PatientId
,v.TotalBalanceDue
,ptInfo.PatientName
,ptInfo.Line1
,ptInfo.Line2
,ptInfo.Line3
,ptInfo.City
,ptInfo.Abbreviation
,ptInfo.PostalCode
,ptInfo.SocialSecurityNumber
,ptInfo.PhoneNumber
ORDER BY ptInfo.PatientName

END
GO


/*
To test this stored procedure,
just execute it:

EXEC model.GetPatientBalancesGroupedByInterval
*/