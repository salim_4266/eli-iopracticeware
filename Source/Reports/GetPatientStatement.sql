IF OBJECT_ID('model.GetPatientStatement') IS NOT NULL
	DROP PROCEDURE model.GetPatientStatement
GO

CREATE PROCEDURE model.GetPatientStatement (@PatientId INT
	,@EncounterId INT = NULL
	,@BillingOrganizationId INT = NULL
	,@ServiceLocations NVARCHAR(MAX) = NULL
	,@Providers NVARCHAR(MAX) = NULL
	,@BillingServiceTransactionIds NVARCHAR(MAX) = NULL
	,@InvoiceIds NVARCHAR(MAX) = NULL
	,@BillingServiceIds NVARCHAR(MAX) = NULL
	)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	IF (@ServiceLocations = '') SET @ServiceLocations = NULL
	IF (@Providers = '') SET @Providers = NULL
	IF (@BillingServiceTransactionIds = '') SET @BillingServiceTransactionIds = NULL
	IF (@InvoiceIds = '') SET @InvoiceIds = NULL
	IF (@BillingServiceIds = '') SET @BillingServiceIds = NULL
	
	SELECT b.* 
	INTO #BillingServices 
	FROM model.BillingServices b
	JOIN model.Invoices i ON i.Id = b.InvoiceId
	JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	WHERE ap.PatientId = @PatientId
	AND ((@BillingServiceIds IS NULL) OR (@BillingServiceIds IS NOT NULL AND b.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingServiceIds,','))))

	SELECT * 
	INTO #Encounters 
	FROM (
		SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN 'P'
				THEN 1
			WHEN 'R'
				THEN 1
			WHEN 'X'
				THEN 14
			WHEN 'Q'
				THEN 13
			WHEN 'F'
				THEN 12
			WHEN 'N'
				THEN 11
			WHEN 'S'
				THEN 10
			WHEN 'Y'
				THEN 9
			WHEN 'O'
				THEN 8
			WHEN 'D'
				THEN 7
			WHEN 'A'
				THEN CASE pa.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,CASE
			WHEN apt.ResourceId8 = 1 AND reASC.ResourceId IS NOT NULL
				THEN 3
			ELSE 1 
			END AS EncounterTypeId
		, CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,pn.PracticeId AS ServiceLocationId
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
		,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
		,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.PracticeName pn ON pn.PracticeType = 'P'
		AND ap.ResourceId2 - 1000 = pn.PracticeId
	INNER JOIN dbo.PracticeInterfaceConfiguration pic ON pic.FieldReference = 'ASC'
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = 'QUESTIONSETS'
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
	LEFT JOIN dbo.Resources reASC ON reASC.PracticeId = pn.PracticeId
		AND pn.LocationReference <> ''
		AND reASC.ResourceType = 'R'
		AND reASC.ServiceCode = '02'
		AND FieldValue = 'T' 
	WHERE ap.AppTime >= 1 
		AND ap.PatientId = @PatientId
	
	UNION ALL
	
	----At main office
	SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN 'P'
				THEN 1
			WHEN 'R'
				THEN 1
			WHEN 'X'
				THEN 14
			WHEN 'Q'
				THEN 13
			WHEN 'F'
				THEN 12
			WHEN 'N'
				THEN 11
			WHEN 'S'
				THEN 10
			WHEN 'Y'
				THEN 9
			WHEN 'O'
				THEN 8
			WHEN 'D'
				THEN 7
			WHEN 'A'
				THEN CASE pa.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,1 AS EncounterTypeId
		,CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,pn.PracticeId AS ServiceLocationId
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
		,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
		,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.PracticeName pn ON pn.PracticeType = 'P'
		AND pn.LocationReference = ''
		AND ap.ResourceId2 = 0
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = 'QUESTIONSETS'
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	WHERE ap.AppTime >= 1
		AND ap.PatientId = @PatientId
	
	UNION ALL
	
	----External Facility, not billing  (SL from Resources table)
	SELECT ap.AppointmentId AS Id
		,COALESCE(CASE ap.ScheduleStatus
			WHEN 'P'
				THEN 1
			WHEN 'R'
				THEN 1
			WHEN 'X'
				THEN 14
			WHEN 'Q'
				THEN 13
			WHEN 'F'
				THEN 12
			WHEN 'N'
				THEN 11
			WHEN 'S'
				THEN 10
			WHEN 'Y'
				THEN 9
			WHEN 'O'
				THEN 8
			WHEN 'D'
				THEN 7
			WHEN 'A'
				THEN CASE pa.[Status]
						WHEN 'W'
							THEN 2
						WHEN 'M'
							THEN 3
						WHEN 'E'
							THEN 4
						WHEN 'G'
							THEN 5
						WHEN 'H'
							THEN 6
						WHEN 'U'
							THEN 15
						END
			END, 14) AS EncounterStatusId
		,1 AS EncounterTypeId
		,CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,(110000000 * 1 + re.ResourceId) AS ServiceLocationId
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,CONVERT(int, confirmStatusPct.Id) AS ConfirmationStatusId
		,DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) AS StartDateTime 
		,DATEADD(hh, 1, DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112))) AS EndDateTime 
	FROM dbo.Appointments ap
	INNER JOIN dbo.AppointmentType apt ON apt.AppTypeId = ap.AppTypeId
	INNER JOIN dbo.Resources re ON re.ResourceId = ap.ResourceId2
		AND re.ResourceType = 'R'
		AND re.ServiceCode = '02' 
	INNER JOIN dbo.PracticeCodeTable pctQU ON pctQU.Code = apt.Question
		AND pctQU.ReferenceType = 'QUESTIONSETS'
	LEFT OUTER JOIN dbo.PracticeCodeTable confirmStatusPct ON SUBSTRING(confirmStatusPct.Code, 1, 1) = ap.ConfirmStatus
		AND confirmStatusPct.ReferenceType = 'CONFIRMSTATUS'
	LEFT JOIN dbo.PracticeActivity pa ON pa.AppointmentId = ap.AppointmentId
	WHERE ap.AppTime >= 1	
		AND ap.PatientId = @PatientId
	
	UNION ALL
	
	----Appointments that are added, either via data conversion ("highlights"), billing or optical. Appointments with "ASC CLAIM" are excluded to avoid to encounterids for internal facility visits.  Surgery orders are included; consider removing.
	SELECT ap.AppointmentId AS Id
		,7 AS EncounterStatusId
		,CASE ap.Comments 
			WHEN 'ADD VIA OPTICAL'
				THEN 4
			ELSE 1 END AS EncounterTypeId
		,CASE ap.ApptInsType
			WHEN 'M'
				THEN 1
			WHEN 'V'
				THEN 2
			WHEN 'A'
				THEN 3
			WHEN 'W'
				THEN 4
			ELSE 1
			END AS InsuranceTypeId
		,ap.PatientId AS PatientId
		,(110000000 * CASE 
			WHEN ResourceId2 = 0 OR ResourceId2 > 1000 THEN 0 
			ELSE 1 
			END + CASE 
			WHEN ResourceId2 = 0 THEN pnMainOffice.PracticeId 
			WHEN ResourceId2 > 1000 THEN ResourceId2 - 1000 
			ELSE ResourceId2 
			END) AS ServiceLocationId
		,EncounterStatusChangeReasonId AS EncounterStatusChangeReasonId
		,EncounterStatusChangeComment AS EncounterStatusChangeComment
		,NULL AS ConfirmationStatusId
		,CONVERT(datetime, ap.AppDate, 112) AS StartDateTime 
		,DATEADD(hh, 1, CONVERT(datetime, ap.AppDate, 112)) AS EndDateTime	
	FROM dbo.Appointments ap
	INNER JOIN dbo.PracticeCodeTable pct ON Referencetype = 'QUESTIONSETS' and Code = 'No Questions'
	INNER JOIN dbo.PracticeName pnMainOffice ON 
		PracticeType = 'P' AND LocationReference = '' 
	WHERE ap.AppTime < 1
		AND ap.Comments <> 'ASC CLAIM'
		AND ap.PatientId = @PatientId
	) v

	SELECT * 
	INTO #Appointments
	FROM (
	SELECT AppointmentId AS Id,
		CASE ap.AppTypeId 
			WHEN 0
				THEN -1 
			ELSE ap.AppTypeId END AS AppointmentTypeId,
		CASE
			WHEN ap.AppTime = -1
				THEN CONVERT(datetime, ap.AppDate, 112)
			ELSE
				DATEADD(n, ap.AppTime, CONVERT(datetime, ap.AppDate, 112)) 
		END AS [DateTime],
		ap.EncounterId AS EncounterId,
		ap.ResourceId1 AS UserId,
		NULL AS EquipmentId,
		NULL AS RoomId,
		CAST(NULL AS datetime) AS OrderDate,
		CASE ap.Comments
			WHEN 'ADD VIA HIGHLIGHTS'
				THEN NULL
			WHEN ''
				THEN NULL
			ELSE ap.Comments END AS Comment,
		CASE 
			WHEN ap.AppTime >= 0
				THEN CONVERT(bit, 1)
			ELSE
				CONVERT(bit, 0)
		END AS DisplayOnCalendar,
		'UserAppointment' AS __EntityType__
	FROM dbo.Appointments ap
	INNER JOIN dbo.Resources re on re.ResourceId = ap.ResourceId1
	WHERE ap.ResourceId1 <> 0 
		AND ap.Comments NOT IN ('ADD VIA BILLING','ADD VIA OPTICAL','ADD VIA SURGERY ORDER')
		AND ap.PatientId = @PatientId
	)v

	SELECT i.* 
	INTO #Insurers 
	FROM model.Insurers i
	JOIN model.InsurancePolicies ip ON ip.InsurerId = i.Id
	JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
	WHERE pi.InsuredPatientId = @PatientId
		OR ip.PolicyHolderPatientId = @PatientId

	SELECT a.* 
	INTO #Adjustments 
	FROM model.Adjustments a
	JOIN model.InvoiceReceivables ir ON ir.Id = a.InvoiceReceivableId
	JOIN model.Invoices i ON i.Id = ir.InvoiceId
	JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	WHERE ap.PatientId = @PatientId

	SELECT i.* 
	INTO #Invoices
	FROM model.Invoices i 
	JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	WHERE ap.PatientId = @PatientId 
	AND ((@InvoiceIds IS NULL) OR (@InvoiceIds IS NOT NULL AND i.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@InvoiceIds,','))))

	SELECT ir.* 
	INTO #InvoiceReceivables
	FROM model.InvoiceReceivables ir
	JOIN model.Invoices i ON i.Id = ir.InvoiceId
	JOIN dbo.Appointments ap ON ap.EncounterId = i.EncounterId
	WHERE ap.PatientId = @PatientId
	
	DECLARE @Today DATETIME 
	DECLARE @Today_30 DATETIME
	DECLARE @Today_60 DATETIME
	DECLARE @Today_90 DATETIME
	DECLARE @Today_120 DATETIME
	DECLARE @Date DATETIME
	SET @Date = CONVERT(DATETIME,CONVERT(NVARCHAR(10),GETDATE(),101),101)
	SET @Today = @Date
	SET @Today_30 = @Date - 30	
	SET @Today_60 = @Date - 60	
	SET @Today_90 = @Date - 90	
	SET @Today_120 = @Date - 120	

	DECLARE @PolicyHolderPatientId INT

	SELECT TOP 1 @PolicyHolderPatientId = ip.PolicyHolderPatientId
	FROM model.Insurers i
	JOIN model.InsurancePolicies ip ON ip.InsurerId = i.Id
	JOIN model.PatientInsurances pi ON pi.InsurancePolicyId = ip.Id
	WHERE pi.InsuredPatientId = @PatientId  AND pi.IsDeleted <> 1 AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= GETDATE())
	ORDER BY InsuranceTypeId,OrdinalId ASC


	IF (@PolicyHolderPatientId IS NULL) 
	BEGIN  
		SET @PolicyHolderPatientId = 0
	END

		
	SELECT DISTINCT pa.PatientId, pa.Line1 + ' ' + COALESCE(pa.Line2,'')AS [Address], pa.City AS City,
		sop.Abbreviation AS [State], pa.PostalCode, pa.OrdinalId,pa.PatientAddressTypeId
	INTO #PatientAddresses
	FROM model.PatientAddresses pa
	INNER JOIN model.StateOrProvinces sop ON pa.StateOrProvinceId = sop.Id
	INNER JOIN #Encounters en ON en.PatientId = pa.PatientId
	WHERE pa.OrdinalId = 1
	AND en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
	
	SELECT DISTINCT pa.PatientId, pa.Line1 + ' ' + COALESCE(pa.Line2,'')AS [Address], pa.City AS City,
		sop.Abbreviation AS [State], pa.PostalCode, pa.OrdinalId,pa.PatientAddressTypeId
	INTO #PolicyHolderPatientAddresses
	FROM model.PatientAddresses pa
	INNER JOIN model.StateOrProvinces sop ON pa.StateOrProvinceId = sop.Id
	WHERE pa.OrdinalId = 1	AND pa.PatientId = @PolicyHolderPatientId 
		
    DECLARE @BillPolicyHolder BIT

	SELECT @BillPolicyHolder = CASE WHEN EXISTS (SELECT 1  FROM model.PatientCommunicationPreferences as pcp 
	INNER JOIN model.PatientAddresses as pa ON pcp.PatientAddressId = pa.Id
	WHERE pcp.patientId = @PatientId AND pcp.PatientCommunicationTypeId = 3 And pa.PatientId=@PolicyHolderPatientId) THEN 1 ELSE 0 END

	DECLARE @ShowPaymentTypes int
	SELECT @ShowPaymentTypes = COUNT(Id) FROM model.PaymentMethods WHERE LegacyDisplayEPaymentTypes = 1

	SELECT en.PatientId, bs.Id As BillingService, CAST(bs.Unit * bs.UnitCharge AS DECIMAL(8,2)) AS StandardCharge
	INTO #Charges
	FROM #BillingServices bs
	INNER JOIN #Invoices inv ON inv.Id = bs.InvoiceId
	INNER JOIN #Encounters en ON en.Id = inv.EncounterId
	LEFT JOIN model.Providers pr ON pr.Id = inv.BillingProviderId
	WHERE en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
	AND (@BillingOrganizationId IS NULL OR pr.BillingOrganizationId = @BillingOrganizationId)

	SELECT en.PatientId, adj.Id AS AdjustmentId, NULL AS FinancialInformationId, bs.Id as BillingService,ip.InsurerId AS Insurer,
		SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS AdjustmentAmount,
		NULL AS FinancialInformationAmount,
		adj.PostedDateTime AS PostDate,
		at.Name AS [Type],
		CASE WHEN adj.IncludeCommentOnStatement = 1 THEN adj.Comment
			ELSE '' END AS Comment,
		CASE WHEN adj.FinancialSourceTypeId = 1 THEN 'I'
			WHEN adj.FinancialSourceTypeId = 2 THEN 'P'
			WHEN adj.FinancialSourceTypeId = 3 THEN 'O'
			ELSE '' END AS [Source],
		1 AS AdjOrInfo
	INTO #Totals
	FROM #InvoiceReceivables ir
	INNER JOIN #Adjustments adj ON adj.InvoiceReceivableId = ir.Id
	INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId AND  at.IsPrintOnStatement = 1 
	INNER JOIN #BillingServices bs ON adj.BillingServiceId = bs.Id
	INNER JOIN #Invoices inv ON inv.Id = bs.InvoiceId AND ir.InvoiceId= inv.Id
	INNER JOIN #Encounters en ON en.Id = inv.EncounterId
	INNER JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	LEFT JOIN model.Providers pr ON pr.Id = inv.BillingProviderId
	LEFT JOIN model.PatientInsurances pins on pins.id = ir.PatientInsuranceId
	LEFT JOIN model.InsurancePolicies ip on ip.id = pins.InsurancePolicyId
	WHERE en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
	AND (@BillingOrganizationId IS NULL OR pr.BillingOrganizationId = @BillingOrganizationId)
	GROUP BY bs.Id, adj.Id, en.patientId, ip.InsurerId, fb.InsurerId, adj.PostedDateTime, at.Name, adj.Comment, adj.FinancialSourceTypeId,adj.IncludeCommentOnStatement

	UNION

	SELECT en.PatientId, NULL AS AdjustmentId, fi.Id AS FinancialInformationId, bs.Id as BillingService, ip.InsurerId AS Insurer, 0 AS AdjustmentAmount,
	SUM(fi.Amount) AS FinancialInformationAmount,
	fi.PostedDateTime AS PostDate,
	fit.Name AS [Type],
	CASE WHEN fi.IncludeCommentOnStatement = 1 THEN fi.Comment ELSE '' END AS Comment,
	CASE WHEN fi.FinancialSourceTypeId = 1 THEN 'I'
		WHEN fi.FinancialSourceTypeId = 2 THEN 'P'
		ELSE 'O' END AS [Source],
		2 AS AdjOrInfo
	FROM #BillingServices bs
	INNER JOIN model.FinancialInformations fi ON fi.BillingServiceId = bs.Id
	INNER JOIN model.FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId AND  fit.IsPrintOnStatement = 1
	INNER JOIN #InvoiceReceivables ir on ir.Id = fi.InvoiceReceivableId
	INNER JOIN #Invoices inv ON inv.Id = bs.InvoiceId
	INNER JOIN #Encounters en ON en.Id = inv.EncounterId
	INNER JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
	LEFT JOIN model.Providers pr ON pr.Id = inv.BillingProviderId
	LEFT JOIN model.PatientInsurances pins on pins.id = ir.PatientInsuranceId
	LEFT JOIN model.InsurancePolicies ip on ip.id = pins.InsurancePolicyId
	WHERE en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
		AND (@BillingOrganizationId IS NULL OR pr.BillingOrganizationId = @BillingOrganizationId)
	AND fit.Name <> 'Void'
	GROUP BY bs.Id, fi.Id, en.patientId, ip.InsurerId, fb.InsurerId, fi.PostedDateTime, fit.Name, fi.Comment, fi.FinancialSourceTypeId,fi.IncludeCommentOnStatement

	----UserStory:23052---Start------

	SELECT SUM(t.AdjustmentAmount) as LeftServiceBal, t.BillingService as BillingService
	INTO #LeftServiceBalance 
	FROM #Totals t
	GROUP BY t.BillingService 
	
	----UserStory:23052---End-----

	SELECT PatientId, Invoice, v.BillingService, v.BilledDate, v.[Transaction], SUM(TotalAmountSent) AS TotalAmountSent
	INTO #AmountSentByBillingService
	FROM (
		SELECT en.PatientId, inv.Id AS Invoice, bs.Id AS BillingService, CONVERT(DATETIME,CONVERT(NVARCHAR(10),COALESCE(bs.PatientBillDate,bst.DateTime),101),101) AS BilledDate, bst.Id AS [Transaction], SUM(AmountSent) AS TotalAmountSent
		FROM #Encounters en
		INNER JOIN #Invoices inv ON inv.EncounterId = en.Id
		INNER JOIN #InvoiceReceivables ir ON ir.InvoiceId = inv.Id
			AND ir.PatientInsuranceId IS NULL
		INNER JOIN #BillingServices bs ON inv.Id = bs.InvoiceId
		INNER JOIN model.BillingServiceTransactions bst ON bst.BillingServiceId = bs.Id
			AND bst.InvoiceReceivableId = ir.Id
			AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
		LEFT JOIN #Appointments ap ON ap.EncounterId = en.Id
		LEFT JOIN model.Providers pr ON pr.Id = inv.BillingProviderId
		WHERE en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
			AND (@BillingOrganizationId IS NULL OR pr.BillingOrganizationId = @BillingOrganizationId)
		GROUP BY inv.Id, bs.Id, en.PatientId, bs.PatientBillDate, bst.DateTime, bst.Id
	) v GROUP BY PatientId, Invoice, v.BillingService, v.BilledDate,v.[Transaction]

	SELECT PatientId, Invoice, SUM(TotalAmountSent) AS TotalAmountSent
	INTO #AmountSentByInvoice
	FROM #AmountSentByBillingService
	GROUP BY PatientId, Invoice

	SELECT PatientId, SUM(TotalAmountSent) AS TotalAmountSent
	INTO #TotalAmountSent
	FROM #AmountSentByInvoice
	GROUP BY PatientId

	SELECT PatientId, 
		SUM(Balance0_30) AS Balance0_30, 
		SUM(Balance30_60) AS Balance30_60, 
		SUM(Balance60_90) AS Balance60_90, 
		SUM(Balance90_120) AS Balance90_120, 
		SUM(Balance120) AS Balance120
	INTO #AgingBalance
	FROM (
		SELECT PatientId
			,CASE WHEN BilledDate BETWEEN @Today_30 AND @Today
				THEN SUM(TotalAmountSent) ELSE CONVERT(DECIMAL(38,2),0) END AS Balance0_30
			,CASE WHEN BilledDate BETWEEN @Today_60 AND DATEADD(DD,-1,@Today_30)
				THEN SUM(TotalAmountSent) ELSE CONVERT(DECIMAL(38,2),0) END AS Balance30_60
			,CASE WHEN BilledDate BETWEEN @Today_90 AND DATEADD(DD,-1,@Today_60)
				THEN SUM(TotalAmountSent) ELSE CONVERT(DECIMAL(38,2),0) END AS Balance60_90
			,CASE WHEN BilledDate BETWEEN @Today_120 AND DATEADD(DD,-1,@Today_90)
				THEN SUM(TotalAmountSent) ELSE CONVERT(DECIMAL(38, 2),0) END AS Balance90_120
			,CASE WHEN BilledDate < @Today_120
				THEN SUM(TotalAmountSent) ELSE CONVERT(DECIMAL(38,2),0) END AS Balance120
		FROM #AmountSentByBillingService
		GROUP BY PatientId, BilledDate
	) v
	GROUP BY PatientId

	DECLARE @Value NVARCHAR(MAX)
	SET @Value = ''
	SELECT @Value = @Value + Value + ' 
' 
	FROM model.PatientFinancialComments WHERE PatientId = @PatientId
	AND IsIncludedOnStatement = 1 AND IsArchived = 0

	DECLARE @InvoiceComment NVARCHAR(MAX)
	SET @InvoiceComment = ''
	SELECT @InvoiceComment = @InvoiceComment + Value + ' 
' 
	FROM model.InvoiceComments ic
	INNER JOIN #AmountSentByInvoice asbi ON asbi.Invoice = ic.InvoiceId
	AND ic.IncludeOnStatement = 1 AND ic.IsArchived = 0

	DECLARE @newLine CHAR(1)
	SET @newLine = CHAR(13)
	WHILE(CHARINDEX(@newLine+CHAR(10)+@newLine+CHAR(10),@InvoiceComment)>0)
	BEGIN 
		SELECT @InvoiceComment = REPLACE(@InvoiceComment,@newLine+CHAR(10)+@newline+CHAR(10),@newLine+CHAR(10))   
	END
	SELECT @InvoiceComment=REPLACE(@InvoiceComment,@newLine,CHAR(13))

	DECLARE @BillingService int
	SET @BillingService = 0
	CREATE TABLE #BillingServiceComments (BillingServiceId INT, Comment NVARCHAR(MAX))

	DECLARE serviceCursor CURSOR FOR 
			SELECT bsc.BillingServiceId 
			FROM #AmountSentByBillingService asbbs 
			INNER JOIN model.BillingServiceComments bsc ON asbbs.BillingService = bsc.BillingServiceId
				AND bsc.IsIncludedOnStatement = 1 AND bsc.IsArchived = 0
		OPEN serviceCursor

			FETCH NEXT FROM serviceCursor INTO @BillingService

			WHILE @@FETCH_STATUS = 0
			BEGIN

			DECLARE @Comment NVARCHAR(MAX)
			SET @Comment = ''
			SELECT @Comment = @Comment + Value + ' 
	' 
			FROM (
					SELECT DISTINCT Value 
					FROM model.BillingServiceComments bsc
					WHERE bsc.BillingServiceId = @BillingService
					AND bsc.IsIncludedOnStatement = 1 
					AND bsc.IsArchived = 0
			) v

			INSERT INTO #BillingServiceComments (BillingServiceId, Comment) VALUES (@BillingService, @Comment)

			FETCH NEXT FROM serviceCursor INTO @BillingService
		END
	CLOSE serviceCursor
	DEALLOCATE serviceCursor

	DECLARE @boName NVARCHAR(MAX)
	DECLARE @boAddress NVARCHAR(MAX)
	DECLARE @boCity NVARCHAR(MAX)
	DECLARE @boState NVARCHAR(MAX)
	DECLARE @boPostalCode NVARCHAR(MAX)
	DECLARE @boPhone NVARCHAR(MAX)
	DECLARE @Invoice int 

	SELECT @Invoice = Invoice FROM 
		(
		SELECT TOP 1 i.Id AS Invoice
		FROM #Invoices i 
		INNER JOIN #InvoiceReceivables ir ON ir.InvoiceId = i.Id
		INNER JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
			AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
			AND ir.PatientInsuranceId IS NULL
		INNER JOIN #Encounters e ON e.Id = i.EncounterId
		LEFT JOIN model.Providers p ON p.Id = i.BillingProviderId
		WHERE e.PatientId = @PatientId
		AND (@EncounterId IS NULL OR @EncounterId = e.Id)
		AND (@BillingOrganizationId IS NULL OR @BillingOrganizationId = p.BillingOrganizationId)
		ORDER BY e.StartDateTime DESC, i.Id ASC
	) v

	SELECT TOP 1 @boName = bo.Name,
		@boAddress = boa.Line1 + ' ' + COALESCE(boa.Line2,''),
		@boCity = boa.City,
		@boState = sop.Abbreviation,
		@boPostalCode = SUBSTRING(boa.PostalCode,0,6) + '-' + SUBSTRING(boa.PostalCode,6,4),
		@boPhone = COALESCE(bop.AreaCode + '-' + SUBSTRING(bop.ExchangeAndSuffix,0,4) + '-' + SUBSTRING(bop.ExchangeAndSuffix,4,4) 
					,bop2.AreaCode + '-' + SUBSTRING(bop2.ExchangeAndSuffix,0,4) + '-' + SUBSTRING(bop2.ExchangeAndSuffix,4,4))
	FROM #Invoices inv
	INNER JOIN model.Providers bp ON bp.Id = inv.BillingProviderId
	INNER JOIN model.BillingOrganizations bo ON bp.BillingOrganizationId = bo.Id
	INNER JOIN model.BillingOrganizationAddresses boa ON boa.BillingOrganizationId = bo.Id
	LEFT JOIN model.BillingOrganizationPhoneNumbers bop ON bop.BillingOrganizationId = bo.Id
		AND bop.BillingOrganizationPhoneNumberTypeId = 1
	LEFT JOIN model.BillingOrganizationPhoneNumbers bop2 ON bop2.BillingOrganizationId = bo.Id
		AND bop2.OrdinalId = 1
	INNER JOIN model.StateOrProvinces sop ON sop.Id = boa.StateOrProvinceId
	WHERE boa.BillingOrganizationAddressTypeId = 5
	AND @Invoice = inv.Id
	ORDER BY bop.BillingOrganizationPhoneNumberTypeId
	
	SELECT BillingServiceId, CASE WHEN bsm.OrdinalId = 1 THEN Code END AS CodePart1
		,CASE WHEN bsm.OrdinalId = 2 THEN Code END AS CodePart2
		,CASE WHEN bsm.OrdinalId = 3 THEN Code END AS CodePart3
		,CASE WHEN bsm.OrdinalId = 4 THEN Code END AS CodePart4
	INTO #ServiceModifiers
	FROM model.BillingServiceModifiers bsm
	INNER JOIN model.ServiceModifiers sm ON bsm.ServiceModifierId = sm.Id
	INNER JOIN model.BillingServices bs ON bs.Id = bsm.BillingServiceId
	INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	LEFT JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	WHERE e.PatientId = @PatientId AND (@EncounterId IS NULL OR e.ID = @EncounterId)
		AND (@BillingOrganizationId IS NULL OR pr.BillingOrganizationId = @BillingOrganizationId)
	ORDER BY bsm.BillingServiceId

	SELECT TOP 1 BillingServiceId,Code
	INTO #ServiceModifiersWithCodes
	FROM (
	SELECT 
	sm1.BillingServiceId,  COALESCE(sm1.CodePart1,'') + 
	COALESCE(' ' + sm2.CodePart2,'') + 
	COALESCE(' ' + sm3.CodePart3,'') +
	COALESCE(' ' + sm4.CodePart4,'') AS Code
	FROM #ServiceModifiers sm1
	INNER JOIN #ServiceModifiers sm2 ON sm2.BillingServiceId = sm1.BillingServiceId
	INNER JOIN #ServiceModifiers sm3 ON sm3.BillingServiceId = sm2.BillingServiceId
	INNER JOIN #ServiceModifiers sm4 ON sm4.BillingServiceId = sm3.BillingServiceId
	) v
	ORDER BY BillingServiceId, LEN(Code)

	SELECT DISTINCT
		en.Id AS Encounter
		,inv.Id AS Invoice
		,p.Id AS PatientId
		,bs.Id AS BillingService
		,COALESCE(@InvoiceComment,'') AS InvoiceComment
		,p.LastName AS PatientLastName
		,p.MiddleName As PatientMiddleName
		,p.FirstName AS PatientFirstName
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 LastName FROM model.Patients WHERE Id = @PolicyHolderPatientId) ELSE p.LastName END AS LastName
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 FirstName FROM model.Patients WHERE Id = @PolicyHolderPatientId) ELSE p.FirstName END AS FirstName
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 MiddleName FROM model.Patients WHERE Id = @PolicyHolderPatientId) ELSE p.MiddleName END AS MiddleName
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 ppa.Address FROM #PolicyHolderPatientAddresses ppa) ELSE paMain.[Address] END AS Address
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 ppa.City FROM #PolicyHolderPatientAddresses ppa) ELSE paMain.[City] END AS City
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 ppa.State FROM #PolicyHolderPatientAddresses ppa) ELSE paMain.[State] END AS State
		,CASE WHEN @PolicyHolderPatientId > 0 AND @BillPolicyHolder = 1 THEN  (SELECT TOP 1 ppa.PostalCode FROM #PolicyHolderPatientAddresses ppa) ELSE paMain.PostalCode END AS PostalCode
		,usBill.DisplayName AS Provider
		,en.StartDateTime,CONVERT(NVARCHAR(10),en.StartDateTime,101) AS InvoiceDate 
		,CONVERT(NVARCHAR(10),COALESCE(bs.PatientBillDate,bst.DateTime),101) AS BilledDateTime
		,bs.OrdinalId, CAST(bs.Unit AS INT) AS Qty, bs.UnitCharge
		,es.Code AS EncounterCode, es.Description AS EncounterDescription, COALESCE(smwc.Code,'') AS ServiceModifierCode
		,ch.StandardCharge AS ServiceCharge		
		,ch.StandardCharge - COALESCE(lsb.LeftServiceBal,0) as LeftServiceBalance				--------UserStory 23052
		,COALESCE(t.AdjustmentAmount,0) AS AdjustmentAmount
		,COALESCE('- ' + CONVERT(nvarchar(20),t.FinancialInformationAmount),'') AS FinancialInformationAmount
		,COALESCE(t.Comment,'') AS Comment
		,COALESCE(CONVERT(NVARCHAR(10),t.PostDate,101),'') AS PostDate
		,COALESCE(t.[Type],'') AS [Type]
		,COALESCE(t.AdjOrInfo,1) AS AdjOrInfo
		,t.AdjustmentId, t.FinancialInformationId
		,CASE WHEN t.[Source] = 'I' THEN ins.Name
			WHEN t.[Source] = 'P' THEN p.LastName
			WHEN t.[Source] = 'O' THEN 'Office'
			ELSE '' END AS [Source]
		,CASE WHEN t.[Type] IS NULL THEN 0
			ELSE 1 END AS Visible
		,COALESCE(tas.TotalAmountSent,0) AS AccountBalance
		,COALESCE(asbi.TotalAmountSent,0) AS ServiceBalance
		,COALESCE(ab.Balance0_30,0) AS Balance0_30
		,COALESCE(ab.Balance30_60,0) AS Balance30_60
		,COALESCE(ab.Balance60_90,0) AS Balance60_90
		,COALESCE(ab.Balance90_120,0) AS Balance90_120
		,COALESCE(ab.Balance120,0) AS Balance120
		,@Value AS PatientFinancialComment
		,COALESCE('- ' + bsc.Comment,'') AS BillingServiceComments
		,@boName AS MainOfficeName,@boAddress AS MainOfficeAddress,@boCity AS MainOfficeCity
		,@boState AS MainOfficeState,@boPostalCode AS MainOfficePostalCode,@boPhone AS MainOfficePhone
		,@ShowPaymentTypes AS ShowPaymentTypes
	FROM model.Patients p
	INNER JOIN #Encounters en ON en.PatientId = p.id
	INNER JOIN #Invoices inv ON inv.EncounterId = en.Id
	INNER JOIN #InvoiceReceivables ir ON ir.InvoiceId = inv.Id
		AND ir.PatientInsuranceId IS NULL
	INNER JOIN #BillingServices bs ON bs.InvoiceId = inv.Id
		AND bs.UnitCharge <> 0
	INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	INNER JOIN model.Providers bp ON bp.Id = inv.BillingProviderId
	INNER JOIN model.Users usBill ON bp.UserId = usBill.Id
	INNER JOIN #Charges ch ON ch.BillingService = bs.Id
	INNER JOIN model.BillingServiceTransactions bst ON bst.InvoiceReceivableId = ir.Id
		AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
		AND ((@BillingServiceTransactionIds IS NULL) OR (@BillingServiceTransactionIds IS NOT NULL AND bst.Id IN (SELECT CONVERT(uniqueidentifier,nstr) FROM CharTable(@BillingServiceTransactionIds,','))))
	LEFT JOIN model.EncounterServiceTypes est ON est.Id = es.EncounterServiceTypeId
	LEFT JOIN #Totals t ON t.BillingService = bs.Id
	LEFT JOIN #AgingBalance ab ON ab.PatientId = p.Id
	LEFT JOIN #TotalAmountSent tas ON tas.PatientId = p.Id
	LEFT JOIN #AmountSentByInvoice asbi ON asbi.Invoice = bs.InvoiceID
	LEFT JOIN #ServiceModifiersWithCodes smwc ON smwc.BillingServiceId = bs.Id
	LEFT JOIN #Insurers ins ON ins.Id = t.Insurer
	LEFT JOIN #PatientAddresses paMain ON paMain.PatientId = p.Id
	LEFT JOIN #BillingServiceComments bsc ON bsc.BillingServiceId = bs.Id
		AND bsc.Comment <> ''
	LEFT JOIN #LeftServiceBalance lsb ON lsb.BillingService = bs.Id					--------UserStory 23052
	WHERE en.PatientId = @PatientId AND (@EncounterId IS NULL OR en.ID = @EncounterId)
	AND (@BillingOrganizationId IS NULL OR bp.BillingOrganizationId = @BillingOrganizationId)
	AND ((@ServiceLocations IS NULL) OR (@ServiceLocations IS NOT NULL AND inv.AttributeToServiceLocationId IN (SELECT CONVERT(INT,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Providers IS NULL) OR (@Providers IS NOT NULL AND bp.Id IN (SELECT CONVERT(INT,nstr) FROM CharTable(@Providers,','))))
SET NOCOUNT OFF
RETURN
END 
GO