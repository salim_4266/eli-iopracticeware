IF OBJECT_ID('model.GetAccountHistoryDetail') IS NOT NULL
	DROP PROCEDURE model.GetAccountHistoryDetail
GO

CREATE PROCEDURE [model].[GetAccountHistoryDetail] (
	@PatientId INT
	,@StartDate DATETIME = NULL
	,@EndDate DATETIME = NULL
	,@PaymentStartDate DATETIME = NULL
	,@PaymentEndDate DATETIME = NULL
	)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
	SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))
	SET @PaymentStartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@PaymentStartDate,101))
	SET @PaymentEndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@PaymentEndDate,101))

	SELECT bs.Id
	,dbo.GetBillingServiceModifiers(bs.Id) AS Modifiers
	,SUM(CONVERT(INT, unit) * unitcharge) AS SumCharges
	INTO #SumCharges
	FROM model.invoices i
	INNER JOIN model.encounters e ON e.id = i.EncounterId
	INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.id
	WHERE e.PatientId = @PatientId 
		AND ((@StartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) >= @StartDate))
		AND ((@EndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) <= @EndDate))
	GROUP BY bs.Id

	SELECT bs.Id, SUM(CASE WHEN at.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS Amount
	INTO #Adjustments
	FROM model.Invoices i
	INNER JOIN model.BillingServices bs ON i.Id = bs.InvoiceId
	INNER JOIN model.Adjustments adj ON bs.Id = adj.BillingServiceId
	INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	INNER JOIN model.Encounters e ON i.EncounterId = e.Id
	WHERE e.PatientId = @PatientId
		AND ((@StartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) >= @StartDate))
		AND ((@EndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) <= @EndDate))
	GROUP BY bs.Id

	SELECT e.PatientId, adj.Id AS Id, bs.Id as BillingService, fb.InsurerId AS Insurer,
		SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS Amount,
		adj.PostedDateTime AS PostDate,
		at.Name AS [Type],
		CASE WHEN adj.IncludeCommentOnStatement = 1 THEN adj.Comment
			ELSE '' END AS Comment,
		CASE WHEN adj.FinancialSourceTypeId = 1 THEN i.Name
			WHEN adj.FinancialSourceTypeId = 2 THEN p.LastName + ', ' + p.FirstName
			WHEN adj.FinancialSourceTypeId = 3 THEN 'O'
			ELSE '' END AS [Source],
			fb.CheckCode,
			pm.Name AS PaymentMethod,
		1 AS AdjOrInfo
	INTO #Totals
	FROM model.InvoiceReceivables ir
	INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
	INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	INNER JOIN model.BillingServices bs ON adj.BillingServiceId = bs.Id
	INNER JOIN model.Invoices inv ON inv.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = inv.EncounterId
	LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	LEFT JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
	LEFT JOIN model.PatientInsurances Pati ON pati.id = ir.patientinsuranceid
    LEFT JOIN model.insurancepolicies ip ON ip.id = pati.insurancepolicyid
	LEFT JOIN model.Insurers i ON i.Id = ip.InsurerId
		AND adj.FinancialSourceTypeId = 1
	LEFT JOIN model.Patients p ON p.Id = fb.PatientId
		AND adj.FinancialSourceTypeId = 2
	WHERE e.PatientId = @PatientId 
		AND ((@PaymentStartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) >=  @PaymentStartDate))
		AND ((@PaymentEndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) <=  @PaymentEndDate))

		AND ((@StartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) >= @StartDate))
		AND ((@EndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) <= @EndDate))

	GROUP BY bs.Id, adj.Id, e.patientId, fb.InsurerId, adj.PostedDateTime, at.Name, adj.Comment,
	 adj.FinancialSourceTypeId,adj.IncludeCommentOnStatement,fb.CheckCode,pm.Name,i.Name,p.LastName,p.FirstName

	UNION

	SELECT e.PatientId, fi.Id AS  Id, bs.Id as BillingService, fi.InsurerId AS Insurer,
	SUM(fi.Amount) AS Amount,
	fi.PostedDateTime AS PostDate,
	fit.Name AS [Type],
	CASE WHEN fi.IncludeCommentOnStatement = 1 THEN fi.Comment ELSE '' END AS Comment,
	CASE WHEN fi.FinancialSourceTypeId = 1 THEN i.Name
		WHEN fi.FinancialSourceTypeId = 2 THEN p.LastName + ', ' + p.FirstName
		ELSE 'O' END AS [Source],
		fb.CheckCode,
		'' AS PaymentMethod,
		2 AS AdjOrInfo
	FROM model.BillingServices bs
	INNER JOIN model.FinancialInformations fi ON fi.BillingServiceId = bs.Id
	INNER JOIN model.FinancialInformationTypes fit ON fit.Id = fi.FinancialInformationTypeId
	INNER JOIN model.Invoices inv ON inv.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = inv.EncounterId
	INNER JOIN model.FinancialBatches fb ON fb.Id = fi.FinancialBatchId
	LEFT JOIN model.invoiceReceivables ir ON ir.id = fi.invoiceReceivableid
    LEFT JOIN model.PatientInsurances pati ON pati.id = ir.patientinsuranceid
    LEFT JOIN model.insurancepolicies ip ON ip.id = pati.insurancepolicyid
    LEFT JOIN model.Insurers i ON i.id = ip.insurerid 
		AND fi.FinancialSourceTypeId = 1
	LEFT JOIN model.Patients p ON p.Id = fi.PatientId
		AND fi.FinancialSourceTypeId = 2
	WHERE e.PatientId = @PatientId 
		  AND ((@PaymentStartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),fi.PostedDateTime, 101)) >=  @PaymentStartDate))
		  AND ((@PaymentEndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),fi.PostedDateTime, 101)) <=  @PaymentEndDate))

		  AND ((@StartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) >= @StartDate))
		  AND ((@EndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) <= @EndDate))

	AND fit.Name <> 'Void'
	GROUP BY bs.Id, fi.Id, e.patientId, fi.InsurerId, fi.PostedDateTime, fit.Name, fi.Comment, 
	fi.FinancialSourceTypeId,fi.IncludeCommentOnStatement,fb.CheckCode,i.Name,p.LastName,p.FirstName

	-- on account payments
	SELECT adj.PatientId, adj.Id, adj.InsurerId,
	SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS Amount,
	adj.PostedDateTime AS PostDate,
		at.Name AS [Type],
		CASE WHEN adj.IncludeCommentOnStatement = 1 THEN adj.Comment
			ELSE '' END AS Comment,
		CASE WHEN adj.FinancialSourceTypeId = 1 THEN i.Name
			WHEN adj.FinancialSourceTypeId = 2 THEN p.LastName + ', ' + p.FirstName
			WHEN adj.FinancialSourceTypeId = 3 THEN 'Office'
			ELSE '' END AS [Source],
			fb.CheckCode,
			pm.Name AS PaymentMethod,
		1 AS AdjOrInfo
	INTO #OnAccountPayments
	FROM model.Adjustments adj
	INNER JOIN model.AdjustmentTypes at ON adj.AdjustmentTypeId = at.Id
	LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	INNER JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
	LEFT JOIN model.Insurers i ON i.Id = fb.InsurerId
		AND fb.FinancialSourceTypeId = 1
	LEFT JOIN model.Patients p ON p.Id = adj.PatientId
		AND adj.FinancialSourceTypeId = 2
	WHERE adj.InvoiceReceivableId IS NULL 
		AND adj.PatientId = @PatientId
		AND ((@PaymentStartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) >=  @PaymentStartDate))
		AND ((@PaymentEndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) <=  @PaymentEndDate))
	GROUP BY adj.Id, adj.patientId, adj.InsurerId, adj.PostedDateTime, at.Name, adj.Comment, 
	adj.FinancialSourceTypeId,adj.IncludeCommentOnStatement,fb.CheckCode,pm.Name,i.Name,p.LastName,p.FirstName

	SELECT 	DISTINCT p.Id AS PatientId
		,p.LastName AS PatientLast
		,p.FirstName AS PatientFirst
		,CASE 
			WHEN pcp.PatientAddressId = (
					SELECT TOP 1 pad.Id
					FROM model.PatientAddresses pad
					WHERE pad.PatientId = ph.Id
					ORDER BY pad.Id
					)
				THEN 'Y'
			ELSE 'N'
			END AS BillParty
		,COALESCE(ph.Id,p.Id) AS PhId
		,COALESCE(ph.LastName,p.LastName) AS PhLast
		,COALESCE(ph.FirstName,p.FirstName) AS PhFirst
		,COALESCE(ph.MiddleName,p.MiddleName) AS PhMiddle
		,COALESCE(pah.Line1,pa.Line1) AS PhLine1
		,COALESCE(pah.Line2,pa.Line2) AS PhLine2
		,COALESCE(pah.City,pa.City) AS PhCity
		,COALESCE(soph.Abbreviation,sop.Abbreviation) AS PhState
		,COALESCE(pah.PostalCode,pa.PostalCode) AS PhZip
		,e.StartDateTime AS StartDateTime
		,CONVERT(NVARCHAR(10),e.StartDateTime,101) AS InvoiceDate
		,i.Id AS InvoiceId
		,bs.Id AS BillingServiceId
		,es.Code AS Service
		,es.Description AS ServiceDescription
		,sc.Modifiers 
		,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS TotalCharge
		,sc.SumCharges - COALESCE(tempad.Amount,0) AS ServiceBalance
		,COALESCE(t.Amount,0) AS Amount
		,COALESCE(t.Comment,'') AS Comment
		,t.PostDate AS PostDateOrdered
		,COALESCE(CONVERT(NVARCHAR(10),t.PostDate,101),'') AS PostDate
		,CASE WHEN t.PaymentMethod = 'Cash' THEN 'Cash' ELSE  COALESCE(t.CheckCode,'') END AS PaymentCheck
		,t.Id
		,COALESCE(t.[Source],'') AS [Source]
		,COALESCE(t.[Type],'') AS [Type]
		,CASE WHEN t.[Type] IS NOT NULL THEN 1
			ELSE 0 END AS Visible
		,COALESCE(oap.Amount,0) AS OnAccountAmount
		,COALESCE(oap.Comment,0) AS OnAccountComment
		,COALESCE(CONVERT(NVARCHAR(10),oap.PostDate,101),'') AS OnAccountPostDate
		,CASE WHEN oap.PaymentMethod = 'Cash' THEN 'Cash' ELSE  COALESCE(oap.CheckCode,'') END AS OnAccountPaymentCheck
		,oap.Id AS OnAccountId
		,oap.PostDate AS OnAccountPostDateOrdered
		,COALESCE(oap.[Source],'') AS OnAccountSource
		,COALESCE(oap.[Type],'') AS OnAccountType
		,CASE WHEN oap.[Type] IS NOT NULL THEN 1
			ELSE 0 END AS OnAccountVisible
	FROM model.invoices i
	INNER JOIN model.encounters e ON e.id = i.EncounterId
	INNER JOIN model.patients p ON p.id = e.PatientId
	INNER JOIN model.billingservices bs ON bs.InvoiceId = i.id
	INNER JOIN model.encounterservices es ON es.Id = bs.EncounterServiceId
	LEFT JOIN model.PatientInsurances pin ON pin.InsuredPatientId = p.Id
	LEFT JOIN model.InsurancePolicies ip ON ip.Id = pin.InsurancePolicyId
	LEFT JOIN model.patients ph ON ph.Id = ip.PolicyHolderPatientId
	LEFT JOIN model.PatientAddresses pa ON pa.patientid = p.id
		AND pa.PatientAddressTypeId = 1
	LEFT JOIN model.StateOrProvinces sop ON sop.id = pa.StateOrProvinceId
	LEFT JOIN model.PatientAddresses pah ON pah.patientid = ph.id
		AND pa.PatientAddressTypeId = 1
	LEFT JOIN model.StateOrProvinces soph ON soph.id = pah.StateOrProvinceId
	LEFT JOIN model.patientcommunicationpreferences pcp ON pcp.patientid = p.id
	LEFT JOIN #SumCharges sc ON sc.Id = bs.Id
	LEFT JOIN #Adjustments tempad ON tempad.Id = bs.Id
	LEFT JOIN #Totals t ON t.BillingService = bs.Id
	LEFT JOIN #OnAccountPayments oap ON oap.PatientId = e.PatientId
	WHERE p.id = @PatientId
	AND (pah.OrdinalId = 1 OR pah.OrdinalId IS NULL) 
	AND (	(
				((@StartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) >= @StartDate))
				AND 
				((@EndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),e.StartDateTime, 101)) <= @EndDate))
			)
			AND
			(
				((@PaymentStartDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),t.PostDate, 101)) >=  @PaymentStartDate))
				AND 
				((@PaymentEndDate IS NULL) OR (CONVERT(datetime, CONVERT(nvarchar(10),t.PostDate, 101)) <=  @PaymentEndDate))
			)
		)
END
GO
 -- exec model.GetAccountHistoryDetail 204432, NULL,NULL
