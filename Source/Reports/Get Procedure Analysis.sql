IF OBJECT_ID('model.GetProcedureAnalysis') IS NOT NULL
	DROP PROCEDURE model.GetProcedureAnalysis

GO

CREATE PROCEDURE model.GetProcedureAnalysis(
							@StartDate datetime, 
							@EndDate datetime,
							@BillingOrganizations NVARCHAR(MAX), 
							@Providers NVARCHAR(MAX), 
							@ServiceLocations NVARCHAR(MAX), 
							@ServiceCategories NVARCHAR(MAX), 
							@ServiceCodes NVARCHAR(MAX),
							@BreakdownByBillingOrganization bit,
							@BreakdownByProvider bit,
							@BreakdownByServiceLocation bit,
							@BreakdownByServiceCategory bit
							)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SET @StartDate = CONVERT(datetime, CONVERT(nvarchar(10),@StartDate, 101))
SET @EndDate = CONVERT(datetime, CONVERT(nvarchar(10),@EndDate, 101))

DECLARE @Name NVARCHAR(50)
DECLARE @Line1 NVARCHAR(50)
DECLARE @Line2 NVARCHAR(50)
DECLARE @City NVARCHAR(50)
DECLARE @Abbreviation NVARCHAR(10)
DECLARE @PostalCode NVARCHAR(20)

SELECT @Name = bo.Name
	,@Line1 = boa.Line1
	,@Line2 = boa.Line2
	,@City = boa.City
	,@Abbreviation = sop.Abbreviation, 
	@PostalCode = SUBSTRING(boa.PostalCode,0,6) + '-' + SUBSTRING(boa.PostalCode,6,4)
FROM model.BillingOrganizations bo
INNER JOIN model.BillingOrganizationAddresses boa ON bo.Id = boa.BillingOrganizationId
	AND boa.BillingOrganizationAddressTypeId = 5
INNER JOIN model.StateOrProvinces sop ON sop.Id = boa.StateOrProvinceId
WHERE bo.IsMain = 1

SELECT DISTINCT CONVERT(datetime,CONVERT(nvarchar(10),en.StartDateTime,101),101) AS InvoiceDate	
	,bs.Id AS BillingServiceId
	,inv.Id AS InvoiceId
	,COALESCE(es.Code, 'No Code') AS ServiceCode
	,bs.Unit AS Qty
	,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS Charges
	,COALESCE(bo.ShortName,'No Billing Organization') AS BillingOrganization
	,COALESCE(usBill.UserName,'No Provider') AS BillToDr
	,COALESCE(sl.ShortName,'No Service Location') AS ServiceLocation
	,COALESCE(est.NAME,'No Service Category') AS CodeCategory
INTO #ProcAll
FROM model.BillingServices bs
INNER JOIN model.Invoices inv ON bs.InvoiceId = inv.Id
INNER JOIN model.encounters en ON en.id = inv.EncounterId
LEFT JOIN model.servicelocations sl ON sl.id = inv.AttributeToServiceLocationId
LEFT JOIN model.providers bp ON bp.id = inv.BillingProviderId
LEFT JOIN model.BillingOrganizations bo ON bo.Id = bp.BillingOrganizationId
LEFT JOIN model.Users usBill ON bp.UserId = usBill.Id
LEFT JOIN model.EncounterServices es ON es.id = bs.EncounterServiceId
LEFT JOIN model.EncounterServiceTypes est ON es.EncounterServiceTypeId = est.id
WHERE  ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations)))
		OR (@BillingOrganizations IS NOT NULL AND (bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))))
	AND ((@Providers IS NULL AND (usBill.Id IS NULL OR usBill.Id IN (SELECT Id FROM model.Users)))
		OR (@Providers IS NOT NULL AND (usBill.ID IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,',')))))
	AND ((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations)))
		OR (@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@ServiceCategories IS NULL and (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes)))
		OR (@ServiceCategories IS NOT NULL AND (est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,',')))))
	AND ((@ServiceCodes IS NULL and (es.Id IS NULL OR es.Id IN (SELECT Id FROM model.EncounterServices)))
		OR (@ServiceCodes IS NOT NULL AND (es.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCodes,',')))))
		
DECLARE @PreviousYearStartDate datetime
SET @PreviousYearStartDate = DATEADD(YY,-1, (SELECT CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)))
DECLARE @CurrentYearStartDate datetime
SET @CurrentYearStartDate = CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)				
DECLARE @PreviousYearEndDate datetime
SET @PreviousYearEndDate = DATEADD(DD,-1, @CurrentYearStartDate)
DECLARE @PreviousYTDEndDate datetime
SET @PreviousYTDEndDate = DATEADD(YY,-1,@EndDate)

DECLARE @TotalCurrentCharges decimal(20,2)
SELECT @TotalCurrentCharges = COALESCE(SUM(Charges),0)
FROM #ProcAll
WHERE InvoiceDate BETWEEN @StartDate AND @EndDate

DECLARE @TotalCurrentYTDCharges decimal(20,2)
SELECT @TotalCurrentYTDCharges = COALESCE(SUM(Charges),0)
FROM #ProcAll
WHERE InvoiceDate BETWEEN @CurrentYearStartDate AND @EndDate

DECLARE @TotalPreviousYearCharges decimal(20,2)
SELECT @TotalPreviousYearCharges = COALESCE(SUM(Charges),0)
FROM #ProcAll pa
WHERE pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate

DECLARE @TotalPreviousYTDCharges decimal(20,2)
SELECT @TotalPreviousYTDCharges = COALESCE(SUM(Charges),0)
FROM #ProcAll pa
WHERE pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate

SELECT 
	v.BillingOrganization
	,v.BillToDr
	,v.ServiceLocation
	,v.CodeCategory
	,v.ServiceCode
	,SUM(v.Qty) AS Qty
	,SUM(v.Charges) AS Charges
	,SUM(v.QtyYTD)  AS QtyYTD
	,SUM(v.ChargesYTD) AS ChargesYTD
	,SUM(v.QtyPrevYTD) AS QtyPrevYTD
	,SUM(v.ChargesPrevYTD) AS ChargesPrevYTD
	,SUM(v.QtyPrevYear) AS QtyPrevYear
	,SUM(v.ChargesPrevYear) AS ChargesPrevYear
	,@TotalCurrentCharges AS TotalCurrentCharges
	,@TotalCurrentYTDCharges AS TotalCurrentYTDCharges
	,@TotalPreviousYearCharges AS TotalPreviousYearCharges
	,@TotalPreviousYTDCharges AS TotalPreviousYTDCharges
	,@Name AS Name
	,@Line1 AS Line1
	,@Line2 AS Line2
	,@City AS City
	,@Abbreviation AS Abbreviation
	,@PostalCode AS PostalCode
FROM (
	SELECT DISTINCT
		CASE WHEN @BreakdownByBillingOrganization = 0 THEN NULL ELSE pa.BillingOrganization END AS BillingOrganization
		,CASE WHEN @BreakdownByProvider = 0 THEN NULL ELSE pa.BillToDr END AS BillToDr 
		,CASE WHEN @BreakdownByServiceLocation = 0 THEN NULL ELSE pa.ServiceLocation END AS ServiceLocation
		,CASE WHEN @BreakdownByServiceCategory = 0 THEN NULL ELSE pa.CodeCategory END AS CodeCategory
		,pa.BillingServiceId
		,pa.InvoiceId
		,pa.ServiceCode
		,CASE WHEN pa.InvoiceDate BETWEEN @StartDate AND @EndDate
			THEN COALESCE(pa.Qty,0) ELSE 0 END AS Qty
		,CASE WHEN pa.InvoiceDate BETWEEN @StartDate AND @EndDate
			THEN COALESCE(pa.Charges, 0) ELSE 0 END AS Charges
		,CASE WHEN pa.InvoiceDate BETWEEN @CurrentYearStartDate AND @EndDate
			THEN COALESCE(pa.Qty,0) ELSE 0 END AS QtyYTD
		,CASE WHEN pa.InvoiceDate BETWEEN @CurrentYearStartDate AND @EndDate
			THEN COALESCE(pa.Charges,0) ELSE 0 END AS ChargesYTD
		,CASE WHEN pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate
			THEN COALESCE(pa.Qty,0) ELSE 0 END AS QtyPrevYear
		,CASE WHEN pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate
			THEN COALESCE(pa.Charges,0) ELSE 0 END AS ChargesPrevYear
		,CASE WHEN pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate
			THEN COALESCE(pa.Qty,0) ELSE 0 END AS QtyPrevYTD
		,CASE WHEN pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate
			THEN COALESCE(pa.Charges,0) ELSE 0 END AS ChargesPrevYTD
	FROM #ProcAll pa
	WHERE pa.InvoiceDate BETWEEN @PreviousYearStartDate AND @EndDate
	) v 
	GROUP BY v.BillingOrganization,v.BillToDr,v.ServiceLocation,v.CodeCategory,v.ServiceCode

DROP TABLE #ProcAll
END
RETURN
END
GO

-- exec model.GetProcedureAnalysis '2014-01-01 00:00:00','2014-06-20 00:00:00',NULL,NULL,NULL,NULL,NULL,1,1,1,1
