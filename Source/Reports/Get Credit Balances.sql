IF OBJECT_ID('model.GetCreditBalance') IS NOT NULL
	DROP PROCEDURE model.GetCreditBalance
GO

CREATE PROCEDURE model.GetCreditBalance (@StartDate DATETIME,@EndDate DATETIME)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT e.Id AS EncounterId
		,SUM(CONVERT(INT,bs.Unit) * bs.UnitCharge) AS SumCharges
	INTO #SumCharges
	FROM model.BillingServices bs
	INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	WHERE e.StartDateTime BETWEEN @StartDate AND @EndDate
	GROUP BY e.Id
	ORDER BY e.Id

	SELECT EncounterId
		,CASE WHEN SumAdjustments IS NULL THEN 0 ELSE SumAdjustments END AS SumAdjustments
	INTO #SumAdjustments
	FROM (
		SELECT e.Id AS EncounterId
			,SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS SumAdjustments
		FROM model.Invoices i
		INNER JOIN model.Encounters e ON e.Id = i.EncounterId
		INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
		LEFT JOIN model.Adjustments adj ON adj.BillingServiceId = bs.Id
		LEFT JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
		WHERE e.StartDateTime BETWEEN @StartDate AND @EndDate
		GROUP BY e.Id
	) v

	SELECT EncounterId
		,CASE WHEN AmountSent IS NULL THEN 0 ELSE AmountSent END AS AmountSent
	INTO #AmountSent
	FROM (
		SELECT e.Id AS EncounterId, bst.BillingServiceTransactionStatusId
			,SUM(bst.AmountSent) AS AmountSent
		FROM model.Invoices i
		INNER JOIN model.Encounters e ON e.Id = i.EncounterId
		INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
		INNER JOIN model.BillingServiceTransactions bst ON bst.BillingServiceId = bs.Id
		WHERE e.StartDateTime BETWEEN @StartDate AND @EndDate
			AND (bst.BillingServiceTransactionStatusId IS NULL OR bst.BillingServiceTransactionStatusId IN (1,2,3,4))
		GROUP BY e.Id,bst.BillingServiceTransactionStatusId
	) v

	SELECT DISTINCT p.Id AS PatientId
		,sc.SumCharges - sa.SumAdjustments AS OpenBalance
		,sc.SumCharges
		,sa.SumAdjustments
		,COALESCE(ams.AmountSent,0) AS AmountSent
		,i.Id AS InvoiceId
		,e.Id AS EncounterId
		,CASE	
			WHEN sc.SumCharges - sa.SumAdjustments <> 0
				THEN sc.SumCharges - sa.SumAdjustments - ams.AmountSent
			WHEN sc.SumCharges - sa.SumAdjustments = 0
				THEN 0
			END AS UnbilledAmount
		,p.LastName AS LastName
		,p.FirstName AS FirstName
		,e.StartDateTime AS DateOfService
	INTO #FinalTable
	FROM model.Invoices i
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.Patients p ON p.Id = e.PatientId
	INNER JOIN #SumCharges sc ON sc.EncounterId = e.Id
	LEFT JOIN #SumAdjustments sa ON sa.EncounterId = e.Id
	LEFT JOIN #AmountSent ams ON ams.EncounterId = e.Id

	SELECT 
		CASE WHEN OpenBalance IS NULL THEN 0 ELSE OpenBalance END AS OpenBalance
		,CONVERT(NVARCHAR,PatientId) + '-' + CONVERT(NVARCHAR,EncounterId) AS Invoice
		,CASE WHEN UnbilledAmount IS NULL THEN 0 ELSE UnbilledAmount END AS UnbilledAmount
		,PatientId
		,LastName
		,FirstName
		,CONVERT(NVARCHAR(10),DateOfService,101) AS DateOfService
	FROM #FinalTable 
	WHERE OpenBalance < 0
	AND DateOfService BETWEEN @StartDate AND @EndDate
	ORDER BY LastName, FirstName, InvoiceId
	
	DROP TABLE #FinalTable
	DROP TABLE #SumAdjustments
	DROP TABLE #SumCharges
	DROP TABLE #AmountSent
END	
GO

-- exec model.GetCreditBalance '2013-01-01 00:00:00','2014-07-01 00:00:00'