IF OBJECT_ID('model.GetUnassignedPayments') IS NOT NULL
	DROP PROCEDURE model.GetUnassignedPayments
GO

CREATE PROCEDURE model.GetUnassignedPayments (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@ServiceLocations NVARCHAR(MAX)
	,@Providers NVARCHAR(MAX)
	,@BreakdownByProvider bit
	,@BreakdownByServiceLocation bit
	)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
	SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

	SELECT p.Id AS ProviderId,
		COALESCE(UserId,ServiceLocationId) AS UserId, 
		COALESCE(u.UserName,sl.ShortName) AS Provider
	INTO #Providers
	FROM model.Providers p
	LEFT JOIN model.Users u ON u.Id = p.UserId
	LEFT JOIN model.ServiceLocations sl ON sl.Id = p.ServiceLocationId

SELECT DISTINCT
	 p.LastName
	,p.FirstName
	,p.Id AS PatientId
	,CONVERT(nvarchar(10),e.StartDateTime,101) AS InvoiceDate
	,CONVERT(nvarchar(10),a.PostedDateTime,101) AS PostedDate
	,CASE WHEN @BreakdownByServiceLocation = 1 THEN sl.ShortName ELSE NULL END AS ServiceLocation
	,CASE WHEN @BreakdownByProvider = 1 THEN pr.Provider ELSE NULL END AS Provider
	,CASE 
		WHEN fb.FinancialSourceTypeId = 1
			THEN ins.NAME
		WHEN fb.FinancialSourceTypeId = 3
			THEN 'Office'
		WHEN fb.FinancialSourceTypeId = 2
			THEN  pPayer.LastName + ', ' + pPayer.FirstName
		ELSE 'Other'
		END AS Payer
	,a.Id AS AdjustmentId
	,CAST(CASE WHEN at.IsDebit = 1 THEN -a.Amount ELSE a.Amount END AS DECIMAL(10,2)) AS Amount
FROM model.Adjustments a
INNER JOIN model.AdjustmentTypes at ON at.Id = a.AdjustmentTypeId
INNER JOIN model.InvoiceReceivables ir ON ir.Id = a.InvoiceReceivableId
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN #Providers pr ON pr.ProviderId = i.BillingProviderId
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
INNER JOIN model.ServiceLocations sl ON sl.Id = e.ServiceLocationId
INNER JOIN model.Patients p ON p.Id = e.PatientId
INNER JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
LEFT JOIN model.Insurers ins ON ins.Id = fb.InsurerId
LEFT JOIN model.Patients pPayer ON pPayer.Id = fb.PatientId
WHERE BillingServiceId IS NULL
	AND InvoiceReceivableId IS NOT NULL
	AND a.Amount <> 0
	AND CONVERT(DATETIME,CONVERT(NVARCHAR(10),a.PostedDateTime,101)) BETWEEN @StartDate AND @EndDate
	AND ((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR 
		(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Providers IS NULL AND (pr.UserId IS NULL OR pr.UserId IN (SELECT DISTINCT UserId FROM #Providers))) OR 
		(@Providers IS NOT NULL AND pr.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,','))))
ORDER BY p.LastName
	,p.FirstName
	,p.Id
END
GO

-- exec model.GetUnassignedPayments '2014-12-09 00:00:00','2014-12-0 00:00:00',NULL,NULL,0,0