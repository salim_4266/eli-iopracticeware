IF OBJECT_ID('model.GetPatientBillHold') IS NOT NULL
	DROP PROCEDURE model.GetPatientBillHold

GO

-- stored procedures need parameters for Telerik to run or
-- it will be run as "exec model.GetPatientBillHold default" and break
CREATE PROCEDURE model.GetPatientBillHold (@Unnecessary bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
--for version 8:

	SELECT Id AS BillingServiceId, SUM(CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2))) AS Charges
	INTO #SumCharges
	FROM model.BillingServices bs
	GROUP BY bs.Id

	SELECT Id AS BillingServiceId, SUM(Adjustments) AS Adjustments 
	INTO #SumAdjustments
	FROM (
		SELECT bs.Id, CASE WHEN at.IsDebit = 1 THEN - a.Amount ELSE a.Amount END AS Adjustments
		FROM model.Adjustments a
		INNER JOIN model.BillingServices bs ON bs.Id = a.BillingServiceId
		INNER JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
	) v
	GROUP BY Id

	SELECT DISTINCT bst.BillingServiceId, 
					bst.BillingServiceTransactionStatusId,
					CONVERT(NVARCHAR(10),COALESCE(bs.PatientBillDate,bst.DateTime),101) AS BilledDate,
					SUM(AmountSent) AS AmountSent
	INTO #AmountSent
	FROM model.BillingServiceTransactions bst
	INNER JOIN model.BillingServices bs ON bs.Id = bst.BillingServiceId
	WHERE bst.BillingServiceTransactionStatusId IN (1,2,3,4) 
	GROUP BY bst.BillingServiceId,bst.BillingServiceTransactionStatusId,bs.PatientBillDate,bst.DateTime

	SELECT e.PatientId, c.BillingServiceId
		,c.Charges - COALESCE(a.Adjustments,0) - COALESCE(as1.AmountSent,0) AS Total
		,COALESCE(c.Charges,0) AS Charges
		,COALESCE(a.Adjustments,0) AS Adjustments
		,COALESCE(as1.AmountSent,0) AS AmountSent
	INTO #Totals
	FROM model.BillingServices bs
	INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN #SumCharges c ON c.BillingServiceId = bs.Id
	LEFT JOIN #SumAdjustments a ON c.BillingServiceId = a.BillingServiceId
	LEFT JOIN #AmountSent as1 ON as1.BillingServiceId = c.BillingServiceId

	SELECT PatientId, SUM(Total) AS Total
	INTO #Unbilled
	FROM #Totals
	GROUP BY PatientId

SELECT v.PatientName, v.Id, v.PatientStatus, COALESCE(SUM(v.OpenBalance),0) AS OpenBalance, v.Unbilled 
FROM (
	SELECT DISTINCT p.LastName + COALESCE(' ' + p.Suffix, '') + ', ' + p.FirstName + ' ' + COALESCE(p.MiddleName,'') AS PatientName
		,p.Id
		, CASE p.PatientStatusId WHEN 1 THEN 'Active'
			WHEN 2 THEN 'Closed, no activity'
			WHEN 3 THEN 'Closed, deceased'
			WHEN 4 THEN 'Closed, moved'
			END AS PatientStatus
		,bs.Id AS BillingService
		,bst.AmountSent AS OpenBalance
		,CONVERT(NVARCHAR(10),COALESCE(bs.PatientBillDate,bst.DateTime),101) AS BilledDate
		,bst.BillingServiceTransactionStatusId
		,t.Total AS Unbilled
	FROM model.Invoices i
	INNER JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.BillingServiceTransactions bst ON bst.BillingServiceId = bs.Id
		AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
		AND ir.PatientInsuranceId IS NULL
	INNER JOIN model.Patients p ON p.Id = e.PatientId
	INNER JOIN model.PatientCommunicationPreferences pcp ON pcp.PatientId = p.Id
		AND pcp.PatientCommunicationTypeId = 3
		AND pcp.IsOptOut = 1
	LEFT JOIN #Unbilled t ON t.PatientId = p.Id
) v
WHERE v.OpenBalance > 0 OR v.Unbilled > 0
GROUP BY v.PatientName, v.Id, v.PatientStatus, v.Unbilled

END

-- exec model.GetPatientBillHold 0