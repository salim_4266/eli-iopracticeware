IF OBJECT_ID('model.GetReferringDoctorDetail') IS NOT NULL
	DROP PROCEDURE model.GetReferringDoctorDetail

GO

CREATE PROCEDURE model.GetReferringDoctorDetail(
		@StartDate datetime
		,@EndDate datetime
		,@BillingOrganizations nvarchar(max) = NULL
		,@ServiceLocations nvarchar(MAX) = NULL
		,@ReferringProviders nvarchar(max) = NULL		
		,@ScheduledProviders nvarchar(MAX) = NULL
		,@ServiceCategories nvarchar(MAX) = NULL
		,@ServiceCodes nvarchar(MAX) = NULL
		,@ShowScheduledProviderBreakdown bit
		,@ShowServiceCategoryBreakdown bit
		,@ShowServiceCodeBreakdown bit
		,@ShowPatientBreakdown bit)

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT bs.Id AS BillingService
	,bs.InvoiceId AS Invoice
	,e.PatientId
	,bs.Unit AS Qty
	,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS Charges
	,CONVERT(datetime,CONVERT(nvarchar(10),e.StartDateTime,101)) AS StartDateTime
INTO #ChargesByBillingService
FROM model.BillingServices bs
INNER JOIN model.Invoices i ON bs.InvoiceId = i.Id
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE CONVERT(datetime,CONVERT(nvarchar(10),e.StartDateTime,101)) BETWEEN DATEADD(YY,-1,@StartDate) AND @EndDate

SELECT 
	a.BillingServiceId AS BillingService
	,e.PatientId
	,CASE WHEN at.IsDebit = 0 THEN a.Amount ELSE -a.Amount END AS Amount
	,CONVERT(DATETIME,CONVERT(NVARCHAR(10),a.PostedDateTime,101)) AS PostedDateTime
INTO #PaymentsByBillingService
FROM model.Adjustments a
INNER JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
INNER JOIN model.BillingServices bs ON bs.Id = a.BillingServiceId
INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE at.IsCash = 1
AND CONVERT(DATETIME,CONVERT(NVARCHAR(10),a.PostedDateTime,101)) BETWEEN DATEADD(YY,-1,@StartDate) AND @EndDate

SELECT pep.ExternalProviderId, SUM(Charges) AS Charges
INTO #PriorCharges
FROM #ChargesByBillingService cbbs 
INNER JOIN model.Invoices i ON i.Id = cbbs.Invoice
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
INNER JOIN model.Patients p ON p.Id = e.PatientId
INNER JOIN model.PatientExternalProviders pep ON pep.PatientId = p.Id
	AND pep.IsReferringPhysician = 1
WHERE cbbs.StartDateTime BETWEEN DATEADD(YY,-1,@StartDate) AND DATEADD(DD,-1,@StartDate)
GROUP BY pep.ExternalProviderId

SELECT pep.ExternalProviderId, SUM(pbbs.Amount) AS Amount
INTO #PriorPayments
FROM #PaymentsByBillingService pbbs
INNER JOIN model.BillingServices bs ON bs.Id = pbbs.BillingService
INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
INNER JOIN model.Patients p ON p.Id = e.PatientId
INNER JOIN model.PatientExternalProviders pep ON pep.PatientId = p.Id
	AND pep.IsReferringPhysician = 1
WHERE pbbs.PostedDateTime BETWEEN DATEADD(YY,-1,@StartDate) AND DATEADD(DD,-1,@StartDate)
GROUP BY pep.ExternalProviderId

SELECT pbbs.BillingService, SUM(pbbs.Amount) AS Amount
INTO #CurrentPayments
FROM #PaymentsByBillingService pbbs
WHERE pbbs.PostedDateTime BETWEEN @StartDate AND @EndDate
GROUP BY pbbs.PatientId, pbbs.BillingService

SELECT BillingService, SUM(Qty) AS Qty, SUM(Charges) AS Charges
INTO #CurrentCharges
FROM #ChargesByBillingService cbbs 
WHERE cbbs.StartDateTime BETWEEN @StartDate AND @EndDate
GROUP BY cbbs.BillingService

SELECT 
	CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.BillingService ELSE '' END AS BillingService
	,r.ReferringProvider
	,CASE WHEN @ShowScheduledProviderBreakdown = 1 THEN r.ScheduledProvider ELSE '' END AS ScheduledProvider
	,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN r.ServiceCategory ELSE '' END AS ServiceCategory
	,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.ServiceCode ELSE '' END AS ServiceCode
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientId ELSE '' END AS PatientID
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientName ELSE '' END AS PatientName
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.DateOfBirth ELSE '' END AS DateOfBirth
	,SUM(COALESCE(r.Qty,0)) AS Qty
	,SUM(r.Charges) AS Charges
	,SUM(r.Payments) AS Payments
	,SUM(r.PriorCharges) AS PriorCharges
	,SUM(r.PriorPayments) AS PriorPayments
FROM (
	SELECT 
	  DISTINCT 
		bs.Id AS BillingService
		,ec.DisplayName AS ReferringProvider
		,COALESCE(sp.UserName,'') AS ScheduledProvider
		,COALESCE(est.Name,'Uncategorized') AS ServiceCategory
		,es.Code + ' - ' + es.[Description] AS ServiceCode
		,CAST(e.PatientId AS nvarchar(20)) AS PatientId
		,p.LastName + ', ' + p.FirstName AS PatientName
		,CONVERT(nvarchar(10),p.DateOfBirth,101) AS DateOfBirth
		,cc.Qty AS Qty
		,COALESCE(cc.Charges,0) AS Charges
		,COALESCE(cp.Amount,0) AS Payments
		,COALESCE(pc.Charges,0) AS PriorCharges
		,COALESCE(pp.Amount,0) AS PriorPayments
	FROM #CurrentCharges cc
	INNER JOIN model.BillingServices bs ON bs.Id = cc.BillingService
	INNER JOIN model.Invoices i ON bs.InvoiceId = i.Id
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.Patients p ON p.Id = e.PatientId
	INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	INNER JOIN model.EncounterServiceTypes est ON est.Id = es.EncounterServiceTypeId
	INNER JOIN model.ServiceLocations sl ON sl.Id = i.AttributeToServiceLocationId
	INNER JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	INNER JOIN model.BillingOrganizations bo ON bo.Id = pr.BillingOrganizationId
	INNER JOIN model.PatientExternalProviders pep ON pep.PatientId = p.Id
		AND pep.IsReferringPhysician = 1
	INNER JOIN model.ExternalContacts ec ON ec.Id = pep.ExternalProviderId
	INNER JOIN model.Users sp ON sp.Id = pr.UserId
	LEFT JOIN #PriorCharges pc ON pep.ExternalProviderId = pc.ExternalProviderId
	LEFT JOIN #PriorPayments pp ON pep.ExternalProviderId = pp.ExternalProviderId
	LEFT JOIN #CurrentPayments cp ON bs.Id = cp.BillingService
	WHERE 
		 ((@BillingOrganizations IS NULL AND bo.Id IN (SELECT Id FROM model.BillingOrganizations)) OR
			(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))) AND
		((@ReferringProviders IS NULL AND ec.Id IN (SELECT Id FROM model.ExternalContacts)) OR
			(@ReferringProviders IS NOT NULL AND ec.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ReferringProviders,',')))) AND
		((@ScheduledProviders IS NULL AND sp.Id IN (SELECT Id FROM model.Users)) OR
			(@ScheduledProviders IS NOT NULL AND sp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ScheduledProviders,',')))) AND
		((@ServiceLocations IS NULL AND sl.Id IN (SELECT Id FROM model.ServiceLocations)) OR
			(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,',')))) AND
		((@ServiceCategories IS NULL AND (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes))) OR
			(@ServiceCategories IS NOT NULL AND (est.Id IS NULL OR est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,','))))) AND
		((@ServiceCodes IS NULL AND es.Code IN (SELECT Code FROM model.EncounterServices)) OR
			(@ServiceCodes IS NOT NULL AND es.Code IN (SELECT CONVERT(nvarchar,nstr) FROM CharTable(@ServiceCodes,','))))
) r
GROUP BY 
	r.ReferringProvider
	,r.ScheduledProvider,r.ServiceCategory,r.ServiceCode,r.BillingService,r.PatientId,r.PatientName,r.DateOfBirth

DROP TABLE #ChargesByBillingService
DROP TABLE #PaymentsByBillingService
DROP TABLE #PriorCharges
DROP TABLE #PriorPayments
DROP TABLE #CurrentPayments

END
END
GO

-- EXEC model.GetReferringDoctorDetail '2014-12-24','2014-12-24',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,1