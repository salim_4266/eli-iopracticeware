IF (OBJECT_ID(N'model.GetGlassesInventory','P')) IS NOT NULL
	DROP PROCEDURE [model].[GetGlassesInventory] 
GO

CREATE PROCEDURE [model].[GetGlassesInventory] (@Unnecessary bit)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
	SELECT PCInventory.SKU, 
	PCInventory.Type_, 
	PCInventory.Manufacturer, 
	PCInventory.Model, 
	PCInventory.Color, 
	PCInventory.LensTint, 
	PCInventory.Qty, 
	PCInventory.Cost, 
	PCInventory.WCost,
	PCInventory.Qty * PCInventory.Cost AS SaleValue,
	PCInventory.Qty * PCInventory.WCost AS WholesaleValue
	FROM dbo.PCInventory PCInventory
	WHERE PCInventory.Qty > 0
	ORDER BY PCInventory.Manufacturer ASC,
	PCInventory.Type_ ASC
END

GO

-- exec model.GetGlassesInventory 0