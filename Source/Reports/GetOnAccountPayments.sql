IF OBJECT_ID('model.GetOnAccountPayments') IS NOT NULL
	DROP PROCEDURE model.GetOnAccountPayments
GO 

CREATE PROCEDURE model.GetOnAccountPayments(@StartDate datetime, 
											@EndDate datetime, 
											@PaymentMethods NVARCHAR(MAX), 
											@Users NVARCHAR(MAX), 
											@BreakdownByPaymentMethod bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

DECLARE @boName NVARCHAR(MAX)
DECLARE @boLine1 NVARCHAR(MAX)
DECLARE @boLine2 NVARCHAR(MAX)
DECLARE @boCity NVARCHAR(MAX)
DECLARE @boState NVARCHAR(10)
DECLARE @boZip NVARCHAR(20)

SELECT @boName = bo.Name, @boLine1 = Line1, @boLine2 = Line2, @boCity = City, @boState = sop.Abbreviation, 
	@boZip = CASE WHEN LEN(PostalCode) > 5 THEN SUBSTRING(PostalCode, 0,6) + '-' + SUBSTRING(PostalCode, 6,4) ELSE PostalCode END 
FROM model.BillingOrganizations bo
INNER JOIN model.BillingOrganizationAddresses boa ON bo.Id = boa.BillingOrganizationId
	AND boa.BillingOrganizationAddressTypeId = 5
INNER JOIN model.StateOrProvinces sop ON sop.Id = boa.StateOrProvinceId
WHERE bo.IsMain = 1

SELECT Id, UserId, UserName
INTO #AuditUser
FROM (
	SELECT ROW_NUMBER () OVER (PARTITION BY a.Id ORDER BY ChangeTypeId DESC, AuditDateTime DESC) AS RANK,a.Id, ae.UserId,u.UserName
	FROM model.Adjustments a 
	LEFT JOIN AuditEntries ae ON ae.KeyValueNumeric = a.Id
	LEFT JOIN model.Users u ON u.Id = ae.UserId
	WHERE ObjectName = 'model.Adjustments'
	AND ae.ChangeTypeId = 0
	GROUP BY a.Id, ae.UserId, u.UserName,ChangeTypeId, AuditDateTime
) v 
WHERE RANK = 1

SELECT SUM(bst.AmountSent) AS PatientBalance,e.PatientId
INTO #PatientBalance
FROM model.BillingServiceTransactions bst
INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
AND ir.PatientInsuranceId IS NULL
INNER JOIN model.Invoices i ON i.Id = ir.InvoiceId
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE BillingServiceTransactionStatusId IN (1,2,3,4)
GROUP BY e.PatientId

SELECT DISTINCT @boName AS boName, @boLine1 AS boLine1, @boLine2 AS boLine2, @boCity AS boCity, @boState AS boState, @boZip AS boZip
	,a.Id
	,CASE WHEN a.FinancialSourceTypeId = 1 THEN COALESCE(i.Name,i2.Name)
		WHEN a.FinancialSourceTypeId = 2 THEN COALESCE(p.LastName + ', ' + p.FirstName,p2.LastName + ', ' + p2.FirstName)
		WHEN a.FinancialSourceTypeId = 3 THEN 'Office' ELSE 'Unknown' END AS Payer
	,a.Amount
	,PostedDateTime
	,pm.Name AS PaymentMethod
	,COALESCE(au.UserName,'UNKNOWN') AS Operator
	,ISNULL(pbal.PatientBalance,'0') AS PatientBalance
	,ISNULL(p.Id,p2.Id) As PatientId
FROM model.Adjustments a
LEFT JOIN #AuditUser au ON au.Id = a.Id
LEFT JOIN model.Patients p ON p.Id = a.PatientId
LEFT JOIN model.Insurers i ON i.Id = a.InsurerId
LEFT JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
INNER JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
LEFT JOIN model.Patients p2 ON p2.Id = fb.PatientId
LEFT JOIN model.Insurers i2 ON i2.Id = fb.InsurerId
LEFT JOIN #PatientBalance pbal on pbal.PatientId = a.PatientId
WHERE InvoiceReceivableId IS NULL AND a.AdjustmentTypeId = 1 AND a.BillingServiceId IS NULL AND (a.PatientId IS NOT NULL OR a.PatientId <> '')
AND CONVERT(DATETIME,CONVERT(NVARCHAR(10),PostedDateTime,101)) BETWEEN @StartDate AND @EndDate
AND a.Amount <> 0
AND ((@Users IS NULL AND (au.UserId IS NULL OR au.UserId IN (SELECT UserId FROM #AuditUser)))
		OR (@Users IS NOT NULL AND (au.UserId IS NULL OR au.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,',')))))
AND ((@PaymentMethods IS NULL AND pm.Id IN (SELECT Id FROM model.PaymentMethods))
		OR (@PaymentMethods IS NOT NULL AND pm.ID IN (SELECT CONVERT(int,nstr) FROM CharTable(@PaymentMethods,','))))

END
RETURN

GO

-- EXEC model.GetOnAccountPayments '2013-01-01', '2014-05-15',NULL,NULL,0,0
