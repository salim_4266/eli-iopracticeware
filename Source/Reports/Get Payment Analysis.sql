IF OBJECT_ID('model.GetPaymentAnalysis') IS NOT NULL
	DROP PROCEDURE model.GetPaymentAnalysis

GO

CREATE PROCEDURE model.GetPaymentAnalysis(
		@StartDate datetime, 
		@EndDate datetime, 
		@BillingOrganizations NVARCHAR(MAX),
		@Providers NVARCHAR(MAX),
		@ServiceLocations NVARCHAR(MAX),
		@ServiceCategories NVARCHAR(MAX), 
		@ServiceCodes NVARCHAR(MAX),
		@ShowBillingOrganizationBreakdown bit,
		@ShowProviderBreakdown bit,
		@ShowServiceLocationBreakdown bit,
		@ShowServiceCategoryBreakdown bit,
		@ShowServiceCodeBreakdown bit
		)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT CONVERT(datetime,CONVERT(nvarchar(10),adj.PostedDateTime,101),101) AS AdjustmentDate	
	,adj.Id AS AdjustmentId
	,COALESCE(es.Code, 'No Service') AS ServiceCode
	,CASE WHEN at.IsDebit = 1 THEN - adj.Amount ELSE adj.Amount END AS Amount
	,COALESCE(bo.ShortName,'No Billing Organization') AS BillingOrganization
	,COALESCE(usBill.UserName,'No Billing Provider') AS BillToDr
	,COALESCE(sl.ShortName,'No Service Location') AS ServiceLocation
	,COALESCE(est.NAME,'Uncategorized') AS CodeCategory
	,COALESCE(pm.Name,'No Payment Method') AS PaymentMethod
INTO #payAll
FROM  model.Adjustments adj
INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	AND at.IsCash = 1
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
LEFT JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
LEFT JOIN model.Invoices inv ON inv.Id = ir.InvoiceId
LEFT JOIN model.Encounters en ON en.id = inv.EncounterId
LEFT JOIN model.ServiceLocations sl ON sl.id = inv.AttributeToServiceLocationId
LEFT JOIN model.BillingServices bs ON adj.BillingServiceId = bs.Id
LEFT JOIN model.providers bp ON bp.id = inv.BillingProviderId
LEFT JOIN model.BillingOrganizations bo ON bo.Id = bp.BillingOrganizationId
LEFT JOIN model.Users usBill ON bp.UserId = usBill.Id
LEFT JOIN model.EncounterServices es ON es.id = bs.EncounterServiceId
LEFT JOIN model.EncounterServiceTypes est ON es.EncounterServiceTypeId = est.id
WHERE ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations))) OR 
		(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,','))))
	AND ((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR 
		(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Providers IS NULL AND (usBill.Id IS NULL OR usBill.Id IN (SELECT UserId FROM model.Providers))) OR 
		(@Providers IS NOT NULL AND usBill.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,','))))
	AND ((@ServiceCategories IS NULL AND (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes))) OR 
		(@ServiceCategories IS NOT NULL AND est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,','))))
	AND ((@ServiceCodes IS NULL AND (es.Code IS NULL OR es.Code IN (SELECT Code FROM model.EncounterServices))) OR 
		(@ServiceCodes IS NOT NULL AND es.Code IN (SELECT CONVERT(nvarchar,nstr) FROM CharTable(@ServiceCodes,','))))

DECLARE @PreviousYearStartDate datetime
SET @PreviousYearStartDate = DATEADD(YY,-1, (SELECT CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)))
DECLARE @CurrentYearStartDate datetime
SET @CurrentYearStartDate = CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)				
DECLARE @PreviousYearEndDate datetime
SET @PreviousYearEndDate = DATEADD(YY,-1, (SELECT CONVERT(DATETIME, '12/31/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)))
DECLARE @PreviousYTDEndDate datetime
SET @PreviousYTDEndDate = DATEADD(YY,-1,@EndDate)
DECLARE @PreviousMTDStartDate datetime
SET @PreviousMTDStartDate = DATEADD(MM,-1,CONVERT(DATETIME, CONVERT(NVARCHAR(2), DATEPART(MONTH, @EndDate)) + '/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101))
DECLARE @PreviousMTDEndDate datetime
SET @PreviousMTDEndDate = DATEADD(MM,-1,@EndDate)
DECLARE @CurrentMTDStartDate datetime
SET @CurrentMTDStartDate = CONVERT(DATETIME, CONVERT(NVARCHAR(2), DATEPART(MONTH, @EndDate)) + '/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)
				
DECLARE @TotalCurrentPayments decimal(20,3)
SELECT @TotalCurrentPayments = SUM(Amount)
FROM #payAll
WHERE AdjustmentDate BETWEEN @StartDate AND @EndDate

DECLARE @TotalCurrentMTDPayments decimal(20,2)
SELECT @TotalCurrentMTDPayments = COALESCE(SUM(Amount),0)
FROM #payAll
WHERE AdjustmentDate BETWEEN @CurrentMTDStartDate AND @EndDate

DECLARE @TotalCurrentYTDPayments decimal(20,2)
SELECT @TotalCurrentYTDPayments = COALESCE(SUM(Amount),0)
FROM #payAll
WHERE AdjustmentDate BETWEEN @CurrentYearStartDate AND @EndDate

DECLARE @TotalPreviousMTDPayments decimal(20,2)
SELECT @TotalPreviousMTDPayments = COALESCE(SUM(Amount),0)
FROM #payAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousMTDStartDate AND @PreviousMTDEndDate

DECLARE @TotalPreviousYearPayments decimal(20,2)
SELECT @TotalPreviousYearPayments = COALESCE(SUM(Amount),0)
FROM #payAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate

DECLARE @TotalPreviousYTDPayments decimal(20,2)
SELECT @TotalPreviousYTDPayments = COALESCE(SUM(Amount),0)
FROM #payAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate

SELECT 
	v.BillingOrganization
	,v.BillToDr
	,v.ServiceLocation
	,v.CodeCategory
	,v.ServiceCode
	,v.PaymentMethod
	,SUM(v.Payments) AS Payments
	,@TotalCurrentPayments AS TotalCurrentPayments
	,SUM(v.PaymentsMTD) AS PaymentsMTD
	,@TotalCurrentMTDPayments AS TotalCurrentMTDPayments
	,SUM(v.PaymentsPrevMTD) AS PaymentsPrevMTD
	,@TotalPreviousMTDPayments AS TotalPreviousMTDPayments
	,CASE SUM(v.PaymentsPrevMTD) WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1),100 * ((SUM(v.PaymentsMTD) - SUM(v.PaymentsPrevMTD)) / SUM(v.PaymentsPrevMTD))) END AS MonthPercent
	,SUM(v.PaymentsYTD) AS PaymentsYTD
	,@TotalCurrentYTDPayments AS TotalCurrentYTDPayments
	,SUM(v.PaymentsPrevYTD) AS PaymentsPrevYTD
	,@TotalPreviousYTDPayments AS TotalPreviousYTDPayments
	,CASE SUM(v.PaymentsPrevYTD) WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1),(100 * (SUM(v.PaymentsYTD) - SUM(v.PaymentsPrevYTD)) / SUM(v.PaymentsPrevYTD))) END AS YearPercent
	,SUM(v.PaymentsPrevYear) AS PaymentsPrevYear
	,@TotalPreviousYearPayments AS TotalPreviousYearPayments
FROM (
	SELECT 
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN pa.BillingOrganization ELSE NULL END AS BillingOrganization
		,CASE WHEN @ShowProviderBreakdown = 1 THEN pa.BillToDr ELSE NULL END AS BillToDr
		,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN pa.ServiceLocation ELSE NULL END AS ServiceLocation
		,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN pa.CodeCategory ELSE NULL END AS CodeCategory
		,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN pa.ServiceCode ELSE NULL END AS ServiceCode
		,pa.PaymentMethod
		,SUM(COALESCE(payDates.Amount, 0)) AS Payments
		,SUM(COALESCE(payMTD.Amount, 0)) AS PaymentsMTD
		,SUM(COALESCE(payPrevMTD.Amount, 0)) AS PaymentsPrevMTD
		,SUM(COALESCE(payYTD.Amount, 0)) AS PaymentsYTD
		,SUM(COALESCE(payPrevYear.Amount, 0)) AS PaymentsPrevYear
		,SUM(COALESCE(payPrevYTD.Amount, 0)) AS PaymentsPrevYTD
	FROM #payAll pa
	LEFT JOIN #payAll payPrevYear ON pa.AdjustmentId = payPrevYear.AdjustmentId
		AND payPrevYear.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate
	LEFT JOIN #payAll payDates ON pa.AdjustmentId = payDates.AdjustmentId
		AND payDates.AdjustmentDate BETWEEN @StartDate AND @EndDate
	LEFT JOIN #payAll payMTD ON pa.AdjustmentId = payMTD.AdjustmentId
		AND payMTD.AdjustmentDate BETWEEN @CurrentMTDStartDate AND @EndDate
	LEFT JOIN #payAll payPrevMTD ON pa.AdjustmentId = payPrevMTD.AdjustmentId
		AND payPrevMTD.AdjustmentDate BETWEEN @PreviousMTDStartDate AND @PreviousMTDEndDate			
	LEFT JOIN #payAll payPrevYTD ON pa.AdjustmentId = payPrevYTD.AdjustmentId
		AND payPrevYTD.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate
	LEFT JOIN #payAll payYTD ON pa.AdjustmentId = payYTD.AdjustmentId
		AND payYTD.AdjustmentDate BETWEEN @CurrentYearStartDate	AND @EndDate
	WHERE pa.AdjustmentDate >= @PreviousYearStartDate
	GROUP BY CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN pa.BillingOrganization ELSE NULL END
		,CASE WHEN @ShowProviderBreakdown = 1 THEN pa.BillToDr ELSE NULL END
		,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN pa.ServiceLocation ELSE NULL END
		,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN pa.CodeCategory ELSE NULL END 
		,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN pa.ServiceCode ELSE NULL END
		,pa.PaymentMethod,pa.BillingOrganization,pa.BillToDr,pa.ServiceLocation,pa.CodeCategory,pa.ServiceCode
	) v
	GROUP BY 
		v.BillingOrganization,v.BillToDr,v.ServiceLocation,v.CodeCategory,v.ServiceCode,v.PaymentMethod


DROP TABLE #PayAll
END 
RETURN
END
GO

-- exec model.GetPaymentAnalysis '2014-02-01 00:00:00','2014-02-28 00:00:00',NULL,NULL,NULL,NULL,NULL,0,0,0,0,0
--N'1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,27,128,129,130,131,132,133,134,135,136,137,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,73,174,175,176,177,178,179,182,183,184,185,186,187,188,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,247,248,249,250,251,252,253,254,255,256,257,258,259,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,317,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,376,377,378,379,380,381,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,402,403,404,405,406,407,408,409,410,411,412,413,414,416,417,418,419,421,423,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,468,472,474,475,476,477,478,479,480,481,482,483,484,485,487,488,490,491,492,493,494,495,496,497,498,499,500,502,503,504,505,506,507,508,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,527,528,529,530,532,534,535,536,537,538,539,540,541,542,549,550,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,621,622,623,624,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,697,698,699,700,703,704,705,706,708,709,710,711,712,713,714,715,716,717,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,200002,200003,200005,200006,200007,200008,200009,300004'

