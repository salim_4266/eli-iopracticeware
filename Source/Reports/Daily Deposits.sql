IF OBJECT_ID('model.GetDailyDeposit') IS NOT NULL
	DROP PROCEDURE model.GetDailyDeposit
GO

CREATE PROCEDURE model.GetDailyDeposit (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@Providers NVARCHAR(MAX)
	,@PayerTypes NVARCHAR(MAX)
	)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
	SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

	IF @StartDate > @EndDate
		RAISERROR (
				'Invalid date range.'
				,16
				,1
				)
	ELSE
	BEGIN
		SELECT TOP 1 bo.Name AS OfficeName, boa.Line1 AS OfficeAddress, boa.City AS OfficeCity, sop.Abbreviation AS OfficeState, SUBSTRING(boa.PostalCode,0,6) AS OfficeZip
		INTO #Office FROM model.BillingOrganizations bo
		INNER JOIN model.BillingOrganizationAddresses boa ON bo.Id = boa.BillingOrganizationId
		INNER JOIN model.StateOrProvinces sop ON sop.ID = boa.StateOrProvinceId
		WHERE bo.IsMain = 1
		
		SELECT DISTINCT  v.PayerId,v.PayerType,v.PaymentMethod,SUM(v.Amount) AS Amount
			,v.PostedDateTime,v.PaymentCheck
			,CASE PaymentMethod 
									WHEN 'Check' THEN 1 
									WHEN 'Money Order' THEN 2 
									WHEN 'Endorsed Ins Check' THEN 3 
									WHEN 'Cash' THEN 4 END  AS PaymentRefOrder
			,v.Source,
			CASE WHEN PaymentMethod ='Cash'
				THEN 'Note'
				ELSE 'Check No.' END AS Title
				,o.OfficeName,o.OfficeAddress,o.OfficeCity,o.OfficeState,o.OfficeZip
		FROM (
			SELECT
				CASE WHEN paym.Name = 'Cash' THEN 0 
					ELSE
						CASE 
							WHEN (adj.FinancialSourceTypeId = 2)
								THEN fb.PatientId
							WHEN (adj.FinancialSourceTypeId = 1)
								THEN fb.InsurerId
							ELSE 0
						END 
					END AS PayerId
				,CONVERT(NVARCHAR(1), CASE 
						WHEN adj.FinancialSourceTypeId = 1
							THEN 'I'
						WHEN adj.FinancialSourceTypeId = 2
							THEN 'P'
						WHEN adj.FinancialSourceTypeId = 3
							THEN 'O'
						END) AS PayerType
				,adj.Amount AS Amount
				,CONVERT(varchar(10), adj.PostedDateTime, 101) AS PostedDateTime
				,CASE WHEN fb.CheckCode IS NULL THEN '' ELSE CONVERT(NVARCHAR, fb.CheckCode) END AS PaymentCheck
				,paym.Name AS PaymentMethod
				,'A' AS Status
				,CASE WHEN paym.Name = 'Cash' THEN 'Cash'
					ELSE 
						CASE WHEN adj.FinancialSourceTypeId = 1
							THEN insur.Name
						WHEN adj.FinancialSourceTypeId = 2
							THEN p.LastName + ', ' + p.FirstName
						ELSE 'Office'
						END
					END AS Source
			FROM  model.adjustments adj 
			INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
			LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
			INNER JOIN model.PaymentMethods paym ON paym.Id = fb.PaymentMethodId
				AND paym.Name IN ('Cash','Check','Money Order','Endorsed Ins Check')
			LEFT JOIN model.invoicereceivables ir ON ir.id = adj.InvoiceReceivableId
			LEFT JOIN model.invoices inv ON inv.Id = ir.invoiceid
			LEFT JOIN model.Encounters e On e.Id = inv.EncounterId
			LEFT JOIN model.patients p ON p.Id = fb.PatientId
			LEFT JOIN model.PatientInsurances Pati ON pati.id = ir.patientinsuranceid
            LEFT JOIN model.insurancepolicies ip ON ip.id = pati.insurancepolicyid
			LEFT JOIN model.Insurers insur ON insur.Id = ip.InsurerId
			LEFT JOIN model.Providers prov ON inv.BillingProviderId = prov.id
			LEFT JOIN model.Users us ON us.id = prov.UserId
			WHERE adj.AdjustmentTypeId = 1 AND adj.FinancialSourceTypeId <> 4
				AND CONVERT(DATETIME,CONVERT(NVARCHAR(10),adj.PostedDateTime,101)) BETWEEN @StartDate AND @EndDate
				AND ((@Providers IS NULL AND (us.Id IS NULL OR us.Id IN (SELECT Id FROM model.Users)))OR 
						(@Providers IS NOT NULL AND us.ID IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,','))))
				AND ((@PayerTypes IS NULL AND adj.FinancialSourceTypeId IN (SELECT Id FROM model.FinancialSourceType_Enumeration)) OR
						(@PayerTypes IS NOT NULL AND adj.FinancialSourceTypeId IN (SELECT CONVERT(int,nstr) FROM CharTable(@PayerTypes,','))))
			) AS v, #Office o
		GROUP BY v.PayerId,v.PayerType,v.PaymentMethod
			,v.PostedDateTime,v.PaymentCheck
			,CASE PaymentMethod
									WHEN 'Check' THEN 1 
									WHEN 'Money Order' THEN 2 
									WHEN 'Endorsed Ins Check' THEN 3 
									WHEN 'Cash' THEN 4 END 
			,v.Source,
			CASE WHEN PaymentMethod ='Cash'
				THEN 'Note'
				ELSE 'Check No.' END 
				,o.OfficeName,o.OfficeAddress,o.OfficeCity,o.OfficeState,o.OfficeZip
	END

	RETURN
END
GO

-- exec model.GetDailyDeposit '2014-11-21','2014-11-21',NULL,NULL
