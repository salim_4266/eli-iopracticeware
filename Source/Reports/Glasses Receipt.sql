IF OBJECT_ID('model.GetGlassesReceipt') IS NOT NULL
	DROP PROCEDURE model.GetGlassesReceipt
GO

CREATE PROCEDURE model.GetGlassesReceipt (@EncounterId INT)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT bo.Id AS BillingOrganizationId
		,icNpi.Value AS NPI
		,icTax.Value AS TaxId
	INTO #PracticeInfo
	FROM model.BillingOrganizations bo 
	INNER JOIN model.IdentifierCodes icNpi ON icNpi.BillingOrganizationId = bo.Id
		AND icNpi.IdentifierCodeTypeId = 2
	INNER JOIN model.IdentifierCodes icTax ON icNpi.BillingOrganizationId = bo.Id
		AND icTax.IdentifierCodeTypeId = 7

	DECLARE @PatientID INT
	DECLARE @AppointmentId INT 
	SET @AppointmentId = @EncounterId - 1

	SELECT @PatientId = PatientId FROM model.Encounters WHERE Id = @EncounterId
	DECLARE @Delimiter2 INT,@Delimiter3 INT,@Delimiter4 INT,@Delimiter5 INT,@Delimiter6 INT,@Delimiter7 INT,@Delimiter8 INT
			,@Delimiter9 INT, @Delimiter10 INT, @Delimiter11 INT, @Delimiter12 INT, @Delimiter13 INT, @Delimiter14 INT
	SELECT @Delimiter2 = CHARINDEX('&',ShipZip,2)
		,@Delimiter3 = CHARINDEX('&',ShipZip, @Delimiter2 + 1)
		,@Delimiter4 = CHARINDEX('&',ShipZip, @Delimiter3 + 1)
		,@Delimiter5 = CHARINDEX('&',ShipZip, @Delimiter4 + 1)
		,@Delimiter6 = CHARINDEX('&',ShipZip, @Delimiter5 + 1)
		,@Delimiter7 = CHARINDEX('&',ShipZip, @Delimiter6 + 1)
		,@Delimiter8 = CHARINDEX('&',ShipZip, @Delimiter7 + 1)
		,@Delimiter9 = CHARINDEX('&',ShipCity, 2)
		,@Delimiter10 = CHARINDEX('&',ShipCity, @Delimiter9 + 1)
		,@Delimiter11 = CHARINDEX('&',ShipCity, @Delimiter10 + 1)
		,@Delimiter12 = CHARINDEX('&',ShipCity, @Delimiter11 + 1)
		,@Delimiter13 = CHARINDEX('&',ShipCity, @Delimiter12 + 1)
		,@Delimiter14 = CHARINDEX('&',ShipCity, @Delimiter13 + 1)
	FROM CLOrders cl
	WHERE PatientId = @PatientId
	AND cl.AppointmentId = @AppointmentId

	DECLARE @FrameNote NVARCHAR(MAX)
	SET @FrameNote = ''
	SELECT @FrameNote = @FrameNote + Note1 + ' 
'  
	FROM dbo.CLOrders cl 
	INNER JOIN dbo.PatientNotes pn ON cl.OrderId = CAST(SUBSTRING(pn.NoteSystem,4,5) AS INT)
	WHERE cl.PatientId = @PatientID
	AND SUBSTRING(NoteSystem,0,4) = 'PCO'
	AND cl.AppointmentId = @AppointmentId
	AND pn.NoteEye = ''

	DECLARE @LensNote NVARCHAR(MAX)
	SET @LensNote = ''
	SELECT @LensNote = @LensNote + Note1 + ' 
'  
	FROM model.Encounters e 
	INNER JOIN model.Invoices i ON i.EncounterId = e.Id
	INNER JOIn model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	INNER JOIN dbo.CLOrders cl ON cl.ReceivableId = ir.Id
	INNER JOIN dbo.PatientNotes pn ON cl.OrderId = CAST(SUBSTRING(pn.NoteSystem,4,5) AS INT)
	WHERE cl.PatientId = @PatientID
	AND SUBSTRING(NoteSystem,0,4) = 'PCO'
	AND cl.AppointmentId = @AppointmentId
	AND pn.NoteEye <> ''

	DECLARE @ShipAddress1 NVARCHAR(MAX)
	SELECT @ShipAddress1 = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ShipAddress1, '&',''),'ODL:',','),'ODC:',','),'ODM:',','),'ODB:',','),'ODP:',','),'ODT:',','),',,,',','),',,',',')
	FROM CLOrders cl 
	WHERE @PatientID = cl.PatientId
	AND @AppointmentId = cl.AppointmentId

	DECLARE @ShipAddress2 NVARCHAR(MAX)
	SELECT @ShipAddress2 = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ShipAddress2, '&',''),'OSL:',','),'OSC:',','),'OSM:',','),'OSB:',','),'OSP:',','),'OST:',','),',,,',','),',,',',')
	FROM CLOrders cl 
	WHERE @PatientID = cl.PatientId
	AND @AppointmentId = cl.AppointmentId

	SET @ShipAddress1 = CASE WHEN @ShipAddress1 <> '' THEN RIGHT(@ShipAddress1,LEN(@ShipAddress1) - 1) + @ShipAddress2 ELSE @ShipAddress2 END

	DECLARE @Services NVARCHAR(MAX)
	SET @Services = ''
 
	SELECT @Services = @Services + es.Description + '
'
	FROM model.EncounterServices es
	WHERE es.Code IN (SELECT CONVERT(nvarchar(10),nstr) FROM CharTable(@ShipAddress1,','))

	SELECT DISTINCT 
		p.Id AS PatientId
		,en.Id AS AppointmentId
		,inv.Id AS InvoiceId
		,p.LastName AS LastName
		,p.FirstName AS FirstName
		,p.MiddleName AS MiddleName
		,CONVERT(NVARCHAR(10),p.DateOfBirth,101) AS DateOfBirth
		,ppn.AreaCode + '-' + ppn.ExchangeAndSuffix AS ExchangeAndSuffix
		,pa.Line1 AS PatientAddress
		,pa.City AS PatientCity
		,sop.Abbreviation AS PatientState
		,pa.PostalCode AS PatientZip
		,u.DisplayName AS Doctor
		,ec.DisplayName AS ReferDr
		,pc.FindingDetail AS FindingDetail
		,sl.ShortName AS ServiceLocation
		,pin.NPI
		,pin.TaxId
		,ir.Id AS InvReceivable
		,bd.OrdinalId AS Diagnosis
		,esem.ExternalSystemEntityKey AS Icd9DiagnosisCode
		,dbo.ExtractTextValue('-2/',pc.FindingDetail,'-3/') AS ODValue
		,dbo.ExtractTextValue('-3/',pc.FindingDetail,'-4/') AS OSValue
		,pci.SKU AS UPC
		,pci.Type_ AS Disc
		,pci.Model AS Model
		,pci.Manufacturer AS Mfr
		,pci.Color AS Color
		,@Services AS [Services]
		,CASE WHEN LEFT(ShipState,1) = '1' THEN 'YES' ELSE 'NO' END AS Uncut
		,CASE WHEN SUBSTRING(ShipZip,2,1) = '0' THEN 'YES' ELSE 'NO' END AS OwnFrame 
		,CASE WHEN SUBSTRING(ShipZip,3,1) = '0' THEN 'YES' ELSE 'NO' END AS [Sent]
		,dbo.ExtractTextValue('OD:',cl.ShipCity,'&&') AS OD
		,RIGHT(SUBSTRING(ShipZip,2,@Delimiter2-2),2) AS Frame_A
		,SUBSTRING(ShipZip,@Delimiter2+1,@Delimiter3-@Delimiter2-1) AS Frame_B
		,SUBSTRING(ShipZip,@Delimiter3+1,@Delimiter4-@Delimiter3-1) AS Frame_ED
		,SUBSTRING(ShipZip,@Delimiter4+1,@Delimiter5-@Delimiter4-1) AS DBL
		,SUBSTRING(ShipZip,@Delimiter5+1,@Delimiter6-@Delimiter5-1) AS Eye
		,SUBSTRING(ShipZip,@Delimiter6+1,@Delimiter7-@Delimiter6-1) AS Bridge
		,SUBSTRING(ShipZip,@Delimiter7+1,@Delimiter8-@Delimiter7-1) AS Temp
		,@FrameNote AS FrameNote
		,@LensNote AS LensNote
		,dbo.ExtractTextValue('OD:',ShipCity,'&') AS ODpddv
		,SUBSTRING(ShipCity,@Delimiter9+1,@Delimiter10-@Delimiter9-1) AS ODpdnv
		,SUBSTRING(ShipCity,@Delimiter10+1,@Delimiter11-@Delimiter10-1) AS ODheight
		,SUBSTRING(SUBSTRING(ShipCity,@Delimiter11+1,@Delimiter12-@Delimiter11-1),4,5) AS OSpddv
		,SUBSTRING(ShipCity,@Delimiter12+1,@Delimiter13-@Delimiter12-1) AS OSpdnv
		,SUBSTRING(ShipCity,@Delimiter13+1,@Delimiter14-@Delimiter13-1) AS OSheight
	FROM model.Encounters en
	INNER JOIN model.Patients p ON p.Id = en.PatientId
	INNER JOIN model.Invoices inv ON inv.EncounterId = en.Id
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = inv.Id
	INNER JOIN model.ServiceLocations sl ON sl.Id = en.ServiceLocationId
	INNER JOIN #PracticeInfo pin ON pin.BillingOrganizationId = p.BillingOrganizationId
	LEFT JOIN model.PatientAddresses pa ON pa.PatientId = p.Id
		AND pa.OrdinalId = 1
	LEFT JOIN model.StateOrProvinces sop ON pa.StateOrProvinceId = sop.Id
	LEFT JOIN model.BillingDiagnosis bd ON bd.InvoiceId = inv.Id
	LEFT JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId
	LEFT JOIN model.PatientPhoneNumbers ppn ON ppn.PatientId = p.Id
		AND ppn.OrdinalId = 1
	LEFT JOIN model.ExternalContacts ec ON ec.Id = inv.ReferringExternalProviderId
	LEFT JOIN CLOrders cl ON cl.ReceivableId = ir.Id
	LEFT JOIN model.Appointments ap ON ap.Id = cl.AppointmentId
	LEFT JOIN PatientClinical pc ON pc.AppointmentId = cl.AppointmentId
	LEFT JOIN model.Users u ON u.Id = ap.UserId
	LEFT JOIN PCInventory pci ON pci.InventoryId = cl.InventoryId
	LEFT JOIN CLInventory cli ON cli.InventoryId = cl.InventoryId
	WHERE pc.ClinicalType = 'A' 
	AND en.PatientId = cl.PatientId
	AND en.ID = @EncounterId
	DROP TABLE #PracticeInfo
END
GO

/*
 exec model.GetGlassesReceipt 385697
 */