IF OBJECT_ID('model.GetAdjustmentAnalysis') IS NOT NULL
	DROP PROCEDURE model.GetAdjustmentAnalysis

GO

CREATE PROCEDURE model.GetAdjustmentAnalysis(
		@StartDate datetime, 
		@EndDate datetime, 
		@BillingOrganizations NVARCHAR(MAX),
		@Providers NVARCHAR(MAX),
		@ServiceLocations NVARCHAR(MAX),
		@ShowBillingOrganizationBreakdown bit,
		@ShowProviderBreakdown bit,
		@ShowServiceLocationBreakdown bit
		)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT CONVERT(datetime,CONVERT(nvarchar(10),adj.PostedDateTime,101),101) AS AdjustmentDate	
	,adj.Id AS AdjustmentId
	,at.ShortName AS AdjustmentType
	,CASE WHEN at.IsDebit = 1 THEN - adj.Amount ELSE adj.Amount END AS Amount
	,bo.ShortName AS BillingOrganization
	,usBill.UserName AS BillToDr
	,sl.ShortName AS ServiceLocation
INTO #adjAll
FROM model.invoices inv
INNER JOIN model.encounters en ON en.id = inv.EncounterId
INNER JOIN model.servicelocations sl ON sl.id = inv.AttributeToServiceLocationId
INNER JOIN model.BillingServices bs ON bs.InvoiceId = inv.Id
INNER JOIN model.Adjustments adj ON adj.BillingServiceId = bs.Id
INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	AND at.IsCash = 0
INNER JOIN model.providers bp ON bp.id = inv.BillingProviderId
INNER JOIN model.BillingOrganizations bo ON bo.Id = bp.BillingOrganizationId
INNER JOIN model.Users usBill ON bp.UserId = usBill.Id
LEFT JOIN model.Appointments app ON app.EncounterId = inv.EncounterId
WHERE ((@BillingOrganizations IS NULL AND bo.Id IN (SELECT Id FROM model.BillingOrganizations)) OR 
		(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))
			OR bo.Id IS NULL)
	AND ((@ServiceLocations IS NULL AND sl.Id IN (SELECT Id FROM model.ServiceLocations)) OR 
		(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,',')))
			OR sl.Id IS NULL)
	AND ((@Providers IS NULL AND usBill.Id IN (SELECT UserId FROM model.Providers)) OR 
		(@Providers IS NOT NULL AND usBill.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,',')))
			OR usBill.Id IS NULL)


DECLARE @PreviousYearStartDate datetime
SET @PreviousYearStartDate = DATEADD(YY,-1, (SELECT CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)))
DECLARE @CurrentYearStartDate datetime
SET @CurrentYearStartDate = CONVERT(DATETIME, '01/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)				
DECLARE @PreviousYearEndDate datetime
SET @PreviousYearEndDate = DATEADD(YY,-1, (SELECT CONVERT(DATETIME, '12/31/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)))
DECLARE @PreviousYTDEndDate datetime
SET @PreviousYTDEndDate = DATEADD(YY,-1,@EndDate)
DECLARE @PreviousMTDStartDate datetime
SET @PreviousMTDStartDate = DATEADD(MM,-1,CONVERT(DATETIME, CONVERT(NVARCHAR(2), DATEPART(MONTH, @EndDate)) + '/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101))
DECLARE @PreviousMTDEndDate datetime
SET @PreviousMTDEndDate = DATEADD(MM,-1,@EndDate)
DECLARE @CurrentMTDStartDate datetime
SET @CurrentMTDStartDate = CONVERT(DATETIME, CONVERT(NVARCHAR(2), DATEPART(MONTH, @EndDate)) + '/01/' + CONVERT(NVARCHAR(4), DATEPART(YEAR, @EndDate)), 101)
				
DECLARE @TotalCurrentAdjustments decimal(20,3)
SELECT @TotalCurrentAdjustments = SUM(Amount)
FROM #adjAll
WHERE AdjustmentDate BETWEEN @StartDate AND @EndDate

DECLARE @TotalCurrentMTDAdjustments decimal(20,2)
SELECT @TotalCurrentMTDAdjustments = COALESCE(SUM(Amount),0)
FROM #adjAll
WHERE AdjustmentDate BETWEEN @CurrentMTDStartDate AND @EndDate

DECLARE @TotalCurrentYTDAdjustments decimal(20,2)
SELECT @TotalCurrentYTDAdjustments = COALESCE(SUM(Amount),0)
FROM #adjAll
WHERE AdjustmentDate BETWEEN @CurrentYearStartDate AND @EndDate

DECLARE @TotalPreviousMTDAdjustments decimal(20,2)
SELECT @TotalPreviousMTDAdjustments = COALESCE(SUM(Amount),0)
FROM #adjAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousMTDStartDate AND @PreviousMTDEndDate

DECLARE @TotalPreviousYearAdjustments decimal(20,2)
SELECT @TotalPreviousYearAdjustments = COALESCE(SUM(Amount),0)
FROM #adjAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYearEndDate

DECLARE @TotalPreviousYTDAdjustments decimal(20,2)
SELECT @TotalPreviousYTDAdjustments = COALESCE(SUM(Amount),0)
FROM #adjAll pa
WHERE pa.AdjustmentDate BETWEEN @PreviousYearStartDate AND @PreviousYTDEndDate

SELECT 
	v.BillingOrganization
	,v.BillToDr
	,v.ServiceLocation
	,v.AdjustmentType
	,SUM(v.Adjustments) AS Adjustments
	,CASE @TotalCurrentAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * SUM(v.Adjustments)) / @TotalCurrentAdjustments) END AS AdjustmentsPercent
	,SUM(v.AdjustmentsMTD) AS AdjustmentsMTD
	,CASE @TotalCurrentMTDAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * SUM(v.AdjustmentsMTD)) / @TotalCurrentMTDAdjustments) END AS AdjustmentsMTDPercent
	,SUM(v.AdjustmentsPrevMTD) AS AdjustmentsPrevMTD
	,CASE @TotalPreviousMTDAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * SUM(v.AdjustmentsPrevMTD)) / @TotalPreviousMTDAdjustments) END AS AdjustmentsPrevMTDPercent
	,CASE SUM(v.AdjustmentsPrevMTD) WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1),100 * ((SUM(v.AdjustmentsMTD) - SUM(v.AdjustmentsPrevMTD)) / SUM(v.AdjustmentsPrevMTD))) END AS MonthPercent
	,SUM(v.AdjustmentsYTD) AS AdjustmentsYTD
	,CASE @TotalCurrentYTDAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * (SUM(v.AdjustmentsYTD)) / @TotalCurrentYTDAdjustments)) END AS AdjustmentsYTDPercent
	,SUM(v.AdjustmentsPrevYTD) AS AdjustmentsPrevYTD
	,CASE @TotalPreviousYTDAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * SUM(v.AdjustmentsPrevYTD)) / @TotalPreviousYTDAdjustments)  END AS AdjustmentsPrevYTDPercent
	,CASE SUM(v.AdjustmentsPrevYTD) WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1),(100 * (SUM(v.AdjustmentsYTD) - SUM(v.AdjustmentsPrevYTD)) / SUM(v.AdjustmentsPrevYTD))) END AS YearPercent
	,SUM(v.AdjustmentsPrevYear) AS AdjustmentsPrevYear
	,CASE @TotalPreviousYearAdjustments WHEN 0 THEN 0 ELSE CONVERT(decimal(20,1), (100 * SUM(v.AdjustmentsPrevYear)) / @TotalPreviousYearAdjustments) END AS AdjustmentsPrevYearPercent
FROM (
	SELECT 
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN COALESCE(pa.BillingOrganization,'No Billing Organization') ELSE NULL END AS BillingOrganization
		,CASE WHEN @ShowProviderBreakdown = 1 THEN COALESCE(pa.BillToDr,'No Provider') ELSE NULL END AS BillToDr
		,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN COALESCE(pa.ServiceLocation,'No Service Location') ELSE NULL END AS ServiceLocation
		,pa.AdjustmentType
		,SUM(COALESCE(adjDates.Amount, 0)) AS Adjustments
		,SUM(COALESCE(adjMTD.Amount, 0)) AS AdjustmentsMTD
		,SUM(COALESCE(adjPrevMTD.Amount, 0)) AS AdjustmentsPrevMTD
		,SUM(COALESCE(adjYTD.Amount, 0)) AS AdjustmentsYTD
		,SUM(COALESCE(adjPrevYear.Amount, 0)) AS AdjustmentsPrevYear
		,SUM(COALESCE(adjPrevYTD.Amount, 0)) AS AdjustmentsPrevYTD
	FROM #adjAll pa
	LEFT JOIN #adjAll adjPrevYear ON pa.AdjustmentId = adjPrevYear.AdjustmentId
		AND adjPrevYear.AdjustmentDate BETWEEN @PreviousYearStartDate
										AND @PreviousYearEndDate
	LEFT JOIN #adjAll adjDates ON pa.AdjustmentId = adjDates.AdjustmentId
		AND adjDates.AdjustmentDate BETWEEN @StartDate AND @EndDate
	LEFT JOIN #adjAll adjMTD ON pa.AdjustmentId = adjMTD.AdjustmentId
		AND adjMTD.AdjustmentDate BETWEEN @CurrentMTDStartDate
			AND @EndDate
	LEFT JOIN #adjAll adjPrevMTD ON pa.AdjustmentId = adjPrevMTD.AdjustmentId
		AND adjPrevMTD.AdjustmentDate BETWEEN @PreviousMTDStartDate
				AND @PreviousMTDEndDate			
	LEFT JOIN #adjAll adjPrevYTD ON pa.AdjustmentId = adjPrevYTD.AdjustmentId
		AND adjPrevYTD.AdjustmentDate BETWEEN @PreviousYearStartDate
									AND @PreviousYTDEndDate
	LEFT JOIN #adjAll adjYTD ON pa.AdjustmentId = adjYTD.AdjustmentId
		AND adjYTD.AdjustmentDate BETWEEN @CurrentYearStartDate
					AND @EndDate
	WHERE pa.AdjustmentDate > @PreviousYearStartDate
	GROUP BY CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN pa.BillingOrganization ELSE NULL END
		,CASE WHEN @ShowProviderBreakdown = 1 THEN pa.BillToDr ELSE NULL END
		,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN pa.ServiceLocation ELSE NULL END
		,pa.AdjustmentType,pa.BillingOrganization,pa.BillToDr,pa.ServiceLocation
	) v
	GROUP BY 
		v.BillingOrganization,v.BillToDr,v.ServiceLocation,v.AdjustmentType


DROP TABLE #adjAll
END 
RETURN
END
GO

-- exec model.GetAdjustmentAnalysis '2014-02-01 00:00:00','2014-02-28 00:00:00',NULL,NULL,NULL,0,0,0
--N'1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,27,128,129,130,131,132,133,134,135,136,137,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,73,174,175,176,177,178,179,182,183,184,185,186,187,188,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,247,248,249,250,251,252,253,254,255,256,257,258,259,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,317,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,376,377,378,379,380,381,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,402,403,404,405,406,407,408,409,410,411,412,413,414,416,417,418,419,421,423,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,468,472,474,475,476,477,478,479,480,481,482,483,484,485,487,488,490,491,492,493,494,495,496,497,498,499,500,502,503,504,505,506,507,508,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,527,528,529,530,532,534,535,536,537,538,539,540,541,542,549,550,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,621,622,623,624,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,697,698,699,700,703,704,705,706,708,709,710,711,712,713,714,715,716,717,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,200002,200003,200005,200006,200007,200008,200009,300004'