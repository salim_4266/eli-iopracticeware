IF OBJECT_ID ( 'model.GetTimeSheet', 'P' ) IS NOT NULL 
	DROP PROCEDURE [model].[GetTimeSheet]
GO

CREATE PROCEDURE [model].[GetTimeSheet](@StartDate datetime, @EndDate datetime, @Users varchar(max), @IncludeTable bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
	BEGIN

	set @StartDate = convert(varchar(8),@StartDate,112)
	set @EndDate = convert(varchar(8),@EndDate,112)

	SELECT eh.UserId, 
		u.DisplayName,
		u.UserName,
		CONVERT(VARCHAR(8),eh.StartDateTime,112) as Date,
		ISNULL(CONVERT(VARCHAR(10),eh.StartDateTime,108), '') as ClockIn,
		ISNULL(CONVERT(VARCHAR(10),eh.EndDateTime,108), '') as ClockOut,		
		ISNULL(CAST(CAST(DATEDIFF(minute,StartDateTime,EndDateTime) AS DECIMAL(6,2))/60 AS DECIMAL(6,2)), 0) AS Total
		FROM model.EmployeeHours eh
		INNER JOIN model.Users u ON u.Id = eh.UserId	
	WHERE ((@Users IS NULL AND eh.UserId IN (SELECT Id FROM model.Users))
		OR (@Users IS NOT NULL AND eh.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,','))))
		AND CONVERT(VARCHAR(8),eh.StartDateTime,112) BETWEEN @StartDate AND @EndDate	
END
RETURN
END