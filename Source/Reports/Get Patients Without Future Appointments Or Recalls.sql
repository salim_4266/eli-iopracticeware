IF OBJECT_ID('model.GetPatientsWithoutFutureAppointmentsOrRecalls') IS NOT NULL
	DROP PROCEDURE model.GetPatientsWithoutFutureAppointmentsOrRecalls

GO

CREATE PROCEDURE model.GetPatientsWithoutFutureAppointmentsOrRecalls (
		@StartDate datetime, 
		@EndDate datetime,
		@DefaultProviders nvarchar(max) = NULL, 
		@Diagnoses nvarchar(max) = NULL, 
		@DefaultProviderBreakdown bit,
		@PreferedLocation nvarchar(max) = NULL)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT
	ppn.PatientId
	,ppn.AreaCode
	,ppn.ExchangeAndSuffix
	,CASE WHEN ISNUMERIC(ppn.Extension) = 1 THEN 'x' + ppn.Extension
		ELSE NULL END AS Extension
	,CASE PatientPhoneNumberTypeId
		WHEN 7 THEN 'Home'
		WHEN 2 THEN 'Business'
		WHEN 3 THEN 'Cell'
		WHEN 12 THEN 'Emergency'
		WHEN 13 THEN 'Work'
		WHEN 14 THEN 'Fax'
		WHEN 15 THEN 'Beeper'
		WHEN 16 THEN 'Night'
		WHEN 17 THEN 'Unknown'
		END AS PhoneType
	,ppn.OrdinalId AS Rank
INTO #RankedPhoneNumbers
FROM model.PatientPhoneNumbers ppn
WHERE ppn.OrdinalId <= 3
-- partition is really slow so if ordinal ids are 2,3,4 then only 2 and 3 will show

CREATE INDEX IX_RankedPhoneNumbersPatient ON #RankedPhoneNumbers(PatientId)

SELECT  r.PatientId
	,CASE WHEN r1.AreaCode IS NULL THEN r1.ExchangeAndSuffix ELSE 
		r1.AreaCode + '-' + r1.ExchangeAndSuffix  END AS MainPhoneNumber
	,COALESCE(r1.Extension, '') AS MainExtension
	,COALESCE(r1.PhoneType, '') AS MainPhoneType
	,CASE WHEN r2.AreaCode IS NULL THEN r2.ExchangeAndSuffix ELSE 
		r2.AreaCode + '-' + r2.ExchangeAndSuffix  END AS SecondPhoneNumber
	,COALESCE(r2.Extension, '') AS SecondExtension
	,COALESCE(r2.PhoneType, '') AS SecondPhoneType
	,CASE WHEN r3.AreaCode IS NULL THEN r3.ExchangeAndSuffix ELSE 
		r3.AreaCode + '-' + r3.ExchangeAndSuffix  END AS ThirdPhoneNumber
	,COALESCE(r3.Extension, '') AS ThirdExtension
	,COALESCE(r3.PhoneType, '') AS ThirdPhoneType
INTO #PatientPhoneNumbers
FROM #RankedPhoneNumbers r
LEFT JOIN #RankedPhoneNumbers r1 ON r1.PatientId = r.PatientId
	AND r1.Rank = 1
LEFT JOIN #RankedPhoneNumbers r2 ON r2.PatientId = r1.PatientId
	AND r2.Rank = 2
LEFT JOIN #RankedPhoneNumbers r3 ON r3.PatientId = r2.PatientId
	AND r3.Rank = 3

CREATE INDEX IX_PatientPhoneNumbersPatient ON #patientPhoneNumbers(PatientId)

SELECT ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY ePast.StartDateTime DESC) AS Rank , ePast.Id, ePast.PatientId,
	 ePast.StartDateTime
INTO #LastEncounter
FROM model.Encounters ePast
WHERE ePast.EncounterStatusId = 7
	AND ePast.StartDateTime < GETDATE()

CREATE INDEX IX_LastEncounterId ON #LastEncounter(Id)

SELECT DISTINCT
	p.LastName
	, p. FirstName
	, p.MiddleName
	, p.Suffix
	, p.Id
	, u.UserName
	, ppn.MainPhoneNumber
	, ppn.MainExtension
	, ppn.MainPhoneType
	, ppn.SecondPhoneNumber
	, ppn.SecondExtension
	, ppn.SecondPhoneType
	, ppn.ThirdPhoneNumber
	, ppn.ThirdExtension
	, ppn.ThirdPhoneType
	, CONVERT(nvarchar(10),le.StartDateTime,101) AS LastVisit
	, esem.ExternalSystemEntityKey AS Icd9DiagnosisCode
INTO #Results
FROM model.Patients p
INNER JOIN #LastEncounter le ON le.PatientId = p.Id
	AND le.Rank = 1
INNER JOIN model.Invoices i ON le.Id = i.EncounterId
LEFT JOIN model.BillingDiagnosis bd ON bd.InvoiceId = i.Id
LEFT JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId
LEFT JOIN model.Users u ON u.Id = p.DefaultUserId
LEFT JOIN #PatientPhoneNumbers ppn ON p.Id = ppn.PatientId
LEFT JOIN model.PatientRecalls r ON r.PatientId = p.Id
	AND r.RecallStatusId = 1
	AND r.DueDateTime >= le.StartDatetime

LEFT JOIN model.Encounters eFuture ON eFuture.PatientId = p.Id
	AND eFuture.EncounterStatusId IN (1, 2, 3, 4, 5, 6, 15)
	AND eFuture.StartDateTime >= le.StartDatetime

WHERE p.LastName <> 'Test'
AND le.StartDateTime BETWEEN 
			CONVERT(datetime,(CONVERT(nvarchar(10),@StartDate,101))) AND
			CONVERT(datetime,(CONVERT(nvarchar(10),@EndDate,101)))
			AND eFuture.PatientId IS NULL
		    AND r.PatientId IS NULL
AND ((@DefaultProviders IS NULL AND (u.Id IS NULL OR u.Id IN (SELECT Id FROM model.Users))) OR
	(@DefaultProviders IS NOT NULL AND (u.Id IS NULL OR u.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@DefaultProviders,',')))))
AND ((@Diagnoses IS NULL AND esem.ExternalSystemEntityKey IN (SELECT DISTINCT esem.ExternalSystemEntityKey AS Icd9DiagnosisCode FROM model.BillingDiagnosis bd JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId)) OR 
		(@Diagnoses IS NOT NULL AND esem.ExternalSystemEntityKey IN (SELECT CONVERT(nvarchar,nstr) FROM CharTable(@Diagnoses,','))))
AND ((@PreferedLocation IS NULL AND (PreferredServiceLocationId IS NULL )) OR (@PreferedLocation IS NOT NULL AND (p.PreferredServiceLocationId IN (SELECT CONVERT(int,nstr) FROM CharTable(@PreferedLocation,',')))))



SELECT DISTINCT
	CASE WHEN @DefaultProviderBreakdown = 1 THEN COALESCE(r.UserName,'') ELSE '' END AS DefaultProvider
	, r.LastName
	, r.FirstName
	, r.MiddleName
	, CASE WHEN r.Suffix = '' THEN ', ' ELSE r.Suffix + ', ' END AS Suffix
	, r.Id
	, r.LastVisit
	, r.MainPhoneNumber
	, r.MainExtension
	, r.MainPhoneType
	, COALESCE(r.SecondPhoneNumber,'0') AS SecondPhoneNumber
	, r.SecondExtension
	, r.SecondPhoneType
	, COALESCE(r.ThirdPhoneNumber,'0') AS ThirdPhoneNumber
	, r.ThirdExtension
	, r.ThirdPhoneType
FROM #Results r


DROP TABLE #RankedPhoneNumbers
DROP TABLE #PatientPhoneNumbers
DROP TABLE #Results
DROP TABLE #LastEncounter

END
GO

-- exec model.GetPatientsWithoutFutureAppointmentsOrRecalls '2014-01-01 00:00:00','2014-04-01 00:00:00',NULL,NULL,0