IF OBJECT_ID('model.GetChargesPaymentsAndAdjustmentsWithPaymentMethodAndOpenBalance') IS NOT NULL
	DROP PROCEDURE model.GetChargesPaymentsAndAdjustmentsWithPaymentMethodAndOpenBalance

GO

CREATE PROCEDURE model.GetChargesPaymentsAndAdjustmentsWithPaymentMethodAndOpenBalance(@StartDate datetime, @EndDate datetime,@AdjStartDate datetime,@AdjEndDate datetime,@GroupByBillingOrganization bit, @GroupByUser bit, @GroupByServiceLocation bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF CONVERT(datetime, CONVERT(nvarchar(10),@StartDate, 101)) > CONVERT(datetime, CONVERT(nvarchar(10),@EndDate, 101)) OR CONVERT(datetime, CONVERT(nvarchar(10),@AdjStartDate, 101)) > CONVERT(datetime, CONVERT(nvarchar(10),@AdjEndDate, 101))
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SET @StartDate = CONVERT(datetime, CONVERT(nvarchar(10),@StartDate,101))
SET @EndDate = DATEADD(ms,-2, DATEADD(dd,1,CONVERT(nvarchar(10),@EndDate,101)))
SET @AdjStartDate = CONVERT(datetime, CONVERT(nvarchar(10),@AdjStartDate,101))
SET @AdjEndDate = CONVERT(datetime,CONVERT(nvarchar(10),DATEADD(ms,-2, DATEADD(dd,1,@AdjEndDate)),101))

DECLARE @NoProvider nvarchar(17)
SET @NoProvider = 'Unlinked Provider'
DECLARE @NoBO nvarchar(29)
SET @NoBO = 'Unlinked Billing Organization'
DECLARE @NoSL nvarchar(25)
SET @NoSL = 'Unlinked Service Location'
CREATE TABLE #Adjustments(AdjustmentId bigint, Amount decimal(36,2), AdjustmentTypeId int, PaymentMethodId int, InvoiceId bigint, DateTime datetime, isCash bit)

INSERT #Adjustments
SELECT a.Id, (CASE WHEN at.IsDebit = 1 THEN -a.Amount ELSE a.Amount END) AS Amount, a.AdjustmentTypeId, f.PaymentMethodId, ir.InvoiceId, a.PostedDateTime, at.IsCash
FROM model.Adjustments a
INNER JOIN model.FinancialBatches f ON a.FinancialBatchId = f.Id
INNER JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
INNER JOIN model.InvoiceReceivables ir ON a.InvoiceReceivableId = ir.Id
WHERE a.PostedDateTime<=ISNULL(@AdjEndDate,@EndDate) 

CREATE INDEX IX_Adjustments_InvoiceReceivableId ON #Adjustments(InvoiceId)

---- Invoices
CREATE TABLE #Invoices(InvoiceId bigint, DateTime datetime, BillingOrganizationId bigint, BillingOrganizationShortName nvarchar(255), UserId bigint, UserName nvarchar(255), ServiceLocationId bigint, ServiceLocationShortName nvarchar(255))

INSERT #Invoices
SELECT i.Id, CONVERT(DATETIME,CONVERT(NVARCHAR(10),e.StartDateTime,101)), bo.Id, bo.ShortName, u.Id, u.UserName, sl.Id, sl.ShortName
FROM model.Invoices i
INNER JOIN model.Encounters e ON i.EncounterId = e.Id
INNER MERGE JOIN model.ServiceLocations sl ON e.ServiceLocationId = sl.Id
LEFT MERGE JOIN model.Providers p ON i.BillingProviderId = p.Id
LEFT MERGE JOIN model.Users u ON p.UserId = u.Id
LEFT MERGE JOIN model.BillingOrganizations bo ON p.BillingOrganizationId = bo.Id
 
CREATE INDEX IX_Invoices_InvoiceId ON #Invoices(InvoiceId)
CREATE INDEX IX_Invoices_BillingOrganization ON #Invoices(BillingOrganizationId, BillingOrganizationShortName)
CREATE INDEX IX_Invoices_User ON #Invoices(UserId, UserName)
CREATE INDEX IX_Invoices_ServiceLocation ON #Invoices(ServiceLocationId, ServiceLocationShortName)

CREATE TABLE #AdjustmentsAndCharges(Id int IDENTITY, Payment decimal(36,2), PaymentMethodId int, NonPayment decimal(36,2), Charge decimal(36,2), InvoiceId bigint, DateTime datetime)

INSERT #AdjustmentsAndCharges 
SELECT 
	CASE WHEN a.isCash = 1 THEN Amount ELSE NULL END AS Payment, 
	a.PaymentMethodId AS PaymentMethodId, 
	CASE WHEN a.isCash = 0 THEN Amount ELSE NULL END AS NonPayment, 
	NULL AS Charge, 
	InvoiceId, DateTime
FROM #Adjustments a
UNION ALL
SELECT NULL AS Payment, NULL AS PaymentMethodId, NULL AS NonPayment, Unit * UnitCharge AS Charge, bs.InvoiceId, i.DateTime
FROM model.BillingServices bs 
JOIN #Invoices i ON bs.InvoiceId = i.InvoiceId
WHERE i.DateTime <= @EndDate

CREATE INDEX IX_AdjustmentsAndCharges_Id_DateTime ON #AdjustmentsAndCharges(Id, DateTime)
CREATE INDEX IX_AdjustmentsAndCharges_InvoiceId ON #AdjustmentsAndCharges(InvoiceId)
CREATE INDEX IX_AdjustmentsAndCharges_PaymentMethodId ON #AdjustmentsAndCharges(PaymentMethodId)

CREATE TABLE #OnAccountPayments(Id int, Amount decimal(36,2), InvoiceReceivableId bigint, Datetime datetime, PaymentMethodId int)

INSERT #OnAccountPayments
SELECT a.Id, a.Amount, a.InvoiceReceivableId, a.PostedDateTime, fb.PaymentMethodId 
FROM model.Adjustments a
LEFT JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
WHERE a.InvoiceReceivableId IS NULL AND (a.BillingServiceId IS NOT NULL OR a.PatientId IS NOT NULL)
AND a.PostedDateTime <= ISNULL(@AdjEndDate,@EndDate)


CREATE INDEX IX_OnAccountPayments_Id_Datetime ON #OnAccountPayments(Id, DateTime)
CREATE INDEX IX_OnAccountPayments_InvoiceReceiveableId ON #OnAccountPayments(InvoiceReceivableId)
CREATE INDEX IX_OnAccountPayments_PaymentMethodId ON #OnAccountPayments(PaymentMethodId)

SELECT SUM(Payment) AS Payment
		,SUM(PaymentMTD) AS PaymentMTD
		,SUM(PaymentYTD) AS PaymentYTD
		,COALESCE(v.PaymentMethodId,0) AS PaymentMethodId
		,COALESCE(v.PaymentMethodName,'OTHER') AS PaymentMethodName
		,SUM(NonPayment) AS NonPayment
		,SUM(Charge) AS Charge
		,SUM(AllCharges) AS AllCharges
		,SUM(AllPayments) AS AllPayments
		,SUM(AllNonPayments) AS AllNonPayments
		,SUM(AllCharges) - SUM(AllPayments) - SUM(v.AllNonPayments) AS OpenBalance
		,SUM(ChargeMTD) AS ChargeMTD
		,SUM(NonPaymentMTD) AS NonPaymentMTD
		,SUM(ChargeYTD) AS ChargeYTD
		,SUM(NonPaymentYTD) AS NonPaymentYTD
		,v.BillingOrganizationId,v.BillingOrganizationShortName
		,v.UserId,v.UserName
		,v.ServiceLocationId,v.ServiceLocationShortName
FROM (
SELECT 
	SUM(COALESCE(aacAfterStartDate.Payment, 0)) AS Payment
	,SUM(COALESCE(aacMonthToDate.Payment, 0)) AS PaymentMTD
	,SUM(COALESCE(aacYearToDate.Payment, 0)) AS PaymentYTD
	,CASE WHEN SUM(COALESCE(aacAfterStartDate.Payment, 0)) + SUM(COALESCE(aacAfterStartDate.NonPayment, 0)) <> 0 THEN aacAll.PaymentMethodId ELSE NULL END AS PaymentMethodId
	,CASE WHEN SUM(COALESCE(aacAfterStartDate.Payment, 0)) + SUM(COALESCE(aacAfterStartDate.NonPayment, 0)) <> 0 THEN 
		CASE WHEN MAX(pm.Name) IS NULL THEN 'OTHER' ELSE MAX(pm.Name) END
		ELSE NULL END AS PaymentMethodName
	,SUM(COALESCE(aacAfterStartDate.NonPayment, 0)) AS NonPayment
	,SUM(COALESCE(aacAfterStartDate.Charge, 0)) AS Charge 
	,SUM(COALESCE(aacAll.Charge, 0)) AS AllCharges
	,SUM(COALESCE(aacAll.Payment, 0)) AS AllPayments
	,SUM(COALESCE(aacAll.NonPayment, 0)) AS AllNonPayments
	,SUM(COALESCE(aacMonthToDate.Charge,0)) AS ChargeMTD
	,SUM(COALESCE(aacMonthToDate.NonPayment,0)) AS NonPaymentMTD
	,SUM(COALESCE(aacYearToDate.Charge,0)) AS ChargeYTD
	,SUM(COALESCE(aacYearToDate.NonPayment,0)) AS NonPaymentYTD
	,CASE WHEN @GroupByBillingOrganization = 1 THEN COALESCE(MAX(i.BillingOrganizationId), 0) END AS BillingOrganizationId
	,CASE WHEN @GroupByBillingOrganization = 1 THEN COALESCE(MAX(i.BillingOrganizationShortName), @NoBO) END AS BillingOrganizationShortName
	,CASE WHEN @GroupByUser = 1 THEN COALESCE(MAX(i.UserId), 0) END AS UserId
	,CASE WHEN @GroupByUser = 1 THEN COALESCE(MAX(i.UserName), @NoProvider) END AS UserName
	,CASE WHEN @GroupByServiceLocation = 1 THEN COALESCE(MAX(i.ServiceLocationId), 0) END AS ServiceLocationId
	,CASE WHEN @GroupByServiceLocation = 1 THEN COALESCE(MAX(i.ServiceLocationShortName), @NoSL) END AS ServiceLocationShortName
FROM #AdjustmentsAndCharges aacAll
	LEFT JOIN #Invoices i ON aacAll.InvoiceId = i.InvoiceId
	LEFT JOIN #AdjustmentsAndCharges aacAfterStartDate ON aacAll.Id = aacAfterStartDate.Id
		AND (
				(aacAfterStartDate.DateTime BETWEEN CONVERT(datetime, CONVERT(nvarchar(10),@StartDate, 101)) AND @EndDate) 
				OR 
				(aacAfterStartDate.DateTime BETWEEN CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjStartDate,@StartDate), 101)) AND ISNULL(@AdjEndDate,@EndDate))
			)
	LEFT JOIN #AdjustmentsAndCharges aacMonthToDate ON aacMonthToDate.Id = aacAll.Id
		AND (
				(aacMonthToDate.DateTime BETWEEN	(SELECT CONVERT(DATETIME,CONVERT(NVARCHAR(2),DATEPART(MONTH,@EndDate))
							+ '/01/' + CONVERT(NVARCHAR(4),DATEPART(YEAR,@EndDate)),101)) AND @EndDate)
				OR
				((aacMonthToDate.DateTime BETWEEN	(SELECT CONVERT(DATETIME,CONVERT(NVARCHAR(2),DATEPART(MONTH,ISNULL(@AdjEndDate,@EndDate)))
							+ '/01/' + CONVERT(NVARCHAR(4),DATEPART(YEAR,ISNULL(@AdjEndDate,@EndDate))),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjEndDate,@EndDate), 101))))
			)
	LEFT JOIN #AdjustmentsAndCharges aacYearToDate ON aacYearToDate.Id = aacAll.id
		AND (
				(aacYearToDate.Datetime BETWEEN 	(SELECT CONVERT(DATETIME,'01/01/' 
							+ CONVERT(NVARCHAR(4),DATEPART(YEAR,@EndDate)),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),@EndDate, 101)))
				OR
				((aacYearToDate.Datetime BETWEEN 	(SELECT CONVERT(DATETIME,'01/01/' 
							+ CONVERT(NVARCHAR(4),DATEPART(YEAR,ISNULL(@AdjEndDate,@EndDate))),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjEndDate,@EndDate), 101))))
			)
	LEFT JOIN model.PaymentMethods pm ON pm.Id = aacAll.PaymentMethodId
GROUP BY
	aacAll.PaymentMethodId,
	CASE WHEN @GroupByBillingOrganization = 1 THEN i.BillingOrganizationId ELSE NULL END,
	CASE WHEN @GroupByUser = 1 THEN i.UserId ELSE NULL END,
	CASE WHEN @GroupByServiceLocation = 1 THEN i.ServiceLocationId ELSE NULL END
UNION
SELECT 
	SUM(COALESCE(oapAfterStartDate.Amount, 0)) AS Payment,
	SUM(COALESCE(oapMonthToDate.Amount,0)) AS PaymentMTD,
	SUM(COALESCE(oapYearToDate.Amount,0)) AS PaymentYTD,
	CASE WHEN SUM(COALESCE(oapAfterStartDate.Amount, 0)) <> 0 THEN oapAll.PaymentMethodId ELSE NULL END AS PaymentMethodId,
	CASE WHEN SUM(COALESCE(oapAfterStartDate.Amount, 0)) <> 0 THEN
			CASE WHEN MAX(pm.Name) IS NULL THEN 'OTHER' ELSE MAX(pm.Name) END
			ELSE NULL END AS PaymentMethodName, 
	0 AS NonPayment,
	0 AS Charge,
	0 AS AllCharges,
	SUM(COALESCE(oapAll.Amount, 0)) AS AllPayments,
	0 AS AllNonPayments,
	0 AS ChargeMTD,
	0 AS ChargeYTD,
	0 AS NonPaymentMTD,
	0 AS NonPaymentYTD,
	CASE WHEN @GroupByBillingOrganization = 1 THEN 0 ELSE NULL END AS BillingOrganizationId,
	CASE WHEN @GroupByBillingOrganization = 1 THEN @NoBO ELSE NULL END AS BillingOrganizationShortName,
	CASE WHEN @GroupByUser = 1 THEN 0 ELSE NULL END AS UserId,
	CASE WHEN @GroupByUser = 1 THEN @NoProvider ELSE NULL END AS UserName,
	CASE WHEN @GroupByServiceLocation = 1 THEN 0 ELSE NULL END AS ServiceLocationId,
	CASE WHEN @GroupByServiceLocation = 1 THEN @NoSL ELSE NULL END ServiceLocationShortName
FROM #OnAccountPayments oapAll
	LEFT JOIN #OnAccountPayments oapAfterStartDate ON oapAll.Id = oapAfterStartDate.Id
		AND (
				(oapAfterStartDate.DateTime BETWEEN CONVERT(datetime, CONVERT(nvarchar(10),@StartDate, 101)) AND @EndDate)
				OR
				(oapAfterStartDate.DateTime BETWEEN CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjStartDate,@StartDate), 101)) AND ISNULL(@AdjEndDate,@EndDate))
			)
	LEFT JOIN #OnAccountPayments oapMonthToDate ON oapAll.Id = oapMonthToDate.Id
		AND (
				(oapMonthToDate.Datetime BETWEEN (SELECT CONVERT(DATETIME,CONVERT(NVARCHAR(2),DATEPART(MONTH,@EndDate))
							+ '/01/' + CONVERT(NVARCHAR(4),DATEPART(YEAR,@EndDate)),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),@EndDate, 101)))
				OR
				(oapMonthToDate.Datetime BETWEEN (SELECT CONVERT(DATETIME,CONVERT(NVARCHAR(2),DATEPART(MONTH,ISNULL(@AdjEndDate,@EndDate)))
							+ '/01/' + CONVERT(NVARCHAR(4),DATEPART(YEAR,ISNULL(@AdjEndDate,@EndDate))),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjEndDate,@EndDate), 101)))
			)
	LEFT JOIN #OnAccountPayments oapYearToDate ON oapYearToDate.Id = oapAll.Id
		AND (
				(oapYearToDate.DateTime BETWEEN (SELECT CONVERT(DATETIME,'01/01/' 
							+ CONVERT(NVARCHAR(4),DATEPART(YEAR,@EndDate)),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),@EndDate, 101)))
				OR
				(oapYearToDate.DateTime BETWEEN (SELECT CONVERT(DATETIME,'01/01/' 
							+ CONVERT(NVARCHAR(4),DATEPART(YEAR,ISNULL(@AdjEndDate,@EndDate))),101)) AND CONVERT(datetime, CONVERT(nvarchar(10),ISNULL(@AdjEndDate,@EndDate), 101)))
			)
	LEFT JOIN model.PaymentMethods pm ON pm.Id = oapAll.PaymentMethodId
GROUP BY
	oapAll.PaymentMethodId
) v
GROUP BY v.PaymentMethodId,v.PaymentMethodName,
	v.BillingOrganizationId,v.BillingOrganizationShortName,
	v.UserId,UserName,
	v.ServiceLocationId,ServiceLocationShortName
ORDER BY BillingOrganizationShortName, UserName, ServiceLocationShortName

END	
RETURN
END

GO

-- exec model.GetChargesPaymentsAndAdjustmentsWithPaymentMethodAndOpenBalance '2014-08-01','2014-08-12',0,0,1