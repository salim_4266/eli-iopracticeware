IF OBJECT_ID('model.GetDailyPayments') IS NOT NULL
	DROP PROCEDURE model.GetDailyPayments

GO

CREATE PROCEDURE model.GetDailyPayments(
	@StartDate datetime, 
	@EndDate datetime, 
	@BillingOrganizations nvarchar(max),
	@Providers nvarchar(max),
	@ServiceLocations nvarchar(max),
	@Users nvarchar(max),
	@PaymentTypes nvarchar(max),
	@ShowBillingOrganizationBreakdown bit,
	@ShowProviderBreakdown bit,
	@ShowServiceLocationBreakdown bit
	)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT
	adj.Id AS AdjustmentId
	,pm.Name AS PaymentMethodName
	,CASE WHEN at.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END AS Amount
	,fb.PatientId AS PayerId
	,ap.PatientId AS PatientId
	,adj.InvoiceReceivableId
	,CONVERT(DATETIME, CONVERT(NVARCHAR(10),adj.PostedDateTime,101)) AS PostedDateTime
	,adj.FinancialSourceTypeId AS Payer
	,es.Code AS Service
	,sl.Id AS ServiceLocationId
	,sl.ShortName AS ServiceLocation
	,COALESCE(fb.CheckCode,'') AS CheckCode
	,COALESCE(ins.Name,'') AS InsurerName
	,COALESCE(ins.PlanName,'') AS InsurerPlan
INTO #Results
FROM model.Adjustments adj
INNER MERGE JOIN model.AdjustmentTypes at ON adj.AdjustmentTypeId = at.Id
LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
LEFT JOIN model.Invoices inv ON inv.Id = ir.InvoiceId
LEFT JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
LEFT JOIN dbo.Appointments ap ON ap.EncounterId = inv.EncounterId
LEFT JOIN model.ServiceLocations sl ON sl.Id = inv.AttributeToServiceLocationId
LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
INNER JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
LEFT JOIN model.PatientInsurances Pati ON pati.id = ir.patientinsuranceid
LEFT JOIN model.insurancepolicies ip ON ip.id = pati.insurancepolicyid
LEFT JOIN model.Insurers ins ON ins.Id = ip.InsurerId
WHERE at.IsCash = 1 
AND (adj.BillingServiceId IS NOT NULL OR adj.InvoiceReceivableId IS NOT NULL OR adj.PatientId IS NOT NULL)
AND ((@PaymentTypes IS NULL AND at.Id IN (SELECT Id FROM model.AdjustmentTypes)) OR 
	(@PaymentTypes IS NOT NULL AND at.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@PaymentTypes,','))))

CREATE INDEX IX_Results_PatientId ON #Results(PatientId)
CREATE INDEX IX_Results_AdjustmentId ON #Results(AdjustmentId)

CREATE TABLE #AuditUser(AdjustmentId int, UserId int, UserName nvarchar(255))

INSERT #AuditUser
SELECT Id, UserId, UserName
FROM (
	SELECT ROW_NUMBER () OVER (PARTITION BY a.Id ORDER BY ChangeTypeId DESC, AuditDateTime DESC) AS RANK,a.Id, ae.UserId,u.UserName
	FROM model.Adjustments a 
	LEFT JOIN AuditEntries ae ON ae.KeyValueNumeric = a.Id
	LEFT JOIN model.Users u ON u.Id = ae.UserId
	WHERE ObjectName = 'model.Adjustments'
	AND ae.ChangeTypeId = 0
	GROUP BY a.Id, ae.UserId, u.UserName,ChangeTypeId, AuditDateTime
) v 
WHERE RANK = 1

SELECT DISTINCT
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN COALESCE(bo.ShortName,'No Billing Organization') ELSE NULL END AS BillingOrganization,
	CASE WHEN @ShowProviderBreakdown = 1 THEN COALESCE(u.UserName,'No Provider') ELSE NULL END AS ProviderGroup,
	CASE WHEN @ShowServiceLocationBreakdown = 1 THEN COALESCE(r.ServiceLocation,'No Service Location') ELSE NULL END AS ServiceLocation,
	COALESCE(au.UserName,'UNKNOWN') AS [User],
	CASE r.Payer WHEN 1 THEN r.InsurerName WHEN 2 THEN pay.LastName + ', ' + pay.FirstName ELSE 'Office' END AS Payer,
	 r.AdjustmentId,r.PostedDatetime AS PostedDateTime
	,CONVERT(NVARCHAR(10),r.PostedDateTime,101) AS PostedDate
	,r.PaymentMethodName
	,r.Service
	,Amount, COALESCE(p.LastName + ', ','') AS LastName
	,p.FirstName AS FirstName,r.PatientId AS PatientId
	,r.CheckCode,r.InsurerName,r.InsurerPlan
 FROM #Results r
	LEFT JOIN model.Patients pay ON pay.Id = r.PayerId
	LEFT JOIN model.Patients p ON p.Id = r.PatientId
	LEFT JOIN model.InvoiceReceivables ir ON ir.Id = r.InvoiceReceivableId
	LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId
	LEFT JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	LEFT JOIN model.BillingOrganizations bo ON bo.Id = pr.BillingOrganizationId
	LEFT JOIN model.Users u ON u.Id = pr.UserId
	LEFT JOIN #AuditUser au ON r.AdjustmentId = au.AdjustmentId
	WHERE CONVERT(DATETIME,CONVERT(NVARCHAR(10),r.PostedDateTime,101)) BETWEEN @StartDate AND @EndDate
	AND ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations))) OR 
		(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,','))))
	AND ((@ServiceLocations IS NULL AND (r.ServiceLocationId IS NULL OR r.ServiceLocationId IN (SELECT Id FROM model.ServiceLocations))) OR 
		(@ServiceLocations IS NOT NULL AND r.ServiceLocationId IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Providers IS NULL AND (u.Id IS NULL OR u.Id IN (SELECT UserId FROM model.Providers))) OR 
		(@Providers IS NOT NULL AND u.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,','))))
	AND ((@Users IS NULL AND (au.UserId IS NULL OR au.UserId IN (SELECT UserId FROM #AuditUser))) OR 
		(@Users IS NOT NULL AND au.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,','))))
	GROUP BY
		CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN bo.ShortName ELSE NULL END,
		CASE WHEN @ShowProviderBreakdown = 1 THEN u.UserName ELSE NULL END,
		CASE WHEN @ShowServiceLocationBreakdown = 1 THEN r.ServiceLocation ELSE NULL END,
		bo.ShortName, u.UserName,r.ServiceLocation,au.UserName,r.Payer,r.AdjustmentId,r.PostedDatetime
		,r.PaymentMethodName, u.UserName, r.Service, r.ServiceLocationId, Amount, pay.LastName, pay.FirstName, p.LastName
		,p.FirstName,r.PatientId,r.CheckCode,r.InsurerName,r.InsurerPlan

END
RETURN
END

GO

-- exec model.GetDailyPayments '2014-11-21','2014-11-21',NULL,NULL,NULL,NULL,NULL,1,1,1
