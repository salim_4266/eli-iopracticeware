IF OBJECT_ID('model.CreatePatientReceipt') IS NOT NULL
	DROP PROCEDURE model.CreatePatientReceipt
GO

CREATE PROCEDURE [model].[CreatePatientReceipt] (@eid INT)
AS

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		i.Id
		,i.DiagnosisTypeId
		,bo.Name AS boName
		,boa.Line1 AS boLine1
		,boa.Line2 AS boLine2
		,boa.City AS boCity
		,sopBO.Abbreviation AS boState
		,CASE 
			WHEN LEN(boa.PostalCode) = 9
				THEN SUBSTRING(boa.PostalCode, 0, 6) + '-' + SUBSTRING(boa.PostalCode, 6, 4)
			ELSE boa.PostalCode
			END AS boZip
		,CASE 
			WHEN bop.AreaCode IS NULL
				THEN ''
			ELSE bop.AreaCode + '-' + SUBSTRING(bop.ExchangeAndSuffix, 0, 4) + '-' + SUBSTRING(bop.ExchangeAndSuffix, 4, 4) END AS ExchangeAndSuffix
		,COALESCE(idc.Value,'') AS TaxID
		,sl.Name AS ServiceLocation
		,sla.Line1 AS slLine1
		,' ' + sla.Line2 AS slLine2
		,sla.City AS slCity
		,sopSL.Abbreviation AS slState
		,CASE 
			WHEN LEN(sla.PostalCode) = 9
				THEN SUBSTRING(sla.PostalCode, 0, 6) + '-' + SUBSTRING(sla.PostalCode, 6, 4)
			ELSE sla.PostalCode
			END AS slZip
		,p.id AS PatientId
		,p.FirstName AS PatientFirst
		,p.MiddleName AS PatientMiddle
		,p.LastName AS PatientLast
		,p.Suffix AS PatientSuffix
		,pa.Line1 AS pLine1
		,CASE WHEN pa.Line2 IS NOT NULL AND pa.Line2 <> '' THEN ', ' + pa.Line2 ELSE '' END AS pLine2
		,pa.City AS pCity
		,sopP.Abbreviation AS pState
		,pa.PostalCOde AS pPostal
	INTO #temp
	FROM model.invoices i
	INNER JOIN model.encounters e ON i.EncounterId = e.Id
	INNER JOIN model.patients p ON p.id = e.PatientId
	INNER JOIN model.Providers bp ON bp.Id = i.BillingProviderId
	INNER JOIN model.BillingOrganizations bo ON bo.id = bp.BillingOrganizationId
	LEFT JOIN model.billingorganizationaddresses boa ON boa.BillingOrganizationId = bo.id
		AND boa.BillingOrganizationAddressTypeId = 5
	INNER JOIN model.ServiceLocations sl ON sl.Id = e.ServiceLocationId
	LEFT JOIN model.ServiceLocationAddresses sla ON sla.ServiceLocationId = sl.Id
		AND sla.ServiceLocationAddressTypeId = 7
	LEFT JOIN model.BillingOrganizationPhoneNumbers bop ON bop.BillingOrganizationId = bo.Id
		AND bop.ordinalid = 1
	LEFT JOIN model.IdentifierCodes idc ON idc.BillingOrganizationId = bo.id
		AND idc.IdentifierCodeTypeId = 7
	LEFT JOIN model.PatientAddresses pa ON pa.PatientId = p.Id
		AND pa.OrdinalId = 1
	LEFT JOIN model.StateOrProvinces sopBO ON sopBO.id = boa.StateOrProvinceId
	LEFT JOIN model.StateOrProvinces sopSL ON sopSL.Id = sla.StateOrProvinceId
	LEFT JOIN model.StateOrProvinces sopP ON sopP.Id = pa.StateOrProvinceId
	WHERE i.EncounterId = @eid

	SELECT CONVERT(nvarchar(10),a.DateTime,101) + ' ' + RIGHT(CONVERT(nvarchar(35),a.DateTime,100),7) AS Apptime
		,u.DisplayName
		,e.PatientId
		,a.Id AS Appointment
		,slA.Line1 + ', ' + sla.City AS FutureLocation
	INTO #FutureAppointments
	FROM model.Appointments a 
	INNER JOIN model.Encounters e on e.Id = a.EncounterId
	INNER JOIN model.Users u ON u.Id = a.UserId
	INNER JOIN model.ServiceLocationAddresses sla ON sla.ServiceLocationId = e.ServiceLocationId
		AND sla.ServiceLocationAddressTypeId = 7
	WHERE a.DateTime > GETDATE() AND e.EncounterStatusId = 1 

	SELECT i.Id
	,SUM(CAST(bs.Unit * bs.UnitCharge AS DECIMAL(10, 2))) AS SumCharges
	INTO #TotalCharge
	FROM model.invoices i
	INNER JOIN model.billingservices bs ON bs.InvoiceId = i.id
	WHERE i.encounterid = @eid
	GROUP BY i.Id

	SELECT Id
			,COALESCE(SUM(Payments),0) AS Payments
			,COALESCE(SUM(NonPayments),0) AS NonPayments
	INTO #TotalPayment 
	FROM (
		SELECT i.Id
			,CASE WHEN at.IsCash = 1
				THEN SUM(CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END) END AS Payments 
			,CASE WHEN at.IsCash = 0 
				THEN SUM(CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END) END AS NonPayments
		FROM model.invoices i
		INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.id
		INNER JOIN model.adjustments adj ON adj.InvoiceReceivableId = ir.Id
		INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
		WHERE i.encounterid = @eid
			AND adj.FinancialSourceTypeId IN (2,3)
		GROUP BY i.Id,at.Name,at.IsCash) v
		GROUP BY Id

	DECLARE @i INT
	DECLARE @encounterCode VARCHAR(20)
	DECLARE @EncounterServices TABLE (
		id INT PRIMARY KEY IDENTITY(1, 1)
		,encounterServiceCode NVARCHAR(20)
		)
	SET @i = 1

	DECLARE @CNTROW INT
	DECLARE @CNTDiagnosisCodes INT
	DECLARE @tmpCode VARCHAR(50) 
	DECLARE @tmpBillingServiceId VARCHAR(50)
	DECLARE @tmpOrdinalIdPart VARCHAR(50) 
	DECLARE @resultOrdinal VARCHAR(50)
	DECLARE @SingleOrdinal VARCHAR(10)
	DECLARE @CNTLEN INT
	DECLARE @TMPCNT INT

	CREATE TABLE #tmpIcd9DiagnosisCode
	(
		OrdinalId INT NOT NULL,
		BillingDiagnosisId INT NOT NULL,
		Icd9DiagnosisCode NVARCHAR(100) NULL
	) 

	CREATE TABLE #tmpDiagnosisCodes
	(
		ID INT IDENTITY(1,1)  NOT NULL, 
		Code VARCHAR(50) NOT NULL,
		BillingServiceId VARCHAR(50) NOT NULL,
		OrdinalIdPart1 VARCHAR(50) NULL,
		OrdinalIdPart2 VARCHAR(50) NULL,
		OrdinalIdPart3 VARCHAR(50) NULL,
		OrdinalIdPart4 VARCHAR(50) NULL,
		OrdinalIdPart5 VARCHAR(50) NULL,				 
		OrdinalIdPart6 VARCHAR(50) NULL,
		OrdinalIdPart7 VARCHAR(50) NULL
	) 

	DECLARE @FlagCNT INT
	SET @FlagCNT = (SELECT COUNT(*) FROM model.BillingDiagnosis WHERE InvoiceID IN 
	(SELECT ID FROM #temp WHERE DiagnosisTypeId = 1))

	IF (@FlagCNT > 0)
	BEGIN
	INSERT INTO #tmpIcd9DiagnosisCode 
	(OrdinalId, BillingDiagnosisId , Icd9DiagnosisCode )
	SELECT BD.OrdinalId, bsbd.BillingDiagnosisId,  esem.ExternalSystemEntityKey AS Icd9DiagnosisCode
	FROM model.ExternalSystemEntityMappings esem
	LEFT JOIN model.BillingDiagnosis BD  ON esem.Id = bd.ExternalSystemEntityMappingId
	LEFT JOIN model.BillingServiceBillingDiagnosis bsbd ON bd.Id = bsbd.BillingDiagnosisId
	LEFT JOIN model.BillingServices bs ON bs.Id = bsbd.BillingServiceId
	LEFT JOIN model.Invoices i ON i.id = bs.InvoiceId
	WHERE i.EncounterId = @eid
	ORDER BY BD.OrdinalId, bsbd.BillingDiagnosisId
	

	INSERT INTO #tmpDiagnosisCodes 
	(Code, BillingServiceId, OrdinalIdPart1, OrdinalIdPart2, OrdinalIdPart3, OrdinalIdPart4, OrdinalIdPart5, OrdinalIdPart6, OrdinalIdPart7 )
	SELECT es.Code, bs.Id AS BillingServiceId,
	CASE WHEN bd.OrdinalId = 1 THEN 'A' END AS OrdinalIdPart1
	,CASE WHEN bd.OrdinalId = 2 THEN 'B' END AS OrdinalIdPart2
	,CASE WHEN bd.OrdinalId = 3 THEN 'C' END AS OrdinalIdPart3
	,CASE WHEN bd.OrdinalId = 4 THEN 'D' END AS OrdinalIdPart4
	,CASE WHEN bd.OrdinalId = 5 THEN 'E' END AS OrdinalIdPart5
	,CASE WHEN bd.OrdinalId = 6 THEN 'F' END AS OrdinalIdPart6
	,CASE WHEN bd.OrdinalId = 7 THEN 'G' END AS OrdinalIdPart7
	FROM model.EncounterServices es
	INNER JOIN model.BillingServices bs ON es.Id = bs.EncounterServiceId
	INNER JOIN model.Invoices i on i.id = bs.InvoiceId
	INNER JOIN model.BillingServiceBillingDiagnosis bsbd ON bs.Id = bsbd.BillingServiceId
	INNER JOIN model.BillingDiagnosis bd ON bd.Id = bsbd.BillingDiagnosisId
	WHERE i.EncounterId = @eid
	ORDER BY bsbd.BillingServiceId, bsbd.OrdinalId
	END
	ELSE 
	BEGIN

	INSERT INTO #tmpIcd9DiagnosisCode 
	(OrdinalId, BillingDiagnosisId , Icd9DiagnosisCode )
	SELECT BD.OrdinalId, bsbd.BillingDiagnosisId, BD.DiagnosisCode AS Icd9DiagnosisCode
	FROM model.BillingDiagnosisIcd10 BD  
	LEFT JOIN model.BillingServiceBillingDiagnosis bsbd ON bd.Id = bsbd.BillingDiagnosisId
	LEFT JOIN model.BillingServices bs ON bs.Id = bsbd.BillingServiceId
	LEFT JOIN model.Invoices i ON i.id = bs.InvoiceId
	WHERE i.EncounterId = @eid
	ORDER BY bsbd.BillingDiagnosisId

	INSERT INTO #tmpDiagnosisCodes 
	(Code, BillingServiceId, OrdinalIdPart1, OrdinalIdPart2, OrdinalIdPart3, OrdinalIdPart4, OrdinalIdPart5, OrdinalIdPart6, OrdinalIdPart7)
	SELECT es.Code, bs.Id AS BillingServiceId,
	CASE WHEN bd.OrdinalId = 1 THEN 'A' END AS OrdinalIdPart1
	,CASE WHEN bd.OrdinalId = 2 THEN 'B' END AS OrdinalIdPart2
	,CASE WHEN bd.OrdinalId = 3 THEN 'C' END AS OrdinalIdPart3
	,CASE WHEN bd.OrdinalId = 4 THEN 'D' END AS OrdinalIdPart4
	,CASE WHEN bd.OrdinalId = 5 THEN 'E' END AS OrdinalIdPart5
	,CASE WHEN bd.OrdinalId = 6 THEN 'F' END AS OrdinalIdPart6
	,CASE WHEN bd.OrdinalId = 7 THEN 'G' END AS OrdinalIdPart7
	FROM model.EncounterServices es
	INNER JOIN model.BillingServices bs ON es.Id = bs.EncounterServiceId
	INNER JOIN model.Invoices i on i.id = bs.InvoiceId
	INNER JOIN model.BillingServiceBillingDiagnosis bsbd ON bs.Id = bsbd.BillingServiceId
	INNER JOIN model.BillingDiagnosisIcd10 bd ON bd.Id = bsbd.BillingDiagnosisId
	WHERE i.EncounterId = @eid
	ORDER BY bsbd.BillingServiceId, bsbd.OrdinalId
	END

	SET @CNTROW = @@ROWCOUNT

	CREATE TABLE #DiagnosisCodes 
	(
		ID INT IDENTITY(1,1)  NOT NULL, 
		Code VARCHAR(50) NOT NULL,
		BillingServiceId VARCHAR(50) NOT NULL,
		OrdinalIdPart VARCHAR(50) NULL,
		OrdinalIdPart1 VARCHAR(50) NULL,
		OrdinalIdPart2 VARCHAR(50) NULL,
		OrdinalIdPart3 VARCHAR(50) NULL,
		OrdinalIdPart4 VARCHAR(50) NULL,
		OrdinalIdPart5 VARCHAR(50) NULL,		
		OrdinalIdPart6 VARCHAR(50) NULL,
		OrdinalIdPart7 VARCHAR(50) NULL		
	)

	INSERT INTO #DiagnosisCodes 
	(Code, BillingServiceId)
	SELECT MAX(Code), MAX(BillingServiceId)	FROM #tmpDiagnosisCodes
	GROUP BY Code, BillingServiceId

	SET @CNTDiagnosisCodes = @@ROWCOUNT

	DECLARE @CNT INT
	SET @CNT = 1
	
	WHILE (@CNT <= @CNTROW)
	BEGIN
		SET @tmpCode = ''
		SET @tmpBillingServiceId = ''
		SET @tmpOrdinalIdPart = ''
		SET @resultOrdinal = ''
		
		SELECT @tmpCode = Code, 
		@tmpBillingServiceId = BillingServiceId,
		@tmpOrdinalIdPart = ISNULL(OrdinalIdpart1, '') + ISNULL(OrdinalIdpart2, '') + ISNULL(OrdinalIdpart3, '') 
		+ ISNULL(OrdinalIdpart4, '') + ISNULL(OrdinalIdpart5, '')  + ISNULL(OrdinalIdpart6, '')  + ISNULL(OrdinalIdpart7, '')
		
		FROM  #tmpDiagnosisCodes WHERE ID = @CNT
		
		UPDATE #DiagnosisCodes
		SET OrdinalIdPart = ISNULL(OrdinalIdPart, '') + @tmpOrdinalIdPart
		WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
		
		SET @CNT = @CNT + 1
	END

	
	SET @CNT = 1
	
	WHILE (@CNT <= @CNTDiagnosisCodes)
	BEGIN
		SET @tmpCode = ''
		SET @tmpBillingServiceId = ''
		SET @tmpOrdinalIdPart = ''
		SET @SingleOrdinal = ''
		SET @TMPCNT = 1

		SELECT @tmpCode = Code, 
		@tmpBillingServiceId = BillingServiceId,
		@tmpOrdinalIdPart = ISNULL(OrdinalIdpart,'') FROM  #DiagnosisCodes WHERE ID = @CNT
		
		SET @CNTLEN = LEN(@tmpOrdinalIdPart)
		PRINT @CNTLEN

		IF @CNTLEN >=1
		BEGIN
			WHILE (@TMPCNT <= @CNTLEN)
			BEGIN
				IF(@TMPCNT = 1)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart1 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 2)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart2 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 3)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart3 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 4)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart4 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 5)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart5 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 6)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart6 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				ELSE IF(@TMPCNT = 7)
				BEGIN
					UPDATE #DiagnosisCodes
					SET OrdinalIdPart7 = SUBSTRING(@tmpOrdinalIdPart,@TMPCNT,1)
					WHERE Code = @tmpCode AND BillingServiceId = @tmpBillingServiceId
				END
				SET @TMPCNT = @TMPCNT + 1
			END
		END
		
		SET @CNT = @CNT + 1
	END

	/*
	SELECT  MAX(Code) AS Code, 
	MAX(BillingServiceId) AS BillingServiceId, 
	MAX(OrdinalIdpart1) AS OrdinalIdPart1, 
	MAX(OrdinalIdPart2) AS OrdinalIdPart2, 
	MAX(OrdinalIdPart3) AS OrdinalIdPart3, 
	MAX(OrdinalIdPart4) AS OrdinalIdPart4,
	MAX(OrdinalIdPart5) AS OrdinalIdPart5
	INTO #DiagnosisCodes
	FROM #tmpDiagnosisCodes
	GROUP BY Code, BillingServiceId 
	*/
	

	SELECT  ROW_NUMBER() OVER (PARTITION BY BillingServiceId ORDER BY BillingServiceId, LEN(OrdinalId) DESC) AS RANK, Code, BillingServiceId,OrdinalId
	INTO #DiagnosisLinks
	FROM (
	SELECT DISTINCT 
	dc1.Code, dc1.BillingServiceId,  
	COALESCE(CONVERT(NVARCHAR(1),dc1.OrdinalIdPart1),'') + 
	COALESCE(CONVERT(NVARCHAR(1),dc2.OrdinalIdPart2),'') + 
	COALESCE(CONVERT(NVARCHAR(1),dc3.OrdinalIdPart3),'') +
	COALESCE(CONVERT(NVARCHAR(1),dc4.OrdinalIdPart4),'') +
	COALESCE(CONVERT(NVARCHAR(1),dc5.OrdinalIdPart5),'') +
	COALESCE(CONVERT(NVARCHAR(1),dc6.OrdinalIdPart6),'') +
	COALESCE(CONVERT(NVARCHAR(1),dc7.OrdinalIdPart7),'') AS OrdinalId
	FROM #DiagnosisCodes dc1
	INNER JOIN #DiagnosisCodes dc2 ON dc2.BillingServiceId = dc1.BillingServiceId
	INNER JOIN #DiagnosisCodes dc3 ON dc3.BillingServiceId = dc2.BillingServiceId
	INNER JOIN #DiagnosisCodes dc4 ON dc4.BillingServiceId = dc3.BillingServiceId
	INNER JOIN #DiagnosisCodes dc5 ON dc5.BillingServiceId = dc4.BillingServiceId
	INNER JOIN #DiagnosisCodes dc6 ON dc6.BillingServiceId = dc5.BillingServiceId
	INNER JOIN #DiagnosisCodes dc7 ON dc7.BillingServiceId = dc6.BillingServiceId
	) v

	
	SELECT en.PatientId, adj.Id AS Id, inv.Id AS Invoice,
		SUM(CASE WHEN IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END) AS Amount,
		adj.PostedDateTime AS PostDate,
		at.Name AS [Type],
		adj.FinancialSourceTypeId,
		at.IsCash,
		CASE WHEN adj.IncludeCommentOnStatement = 1 THEN adj.Comment
			ELSE '' END AS Comment,
		fb.CheckCode,
		pm.Name AS PaymentMethod
	INTO #Totals
	FROM model.InvoiceReceivables ir
	INNER JOIN model.Adjustments adj ON adj.InvoiceReceivableId = ir.Id
	INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId
	INNER JOIN model.Invoices inv ON inv.Id = ir.InvoiceId
	INNER JOIN model.Encounters en ON en.Id = inv.EncounterId
	LEFT JOIN model.BillingServices bs ON adj.BillingServiceId = bs.Id
	LEFT JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	INNER JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
	WHERE en.Id = @eid
		AND adj.FinancialSourceTypeId IN (2,3)
	GROUP BY inv.Id, adj.Id, en.patientId, fb.InsurerId, adj.PostedDateTime, at.Name, adj.Comment, adj.FinancialSourceTypeId,adj.IncludeCommentOnStatement,fb.CheckCode,pm.Name,at.IsCash

	DECLARE @InvoiceComment NVARCHAR(MAX)
	SET @InvoiceComment = ''
	SELECT @InvoiceComment = @InvoiceComment + Value + ' 
	' 
	FROM model.InvoiceComments ic
	INNER JOIN #temp t ON ic.InvoiceId = t.id
	AND ic.IncludeOnStatement = 1 AND ic.IsArchived = 0

	DECLARE @BillingServiceComment NVARCHAR(MAX)
	SET @BillingServiceComment = ''
	SELECT @BillingServiceComment = @BillingServiceComment + Value + ' 
	' 
	FROM model.billingservicecomments bc
	INNER JOIN #DiagnosisCodes dc ON bc.BillingServiceId = dc.BillingServiceId
	AND bc.IsIncludedOnStatement = 1 AND bc.IsArchived = 0

	SELECT DISTINCT t.*
		,CASE WHEN bs.Id IS NULL THEN '' ELSE CONVERT(VARCHAR(10), e.StartDateTime, 101) END AS DATETIME
		,es.Code AS EncounterCode
		,bs.OrdinalId AS bsOrdinal
		,dbo.GetBillingServiceModifiers(bs.Id) AS Modifier
		,CAST(bs.Unit AS DECIMAL(10, 2)) AS Qty
		,icd.OrdinalID AS Diagnosis
		,CASE icd.OrdinalId 
			WHEN 1 THEN 'A'
			WHEN 2 THEN 'B'
			WHEN 3 THEN 'C'
			WHEN 4 THEN 'D'
			WHEN 5 THEN 'E'
			WHEN 6 THEN 'F'
			WHEN 7 THEN 'G'
			WHEN 8 THEN 'H'
			WHEN 9 THEN 'I'
			WHEN 10 THEN 'J'
			WHEN 11 THEN 'K'
			WHEN 12 THEN 'L'
			END AS LetterDiagnosis
		,dl.OrdinalId AS NumDiags
		,es.Description AS EncounterServiceDescription
		,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20, 2)) AS Charge
		,COALESCE(tc.SumCharges,0) AS SumCharges
		,icd.Icd9DiagnosisCode AS Icd9DiagnosisCode
		,COALESCE(tp.Payments,0) AS SumPayments
		,COALESCE(tp.NonPayments,0) AS SumAdjustments
		,tt.PostDate AS PaymentDateTime
		,tt.Id AS AdjustmentId
		,tt.Amount
		,tt.Type AS PaymentType
		,tt.CheckCode
		,tt.PaymentMethod AS PaymentName
		,COALESCE(@InvoiceComment,'') AS InvoiceNotes
		,COALESCE(@BillingServiceComment,'') AS BillingServiceNote
		,id.Value AS NPI
		,u.DisplayName
		,CASE tt.FinancialSourceTypeId WHEN 2 THEN t.PatientLast 
										WHEN 3 THEN 'Office' END AS Payer
		,bs.Id AS BillingServiceId
		,fa.Appointment AS FutureAppointment
		,fa.Apptime AS FutureAppointmentDate
		,fa.DisplayName AS FutureDoctor
		,fa.FutureLocation
		,CASE WHEN fa.Appointment IS NULL THEN 0 ELSE 1 END AS HasFutureAppointment
	FROM model.invoices i
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
	INNER JOIN model.Providers bp ON bp.Id = i.BillingProviderId
	INNER JOIN model.Users u ON u.Id = bp.UserId
	INNER JOIN #temp t ON t.Id = i.Id
	LEFT JOIN #FutureAppointments fa ON fa.PatientId = t.PatientId
	LEFT JOIN #TotalCharge tc ON tc.Id = i.Id
	LEFT JOIN #TotalPayment tp ON tp.Id = i.Id
	LEFT JOIN model.BillingServices bs ON bs.InvoiceId = i.Id
	LEFT JOIN model.BillingServiceBillingDiagnosis bsbd ON bs.Id = bsbd.BillingServiceId
	LEFT JOIN model.BillingDiagnosis bd ON bd.Id = bsbd.BillingDiagnosisId
	LEFT JOIN #tmpIcd9DiagnosisCode icd ON icd.BillingDiagnosisId = bsbd.BillingDiagnosisId
	LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	LEFT JOIN #DiagnosisLinks dl ON dl.Code = es.Code
	AND dl.BillingServiceId = bs.ID
		AND Rank = 1
	LEFT JOIN #Totals tt ON tt.Invoice = i.Id
	LEFT JOIN model.IdentifierCodes id ON id.ProviderId = i.BillingProviderId
		AND id.IdentifierCodeTypeId = 2
	WHERE i.encounterId = @eid
    ORDER BY fa.Apptime ASC
END

GO