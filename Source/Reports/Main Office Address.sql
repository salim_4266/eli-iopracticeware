IF OBJECT_ID('model.GetMainOfficeAddress') IS NOT NULL
	DROP PROCEDURE model.GetMainOfficeAddress

GO

CREATE PROCEDURE model.GetMainOfficeAddress
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT TOP 1 
	a.Id
	,model.ToUpperCaseWords(bo.Name) AS MainOfficeName
	,a.Line1 AS MainOfficeLine1
	,model.ToUpperCaseWords(a.Line2) AS MainOfficeLine2
	,a.Line3 AS MainOfficeLine3
	,model.ToUpperCaseWords(a.City) AS MainOfficeCity
	,sop.Abbreviation AS MainOfficeState
	,SUBSTRING(a.PostalCode, 0,6) AS MainOfficePostalCode
FROM model.BillingOrganizationAddresses a
JOIN model.StateOrProvinces sop ON a.StateOrProvinceId = sop.Id
JOIN model.BillingOrganizations bo ON a.BillingOrganizationId = bo.Id
WHERE bo.IsMain = 1

RETURN
END

GO
