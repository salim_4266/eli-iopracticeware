IF OBJECT_ID('model.GetDailyPickUpSchedule') IS NOT NULL
	DROP PROCEDURE model.GetDailyPickUpSchedule

GO

CREATE PROCEDURE model.GetDailyPickUpSchedule(@StartDate datetime, 
										@EndDate datetime, 
										@ServiceLocations nvarchar(max), 
										@Doctors nvarchar(max))
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT a.Id AS AppointmentId,
	a.[Datetime] AS Fulldatetime,
	CONVERT(nvarchar(10), a.DateTime, 101) AS AppDate,
	RIGHT(CONVERT(nvarchar(20), a.DateTime, 100), 7) AS AppTime,
	p.Id AS PatientId,
	p.LastName AS LastName,
	CASE WHEN p.Suffix = '' OR p.Suffix = ' ' 
		THEN ''
		ELSE p.Suffix END AS Suffix,
	p.FirstName AS FirstName,
	p.MiddleName AS MiddleName,
	CASE WHEN ppn.AreaCode IS NULL THEN ppn.ExchangeAndSuffix
	ELSE ppn.AreaCode + '\n' + ppn.ExchangeAndSuffix END AS PhoneNumber,
	p.PriorPatientCode AS PriorId,
	CASE 
		WHEN Pt.DisplayName IS NULL
			THEN '' 
		ELSE Pt.DisplayName
		END AS PatientType,
	CONVERT(NVARCHAR(10),p.DateOfBirth,101) AS DOB,
	model.GetAgeFromDob(p.DateOfBirth) AS Age,	
	padd.Line1 + CASE WHEN COALESCE(padd.Line2,'') = '' THEN '' ELSE '<Br/>' +padd.Line2 END
	+ CASE WHEN  COALESCE(padd.Line3,'') = '' THEN '' ELSE '<Br/>' +padd.Line3 END
	+ '<Br/>' + padd.City + ', ' + stat.Abbreviation + ' ' + padd.PostalCode AS [Address],
	COALESCE(a.Comment,'') AS Notes,
	sl.ShortName AS ServiceLocationShortName,
	u.DisplayName AS Provider
FROM model.Appointments a
INNER JOIN model.Encounters e ON e.Id = a.EncounterId
INNER JOIN model.Patients p ON p.Id = e.PatientId
INNER MERGE JOIN model.AppointmentTypes at ON at.Id = a.AppointmentTypeId
	AND at.Id > 0
INNER JOIN model.ServiceLocations sl ON sl.Id = e.ServiceLocationId
INNER JOIN model.Users u ON u.Id = a.UserId
LEFT JOIN model.PatientExternalProviders pep ON pep.PatientId = p.Id
	AND pep.IsReferringPhysician = 1
LEFT JOIN model.PatientPhoneNumbers ppn ON ppn.PatientId = p.Id
	AND ppn.OrdinalId = 1
LEFT JOIN model.PatientAddresses padd ON padd.PatientId = p.Id
	AND padd.OrdinalId = 1
INNER JOIN model.StateOrProvinces stat ON stat.Id = padd.StateOrProvinceId
LEFT JOIN model.ExternalContacts ec on ec.Id = pep.ExternalProviderId
LEFT JOIN model.PatientTags ppt ON ppt.PatientId = p.Id
	AND ppt.OrdinalId = 1
LEFT MERGE JOIN model.Tags pt ON pt.Id = ppt.TagId
WHERE e.EncounterStatusId IN (1,2,3,4,5,6,7,15)
AND CONVERT(datetime, CONVERT(nvarchar(10),a.DateTime, 101)) BETWEEN CONVERT(datetime, CONVERT(nvarchar(10), @StartDate, 101)) AND CONVERT(datetime, CONVERT(nvarchar(10), @EndDate, 101))
	AND ((@ServiceLocations IS NULL AND sl.ID IN (SELECT Id FROM model.ServiceLocations))
		OR (@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Doctors IS NULL AND u.Id IN (SELECT Id FROM model.Users)) OR 
		(@Doctors IS NOT NULL AND u.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Doctors,','))))
END
RETURN
END

GO

-- exec model.GetDailySchedule '2014-03-01','2014-04-01',NULL,NULL 