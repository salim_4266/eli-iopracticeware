IF OBJECT_ID('model.GetPatientInsurers') IS NOT NULL
	DROP PROCEDURE model.GetPatientInsurers

GO

CREATE PROCEDURE model.GetPatientInsurers(@StartDate datetime,
											@EndDate datetime, 
											@Insurers nvarchar(max), 
											@EncounterServices nvarchar(max), 
											@DiagnosisCodes nvarchar(max),
											@BreakdownByInsurer bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY i.Id, pi.InsuranceTypeId ORDER BY pi.OrdinalId, ir.Id ASC) AS Rank, 
	i.Id AS InvoiceId, CAST(ir.InvoiceId AS NVARCHAR(20)) + '-' + CAST(ir.PatientInsuranceId AS NVARCHAR(20)) AS InvoiceInsurance,ip.InsurerId
	into #Patientinsurancesordered
	FROM model.Invoices i 
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id 
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId

SELECT DISTINCT
	CASE WHEN @BreakdownByInsurer = 1 THEN ins.Name ELSE NULL END AS Name
	,p.LastName + ', ' + p.FirstName as PatientsName
	,p.Id
	,e.StartDateTime
	,CONVERT(NVARCHAR(10),e.StartDateTime,101) AS StartDate
	,esem.ExternalSystemEntityKey AS Icd9DiagnosisCode
	,es.Code
	,es.Description
FROM
	model.Invoices i
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.Patients p ON p.Id = e.PatientId
	INNER JOIN model.InvoiceReceivables ir ON i.Id = ir.InvoiceId
	INNER JOIN #PatientInsurancesOrdered pio ON CAST(ir.InvoiceId AS NVARCHAR(20)) + '-' + CAST(ir.PatientInsuranceId AS NVARCHAR(20)) = pio.InvoiceInsurance
		AND Rank = 1
	INNER JOIN model.Insurers ins ON ins.Id = pio.InsurerId
	INNER JOIN model.BillingServices bs ON i.Id = bs.InvoiceId
	INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
	INNER JOIN model.BillingDiagnosis bd ON bd.InvoiceId = i.Id
	INNER JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId
WHERE CONVERT(DATETIME,(CONVERT(NVARCHAR(10),e.[StartDateTime],101))) BETWEEN @StartDate AND @EndDate
AND ((@Insurers IS NULL AND ins.Id IN (SELECT Id FROM model.Insurers)) OR 
	(@Insurers IS NOT NULL AND ins.Name IN (SELECT CONVERT(nvarchar(max),nstr) FROM CharTable(@Insurers,','))))
AND ((@EncounterServices IS NULL AND es.Code IN (SELECT Code FROM model.EncounterServices)) OR 
	(@EncounterServices IS NOT NULL AND es.Code IN (SELECT CONVERT(nvarchar(max),nstr) FROM CharTable(@EncounterServices,','))))
AND ((@DiagnosisCodes IS NULL AND esem.Id IN (SELECT ExternalSystemEntityMappingId FROM model.BillingDiagnosis)) OR
	(@DiagnosisCodes IS NOT NULL AND esem.ExternalSystemEntityKey IN (SELECT CONVERT(nvarchar(max),nstr) FROM CharTable(@DiagnosisCodes,','))))
END
RETURN
END

GO

--  exec model.GetPatientInsurers '2014-01-01','2014-01-30',NULL,NULL,NULL,0