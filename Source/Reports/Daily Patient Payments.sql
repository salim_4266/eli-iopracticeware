IF OBJECT_ID('model.GetDailyPatientPayments') IS NOT NULL
	DROP PROCEDURE model.GetDailyPatientPayments

GO

CREATE PROCEDURE model.GetDailyPatientPayments(@StartDate datetime, 
											@EndDate datetime,
											@BillingOrganizations nvarchar(max), 
											@Providers nvarchar(max), 
											@ServiceLocations nvarchar(max), 
											@Users nvarchar(max), 
											@PaymentTypes nvarchar(max),
											@BreakdownByBillingOrganization bit,
											@BreakdownByProvider bit,
											@BreakdownByServiceLocation bit,
											@BreakdownByPaymentMethod bit)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SET @StartDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@StartDate,101))
SET @EndDate = CONVERT(DATETIME,CONVERT(NVARCHAR(10),@EndDate,101))

SELECT a.Id AS AdjustmentId
	,at.Name AS PaymentType
	,pm.Name AS PaymentMethodName
	,fb.CheckCode AS ReferenceNumber
	,fb.Amount AS CheckAmount
	,CASE WHEN at.IsDebit = 1 THEN - a.Amount ELSE a.Amount END AS Amount
	,fb.PatientId
	,a.InvoiceReceivableId
	,a.BillingServiceId
	,a.PostedDateTime AS DateTime
	,a.FinancialSourceTypeId
INTO #Results
FROM model.Adjustments a
INNER MERGE JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
LEFT JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
INNER MERGE JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
WHERE at.IsCash = 1 AND a.FinancialSourceTypeId = 2
AND ((@PaymentTypes IS NULL AND at.Id IN (SELECT Id FROM model.AdjustmentTypes WHERE at.IsCash = 1))
	 OR (@PaymentTypes IS NOT NULL AND at.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@PaymentTypes,','))))

UNION 

SELECT a.Id AS AdjustmentId
	,at.Name AS PaymentType
	,pm.Name AS PaymentMethodName
	,fb.CheckCode AS ReferenceNumber
	,fb.Amount AS CheckAmount
	,CASE WHEN at.IsDebit = 1 THEN - a.Amount ELSE a.Amount END AS Amount
	,COALESCE(a.PatientId, e.Patientid) AS PatientId
	,a.InvoiceReceivableId
	,a.BillingServiceId
	,a.PostedDateTime AS DateTime
	,a.FinancialSourceTypeId
FROM model.Adjustments a
INNER MERGE JOIN model.AdjustmentTypes at ON a.AdjustmentTypeId = at.Id
LEFT JOIN model.FinancialBatches fb ON fb.Id = a.FinancialBatchId
INNER MERGE JOIN model.PaymentMethods pm ON pm.Id = fb.PaymentMethodId
LEFT JOIN model.InvoiceReceivables ir ON ir.Id = a.InvoiceReceivableId
LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE at.IsCash = 1 AND a.FinancialSourceTypeId = 3
AND (a.InvoiceReceivableId IS NOT NULL OR a.BillingServiceId IS NOT NULL OR a.PatientId IS NOT NULL)
AND ((@PaymentTypes IS NULL AND at.Id IN (SELECT Id FROM model.AdjustmentTypes WHERE at.IsCash = 1))
	 OR (@PaymentTypes IS NOT NULL AND at.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@PaymentTypes,','))))

CREATE INDEX IX_Results_PatientId ON #Results(PatientId)
CREATE INDEX IX_Results_AdjustmentId ON #Results(AdjustmentId)

CREATE TABLE #AuditUser(AdjustmentId int, UserId int, UserName nvarchar(255))

INSERT #AuditUser
SELECT Id, UserId, UserName
FROM (
	SELECT ROW_NUMBER () OVER (PARTITION BY a.Id ORDER BY ChangeTypeId DESC, AuditDateTime DESC) AS RANK,a.Id, ae.UserId,u.UserName
	FROM model.Adjustments a 
	LEFT JOIN AuditEntries ae ON ae.KeyValueNumeric = a.Id
	LEFT JOIN model.Users u ON u.Id = ae.UserId
	WHERE ObjectName = 'model.Adjustments'
	AND ae.ChangeTypeId = 0
	GROUP BY a.Id, ae.UserId, u.UserName,ChangeTypeId, AuditDateTime
) v 
WHERE RANK = 1

SELECT DISTINCT 	
	CASE WHEN @BreakdownByBillingOrganization = 0 THEN NULL ELSE COALESCE(bo.ShortName,'No Billing Organization') END AS BillingOrganizationShortName
	,CASE WHEN @BreakdownByServiceLocation = 0 THEN NULL ELSE COALESCE(sl.ShortName,'No Service Location') END AS ServiceLocation 
	,CASE WHEN @BreakdownByProvider = 0 THEN NULL ELSE COALESCE(u.UserName,'No Provider') END AS Provider
	,r.AdjustmentId,	r.Datetime AS PostedDateTime
	,COALESCE(au.UserName,'UNKNOWN') AS [User]
	,PaymentType, COALESCE(r.ReferenceNumber,'') AS ReferenceNumber
	,PaymentMethodName, r.Amount
	,CASE WHEN r.FinancialSourceTypeId = 2 THEN p.LastName + ', ' + p.FirstName
	 ELSE 'Office' END AS Source,r.PatientId AS PatientId
 FROM #Results r
	INNER JOIN model.Patients p ON p.Id = r.PatientId
	LEFT JOIN model.InvoiceReceivables ir ON ir.Id = r.InvoiceReceivableId
	LEFT JOIN model.BillingServices bs ON bs.Id = r.BillingServiceId
	LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId
	LEFT JOIN model.ServiceLocations sl ON sl.Id = i.AttributeToServiceLocationId
	LEFT MERGE JOIN model.Providers pr ON pr.Id = i.BillingProviderId
	LEFT MERGE JOIN model.BillingOrganizations bo ON bo.Id = pr.BillingOrganizationId
	LEFT MERGE JOIN model.Users u ON u.Id = pr.UserId
	LEFT JOIN #AuditUser au ON r.AdjustmentId = au.AdjustmentId
	WHERE CONVERT(DATETIME,CONVERT(NVARCHAR(10),r.DateTime,101)) BETWEEN @StartDate AND @EndDate
	AND ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations)))
		OR (@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,','))))
	AND ((@Providers IS NULL AND (u.Id IS NULL OR u.Id IN (SELECT UserId FROM model.Providers)))
		OR (@Providers IS NOT NULL AND u.ID IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,','))))
	AND ((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations)))
		OR (sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,','))))
	AND ((@Users IS NULL AND (au.UserId IS NULL OR au.UserId IN (SELECT Id FROM model.Users)))
		OR (@Users IS NOT NULL AND au.UserId IN (SELECT CONVERT(int,nstr) FROM CharTable(@Users,','))))

END
RETURN
END

GO
/*
exec model.GetDailyPatientPayments '2014-04-01'
									,'2014-05-05'
									,NULL
									,NULL
									,NULL
									,NULL
									,NULL,0,0,0,0
									*/