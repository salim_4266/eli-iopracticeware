SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ============================================================
-- Author:		Rawle A.
-- Create date: 20130409
-- Scope of SP: This stored proc will find appointments that are not in the model.ScheduleBlockAppointmentCategories table.
--				Example scenarios:
--				1)Doctor sees patient in office on Thursdays & decides he wants to do surgery every Thursday from this day forward.
--				Practice creates a template with location ASC and hits apply forward. The appointments that were schedule prior 
--				to this date that have locations that are not in the ASC are now orphans. 
--				2)Orphans that are out of the time range (doctor decides he wants to work till 4 instead of 5.) They create a 
--				template and apply it forward. There are appointments scheduled at 4:15 and 4:30 prior to the change. 
--				They are now orphans.
-- ============================================================*/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'model.GetOrphanedAppointments') AND type in (N'P', N'PC'))
	EXEC('DROP PROCEDURE model.GetOrphanedAppointments')
GO

CREATE PROCEDURE model.GetOrphanedAppointments
(
	@StartDate DATETIME
	,@EndDate DATETIME
	,@Doctor NVARCHAR(MAX)
	,@Office NVARCHAR(MAX)
	,@Resource NVARCHAR(MAX)
	,@DobOrAge INT
)

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

DECLARE @NumOperatorsComingIn INT
SET @NumOperatorsComingIn =
(
	SELECT 
	COUNT(CONVERT(INT,nstr)) AS NumOperatorsInString
	FROM CharTable(@Resource, ',')
)

DECLARE @TotalNumOperators INT
SET @TotalNumOperators =
(
	SELECT
	COUNT(Id) AS TotalNumOperators
	FROM model.Users
	WHERE __EntityType__ = 'User'
)

;WITH CTE AS
(
SELECT 
* 
FROM model.Appointments
WHERE Id NOT IN
(
	SELECT 
	AppointmentId
	FROM model.ScheduleBlockAppointmentCategories
	WHERE AppointmentId IS NOT NULL
)
AND DateTime BETWEEN @StartDate AND DATEADD(dd,1,@EndDate)
)
SELECT 
CONVERT(NVARCHAR,a.DateTime,101) AS AppointmentDate
,LTRIM(RIGHT(CONVERT(NVARCHAR(25),a.DateTime,100),7)) AS AppointmentTime
,u.DisplayName AS DoctorName
,sl.ShortName AS Office
,ec.PatientId
,p.PriorPatientCode
,p.LastName+', '+p.FirstName+' '+p.MiddleName AS PatientName
,@DobOrAge AS AgeDobDisplayListId
,CASE 
	WHEN @DobOrAge = 1
		THEN DATEDIFF(yy,p.DateOfBirth,GETDATE())
	ELSE 
		CONVERT(NVARCHAR,DateOfBirth,112)
END AS RequestedAgeType
,at.Name AS AppointmentType
,RTRIM(SUBSTRING(pt.Name, 1, CHARINDEX(' -', pt.Name))) AS PatientType
,a.Comment AS AppointmentComment
,dbore.ResourceLastName+', '+dbore.ResourceFirstName AS CreatedBy
,@Doctor AS DoctorParameter
FROM CTE a
JOIN model.Encounters ec ON ec.Id=a.EncounterId
JOIN dbo.Appointments dboap ON dboap.AppointmentId=ec.Id
LEFT JOIN dbo.Resources dbore ON dbore.ResourceId=dboap.TechApptTypeId
JOIN model.Patients p ON p.Id=ec.PatientId
LEFT JOIN model.AppointmentTypes at ON at.Id=a.AppointmentTypeId
JOIN model.ServiceLocations sl ON sl.Id=ec.ServiceLocationId
JOIN model.Users u ON u.Id=a.UserId
LEFT JOIN model.PatientTags ppt ON ppt.PatientId=p.Id
LEFT JOIN model.Tags pt ON pt.Id=ppt.TagId
WHERE 
ec.EncounterStatusId IN (1,2) --where appointment is either pending or rescheduled
AND ((@Office IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR
	(@Office IS NOT NULL AND sl.Id IN (SELECT CONVERT(INT,nstr) FROM CharTable(@Office, ',')))) --exsists as a parameter for the location
AND ((@Doctor IS NULL AND (u.Id IS NULL OR u.Id IN (SELECT Id FROM model.Users))) OR
	(@Doctor IS NOT NULL AND u.Id IN (SELECT CONVERT(INT,nstr) FROM CharTable(@Doctor, ',')))) --exsists as a parameter for the doctor
AND ((@Resource IS NULL AND (dbore.ResourceId IS NULL OR dbore.ResourceId IN (SELECT Id FROM model.Users))) OR 
	(@Resource IS NOT NULL AND (dbore.ResourceId IN (SELECT CONVERT(INT,nstr) FROM CharTable(@Resource, ',')))))
END
END
GO




/*
TO TEST THIS STORED PROC:


DECLARE @StartDate DATETIME = (SELECT GETDATE()) --start date is today's date
DECLARE @EndDate DATETIME = (SELECT GETDATE()+30) --end date is 30 days from now

DECLARE @Doctor VARCHAR(MAX)												--all doctors
SELECT @Doctor = COALESCE(@Doctor + ', ', '') + CAST(Id AS VARCHAR(MAX))
FROM model.Users
WHERE __EntityType__ = 'Doctor'

DECLARE @Office VARCHAR(MAX)												--all offices
SELECT @Office = COALESCE(@Office + ', ', '') + CAST(Id AS VARCHAR(MAX))
FROM model.BillingOrganizations 
WHERE __EntityType__ IN ('BillingOrganization')

DECLARE @Resource VARCHAR(MAX)												--all operators
SELECT @Resource = COALESCE(@Resource + ', ', '') + CAST(Id AS VARCHAR(MAX))
FROM model.Users
WHERE __EntityType__ = 'User'

DECLARE @DobOrAge INT = 1 --get age instead of DoB. (set to 2 for the latter)

EXEC model.GetOrphanedAppointments	@StartDate,@EndDate, @Doctor,@Office,@Resource,@DobOrAge

*/
