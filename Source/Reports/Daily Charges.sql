IF OBJECT_ID('model.GetDailyCharges') IS NOT NULL
	DROP PROCEDURE model.GetDailyCharges
GO

CREATE PROCEDURE model.GetDailyCharges (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@BillingOrganizations NVARCHAR(MAX)
	,@Providers NVARCHAR(MAX)
	,@ScheduledProviders NVARCHAR(MAX)
	,@Locations NVARCHAR(MAX)
	,@BreakdownByBillingOrganization bit
	,@BreakdownByBillingProvider bit
	,@BreakdownByScheduledProvider bit
	,@BreakdownByServiceLocation bit
)
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
		RAISERROR (
				'Invalid date range.'
				,16
				,1
				)
ELSE
BEGIN
SET NOCOUNT ON
	SELECT DISTINCT CAST(COALESCE(u.id, p.Id) AS bigint) AS Id
		,COALESCE(u.UserName, sl.ShortName) AS Provider
	INTO #Providers
	FROM model.providers p
	LEFT JOIN model.users u ON u.id = p.userid
	LEFT JOIN model.ServiceLocations sl ON sl.id = p.ServiceLocationId
		AND p.userid IS NULL

	-- billed
	SELECT 1 AS Rank, bs.Id AS BillingService, ir.InvoiceId, ir.PatientInsuranceId 
	INTO #PatientInsurancesOrdered
	FROM model.BillingServices bs
	INNER JOIN model.BillingServiceTransactions bst ON bst.BillingServiceId = bs.Id
		AND bst.BillingServiceTransactionStatusId IN (1,2,3,4)
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = bst.InvoiceReceivableId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId

	-- not billed
	SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY bs.Id ORDER BY pi.OrdinalId ASC) AS Rank, bs.Id AS BillingService,
	i.Id AS InvoiceId, pi.Id AS PatientInsuranceId 
	INTO #PatientInsurancesOrdered2
	FROM model.BillingServices bs
	INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.PatientInsurances pi ON pi.InsuredPatientId = e.PatientId
		AND pi.IsDeleted = 0 
		AND (pi.EndDateTime IS NULL OR pi.EndDateTime >= e.StartDateTime)
		AND pi.InsuranceTypeId = e.InsuranceTypeId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId
		AND ip.StartDateTime <= e.StartDateTime
	LEFT JOIN model.BillingServiceTransactions bst ON bst.BillingServiceId = bs.Id
	WHERE (bst.Id IS NULL OR bst.BillingServiceTransactionStatusId NOT IN (1,2,3,4))
	AND bs.id NOT IN (SELECT BillingService FROM #PatientInsurancesOrdered)

	SELECT Rank, BillingService, InvoiceId, PatientInsuranceId
	INTO #PatientInsurances
	FROM #PatientInsurancesOrdered

	UNION ALL 

	SELECT Rank, BillingService, InvoiceId, PatientInsuranceId
	FROM #PatientInsurancesOrdered2


	SELECT BillingServiceId, CASE WHEN bsm.OrdinalId = 1 THEN Code END AS CodePart1
		,CASE WHEN bsm.OrdinalId = 2 THEN Code END AS CodePart2
		,CASE WHEN bsm.OrdinalId = 3 THEN Code END AS CodePart3
		,CASE WHEN bsm.OrdinalId = 4 THEN Code END AS CodePart4
	INTO #ServiceModifiers
	FROM model.BillingServiceModifiers bsm
	INNER JOIN model.ServiceModifiers sm ON bsm.ServiceModifierId = sm.Id
	ORDER BY bsm.BillingServiceId

	SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY BillingServiceId ORDER BY BillingServiceId, LEN(CODE) DESC) AS RANK, 
	 BillingServiceId,Code
	INTO #ServiceModifiersWithCodes
	FROM (
		SELECT DISTINCT
		sm1.BillingServiceId,  COALESCE(sm1.CodePart1,'') + 
		COALESCE(' ' + sm2.CodePart2,'') + 
		COALESCE(' ' + sm3.CodePart3,'') +
		COALESCE(' ' + sm4.CodePart4,'') AS Code
		FROM #ServiceModifiers sm1
		INNER JOIN #ServiceModifiers sm2 ON sm2.BillingServiceId = sm1.BillingServiceId
		INNER JOIN #ServiceModifiers sm3 ON sm3.BillingServiceId = sm2.BillingServiceId
		INNER JOIN #ServiceModifiers sm4 ON sm4.BillingServiceId = sm3.BillingServiceId
	) v
	
		SELECT DISTINCT
			CASE WHEN @BreakdownByBillingOrganization = 0 THEN NULL ELSE bo.ShortName END AS BillingOrganizationName,
			CASE WHEN @BreakdownByBillingProvider = 0 THEN NULL ELSE pr.Provider END AS Provider,
			CASE WHEN @BreakdownByScheduledProvider = 0 THEN NULL ELSE COALESCE(usApp.UserName,'') END AS ScheduledProvider,
			CASE WHEN @BreakdownByServiceLocation = 0 THEN NULL ELSE sl.ShortName END AS ServiceLocation,
			e.StartDateTime AS Datetime
			,CONVERT(NVARCHAR(10),e.StartDateTime, 101) AS DATE
			,p.Id AS PatientID,p.LastName,p.FirstName
			,inv.Id AS Invoice
			,ens.Code
			,ens.Description
			,bs.Unit AS Unit
			,CAST(Unit * UnitCharge AS DECIMAL(10,2)) AS TotalCharge
			,COALESCE(insur.Name, '') AS InsurerName
			,bs.Id AS BillingService
			,COALESCE(sm1.Code,'') AS Modifier
			,COALESCE(icd10.DiagnosisCode,esem.ExternalSystemEntityKey,'') AS Icd9DiagnosisCode
		FROM model.Billingservices bs
		INNER JOIN model.Invoices inv ON inv.Id = bs.InvoiceId
		INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = inv.Id
		INNER JOIN model.EncounterServices ens ON ens.Id = bs.EncounterServiceId
		INNER JOIN model.Encounters e ON e.Id = inv.EncounterId
		INNER JOIN model.Patients p ON p.Id = e.PatientId
		INNER JOIN model.ServiceLocations sl ON sl.Id = inv.AttributeToServiceLocationId
		LEFT JOIN model.BillingServiceBillingDiagnosis bsbd ON bsbd.BillingServiceId = bs.Id
		LEFT JOIN  #ServiceModifiersWithCodes sm1 ON bs.Id = sm1.BillingServiceId
			AND sm1.Rank=1
		LEFT JOIN model.BillingDiagnosis bd ON bd.InvoiceId = inv.Id
			AND bd.OrdinalId = 1
		LEFT JOIN model.ExternalSystemEntityMappings esem ON esem.Id = bd.ExternalSystemEntityMappingId
		LEFT JOIN #PatientInsurances pio ON bs.Id = pio.BillingService
			AND pio.Rank = 1
		LEFT JOIN model.PatientInsurances pins ON pins.Id = pio.PatientInsuranceId
		LEFT JOIN model.InsurancePolicies inspol ON pins.InsurancePolicyId = inspol.Id
		LEFT JOIN model.Insurers insur ON insur.Id = inspol.InsurerId
		LEFT JOIN model.BillingServiceModifiers bsm ON bsm.BillingServiceId = bs.Id
		LEFT JOIN model.ServiceModifiers sm ON sm.Id = bsm.ServiceModifierId
		LEFT JOIN model.Providers prov ON inv.BillingProviderId = prov.id
		LEFT JOIN #Providers pr ON (pr.Id = prov.UserId AND prov.UserId IS NOT NULL)
			OR (pr.Id = prov.Id AND prov.UserId IS NULL)
		LEFT JOIN model.Appointments ap ON ap.EncounterId = e.Id
		LEFT JOIN model.Users usApp ON usApp.Id = ap.UserId
		LEFT JOIN model.BillingOrganizations bo ON bo.Id = prov.BillingOrganizationId
		LEFT JOIN model.BillingDiagnosisIcd10 icd10 on icd10.invoiceid=inv.id and icd10.OrdinalId = 1
		WHERE 
			(CONVERT(DATETIME,CONVERT(NVARCHAR(12),e.StartDateTime,101),101) BETWEEN @StartDate AND @EndDate)		
			AND ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations)))
				OR (@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,','))))
			AND ((@Providers IS NULL AND (pr.Id IS NULL OR pr.Id IN (SELECT Id FROM #Providers)))
				OR (@Providers IS NOT NULL AND (pr.Id IN (SELECT CONVERT(bigint,nstr) FROM CharTable(@Providers,',')))))
			AND ((@ScheduledProviders IS NULL AND (usApp.Id IS NULL OR usApp.Id IN (SELECT Id FROM model.Users)))
				OR (@ScheduledProviders IS NOT NULL AND usApp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ScheduledProviders,','))))
			AND ((@Locations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations)))
				OR (@Locations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Locations,','))))
SET NOCOUNT OFF
END

RETURN
END
GO

/*
exec model.GetDailyCharges '01/05/2014'
							,'06/05/2014'
							,NULL
							,NULL
							,NULL
							,NULL,1,1,1,1
*/
