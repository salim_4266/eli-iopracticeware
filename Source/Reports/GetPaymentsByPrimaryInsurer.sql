IF OBJECT_ID('model.GetPaymentsByPrimaryInsurers') IS NOT NULL
	DROP PROCEDURE model.GetPaymentsByPrimaryInsurers

GO

CREATE PROCEDURE model.GetPaymentsByPrimaryInsurers(
		@StartDate datetime
		,@EndDate datetime
		,@BillingOrganizations nvarchar(max) = NULL
		,@AttributeToServiceLocations nvarchar(max) = NULL
		,@ServiceLocations nvarchar(MAX) = NULL
		,@Providers nvarchar(MAX) = NULL
		,@Insurers nvarchar(MAX) = NULL
		,@ServiceCategories nvarchar(MAX) = NULL
		,@ShowBillingOrganizationBreakdown bit
		,@ShowAttributeToServiceLocationBreakdown bit
		,@ShowServiceLocationBreakdown bit
		,@ShowProviderBreakdown bit
		,@ShowInsurerBreakdown bit
		,@ShowServiceCategoryBreakdown bit
		)

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @StartDate > @EndDate
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY i.Id, pi.InsuranceTypeId ORDER BY pi.OrdinalId, ir.Id ASC) AS Rank, 
	i.Id AS InvoiceId, CAST(ir.InvoiceId AS NVARCHAR(20)) + '-' + CAST(ir.PatientInsuranceId AS NVARCHAR(20)) AS InvoiceInsurance,ip.InsurerId
	,ip.PolicyCode
	INTO #PatientInsurancesOrdered
	FROM model.Invoices i 
	INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id 
	INNER JOIN model.Encounters e ON e.Id = i.EncounterId
	INNER JOIN model.PatientInsurances pi ON pi.Id = ir.PatientInsuranceId AND pi.IsDeleted = 0 AND e.InsuranceTypeId = pi.InsuranceTypeId
	INNER JOIN model.InsurancePolicies ip ON ip.Id = pi.InsurancePolicyId AND ip.StartDateTime <= e.StartdateTime
	WHERE  (pi.EndDateTime IS NULL 
	OR pi.EndDateTime = '' 
	OR pi.EndDateTime >= e.StartDateTime)

	CREATE INDEX PatientInsurancesOrdered_InvoiceInsurance ON #PatientInsurancesOrdered(InvoiceInsurance)

-- primary insurance
SELECT BillingService, InsurerId, SUM(PrimaryPayments) AS PrimaryPayments, SUM(PrimaryAdjustments) AS PrimaryAdjustments
INTO #PrimaryStuff
FROM (
	SELECT DISTINCT adj.Id AS AdjustmentId, adj.BillingServiceId AS BillingService, fb.InsurerId, pio.InvoiceInsurance AS PatientInsuranceId,
		CASE 
			WHEN adjt.IsCash = 1
				THEN CASE WHEN adjt.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END  
				ELSE 0
		END AS PrimaryPayments,
		CASE 
			WHEN adjt.IsCash = 0
				THEN CASE WHEN adjt.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END  
				ELSE 0
		END AS PrimaryAdjustments
	FROM model.Adjustments adj
	INNER JOIN model.AdjustmentTypes adjt ON adjt.Id = adj.AdjustmentTypeId
	INNER JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	INNER JOIN #PatientInsurancesOrdered pio on pio.InvoiceInsurance = CAST(ir.InvoiceId AS NVARCHAR(20)) + '-' + CAST(ir.PatientInsuranceId AS NVARCHAR(20))
				AND pio.Rank = 1 
				AND fb.InsurerId = pio.InsurerId
	WHERE adj.FinancialSourceTypeId = 1 AND
		CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) BETWEEN CONVERT(datetime, CONVERT(nvarchar(10), @StartDate, 101)) AND CONVERT(datetime, CONVERT(nvarchar(10), @EndDate, 101))
) v
GROUP BY v.BillingService, v.InsurerId

-- other 
SELECT v.BillingService, SUM(OtherPayments) AS OtherPayments, SUM(OtherAdjustments) AS OtherAdjustments
INTO #OtherStuff 
FROM (
	SELECT DISTINCT adj.Id AS AdjustmentId, adj.BillingServiceId AS BillingService, fb.InsurerId, pio.InvoiceInsurance AS PatientInsuranceId,
		CASE 
			WHEN adjt.IsCash = 1
				THEN CASE WHEN adjt.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END  
				ELSE 0
		END AS OtherPayments,
		CASE 
			WHEN adjt.IsCash = 0
				THEN CASE WHEN adjt.IsDebit = 1 THEN -adj.Amount ELSE adj.Amount END  
				ELSE 0
		END AS OtherAdjustments
	FROM model.Adjustments adj
	INNER JOIN model.AdjustmentTypes adjt ON adjt.Id = adj.AdjustmentTypeId
	INNER JOIN model.FinancialBatches fb ON fb.Id = adj.FinancialBatchId
	INNER JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
	LEFT JOIN #PatientInsurancesOrdered pio on pio.InvoiceInsurance = CAST(ir.InvoiceId AS NVARCHAR(20)) + '-' + CAST(ir.PatientInsuranceId AS NVARCHAR(20))
	WHERE (adj.FinancialSourceTypeId <> 1 OR  pio.Rank <> 1) AND
	CONVERT(datetime, CONVERT(nvarchar(10),adj.PostedDateTime, 101)) BETWEEN CONVERT(datetime, CONVERT(nvarchar(10), @StartDate, 101)) AND CONVERT(datetime, CONVERT(nvarchar(10), @EndDate, 101))
) v
GROUP BY v.BillingService

SELECT BillingOrganization, AttributedServiceLocation, ServiceLocation,Provider,Insurer,ServiceCategory,Service,
	COALESCE(SUM(Quantity),0) AS Quantity,
	COALESCE(SUM(Charges),0) AS Charges,
	SUM(PrimaryPayments) AS PrimaryPayments,
	SUM(OtherPayments) AS OtherPayments,
	SUM(TotalPayments) AS TotalPayments,
	SUM(PrimaryAdjustments) AS PrimaryAdjustments,
	SUM(OtherAdjustments) AS OtherAdjustments,
	SUM(TotalAdjustments) AS TotalAdjustments
FROM (
	SELECT DISTINCT
		CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN bo.ShortName ELSE NULL END AS BillingOrganization,
		CASE WHEN @ShowAttributeToServiceLocationBreakdown = 1 THEN attsl.ShortName ELSE NULL END AS AttributedServiceLocation,
		CASE WHEN @ShowServiceLocationBreakdown = 1 THEN COALESCE(sl.ShortName,'Manual Claim') ELSE NULL END AS ServiceLocation,
		CASE WHEN @ShowProviderBreakdown = 1 THEN doc.UserName ELSE NULL END AS Provider,
		CASE WHEN @ShowInsurerBreakdown = 1 THEN ins.Name ELSE NULL END AS Insurer,
		CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN COALESCE(est.Name,'No Service Category') ELSE NULL END AS ServiceCategory,
		COALESCE(ens.Code + ' - ' + ens.Description,'No Service Code') AS Service,
		bs.Id,
		bs.Unit AS Quantity,
		CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS Charges,
		COALESCE(ps.PrimaryPayments,0) AS PrimaryPayments,
		COALESCE(os.OtherPayments,0) AS OtherPayments,
		COALESCE(ps.PrimaryPayments,0) + COALESCE(os.OtherPayments,0) AS TotalPayments,
		COALESCE(ps.PrimaryAdjustments,0) AS PrimaryAdjustments,
		COALESCE(os.OtherAdjustments,0) AS OtherAdjustments,
		COALESCE(ps.PrimaryAdjustments,0) + COALESCE(os.OtherAdjustments,0) AS TotalAdjustments
	FROM #PrimaryStuff ps
		LEFT JOIN #OtherStuff os ON os.BillingService = ps.BillingService
		LEFT JOIN model.BillingServices bs ON bs.Id = ps.BillingService
		LEFT JOIN model.EncounterServices ens ON bs.EncounterServiceId = ens.Id
		LEFT JOIN model.EncounterServiceTypes est ON ens.EncounterServiceTypeId = est.Id
		LEFT JOIN model.Invoices i ON i.Id = bs.InvoiceId
		LEFT JOIN model.Providers prov ON i.BillingProviderId = prov.Id
		LEFT JOIN model.BillingOrganizations bo ON prov.BillingOrganizationId = bo.Id
		LEFT JOIN model.Users doc ON prov.UserId = doc.Id
		LEFT JOIN model.Encounters e ON e.Id = i.EncounterId
		LEFT JOIN model.ServiceLocations sl ON e.ServiceLocationId = sl.Id
		LEFT JOIN model.ServiceLocations attsl ON i.AttributeToServiceLocationId = attsl.Id
		LEFT JOIN model.Insurers ins ON ins.Id = ps.InsurerId
	WHERE ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations))) OR
			(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))) AND
		((@AttributeToServiceLocations IS NULL AND (attsl.Id IS NULL OR attsl.Id IN (SELECT Id FROM model.ServiceLocations))) OR
			(@AttributeToServiceLocations IS NOT NULL AND attsl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@AttributeToServiceLocations,',')))) AND
		((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR
			(@ServiceLocations IS NOT NULL AND prov.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,',')))) AND
		((@Providers IS NULL AND (doc.Id IS NULL OR doc.Id IN (SELECT UserId FROM model.Providers))) OR
			(@Providers IS NOT NULL AND doc.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Providers,',')))) AND
		((@Insurers IS NULL AND (ins.Id IS NULL OR ins.Id IN (SELECT Id FROM model.Insurers))) OR
			(@Insurers IS NOT NULL AND ins.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@Insurers,',')))) AND
		((@ServiceCategories IS NULL AND (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes))) OR
			(@ServiceCategories IS NOT NULL AND (est.Id IS NULL OR est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,',')))))
) v
GROUP BY BillingOrganization, AttributedServiceLocation, ServiceLocation,Provider,Insurer,ServiceCategory,Service
ORDER BY Service

END
END
GO

/*
EXEC model.GetPaymentsByPrimaryInsurers '11/01/2014','11/20/2014',NULL,NULL,NULL,NULL, NULL,NULL,0,0,0,0,0,0
 */