IF OBJECT_ID('model.GetChargesPaymentsAndAdjustmentsDetail') IS NOT NULL
	DROP PROCEDURE model.GetChargesPaymentsAndAdjustmentsDetail

GO

CREATE PROCEDURE model.GetChargesPaymentsAndAdjustmentsDetail(
		@StartDate datetime
		,@EndDate datetime
		,@AdjStartDate datetime
		,@AdjEndDate datetime
		,@BillingOrganizations nvarchar(max) 
		,@BillingProviders nvarchar(max)
		,@ScheduledProviders nvarchar(MAX) 
		,@AttributeToLocations nvarchar(max)
		,@ServiceLocations nvarchar(MAX) 
		,@ServiceCategories nvarchar(MAX) 
		,@ServiceCodes nvarchar(MAX) 
		,@ShowBillingOrganizationBreakdown bit
		,@ShowMonthlyBreakdown bit
		,@ShowBillingProviderBreakdown bit
		,@ShowScheduledProviderBreakdown bit
		,@ShowAttributeToLocationBreakdown bit
		,@ShowServiceLocationBreakdown bit
		,@ShowServiceCategoryBreakdown bit
		,@ShowServiceCodeBreakdown bit
		,@ShowPatientBreakdown bit
		)

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF (@StartDate > @EndDate) OR (@AdjStartDate > @AdjEndDate)
	RAISERROR ('Invalid date range.',16,1)
ELSE
BEGIN

SET @StartDate = CONVERT(datetime, CONVERT(nvarchar(10),@StartDate,101))
SET @EndDate = DATEADD(ms,-2, DATEADD(dd,1,@EndDate))
SET @AdjStartDate = CONVERT(datetime, CONVERT(nvarchar(10),@AdjStartDate,101))
SET @AdjEndDate = CONVERT(datetime,CONVERT(nvarchar(10),DATEADD(ms,-2, DATEADD(dd,1,@AdjEndDate)),101))

-- sum of adjustments and payments
-- need to be separate from charges because dates are based on different columns
SELECT DISTINCT adj.Id AS AdjustmentId
	,bs.Id AS BillingService
	,i.Id AS Invoice
	,CASE WHEN at.IsDebit = 0 AND at.IsCash = 0 THEN adj.Amount END AS PositiveAdjustments
	,CASE WHEN at.IsDebit = 1 AND at.IsCash = 0 THEN -adj.Amount END AS NegativeAdjustments
	,CASE WHEN adj.FinancialSourceTypeId = 1 AND at.IsCash = 1 THEN 
		CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END END AS InsurerPayment
	,CASE WHEN (adj.FinancialSourceTypeId = 2 OR adj.FinancialSourceTypeId = 3) AND at.IsCash = 1 THEN 
		CASE WHEN at.IsDebit = 0 THEN adj.Amount ELSE - adj.Amount END END AS PatientPayment
	,NULL AS Qty
	,NULL AS Charges
	,es.Id AS EncounterService
	,adj.PostedDateTime AS Date
	,COALESCE(adj.PatientId,e.PatientId) AS PatientId
INTO #CPA
FROM model.Adjustments adj
INNER JOIN model.AdjustmentTypes at ON at.Id = adj.AdjustmentTypeId 
LEFT JOIN model.InvoiceReceivables ir ON ir.Id = adj.InvoiceReceivableId
LEFT JOIN model.Invoices i ON i.Id = ir.InvoiceId
LEFT JOIN model.Encounters e ON e.Id = i.EncounterId
LEFT JOIN model.BillingServices bs ON bs.Id = adj.BillingServiceId
LEFT JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
WHERE (adj.PatientId IS NOT NULL OR adj.BillingServiceId IS NOT NULL OR adj.InvoiceReceivableId IS NOT NULL)
AND (( adj.PostedDateTime BETWEEN @AdjStartDate AND @AdjEndDate)  OR (1 = (	CASE WHEN @AdjStartDate IS NULL OR @AdjEndDate IS NULL THEN 1 ELSE 0  END)))

UNION ALL

SELECT DISTINCT NULL AS AdjustmentId
	,bs.Id AS BillingService
	,i.Id AS Invoice
	,NULL AS PositiveAdjustments
	,NULL AS NegativeAdjustments
	,NULL AS InsurerPayment
	,NULL AS PatientPayment
	,bs.Unit AS Qty
	,CAST(bs.Unit * bs.UnitCharge AS DECIMAL(20,2)) AS Charges
	,es.Id AS EncounterService
	,CONVERT(datetime,CONVERT(nvarchar(10),e.StartDateTime,101)) AS Date
	,e.PatientId AS PatientId
FROM model.BillingServices bs
INNER JOIN model.EncounterServices es ON es.Id = bs.EncounterServiceId
INNER JOIN model.Invoices i ON i.Id = bs.InvoiceId
INNER JOIN model.InvoiceReceivables ir ON ir.InvoiceId = i.Id
INNER JOIN model.Encounters e ON e.Id = i.EncounterId
WHERE (( e.StartDateTime >= @StartDate AND  e.EndDateTime  <= @EndDate)  OR (1 = (CASE WHEN @StartDate IS NULL OR @EndDate IS NULL THEN 1 ELSE 0  END)))

SELECT DISTINCT 
	cpa.AdjustmentId
	,cpa.BillingService
	,cpa.Invoice
	,cpa.Date
	,bo.ShortName AS BillingOrganization
	,CAST(CAST(DATEPART(YEAR,cpa.Date) AS NVARCHAR(4)) + RIGHT('0' + CAST(DATEPART(MONTH,cpa.Date) AS NVARCHAR(2)),2) AS INT) AS MonthYearInt
	,DATENAME(MONTH,cpa.Date) + ' ' + CAST(DATEPART(YEAR,cpa.Date) AS NVARCHAR(4)) AS MonthYear
	,bp.UserName AS BillingProvider
	,COALESCE(sp.UserName,'No scheduled provider') AS ScheduledProvider
	,COALESCE(asl.ShortName,'On Account') AS AttributedServiceLocation
	,COALESCE(sl.ShortName,'On Account') AS ServiceLocation
	,COALESCE(est.Name,'Uncategorized') AS ServiceCategory
	,COALESCE(es.Code + ' - ' + es.[Description],'No billing service') AS ServiceCode
	,p.LastName + ', ' + p.FirstName AS PatientName
	,cpa.PatientId
	,COALESCE(cpa.Qty,0) AS Qty
	,COALESCE(cpa.Charges,0) AS Charges
	,COALESCE(cpa.PositiveAdjustments,0) AS PositiveAdjustments
	,COALESCE(cpa.NegativeAdjustments,0) AS NegativeAdjustments
	,COALESCE(cpa.InsurerPayment,0) AS InsurerPayment
	,COALESCE(cpa.PatientPayment,0) AS PatientPayment
INTO #Results
FROM #CPA cpa
LEFT JOIN model.EncounterServices es ON cpa.EncounterService = es.Id
LEFT JOIN model.Invoices i ON cpa.Invoice = i.Id
LEFT JOIN model.ServiceLocations asl ON asl.Id = i.AttributeToServiceLocationId
LEFT JOIN model.Encounters e ON i.EncounterId = e.Id
LEFT JOIN model.Patients p ON cpa.PatientId = p.Id
LEFT JOIN model.ServiceLocations sl ON e.ServiceLocationId = sl.Id
LEFT JOIN model.Appointments a ON a.EncounterId = e.Id
LEFT JOIN model.Providers pr ON pr.Id = i.BillingProviderId
LEFT JOIN model.Users bp ON bp.Id = pr.UserId
LEFT JOIN model.Users sp ON sp.Id = a.UserId
LEFT JOIN model.BillingOrganizations bo ON bo.Id = p.BillingOrganizationId
LEFT JOIN model.EncounterServiceTypes est ON es.EncounterServiceTypeId = est.Id
WHERE ((cpa.Date BETWEEN @StartDate AND @EndDate) OR (cpa.Date BETWEEN @AdjStartDate AND @AdjEndDate)) AND
	 ((@BillingOrganizations IS NULL AND (bo.Id IS NULL OR bo.Id IN (SELECT Id FROM model.BillingOrganizations))) OR
		(@BillingOrganizations IS NOT NULL AND bo.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingOrganizations,',')))) AND
	((@BillingProviders IS NULL AND (bp.Id IS NULL OR bp.Id IN (SELECT Id FROM model.Users))) OR
		(@BillingProviders IS NOT NULL AND bp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@BillingProviders,',')))) AND
	((@ScheduledProviders IS NULL AND (sp.Id IS NULL OR sp.Id IN (SELECT Id FROM model.Users))) OR
		(@ScheduledProviders IS NOT NULL AND sp.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ScheduledProviders,',')))) AND
	((@AttributeToLocations IS NULL AND (asl.Id IS NULL OR asl.Id IN (SELECT Id FROM model.ServiceLocations))) OR
		(@AttributeToLocations IS NOT NULL AND asl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@AttributeToLocations,',')))) AND
	((@ServiceLocations IS NULL AND (sl.Id IS NULL OR sl.Id IN (SELECT Id FROM model.ServiceLocations))) OR
		(@ServiceLocations IS NOT NULL AND sl.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceLocations,',')))) AND
	((@ServiceCategories IS NULL AND (est.Id IS NULL OR est.Id IN (SELECT Id FROM model.EncounterServiceTypes))) OR
		(@ServiceCategories IS NOT NULL AND est.Id IN (SELECT CONVERT(int,nstr) FROM CharTable(@ServiceCategories,',')))) AND
	((@ServiceCodes IS NULL AND (es.Code IS NULL OR es.Code IN (SELECT Code FROM model.EncounterServices))) OR
		(@ServiceCodes IS NOT NULL AND es.Code IN (SELECT CONVERT(nvarchar,nstr) FROM CharTable(@ServiceCodes,',')))) 

SELECT  
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN r.BillingOrganization ELSE NULL END AS BillingOrganization
	,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYear ELSE NULL END AS MonthYear
	,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYearInt ELSE NULL END AS MonthYearInt
	,CASE WHEN @ShowBillingProviderBreakdown = 1 THEN r.BillingProvider ELSE NULL END AS BillingProvider
	,CASE WHEN @ShowScheduledProviderBreakdown = 1 THEN r.ScheduledProvider ELSE NULL END AS ScheduledProvider
	,CASE WHEN @ShowAttributeToLocationBreakdown = 1 THEN r.AttributedServiceLocation ELSE NULL END AS AttributedServiceLocation
	,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN r.ServiceLocation ELSE NULL END AS ServiceLocation
	,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN r.ServiceCategory ELSE NULL END AS ServiceCategory
	,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.ServiceCode ELSE NULL END AS ServiceCode
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientName ELSE NULL END AS PatientName
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientId ELSE NULL END AS PatientId
	,BillingService
	,SUM(r.Qty) AS Qty
	,SUM(r.Charges) AS Charges
	,SUM(r.PositiveAdjustments) AS PositiveAdjustments
	,SUM(r.NegativeAdjustments) AS NegativeAdjustments
	,SUM(r.PositiveAdjustments) + SUM(r.NegativeAdjustments) AS TotalAdjustments
	,SUM(r.InsurerPayment) AS InsurerPayment
	,SUM(r.PatientPayment) AS PatientPayment
	,SUM(r.InsurerPayment) + SUM(r.PatientPayment) AS TotalPayments
FROM #Results r
GROUP BY 
	CASE WHEN @ShowBillingOrganizationBreakdown = 1 THEN r.BillingOrganization ELSE NULL END
	,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYear ELSE NULL END
	,CASE WHEN @ShowMonthlyBreakdown = 1 THEN r.MonthYearInt ELSE NULL END 
	,CASE WHEN @ShowBillingProviderBreakdown = 1 THEN r.BillingProvider ELSE NULL END
	,CASE WHEN @ShowScheduledProviderBreakdown = 1 THEN r.ScheduledProvider ELSE NULL END
	,CASE WHEN @ShowAttributeToLocationBreakdown = 1 THEN r.AttributedServiceLocation ELSE NULL END 
	,CASE WHEN @ShowServiceLocationBreakdown = 1 THEN r.ServiceLocation ELSE NULL END
	,CASE WHEN @ShowServiceCategoryBreakdown = 1 THEN r.ServiceCategory ELSE NULL END
	,CASE WHEN @ShowServiceCodeBreakdown = 1 THEN r.ServiceCode ELSE NULL END
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientName ELSE NULL END 
	,CASE WHEN @ShowPatientBreakdown = 1 THEN r.PatientId ELSE NULL END 
	,BillingService

DROP TABLE #CPA
DROP TABLE #Results

END
END
GO

-- EXEC model.GetChargesPaymentsAndAdjustmentsDetail '2014-05-29','2014-05-31',NULL,N'27',NULL,NULL,NULL,NULL,NULL,0,0,0,1,0,0,0,1,1
