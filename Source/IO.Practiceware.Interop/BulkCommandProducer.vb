﻿Imports System.Text.RegularExpressions

''' <summary>
''' Produces bulk version of SQL commands that can be paired with a filter/sort applied locally.
''' </summary>
Public Class BulkCommandProducer
    Private ReadOnly _expressions As New List(Of String)

    Public ReadOnly Property Expressions As List(Of String)
        Get
            Return _expressions
        End Get
    End Property

    Public Sub AddFor(objectName As String, filter As String)
        Expressions.Add(String.Format("(?<CommandText>SELECT\s*\*\s*FROM\s*{0}{1})\s*{2}(?<Filter>.*?)($|ORDER BY(?<Sort>.*))", objectName, If(String.IsNullOrWhiteSpace(filter), String.Empty, "\s*WHERE\s*" + filter), If(String.IsNullOrWhiteSpace(filter), "(WHERE )?", "(AND )?")))
    End Sub

    Public Sub Clear()
        Expressions.Clear()
    End Sub

    Public Function ProduceBulkCommand(commandText As String) As BulkCommand
        For Each e In Expressions.Select(Function(i) New Regex(i, RegexOptions.IgnoreCase))
            Dim match = e.Match(commandText)
            If match.Success Then
                Return New BulkCommand(match.Groups("CommandText").Value.Trim(), match.Groups("Filter").Value.Trim(), match.Groups("Sort").Value.Trim())
            End If
        Next

        Return Nothing
    End Function
End Class

Public Class BulkCommand
    Private ReadOnly _commandText As String
    Private ReadOnly _filter As String
    Private ReadOnly _sort As String

    Public ReadOnly Property CommandText As String
        Get
            Return _commandText
        End Get
    End Property

    Public ReadOnly Property Filter As String
        Get
            Return _filter
        End Get
    End Property

    Public ReadOnly Property Sort As String
        Get
            Return _sort
        End Get
    End Property

    Public Sub New(commandText As String, filter As String, sort As String)
        _commandText = commandText
        _filter = filter
        _sort = sort
    End Sub
End Class
