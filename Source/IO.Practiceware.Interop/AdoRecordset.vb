﻿Imports ADODB
Imports System.ComponentModel
Imports System.Collections.Concurrent
Imports System.Text.RegularExpressions
Imports Soaf
Imports Soaf.Data
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Collections.Generic
Imports System.Windows.Threading
Imports System.Transactions
Imports Soaf.ComponentModel

<ComSourceInterfaces(GetType(RecordsetEvents))>
Public Class AdoRecordset
    Inherits CustomDispatch
    Implements ADODB.Recordset, IDisposable

    Private _innerRecordset As ADODB.Recordset
    Private _innerConnection As AdoConnection
    Private _innerSource As System.String
    Private _filter As String
    Private _sort As String
    Private ReadOnly _asyncFetchPendingEvent As New ManualResetEvent(True)
    Private Shared ReadOnly AsyncFetchPendingEvents As New ConcurrentBag(Of ManualResetEvent)
    Private _lockType As LockTypeEnum = LockTypeEnum.adLockUnspecified
    Private _cursorType As CursorTypeEnum
    Private _disposed As Boolean

    Public Sub New()
        _innerConnection = New AdoConnection()
    End Sub

    Private Property InnerRecordset As ADODB.Recordset
        Get
            If _innerRecordset Is Nothing Then
                _innerRecordset = New Recordset()
            End If
            Return _innerRecordset
        End Get
        Set(value As ADODB.Recordset)

            If _innerRecordset IsNot Nothing Then
                ComWrapper.ReleaseComObject(_innerRecordset)
            End If

            _innerRecordset = value
            If _innerRecordset IsNot Nothing Then
                If _filter IsNot Nothing Then
                    _innerRecordset = ApplyFilter(value, _filter)
                End If
                If _sort IsNot Nothing Then _innerRecordset.Sort = _sort
            End If
        End Set
    End Property

    Private Shared Function ApplyFilter(ByVal recordset As Recordset, ByVal filter As String) As Recordset
        If TryApplyFilter(recordset, filter) Then
            Return recordset
        End If

        Dim clauses As New List(Of String)
        Dim inGroup As Boolean = False
        Dim currentClause As String = String.Empty
        For Each c In New Regex(" AND ", RegexOptions.IgnoreCase).Split(filter)
            If c.Trim().StartsWith("(") Then
                currentClause = c.Trim().Substring(1)
                inGroup = True
            ElseIf inGroup Then
                currentClause += " AND " + c.Substring(0, c.Length - 1).Trim()
            Else
                clauses.Add(c)
            End If

            If inGroup AndAlso currentClause.EndsWith(")") Then
                ' remove trailing parenthesis
                currentClause = currentClause.Substring(0, currentClause.Length - 1)
                clauses.Add(currentClause)
                currentClause = String.Empty
                inGroup = False
            End If

        Next

        For Each clause In clauses
            If TypeOf recordset.Filter Is String AndAlso CType(recordset.Filter, String).Length > 0 Then
                recordset = AdoCom.DeserializeRecordset(recordset.SerializeToByteArray())
            End If
            recordset.Filter = clause
        Next

        Return recordset
    End Function

    <DebuggerNonUserCode>
    Private Shared Function TryApplyFilter(ByVal recordset As Recordset, ByVal filter As String) As Boolean
        Try
            recordset.Filter = filter
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub let_ActiveConnection(pvar As System.Object) Implements _Recordset.let_ActiveConnection, Recordset21.let_ActiveConnection, Recordset20.let_ActiveConnection, Recordset15.let_ActiveConnection
        _innerConnection = DirectCast(pvar, AdoConnection)
    End Sub

    Public Sub let_Source(pvSource As System.String) Implements _Recordset.let_Source, Recordset21.let_Source, Recordset20.let_Source, Recordset15.let_Source
        _innerSource = pvSource
    End Sub

    Public Sub AddNew(Optional FieldList As System.Object = Nothing, Optional Values As System.Object = Nothing) Implements _Recordset.AddNew, Recordset21.AddNew, Recordset20.AddNew, Recordset15.AddNew
        If FieldList Is Nothing OrElse Values Is Nothing Then
            InnerRecordset.AddNew()
        Else
            InnerRecordset.AddNew(FieldList, Values)
        End If
    End Sub

    Public Sub CancelUpdate() Implements _Recordset.CancelUpdate, Recordset21.CancelUpdate, Recordset20.CancelUpdate, Recordset15.CancelUpdate
        InnerRecordset.CancelUpdate()
    End Sub

    Public Sub Close() Implements _Recordset.Close, Recordset21.Close, Recordset20.Close, Recordset15.Close
        If _innerRecordset IsNot Nothing Then InnerRecordset.Close()
    End Sub

    Public Sub Delete(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectCurrent) Implements _Recordset.Delete, Recordset21.Delete, Recordset20.Delete, Recordset15.Delete
        InnerRecordset.Delete(AffectRecords)
        If LockType <> LockTypeEnum.adLockBatchOptimistic Then
            UpdateBatch(AffectRecords)
        End If
    End Sub

    Public Function GetRows(Optional Rows As System.Int32 = -1, Optional Start As System.Object = Nothing, Optional Fields As System.Object = Nothing) As System.Object Implements _Recordset.GetRows, Recordset21.GetRows, Recordset20.GetRows, Recordset15.GetRows
        Return InnerRecordset.GetRows(Rows, Start, Fields)
    End Function

    Public Sub Move(NumRecords As System.Int32, Optional Start As System.Object = Nothing) Implements _Recordset.Move, Recordset21.Move, Recordset20.Move, Recordset15.Move
        RaiseEvent WillMove(EventReasonEnum.adRsnMove, EventStatusEnum.adStatusOK, Me)
        If IsNothing(Start) Then
            InnerRecordset.Move(NumRecords)
        Else
            InnerRecordset.Move(NumRecords, Start)
        End If
        RaiseEvent MoveComplete(EventReasonEnum.adRsnMove, Nothing, EventStatusEnum.adStatusOK, Me)

        If EOF Then RaiseEvent EndOfRecordset(False, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Sub MoveNext() Implements _Recordset.MoveNext, Recordset21.MoveNext, Recordset20.MoveNext, Recordset15.MoveNext
        RaiseEvent WillMove(EventReasonEnum.adRsnMoveNext, EventStatusEnum.adStatusOK, Me)
        InnerRecordset.MoveNext()
        RaiseEvent MoveComplete(EventReasonEnum.adRsnMoveNext, Nothing, EventStatusEnum.adStatusOK, Me)

        If EOF Then RaiseEvent EndOfRecordset(False, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Sub MovePrevious() Implements _Recordset.MovePrevious, Recordset21.MovePrevious, Recordset20.MovePrevious, Recordset15.MovePrevious
        RaiseEvent WillMove(EventReasonEnum.adRsnMovePrevious, EventStatusEnum.adStatusOK, Me)
        InnerRecordset.MovePrevious()
        RaiseEvent MoveComplete(EventReasonEnum.adRsnMovePrevious, Nothing, EventStatusEnum.adStatusOK, Me)

        If EOF Then RaiseEvent EndOfRecordset(False, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Sub MoveFirst() Implements _Recordset.MoveFirst, Recordset21.MoveFirst, Recordset20.MoveFirst, Recordset15.MoveFirst
        RaiseEvent WillMove(EventReasonEnum.adRsnMoveFirst, EventStatusEnum.adStatusOK, Me)
        InnerRecordset.MoveFirst()
        RaiseEvent MoveComplete(EventReasonEnum.adRsnMoveFirst, Nothing, EventStatusEnum.adStatusOK, Me)

        If EOF Then RaiseEvent EndOfRecordset(False, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Sub MoveLast() Implements _Recordset.MoveLast, Recordset21.MoveLast, Recordset20.MoveLast, Recordset15.MoveLast
        RaiseEvent WillMove(EventReasonEnum.adRsnMoveLast, EventStatusEnum.adStatusOK, Me)
        InnerRecordset.MoveLast()
        RaiseEvent MoveComplete(EventReasonEnum.adRsnMoveLast, Nothing, EventStatusEnum.adStatusOK, Me)

        If EOF Then RaiseEvent EndOfRecordset(False, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Sub Open(Optional Source As System.Object = Nothing, Optional ActiveConnection As System.Object = Nothing, Optional CursorType As ADODB.CursorTypeEnum = CursorTypeEnum.adOpenUnspecified, Optional LockType As ADODB.LockTypeEnum = LockTypeEnum.adLockUnspecified, Optional Options As System.Int32 = -1) Implements _Recordset.Open, Recordset21.Open, Recordset20.Open, Recordset15.Open
        Try
            _lockType = LockType

            Dim sql As String = String.Empty

            If Not System.String.IsNullOrWhiteSpace(_innerSource) Then
                sql = _innerSource
            End If

            Dim parameters As New List(Of AdoComServiceParameter)
            Dim command = TryCast(Source, AdoCommand)
            Dim commandType As Integer = CInt(CommandTypeEnum.adCmdUnknown)

            If command IsNot Nothing Then
                _innerConnection = DirectCast(command.ActiveConnection, AdoConnection)
                commandType = CInt(command.CommandType)
                command.CommandType = CommandTypeEnum.adCmdText ' Switch to text to get the real command text
                sql = command.CommandText
                For Each parameter As Parameter In DirectCast(command, Command).Parameters
                    parameters.Add(New AdoComServiceParameter With {.DataType = parameter.Type, .Direction = parameter.Direction, .Name = parameter.Name, .Size = parameter.Size, .Value = parameter.Value})
                Next
            End If

            If Source IsNot Nothing AndAlso TypeOf Source Is String Then
                sql = DirectCast(Source, System.String)
            End If

            If ActiveConnection IsNot Nothing AndAlso TypeOf ActiveConnection Is AdoConnection Then
                _innerConnection = DirectCast(ActiveConnection, AdoConnection)
            End If

            Dim connectionString = _innerConnection.ConnectionString

            If ActiveConnection IsNot Nothing AndAlso TypeOf ActiveConnection Is String Then
                connectionString = DirectCast(ActiveConnection, String)
            End If

            Dim serviceCommand As New AdoComServiceOpenRecordsetCommand With {.CommandText = sql, .CommandType = commandType, .ConnectionString = connectionString, .Parameters = parameters}

            If _innerConnection.IsBatchInProgress Then
                _innerConnection.AddToBatch(serviceCommand, Me)
            Else
                Dim openOptions = CType(Options, RecordOpenOptionsEnum)
                Dim isAsync = False
                If _innerConnection.IsAsyncExecuteEnabled OrElse _innerConnection.IsAsyncOpenEnabled OrElse (openOptions <> RecordOpenOptionsEnum.adOpenRecordUnspecified AndAlso openOptions.HasFlag(RecordOpenOptionsEnum.adOpenAsync)) Then
                    isAsync = True
                End If
                ExecuteCommands({tuple.Create(DirectCast(serviceCommand, AdoComServiceCommand), Me)}, isAsync)
            End If


        Catch ex As Exception
            Trace.TraceError(ex.ToString())
            OnFetchComplete(New AdoError(ex.ToString(), Me.GetType().Name))
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Opens this recordset asynchronously.
    ''' </summary>
    Friend Shared Sub ExecuteCommands(commands As IEnumerable(Of Tuple(Of AdoComServiceCommand, AdoRecordset)), isAsyncExecuteEnabled As Boolean)

        Dim commandsList = commands.ToList()

        For Each command In commandsList.ToArray()

            TryProduceBulkCommand(TryCast(command.Item1, AdoComServiceOpenRecordsetCommand), command.Item2)

            If Not command.Item2._asyncFetchPendingEvent.WaitOne(0) Then Throw New InvalidOperationException("There is already an asynchronous operation pending.")

            command.Item2.OnFetchProgress(0, EventStatusEnum.adStatusOK)

            Dim cachedResult As Recordset = Nothing
            Dim cachedData As Byte() = Nothing
            If command.Item2._innerConnection.IsCachingEnabled AndAlso TypeOf command.Item1 Is AdoComServiceOpenRecordsetCommand AndAlso command.Item2._innerConnection.Cache.TryGetValue(DirectCast(command.Item1, AdoComServiceOpenRecordsetCommand), cachedResult, cachedData) Then

                command.Item2.InnerRecordset = If(cachedResult Is Nothing, CreateOpenRecordset(), cachedResult)

                command.Item2.OnFetchComplete()
                commandsList.Remove(command)
            ElseIf isAsyncExecuteEnabled Then
                ' Add so that we can WaitAll for all async operations to finish.
                AsyncFetchPendingEvents.Add(command.Item2._asyncFetchPendingEvent)

                ' Block until complete
                command.Item2._asyncFetchPendingEvent.Reset()
            End If
        Next

        If Not commandsList.Any() Then
            Return
        End If

        Dim transaction As Transaction = Nothing
        If isAsyncExecuteEnabled Then

            ' Make sure we can dispatch events when complete.
            If Not TypeOf SynchronizationContext.Current Is DispatcherSynchronizationContext Then
                SynchronizationContext.SetSynchronizationContext(New DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher))
            End If

            transaction = transaction.Current
            Dim bw As New System.ComponentModel.BackgroundWorker

            AddHandler bw.DoWork, Sub() ExecuteCommands(commandsList, transaction)

            AddHandler bw.RunWorkerCompleted, Sub(sender, e) OnExecuteAsyncCompleted(commandsList, e.Error)

            bw.RunWorkerAsync()
        Else
            ExecuteCommands(commandsList, transaction)
        End If

    End Sub

    Private Shared Sub TryProduceBulkCommand(command As AdoComServiceOpenRecordsetCommand, ByVal recordset As AdoRecordset)
        If command Is Nothing Then Return

        Dim bulkCommand = recordset._innerConnection.BulkCommandProducer.ProduceBulkCommand(command.CommandText)

        If bulkCommand IsNot Nothing Then
            command.CommandText = bulkCommand.CommandText
            recordset._filter = bulkCommand.Filter
            recordset._sort = bulkCommand.Sort
        End If
    End Sub


    Private Sub OnFetchProgress(ByVal progress As Integer, ByVal status As EventStatusEnum)
        RaiseEvent FetchProgress(progress, 100, status, Me)
    End Sub

    Private Sub OnFetchComplete(Optional err As ADODB.Error = Nothing)
        Dim eventStatus = If(err Is Nothing, EventStatusEnum.adStatusOK, EventStatusEnum.adStatusErrorsOccurred)

        If _innerConnection IsNot Nothing Then _innerConnection.OnExecuteComplete(GetRecordCountIfExists(), err, eventStatus, New AdoCommand(), Me)

        RaiseEvent FetchComplete(err, eventStatus, Me)
    End Sub

    Private Function GetRecordCountIfExists() As Integer
        If (State = 1) Then
            Return RecordCount
        End If
        Return 0
    End Function


    Private Shared Sub OnExecuteAsyncCompleted(commands As IEnumerable(Of Tuple(Of AdoComServiceCommand, AdoRecordset)), ex As Exception)
        For Each command In commands

            Try
                Dim e As AdoError = Nothing
                If ex IsNot Nothing Then
                    Trace.TraceError(ex.ToString())
                    e = New AdoError(ex.ToString(), command.Item2.GetType().Name)
                    command.Item2.OnFetchComplete(e)
                End If

                If TypeOf command.Item1 Is AdoComServiceOpenRecordsetCommand Then
                    command.Item2.OnFetchComplete(e)
                End If
            Finally
                command.Item2._asyncFetchPendingEvent.Set()
                If Not AsyncFetchPendingEvents.TryTake(command.Item2._asyncFetchPendingEvent) Then
                    Throw New InvalidOperationException("Could not remove the asynchronous operation.")
                End If
            End Try
        Next

    End Sub

    ''' <summary>
    ''' Gets the binary recordset data via the AdoComService. Uses cache and/or transaction if specified.
    ''' </summary>    
    Friend Shared Sub ExecuteCommands(commands As IEnumerable(Of Tuple(Of AdoComServiceCommand, AdoRecordset)), transaction As Transaction)
        If transaction IsNot Nothing Then
            Using ts As New TransactionScope
                ExecuteCommands(commands)
                ts.Complete()
            End Using
        Else
            ExecuteCommands(commands)
        End If
    End Sub


    ''' <summary>
    ''' Gets the binary recordset data via the AdoComService. Uses cache if specified.
    ''' </summary>    
    Private Shared Sub ExecuteCommands(commands As IEnumerable(Of Tuple(Of AdoComServiceCommand, AdoRecordset)))

        commands = commands.ToArray()

        ' Execute against service
        Dim serviceResults = ComWrapper.ServiceProvider.GetService(Of IAdoComService)().ExecuteCommands(commands.Select(Function(i) i.Item1).ToArray()).ToArray()

        Dim index = 0

        For Each command In commands

            Dim result = serviceResults(index)

            If result IsNot Nothing AndAlso result.Recordset IsNot Nothing Then
                command.Item2.InnerRecordset = result.Recordset
            Else
                command.Item2.InnerRecordset = CreateOpenRecordset()
            End If

            If command.Item2._innerConnection.IsCachingEnabled AndAlso TypeOf command.Item1 Is AdoComServiceOpenRecordsetCommand Then
                command.Item2._innerConnection.Cache.Store(DirectCast(command.Item1, AdoComServiceOpenRecordsetCommand), result)
            End If

            index = index + 1
        Next
    End Sub

    ''' <summary>
    ''' Waits for an asynchronous open operation to complete in this recordset if one is running.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Wait()
        While Not _asyncFetchPendingEvent.WaitOne(1)
            ' Need to wait in a loop to allow Post from background thread back onto UI thread without blocking it
            ProcessDispatcherQueue(Dispatcher.CurrentDispatcher)
        End While
    End Sub

    ''' <summary>
    ''' Waits for any asynchronous open operations to complete across all active recordsets.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub WaitAll()
        WaitAllInternal()
    End Sub

    Friend Shared Sub WaitAllInternal()
        Dim events = AsyncFetchPendingEvents.ToArray()
        If events.Length = 0 Then Return
        For Each e In events
            While Not e.WaitOne(1)
                ' Need to wait in a loop to allow Post from background thread back onto UI thread without blocking it
                ProcessDispatcherQueue(Dispatcher.CurrentDispatcher)
            End While
        Next
    End Sub

    ''' <summary>
    ''' Does any events pending on the dispatcher thread. Do NOT use as part of normal coding. To be used only for extreme cases.
    ''' </summary>
    Private Shared Sub ProcessDispatcherQueue(dispatcher As Dispatcher)
        Dim frame = New DispatcherFrame()
        dispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(AddressOf ExitFrame), frame)
        dispatcher.PushFrame(frame)
    End Sub

    Private Shared Function ExitFrame(f As Object) As Object
        DirectCast(f, DispatcherFrame).[Continue] = False
        Return Nothing
    End Function

    Private Shared Function CreateOpenRecordset() As Recordset
        Dim rs As New Recordset
        rs.Fields.Append("Null", DataTypeEnum.adInteger)
        rs.Open()

        Return rs
    End Function

    Public Sub Requery(Optional Options As System.Int32 = -1) Implements _Recordset.Requery, Recordset21.Requery, Recordset20.Requery, Recordset15.Requery
        InnerRecordset.Requery(Options)
    End Sub

    Public Sub _xResync(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll) Implements _Recordset._xResync, Recordset21._xResync, Recordset20._xResync, Recordset15._xResync
        InnerRecordset._xResync(AffectRecords)
    End Sub

    Public Sub Update(Optional Fields As System.Object = Nothing, Optional Values As System.Object = Nothing) Implements _Recordset.Update, Recordset21.Update, Recordset20.Update, Recordset15.Update
        UpdateBatch()
    End Sub

    Public Function _xClone() As ADODB.Recordset Implements _Recordset._xClone, Recordset21._xClone, Recordset20._xClone, Recordset15._xClone
        Return InnerRecordset._xClone()
    End Function

    Public Sub UpdateBatch(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll) Implements _Recordset.UpdateBatch, Recordset21.UpdateBatch, Recordset20.UpdateBatch, Recordset15.UpdateBatch
        Try
            Dim command = New AdoComServiceUpdateRecordsetCommand With {.ConnectionString = _innerConnection.ConnectionString, .Recordset = New AdoComRecordset(InnerRecordset)}
            If _innerConnection.IsBatchInProgress Then
                _innerConnection.AddToBatch(command, Me)
            ElseIf _innerConnection.IsUpdateBatchInProgress Then
                _innerConnection.AddToUpdateBatch(command, Me)
            Else
                ExecuteCommands({tuple.Create(DirectCast(command, AdoComServiceCommand), Me)}, _innerConnection.IsAsyncExecuteEnabled OrElse _innerConnection.IsAsyncUpdateEnabled)
            End If
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
            Throw
        End Try
    End Sub

    Public Sub CancelBatch(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll) Implements _Recordset.CancelBatch, Recordset21.CancelBatch, Recordset20.CancelBatch, Recordset15.CancelBatch
        InnerRecordset.CancelBatch(AffectRecords)
    End Sub

    Public Function NextRecordset(Optional ByRef RecordsAffected As System.Object = Nothing) As ADODB.Recordset Implements _Recordset.NextRecordset, Recordset21.NextRecordset, Recordset20.NextRecordset, Recordset15.NextRecordset
        Return InnerRecordset.NextRecordset(RecordsAffected)
    End Function

    Public Function Supports(CursorOptions As ADODB.CursorOptionEnum) As System.Boolean Implements _Recordset.Supports, Recordset21.Supports, Recordset20.Supports, Recordset15.Supports
        Return InnerRecordset.Supports(CursorOptions)
    End Function

    Public Sub Find(Criteria As System.String, Optional SkipRecords As System.Int32 = 0, Optional SearchDirection As ADODB.SearchDirectionEnum = SearchDirectionEnum.adSearchForward, Optional Start As System.Object = Nothing) Implements _Recordset.Find, Recordset21.Find, Recordset20.Find, Recordset15.Find
        InnerRecordset.Find(Criteria, SkipRecords, SearchDirection, Start)
    End Sub

    Public Sub Cancel() Implements _Recordset.Cancel, Recordset21.Cancel, Recordset20.Cancel
        InnerRecordset.Cancel()
    End Sub

    Public Sub _xSave(Optional FileName As System.String = "", Optional PersistFormat As ADODB.PersistFormatEnum = PersistFormatEnum.adPersistADTG) Implements _Recordset._xSave, Recordset21._xSave, Recordset20._xSave
        InnerRecordset._xSave(FileName, PersistFormat)
    End Sub

    Public Function GetString(Optional StringFormat As ADODB.StringFormatEnum = StringFormatEnum.adClipString, Optional NumRows As System.Int32 = -1, Optional ColumnDelimeter As System.String = "", Optional RowDelimeter As System.String = "", Optional NullExpr As System.String = "") As System.String Implements _Recordset.GetString, Recordset21.GetString, Recordset20.GetString
        Return InnerRecordset.GetString(StringFormat, NumRows, ColumnDelimeter, RowDelimeter, NullExpr)
    End Function

    Public Function CompareBookmarks(Bookmark1 As System.Object, Bookmark2 As System.Object) As ADODB.CompareEnum Implements _Recordset.CompareBookmarks, Recordset21.CompareBookmarks, Recordset20.CompareBookmarks
        Return InnerRecordset.CompareBookmarks(Bookmark1, Bookmark2)
    End Function

    Public Function Clone(Optional LockType As ADODB.LockTypeEnum = LockTypeEnum.adLockUnspecified) As ADODB.Recordset Implements _Recordset.Clone, Recordset21.Clone, Recordset20.Clone
        Return InnerRecordset.Clone(LockType)
    End Function

    Public Sub Resync(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll, Optional ResyncValues As ADODB.ResyncEnum = ResyncEnum.adResyncAllValues) Implements _Recordset.Resync, Recordset21.Resync, Recordset20.Resync
        InnerRecordset.Resync(AffectRecords, ResyncValues)
    End Sub

    Public Sub Seek(KeyValues As System.Object, Optional SeekOption As ADODB.SeekEnum = SeekEnum.adSeekFirstEQ) Implements _Recordset.Seek, Recordset21.Seek
        InnerRecordset.Seek(KeyValues, SeekOption)
    End Sub

    Public Sub Save(Optional Destination As System.Object = Nothing, Optional PersistFormat As ADODB.PersistFormatEnum = PersistFormatEnum.adPersistADTG) Implements _Recordset.Save
        InnerRecordset.Save(Destination, PersistFormat)
    End Sub

    Public ReadOnly Property Properties() As ADODB.Properties Implements _Recordset.Properties, Recordset21.Properties, Recordset20.Properties, Recordset15.Properties, _ADO.Properties
        Get
            Return InnerRecordset.Properties()
        End Get
    End Property

    Public Property AbsolutePosition() As ADODB.PositionEnum_Param Implements _Recordset.AbsolutePosition, Recordset21.AbsolutePosition, Recordset20.AbsolutePosition, Recordset15.AbsolutePosition
        Get
            Return InnerRecordset.AbsolutePosition()
        End Get
        Set(value As ADODB.PositionEnum_Param)
            InnerRecordset.AbsolutePosition = value
        End Set
    End Property

    Public Property ActiveConnection() As System.Object Implements _Recordset.ActiveConnection, Recordset21.ActiveConnection, Recordset20.ActiveConnection, Recordset15.ActiveConnection
        Get
            Return _innerConnection
        End Get
        Set(value As System.Object)
            _innerConnection = DirectCast(value, AdoConnection)
        End Set
    End Property

    Public ReadOnly Property BOF() As System.Boolean Implements _Recordset.BOF, Recordset21.BOF, Recordset20.BOF, Recordset15.BOF
        Get
            Return InnerRecordset.BOF()
        End Get
    End Property

    Public Property Bookmark() As System.Object Implements _Recordset.Bookmark, Recordset21.Bookmark, Recordset20.Bookmark, Recordset15.Bookmark
        Get
            Return InnerRecordset.Bookmark()
        End Get
        Set(value As System.Object)
            InnerRecordset.Bookmark = value
        End Set
    End Property

    Public Property CacheSize() As System.Int32 Implements _Recordset.CacheSize, Recordset21.CacheSize, Recordset20.CacheSize, Recordset15.CacheSize
        Get
            Return InnerRecordset.CacheSize()
        End Get
        Set(value As System.Int32)
            InnerRecordset.CacheSize = value
        End Set
    End Property

    Public Property CursorType() As ADODB.CursorTypeEnum Implements _Recordset.CursorType, Recordset21.CursorType, Recordset20.CursorType, Recordset15.CursorType
        Get
            Return _cursorType
        End Get
        Set(value As ADODB.CursorTypeEnum)
            _cursorType = value
        End Set
    End Property

    Public ReadOnly Property EOF() As System.Boolean Implements _Recordset.EOF, Recordset21.EOF, Recordset20.EOF, Recordset15.EOF
        Get
            Return _innerRecordset Is Nothing OrElse InnerRecordset.State <> 1 OrElse InnerRecordset.EOF()
        End Get
    End Property

    Public ReadOnly Property Fields() As ADODB.Fields Implements _Recordset.Fields, Recordset21.Fields, Recordset20.Fields, Recordset15.Fields
        Get
            Return InnerRecordset.Fields()
        End Get
    End Property

    <DispId(0)>
    Default Public Property Item(ByVal index As Object) As Object
        Get
            Return InnerRecordset.Fields(index)
        End Get
        Set(value As Object)
            RaiseEvent WillChangeField(If(TypeOf index Is Integer, CInt(index), 0), Fields, EventStatusEnum.adStatusOK, Me)
            InnerRecordset.Fields(index).Value = value
            RaiseEvent FieldChangeComplete(If(TypeOf index Is Integer, CInt(index), 0), Fields, Nothing, EventStatusEnum.adStatusOK, Me)
        End Set
    End Property

    Public Property LockType() As ADODB.LockTypeEnum Implements _Recordset.LockType, Recordset21.LockType, Recordset20.LockType, Recordset15.LockType
        Get
            Return _lockType
        End Get
        Set(value As ADODB.LockTypeEnum)
            _lockType = value
        End Set
    End Property

    Public Property MaxRecords() As System.Int32 Implements _Recordset.MaxRecords, Recordset21.MaxRecords, Recordset20.MaxRecords, Recordset15.MaxRecords
        Get
            Return InnerRecordset.MaxRecords()
        End Get
        Set(value As System.Int32)
            InnerRecordset.MaxRecords = value
        End Set
    End Property

    Public ReadOnly Property RecordCount() As System.Int32 Implements _Recordset.RecordCount, Recordset21.RecordCount, Recordset20.RecordCount, Recordset15.RecordCount
        Get
            Return InnerRecordset.RecordCount()
        End Get
    End Property

    Public Property Source() As System.Object Implements _Recordset.Source, Recordset21.Source, Recordset20.Source, Recordset15.Source
        Get
            Return _innerSource
        End Get
        Set(value As System.Object)
            _innerSource = DirectCast(value, System.String)
        End Set
    End Property

    Public Property AbsolutePage() As ADODB.PositionEnum_Param Implements _Recordset.AbsolutePage, Recordset21.AbsolutePage, Recordset20.AbsolutePage, Recordset15.AbsolutePage
        Get
            Return InnerRecordset.AbsolutePage()
        End Get
        Set(value As ADODB.PositionEnum_Param)
            InnerRecordset.AbsolutePage = value
        End Set
    End Property

    Public ReadOnly Property EditMode() As ADODB.EditModeEnum Implements _Recordset.EditMode, Recordset21.EditMode, Recordset20.EditMode, Recordset15.EditMode
        Get
            Return InnerRecordset.EditMode()
        End Get
    End Property

    Public Property Filter() As System.Object Implements _Recordset.Filter, Recordset21.Filter, Recordset20.Filter, Recordset15.Filter
        Get
            Return InnerRecordset.Filter()
        End Get
        Set(value As System.Object)
            InnerRecordset.Filter = value
        End Set
    End Property

    Public ReadOnly Property PageCount() As System.Int32 Implements _Recordset.PageCount, Recordset21.PageCount, Recordset20.PageCount, Recordset15.PageCount
        Get
            Return InnerRecordset.PageCount()
        End Get
    End Property

    Public Property PageSize() As System.Int32 Implements _Recordset.PageSize, Recordset21.PageSize, Recordset20.PageSize, Recordset15.PageSize
        Get
            Return InnerRecordset.PageSize()
        End Get
        Set(value As System.Int32)
            InnerRecordset.PageSize = value
        End Set
    End Property

    Public Property Sort() As System.String Implements _Recordset.Sort, Recordset21.Sort, Recordset20.Sort, Recordset15.Sort
        Get
            Return InnerRecordset.Sort()
        End Get
        Set(value As System.String)
            InnerRecordset.Sort = value
        End Set
    End Property

    Public ReadOnly Property Status() As System.Int32 Implements _Recordset.Status, Recordset21.Status, Recordset20.Status, Recordset15.Status
        Get
            Return InnerRecordset.Status()
        End Get
    End Property

    Public ReadOnly Property State() As System.Int32 Implements _Recordset.State, Recordset21.State, Recordset20.State, Recordset15.State
        Get
            Return InnerRecordset.State()
        End Get
    End Property

    Public Property CursorLocation() As ADODB.CursorLocationEnum Implements _Recordset.CursorLocation, Recordset21.CursorLocation, Recordset20.CursorLocation, Recordset15.CursorLocation
        Get
            Return InnerRecordset.CursorLocation()
        End Get
        Set(value As ADODB.CursorLocationEnum)
            InnerRecordset.CursorLocation = value
        End Set
    End Property

    Public Property Collect(Index As System.Object) As System.Object Implements _Recordset.Collect, Recordset21.Collect, Recordset20.Collect, Recordset15.Collect
        Get
            Return InnerRecordset.Collect(Index)
        End Get
        Set(value As System.Object)
            InnerRecordset.Collect(Index) = value
        End Set
    End Property

    Public Property MarshalOptions() As ADODB.MarshalOptionsEnum Implements _Recordset.MarshalOptions, Recordset21.MarshalOptions, Recordset20.MarshalOptions, Recordset15.MarshalOptions
        Get
            Return InnerRecordset.MarshalOptions()
        End Get
        Set(value As ADODB.MarshalOptionsEnum)
            InnerRecordset.MarshalOptions = value
        End Set
    End Property

    Public Property DataSource() As System.Object Implements _Recordset.DataSource, Recordset21.DataSource, Recordset20.DataSource
        Get
            Return InnerRecordset.DataSource()
        End Get
        Set(value As System.Object)
            InnerRecordset.DataSource = value
        End Set
    End Property

    Public ReadOnly Property ActiveCommand() As System.Object Implements _Recordset.ActiveCommand, Recordset21.ActiveCommand, Recordset20.ActiveCommand
        Get
            Return InnerRecordset.ActiveCommand()
        End Get
    End Property

    Public Property StayInSync() As System.Boolean Implements _Recordset.StayInSync, Recordset21.StayInSync, Recordset20.StayInSync
        Get
            Return InnerRecordset.StayInSync()
        End Get
        Set(value As System.Boolean)
            InnerRecordset.StayInSync = value
        End Set
    End Property

    Public Property DataMember() As System.String Implements _Recordset.DataMember, Recordset21.DataMember, Recordset20.DataMember
        Get
            Return InnerRecordset.DataMember()
        End Get
        Set(value As System.String)
            InnerRecordset.DataMember = value
        End Set
    End Property

    Public Property Index() As System.String Implements _Recordset.Index, Recordset21.Index
        Get
            Return InnerRecordset.Index()
        End Get
        Set(value As System.String)
            InnerRecordset.Index = value
        End Set
    End Property

    Public Event WillChangeField As ADODB.RecordsetEvents_WillChangeFieldEventHandler Implements RecordsetEvents_Event.WillChangeField
    Public Event FieldChangeComplete As ADODB.RecordsetEvents_FieldChangeCompleteEventHandler Implements RecordsetEvents_Event.FieldChangeComplete
    Public Event WillChangeRecord As ADODB.RecordsetEvents_WillChangeRecordEventHandler Implements RecordsetEvents_Event.WillChangeRecord
    Public Event RecordChangeComplete As ADODB.RecordsetEvents_RecordChangeCompleteEventHandler Implements RecordsetEvents_Event.RecordChangeComplete
    Public Event WillChangeRecordset As ADODB.RecordsetEvents_WillChangeRecordsetEventHandler Implements RecordsetEvents_Event.WillChangeRecordset
    Public Event RecordsetChangeComplete As ADODB.RecordsetEvents_RecordsetChangeCompleteEventHandler Implements RecordsetEvents_Event.RecordsetChangeComplete
    Public Event WillMove As ADODB.RecordsetEvents_WillMoveEventHandler Implements RecordsetEvents_Event.WillMove
    Public Event MoveComplete As ADODB.RecordsetEvents_MoveCompleteEventHandler Implements RecordsetEvents_Event.MoveComplete
    Public Event EndOfRecordset As ADODB.RecordsetEvents_EndOfRecordsetEventHandler Implements RecordsetEvents_Event.EndOfRecordset
    Public Event FetchProgress As ADODB.RecordsetEvents_FetchProgressEventHandler Implements RecordsetEvents_Event.FetchProgress
    Public Event FetchComplete As ADODB.RecordsetEvents_FetchCompleteEventHandler Implements RecordsetEvents_Event.FetchComplete

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not _disposed Then
            If disposing Then
                _asyncFetchPendingEvent.Dispose()
            End If

            Dim toRelease = _innerRecordset
            _innerRecordset = Nothing
            ComWrapper.ReleaseComObject(toRelease)

            _disposed = True
        End If
    End Sub

    Public Sub UpdateIfChanged()
        Dim bookmark = InnerRecordset.Bookmark

        If InnerRecordset.RecordCount = 0 Then
            Return
        End If

        Dim isChanged = False

        InnerRecordset.MoveFirst()

        While Not InnerRecordset.EOF
            For Each f As Field In InnerRecordset.Fields
                If Not Equals(f.OriginalValue, f.Value) Then
                    isChanged = True
                    Exit While
                End If
            Next

            InnerRecordset.MoveNext()
        End While

        InnerRecordset.Bookmark = bookmark

        If isChanged Then
            ' Only update if the data has changed since opening
            Update()
        End If

    End Sub

End Class
