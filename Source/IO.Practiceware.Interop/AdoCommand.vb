﻿Imports ADODB
Imports System.Runtime.InteropServices
Imports Soaf.Data

Public Class AdoCommand
    Implements ADODB.Command, IDisposable

    Private _innerCommand As ADODB.Command
    Private _innerConnection As AdoConnection
    Private _innerParameters As AdoParameters
    Private _disposed As Boolean

    Private ReadOnly Property InnerCommand As ADODB.Command
        Get
            If _innerCommand Is Nothing Then
                _innerCommand = New ADODB.Command()
            End If
            Return _innerCommand
        End Get
    End Property

    Private ReadOnly Property InnerParameters As ADODB.Parameters
        Get
            If _innerParameters Is Nothing Then
                _innerParameters = New AdoParameters(InnerCommand.Parameters)
            End If
            Return _innerParameters
        End Get
    End Property

    Public Sub New()
        _innerConnection = New AdoConnection()
    End Sub

    Public Sub let_ActiveConnection(ppvObject As System.Object) Implements _Command.let_ActiveConnection, Command25.let_ActiveConnection, Command15.let_ActiveConnection
        _innerConnection = DirectCast(ppvObject, AdoConnection)
    End Sub

    Public Function Execute(Optional ByRef RecordsAffected As System.Object = Nothing, Optional ByRef Parameters As System.Object = Nothing, Optional Options As System.Int32 = -1) As ADODB.Recordset Implements _Command.Execute, Command25.Execute, Command15.Execute

        Dim rs As New AdoRecordset With {.ActiveConnection = _innerConnection}
        Try
            _innerConnection.OnWillExecute(Me.GetType().Name, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockBatchOptimistic, Options, EventStatusEnum.adStatusOK, Me, rs)

            Dim executeOptions = CType(Options, ExecuteOptionEnum)
            Dim isAsync As Boolean = False
            If executeOptions <> ExecuteOptionEnum.adOptionUnspecified AndAlso (executeOptions.HasFlag(ExecuteOptionEnum.adAsyncFetch) OrElse executeOptions.HasFlag(ExecuteOptionEnum.adAsyncFetch)) Then
                isAsync = True
            End If

            If isAsync Then
                rs.Open(Me, _innerConnection, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockUnspecified, CInt(RecordOpenOptionsEnum.adOpenAsync))
            Else
                rs.Open(Me, _innerConnection, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockUnspecified, CInt(RecordOpenOptionsEnum.adOpenRecordUnspecified))

                Dim recordCount = 0
                If (rs.State = 1) Then
                    recordCount = rs.RecordCount
                    RecordsAffected = recordCount
                End If
            End If

            Return rs
        Catch ex As Exception
            Trace.TraceError(ex.ToString())

            _innerConnection.OnExecuteComplete(-1, New AdoError(ex.ToString(), Me.GetType().Name), EventStatusEnum.adStatusErrorsOccurred, Me, rs)
            Throw
        End Try
    End Function

    Public Function CreateParameter(Optional Name As System.String = "", Optional Type As ADODB.DataTypeEnum = DataTypeEnum.adEmpty, Optional Direction As ADODB.ParameterDirectionEnum = ParameterDirectionEnum.adParamInput, Optional Size As System.Int32 = 0, Optional Value As System.Object = Nothing) As ADODB.Parameter Implements _Command.CreateParameter, Command25.CreateParameter, Command15.CreateParameter
        Return InnerCommand.CreateParameter(Name, Type, Direction, Size, Value)
    End Function

    Public Sub Cancel() Implements _Command.Cancel, Command25.Cancel
        InnerCommand.Cancel()
    End Sub

    Public ReadOnly Property Properties() As ADODB.Properties Implements _Command.Properties, Command25.Properties, Command15.Properties, _ADO.Properties
        Get
            Return InnerCommand.Properties()
        End Get
    End Property

    Public Property ActiveConnection() As ADODB.Connection Implements _Command.ActiveConnection, Command25.ActiveConnection, Command15.ActiveConnection
        Get
            Return _innerConnection
        End Get
        Set(value As ADODB.Connection)
            _innerConnection = CType(value, AdoConnection)
        End Set
    End Property

    Public Property CommandText() As System.String Implements _Command.CommandText, Command25.CommandText, Command15.CommandText
        Get
            Return InnerCommand.CommandText()
        End Get
        Set(value As System.String)
            InnerCommand.CommandText() = value
        End Set
    End Property

    Public Property CommandTimeout() As System.Int32 Implements _Command.CommandTimeout, Command25.CommandTimeout, Command15.CommandTimeout
        Get
            Return InnerCommand.CommandTimeout()
        End Get
        Set(value As System.Int32)
            InnerCommand.CommandTimeout() = value
        End Set
    End Property

    Public Property Prepared() As System.Boolean Implements _Command.Prepared, Command25.Prepared, Command15.Prepared
        Get
            Return InnerCommand.Prepared()
        End Get
        Set(value As System.Boolean)
            InnerCommand.Prepared() = value
        End Set
    End Property

    Private ReadOnly Property Command_Parameters() As ADODB.Parameters Implements _Command.Parameters, Command25.Parameters, Command15.Parameters
        Get
            Return InnerParameters
        End Get
    End Property

    Public ReadOnly Property Parameters() As Object
        Get
            Return InnerParameters
        End Get
    End Property

    Public Property CommandType() As ADODB.CommandTypeEnum Implements _Command.CommandType, Command25.CommandType, Command15.CommandType
        Get
            Return InnerCommand.CommandType()
        End Get
        Set(value As ADODB.CommandTypeEnum)
            InnerCommand.CommandType() = value
        End Set
    End Property

    Public Property Name() As System.String Implements _Command.Name, Command25.Name, Command15.Name
        Get
            Return InnerCommand.Name()
        End Get
        Set(value As System.String)
            InnerCommand.Name() = value
        End Set
    End Property

    Public ReadOnly Property State() As System.Int32 Implements _Command.State, Command25.State
        Get
            Return InnerCommand.State()
        End Get
    End Property

    Public Property CommandStream() As System.Object Implements _Command.CommandStream
        Get
            Return InnerCommand.CommandStream()
        End Get
        Set(value As System.Object)
            InnerCommand.CommandStream() = value
        End Set
    End Property

    Public Property Dialect() As System.String Implements _Command.Dialect
        Get
            Return InnerCommand.Dialect()
        End Get
        Set(value As System.String)
            InnerCommand.Dialect() = value
        End Set
    End Property

    Public Property NamedParameters() As System.Boolean Implements _Command.NamedParameters
        Get
            Return InnerCommand.NamedParameters()
        End Get
        Set(value As System.Boolean)
            InnerCommand.NamedParameters() = value
        End Set
    End Property

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not _disposed Then
            If disposing Then
                If _innerParameters IsNot Nothing Then
                    _innerParameters.Dispose()
                End If
            End If

            ComWrapper.ReleaseComObject(_innerCommand)
            _innerCommand = Nothing

            _disposed = True
        End If
    End Sub

    Public Sub ConvertToParameterizedSql
        AdoCom.ConvertToParameterizedSql(Me)
    End Sub
End Class
