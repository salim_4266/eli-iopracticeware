﻿
Imports ADODB

Public Class DraftAdoConnection 
	Implements ADODB.Connection
    
	Private _innerConnection As ADODB.Connection
	
	Public Sub New()
		_innerConnection = New ADODB.Connection()

		AddHandler _innerConnection.InfoMessage, Sub(ByVal p0 As ADODB.Error, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Connection) RaiseEvent InfoMessage(p0, p1, p2)
		AddHandler _innerConnection.BeginTransComplete, Sub(ByVal p0 As System.Int32, ByVal p1 As ADODB.Error, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Connection) RaiseEvent BeginTransComplete(p0, p1, p2, p3)
		AddHandler _innerConnection.CommitTransComplete, Sub(ByVal p0 As ADODB.Error, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Connection) RaiseEvent CommitTransComplete(p0, p1, p2)
		AddHandler _innerConnection.RollbackTransComplete, Sub(ByVal p0 As ADODB.Error, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Connection) RaiseEvent RollbackTransComplete(p0, p1, p2)
		AddHandler _innerConnection.WillExecute, Sub(ByRef p0 As System.String, ByRef p1 As ADODB.CursorTypeEnum, ByRef p2 As ADODB.LockTypeEnum, ByRef p3 As System.Int32, ByRef p4 As ADODB.EventStatusEnum, ByVal p5 As ADODB.Command, ByVal p6 As ADODB.Recordset, ByVal p7 As ADODB.Connection) RaiseEvent WillExecute(p0, p1, p2, p3, p4, p5, p6, p7)
		AddHandler _innerConnection.ExecuteComplete, Sub(ByVal p0 As System.Int32, ByVal p1 As ADODB.Error, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Command, ByVal p4 As ADODB.Recordset, ByVal p5 As ADODB.Connection) RaiseEvent ExecuteComplete(p0, p1, p2, p3, p4, p5)
		AddHandler _innerConnection.WillConnect, Sub(ByRef p0 As System.String, ByRef p1 As System.String, ByRef p2 As System.String, ByRef p3 As System.Int32, ByRef p4 As ADODB.EventStatusEnum, ByVal p5 As ADODB.Connection) RaiseEvent WillConnect(p0, p1, p2, p3, p4, p5)
		AddHandler _innerConnection.ConnectComplete, Sub(ByVal p0 As ADODB.Error, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Connection) RaiseEvent ConnectComplete(p0, p1, p2)
		AddHandler _innerConnection.Disconnect, Sub(ByRef p0 As ADODB.EventStatusEnum, ByVal p1 As ADODB.Connection) RaiseEvent Disconnect(p0, p1)		
	End Sub

	Public Sub Close()  Implements _Connection.Close, Connection15.Close
		_innerConnection.Close()
	End Sub

	Public Function Execute(CommandText As System.String, Optional  ByRef RecordsAffected As System.Object = Nothing, Optional Options As System.Int32 = -1)  As ADODB.Recordset Implements _Connection.Execute, Connection15.Execute
		Return _innerConnection.Execute(CommandText, RecordsAffected, Options)
	End Function

	Public Function BeginTrans()  As System.Int32 Implements _Connection.BeginTrans, Connection15.BeginTrans
		Return _innerConnection.BeginTrans()
	End Function

	Public Sub CommitTrans()  Implements _Connection.CommitTrans, Connection15.CommitTrans
		_innerConnection.CommitTrans()
	End Sub

	Public Sub RollbackTrans()  Implements _Connection.RollbackTrans, Connection15.RollbackTrans
		_innerConnection.RollbackTrans()
	End Sub

	Public Sub Open(Optional ConnectionString As System.String = "", Optional UserID As System.String = "", Optional Password As System.String = "", Optional Options As System.Int32 = -1)  Implements _Connection.Open, Connection15.Open
		_innerConnection.Open(ConnectionString, UserID, Password, Options)
	End Sub

	Public Function OpenSchema(Schema As ADODB.SchemaEnum, Optional Restrictions As System.Object = Nothing, Optional SchemaID As System.Object = Nothing)  As ADODB.Recordset Implements _Connection.OpenSchema, Connection15.OpenSchema
		Return _innerConnection.OpenSchema(Schema, Restrictions, SchemaID)
	End Function

	Public Sub Cancel()  Implements _Connection.Cancel
		_innerConnection.Cancel()
	End Sub

	Public ReadOnly Property Properties() As ADODB.Properties Implements _Connection.Properties, Connection15.Properties, _ADO.Properties
		Get
			Return _innerConnection.Properties()
		End Get
    End Property

	Public Property ConnectionString() As System.String Implements _Connection.ConnectionString, Connection15.ConnectionString
		Get
			Return _innerConnection.ConnectionString()
		End Get
		Set(value As System.String)
			_innerConnection.ConnectionString() = value
		End Set
    End Property

	Public Property CommandTimeout() As System.Int32 Implements _Connection.CommandTimeout, Connection15.CommandTimeout
		Get
			Return _innerConnection.CommandTimeout()
		End Get
		Set(value As System.Int32)
			_innerConnection.CommandTimeout() = value
		End Set
    End Property

	Public Property ConnectionTimeout() As System.Int32 Implements _Connection.ConnectionTimeout, Connection15.ConnectionTimeout
		Get
			Return _innerConnection.ConnectionTimeout()
		End Get
		Set(value As System.Int32)
			_innerConnection.ConnectionTimeout() = value
		End Set
    End Property

	Public ReadOnly Property Version() As System.String Implements _Connection.Version, Connection15.Version
		Get
			Return _innerConnection.Version()
		End Get
    End Property

	Public ReadOnly Property Errors() As ADODB.Errors Implements _Connection.Errors, Connection15.Errors
		Get
			Return _innerConnection.Errors()
		End Get
    End Property

	Public Property DefaultDatabase() As System.String Implements _Connection.DefaultDatabase, Connection15.DefaultDatabase
		Get
			Return _innerConnection.DefaultDatabase()
		End Get
		Set(value As System.String)
			_innerConnection.DefaultDatabase() = value
		End Set
    End Property

	Public Property IsolationLevel() As ADODB.IsolationLevelEnum Implements _Connection.IsolationLevel, Connection15.IsolationLevel
		Get
			Return _innerConnection.IsolationLevel()
		End Get
		Set(value As ADODB.IsolationLevelEnum)
			_innerConnection.IsolationLevel() = value
		End Set
    End Property

	Public Property Attributes() As System.Int32 Implements _Connection.Attributes, Connection15.Attributes
		Get
			Return _innerConnection.Attributes()
		End Get
		Set(value As System.Int32)
			_innerConnection.Attributes() = value
		End Set
    End Property

	Public Property CursorLocation() As ADODB.CursorLocationEnum Implements _Connection.CursorLocation, Connection15.CursorLocation
		Get
			Return _innerConnection.CursorLocation()
		End Get
		Set(value As ADODB.CursorLocationEnum)
			_innerConnection.CursorLocation() = value
		End Set
    End Property

	Public Property Mode() As ADODB.ConnectModeEnum Implements _Connection.Mode, Connection15.Mode
		Get
			Return _innerConnection.Mode()
		End Get
		Set(value As ADODB.ConnectModeEnum)
			_innerConnection.Mode() = value
		End Set
    End Property

	Public Property Provider() As System.String Implements _Connection.Provider, Connection15.Provider
		Get
			Return _innerConnection.Provider()
		End Get
		Set(value As System.String)
			_innerConnection.Provider() = value
		End Set
    End Property

	Public ReadOnly Property State() As System.Int32 Implements _Connection.State, Connection15.State
		Get
			Return _innerConnection.State()
		End Get
    End Property

	Public Event InfoMessage As ADODB.ConnectionEvents_InfoMessageEventHandler Implements ConnectionEvents_Event.InfoMessage
	Public Event BeginTransComplete As ADODB.ConnectionEvents_BeginTransCompleteEventHandler Implements ConnectionEvents_Event.BeginTransComplete
	Public Event CommitTransComplete As ADODB.ConnectionEvents_CommitTransCompleteEventHandler Implements ConnectionEvents_Event.CommitTransComplete
	Public Event RollbackTransComplete As ADODB.ConnectionEvents_RollbackTransCompleteEventHandler Implements ConnectionEvents_Event.RollbackTransComplete
	Public Event WillExecute As ADODB.ConnectionEvents_WillExecuteEventHandler Implements ConnectionEvents_Event.WillExecute
	Public Event ExecuteComplete As ADODB.ConnectionEvents_ExecuteCompleteEventHandler Implements ConnectionEvents_Event.ExecuteComplete
	Public Event WillConnect As ADODB.ConnectionEvents_WillConnectEventHandler Implements ConnectionEvents_Event.WillConnect
	Public Event ConnectComplete As ADODB.ConnectionEvents_ConnectCompleteEventHandler Implements ConnectionEvents_Event.ConnectComplete
	Public Event Disconnect As ADODB.ConnectionEvents_DisconnectEventHandler Implements ConnectionEvents_Event.Disconnect
End Class
