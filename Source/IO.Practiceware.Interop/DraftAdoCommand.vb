﻿
Imports ADODB

Public Class DraftAdoCommand
	Implements ADODB.Command
    
	Private _innerCommand As ADODB.Command
	
	Public Sub New()
		_innerCommand = New ADODB.Command()
		
	End Sub

	Public Sub let_ActiveConnection(ppvObject As System.Object)  Implements _Command.let_ActiveConnection, Command25.let_ActiveConnection, Command15.let_ActiveConnection
		_innerCommand.let_ActiveConnection(ppvObject)
	End Sub

	Public Function Execute(Optional  ByRef RecordsAffected As System.Object = Nothing, Optional  ByRef Parameters As System.Object = Nothing, Optional Options As System.Int32 = -1)  As ADODB.Recordset Implements _Command.Execute, Command25.Execute, Command15.Execute
		Return _innerCommand.Execute(RecordsAffected, Parameters, Options)
	End Function

	Public Function CreateParameter(Optional Name As System.String = "", Optional Type As ADODB.DataTypeEnum = DataTypeEnum.adEmpty, Optional Direction As ADODB.ParameterDirectionEnum = ParameterDirectionEnum.adParamInput, Optional Size As System.Int32 = 0, Optional Value As System.Object = Nothing)  As ADODB.Parameter Implements _Command.CreateParameter, Command25.CreateParameter, Command15.CreateParameter
		Return _innerCommand.CreateParameter(Name, Type, Direction, Size, Value)
	End Function

	Public Sub Cancel()  Implements _Command.Cancel, Command25.Cancel
		_innerCommand.Cancel()
	End Sub

	Public ReadOnly Property Properties() As ADODB.Properties Implements _Command.Properties, Command25.Properties, Command15.Properties, _ADO.Properties
		Get
			Return _innerCommand.Properties()
		End Get
    End Property

	Public Property ActiveConnection() As ADODB.Connection Implements _Command.ActiveConnection, Command25.ActiveConnection, Command15.ActiveConnection
		Get
			Return _innerCommand.ActiveConnection()
		End Get
		Set(value As ADODB.Connection)
			_innerCommand.ActiveConnection() = value
		End Set
    End Property

	Public Property CommandText() As System.String Implements _Command.CommandText, Command25.CommandText, Command15.CommandText
		Get
			Return _innerCommand.CommandText()
		End Get
		Set(value As System.String)
			_innerCommand.CommandText() = value
		End Set
    End Property

	Public Property CommandTimeout() As System.Int32 Implements _Command.CommandTimeout, Command25.CommandTimeout, Command15.CommandTimeout
		Get
			Return _innerCommand.CommandTimeout()
		End Get
		Set(value As System.Int32)
			_innerCommand.CommandTimeout() = value
		End Set
    End Property

	Public Property Prepared() As System.Boolean Implements _Command.Prepared, Command25.Prepared, Command15.Prepared
		Get
			Return _innerCommand.Prepared()
		End Get
		Set(value As System.Boolean)
			_innerCommand.Prepared() = value
		End Set
    End Property

	Public ReadOnly Property Parameters() As ADODB.Parameters Implements _Command.Parameters, Command25.Parameters, Command15.Parameters
		Get
			Return _innerCommand.Parameters()
		End Get
    End Property

	Public Property CommandType() As ADODB.CommandTypeEnum Implements _Command.CommandType, Command25.CommandType, Command15.CommandType
		Get
			Return _innerCommand.CommandType()
		End Get
		Set(value As ADODB.CommandTypeEnum)
			_innerCommand.CommandType() = value
		End Set
    End Property

	Public Property Name() As System.String Implements _Command.Name, Command25.Name, Command15.Name
		Get
			Return _innerCommand.Name()
		End Get
		Set(value As System.String)
			_innerCommand.Name() = value
		End Set
    End Property

	Public ReadOnly Property State() As System.Int32 Implements _Command.State, Command25.State
		Get
			Return _innerCommand.State()
		End Get
    End Property

	Public Property CommandStream() As System.Object Implements _Command.CommandStream
		Get
			Return _innerCommand.CommandStream()
		End Get
		Set(value As System.Object)
			_innerCommand.CommandStream() = value
		End Set
    End Property

	Public Property Dialect() As System.String Implements _Command.Dialect
		Get
			Return _innerCommand.Dialect()
		End Get
		Set(value As System.String)
			_innerCommand.Dialect() = value
		End Set
    End Property

	Public Property NamedParameters() As System.Boolean Implements _Command.NamedParameters
		Get
			Return _innerCommand.NamedParameters()
		End Get
		Set(value As System.Boolean)
			_innerCommand.NamedParameters() = value
		End Set
    End Property

End Class
