﻿Imports System.Reflection
Imports System.Windows.Interop
Imports System.Runtime.Serialization
Imports Soaf.Presentation.Interop
Imports IO.Practiceware.Presentation.Common
Imports IO.Practiceware.Presentation
Imports Soaf.Presentation
Imports Soaf.Reflection
Imports Soaf
Imports Soaf.Logging
Imports System.Windows
Imports System.Runtime.InteropServices
Imports IO.Practiceware.Storage

<ComClass(ComWrapper.ClassId, ComWrapper.InterfaceId, ComWrapper.EventsId)>
Public Class ComWrapper

#Region "COM GUIDs"

    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "2afdbb7e-80d8-4c88-be9d-fe94d6beb968"
    Public Const InterfaceId As String = "0a9abeb7-c95e-4377-b67a-2b96d2ea23a3"
    Public Const EventsId As String = "14f3d006-dee7-4589-84e4-9d7ce6cc6532"

#End Region

    Shared Sub New()
        Using New TimedScope(Sub(s) Debug.WriteLine("Initialized ComWrapper in {0}.".FormatWith(s)))
            Try
                ServiceProvider = Bootstrapper.Run(Of IServiceProvider)()
                PresentationBootstrapper.Run()
                Security.PrincipalContext.Current.Scope = Security.PrincipalScope.AppDomain

                Windows.Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown
            Catch ex As Exception
                Trace.TraceError(ex.ToString())
            End Try
        End Using
    End Sub

    Friend Shared ServiceProvider As IServiceProvider

    Private _eventHandlers As Dictionary(Of String, List(Of IEventHandler))
    Private _eventHandlersInternal As Dictionary(Of String, [Delegate])

    Private _instance As Object
    Private _exception As Exception
    Private _rethrowExceptions As Boolean = True

    Public Sub Initialize()

    End Sub

    Public Sub Uninitialize()
        FileManager.Instance.ClearCache()
    End Sub

    ''' <summary>
    ''' Attempts to release a COM object if it is in fact a non-null COM object instance. Errors are logged, but not thrown.
    ''' </summary>
    ''' <param name="instance">The instance.</param>
    Friend Shared Sub ReleaseComObject(instance As Object)
        Try
            If instance IsNot Nothing AndAlso Marshal.IsComObject(instance) Then
                Marshal.FinalReleaseComObject(instance)
            End If
        Catch ex As Exception
            Dim exception = New Exception("Unable to release com object {0}".FormatWith(instance), ex)
            Trace.TraceError(exception.ToString())
        End Try
    End Sub

    Public Property Instance() As Object
        Get
            Return _instance
        End Get
        Private Set(ByVal value As Object)
            _instance = value
            _eventHandlers = New Dictionary(Of String, List(Of IEventHandler))
            _eventHandlersInternal = New Dictionary(Of String, [Delegate])
        End Set
    End Property

    Public Property RethrowExceptions As Boolean
        Get
            Return _rethrowExceptions
        End Get
        Set(value As Boolean)
            _rethrowExceptions = value
        End Set
    End Property

    Public Property Exception As Exception
        Get
            Return _exception
        End Get
        Set(value As Exception)
            _exception = value
        End Set
    End Property

    Private Sub HandleCaughtException(ex As Exception)
        Trace.TraceError(ex.ToString())
        Dim targetInvocationException = TryCast(ex, TargetInvocationException)

        If RethrowExceptions = False Then
            If (targetInvocationException IsNot Nothing) Then
                ex = targetInvocationException.InnerException
            End If

            Exception = ex
            InteractionManager.Current.Alert(Exception.Message)
        Else
            If (targetInvocationException IsNot Nothing) Then
                targetInvocationException.InnerException.Rethrow()
            End If
            ex.Rethrow()
        End If
    End Sub

    Public Function Evaluate(ByVal expression As String) As Object
        Try
            If Instance IsNot Nothing Then
                Return Evaluator.Current.Evaluate(expression, Instance)
            Else
                Return Evaluator.Current.Evaluate(expression)
            End If
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Sub SetInstance(ByVal instance As Object)
        Me.Instance = ObjectReference.UnwrapIfObjectReference(instance)
    End Sub

    Public Function Create(ByVal typeName As String, ByRef args() As Object) As ComWrapper
        Try
            Dim type = System.Type.GetType(typeName).IfNull(Function() Reflector.FindTypeInAppDomain(typeName))
            If (type IsNot Nothing) Then
                If args Is Nothing OrElse args.Length = 0 Then
                    Instance = ServiceProvider.GetService(type)
                Else
                    ObjectReference.UnwrapArrayObjectReferences(args)
                    Instance = Activator.CreateInstance(type, args)
                End If
            End If
            If Instance Is Nothing Then
                Trace.TraceError("Could not create instance of type {0}.".FormatWith(typeName))
            End If
            Return Me
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Function InvokeGet(ByVal memberName As String) As Object
        Try
            Return ObjectReference.WrapIfNotComVisible(_instance.GetType().GetProperty(memberName, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(_instance, Nothing))
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Sub InvokeSet(ByVal memberName As String, ByRef arg As Object)
        Try
            Dim p = _instance.GetType().GetProperty(memberName, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance)

            p.EnsureNotDefault("Could not find property {0} on type {1}.".FormatWith(memberName, IIf(_instance Is Nothing, "null", _instance.GetType().Name)))

            Dim propertyType = p.PropertyType.AsUnderlyingTypeIfNullable()

            If propertyType.IsEnum Then
                arg = [Enum].ToObject(propertyType, arg)
            End If

            If arg IsNot Nothing Then
                arg = Convert.ChangeType(arg, propertyType)
            End If

            arg = ObjectReference.UnwrapIfObjectReference(arg)

            p.SetValue(_instance, arg, Nothing)
        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Public Function InvokeMethod(ByVal memberName As String, ByRef args() As Object) As Object
        Try
            ObjectReference.UnwrapArrayObjectReferences(args)
            Return ObjectReference.WrapIfNotComVisible(_instance.GetType().InvokeMember(memberName, BindingFlags.InvokeMethod, Nothing, _instance, args))
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Sub AddEventHandler(ByVal eventName As String, ByVal eventHandler As IEventHandler)
        Try
            Dim eventHandlerInternal As [Delegate] = Nothing

            Dim eventHandlers As List(Of IEventHandler) = Nothing
            If (Not _eventHandlers.TryGetValue(eventName, eventHandlers)) Then
                eventHandlers = New List(Of IEventHandler)()
                _eventHandlers(eventName) = eventHandlers
            End If
            eventHandlers.Add(eventHandler)

            If (Not _eventHandlersInternal.TryGetValue(eventName, eventHandlerInternal)) Then
                _eventHandlersInternal(eventName) = Reflector.BindToEvent(_instance, Nothing, Me, eventName, AddressOf OnEvent).Value
            End If
        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Public Sub RemoveEventHandler(ByVal eventName As String, ByVal eventHandler As IEventHandler)
        Try
            If (_eventHandlers.ContainsKey(eventName) And _eventHandlers(eventName).Contains(eventHandler)) Then
                _eventHandlers(eventName).Remove(eventHandler)
                If (_eventHandlers(eventName).Count = 0) Then
                    _instance.GetType().GetEvent(eventName).RemoveEventHandler(_instance, _eventHandlersInternal(eventName))
                    _eventHandlersInternal.Remove(eventName)
                End If
            End If
        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Public Sub AddStaticEventHandler(ByVal typeName As String, ByVal eventName As String, ByVal eventHandler As IEventHandler)
        Try
            Dim eventHandlerInternal As [Delegate] = Nothing

            Dim eventHandlers As List(Of IEventHandler) = Nothing
            Dim eventKey = String.Format("{0}_{1}", typeName, eventName)
            If (Not _eventHandlers.TryGetValue(eventKey, eventHandlers)) Then
                eventHandlers = New List(Of IEventHandler)()
                _eventHandlers(eventKey) = eventHandlers
            End If
            eventHandlers.Add(eventHandler)

            If (Not _eventHandlersInternal.TryGetValue(eventKey, eventHandlerInternal)) Then
                _eventHandlersInternal(eventKey) = Reflector.BindToEvent(Nothing, Type.GetType(typeName).IfNull(Function() Reflector.FindTypeInAppDomain(typeName)).EnsureNotDefault("Could not resolve type {0}.".FormatWith(typeName)), Me, eventName, AddressOf OnEvent).Value
            End If
        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Public Sub RemoveStaticEventHandler(ByVal typeName As String, ByVal eventName As String, ByVal eventHandler As IEventHandler)
        Try
            Dim eventKey = String.Format("{0}_{1}", typeName, eventName)
            If (_eventHandlers.ContainsKey(eventKey) And _eventHandlers(eventKey).Contains(eventHandler)) Then
                _eventHandlers(eventKey).Remove(eventHandler)
                If (_eventHandlers(eventKey).Count = 0) Then
                    _instance.GetType().GetEvent(eventName).RemoveEventHandler(Nothing, _eventHandlersInternal(eventKey))
                    _eventHandlersInternal.Remove(eventKey)
                End If
            End If
        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Protected Sub OnEvent(ByVal eventName As String, ByVal sender As Object, ByVal e As Object)
        Dim methodBody As Action
        methodBody = New Action(
            Sub()
                ' Hide .NET windows in reverse order
                Dim modalWindows = Windows.Application.Current.Windows.OfType(Of Windows.Window)().ToArray()
                For Each w In modalWindows.Reverse()
                    Dim wih As New WindowInteropHelper(w)
                    WindowInteropUtility.User32.ShowWindow(wih.Handle, WindowInteropUtility.Enums.WindowShowStyle.Hide)
                Next

                Try
                    For Each eventHandler In _eventHandlers(eventName)
                        Try
                            eventHandler.OnEvent(sender, e)
                        Catch ex As Exception
                            Trace.TraceError(ex.ToString())
                        End Try
                    Next
                Catch ex As Exception
                    Trace.TraceError("No event is registered with name :" & eventName & ". " & ex.ToString())
                End Try

                ' Show back .NET windows
                ' Also, we shouldn't attempt restore Loading Window, since it might have already been closed (it's state is managed by Soaf.Presentation.Window)
                For Each w In modalWindows.Where(Function(wnd) Not (TypeOf wnd Is Soaf.Presentation.Window.LoadingWindow))
                    Try
                        Dim wih As New WindowInteropHelper(w)
                        WindowInteropUtility.User32.ShowWindow(wih.Handle, WindowInteropUtility.Enums.WindowShowStyle.Show)
                    Catch ex As Exception
                        Trace.TraceError(ex.ToString())
                    End Try
                Next
            End Sub)

        ' Execute on main thread
        If (Windows.Application.Current.Dispatcher.CheckAccess()) Then
            methodBody()
        Else
            Windows.Application.Current.Dispatcher.Invoke(methodBody)
        End If
    End Sub

    Public Function InvokeStaticGet(ByVal typeName As String, ByVal memberName As String) As Object
        Try
            Return ObjectReference.WrapIfNotComVisible(Type.GetType(typeName).IfNull(Function() Reflector.FindTypeInAppDomain(typeName)).EnsureNotDefault("Could not resolve type {0}.".FormatWith(typeName)).GetProperty(memberName, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Static).GetValue(Nothing, Nothing))
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Sub InvokeStaticSet(ByVal typeName As String, ByVal memberName As String, ByRef arg As Object)
        Try
            Dim p = Type.GetType(typeName).IfNull(Function() Reflector.FindTypeInAppDomain(typeName)).EnsureNotDefault("Could not resolve type {0}.".FormatWith(typeName)).GetProperty(memberName, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Static)

            p.EnsureNotDefault("Could not find property {0} on type {1}.".FormatWith(memberName, typeName))

            Dim propertyType = p.PropertyType.AsUnderlyingTypeIfNullable()

            If propertyType.IsEnum Then
                arg = [Enum].ToObject(propertyType, arg)
            End If

            If arg IsNot Nothing Then
                arg = Convert.ChangeType(arg, propertyType)
            End If

            arg = ObjectReference.UnwrapIfObjectReference(arg)

            p.SetValue(Nothing, arg, Nothing)

        Catch ex As Exception
            HandleCaughtException(ex)
        End Try
    End Sub

    Public Function InvokeStaticMethod(ByVal typeName As String, ByVal memberName As String, ByRef args() As Object) As Object
        Try
            ObjectReference.UnwrapArrayObjectReferences(args)
            Return ObjectReference.WrapIfNotComVisible(Type.GetType(typeName).IfNull(Function() Reflector.FindTypeInAppDomain(typeName)).EnsureNotDefault("Could not resolve type {0}.".FormatWith(typeName)).InvokeMember(memberName, BindingFlags.InvokeMethod Or BindingFlags.Static Or BindingFlags.Public, Nothing, Nothing, args))
        Catch ex As Exception
            HandleCaughtException(ex)
            Return Nothing
        End Try
    End Function

    Public Sub Load(ByVal assemblyName As String)
        Try
            AppDomain.CurrentDomain.Load(assemblyName)
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
        End Try
        Try
            Assembly.LoadFrom(assemblyName)
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
        End Try
    End Sub

    Public Sub CloseAllActiveForms()
        Try
            ' Close any open windows
            WindowsUtilities.ForceCloseAllAppWindows()
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
        End Try
    End Sub

End Class

Public Class ObjectReference
    Inherits CustomDispatch
    Implements IObjectReference

    Private ReadOnly _instance As Object

    Sub New(ByVal instance As Object)
        _instance = instance
    End Sub

    Public ReadOnly Property Instance As Object
        Get
            Return _instance
        End Get
    End Property

    Public Function GetRealObject(ByVal context As StreamingContext) As Object Implements IObjectReference.GetRealObject
        Return Instance
    End Function

    Protected Overrides Function GetTargetInstance() As Object
        Return If(_instance, MyBase.GetTargetInstance())
    End Function

    Protected Overrides Function IReflect_InvokeMember(name As String, invokeAttr As BindingFlags, binder As System.Reflection.Binder, target As Object, args() As Object, modifiers() As ParameterModifier, culture As Globalization.CultureInfo, namedParameters() As String) As Object
        UnwrapArrayObjectReferences(args)

        target = UnwrapIfObjectReference(target)

        Return MyBase.IReflect_InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters)
    End Function


    ''' <summary>
    ''' If Instance is not ComVisible returns wrapped instance.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Shared Function WrapIfNotComVisible(instance As Object) As Object
        If instance Is Nothing Then
            Return instance
        End If

        If {instance.GetType}.Concat(instance.GetType().GetBaseTypes()).All(AddressOf Marshal.IsTypeVisibleFromCom) Then
            Return instance
        End If

        Return New ObjectReference(instance)
    End Function

    Friend Shared Function UnwrapIfObjectReference(instance As Object) As Object
        Dim reference = TryCast(instance, ObjectReference)
        If reference IsNot Nothing Then
            Return reference.Instance
        End If
        Return instance
    End Function


    Friend Shared Sub UnwrapArrayObjectReferences(ByRef objects As Object())
        If objects Is Nothing Then Return

        For i As Integer = 0 To objects.Length - 1
            Dim o = objects(i)
            If TypeOf o Is ObjectReference Then
                objects(i) = UnwrapIfObjectReference(o)
            End If
        Next
    End Sub

End Class