﻿Imports System.Runtime.InteropServices

<ClassInterface(ClassInterfaceType.None)>
Public Class AdoParameters
    Inherits CustomDispatch
    Implements ADODB.Parameters, IDisposable

    Private _innerParameters As ADODB.Parameters
    Private _disposed As Boolean

    Public Sub New(innerParameters As ADODB.Parameters)
        _innerParameters = innerParameters
    End Sub

    Public ReadOnly Property Count() As System.Int32 Implements ADODB.Parameters.Count, ADODB._Collection.Count, ADODB._DynaCollection.Count
        Get
            Return _innerParameters.Count()
        End Get
    End Property

    <DispId(0)>
    Public Property Item(ByVal Index As Object) As ADODB.Parameter
        Get
            Return _innerParameters(Index)
        End Get
        Set(value As ADODB.Parameter)
            Dim indexInt = CType(Index, Integer)

            If value.Size = 0 AndAlso TypeOf value.Value Is String Then
                value.Size = CStr(value.Value).Length + 1
            End If

            If indexInt >= _innerParameters.Count Then
                _innerParameters.Append(value)
            Else
                Dim newParameters As New List(Of Object)
                For i As Integer = 0 To _innerParameters.Count - 1
                    If i = indexInt Then
                        newParameters.Add(value)
                    Else
                        If value.Name <> _innerParameters(i).Name Then
                            newParameters.Add(_innerParameters(i))
                        End If
                    End If
                Next

                While _innerParameters.Count > 0
                    _innerParameters.Delete(0)
                End While

                For Each parameter In newParameters
                    _innerParameters.Append(parameter)
                Next
            End If
        End Set
    End Property

    Private ReadOnly Property Parameters_Item(ByVal Index As Object) As ADODB.Parameter Implements ADODB.Parameters.Item
        Get
            Return _innerParameters(Index)
        End Get
    End Property

    <DispId(-4)> _
    Public Function GetEnumerator() As <MarshalAs(UnmanagedType.CustomMarshaler, MarshalType:="System.Runtime.InteropServices.CustomMarshalers.EnumeratorToEnumVariantMarshaler, CustomMarshalers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")> System.Collections.IEnumerator Implements ADODB.Parameters.GetEnumerator, IEnumerable.GetEnumerator, ADODB._Collection.GetEnumerator, ADODB._DynaCollection.GetEnumerator
        Return _innerParameters.GetEnumerator()
    End Function

    Public Sub Refresh() Implements ADODB.Parameters.Refresh, ADODB._Collection.Refresh, ADODB._DynaCollection.Refresh
        _innerParameters.Refresh()
    End Sub

    Public Sub Append([Object] As System.Object) Implements ADODB.Parameters.Append, ADODB._DynaCollection.Append
        _innerParameters.Append([Object])
    End Sub

    Public Sub Delete(Index As System.Object) Implements ADODB.Parameters.Delete, ADODB._DynaCollection.Delete
        _innerParameters.Delete(Index)
    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not _disposed Then

            ComWrapper.ReleaseComObject(_innerParameters)
            _innerParameters = Nothing

            _disposed = True

        End If
    End Sub
End Class
