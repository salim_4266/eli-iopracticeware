﻿Imports ADODB
Imports System.Runtime.InteropServices

<ClassInterface(ClassInterfaceType.None)>
Public Class AdoProperties
    Inherits List(Of [Property])
    Implements Properties

    Public Sub Refresh() Implements _Collection.Refresh, Properties.Refresh
    End Sub

    Public Overloads ReadOnly Property Count As Integer Implements Properties.Count, _Collection.Count
        Get
            Return MyBase.Count
        End Get
    End Property

    Public Overloads Function GetEnumerator() As IEnumerator Implements Properties.GetEnumerator, _Collection.GetEnumerator
        Return MyBase.GetEnumerator()
    End Function

    Default Public Overloads ReadOnly Property Item(Index As Object) As [Property] Implements Properties.Item
        Get
            Return MyBase.Item(CInt(Index))
        End Get
    End Property
End Class
