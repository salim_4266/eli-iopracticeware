﻿Imports System.Reflection
Imports System.Runtime.InteropServices

Imports System.Text.RegularExpressions
Imports Soaf.Reflection
Imports Soaf

' Modified version of http://stackoverflow.com/questions/317759/why-is-the-indexer-on-my-net-component-not-always-accessible-from-vbscript
Public Class CustomDispatch
    Implements IReflect

    Private Shared ReadOnly DispIdRegex As Regex = New Regex("\[DISPID=(?<Id>\d+?)\]")

    ''' <summary>
    ''' Provides customizability in what members are exposed/invokable by specifying a different target instance.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function GetTargetInstance() As Object
        Return Me
    End Function
    
    ' Called by CLR to get DISPIDs and names for properties 
    Protected Overridable Function IReflect_GetProperties(bindingAttr As BindingFlags) As PropertyInfo() Implements IReflect.GetProperties
        Return GetTargetInstance().[GetType]().GetProperties(bindingAttr)
    End Function

    ' Called by CLR to get DISPIDs and names for fields 
    Protected Overridable Function IReflect_GetFields(bindingAttr As BindingFlags) As FieldInfo() Implements IReflect.GetFields
        Return GetTargetInstance().[GetType]().GetFields(bindingAttr)
    End Function

    ' Called by CLR to get DISPIDs and names for methods 
    Protected Overridable Function IReflect_GetMethods(bindingAttr As BindingFlags) As MethodInfo() Implements IReflect.GetMethods
        Return GetTargetInstance().[GetType]().GetMethods(bindingAttr)
    End Function

    ' Called by CLR to invoke a member 
    Protected Overridable Function IReflect_InvokeMember(name As String, invokeAttr As BindingFlags, binder As Binder, target As Object, args As Object(), modifiers As ParameterModifier(), _
        culture As Globalization.CultureInfo, namedParameters As String()) As Object Implements IReflect.InvokeMember

        If target Is Me Then
            target = GetTargetInstance()
        End If
        Try
            name = ResolveMemberName(name)
            ' Test if it is an indexed Property 
            If name <> "Item" AndAlso IsPropertyAccess(invokeAttr) AndAlso args.Length > 0 AndAlso GetTargetInstance().[GetType]().GetProperty(name) IsNot Nothing Then
                Dim indexedProperty As Object = GetTargetInstance().[GetType]().InvokeMember(name, BindingFlags.GetProperty, New CustomBinder(If(binder, Type.DefaultBinder)), target, Nothing, modifiers, _
                    culture, namedParameters)
                If indexedProperty IsNot Nothing AndAlso indexedProperty.GetType().GetProperty("Item") IsNot Nothing Then
                    Return indexedProperty.[GetType]().InvokeMember("Item", invokeAttr, binder, indexedProperty, args, modifiers, _
                        culture, namedParameters)
                End If
            End If
            ' default InvokeMember 
            Return GetTargetInstance().[GetType]().InvokeMember(name, invokeAttr, New CustomBinder(If(binder, Type.DefaultBinder)), target, args, modifiers, _
                culture, namedParameters)
        Catch ex As MissingMemberException
            ' Well-known HRESULT returned by IDispatch.Invoke: 
            Const DISP_E_MEMBERNOTFOUND As Integer = CInt(&H80020003)
            Throw New COMException(ex.Message, DISP_E_MEMBERNOTFOUND)
        End Try
    End Function

    Protected Overridable Function ResolveMemberName(ByVal name As String) As String
        Dim match = DispIdRegex.Match(name)
        If match.Success Then
            Dim id = match.Groups("Id").Value.ToInt()
            If id.HasValue Then
                Dim member = Me.GetType().GetMembers().FirstOrDefault(Function(m) m.GetAttributes(Of DispIdAttribute)().Any(Function(a) a.Value = id.Value))
                If member IsNot Nothing Then
                    name = member.Name
                End If
            End If
        End If
        Return name
    End Function

    Private Function IsPropertyAccess(ByVal f As BindingFlags) As Boolean
        Return f.HasFlag(BindingFlags.GetProperty) OrElse f.HasFlag(BindingFlags.SetProperty) OrElse f.HasFlag(BindingFlags.PutDispProperty) OrElse f.HasFlag(BindingFlags.PutRefDispProperty)
    End Function

    Private Function IReflect_GetField(name As String, bindingAttr As BindingFlags) As FieldInfo Implements IReflect.GetField
        Return GetTargetInstance().[GetType]().GetField(name, bindingAttr)
    End Function

    Private Function IReflect_GetMember(name As String, bindingAttr As BindingFlags) As MemberInfo() Implements IReflect.GetMember
        Return GetTargetInstance().[GetType]().GetMember(name, bindingAttr)
    End Function

    Private Function IReflect_GetMembers(bindingAttr As BindingFlags) As MemberInfo() Implements IReflect.GetMembers
        Return GetTargetInstance().[GetType]().GetMembers(bindingAttr)
    End Function

    Private Function IReflect_GetMethod(name As String, bindingAttr As BindingFlags) As MethodInfo Implements IReflect.GetMethod
        Return GetTargetInstance().[GetType]().GetMethod(name, bindingAttr)
    End Function

    Private Function IReflect_GetMethod(name As String, bindingAttr As BindingFlags, binder As Binder, types As Type(), modifiers As ParameterModifier()) As MethodInfo Implements IReflect.GetMethod
        Return GetTargetInstance().[GetType]().GetMethod(name, bindingAttr, binder, types, modifiers)
    End Function

    Private Function IReflect_GetProperty(name As String, bindingAttr As BindingFlags, binder As Binder, returnType As Type, types As Type(), modifiers As ParameterModifier()) As PropertyInfo Implements IReflect.GetProperty
        Return GetTargetInstance().[GetType]().GetProperty(name, bindingAttr, binder, returnType, types, modifiers)
    End Function

    Private Function IReflect_GetProperty(name As String, bindingAttr As BindingFlags) As PropertyInfo Implements IReflect.GetProperty
        Return GetTargetInstance().[GetType]().GetProperty(name, bindingAttr)
    End Function

    Private ReadOnly Property IReflect_UnderlyingSystemType() As Type Implements IReflect.UnderlyingSystemType
        Get
            Return GetTargetInstance().[GetType]().UnderlyingSystemType
        End Get
    End Property
End Class
