﻿Imports System.Runtime.InteropServices
Imports ADODB

<ClassInterface(ClassInterfaceType.None)>
Public Class AdoErrors
    Inherits List(Of AdoError)
    Implements Errors

    Public Overloads Function GetEnumerator() As IEnumerator Implements _Collection.GetEnumerator, Errors.GetEnumerator
        Return MyBase.GetEnumerator()
    End Function

    Public Sub Refresh() Implements Errors.Refresh, _Collection.Refresh
    End Sub

    Public Overloads Sub Clear() Implements Errors.Clear
        MyBase.Clear()
    End Sub

    Public Overloads ReadOnly Property Count() As Integer Implements _Collection.Count, Errors.Count
        Get
            Return MyBase.Count
        End Get
    End Property

    Default Public Overloads ReadOnly Property Item(ByVal Index As Object) As [Error] Implements Errors.Item
        Get
            Return MyBase.Item(CInt(Index))
        End Get
    End Property
End Class


Public Class AdoError
    Implements [Error]

    Private ReadOnly _description As String
    Private ReadOnly _source As String

    Public Sub New(description As String, source As String)
        _description = description
        _source = source
    End Sub

    Public ReadOnly Property Number() As Integer Implements [Error].Number
        Get
            Return - 1
        End Get
    End Property

    Public ReadOnly Property Source() As String Implements [Error].Source
        Get
            Return _source
        End Get
    End Property

    Public ReadOnly Property Description() As String Implements [Error].Description
        Get
            Return _description
        End Get
    End Property

    Public ReadOnly Property HelpFile() As String Implements [Error].HelpFile
        Get
            Return String.Empty
        End Get
    End Property

    Public ReadOnly Property HelpContext() As Integer Implements [Error].HelpContext
        Get
            Return 0
        End Get
    End Property

    Public ReadOnly Property SQLState() As String Implements [Error].SQLState
        Get
            Return DateTime.Now.ToClientTime().ToString()
        End Get
    End Property

    Public ReadOnly Property NativeError() As Integer Implements [Error].NativeError
        Get
            Return 0
        End Get
    End Property
End Class