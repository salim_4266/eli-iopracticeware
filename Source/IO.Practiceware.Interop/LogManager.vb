﻿Imports Soaf
Imports Soaf.Logging

''' <summary>
''' A COM exposed LogManager.
''' </summary>
    <ComClass (LogManager.ClassId, LogManager.InterfaceId, LogManager.EventsId)>
Public Class LogManager

#Region "COM GUIDs"

    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "9d8c1a4f-d415-423c-bb5f-824abfa4668a"
    Public Const InterfaceId As String = "d61f6748-f0a3-49fc-8b39-cc1c8eefd507"
    Public Const EventsId As String = "f7cf8f9f-4286-4222-8e2d-1b6dcf769dfa"

#End Region

    ' A creatable COM class must have a Public Sub New() 
    ' with no parameters, otherwise, the class will not be 
    ' registered in the COM registry and cannot be created 
    ' via CreateObject.
    Public Sub New()
        MyBase.New()
    End Sub


    Public Function GetLogger(ByVal source As String) As Logger
        Return New Logger(ComWrapper.ServiceProvider.GetService(Of ILogManager)().GetLogger(source))
    End Function
End Class


