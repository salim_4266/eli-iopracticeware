﻿Imports Soaf.Logging

Public Class Logger

    Friend Sub New(ByVal netLogger As ILogger)
        _netLogger = netLogger
    End Sub

    Private ReadOnly _netLogger As ILogger

    Public Sub Log(ByVal message As String, ByVal severity As Severity, ByVal source As String, ByVal title As String, ByRef categories() As Object, ByRef properties() As Object)
        Dim propertiesResolved As IEnumerable(Of LogEntryProperty) = Nothing
        If (properties IsNot Nothing) Then
            propertiesResolved = properties.Cast(Of LogEntryProperty)().ToArray()
        End If
        Dim categoriesResolved As IEnumerable(Of String) = Nothing
        If (categories IsNot Nothing) Then
            categoriesResolved = categories.Cast(Of String)().ToArray()
        End If
        _netLogger.Log(message, severity, categoriesResolved, title, source, propertiesResolved)
    End Sub


    Public Function CreateProperty(ByVal name As String, ByVal value As String) As LogEntryProperty
        Return New LogEntryProperty With {.Id = Guid.NewGuid(), .Name = name, .Value = value}
    End Function
End Class
