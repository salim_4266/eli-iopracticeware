﻿
Imports ADODB

Public Class DraftAdoRecordset 
	Implements ADODB.Recordset
    
	Private _innerRecordset As ADODB.Recordset
	
	Public Sub New()
		_innerRecordset = New ADODB.Recordset()

	AddHandler _innerRecordset.WillChangeField, Sub(ByVal p0 As System.Int32, ByVal p1 As System.Object, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Recordset) RaiseEvent WillChangeField(p0, p1, p2, p3)
	AddHandler _innerRecordset.FieldChangeComplete, Sub(ByVal p0 As System.Int32, ByVal p1 As System.Object, ByVal p2 As ADODB.Error, ByRef p3 As ADODB.EventStatusEnum, ByVal p4 As ADODB.Recordset) RaiseEvent FieldChangeComplete(p0, p1, p2, p3, p4)
	AddHandler _innerRecordset.WillChangeRecord, Sub(ByVal p0 As ADODB.EventReasonEnum, ByVal p1 As System.Int32, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Recordset) RaiseEvent WillChangeRecord(p0, p1, p2, p3)
	AddHandler _innerRecordset.RecordChangeComplete, Sub(ByVal p0 As ADODB.EventReasonEnum, ByVal p1 As System.Int32, ByVal p2 As ADODB.Error, ByRef p3 As ADODB.EventStatusEnum, ByVal p4 As ADODB.Recordset) RaiseEvent RecordChangeComplete(p0, p1, p2, p3, p4)
	AddHandler _innerRecordset.WillChangeRecordset, Sub(ByVal p0 As ADODB.EventReasonEnum, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Recordset) RaiseEvent WillChangeRecordset(p0, p1, p2)
	AddHandler _innerRecordset.RecordsetChangeComplete, Sub(ByVal p0 As ADODB.EventReasonEnum, ByVal p1 As ADODB.Error, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Recordset) RaiseEvent RecordsetChangeComplete(p0, p1, p2, p3)
	AddHandler _innerRecordset.WillMove, Sub(ByVal p0 As ADODB.EventReasonEnum, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Recordset) RaiseEvent WillMove(p0, p1, p2)
	AddHandler _innerRecordset.MoveComplete, Sub(ByVal p0 As ADODB.EventReasonEnum, ByVal p1 As ADODB.Error, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Recordset) RaiseEvent MoveComplete(p0, p1, p2, p3)
	AddHandler _innerRecordset.EndOfRecordset, Sub(ByRef p0 As System.Boolean, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Recordset) RaiseEvent EndOfRecordset(p0, p1, p2)
	AddHandler _innerRecordset.FetchProgress, Sub(ByVal p0 As System.Int32, ByVal p1 As System.Int32, ByRef p2 As ADODB.EventStatusEnum, ByVal p3 As ADODB.Recordset) RaiseEvent FetchProgress(p0, p1, p2, p3)
	AddHandler _innerRecordset.FetchComplete, Sub(ByVal p0 As ADODB.Error, ByRef p1 As ADODB.EventStatusEnum, ByVal p2 As ADODB.Recordset) RaiseEvent FetchComplete(p0, p1, p2)		
	End Sub

	Public Sub let_ActiveConnection(pvar As System.Object)  Implements _Recordset.let_ActiveConnection, Recordset21.let_ActiveConnection, Recordset20.let_ActiveConnection, Recordset15.let_ActiveConnection
		_innerRecordset.let_ActiveConnection(pvar)
	End Sub

	Public Sub let_Source(pvSource As System.String)  Implements _Recordset.let_Source, Recordset21.let_Source, Recordset20.let_Source, Recordset15.let_Source
		_innerRecordset.let_Source(pvSource)
	End Sub

	Public Sub AddNew(Optional FieldList As System.Object = Nothing, Optional Values As System.Object = Nothing)  Implements _Recordset.AddNew, Recordset21.AddNew, Recordset20.AddNew, Recordset15.AddNew
		_innerRecordset.AddNew(FieldList, Values)
	End Sub

	Public Sub CancelUpdate()  Implements _Recordset.CancelUpdate, Recordset21.CancelUpdate, Recordset20.CancelUpdate, Recordset15.CancelUpdate
		_innerRecordset.CancelUpdate()
	End Sub

	Public Sub Close()  Implements _Recordset.Close, Recordset21.Close, Recordset20.Close, Recordset15.Close
		_innerRecordset.Close()
	End Sub

	Public Sub Delete(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectCurrent)  Implements _Recordset.Delete, Recordset21.Delete, Recordset20.Delete, Recordset15.Delete
		_innerRecordset.Delete(AffectRecords)
	End Sub

	Public Function GetRows(Optional Rows As System.Int32 = -1, Optional Start As System.Object = Nothing, Optional Fields As System.Object = Nothing)  As System.Object Implements _Recordset.GetRows, Recordset21.GetRows, Recordset20.GetRows, Recordset15.GetRows
		Return _innerRecordset.GetRows(Rows, Start, Fields)
	End Function

	Public Sub Move(NumRecords As System.Int32, Optional Start As System.Object = Nothing)  Implements _Recordset.Move, Recordset21.Move, Recordset20.Move, Recordset15.Move
		_innerRecordset.Move(NumRecords, Start)
	End Sub

	Public Sub MoveNext()  Implements _Recordset.MoveNext, Recordset21.MoveNext, Recordset20.MoveNext, Recordset15.MoveNext
		_innerRecordset.MoveNext()
	End Sub

	Public Sub MovePrevious()  Implements _Recordset.MovePrevious, Recordset21.MovePrevious, Recordset20.MovePrevious, Recordset15.MovePrevious
		_innerRecordset.MovePrevious()
	End Sub

	Public Sub MoveFirst()  Implements _Recordset.MoveFirst, Recordset21.MoveFirst, Recordset20.MoveFirst, Recordset15.MoveFirst
		_innerRecordset.MoveFirst()
	End Sub

	Public Sub MoveLast()  Implements _Recordset.MoveLast, Recordset21.MoveLast, Recordset20.MoveLast, Recordset15.MoveLast
		_innerRecordset.MoveLast()
	End Sub

	Public Sub Open(Optional Source As System.Object = Nothing, Optional ActiveConnection As System.Object = Nothing, Optional CursorType As ADODB.CursorTypeEnum = CursorTypeEnum.adOpenUnspecified, Optional LockType As ADODB.LockTypeEnum = LockTypeEnum.adLockUnspecified, Optional Options As System.Int32 = -1)  Implements _Recordset.Open, Recordset21.Open, Recordset20.Open, Recordset15.Open
		_innerRecordset.Open(Source, ActiveConnection, CursorType, LockType, Options)
	End Sub

	Public Sub Requery(Optional Options As System.Int32 = -1)  Implements _Recordset.Requery, Recordset21.Requery, Recordset20.Requery, Recordset15.Requery
		_innerRecordset.Requery(Options)
	End Sub

	Public Sub _xResync(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll)  Implements _Recordset._xResync, Recordset21._xResync, Recordset20._xResync, Recordset15._xResync
		_innerRecordset._xResync(AffectRecords)
	End Sub

	Public Sub Update(Optional Fields As System.Object = Nothing, Optional Values As System.Object = Nothing)  Implements _Recordset.Update, Recordset21.Update, Recordset20.Update, Recordset15.Update
		_innerRecordset.Update(Fields, Values)
	End Sub

	Public Function _xClone()  As ADODB.Recordset Implements _Recordset._xClone, Recordset21._xClone, Recordset20._xClone, Recordset15._xClone
		Return _innerRecordset._xClone()
	End Function

	Public Sub UpdateBatch(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll)  Implements _Recordset.UpdateBatch, Recordset21.UpdateBatch, Recordset20.UpdateBatch, Recordset15.UpdateBatch
		_innerRecordset.UpdateBatch(AffectRecords)
	End Sub

	Public Sub CancelBatch(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll)  Implements _Recordset.CancelBatch, Recordset21.CancelBatch, Recordset20.CancelBatch, Recordset15.CancelBatch
		_innerRecordset.CancelBatch(AffectRecords)
	End Sub

	Public Function NextRecordset(Optional  ByRef RecordsAffected As System.Object = Nothing)  As ADODB.Recordset Implements _Recordset.NextRecordset, Recordset21.NextRecordset, Recordset20.NextRecordset, Recordset15.NextRecordset
		Return _innerRecordset.NextRecordset(RecordsAffected)
	End Function

	Public Function Supports(CursorOptions As ADODB.CursorOptionEnum)  As System.Boolean Implements _Recordset.Supports, Recordset21.Supports, Recordset20.Supports, Recordset15.Supports
		Return _innerRecordset.Supports(CursorOptions)
	End Function

	Public Sub Find(Criteria As System.String, Optional SkipRecords As System.Int32 = 0, Optional SearchDirection As ADODB.SearchDirectionEnum = SearchDirectionEnum.adSearchForward, Optional Start As System.Object = Nothing)  Implements _Recordset.Find, Recordset21.Find, Recordset20.Find, Recordset15.Find
		_innerRecordset.Find(Criteria, SkipRecords, SearchDirection, Start)
	End Sub

	Public Sub Cancel()  Implements _Recordset.Cancel, Recordset21.Cancel, Recordset20.Cancel
		_innerRecordset.Cancel()
	End Sub

	Public Sub _xSave(Optional FileName As System.String = "", Optional PersistFormat As ADODB.PersistFormatEnum = PersistFormatEnum.adPersistADTG)  Implements _Recordset._xSave, Recordset21._xSave, Recordset20._xSave
		_innerRecordset._xSave(FileName, PersistFormat)
	End Sub

	Public Function GetString(Optional StringFormat As ADODB.StringFormatEnum = StringFormatEnum.adClipString, Optional NumRows As System.Int32 = -1, Optional ColumnDelimeter As System.String = "", Optional RowDelimeter As System.String = "", Optional NullExpr As System.String = "")  As System.String Implements _Recordset.GetString, Recordset21.GetString, Recordset20.GetString
		Return _innerRecordset.GetString(StringFormat, NumRows, ColumnDelimeter, RowDelimeter, NullExpr)
	End Function

	Public Function CompareBookmarks(Bookmark1 As System.Object, Bookmark2 As System.Object)  As ADODB.CompareEnum Implements _Recordset.CompareBookmarks, Recordset21.CompareBookmarks, Recordset20.CompareBookmarks
		Return _innerRecordset.CompareBookmarks(Bookmark1, Bookmark2)
	End Function

	Public Function Clone(Optional LockType As ADODB.LockTypeEnum = LockTypeEnum.adLockUnspecified)  As ADODB.Recordset Implements _Recordset.Clone, Recordset21.Clone, Recordset20.Clone
		Return _innerRecordset.Clone(LockType)
	End Function

	Public Sub Resync(Optional AffectRecords As ADODB.AffectEnum = AffectEnum.adAffectAll, Optional ResyncValues As ADODB.ResyncEnum = ResyncEnum.adResyncAllValues)  Implements _Recordset.Resync, Recordset21.Resync, Recordset20.Resync
		_innerRecordset.Resync(AffectRecords, ResyncValues)
	End Sub

	Public Sub Seek(KeyValues As System.Object, Optional SeekOption As ADODB.SeekEnum = SeekEnum.adSeekFirstEQ)  Implements _Recordset.Seek, Recordset21.Seek
		_innerRecordset.Seek(KeyValues, SeekOption)
	End Sub

	Public Sub Save(Optional Destination As System.Object = Nothing, Optional PersistFormat As ADODB.PersistFormatEnum = PersistFormatEnum.adPersistADTG)  Implements _Recordset.Save
		_innerRecordset.Save(Destination, PersistFormat)
	End Sub

	Public ReadOnly Property Properties() As ADODB.Properties Implements _Recordset.Properties, Recordset21.Properties, Recordset20.Properties, Recordset15.Properties, _ADO.Properties
		Get
			Return _innerRecordset.Properties()
		End Get
    End Property

	Public Property AbsolutePosition() As ADODB.PositionEnum_Param Implements _Recordset.AbsolutePosition, Recordset21.AbsolutePosition, Recordset20.AbsolutePosition, Recordset15.AbsolutePosition
		Get
			Return _innerRecordset.AbsolutePosition()
		End Get
		Set(value As ADODB.PositionEnum_Param)
			_innerRecordset.AbsolutePosition() = value
		End Set
    End Property

	Public Property ActiveConnection() As System.Object Implements _Recordset.ActiveConnection, Recordset21.ActiveConnection, Recordset20.ActiveConnection, Recordset15.ActiveConnection
		Get
			Return _innerRecordset.ActiveConnection()
		End Get
		Set(value As System.Object)
			_innerRecordset.ActiveConnection() = value
		End Set
    End Property

	Public ReadOnly Property BOF() As System.Boolean Implements _Recordset.BOF, Recordset21.BOF, Recordset20.BOF, Recordset15.BOF
		Get
			Return _innerRecordset.BOF()
		End Get
    End Property

	Public Property Bookmark() As System.Object Implements _Recordset.Bookmark, Recordset21.Bookmark, Recordset20.Bookmark, Recordset15.Bookmark
		Get
			Return _innerRecordset.Bookmark()
		End Get
		Set(value As System.Object)
			_innerRecordset.Bookmark() = value
		End Set
    End Property

	Public Property CacheSize() As System.Int32 Implements _Recordset.CacheSize, Recordset21.CacheSize, Recordset20.CacheSize, Recordset15.CacheSize
		Get
			Return _innerRecordset.CacheSize()
		End Get
		Set(value As System.Int32)
			_innerRecordset.CacheSize() = value
		End Set
    End Property

	Public Property CursorType() As ADODB.CursorTypeEnum Implements _Recordset.CursorType, Recordset21.CursorType, Recordset20.CursorType, Recordset15.CursorType
		Get
			Return _innerRecordset.CursorType()
		End Get
		Set(value As ADODB.CursorTypeEnum)
			_innerRecordset.CursorType() = value
		End Set
    End Property

	Public ReadOnly Property EOF() As System.Boolean Implements _Recordset.EOF, Recordset21.EOF, Recordset20.EOF, Recordset15.EOF
		Get
			Return _innerRecordset.EOF()
		End Get
    End Property

	Public ReadOnly Property Fields() As ADODB.Fields Implements _Recordset.Fields, Recordset21.Fields, Recordset20.Fields, Recordset15.Fields
		Get
			Return _innerRecordset.Fields()
		End Get
    End Property

	Public Property LockType() As ADODB.LockTypeEnum Implements _Recordset.LockType, Recordset21.LockType, Recordset20.LockType, Recordset15.LockType
		Get
			Return _innerRecordset.LockType()
		End Get
		Set(value As ADODB.LockTypeEnum)
			_innerRecordset.LockType() = value
		End Set
    End Property

	Public Property MaxRecords() As System.Int32 Implements _Recordset.MaxRecords, Recordset21.MaxRecords, Recordset20.MaxRecords, Recordset15.MaxRecords
		Get
			Return _innerRecordset.MaxRecords()
		End Get
		Set(value As System.Int32)
			_innerRecordset.MaxRecords() = value
		End Set
    End Property

	Public ReadOnly Property RecordCount() As System.Int32 Implements _Recordset.RecordCount, Recordset21.RecordCount, Recordset20.RecordCount, Recordset15.RecordCount
		Get
			Return _innerRecordset.RecordCount()
		End Get
    End Property

	Public Property Source() As System.Object Implements _Recordset.Source, Recordset21.Source, Recordset20.Source, Recordset15.Source
		Get
			Return _innerRecordset.Source()
		End Get
		Set(value As System.Object)
			_innerRecordset.Source() = value
		End Set
    End Property

	Public Property AbsolutePage() As ADODB.PositionEnum_Param Implements _Recordset.AbsolutePage, Recordset21.AbsolutePage, Recordset20.AbsolutePage, Recordset15.AbsolutePage
		Get
			Return _innerRecordset.AbsolutePage()
		End Get
		Set(value As ADODB.PositionEnum_Param)
			_innerRecordset.AbsolutePage() = value
		End Set
    End Property

	Public ReadOnly Property EditMode() As ADODB.EditModeEnum Implements _Recordset.EditMode, Recordset21.EditMode, Recordset20.EditMode, Recordset15.EditMode
		Get
			Return _innerRecordset.EditMode()
		End Get
    End Property

	Public Property Filter() As System.Object Implements _Recordset.Filter, Recordset21.Filter, Recordset20.Filter, Recordset15.Filter
		Get
			Return _innerRecordset.Filter()
		End Get
		Set(value As System.Object)
			_innerRecordset.Filter() = value
		End Set
    End Property

	Public ReadOnly Property PageCount() As System.Int32 Implements _Recordset.PageCount, Recordset21.PageCount, Recordset20.PageCount, Recordset15.PageCount
		Get
			Return _innerRecordset.PageCount()
		End Get
    End Property

	Public Property PageSize() As System.Int32 Implements _Recordset.PageSize, Recordset21.PageSize, Recordset20.PageSize, Recordset15.PageSize
		Get
			Return _innerRecordset.PageSize()
		End Get
		Set(value As System.Int32)
			_innerRecordset.PageSize() = value
		End Set
    End Property

	Public Property Sort() As System.String Implements _Recordset.Sort, Recordset21.Sort, Recordset20.Sort, Recordset15.Sort
		Get
			Return _innerRecordset.Sort()
		End Get
		Set(value As System.String)
			_innerRecordset.Sort() = value
		End Set
    End Property

	Public ReadOnly Property Status() As System.Int32 Implements _Recordset.Status, Recordset21.Status, Recordset20.Status, Recordset15.Status
		Get
			Return _innerRecordset.Status()
		End Get
    End Property

	Public ReadOnly Property State() As System.Int32 Implements _Recordset.State, Recordset21.State, Recordset20.State, Recordset15.State
		Get
			Return _innerRecordset.State()
		End Get
    End Property

	Public Property CursorLocation() As ADODB.CursorLocationEnum Implements _Recordset.CursorLocation, Recordset21.CursorLocation, Recordset20.CursorLocation, Recordset15.CursorLocation
		Get
			Return _innerRecordset.CursorLocation()
		End Get
		Set(value As ADODB.CursorLocationEnum)
			_innerRecordset.CursorLocation() = value
		End Set
    End Property

	Public Property Collect(Index As System.Object) As System.Object Implements _Recordset.Collect, Recordset21.Collect, Recordset20.Collect, Recordset15.Collect
		Get
			Return _innerRecordset.Collect(Index)
		End Get
		Set(value As System.Object)
			_innerRecordset.Collect(Index) = value
		End Set
    End Property

	Public Property MarshalOptions() As ADODB.MarshalOptionsEnum Implements _Recordset.MarshalOptions, Recordset21.MarshalOptions, Recordset20.MarshalOptions, Recordset15.MarshalOptions
		Get
			Return _innerRecordset.MarshalOptions()
		End Get
		Set(value As ADODB.MarshalOptionsEnum)
			_innerRecordset.MarshalOptions() = value
		End Set
    End Property

	Public Property DataSource() As System.Object Implements _Recordset.DataSource, Recordset21.DataSource, Recordset20.DataSource
		Get
			Return _innerRecordset.DataSource()
		End Get
		Set(value As System.Object)
			_innerRecordset.DataSource() = value
		End Set
    End Property

	Public ReadOnly Property ActiveCommand() As System.Object Implements _Recordset.ActiveCommand, Recordset21.ActiveCommand, Recordset20.ActiveCommand
		Get
			Return _innerRecordset.ActiveCommand()
		End Get
    End Property

	Public Property StayInSync() As System.Boolean Implements _Recordset.StayInSync, Recordset21.StayInSync, Recordset20.StayInSync
		Get
			Return _innerRecordset.StayInSync()
		End Get
		Set(value As System.Boolean)
			_innerRecordset.StayInSync() = value
		End Set
    End Property

	Public Property DataMember() As System.String Implements _Recordset.DataMember, Recordset21.DataMember, Recordset20.DataMember
		Get
			Return _innerRecordset.DataMember()
		End Get
		Set(value As System.String)
			_innerRecordset.DataMember() = value
		End Set
    End Property

	Public Property Index() As System.String Implements _Recordset.Index, Recordset21.Index
		Get
			Return _innerRecordset.Index()
		End Get
		Set(value As System.String)
			_innerRecordset.Index() = value
		End Set
    End Property

	Public Event WillChangeField As ADODB.RecordsetEvents_WillChangeFieldEventHandler Implements RecordsetEvents_Event.WillChangeField
	Public Event FieldChangeComplete As ADODB.RecordsetEvents_FieldChangeCompleteEventHandler Implements RecordsetEvents_Event.FieldChangeComplete
	Public Event WillChangeRecord As ADODB.RecordsetEvents_WillChangeRecordEventHandler Implements RecordsetEvents_Event.WillChangeRecord
	Public Event RecordChangeComplete As ADODB.RecordsetEvents_RecordChangeCompleteEventHandler Implements RecordsetEvents_Event.RecordChangeComplete
	Public Event WillChangeRecordset As ADODB.RecordsetEvents_WillChangeRecordsetEventHandler Implements RecordsetEvents_Event.WillChangeRecordset
	Public Event RecordsetChangeComplete As ADODB.RecordsetEvents_RecordsetChangeCompleteEventHandler Implements RecordsetEvents_Event.RecordsetChangeComplete
	Public Event WillMove As ADODB.RecordsetEvents_WillMoveEventHandler Implements RecordsetEvents_Event.WillMove
	Public Event MoveComplete As ADODB.RecordsetEvents_MoveCompleteEventHandler Implements RecordsetEvents_Event.MoveComplete
	Public Event EndOfRecordset As ADODB.RecordsetEvents_EndOfRecordsetEventHandler Implements RecordsetEvents_Event.EndOfRecordset
	Public Event FetchProgress As ADODB.RecordsetEvents_FetchProgressEventHandler Implements RecordsetEvents_Event.FetchProgress
	Public Event FetchComplete As ADODB.RecordsetEvents_FetchCompleteEventHandler Implements RecordsetEvents_Event.FetchComplete
End Class
