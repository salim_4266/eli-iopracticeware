@ECHO OFF

..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\IO.Practiceware.Interop.ComWrapper /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\IO.Practiceware.Interop.EventHandler /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\IO.Practiceware.Interop.LogManager /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\IO.Practiceware.Interop.ComWrapper /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\IO.Practiceware.Interop.EventHandler /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\IO.Practiceware.Interop.LogManager /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Wow6432Node\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /grant=Users=F > null

..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /grant=Users=F
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /grant=Users=F > null
..\..\Dependencies\subinacl /subkeyreg HKEY_CLASSES_ROOT\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /grant=Users=F > null


reg delete HKCR\IO.Practiceware.Interop.ComWrapper /f > null
reg delete HKCR\IO.Practiceware.Interop.EventHandler /f > null
reg delete HKCR\IO.Practiceware.Interop.LogManager /f > null
reg delete HKCR\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /f > null
reg delete HKCR\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /f > null
reg delete HKCR\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /f > null
reg delete HKCR\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.ComWrapper /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.EventHandler /f > null
reg delete HKCR\Wow6432Node\IO.Practiceware.Interop.LogManager /f > null
reg delete HKCR\Wow6432Node\CLSID\{2AFDBB7E-80D8-4C88-BE9D-FE94D6BEB968} /f > null
reg delete HKCR\Wow6432Node\CLSID\{AC5B9762-C5D1-4104-99DD-8055777DBDDF} /f > null
reg delete HKCR\Wow6432Node\CLSID\{9D8C1A4F-D415-423C-BB5F-824ABFA4668A} /f > null
reg delete HKCR\Wow6432Node\TypeLib\{D6F6B5FE-B077-45BC-8DBE-3158FC011F32} /f > null

reg delete HKCR\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /f > null
reg delete HKCR\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /f > null
reg delete HKCR\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /f > null
reg delete HKCR\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /f > null
reg delete HKCR\Interface\{0A9ABEB7-C95E-4377-B67A-2B96D2EA23A3} /f > null
reg delete HKCR\Interface\{0896D946-8A8B-4E7D-9D0D-BB29A52B5D08} /f > null
reg delete HKCR\Interface\{D61F6748-F0A3-49FC-8B39-CC1C8EEFD507} /f > null
reg delete HKCR\Interface\{2704B256-B8DB-3C62-B846-B02DD69B1AEC} /f > null

ECHO Deleted Registrations
EXIT /B 0