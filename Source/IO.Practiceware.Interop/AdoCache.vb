﻿Imports ADODB
Imports Soaf.Data
Imports Soaf.Collections

''' <summary>
''' Cache for storing results of ADO calls.
''' </summary>
Public Class AdoCache
    Private ReadOnly _internalCache As IDictionary(Of AdoComServiceOpenRecordsetCommand, Byte()) = New Dictionary(Of AdoComServiceOpenRecordsetCommand, Byte())().Synchronized()

    Public Function TryGetValue(command As AdoComServiceOpenRecordsetCommand, ByRef value As Recordset, Optional ByRef data As Byte() = Nothing) As Boolean
        value = Nothing

        Dim result = _internalCache.TryGetValue(command, data)
        If result AndAlso data IsNot Nothing Then
            value = AdoCom.DeserializeRecordset(data)
        End If
        Return result
    End Function

    Public Sub Store(command As AdoComServiceOpenRecordsetCommand, value As AdoComRecordset)
        _internalCache(command) = value.RecordsetData
    End Sub

    Public Function PrintCacheQueries() As String
        Return String.Join(Environment.NewLine, _internalCache.Keys.Select(Function(v) v.CommandText).ToArray())
    End Function
End Class

