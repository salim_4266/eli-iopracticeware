﻿Imports ADODB
Imports Soaf
Imports Soaf.Data
Imports System
Imports System.Transactions
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Windows.Threading

<ComSourceInterfaces(GetType(ConnectionEvents))>
Public Class AdoConnection
    Implements ADODB.Connection, IDisposable

    Private _transactionScope As TransactionScope
    Private _isCachingEnabled As Boolean
    Private _cache As New AdoCache
    Private _isAsyncExecuteEnabled As Boolean

    Private ReadOnly _batch As New List(Of Tuple(Of AdoComServiceCommand, AdoRecordset))()
    Private ReadOnly _updateBatch As New List(Of Tuple(Of AdoComServiceCommand, AdoRecordset))()
    Private _isBatchInProgress As Boolean
    Private _isUpdateBatchInProgress As Boolean
    Private _isAsyncOpenEnabled As Boolean
    Private _isAsyncUpdateEnabled As Boolean
    Private _bulkCommandProducer As New BulkCommandProducer()
    Private _connectionString As String
    Private _commandTimeout As Integer
    Private _connectionTimeout As Integer
    Private _defaultDatabase As String
    Private _isolationLevel As IsolationLevelEnum
    Private _attributes As Integer
    Private _cursorLocation As CursorLocationEnum
    Private _mode As ConnectModeEnum
    Private _provider As String
    Private _state As Integer
    Private _disposed As Boolean

    Private _cachingScopeDepthCount As Integer

    Public Sub BeginCachingScope()
        IsCachingEnabled = True
        _cachingScopeDepthCount += 1
    End Sub

    Public Sub EndCachingScope()
        If _cachingScopeDepthCount > 0 Then _cachingScopeDepthCount -= 1

        If _cachingScopeDepthCount = 0 Then
            IsCachingEnabled = False
            ClearCache()
        End If
    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether to attempt to load in bulk under certain scenarios.
    ''' </summary>
    ''' <value>
    '''   <c>true</c> if [load in bulk]; otherwise, <c>false</c>.
    ''' </value>
    Public Property BulkCommandProducer As BulkCommandProducer
        Get
            Return _bulkCommandProducer
        End Get
        Set(value As BulkCommandProducer)
            _bulkCommandProducer = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether a batch is in progress.
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if this instance is batch in progress; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsBatchInProgress As Boolean
        Get
            Return _isBatchInProgress
        End Get
        Private Set(value As Boolean)
            _isBatchInProgress = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether a batch is in progress.
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if this instance is batch in progress; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsUpdateBatchInProgress As Boolean
        Get
            Return _isUpdateBatchInProgress
        End Get
        Private Set(value As Boolean)
            _isUpdateBatchInProgress = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether all commands will occur async by default (Open and Update)
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if this instance is async execute enabled; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsAsyncExecuteEnabled As Boolean
        Get
            Return _isAsyncExecuteEnabled
        End Get
        Set(value As Boolean)
            _isAsyncExecuteEnabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether opens will occur async by default.
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if this instance is async open enabled; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsAsyncOpenEnabled As Boolean
        Get
            Return _isAsyncOpenEnabled
        End Get
        Set(value As Boolean)
            _isAsyncOpenEnabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether updates will occur async by default.
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if this instance is async update enabled; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsAsyncUpdateEnabled As Boolean
        Get
            Return _isAsyncUpdateEnabled
        End Get
        Set(value As Boolean)
            _isAsyncUpdateEnabled = value
        End Set
    End Property

    Public Sub BeginBatch()
        If IsBatchInProgress Then
            Throw New InvalidOperationException("A batch is already in progress.")
        End If
        IsBatchInProgress = True
    End Sub

    Public Sub BeginUpdateBatch()
        If IsUpdateBatchInProgress Then
            Throw New InvalidOperationException("A batch is already in progress.")
        End If
        IsUpdateBatchInProgress = True
    End Sub

    Public Sub ExecuteUpdateBatch()
        RaiseEvent WillExecute(Me.GetType().Name, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockBatchOptimistic, -1, EventStatusEnum.adStatusOK, New AdoCommand(), Nothing, Me)

        Dim toExecuteBatch = _updateBatch.ToArray()
        _updateBatch.Clear()

        AdoRecordset.ExecuteCommands(toExecuteBatch.ToList(), IsAsyncExecuteEnabled)

        IsUpdateBatchInProgress = False
    End Sub

    Public Sub ExecuteBatch()
        RaiseEvent WillExecute(Me.GetType().Name, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockBatchOptimistic, -1, EventStatusEnum.adStatusOK, New AdoCommand(), Nothing, Me)

        Dim toExecuteBatch = _batch.ToArray()
        _batch.Clear()
        AdoRecordset.ExecuteCommands(toExecuteBatch.ToList(), IsAsyncExecuteEnabled)

        IsBatchInProgress = False
    End Sub

    Public Sub CancelUpdateBatch()
        _updateBatch.Clear()
        IsBatchInProgress = False
    End Sub

    Public Sub CancelBatch()
        _batch.Clear()
        IsBatchInProgress = False
    End Sub

    Friend Sub AddToBatch(command As AdoComServiceCommand, recordset As AdoRecordset)
        Dim existing = _batch.ToArray().FirstOrDefault(Function(i) i.Item2 Is recordset)
        If existing IsNot Nothing Then _batch.Remove(existing)
        _batch.Add(Tuple.Create(command, recordset))
    End Sub

    Friend Sub AddToUpdateBatch(command As AdoComServiceCommand, recordset As AdoRecordset)
        Dim existing = _updateBatch.ToArray().FirstOrDefault(Function(i) i.Item2 Is recordset)
        If existing IsNot Nothing Then _updateBatch.Remove(existing)
        _updateBatch.Add(Tuple.Create(command, recordset))
    End Sub

    Public Sub New()
    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether to enable caching of requests made using this connection.
    ''' </summary>
    ''' <value>
    '''   <c>true</c> if [enable caching]; otherwise, <c>false</c>.
    ''' </value>
    Public Property IsCachingEnabled As Boolean
        Get
            Return _isCachingEnabled
        End Get
        Set(value As Boolean)
            _isCachingEnabled = value
        End Set
    End Property

    Friend ReadOnly Property Cache As AdoCache
        Get
            Return _cache
        End Get
    End Property

    Public Sub ClearCache()
        _cache = New AdoCache()
    End Sub

    Friend Sub OnExecuteComplete(recordsAffected As Integer, err As ADODB.Error, status As EventStatusEnum, command As ADODB.Command, rs As ADODB.Recordset)
        RaiseEvent ExecuteComplete(recordsAffected, err, status, command, rs, Me)
    End Sub

    Friend Sub OnWillExecute(ByRef source As String, ByRef cursortype As CursorTypeEnum, ByRef locktype As LockTypeEnum, ByRef options As Integer, ByRef adstatus As EventStatusEnum, ByVal pcommand As Command, ByVal precordset As Recordset)
        RaiseEvent WillExecute(source, cursortype, locktype, options, adstatus, pcommand, precordset, Me)
    End Sub

    Public Sub Close() Implements _Connection.Close, Connection15.Close
        '_innerConnection.Close()
        RaiseEvent Disconnect(EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Function Execute(CommandText As System.String, Optional ByRef RecordsAffected As System.Object = Nothing, Optional Options As System.Int32 = -1) As ADODB.Recordset Implements _Connection.Execute, Connection15.Execute

        Dim rs As New AdoRecordset
        Try
            RaiseEvent WillExecute(Me.GetType().Name, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockBatchOptimistic, Options, EventStatusEnum.adStatusOK, Nothing, rs, Me)

            Dim executeOptions = CType(Options, ExecuteOptionEnum)
            Dim isAsync As Boolean = False
            If executeOptions <> ExecuteOptionEnum.adOptionUnspecified AndAlso (executeOptions.HasFlag(ExecuteOptionEnum.adAsyncFetch) OrElse executeOptions.HasFlag(ExecuteOptionEnum.adAsyncFetch)) Then
                isAsync = True
            End If
            If isAsync Then
                rs.Open(CommandText, Me, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockUnspecified, CInt(RecordOpenOptionsEnum.adOpenAsync))
            Else
                rs.Open(CommandText, Me, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockUnspecified, CInt(RecordOpenOptionsEnum.adOpenRecordUnspecified))

                If (rs.State = 1) Then
                    RecordsAffected = rs.RecordCount
                End If

            End If

            Return rs
        Catch ex As Exception
            Trace.TraceError(ex.ToString())

            RaiseEvent ExecuteComplete(-1, New AdoError(ex.ToString(), Me.GetType().Name), EventStatusEnum.adStatusErrorsOccurred, New Command(), rs, Me)
            Throw
        End Try
    End Function

    Public Function BeginTrans() As System.Int32 Implements _Connection.BeginTrans, Connection15.BeginTrans
        'Return _innerConnection.BeginTrans()      
        If (_transactionScope IsNot Nothing) Then
            Throw New InvalidOperationException("Transaction already in progress.")
        End If

        _transactionScope = New TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(20))

        AddHandler Transaction.Current.TransactionCompleted, AddressOf OnTransactionCompleted

        RaiseEvent BeginTransComplete(0, Nothing, EventStatusEnum.adStatusOK, Me)

        Return 0
    End Function

    Private Sub OnTransactionCompleted(ByVal sender As Object, ByVal e As TransactionEventArgs)
        _transactionScope = Nothing
    End Sub

    Public Sub CommitTrans() Implements _Connection.CommitTrans, Connection15.CommitTrans
        '_innerConnection.CommitTrans()
        Try
            If (_transactionScope Is Nothing) Then
                Throw New InvalidOperationException("Transaction not in progress")
            End If

            Try
                Using _transactionScope
                    _transactionScope.Complete()
                End Using
            Finally                
                _transactionScope = Nothing
            End Try

            RaiseEvent CommitTransComplete(Nothing, EventStatusEnum.adStatusOK, Me)
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
            RaiseEvent CommitTransComplete(New AdoError(ex.ToString(), Me.GetType().Name), EventStatusEnum.adStatusErrorsOccurred, Me)
        End Try
    End Sub

    Public Sub RollbackTrans() Implements _Connection.RollbackTrans, Connection15.RollbackTrans
        Try
            '_innerConnection.RollbackTrans()
            If (_transactionScope Is Nothing) Then
                Throw New InvalidOperationException("Transaction not in progress")
            End If
            _transactionScope.Dispose()
            _transactionScope = Nothing
            RaiseEvent RollbackTransComplete(Nothing, EventStatusEnum.adStatusOK, Me)
        Catch ex As Exception
            Trace.TraceError(ex.ToString())
            RaiseEvent RollbackTransComplete(New AdoError(ex.ToString(), Me.GetType().Name), EventStatusEnum.adStatusErrorsOccurred, Me)
        End Try
    End Sub

    Public Sub Open(Optional ConnectionString As System.String = "", Optional UserID As System.String = "", Optional Password As System.String = "", Optional Options As System.Int32 = -1) Implements _Connection.Open, Connection15.Open
        '_innerConnection.Open(ConnectionString, UserID, Password, Options)
        RaiseEvent WillConnect(ConnectionString, UserID, Password, Options, EventStatusEnum.adStatusOK, Me)
        _connectionString = ConnectionString
        _state = 1
        RaiseEvent ConnectComplete(Nothing, EventStatusEnum.adStatusOK, Me)
    End Sub

    Public Function OpenSchema(Schema As ADODB.SchemaEnum, Optional Restrictions As System.Object = Nothing, Optional SchemaID As System.Object = Nothing) As ADODB.Recordset Implements _Connection.OpenSchema, Connection15.OpenSchema
        Return Nothing
    End Function

    Public Sub Cancel() Implements _Connection.Cancel
    End Sub

    Public Sub WaitAll()
        AdoRecordset.WaitAllInternal()
    End Sub


    Public ReadOnly Property Properties() As ADODB.Properties Implements _Connection.Properties, Connection15.Properties, _ADO.Properties
        Get
            Return New AdoProperties()
        End Get
    End Property

    Public Property ConnectionString() As System.String Implements _Connection.ConnectionString, Connection15.ConnectionString
        Get
            Return _connectionString
        End Get
        Set(value As System.String)
            _connectionString = value
        End Set
    End Property

    Public Property CommandTimeout() As System.Int32 Implements _Connection.CommandTimeout, Connection15.CommandTimeout
        Get
            Return _commandTimeout
        End Get
        Set(value As System.Int32)
            _commandTimeout = value
        End Set
    End Property

    Public Property ConnectionTimeout() As System.Int32 Implements _Connection.ConnectionTimeout, Connection15.ConnectionTimeout
        Get
            Return _connectionTimeout
        End Get
        Set(value As System.Int32)
            _connectionTimeout = value
        End Set
    End Property

    Public ReadOnly Property Version() As System.String Implements _Connection.Version, Connection15.Version
        Get
            Return "2.8"
        End Get
    End Property

    Public ReadOnly Property Errors() As ADODB.Errors Implements _Connection.Errors, Connection15.Errors
        Get
            Return New AdoErrors()
        End Get
    End Property

    Public Property DefaultDatabase() As System.String Implements _Connection.DefaultDatabase, Connection15.DefaultDatabase
        Get
            Return _defaultDatabase
        End Get
        Set(value As System.String)
            _defaultDatabase = value
        End Set
    End Property

    Public Property IsolationLevel() As ADODB.IsolationLevelEnum Implements _Connection.IsolationLevel, Connection15.IsolationLevel
        Get
            Return _isolationLevel
        End Get
        Set(value As ADODB.IsolationLevelEnum)
            _isolationLevel = value
        End Set
    End Property

    Public Property Attributes() As System.Int32 Implements _Connection.Attributes, Connection15.Attributes
        Get
            Return _attributes
        End Get
        Set(value As System.Int32)
            _attributes = value
        End Set
    End Property

    Public Property CursorLocation() As ADODB.CursorLocationEnum Implements _Connection.CursorLocation, Connection15.CursorLocation
        Get
            Return _cursorLocation
        End Get
        Set(value As ADODB.CursorLocationEnum)
            _cursorLocation = value
        End Set
    End Property

    Public Property Mode() As ADODB.ConnectModeEnum Implements _Connection.Mode, Connection15.Mode
        Get
            Return _mode
        End Get
        Set(value As ADODB.ConnectModeEnum)
            _mode = value
        End Set
    End Property

    Public Property Provider() As System.String Implements _Connection.Provider, Connection15.Provider
        Get
            Return _provider
        End Get
        Set(value As System.String)
            _provider = value
        End Set
    End Property

    Public ReadOnly Property State() As System.Int32 Implements _Connection.State, Connection15.State
        Get
            Return _state
        End Get
    End Property

    Public Event InfoMessage As ADODB.ConnectionEvents_InfoMessageEventHandler Implements ConnectionEvents_Event.InfoMessage
    Public Event BeginTransComplete As ADODB.ConnectionEvents_BeginTransCompleteEventHandler Implements ConnectionEvents_Event.BeginTransComplete
    Public Event CommitTransComplete As ADODB.ConnectionEvents_CommitTransCompleteEventHandler Implements ConnectionEvents_Event.CommitTransComplete
    Public Event RollbackTransComplete As ADODB.ConnectionEvents_RollbackTransCompleteEventHandler Implements ConnectionEvents_Event.RollbackTransComplete
    Public Event WillExecute As ADODB.ConnectionEvents_WillExecuteEventHandler Implements ConnectionEvents_Event.WillExecute
    Public Event ExecuteComplete As ADODB.ConnectionEvents_ExecuteCompleteEventHandler Implements ConnectionEvents_Event.ExecuteComplete
    Public Event WillConnect As ADODB.ConnectionEvents_WillConnectEventHandler Implements ConnectionEvents_Event.WillConnect
    Public Event ConnectComplete As ADODB.ConnectionEvents_ConnectCompleteEventHandler Implements ConnectionEvents_Event.ConnectComplete
    Public Event Disconnect As ADODB.ConnectionEvents_DisconnectEventHandler Implements ConnectionEvents_Event.Disconnect

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not _disposed Then
            If disposing Then
                If _transactionScope IsNot Nothing Then _transactionScope.Dispose()
                _batch.Clear()
                _updateBatch.Clear()

            End If

            _disposed = True
        End If
    End Sub
End Class
