﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="clientApplicationConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Configuration.ClientApplicationConfiguration, IO.Practiceware.Core]], Soaf" />
    <section name="applicationServerClient" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Configuration.ApplicationServerClientConfiguration, IO.Practiceware.Core]], Soaf" />
    <section name="entityFrameworkConfiguration" type="Soaf.Configuration.XmlConfigurationSection`1[[Soaf.EntityFramework.EntityFrameworkConfiguration, Soaf]], Soaf" />
    <section name="electronicClinicalQualityMeasures" type="Soaf.Configuration.XmlConfigurationSection`1[[IO.Practiceware.Integration.Cda.ElectronicClinicalQualityMeasuresConfiguration, IO.Practiceware.Core]], Soaf" />

    <!-- Settings -->
    <section name="systemAppSettings" type="Soaf.Configuration.XmlConfigurationSection`1[[Soaf.Configuration.KeyValueConfigurationCollection, Soaf]], Soaf" />
    <section name="customAppSettings" type="IO.Practiceware.Configuration.ClientContextConfigurationSection`1[[Soaf.Configuration.KeyValueConfigurationCollection, Soaf]], IO.Practiceware.Core" />
    <section name="customConnectionStrings" type="IO.Practiceware.Configuration.ClientContextConfigurationSection`1[[Soaf.Configuration.ConnectionStringConfigurationCollection, Soaf]], IO.Practiceware.Core" />

    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
  </configSections>
  <entityFrameworkConfiguration>
    <connection repositoryTypeName="IO.Practiceware.Model.Legacy.IDynamicFormsRepository, IO.Practiceware.Model" connectionStringName="DynamicFormsRepository" metadata="res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.csdl|res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.ssdl|res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.msl" lazyLoadingEnabled="false" proxyCreationEnabled="false" />
    <connection repositoryTypeName="IO.Practiceware.Model.Legacy.ILegacyPracticeRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository" metadata="res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.csdl|res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.ssdl|res://IO.Practiceware.Model/Legacy.LegacyPracticeRepositoryModel.msl" lazyLoadingEnabled="false" proxyCreationEnabled="false" />
    <connection repositoryTypeName="IO.Practiceware.Model.IPracticeRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository"  lazyLoadingEnabled="false" proxyCreationEnabled="false" metadata="res://IO.Practiceware.Model/PracticeRepositoryModel.csdl|res://IO.Practiceware.Model/PracticeRepositoryModel.ssdl|res://IO.Practiceware.Model/PracticeRepositoryModel.msl" cacheTypeName="Edm_EntityMappingGeneratedViews.ViewsForBaseEntitySetsOfIOPracticewareModelPracticeRepository" />
    <connection repositoryTypeName="IO.Practiceware.Model.Auditing.IAuditRepository, IO.Practiceware.Model" connectionStringName="PracticeRepository"  lazyLoadingEnabled="false" proxyCreationEnabled="false" metadata="res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.csdl|res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.ssdl|res://IO.Practiceware.Model/Auditing.AuditRepositoryModel.msl" />
    <connection repositoryTypeName="IO.Practiceware.Model.Legacy.IDynamicFormsRepository, IO.Practiceware.Model" connectionStringName="DynamicFormsRepository" metadata="res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.csdl|res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.ssdl|res://IO.Practiceware.Model/Legacy.DynamicFormsRepositoryModel.msl" lazyLoadingEnabled="false" proxyCreationEnabled="false" />
  </entityFrameworkConfiguration>

  <systemAppSettings>
    <add key="HelpServerUrl" value ="http://help.iopsupport.com:8080/"/>
    <add key="SupportSiteUrl" value ="https://support.iopracticeware.com/"/>
    <add key="LOGENTRIES_TOKEN" value="c8d9d819-92cf-4420-b39b-a62384bdbb9e" />
  </systemAppSettings>

  <customAppSettings source="IO.Practiceware.Custom.config" xpath="/configuration/appSettings" />
  <clientApplicationConfiguration source="IO.Practiceware.Custom.config" xpath="/configuration/clientApplicationConfiguration" />
  <applicationServerClient source="IO.Practiceware.Custom.config" xpath="/configuration/applicationServerClient" />
  <customConnectionStrings source="IO.Practiceware.Custom.config" xpath="/configuration/connectionStrings" />
  
  <nlog>
    <targets>
      <target name="logentries" type="Logentries" debug="false" httpPut="false" ssl="false"
      layout="${date:format=ddd MM-dd-yyy} ${time:format=HH:mm:ss} ${date:format=zzz} | LoggerSource = ${logger} | Level = ${LEVEL} | ${message}"/>
    </targets>
    <rules>
      <logger name="*" minLevel="Warn" appendTo="logentries"/>
    </rules>
  </nlog>

  <electronicClinicalQualityMeasures>
    <category3ReportingService serviceUri="https://cqm.iopracticeware.com" login="cqm" password="GMw4wsg8gV" />
  </electronicClinicalQualityMeasures>

  <system.web>
    <membership defaultProvider="MembershipProvider">
      <providers>
        <clear />
        <add name="MembershipProvider" type="Soaf.Security.MembershipProvider, Soaf.Extensions" />
      </providers>
    </membership>

    <roleManager enabled="true" defaultProvider="RoleProvider">
      <providers>
        <clear />
        <add name="RoleProvider" type="Soaf.Security.RoleProvider, Soaf.Extensions"/>
      </providers>
    </roleManager>
  </system.web>

  <system.serviceModel>
    <!-- The empty client section be present because endpoints are added dynamically to it 
    and those endpoints must have the contextual information from this config file. -->
    <client>
    </client>
    <bindings>
      <basicHttpBinding>
        <binding transferMode="Streamed" closeTimeout="00:02:00" openTimeout="00:02:00" sendTimeout="00:20:00" maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" name="HttpsBinding">
          <readerQuotas maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" maxNameTableCharCount="2147483647" maxStringContentLength="2147483647" />
          <security mode="Transport">
            <transport clientCredentialType="None" />
          </security>
        </binding>
      </basicHttpBinding>
      <customBinding>
        <binding name="WsHttpBinding" openTimeout="00:02:00" receiveTimeout="00:20:00" sendTimeout="00:20:00">
          <transactionFlow transactionProtocol="WSAtomicTransactionOctober2004" />
          <textMessageEncoding maxReadPoolSize="2147483647" maxWritePoolSize="2147483647">
            <readerQuotas maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" maxNameTableCharCount="2147483647" maxStringContentLength="2147483647" />
          </textMessageEncoding>
          <httpsTransport transferMode="Streamed" maxReceivedMessageSize="2147483647" maxBufferPoolSize="2147483647" allowCookies="false" maxBufferSize="2147483647" />
        </binding>
      </customBinding>
    </bindings>
    <behaviors>
      <endpointBehaviors>
        <behavior name="ProtoEndpointBehavior">
          <protobuf/>
        </behavior>
      </endpointBehaviors>
    </behaviors>
    <extensions>
      <behaviorExtensions>
        <add name="protobuf" type="Soaf.Web.ProtoBehaviorExtension, Soaf"/>
      </behaviorExtensions>
    </extensions>
  </system.serviceModel>

  <system.diagnostics>
    <trace autoflush="true"/>
    <sources>
      <!--
      <source name="AdoService" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener"
            type="Soaf.Logging.RawTextWriterTraceListener, Soaf.Extensions"
            initializeData="AdoServiceLog.log" traceOutputOptions="None" />
          <remove name="Default" />
        </listeners>
      </source>
      <source name="AdoComService" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener"
            type="Soaf.Logging.RawTextWriterTraceListener, Soaf.Extensions"
            initializeData="AdoComServiceLog.log" traceOutputOptions="None" />
          <remove name="Default" />
        </listeners>
      </source>
      <source name="FileManager" switchName="SourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="listener" type="IO.Practiceware.Logging.ExtendedFileTraceListener, IO.Practiceware.Core" initializeData="FileManager.log" />
          <remove name="Default" />
        </listeners>
      </source>
      -->
    </sources>
    <switches>
      <add name="SourceSwitch" value="All" />
    </switches>
  </system.diagnostics>
</configuration>
