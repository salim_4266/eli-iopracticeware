﻿Imports IO.Practiceware.Services.ApplicationSettings
Imports IO.Practiceware.Model
Imports Soaf

Public Class ConfigurationsSettings

    Private Sub btnBrowse1_Click(sender As Object, e As EventArgs) Handles btnBrowse1.Click
        Dim result = FolderBrowserDialog1.ShowDialog()

        If (result = DialogResult.OK) Then
            txtElectronicClaimDir.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnBrowse2_Click(sender As Object, e As EventArgs) Handles btnBrowse2.Click
        Dim result = FolderBrowserDialog1.ShowDialog()

        If (result = DialogResult.OK) Then
            txtPaperClaimDirectory.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnSetConfiguration_Click(sender As Object, e As EventArgs) Handles btnSetConfiguration.Click

        Dim setMachineService = ServiceProvider.GetService(Of IApplicationSettingsService)()

        Try
            Dim viewModel = New ClaimsConfigurationApplicationSetting()
            viewModel.ElectronicClaimsFileDirectory = txtElectronicClaimDir.Text
            viewModel.PaperClaimsFileDirectory = txtPaperClaimDirectory.Text

            Dim setting = New ApplicationSettingContainer(Of ClaimsConfigurationApplicationSetting)()
            setting.Name=ApplicationSetting.ClaimsDirectory
            setting.MachineName = txtMachineName.Text
            setting.Value = viewModel

            setMachineService.SetSetting(setting)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            MessageBox.Show("Success")
        End Try

    End Sub
End Class