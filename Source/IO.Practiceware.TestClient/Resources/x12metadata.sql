

IF OBJECT_ID('[dbo].[InsteadOfInsertIntoPatientFinancial]', 'TR') IS NOT NULL
BEGIN
    EXEC('DROP TRIGGER [dbo].[InsteadOfInsertIntoPatientFinancial]')
END

GO

CREATE TRIGGER InsteadOfInsertIntoPatientFinancial ON dbo.PatientFinancial
INSTEAD OF INSERT
AS 
BEGIN
	INSERT INTO model.InsurancePolicies
	(PolicyCode
	,GroupCode
	,Copay
	,Deductible
	,StartDateTime
	,PolicyHolderPatientId
	,InsurerId
	,MedicareSecondaryReasonCodeId)
	SELECT
	i.FinancialPerson
	,i.FinancialGroupId
	,i.FinancialCopay
	,0
	,CONVERT(DATETIME,i.FinancialStartDate)
	,i.PatientId
	,i.FinancialInsurerId
	,NULL
	FROM Inserted i

	DECLARE @CurrentPatientId INT
	SET @CurrentPatientId = (SELECT PatientId FROM Inserted)

	INSERT INTO model.PatientInsurances
	(InsuranceTypeId
	,PolicyHolderRelationshipTypeId
	,OrdinalId
	,InsuredPatientId
	,InsurancePolicyId
	,EndDateTime
	,IsDeleted)
	SELECT
	CASE i.FinancialInsType
		WHEN 'V'
			THEN 2
		WHEN 'A'
			THEN 3			
		WHEN 'W'
			THEN 4
		ELSE 1
	END
	,CASE (
		CASE 
			WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
				THEN ISNULL(dbo.GetPolicyHolderRelationshipType(p.Id),'Y')
			ELSE ISNULL(dbo.GetSecondPolicyHolderRelationshipType(p.Id),'')
			END
		)
		WHEN 'Y'
			THEN 1
		WHEN 'S'
			THEN 2
		WHEN 'C'
			THEN 3
		WHEN 'E'
			THEN 4
		WHEN 'L'
			THEN 6
		WHEN 'O'
			THEN 7
		WHEN 'D'
			THEN 7
		WHEN 'P'
			THEN 7
		ELSE CASE 
				WHEN ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id) = i.PatientId
					THEN 1
				WHEN ISNULL(dbo.GetSecondPolicyHolderPatientId(p.Id),0) = i.PatientId
					THEN 1
			ELSE 5
			END
	END
	,CASE i.FinancialInsType
			WHEN 'V'
				THEN 200
			WHEN 'A'
				THEN 3000			
			WHEN 'W'
				THEN 40000
			ELSE 10
			END + CASE 
					WHEN i.PatientId = ISNULL(dbo.GetPolicyHolderPatientId(p.Id),p.Id)
						THEN 0
					ELSE 3
				  END + CONVERT(INT, i.FinancialPIndicator)
	,i.PatientId
	,(SELECT TOP 1 Id FROM model.InsurancePolicies WHERE PolicyHolderPatientId = @CurrentPatientId ORDER BY Id DESC)
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(DATETIME,i.FinancialEndDate)
					END
		ELSE NULL
	END
	,CASE 
		WHEN (LEN(i.FinancialEndDate) = 8)
			THEN CASE
					WHEN i.FinancialEndDate >= i.FinancialStartDate
						THEN CONVERT(BIT, 1)
					END
		ELSE CONVERT(BIT, 0)
	END
	FROM Inserted i
	JOIN model.Patients p ON p.Id = i.PatientId
END
GO

--patient #3 Medicare only
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'ONLY'
	,'B'
	,''
	,''
	,'450 RIVERSIDE DRIVE'
	,'APT 2B'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'2123335544'
	,'7186669955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19490105'
	,''
	,'SPANISH'
	,''
	,''
	,''
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #3 MEDICARE ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, '666999888A', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD APPOINTMENT PATIENT #3
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120120,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SRG'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'SOO LIN'

--ADD ACTIVITY RECORD PATIENT #3
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120120, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--ADD CLINICAL DATA AND PRESCRIPTIONS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition], [PostOpPeriod], [Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Y', '', '1', 'COMPREHENSIVE', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'H', '', '/CHIEFCOMPLAINTS', 'CC:FLASHES', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', 'TIME=01:07 PM (SMITH)', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'F', '', '/SMOKING', '*CURRENTEVERYDAY=T14854 <CURRENTEVERYDAY>', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OD', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Q', 'OS', '1', '366.16.41', 'J', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OD', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'U', 'OS', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '5', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '4', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'Z', '', '1', '92012', '', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/LENTICULAR SINGLE VISION-2/ PLANO ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', '')

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'A', '', '2', 'DISPENSE CL RX-9/-1/-2/ +2.00 -0.50 X180 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-3/ +0.25 ADD-READING:+1.75 BASE CURVE:8.2 DIAMETER:8-4/-5/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-6/' + cast(IDENT_CURRENT('clinventory') as nvarchar) + '-7/', '^', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #3
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '6374.75'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120120
	,'6385'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'5940.82'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN PracticeVendors ON VendorLastName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #3

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/6374.75'
	,'B'
	)


--SERVICES Patient #3
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'67028', 'RT', '1', '250.00', '04', '11'
		,20120120
        , '2', 'A', 'F', 'F', '156.39', '1', 0,
convert(varchar(8),getdate(),112), 'INTRAVITREAL I')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
		+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'J3490', '', '5', '1050.00', '12', '11'
		,20120120
        , '1234', 'A', 'F', 'F', '5126.78', '2', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')


--NOTE for Drug Code
INSERT INTO PatientNotes
           ([PatientId]
           ,[AppointmentId]
           ,[NoteType]
           ,[Note1]
           ,[Note2]
           ,[Note3]
           ,[Note4]
           ,[UserName]
           ,[NoteDate]
           ,[NoteSystem]
           ,[NoteEye]
           ,[NoteCommentOn]
           ,[NoteCategory]
           ,[NoteOffDate]
           ,[NoteHighlight]
           ,[NoteILPNRef]
           ,[NoteAlertType]
           ,[NoteAudioOn]
           ,[NoteClaimOn])
SELECT IDENT_CURRENT('model.Patients'), 
	IDENT_CURRENT('Appointments'), 
	'B',
	'HI VINESH THIS IS A DRUG NOTE',
	'',
	'',
	'',
	'IOP',
	20120120,
	'R000' +  CONVERT(nvarchar, IDENT_CURRENT('model.Patients')) + '-000' + CONVERT(nvarchar, IDENT_CURRENT('Appointments')) + 'CJ3490',
	'',
	'F',
	'',
	'',
	'',
	'',
	'00000000000000000000000000000000',
	'F',
	'T'


-- 2d ServiceTransaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--3rd service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '', '1', '225.00', '01', '11'
		,20120120
        , '', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- PatientReceivablePayments Patient #3
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,10.25
	,N'20120101'
	,N'105289455'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''

--payment for Patient #3
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,10.25
	,N'20120101'
	,N'105289455'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N'20110101'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 3rd ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--4th Service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'RT', '1', '170.00', '02', '11'
		,20120120
        , '2', 'A', 'T', 'F', '112.45', '4', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 4th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--5th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
			+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'LT', '1', '170.00', '02', '11'
		,20120120
        , '2', 'A', 'T', 'F', '112.45', '5', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 5th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--6th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92133', '', '1', '120.00', '02', '11'
		,20120120
        , '1', 'A', 'T', 'F', '123.22', '6', 0,
convert(varchar(8),getdate(),112), 'OCT')


-- 6th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--7th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92250', '', '1', '110.00', '02', '11'
		,20120120
        , '1', 'A', 'T', 'F', '98.16', '7', 0,
convert(varchar(8),getdate(),112), 'FUNDUS PHOTOS')


-- 7th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--8th service Patient #3
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92083', '', '1', '90.00', '02', '11'
		,20120120
        , '5', 'A', 'T', 'F', '78.81', '8', 0,
convert(varchar(8),getdate(),112), 'VISUAL FIELD')

-- 8th ServiceTransaction Patient #3
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


---PATIENT #1 AETNA ONLY	
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'AETNA'
	,'ONLY'
	,''
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)



--PatientFinancial #1 AETNA ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '12345', ''
, 25, 1, '20110201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'AETNA'




--ADD APPOINTMENT PATIENT #1
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111207,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #1
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111207, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #1

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.15', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #1
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '983.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111207
	,'993.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'993'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN PracticeVendors ON VendorLastName = 'ABDY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #1
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/983'
	,'B'
	)


--SERVICES  patient #1
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'99244', '24', '1', '675.00', '01', '11',
			20111207, '', 'A', 'F', 'T', '675.00', '1', 0,
			20111207, 'CONSULTATION')


-- PatientReceivablePayments  patient #1
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'='
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''


--payment from Patient #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,10
	,N'20111207'
	,N'10525'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,dbo.GetIdentCurrent('dbo.PatientReceivablePayments')
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 1st ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,665
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2d service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag]
		  ,[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92250', '', '1', '318.00', '01', '11',
			20111207, '2', 'A', 'T', 'F', '318.00', '2', 0,
			20111207, 'FUNDUS PHOTOS')
			
-- 2d ServiceTransaction  patient #1
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120101
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')




--PATIENT #2 Blue Shield And Spouse's Aetna
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'BLUESHIELD'
	,'AETNA'
	,'SPOUSE'
	,''
	,''
	,'450 EAST 50TH ST'
	,'APT 2A'
	,'NEW YORK'
	,'NY'
	,'10012'
	,'2123334444'
	,'7186665555'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19610105'
	,''
	,'RUSSIAN'
	,'0'
	,IDENT_CURRENT('PracticeVendors')
	,IDENT_CURRENT('PracticeVendors')-1
	,''
	,''
	,''
	,0
	,''
	,'N'
	,PatientId
	,'S'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20110501'
	,'A'
	,'MRS'
	,'T'
	,''
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'

UPDATE PatientDemographics SET PolicyPatientId = PatientId, Relationship = 'Y'
WHERE LastName = 'BLUESHIELD' AND FirstName = 'AETNA' AND MiddleInitial = 'SPOUSE'

--PatientFinancial #2
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'YLK098765', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'BLUE SHIELD'

--ADD APPOINTMENT PATIENT #2
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120104,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #2
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120104, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Diagnoses for Patient #2
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '3', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '365.01', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '4', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '550'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120104
	,'550'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'550'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #2
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/550'
	,'B'
	)


--SERVICES
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ( '000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92235', 'RTLT', '2', '200.00', '01', '11',
			20120104, '1', 'A', 'T', 'F', '200.00', '1', 0,
			20120104, 'FLUORESCEIN')

-- 1st ServiceTransaction  patient #2
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2d service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92014', '', '1', '150.00', '01', '11',
			20120104, '', 'A', 'F', 'F', '150.00', '2', 0,
			20120104, 'OFFICE VISIT')

-- 2d ServiceTransaction  patient #2
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--Patient #4 Medicaid only
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAID'
	,'ONLY'
	,'G'
	,''
	,''
	,'46699 EASTERN BOULEVARD'
	,''
	,'PHILADELPHIA'
	,'PA'
	,'19019'
	,'2153335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19280105'
	,''
	,'SPANISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #4 medicaid only
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'DEU99008', ''
, 0, 1, '20100711', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICAID'


--ADD APPOINTMENT PATIENT #4
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111122,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	fac.ResourceId, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111122
FROM Resources dr
INNER JOIN Resources fac ON fac.ResourceType = 'R' and fac.ServiceCode = '02' and fac.ResourceName = 'BROOK'
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #4
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111122, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #4
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')

--Add Authorization
INSERT INTO PatientPreCerts (PatientId, PreCert, PreCertInsId, PreCertDate, PreCertApptId, PreCertComment, PreCertLocationId, PreCertStatus)
SELECT IDENT_CURRENT('model.Patients'), '9080ABDC' , InsurerId, 20111122, IDENT_CURRENT('Appointments'), '', 0, 'A'
FROM PracticeInsurers
WHERE InsurerName = 'MEDICAID'

UPDATE Appointments 
SET PreCertId = IDENT_CURRENT('PatientPreCerts')
WHERE AppointmentId = IDENT_CURRENT('Appointments')


-- PatientReceivables Patient #4
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111122
	,'1500'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,ap.ResourceId2
	,''
	,''
	,623.63
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE ap.AppointmentId = IDENT_CURRENT('Appointments')


-- BILLING TRANSACTIONS patient #4
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120121
	,654
	,'I'
	,'//Amt/1500'
	,'B'
	)

--1st service  patient #4
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'66984', 'RT', '1', '1500.00', '01', '24',
			20111122, '', 'A', 'F', 'F', '1500.00', '1', 0,
			20111122, 'CATARACT SURGERY')

-- 1st ServiceTransaction  patient #4
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--Patient #5 Medicare/Medicaid
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'MEDICAID'
	,'C'
	,''
	,''
	,'85 RED ROSE STREET'
	,''
	,'ATLANTA'
	,'GA'
	,'10022'
	,'3033335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19151123'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #5 MEDICARE/MEDICAID
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '999663333A', ''
, 0, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD 2d INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '345TYU', ''
, 0, 2, '20110501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICAID'


--ADD APPOINTMENT #1 PATIENT #5
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20110919,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = 'TLC'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #5
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110919, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #5
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables #1 Patient #5
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110919
	,'885.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'885.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #5
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110919
	,654
	,'I'
	,'//Amt/885'
	,'B'
	)


--1st service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '', '1', '225.00', '01', '11'
		,20110919
        , '123', 'A', 'F', 'F', '132.56', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--2nd Service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'RT', '1', '170.00', '02', '11'
		,20110919
        , '2', 'A', 'T', 'F', '89.42', '2', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 2nd ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--3rd service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
			+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'LT', '1', '170.00', '02', '11'
		,20110919
        , '2', 'A', 'T', 'F', '89.42', '3', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 3rd ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--4th service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92133', '', '1', '120.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '123.22', '4', 0,
convert(varchar(8),getdate(),112), 'OCT')


-- 4th ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--5th service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92250', '', '1', '110.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '98.16', '5', 0,
convert(varchar(8),getdate(),112), 'FUNDUS PHOTOS')


-- 5th ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--6th service Patient #5
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92083', '', '1', '90.00', '02', '11'
		,20110919
        , '1', 'A', 'T', 'F', '78.81', '6', 0,
convert(varchar(8),getdate(),112), 'VISUAL FIELD')

-- 6th ServiceTransaction Patient #5
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110919
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- Patient #5 Test #6 MedicareMedicaid - Medicaid Secondary
--ADD APPOINTMENT PATIENT #5, test #7
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20110715,
	900, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--ADD ACTIVITY RECORD PATIENT #5/6th test
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110715, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #5/6th test
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables  Patient #5/Test #6 - Medicare
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '82.09'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110715
	,'425.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'425.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN PracticeVendors ON VendorLastName = 'COX'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #5/Test #6 Medicare Primary CLOSED
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20110715
	,654
	,'I'
	,'//Amt/425'
	,'B'
	)


--1st service Patient #5/#6 test
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99204', '', '1', '225.00', '01', '11'
		,20110715
        , '12', 'A', 'F', 'F', '125.51', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #5/#6 test TO MEDICARE
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,225
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- PatientReceivablePayments  patient #5/#6
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5236.36
	,N'20110821'
	,N'7989'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'MEDICARE'
DECLARE @BatchId int
SET @BatchId = (SELECT dbo.GetIdentCurrent('dbo.PatientReceivablePayments'))

-- payment for Patient #5/Test #6 from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,98.56
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--contractual writeoff for Patient #5/Test #6 from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'X'
	,99.49
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--coinsurance for Patient #5/Test #6 from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'|'
	,26.95
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--2nd Service Patient #5/Test #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'RT', '1', '100.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '89.42', '2', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 2nd Service 1st Transaction Patient #5 Test #6 TO MEDICARE
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- PatientReceivablePayments  patient #5/#6 2nd service
-- payment for Patient #5/Test #5 from Medicare service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,67.43
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--contractual writeoff for Patient #5/Test #6 from Medicare Service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--deductible for Patient #5/Test #6 from Medicare Service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'!'
	,3.58
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--coinsurance for Patient #5/Test #6 from Medicare Service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'|'
	,18.99
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--3rd service Patient #5/#6 test
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
			+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'LT', '1', '100.00', '02', '11'
		,20110715
        , '2', 'A', 'T', 'F', '89.42', '3', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 3rd ServiceTransaction Patient #5/#6 test TO MEDICARE
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- PatientReceivablePayments  patient #5/#6 3d service
-- payment for Patient #5/Test #6 from Medicare service #2
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,57.43
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--contractual writeoff for Patient #5/Test #6 from Medicare Service #3
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--deductible for Patient #5/Test #6 from Medicare Service #3
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'!'
	,13.58
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--coinsurance for Patient #5/Test #6 from Medicare Service #3
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'|'
	,18.99
	,N'20110825'
	,N'7989'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'1'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- PatientReceivables Patient #5/6th test - Medicaid
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '82.09'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110715
	,'425.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'425.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #5/Test #6
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110815
	,654
	,'I'
	,'//Amt/82.09'
	,'B'
	)
	


-- 1st Service, 2D Transaction Patient #5/#6 test - TO Medicaid
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110815
	,26.95
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '99204'
AND InsurerName = 'MEDICAID'


-- 2nd Service 2nd Transaction Patient #5 Test #6 TO MEDICAID
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110715
	,22.57
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92235' and Modifier = 'RT'
AND InsurerName = 'MEDICAID'


-- 3rd Service 2D Transaction Patient #5/#6 test TO MEDICAID
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal')
	,ItemId
	,20110715
	,32.57
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri on pri.InsurerId = pr.InsurerId
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
AND Service = '92235' and Modifier = 'LT'
AND InsurerName = 'MEDICAID'





----------PATIENT #6 ----
--Patient #6 Aetna spouse/cigna self
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'AETNASPOUSEPRIMARY'
	,'CIGNASELFSECONDARY'
	,''
	,''
	,''
	,'448 STATE STREET'
	,''
	,'BROOKLYN'
	,'NY'
	,'12117'
	,'7188750653'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19550715'
	,''
	,'FRENCH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,pdSpouse.PatientId
	,'S'
	,'N'
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'HISP'
FROM PatientDemographics pdSpouse
WHERE pdSpouse.LastName = 'AETNA' AND pdSpouse.FirstName = 'ONLY'

UPDATE PatientDemographics
SET SecondPolicyPatientId = PatientId
WHERE LastName = 'AETNASPOUSEPRIMARY' and FirstName = 'CIGNASELFSECONDARY'

--PatientFinancial #6 AETNA SPOUSE IS PRIMARY; THIS POLICY IS THE CIGNA SECONDARY SELF
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGNAPOLICY', ''
, 0, 1, '20101201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'


--Add referral
INSERT INTO PatientReferral
           ([PatientId]
           ,[Referral]
           ,[ReferredFrom]
           ,[ReferredFromId]
           ,[ReferralDate]
           ,[ReferralExpireDate]
           ,[ReferralVisits]
           ,[ReferralVisitsLeft]
           ,[Reason]
           ,[ReferredTo]
           ,[ReferredInsurer]
           ,[Status])
SELECT IDENT_CURRENT('model.Patients'),
	'referral4567',
	'',
	VendorId,
	20111201,
	20121201,
	6,
	2,
	'',
	ResourceId,
	InsurerId,
	'A'
FROM PracticeVendors pv 
INNER JOIN Resources re ON ResourceType = 'D' and ResourceLastName = 'Reiss'
INNER JOIN PracticeInsurers pri ON InsurerName = 'AETNA'
WHERE VendorLastName = 'COX'

--ADD APPOINTMENT PATIENT #6
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20120107,
	700, 10, IDENT_CURRENT('PatientReferral'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120201
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'NPG'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #6
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20120107, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #6
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '365.10', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #6
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '885'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120107
	,'885.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,IDENT_CURRENT('PracticeVendors')
	,0
	,''
	,''
	,0
	,''
	,''
	,'775.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON LastName = 'AETNA' and FirstName = 'ONLY'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #6
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120107
	,654
	,'I'
	,'//Amt/375'
	,'B'
	)


--1st service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'67028', 'RT', '1', '375', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '175', '1', 0,
convert(varchar(8),getdate(),112), 'INJECTION')

-- 1st ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2d service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '2459RT', '1', '100.00', '01', '11'
		,20120107
        , '213', 'A', 'F', 'F', '100', '2', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')

-- 2ND ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- 3rD Service Patient #6
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'25999', '', '1', '100', '01', '11'
		,20120107
        , '1', 'A', 'F', 'F', '100', '3', 0,
convert(varchar(8),getdate(),112), 'NOS DRUG')

--NOTE for NOS Code
INSERT INTO PatientNotes
           ([PatientId]
           ,[AppointmentId]
           ,[NoteType]
           ,[Note1]
           ,[Note2]
           ,[Note3]
           ,[Note4]
           ,[UserName]
           ,[NoteDate]
           ,[NoteSystem]
           ,[NoteEye]
           ,[NoteCommentOn]
           ,[NoteCategory]
           ,[NoteOffDate]
           ,[NoteHighlight]
           ,[NoteILPNRef]
           ,[NoteAlertType]
           ,[NoteAudioOn]
           ,[NoteClaimOn])
SELECT IDENT_CURRENT('model.Patients'), 
	IDENT_CURRENT('Appointments'), 
	'B',
	'HI VINESH THIS IS AN NOS NOTE',
	'',
	'',
	'',
	'IOP',
	20120107,
	'R000' +  CONVERT(nvarchar, IDENT_CURRENT('model.Patients')) + '-000' + CONVERT(nvarchar, IDENT_CURRENT('Appointments')) + 'C25999',
	'',
	'F',
	'',
	'',
	'',
	'',
	'00000000000000000000000000000000',
	'F',
	'T'

-- 3rd ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-------Patient #6 Test Paper Claim
--ADD APPOINTMENT PATIENT #6
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110204,
	700, 10, IDENT_CURRENT('PatientReferral'), 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110204
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PracticeName pn ON LocationReference = '' AND pn.PracticeType = 'P'
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #6
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110204, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #6 Paper Claim Test
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #6 Paper Claim Test - primary (aetna)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '100'
	,'B'
	,pdPatient.PatientId
	,pdInsured.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110204
	,'150.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'150.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdInsured ON pdInsured.LastName = 'AETNA' and pdInsured.FirstName = 'ONLY'
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'AETNASPOUSEPRIMARY' and pdPatient.FirstName = 'CIGNASELFSECONDARY'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #6 Paper Claim Test (AETNA PRIMARY CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20110204
	,654
	,'I'
	,'//Amt/150'
	,'B'
	)


--1st service Patient #6 Paper Claim Test
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '', '1', '150.00', '01', '11'
		,20110204
        , '1', 'A', 'F', 'F', '150', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #6
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,ServiceDate
	,Charge
	,'1'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- PatientReceivablePayments   #6 Paper Claim Test
-- payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,40
	,N'20110304'
	,N'57689030405430'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,0
	,N''
	,N'20110227'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--contractual writeoff for   #6 Paper Claim Test
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'X'
	,10
	,N'20110304'
	,N'57689030405430'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,0
	,N''
	,N'20110227'
	FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- PatientReceivables Patient #6 Paper Claim Test - secondary
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '100'
	,'B'
	,pdPatient.PatientId
	,pdPatient.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110204
	,'150.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'150.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pdPatient on PdPatient.LastName = 'AETNASPOUSEPRIMARY' and pdPatient.FirstName = 'CIGNASELFSECONDARY'
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


--	SECONDARY (CIGNA) BILLING TRANSACTION Patient #6 Paper Claim Test
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110404
	,654
	,'I'
	,'//Amt/100'
	,'P'
	)


-- 2dy ServiceTransaction Patient #6 Paper Claim Test
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110404
	,100
	,'P'
	,'2012-02-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--------End Patient #6 Test Paper Claim

--patient #7 MedicarePri/CignaSec
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICAREPRI'
	,'CIGNASEC'
	,'W'
	,''
	,''
	,'85 LIME STREET'
	,''
	,'BOSTON'
	,'MA'
	,'10022'
	,'6173335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'M'
	,'19450905'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
	)

--PatientFinancial #7 MEDICARE PRIMARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '866663333A', ''
, 0, 1, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--PatientFinancial #7 CIGNA  SECONDARY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGNAPOLICY2', ''
, 0, 2, '20110101', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'

DECLARE @nextPatientId int
SET @nextPatientId = IDENT_CURRENT('model.Patients') + 1

--PATIENT #8 /Test #7 BlueShield Self, Medicare/Cigna Spouse 
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty]
	,[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'BLUESHIELDSELF'
	,'CIGNASPOUSE'
	,'WILLIAM'
	,''
	,''
	,'85 LIME STREET','','BOSTON','MA','10022'
	,'6173335544'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19441123',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,@nextPatientId,'Y','N'
	,PatientId,'S','N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'MEDICAREPRI' AND FirstName = 'CIGNASEC'

--PATIENT #8 /Test #7  BLUESHIELD SELF/CIGNA SPOUSE
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'YLK9663333', 'BLUEGROUP444'
, 0, 1, '20110302', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'BLUE SHIELD'



--ADD APPOINTMENT #1 PATIENT #8 /Test #7 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20110302,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
WHERE ResourceType = 'D' and ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #8 /Test #7 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110302, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT #8 /Test #7 

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables PATIENT #8 /Test #7 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '500.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110302
	,'500.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'500.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS PATIENT #8 /Test #7 
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110302
	,654
	,'I'
	,'//Amt/500'
	,'B'
	)


--1st service PATIENT #8 /Test #7 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '', '1', '225.00', '01', '11'
		,20110302
        , '12', 'A', 'F', 'F', '132.56', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction PATIENT #8 /Test #7 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--2nd Service PATIENT #8 /Test #7 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'RT', '1', '175.00', '02', '11'
		,20110302
        , '2', 'A', 'T', 'F', '89.42', '2', 0,
convert(varchar(8),getdate(),112), 'FLUORESCEIN')


-- 2nd ServiceTransaction PATIENT #8 /Test #7 
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


----PATIENT #9 VISIONPLAN ONLY USED FOR THE TEST #8, next
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'VISIONPLAN'
	,'ONLY'
	,''
	,''
	,''
	,'788 CRANBERRY STREET','','BURLINGTON','MA','01805'
	,'6173335544'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'M'
	,'19471015'
	,''
	,'ENGLISH'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'N'
	,'Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
	)

--PatientFinancial #9 VISIONPLAN ONLY
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'VIZ12345', ''
, 0, 1, '20050101', '', 'C', 'V'
FROM PracticeInsurers where InsurerName = 'VSP'

SET @nextPatientId = IDENT_CURRENT('model.Patients') + 1

----PATIENT #10/Test #8 BlueShield self/Vision spouse
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty]
	,[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'BLUESHIELDSELF'
	,'VISIONPLANSPOUSE'
	,''
	,''
	,''
	,'788 CRANBERRY STREET','','BURLINGTON','MA','01803'
	,'6173335544'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19480116',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,@nextPatientId,'Y','N'
	,PatientId,'S','N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--PatientFinancial PATIENT #10/Test #8 BLUESHIELD SELF/Vision SPOUSE
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'YLK9663333', ''
, 0, 1, '20050302', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'BLUE SHIELD'



--ADD APPOINTMENT #1 PATIENT #10/Test #8 
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20101225,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	sl.ResourceId, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
INNER JOIN Resources sl on sl.ResourceType = 'R' and sl.ResourceName = 'BROOK'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'Reiss'

--ADD ACTIVITY RECORD PATIENT #10/Test #8 
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20101225, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT #10/Test #8 

INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '2', '362.04', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.15', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables PATIENT #10/Test #8 
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1200.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20101225
	,'1200.00'
	,ResourceId
	,'A'
	,'CT'
	,'12/24/2010'
	,'12/31/2010'
	,''
	,''
	,''
	,'12/25/2010'
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'1200.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
INNER JOIN Resources re ON ResourceType = 'D' 
	AND ResourceLastName = 'Reiss'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS PATIENT #10/Test #8 
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20101225
	,654
	,'I'
	,'//Amt/1200'
	,'B'
	)


--1st service Patient #9
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'67036', 'RT', '1', '1200.00', '02', '21'
		,20110302
        , '12', 'A', 'F', 'F', '1032.56', '1', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #9
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120121
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-------Patient #11/Test 9 CignaPrimary/MedicarePrimaryArchived/AetnaSecondary--------
INSERT INTO [PatientDemographics] (
	[LastName],[FirstName],[MiddleInitial],[NameReference],[SocialSecurity],[Address],[Suite],[City],[State],[Zip]
	,[HomePhone],[CellPhone],[Email],[EmergencyName],[EmergencyPhone],[EmergencyRel],[Occupation]
	,[BusinessName],[BusinessAddress],[BusinessSuite],[BusinessCity],[BusinessState],[BusinessZip],[BusinessPhone],[BusinessType]
	,[Gender],[Marital],[BirthDate],[NationalOrigin],[Language],[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType],[PolicyPatientId],[Relationship],[BillParty]
	,[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty],[ReferralCatagory]
	,[BulkMailing],[FinancialAssignment],[FinancialSignature],[FinancialSignatureDate],[Status]
	,[Salutation],[Hipaa],[PatType],[BillingOffice],[SendStatements],[SendConsults],[OldPatient]
	,[ReferralRequired],[MedicareSecondary],[PostOpExpireDate],[PostOpService],[EmployerPhone]
	,[Religion],[Race],[Ethnicity]
	)
SELECT
	'CIGNAPRIMARY'
	,'MEDICAREARCHIVED'
	,'AETNASECONDARY'
	,''
	,''
	,'55669 ORANGE STREET','','GLOUCESTER','MA','01803'
	,'6173856644'
	,'','','','','','',''
	,'','','','','','',''
	,'M','M','19390123',''
	,'ENGLISH','0'
	,'0'
	,'0'
	,'','',''
	,IDENT_CURRENT('model.Patients'),'S','N'
	,PatientId,'S','N'
	,''
	,'Y','Y','Y'
	,'20120101'
	,'A'
	,'MS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'WHITE'
	,'NOT HISP'
FROM PatientDemographics WHERE LastName = 'AETNA' AND FirstName = 'ONLY'

UPDATE PatientDemographics
SET PolicyPatientId = PatientId, Relationship = 'Y'
WHERE LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

--ADD INSURANCE PLAN - MEDICARE ARCHIVED Patient #11/Test 9
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '012336655A', ''
, 0, 1, '20000101', '20100901', 'X', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'

--ADD INSURANCE PLAN - CIGNA CURRENT Patient #11/Test 9
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'CIGPOL123', ''
, 0, 1, '20100902', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'CIGNA'

--ADD INSURANCE PLAN - VSP VISION CURRENT Patient #11/Test 9
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'VSPPOL567', ''
, 0, 1, '20100902', '', 'C', 'V'
FROM PracticeInsurers where InsurerName = 'VSP'


--ADD APPOINTMENT Patient #11/Test 9
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	Apptypeid, 
	20100902,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 
	0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20090302
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
INNER JOIN PracticeName pn ON LocationReference = ''
WHERE ResourceType = 'D' and ResourceLastName = 'Morgenstern'

--ADD ACTIVITY RECORD Patient #11/Test 9
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20100902, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses PATIENT Patient #11/Test 9
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.15', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #11/Test 9 - MEDICARE/ARCHIVED
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTION Patient #11/Test 9 ARCHIVED MEDICARE
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20100902
	,654
	,'I'
	,'//Amt/3130'
	,'B'
	)


-- PatientReceivables Patient #11/Test 9 - AETNA 2D
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'AETNA'
INNER JOIN PatientDemographics pd ON LastName = 'AETNA' AND FirstName = 'ONLY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #11/Test 9 AETNA SECOND
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20100902
	,654
	,'I'
	,'//Amt/0'
	,'B'
	)


-- PatientReceivables Patient #11/Test 9  -- CIGNA PRIMARY
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #11/Test 9 CIGNA PRIMARY
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20100903
	,654
	,'I'
	,'//Amt/1130'
	,'B'
	)


-- PatientReceivables Patient #11/Test 9 - vsp vision primary for drug service
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '3130.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	, 20100902
	,'3130.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'3130.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'VSP'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTION Patient #11/Test 9 vsp primary for drug
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20100903
	,654
	,'I'
	,'//Amt/2000'
	,'B'
	)


--1st service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'99214', '', '1', '225.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '132.56', '3', 0,
convert(varchar(8),getdate(),112), 'OFFICE VISIT')


-- 1st ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 1st Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'X'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 1st Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2nd service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn]
		  ,[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'J2778', '', '5', '1800.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '1312.88', '1', 0,
convert(varchar(8),getdate(),112), 'LUCENTIS')


-- 2ND Service 1st ServiceTransaction Patient #11/Test 9 (medicare archived)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- 2nd Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'X'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName =  'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- 2d Service 3rd ServiceTransaction Patient #11/Test 9  (VSP pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'VSP'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- 3RD service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'67028', 'RT', '1', '725.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '598.24', '2', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')



-- 3rd Service #1 ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 3rd Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'X'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 3rd Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE [Service] = '67028' 
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'



-- 4TH service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'RT', '1', '90.00', '01', '11'
		,20100902
        , '1', 'A', 'T', 'F', '89.5', '4', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')

-- 4th ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 4th Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'X'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 4th Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'RT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'


-- 5TH service Patient #11/Test 9 
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES    ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92235', 'LT', '1', '90.00', '01', '11'
		,20100902
        , '1', 'A', 'F', 'F', '89.5', '5', 0,
convert(varchar(8),getdate(),112), 'INTRAVIT INJ')

-- 5th ServiceTransaction Patient #11/Test 9  (archived medicare)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'0'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'MEDICARE'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 5th Service 2d ServiceTransaction Patient #11/Test 9  (Aetna cross over)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100902
	,prs.charge * prs.Quantity
	,'X'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'AETNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'

-- 5th Service 3d ServiceTransaction Patient #11/Test 9  (Cigna pri)
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20100903
	,prs.charge * prs.Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices  prs
INNER JOIN PatientReceivables pr ON pr.Invoice = prs.Invoice
INNER JOIN PatientDemographics pd on pd.PatientId = pr.PatientId
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId
	AND InsurerName = 'CIGNA'
INNER JOIN PracticeTransactionJournal ptj ON pr.ReceivableId = ptj.TransactionTypeId
	AND TransactionType = 'R'
WHERE  [Service] = '92235' 
	AND Modifier = 'LT'
	AND LastName = 'CIGNAPRIMARY' and FirstName = 'MEDICAREARCHIVED'


-----patient #12 MedicareReportPatientPaid Test #11
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
VALUES (
	'MEDICARE'
	,'REPORTPATIENTPAID'
	,'B'
	,''
	,''
	,'345 GROVE STREET'
	,''
	,'ALPINE'
	,'NY'
	,'10022'
	,'2129935544'
	,'7183269955'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'S'
	,'19660503'
	,''
	,'KOREAN'
	,''
	,''
	,''
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'Y'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
	)

--PatientFinancial #12 MEDICARE ONLY  Test #11
--ADD INSURANCE PLAN
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, 'A223999888', ''
, 25, 1, '20100501', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE RAILROAD'

--ADD APPOINTMENT #1 PATIENT #12  Test #11
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110616,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'REISS'

--ADD ACTIVITY RECORD #1 PATIENT #12  Test #11
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110616, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add diagnoses
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '3', '362.52', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables #1 Patient #12  Test #11
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '0'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110616
	,'350'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'350'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE RAILROAD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #12  Test #11  1ST appt

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110616
	,654
	,'I'
	,'//Amt/0'
	,'B'
	)


--SERVICES Patient #12  Test #11  1ST appt
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92014', '', '1', '250.00', '01', '11'
		,20110616
        , '12', 'A', 'F', 'F', '250', '1', 0
		,convert(varchar(8),getdate(),112), 'OFFICE VIZ')


--payment from Patient #12 1ST APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,250
	,N'20110616'
	,N'887766'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110616'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


-- 1st ServiceTransactions   Patient #12, 1ST appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110616
	,0
	,'B'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2ND service   Patient #12, 1ST appt
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92015', '', '1', '100.00', '01', '11'
		,20110616
        , '', 'A', 'F', 'F', '100', '2', 0
		,convert(varchar(8),getdate(),112), 'REFRACT')


--payment from Patient #12 1ST APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,100
	,N'20110616'
	,N'887766'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110616'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 2ND ServiceTransactions  Patient #12, 1ST appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110616
	,0
	,'B'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--ADD APPOINTMENT #2 PATIENT #12 - Test #11  SMALL BALANCE AFTER PATIENT PAID
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20110214,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'REISS'

--ADD ACTIVITY RECORD #1 PATIENT #12 2nd appt
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20110214, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add diagnoses  Patient #12, Test #11 2nd appt
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'K', '', '1', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #12  Test #11  2nd VISIT
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '0.05'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110214
	,'350'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'350'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE RAILROAD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- 2nd appt BILLING TRANSACTIONS Patient #12

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110214
	,654
	,'I'
	,'//Amt/0.05'
	,'B'
	)


--SERVICES 2nd appt Patient #12
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92014', '', '1', '250.03', '01', '11'
		,20110214
        , '12', 'A', 'F', 'F', '250.03', '1', 0
		,convert(varchar(8),getdate(),112), 'OFFICE VIS')

--payment from Patient #12 2ND APPT 1ST SVC
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,250
	,N'20110214'
	,N'554433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110214'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 1st ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110214
	,0.03
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '92014' and ServiceDate = 20110214

--2ND service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate]
		  ,[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
        ,'92015', '', '1', '100.02', '01', '11'
		,20110214
        , '', 'A', 'F', 'F', '100.02', '2', 0
		,convert(varchar(8),getdate(),112), 'REFRACTION')

--payment 2nd service  Patient #12, 2nd appt
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,IDENT_CURRENT('model.Patients')
	,N'P'
	,N'P'
	,100
	,N'20110214'
	,N'554433'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'1'
	,0
	,N''
	,N'20110214'
FROM PatientReceivableServices 
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- 2ND ServiceTransactions Patient #12, 2nd appt
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110214
	,0.02
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE Service = '92015' and ServiceDate = 20110214



---- Patient #5 Test #12 MedicareMedicaid - Medicaid Secondary Resubmit disability
--ADD APPOINTMENT PATIENT #5, test #12
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	Apptypeid, 
	20111115,
	900, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 
	0, 0, 0, 0, 0, 0, 'M',  '', 
	20111207
FROM Resources dr
INNER JOIN PatientDemographics ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN AppointmentType ON AppointmentType = 'LONG'
WHERE ResourceType = 'D' and ResourceLastName = 'REISS'



--ADD ACTIVITY RECORD PATIENT #5/test #12
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20111115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'MEDICARE' AND FirstName = 'MEDICAID'


--Add Diagnoses Patient #5/test #12
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '362.04', '', ''
, 'A', '', '', '', '', 0, '', '')


-- PatientReceivables  Patient #5/test #12  (Medicare primary)
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '24.55'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( ap.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'100.00'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #5/Test #12
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20111115
	,654
	,'I'
	,'//Amt/100'
	,'B'
	)



-- PatientReceivables Patient #5 for Medicaid Secondary
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '24.55'
	,'B'
	,ap.PatientId
	,ap.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( ap.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111115
	,'100.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,'P'
	,'P'
	,'11/10/2011'
	,''
	,''
	,'A'
	,0
	,0
	,'ICN NUMBER'
	,'3'
	,0
	,''
	,''
	,'100.00'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #5 MEDICAID SECONDARY (CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20111215
	,654
	,'I'
	,'//Amt/12.55'
	,'B'
	)
	
--1st service Patient #5 Test #12
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(PatientId as nvarchar (5))
			+ '-000' + cast(AppointmentID as nvarchar (4))
        ,'99203', '', '1', '100.00', '01', '11'
		,20111115
        , '12', 'A', 'F', 'F', '75', '1', 0,
		convert(varchar(8),getdate(),112), 'OFFICE VIS'
FROM Appointments where AppointmentId = IDENT_CURRENT('Appointments')

-- 1st ServiceTransaction Patient #5/Test #12 TO MEDICARE PRIMARY
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId 
	,ItemId
	,20111115
	,100
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr on pr.Invoice = prs.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND InsurerName = 'MEDICARE'

-- PatientReceivablePayments  patient #5 Test #12
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,5125
	,N'20111215'
	,N'7892'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111207'
FROM PracticeInsurers WHERE InsurerName = 'MEDICARE'

SET @BatchId = (SELECT dbo.GetIdentCurrent('dbo.PatientReceivablePayments'))
-- payment for Patient #5/Test #12
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')-1
	,InsurerId
	,N'I'
	,N'P'
	,50
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--contractual writeoff for Patient #5
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')-1
	,InsurerId
	,N'I'
	,N'X'
	,25
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--coinsurance for Patient #5/Test #12 from Medicare service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')-1
	,InsurerId
	,N'I'
	,N'|'
	,24.55
	,N'20111215'
	,N'7892'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,@BatchId
	,N''
	,N'20111207'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- 2ND ServiceTransaction Patient #5/Test #12 TO MEDICAID SECONDARY [CLOSED]
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	TransactionId
	,ItemId
	,20111215
	,20
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PracticeInsurers pri ON pri.InsurerId = pr.InsurerId
INNER JOIN PracticeTransactionJournal ptj ON ptj.TransactionTypeId = pr.ReceivableId
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND InsurerName = 'MEDICAID'
AND ptj.TransactionRemark = '//Amt/12.55'


-- PAYMENT FROM MEDICAID  Patient #5/Test #12 from Medicare service #1
--Batch
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,20
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--payment
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	dbo.GetIdentCurrent('dbo.PatientReceivables')
	,InsurerId
	,N'I'
	,N'P'
	,20
	,N'20111230'
	,N'33BB'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,PaymentId
	,N''
	,N'20111221'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICAID'
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PatientReceivablePayments ON PaymentAmount = 20 
	AND PaymentCheck = '33BB'
	AND PaymentType = '='
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

-- ANOTHER PAYMENT FROM MEDICARE - Patient #5/Test #12
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)

SELECT top 1
	pr.ReceivableId
	,pri.InsurerId
	,N'I'
	,N'P'
	,.45
	,N'20120105'
	,N'care02'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,0
	,N''
	,N'20120101'
FROM PatientReceivableServices 
INNER JOIN PracticeInsurers pri ON InsurerName = 'MEDICARE'
INNER JOIN PatientDemographics pd ON LastName = 'MEDICARE' AND FirstName = 'MEDICAID'
INNER JOIN PatientReceivables pr ON pr.InsurerId = pri.InsurerId
	AND InvoiceDate = 20111115
INNER JOIN Resources ON ResourceType <> 'R'
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- SECOND BILLING TRANSACTIONS Patient #5 MEDICAID SECONDARY - RESUBMIT
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20120105
	,654
	,'I'
	,'//Amt/24.55'
	,'B'
	)
	
-- SERVICE TRANSACTION #3 TO MEDICAID -- RESUBMIT
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120105
	,4.55
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


----Patient #13 THREE INSURERS, override, MEDICARE SECONDARY
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor],[PrimaryCarePhysician],[ReferringPhysician]
	,[ProfileComment1],[ProfileComment2],[ContactType]
	,[PolicyPatientId],[Relationship],[BillParty]
	,[SecondPolicyPatientId],[SecondRelationship],[SecondBillParty]
	,[ReferralCatagory],[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'INSURERS'
	,'THREE'
	,''
	,''
	,''
	,'4556 BROADWAY'
	,'APT 26'
	,'NEW YORK'
	,'NY'
	,'10024'
	,'2123444488'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'M'
	,'S'
	,'19800731'
	,''
	,'RUSSIAN'
	,'0'
	,'0'
	,'0'
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'S'
	,'N'
	,PatientId
	,'S'
	,'N'
	,''
	,'Y'
	,'N'
	,'N'
	,'20110501'
	,'A'
	,'MR'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,'12'
	,0
	,0
	,''
	,''
	,'BLACK'
	,'NOT HISP'
FROM PatientDemographics
WHERE LastName = 'AETNA' AND FirstName = 'ONLY'

UPDATE PatientDemographics
SET PolicyPatientId = PatientId, Relationship = 'Y'
WHERE LastName = 'INSURERS' AND FirstName = 'THREE'

--PatientFinancial 1st Test  Patient #13 THREE INSURERS
--ADD INSURANCE PLAN (MEDICARE SECONDARY)
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, '111223333A', ''
, 0, 2, '20111201', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'MEDICARE'


--ADD INSURANCE PLAN (SECURE HORIZONS PRIMARY)
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerId, 'SECURE789', ''
, 25, 1, '20110301', '', 'C', 'M'
FROM PracticeInsurers where InsurerName = 'SECURE HORIZONS'



--ADD APPOINTMENT PATIENT #13 , 1st Test  THREE INSURERS
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111027,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111007
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'

--ADD ACTIVITY RECORD PATIENT #13, 1st Test   THREE INSURERS
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111027, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #13, Test #13  THREE INSURERS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #13, 1st Test   THREE INSURERS, SECURE HORIZONS
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '225.00'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111027
	,'225.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'225'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'SECURE HORIZONS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #13 THREE INSURERS
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20111027
	,654
	,'I'
	,'//Amt/225'
	,'B'
	)


--SERVICES  patient #13, tEST #13 THREE INSURERS
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92014', '', '1', '125.00', '02', '11',
			20111027, '', 'A', 'F', 'F', '125.00', '1', 0,
			20111027, 'OFFICE VISIT')

--1ST service ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111027
	,100
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2nd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92083', '', '1', '100.00', '02', '11',
			20111027, '', 'A', 'T', 'F', '100.00', '2', 0,
			20111027, 'VISUAL FIELD')

--2nd service ServiceTransactions
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111027
	,125
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')





---Patient 13 THREE INSURERS 2nd appt MEDICARE SECONDARY
--ADD APPOINTMENT PATIENT #13 , 2nd test THREE INSURERS
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT IDENT_CURRENT('model.Patients'),
	AppTypeId, 
	20111230,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111230
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
WHERE ResourceType = 'D' and ResourceLastName = 'Morgenstern'

--ADD ACTIVITY RECORD PATIENT #13,  THREE INSURERS
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
VALUES (IDENT_CURRENT('model.Patients'), 0, 20111230, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:05 AM', 3, 'N', 0, 0, '', '', '', 938)

--Add Diagnoses Patient #13, Test #1  THREE INSURERS
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', '')

-- PatientReceivables Patient #13, Test #1  THREE INSURERS, SECURE HORIZONS
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '16.44'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111230
	,'85.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'SECURE HORIZONS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #13 THREE INSURERS (SECURE HORIZONS, CLOSED)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'Z'
	,20111230
	,800
	,'I'
	,'//Amt/85'
	,'B'
	)


--SERVICES  patient #13, THREE INSURERS
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'92250', '', '1', '85.00', '02', '11',
			20111230, '', 'A', 'T', 'F', '85.00', '1', 0,
			20111230, 'FUNDUS PHOTOS')

--1ST service transaction
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20111230
	,85
	,'1'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



-- PatientReceivablePayments  patient #13 2d test
--batch file
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	0
	,InsurerId
	,N'I'
	,N'='
	,56.33
	,N'20120105'
	,N'8899'
	,N'0'
	,N''
	,N'A'
	,''
	,N'F'
	,N''
	,0
	,0
	,N''
	,0
	,N''
	,N''
FROM PracticeInsurers WHERE InsurerName = 'SECURE HORIZONS'

-- payment for Patient #13 2d test from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'P'
	,24.34
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N''
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230


--contractual writeoff for Patient #13/Test #14 from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'X'
	,44.22
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N'C'
	,ItemId
	,ResourceId
	,N'45'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230

--coinsurance for Patient #13/Test #14 from Secure Horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'|'
	,12.16
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230


--deductible for Patient #13/2nd test from secure horizons service #1
INSERT [dbo].[PatientReceivablePayments] (
	[ReceivableId]
	,[PayerId]
	,[PayerType]
	,[PaymentType]
	,[PaymentAmount]
	,[PaymentDate]
	,[PaymentCheck]
	,[PaymentRefId]
	,[PaymentRefType]
	,[Status]
	,[PaymentService]
	,[PaymentCommentOn]
	,[PaymentFinancialType]
	,[PaymentServiceItem]
	,[PaymentAssignBy]
	,[PaymentReason]
	,[PaymentBatchCheckId]
	,[PaymentAppealNumber]
	,[PaymentEOBDate]
	)
SELECT top 1
	pr.ReceivableId
	,pr.InsurerId
	,N'I'
	,N'!'
	,4.28
	,N'20120105'
	,N'8899'
	,N'0'
	,N'V'
	,N'A'
	,Service
	,N'F'
	,N''
	,ItemId
	,ResourceId
	,N'2'
	,prp.PaymentId
	,N''
	,N'20120105'
FROM PatientReceivableServices prs
INNER JOIN PatientReceivables pr ON prs.Invoice = pr.Invoice
INNER JOIN PatientDemographics pd ON pd.PatientId = pr.PatientId
INNER JOIN Resources ON ResourceType <> 'R'
INNER JOIN PracticeInsurers pri ON  pri.InsurerId = pr.InsurerId AND InsurerName = 'SECURE HORIZONS'
INNER JOIN PatientReceivablePayments prp ON prp.PaymentType = '=' AND prp.PaymentAmount = 56.33
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')
AND LastName = 'INSURERS' AND FirstName = 'THREE'
And InvoiceDate = 20111230

-- PatientReceivables Patient #13, 2nd appt  THREE INSURERS, MEDICARE SECONDARY
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '16.44'
	,'B'
	,IDENT_CURRENT('model.Patients')
	,IDENT_CURRENT('model.Patients')
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20111230
	,'85'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,0
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'F'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #13 THREE INSURERS (MEDICARE SECONDARY)
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'P'
	,20120110
	,800
	,'I'
	,'//Amt/16.44'
	,'B'
	)


--1ST service transaction Medicare Secondary
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120110
	,16.44
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')





----Test #14 VISION CLAIM - USING PATIENT "ONLY VISIONPLAN"
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20120115,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId AS ResourceId1, pn.PracticeId + 1000 AS ResourceId2, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20111130
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PatientDemographics ON LastName = 'VISIONPLAN' AND FirstName = 'ONLY'
INNER JOIN PracticeName pn ON LocationReference = 'OPTICAL' and PracticeType = 'P'
WHERE ResourceType = 'Q' and ResourceLastName = 'BARTELS'


--ADD ACTIVITY RECORD test #14,  VISION CLAIM
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20120115, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '11:45 AM', 3, 'N', 0, 0, '', '', '', 938 
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--Add Diagnoses  Test #14 VISION CLAIM
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'B', 'OU', '1', 'V43.1', '', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

--Add glasses prescription  Test #14 VISION CLAIM
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
SELECT IDENT_CURRENT('Appointments'), PatientId, 'A', '', '1', 'DISPENSE SPECTACLE RX-9/-1/SINGLE VISION-2/ -5.25 ADD-READING:+1.00-3/ -5.00 ADD-READING:+1.00-4/-5/-6/-7/', '^', '', 'A', '', '', '', '', 0, '', ''
FROM PatientDemographics WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'

-- PatientReceivables Test #14 VISION CLAIM
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '620'
	,'B'
	,PatientId
	,PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast( IDENT_CURRENT('model.Patients') AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20120115
	,'620.00'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'620'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'VSP'
INNER JOIN PracticeVendors ON VendorLastName = 'BARTELS'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId



--Test #14 Vision Claim Add Glasses Order
INSERT INTO CLOrders
           ([InventoryId]
           ,[PatientId]
           ,[AppointmentId]
           ,[ShipTo]
           ,[ShipAddress1]
           ,[ShipAddress2]
           ,[ShipCity]
           ,[ShipState]
           ,[ShipZip]
           ,[Eye]
           ,[Qty]
           ,[Cost]
           ,[OrderDate]
           ,[OrderComplete]
           ,[Status]
           ,[OrderType]
           ,[ReceivableId]
           ,[Reference]
           ,[PONumber]
           ,[AlternateReference]
           ,[Rx])
SELECT IDENT_CURRENT('PCInventory'),PatientId, IDENT_CURRENT('Appointments'), 'O', '&ODC:V2750F,&ODM:V2784&ODP:V2715A&ODB:V2781AG&ODT:PROG&', '&OSC:V2750F,&OSM:V2784&OSP:V2715A&OSB:V2781AG&OST:PROG&'
	, 'OD:33.0&&18&OS:33.5&&18&' , '10', '&01&&&&54&17&130&', 'OU', '1', 500, 20120115, '', 10, 'G', dbo.GetIdentCurrent('dbo.PatientReceivables'), '', '', '', ''
FROM PatientDemographics  WHERE LastName = 'VISIONPLAN' AND FirstName = 'ONLY'


-- BILLING TRANSACTIONS TEST #14 VISION CLAIM
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120115
	,790
	,'I'
	,'//Amt/620'
	,'B'
	)


--SERVICES  TEST #14 VISION CLAIM
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2020', '', '1', '250.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '250.00', '1', 0,
			20120115, 'FRAMES')

--1ST service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,250
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--2D service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2781AG', 'RTLT', '2', '75.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '150.00', '2', 0,
			20120115, 'THREE RIVERS')

--2D service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,150
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--3rd service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2750F', 'RTLT', '2', '25.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '50.00', '4', 0,
			20120115, 'ANTIREFLECT COAT')

--3D service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,50
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


--4th service DELETED
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2715A', 'RT', '1', '65.00', '12', '12',
			20120115, '', 'X', 'F', 'F', '65.00', '4', 0,
			20120115, 'TRANSITIONS BR')


--4th service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2715A', 'RTLT', '2', '65.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '130.00', '3', 0,
			20120115, 'TRANSITIONS BR')


-- 4TH service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,130
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

--5TH SERVICE
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status]
		  ,[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
VALUES     ('000' + cast(IDENT_CURRENT('model.Patients') as nvarchar (5))
				+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'V2784', '', '1', '40.00', '12', '12',
			20120115, '', 'A', 'F', 'F', '40.00', '5', 0,
			20120115, 'High Index 1.6')

--5TH service transaction  TEST #14 VISION CLAIM
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120115
	,40
	,'P'
	,'2012-01-15 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')


------Patient #14 - Cigna Pri, Medicare Sec
INSERT INTO [PatientDemographics] (
	[LastName]
	,[FirstName]
	,[MiddleInitial]
	,[NameReference]
	,[SocialSecurity]
	,[Address]
	,[Suite]
	,[City]
	,[State]
	,[Zip]
	,[HomePhone]
	,[CellPhone]
	,[Email]
	,[EmergencyName]
	,[EmergencyPhone]
	,[EmergencyRel]
	,[Occupation]
	,[BusinessName]
	,[BusinessAddress]
	,[BusinessSuite]
	,[BusinessCity]
	,[BusinessState]
	,[BusinessZip]
	,[BusinessPhone]
	,[BusinessType]
	,[Gender]
	,[Marital]
	,[BirthDate]
	,[NationalOrigin]
	,[Language]
	,[SchedulePrimaryDoctor]
	,[PrimaryCarePhysician]
	,[ReferringPhysician]
	,[ProfileComment1]
	,[ProfileComment2]
	,[ContactType]
	,[PolicyPatientId]
	,[Relationship]
	,[BillParty]
	,[SecondPolicyPatientId]
	,[SecondRelationship]
	,[SecondBillParty]
	,[ReferralCatagory]
	,[BulkMailing]
	,[FinancialAssignment]
	,[FinancialSignature]
	,[FinancialSignatureDate]
	,[Status]
	,[Salutation]
	,[Hipaa]
	,[PatType]
	,[BillingOffice]
	,[SendStatements]
	,[SendConsults]
	,[OldPatient]
	,[ReferralRequired]
	,[MedicareSecondary]
	,[PostOpExpireDate]
	,[PostOpService]
	,[EmployerPhone]
	,[Religion]
	,[Race]
	,[Ethnicity]
	)
SELECT
	'CIGNAPRIMARY'
	,'MEDICARESECONDARY'
	,''
	,''
	,''
	,'98-15 EAST 23RD ROAD'
	,'GARDEN LEVEL'
	,'NEW YORK'
	,'NY'
	,'10022'
	,'7186754433'
	,'7186669932'
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'F'
	,'M'
	,'19230819'
	,''
	,'ENGLISH'
	,''
	,''
	,VendorId
	,''
	,''
	,''
	,IDENT_CURRENT('model.Patients')
	,'S'
	,'N'
	,0
	,''
	,''
	,''
	,'Y'
	,'Y'
	,'Y'
	,'20120101'
	,'A'
	,'MRS'
	,'T'
	,'E'
	,'0'
	,'Y'
	,'Y'
	,''
	,'N'
	,''
	,0
	,0
	,''
	,''
	,'ASIAN'
	,'NOT HISP'
FROM PracticeVendors WHERE VendorLastName = 'REFERRAL' AND VendorFirstName = 'SELF'

UPDATE PatientDemographics SET PolicyPatientId = PatientId, Relationship = 'Y'
WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'


--PatientFinancial #14 CIGNA PRI MEDICARE SEC
--ADD INSURANCE PLAN #1
INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT PatientId, InsurerID, 'CIGNAPRI567', ''
, 25, 1, '20110501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'CIGNA'

INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, '666999888A', ''
, 25, 2, '20110501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'MEDICARE'

INSERT INTO PatientFinancial
(PatientId, FinancialInsurerId, FinancialPerson, FinancialGroupId, FinancialCopay, FinancialPIndicator
, FinancialStartDate, FinancialEndDate, Status, FinancialInsType)
SELECT IDENT_CURRENT('model.Patients'), InsurerID, 'bakersplan', ''
, 10, 3, '20110501', '', 'C', 'M'
FROM PracticeInsurers 
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE InsurerName = 'LOCAL 104'

--ADD APPOINTMENT PATIENT #14
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110819,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, 0, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20120120
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'BRF'
INNER JOIN PatientDemographics ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE dr.ResourceType = 'D' and dr.ResourceLastName = 'SOO LIN'

--ADD ACTIVITY RECORD PATIENT #14
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110819, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'

--ADD Diagnosis patient #14
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '1', '366.16.41', '', '', 'A', '', '', '', '', 0, '', '')


--patient #14
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '150'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110819
	,'6385'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,VendorId
	,0
	,''
	,''
	,0
	,''
	,''
	,'85'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PracticeInsurers ON InsurerName = 'CIGNA'
INNER JOIN PracticeVendors ON VendorLastName = 'REFERRAL' AND VendorFirstName = 'SELF'
INNER JOIN PatientDemographics pd ON LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS Patient #14

INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20120403
	,654
	,'I'
	,'//Amt/250.00'
	,'B'
	)


--SERVICES Patient #14
--1st service
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(pd.PatientId as nvarchar (5))
+ '-000' + cast(AppointmentId as nvarchar (4))
        ,'67028', 'LT', '1', '250.00', '04', '11'
		,20110819
        , '1', 'A', 'F', 'F', '85', '1', 0,
		convert(varchar(8),getdate(),112), 'INTRAVITREAL I'
FROM PatientDemographics pd
INNER JOIN Appointments ap ON ap.PatientId = pd.PatientId AND AppDate = 20110819
WHERE LastName = 'CIGNAPRIMARY' AND FirstName = 'MEDICARESECONDARY'


-- 1st ServiceTransactions #14
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20120403
	,Charge
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



----#17 ASC CLAIMS FACILITY CLAIMS
-------INSTITUTIONAL------
--Surgery Appointment WITH DOCTOR for Patient #2 (THIS WILL BE THE ENCOUNTERID)
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD PATIENT #2  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110504, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'



--Diagnoses for Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2  Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(pd.PatientId as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'66982', 'LT', '1', '1000.00', '02', '24',
			20110504, '123', 'A', 'F', 'F', '665.87', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Patient #2  Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OS', '3', '250.50', '', '', 'A', '', '', '', '', 0, '', '')


INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OU', '2', '362.02', '', '', 'A', '', '', '', '', 0, '', '')


-- PatientReceivables Patient #2 Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'
INNER JOIN PracticeInsurers ON InsurerName = 'BLUE SHIELD'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  patient #2
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(pd.PatientId as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'66982', 'LT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '665.87', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'BLUESHIELD' AND pd.FirstName = 'AETNA' AND pd.MiddleInitial = 'SPOUSE'

-- 1st ServiceTransaction  patient #2  Surgery Claim for Facility
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')






-----------FACILITY CLAIM PROFESSIONAL - MEDICARE
---DOCTOR'S CLAIM
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	600, 10, 0, 0, 
	dr.ResourceId, 'D', 'D', '',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'


--ADD ACTIVITY RECORD PATIENT #1  Surgery Claim for Doctor
INSERT INTO [PracticeActivity]
([PatientId],[StationId],[ActivityDate],[AppointmentId],[QuestionSet],[Status],[ActivityStatusTime],[ResourceId],[TechConfirmed],[CurrentRId],[LocationId],[PayAmount],[PayMethod],[PayReference],[ActivityStatusTRef])
SELECT PatientId, 0, 20110504, IDENT_CURRENT('Appointments'), 'No Questions', 'D', '10:15 AM', 3, 'N', 0, 0, '', '', '', 938
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 



--Diagnoses for Patient #1  Surgery Claim for Doctor
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #1  Surgery Claim for Doctor
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Doctor
--1st service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(pd.PatientId as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '660.22', '1', 0,
			20110506, 'CATARACT SX'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')



--Surgery appt with facility - use doctor appt as encounterid - this appt is not currently modeled in New IO.
INSERT INTO Appointments
(PatientId, AppTypeId, AppDate, AppTime, Duration, ReferralId,
PreCertId, TechApptTypeId, ScheduleStatus, ActivityStatus, Comments,
ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5,
ResourceId6, ResourceId7, ResourceId8, ApptInsType, ConfirmStatus,
SetDate)
SELECT PatientId,
	AppTypeId, 
	20110504,
	0, 0, 0, 0, 
	dr.ResourceId, 'D', 'D', 'ASC CLAIM',
	dr.ResourceId, pn.PracticeId + 1000, 0, 0, 0, 0, 0, 0, 'M',  '', 
	20110207
FROM Resources dr
INNER JOIN AppointmentType ON AppointmentType = 'SURGERY'
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeName pn ON pn.LocationReference = 'ASC'
WHERE ResourceType = 'D' and ResourceLastName = 'Batiste'



--Diagnoses for Patient #1  Surgery Claim for Facility
INSERT INTO [PatientClinical]
           ([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom]
           ,[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status]
           ,[DrawFileName],[Highlights],[FollowUp],[PermanentCondition],[PostOpPeriod],[Surgery],[Activity])
VALUES (IDENT_CURRENT('Appointments'), IDENT_CURRENT('model.Patients'), 'B', 'OD', '1', '366.16', '', '', 'A', '', '', '', '', 0, '', '')



-- PatientReceivables Patient #1 Surgery Claim for Facility
INSERT INTO [PatientReceivables] (
	[OpenBalance]
	,[ReceivableType]
	,[PatientId]
	,[InsuredId]
	,[InsurerId]
	,[AppointmentId]
	,[Invoice]
	,[InvoiceDate]
	,[Charge]
	,[BillToDr]
	,[AccType]
	,[AccState]
	,[HspAdmDate]
	,[HspDisDate]
	,[Disability]
	,[RsnType]
	,[RsnDate]
	,[FirstConsDate]
	,[PrevCond]
	,[Status]
	,[ReferDr]
	,[UnallocatedBalance]
	,[ExternalRefInfo]
	,[Over90]
	,[BillingOffice]
	,[PatientBillDate]
	,[LastPayDate]
	,[ChargesByFee]
	,[InsurerDesignation]
	,[WriteOff]
	)
SELECT top 1 '1000'
	,'B'
	,pd.PatientId
	,pd.PatientId
	,InsurerId
	,IDENT_CURRENT('Appointments')
	,'000' + cast(pd.PatientId AS NVARCHAR) + '-000' + cast(IDENT_CURRENT('Appointments') AS NVARCHAR)
	,20110504
	,'1000'
	,ResourceId1
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,''
	,'A'
	,''
	,0
	,''
	,''
	,0
	,''
	,''
	,'665.87'
	,'T'
	,''
FROM Appointments ap
INNER JOIN PatientDemographics pd ON pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 
INNER JOIN PracticeInsurers ON InsurerName = 'MEDICARE'
WHERE IDENT_CURRENT('Appointments') = ap.AppointmentId


-- BILLING TRANSACTIONS patient #1  Surgery Claim for Facility
INSERT INTO [dbo].[PracticeTransactionJournal] (
	[TransactionType]
	,[TransactionTypeId]
	,[TransactionStatus]
	,[TransactionDate]
	,[TransactionTime]
	,[TransactionRef]
	,[TransactionRemark]
	,[TransactionAction]
	)
VALUES (
	'R'
	,dbo.GetIdentCurrent('dbo.PatientReceivables')
	,'S'
	,20110506
	,654
	,'I'
	,'//Amt/1000'
	,'B'
	)


--SERVICES  Surgery Claim for Facility
--1st service  patient #1
INSERT INTO PatientReceivableServices
          ([Invoice],[Service],[Modifier],[Quantity],[Charge],[TypeOfService],[PlaceOfService],[ServiceDate],[LinkedDiag],[Status],[OrderDoc],[ConsultOn],[FeeCharge],[ItemOrder],[ECode],[PostDate],[ExternalRefInfo])
SELECT '000' + cast(pd.PatientId as nvarchar (5))
+ '-000' + cast(IDENT_CURRENT('Appointments') as nvarchar (4))
          ,'66984', 'RT', '1', '1000.00', '02', '24',
			20110504, '1', 'A', 'F', 'F', '703.96', '1', 0,
			20110506, 'CATARACT SUR'
FROM PatientDemographics pd WHERE pd.LastName = 'MEDICARE' AND pd.FirstName = 'ONLY' 

-- 1st ServiceTransaction  patient #1  Surgery Claim for Doctor
INSERT INTO [dbo].[ServiceTransactions] (
	[TransactionId]
	,[ServiceId]
	,[TransactionDate]
	,[Amount]
	,[TransportAction]
	,[ModTime]
	)
SELECT 
	IDENT_CURRENT('PracticeTransactionJournal') 
	,ItemId
	,20110506
	,Charge*Quantity
	,'P'
	,'2012-01-01 14:00:46.693'
FROM PatientReceivableServices 
WHERE ItemId = IDENT_CURRENT('PatientReceivableServices')

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'InsteadOfInsertIntoPatientFinancial' AND type = 'TR')
 BEGIN
     DROP TRIGGER InsteadOfInsertIntoPatientFinancial
 END
 GO