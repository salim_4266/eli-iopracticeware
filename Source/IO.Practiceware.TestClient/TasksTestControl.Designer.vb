﻿
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TasksTestControl
    Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SetPatientButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.CreateTaskButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ReviewTasksButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.SetResourceButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.PatientNotesButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.AdministrationButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.TaskActivityButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.SuspendLayout()
        '
        'SetPatientButton
        '
        Me.SetPatientButton.Location = New System.Drawing.Point(14, 79)
        Me.SetPatientButton.Margin = New System.Windows.Forms.Padding(0)
        Me.SetPatientButton.Name = "SetPatientButton"
        Me.SetPatientButton.Size = New System.Drawing.Size(128, 55)
        Me.SetPatientButton.TabIndex = 16
        Me.SetPatientButton.Text = "Set Active Patient"
        '
        'CreateTaskButton
        '
        Me.CreateTaskButton.Location = New System.Drawing.Point(170, 12)
        Me.CreateTaskButton.Margin = New System.Windows.Forms.Padding(0)
        Me.CreateTaskButton.Name = "CreateTaskButton"
        Me.CreateTaskButton.Size = New System.Drawing.Size(92, 55)
        Me.CreateTaskButton.TabIndex = 12
        Me.CreateTaskButton.Text = "Create Task"
        '
        'ReviewTasksButton
        '
        Me.ReviewTasksButton.Location = New System.Drawing.Point(284, 13)
        Me.ReviewTasksButton.Margin = New System.Windows.Forms.Padding(0)
        Me.ReviewTasksButton.Name = "ReviewTasksButton"
        Me.ReviewTasksButton.Size = New System.Drawing.Size(92, 54)
        Me.ReviewTasksButton.TabIndex = 13
        Me.ReviewTasksButton.Text = "Review Tasks"
        '
        'SetResourceButton
        '
        Me.SetResourceButton.Location = New System.Drawing.Point(14, 12)
        Me.SetResourceButton.Margin = New System.Windows.Forms.Padding(0)
        Me.SetResourceButton.Name = "SetResourceButton"
        Me.SetResourceButton.Size = New System.Drawing.Size(128, 55)
        Me.SetResourceButton.TabIndex = 14
        Me.SetResourceButton.Text = "Set Current Resource"
        '
        'PatientNotesButton
        '
        Me.PatientNotesButton.Location = New System.Drawing.Point(170, 79)
        Me.PatientNotesButton.Margin = New System.Windows.Forms.Padding(0)
        Me.PatientNotesButton.Name = "PatientNotesButton"
        Me.PatientNotesButton.Size = New System.Drawing.Size(92, 55)
        Me.PatientNotesButton.TabIndex = 15
        Me.PatientNotesButton.Text = "Patient Notes"
        '
        'AdministrationButton
        '
        Me.AdministrationButton.Location = New System.Drawing.Point(284, 80)
        Me.AdministrationButton.Margin = New System.Windows.Forms.Padding(0)
        Me.AdministrationButton.Name = "AdministrationButton"
        Me.AdministrationButton.Size = New System.Drawing.Size(92, 55)
        Me.AdministrationButton.TabIndex = 17
        Me.AdministrationButton.Text = "Administration"
        '
        'TaskActivityButton
        '
        Me.TaskActivityButton.Location = New System.Drawing.Point(389, 13)
        Me.TaskActivityButton.Margin = New System.Windows.Forms.Padding(0)
        Me.TaskActivityButton.Name = "TaskActivityButton"
        Me.TaskActivityButton.Size = New System.Drawing.Size(92, 54)
        Me.TaskActivityButton.TabIndex = 18
        Me.TaskActivityButton.Text = "Task Activity"
        '
        'TasksTestControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.TaskActivityButton)
        Me.Controls.Add(Me.SetPatientButton)
        Me.Controls.Add(Me.CreateTaskButton)
        Me.Controls.Add(Me.ReviewTasksButton)
        Me.Controls.Add(Me.SetResourceButton)
        Me.Controls.Add(Me.PatientNotesButton)
        Me.Controls.Add(Me.AdministrationButton)
        Me.Name = "TasksTestControl"
        Me.Size = New System.Drawing.Size(491, 151)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SetPatientButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents CreateTaskButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ReviewTasksButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents SetResourceButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents PatientNotesButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents AdministrationButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents TaskActivityButton As Soaf.Presentation.Controls.WindowsForms.Button

End Class