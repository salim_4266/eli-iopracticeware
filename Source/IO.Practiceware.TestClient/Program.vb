﻿Imports IO.Practiceware.Interop
Imports IO.Practiceware.Application
Imports IO.Practiceware.Presentation
Imports Soaf.Presentation
Imports IO.Practiceware.Presentation.Views.StandardExam

Public Module Program
    <STAThread()>
    Public Sub Main(ByVal args As String())
        ServiceProvider = Bootstrapper.Run(Of IServiceProvider)()
        PresentationBootstrapper.Run()

        Soaf.Security.PrincipalContext.Current.Scope = Soaf.Security.PrincipalScope.AppDomain

        If args.Any(Function(a) a.ToLower() = "showlogin") Then
            Dim comWrapper As New ComWrapper()
            Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Login.LoginViewManager, IO.Practiceware.Presentation", Nothing).Instance
            manager.ShowLoginScreen()
        Else
            UserContext.Current.Login("9911")
        End If
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = New MainControl(), .ResizeMode = Windows.ResizeMode.CanResize})
        'InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = New StandardExamView(), .ResizeMode = Windows.ResizeMode.CanResize})

        Environment.Exit(0)
    End Sub

    Friend Property ServiceProvider() As IServiceProvider
End Module
