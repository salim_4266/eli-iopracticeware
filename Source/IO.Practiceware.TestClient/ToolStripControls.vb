﻿Imports System.ComponentModel
Imports System.Drawing.Design

Public Class ToolStripCustomTextBox
    Inherits ToolStripControlHost

    Public Sub New()
        MyBase.New(New TextBox())

        TextBoxTextAlign = HorizontalAlignment.Left
        ForeColor = SystemColors.WindowText
    End Sub


    Public ReadOnly Property TextBox() As TextBox
        Get
            Return CType(Control, TextBox)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets a custom string collection to use when the <see cref="P:System.Windows.Forms.ToolStripTextBox.AutoCompleteSource"/> property is set to CustomSource.
    ''' </summary>
    ''' 
    ''' <returns>
    ''' An <see cref="T:System.Windows.Forms.AutoCompleteStringCollection"/> to use with <see cref="P:System.Windows.Forms.TextBox.AutoCompleteSource"/>.
    ''' </returns>
    ''' <filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>
    <Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", GetType(UITypeEditor))> _
    <Browsable(True)> _
    <EditorBrowsable(EditorBrowsableState.Always)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Content)> _
    <Localizable(True)> _
    Public Property AutoCompleteCustomSource() As AutoCompleteStringCollection
        Get
            Return TextBox.AutoCompleteCustomSource
        End Get
        Set(value As AutoCompleteStringCollection)
            TextBox.AutoCompleteCustomSource = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets an option that controls how automatic completion works for the <see cref="T:System.Windows.Forms.ToolStripTextBox"/>.
    ''' </summary>
    ''' 
    ''' <returns>
    ''' One of the <see cref="T:System.Windows.Forms.AutoCompleteMode"/> values. The default is <see cref="F:System.Windows.Forms.AutoCompleteMode.None"/>.
    ''' </returns>
    ''' <filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>    
    <DefaultValue(AutoCompleteMode.None)> _
    <EditorBrowsable(EditorBrowsableState.Always)> _
    <Browsable(True)> _
    Public Property AutoCompleteMode() As AutoCompleteMode
        Get
            Return TextBox.AutoCompleteMode
        End Get
        Set(value As AutoCompleteMode)
            TextBox.AutoCompleteMode = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets a value specifying the source of complete strings used for automatic completion.
    ''' </summary>
    ''' 
    ''' <returns>
    ''' One of the <see cref="T:System.Windows.Forms.AutoCompleteSource"/> values. The default is <see cref="F:System.Windows.Forms.AutoCompleteSource.None"/>.
    ''' </returns>
    ''' <filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>
    <DefaultValue(AutoCompleteSource.None)> _
    <EditorBrowsable(EditorBrowsableState.Always)> _
    <Browsable(True)> _
    Public Property AutoCompleteSource() As AutoCompleteSource
        Get
            Return TextBox.AutoCompleteSource
        End Get
        Set(value As AutoCompleteSource)
            TextBox.AutoCompleteSource = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets how text is aligned in a <see cref="T:System.Windows.Forms.TextBox"/> control.
    ''' </summary>
    ''' 
    ''' <returns>
    ''' One of the <see cref="T:System.Windows.Forms.HorizontalAlignment"/> enumeration values that specifies how text is aligned in the control. The default is <see cref="F:System.Windows.Forms.HorizontalAlignment.Left"/>.
    ''' </returns>
    <DefaultValue(HorizontalAlignment.Left)> _
    <Localizable(True)> _
    Public Property TextBoxTextAlign() As HorizontalAlignment
        Get
            Return TextBox.TextAlign
        End Get
        Set(value As HorizontalAlignment)
            TextBox.TextAlign = value
        End Set
    End Property


    <EditorBrowsable(EditorBrowsableState.Always)> _
   <Browsable(True)>
    Public Property BorderStyle() As BorderStyle
        Get
            Return TextBox.BorderStyle
        End Get
        Set(value As BorderStyle)
            TextBox.BorderStyle = value
        End Set
    End Property

End Class




Public Class ToolStripCustomButton
    Inherits ToolStripControlHost

    Shared ReadOnly _InnerButtonControl As Button = New Button

    Public Sub New()
        MyBase.New(_InnerButtonControl)
    End Sub

    <EditorBrowsable(EditorBrowsableState.Always)> _
    <Browsable(True)>
    Public ReadOnly Property InnerButton() As Button
        Get
            Return CType(Control, Button)
        End Get
    End Property

    <EditorBrowsable(EditorBrowsableState.Always)> _
    <Browsable(True)>
    Public Property ButtonFlatStyle() As FlatStyle
        Get
            Return InnerButton.FlatStyle
        End Get
        Set(value As FlatStyle)
            InnerButton.FlatStyle = value
        End Set
    End Property

    <EditorBrowsable(EditorBrowsableState.Always)> _
    <Browsable(True)>
    Public Property ButtonSize() As System.Drawing.Size
        Get
            Return InnerButton.Size
        End Get
        Set(value As System.Drawing.Size)
            InnerButton.Size = value
        End Set
    End Property

End Class