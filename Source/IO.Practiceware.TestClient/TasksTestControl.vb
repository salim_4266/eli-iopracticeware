﻿Option Strict Off

Imports Soaf.Presentation
Imports ApplicationContext = IO.Practiceware.Application.ApplicationContext

Public Class TasksTestControl

    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Sub SetUserContext()
        Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))

        Dim monitor = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskMonitor", Nothing).Instance
        monitor.Stop()

        Dim resourceIDString = InteractionManager.Current.Prompt("Enter a login number. (dbo.Resources.PId)").Input

        If (Not (resourceIDString Is Nothing)) Then

            Dim resourceID = Integer.Parse(resourceIDString)
            Dim userContext = wrapper.InvokeStaticGet("IO.Practiceware.Application.UserContext", "Current")
            userContext.Login(resourceID.ToString)

            Dim taskMonitor = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskMonitor", Nothing).Instance
            taskMonitor.Start()
        End If

    End Sub

    Sub SetPatient()
        Try
            Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))

            Dim patientID = Integer.Parse(InputBox("Enter a patient ID"))
            Dim applicationContext = Application.ApplicationContext.Current
            applicationContext.PatientID = patientID

        Catch ex As Exception
            InteractionManager.Current.Alert("Invalid patient ID entered.")
        End Try
    End Sub

    Private Sub SetResourceButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles SetResourceButton.Click
        SetUserContext()
    End Sub

    Private Sub SetPatientButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles SetPatientButton.Click
        SetPatient()
    End Sub

    Private Sub CreateTaskButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles CreateTaskButton.Click
        Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
        Dim taskManager = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskViewManager", Nothing).Instance
        taskManager.CreateTask()
    End Sub

    Private Sub ReviewTasksButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles ReviewTasksButton.Click
        Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
        Dim taskManager = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskViewManager", Nothing).Instance
        taskManager.ReviewTasks()
    End Sub

    Private Sub PatientNotesButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles PatientNotesButton.Click
        If (ApplicationContext.Current.PatientId IsNot Nothing) Then
            Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
            Dim taskManager = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskViewManager", Nothing).Instance
            taskManager.ReviewActivePatientActiveTasks()
        End If
    End Sub

    Protected Overrides Sub OnHandleDestroyed(e As EventArgs)
        MyBase.OnHandleDestroyed(e)

        If (InteractionManager.Current.Confirm("Do you want to stop Task Monitor and Logout user?")) Then
            Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
            Dim taskMonitor = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskMonitor", Nothing).Instance
            taskMonitor.Stop()
            Dim userContext = wrapper.InvokeStaticGet("IO.Practiceware.Application.UserContext", "Current")
            userContext.Logout()
        End If
    End Sub

    Private Sub AdministrationButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles AdministrationButton.Click
        Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
        Dim taskManager = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskViewManager", Nothing).Instance
        taskManager.AdministerTasks()
    End Sub

    Private Sub TaskActivityButtonClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles TaskActivityButton.Click
        Dim wrapper = Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
        Dim taskManager = wrapper.Create("IO.Practiceware.Presentation.Views.Tasks.TaskViewManager", Nothing).Instance
        taskManager.TaskActivityView()
    End Sub
End Class