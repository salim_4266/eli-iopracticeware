﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrinterSetupTestControl
    Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonPrintDocument = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ButtonPrintText = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ButtonPrintImage = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ButtonPrintOther = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.SuspendLayout()
        '
        'ButtonPrintDocument
        '
        Me.ButtonPrintDocument.Location = New System.Drawing.Point(12, 17)
        Me.ButtonPrintDocument.Name = "ButtonPrintDocument"
        Me.ButtonPrintDocument.Size = New System.Drawing.Size(99, 30)
        Me.ButtonPrintDocument.TabIndex = 1
        Me.ButtonPrintDocument.Text = "Print Document"
        '
        'ButtonPrintText
        '
        Me.ButtonPrintText.Location = New System.Drawing.Point(132, 17)
        Me.ButtonPrintText.Name = "ButtonPrintText"
        Me.ButtonPrintText.Size = New System.Drawing.Size(99, 30)
        Me.ButtonPrintText.TabIndex = 2
        Me.ButtonPrintText.Text = "Print Text"
        '
        'ButtonPrintImage
        '
        Me.ButtonPrintImage.Location = New System.Drawing.Point(12, 65)
        Me.ButtonPrintImage.Name = "ButtonPrintImage"
        Me.ButtonPrintImage.Size = New System.Drawing.Size(99, 30)
        Me.ButtonPrintImage.TabIndex = 3
        Me.ButtonPrintImage.Text = "Print Image"
        '
        'ButtonPrintOther
        '
        Me.ButtonPrintOther.Location = New System.Drawing.Point(132, 65)
        Me.ButtonPrintOther.Name = "ButtonPrintOther"
        Me.ButtonPrintOther.Size = New System.Drawing.Size(99, 30)
        Me.ButtonPrintOther.TabIndex = 4
        Me.ButtonPrintOther.Text = "Print Other"
        '
        'PrinterSetupTestControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ButtonPrintOther)
        Me.Controls.Add(Me.ButtonPrintImage)
        Me.Controls.Add(Me.ButtonPrintText)
        Me.Controls.Add(Me.ButtonPrintDocument)
        Me.Name = "PrinterSetupTestControl"
        Me.Size = New System.Drawing.Size(253, 117)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButtonPrintDocument As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ButtonPrintText As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ButtonPrintImage As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ButtonPrintOther As Soaf.Presentation.Controls.WindowsForms.Button

End Class
