﻿Imports IO.Practiceware.Presentation.Views.Documents
Imports Soaf

Public Class PrinterSetupTestControl
    Private Shared Sub ButtonPrintDocumentClick(ByVal sender As Object, ByVal e As EventArgs) _
        Handles ButtonPrintDocument.Click
        ServiceProvider.GetService(Of DocumentViewManager)().PrintDocument(
            "H:\Pinpoint\Templates\StandardReferralLetter.doc", "H:\Pinpoint\Document\Ltrs-S6509.doc")
    End Sub

    Private Shared Sub ButtonPrintTextClick(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonPrintText.Click
        ServiceProvider.GetService(Of DocumentViewManager)().PrintDocument(
            "Proof", "G:\Pinpoint\ExportStatements-20111207.txt")
    End Sub

    Private Shared Sub ButtonPrintImageClick(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonPrintImage.Click
        ServiceProvider.GetService(Of DocumentViewManager)().PrintDocument(
            "Image", "G:\pinpoint\MyScan\Photo_4843.JPG")
    End Sub

    Private Shared Sub ButtonPrintOtherClick(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonPrintOther.Click
        ServiceProvider.GetService(Of DocumentViewManager)().PrintDocument(
            "Other", "G:\pinpoint\MyScan\O-InsCard-__48604-20110920.pdf")
    End Sub
End Class
