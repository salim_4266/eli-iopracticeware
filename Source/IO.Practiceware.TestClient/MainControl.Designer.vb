﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainControl
    Inherits Soaf.Presentation.Controls.WindowsForms.UserControl

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CameraButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._SurgeryButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._scanButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._tasksButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._mailMergeButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._viewDocumentButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me._printDocumentButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnUtilities = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnFileViewer = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ColorsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.BatchbuilderButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.X12MessagesButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.PrinteroptionButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.appointmentSearch = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.LaunchBatchButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ReportViewerButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ReportDesignerButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.scheduleTemplateBuilder = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.CardScannerButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.multiCalendar = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.TestAdoComServiceButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnPerformMailMerge = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.AscExamPrintButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ConfigureCategoriesButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.scheduleAppointment = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnPreAuthorizations = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnReferralScreen = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnRecalls = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.patientSearch = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnPatientInfo = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnRescheduleAppointment = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.cancelAppointment = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnUb04Claim = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnSubmitTransactions = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.glaucomaMonitorDemo = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnSetConfiguration = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.UserPermissionsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.AddInsurancePolicyButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ManageInsurancePlansButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnLoginView = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ClinicalDataFilesButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.HomeScreenButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.PatientDemographicsSetupButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.btnImportClinicalFiles = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.NewCropMUReportButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.GetNewCropMUReportButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.toolStripBarSelectConnectionMode = New System.Windows.Forms.ToolStrip()
        Me.ToolStripComboBoxCurrentConnection = New System.Windows.Forms.ToolStripComboBox()
        Me.txtServerName = New IO.Practiceware.TestClient.ToolStripCustomTextBox()
        Me.txtDatabaseName = New IO.Practiceware.TestClient.ToolStripCustomTextBox()
        Me.btnConnectAndSave = New IO.Practiceware.TestClient.ToolStripCustomButton()
        Me.lblCurrentServer = New System.Windows.Forms.ToolStripLabel()
        Me.lblCurrentConnection = New System.Windows.Forms.ToolStripLabel()
        Me.PatientStatementsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.StatementsButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.AdminstrativeSetupButton = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.BillingNotes = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ApplicationSetup = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.BtnExamBillingView = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.ClinicalSetup = New Soaf.Presentation.Controls.WindowsForms.Button()
        Me.toolStripBarSelectConnectionMode.SuspendLayout()
        Me.SuspendLayout()
        '
        'CameraButton
        '
        Me.CameraButton.Location = New System.Drawing.Point(200, 55)
        Me.CameraButton.Name = "CameraButton"
        Me.CameraButton.Size = New System.Drawing.Size(75, 55)
        Me.CameraButton.TabIndex = 21
        Me.CameraButton.Text = "Camera"
        '
        '_SurgeryButton
        '
        Me._SurgeryButton.Location = New System.Drawing.Point(104, 135)
        Me._SurgeryButton.Name = "_SurgeryButton"
        Me._SurgeryButton.Size = New System.Drawing.Size(75, 55)
        Me._SurgeryButton.TabIndex = 23
        Me._SurgeryButton.Text = "Surgery"
        '
        '_scanButton
        '
        Me._scanButton.Location = New System.Drawing.Point(10, 55)
        Me._scanButton.Name = "_scanButton"
        Me._scanButton.Size = New System.Drawing.Size(75, 55)
        Me._scanButton.TabIndex = 17
        Me._scanButton.Text = "Scan"
        '
        '_tasksButton
        '
        Me._tasksButton.Location = New System.Drawing.Point(104, 55)
        Me._tasksButton.Name = "_tasksButton"
        Me._tasksButton.Size = New System.Drawing.Size(75, 55)
        Me._tasksButton.TabIndex = 18
        Me._tasksButton.Text = "Tasks"
        '
        '_mailMergeButton
        '
        Me._mailMergeButton.Location = New System.Drawing.Point(10, 135)
        Me._mailMergeButton.Name = "_mailMergeButton"
        Me._mailMergeButton.Size = New System.Drawing.Size(75, 55)
        Me._mailMergeButton.TabIndex = 19
        Me._mailMergeButton.Text = "Mail Merge"
        '
        '_viewDocumentButton
        '
        Me._viewDocumentButton.Location = New System.Drawing.Point(10, 215)
        Me._viewDocumentButton.Name = "_viewDocumentButton"
        Me._viewDocumentButton.Size = New System.Drawing.Size(75, 55)
        Me._viewDocumentButton.TabIndex = 24
        Me._viewDocumentButton.Text = "View Document"
        '
        '_printDocumentButton
        '
        Me._printDocumentButton.Location = New System.Drawing.Point(107, 215)
        Me._printDocumentButton.Name = "_printDocumentButton"
        Me._printDocumentButton.Size = New System.Drawing.Size(75, 55)
        Me._printDocumentButton.TabIndex = 25
        Me._printDocumentButton.Text = "Print document"
        '
        'btnUtilities
        '
        Me.btnUtilities.Location = New System.Drawing.Point(295, 55)
        Me.btnUtilities.Name = "btnUtilities"
        Me.btnUtilities.Size = New System.Drawing.Size(75, 55)
        Me.btnUtilities.TabIndex = 27
        Me.btnUtilities.Text = "Utilities"
        '
        'btnFileViewer
        '
        Me.btnFileViewer.Location = New System.Drawing.Point(200, 215)
        Me.btnFileViewer.Name = "btnFileViewer"
        Me.btnFileViewer.Size = New System.Drawing.Size(75, 55)
        Me.btnFileViewer.TabIndex = 26
        Me.btnFileViewer.Text = "File Viewer"
        '
        'ColorsButton
        '
        Me.ColorsButton.Location = New System.Drawing.Point(392, 135)
        Me.ColorsButton.Name = "ColorsButton"
        Me.ColorsButton.Size = New System.Drawing.Size(75, 55)
        Me.ColorsButton.TabIndex = 31
        Me.ColorsButton.Text = "Colors"
        '
        'BatchbuilderButton
        '
        Me.BatchbuilderButton.Location = New System.Drawing.Point(295, 215)
        Me.BatchbuilderButton.Name = "BatchbuilderButton"
        Me.BatchbuilderButton.Size = New System.Drawing.Size(75, 55)
        Me.BatchbuilderButton.TabIndex = 32
        Me.BatchbuilderButton.Text = "Batch Builder"
        '
        'X12MessagesButton
        '
        Me.X12MessagesButton.Location = New System.Drawing.Point(488, 55)
        Me.X12MessagesButton.Name = "X12MessagesButton"
        Me.X12MessagesButton.Size = New System.Drawing.Size(75, 55)
        Me.X12MessagesButton.TabIndex = 33
        Me.X12MessagesButton.Text = "Create X12 Messages"
        '
        'PrinteroptionButton
        '
        Me.PrinteroptionButton.Location = New System.Drawing.Point(488, 135)
        Me.PrinteroptionButton.Name = "PrinteroptionButton"
        Me.PrinteroptionButton.Size = New System.Drawing.Size(75, 55)
        Me.PrinteroptionButton.TabIndex = 34
        Me.PrinteroptionButton.Text = "Printer Option"
        '
        'appointmentSearch
        '
        Me.appointmentSearch.Location = New System.Drawing.Point(392, 215)
        Me.appointmentSearch.Name = "appointmentSearch"
        Me.appointmentSearch.Size = New System.Drawing.Size(75, 55)
        Me.appointmentSearch.TabIndex = 35
        Me.appointmentSearch.Text = "Appointment Search"
        '
        'LaunchBatchButton
        '
        Me.LaunchBatchButton.Location = New System.Drawing.Point(11, 295)
        Me.LaunchBatchButton.Name = "LaunchBatchButton"
        Me.LaunchBatchButton.Size = New System.Drawing.Size(75, 55)
        Me.LaunchBatchButton.TabIndex = 36
        Me.LaunchBatchButton.Text = "Launch Batch Builder"
        '
        'ReportViewerButton
        '
        Me.ReportViewerButton.Location = New System.Drawing.Point(104, 295)
        Me.ReportViewerButton.Name = "ReportViewerButton"
        Me.ReportViewerButton.Size = New System.Drawing.Size(75, 55)
        Me.ReportViewerButton.TabIndex = 37
        Me.ReportViewerButton.Text = "Report Viewer"
        '
        'ReportDesignerButton
        '
        Me.ReportDesignerButton.Location = New System.Drawing.Point(200, 295)
        Me.ReportDesignerButton.Name = "ReportDesignerButton"
        Me.ReportDesignerButton.Size = New System.Drawing.Size(75, 55)
        Me.ReportDesignerButton.TabIndex = 38
        Me.ReportDesignerButton.Text = "Report Designer"
        '
        'scheduleTemplateBuilder
        '
        Me.scheduleTemplateBuilder.Location = New System.Drawing.Point(295, 295)
        Me.scheduleTemplateBuilder.Name = "scheduleTemplateBuilder"
        Me.scheduleTemplateBuilder.Size = New System.Drawing.Size(75, 55)
        Me.scheduleTemplateBuilder.TabIndex = 39
        Me.scheduleTemplateBuilder.Text = "Schedule Template Builder"
        '
        'CardScannerButton
        '
        Me.CardScannerButton.Location = New System.Drawing.Point(392, 295)
        Me.CardScannerButton.Name = "CardScannerButton"
        Me.CardScannerButton.Size = New System.Drawing.Size(75, 55)
        Me.CardScannerButton.TabIndex = 40
        Me.CardScannerButton.Text = "Show Card Scanner"
        '
        'multiCalendar
        '
        Me.multiCalendar.Location = New System.Drawing.Point(488, 215)
        Me.multiCalendar.Name = "multiCalendar"
        Me.multiCalendar.Size = New System.Drawing.Size(75, 55)
        Me.multiCalendar.TabIndex = 41
        Me.multiCalendar.Text = "Show Multi-Calendar"
        '
        'TestAdoComServiceButton
        '
        Me.TestAdoComServiceButton.Location = New System.Drawing.Point(392, 294)
        Me.TestAdoComServiceButton.Name = "TestAdoComServiceButton"
        Me.TestAdoComServiceButton.Size = New System.Drawing.Size(75, 55)
        Me.TestAdoComServiceButton.TabIndex = 40
        Me.TestAdoComServiceButton.Text = "Test Ado Com Service"
        '
        'btnPerformMailMerge
        '
        Me.btnPerformMailMerge.Location = New System.Drawing.Point(104, 367)
        Me.btnPerformMailMerge.Name = "btnPerformMailMerge"
        Me.btnPerformMailMerge.Size = New System.Drawing.Size(75, 56)
        Me.btnPerformMailMerge.TabIndex = 42
        Me.btnPerformMailMerge.Text = "Cms 1500 Claim"
        '
        'AscExamPrintButton
        '
        Me.AscExamPrintButton.Location = New System.Drawing.Point(200, 135)
        Me.AscExamPrintButton.Name = "AscExamPrintButton"
        Me.AscExamPrintButton.Size = New System.Drawing.Size(75, 55)
        Me.AscExamPrintButton.TabIndex = 42
        Me.AscExamPrintButton.Text = "ASC Exam Printing"
        '
        'ConfigureCategoriesButton
        '
        Me.ConfigureCategoriesButton.Location = New System.Drawing.Point(488, 295)
        Me.ConfigureCategoriesButton.Name = "ConfigureCategoriesButton"
        Me.ConfigureCategoriesButton.Size = New System.Drawing.Size(75, 55)
        Me.ConfigureCategoriesButton.TabIndex = 43
        Me.ConfigureCategoriesButton.Text = "Configure Categories"
        '
        'scheduleAppointment
        '
        Me.scheduleAppointment.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.scheduleAppointment.Location = New System.Drawing.Point(7, 367)
        Me.scheduleAppointment.Name = "scheduleAppointment"
        Me.scheduleAppointment.Size = New System.Drawing.Size(78, 55)
        Me.scheduleAppointment.TabIndex = 44
        Me.scheduleAppointment.Text = "Schedule Appointment"
        '
        'btnPreAuthorizations
        '
        Me.btnPreAuthorizations.Location = New System.Drawing.Point(104, 442)
        Me.btnPreAuthorizations.Name = "btnPreAuthorizations"
        Me.btnPreAuthorizations.Size = New System.Drawing.Size(75, 56)
        Me.btnPreAuthorizations.TabIndex = 45
        Me.btnPreAuthorizations.Text = "Pre Authorizations"
        '
        'btnReferralScreen
        '
        Me.btnReferralScreen.Location = New System.Drawing.Point(295, 366)
        Me.btnReferralScreen.Name = "btnReferralScreen"
        Me.btnReferralScreen.Size = New System.Drawing.Size(75, 56)
        Me.btnReferralScreen.TabIndex = 46
        Me.btnReferralScreen.Text = "Referrals"
        '
        'btnRecalls
        '
        Me.btnRecalls.Location = New System.Drawing.Point(392, 367)
        Me.btnRecalls.Name = "btnRecalls"
        Me.btnRecalls.Size = New System.Drawing.Size(75, 56)
        Me.btnRecalls.TabIndex = 47
        Me.btnRecalls.Text = "Recalls"
        '
        'patientSearch
        '
        Me.patientSearch.Location = New System.Drawing.Point(295, 135)
        Me.patientSearch.Name = "patientSearch"
        Me.patientSearch.Size = New System.Drawing.Size(75, 55)
        Me.patientSearch.TabIndex = 47
        Me.patientSearch.Text = "Patient Search"
        '
        'btnPatientInfo
        '
        Me.btnPatientInfo.Location = New System.Drawing.Point(488, 366)
        Me.btnPatientInfo.Name = "btnPatientInfo"
        Me.btnPatientInfo.Size = New System.Drawing.Size(75, 56)
        Me.btnPatientInfo.TabIndex = 48
        Me.btnPatientInfo.Text = "Patient Info"
        '
        'btnRescheduleAppointment
        '
        Me.btnRescheduleAppointment.Location = New System.Drawing.Point(7, 442)
        Me.btnRescheduleAppointment.Name = "btnRescheduleAppointment"
        Me.btnRescheduleAppointment.Size = New System.Drawing.Size(75, 56)
        Me.btnRescheduleAppointment.TabIndex = 49
        Me.btnRescheduleAppointment.Text = "Reschedule Appt"
        '
        'cancelAppointment
        '
        Me.cancelAppointment.Location = New System.Drawing.Point(392, 55)
        Me.cancelAppointment.Name = "cancelAppointment"
        Me.cancelAppointment.Size = New System.Drawing.Size(78, 55)
        Me.cancelAppointment.TabIndex = 50
        Me.cancelAppointment.Text = "Cancel Appointment"
        '
        'btnUb04Claim
        '
        Me.btnUb04Claim.Location = New System.Drawing.Point(200, 367)
        Me.btnUb04Claim.Name = "btnUb04Claim"
        Me.btnUb04Claim.Size = New System.Drawing.Size(75, 56)
        Me.btnUb04Claim.TabIndex = 51
        Me.btnUb04Claim.Text = "Ub04 Claim"
        '
        'btnSubmitTransactions
        '
        Me.btnSubmitTransactions.Location = New System.Drawing.Point(200, 442)
        Me.btnSubmitTransactions.Name = "btnSubmitTransactions"
        Me.btnSubmitTransactions.Size = New System.Drawing.Size(75, 56)
        Me.btnSubmitTransactions.TabIndex = 52
        Me.btnSubmitTransactions.Text = "Submit Transactions"
        '
        'glaucomaMonitorDemo
        '
        Me.glaucomaMonitorDemo.Location = New System.Drawing.Point(295, 442)
        Me.glaucomaMonitorDemo.Name = "glaucomaMonitorDemo"
        Me.glaucomaMonitorDemo.Size = New System.Drawing.Size(75, 56)
        Me.glaucomaMonitorDemo.TabIndex = 53
        Me.glaucomaMonitorDemo.Text = "Glaucoma Monitor Demo"
        '
        'btnSetConfiguration
        '
        Me.btnSetConfiguration.Location = New System.Drawing.Point(392, 442)
        Me.btnSetConfiguration.Name = "btnSetConfiguration"
        Me.btnSetConfiguration.Size = New System.Drawing.Size(75, 56)
        Me.btnSetConfiguration.TabIndex = 54
        Me.btnSetConfiguration.Text = "Set Configuration"
        '
        'UserPermissionsButton
        '
        Me.UserPermissionsButton.Location = New System.Drawing.Point(488, 442)
        Me.UserPermissionsButton.Name = "UserPermissionsButton"
        Me.UserPermissionsButton.Size = New System.Drawing.Size(75, 56)
        Me.UserPermissionsButton.TabIndex = 55
        Me.UserPermissionsButton.Text = "Set User Permissions"
        '
        'AddInsurancePolicyButton
        '
        Me.AddInsurancePolicyButton.Location = New System.Drawing.Point(580, 134)
        Me.AddInsurancePolicyButton.Name = "AddInsurancePolicyButton"
        Me.AddInsurancePolicyButton.Size = New System.Drawing.Size(75, 56)
        Me.AddInsurancePolicyButton.TabIndex = 57
        Me.AddInsurancePolicyButton.Text = "Add Insurance Policy"
        '
        'ManageInsurancePlansButton
        '
        Me.ManageInsurancePlansButton.Location = New System.Drawing.Point(580, 215)
        Me.ManageInsurancePlansButton.Name = "ManageInsurancePlansButton"
        Me.ManageInsurancePlansButton.Size = New System.Drawing.Size(75, 56)
        Me.ManageInsurancePlansButton.TabIndex = 58
        Me.ManageInsurancePlansButton.Text = "Manage Insurance Plans"
        '
        'btnLoginView
        '
        Me.btnLoginView.Location = New System.Drawing.Point(580, 55)
        Me.btnLoginView.Name = "btnLoginView"
        Me.btnLoginView.Size = New System.Drawing.Size(75, 56)
        Me.btnLoginView.TabIndex = 60
        Me.btnLoginView.Text = "Login View"
        '
        'ClinicalDataFilesButton
        '
        Me.ClinicalDataFilesButton.Location = New System.Drawing.Point(580, 293)
        Me.ClinicalDataFilesButton.Name = "ClinicalDataFilesButton"
        Me.ClinicalDataFilesButton.Size = New System.Drawing.Size(75, 56)
        Me.ClinicalDataFilesButton.TabIndex = 59
        Me.ClinicalDataFilesButton.Text = "Clinical Data Files"
        '
        'HomeScreenButton
        '
        Me.HomeScreenButton.Location = New System.Drawing.Point(580, 366)
        Me.HomeScreenButton.Name = "HomeScreenButton"
        Me.HomeScreenButton.Size = New System.Drawing.Size(75, 56)
        Me.HomeScreenButton.TabIndex = 61
        Me.HomeScreenButton.Text = "Home screen"
        '
        'PatientDemographicsSetupButton
        '
        Me.PatientDemographicsSetupButton.Location = New System.Drawing.Point(580, 442)
        Me.PatientDemographicsSetupButton.Name = "PatientDemographicsSetupButton"
        Me.PatientDemographicsSetupButton.Size = New System.Drawing.Size(75, 56)
        Me.PatientDemographicsSetupButton.TabIndex = 61
        Me.PatientDemographicsSetupButton.Text = "Patient Demographics Setup"
        '
        'btnImportClinicalFiles
        '
        Me.btnImportClinicalFiles.Location = New System.Drawing.Point(672, 55)
        Me.btnImportClinicalFiles.Name = "btnImportClinicalFiles"
        Me.btnImportClinicalFiles.Size = New System.Drawing.Size(75, 56)
        Me.btnImportClinicalFiles.TabIndex = 62
        Me.btnImportClinicalFiles.Text = "Import Clinical Files"
        '
        'NewCropMUReportButton
        '
        Me.NewCropMUReportButton.Location = New System.Drawing.Point(672, 134)
        Me.NewCropMUReportButton.Name = "NewCropMUReportButton"
        Me.NewCropMUReportButton.Size = New System.Drawing.Size(75, 56)
        Me.NewCropMUReportButton.TabIndex = 63
        Me.NewCropMUReportButton.Text = "Generate NewCrop MU Report"
        '
        'GetNewCropMUReportButton
        '
        Me.GetNewCropMUReportButton.Location = New System.Drawing.Point(672, 215)
        Me.GetNewCropMUReportButton.Name = "GetNewCropMUReportButton"
        Me.GetNewCropMUReportButton.Size = New System.Drawing.Size(75, 56)
        Me.GetNewCropMUReportButton.TabIndex = 64
        Me.GetNewCropMUReportButton.Text = "Get NewCrop MU Report"
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(150, 150)
        '
        'toolStripBarSelectConnectionMode
        '
        Me.toolStripBarSelectConnectionMode.AllowMerge = False
        Me.toolStripBarSelectConnectionMode.BackColor = System.Drawing.SystemColors.Window
        Me.toolStripBarSelectConnectionMode.CanOverflow = False
        Me.toolStripBarSelectConnectionMode.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.toolStripBarSelectConnectionMode.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.toolStripBarSelectConnectionMode.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripComboBoxCurrentConnection, Me.txtServerName, Me.txtDatabaseName, Me.btnConnectAndSave, Me.lblCurrentServer, Me.lblCurrentConnection})
        Me.toolStripBarSelectConnectionMode.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.toolStripBarSelectConnectionMode.Location = New System.Drawing.Point(5, 20)
        Me.toolStripBarSelectConnectionMode.Name = "toolStripBarSelectConnectionMode"
        Me.toolStripBarSelectConnectionMode.Padding = New System.Windows.Forms.Padding(0, 3, 1, 3)
        Me.toolStripBarSelectConnectionMode.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.toolStripBarSelectConnectionMode.Size = New System.Drawing.Size(748, 31)
        Me.toolStripBarSelectConnectionMode.Stretch = True
        Me.toolStripBarSelectConnectionMode.TabIndex = 65
        '
        'ToolStripComboBoxCurrentConnection
        '
        Me.ToolStripComboBoxCurrentConnection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ToolStripComboBoxCurrentConnection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ToolStripComboBoxCurrentConnection.AutoSize = False
        Me.ToolStripComboBoxCurrentConnection.DropDownWidth = 100
        Me.ToolStripComboBoxCurrentConnection.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.ToolStripComboBoxCurrentConnection.Items.AddRange(New Object() {"Cloud", "Sql Server"})
        Me.ToolStripComboBoxCurrentConnection.MaxDropDownItems = 2
        Me.ToolStripComboBoxCurrentConnection.Name = "ToolStripComboBoxCurrentConnection"
        Me.ToolStripComboBoxCurrentConnection.Size = New System.Drawing.Size(85, 21)
        Me.ToolStripComboBoxCurrentConnection.ToolTipText = "Choose a Connection"
        '
        'txtServerName
        '
        Me.txtServerName.AutoCompleteCustomSource.AddRange(New String() {"localhost", "dev-sql01", "dev-sql02", "dev-sql03"})
        Me.txtServerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtServerName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtServerName.AutoSize = False
        Me.txtServerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtServerName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServerName.Margin = New System.Windows.Forms.Padding(3, 0, 5, 0)
        Me.txtServerName.Name = "txtServerName"
        Me.txtServerName.Size = New System.Drawing.Size(125, 20)
        '
        'txtDatabaseName
        '
        Me.txtDatabaseName.AutoCompleteCustomSource.AddRange(New String() {"PracticeRepositoryMain"})
        Me.txtDatabaseName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtDatabaseName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtDatabaseName.AutoSize = False
        Me.txtDatabaseName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDatabaseName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDatabaseName.Name = "txtDatabaseName"
        Me.txtDatabaseName.Size = New System.Drawing.Size(150, 20)
        Me.txtDatabaseName.ToolTipText = "Enter the default database name  (e.g. PracticeRepositoryMain)"
        '
        'btnConnectAndSave
        '
        Me.btnConnectAndSave.AutoSize = False
        Me.btnConnectAndSave.BackColor = System.Drawing.SystemColors.Window
        Me.btnConnectAndSave.ButtonFlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnectAndSave.ButtonSize = New System.Drawing.Size(60, 21)
        Me.btnConnectAndSave.ForeColor = System.Drawing.SystemColors.WindowText
        Me.btnConnectAndSave.Margin = New System.Windows.Forms.Padding(4, 1, 4, 2)
        Me.btnConnectAndSave.Name = "btnConnectAndSave"
        Me.btnConnectAndSave.Size = New System.Drawing.Size(60, 21)
        Me.btnConnectAndSave.Text = "Connect"
        '
        'lblCurrentServer
        '
        Me.lblCurrentServer.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblCurrentServer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblCurrentServer.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblCurrentServer.IsLink = True
        Me.lblCurrentServer.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lblCurrentServer.Name = "lblCurrentServer"
        Me.lblCurrentServer.Size = New System.Drawing.Size(0, 22)
        '
        'lblCurrentConnection
        '
        Me.lblCurrentConnection.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.lblCurrentConnection.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblCurrentConnection.BackColor = System.Drawing.SystemColors.Window
        Me.lblCurrentConnection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblCurrentConnection.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblCurrentConnection.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblCurrentConnection.LinkColor = System.Drawing.SystemColors.WindowText
        Me.lblCurrentConnection.Name = "lblCurrentConnection"
        Me.lblCurrentConnection.Size = New System.Drawing.Size(43, 22)
        Me.lblCurrentConnection.Text = "Active:"
        '
        'PatientStatementsButton
        '
        Me.PatientStatementsButton.Location = New System.Drawing.Point(672, 293)
        Me.PatientStatementsButton.Name = "PatientStatementsButton"
        Me.PatientStatementsButton.Size = New System.Drawing.Size(75, 56)
        Me.PatientStatementsButton.TabIndex = 67
        Me.PatientStatementsButton.Text = "Patient Statements"
        '
        'StatementsButton
        '
        Me.StatementsButton.Location = New System.Drawing.Point(672, 366)
        Me.StatementsButton.Name = "StatementsButton"
        Me.StatementsButton.Size = New System.Drawing.Size(75, 56)
        Me.StatementsButton.TabIndex = 68
        Me.StatementsButton.Text = "Statements"
        '
        'AdminstrativeSetupButton
        '
        Me.AdminstrativeSetupButton.Location = New System.Drawing.Point(672, 442)
        Me.AdminstrativeSetupButton.Name = "AdminstrativeSetupButton"
        Me.AdminstrativeSetupButton.Size = New System.Drawing.Size(75, 56)
        Me.AdminstrativeSetupButton.TabIndex = 69
        Me.AdminstrativeSetupButton.Text = "AdministrativeSetup"
        '
        'BillingNotes
        '
        Me.BillingNotes.Location = New System.Drawing.Point(675, 442)
        Me.BillingNotes.Name = "BillingNotes"
        Me.BillingNotes.Size = New System.Drawing.Size(75, 56)
        Me.BillingNotes.TabIndex = 69
        Me.BillingNotes.Text = "Billing Notes"
        '
        'ApplicationSetup
        '
        Me.ApplicationSetup.Location = New System.Drawing.Point(8, 512)
        Me.ApplicationSetup.Name = "ApplicationSetup"
        Me.ApplicationSetup.Size = New System.Drawing.Size(75, 56)
        Me.ApplicationSetup.TabIndex = 70
        Me.ApplicationSetup.Text = "Application Setup"
        '
        'BtnExamBillingView
        '
        Me.BtnExamBillingView.Location = New System.Drawing.Point(104, 512)
        Me.BtnExamBillingView.Name = "BtnExamBillingView"
        Me.BtnExamBillingView.Size = New System.Drawing.Size(75, 56)
        Me.BtnExamBillingView.TabIndex = 71
        Me.BtnExamBillingView.Text = "ExamBillingView"
        '
        'ClinicalSetup
        '
        Me.ClinicalSetup.Location = New System.Drawing.Point(200, 512)
        Me.ClinicalSetup.Name = "ClinicalSetup"
        Me.ClinicalSetup.Size = New System.Drawing.Size(75, 56)
        Me.ClinicalSetup.TabIndex = 72
        Me.ClinicalSetup.Text = "Clinical Setup"
        '
        'MainControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.AdminstrativeSetupButton)
        Me.Controls.Add(Me.ClinicalSetup)
        Me.Controls.Add(Me.BtnExamBillingView)
        Me.Controls.Add(Me.ApplicationSetup)
        Me.Controls.Add(Me.BillingNotes)
        Me.Controls.Add(Me.StatementsButton)
        Me.Controls.Add(Me.PatientStatementsButton)
        Me.Controls.Add(Me.toolStripBarSelectConnectionMode)
        Me.Controls.Add(Me.GetNewCropMUReportButton)
        Me.Controls.Add(Me.NewCropMUReportButton)
        Me.Controls.Add(Me.btnImportClinicalFiles)
        Me.Controls.Add(Me.PatientDemographicsSetupButton)
        Me.Controls.Add(Me.btnLoginView)
        Me.Controls.Add(Me.ClinicalDataFilesButton)
        Me.Controls.Add(Me.ManageInsurancePlansButton)
        Me.Controls.Add(Me.AddInsurancePolicyButton)
        Me.Controls.Add(Me.UserPermissionsButton)
        Me.Controls.Add(Me.btnSetConfiguration)
        Me.Controls.Add(Me.glaucomaMonitorDemo)
        Me.Controls.Add(Me.btnSubmitTransactions)
        Me.Controls.Add(Me.btnUb04Claim)
        Me.Controls.Add(Me.cancelAppointment)
        Me.Controls.Add(Me.btnRescheduleAppointment)
        Me.Controls.Add(Me.btnPatientInfo)
        Me.Controls.Add(Me.btnRecalls)
        Me.Controls.Add(Me.patientSearch)
        Me.Controls.Add(Me.btnReferralScreen)
        Me.Controls.Add(Me.btnPreAuthorizations)
        Me.Controls.Add(Me.scheduleAppointment)
        Me.Controls.Add(Me.ConfigureCategoriesButton)
        Me.Controls.Add(Me.btnPerformMailMerge)
        Me.Controls.Add(Me.AscExamPrintButton)
        Me.Controls.Add(Me.multiCalendar)
        Me.Controls.Add(Me.CardScannerButton)
        Me.Controls.Add(Me.TestAdoComServiceButton)
        Me.Controls.Add(Me.scheduleTemplateBuilder)
        Me.Controls.Add(Me.ReportDesignerButton)
        Me.Controls.Add(Me.ReportViewerButton)
        Me.Controls.Add(Me.LaunchBatchButton)
        Me.Controls.Add(Me.appointmentSearch)
        Me.Controls.Add(Me.PrinteroptionButton)
        Me.Controls.Add(Me.X12MessagesButton)
        Me.Controls.Add(Me.BatchbuilderButton)
        Me.Controls.Add(Me.ColorsButton)
        Me.Controls.Add(Me.btnFileViewer)
        Me.Controls.Add(Me._printDocumentButton)
        Me.Controls.Add(Me._viewDocumentButton)
        Me.Controls.Add(Me.CameraButton)
        Me.Controls.Add(Me._SurgeryButton)
        Me.Controls.Add(Me._scanButton)
        Me.Controls.Add(Me._tasksButton)
        Me.Controls.Add(Me._mailMergeButton)
        Me.Controls.Add(Me.btnUtilities)
        Me.Controls.Add(Me.HomeScreenButton)
        Me.Name = "MainControl"
        Me.OverrideBackColor = True
        Me.Padding = New System.Windows.Forms.Padding(5, 20, 5, 5)
        Me.Size = New System.Drawing.Size(758, 576)
        Me.toolStripBarSelectConnectionMode.ResumeLayout(False)
        Me.toolStripBarSelectConnectionMode.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CameraButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _SurgeryButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _scanButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _tasksButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _mailMergeButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _viewDocumentButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents _printDocumentButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnUtilities As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnFileViewer As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ColorsButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents BatchbuilderButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents X12MessagesButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents PrinteroptionButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents appointmentSearch As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents LaunchBatchButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ReportViewerButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ReportDesignerButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents scheduleTemplateBuilder As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents TestAdoComServiceButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents CardScannerButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents multiCalendar As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnPerformMailMerge As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents AscExamPrintButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ConfigureCategoriesButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents scheduleAppointment As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnPreAuthorizations As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnReferralScreen As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnRecalls As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents patientSearch As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnPatientInfo As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnRescheduleAppointment As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents cancelAppointment As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnUb04Claim As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnSubmitTransactions As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents glaucomaMonitorDemo As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnSetConfiguration As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents UserPermissionsButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents AddInsurancePolicyButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ManageInsurancePlansButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnLoginView As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ClinicalDataFilesButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents HomeScreenButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents PatientDemographicsSetupButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents btnImportClinicalFiles As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents NewCropMUReportButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents GetNewCropMUReportButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents toolStripBarSelectConnectionMode As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripComboBoxCurrentConnection As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents txtServerName As ToolStripCustomTextBox
    Friend WithEvents txtDatabaseName As ToolStripCustomTextBox
    Friend WithEvents btnConnectAndSave As ToolStripCustomButton
    Friend WithEvents lblCurrentConnection As System.Windows.Forms.ToolStripLabel
    Friend WithEvents lblCurrentServer As System.Windows.Forms.ToolStripLabel
    Friend WithEvents PatientStatementsButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents StatementsButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents AdminstrativeSetupButton As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents BillingNotes As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ApplicationSetup As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents BtnExamBillingView As Soaf.Presentation.Controls.WindowsForms.Button
    Friend WithEvents ClinicalSetup As Soaf.Presentation.Controls.WindowsForms.Button


End Class

