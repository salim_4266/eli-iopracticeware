﻿Imports System.IO
Imports System.Windows.Controls
Imports ADODB
Imports IO.Practiceware.Presentation.Views.GenerateStatements
Imports IO.Practiceware.Presentation.Views.Financials.PatientStatements
Imports IO.Practiceware.Presentation.Views.Setup
Imports Soaf.IO
Imports IO.Practiceware.Application
Imports IO.Practiceware.Presentation.ViewModels.InsurancePolicy
Imports IO.Practiceware.Integration.PaperClaims
Imports IO.Practiceware.Presentation.Views.PatientInfo
Imports System.Collections.ObjectModel
Imports Soaf.Data
Imports IO.Practiceware.Configuration
Imports System.Reflection
Imports Soaf.Collections
Imports IO.Practiceware.Presentation.Views.Documents
Imports IO.Practiceware.Services.Documents
Imports IO.Practiceware.Interop
Imports IO.Practiceware.Model
Imports Soaf.Logging
Imports Soaf
Imports Soaf.Presentation
Imports System.Windows
Imports System.Text
Imports IO.Practiceware.Integration.X12
Imports IO.Practiceware.Presentation.ViewModels.SubmitTransactions
Imports Soaf.Threading
Imports IO.Practiceware.Services.Utilities
Imports System.Data.SqlTypes
Imports IO.Practiceware.SqlClr
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports IO.Practiceware.Storage
Imports Task = System.Threading.Tasks.Task
Imports System.Transactions

Public Class MainControl
    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Header = "Test Client"

        ' Hook into events sent to Vb6, so that window would close and open notifying about what happens
        Dim patientInfoViewManagerWrapper As New ComWrapper()
        Dim openLegacyScreenHandler As New OpenLegacyPatientScreenEventHandler
        patientInfoViewManagerWrapper.Create("IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoViewManager, IO.Practiceware.Presentation", Nothing)
        patientInfoViewManagerWrapper.AddEventHandler("OpenLegacyPatientScreen", openLegacyScreenHandler)

        Dim integrationManagerWrapper As New ComWrapper()
        Dim handler As New Vb6FeatureExecutionEventHandler
        integrationManagerWrapper.Create("IO.Practiceware.Application.ExternalFeatureIntegrationManager, IO.Practiceware.Core", Nothing)
        integrationManagerWrapper.AddEventHandler("ExternalFeatureExectionRequested", handler)

        Dim adminUtilitiesViewManagerWrapper As New ComWrapper()
        Dim openMachineConfigurationHandler As New OpenMachineConfigurationEventHandler
        adminUtilitiesViewManagerWrapper.Create("IO.Practiceware.Presentation.Views.Setup.AdminUtilities.AdminUtilitiesViewManager, IO.Practiceware.Presentation", Nothing)
        adminUtilitiesViewManagerWrapper.AddEventHandler("OpenMachineConfiguration", openMachineConfigurationHandler)

        Dim standardExamViewManagerWrapper As New ComWrapper()
        Dim loadPlanFormHandler As New LoadPlanFormEventHandler
        standardExamViewManagerWrapper.Create("IO.Practiceware.Presentation.Views.StandardExam.StandardExamViewManager, IO.Practiceware.Presentation", Nothing)
        standardExamViewManagerWrapper.AddEventHandler("LoadPlanFormHandler", loadPlanFormHandler)
        Dim loadImpressionFormHandler As New LoadImpressionFormEventHandler
        standardExamViewManagerWrapper.AddEventHandler("LoadImpressionFormHandler", loadImpressionFormHandler)
        Dim loadChartFormHandler As New LoadChartFormEventHandler
        standardExamViewManagerWrapper.AddEventHandler("LoadChartFormHandler", loadChartFormHandler)
        Dim loadHighlightFormHandler As New LoadHighlightFormEventHandler
        standardExamViewManagerWrapper.AddEventHandler("LoadHighlightFormHandler", loadHighlightFormHandler)


        AddHandler _timerForLabel.Tick, AddressOf TimerForLabel_Tick
    End Sub


    Private Sub ScanButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles _scanButton.Click
        Dim comWrapper As New ComWrapper()
        comWrapper.Create("IO.Practiceware.Presentation.Views.Imaging.ImagingViewManager", Nothing)
        Directory.CreateDirectory("C:\temp")
        comWrapper.Instance.ShowScanView("C:\temp\scan.pdf")
    End Sub

    Private Sub TasksButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles _tasksButton.Click
        Dim control As New TasksTestControl()
        InteractionManager.Current.ShowModal(
            New WindowInteractionArguments _
                                                With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Private Sub MailMergeButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles _mailMergeButton.Click
        ServiceProvider.GetService(Of IDocumentGenerationService)().PerformMailMerge("D:\IOP\Pinpoint\Templates\DIPrint.doc",
                                                                                       FileManager.Instance.ReadContentsAsText(
                                                                                           "C:\temp\Mergedata.txt"),
                                                                                       "C:\temp\mergeout.doc",
                                                                                       "D:\IOP\pinpoint\Templates\BCSig.doc",
                                                                                       True, False)

        ServiceProvider.GetService(Of DocumentViewManager)().PrintDocument("G:\pinpoint\Templates\DIPrint.doc", "C:\temp\mergeout.doc")
    End Sub

    Private Sub ViewDocumentButtonClick(ByVal sender As Object, ByVal e As EventArgs) _
        Handles _viewDocumentButton.Click
        ServiceProvider.GetService(Of DocumentViewManager)().ViewDocument("H:\Pinpoint\Document\Ltrs-S6509.doc")
    End Sub

    Private Sub PrintDocumentButtonClick(ByVal sender As Object, ByVal e As EventArgs) _
        Handles _printDocumentButton.Click
        Dim control As New PrinterSetupTestControl()
        InteractionManager.Current.ShowModal(
            New WindowInteractionArguments _
                                                With {.Content = control, .ResizeMode = ResizeMode.NoResize})
    End Sub

    Private Sub CameraButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles CameraButton.Click
        Dim comWrapper As New ComWrapper()
        comWrapper.Create("IO.Practiceware.Presentation.Views.Imaging.ImagingViewManager", Nothing)
        Dim instance = comWrapper.Instance
        instance.ShowCameraView()
    End Sub

    Private Sub SurgeryButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles _SurgeryButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Exam.ExamViewManager", Nothing).Instance
        manager.ShowSurgeryScreen(10911)
    End Sub

    Private Sub FileViewerClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnFileViewer.Click
        Dim filename As String
        'filename = "c:\aaa\CMS_EXAMPLE_999.TXT"
        'filename = "c:\aaa\CMS_EXAMPLE_TA1.TXT"
        'filename = "c:\aaa\277CA_NYMEDICAID.txt"
        filename = "c:\aaa\Texas Medicaid 277CA.txt"

        Dim objReader As New StreamReader(filename)
        Dim messageFile As String = objReader.ReadToEnd
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create("IO.Practiceware.Integration.X12.UI.X12UIManager, IO.Practiceware.Core", Nothing).
                Instance

        manager.ShowFileDisplayScreen(messageFile)
    End Sub

    Private Sub UtilitiesClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnUtilities.Click
        Dim wrapper =
                Activator.CreateInstance(Type.GetType("IO.Practiceware.Interop.ComWrapper, IO.Practiceware.Interop"))
        wrapper.InvokeStaticMethod("IO.Practiceware.Utilities.LauncherControl,IO.Practiceware.Utilities",
                                   "ShowLauncher", Nothing)
    End Sub

    Private Sub ColorsButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles ColorsButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create("IO.Practiceware.Presentation.Views.Scheduling.SchedulingViewManager", Nothing).
                Instance
        manager.ViewColors()
    End Sub

    Private Sub BatchBuilderButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles BatchbuilderButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create(
                    "IO.Practiceware.Presentation.Views.BatchBuilder.BatchBuilderViewManager, IO.Practiceware.Presentation",
                    Nothing).Instance
        manager.ShowBatchBuilder("")
    End Sub

    Private Sub X12MessagesButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles X12MessagesButton.Click
        Dim count =
               Task.Factory.StartNew(Function() ServiceProvider.GetService(Of IPracticeRepository)().BillingServiceTransactions.Where(
                    Function(t) _
                                                                                                          t.MethodSent =
                                                                                                          MethodSent.
                                                                                                              Electronic AndAlso
                                                                                                          t.
                                                                                                              BillingServiceTransactionStatus =
                                                                                                          BillingServiceTransactionStatus _
                                                                                                              .Queued AndAlso
                                                                                                          t.
                                                                                                              ClaimFileReceiver IsNot _
                                                                                                          Nothing).
                Select(Function(t) t.Id).Count()).Result
        Using _
            New TimedScope(
                Sub(s) Debug.WriteLine("Generated 837 message for {0} transactions in {1}".FormatWith(count, s)))
            Dim comWrapper As New ComWrapper()
            Dim producer =
                    comWrapper.Create("IO.Practiceware.Integration.X12.MessageProducer837, IO.Practiceware.Core", {}).
                    Instance
            Dim message =
                Task.Factory.StartNew(Function()
                                          Return producer.ProduceMessage(
                                               ServiceProvider.GetService(Of IPracticeRepository)().BillingServiceTransactions.Where(
                                                   Function(t) _
                                                                                                                                         t.
                                                                                                                                             MethodSent =
                                                                                                                                         MethodSent _
                                                                                                                                             .
                                                                                                                                             Electronic AndAlso
                                                                                                                                         t.
                                                                                                                                             BillingServiceTransactionStatus =
                                                                                                                                         BillingServiceTransactionStatus _
                                                                                                                                             .
                                                                                                                                             Queued AndAlso
                                                                                                                                         t.
                                                                                                                                             ClaimFileReceiver IsNot _
                                                                                                                                         Nothing) _
                                                                      .Select(Function(t) t.Id).ToArray())
                                      End Function).Result
            Dim stringBuilder As New StringBuilder
            DirectCast(message, X12Message).Messages.ForEach(
                Function(t) stringBuilder.AppendLine(t.SerializeToX12(True)))
            Dim textBox As New TextBox With {.Width = 500, .Height = 300}
            textBox.Text = stringBuilder.ToString()
            InteractionManager.Current.Show(New WindowInteractionArguments _
                                               With {.Content = textBox, .DialogButtons = DialogButtons.Ok})
        End Using
    End Sub

    Private Sub PrinteroptionButtonClick(ByVal sender As Object, ByVal e As EventArgs) _
        Handles PrinteroptionButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create(
                    "IO.Practiceware.Presentation.Views.PrinterSetup.PrinterSetupViewManager, IO.Practiceware.Presentation",
                    Nothing).Instance
        manager.ShowPrinterOptions()
    End Sub

    Private Sub AppointmentSearchClick(ByVal sender As Object, ByVal e As EventArgs) Handles appointmentSearch.Click
        Dim input = InteractionManager.Current.Prompt("Enter a patient Id").Input
        If input IsNot Nothing Then
            Dim patientId = Integer.Parse(input)
            Dim comWrapper As New ComWrapper
            Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.AppointmentSearch.AppointmentSearchViewManager, IO.Practiceware.Presentation", Nothing).Instance
            manager.ShowAppointmentSearch(patientId)
        End If
    End Sub

    Private Sub LaunchBatchButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles LaunchBatchButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create(
                    "IO.Practiceware.Presentation.Views.BatchBuilder.BatchBuilderViewManager, IO.Practiceware.Presentation",
                    Nothing).Instance
        manager.ShowBatchBuilder("PatientSearch1")
    End Sub

    Private Sub OnReportViewerButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles ReportViewerButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create(
                    "IO.Practiceware.Presentation.Views.Reporting.ReportViewManager, IO.Practiceware.Presentation",
                    Nothing).Instance
        manager.ViewReportList()
    End Sub

    Private Sub ReportDesignerButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles ReportDesignerButton.Click
        Dim reportDesignerFileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "..\..\..\..\..\Dependencies\Telerik.ReportDesigner.exe")
        Dim destinationConfigFile = reportDesignerFileName + ".config"
        Dim configurationXml = "<configuration><connectionStrings><add name=""PracticeRepository"" connectionString=""{0}"" providerName=""System.Data.SqlClient"" /></connectionStrings></configuration>"
        Dim connectionString = ServiceProvider.GetService(Of IAdoService).ResolveConnectionString("PracticeRepository")
        configurationXml = configurationXml.FormatWith(connectionString)
        File.WriteAllText(destinationConfigFile, configurationXml)
        Process.Start(reportDesignerFileName)
    End Sub

    Private Sub ScheduleTemplateBuilderClick(ByVal sender As Object, ByVal e As EventArgs) Handles scheduleTemplateBuilder.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder.ScheduleTemplateBuilderViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowScheduleTemplateBuilder()
    End Sub

    Private Sub CardScannerButtonClick(sender As Object, e As EventArgs) Handles CardScannerButton.Click
        Dim comWrapper As New ComWrapper()
        comWrapper.Create("IO.Practiceware.Presentation.Views.Imaging.ImagingViewManager", Nothing)
        Dim patientId = comWrapper.Instance.ShowCardScannerView(InteractionManager.Current.Prompt("Enter patient id").Input)
        If patientId > 0 Then
            MsgBox("Patient ID : " & patientId.ToString & " Inserted/Updated")
        End If
    End Sub

    Private Sub MultiCalendarClick(sender As Object, e As EventArgs) Handles multiCalendar.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.MultiCalendar.MultiCalendarViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowMultiCalendar()
    End Sub

    Private Sub ConfigureCategoriesButtonClick(sender As Object, e As EventArgs) Handles ConfigureCategoriesButton.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.ConfigureCategories.ConfigureCategoriesViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowConfigureCategories()
    End Sub

    Private Sub TestAdoComServiceButtonClick(sender As Object, e As EventArgs) Handles TestAdoComServiceButton.Click
        Const sql As String = "Select * FROM PatientDemographics WHERE LastName >='AMA' ORDER BY LastName ASC, FirstName ASC, MiddleInitial ASC, NameReference ASC"
        Dim connection As New AdoConnection With {.ConnectionString = "PracticeRepository"}
        Dim command As New AdoCommand With {.ActiveConnection = connection, .CommandText = sql, .CommandType = CommandTypeEnum.adCmdText}
        Dim rs As New AdoRecordset With {.ActiveConnection = connection}
        rs.Open(command, Nothing, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic, -1)

    End Sub

    Private Sub AscExamPrintButtonClick(sender As Object, e As EventArgs) Handles AscExamPrintButton.Click
        Task.Factory.StartNewWithCurrentTransaction(Sub() ServiceProvider.GetService(Of DocumentViewManager)().PrintEncounterDocuments(10911)).Wait()
    End Sub

    Private Function GetBillingServiceTransIds() As List(Of Guid)
        Dim input = InteractionManager.Current.Prompt("Enter a comma separated list of Billing Service Transaction Ids. Leave empty for default data.").Input

        Dim separator As Char() = New Char() {","c}

        Dim strArray As String() = input.Split(separator)

        Dim billingServiceTransIds As List(Of Guid) = New List(Of Guid)

        For Each str2 In strArray
            Dim item As New Guid(str2)
            billingServiceTransIds.Add(item)
        Next

        Return billingServiceTransIds
    End Function


    Private Sub btnPerformMailMerge_Click(sender As Object, e As EventArgs) Handles btnPerformMailMerge.Click


        Dim billingServiceTransIds = GetBillingServiceTransIds()

        Task.Factory.StartNew(Sub()


                                  ' Create a TransactionScope so any DB modifications can be rolled back.
                                  Dim transOptions = New TransactionOptions()
                                  transOptions.IsolationLevel = IsolationLevel.ReadCommitted
                                  transOptions.Timeout = TimeSpan.FromMinutes(10)
                                  Dim transactionScope = New TransactionScope(TransactionScopeOption.RequiresNew, transOptions)
                                  Try

                                      If ConfigurationManager.PracticeRepositoryConnectionString = "PracticeRepository" Then
                                          MessageBox.Show("You must set the connection string in the configuration file to a direct connection.")
                                          Return
                                      End If


                                      If billingServiceTransIds.IsNullOrEmpty() Then
                                          MessageBox.Show("You must enter a set of billing transaction ids.")
                                          Return
                                      End If


                                      Dim practiceRepository = ServiceProvider.GetService(Of IPracticeRepository)()

                                      practiceRepository.IsLazyLoadingEnabled = True

                                      Dim messageProducer = ServiceProvider.GetService(Of PaperClaimsMessageProducer)()

                                      Dim dataCms As Byte()
                                      dataCms = GetDocumentBytes("CMS-1500.cshtml")
                                      Dim stringCms = Encoding.UTF8.GetString(dataCms)

                                      messageProducer.IsTesting = True
                                      Dim message = CType(messageProducer.ProduceMessage(billingServiceTransIds, stringCms, ContentType.RazorTemplate), HtmlDocument)

                                      Dim pdfMessage = WkHtmlToPdfWrapperService.GeneratePdf(message.Content, 5, 10, 5, 10)

                                      If (Not FileManager.Instance.DirectoryExists("c:\\Temp")) Then
                                          FileManager.Instance.CreateDirectory("c:\\Temp")
                                      End If
                                      Dim templatetempfileCms As String = "c:\\Temp\\TestOutputCms1500.pdf"
                                      FileManager.Instance.CommitContents(templatetempfileCms, pdfMessage)

                                      MessageBox.Show(String.Format("Successfully wrote merged files to: {0}", templatetempfileCms))
                                  Catch ex As Exception
                                      MessageBox.Show("An Error Occurred: \r\n " + ex.ToString())
                                  Finally
                                      transactionScope.Dispose()
                                  End Try

                              End Sub).Wait()


    End Sub


    Private Sub btnUb04Claim_Click(sender As Object, e As EventArgs) Handles btnUb04Claim.Click
        Dim billingServiceTransIds = GetBillingServiceTransIds()

        Task.Factory.StartNew(Sub()


                                  ' Create a TransactionScope so any DB modifications can be rolled back.
                                  Dim transOptions = New TransactionOptions()
                                  transOptions.IsolationLevel = IsolationLevel.ReadCommitted
                                  transOptions.Timeout = TimeSpan.FromMinutes(10)
                                  Dim transactionScope = New TransactionScope(TransactionScopeOption.RequiresNew, transOptions)
                                  Try

                                      If ConfigurationManager.PracticeRepositoryConnectionString = "PracticeRepository" Then
                                          MessageBox.Show("You must set the connection string in the configuration file to a direct connection.")
                                          Return
                                      End If


                                      If billingServiceTransIds.IsNullOrEmpty() Then
                                          MessageBox.Show("You must enter a set of billing transaction ids.")
                                          Return
                                      End If


                                      Dim practiceRepository = ServiceProvider.GetService(Of IPracticeRepository)()


                                      practiceRepository.IsLazyLoadingEnabled = True

                                      Dim messageProducer = ServiceProvider.GetService(Of PaperClaimsMessageProducer)()


                                      Dim dataUb04 As Byte()
                                      dataUb04 = GetDocumentBytes("UB-04.cshtml")

                                      Dim stringUb04 = Encoding.UTF8.GetString(dataUb04)

                                      messageProducer.IsTesting = True
                                      Dim message = CType(messageProducer.ProduceMessage(billingServiceTransIds, stringUb04, ContentType.RazorTemplate), HtmlDocument)

                                      Dim pdfMessage = WkHtmlToPdfWrapperService.GeneratePdf(message.Content, 5, 0, 5, 0)

                                      If (Not FileManager.Instance.DirectoryExists("c:\\Temp")) Then
                                          FileManager.Instance.CreateDirectory("c:\\Temp")
                                      End If
                                      Dim templatetempfileUb04 As String = "c:\\Temp\\TestOutputUb04.pdf"
                                      FileManager.Instance.CommitContents(templatetempfileUb04, pdfMessage)


                                      MessageBox.Show(String.Format("Successfully wrote merged files to: {0}", templatetempfileUb04))
                                  Catch ex As Exception
                                      MessageBox.Show("An Error Occurred: \r\n " + ex.ToString())
                                  Finally
                                      transactionScope.Dispose()
                                  End Try

                              End Sub).Wait()


    End Sub

    Private Const PaperClaimsDocumentPath As String = "IO.Practiceware.DbMigration.Documents."

    Public Shared Function GetDocumentBytes(documentName As String) As Byte()
        Dim path = PaperClaimsDocumentPath & documentName

        Dim assembly1 = Bootstrapper.TryLoadAssembly("IO.Practiceware.DbMigration")

        Try
            Using stream = assembly1.GetManifestResourceStream(path)
                Return stream.ToArray()
            End Using
        Catch
            Return Nothing
        End Try
    End Function

    Private Sub scheduleAppointment_Click(sender As Object, e As EventArgs) Handles scheduleAppointment.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.ScheduleAppointment.ScheduleAppointmentViewManager, IO.Practiceware.Presentation", Nothing).Instance

        Dim patientId As Integer
        Integer.TryParse(InteractionManager.Current.Prompt("Enter PATIENT ID").Input, patientId)

        Dim source As String
        source = InteractionManager.Current.Prompt("Enter either APPOINTMENT ID or SCHEDULE BLOCK ID" + Environment.NewLine + "Please prefix with A or S (e.g A=12345 or S=987)").Input

        If (Not IsNothing(source)) Then
            Dim array As String()
            array = source.Split("="c)

            Dim id As Integer
            Integer.TryParse(array(1), id)

            Dim categoryId As Integer
            If array(0).Equals("S", StringComparison.OrdinalIgnoreCase) Then
                Integer.TryParse(InteractionManager.Current.Prompt("Enter optional CATEGORY ID").Input, categoryId)
            End If

            If patientId = 0 OrElse id = 0 OrElse ((Not array(0).Equals("A", StringComparison.OrdinalIgnoreCase)) AndAlso (Not array(0).Equals("S", StringComparison.OrdinalIgnoreCase))) Then
                InteractionManager.Current.Alert("PATIENT ID and APPOINTMENT ID or SCHEDULE BLOCK ID required")
                Return
            End If

            Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.ScheduleAppointment.ScheduleAppointmentLoadArguments, IO.Practiceware.Presentation", Nothing).Instance
            loadArguments.PatientId = patientId
            loadArguments.AppointmentCategoryId = If(categoryId = 0, New Integer?(), categoryId)

            If array(0).Equals("A", StringComparison.OrdinalIgnoreCase) Then
                loadArguments.AppointmentId = id
            End If

            If array(0).Equals("S", StringComparison.OrdinalIgnoreCase) Then
                loadArguments.ScheduleBlockId = id
            End If

            Dim returnArguments = manager.ShowScheduleAppointment(loadArguments)

            If returnArguments Is Nothing OrElse returnArguments.AppointmentId Is Nothing Then
                InteractionManager.Current.Alert("No appointment created")
            Else
                Dim msg As String
                msg = [String].Format("Appointment created (or already existing) with ID = {0}", returnArguments.AppointmentId)
                InteractionManager.Current.Alert(msg)
            End If

        End If

    End Sub

    Private Sub btnPreAuthorizations_Click(sender As Object, e As EventArgs) Handles btnPreAuthorizations.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization.PatientInsuranceAuthorizationsViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization.PatientInsuranceAuthorizationsLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        Dim authId As Long
        Long.TryParse(InteractionManager.Current.Prompt("Enter a patient insurance authorization Id").Input, authId)

        If authId = 0 Then
            Dim encounterId As Long
            Long.TryParse(InteractionManager.Current.Prompt("Enter an encounter Id").Input, encounterId)

            If encounterId = 0 Then
                MessageBox.Show("You must enter a valid authorization id or a valid encounter id")
                Return
            Else
                loadArguments.EncounterId = encounterId
            End If
        Else
            loadArguments.PatientInsuranceAuthorizationId = authId
        End If

        manager.ShowPatientInsuranceAuthorization(loadArguments)
    End Sub

    Private Sub btnReferralScreen_Click(sender As Object, e As EventArgs) Handles btnReferralScreen.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Referrals.ReferralsViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.Referrals.ReferralLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        If (Not DateTime.TryParse(InteractionManager.Current.Prompt("Enter an appointment date (e.g. MM/DD/YYYY").Input, CDate(loadArguments.AppointmentDate))) Then
            Return
        End If

        Dim referralId As Integer

        Integer.TryParse(InteractionManager.Current.Prompt("Enter a referral Id").Input, referralId)

        If referralId = 0 Then
            Dim patientId As Integer
            Integer.TryParse(InteractionManager.Current.Prompt("Enter an patient Id").Input, patientId)
            If patientId = 0 Then
                Return
            Else
                loadArguments.PatientId = patientId
            End If
        Else
            loadArguments.ReferralId = referralId
        End If

        manager.ShowReferrals(loadArguments)
    End Sub

    Private Sub btnRecalls_Click(sender As Object, e As EventArgs) Handles btnRecalls.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Recalls.RecallsViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.Recalls.RecallLoadArguments, IO.Practiceware.Presentation", Nothing).Instance


        Dim patientId As Integer
        Integer.TryParse(InteractionManager.Current.Prompt("Enter an patient Id").Input, patientId)
        If patientId = 0 Then
            Return
        Else
            loadArguments.PatientId = patientId
        End If

        manager.ShowRecalls(loadArguments)
    End Sub

    Private Sub patientSearch_Click(sender As Object, e As EventArgs) Handles patientSearch.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientSearch.PatientSearchViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim returnArguments = manager.ShowPatientSearchView()
        If returnArguments Is Nothing Then
            InteractionManager.Current.Alert("No patient selected")
        Else
            InteractionManager.Current.Alert(returnArguments.PatientId)
        End If
    End Sub

    Private Sub btnPatientInfo_Click(sender As Object, e As EventArgs) Handles btnPatientInfo.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        Dim patientId As Integer
        Dim promptResult = InteractionManager.Current.Prompt("Enter an patient Id")

        If promptResult.DialogResult.HasValue And promptResult.DialogResult.Value Then
            Dim parsedCorrectly = Integer.TryParse(promptResult.Input, patientId)

            If parsedCorrectly Then
                loadArguments.PatientId = patientId
            End If

            manager.ShowPatientInfo(loadArguments)
        End If

    End Sub

    Private Sub btnRescheduleAppointment_Click(sender As Object, e As EventArgs) Handles btnRescheduleAppointment.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.RescheduleAppointment.RescheduleAppointmentViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.RescheduleAppointment.RescheduleAppointmentLoadArguments, IO.Practiceware.Presentation", Nothing).Instance


        Dim input = InteractionManager.Current.Prompt("Enter appointment Ids separated by a comma").Input

        Dim separator As Char() = New Char() {","c}

        Dim strArray As String() = input.Split(separator)

        Dim appointmentIds As List(Of Integer) = New List(Of Integer)

        For Each str2 In strArray
            appointmentIds.Add(Integer.Parse(str2))
        Next

        loadArguments.AppointmentIds = appointmentIds

        manager.ShowRescheduleAppointment(loadArguments)
    End Sub

    Private Sub cancelAppointment_Click(sender As Object, e As EventArgs) Handles cancelAppointment.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.CancelAppointment.CancelAppointmentViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.CancelAppointment.CancelAppointmentLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        Dim appointmentId As Integer
        Integer.TryParse(InteractionManager.Current.Prompt("Enter an Appointment Id").Input, appointmentId)
        If appointmentId = 0 Then
            Return
        Else
            Dim appointmentIds As List(Of Integer) = New List(Of Integer)
            appointmentIds.Add(appointmentId)
            loadArguments.ListOfAppointmentId = appointmentIds
        End If

        Dim returnArguments = manager.ShowCancelAppointment(loadArguments)
        If returnArguments Is Nothing Then
            InteractionManager.Current.Alert("No Appointment Canceled")
        Else
            Dim message As String = Nothing
            For Each id As Integer In DirectCast(returnArguments.ListOfAppointmentId, IEnumerable)
                message = message + "[" + id.ToString() + "]"
            Next
            InteractionManager.Current.Alert(message)
        End If
    End Sub

    Private Sub btnSubmitTransactions_Click(sender As Object, e As EventArgs) Handles btnSubmitTransactions.Click

        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.SubmitTransactions.SubmitTransactionsViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.SubmitTransactions.SubmitTransactionsLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        loadArguments.DefaultClaimType = ClaimType.EClaims

        manager.ShowSubmitTransactions(loadArguments)
    End Sub

    Private Sub glaucomaMonitorDemo_Click(sender As Object, e As EventArgs) Handles glaucomaMonitorDemo.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.GlaucomaMonitor.GlaucomaMonitorViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowGlaucomaMonitor()

    End Sub

    Private Sub btnSetConfiguration_Click(sender As Object, e As EventArgs) Handles btnSetConfiguration.Click
        Dim frm = New ConfigurationsSettings()
        frm.ShowDialog()
    End Sub

    Private Sub UserPermissionsButton_Click(sender As Object, e As EventArgs) Handles UserPermissionsButton.Click
        Dim input = InteractionManager.Current.Prompt("Enter a User Id").Input
        Dim userId As Integer
        If (Integer.TryParse(input, userId)) Then
            Dim comWrapper As New ComWrapper()
            Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Permissions.PermissionsViewManager, IO.Practiceware.Presentation", Nothing).Instance
            Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.Permissions.PermissionLoadArguments, IO.Practiceware.Presentation", Nothing).Instance
            loadArguments.UserId = userId
            manager.ShowPermissionsView(loadArguments)
        End If
    End Sub

    Private Sub WizardButton_Click(sender As Object, e As EventArgs)
        Dim comWrapper As New ComWrapper()
        Dim navigableInteractionManager = comWrapper.Create("Soaf.Presentation.NavigableInteractionManager, Soaf", Nothing).Instance
        Dim navigationArguments = comWrapper.Create("Soaf.Presentation.NavigableInteractionArguments, Soaf", Nothing).Instance
        Dim sampleNavigationView = comWrapper.Create("IO.Practiceware.Presentation.Views.Common.SampleWizardView, IO.Practiceware.Presentation", Nothing).Instance

        navigationArguments.Content = sampleNavigationView
        navigableInteractionManager.Show(navigationArguments)
    End Sub

    Private Sub AddInsurancePolicy_Click(sender As Object, e As EventArgs) Handles AddInsurancePolicyButton.Click
        Dim input = InteractionManager.Current.Prompt("Enter a patient Id").Input
        If input IsNot Nothing Then
            Dim patientId = Integer.Parse(input)

            Dim comWrapper As New ComWrapper()
            Dim navigableInteractionManager = comWrapper.Create("Soaf.Presentation.INavigableInteractionManager, Soaf", Nothing).Instance
            Dim addPolicySelectionView = comWrapper.Create("IO.Practiceware.Presentation.Views.InsurancePolicy.AddPolicySelectionView, IO.Practiceware.Presentation", Nothing).Instance
            Dim navigationArguments = comWrapper.Create("Soaf.Presentation.NavigableInteractionArguments, Soaf", Nothing).Instance

            Dim genericInteractionContextType = GetType(IInteractionContext(Of InsurancePolicyNavigationViewModel))
            Dim genericInteractionContext = comWrapper.Create(genericInteractionContextType.AssemblyQualifiedName, Nothing).Instance

            genericInteractionContext.Context.PatientId = patientId
            genericInteractionContext.Context.InsuranceType = InsuranceType.Vision
            navigationArguments.InteractionContext = genericInteractionContext

            navigationArguments.Content = addPolicySelectionView
            navigableInteractionManager.Show(navigationArguments)
        End If
    End Sub

    Private Sub ManageInsurancePlansButton_Click(sender As Object, e As EventArgs) Handles ManageInsurancePlansButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.InsurancePlansSetup.InsurancePlansSetupViewManager, IO.Practiceware.Presentation", Nothing).Instance
        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.Views.InsurancePlansSetup.ManageInsurancePlansLoadArguments, IO.Practiceware.Presentation", Nothing).Instance

        manager.ManageInsurancePlans(loadArguments)
    End Sub

    Private Sub btnLoginView_Click(sender As Object, e As EventArgs) Handles btnLoginView.Click
        UserContext.Current.Logout()

        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Login.LoginViewManager, IO.Practiceware.Presentation", Nothing).Instance

        manager.ShowLoginScreen()

        SetCurrentServer()

    End Sub

    Private Sub ClinicalDataFilesButtonClick(sender As Object, e As EventArgs) Handles ClinicalDataFilesButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager =
                comWrapper.Create(
                    "IO.Practiceware.Presentation.Views.ClinicalDataFiles.ClinicalDataFilesViewManager, IO.Practiceware.Presentation",
                    Nothing).Instance
        manager.ShowClinicalDataFilesGenerator()
    End Sub

    Private Sub HomeScreenButton_Click(sender As Object, e As EventArgs) Handles HomeScreenButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Home.HomeScreenViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowHome()
    End Sub

    Private Sub PatientDemographicsSetupButtonClick(sender As Object, e As EventArgs) Handles PatientDemographicsSetupButton.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientDemographicsSetupViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowPatientDemographicsSetup()
    End Sub

    Private Sub btnImportClinicalFiles_Click(sender As Object, e As EventArgs) Handles btnImportClinicalFiles.Click
        Dim comWrapper As New ComWrapper
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.ClinicalDataFiles.ClinicalDataFilesViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowClinicalDataFilesImportView()
    End Sub

    Private Sub NewCropMUReportButton_Click(sender As Object, e As EventArgs) Handles NewCropMUReportButton.Click
        SqlClr.Common.ConnectionString = "Data Source=dev-sql03;Initial Catalog=PracticeRepositoryMeaningfulUsePartDeux;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"
        NewCropFunctions.GenerateMeaningfulUseUtilizationReport("demo", "D", "3", New DateTime(2013, 12, 9), New DateTime(2013, 12, 11), SqlInt32.Null, SqlString.Null, 0, "Y", SqlString.Null)
    End Sub

    Private Sub GetNewCropMUReportButton_Click(sender As Object, e As EventArgs) Handles GetNewCropMUReportButton.Click
        SqlClr.Common.ConnectionString = "Data Source=dev-sql03;Initial Catalog=PracticeRepositoryMeaningfulUsePartDeux;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=1200"
        NewCropFunctions.GetMeaningfulUseUtilizationReport(InteractionManager.Current.Prompt("Enter transaction id").Input, InteractionManager.Current.Prompt("Enter doctor id").Input, "D", Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

    End Sub

    Private Sub ToolStripComboBoxCurrentConnection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ToolStripComboBoxCurrentConnection.SelectedIndexChanged

        If CloudModeSelected Then
            If ConfigurationManager.ApplicationServerClientConfiguration.HostName.IsNotNullOrEmpty() Then
                txtServerName.Text = ConfigurationManager.ApplicationServerClientConfiguration.HostName
            End If
            txtDatabaseName.Visible = False
            txtServerName.ToolTipText = "Enter the web server name (e.g. localhost, staging.iopracticeware.com)"
        Else
            If ConfigurationManager.PracticeRepositoryConnectionString.IsNotNullOrEmpty() And ConfigurationManager.PracticeRepositoryConnectionString <> ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey Then
                Dim sqlBuilder As New SqlConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString)
                txtServerName.Text = If(sqlBuilder.DataSource.IsNullOrEmpty(), txtServerName.Text, sqlBuilder.DataSource)
                txtDatabaseName.Text = If(sqlBuilder.InitialCatalog.IsNullOrEmpty(), txtDatabaseName.Text, sqlBuilder.InitialCatalog)
            ElseIf ConfigurationManager.PreviousPracticeRepositoryConnectionString.IsNotNullOrEmpty() Then
                Dim sqlBuilder As New SqlConnectionStringBuilder(ConfigurationManager.PreviousPracticeRepositoryConnectionString)
                txtServerName.Text = If(sqlBuilder.DataSource.IsNullOrEmpty(), txtServerName.Text, sqlBuilder.DataSource)
                txtDatabaseName.Text = If(sqlBuilder.InitialCatalog.IsNullOrEmpty(), txtDatabaseName.Text, sqlBuilder.InitialCatalog)
            End If
            txtDatabaseName.Visible = True
            txtServerName.ToolTipText = "Enter the data source name (e.g. localhost, dev-sql01)"
        End If
    End Sub

    Private ReadOnly _timerForLabel As Timer = New Timer()
    Private mCurrentServer As String
    Private Const CLOUD_MODE As String = "Cloud"
    Private Const DATABASE_MODE As String = "Sql Server"

    Private ReadOnly Property CloudModeSelected As Boolean
        Get
            Return SelectedConnectionType = CLOUD_MODE
        End Get
    End Property

    Private ReadOnly Property DatabaseModeSelected As Boolean
        Get
            Return SelectedConnectionType = DATABASE_MODE
        End Get
    End Property

    Private WithEvents _RecentCloudConnectionInfo As ObservableCollection(Of String) = New ObservableCollection(Of String)
    Private WithEvents _RecentDatabaseConnectionInfo As ObservableCollection(Of String) = New ObservableCollection(Of String)

    Private ReadOnly Property SelectedConnectionType As String
        Get
            Return If(ToolStripComboBoxCurrentConnection.SelectedItem IsNot Nothing, ToolStripComboBoxCurrentConnection.SelectedItem.ToString(), "")
        End Get
    End Property

    Private Function SwitchToApplicationServer(serverName As String) As Boolean

        ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = True
        ConfigurationManager.ApplicationServerClientConfiguration.HostName = serverName
        ConfigurationManager.PracticeRepositoryConnectionString = ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey
        ConfigurationManager.Save()

        Dim IoUserId = UserContext.Current.Login("9911")
        If (IoUserId = -1) Then
            Return False
        End If

        _RecentCloudConnectionInfo.Add(serverName)

        Return True
    End Function

    Private Function SwitchToSqlServer(sqlConnectionString As String) As Boolean

        Dim builder = New SqlConnectionStringBuilder()
        Try
            builder.ConnectionString = sqlConnectionString
            ' throws if invalid connection string
            ' manually set the timout
            builder.ConnectTimeout = 5
            Using conn = New SqlConnection(builder.ToString())
                ' throws if invalid
                conn.Open()
            End Using
        Catch
            Return False
        End Try

        ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = False
        ConfigurationManager.PracticeRepositoryConnectionString = sqlConnectionString
        ConfigurationManager.Save()

        ' Store current connection info in memory
        _RecentDatabaseConnectionInfo.Add(sqlConnectionString)
        Return True
    End Function

    Private Sub btnConnectAndSave_Click(sender As Object, e As EventArgs) Handles btnConnectAndSave.Click
        If CloudModeSelected Then
            If (Not ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled Or (txtServerName.Text IsNot Nothing And ConfigurationManager.ApplicationServerClientConfiguration.HostName <> txtServerName.Text)) Then
                Dim success = SwitchToApplicationServer(txtServerName.Text)
                SetLabelAndTimerSuccess(String.Format("{0} ({1})", ConfigurationManager.ApplicationServerClientConfiguration.HostName.ToString(), CLOUD_MODE), success)
            End If
        ElseIf DatabaseModeSelected Then
            Dim scb As SqlConnectionStringBuilder = New SqlConnectionStringBuilder

            If (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled) Then
                If DbConnections.TryGetConnectionStringBuilder(ConfigurationManager.DefaultConnectionStringFormat, scb) Then
                    scb.InitialCatalog = txtDatabaseName.Text
                    scb.DataSource = txtServerName.Text
                    Dim success = SwitchToSqlServer(scb.ConnectionString)
                    SetLabelAndTimerSuccess(String.Format("{0} - {1} ({2})", scb.DataSource, scb.InitialCatalog, DATABASE_MODE), success)
                End If
            Else
                If DbConnections.TryGetConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString, scb) Then
                    If (scb.InitialCatalog <> txtDatabaseName.Text Or scb.DataSource <> txtServerName.Text) Then
                        scb.InitialCatalog = txtDatabaseName.Text
                        scb.DataSource = txtServerName.Text
                        Dim success = SwitchToSqlServer(scb.ConnectionString)
                        SetLabelAndTimerSuccess(String.Format("{0} - {1} ({2})", scb.DataSource, scb.InitialCatalog, DATABASE_MODE), success)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub SetLabelAndTimerSuccess(mMessage As String, mSuccess As Boolean)
        mCurrentServer = mMessage
        _timerForLabel.Interval = 2700
        _timerForLabel.Enabled = True
        _timerForLabel.Start()
        lblCurrentServer.Text = If(mSuccess, "Connect Success!", "Connect Failed!")
        lblCurrentServer.IsLink = False
        lblCurrentServer.ForeColor = If(mSuccess, Color.LimeGreen, Color.Red)
    End Sub

    Private Sub TimerForLabel_Tick(sender As Object, e As EventArgs)
        lblCurrentServer.Text = mCurrentServer
        lblCurrentServer.IsLink = True
        lblCurrentServer.LinkColor = Color.Blue
        _timerForLabel.Stop()
        _timerForLabel.Enabled = False
    End Sub

    Private Sub MainControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        Dim sqlBuilder As SqlConnectionStringBuilder = Nothing

        ' initialize the connection controls with the current values from the configuration manager
        If (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled) Then
            ' cloud mode
            ToolStripComboBoxCurrentConnection.SelectedIndex = ToolStripComboBoxCurrentConnection.Items.IndexOf(CLOUD_MODE)
            lblCurrentServer.Text = String.Format("{0} ({1})", ConfigurationManager.ApplicationServerClientConfiguration.HostName.ToString(), CLOUD_MODE)
            _RecentCloudConnectionInfo.Add(ConfigurationManager.ApplicationServerClientConfiguration.HostName.ToString())
        Else
            ' sql server mode
            ToolStripComboBoxCurrentConnection.SelectedIndex = ToolStripComboBoxCurrentConnection.Items.IndexOf(DATABASE_MODE)
            sqlBuilder = New SqlConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString)
            lblCurrentServer.Text = If(sqlBuilder IsNot Nothing And sqlBuilder.DataSource.IsNotNullOrEmpty(), String.Format("{0} - {1} ({2})", sqlBuilder.DataSource, sqlBuilder.InitialCatalog, DATABASE_MODE), "(Not Connected)")
            _RecentDatabaseConnectionInfo.Add(sqlBuilder.ToString())
        End If

        If (sqlBuilder Is Nothing) Then
            sqlBuilder = New SqlConnectionStringBuilder(ConfigurationManager.DefaultConnectionStringFormat)
            sqlBuilder.InitialCatalog = ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey
        End If

        _RecentCloudConnectionInfo.Add("localhost")

        sqlBuilder.DataSource = "localhost"
        _RecentDatabaseConnectionInfo.Add(sqlBuilder.ToString())
        sqlBuilder.DataSource = "dev-sql01"
        _RecentDatabaseConnectionInfo.Add(sqlBuilder.ToString())
        sqlBuilder.DataSource = "dev-sql02"
        _RecentDatabaseConnectionInfo.Add(sqlBuilder.ToString())
        sqlBuilder.DataSource = "dev-sql03"
        _RecentDatabaseConnectionInfo.Add(sqlBuilder.ToString())

        txtServerName.AutoCompleteCustomSource.AddRange(_RecentCloudConnectionInfo)

    End Sub

    Private _SyncAutoCompleteInProgress As Boolean = False
    Private Sub RecentCloudConnectionInfoCollectionsChanged(sender As Object, args As NotifyCollectionChangedEventArgs) Handles _RecentCloudConnectionInfo.CollectionChanged

        If _SyncAutoCompleteInProgress Then
            Return
        End If

        _SyncAutoCompleteInProgress = True

        If CloudModeSelected Then
            txtServerName.AutoCompleteCustomSource.Clear()
            txtServerName.AutoCompleteCustomSource.AddRange(_RecentCloudConnectionInfo.Distinct())
            _RecentCloudConnectionInfo.Clear()
            _RecentCloudConnectionInfo.AddRange(txtServerName.AutoCompleteCustomSource)
        End If

        _SyncAutoCompleteInProgress = False
    End Sub

    Private Sub RecentDatabaseConnectionInfoCollectionsChanged(sender As Object, args As NotifyCollectionChangedEventArgs) Handles _RecentDatabaseConnectionInfo.CollectionChanged
        If _SyncAutoCompleteInProgress Then
            Return
        End If

        _SyncAutoCompleteInProgress = True

        If DatabaseModeSelected Then
            txtServerName.AutoCompleteCustomSource.Clear()
            Dim scb As SqlConnectionStringBuilder
            Dim list As List(Of String) = New List(Of String)
            For Each item As String In _RecentDatabaseConnectionInfo.Distinct()
                scb = New SqlConnectionStringBuilder(item)
                txtServerName.AutoCompleteCustomSource.Add(scb.DataSource)
                txtDatabaseName.AutoCompleteCustomSource.Add(scb.InitialCatalog)
                list.Add(item)
            Next

            _RecentDatabaseConnectionInfo.Clear()
            _RecentDatabaseConnectionInfo.AddRange(list)
        End If

        _SyncAutoCompleteInProgress = False
    End Sub


    Private Sub ToolStripLabelCurrentActiveConnection_Click(sender As Object, e As EventArgs) Handles lblCurrentConnection.Click
        btnLoginView_Click(sender, e)
    End Sub

    Private Sub SetCurrentServer()
        If ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled Then
            lblCurrentServer.Text = String.Format("{0} ({1})", ConfigurationManager.ApplicationServerClientConfiguration.HostName.ToString(), CLOUD_MODE)
        Else
            Dim scb = New SqlConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString)
            lblCurrentServer.Text = String.Format("{0} - {1} ({2})", scb.DataSource, scb.InitialCatalog, DATABASE_MODE)
        End If
    End Sub

    Private Sub txtDatabaseName_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDatabaseName.KeyUp
        If (e.KeyCode = Keys.Enter) Then
            btnConnectAndSave_Click(sender, e)
        End If
    End Sub


    Private Sub lblCurrentServer_Click(sender As Object, e As EventArgs) Handles lblCurrentServer.Click
        btnLoginView_Click(sender, e)
    End Sub

    Private Shared Sub PatientStatementsButton_Click(sender As Object, e As EventArgs) Handles PatientStatementsButton.Click
        Dim viewManager As PatientStatementsViewManager
        viewManager = ServiceProvider.GetService(Of PatientStatementsViewManager)()
        Dim patientId As Integer
        Dim promptResult = InteractionManager.Current.Prompt("Enter an patient Id")

        If promptResult.DialogResult.HasValue And promptResult.DialogResult.Value Then
            Dim parsedCorrectly = Integer.TryParse(promptResult.Input, patientId)

            If parsedCorrectly Then
                viewManager.ShowPatientStatements(patientId) '15581
            End If
        End If
    End Sub

    Private Sub StatementsButton_Click(sender As Object, e As EventArgs) Handles StatementsButton.Click
        InteractionManager.Current.ShowModal(New WindowInteractionArguments With {.Content = New GenerateStatementsView(), .ResizeMode = Windows.ResizeMode.CanResize})
    End Sub

    Private Sub BillingNotes_Click(sender As Object, e As EventArgs) Handles BillingNotes.Click
        Dim input = InteractionManager.Current.Prompt("Enter a patient Id").Input
        If input IsNot Nothing Then
            Dim patientId = Integer.Parse(input)
            Dim comWrapper As New ComWrapper()
            Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.ViewModels.Notes.NoteLoadArguments", Nothing).Instance
            loadArguments.EntityId = patientId
            loadArguments.NoteTypeId = 1
            Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Notes.NoteViewManager", Nothing).Instance
            manager.LaunchManageNotesView(loadArguments)
        End If
    End Sub

    Private Sub ApplicationSetup_Click(sender As Object, e As EventArgs) Handles ApplicationSetup.Click
        Dim comWrapper As New ComWrapper()
        comWrapper.Create("IO.Practiceware.Presentation.Views.Setup.ApplicationSetupViewManager", Nothing)
        Dim instance = comWrapper.Instance
        instance.ShowApplicationSetup()
    End Sub

    Private Sub ExamButtonView_Click(sender As Object, e As EventArgs) Handles BtnExamBillingView.Click
        ''Dim patientInput = InteractionManager.Current.Prompt("Enter a patient Id").Input
        Dim patientId = 6860
        'If patientInput IsNot Nothing Then
        '    patientId = Integer.Parse(patientInput)
        'End If

        'Dim encounterInput = InteractionManager.Current.Prompt("Enter a Encounter Id").Input
        Dim encounterId = 39869
        'If encounterInput IsNot Nothing Then
        '    encounterId = Integer.Parse(encounterInput)
        'End If

        Dim comWrapper As New ComWrapper()
        Dim listdiagnosesLoadArguments As New List(Of IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments)
        Dim listdiagnoses = New String() {"373.00.69"}

        Dim diaglogArg = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg.DiagnosisCode = "362.17" '"373.00.69"
        diaglogArg.EyeLidLocation = ""
        diaglogArg.EyeContext = "OD"
        diaglogArg.DiagnosisDescription = "Avulsed Vessel"
        listdiagnosesLoadArguments.Add(diaglogArg)

        Dim diaglogArg1 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg1.DiagnosisCode = "364.05"
        diaglogArg1.EyeLidLocation = ""
        diaglogArg1.DiagnosisDescription = "HypopyOn"
        diaglogArg1.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg1)

        Dim diaglogArg2 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg2.DiagnosisCode = "362.02"
        diaglogArg2.EyeLidLocation = ""
        diaglogArg2.DiagnosisDescription = "PDR"
        diaglogArg2.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg2)


        Dim diaglogArg3 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg3.DiagnosisCode = "250.51"
        diaglogArg3.EyeLidLocation = ""
        diaglogArg3.DiagnosisDescription = "diabetes type 1,w"
        diaglogArg3.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg3)

        Dim diaglogArg4 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg4.DiagnosisCode = "379.31"
        diaglogArg4.EyeLidLocation = ""
        diaglogArg4.DiagnosisDescription = "Aphakia"
        diaglogArg4.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg4)

        Dim diaglogArg5 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg5.DiagnosisCode = "366.15"
        diaglogArg5.EyeLidLocation = ""
        diaglogArg5.DiagnosisDescription = "ortical senile cataract"
        diaglogArg5.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg5)


        Dim diaglogArg6 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg6.DiagnosisCode = "366.16"
        diaglogArg6.EyeLidLocation = ""
        diaglogArg6.DiagnosisDescription = "Senile nuclear sclerosis"
        diaglogArg6.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg6)


        Dim diaglogArg7 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg7.DiagnosisCode = "366.53"
        diaglogArg7.EyeLidLocation = ""
        diaglogArg7.DiagnosisDescription = "After-cataract obscuring vision"
        diaglogArg7.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg7)

        Dim diaglogArg8 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg8.DiagnosisCode = "366.52"
        diaglogArg8.EyeLidLocation = ""
        diaglogArg8.DiagnosisDescription = "other after-cataract not obscuring vision"
        diaglogArg8.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg8)

        Dim diaglogArg9 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        diaglogArg9.DiagnosisCode = "379.23"
        diaglogArg9.EyeLidLocation = ""
        diaglogArg9.DiagnosisDescription = "Vitreous hemorrhage"
        diaglogArg9.EyeContext = "OS"
        listdiagnosesLoadArguments.Add(diaglogArg9)

        'Dim diaglogArg10 = New IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments()
        'diaglogArg10.DiagnosisCode = "P304"
        'diaglogArg10.EyeLidLocation = ""
        'diaglogArg10.DiagnosisDescription = "Aphakia"
        'diaglogArg10.EyeContext = "OS"
        'listdiagnosesLoadArguments.Add(diaglogArg10)


        Dim loadArguments = comWrapper.Create("IO.Practiceware.Presentation.ViewModels.StandardExam.StandardExamLoadArguments", Nothing).Instance
        loadArguments.PatientId = patientId
        loadArguments.EncounterId = encounterId
        loadArguments.DiagnosesLoadArguments = listdiagnosesLoadArguments
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.StandardExam.StandardExamViewManager", Nothing).Instance
        manager.ShowStandardExam(loadArguments)
    End Sub

    Private Sub ClinicalSetup_Click(sender As Object, e As EventArgs) Handles ClinicalSetup.Click
        Dim comWrapper As New ComWrapper()
        comWrapper.Create("IO.Practiceware.Presentation.Views.Setup.Clinical.ClinicalSetupViewManager", Nothing)
        Dim instance = comWrapper.Instance
        instance.ShowApplicationSetup()
    End Sub

    Private Sub AdminstrativeSetupButton_Click(sender As Object, e As EventArgs) Handles AdminstrativeSetupButton.Click
        Dim comWrapper As New ComWrapper()
        Dim manager = comWrapper.Create("IO.Practiceware.Presentation.Views.Setup.ApplicationSetup.AdministrativeSetup.AdministrativeSetupViewManager, IO.Practiceware.Presentation", Nothing).Instance
        manager.ShowAdministrativeSetup()
    End Sub
End Class

Public Class OpenLegacyPatientScreenEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        Dim args As PatientInfoScreenOpenEventArgs
        args = DirectCast(e, PatientInfoScreenOpenEventArgs)
        InteractionManager.Current.Alert(String.Format("Will open {0} for patient id: {1}", args.ScreenToOpen, args.PatientId))
    End Sub

End Class

Public Class Vb6FeatureExecutionEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        Dim featureKey As String = DirectCast(e, FeatureExectionRequestEventArgs).FeatureKey
        If (featureKey = "Application_LogOut") Then
            InteractionManager.Current.Alert("Log out requested. We will simulate it by closing all app windows (and thus quiting app)")
            Dim comWrapper As New ComWrapper
            comWrapper.CloseAllActiveForms()
        Else
            InteractionManager.Current.Alert("Following will open in VB6: " + featureKey)
        End If
    End Sub

End Class

Public Class OpenMachineConfigurationEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub
End Class

Public Class LoadPlanFormEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub

End Class

'Public Class LoadImpressionFormEventHandler
'    Implements IEventHandler
'    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
'        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
'    End Sub
'End Class


Public Class LoadPatientFormEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub

End Class

Public Class LoadImpressionFormEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub

End Class

Public Class LoadChartFormEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub

End Class

Public Class LoadHighlightFormEventHandler
    Implements IEventHandler
    Public Sub OnEvent(sender As Object, e As Object) Implements IEventHandler.OnEvent
        InteractionManager.Current.Alert("This will open Machine Configuration Screen of VB6")
    End Sub

End Class



