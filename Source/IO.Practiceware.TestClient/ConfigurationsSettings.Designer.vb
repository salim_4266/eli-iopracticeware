﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigurationsSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSetConfiguration = New System.Windows.Forms.Button()
        Me.txtElectronicClaimDir = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPaperClaimDirectory = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnBrowse1 = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.btnBrowse2 = New System.Windows.Forms.Button()
        Me.txtMachineName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnSetConfiguration
        '
        Me.btnSetConfiguration.Location = New System.Drawing.Point(146, 230)
        Me.btnSetConfiguration.Name = "btnSetConfiguration"
        Me.btnSetConfiguration.Size = New System.Drawing.Size(75, 23)
        Me.btnSetConfiguration.TabIndex = 0
        Me.btnSetConfiguration.Text = "Save"
        Me.btnSetConfiguration.UseVisualStyleBackColor = True
        '
        'txtElectronicClaimDir
        '
        Me.txtElectronicClaimDir.Location = New System.Drawing.Point(146, 98)
        Me.txtElectronicClaimDir.Name = "txtElectronicClaimDir"
        Me.txtElectronicClaimDir.Size = New System.Drawing.Size(434, 20)
        Me.txtElectronicClaimDir.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Electronic Claims Directory"
        '
        'txtPaperClaimDirectory
        '
        Me.txtPaperClaimDirectory.Location = New System.Drawing.Point(146, 139)
        Me.txtPaperClaimDirectory.Name = "txtPaperClaimDirectory"
        Me.txtPaperClaimDirectory.Size = New System.Drawing.Size(434, 20)
        Me.txtPaperClaimDirectory.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 139)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Paper Claims Directory"
        '
        'btnBrowse1
        '
        Me.btnBrowse1.Location = New System.Drawing.Point(586, 96)
        Me.btnBrowse1.Name = "btnBrowse1"
        Me.btnBrowse1.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse1.TabIndex = 5
        Me.btnBrowse1.Text = "Browse..."
        Me.btnBrowse1.UseVisualStyleBackColor = True
        '
        'btnBrowse2
        '
        Me.btnBrowse2.Location = New System.Drawing.Point(586, 139)
        Me.btnBrowse2.Name = "btnBrowse2"
        Me.btnBrowse2.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse2.TabIndex = 6
        Me.btnBrowse2.Text = "Browse..."
        Me.btnBrowse2.UseVisualStyleBackColor = True
        '
        'txtMachineName
        '
        Me.txtMachineName.Location = New System.Drawing.Point(145, 191)
        Me.txtMachineName.Name = "txtMachineName"
        Me.txtMachineName.Size = New System.Drawing.Size(434, 20)
        Me.txtMachineName.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(60, 194)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Machine Name"
        '
        'ConfigurationsSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 402)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtMachineName)
        Me.Controls.Add(Me.btnBrowse2)
        Me.Controls.Add(Me.btnBrowse1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPaperClaimDirectory)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtElectronicClaimDir)
        Me.Controls.Add(Me.btnSetConfiguration)
        Me.Name = "ConfigurationsSettings"
        Me.Text = "ConfigurationsSettings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSetConfiguration As System.Windows.Forms.Button
    Friend WithEvents txtElectronicClaimDir As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPaperClaimDirectory As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse1 As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btnBrowse2 As System.Windows.Forms.Button
    Friend WithEvents txtMachineName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
