﻿
namespace IO.Practiceware.Model
{
    /// <summary>
    /// How to handle operations when an item isn't found.
    /// </summary>
    public enum NotFoundBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        None,
        /// <summary>
        /// 
        /// </summary>
        Exception,
        /// <summary>
        /// 
        /// </summary>
        Add
    }
}
