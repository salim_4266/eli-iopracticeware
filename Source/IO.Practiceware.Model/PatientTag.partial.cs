﻿using Soaf;
using Soaf.Reflection;

namespace IO.Practiceware.Model
{
    public partial class PatientTag : IHasOrdinalId
    {
        public PatientTag()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => Patient.IfNotNull(p => p.PatientTags),
               Reflector.GetMember(() => Patient).Name);
        }
    }
}
