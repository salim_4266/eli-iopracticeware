﻿namespace IO.Practiceware.Model
{
    public partial class PaymentMethod
    {
        // Match these non case-sensitive

        public const string Check = "Check";
        public const string Cash = "Cash";
    }
}
