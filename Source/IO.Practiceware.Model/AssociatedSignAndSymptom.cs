//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Associated signs and symptoms describe the symptom/pain and other things that happen when this symptom/pain occurs (chest pain leads to shortness of breath, headache leads to vision constriction).
    /// </summary>
    public partial class AssociatedSignAndSymptom : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;

        #endregion

        #region Navigation Properties
    
    	[Association("EncounterReasonForVisits", "", "")]
        public virtual System.Collections.Generic.ICollection<EncounterReasonForVisit> EncounterReasonForVisits
        {
            get
            {
                if (_encounterReasonForVisits == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<EncounterReasonForVisit>();
                    newCollection.ItemSet += FixupEncounterReasonForVisitsItemSet;
                    newCollection.ItemRemoved += FixupEncounterReasonForVisitsItemRemoved;
                    _encounterReasonForVisits = newCollection;
                }
                return _encounterReasonForVisits;
            }
            set
            {
                if (!ReferenceEquals(_encounterReasonForVisits, value))
                {
                    var previousValue = _encounterReasonForVisits as Soaf.Collections.FixupCollection<EncounterReasonForVisit>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupEncounterReasonForVisitsItemSet;
                        previousValue.ItemRemoved -= FixupEncounterReasonForVisitsItemRemoved;
                    }
                    _encounterReasonForVisits = value;
                    var newValue = value as Soaf.Collections.FixupCollection<EncounterReasonForVisit>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupEncounterReasonForVisitsItemSet;
                        newValue.ItemRemoved += FixupEncounterReasonForVisitsItemRemoved;
                    }
    				OnPropertyChanged("EncounterReasonForVisits");
                }
            }
        }
        private System.Collections.Generic.ICollection<EncounterReasonForVisit> _encounterReasonForVisits;

        #endregion

        #region Association Fixup
    
        private void FixupEncounterReasonForVisitsItemSet(object sender, Soaf.EventArgs<EncounterReasonForVisit> e)
        {
            var item = e.Value;
     
            if (!item.AssociatedSignAndSymptom.Contains(this))
            {
                item.AssociatedSignAndSymptom.Add(this);
            }
        }
    
        private void FixupEncounterReasonForVisitsItemRemoved(object sender, Soaf.EventArgs<EncounterReasonForVisit> e)
        {
            var item = e.Value;
     
            if (item.AssociatedSignAndSymptom.Contains(this))
            {
                item.AssociatedSignAndSymptom.Remove(this);
            }
        }
    

        #endregion

    }
}
