﻿
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum FinancialInformationTypeId
    {
        Deductible = 1,
        [Display(Name = "Pt Coinsurance")]
        Coinsurance = 2,
        [Display(Name = "Pt Resp Other")]
        PatientResponsibilityOther = 3,
        [Display(Name = "Appeal Closed")]
        AppealClosed = 4,
        Denial = 5,
        [Display(Name = "Denial Closed")]
        DenialClosed = 6,
        [Display(Name = "Info")]
        Information = 7,
        [Display(Name = "Sent to Pt")]
        SentToPatient = 8,
        Void = 9
    }
}