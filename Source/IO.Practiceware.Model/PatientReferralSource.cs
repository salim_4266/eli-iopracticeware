//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Link patients to referral source types.
    /// </summary>
    public partial class PatientReferralSource : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual int ReferralSourceTypeId
        {
            get { return _referralSourceTypeId; }
            set
            {
                if (_referralSourceTypeId != value)
                {
                    if (PatientReferralSourceType != null && PatientReferralSourceType.Id != value)
                    {
                        PatientReferralSourceType = null;
                    }
                    _referralSourceTypeId = value;
    
    				OnPropertyChanged("ReferralSourceTypeId");
                }
            }
        }
        private int _referralSourceTypeId;
    
    	public virtual int PatientId
        {
            get { return _patientId; }
            set
            {
                if (_patientId != value)
                {
                    if (Patient != null && Patient.Id != value)
                    {
                        Patient = null;
                    }
                    _patientId = value;
    
    				OnPropertyChanged("PatientId");
                }
            }
        }
        private int _patientId;
    
    	public virtual string Comment
        {
            get { return _comment; }
            set 
    		{ 
    			var isChanged = _comment != value; 
    			_comment = value; 
    			if (isChanged) OnPropertyChanged("Comment");
    		}
        }
        private string _comment;

        #endregion

        #region Navigation Properties
    
    	[Association("PatientReferralSourceType", "ReferralSourceTypeId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual PatientReferralSourceType PatientReferralSourceType
        {
            get { return _patientReferralSourceType; }
            set
            {
                if (!ReferenceEquals(_patientReferralSourceType, value))
                {
                    var previousValue = _patientReferralSourceType;
                    _patientReferralSourceType = value;
                    FixupPatientReferralSourceType(previousValue);
    				OnPropertyChanged("PatientReferralSourceType");
                }
            }
        }
        private PatientReferralSourceType _patientReferralSourceType;
    
    	[Association("Patient", "PatientId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Patient Patient
        {
            get { return _patient; }
            set
            {
                if (!ReferenceEquals(_patient, value))
                {
                    var previousValue = _patient;
                    _patient = value;
                    FixupPatient(previousValue);
    				OnPropertyChanged("Patient");
                }
            }
        }
        private Patient _patient;

        #endregion

        #region Association Fixup
    
        private void FixupPatientReferralSourceType(PatientReferralSourceType previousValue)
        {
            if (previousValue != null && previousValue.PatientReferralSources.Contains(this))
            {
                previousValue.PatientReferralSources.Remove(this);
            }
    
            if (PatientReferralSourceType != null)
            {
                if (!PatientReferralSourceType.PatientReferralSources.Contains(this))
                {
                    PatientReferralSourceType.PatientReferralSources.Add(this);
                }
                if (PatientReferralSourceType != null && ReferralSourceTypeId != PatientReferralSourceType.Id)
                {
                    ReferralSourceTypeId = PatientReferralSourceType.Id;
                }
            }
        }
    
        private void FixupPatient(Patient previousValue)
        {
            if (previousValue != null && previousValue.PatientReferralSources.Contains(this))
            {
                previousValue.PatientReferralSources.Remove(this);
            }
    
            if (Patient != null)
            {
                if (!Patient.PatientReferralSources.Contains(this))
                {
                    Patient.PatientReferralSources.Add(this);
                }
                if (Patient != null && PatientId != Patient.Id)
                {
                    PatientId = Patient.Id;
                }
            }
        }

        #endregion

    }
}
