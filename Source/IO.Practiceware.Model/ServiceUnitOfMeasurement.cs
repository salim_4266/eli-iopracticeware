﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum ServiceUnitOfMeasurement
    {
        [Display(Name = "MJ")]
        Minute = 1,
        [Display(Name = "UN")]
        Unit = 2
    }
}
