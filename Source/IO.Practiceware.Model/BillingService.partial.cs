﻿using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class BillingService
    {
        /// <summary>
        /// Gets the charge calculated by unit * unit charge.
        /// </summary>
        public decimal? Charge
        {
            get { return Unit.HasValue && UnitCharge.HasValue ? (Unit * UnitCharge) : new decimal?(); }
        }
    }

    public static class BillingServices
    {
        /// <summary>
        /// Sums the charges across the collection of billing services.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static decimal SumCharges(this IEnumerable<BillingService> source)
        {
            return source.Select(i => i.Charge ?? 0).Sum();
        }
    }
}
