﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    using System.ComponentModel.DataAnnotations;

    [DisplayName("Insurer")]
    [MetadataType(typeof(InsurerMetadata))]
    public partial class Insurer
    {

        public Insurer()
        {
            ClaimFilingIndicatorCode = ClaimFilingIndicatorCode.OtherNonFederalPrograms;
        }

        public bool IsMedicare
        {
            get { return (ClaimFilingIndicatorCode == ClaimFilingIndicatorCode.MedicarePartB); }
        }

        public InsurerPayType? InsurerPayType
        {
            get
            {
                if (InsurerPayTypeId != null) return (InsurerPayType)InsurerPayTypeId.Value;
                return null;
            }

            set
            {
                if (value != null) InsurerPayTypeId = (int)value;
                else InsurerPayTypeId = null;
            }
        }

    }

    public class InsurerMetadata
    {
        [Display(Name = "Plan can be used by Dependent")]
        public bool AllowDependents { get; set; }

        [Display(Name = "Receiver")]
        public ClaimFileReceiver ClaimFileReceiver { get; set; }

        [Display(Name = "Claim Filing Indicator Code")]
        public ClaimFilingIndicatorCode ClaimFilingIndicatorCode { get; set; }

        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Display(Name = "Universal Identification Id")]
        public string Hpid { get; set; }

        [Display(Name = "Business Class")]
        public InsurerBusinessClass InsurerBusinessClass { get; set; }

        [Display(Name = "Insurer pay type")]
        public InsurerPayType InsurerPayType { get; set; }

        [Display(Name = "Plan Type")]
        public InsurerPlanType InsurerPlanType { get; set; }

        [Display(Name = "Archived")]
        public bool IsArchived { get; set; }

        [Display(Name = "Medicare Advantage")]
        public bool IsMedicareAdvantage { get; set; }

        [Display(Name = "Referral Required")]
        public bool IsReferralRequired { get; set; }

        [Display(Name = "Paper Claim if not Primary")]
        public bool IsSecondaryClaimPaper { get; set; }

        [Display(Name = "Jurisdiction State")]
        public int? JurisdictionStateOrProvinceId { get; set; }

        [Display(Name = "Medigap Code")]
        public string MedigapCode { get; set; }

        [Display(Name = "Alternate Identifier Code")]
        public string Oeid { get; set; }

        [Display(Name = "Payer Code")]
        public string PayerCode { get; set; }

        [Display(Name = "Plan Name")]
        public string PlanName { get; set; }

        [Display(Name = "Policy Number Format")]
        public string PolicyNumberFormat { get; set; }

        [Display(Name = "Allow 0 Amount")]
        public bool SendZeroCharge { get; set; }
    }

    [DisplayName("Doctor assignment")]
    [MetadataType(typeof(DoctorInsurerAssignmentMetadata))]
    public partial class DoctorInsurerAssignment
    {
    }

    public class DoctorInsurerAssignmentMetadata
    {
        [Display(Name = "Accept assignment")]
        public bool IsAssignmentAccepted { get; set; }

        [Display(Name = "Suppress payments")]
        public bool SuppressPatientPayments { get; set; }
    }

    public enum ClaimFilingIndicatorCode
    {
        [Display(Name = "Central Certification")]
        CentralCertification = 1,
        [Display(Name = "Other Non-Federal Programs")]
        OtherNonFederalPrograms = 2,
        [Display(Name = "Preferred Provider Organization (PPO)")]
        PreferredProviderOrganization = 3,
        [Display(Name = "Point of Service (POS)")]
        PointOfService = 4,
        [Display(Name = "Exclusive Provider Organization (EPO)")]
        ExclusiveProviderOrganization = 5,
        [Display(Name = "Idemnity Insurance")]
        IdemnityInsurance = 6,
        [Display(Name = "Health Maintenance Organization (HMO) Medicare Risk")]
        HealthMaintenanceOrganizationMedicareRisk = 7,
        [Display(Name = "Dental Maintenance Organization")]
        DentalMaintenanceOrganization = 8,
        [Display(Name = "Automobile Medical")]
        AutomobileMedical = 9,
        [Display(Name = "Blue Cross/Blue Shield")]
        BlueCrossBlueShield = 10,
        [Display(Name = "Champus")]
        Champus = 11,
        [Display(Name = "Commercial Insurance Co")]
        CommercialInsurance = 12,
        [Display(Name = "Disability")]
        Disability = 13,
        [Display(Name = "Federal Employees Program")]
        FederalEmployeesProgram = 14,
        [Display(Name = "Health Maintenance Organization")]
        HealthMaintenanceOrganization = 15,
        [Display(Name = "Liability Medical")]
        LiabilityMedical = 16,
        [Display(Name = "Medicare Part A")]
        MedicarePartA = 17,
        [Display(Name = "Medicare Part B")]
        MedicarePartB = 18,
        [Display(Name = "Medicaid")]
        Medicaid = 19,
        [Display(Name = "Other Federal Program")]
        OtherFederalProgram = 20,
        [Display(Name = "Title V")]
        TitleV = 21,
        [Display(Name = "Veterans Affairs Plan")]
        VeteransAffairsPlan = 22,
        [Display(Name = "Workers' Compensation Health Claim")]
        WorkersCompensation = 23,
        [Display(Name = "Mutually Defined")]
        MutuallyDefined = 24
    }

    public enum InsurerPayType
    {
        [Display(Name = "Supplemental Policy")]
        SupplementalPolicy = 1,
        [Display(Name = "Other Policy")]
        OtherPolicy = 2,
        Litigation = 3,
        [Display(Name = "Individual Policy")]
        IndividualPolicy = 4,
        [Display(Name = "Group Policy")]
        GroupPolicy = 5,
        [Display(Name = "Personal Payment Policy")]
        PersonalPaymentPolicy = 6,
        [Display(Name = "Medicare Gap Policy")]
        MedicareGapPolicy = 7,
        [Display(Name = "Long Term Policy")]
        LongTermPolicy = 8,
        [Display(Name = "Auto Insurance Policy")]
        AutoInsurancePolicy = 9,
    }
}

