﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A person's sex.
    /// </summary>
    public enum Sex
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [Description("Unknown")]
        Unknown = 3,

        /// <summary>
        /// Female
        /// </summary>
        [Description("Female")]
        Female = 2,

        /// <summary>
        /// Male
        /// </summary>
        [Description("Male")]
        Male = 1,
    }
}
