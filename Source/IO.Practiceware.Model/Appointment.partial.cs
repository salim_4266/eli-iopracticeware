﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Model
{
    [MetadataType(typeof(AppointmentMetadata))]
    public partial class Appointment
    {
        public Appointment()
        {
            Encounter = new Encounter();
        }

        public String ResourceName
        {
            get
            {
                var userAppointment = this as UserAppointment;
                if (userAppointment != null && userAppointment.User != null)
                {
                    return userAppointment.User.UserName;
                }

                var roomAppointment = this as RoomAppointment;
                if (roomAppointment != null && roomAppointment.Room != null)
                {
                    return roomAppointment.Room.Name;
                }

                var equipmentAppointment = this as EquipmentAppointment;
                if (equipmentAppointment != null && equipmentAppointment.Equipment != null)
                {
                    return equipmentAppointment.Equipment.Name;
                }

                return string.Empty;
            }
        }

        public int? ResourceId
        {
            get
            {
                var userAppointment = this as UserAppointment;
                if (userAppointment != null)
                {
                    return userAppointment.UserId;
                }

                var roomAppointment = this as RoomAppointment;
                if (roomAppointment != null)
                {
                    return roomAppointment.RoomId;
                }

                var equipmentAppointment = this as EquipmentAppointment;
                if (equipmentAppointment != null)
                {
                    return equipmentAppointment.EquipmentId;
                }

                return null;
            }
        }

        public PatientInsurance PatientInsurance
        {
            get
            {
                if (Encounter == null || Encounter.Patient == null) return null;
                return Encounter.Patient.PatientInsurances.FirstOrDefault(i => i.InsuranceType == Encounter.InsuranceType
                                                                               && i.InsurancePolicy != null
                                                                               && i.IsActiveForDateTime(DateTime));
            }
        }

        /// <summary>
        /// Returns the first Schedule Block Appointment Category for this appointment.  There should only ever be 1 ScheduleBlockAppointmentCategory per appointment
        /// but since entity framework does not support 0..1 to 0..1 relationships we had to make it a 0..1 to many. Note: Although EF does support 0..1 to 1, 
        /// it requires using the same primary key on both tables.
        /// </summary>
        public ScheduleBlockAppointmentCategory SingleScheduleBlockAppointmentCategory
        {
            get
            {
                if (ScheduleBlockAppointmentCategories == null || !ScheduleBlockAppointmentCategories.Any()) return null;

                return ScheduleBlockAppointmentCategories.FirstOrDefault();
            }
        }
    }


    public class AppointmentMetadata
    {
        [Display(Name ="Appointment Type")]
        public int AppointmentTypeId { get; set; }

        [Display(Name = "Date/Time")]
        public DateTime DateTime { get; set; }
    }
}