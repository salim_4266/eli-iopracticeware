﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class PatientRecall
    {
        public MethodSent? MethodSent
        {
            get { return MethodSentId.HasValue ? (MethodSent)MethodSentId : new MethodSent?(); }
            set { MethodSentId = value.HasValue ? (int)value : new int?(); }
        }

        public RecallStatus RecallStatus
        {
            get { return (RecallStatus) RecallStatusId; }
            set { RecallStatusId = (int) value; }
        }
    }

    public enum RecallStatus
    {
        [Display(Name = "Pending")]
        Pending = 1,
        [Display(Name = "Sent")]
        Sent = 2,
        [Display(Name = "Closed")]
        Closed = 3
    }
}
