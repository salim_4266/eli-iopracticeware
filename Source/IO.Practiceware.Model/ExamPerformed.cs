//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Configuration of screens, questions asked, values recorded and billing codes triggered during different types of physical exams, including retinal exams, slit lamp exams, Contact lens exams, Clinical Study exams
    /// </summary>
    public partial class ExamPerformed : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;
    
    	public virtual int ScreenId
        {
            get { return _screenId; }
            set
            {
                if (_screenId != value)
                {
                    if (Screen != null && Screen.Id != value)
                    {
                        Screen = null;
                    }
                    _screenId = value;
    
    				OnPropertyChanged("ScreenId");
                }
            }
        }
        private int _screenId;

        #endregion

        #region Navigation Properties
    
    	[Association("Screen", "ScreenId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Screen Screen
        {
            get { return _screen; }
            set
            {
                if (!ReferenceEquals(_screen, value))
                {
                    var previousValue = _screen;
                    _screen = value;
                    FixupScreen(previousValue);
    				OnPropertyChanged("Screen");
                }
            }
        }
        private Screen _screen;

        #endregion

        #region Association Fixup
    
        private void FixupScreen(Screen previousValue)
        {
            if (previousValue != null && previousValue.SpecialExamPerformeds.Contains(this))
            {
                previousValue.SpecialExamPerformeds.Remove(this);
            }
    
            if (Screen != null)
            {
                if (!Screen.SpecialExamPerformeds.Contains(this))
                {
                    Screen.SpecialExamPerformeds.Add(this);
                }
                if (Screen != null && ScreenId != Screen.Id)
                {
                    ScreenId = Screen.Id;
                }
            }
        }

        #endregion

    }
}
