//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class ScheduleTemplateBlock : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return new object[] { OrdinalId, ScheduleTemplateId }; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
    	/// <summary>
    	/// Determines the order for which the block will be displayed on the template.
    	/// </summary>
        [Key]
    	public virtual int OrdinalId
        {
            get { return _ordinalId; }
            set 
    		{ 
    			var isChanged = _ordinalId != value; 
    			_ordinalId = value; 
    			if (isChanged) OnPropertyChanged("OrdinalId");
    		}
        }
        private int _ordinalId;
    
        [Key]
    	public virtual int ScheduleTemplateId
        {
            get { return _scheduleTemplateId; }
            set
            {
                if (_scheduleTemplateId != value)
                {
                    if (ScheduleTemplate != null && ScheduleTemplate.Id != value)
                    {
                        ScheduleTemplate = null;
                    }
                    _scheduleTemplateId = value;
    
    				OnPropertyChanged("ScheduleTemplateId");
                }
            }
        }
        private int _scheduleTemplateId;
    
    	/// <summary>
    	/// If IsUnavaialble = T than do not display appointment Categories 
    	/// </summary>
        [Required]
    	public virtual bool IsUnavailable
        {
            get { return _isUnavailable; }
            set 
    		{ 
    			var isChanged = _isUnavailable != value; 
    			_isUnavailable = value; 
    			if (isChanged) OnPropertyChanged("IsUnavailable");
    		}
        }
        private bool _isUnavailable = false;
    
    	public virtual int ServiceLocationId
        {
            get { return _serviceLocationId; }
            set
            {
                if (_serviceLocationId != value)
                {
                    if (ServiceLocation != null && ServiceLocation.Id != value)
                    {
                        ServiceLocation = null;
                    }
                    _serviceLocationId = value;
    
    				OnPropertyChanged("ServiceLocationId");
                }
            }
        }
        private int _serviceLocationId;
    
    	public virtual string Comment
        {
            get { return _comment; }
            set 
    		{ 
    			var isChanged = _comment != value; 
    			_comment = value; 
    			if (isChanged) OnPropertyChanged("Comment");
    		}
        }
        private string _comment;

        #endregion

        #region Navigation Properties
    
    	[Association("ScheduleTemplate", "ScheduleTemplateId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ScheduleTemplate ScheduleTemplate
        {
            get { return _scheduleTemplate; }
            set
            {
                if (!ReferenceEquals(_scheduleTemplate, value))
                {
                    var previousValue = _scheduleTemplate;
                    _scheduleTemplate = value;
                    FixupScheduleTemplate(previousValue);
    				OnPropertyChanged("ScheduleTemplate");
                }
            }
        }
        private ScheduleTemplate _scheduleTemplate;
    
    	[Association("ScheduleTemplateBlockAppointmentCategories", "OrdinalId, ScheduleTemplateId", "ScheduleTemplateBlockOrdinalId, ScheduleTemplateBlockScheduleTemplateId")]
        public virtual System.Collections.Generic.ICollection<ScheduleTemplateBlockAppointmentCategory> ScheduleTemplateBlockAppointmentCategories
        {
            get
            {
                if (_scheduleTemplateBlockAppointmentCategories == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ScheduleTemplateBlockAppointmentCategory>();
                    newCollection.ItemSet += FixupScheduleTemplateBlockAppointmentCategoriesItemSet;
                    newCollection.ItemRemoved += FixupScheduleTemplateBlockAppointmentCategoriesItemRemoved;
                    _scheduleTemplateBlockAppointmentCategories = newCollection;
                }
                return _scheduleTemplateBlockAppointmentCategories;
            }
            set
            {
                if (!ReferenceEquals(_scheduleTemplateBlockAppointmentCategories, value))
                {
                    var previousValue = _scheduleTemplateBlockAppointmentCategories as Soaf.Collections.FixupCollection<ScheduleTemplateBlockAppointmentCategory>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupScheduleTemplateBlockAppointmentCategoriesItemSet;
                        previousValue.ItemRemoved -= FixupScheduleTemplateBlockAppointmentCategoriesItemRemoved;
                    }
                    _scheduleTemplateBlockAppointmentCategories = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ScheduleTemplateBlockAppointmentCategory>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupScheduleTemplateBlockAppointmentCategoriesItemSet;
                        newValue.ItemRemoved += FixupScheduleTemplateBlockAppointmentCategoriesItemRemoved;
                    }
    				OnPropertyChanged("ScheduleTemplateBlockAppointmentCategories");
                }
            }
        }
        private System.Collections.Generic.ICollection<ScheduleTemplateBlockAppointmentCategory> _scheduleTemplateBlockAppointmentCategories;
    
    	[Association("ServiceLocation", "ServiceLocationId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ServiceLocation ServiceLocation
        {
            get { return _serviceLocation; }
            set
            {
                if (!ReferenceEquals(_serviceLocation, value))
                {
                    var previousValue = _serviceLocation;
                    _serviceLocation = value;
                    FixupServiceLocation(previousValue);
    				OnPropertyChanged("ServiceLocation");
                }
            }
        }
        private ServiceLocation _serviceLocation;

        #endregion

        #region Association Fixup
    
        private void FixupScheduleTemplate(ScheduleTemplate previousValue)
        {
            if (previousValue != null && previousValue.ScheduleTemplateBlocks.Contains(this))
            {
                previousValue.ScheduleTemplateBlocks.Remove(this);
            }
    
            if (ScheduleTemplate != null)
            {
                if (!ScheduleTemplate.ScheduleTemplateBlocks.Contains(this))
                {
                    ScheduleTemplate.ScheduleTemplateBlocks.Add(this);
                }
                if (ScheduleTemplate != null && ScheduleTemplateId != ScheduleTemplate.Id)
                {
                    ScheduleTemplateId = ScheduleTemplate.Id;
                }
            }
        }
    
        private void FixupServiceLocation(ServiceLocation previousValue)
        {
            if (previousValue != null && previousValue.ScheduleTemplateBlocks.Contains(this))
            {
                previousValue.ScheduleTemplateBlocks.Remove(this);
            }
    
            if (ServiceLocation != null)
            {
                if (!ServiceLocation.ScheduleTemplateBlocks.Contains(this))
                {
                    ServiceLocation.ScheduleTemplateBlocks.Add(this);
                }
                if (ServiceLocation != null && ServiceLocationId != ServiceLocation.Id)
                {
                    ServiceLocationId = ServiceLocation.Id;
                }
            }
        }
    
        private void FixupScheduleTemplateBlockAppointmentCategoriesItemSet(object sender, Soaf.EventArgs<ScheduleTemplateBlockAppointmentCategory> e)
        {
            var item = e.Value;
     
            item.ScheduleTemplateBlock = this;
        }
    
        private void FixupScheduleTemplateBlockAppointmentCategoriesItemRemoved(object sender, Soaf.EventArgs<ScheduleTemplateBlockAppointmentCategory> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ScheduleTemplateBlock, this))
            {
                item.ScheduleTemplateBlock = null;
            }
        }
    

        #endregion

    }
}
