//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// There are many other notes not yet included in the below query, including Patient Financial, Patient Collection, Practice, Red (CRM), Surgery and Clinical notes.  We also need additional properties to the comments (or a restructuring) to include StartDateTime, EndDateTime, IsDeleted, IsIncludedOnClaim, IsIncludedOnStatement, IsIncludedOnRx, IsAlertOn and additional granularity such as NoteSystem, NoteCategory.
    /// </summary>
    public partial class Comment : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual long Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private long _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Value
        {
            get { return _value; }
            set 
    		{ 
    			var isChanged = _value != value; 
    			_value = value; 
    			if (isChanged) OnPropertyChanged("Value");
    		}
        }
        private string _value;
    
    	internal virtual Nullable<int> PatientReferralSourceId
        {
            get { return _patientReferralSourceId; }
            set 
    		{ 
    			var isChanged = _patientReferralSourceId != value; 
    			_patientReferralSourceId = value; 
    			if (isChanged) OnPropertyChanged("PatientReferralSourceId");
    		}
        }
        private Nullable<int> _patientReferralSourceId;
    
        [Required]
    	public virtual bool IsArchived
        {
            get { return _isArchived; }
            set 
    		{ 
    			var isChanged = _isArchived != value; 
    			_isArchived = value; 
    			if (isChanged) OnPropertyChanged("IsArchived");
    		}
        }
        private bool _isArchived = false;
    
    	public virtual Nullable<int> EncounterLensesPrescriptionId
        {
            get { return _encounterLensesPrescriptionId; }
            set 
    		{ 
    			var isChanged = _encounterLensesPrescriptionId != value; 
    			_encounterLensesPrescriptionId = value; 
    			if (isChanged) OnPropertyChanged("EncounterLensesPrescriptionId");
    		}
        }
        private Nullable<int> _encounterLensesPrescriptionId;

        #endregion

    }
}
