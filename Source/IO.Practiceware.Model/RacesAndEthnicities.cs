﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A person's Races And Ethnicities details in combined format
    /// </summary>
    public enum RacesAndEthnicities
    {
        /// <summary>
        /// Declined to Specify
        /// </summary>
        [Description("Declined to Specify")]
        Decline = 8,

        /// <summary>
        /// Not Hispanic or Latino
        /// </summary>
        [Description("Not Hispanic or Latino")]
        NotHispanic = 7,

        /// <summary>
        /// Hispanic or Latino
        /// </summary>
        [Description("Hispanic or Latino")]
        Hispanic = 6,

        /// <summary>
        /// White
        /// </summary>
        [Description("White")]
        White = 5,

        /// <summary>
        /// Native Hawaiian or Other Pacific Islander
        /// </summary>
        [Description("Native Hawaiian or Other Pacific Islander")]
        Native = 4,

        /// <summary>
        /// Black or African American
        /// </summary>
        [Description("Black or African American")]
        Black = 3,

        /// <summary>
        /// Asian
        /// </summary>
        [Description("Asian")]
        Asian = 2,

        /// <summary>
        /// American Indian or Alaska Native
        /// </summary>
        [Description("American Indian or Alaska Native")]
        American = 1,
    }
}
