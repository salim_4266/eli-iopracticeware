﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    [DisplayName("Claim Form")]
    [MetadataType(typeof(InsurerClaimFormMetadata))]
    public partial class InsurerClaimForm
    {
        public InvoiceType InvoiceType
        {
            get { return (InvoiceType)InvoiceTypeId; }
            set { InvoiceTypeId = (int)value; }
        }
    }

    public class InsurerClaimFormMetadata
    {
        [Display(Name = "Type")]
        public ClaimFormType ClaimFormType { get; set; }

        [Display(Name = "Insurer")]
        public Insurer Insurer { get; set; }

        [Display(Name = "Invoice Type")]
        public InvoiceType InvoiceType { get; set; }
    }
}