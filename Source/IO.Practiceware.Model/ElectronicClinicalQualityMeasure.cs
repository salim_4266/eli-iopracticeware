﻿using System.Linq;
using System.Reflection;

namespace IO.Practiceware.Model
{
    public struct ElectronicClinicalQualityMeasure
    {
        public static readonly ElectronicClinicalQualityMeasure ControllingHighBloodPressure = new ElectronicClinicalQualityMeasure(1, "Controlling High Blood Pressure", "165", "abdc37cc-bac6-4156-9b91-d1be2c8b7268", "2", "0018", "40280381-3d61-56a7-013e-66bc02da4dee");
        public static readonly ElectronicClinicalQualityMeasure HighRiskMedications1 = new ElectronicClinicalQualityMeasure(2, "Use of High-Risk Medications in the Elderly who were ordered at least one high-risk medication", "156", "a3837ff8-1abc-4ba9-800e-fd4e7953adbd", "2", "0022", "40280381-3d61-56a7-013e-65c9c3043e54");
        public static readonly ElectronicClinicalQualityMeasure HighRiskMedications2 = new ElectronicClinicalQualityMeasure(3, "Use of High-Risk Medications in the Elderly who were ordered at least two different high-risk medications", "156", "a3837ff8-1abc-4ba9-800e-fd4e7953adbd", "2", "0022", "40280381-3d61-56a7-013e-65c9c3043e54");
        public static readonly ElectronicClinicalQualityMeasure TobaccoScreening = new ElectronicClinicalQualityMeasure(4, "Preventive Care and Screening: Tobacco Use: Screening and Cessation Intervention", "138", "e35791df-5b25-41bb-b260-673337bc44a8", "2", "0028", "40280381-3d61-56a7-013e-5cd94a4d64fa");
        public static readonly ElectronicClinicalQualityMeasure DiabetesEyeExam = new ElectronicClinicalQualityMeasure(5, "Diabetes: Eye Exam", "131", "d90bdab4-b9d2-4329-9993-5c34e2c0dc66", "2", "0055", "40280381-3d61-56a7-013e-62237d5d24e1");
        public static readonly ElectronicClinicalQualityMeasure Poag = new ElectronicClinicalQualityMeasure(6, "Primary Open-Angle Glaucoma (POAG): Optic Nerve Evaluation", "143", "db9d9f09-6b6a-4749-a8b2-8c1fdb018823", "2", "0086", "40280381-3d61-56a7-013e-425ad8e9179c");
        public static readonly ElectronicClinicalQualityMeasure DiabeticRetinopathyDocumentation= new ElectronicClinicalQualityMeasure(7, "Diabetic Retinopathy: Documentation of Presence or Absence of Macular Edema and Level of Severity of Retinopathy", "167", "50164228-9d64-4efc-af67-da0547ff61f1", "2", "0088", "40280381-3d61-56a7-013e-57fe7ed437d7");
        public static readonly ElectronicClinicalQualityMeasure DiabeticRetinopathyCommunication = new ElectronicClinicalQualityMeasure(8, "Diabetic Retinopathy: Communication with the Physician Managing Ongoing Diabetes Care", "142", "53d6d7c3-43fb-4d24-8099-17e74c022c05", "2", "0089", "40280381-3d61-56a7-013e-66b2ca294c47");
        public static readonly ElectronicClinicalQualityMeasure DocumentationOfMedications = new ElectronicClinicalQualityMeasure(9, "Documentation of Current Medications in the Medical Record", "68", "9a032d9c-3d9b-11e1-8634-00237d5bf174", "3", "0419", "40280381-3e93-d1af-013e-a36090b72cc8");
        public static readonly ElectronicClinicalQualityMeasure CataractComplications = new ElectronicClinicalQualityMeasure(10, "Cataracts: Complications within 30 Days Following Cataract Surgery Requiring Additional Surgical Procedures", "132", "9a0339c2-3d9b-11e1-8634-00237d5bf174", "2", "0564", "40280381-3d61-56a7-013e-66ae98364ade");
        public static readonly ElectronicClinicalQualityMeasure CataractSurgeryVisualAcuity = new ElectronicClinicalQualityMeasure(11, "Cataracts: 20/40 or Better Visual Acuity within 90 Days Following Cataract Surgery", "133", "39e0424a-1727-4629-89e2-c46c2fbb3f5f", "2", "0565", "40280381-3d61-56a7-013e-846aae0f021f");
        public static readonly ElectronicClinicalQualityMeasure CloseTheReferralLoop = new ElectronicClinicalQualityMeasure(12, "Closing the referral loop: receipt of specialist report", "50", "f58fc0d6-edf5-416a-8d29-79afbfd24dea", "2", "XXXX", "40280381-3d61-56a7-013e-7aa509de6258");
        public static readonly ElectronicClinicalQualityMeasure BmiPop1 = new ElectronicClinicalQualityMeasure(13, "Preventive Care and Screening: Body Mass Index (BMI) Screening and Follow-Up for Patients of Age 18-64 years", "69", "9a031bb8-3d9b-11e1-8634-00237d5bf174", "2", "0421", "40280381-3e93-d1af-013e-d6e2b772150d");
        public static readonly ElectronicClinicalQualityMeasure BmiPop2 = new ElectronicClinicalQualityMeasure(14, "Preventive Care and Screening: Body Mass Index (BMI) Screening and Follow-Up for Patients of Age 65 years and Above", "69", "9a031bb8-3d9b-11e1-8634-00237d5bf174", "2", "0421", "40280381-3e93-d1af-013e-d6e2b772150d");
        

        private ElectronicClinicalQualityMeasure(int id, string measureTitle, string centersForMedicareAndMedicaidcode, string versionNeutralId, string measureVersion, string nationalQualityForumCode, string versionSpecificId)
            : this()
        {
            Id = id;
            MeasureTitle = measureTitle;
            CentersForMedicareAndMedicaidcode = centersForMedicareAndMedicaidcode;
            VersionNeutralId = versionNeutralId;
            MeasureVersion = measureVersion;
            NationalQualityForumCode = nationalQualityForumCode;
            VersionSpecificId = versionSpecificId;
        }

        public int Id { get; set; }
        public string MeasureTitle { get; private set; }
        public string CentersForMedicareAndMedicaidcode { get; private set; }
        public string VersionNeutralId { get; private set; }
        public string MeasureVersion { get; private set; }
        public string NationalQualityForumCode { get; private set; }
        public string VersionSpecificId { get; private set; }

        public bool Equals(ElectronicClinicalQualityMeasure other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ElectronicClinicalQualityMeasure && Equals((ElectronicClinicalQualityMeasure)obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static implicit operator string(ElectronicClinicalQualityMeasure value)
        {
            return value.CentersForMedicareAndMedicaidcode;
        }

        public static implicit operator ElectronicClinicalQualityMeasure(string value)
        {
            return typeof(ElectronicClinicalQualityMeasure).GetFields(BindingFlags.Public | BindingFlags.Static).Select(f => ((ElectronicClinicalQualityMeasure)f.GetValue(null)))
                .FirstOrDefault(v => v.CentersForMedicareAndMedicaidcode == value);
        }

        public static implicit operator int(ElectronicClinicalQualityMeasure value)
        {
            return value.Id;
        }

        public static implicit operator ElectronicClinicalQualityMeasure(int value)
        {
            return typeof(ElectronicClinicalQualityMeasure).GetFields(BindingFlags.Public | BindingFlags.Static).Select(f => ((ElectronicClinicalQualityMeasure)f.GetValue(null)))
                .FirstOrDefault(v => v.Id == value);
        }

        public static bool operator ==(ElectronicClinicalQualityMeasure x, ElectronicClinicalQualityMeasure y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(ElectronicClinicalQualityMeasure x, ElectronicClinicalQualityMeasure y)
        {
            return !(x == y);
        }

        public override string ToString()
        {
            return MeasureTitle;
        }
    }
}