﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    [MetadataType(typeof(UserAppointmentMetadata))]
    public partial class UserAppointment
    {

    }

    public class UserAppointmentMetadata
    {
        [Display(Name = "Resource")]
        public int UserId { get; set; }
    }
}
