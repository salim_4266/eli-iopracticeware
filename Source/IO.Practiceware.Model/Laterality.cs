﻿namespace IO.Practiceware.Model
{
    public enum Laterality
    {
        Right = 1,
        Left = 2,
        Both = 3
    }

    public partial class EncounterChiefComplaint
    {
        public Laterality? Laterality
        {
            get { return (Laterality?)LateralityId; }
            set { LateralityId = (int?)value; }
        }
    }

    public partial class EncounterReasonForVisit
    {
        public Laterality? Laterality
        {
            get { return (Laterality?)LateralityId; }
            set { LateralityId = (int?)value; }
        }
    }

    public partial class EncounterReasonForVisitComment
    {
        public Laterality? Laterality
        {
            get { return (Laterality?)LateralityId; }
            set { LateralityId = (int?)value; }
        }
    }

    public partial class PatientDiagnosis
    {
        public Laterality? Laterality
        {
            get { return (Laterality?)LateralityId; }
            set { LateralityId = (int?)value; }
        }
    }

    public partial class PatientDiagnosisComment
    {
        public Laterality? Laterality
        {
            get { return (Laterality?)LateralityId; }
            set { LateralityId = (int?)value; }
        }
    }
}
