﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A person's sex.
    /// </summary>
    public enum SexualOrientation
    {
        /// <summary>
        /// Decline to answer
        /// </summary>
        [Description("Choose not to disclose")]
        Decline = 6,

        /// <summary>
        /// Don't know
        /// </summary>
        [Description("Don't know")]
        Dont = 5,

        /// <summary>
        /// Something else, please describe
        /// </summary>
        [Description("Something else, please describe")]
        Someting = 4,

        /// <summary>
        /// Bisexual
        /// </summary>
        [Description("Bisexual")]
        Bisexual = 3,

        /// <summary>
        /// Straight or heterosexual
        /// </summary>
        [Description("Straight or heterosexual")]
        Straight = 2,

        /// <summary>
        /// Lesbian, gay or homosexual
        /// </summary>
        [Description("Lesbian, gay or homosexual")]
        Lesbian = 1,
    }
}
