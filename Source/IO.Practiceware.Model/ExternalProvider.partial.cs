﻿using System.Linq;
using Soaf;

namespace IO.Practiceware.Model
{
    public partial class ExternalProvider
    {
        /// <summary>
        /// Gets the name of the ExternalProvider, formatted for readability.
        /// </summary>
        /// <returns>Returns the DisplayName if it exists.
        /// Otherwise returns "{FirstName} {MiddleName} {LastNameOrEntityName}", where whitespace is trimmed.</returns>
        public string GetFormattedName()
        {
            // DisplayName may contain additional details such as "M.D." that other properties can't provide.
            if (!string.IsNullOrEmpty(DisplayName)) { return DisplayName; }

            string formattedName = string.Empty;
            bool firstEntry = true;
            foreach (string name in new[] { FirstName, MiddleName, LastNameOrEntityName }.Where(name => !string.IsNullOrEmpty(name)))
            {
                formattedName += (firstEntry) ? name : " " + name;
                firstEntry = false;
            }
            return formattedName;
        }

        public string GetFormattedNameAndClinicalSpecialtyType()
        {
            return GetFormattedName() + GetFormattedClinicalSpecialtyType();
        }

        public string GetFormattedClinicalSpecialtyType()
        {
            return ExternalProviderClinicalSpecialtyTypes.FirstOrDefault().IfNotNull(st => " (" + st.ClinicalSpecialtyType.Name + ")");
        }

        public string GetFormattedNpi()
        {
            return " (" +  IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).ToStringIfNotNull(" (NPI not documented)") + ")";
        }

        public string GetFormattedAddress()
        {
            string formattedAddress = string.Empty;
            if (ExternalContactAddresses != null) { formattedAddress = ExternalContactAddresses.FirstOrDefault().IfNotNull(add => add.Line1 + ", " + add.City + " " + add.StateOrProvince.Abbreviation); }
            return formattedAddress;
        }


        public string GetFormattedPhoneNumber()
        {
            string formattedPhoneNumber = string.Empty;
            if (ExternalContactPhoneNumbers != null) { formattedPhoneNumber = ExternalContactPhoneNumbers.FirstOrDefault().IfNotNull(pn => FormatAreaCode(pn.AreaCode) + FormatExchangeAndSuffix(pn.ExchangeAndSuffix)); }
            return formattedPhoneNumber;
        }

        public string FormatAreaCode(string str)
        {
            return str == null ? null : "(" + str + ")";
        }

        public string FormatExchangeAndSuffix(string str)
        {
            return str.Length != 7 ? str : str.Substring(0, 3) + "-" + str.Substring(3, 4);
        }

        public string GetFormattedAddressAndPhoneNumber()
        {
            return GetFormattedAddress().ToStringIfNotNull("Address not documented") + " | " + GetFormattedPhoneNumber().ToStringIfNotNull("Phone number not documented");
        }
    }
}
