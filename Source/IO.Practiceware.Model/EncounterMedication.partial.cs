﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class EncounterMedicationOrder
    {
        public MedicationOrderAction MedicationOrderAction
        {
            get { return (MedicationOrderAction)MedicationOrderActionId; }
            set { MedicationOrderActionId = (int)value; }
        }
    }
    public enum MedicationOrderAction
    {
        Prescribe = 1,
        Continue = 2,
        Change = 3,
        Refill = 4,
        Discontinue = 5
    }
}
