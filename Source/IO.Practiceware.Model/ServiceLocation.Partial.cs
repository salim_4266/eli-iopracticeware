﻿using Soaf;
using System;
using System.Windows.Media;

namespace IO.Practiceware.Model
{
    public partial class ServiceLocation
    {
        public Color Color
        {
            get
            {
                var hexColor = (HexColor.StartsWith("#") ? HexColor : "#" + HexColor);

                var colorObj = ColorConverter.ConvertFromString(hexColor);
                if (colorObj == null)
                {
                    throw new NullReferenceException("Could not convert the HexColor {0} to a Color".FormatWith(hexColor));
                }

                return (Color)colorObj;
            }
        }
    }
}
