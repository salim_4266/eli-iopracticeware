//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// As soon as possible, Stat, Next Visit, As soon as convenient.  Used instead of TimeFrame + Time Unit
    /// </summary>
    public partial class RelativeTimeType : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;

        #endregion

        #region Navigation Properties
    
    	[Association("EncounterDiagnosticTestOrders", "Id", "RelativeTimeTypeId")]
        public virtual System.Collections.Generic.ICollection<EncounterDiagnosticTestOrder> EncounterDiagnosticTestOrders
        {
            get
            {
                if (_encounterDiagnosticTestOrders == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<EncounterDiagnosticTestOrder>();
                    newCollection.ItemSet += FixupEncounterDiagnosticTestOrdersItemSet;
                    newCollection.ItemRemoved += FixupEncounterDiagnosticTestOrdersItemRemoved;
                    _encounterDiagnosticTestOrders = newCollection;
                }
                return _encounterDiagnosticTestOrders;
            }
            set
            {
                if (!ReferenceEquals(_encounterDiagnosticTestOrders, value))
                {
                    var previousValue = _encounterDiagnosticTestOrders as Soaf.Collections.FixupCollection<EncounterDiagnosticTestOrder>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupEncounterDiagnosticTestOrdersItemSet;
                        previousValue.ItemRemoved -= FixupEncounterDiagnosticTestOrdersItemRemoved;
                    }
                    _encounterDiagnosticTestOrders = value;
                    var newValue = value as Soaf.Collections.FixupCollection<EncounterDiagnosticTestOrder>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupEncounterDiagnosticTestOrdersItemSet;
                        newValue.ItemRemoved += FixupEncounterDiagnosticTestOrdersItemRemoved;
                    }
    				OnPropertyChanged("EncounterDiagnosticTestOrders");
                }
            }
        }
        private System.Collections.Generic.ICollection<EncounterDiagnosticTestOrder> _encounterDiagnosticTestOrders;
    
    	[Association("EncounterVaccinationOrders", "Id", "RelativeTimeTypeId")]
        public virtual System.Collections.Generic.ICollection<EncounterVaccinationOrder> EncounterVaccinationOrders
        {
            get
            {
                if (_encounterVaccinationOrders == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<EncounterVaccinationOrder>();
                    newCollection.ItemSet += FixupEncounterVaccinationOrdersItemSet;
                    newCollection.ItemRemoved += FixupEncounterVaccinationOrdersItemRemoved;
                    _encounterVaccinationOrders = newCollection;
                }
                return _encounterVaccinationOrders;
            }
            set
            {
                if (!ReferenceEquals(_encounterVaccinationOrders, value))
                {
                    var previousValue = _encounterVaccinationOrders as Soaf.Collections.FixupCollection<EncounterVaccinationOrder>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupEncounterVaccinationOrdersItemSet;
                        previousValue.ItemRemoved -= FixupEncounterVaccinationOrdersItemRemoved;
                    }
                    _encounterVaccinationOrders = value;
                    var newValue = value as Soaf.Collections.FixupCollection<EncounterVaccinationOrder>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupEncounterVaccinationOrdersItemSet;
                        newValue.ItemRemoved += FixupEncounterVaccinationOrdersItemRemoved;
                    }
    				OnPropertyChanged("EncounterVaccinationOrders");
                }
            }
        }
        private System.Collections.Generic.ICollection<EncounterVaccinationOrder> _encounterVaccinationOrders;
    
    	[Association("EncounterProcedureOrders", "Id", "RelativeTimeTypeId")]
        public virtual System.Collections.Generic.ICollection<EncounterProcedureOrder> EncounterProcedureOrders
        {
            get
            {
                if (_encounterProcedureOrders == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<EncounterProcedureOrder>();
                    newCollection.ItemSet += FixupEncounterProcedureOrdersItemSet;
                    newCollection.ItemRemoved += FixupEncounterProcedureOrdersItemRemoved;
                    _encounterProcedureOrders = newCollection;
                }
                return _encounterProcedureOrders;
            }
            set
            {
                if (!ReferenceEquals(_encounterProcedureOrders, value))
                {
                    var previousValue = _encounterProcedureOrders as Soaf.Collections.FixupCollection<EncounterProcedureOrder>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupEncounterProcedureOrdersItemSet;
                        previousValue.ItemRemoved -= FixupEncounterProcedureOrdersItemRemoved;
                    }
                    _encounterProcedureOrders = value;
                    var newValue = value as Soaf.Collections.FixupCollection<EncounterProcedureOrder>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupEncounterProcedureOrdersItemSet;
                        newValue.ItemRemoved += FixupEncounterProcedureOrdersItemRemoved;
                    }
    				OnPropertyChanged("EncounterProcedureOrders");
                }
            }
        }
        private System.Collections.Generic.ICollection<EncounterProcedureOrder> _encounterProcedureOrders;

        #endregion

        #region Association Fixup
    
        private void FixupEncounterDiagnosticTestOrdersItemSet(object sender, Soaf.EventArgs<EncounterDiagnosticTestOrder> e)
        {
            var item = e.Value;
     
            item.RelativeTimeType = this;
        }
    
        private void FixupEncounterDiagnosticTestOrdersItemRemoved(object sender, Soaf.EventArgs<EncounterDiagnosticTestOrder> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.RelativeTimeType, this))
            {
                item.RelativeTimeType = null;
            }
        }
    
    
        private void FixupEncounterVaccinationOrdersItemSet(object sender, Soaf.EventArgs<EncounterVaccinationOrder> e)
        {
            var item = e.Value;
     
            item.RelativeTimeType = this;
        }
    
        private void FixupEncounterVaccinationOrdersItemRemoved(object sender, Soaf.EventArgs<EncounterVaccinationOrder> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.RelativeTimeType, this))
            {
                item.RelativeTimeType = null;
            }
        }
    
    
        private void FixupEncounterProcedureOrdersItemSet(object sender, Soaf.EventArgs<EncounterProcedureOrder> e)
        {
            var item = e.Value;
     
            item.RelativeTimeType = this;
        }
    
        private void FixupEncounterProcedureOrdersItemRemoved(object sender, Soaf.EventArgs<EncounterProcedureOrder> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.RelativeTimeType, this))
            {
                item.RelativeTimeType = null;
            }
        }
    

        #endregion

    }
}
