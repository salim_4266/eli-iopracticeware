﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Model
{
    [DataContract]
    public enum InsuranceType
    {
        [EnumMember]
        [Display(Name = "Major Medical", ShortName ="M", Order = 1)] 
        MajorMedical = 1,
        
        [EnumMember]
        [Display(Name = "Vision", ShortName ="V", Order = 2)] 
        Vision = 2,
        
        [EnumMember] 
        [Display(Name = "Ambulatory", ShortName ="O", Order = 4)] 
        Ambulatory = 3,
        
        [EnumMember] 
        [Display(Name = "Workers Comp", ShortName ="W", Order = 3)] 
        WorkersComp = 4
    }
}