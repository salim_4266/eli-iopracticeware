﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Well known patient address types.
    /// </summary>
    public enum PatientAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Home = 1,
        /// <summary>
        /// 
        /// </summary>
        Business = 2
    }

    /// <summary>
    /// Well known insurer address types.
    /// </summary>
    public enum InsurerAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Claims = 3,
        /// <summary>
        /// 
        /// </summary>
        Appeals = 4
    }

    /// <summary>
    /// Well known billing organization address types.
    /// </summary>
    public enum BillingOrganizationAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Physical Location")]
        PhysicalLocation = 5,
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Pay To")]
        PayTo = 6
    }

    /// <summary>
    /// Well known service location address types.
    /// </summary>
    public enum ServiceLocationAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Main Office")]
        MainOffice = 7
    }

    /// <summary>
    /// Well known external provider address types.
    /// </summary>
    public enum ExternalContactAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Main Office")]
        MainOffice = 8
    }

    /// <summary>
    /// Well known user address types
    /// </summary>
    public enum UserAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Home = 9,
        /// <summary>
        /// 
        /// </summary>
        Work = 10
    }

    /// <summary>
    /// Well known external organization address types.
    /// </summary>
    public enum ExternalOrganizationAddressTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Main Office")]
        MainOffice = 11
    }
}
