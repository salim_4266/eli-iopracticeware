﻿using IO.Practiceware.Model.Legacy;
using Soaf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace IO.Practiceware.Model
{
    public partial class EncounterLensesPrescription
    {
        private LensesPrescription _lensesPrescription;

        public virtual LensesPrescription LensesPrescription
        {
            get { return _lensesPrescription ?? (_lensesPrescription = LensesPrescriptionBuilder.Create(this)); }
        }
    }

    public abstract class LensesPrescription
    {
    }

    public class SpectaclePrescription : LensesPrescription
    {
        public virtual string GlassesType { get; set; }

        public virtual SpectacleEyePrescription ODPrescription { get; set; }

        public virtual SpectacleEyePrescription OSPrescription { get; set; }

        public virtual string Comment { get; set; }
    }

    public class SpectacleEyePrescription
    {
        public virtual string Sphere { get; set; }

        public virtual string Cylinder { get; set; }

        public virtual string Axis { get; set; }

        public virtual string Reading { get; set; }

        public virtual string Intermediate { get; set; }

        public virtual string PrismOfAngle1 { get; set; }

        public virtual string PrismOfAngle2 { get; set; }

        public virtual string VertexDistance { get; set; }
    }

    public class ContactLensesPrescription : LensesPrescription
    {
        public ContactLensEyePrescription ODPrescription { get; set; }

        public ContactLensEyePrescription OSPrescription { get; set; }
    }

    public class ContactLensEyePrescription
    {
        public virtual string Sphere { get; set; }

        public virtual string Cylinder { get; set; }

        public virtual string Axis { get; set; }

        public virtual string Reading { get; set; }

        public virtual string BaseCurve { get; set; }

        public virtual string Diameter { get; set; }

        public virtual string PeripheralCurve { get; set; }

        public virtual bool? Dominant { get; set; }

        public virtual int InventoryId { get; set; }

        public virtual string Comment { get; set; }
    }


    internal static class LensesPrescriptionBuilder
    {
        public static LensesPrescription Create(EncounterLensesPrescription encounterLensesPrescription)
        {
            try
            {
                if (encounterLensesPrescription.FindingDetail.StartsWith(PatientClinical.SpectaclePrescriptionIndicator))
                {
                    return CreateSpectaclePrescription(encounterLensesPrescription);
                }
                if (encounterLensesPrescription.FindingDetail.StartsWith(PatientClinical.ContactLensesPrescriptionIndicator))
                {
                    return CreateContactLensesPrescription(encounterLensesPrescription);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Could not create prescription for ClinicalId {0}.".FormatWith(encounterLensesPrescription.Id), ex);
            }
            return null;
        }

        private static ContactLensesPrescription CreateContactLensesPrescription(EncounterLensesPrescription encounterLensesPrescription)
        {
            var prescription = new ContactLensesPrescription();

            string[] segments = GetFindingDetailSegments(encounterLensesPrescription);
            if (segments == null) return null;

            prescription.ODPrescription = CreateContactLensEyePrescription(segments[3]);
            int odInventoryId;
            if (int.TryParse(segments[6].Trim(), out odInventoryId))
            {
                prescription.ODPrescription.InventoryId = odInventoryId;
            }
            prescription.OSPrescription = CreateContactLensEyePrescription(segments[4]);

            int osInventoryId;
            if (int.TryParse(segments[7].Trim(), out osInventoryId))
            {
                prescription.OSPrescription.InventoryId = osInventoryId;
            }

            if (encounterLensesPrescription.ImageDescriptor != null && encounterLensesPrescription.ImageDescriptor.Length > 4)
            {
                prescription.ODPrescription.Comment = encounterLensesPrescription.ImageDescriptor.Substring(4);
                if (prescription.ODPrescription.Comment.Contains("OS:"))
                {
                    prescription.ODPrescription.Comment = prescription.ODPrescription.Comment.Substring(0, prescription.ODPrescription.Comment.IndexOf("OS:"));
                    prescription.OSPrescription.Comment = encounterLensesPrescription.ImageDescriptor.Substring(encounterLensesPrescription.ImageDescriptor.IndexOf("OS:") + 3);
                }
            }

            return prescription;
        }

        private static ContactLensEyePrescription CreateContactLensEyePrescription(string text)
        {
            const string addReadingPrefix = "ADD-READING:";
            const string baseCurvePrefix = "BASE CURVE:";
            const string diameterPrefix = "DIAMETER:";
            const string peripheralCurvePrefix = "PERIPH CURVE:";
            const string dOrN = "D OR N:";

            var prefixes = new[] {addReadingPrefix, baseCurvePrefix, diameterPrefix, peripheralCurvePrefix, dOrN};

            var prescription = new ContactLensEyePrescription();

            string sphere, cylinder, axis;
            GetSphereCylinderAndAxis(ref text, out sphere, out cylinder, out axis, prefixes);
            prescription.Sphere = sphere;
            prescription.Cylinder = cylinder;
            prescription.Axis = axis;

            prescription.Reading = GetFindingDetailPrefixedValue(addReadingPrefix, prefixes.Except(new[] {addReadingPrefix}), ref text);
            prescription.BaseCurve = GetFindingDetailPrefixedValue(baseCurvePrefix, prefixes.Except(new[] {baseCurvePrefix}), ref text);
            prescription.Diameter = GetFindingDetailPrefixedValue(diameterPrefix, prefixes.Except(new[] {diameterPrefix}), ref text);
            prescription.PeripheralCurve = GetFindingDetailPrefixedValue(peripheralCurvePrefix, prefixes.Except(new[] {peripheralCurvePrefix}), ref text);
            string dOrNText = GetFindingDetailPrefixedValue(dOrN, prefixes.Except(new[] {dOrN}), ref text);
            if (dOrNText != null)
            {
                if (dOrNText.Contains("D"))
                {
                    prescription.Dominant = true;
                }
                else if (dOrNText.Contains("N"))
                {
                    prescription.Dominant = false;
                }
            }

            return prescription;
        }

        private static SpectaclePrescription CreateSpectaclePrescription(EncounterLensesPrescription encounterLensesPrescription)
        {
            var prescription = new SpectaclePrescription();

            string[] segments = GetFindingDetailSegments(encounterLensesPrescription);
            if (segments == null) return null;

            prescription.GlassesType = segments[2];
            prescription.ODPrescription = CreateSpectaclePrescriptionEyePrescription(segments[3].Trim());
            prescription.OSPrescription = CreateSpectaclePrescriptionEyePrescription(segments[4].Trim());

            if (encounterLensesPrescription.ImageDescriptor != null && encounterLensesPrescription.ImageDescriptor.Length > 4)
            {
                prescription.Comment = encounterLensesPrescription.ImageDescriptor.Substring(4);
                if (prescription.Comment.Length > 3)
                {
                    prescription.Comment = prescription.Comment.Substring(0, prescription.Comment.Length - 3);
                }
            }

            return prescription;
        }

        private static SpectacleEyePrescription CreateSpectaclePrescriptionEyePrescription(string text)
        {
            const string addReadingPrefix = "ADD-READING:";
            const string addIntermdiatePrefix = "ADD-INTERMEDIATE:";
            const string prismOfAngle1Prefix = "PRISM OF ANGLE 1:";
            const string prismOfAngle2Prefix = "PRISM OF ANGLE 2:";
            const string vertexDistancePrefix = "VERTEX DIST:";

            var prefixes = new[] {addReadingPrefix, addIntermdiatePrefix, prismOfAngle1Prefix, prismOfAngle2Prefix, vertexDistancePrefix};

            var prescription = new SpectacleEyePrescription();

            string sphere, cylinder, axis;
            GetSphereCylinderAndAxis(ref text, out sphere, out cylinder, out axis, prefixes);
            prescription.Sphere = sphere;
            prescription.Cylinder = cylinder;
            prescription.Axis = axis;

            prescription.Reading = GetFindingDetailPrefixedValue(addReadingPrefix, prefixes.Except(new[] {addReadingPrefix}), ref text);
            prescription.Intermediate = GetFindingDetailPrefixedValue(addIntermdiatePrefix, prefixes.Except(new[] {addIntermdiatePrefix}), ref text);
            prescription.PrismOfAngle1 = GetFindingDetailPrefixedValue(prismOfAngle1Prefix, prefixes.Except(new[] {prismOfAngle1Prefix}), ref text);
            prescription.PrismOfAngle2 = GetFindingDetailPrefixedValue(prismOfAngle2Prefix, prefixes.Except(new[] {prismOfAngle2Prefix}), ref text);
            prescription.VertexDistance = GetFindingDetailPrefixedValue(vertexDistancePrefix, prefixes.Except(new[] {vertexDistancePrefix}), ref text);

            return prescription;
        }

        private static void GetSphereCylinderAndAxis(ref string text, out string sphere, out string cylinder, out string axis, IEnumerable<string> prefixes)
        {
            string t = text;
            int firstIndexOfNamedPrefixes = prefixes.Select(p => t.IndexOf(p)).Where(i => i >= 0).DefaultIfEmpty(-1).Min();

            string unPrefixedValuesText = firstIndexOfNamedPrefixes >= 0 ? text.Substring(0, firstIndexOfNamedPrefixes) : text;

            string[] unPrefixedValues = unPrefixedValuesText.Trim().Split(' ');

            switch (unPrefixedValues.Length)
            {
                case 1:
                    sphere = unPrefixedValues[0];
                    cylinder = null;
                    axis = null;
                    break;
                case 2:
                    sphere = null;
                    cylinder = unPrefixedValues[0];
                    axis = unPrefixedValues[1];
                    break;
                case 3:
                    sphere = unPrefixedValues[0];
                    cylinder = unPrefixedValues[1];
                    axis = unPrefixedValues[2];
                    break;
                default:
                    sphere = null;
                    cylinder = null;
                    axis = null;
                    break;
            }

            text = text.Substring(unPrefixedValuesText.Length);
        }

        private static string GetFindingDetailPrefixedValue(string prefix, IEnumerable<string> otherPrefixes, ref string text)
        {
            if (text.StartsWith(prefix))
            {
                string value = text.Substring(prefix.Length);
                value = value.Substring(0, otherPrefixes.Select(i => value.IndexOf(i)).Concat(new[] {value.Length}).Where(i => i >= 0).Min()).Trim();
                text = text.Substring(prefix.Length + value.Length).Trim();
                return value;
            }
            return null;
        }

        private static string[] GetFindingDetailSegments(EncounterLensesPrescription encounterLensesPrescription)
        {
            string[] segments = new Regex("-[1-9]\\/").Split(encounterLensesPrescription.FindingDetail);

            if (segments.Length != 9)
            {
                return null;
            }

            return segments;
        }
    }
}