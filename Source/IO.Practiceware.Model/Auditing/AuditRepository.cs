﻿using IO.Practiceware.Model.Legacy;
using Soaf;
using Soaf.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace IO.Practiceware.Model.Auditing
{
    public enum AuditEntryChangeType
    {
        [Display(Name = "Insertion")]
        Insert = 0,

        [Display(Name = "Update")]
        Update = 1,

        [Display(Name = "Deletion")]
        Delete = 2
    }

    public static class AuditRepositoryQueries
    {
        private static readonly Dictionary<Type, Type> LegacyTypeMap = new Dictionary<Type, Type>
        {
            {typeof (AppointmentType), typeof (Legacy.AppointmentType)},
            {typeof (UserAppointment), typeof (Legacy.Appointment)},
        };

        const string IdColumn = "Id";

        public static IQueryable<AuditEntry> AuditEntriesFor(this IAuditRepository repository, Type entityType, object key = null)
        {
            return repository.AuditEntriesFor(entityType, key == null ? null : new[] { key });
        }

        public static IQueryable<AuditEntry> AuditEntriesFor(this IAuditRepository repository, Type entityType, IEnumerable keys)
        {
            var objectName = PracticeRepository.GetDbObjectName(entityType) ?? LegacyPracticeRepository.GetDbObjectName(entityType) ?? entityType.Name;

            var result = repository.AuditEntries.Where(ae => ae.ObjectName == objectName);

            // If we have keys to filter on
            if (keys != null)
            {
                // Have keys in the collection?
                if (keys.OfType<object>().Any())
                {
                    var keyInts = keys.OfType<int>().ToArray();
                    if (keyInts.Length > 0) keys = keyInts.Select(i => (long)i).OfType<object>().ToArray();

                    var keyLongs = keys.OfType<long>().ToArray();
                    if (keyLongs.Length > 0)
                    {
                        result = result.Where(i => i.KeyValueNumeric != null && keyLongs.Contains(i.KeyValueNumeric.Value));
                    }
                    else
                    {
                        var keyStrings = keys.OfType<object>().Select(i => i.ToString()).ToArray();
                        result = result.Where(i => keyStrings.Any(s => i.KeyValues.Contains(s)));
                    }
                }
                else
                {
                    // We are expected to return no results, since empty key enumeration has been provided
                    result = result.Where(p => p.Id == Guid.Empty);
                }
            }

            Type legacyEntityType;
            if (LegacyTypeMap.TryGetValue(entityType, out legacyEntityType))
            {
                result = result.Concat(AuditEntriesFor(repository, legacyEntityType, keys));
            }

            return result;
        }

        public static IQueryable<AuditEntry> AuditEntriesFor<T>(this IAuditRepository repository, object key = null)
        {
            return repository.AuditEntriesFor(typeof(T), key == null ? null : new[] { key });
        }

        public static IQueryable<AuditEntry> AuditEntriesFor<T>(this IAuditRepository repository, IEnumerable keys)
        {
            return repository.AuditEntriesFor(typeof(T), keys);
        }

        /// <summary>
        /// Determines whether this audit entry is for the specified entity type.
        /// </summary>
        /// <param name="auditEntry">The audit entry.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [is audit entry for] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAuditEntryFor(this AuditEntry auditEntry, Type type)
        {
            var objectName = auditEntry.ObjectName;
            if (PracticeRepository.GetDbObjectName(type) == objectName) return true;

            Type legacyType;
            if (LegacyTypeMap.TryGetValue(type, out legacyType) && LegacyPracticeRepository.GetDbObjectName(legacyType) == objectName) return true;

            if (objectName.StartsWith("model.")) objectName = objectName.Substring("model.".Length);

            if (objectName.StartsWith("dbo.")) objectName = objectName.Substring("dbo.".Length);

            return objectName == type.Name || (legacyType != null && legacyType.Name == objectName);
        }

        /// <summary>
        /// Gets the display name for the column/property that was changed
        /// </summary>
        /// <param name="change"></param>        
        /// <returns></returns>
        public static string GetDisplayName(this AuditEntryChange change)
        {
            // Check that we have identified the type for audit entry
            var type = change.AuditEntry.ObjectType;
            if (type == null) return change.ColumnName;

            // Get the property
            var prop = type.GetProperty(change.ColumnName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            var propDisplayName = prop.IfNotNull(p => p.GetDisplayName(), change.ColumnName);

            // If we haven't found a nicer display name and it's a Something"Id" Property -> try get display from relation property
            if (propDisplayName == change.ColumnName
                && change.ColumnName.Length > IdColumn.Length
                && change.ColumnName.EndsWith(IdColumn))
            {
                var withoutId = change.ColumnName.Substring(0, change.ColumnName.Length - IdColumn.Length);
                var relationProp = type.GetProperty(withoutId, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);

                propDisplayName = relationProp.IfNotNull(p => p.GetDisplayName(), change.ColumnName);
            }

            return string.Format("{0} {1}", type.GetDisplayName(), propDisplayName);
        }

        /// <summary>
        /// Gets the new value for an audit entry change (the value the old value was changed to).
        /// </summary>
        /// <param name="auditEntry">The audit entry.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="otherAuditEntryChanges">The other audit entry changes.</param>
        /// <param name="currentInstance">The current instance.</param>
        /// <returns></returns>
        public static object GetNewValue(this AuditEntry auditEntry, string columnName, IEnumerable<AuditEntryChange> otherAuditEntryChanges, object currentInstance)
        {
            var newValues =
                otherAuditEntryChanges
                    .OrderBy(i => i.AuditEntry.AuditDateTime)
                    .Where(i => i.AuditEntry.AuditDateTime > auditEntry.AuditDateTime
                                && i.AuditEntry.ObjectName == auditEntry.ObjectName
                                && i.AuditEntry.KeyValues == auditEntry.KeyValues
                                && i.ColumnName == columnName).Select(c => c.OldValue).ToArray();


            if (newValues.Length == 0)
            {
                var property = GetPropertyForColumn(columnName, currentInstance.GetType());
                if (property != null)
                {
                    return property.GetGetMethod(true).GetInvoker()(currentInstance);
                }
                return null;
            }

            return ConvertValueToOriginalType(columnName, newValues[0], currentInstance.GetType());
        }

        /// <summary>
        /// Gets the new value for an audit entry change (the value the old value was changed to).
        /// </summary>
        /// <param name="auditEntry">The audit entry.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="otherAuditEntries">The other audit entries.</param>
        /// <param name="currentInstance">The current instance.</param>
        /// <returns></returns>
        public static object GetNewValue(this AuditEntry auditEntry, string columnName, IEnumerable<AuditEntry> otherAuditEntries, object currentInstance)
        {
            return auditEntry.GetNewValue(columnName, otherAuditEntries.SelectMany(a => a.AuditEntryChanges), currentInstance);
        }

        /// <summary>
        /// Gets the new value for an audit entry change (the value the old value was changed to).
        /// </summary>
        /// <param name="change">The change.</param>
        /// <param name="otherAuditEntryChanges">The other audit entry changes.</param>
        /// <param name="currentInstance">The current instance.</param>
        /// <returns></returns>
        public static object GetNewValue(this AuditEntryChange change, IEnumerable<AuditEntryChange> otherAuditEntryChanges, object currentInstance)
        {
            return change.AuditEntry.GetNewValue(change.ColumnName, otherAuditEntryChanges, currentInstance);
        }

        /// <summary>
        /// Gets the new value for an audit entry change (the value the old value was changed to).
        /// </summary>
        /// <param name="change">The change.</param>
        /// <param name="otherAuditEntries">The other audit entries.</param>
        /// <param name="currentInstance">The current instance.</param>
        /// <returns></returns>
        public static object GetNewValue(this AuditEntryChange change, IEnumerable<AuditEntry> otherAuditEntries, object currentInstance)
        {
            return change.GetNewValue(otherAuditEntries.SelectMany(a => a.AuditEntryChanges), currentInstance);
        }

        /// <summary>
        /// Gets the new value for an audit entry change (formatted to original type).
        /// </summary>
        /// <param name="change">The change.</param>
        /// <returns></returns>
        public static object GetNewValue(this AuditEntryChange change)
        {
            return ConvertValueToOriginalType(change.ColumnName, change.NewValue, change.AuditEntry.ObjectType);
        }

        /// <summary>
        /// Gets the old value for an audit entry change (formatted to original type).
        /// </summary>
        /// <param name="auditEntryChange">The audit entry change.</param>
        /// <param name="currentInstance">The current instance.</param>
        /// <returns></returns>
        public static object GetOldValue(this AuditEntryChange auditEntryChange, object currentInstance)
        {
            return ConvertValueToOriginalType(auditEntryChange.ColumnName, auditEntryChange.OldValue, currentInstance.GetType());
        }

        /// <summary>
        /// Gets the old value for an audit entry change (formatted to original type).
        /// </summary>
        /// <param name="change">The change.</param>
        /// <returns></returns>
        public static object GetOldValue(this AuditEntryChange change)
        {
            return ConvertValueToOriginalType(change.ColumnName, change.OldValue, change.AuditEntry.ObjectType);
        }

        #region Private helpers

        private static object ConvertValueToOriginalType(string columnName, string value, Type entityType)
        {
            // Skip for null or empty strings
            if (value.IsNullOrEmpty()) return value;

            var property = GetPropertyForColumn(columnName, entityType);
            if (property == null) return value;

            // Get the property type (unwrap nullable)
            var propertyType = property.PropertyType.IsNullableType()
                ? property.PropertyType.GetGenericArguments()[0]
                : property.PropertyType;

            // Parse the result
            object result;
            if (propertyType.IsEnum)
            {
                result = Enum.Parse(property.PropertyType, value);
            }
            // Often, Boolean is stored as "0" or "1" in DB which is not convertible
            else if (propertyType == typeof(bool) && (value == 0.ToString() || value == 1.ToString()))
            {
                result = value != 0.ToString();
            }
            else
            {
                result = value.ConvertTo(propertyType);
            }

            return result;
        }

        private static PropertyInfo GetPropertyForColumn(string columnName, Type entityType)
        {
            PropertyInfo property = null;

            // For Something"Id" properties which have corresponding enum typed properties -> use that property instead
            if (columnName.Length > IdColumn.Length
                && columnName.EndsWith(IdColumn))
            {
                var withoutId = columnName.Substring(0, columnName.Length - IdColumn.Length);
                var enumProperty = entityType.GetProperty(withoutId, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (enumProperty != null && enumProperty.PropertyType.IsEnum)
                {
                    property = enumProperty;
                }
            }

            // Use default property
            if (property == null)
            {
                property = entityType.GetProperty(columnName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            }

            return property;
        }

        #endregion
    }
}
