﻿using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Model.Auditing
{
    public partial class AuditEntry
    {
        public Type ObjectType
        {
            get { return PracticeRepository.GetEntityType(ObjectName); }
        }

        public AuditEntryChangeType ChangeType
        {
            get { return (AuditEntryChangeType) ChangeTypeId; }
            set { ChangeTypeId = (int) value; }
        }
    }
}