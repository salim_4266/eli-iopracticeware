﻿using Soaf.Linq;
using System;
using System.Linq;

namespace IO.Practiceware.Model.Auditing
{
    public partial interface IAuditRepository
    {
        Func<Type, IQueryable> AsQueryableFactory();
    }

    public partial class AuditRepository
    {
        private Func<Type, IQueryable> _queryableFactory;

        public Func<Type, IQueryable> AsQueryableFactory()
        {
            return _queryableFactory ?? (_queryableFactory = Queryables.GetQueryableFactory(this));
        }
    }
}
