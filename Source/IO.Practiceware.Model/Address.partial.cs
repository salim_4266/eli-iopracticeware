﻿using System.Text.RegularExpressions;
using Soaf;
using Soaf.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace IO.Practiceware.Model
{
    public interface IAddress
    {
        long Id { get; set; }

        string Line1 { get; set; }
        string Line2 { get; set; }
        string Line3 { get; set; }

        string City { get; set; }

        [Display(Name = "Postal Code")]
        string PostalCode { get; set; }

        [Display(Name = "State")]
        StateOrProvince StateOrProvince { get; set; }

        int StateOrProvinceId { get; set; }

        [Display(Name = "Address Type")]
        IAddressType AddressType { get; set; }
}

    public interface IAddressType
    {
        int Id { get; set; }
        string Name { get; set; }
        bool IsArchived { get; set; }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class BillingOrganizationAddress : IAddress
    {
        public IAddressType AddressType
        {
            get { return BillingOrganizationAddressType; }
            set { BillingOrganizationAddressType = (BillingOrganizationAddressType)value; }
        }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class ExternalContactAddress : IAddress, IHasOrdinalId
    {
        public IAddressType AddressType
        {
            get { return ExternalContactAddressType; }
            set { ExternalContactAddressType = (ExternalContactAddressType)value; }
        }

        public int OrdinalId { get; set; }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class ExternalOrganizationAddress : IAddress
    {
        public IAddressType AddressType
        {
            get { return ExternalOrganizationAddressType; }
            set { ExternalOrganizationAddressType = (ExternalOrganizationAddressType)value; }
        }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class InsurerAddress : IAddress
    {
        public IAddressType AddressType
        {
            get { return InsurerAddressType; }
            set { InsurerAddressType = (InsurerAddressType)value; }
        }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class PatientAddress : IAddress, IHasOrdinalId
    {
        public PatientAddress()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => Patient.IfNotNull(p => p.PatientAddresses),
                Reflector.GetMember(() => Patient).Name);
        }

        public IAddressType AddressType
        {
            get { return PatientAddressType; }
            set { PatientAddressType = (PatientAddressType)value; }
        }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class ServiceLocationAddress : IAddress
    {
        public IAddressType AddressType
        {
            get { return ServiceLocationAddressType; }
            set { ServiceLocationAddressType = (ServiceLocationAddressType)value; }
        }
    }

    [DisplayName("Address")]
    [MetadataType(typeof(IAddress))]
    public partial class UserAddress : IAddress
    {
        public IAddressType AddressType
        {
            get { return UserAddressType; }
            set { UserAddressType = (UserAddressType)value; }
        }
    }

    public partial class BillingOrganizationAddressType : IAddressType
    {

    }

    public partial class ExternalContactAddressType : IAddressType
    {

    }

    public partial class ExternalOrganizationAddressType : IAddressType
    {

    }

    public partial class InsurerAddressType : IAddressType
    {

    }

    public partial class PatientAddressType : IAddressType
    {

    }

    public partial class ServiceLocationAddressType : IAddressType
    {

    }

    public partial class UserAddressType : IAddressType
    {

    }


    /// <summary>
    ///   Helper/extension methods for Addresses.
    /// </summary>
    public static class Addresses
    {
        /// <summary>
        ///   Gets the address of the specified type or adds one.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="type"> The type. </param>
        /// <param name="notFoundBehavior"> The not found behavior. </param>
        /// <returns> </returns>
        public static T WithAddressType<T>(this ICollection<T> source, Enum type, NotFoundBehavior notFoundBehavior) where T : IAddress, new()
        {
            PropertyInfo addressTypeProperty = typeof(T).GetProperties().FirstOrDefault(p => typeof(IAddressType).IsAssignableFrom(p.PropertyType) && p.PropertyType != typeof(IAddressType)).EnsureNotDefault("Could not find address type for entity {0}.".FormatWith(typeof(T).Name));

            PropertyInfo addressTypeIdProperty = typeof(T).GetProperty(addressTypeProperty.Name + "Id").EnsureNotDefault("Could not find address type id for entity {0}.".FormatWith(typeof(T).Name));

            T address = source.FirstOrDefault(a => (int)addressTypeIdProperty.GetValue(a, null) == Convert.ToInt32(type));

            if (Equals(address, default(T)))
            {
                if (notFoundBehavior == NotFoundBehavior.Exception) throw new ApplicationException("Could not find address with type {0}.".FormatWith(type));
                if (notFoundBehavior == NotFoundBehavior.Add)
                {
                    address = Activator.CreateInstance<T>();
                    addressTypeIdProperty.SetValue(address, Convert.ToInt32(type), null);
                    source.Add(address);
                }
            }
            return address;
        }

        public static T WithAddressType<T>(this ICollection<T> source, Enum type) where T : IAddress, new()
        {
            return WithAddressType(source, type, NotFoundBehavior.None);
        }

        public static bool ContainsAddressType<T>(this ICollection<T> source, Enum type) where T : IAddress, new()
        {
            return !source.WithAddressType(type).Equals(default(T));
        }

        /// <summary>
        /// Get Line1, Line2 and Line3 combined
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string AllLines(this IAddress source)
        {
            var notEmptyLines = new[] { source.Line1, source.Line2, source.Line3 }
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .ToArray();

            return string.Join(Environment.NewLine, notEmptyLines);
        }

        /// <summary>
        /// Sets Line 1, 2 and 3 from concatenated address string
        /// </summary>
        /// <param name="target"></param>
        /// <param name="line1"></param>
        /// <param name="line2"></param>
        /// <param name="line3"></param>
        public static void SetLines(this IAddress target, string line1, string line2, string line3)
        {
            // Set Line 1, 2 and 3
            target.Line1 = line1 ?? ""; // Required
            target.Line2 = line2;
            target.Line3 = line3;
        }

        /// <summary>
        /// Validate the postal code.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool IsValidPostalCode(this IAddress target)
        {
            if (target.PostalCode.IsNullOrEmpty())
            {
                return false;
            }
            if (target.StateOrProvince != null && target.StateOrProvince.IsInUnitedStates)
            {
                var length = target.PostalCode.Length;
                if (target.PostalCode.Substring(length - 1) == "-")
                {
                    target.PostalCode = target.PostalCode.Remove(length - 1);
                }
                var postalCodeExpression = new Regex(@"^[0-9]+\-?[0-9]+$");
                return postalCodeExpression.IsMatch(target.PostalCode);
            }
            return true;
        }
    }
}