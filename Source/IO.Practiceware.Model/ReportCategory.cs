﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    ///     Categories of reports.
    /// </summary>
    public enum ReportCategory
    {
        Scheduling = 1,
        Charges = 2,
        [Display(Name = "Payments and Adjustments")]
        PaymentsAndAdjustments = 3,
        Billing = 4,
        [Display(Name = "Accounts Receivable")]
        AccountsReceivable = 5,
        General = 6,
        Miscellaneous = 7,
    }
}