﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class CommunicationWithOtherProviderOrder
    {
        public ExternalProviderCommunicationType ExternalProviderCommunicationType
        {
            get { return (ExternalProviderCommunicationType)ExternalProviderCommunicationTypeId; }
            set { ExternalProviderCommunicationTypeId = (int)value; }
        }
    }

    public partial class EncounterTransitionOfCareOrder
    {
        public ExternalProviderCommunicationType? ExternalProviderCommunicationType
        {
            get { return ExternalProviderCommunicationTypeId.HasValue ? (ExternalProviderCommunicationType)ExternalProviderCommunicationTypeId : new ExternalProviderCommunicationType?(); }
            set
            {
                if (value.HasValue) ExternalProviderCommunicationTypeId = (int)value;
                else ExternalProviderCommunicationTypeId = null;
            }
        }
    }
    public enum ExternalProviderCommunicationType
    {
        Phone = 1,
        Email = 2,
        Letter = 3,
        ClinicalDocumentArchitectureFile = 4
    }
}
