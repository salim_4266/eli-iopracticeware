<# 
//----------------------------------------------------------------------------
// <copyright file="CSDLToMSL_TPH.tt" company="Microsoft">
//      Copyright � Microsoft Corporation.  All Rights Reserved.
//      This code released under the terms of the 
//      Microsoft Pre-Release Software License Terms
// </copyright>
//----------------------------------------------------------------------------
// This T4 template generates the Entity Data Model Mappings (MSL)
// from the Conceptual Layer (CSDL) using a Table-per-Hierarchy strategy
//---------------------------------------------------------------------
// Note: We will resolve all paths in assembly directives at runtime, taking 
// macros into account (e.g. $(DevEnvDir), $(ProjectDir), etc.). Note that only
// $(DevEnvDir) will be available when the owning project is a website.
#>
<#@ assembly name="System.Core" #>
<#@ assembly name="System.Data" #>
<#@ assembly name="System.Data.Entity" #>
<#@ assembly name="System.Data.Entity.Design" #>
<#@ assembly name="$(SolutionDir)..\Dependencies\Microsoft.Data.Entity.Design.DatabaseGeneration.dll"#>
<#@ assembly name="$(SolutionDir)..\Dependencies\Microsoft.Data.Entity.Design.DatabaseGenerationToolkit.dll"#>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Data" #>
<#@ import namespace="System.Data.Common" #>
<#@ import namespace="System.Data.Entity.Design" #>
<#@ import namespace="System.Data.Metadata.Edm" #>
<#@ import namespace="System.Diagnostics" #>
<#@ import namespace="System.Globalization" #>
<#@ import namespace="Microsoft.Data.Entity.Design.DatabaseGeneration" #>
<#@ import namespace="Microsoft.Data.Entity.Design.DatabaseGenerationToolkit" #>
<#@ import namespace="System.Runtime.Remoting.Messaging" #>
<#@ import namespace="System.Text.RegularExpressions" #>
<#@ template language="C#" debug="true" hostspecific="true" #>
<#@ include file="GenerateTSQL.Utility.ttinclude"#>
<#@ include file="$(ProjectDir)\GenerateEDM.Utility.ttinclude"#>
<#@ output extension = ".xml" #>
<#
	// ==================================================================== //
	// ==================================================================== //
	// TEMPLATE SETUP														//
	// ==================================================================== //
	// ==================================================================== //
Version targetVersion = this.GetInput<Version>(EdmParameterBag.ParameterName.TargetVersion.ToString());
EdmItemCollection edm = EdmExtension.CreateAndValidateEdmItemCollection(this.GetInput<string>(EdmConstants.csdlInputName), targetVersion);
string mslNamespace = SchemaManager.GetMSLNamespaceName(targetVersion);

string csdlModelNamespace = edm.GetNamespace();

#>

<Mapping Space="C-S" xmlns="<#=mslNamespace#>">
	<EntityContainerMapping StorageEntityContainer="<#=WriteEntityContainerName(csdlModelNamespace)#>" CdmEntityContainer="<#=edm.GetEntityContainerName()#>">
<#
	// ==================================================================== //
	// ==================================================================== //
	// ENTITY SET MAPPINGS 													//
	// ==================================================================== //
	// ==================================================================== //
	foreach (EntitySet set in edm.GetAllEntitySets()) {
#>
		<EntitySetMapping Name="<#=set.Name#>">
<#
        IEnumerable<EntityType> containingTypes = set.GetContainingTypes(edm);
        bool containsInheritanceHierarchy = containingTypes.Count() > 1;
        foreach (EntityType type in containingTypes)
        {
#>
			<EntityTypeMapping TypeName="<#=type.Abstract || (type.BaseType != null && type.BaseType.Abstract) ? "IsTypeOf(" + type.FullName + ")" : type.FullName#>">
				<MappingFragment StoreEntitySet="<#=WriteEntityTypeName(set.ElementType, edm)#>">
<#
            foreach (EdmProperty property in set.ElementType.GetKeyProperties()) {
#>
					<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=property.Name#>" />
<#
			}
			
            foreach (EdmProperty property in (type.GetPropertiesInAllBaseTypes().Except(set.ElementType.GetKeyProperties())).Distinct()) {
				if (property.IsComplexProperty()) {

					ConstructComplexProperty(property, property.Name, csdlModelNamespace);
				} else {
#>
					<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=property.Name + (property.TypeUsage.EdmType is EnumType ? "Id" : "")#>" />
<#
				}
			}
            
            if (containsInheritanceHierarchy && !type.Abstract)
            {
#>
                    <Condition ColumnName="__EntityType__" Value="<#=type.Name#>" />
<#          
			} 
#>
				</MappingFragment>
			</EntityTypeMapping>
<#  	} #>
		</EntitySetMapping>
<# 	} 
	
	// ==================================================================== //
	// ==================================================================== //
	// ASSOCIATION SET MAPPINGS												//
	// ==================================================================== //
	// ==================================================================== //	
	
foreach (AssociationSet associationSet in edm.GetAllAssociationSets())
{
	AssociationType association = associationSet.ElementType;
	
	// -------------------------------------------------------------------- //
	// Many-to-Many Associations										    //
	// -------------------------------------------------------------------- //
	
	if (association.IsManyToMany())
	{
#>
		<AssociationSetMapping Name="<#=associationSet.Name#>" TypeName="<#=csdlModelNamespace#>.<#=associationSet.ElementType.Name#>" StoreEntitySet="<#=associationSet.Name#>">
			<EndProperty Name="<#=association.GetEnd1().Name#>">
<#
		foreach (EdmProperty property in association.GetEnd1().GetEntityType().GetKeyProperties()) {
#>
				<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=GetFkName(association, association.GetEnd2(), property.Name)#>"/>
<#		} #>
			</EndProperty>
			<EndProperty Name="<#=association.GetEnd2().Name#>">
<#
		foreach (EdmProperty property in association.GetEnd2().GetEntityType().GetKeyProperties()) {
#>
				<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=GetFkName(association, association.GetEnd1(), property.Name)#>" />
<#		} #>
			</EndProperty>
		</AssociationSetMapping>
<#
	}
	else
	{
		
	// -------------------------------------------------------------------- //
	// Non Many-to-Many Associations										//
	// -------------------------------------------------------------------- //
		
		if (targetVersion == EntityFrameworkVersions.Version1 || association.ReferentialConstraints.Count <= 0)
		{
			AssociationEndMember dependentEnd = association.GetDependentEnd();
			AssociationEndMember principalEnd = association.GetOtherEnd(dependentEnd);
			if (dependentEnd != null && principalEnd != null)
			{
#>
		<AssociationSetMapping Name="<#=associationSet.Name#>" TypeName="<#=csdlModelNamespace#>.<#=associationSet.ElementType.Name#>" StoreEntitySet="<#=WriteEntityTypeName(dependentEnd.GetEntityType(), edm)#>">
			<EndProperty Name="<#=principalEnd.Name#>">
<#
				foreach (EdmProperty property in principalEnd.GetEntityType().GetKeyProperties())
				{
					string columnName = (association.ReferentialConstraints.Count > 0)
						? property.GetDependentProperty(association.ReferentialConstraints.FirstOrDefault()).Name
						: GetFkName(association, dependentEnd, property.Name);
#>
				<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=columnName#>"/>
<#
				}
#>
			</EndProperty>
			<EndProperty Name="<#=dependentEnd.Name#>">
<#
				foreach (EdmProperty property in dependentEnd.GetEntityType().GetKeyProperties())
				{
#>
				<ScalarProperty Name="<#=property.Name#>" ColumnName="<#=property.Name#>"/>
<#
				}				
#>
			</EndProperty>
<#
				// All 0..1 ends of a non PK:PK association require an IsNull=false condition
				if (dependentEnd.RelationshipMultiplicity == RelationshipMultiplicity.ZeroOrOne && !association.IsPKToPK())
				{
					foreach (EdmProperty key in dependentEnd.GetEntityType().GetKeyProperties())
					{
#>
			<Condition ColumnName="<#=key.Name#>" IsNull="false"/>
<#
					}
				}
				
				if (principalEnd.RelationshipMultiplicity == RelationshipMultiplicity.ZeroOrOne)
				{
					foreach (EdmProperty key in principalEnd.GetEntityType().GetKeyProperties())
					{
#>
			<Condition ColumnName="<#=GetFkName(association, dependentEnd, key.Name)#>" IsNull="false"/>
<#
					}
				}
#>
		</AssociationSetMapping>

<#
			}
		}
	}
}	
#>
	</EntityContainerMapping>
</Mapping>
	
<#+
private void ConstructComplexProperty(EdmProperty complexProperty, string columnNamePrefix, string csdlModelNamespace)
{
	// don't add anything if the complex type associated with this property is empty.
	if (complexProperty == null ||
	complexProperty.TypeUsage == null ||
	false == (complexProperty.TypeUsage.EdmType is ComplexType))
	{
		Debug.Fail("We should not have called ConstructComplexProperty on a property that is not a complex property");
		return;
	}
	
	ComplexType complexType = complexProperty.TypeUsage.EdmType as ComplexType;
	if (complexType != null)
	{
		if (complexType.Properties.Count == 0)
		{
			return;
		}
	}
	
#>
	<ComplexProperty Name="<#=complexProperty.Name#>" TypeName="<#=csdlModelNamespace#>.<#=complexProperty.TypeUsage.EdmType.Name#>">
<#+
	complexProperty.VisitComplexProperty(delegate(string namePrefix, EdmProperty nestedProperty)
	{
		if (nestedProperty.IsComplexProperty())
		{
			ConstructComplexProperty(nestedProperty, columnNamePrefix + "_" + nestedProperty.Name, csdlModelNamespace);
		}
		else
		{
#>
		<ScalarProperty Name="<#=nestedProperty.Name#>" ColumnName="<#=columnNamePrefix#>_<#=nestedProperty.Name#>" />
<#+
		}
	}, "_", false);
	
	//if (complexPropertyElement.Nodes().Count() > 0)
	//{
	//	return complexPropertyElement;
	//}
#>
	
	</ComplexProperty>
<#+

}	
	
#>