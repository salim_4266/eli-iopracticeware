﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A person's gender.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Decline to answer
        /// </summary>
        [Description("Choose not to disclose")]
        Decline = 8,

        /// <summary>
        /// Genderqueer, neither exclusively male nor female
        /// </summary>
        [Description("Genderqueer, neither exclusively male nor female")]
        Genderqueer = 7,

        /// <summary>
        /// Male-to-Female (MTF)/ Transgender Female/Trans Woman
        /// </summary>
        [Description("Male-to-Female (MTF)/ Transgender Female/Trans Woman")]
        MTF = 6,

        /// <summary>
        /// Female-to-Male (FTM)/Transgender Male/Trans Man
        /// </summary>
        [Description("Female-to-Male (FTM)/Transgender Male/Trans Man")]
        FTM = 5,

        /// <summary>
        /// Additional gender category or other, please specify
        /// </summary>
        [Description("Additional gender category or other, please specify")]
        Other = 4,

        /// <summary>
        /// Male
        /// </summary>
        [Description("Identifies as Male")]
        Male = 3,

        /// <summary>
        /// Female
        /// </summary>
        [Description("Identifies as Female")]
        Female = 2,

    }
}