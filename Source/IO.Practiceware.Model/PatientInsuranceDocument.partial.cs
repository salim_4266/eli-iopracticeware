﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public class PatientInsuranceDocumentMetadata
    {
        [ContentType(ContentType.Jpg)]
        public byte[] Content { get; set; }
    }

    [MetadataType(typeof(PatientInsuranceDocumentMetadata))]
    public partial class PatientInsuranceDocument
    {
    }
}
