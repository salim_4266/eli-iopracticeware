﻿using System;
using System.Drawing;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class User
    {
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public Color Color
        {
            get { return OleColor.HasValue ? ColorTranslator.FromOle(OleColor.Value) : Color.Empty; }
            set { OleColor = value == Color.Empty ? new int?() : ColorTranslator.ToOle(value); }
        }

        /// <summary>
        /// Gets the name of the ExternalProvider, formatted for readability.
        /// </summary>
        /// <returns>Returns the DisplayName if it exists.
        /// Otherwise returns "{FirstName} {MiddleName} {LastName}", where whitespace is trimmed.</returns>
        public string GetFormattedName()
        {
            // DisplayName may contain additional details such as "M.D." that other properties can't provide.
            if (!string.IsNullOrEmpty(DisplayName)) { return DisplayName; }

            string formattedName = string.Empty;
            bool firstEntry = true;
            foreach (string name in new[] { FirstName, MiddleName, LastName }.Where(name => !string.IsNullOrEmpty(name)))
            {
                formattedName += (firstEntry) ? name : " " + name;
                firstEntry = false;
            }
            return formattedName;
        }
    }
}