﻿using Soaf;
using Soaf.Reflection;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Well known phone number types for billing organizations.
    /// </summary>
    public enum BillingOrganizationPhoneNumberTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Billing = 1,
        /// <summary>
        /// 
        /// </summary>
        Main = 9,

        /// <summary>
        /// 
        /// </summary>
        Fax = 6
    }    

    /// <summary>
    /// Well known phone number types for patients.
    /// </summary>
    public struct PatientPhoneNumberType : IPhoneNumberType, IComparable
    {

        public PatientPhoneNumberType(int value)
        {
            _value = value;
        }

        [Display(Name = "Business")]
        public static readonly PatientPhoneNumberType Business = 2;

        [Display(Name = "Cell")]
        public static readonly PatientPhoneNumberType Cell = 3;

        [Display(Name = "Emergency")]
        public static readonly PatientPhoneNumberType Emergency = 12;

        [Display(Name = "Fax")]
        public static readonly PatientPhoneNumberType Fax = 14;

        [Display(Name = "Beeper")]
        public static readonly PatientPhoneNumberType Beeper = 15;

        [Display(Name = "Night")]
        public static readonly PatientPhoneNumberType Night = 16;

        [Display(Name = "Unknown")]
        public static readonly PatientPhoneNumberType Unknown = 17;
        
        [Display(Name = "Home")]
        public readonly static PatientPhoneNumberType Home = 7;


        public static implicit operator PatientPhoneNumberType(int value)
        {
            return new PatientPhoneNumberType(value);
        }

        public static implicit operator int(PatientPhoneNumberType value)
        {
            return value._value;
        }

        public static bool operator ==(PatientPhoneNumberType x, PatientPhoneNumberType y)
        {
            return x._value == y._value;
        }

        public static bool operator !=(PatientPhoneNumberType x, PatientPhoneNumberType y)
        {
            return x._value != y._value;
        }

        public override bool Equals(object obj)
        {
            if (obj is int || obj is PatientPhoneNumberType) return this == obj.CastTo<PatientPhoneNumberType>();

            return false;
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return GetMemberInfo().Name;           
        }

        private MemberInfo GetMemberInfo()
        {
            if (this == Home)
            {
                return Reflector.GetMember(() => Home);
            }
            if (this == Business)
            {
                return Reflector.GetMember(() => Business);
            }
            if (this == Cell)
            {
                return Reflector.GetMember(() => Cell);
            }
            if (this == Emergency)
            {
                return Reflector.GetMember(() => Emergency);
            }
         
            if (this == Fax)
            {
                return Reflector.GetMember(() => Fax);
            }
            if (this == Beeper)
            {
                return Reflector.GetMember(() => Beeper);
            }
            if (this == Night)
            {
                return Reflector.GetMember(() => Night);
            }
            if (this == Unknown)
            {
                return Reflector.GetMember(() => Unknown);
            }

            throw new Exception("The PatientPhoneNumberType value has no well known value");
        }

        private readonly int _value;

        public int Id
        {
            get { return _value; }
        }

        public string Name
        {
            get { return GetMemberInfo().GetDisplayName(); }
            set
            {

            }
        }

        public string Abbreviation
        {
            get
            {
                return string.Empty;
            }
            set { }
        }

        public bool IsArchived
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        public int CompareTo(object obj)
        {
            return _value.CompareTo(obj.CastTo<PatientPhoneNumberType>()._value);
        }       
    }
   

    /// <summary>
    /// Well known phone number types for insurers.
    /// </summary>
    public enum InsurerPhoneNumberTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Main = 1,
        Authorization = 2,
        Eligibility = 3, // fax
        Provider = 4,
        Claims = 5,
        Submissions = 6,
        Fax = 7
    }

    /// <summary>
    /// Well known phone number types for contacts.
    /// </summary>
    public enum ExternalContactPhoneNumberTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Main = 5,

        /// <summary>
        /// 
        /// </summary>
        Fax = 4
    }

    /// <summary>
    /// Well known phone number types for users.
    /// </summary>
    public enum UserPhoneNumberTypeId
    {
        /// <summary>
        /// 
        /// </summary>
        Main = 10,
        /// <summary>
        /// 
        /// </summary>
        Fax = 11
    }
}