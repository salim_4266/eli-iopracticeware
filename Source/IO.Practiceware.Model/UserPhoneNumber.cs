//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class UserPhoneNumber : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
    	public virtual int UserId
        {
            get { return _userId; }
            set
            {
                if (_userId != value)
                {
                    if (User != null && User.Id != value)
                    {
                        User = null;
                    }
                    _userId = value;
    
    				OnPropertyChanged("UserId");
                }
            }
        }
        private int _userId;
    
    	public virtual int UserPhoneNumberTypeId
        {
            get { return _userPhoneNumberTypeId; }
            set
            {
                if (_userPhoneNumberTypeId != value)
                {
                    if (UserPhoneNumberType != null && UserPhoneNumberType.Id != value)
                    {
                        UserPhoneNumberType = null;
                    }
                    _userPhoneNumberTypeId = value;
    
    				OnPropertyChanged("UserPhoneNumberTypeId");
                }
            }
        }
        private int _userPhoneNumberTypeId;
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string ExchangeAndSuffix
        {
            get { return _exchangeAndSuffix; }
            set 
    		{ 
    			var isChanged = _exchangeAndSuffix != value; 
    			_exchangeAndSuffix = value; 
    			if (isChanged) OnPropertyChanged("ExchangeAndSuffix");
    		}
        }
        private string _exchangeAndSuffix;
    
    	public virtual string Extension
        {
            get { return _extension; }
            set 
    		{ 
    			var isChanged = _extension != value; 
    			_extension = value; 
    			if (isChanged) OnPropertyChanged("Extension");
    		}
        }
        private string _extension;
    
        [Required]
    	public virtual bool IsInternational
        {
            get { return _isInternational; }
            set 
    		{ 
    			var isChanged = _isInternational != value; 
    			_isInternational = value; 
    			if (isChanged) OnPropertyChanged("IsInternational");
    		}
        }
        private bool _isInternational = false;
    
        [Required]
    	public virtual int OrdinalId
        {
            get { return _ordinalId; }
            set 
    		{ 
    			var isChanged = _ordinalId != value; 
    			_ordinalId = value; 
    			if (isChanged) OnPropertyChanged("OrdinalId");
    		}
        }
        private int _ordinalId = 0;
    
    	public virtual string AreaCode
        {
            get { return _areaCode; }
            set 
    		{ 
    			var isChanged = _areaCode != value; 
    			_areaCode = value; 
    			if (isChanged) OnPropertyChanged("AreaCode");
    		}
        }
        private string _areaCode;
    
    	public virtual string CountryCode
        {
            get { return _countryCode; }
            set 
    		{ 
    			var isChanged = _countryCode != value; 
    			_countryCode = value; 
    			if (isChanged) OnPropertyChanged("CountryCode");
    		}
        }
        private string _countryCode;

        #endregion

        #region Navigation Properties
    
    	[Association("User", "UserId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual User User
        {
            get { return _user; }
            set
            {
                if (!ReferenceEquals(_user, value))
                {
                    var previousValue = _user;
                    _user = value;
                    FixupUser(previousValue);
    				OnPropertyChanged("User");
                }
            }
        }
        private User _user;
    
    	[Association("UserPhoneNumberType", "UserPhoneNumberTypeId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual UserPhoneNumberType UserPhoneNumberType
        {
            get { return _userPhoneNumberType; }
            set
            {
                if (!ReferenceEquals(_userPhoneNumberType, value))
                {
                    var previousValue = _userPhoneNumberType;
                    _userPhoneNumberType = value;
                    FixupUserPhoneNumberType(previousValue);
    				OnPropertyChanged("UserPhoneNumberType");
                }
            }
        }
        private UserPhoneNumberType _userPhoneNumberType;

        #endregion

        #region Association Fixup
    
        private void FixupUser(User previousValue)
        {
            if (previousValue != null && previousValue.UserPhoneNumbers.Contains(this))
            {
                previousValue.UserPhoneNumbers.Remove(this);
            }
    
            if (User != null)
            {
                if (!User.UserPhoneNumbers.Contains(this))
                {
                    User.UserPhoneNumbers.Add(this);
                }
                if (User != null && UserId != User.Id)
                {
                    UserId = User.Id;
                }
            }
        }
    
        private void FixupUserPhoneNumberType(UserPhoneNumberType previousValue)
        {
            if (previousValue != null && previousValue.UserPhoneNumbers.Contains(this))
            {
                previousValue.UserPhoneNumbers.Remove(this);
            }
    
            if (UserPhoneNumberType != null)
            {
                if (!UserPhoneNumberType.UserPhoneNumbers.Contains(this))
                {
                    UserPhoneNumberType.UserPhoneNumbers.Add(this);
                }
                if (UserPhoneNumberType != null && UserPhoneNumberTypeId != UserPhoneNumberType.Id)
                {
                    UserPhoneNumberTypeId = UserPhoneNumberType.Id;
                }
            }
        }

        #endregion

    }
}
