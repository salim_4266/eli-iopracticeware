//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Documents a SpecialExam performed, usually during a patient encounter - I broke down exam elements into four types:  procedures, diagnostic tests, special exam elements and other exam elements.  I don't think Other Exam Elements are ever ordered.  Special exam elements are handled with Schedule Appointment (for CL Fitting, Study Visit, Cataract Eval, Lasik Eval, etc) so I didn’t provide orders for special exams or other exam elements.
    /// </summary>
    public partial class PatientExamPerformed : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual int EncounterId
        {
            get { return _encounterId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_encounterId != value)
                    {
                        if (Encounter != null && Encounter.Id != value)
                        {
                            Encounter = null;
                        }
                        _encounterId = value;
        
        				OnPropertyChanged("EncounterId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _encounterId;
    
    	/// <summary>
    	/// Special exam performed.  This is not the correct structure but it will do until we develop the Screen/Question/Answer structure
    	/// </summary>
    	public virtual int ClinicalProcedureId
        {
            get { return _clinicalProcedureId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_clinicalProcedureId != value)
                    {
                        if (ClinicalProcedure != null && ClinicalProcedure.Id != value)
                        {
                            ClinicalProcedure = null;
                        }
                        _clinicalProcedureId = value;
        
        				OnPropertyChanged("ClinicalProcedureId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _clinicalProcedureId;
    
    	public virtual int ClinicalDataSourceTypeId
        {
            get { return _clinicalDataSourceTypeId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_clinicalDataSourceTypeId != value)
                    {
                        if (ClinicalDataSourceType != null && ClinicalDataSourceType.Id != value)
                        {
                            ClinicalDataSourceType = null;
                        }
                        _clinicalDataSourceTypeId = value;
        
        				OnPropertyChanged("ClinicalDataSourceTypeId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _clinicalDataSourceTypeId;
    
    	/// <summary>
    	/// Analogous to provider circling procedures on a superbill for billing, or selecting procedure codes on the vb6 Diagnoses For Billing screen (ClinicalType P)
    	/// </summary>
        [Required]
    	public virtual bool IsBillable
        {
            get { return _isBillable; }
            set 
    		{ 
    			var isChanged = _isBillable != value; 
    			_isBillable = value; 
    			if (isChanged) OnPropertyChanged("IsBillable");
    		}
        }
        private bool _isBillable;
    
    	public virtual int PatientId
        {
            get { return _patientId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_patientId != value)
                    {
                        if (Patient != null && Patient.Id != value)
                        {
                            Patient = null;
                        }
                        _patientId = value;
        
        				OnPropertyChanged("PatientId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _patientId;
    
        [Required]
    	public virtual System.DateTime PerformedDateTime
        {
            get { return _performedDateTime; }
            set 
    		{ 
    			var isChanged = _performedDateTime != value; 
    			_performedDateTime = value; 
    			if (isChanged) OnPropertyChanged("PerformedDateTime");
    		}
        }
        private System.DateTime _performedDateTime;
    
    	public virtual Nullable<int> TimeQualifierId
        {
            get { return _timeQualifierId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_timeQualifierId != value)
                    {
                        if (TimeQualifier != null && TimeQualifier.Id != value)
                        {
                            TimeQualifier = null;
                        }
                        _timeQualifierId = value;
        
        				OnPropertyChanged("TimeQualifierId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _timeQualifierId;

        #endregion

        #region Navigation Properties
    
    	[Association("Encounter", "EncounterId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Encounter Encounter
        {
            get { return _encounter; }
            set
            {
                if (!ReferenceEquals(_encounter, value))
                {
                    var previousValue = _encounter;
                    _encounter = value;
                    FixupEncounter(previousValue);
    				OnPropertyChanged("Encounter");
                }
            }
        }
        private Encounter _encounter;
    
    	[Association("ClinicalProcedure", "ClinicalProcedureId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ClinicalProcedure ClinicalProcedure
        {
            get { return _clinicalProcedure; }
            set
            {
                if (!ReferenceEquals(_clinicalProcedure, value))
                {
                    var previousValue = _clinicalProcedure;
                    _clinicalProcedure = value;
                    FixupClinicalProcedure(previousValue);
    				OnPropertyChanged("ClinicalProcedure");
                }
            }
        }
        private ClinicalProcedure _clinicalProcedure;
    
    	[Association("ClinicalDataSourceType", "ClinicalDataSourceTypeId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ClinicalDataSourceType ClinicalDataSourceType
        {
            get { return _clinicalDataSourceType; }
            set
            {
                if (!ReferenceEquals(_clinicalDataSourceType, value))
                {
                    var previousValue = _clinicalDataSourceType;
                    _clinicalDataSourceType = value;
                    FixupClinicalDataSourceType(previousValue);
    				OnPropertyChanged("ClinicalDataSourceType");
                }
            }
        }
        private ClinicalDataSourceType _clinicalDataSourceType;
    
    	[Association("Patient", "PatientId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Patient Patient
        {
            get { return _patient; }
            set
            {
                if (!ReferenceEquals(_patient, value))
                {
                    var previousValue = _patient;
                    _patient = value;
                    FixupPatient(previousValue);
    				OnPropertyChanged("Patient");
                }
            }
        }
        private Patient _patient;
    
    	[Association("PatientExamPerformedQuestionAnswers", "Id", "PatientExamPerformedId")]
        public virtual System.Collections.Generic.ICollection<PatientExamPerformedQuestionAnswer> PatientExamPerformedQuestionAnswers
        {
            get
            {
                if (_patientExamPerformedQuestionAnswers == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<PatientExamPerformedQuestionAnswer>();
                    newCollection.ItemSet += FixupPatientExamPerformedQuestionAnswersItemSet;
                    newCollection.ItemRemoved += FixupPatientExamPerformedQuestionAnswersItemRemoved;
                    _patientExamPerformedQuestionAnswers = newCollection;
                }
                return _patientExamPerformedQuestionAnswers;
            }
            set
            {
                if (!ReferenceEquals(_patientExamPerformedQuestionAnswers, value))
                {
                    var previousValue = _patientExamPerformedQuestionAnswers as Soaf.Collections.FixupCollection<PatientExamPerformedQuestionAnswer>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupPatientExamPerformedQuestionAnswersItemSet;
                        previousValue.ItemRemoved -= FixupPatientExamPerformedQuestionAnswersItemRemoved;
                    }
                    _patientExamPerformedQuestionAnswers = value;
                    var newValue = value as Soaf.Collections.FixupCollection<PatientExamPerformedQuestionAnswer>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupPatientExamPerformedQuestionAnswersItemSet;
                        newValue.ItemRemoved += FixupPatientExamPerformedQuestionAnswersItemRemoved;
                    }
    				OnPropertyChanged("PatientExamPerformedQuestionAnswers");
                }
            }
        }
        private System.Collections.Generic.ICollection<PatientExamPerformedQuestionAnswer> _patientExamPerformedQuestionAnswers;
    
    	[Association("TimeQualifier", "TimeQualifierId", "Id")]
        public virtual TimeQualifier TimeQualifier
        {
            get { return _timeQualifier; }
            set
            {
                if (!ReferenceEquals(_timeQualifier, value))
                {
                    var previousValue = _timeQualifier;
                    _timeQualifier = value;
                    FixupTimeQualifier(previousValue);
    				OnPropertyChanged("TimeQualifier");
                }
            }
        }
        private TimeQualifier _timeQualifier;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupEncounter(Encounter previousValue)
        {
            if (previousValue != null && previousValue.PatientExamPerformeds.Contains(this))
            {
                previousValue.PatientExamPerformeds.Remove(this);
            }
    
            if (Encounter != null)
            {
                if (!Encounter.PatientExamPerformeds.Contains(this))
                {
                    Encounter.PatientExamPerformeds.Add(this);
                }
                if (Encounter != null && EncounterId != Encounter.Id)
                {
                    EncounterId = Encounter.Id;
                }
            }
        }
    
        private void FixupClinicalProcedure(ClinicalProcedure previousValue)
        {
            if (previousValue != null && previousValue.PatientExamPerformeds.Contains(this))
            {
                previousValue.PatientExamPerformeds.Remove(this);
            }
    
            if (ClinicalProcedure != null)
            {
                if (!ClinicalProcedure.PatientExamPerformeds.Contains(this))
                {
                    ClinicalProcedure.PatientExamPerformeds.Add(this);
                }
                if (ClinicalProcedure != null && ClinicalProcedureId != ClinicalProcedure.Id)
                {
                    ClinicalProcedureId = ClinicalProcedure.Id;
                }
            }
        }
    
        private void FixupClinicalDataSourceType(ClinicalDataSourceType previousValue)
        {
            if (previousValue != null && previousValue.PatientExamPerformeds.Contains(this))
            {
                previousValue.PatientExamPerformeds.Remove(this);
            }
    
            if (ClinicalDataSourceType != null)
            {
                if (!ClinicalDataSourceType.PatientExamPerformeds.Contains(this))
                {
                    ClinicalDataSourceType.PatientExamPerformeds.Add(this);
                }
                if (ClinicalDataSourceType != null && ClinicalDataSourceTypeId != ClinicalDataSourceType.Id)
                {
                    ClinicalDataSourceTypeId = ClinicalDataSourceType.Id;
                }
            }
        }
    
        private void FixupPatient(Patient previousValue)
        {
            if (previousValue != null && previousValue.PatientExamPerformeds.Contains(this))
            {
                previousValue.PatientExamPerformeds.Remove(this);
            }
    
            if (Patient != null)
            {
                if (!Patient.PatientExamPerformeds.Contains(this))
                {
                    Patient.PatientExamPerformeds.Add(this);
                }
                if (Patient != null && PatientId != Patient.Id)
                {
                    PatientId = Patient.Id;
                }
            }
        }
    
        private void FixupTimeQualifier(TimeQualifier previousValue)
        {
            if (previousValue != null && previousValue.PatientExamPerformeds.Contains(this))
            {
                previousValue.PatientExamPerformeds.Remove(this);
            }
    
            if (TimeQualifier != null)
            {
                if (!TimeQualifier.PatientExamPerformeds.Contains(this))
                {
                    TimeQualifier.PatientExamPerformeds.Add(this);
                }
                if (TimeQualifier != null && TimeQualifierId != TimeQualifier.Id)
                {
                    TimeQualifierId = TimeQualifier.Id;
                }
            }
            else if (!_settingFK)
            {
                TimeQualifierId = null;
            }
        }
    
        private void FixupPatientExamPerformedQuestionAnswersItemSet(object sender, Soaf.EventArgs<PatientExamPerformedQuestionAnswer> e)
        {
            var item = e.Value;
     
            item.PatientExamPerformed = this;
        }
    
        private void FixupPatientExamPerformedQuestionAnswersItemRemoved(object sender, Soaf.EventArgs<PatientExamPerformedQuestionAnswer> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.PatientExamPerformed, this))
            {
                item.PatientExamPerformed = null;
            }
        }
    

        #endregion

    }
}
