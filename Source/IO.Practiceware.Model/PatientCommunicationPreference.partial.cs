﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum PatientCommunicationType
    {
        [Display(Name = "Recalls", Order = 3)]
        Recall = 1,

        [Display(Name = "Confirmations", Order = 2)]
        AppointmentConfirmation = 2,

        [Display(Name = "Statements", Order = 4)]
        Statement = 3,

        [Display(Name = "Clinical Documents", Order = 6)]
        ClinicalInformation = 4,

        [Display(Name = "General Announcements", Order = 8)]
        GeneralCommunication = 5,

        [Display(Name = "Clinical Follow-Up", Order = 7)]
        ClinicalFollowUp = 6,

        [Display(Name = "Emergency", Order = 1)]
        Emergency = 7,

        [Display(Name = "Collections", Order = 5)]
        Collection = 8
    }

    public enum CommunicationMethodType
    {
        [Display(Name = "Email")]
        Email = 1,
        [Display(Name = "Fax")]
        Fax = 2,
        [Display(Name = "Letter")]
        Letter = 3,
        [Display(Name = "Postcard")]
        Postcard = 4,
        [Display(Name = "Text Message")]
        TextMessage = 5,
        [Display(Name = "Phone Call")]
        PhoneCall = 6
    }

    public partial class CommunicationTransaction
    {
        public CommunicationMethodType CommunicationMethodType
        {
            get { return (CommunicationMethodType)CommunicationMethodTypeId; }
            set { CommunicationMethodTypeId = (int)value; }
        }
    }
    
    public partial class PatientCommunicationPreference
    {
        public PatientCommunicationType PatientCommunicationType
        {
            get { return (PatientCommunicationType)PatientCommunicationTypeId; }
            set { PatientCommunicationTypeId = (int)value; }
        }

        public CommunicationMethodType CommunicationMethodType
        {
            get { return (CommunicationMethodType)CommunicationMethodTypeId; }
            set { CommunicationMethodTypeId = (int)value; }
        }

        public int SelectedContactPatientId
        {
            get
            {
                var destType = GetType();
                if (destType == typeof(PatientAddressCommunicationPreference))
                {
                    var custom = (PatientAddressCommunicationPreference) this;

                    if (custom.PatientAddress != null) return custom.PatientAddress.PatientId;
                }
                else if (destType == typeof(PatientEmailAddressCommunicationPreference))
                {
                    var custom = (PatientEmailAddressCommunicationPreference)this;

                    if (custom.PatientEmailAddress != null) return custom.PatientEmailAddress.PatientId;
                }
                else if (destType == typeof(PatientPhoneNumberCommunicationPreference))
                {
                    var custom = (PatientPhoneNumberCommunicationPreference)this;

                    if (custom.PatientPhoneNumber != null) return custom.PatientPhoneNumber.PatientId;                    
                }

                throw new MemberAccessException(string.Format("The destination type {0} does not match with any well-known types.", destType.FullName));
            }
        }

        public long DestinationId
        {
            get
            {
                var destType = GetType();
                if (destType == typeof(PatientAddressCommunicationPreference))
                {
                    return (int)((PatientAddressCommunicationPreference) this).PatientAddressId;
                }
                if (destType == typeof(PatientEmailAddressCommunicationPreference))
                {
                    return ((PatientEmailAddressCommunicationPreference) this).PatientEmailAddressId;
                }
                if (destType == typeof(PatientPhoneNumberCommunicationPreference))
                {
                    return ((PatientPhoneNumberCommunicationPreference) this).PatientPhoneNumberId;
                }

                throw new MemberAccessException(string.Format("The destination type {0} does not match with any well-known types.", destType.FullName));
            }
            set
            {
                var destType = GetType();
                if (destType == typeof(PatientAddressCommunicationPreference))
                {
                    ((PatientAddressCommunicationPreference)this).PatientAddressId = value;
                }
                else if (destType == typeof(PatientEmailAddressCommunicationPreference))
                {
                    ((PatientEmailAddressCommunicationPreference)this).PatientEmailAddressId = (int)value;
                }
                else if (destType == typeof (PatientPhoneNumberCommunicationPreference))
                {
                    ((PatientPhoneNumberCommunicationPreference) this).PatientPhoneNumberId = (int)value;
                }
                else
                {
                    throw new MemberAccessException(string.Format("The destination type {0} does not match with any well-known types.", destType.FullName));                    
                }
            }
        }
    }

    public static class PatientCommunicationPreferenceFactory
    {
        public static PatientCommunicationPreference CreatePreference<T>() where T : PatientCommunicationPreference
        {
            return CreatePreference(typeof (T));
        }

        public static PatientCommunicationPreference CreatePreference(Type preferenceType)
        {
            if (preferenceType.BaseType != typeof (PatientCommunicationPreference))
            {
                throw new Exception(string.Format("The preferenceType {0}'s base type is not of type {1}", preferenceType.FullName, typeof (PatientCommunicationPreference).FullName));
            }

            return (PatientCommunicationPreference)Activator.CreateInstance(preferenceType);
        }
    }

    public static class PatientCommunicationTypeExtensions
    {
        /// <summary>
        /// Determines whether or not this type has IO automations associated with it.  E.g. sending statements
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool HasIoAutomation(this PatientCommunicationType type)
        {
            return PatientCommunicationMappings.PatientCommunicationTypeToHasIoAutomationMapping[type];
        }
    }

    public static class PatientCommunicationMappings
    {
        public static Dictionary<PatientCommunicationType, bool> PatientCommunicationTypeToHasIoAutomationMapping = new Dictionary<PatientCommunicationType, bool>
                                                                                                                        {

                                                                                                                            {PatientCommunicationType.Recall, true},
                                                                                                                            {PatientCommunicationType.AppointmentConfirmation, true},
                                                                                                                            {PatientCommunicationType.Statement, true},
                                                                                                                            {PatientCommunicationType.ClinicalInformation, true},
                                                                                                                            {PatientCommunicationType.GeneralCommunication, true},
                                                                                                                            {PatientCommunicationType.ClinicalFollowUp, true},
                                                                                                                            {PatientCommunicationType.Emergency, false},
                                                                                                                            {PatientCommunicationType.Collection, true}
                                                                                                                        };

        /// <summary>
        /// Mapping of Communication Method (e.g. email, fax, letter, postcard ...) to the Type of 
        /// PatientCommunicationPreference (e.g. PatientEmailAddressCommunicationPreference, PatientPhoneNumberCommunicationPreference, 
        /// PatientAddressCommunicationPreference)
        /// </summary>
        public static Dictionary<CommunicationMethodType, Type> CommunicationMethodToPatientCommunicationPreferenceMapping = new Dictionary<CommunicationMethodType, Type>
                                                                                                                    {
                                                                                                                        {CommunicationMethodType.Email, typeof (PatientEmailAddressCommunicationPreference)},
                                                                                                                        {CommunicationMethodType.Fax, typeof (PatientPhoneNumberCommunicationPreference)},
                                                                                                                        {CommunicationMethodType.Letter, typeof (PatientAddressCommunicationPreference)},
                                                                                                                        {CommunicationMethodType.Postcard, typeof (PatientAddressCommunicationPreference)},
                                                                                                                        {CommunicationMethodType.TextMessage, typeof (PatientPhoneNumberCommunicationPreference)},
                                                                                                                        {CommunicationMethodType.PhoneCall, typeof (PatientPhoneNumberCommunicationPreference)},
                                                                                                                    };

        /// <summary>
        /// Mapping of PatientCommunicationType (e.g. Clinical Documents, Statements ...) to which method of communication it 
        /// supports (e.g. E-mail, Fax, Letter, Postcard ...)
        /// </summary>
        public static Dictionary<PatientCommunicationType, List<CommunicationMethodType>> PatientCommunicationTypeToCommunicationMethodMapping = new Dictionary<PatientCommunicationType, List<CommunicationMethodType>>
                                                                                                                                                     {
                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.AppointmentConfirmation, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter,
                                                                                                                                                                                                                       CommunicationMethodType.Postcard,
                                                                                                                                                                                                                       CommunicationMethodType.TextMessage,
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.ClinicalFollowUp, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter,                                                                                                                                                                                                                     
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.ClinicalInformation, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter                                                                                                                                                                                                                      
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.Collection, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter,                                                                                                                                                                                                                       
                                                                                                                                                                                                                       CommunicationMethodType.TextMessage,
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.Emergency, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {                                                                                                                                                                                                                      
                                                                                                                                                                                                                       CommunicationMethodType.TextMessage,
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.GeneralCommunication, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter,                                                                                                                                                                                                                       
                                                                                                                                                                                                                       CommunicationMethodType.TextMessage,
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.Recall, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter,
                                                                                                                                                                                                                       CommunicationMethodType.Postcard,
                                                                                                                                                                                                                       CommunicationMethodType.TextMessage,
                                                                                                                                                                                                                       CommunicationMethodType.PhoneCall
                                                                                                                                                                                                                   }
                                                                                                                                                         },

                                                                                                                                                         {
                                                                                                                                                             PatientCommunicationType.Statement, new List<CommunicationMethodType>
                                                                                                                                                                                                                   {
                                                                                                                                                                                                                       CommunicationMethodType.Email,
                                                                                                                                                                                                                       CommunicationMethodType.Fax,
                                                                                                                                                                                                                       CommunicationMethodType.Letter                                                                                                                                                                                                                       
                                                                                                                                                                                                                   }
                                                                                                                                                         }                                                                                                                                                        
                                                                                                                                                     };
    }
}

