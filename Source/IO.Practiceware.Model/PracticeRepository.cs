﻿using System.Data.Entity;
using System.Diagnostics;
using System.Reflection;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

namespace IO.Practiceware.Model
{
    public partial interface IPracticeRepository : ISupportsLazyLoading
    {
        Func<Type, IQueryable> AsQueryableFactory();
    }

    /// <summary>
    /// </summary>
    public partial class PracticeRepository
    {
        private Func<Type, IQueryable> _queryableFactory;

        private static readonly Dictionary<Type, string> TypeToDbObjectNameMap = typeof(IPracticeRepository)
            .GetProperties()
            .Where(p => p.PropertyType.IsGenericTypeFor(typeof(IQueryable<>)))
            .ToDictionary(p => p.PropertyType.GetGenericArguments()[0], p => "model." + p.Name);

        private static readonly Dictionary<string, Type> DbObjectNameToTypeMap =
            TypeToDbObjectNameMap
            .ToDictionary(i => i.Value, i => i.Key);


        public static Type GetEntityType(string objectName)
        {
            return DbObjectNameToTypeMap.GetValue(objectName);
        }

        public static string GetDbObjectName(Type entityType)
        {
            return TypeToDbObjectNameMap.GetValue(entityType);
        }

        #region IPracticeRepository Members

        public virtual bool IsLazyLoadingEnabled { get; set; }

        public Func<Type, IQueryable> AsQueryableFactory()
        {
            return _queryableFactory ?? (_queryableFactory = Queryables.GetQueryableFactory(this));
        }

        #endregion

        public void OnAcceptingChanges(object value, ObjectState state)
        {
            //    if (new[] { ObjectState.Deleted, ObjectState.Unchanged }.Contains(state)) return;
            //    value.Validate(true);
        }
    }

    public static class CommonQueries
    {
        [DbFunction("IO.Practiceware.Model", "ConvertToMMDDYYString")]
        public static string ConvertToMMDDYYString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("MMddyy") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "ConvertToMMDDYYYYString")]
        public static string ConvertToMMDDYYYYString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("MMddyyyy") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "GetMonthString")]
        public static string GetMonthString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("MM") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "GetDayString")]
        public static string GetDayString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("dd") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "GetYearString")]
        public static string GetYearString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("yy") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "GetFullYearString")]
        public static string GetFullYearString(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("yyyy") : String.Empty;
        }

        [DbFunction("IO.Practiceware.Model", "ConvertInt32ToString")]
        public static string ConvertInt32ToString(int value)
        {
            return value.ToString();
        }

        [DbFunction("IO.Practiceware.Model", "ConvertGuidToString")]
        public static string ConvertGuidToString(Guid value)
        {
            return value.ToString();
        }

        [DbFunction("Edm", "AddDays")]
        public static DateTime? AddDays(DateTime? value, int? days)
        {
            return value.GetValueOrDefault().AddDays(days.GetValueOrDefault());
        }

        [DbFunction("Edm", "AddYears")]
        public static DateTime? AddYears(DateTime? value, int? years)
        {
            return value.GetValueOrDefault().AddYears(years.GetValueOrDefault());
        }

        [DbFunction("IO.Practiceware.Model", "GetDatePart")]
        public static DateTime GetDatePart(DateTime value)
        {
            return value.Date;
        }

        [DbFunction("IO.Practiceware.Model", "GetTimePartWithoutSeconds")]
        public static DateTime GetTimePartWithoutSeconds(DateTime value)
        {
            return new DateTime().AddMinutes(value.TimeOfDay.TotalMinutes);
        }
    }

    /// <summary>
    /// Helper/extension methods for Countries.
    /// </summary>
    public static class CountryQueries
    {
        /// <summary>
        /// Orders alphabetically, but with the US first.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static IQueryable<Country> OrderedUnitedStatesFirst(this IQueryable<Country> source)
        {
            return source.OrderBy(i => i.Abbreviation2Letters == "US" ? 0 : 1).ThenBy(i => i.Name);
        }

        /// <summary>
        /// Returns only countries with states or provinces.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static IQueryable<Country> WithStateOrProvinces(this IQueryable<Country> source)
        {
            return source.Where(i => i.StateProvinces.Any());
        }
    }

    /// <summary>
    ///   Helper/extension methods for Patients.
    /// </summary>
    public static class PatientQueries
    {
        public static Expression<Func<PatientPhoneNumber, string>> GetRawPhoneNumber = pn =>
            (pn.CountryCode == null ? "" : pn.CountryCode.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""))
            + (pn.AreaCode == null ? "" : pn.AreaCode.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""))
            + (pn.ExchangeAndSuffix == null ? "" : pn.ExchangeAndSuffix.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""))
            + (pn.Extension == null ? "" : pn.Extension.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""));

        public static Expression<Func<PatientPhoneNumber, string>> FormatPhoneNumber = pn =>
            (pn.CountryCode == "1" || pn.CountryCode == null || pn.CountryCode == ""
                ? "(" : (pn.CountryCode + " ("))
            + (pn.AreaCode == "" || pn.AreaCode == null ? pn.ExchangeAndSuffix.Substring(0, 3) : pn.AreaCode) + ") "
            + (pn.AreaCode == "" || pn.AreaCode == null ? pn.ExchangeAndSuffix.Substring(3, 3) + "-" + pn.ExchangeAndSuffix.Substring(6, 4) :
                                                          pn.ExchangeAndSuffix.Substring(0, 3) + "-" + pn.ExchangeAndSuffix.Substring(3, 4))
            + (pn.Extension == null || pn.Extension == "" ? "" : (" x" + pn.Extension));

        [DbFunction("IO.Practiceware.Model", "GetAge")]
        public static int GetAge(Patient patient)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///   Includes the addresses and related entities for the query.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static IQueryable<Patient> IncludeAddresses(this IQueryable<Patient> source)
        {
            return Queryables.Include(source, i => i.PatientAddresses.Select(pa => new { pa.PatientAddressType, pa.StateOrProvince.Country }));
        }

        /// <summary>
        ///   Includes the phone numbers and related entities for the query.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static IQueryable<Patient> IncludePhoneNumbers(this IQueryable<Patient> source)
        {
            return Queryables.Include(source, p => p.PatientPhoneNumbers);
        }

        /// <summary>
        ///   Searches the patients using the specified search text. Tries to parse the search text into usable search criteria.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <param name="searchText"> The search text. </param>
        /// <returns> </returns>
        public static IQueryable<Patient> Search(this IQueryable<Patient> source, string searchText)
        {
            Expression<Func<Patient, bool>> predicate = PredicateBuilder<Patient>.True;

            // split search text into individual terms
            IEnumerable<string> searchTerms = searchText.Split(' ')
                .Select(i => i.Trim().Replace("-", string.Empty))
                .Where(i => i.IsNotNullOrEmpty());

            predicate = searchTerms.Aggregate(predicate, (current, searchTerm) => current.And(VisitSearchTerm(searchTerm)));

            return source.Where(predicate);
        }


        /// <summary>
        ///   Visits the search term and builds a predicate.
        /// </summary>
        /// <param name="searchTerm"> The search term. </param>
        /// <returns> </returns>
        private static Expression<Func<Patient, bool>> VisitSearchTerm(string searchTerm)
        {
            DateTime dateTime;
            if (DateTime.TryParse(searchTerm, out dateTime))
            {
                return p => p.DateOfBirth == dateTime;
            }

            int number;
            if (int.TryParse(searchTerm, out number))
            {
                // try match on patient id, prior patient code, or ssn
                Expression<Func<Patient, bool>> predicate = PredicateBuilder<Patient>.Create(p => p.Id == number)
                    .Or(p => p.PriorPatientCode == number.ToString())
                    .Or(p => p.SocialSecurityNumber == number.ToString());

                return predicate;
            }

            if (searchTerm.Count(i => i == ',') == 1)
            {
                // search on lastname, firstname

                string[] nameParts = searchTerm.Split(',').Select(i => i.Trim()).ToArray();

                return p => p.LastName.ToLower().StartsWith(nameParts[0].ToLower()) && p.FirstName.ToLower().StartsWith(nameParts[1].ToLower());
            }


            // default to an "Or" search on firstname, lastname and display name
            return PredicateBuilder<Patient>.Create(
                p => p.LastName.ToLower().StartsWith(searchTerm.ToLower())).Or(
                    p => p.FirstName.ToLower().StartsWith(searchTerm.ToLower()));
        }

        /// <summary>
        ///   Returns a dictionary of patients by prior patient code.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static IDictionary<string, int> ByPriorPatientCode(this IQueryable<Patient> source)
        {
            return source.Where(i => i.PriorPatientCode != null && i.PriorPatientCode != string.Empty)
                .GroupBy(i => i.PriorPatientCode)
                .Select(i => new { i.Key, i.FirstOrDefault().Id })
                .ToDictionary(i => i.Key, i => i.Id);
        }
    }

    public static class InsuranceQueries
    {
        public static readonly Expression<Func<PatientInsurance, InsuranceType, bool>> IsInsuranceType = (pi, it) => pi.InsuranceType == it;

        public static readonly Expression<Func<PatientInsurance, Appointment, bool>> IsForAppointment = (i, appointment) => i.InsuredPatientId == appointment.Encounter.PatientId && i.InsurancePolicy.StartDateTime <= appointment.DateTime && (i.EndDateTime == null || i.EndDateTime >= appointment.DateTime);

        public static readonly Expression<Func<PatientInsurance, DateTime, bool>> IsActiveForDateTime = (i, d) => i.InsurancePolicy.StartDateTime <= d && (i.EndDateTime == null || i.EndDateTime >= d);

        public static IQueryable<PatientInsurance> ForAppointment(this IQueryable<PatientInsurance> source, Appointment appointment)
        {
            return source.Where(i => IsForAppointment.Invoke(i, appointment));
        }
    }


    public static class EligibilityQueries
    {
        public static void LoadForEligibilityFileGeneration(this IEnumerable<Patient> source, IPracticeRepository practiceRepository)
        {
        }
    }

    public static class InvoiceQueries
    {

        public static void LoadForClaimFileGeneration(this IEnumerable<Invoice> source, IPracticeRepository practiceRepository)
        {
            Func<Type, IQueryable> factory = practiceRepository.AsQueryableFactory();

            Invoice[] sourceItems = source.ToArray();

            IEnumerable<Tuple<object, PropertyInfo>> failures;

            factory.Load(sourceItems, out failures,
                    i => i.InvoiceReceivables.Select(ir => ir.Adjustments.Select(a => a.ClaimAdjustmentReasonCode)),
                    i => i.InvoiceReceivables.Select(ir => ir.Adjustments.Select(a => a.AdjustmentType)),
                    i => i.InvoiceReceivables.Select(ir => ir.Adjustments.Select(a => a.Insurer)),
                    i => i.InvoiceReceivables.Select(ir => ir.Adjustments.Select(a => a.FinancialBatch)),
                    i => i.InvoiceReceivables.Select(ir => ir.FinancialInformations.Select(fi => fi.ClaimAdjustmentReasonCode)),
                    i => i.InvoiceReceivables.Select(ir => ir.FinancialInformations.Select(fi => fi.FinancialBatch)),
                    i => i.InvoiceReceivables.Select(ir => ir.PatientInsuranceReferral.ExternalProvider.IdentifierCodes),
                    i => i.InvoiceReceivables.Select(ir => ir.PatientInsuranceAuthorization),
                    i => i.InvoiceReceivables.Select(ir => ir.PatientInsurance.InsuredPatient),
                    i => i.InvoiceReceivables.Select(ir => ir.PatientInsurance.InsurancePolicy.PolicyholderPatient.PatientPhoneNumbers),
                    i => i.InvoiceReceivables.Select(ir => ir.BillingServiceTransactions.Select(bst => new object[]
                             {
                                 bst.BillingService.BillingServiceModifiers.Select(bsm => bsm.ServiceModifier), 
                                 bst.BillingService.Adjustments.Select(a => a.AdjustmentType), 
                                 bst.BillingService.Invoice, 
                                 bst.BillingService.BillingServiceBillingDiagnoses.Select(bsbd => bsbd.BillingDiagnosis), 
                                 bst.ClaimFileReceiver,
                                 bst.BillingService.OrderingExternalProvider.IdentifierCodes,
                                 bst.BillingService.EncounterService,
                                 bst.BillingService.FacilityEncounterService,
                                 bst.BillingService.FacilityEncounterService.RevenueCode,
                                 bst.BillingService.BillingServiceComments,
                                 bst.BillingService.EncounterService.EncounterServiceType
                             })),
                    i => i.InvoiceReceivables.Select(ir => new
                    {
                        InsurerAddress = ir.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses.Select(a => a.StateOrProvince.Country),
                        PatientAddress = ir.PatientInsurance.InsurancePolicy.PolicyholderPatient.PatientAddresses.Select(a => a.StateOrProvince.Country),
                        Doctor = ir.PatientInsurance.InsurancePolicy.Insurer.DoctorInsurerAssignments.Select(dia => dia.Doctor)
                    }),
                    i => i.BillingProvider.User.Roles,
                    i => i.BillingProvider.User.Providers.Select(p => p.ClaimFileOverrideInsurers),
                    i => i.BillingProvider.User.Providers.Select(p => p.BillingOrganization.BillingOrganizationPhoneNumbers),
                    i => i.BillingProvider.User.Providers.Select(p => p.BillingOrganization.BillingOrganizationAddresses.Select(a => a.StateOrProvince.Country)),
                    i => i.BillingProvider.User.Providers.Select(p => p.BillingOrganization.IdentifierCodes),
                    i => i.BillingProvider.User.Providers.Select(p => p.IdentifierCodes),
                    i => i.BillingProvider.User.Providers.Select(p => p.ServiceLocation.ServiceLocationAddresses.Select(a => a.StateOrProvince.Country)),
                    i => i.BillingProvider.User.Providers.Select(p => p.ServiceLocation.IdentifierCodes),
                    i => i.BillingProvider.BillingOrganization.BillingOrganizationPhoneNumbers,
                    i => i.BillingProvider.BillingOrganization.BillingOrganizationAddresses.Select(boa => boa.StateOrProvince),
                    i => i.BillingProvider.BillingOrganization.IdentifierCodes,
                    i => i.BillingProvider.IdentifierCodes,
                    i => i.BillingProvider.ServiceLocation.ServiceLocationAddresses,
                    i => i.BillingProvider.ServiceLocation.ServiceLocationAddresses.Select(a => a.StateOrProvince.Country),
                    i => i.BillingProvider.ServiceLocation.IdentifierCodes,
                    i => i.InvoiceSupplemental.AccidentStateOrProvince,
                    i => i.InvoiceSupplemental.OrderingProvider,
                    i => i.InvoiceSupplemental.OrderingProvider.IdentifierCodes,
                    i => i.InvoiceSupplemental.SupervisingProvider,
                    i => i.InvoiceSupplemental.SupervisingProvider.User,
                    i => i.InvoiceSupplemental.SupervisingProvider.ServiceLocation,
                    i => i.InvoiceSupplemental.SupervisingProvider.IdentifierCodes,
                    i => i.ClinicalInvoiceProvider.Provider.User,
                    i => i.Encounter.ServiceLocation.ServiceLocationAddresses.Select(a => a.StateOrProvince),
                    i => i.Encounter.ServiceLocation.ServiceLocationAddresses,
                    i => i.Encounter.ServiceLocation.IdentifierCodes,
                    i => i.Encounter.Patient.PatientPhoneNumbers,
                    i => i.Encounter.Patient.PatientAddresses.Select(a => a.StateOrProvince.Country),
                    i => i.ServiceLocationCode,
                    i => i.Encounter.Appointments.OfType<UserAppointment>().Select(ua => ua.User).Select(u => u.Providers.Select(p => p.IdentifierCodes)),
                    i => i.ReferringExternalProvider.IdentifierCodes,
                    i => i.BillingDiagnoses,
                    i => i.BillingDiagnoses.Select(bd => bd.ExternalSystemEntityMapping)
                    );

            if (failures.Any())
            {
                var message = failures.Select(f => "Property {0} failed to load on {1} with Id {2}.".FormatWith(f.Item2.Name, f.Item2.DeclaringType.IfNotNull(t => t.Name) ?? f.Item1.IfNotNull(i => i.GetType().Name), f.Item1 is IHasId ? f.Item1.CastTo<IHasId>().Id : f.Item1.IfNotNull(i => i.ToString())))
                    .Join(Environment.NewLine);
                Trace.TraceWarning(message);
            }

        }
    }

    public static class AppointmentSearchQueries
    {
        [DbFunction("IO.Practiceware.Model", "GetTimeOfDayInSeconds")]
        public static int GetTimeOfDayInSeconds(ScheduleBlock scheduleBlock)
        {
            return (int)scheduleBlock.StartDateTime.TimeOfDay.TotalSeconds;
        }

        [DbFunction("IO.Practiceware.Model", "GetDayOfWeek")]
        public static string GetDayOfWeek(ScheduleBlock scheduleBlock)
        {
            return scheduleBlock.StartDateTime.DayOfWeek.ToString();
        }

        public static void LoadForAppointmentSearch(this IEnumerable<Encounter> source, IPracticeRepository practiceRepository)
        {
            var factory = practiceRepository.AsQueryableFactory();
            var sourceItems = source.ToArray();

            factory.Load(sourceItems,
                         e => e.Appointments.Select(a => a.AppointmentType),
                         e => e.ServiceLocation,
                         e => e.Appointments.OfType<UserAppointment>().Select(a => a.User),
                         e => e.PatientInsuranceReferrals);
        }
    }

    public static class MultiCalendarQueries
    {
        public static void LoadForMultiCalendar(this IEnumerable<UserAppointment> source, IPracticeRepository practiceRepository)
        {
            var factory = practiceRepository.AsQueryableFactory();
            factory.Load(source,
                         a => a.AppointmentType,
                         a => a.Encounter.ServiceLocation,
                         a => a.Encounter.Patient.PatientPhoneNumbers,
                         a => a.Encounter.Patient.PatientInsurances.Select(pi => pi.InsurancePolicy.Insurer).FirstOrDefault(),
                         a => a.User);
        }

        [DbFunction("IO.Practiceware.Model", "GetUserScheduleBlockDate")]
        public static DateTime GetUserScheduleBlockDate(UserScheduleBlock userScheduleBlock)
        {
            return userScheduleBlock.StartDateTime.Date;
        }
    }

    public static class StringQueries
    {
        [DbFunction("IO.Practiceware.Model", "StartsWithIgnoreCase")]
        public static bool StartsWithIgnoreCase(this string input, string startsWithValue)
        {
            return input != null && input.StartsWith(startsWithValue, StringComparison.InvariantCultureIgnoreCase);
        }

        [DbFunction("IO.Practiceware.Model", "ContainsIgnoreCase")]
        public static bool ContainsIgnoreCase(this string input, string containsValue)
        {
            return input != null && input.ToLowerInvariant().Contains(containsValue.ToLowerInvariant());
        }

        [DbFunction("IO.Practiceware.Model", "ConvertIntToString")]
        public static string ConvertToString(this int input)
        {
            return input.ToString();
        }

        [DbFunction("SqlServer", "ConvertDoubleToString")]
        public static string ConvertToString(this double input)
        {
            return input.ToString();
        }

        [DbFunction("IO.Practiceware.Model", "TakeLast")]
        public static string TakeLast(this string input, int takeCount)
        {
            return new string(input.Skip(Math.Max(0, input.Count() - takeCount)).Take(takeCount).ToArray());
        }
    }

    public static class DayOfWeekExtensions
    {
        public static int GetValue(this DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday: return 1;
                case DayOfWeek.Monday: return 2;
                case DayOfWeek.Tuesday: return 3;
                case DayOfWeek.Wednesday: return 4;
                case DayOfWeek.Thursday: return 5;
                case DayOfWeek.Friday: return 6;
                default: return 7;
            }
        }
    }

    public static class EncounterQueries
    {
        public static readonly Expression<Func<Encounter, bool>> IsCancelled = e => e.EncounterStatusId >= (int)EncounterStatus.CancelOffice && e.EncounterStatusId <= (int)EncounterStatus.CancelOther;
    }

    public static class Queries
    {
        /// <summary>
        /// Creates a predicate that matches any of the specified dates.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="dateTimeSelector">The date time selector.</param>
        /// <param name="dates">The dates.</param>
        /// <returns></returns>
        public static Expression<Func<TSource, bool>> IsOnDates<TSource>(Expression<Func<TSource, DateTime>> dateTimeSelector, IEnumerable<DateTime> dates)
        {
            var selectorBody = dateTimeSelector.Body;
            if (selectorBody.NodeType == ExpressionType.Quote)
            {
                selectorBody = ((UnaryExpression)selectorBody).Operand;
            }
            var selectorParameter = dateTimeSelector.Parameters.First();

            var predicates = new List<Expression<Func<TSource, bool>>>();
            foreach (var d in dates)
            {
                var date = d;

                var parameter = Expression.Parameter(typeof(TSource), "i");

                var currentSelectorBody = selectorBody.Replace(selectorParameter, parameter);

                var lambda = Expression.Lambda<Func<TSource, bool>>(
                    Expression.Call(typeof(Expressions), "Invoke",
                        new[] { typeof(DateTime), typeof(DateTime), typeof(bool) },
                        Expression.Constant(Queryables.IsOnDate),
                        currentSelectorBody, Expression.Constant(date)), parameter
                    );

                predicates.Add(lambda);
            }

            return predicates.Aggregate((x, y) => x.Or(y));
        }
    }

    public interface IHasOrdinalId
    {
        [Display(Name = "Ordinal")]
        int OrdinalId { get; set; }
    }

    public static class OrdinalIdExtensions
    {
        /// <summary>
        /// Ensures that Ordinal Ids represent a sequence without gaps (like 1, 2, 3... N).
        /// </summary>
        /// <typeparam name="THasOrdinalIdClass">The implemented type of IHasOrdinalId.</typeparam>
        /// <param name="hasOrdinalIdObjects">The objects containing Ordinal Ids.</param>
        /// <exception cref="System.ArgumentNullException">hasOrdinalIdObjects</exception>
        public static void EnsureOrdinalIdSequential<THasOrdinalIdClass>(this ICollection<THasOrdinalIdClass> hasOrdinalIdObjects)
            where THasOrdinalIdClass : class, IHasOrdinalId
        {
            hasOrdinalIdObjects.EnsureNotDefault("hasOrdinalIdObjects cannot be null");

            using (new SuppressOrdinalIdFixupScope())
            {
                var i = 1;
                foreach (var ordinalIdObject in hasOrdinalIdObjects)
                {
                    ordinalIdObject.OrdinalId = i++;
                }
            }
        }

        /// <summary>
        /// Creates handler which can automatically sort phone number collection by OrdinalId and
        /// ensures there will be no duplicates when OrdinalId is explicitly set.
        /// </summary>
        /// <remarks>
        /// IMPORTANT: however, it will not automatically align OrdinalIds to represent sequence without gaps. This is to preserve values stored in DB.
        /// Use <see cref="EnsureOrdinalIdSequential{THasOrdinalIdClass}"/> method to do that before saving.
        /// </remarks>
        /// <typeparam name="THasOrdinalIdClass">The implemented type of IHasOrdinalId.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="getCollection">The collection getter.</param>
        /// <param name="otherPropertiesToAutoSortOn">The other properties to auto sort on when they change. 
        /// Usually it will be a parent property through which collection is retrieved</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        public static PropertyChangedEventHandler CreateOrdinalIdFixHandler<THasOrdinalIdClass>(
            THasOrdinalIdClass instance,
            Func<ICollection<THasOrdinalIdClass>> getCollection,
            params string[] otherPropertiesToAutoSortOn)
            where THasOrdinalIdClass : class, IHasOrdinalId
        {
            if (instance == null) throw new ArgumentNullException("instance");

            return (sender, e) =>
            {
                // Quit if we are already in process
                if (SuppressOrdinalIdFixupScope.Current != null) return;

                // Get the collection and check that it is present
                var genericCollection = getCollection();
                if (genericCollection == null)
                {
                    return;
                }

                // Ensure collection is of FixupCollection type, since we need it's Sort method
                var collection = genericCollection as FixupCollection<THasOrdinalIdClass>;
                if (collection == null)
                {
                    throw new ArgumentException("Phone number collection must be of FixupCollection<THasOrdinalIdClass> type to allow OrdinalId fixing");
                }

                // Perform the actual ordinal Id fixing
                using (new SuppressOrdinalIdFixupScope())
                {
                    var hasOrdinalIdBeenUpdated = e.PropertyName == "OrdinalId";

                    // Let's ensure OrdinalId is set to a valid number
                    if (instance.OrdinalId <= 0)
                    {
                        instance.OrdinalId = collection.IndexOf(instance) + 1;
                        // Mark that we've just updated the OrdinalId
                        hasOrdinalIdBeenUpdated = true;
                    }

                    // Not updated -> no conflicts to resolve
                    if (!hasOrdinalIdBeenUpdated)
                    {
                        // Sort if property, which can affect sorting, is changed
                        if (otherPropertiesToAutoSortOn.Contains(e.PropertyName))
                        {
                            collection.Sort((x, y) => x.OrdinalId - y.OrdinalId);
                        }

                        if (!otherPropertiesToAutoSortOn.Contains(e.PropertyName)) return;
                    }

                    // Ensure no other item has the same OrdinalId
                    var conflictCandidates = new Queue<THasOrdinalIdClass>();
                    conflictCandidates.Enqueue(instance);

                    while (conflictCandidates.Count > 0)
                    {
                        var conflictCandidate = conflictCandidates.Dequeue();

                        // Find phone number which has the same OrdinalId
                        var conflictingItems = collection
                            .Where(p => p.OrdinalId == conflictCandidate.OrdinalId && p != conflictCandidate)
                            .ToArray();

                        // Increment ordinal Id for each conflict
                        foreach (var conflictingItem in conflictingItems)
                        {
                            // Fix it's ordinal Id and enqueue it (new ordinal id might conflict with some other item)
                            conflictingItem.OrdinalId++;
                            if (!conflictCandidates.Contains(conflictingItem))
                            {
                                conflictCandidates.Enqueue(conflictingItem);
                            }
                        }
                    }

                    // Now sort the collection according to currently set ordinal Ids
                    collection.Sort((x, y) => x.OrdinalId - y.OrdinalId);
                }
            };
        }

        /// <summary>
        /// Denotes to suppress recursive OrdinalId fix attempts
        /// </summary>
        private class SuppressOrdinalIdFixupScope : IDisposable
        {
            [ThreadStatic]
            private static SuppressOrdinalIdFixupScope _current;
            private readonly SuppressOrdinalIdFixupScope _last;

            public static SuppressOrdinalIdFixupScope Current
            {
                get { return _current; }
            }

            public SuppressOrdinalIdFixupScope()
            {
                _last = Current;
                _current = this;
            }

            public void Dispose()
            {
                _current = _last;
            }
        }
    }
}