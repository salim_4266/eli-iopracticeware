﻿
namespace IO.Practiceware.Model
{
    public enum ExternalSystemMessageProcessingStateId
    {
        Unprocessed = 1,
        Processing = 2,
        Processed = 3
    }
}
