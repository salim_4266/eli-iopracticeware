﻿using System;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A class which provides user-defined datetime conversions.  Typically the DateTimeConversionBootstrapper.BootstrapToClientTimeFunction
    /// is called during application startup to set the custom conversion function.  One example use case is converting datetimes based on a
    /// TimeZone.  In the IO.Practiceware.Core project, we provide a utility to convert DateTime.Now.ToClientTime() (which may be executed on our cloud server)
    /// to the currently logged in client's timezone.  Since this is a stand-alone library, we needed a way to provide this conversion for DateTime.Now.ToClientTime()
    /// calls within this assembly.  It must be specified by user code because this assembly does not have initial access to the client's 
    /// time zone.
    /// </summary>
    internal static class DateTimeConverter
    {              
        /// <summary>
        /// Converts the datetime using the specified custom conversion.  See DateTimeConversionBootstrapper.SetMethodForCustomDateTimeConversion to 
        /// set the conversion.  If conversion function is not set, returns the original datetime.
        /// The purpose is to provide the Model library with a method that converts the supplied datetime to the datetime according to 
        /// the currently logged in users time zone.  Model project does not have access to that information so the conversion must be supplied.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime ToClientTime(this DateTime dt)
        {
            if (DateTimeConversionBootstrapper.UserDefinedToClientTimeConversionMethod != null)
            {
                return DateTimeConversionBootstrapper.UserDefinedToClientTimeConversionMethod(dt);
            }

            return dt;
        }
    }

    public static class DateTimeConversionBootstrapper
    {
        /// <summary>
        /// A delegate which takes a datetime, performs some conversion/formatting, and returns the new datetime.
        /// </summary>
        internal static Func<DateTime, DateTime> UserDefinedToClientTimeConversionMethod { get; set; }

        /// <summary>
        /// Sets/Stores the function that performs custom conversion when DateTimeConverter.ToClientTime method is called.
        /// </summary>
        /// <param name="dateTimeConversion"></param>
        public static void BootstrapToClientTimeFunction(Func<DateTime, DateTime> dateTimeConversion)
        {
            UserDefinedToClientTimeConversionMethod = dateTimeConversion;
        }
    }
}
