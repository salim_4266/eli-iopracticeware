﻿using Soaf;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class TemplateDocument
    {
        public TemplateDocumentCategory TemplateDocumentCategory
        {
            get { return (TemplateDocumentCategory)TemplateDocumentCategoryId; }
            set { TemplateDocumentCategoryId = (int)value; }
        }

        public ContentType ContentType
        {
            get { return (ContentType)ContentTypeId; }
            set { ContentTypeId = (int)value; }
        }

        public byte[] ContentAsByteArray
        {
            get
            {
                if (!ContentIsByteArray)
                {
                    return null;
                }

                return Content;
            }
        }

        public bool ContentIsByteArray
        {
            get { return GetContentDataType(ContentType) == typeof(byte[]); }
        }

        public bool ContentIsAscii
        {
            get { return GetContentDataType(ContentType) == typeof(string); }
        }

        /// <summary>
        /// Sets the template content based on the type of the content.
        /// </summary>
        /// <param name="content"></param>
        public void SetContent(object content)
        {
            if (content == null)
            {
                Content = null;
                return;
            }

            if (content is byte[])
            {
                var bytes = content.CastTo<byte[]>();
                if (ContentOriginal == null || !bytes.SequenceEqual(ContentOriginal))
                {
                    Content = content.CastTo<byte[]>();
                }
                else
                {
                    Content = null;   // since both are now the same, we null out Content and rely on ContentOriginal
                }
            }

            if (content is string)
            {
                var c = (string)content;

                string o = null;
                if (ContentOriginal != null)
                {
                    o = System.Text.Encoding.UTF8.GetString(ContentOriginal);
                }

                if (o == null || !c.Equals(o, StringComparison.Ordinal))
                {
                    Content = System.Text.Encoding.UTF8.GetBytes((string)content);
                }
                else
                {
                    Content = null;   // since both are now the same, we null out Content and rely on ContentOriginal
                }
            }
        }

        public object GetContent()
        {
            if (ContentIsByteArray)
            {
                return Content ?? ContentOriginal;
            }

            if (ContentIsAscii)
            {
                if (Content != null)
                {
                    return System.Text.Encoding.UTF8.GetString(Content);
                }

                if (ContentOriginal != null)
                {
                    return System.Text.Encoding.UTF8.GetString(ContentOriginal);
                }
            }

            return Content ?? ContentOriginal;
        }

        public Type GetContentDataType(ContentType contentType)
        {
            switch (contentType)
            {
                case ContentType.RazorTemplate:
                case ContentType.TextFile:
                    return typeof(string);
                case ContentType.WordDocument:
                    return typeof(byte[]);
                default:
                    return null;
            }
        }

        public void ResetContentToOriginal()
        {
            Content = ContentOriginal;
        }
    }

    public enum ContentType
    {
        Unknown = 1,
        RazorTemplate = 2,
        WordDocument = 3,
        TextFile = 4,
        Jpg = 6,
        Text = 7
    }

    /// <summary>
    /// Denotes the content type of a property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ContentTypeAttribute : Attribute
    {
        public ContentTypeAttribute(ContentType contentType)
        {
            ContentType = contentType;
        }

        public ContentType ContentType { get; private set; }
    }

    public enum TemplateDocumentCategory
    {
        [Display(Name = "Batch Builder Documents")]
        BatchBuilderDocuments = 1,

        [Display(Name = "Consultation Letters")]
        Consultationletters = 2,

        [Display(Name = "Referral Letters")]
        Referralletters = 3,

        [Display(Name = "Collection Letters")]
        Collectionletters = 4,

        [Display(Name = "Prescriptions")]
        Prescriptions = 5,

        [Display(Name = "Financial Documents")]
        FinancialDocuments = 6,

        [Display(Name = "Surgery/Procedure Forms")]
        SurgeryProcedureForms = 7,

        [Display(Name = null /* hidden (no name) */)]
        Reports = 8,

        [Display(Name = "Front Desk Documents")]
        FrontDeskDocuments = 9,

        [Display(Name = "Clinical Documents")]
        ClinicalDocuments = 10,

        [Display(Name = "Images")]
        Images = 11,

        [Display(Name = "Written Instructions")]
        WrittenInstructions = 12,

        [Display(Name = "Exam Documents")]
        ExamDocuments = 13,

        [Display(Name = "Surgery Documents")]
        SurgeryDocuments = 14,

        [Display(Name = "Unknown")]
        Unknown = 15,
    }
}