//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Doctor's order for the patient to undergo diagnostic laboratory tests such as blood work or x rays
    /// </summary>
    public partial class EncounterLaboratoryTestOrder : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual int EncounterId
        {
            get { return _encounterId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_encounterId != value)
                    {
                        if (Encounter != null && Encounter.Id != value)
                        {
                            Encounter = null;
                        }
                        _encounterId = value;
        
        				OnPropertyChanged("EncounterId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _encounterId;
    
    	/// <summary>
    	/// Comment containing specific instructions for test - User can pick from associated TreatmentPlanOrderInstructions, edit these instructions or type totally new text
    	/// </summary>
    	public virtual string Comment
        {
            get { return _comment; }
            set 
    		{ 
    			var isChanged = _comment != value; 
    			_comment = value; 
    			if (isChanged) OnPropertyChanged("Comment");
    		}
        }
        private string _comment;
    
    	public virtual int LaboratoryTestOrderId
        {
            get { return _laboratoryTestOrderId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_laboratoryTestOrderId != value)
                    {
                        if (LaboratoryTestOrder != null && LaboratoryTestOrder.Id != value)
                        {
                            LaboratoryTestOrder = null;
                        }
                        _laboratoryTestOrderId = value;
        
        				OnPropertyChanged("LaboratoryTestOrderId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _laboratoryTestOrderId;
    
    	/// <summary>
    	/// The lab where the test should be processed
    	/// </summary>
    	public virtual Nullable<int> ExternalOrganizationId
        {
            get { return _externalOrganizationId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_externalOrganizationId != value)
                    {
                        if (ExternalOrganization != null && ExternalOrganization.Id != value)
                        {
                            ExternalOrganization = null;
                        }
                        _externalOrganizationId = value;
        
        				OnPropertyChanged("ExternalOrganizationId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _externalOrganizationId;
    
    	/// <summary>
    	/// e.g.:  xray of "4th finger"
    	/// </summary>
    	public virtual Nullable<int> BodyPartId
        {
            get { return _bodyPartId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_bodyPartId != value)
                    {
                        if (BodyPart != null && BodyPart.Id != value)
                        {
                            BodyPart = null;
                        }
                        _bodyPartId = value;
        
        				OnPropertyChanged("BodyPartId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _bodyPartId;
    
    	internal virtual Nullable<int> LateralityId
        {
            get { return _lateralityId; }
            set 
    		{ 
    			var isChanged = _lateralityId != value; 
    			_lateralityId = value; 
    			if (isChanged) OnPropertyChanged("LateralityId");
    		}
        }
        private Nullable<int> _lateralityId;
    
    	/// <summary>
    	/// Priority of processing:  routine, emergency, etc
    	/// </summary>
    	public virtual Nullable<int> LaboratoryTestingPriorityId
        {
            get { return _laboratoryTestingPriorityId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_laboratoryTestingPriorityId != value)
                    {
                        if (LaboratoryTestingPriority != null && LaboratoryTestingPriority.Id != value)
                        {
                            LaboratoryTestingPriority = null;
                        }
                        _laboratoryTestingPriorityId = value;
        
        				OnPropertyChanged("LaboratoryTestingPriorityId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _laboratoryTestingPriorityId;
    
    	/// <summary>
    	/// One set of lab results can satisfy many lab test orders
    	/// </summary>
    	public virtual Nullable<int> PatientLaboratoryTestResultId
        {
            get { return _patientLaboratoryTestResultId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_patientLaboratoryTestResultId != value)
                    {
                        if (PatientLaboratoryTestResult != null && PatientLaboratoryTestResult.Id != value)
                        {
                            PatientLaboratoryTestResult = null;
                        }
                        _patientLaboratoryTestResultId = value;
        
        				OnPropertyChanged("PatientLaboratoryTestResultId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _patientLaboratoryTestResultId;
    
    	/// <summary>
    	/// Appointment scheduled for performing the lab test
    	/// </summary>
    	public virtual Nullable<int> AppointmentId
        {
            get { return _appointmentId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_appointmentId != value)
                    {
                        if (Appointment != null && Appointment.Id != value)
                        {
                            Appointment = null;
                        }
                        _appointmentId = value;
        
        				OnPropertyChanged("AppointmentId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _appointmentId;

        #endregion

        #region Navigation Properties
    
    	[Association("Encounter", "EncounterId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Encounter Encounter
        {
            get { return _encounter; }
            set
            {
                if (!ReferenceEquals(_encounter, value))
                {
                    var previousValue = _encounter;
                    _encounter = value;
                    FixupEncounter(previousValue);
    				OnPropertyChanged("Encounter");
                }
            }
        }
        private Encounter _encounter;
    
    	[Association("LaboratoryTestOrder", "LaboratoryTestOrderId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual LaboratoryTestOrder LaboratoryTestOrder
        {
            get { return _laboratoryTestOrder; }
            set
            {
                if (!ReferenceEquals(_laboratoryTestOrder, value))
                {
                    var previousValue = _laboratoryTestOrder;
                    _laboratoryTestOrder = value;
                    FixupLaboratoryTestOrder(previousValue);
    				OnPropertyChanged("LaboratoryTestOrder");
                }
            }
        }
        private LaboratoryTestOrder _laboratoryTestOrder;
    
    	[Association("ExternalOrganization", "ExternalOrganizationId", "Id")]
        public virtual ExternalOrganization ExternalOrganization
        {
            get { return _externalOrganization; }
            set
            {
                if (!ReferenceEquals(_externalOrganization, value))
                {
                    var previousValue = _externalOrganization;
                    _externalOrganization = value;
                    FixupExternalOrganization(previousValue);
    				OnPropertyChanged("ExternalOrganization");
                }
            }
        }
        private ExternalOrganization _externalOrganization;
    
    	[Association("ClinicalConditions", "", "")]
        public virtual System.Collections.Generic.ICollection<ClinicalCondition> ClinicalConditions
        {
            get
            {
                if (_clinicalConditions == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ClinicalCondition>();
                    newCollection.ItemSet += FixupClinicalConditionsItemSet;
                    newCollection.ItemRemoved += FixupClinicalConditionsItemRemoved;
                    _clinicalConditions = newCollection;
                }
                return _clinicalConditions;
            }
            set
            {
                if (!ReferenceEquals(_clinicalConditions, value))
                {
                    var previousValue = _clinicalConditions as Soaf.Collections.FixupCollection<ClinicalCondition>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupClinicalConditionsItemSet;
                        previousValue.ItemRemoved -= FixupClinicalConditionsItemRemoved;
                    }
                    _clinicalConditions = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ClinicalCondition>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupClinicalConditionsItemSet;
                        newValue.ItemRemoved += FixupClinicalConditionsItemRemoved;
                    }
    				OnPropertyChanged("ClinicalConditions");
                }
            }
        }
        private System.Collections.Generic.ICollection<ClinicalCondition> _clinicalConditions;
    
    	[Association("BodyPart", "BodyPartId", "Id")]
        public virtual BodyPart BodyPart
        {
            get { return _bodyPart; }
            set
            {
                if (!ReferenceEquals(_bodyPart, value))
                {
                    var previousValue = _bodyPart;
                    _bodyPart = value;
                    FixupBodyPart(previousValue);
    				OnPropertyChanged("BodyPart");
                }
            }
        }
        private BodyPart _bodyPart;
    
    	[Association("LaboratoryTestingPriority", "LaboratoryTestingPriorityId", "Id")]
        public virtual LaboratoryTestingPriority LaboratoryTestingPriority
        {
            get { return _laboratoryTestingPriority; }
            set
            {
                if (!ReferenceEquals(_laboratoryTestingPriority, value))
                {
                    var previousValue = _laboratoryTestingPriority;
                    _laboratoryTestingPriority = value;
                    FixupLaboratoryTestingPriority(previousValue);
    				OnPropertyChanged("LaboratoryTestingPriority");
                }
            }
        }
        private LaboratoryTestingPriority _laboratoryTestingPriority;
    
    	[Association("PatientLaboratoryTestResult", "PatientLaboratoryTestResultId", "Id")]
        public virtual PatientLaboratoryTestResult PatientLaboratoryTestResult
        {
            get { return _patientLaboratoryTestResult; }
            set
            {
                if (!ReferenceEquals(_patientLaboratoryTestResult, value))
                {
                    var previousValue = _patientLaboratoryTestResult;
                    _patientLaboratoryTestResult = value;
                    FixupPatientLaboratoryTestResult(previousValue);
    				OnPropertyChanged("PatientLaboratoryTestResult");
                }
            }
        }
        private PatientLaboratoryTestResult _patientLaboratoryTestResult;
    
    	[Association("Appointment", "AppointmentId", "Id")]
        public virtual Appointment Appointment
        {
            get { return _appointment; }
            set
            {
                if (!ReferenceEquals(_appointment, value))
                {
                    var previousValue = _appointment;
                    _appointment = value;
                    FixupAppointment(previousValue);
    				OnPropertyChanged("Appointment");
                }
            }
        }
        private Appointment _appointment;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupEncounter(Encounter previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (Encounter != null)
            {
                if (!Encounter.EncounterLaboratoryTestOrders.Contains(this))
                {
                    Encounter.EncounterLaboratoryTestOrders.Add(this);
                }
                if (Encounter != null && EncounterId != Encounter.Id)
                {
                    EncounterId = Encounter.Id;
                }
            }
        }
    
        private void FixupLaboratoryTestOrder(LaboratoryTestOrder previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (LaboratoryTestOrder != null)
            {
                if (!LaboratoryTestOrder.EncounterLaboratoryTestOrders.Contains(this))
                {
                    LaboratoryTestOrder.EncounterLaboratoryTestOrders.Add(this);
                }
                if (LaboratoryTestOrder != null && LaboratoryTestOrderId != LaboratoryTestOrder.Id)
                {
                    LaboratoryTestOrderId = LaboratoryTestOrder.Id;
                }
            }
        }
    
        private void FixupExternalOrganization(ExternalOrganization previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (ExternalOrganization != null)
            {
                if (!ExternalOrganization.EncounterLaboratoryTestOrders.Contains(this))
                {
                    ExternalOrganization.EncounterLaboratoryTestOrders.Add(this);
                }
                if (ExternalOrganization != null && ExternalOrganizationId != ExternalOrganization.Id)
                {
                    ExternalOrganizationId = ExternalOrganization.Id;
                }
            }
            else if (!_settingFK)
            {
                ExternalOrganizationId = null;
            }
        }
    
        private void FixupBodyPart(BodyPart previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (BodyPart != null)
            {
                if (!BodyPart.EncounterLaboratoryTestOrders.Contains(this))
                {
                    BodyPart.EncounterLaboratoryTestOrders.Add(this);
                }
                if (BodyPart != null && BodyPartId != BodyPart.Id)
                {
                    BodyPartId = BodyPart.Id;
                }
            }
            else if (!_settingFK)
            {
                BodyPartId = null;
            }
        }
    
        private void FixupLaboratoryTestingPriority(LaboratoryTestingPriority previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (LaboratoryTestingPriority != null)
            {
                if (!LaboratoryTestingPriority.EncounterLaboratoryTestOrders.Contains(this))
                {
                    LaboratoryTestingPriority.EncounterLaboratoryTestOrders.Add(this);
                }
                if (LaboratoryTestingPriority != null && LaboratoryTestingPriorityId != LaboratoryTestingPriority.Id)
                {
                    LaboratoryTestingPriorityId = LaboratoryTestingPriority.Id;
                }
            }
            else if (!_settingFK)
            {
                LaboratoryTestingPriorityId = null;
            }
        }
    
        private void FixupPatientLaboratoryTestResult(PatientLaboratoryTestResult previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (PatientLaboratoryTestResult != null)
            {
                if (!PatientLaboratoryTestResult.EncounterLaboratoryTestOrders.Contains(this))
                {
                    PatientLaboratoryTestResult.EncounterLaboratoryTestOrders.Add(this);
                }
                if (PatientLaboratoryTestResult != null && PatientLaboratoryTestResultId != PatientLaboratoryTestResult.Id)
                {
                    PatientLaboratoryTestResultId = PatientLaboratoryTestResult.Id;
                }
            }
            else if (!_settingFK)
            {
                PatientLaboratoryTestResultId = null;
            }
        }
    
        private void FixupAppointment(Appointment previousValue)
        {
            if (previousValue != null && previousValue.EncounterLaboratoryTestOrders.Contains(this))
            {
                previousValue.EncounterLaboratoryTestOrders.Remove(this);
            }
    
            if (Appointment != null)
            {
                if (!Appointment.EncounterLaboratoryTestOrders.Contains(this))
                {
                    Appointment.EncounterLaboratoryTestOrders.Add(this);
                }
                if (Appointment != null && AppointmentId != Appointment.Id)
                {
                    AppointmentId = Appointment.Id;
                }
            }
            else if (!_settingFK)
            {
                AppointmentId = null;
            }
        }
    
        private void FixupClinicalConditionsItemSet(object sender, Soaf.EventArgs<ClinicalCondition> e)
        {
            var item = e.Value;
     
            if (!item.EncounterLaboratoryTestOrders.Contains(this))
            {
                item.EncounterLaboratoryTestOrders.Add(this);
            }
        }
    
        private void FixupClinicalConditionsItemRemoved(object sender, Soaf.EventArgs<ClinicalCondition> e)
        {
            var item = e.Value;
     
            if (item.EncounterLaboratoryTestOrders.Contains(this))
            {
                item.EncounterLaboratoryTestOrders.Remove(this);
            }
        }
    

        #endregion

    }
}
