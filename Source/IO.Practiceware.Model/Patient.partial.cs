﻿using Soaf;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Metadata for the patient model.
    /// </summary>
    public class PatientMetadata
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "DOB")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? DateOfBirth { get; set; }
    }

    [MetadataType(typeof (PatientMetadata))]
    public partial class Patient
    {
        public Patient()
        {
            PatientStatus = PatientStatus.Active;
        }

        /// <summary>
        ///   Gets or sets the gender.
        /// </summary>
        /// <value> The gender. </value>
        public Gender? Gender
        {
            get { return GenderId.HasValue ? (Gender) GenderId : new Gender?(); }
            set { GenderId = value.HasValue ? (int) value.Value : new int?(); }
        }

        /// <summary>
        ///   Gets or sets the marital status.
        /// </summary>
        /// <value> The marital status. </value>
        public MaritalStatus? MaritalStatus
        {
            get { return MaritalStatusId.HasValue ? (MaritalStatus) MaritalStatusId : new MaritalStatus?(); }
            set { MaritalStatusId = value.HasValue ? (int) value.Value : new int?(); }
        }


        /// <summary>
        ///   Gets or sets the patient status.
        /// </summary>
        /// <value> The patient status. </value>
        public PatientStatus PatientStatus
        {
            get { return (PatientStatus) PatientStatusId; }
            set { PatientStatusId = (int) value; }
        }

        /// <summary>
        ///   Gets the display name.
        /// </summary>
        public string DisplayName
        {
            get { return "{0}{1}, {2} {3}".FormatWith(LastName, Suffix.IsNullOrEmpty() ? string.Empty : " " + Suffix, FirstName, MiddleName); }
        }

        /// <summary>
        /// Gets or sets the most recent discharged encounter.
        /// </summary>
        /// <value>
        /// The last encounter.
        /// </value>
        public Encounter LastEncounter
        {
            get
            {
                   return Encounters.Where(e => e.EncounterStatus == EncounterStatus.Discharged)
                                 .OrderByDescending(e => e.StartDateTime).FirstOrDefault(e => e.StartDateTime < DateTime.Now.ToClientTime());
            }
        }

        /// <summary>
        ///   Gets the LastAppointment.
        ///   The last appointment is defined as the most recent discharged appointment.
        /// </summary>
        public UserAppointment LastAppointment
        {
            get
            {
                return Encounters.SelectMany(e => e.Appointments.OfType<UserAppointment>())
                                 .Where(a => a.Encounter.EncounterStatus == EncounterStatus.Discharged)
                                 .OrderByDescending(a => a.DateTime).FirstOrDefault(ap => ap.DateTime < DateTime.Now.ToClientTime());
            }
        }

        [Display(Name = "Last Appt")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? LastAppointmentDateTime
        {
            get { return LastAppointment.IfNotNull(a => a.DateTime, () => new DateTime?()); }
        }

        /// <summary>
        ///   Gets the NextAppointment.
        ///   The next appointment is defined as the most recent discharged appointment.
        /// </summary>
        public UserAppointment NextAppointment
        {
            get
            {
                return Encounters.SelectMany(e => e.Appointments.OfType<UserAppointment>())
                                 .Where(a => !a.Encounter.EncounterStatus.IsCancelledStatus())
                                 .OrderBy(a => a.DateTime).FirstOrDefault(ap => ap.DateTime >= DateTime.Now.ToClientTime());
            }
        }

        [Display(Name = "Next Appt")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? NextAppointmentDateTime
        {
            get { return NextAppointment.IfNotNull(a => a.DateTime, () => new DateTime?()); }
        }


        /// <summary>
        /// Gets the primary phone number of the patient
        /// </summary>
        [Display(Name = "Primary")]
        public PatientPhoneNumber PrimaryPhoneNumber
        {
            get
            {
                PatientPhoneNumber phoneNumber = PatientPhoneNumbers.OrderBy(x => x.OrdinalId).FirstOrDefault();
                return phoneNumber;
            }
        }

        /// <summary>
        ///   Gets the home phone number.
        /// </summary>
        [Display(Name = "Home")]
        public PatientPhoneNumber HomePhoneNumber
        {
            get
            {
                PatientPhoneNumber phoneNumber = PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home);
                return phoneNumber;
            }
        }

        /// <summary>
        ///   Gets the cell phone number.
        /// </summary>
        [Display(Name = "Cell")]
        public PatientPhoneNumber CellPhoneNumber
        {
            get
            {
                PatientPhoneNumber cellPhonenumber = PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell);
                return cellPhonenumber;
            }
        }

        /// <summary>
        ///   Gets the business phone number.
        /// </summary>
        [Display(Name = "Business")]
        public PatientPhoneNumber BusinessPhoneNumber
        {
            get
            {
                PatientPhoneNumber businessPhoneNumber = PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business);
                return businessPhoneNumber;
            }
        }

        /// <summary>
        /// Gets the name of the ExternalProvider, formatted for readability.
        /// </summary>
        /// <returns>Returns the DisplayName if it exists.
        /// Otherwise returns "{FirstName} {MiddleName} {LastName}", where whitespace is trimmed.</returns>
        public string GetFormattedName()
        {
            // DisplayName may contain additional details such as "M.D." that other properties can't provide.
            if (!string.IsNullOrEmpty(DisplayName))
            {
                return DisplayName;
            }

            string formattedName = string.Empty;
            bool firstEntry = true;
            foreach (string name in new[] {FirstName, MiddleName, LastName}.Where(name => !string.IsNullOrEmpty(name)))
            {
                formattedName += (firstEntry) ? name : " " + name;
                firstEntry = false;
            }
            return formattedName;
        }

        /// <summary>
        /// Calculates the patient's age in years based on their birthdate and today's date.
        /// </summary>
        public int? Age
        {
            get
            {
                if (DateOfBirth == null) return null;

                var now = DateTime.Today;
                var age = now.Year - DateOfBirth.Value.Year;
                if (now < DateOfBirth.Value.AddYears(age)) age--;

                return age;
            }
        }
   
        public PaymentOfBenefitsToProvider PaymentOfBenefitsToProvider
        {
            get { return (PaymentOfBenefitsToProvider) PaymentOfBenefitsToProviderId; }
            set { PaymentOfBenefitsToProviderId = (int)value; }
        }

        public ReleaseOfInformationCode? ReleaseOfInformationCode
        {
            get { return ReleaseOfInformationCodeId.HasValue ? (ReleaseOfInformationCode) ReleaseOfInformationCodeId : new ReleaseOfInformationCode?(); }
            set { ReleaseOfInformationCodeId = value.HasValue ? (int) value.Value : new int?(); }
        }
    }

    /// <summary>
    /// </summary>
    public enum PatientStatus
    {
        /// <summary>
        /// </summary>
        [Display(Name = "Active")]
        Active = 1,

        /// <summary>
        /// </summary>
        [Display(Name = "Closed no activity")]
        ClosedNoActivity = 2,

        /// <summary>
        /// </summary>
        [Display(Name = "Closed deceased")]
        ClosedDeceased = 3,

        /// <summary>
        /// </summary>
        [Display(Name = "Closed moved")]
        ClosedMoved = 4
    }

    public enum PaymentOfBenefitsToProvider
    {
        [Display(Name = "No")]
        N = 0,
        [Display(Name = "Yes")]
        Y = 1,
        [Description("Patient refuses to assign benefits")]
        [Display(Name = "Patient Refuses")]
        W = 2
    }

    public enum ReleaseOfInformationCode
    {
        [Description("Signature not collected and not required.")]
        [Display(Name = "Not Required")]
        I = 0,
        [Description("Statement signed permitting release of medical billing data.")]
        [Display(Name ="Yes")]
        Y = 1,
        [Display(Name = "No")]
        N = 2
    }
}