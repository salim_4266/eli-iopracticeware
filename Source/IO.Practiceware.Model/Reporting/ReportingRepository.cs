﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;
using IO.Practiceware.Model.Reporting;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Soaf.Linq.Expressions;

[assembly: Component(typeof(ReportingRepository), typeof(IReportingRepository))]

namespace IO.Practiceware.Model.Reporting
{
    public interface IReportingRepository
    {
        [Display(Name = "Adjustments (grid)", GroupName = "PaymentsAndAdjustments")]
        IQueryable<AdjustmentInformation> Adjustments { get; }

        [Display(Name = "Payments (grid)", GroupName = "PaymentsAndAdjustments")]
        IQueryable<PaymentInformation> Payments { get; }

        [Display(Name = "Informational Postings (grid)", GroupName = "Billing")]
        IQueryable<FinancialInformation> Financials { get; }

        [Display(Name = "Appointments (grid)", GroupName = "Scheduling")]
        IQueryable<ScheduleInformation> Schedule { get; }

        [Display(Name = "Last Visit (grid)", GroupName = "Scheduling")]
        IQueryable<LastEncounterInformation> LastEncounter { get; }

        [Display(Name = "Charges (grid)", GroupName = "Charges")]
        IQueryable<ChargeInformation> Charges { get; }
    }

    public class ReportingRepository : IReportingRepository
    {
        private readonly IPracticeRepository _practiceRepository;

        public ReportingRepository(IPracticeRepository practiceRepository)
        {
            // required for UI support
            BackgroundThreadOnlyAttribute.IsEnabled = false;
            _practiceRepository = practiceRepository;
        }

        public IQueryable<AdjustmentInformation> Adjustments
        {
            get
            {
                var adjustments = from a in _practiceRepository.Adjustments
                                  join at in _practiceRepository.AdjustmentTypes on a.AdjustmentTypeId equals at.Id
                                  join ir in _practiceRepository.InvoiceReceivables on a.InvoiceReceivableId equals ir.Id
                                  join i in _practiceRepository.Invoices on ir.InvoiceId equals i.Id
                                  join e in _practiceRepository.Encounters on i.EncounterId equals e.Id
                                  join app in _practiceRepository.Appointments.OfType<UserAppointment>() on e.Id equals app.EncounterId
                                  join p in _practiceRepository.Patients on e.PatientId equals p.Id
                                  join bp in _practiceRepository.Providers on i.BillingProviderId equals bp.Id
                                  join bo in _practiceRepository.BillingOrganizations on bp.BillingOrganizationId equals bo.Id
                                  join u in _practiceRepository.Users on bp.UserId equals u.Id
                                  join su in _practiceRepository.Users on app.UserId equals su.Id
                                  join sl in _practiceRepository.ServiceLocations on e.ServiceLocationId equals sl.Id
                                  join bs in _practiceRepository.BillingServices on i.Id equals bs.InvoiceId
                                  join ens in _practiceRepository.EncounterServices on bs.EncounterServiceId equals ens.Id
                                  // explicit inner joins are necessary to prevent EF from emitting all left joins
                                  select new AdjustmentInformation
                                  {
                                      BillingProvider = u.UserName,
                                      ScheduledProvider = su.UserName,
                                      ServiceLocation = sl.ShortName,
                                      BillingOrganization = bo.ShortName,
                                      AdjustmentTypeName = at.Name,
                                      AdjustmentTypeShortName = at.ShortName,
                                      DateTimePosted = a.PostedDateTime,
                                      MonthPosted = ("0" + CommonQueries.ConvertInt32ToString(SqlFunctions.DatePart("month", a.PostedDateTime).Value)).TakeLast(2) + ". " + SqlFunctions.DateName("month", a.PostedDateTime),
                                      YearPosted = SqlFunctions.DateName("year", a.PostedDateTime),
                                      PatientLastName = p.LastName,
                                      PatientFirstName = p.FirstName,
                                      PatientId = p.Id,
                                      Amount = (at.IsDebit) ? a.Amount * -1 : a.Amount,
                                      InvoiceDateTime = i.Encounter.StartDateTime,
                                      ServiceCode = ens.Code,
                                      Unit = bs.Unit
                                  };

                return adjustments.OrderBy(a => a.InvoiceDateTime);

            }
        }

        public IQueryable<PaymentInformation> Payments
        {
            get
            {
                var payments = from a in _practiceRepository.Adjustments
                               join at in _practiceRepository.AdjustmentTypes on a.AdjustmentTypeId equals at.Id
                               join ir in _practiceRepository.InvoiceReceivables on a.InvoiceReceivableId equals ir.Id
                               join i in _practiceRepository.Invoices on ir.InvoiceId equals i.Id
                               join e in _practiceRepository.Encounters on i.EncounterId equals e.Id
                               join fb in _practiceRepository.FinancialBatches on a.FinancialBatchId equals fb.Id

                               join bp in _practiceRepository.Providers on i.BillingProviderId equals bp.Id
                               join bo in _practiceRepository.BillingOrganizations on bp.BillingOrganizationId equals bo.Id
                               join u in _practiceRepository.Users on bp.UserId equals u.Id
                               join sl in _practiceRepository.ServiceLocations on e.ServiceLocationId equals sl.Id
                               join p in _practiceRepository.Patients on e.PatientId equals p.Id

                               join bs in _practiceRepository.BillingServices on i.Id equals bs.InvoiceId
                               join ens in _practiceRepository.EncounterServices on bs.EncounterServiceId equals ens.Id
                               join insur in _practiceRepository.Insurers on fb.InsurerId equals insur.Id into lins
                               from insur in lins.DefaultIfEmpty()

                               where at.IsCash && a.BillingServiceId == bs.Id
                               // explicit inner joins are necessary to prevent EF from emitting all left joins
                               select new PaymentInformation
                               {
                                   BillingProvider = u.UserName,
                                   ServiceLocation = sl.ShortName,
                                   BillingOrganization = bo.Name,
                                   DateTimePosted = DbFunctions.TruncateTime(a.PostedDateTime).Value,
                                   MonthPosted = ("0" + CommonQueries.ConvertInt32ToString(SqlFunctions.DatePart("month", a.PostedDateTime).Value)).TakeLast(2) + ". " + SqlFunctions.DateName("month", a.PostedDateTime),
                                   YearPosted = SqlFunctions.DateName("year", a.PostedDateTime),
                                   PatientLastName = p.LastName,
                                   PatientFirstName = p.FirstName,
                                   PatientId = p.Id,
                                   Amount = (at.IsDebit) ? a.Amount * -1 : a.Amount,
                                   InvoiceDateTime = DbFunctions.TruncateTime(i.Encounter.StartDateTime).Value,
                                   Payer = ((fb.FinancialSourceTypeId == 1) ? insur.Name :
                                             (fb.FinancialSourceTypeId == 2) ? p.LastName :
                                          ((fb.PatientId == null) &&
                                              (fb.InsurerId == null) &&
                                                 (fb.FinancialSourceTypeId == 3)) ? "Office" :
                                             ((fb.PatientId == null) &&
                                                 (fb.InsurerId == null) &&
                                                 (fb.FinancialSourceTypeId == 4)) ? "External Organization" : ""),
                                   ServiceCode = ens.Code,
                                   Unit = bs.Unit
                               };

                return payments.OrderBy(a => a.InvoiceDateTime);
            }
        }

        public IQueryable<FinancialInformation> Financials
        {
            get
            {
                var financials = from fi in _practiceRepository.FinancialInformations
                                 join ft in _practiceRepository.FinancialInformationTypes on fi.FinancialInformationTypeId equals ft.Id
                                 join fg in _practiceRepository.FinancialTypeGroups on fi.FinancialSourceTypeId equals fg.Id
                                 join ir in _practiceRepository.InvoiceReceivables on fi.InvoiceReceivableId equals ir.Id
                                 join i in _practiceRepository.Invoices on ir.InvoiceId equals i.Id
                                 join e in _practiceRepository.Encounters on i.EncounterId equals e.Id
                                 join p in _practiceRepository.Patients on e.PatientId equals p.Id
                                 join bp in _practiceRepository.Providers on i.BillingProviderId equals bp.Id
                                 join bo in _practiceRepository.BillingOrganizations on bp.BillingOrganizationId equals bo.Id
                                 join u in _practiceRepository.Users on bp.UserId equals u.Id
                                 join sl in _practiceRepository.ServiceLocations on e.ServiceLocationId equals sl.Id
                                 select new FinancialInformation
                                            {
                                                BillingProvider = u.UserName,
                                                ServiceLocation = sl.ShortName,
                                                BillingOrganization = bo.Name,
                                                FinancialTypeGroup = fg.Name.Substring(0, fg.Name.IndexOf(" ")),
                                                FinancialInformationTypeName = ft.Name,
                                                FinancialInformationTypeShortName = ft.ShortName,
                                                DateTimePosted = fi.PostedDateTime,
                                                MonthPosted = ("0" + CommonQueries.ConvertInt32ToString(SqlFunctions.DatePart("month", fi.PostedDateTime).Value)).TakeLast(2) + ". " + SqlFunctions.DateName("month", fi.PostedDateTime),
                                                YearPosted = SqlFunctions.DateName("year", fi.PostedDateTime),
                                                PatientLastName = p.LastName,
                                                PatientFirstName = p.FirstName,
                                                PatientId = p.Id,
                                                Amount = fi.Amount,
                                                InvoiceDateTime = DbFunctions.TruncateTime(i.Encounter.StartDateTime).Value
                                            };

                return financials.OrderBy(a => a.InvoiceDateTime);
            }
        }

        public IQueryable<ScheduleInformation> Schedule
        {
            get
            {
                var schedule = from app in _practiceRepository.Appointments.OfType<UserAppointment>()
                               join at in _practiceRepository.AppointmentTypes on app.AppointmentTypeId equals at.Id
                               where at.Id > 0
                               join e in _practiceRepository.Encounters on app.EncounterId equals e.Id
                               join sl in _practiceRepository.ServiceLocations on e.ServiceLocationId equals sl.Id
                               join u in _practiceRepository.Users on app.UserId equals u.Id
                               join p in _practiceRepository.Patients on e.PatientId equals p.Id
                               join pn in _practiceRepository.PatientPhoneNumbers on p.Id equals pn.PatientId into lpn
                               from pn in lpn.Where(pn => pn.OrdinalId == 1).DefaultIfEmpty()
                               join pt in _practiceRepository.PatientTags on p.Id equals pt.PatientId into lpt
                               from pt in lpt.Where(pt => pt.OrdinalId == 1).DefaultIfEmpty()
                               join t in _practiceRepository.Tags on pt.TagId equals t.Id into lt
                               from t in lt.DefaultIfEmpty()
                               join pi in _practiceRepository.PatientInsurances on p.Id equals pi.InsuredPatientId into lpi
                               from fpi in lpi.Where(pi => InsuranceQueries.IsForAppointment.Invoke(pi, app)).OrderBy(x => x.OrdinalId).Take(1).DefaultIfEmpty()
                               join pi2 in _practiceRepository.PatientInsurances on p.Id equals pi2.InsuredPatientId into lpi2
                               from fpi2 in lpi2.Where(pi2 => InsuranceQueries.IsForAppointment.Invoke(pi2, app)).OrderBy(x => x.OrdinalId).Skip(1).Take(1).DefaultIfEmpty()

                               select new ScheduleInformation
                                      {
                                          AppDate = DbFunctions.TruncateTime(app.DateTime).Value,
                                          AppTime = CommonQueries.GetTimePartWithoutSeconds(app.DateTime),
                                          AppType = at.Name,
                                          AppComment = app.Comment,
                                          AppLocation = sl.ShortName,
                                          AppStatus = (EncounterStatus)e.EncounterStatusId,
                                          ConfirmStatus = e.ConfirmationStatus.Code ?? "No Confirmation Status",
                                          SchedDoctor = u.DisplayName,
                                          RefDoctor = e.Invoices.FirstOrDefault().ReferringExternalProvider.DisplayName ?? "No Referring Doctor",
                                          PatientId = p.Id,
                                          LastName = p.LastName,
                                          FirstName = p.FirstName,
                                          Dob = p.DateOfBirth,
                                          Age = CommonQueries.ConvertInt32ToString(PatientQueries.GetAge(p)),
                                          Tag = t.Name ?? "No Tag",
                                          Gender = (Gender)p.GenderId,
                                          PatientPhoneNumber = pn.AreaCode + "-" + pn.ExchangeAndSuffix,
                                          LastDischargedVisit = p.Encounters.Where(a => a.EncounterStatus == EncounterStatus.Discharged)
                                            .Where(a => a.StartDateTime < DateTime.Now.ToClientTime()).OrderByDescending(a => a.StartDateTime).FirstOrDefault().StartDateTime,
                                          PrimaryInsurer = fpi.InsurancePolicy.Insurer.Name ?? "No Primary Insurance",
                                          PrimaryPolicy = fpi.InsurancePolicy.PolicyCode,
                                          Copay = fpi.InsurancePolicy.Copay,
                                          SecondaryInsurer = fpi2.InsurancePolicy.Insurer.Name ?? "No Secondary Insurance",
                                          SecondaryPolicy = fpi2.InsurancePolicy.PolicyCode

                                      };
                return schedule.Where(x => x.AppTime.Hour > 0).OrderBy(x => x.AppDate).ThenBy(x => x.AppTime);
            }
        }

        public IQueryable<LastEncounterInformation> LastEncounter
        {
            get
            {
                var query = (IQueryable<LastEncounterInformation>)ContextData.Get(typeof(IQueryable<LastEncounterInformation>).FullName);
                if (query != null) return query;

                var lastEncounter = from p in _practiceRepository.Patients
                                    join pa in _practiceRepository.PatientAddresses on p.Id equals pa.PatientId
                                    where pa.OrdinalId == 1
                                    join sop in _practiceRepository.StateOrProvinces on pa.StateOrProvinceId equals sop.Id
                                    join en in _practiceRepository.Encounters on p.Id equals en.PatientId
                                    join pn in _practiceRepository.PatientPhoneNumbers on p.Id equals pn.PatientId into lpn
                                    from pn in lpn.DefaultIfEmpty()
                                    where pn.OrdinalId == 1
                                    join pe in _practiceRepository.PatientEmailAddresses on p.Id equals pe.PatientId into lpe
                                    from pe in lpe.DefaultIfEmpty()
                                    where pe.OrdinalId == 1
                                    join pr in _practiceRepository.PatientRecalls on p.Id equals pr.PatientId into lpr
                                    from pr in lpr.DefaultIfEmpty()
                                    join inv in _practiceRepository.Invoices on en.Id equals inv.EncounterId into li
                                    from inv in li.DefaultIfEmpty()
                                    join bd in _practiceRepository.BillingDiagnosis on inv.Id equals bd.InvoiceId into lbd
                                    from bd in lbd.DefaultIfEmpty()
                                    join bdIcd10 in _practiceRepository.BillingDiagnosisIcd10 on inv.Id equals bdIcd10.InvoiceId into ineIcd10
                                    from bdIcd10 in ineIcd10.DefaultIfEmpty()
                                    select new LastEncounterInformation
                                    {
                                        PatientId = p.Id,
                                        FirstName = p.FirstName,
                                        LastName = p.LastName,
                                        PriorPatientId = p.PriorPatientCode,
                                        PatientAddress = pa.Line1 + " " + pa.Line2,
                                        PatientCity = pa.City,
                                        PatientState = sop.Abbreviation,
                                        PatientZip = pa.PostalCode,
                                        PatientPhoneNumber = pn.AreaCode + "-" + pn.ExchangeAndSuffix,
                                        PatientEmailAddress = pe.Value,
                                        LastDischargedVisit = p.Encounters.Where(a => a.EncounterStatus == EncounterStatus.Discharged)
                                            .Where(a => a.StartDateTime < DateTime.Now.ToClientTime()).OrderByDescending(a => a.StartDateTime).FirstOrDefault().StartDateTime,
                                        NextScheduledVisit = p.Encounters.Where(a => !EncounterQueries.IsCancelled.Invoke(a))
                                            .Where(a => a.EndDateTime >= DateTime.Now.ToClientTime()).OrderBy(a => a.EndDateTime).FirstOrDefault().EndDateTime,
                                        NextPendingRecall = p.PatientRecalls.Where(a => a.RecallStatus != RecallStatus.Closed)
                                            .Where(a => a.DueDateTime >= DateTime.Now.ToClientTime()).OrderBy(a => a.DueDateTime).FirstOrDefault().DueDateTime,
                                        DiagnosisCode = inv.DiagnosisTypeId == 1 ? bd.ExternalSystemEntityMapping.ExternalSystemEntityKey ?? "No Diagnosis" : bdIcd10.DiagnosisCode ?? "No Diagnosis",
                                    };
                query = lastEncounter.Distinct().OrderBy(a => a.PatientId).AsEnumerable().Cached().ToList().AsQueryable();

                ContextData.Set(typeof(IQueryable<LastEncounterInformation>).FullName, query);

                return query;

            }
        }


        public IQueryable<ChargeInformation> Charges
        {
            get
            {
                var charges = from bs in _practiceRepository.BillingServices
                              join inv in _practiceRepository.Invoices on bs.InvoiceId equals inv.Id
                              join ens in _practiceRepository.EncounterServices on bs.EncounterServiceId equals ens.Id

                              join bsm in _practiceRepository.BillingServiceModifiers on bs.Id equals bsm.BillingServiceId into lbsm
                              from bsm in lbsm.Where(a => a.OrdinalId == 1).DefaultIfEmpty()
                              join bsm2 in _practiceRepository.BillingServiceModifiers on bs.Id equals bsm2.BillingServiceId into lbsm2
                              from bsm2 in lbsm2.Where(a => a.OrdinalId == 2).DefaultIfEmpty()
                              join bsm3 in _practiceRepository.BillingServiceModifiers on bs.Id equals bsm3.BillingServiceId into lbsm3
                              from bsm3 in lbsm3.Where(a => a.OrdinalId == 3).DefaultIfEmpty()
                              join bsm4 in _practiceRepository.BillingServiceModifiers on bs.Id equals bsm4.BillingServiceId into lbsm4
                              from bsm4 in lbsm4.Where(a => a.OrdinalId == 4).DefaultIfEmpty()
                              join smc in _practiceRepository.ServiceModifiers on bsm.ServiceModifierId equals smc.Id into ssm
                              from smc in ssm.DefaultIfEmpty()
                              join smc2 in _practiceRepository.ServiceModifiers on bsm2.ServiceModifierId equals smc2.Id into ssm2
                              from smc2 in ssm2.DefaultIfEmpty()
                              join smc3 in _practiceRepository.ServiceModifiers on bsm3.ServiceModifierId equals smc3.Id into ssm3
                              from smc3 in ssm3.DefaultIfEmpty()
                              join smc4 in _practiceRepository.ServiceModifiers on bsm4.ServiceModifierId equals smc4.Id into ssm4
                              from smc4 in ssm4.DefaultIfEmpty()

                              join est in _practiceRepository.EncounterServiceTypes on ens.EncounterServiceTypeId equals est.Id into st
                              from est in st.DefaultIfEmpty()

                              join e in _practiceRepository.Encounters on inv.EncounterId equals e.Id
                              join prov in _practiceRepository.Providers on inv.BillingProviderId equals prov.Id
                              join bp in _practiceRepository.Users on prov.UserId equals bp.Id // bp meaning Billihg provider
                              join p in _practiceRepository.Patients on e.PatientId equals p.Id
                              join sl in _practiceRepository.ServiceLocations on e.ServiceLocationId equals sl.Id
                              join asl in _practiceRepository.ServiceLocations on inv.AttributeToServiceLocationId equals asl.Id
                              join bo in _practiceRepository.BillingOrganizations on prov.BillingOrganizationId equals bo.Id

                              join app in _practiceRepository.Appointments.OfType<UserAppointment>() on e.Id equals app.EncounterId into sa
                              from app in sa.DefaultIfEmpty()

                              join sp in _practiceRepository.Users on app.UserId equals sp.Id into ssp // sp meaning Service Provider
                              from sp in ssp.DefaultIfEmpty()

                              join bd in _practiceRepository.BillingDiagnosis on inv.Id equals bd.InvoiceId into ine
                              from bd in ine.DefaultIfEmpty()
                              where bd.OrdinalId == 1

                              join bdIcd10 in _practiceRepository.BillingDiagnosisIcd10 on inv.Id equals bdIcd10.InvoiceId into ineIcd10
                              from bdIcd10 in ineIcd10.DefaultIfEmpty()
                              where bdIcd10.OrdinalId == 1

                              join bdpi in _practiceRepository.PatientInsurances on e.PatientId equals bdpi.InsuredPatientId into patins
                              from bdpi in patins.Where(pi => InsuranceQueries.IsForAppointment.Invoke(pi, app)).OrderBy(x => x.OrdinalId).Take(1).DefaultIfEmpty()
                              join ip in _practiceRepository.InsurancePolicies on bdpi.InsurancePolicyId equals ip.Id into lip
                              from ip in lip.DefaultIfEmpty()
                              join insur in _practiceRepository.Insurers on ip.InsurerId equals insur.Id into lins
                              from insur in lins.DefaultIfEmpty()

                              select new ChargeInformation
                              {
                                  ChargeDate = DbFunctions.TruncateTime(e.StartDateTime).Value,
                                  Month = e.StartDateTime.Month.ConvertToString(),
                                  Year = e.StartDateTime.Year.ConvertToString(),
                                  BillingOrganization = bo.ShortName,
                                  AttributeToServiceLocation = asl.ShortName,
                                  ServiceLocation = sl.ShortName,
                                  ScheduledProvider = sp.UserName ?? "No Scheduled Provider",
                                  BilledProvider = bp.UserName,
                                  SeviceCategory = est.Name ?? "No Service Category",
                                  Code = ens.Code,
                                  Description = ens.Description,
                                  Modifier = (smc.Code ?? "") + " " + (smc2.Code ?? "") + " " + (smc3.Code ?? "") + " " + (smc4.Code ?? ""),
                                  Units = bs.Unit,
                                  UnitCharge = bs.UnitCharge,
                                  TotalCharge = bs.Unit * bs.UnitCharge,
                                  TotalAllowable = bs.UnitAllowableExpense,
                                  RelativeValueUnit = ens.RelativeValueUnit,
                                  PatientFirstName = p.FirstName,
                                  PatientLastName = p.LastName,
                                  PatientId = p.Id,
                                  Diagnosis = inv.DiagnosisTypeId == 1 ? bd.ExternalSystemEntityMapping.ExternalSystemEntityKey ?? "No Diagnosis" : bdIcd10.DiagnosisCode ?? "No Diagnosis",
                                  InsurerName = insur.Name ?? "No Insurer"
                              };

                return charges.Distinct().OrderBy(bs => bs.ChargeDate);
            }
        }
    }

    public class AdjustmentInformation
    {
        [Display(Name = "Invoice Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime InvoiceDateTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Amount { get; set; }

        [Display(Name = "Billing Provider")]
        public string BillingProvider { get; set; }

        [Display(Name = "Scheduled Provider")]
        public string ScheduledProvider { get; set; }

        [Display(Name = "Service Location")]
        public string ServiceLocation { get; set; }

        [Display(Name = "Billing Organization")]
        public string BillingOrganization { get; set; }

        [Display(Name = "Adjustment Type")]
        public string AdjustmentTypeName { get; set; }

        [Display(Name = "Adj. Type (Short)")]
        public string AdjustmentTypeShortName { get; set; }

        [Display(Name = "Posted")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime DateTimePosted { get; set; }

        [Display(Name = "Month Posted")]
        public string MonthPosted { get; set; }

        [Display(Name = "Year Posted")]
        public string YearPosted { get; set; }

        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }

        [Display(Name = "Service Code")]
        public string ServiceCode { get; set; }

        [Display(Name = "Unit")]
        public decimal? Unit { get; set; }
    }
    public class PaymentInformation
    {
        [Display(Name = "Invoice Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime InvoiceDateTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Amount { get; set; }

        [Display(Name = "Provider")]
        public string BillingProvider { get; set; }

        [Display(Name = "Service Location")]
        public string ServiceLocation { get; set; }

        [Display(Name = "Billing Organization")]
        public string BillingOrganization { get; set; }

        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }

        public string Payer { get; set; }

        [Display(Name = "Pay Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime DateTimePosted { get; set; }

        [Display(Name = "Month Paid")]
        public string MonthPosted { get; set; }

        [Display(Name = "Year Paid")]
        public string YearPosted { get; set; }

        [Display(Name = "Service Code")]
        public string ServiceCode { get; set; }

        [Display(Name = "Unit")]
        public decimal? Unit { get; set; }
    }

    public class FinancialInformation
    {
        [Display(Name = "Invoice Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime InvoiceDateTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Amount { get; set; }

        [Display(Name = "Provider")]
        public string BillingProvider { get; set; }

        [Display(Name = "Service Location")]
        public string ServiceLocation { get; set; }

        [Display(Name = "Billing Organization")]
        public string BillingOrganization { get; set; }

        [Display(Name = "Financial Group")]
        public string FinancialTypeGroup { get; set; }

        [Display(Name = "Financial Type")]
        public string FinancialInformationTypeName { get; set; }

        [Display(Name = "Fin. Type (Short)")]
        public string FinancialInformationTypeShortName { get; set; }

        [Display(Name = "Date Posted")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime DateTimePosted { get; set; }

        [Display(Name = "Month Posted")]
        public string MonthPosted { get; set; }

        [Display(Name = "Year Posted")]
        public string YearPosted { get; set; }

        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }
    }

    public class ScheduleInformation
    {
        [Display(Name = "App Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime AppDate { get; set; }

        [Display(Name = "App Time")]
        [DisplayFormat(DataFormatString = "t")]
        public DateTime AppTime { get; set; }

        [Display(Name = "App Type")]
        public string AppType { get; set; }

        [Display(Name = "App Comment")]
        public string AppComment { get; set; }

        [Display(Name = "Location")]
        public string AppLocation { get; set; }

        [Display(Name = "Appointment Status")]
        public EncounterStatus AppStatus { get; set; }

        [Display(Name = "Confirmation Status")]
        public string ConfirmStatus { get; set; }

        [Display(Name = "Schedule Doctor")]
        public string SchedDoctor { get; set; }

        [Display(Name = "Referring Doctor")]
        public string RefDoctor { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }

        [Display(Name = "Patient Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Patient First Name")]
        public string FirstName { get; set; }

        [Display(Name = "DOB")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? Dob { get; set; }

        [Display(Name = "Age")]
        public string Age { get; set; }

        [Display(Name = "Tag")]
        public string Tag { get; set; }

        [Display(Name = "Gender")]
        public Gender? Gender { get; set; }

        [Display(Name = "Patient Phone Number")]
        public string PatientPhoneNumber { get; set; }

        [Display(Name = "Last Visit")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? LastDischargedVisit { get; set; }

        [Display(Name = "Primary Insurer")]
        public string PrimaryInsurer { get; set; }

        [Display(Name = "Primary Policy #")]
        public string PrimaryPolicy { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? Copay { get; set; }

        [Display(Name = "Secondary Insurer")]
        public string SecondaryInsurer { get; set; }

        [Display(Name = "Secondary Policy #")]
        public string SecondaryPolicy { get; set; }
    }

    public class LastEncounterInformation
    {
        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }

        [Display(Name = "Patient Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Patient First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Prior Patient Id")]
        public string PriorPatientId { get; set; }

        [Display(Name = "Patient Address")]
        public string PatientAddress { get; set; }

        [Display(Name = "Patient City")]
        public string PatientCity { get; set; }

        [Display(Name = "Patient State")]
        public string PatientState { get; set; }

        [Display(Name = "Patient Zip")]
        public string PatientZip { get; set; }

        [Display(Name = "Patient Phone Number")]
        public string PatientPhoneNumber { get; set; }

        [Display(Name = "Patient Email Address")]
        public string PatientEmailAddress { get; set; }

        [Display(Name = "Last Discharged Visit")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? LastDischargedVisit { get; set; }

        [Display(Name = "Next Scheduled Visit")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? NextScheduledVisit { get; set; }

        [Display(Name = "Next Pending Recall")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime? NextPendingRecall { get; set; }

        [Display(Name = "Diagnosis Code")]
        public string DiagnosisCode { get; set; }
    }

    public class ChargeInformation
    {
        [Display(Name = "Charge Date")]
        [DisplayFormat(DataFormatString = "d")]
        public DateTime ChargeDate { get; set; }

        [Display(Name = "Charge Month")]
        public string Month { get; set; }

        [Display(Name = "Charge Year")]
        public string Year { get; set; }

        [Display(Name = "Billing Organization")]
        public string BillingOrganization { get; set; }

        [Display(Name = "Attribute To")]
        public string AttributeToServiceLocation { get; set; }

        [Display(Name = "Service Location")]
        public string ServiceLocation { get; set; }

        [Display(Name = "Scheduled Provider")]
        public string ScheduledProvider { get; set; }

        [Display(Name = "Billing Provider")]
        public string BilledProvider { get; set; }

        [Display(Name = "Service Category")]
        public string SeviceCategory { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Modifier { get; set; }

        public decimal? Units { get; set; }

        [Display(Name = "Unit Charge")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? UnitCharge { get; set; }

        [Display(Name = "Total Charge")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? TotalCharge { get; set; }

        [Display(Name = "Total Allowable")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal? TotalAllowable { get; set; }

        [Display(Name = "Relative Value Unit")]
        public decimal? RelativeValueUnit { get; set; }

        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        [Display(Name = "Patient Id")]
        public int PatientId { get; set; }

        public string Diagnosis { get; set; }

        [Display(Name = "Insurer")]
        public string InsurerName { get; set; }
    }
}