﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Enumeration of valid encounter status Ids.
    /// </summary>
    [DataContract]
    public enum PatientDemographicsList

    {
        [EnumMember]
        [Display(Name = "External Providers ")]
        ExternalProviders  = 1,

        [EnumMember]
        [Display(Name = "Language")]
        Language = 2,

        [EnumMember]
        [Display(Name = "Physicians")]
        Physicians = 3,
        
        [EnumMember]
        [Display(Name = "Referring Category")]
        ReferringCategory = 4
    }
}
