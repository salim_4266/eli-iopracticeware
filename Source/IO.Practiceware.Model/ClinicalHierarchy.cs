﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// SNOMED CT concepts are organized into hierarchies. The the root of the hierarchy, named "SNOMED CT Concept", contains all Concepts in SNOMED CT. The root
    /// subsumes (is the supertype of) the top-level concepts (hierarchies parents) and all the concepts beneath them (their subtypes). As the hierarchies are descended,
    /// the concepts withinthem become increasingly specific.  This is a list of the top level hierarchies.
    /// </summary>
    public enum ClinicalHierarchy
    {
        BodyStructure = 1,
        ClinicalFinding = 2,
        Event = 3,
        EnvironmentOrGeographicalLocation = 4,
        ObservableEntity = 5,
        Organism = 6,
        PharmaceuticalOrBiologicProduct = 7,
        PhysicalForce = 8,
        PhysicalObject = 9,
        Procedure = 10,
        QualifierValue = 11,
        RecordArtifact = 12,
        SituationWithExplicitContext = 13,
        SocialContext = 14,
        SpecialConcept = 15,
        Specimen = 16,
        SnomedCtModelComponent = 17,
        StagingAndScales = 18,
        Substance = 19
    }

    public partial class ClinicalHierarchyClinicalAttribute
    {
        public ClinicalHierarchy ClinicalHierarchy
        {
            get { return (ClinicalHierarchy)ClinicalHierarchyId; }
            set { ClinicalHierarchyId = (int)value; }
        }
    }

}
