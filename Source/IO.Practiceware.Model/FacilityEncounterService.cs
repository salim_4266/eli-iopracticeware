//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// List of services rendered by a practice's internal facility (ambulatory surgery center or ASC)
    /// </summary>
    public partial class FacilityEncounterService : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Code
        {
            get { return _code; }
            set 
    		{ 
    			var isChanged = _code != value; 
    			_code = value; 
    			if (isChanged) OnPropertyChanged("Code");
    		}
        }
        private string _code;
    
        [Required]
    	public virtual decimal DefaultUnit
        {
            get { return _defaultUnit; }
            set 
    		{ 
    			var isChanged = _defaultUnit != value; 
    			_defaultUnit = value; 
    			if (isChanged) OnPropertyChanged("DefaultUnit");
    		}
        }
        private decimal _defaultUnit;
    
        [Required]
    	public virtual decimal UnitFee
        {
            get { return _unitFee; }
            set 
    		{ 
    			var isChanged = _unitFee != value; 
    			_unitFee = value; 
    			if (isChanged) OnPropertyChanged("UnitFee");
    		}
        }
        private decimal _unitFee;
    
    	public virtual string Description
        {
            get { return _description; }
            set 
    		{ 
    			var isChanged = _description != value; 
    			_description = value; 
    			if (isChanged) OnPropertyChanged("Description");
    		}
        }
        private string _description;
    
        [Required]
    	public virtual bool IsZeroChargeAllowedOnClaim
        {
            get { return _isZeroChargeAllowedOnClaim; }
            set 
    		{ 
    			var isChanged = _isZeroChargeAllowedOnClaim != value; 
    			_isZeroChargeAllowedOnClaim = value; 
    			if (isChanged) OnPropertyChanged("IsZeroChargeAllowedOnClaim");
    		}
        }
        private bool _isZeroChargeAllowedOnClaim;
    
    	public virtual Nullable<decimal> RelativeValueUnit
        {
            get { return _relativeValueUnit; }
            set 
    		{ 
    			var isChanged = _relativeValueUnit != value; 
    			_relativeValueUnit = value; 
    			if (isChanged) OnPropertyChanged("RelativeValueUnit");
    		}
        }
        private Nullable<decimal> _relativeValueUnit;
    
    	public virtual Nullable<System.DateTime> StartDateTime
        {
            get { return _startDateTime; }
            set 
    		{ 
    			var isChanged = _startDateTime != value; 
    			_startDateTime = value; 
    			if (isChanged) OnPropertyChanged("StartDateTime");
    		}
        }
        private Nullable<System.DateTime> _startDateTime;
    
    	public virtual Nullable<System.DateTime> EndDateTime
        {
            get { return _endDateTime; }
            set 
    		{ 
    			var isChanged = _endDateTime != value; 
    			_endDateTime = value; 
    			if (isChanged) OnPropertyChanged("EndDateTime");
    		}
        }
        private Nullable<System.DateTime> _endDateTime;
    
        [Required]
    	public virtual bool IsArchived
        {
            get { return _isArchived; }
            set 
    		{ 
    			var isChanged = _isArchived != value; 
    			_isArchived = value; 
    			if (isChanged) OnPropertyChanged("IsArchived");
    		}
        }
        private bool _isArchived;
    
        [Required]
    	public virtual bool IsCliaCertificateRequiredOnClaim
        {
            get { return _isCliaCertificateRequiredOnClaim; }
            set 
    		{ 
    			var isChanged = _isCliaCertificateRequiredOnClaim != value; 
    			_isCliaCertificateRequiredOnClaim = value; 
    			if (isChanged) OnPropertyChanged("IsCliaCertificateRequiredOnClaim");
    		}
        }
        private bool _isCliaCertificateRequiredOnClaim;
    
    	public virtual string UnclassifiedServiceDescription
        {
            get { return _unclassifiedServiceDescription; }
            set 
    		{ 
    			var isChanged = _unclassifiedServiceDescription != value; 
    			_unclassifiedServiceDescription = value; 
    			if (isChanged) OnPropertyChanged("UnclassifiedServiceDescription");
    		}
        }
        private string _unclassifiedServiceDescription;
    
    	public virtual Nullable<int> EncounterServiceTypeId
        {
            get { return _encounterServiceTypeId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_encounterServiceTypeId != value)
                    {
                        if (EncounterServiceType != null && EncounterServiceType.Id != value)
                        {
                            EncounterServiceType = null;
                        }
                        _encounterServiceTypeId = value;
        
        				OnPropertyChanged("EncounterServiceTypeId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _encounterServiceTypeId;
    
    	public virtual Nullable<int> RevenueCodeId
        {
            get { return _revenueCodeId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_revenueCodeId != value)
                    {
                        if (RevenueCode != null && RevenueCode.Id != value)
                        {
                            RevenueCode = null;
                        }
                        _revenueCodeId = value;
        
        				OnPropertyChanged("RevenueCodeId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _revenueCodeId;
    
        [Required]
    	internal virtual int ServiceUnitOfMeasurementId
        {
            get { return _serviceUnitOfMeasurementId; }
            set 
    		{ 
    			var isChanged = _serviceUnitOfMeasurementId != value; 
    			_serviceUnitOfMeasurementId = value; 
    			if (isChanged) OnPropertyChanged("ServiceUnitOfMeasurementId");
    		}
        }
        private int _serviceUnitOfMeasurementId;
    
    	public virtual string NDC
        {
            get { return _nDC; }
            set 
    		{ 
    			var isChanged = _nDC != value; 
    			_nDC = value; 
    			if (isChanged) OnPropertyChanged("NDC");
    		}
        }
        private string _nDC;
    
        [Required]
    	public virtual decimal DrugQuantity
        {
            get { return _drugQuantity; }
            set 
    		{ 
    			var isChanged = _drugQuantity != value; 
    			_drugQuantity = value; 
    			if (isChanged) OnPropertyChanged("DrugQuantity");
    		}
        }
        private decimal _drugQuantity;
    
        [Required]
    	public virtual int DrugUnitOfMeasurementId
        {
            get { return _drugUnitOfMeasurementId; }
            set 
    		{ 
    			var isChanged = _drugUnitOfMeasurementId != value; 
    			_drugUnitOfMeasurementId = value; 
    			if (isChanged) OnPropertyChanged("DrugUnitOfMeasurementId");
    		}
        }
        private int _drugUnitOfMeasurementId;

        #endregion

        #region Navigation Properties
    
    	[Association("EncounterServiceType", "EncounterServiceTypeId", "Id")]
        public virtual EncounterServiceType EncounterServiceType
        {
            get { return _encounterServiceType; }
            set
            {
                if (!ReferenceEquals(_encounterServiceType, value))
                {
                    var previousValue = _encounterServiceType;
                    _encounterServiceType = value;
                    FixupEncounterServiceType(previousValue);
    				OnPropertyChanged("EncounterServiceType");
                }
            }
        }
        private EncounterServiceType _encounterServiceType;
    
    	[Association("RevenueCode", "RevenueCodeId", "Id")]
        public virtual RevenueCode RevenueCode
        {
            get { return _revenueCode; }
            set
            {
                if (!ReferenceEquals(_revenueCode, value))
                {
                    var previousValue = _revenueCode;
                    _revenueCode = value;
                    FixupRevenueCode(previousValue);
    				OnPropertyChanged("RevenueCode");
                }
            }
        }
        private RevenueCode _revenueCode;
    
    	[Association("BillingServices", "Id", "FacilityEncounterServiceId")]
        public virtual System.Collections.Generic.ICollection<BillingService> BillingServices
        {
            get
            {
                if (_billingServices == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<BillingService>();
                    newCollection.ItemSet += FixupBillingServicesItemSet;
                    newCollection.ItemRemoved += FixupBillingServicesItemRemoved;
                    _billingServices = newCollection;
                }
                return _billingServices;
            }
            set
            {
                if (!ReferenceEquals(_billingServices, value))
                {
                    var previousValue = _billingServices as Soaf.Collections.FixupCollection<BillingService>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupBillingServicesItemSet;
                        previousValue.ItemRemoved -= FixupBillingServicesItemRemoved;
                    }
                    _billingServices = value;
                    var newValue = value as Soaf.Collections.FixupCollection<BillingService>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupBillingServicesItemSet;
                        newValue.ItemRemoved += FixupBillingServicesItemRemoved;
                    }
    				OnPropertyChanged("BillingServices");
                }
            }
        }
        private System.Collections.Generic.ICollection<BillingService> _billingServices;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupEncounterServiceType(EncounterServiceType previousValue)
        {
            if (EncounterServiceType != null)
            {
                if (EncounterServiceTypeId != EncounterServiceType.Id)
                {
                    EncounterServiceTypeId = EncounterServiceType.Id;
                }
            }
            else if (!_settingFK)
            {
                EncounterServiceTypeId = null;
            }
        }
    
        private void FixupRevenueCode(RevenueCode previousValue)
        {
            if (RevenueCode != null)
            {
                if (RevenueCodeId != RevenueCode.Id)
                {
                    RevenueCodeId = RevenueCode.Id;
                }
            }
            else if (!_settingFK)
            {
                RevenueCodeId = null;
            }
        }
    
        private void FixupBillingServicesItemSet(object sender, Soaf.EventArgs<BillingService> e)
        {
            var item = e.Value;
     
            item.FacilityEncounterService = this;
        }
    
        private void FixupBillingServicesItemRemoved(object sender, Soaf.EventArgs<BillingService> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.FacilityEncounterService, this))
            {
                item.FacilityEncounterService = null;
            }
        }
    

        #endregion

    }
}
