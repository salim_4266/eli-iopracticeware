//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class PatientRelationship : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual int FromPatientId
        {
            get { return _fromPatientId; }
            set
            {
                if (_fromPatientId != value)
                {
                    if (FromPatient != null && FromPatient.Id != value)
                    {
                        FromPatient = null;
                    }
                    _fromPatientId = value;
    
    				OnPropertyChanged("FromPatientId");
                }
            }
        }
        private int _fromPatientId;
    
    	public virtual int ToPatientId
        {
            get { return _toPatientId; }
            set
            {
                if (_toPatientId != value)
                {
                    if (ToPatient != null && ToPatient.Id != value)
                    {
                        ToPatient = null;
                    }
                    _toPatientId = value;
    
    				OnPropertyChanged("ToPatientId");
                }
            }
        }
        private int _toPatientId;
    
        [Required]
    	public virtual bool HasSignedHipaa
        {
            get { return _hasSignedHipaa; }
            set 
    		{ 
    			var isChanged = _hasSignedHipaa != value; 
    			_hasSignedHipaa = value; 
    			if (isChanged) OnPropertyChanged("HasSignedHipaa");
    		}
        }
        private bool _hasSignedHipaa;
    
    	public virtual string PatientRelationshipDescription
        {
            get { return _patientRelationshipDescription; }
            set 
    		{ 
    			var isChanged = _patientRelationshipDescription != value; 
    			_patientRelationshipDescription = value; 
    			if (isChanged) OnPropertyChanged("PatientRelationshipDescription");
    		}
        }
        private string _patientRelationshipDescription;

        #endregion

        #region Navigation Properties
    
    	[Association("FromPatient", "FromPatientId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Patient FromPatient
        {
            get { return _fromPatient; }
            set
            {
                if (!ReferenceEquals(_fromPatient, value))
                {
                    var previousValue = _fromPatient;
                    _fromPatient = value;
                    FixupFromPatient(previousValue);
    				OnPropertyChanged("FromPatient");
                }
            }
        }
        private Patient _fromPatient;
    
    	[Association("ToPatient", "ToPatientId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Patient ToPatient
        {
            get { return _toPatient; }
            set
            {
                if (!ReferenceEquals(_toPatient, value))
                {
                    var previousValue = _toPatient;
                    _toPatient = value;
                    FixupToPatient(previousValue);
    				OnPropertyChanged("ToPatient");
                }
            }
        }
        private Patient _toPatient;

        #endregion

        #region Association Fixup
    
        private void FixupFromPatient(Patient previousValue)
        {
            if (previousValue != null && previousValue.FromPatientRelationships.Contains(this))
            {
                previousValue.FromPatientRelationships.Remove(this);
            }
    
            if (FromPatient != null)
            {
                if (!FromPatient.FromPatientRelationships.Contains(this))
                {
                    FromPatient.FromPatientRelationships.Add(this);
                }
                if (FromPatient != null && FromPatientId != FromPatient.Id)
                {
                    FromPatientId = FromPatient.Id;
                }
            }
        }
    
        private void FixupToPatient(Patient previousValue)
        {
            if (previousValue != null && previousValue.ToPatientRelationships.Contains(this))
            {
                previousValue.ToPatientRelationships.Remove(this);
            }
    
            if (ToPatient != null)
            {
                if (!ToPatient.ToPatientRelationships.Contains(this))
                {
                    ToPatient.ToPatientRelationships.Add(this);
                }
                if (ToPatient != null && ToPatientId != ToPatient.Id)
                {
                    ToPatientId = ToPatient.Id;
                }
            }
        }

        #endregion

    }
}
