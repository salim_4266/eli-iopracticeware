﻿namespace IO.Practiceware.Model
{
    public partial class EncounterService
    {
        public ServiceUnitOfMeasurement ServiceUnitOfMeasurement
        {
            get { return (ServiceUnitOfMeasurement) ServiceUnitOfMeasurementId; }
            set { ServiceUnitOfMeasurementId = (int) value; }
        }


        public DrugUnitOfMeasurement DrugUnitOfMeasurement { get; set; }


    }
}