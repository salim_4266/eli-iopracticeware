﻿using System;
using System.ComponentModel;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class Task
    {
        /// <summary>
        /// Gets a value indicating whether this instance is complete.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is complete; otherwise, <c>false</c>.
        /// </value>
        public bool IsComplete
        {
            get { return TaskActivities.All(ta => ta.IsComplete); }
        }

        /// <summary>
        /// Gets a value indicating whether [alert active].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [alert active]; otherwise, <c>false</c>.
        /// </value>
        public bool AlertActive
        {
            get { return TaskActivities.Any(ta => ta.TaskActivityUsers.Any(tar => tar.AlertActive)); }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public string Status
        {
            get
            {
                if (IsComplete)
                {
                    return "Complete";
                }
                if (AlertActive)
                {
                    return "Alarm Active";
                }
                return "Active";
            }
        }

        /// <summary>
        /// Gets the display name of the patient.
        /// </summary>
        /// <value>
        /// The display name of the patient.
        /// </value>
        [DisplayName(@"Patient")]
        public string PatientDisplayName
        {
            get { return Patient != null ? Patient.DisplayName : "<No Patient>"; }
        }

        /// <summary>
        /// Gets the name of the task activity type.
        /// </summary>
        /// <value>
        /// The name of the task activity type.
        /// </value>
        [DisplayName(@"Type")]
        public string TaskActivityTypeName
        {
            get { return TaskActivities.Where(ta => ta.TaskActivityType != null).Select(ta => ta.TaskActivityType.Name).LastOrDefault(); }
        }

        /// <summary>
        /// Gets the created date time.
        /// </summary>
        [Description("Created")]
        public DateTime? CreatedDateTime
        {
            get { return TaskActivities.Select(ta => ta.CreatedDateTime).FirstOrDefault(); }
        }
    }
}
