﻿namespace IO.Practiceware.Model
{
    public partial class ClinicalQualifierCategory
    {
        public const string Severity = "SEVERITY";
        public const string Chronicity = "CHRONICITY";
    }
}
