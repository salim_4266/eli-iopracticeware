﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum MaritalStatus
    {
        [Display(Name = "S", Order = 1)]
        Single = 1,

        [Display(Name = "M", Order = 2)]
        Married = 2,

        [Display(Name = "D", Order = 4)]
        Divorced = 3,

        [Display(Name = "W", Order = 5)]
        Widowed = 4,

        [Display(Name = "A", Order = 6)]
        Annulled = 5,

        [Display(Name = "T", Order = 3)]
        DomesticPartner = 6,

        [Display(Name = "I", Order = 7)]
        Interlocutory = 7,

        [Display(Name = "L", Order = 8)]
        LegallySeparated = 8,

        [Display(Name = "P", Order = 10)]
        Polygamous = 9,

        [Display(Name = "N", Order = 9)]
        NeverMarried = 10
    }
}
