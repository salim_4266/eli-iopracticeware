﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="edmx ssdl mapping"
  xmlns:ssdl="http://schemas.microsoft.com/ado/2009/11/edm/ssdl" xmlns:mapping="http://schemas.microsoft.com/ado/2009/11/mapping/cs"
  xmlns:edmx="http://schemas.microsoft.com/ado/2009/11/edmx">
  <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//edmx:Edmx/edmx:Runtime/edmx:StorageModels/ssdl:Schema">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:text>
      </xsl:text>
      <!-- Fix SSDL for function import which disappears with our GenerateEDM.tt file -->
      <Function xmlns="http://schemas.microsoft.com/ado/2009/11/edm/ssdl" Name="CheckPatientHasPendingAlert" Aggregate="false" BuiltIn="false" NiladicFunction="false" IsComposable="false" ParameterTypeSemantics="AllowImplicitConversion" Schema="model">
        <Parameter Name="PatientId" Type="int" Mode="In" />
        <Parameter Name="AlertType" Type="int" Mode="In" />
      </Function>
      
      <Function xmlns="http://schemas.microsoft.com/ado/2009/11/edm/ssdl" Name="GetRankedPatientSearchResults" Aggregate="false" BuiltIn="false" NiladicFunction="false" IsComposable="false" ParameterTypeSemantics="AllowImplicitConversion" Schema="model">
        <Parameter Name="Fields" Mode="In" Type="nvarchar(max)" />
        <Parameter Name="SearchText" Mode="In" Type="nvarchar(max)" />
        <Parameter Name="TextSearchMode" Mode="In" Type="nvarchar(max)" />
        <Parameter Name="IsClinical" Mode="In" Type="bit" />
        <Parameter Name="IsActive" Mode="In" Type="bit" />
      </Function>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="//edmx:Edmx/edmx:Runtime/edmx:Mappings/mapping:Mapping/mapping:EntityContainerMapping">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:text>
      </xsl:text>
        <!-- Fix C-S for function import which disappears with our GenerateEDM.tt file -->
        <FunctionImportMapping xmlns="http://schemas.microsoft.com/ado/2009/11/mapping/cs" FunctionImportName="CheckPatientHasPendingAlert" FunctionName="IO.Practiceware.Model.Store.CheckPatientHasPendingAlert" />
      
        <FunctionImportMapping xmlns="http://schemas.microsoft.com/ado/2009/11/mapping/cs" FunctionImportName="GetRankedPatientSearchResults" FunctionName="IO.Practiceware.Model.Store.GetRankedPatientSearchResults" />
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>