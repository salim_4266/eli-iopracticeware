﻿using System;

namespace IO.Practiceware.Model
{
    public partial class PatientMedicationDetail
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }

    public partial class MedicationOrderFavorite
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }

    public partial class EncounterDiagnosticTestOrder
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }

    public partial class EncounterProcedureOrder
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }

    public partial class EncounterVaccinationOrder
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }

    public partial class EncounterSubsequentVisitOrder
    {
        public TimeFrame? TimeFrame
        {
            get { return (TimeFrame?)TimeFrameId; }
            set { TimeFrameId = (int?)value; }
        }
    }
    public partial class AllergenOccurrence
    {
        public TimeFrame TimeFrame
        {
            get { return (TimeFrame)TimeFrameId; }
            set { TimeFrameId = (int)value; }
        }
    }

    public enum TimeFrame
    {
        Minute = 1,
        Hour = 2,
        Day = 3,
        Week = 4,
        Month = 5,
        Year = 6
    }

    public static class TimeFrames
    {
        public static DateTime Add(this DateTime value, TimeFrame timeFrame, int timeUnit)
        {
            switch (timeFrame)
            {
                case TimeFrame.Day:
                    return value.AddDays(timeUnit);
                case TimeFrame.Hour:
                    return value.AddHours(timeUnit);
                case TimeFrame.Minute:
                    return value.AddMinutes(timeUnit);
                case TimeFrame.Month:
                    return value.AddMonths(timeUnit);
                case TimeFrame.Week:
                    return value.AddDays(timeUnit * 7);
                case TimeFrame.Year:
                    return value.AddYears(timeUnit);
                default:
                    throw new NotSupportedException();
            }
        }
    }
}