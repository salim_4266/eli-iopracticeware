﻿
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Well known external systems.
    /// </summary>
    public enum ExternalSystemId
    {
        [Display(Name = "IO Practiceware")]
        IOPracticeware = 1,
        Email = 2,
        TextMessage = 3,
        ClaimFileMessage = 4,
        Printer = 5,
        Cpt4 = 6,
        [Display(Name = "CVX")]
        CvxVaccineAdministered = 7,
        Fdb = 8,
        [Display(Name = "ICD10")]
        Icd10 = 9,
        [Display(Name = "ICD9")]
        Icd9 = 10,
        IoLegacy = 11,
        Loinc = 12,
        NciThesaurus = 13,
        NewCrop = 14,
        RxNorm = 15,
        [Display(Name="SNOMED-CT")]
        SnomedCt = 16,
        UnifiedCodeForUnitsOfMeasure = 17,
        UnifiedSampleIdentifier = 18,
        UniqueIngredientIdentifier = 19,
        CDC = 20,
        [Display(Name = "OMEDIX")]
        Omedix = 21,
        ExternalLaboratory = 22,
        QualityDataModel = 23,
        Saved = 24,
        Payer = 25,
        PatientStatement = 26
    }
}
