﻿
namespace IO.Practiceware.Model
{
    public enum PatientRelationshipTypeId
    {
        Brother = 1,
        Sister = 2,
        Mother = 3,
        Father = 4,
        Spouse = 5,
        Guardian = 6,
        Unknown = 7, 
        Self = 8
    }
}