﻿using System.Runtime.Serialization;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// This class represents data that will be stored as Xml in the ApplicationSettings table.  The Application Settings table can contain 
    /// many different types of Configuration Settings.  This is one of those types.  Particularly this class represents file directory
    /// settings on a per machine basis for claim generation.
    /// </summary>
    [DataContract]
    public class ClaimsConfigurationApplicationSetting : IApplicationSetting
    {
        [DataMember]
        public string ElectronicClaimsFileDirectory { get; set; }

        [DataMember]
        public string PaperClaimsFileDirectory { get; set; }
    }
}
