﻿using Soaf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A Tag is a user-defined unit of meta-data that can be applied to anything.  It is a general purpose way of attaching some identifying
    /// information to an existing entity. Sometimes we cannot always pre-determine what user's will want and so this is a way of storing 
    /// extra information that our user's define. For example: Patients are one type of entity that may potentially contain lots and lots of information.
    /// One piece of information is 'Patient Category'.  With tags, we can attach as many 'Categories' as we like to a patient.
    /// </summary>
    public partial class Tag
    {
        public TagType TagType
        {
            get { return (TagType)TagTypeId; }
            set { TagTypeId = (int)value; }
        }

       public Color Color 
       {
            get
            {
                var hexColor = (HexColor.StartsWith("#") ? HexColor : "#" + HexColor);

                var colorObj = ColorConverter.ConvertFromString(hexColor);
                if (colorObj == null)
                {
                    throw new NullReferenceException("Could not convert the HexColor {0} to a Color".FormatWith(hexColor));
                }

                return (Color)colorObj;
            }
        }
    }

    public static class TagExtensions
    {
        /// <summary>
        /// Returns only those tags which have a type of 'Patient Category'.  These tags represent a classification or category of patient.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public static IEnumerable<Tag> WithTagTypePatientCategory(this IEnumerable<Tag> tags)
        {
            return tags.WithTagType(TagType.PatientCategory);
        }

        /// <summary>
        /// Filter the current collection of tags by TagType.  If you do not pass a type in, it will return tags that have no type (null).
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IEnumerable<Tag> WithTagType(this IEnumerable<Tag> tags, TagType type)
        {
            return tags.Where(x => x.TagType == type);
        }       
    }

    /// <summary>
    /// Represents a categorization or classification for tags.
    /// </summary>
    public enum TagType
    {
        /// <summary>
        /// No defined category
        /// </summary>
        None = 0,

        /// <summary>
        /// User defined.  Used in reporting and various displays throughout IO to put groups of patients into various categories.  
        /// Common use is to indicate whether the patient has only electronic health records (E), or only Paper records (P) or both electronic and paper (B).
        /// </summary>
        PatientCategory = 1
    }
}
