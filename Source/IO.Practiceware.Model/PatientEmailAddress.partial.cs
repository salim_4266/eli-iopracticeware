﻿using Soaf;
using Soaf.Reflection;

namespace IO.Practiceware.Model
{
    public partial class PatientEmailAddress : IHasOrdinalId, IEmailAddress
    {
        public PatientEmailAddress()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => Patient.IfNotNull(p => p.PatientEmailAddresses),
                Reflector.GetMember(() => Patient).Name);
        }

        public EmailAddressType EmailAddressType
        {
            get { return (EmailAddressType)EmailAddressTypeId; }
            set { EmailAddressTypeId = (int)value; }
        }
    }
}
