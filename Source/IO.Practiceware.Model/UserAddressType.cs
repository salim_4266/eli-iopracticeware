//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class UserAddressType : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;
    
        [Required]
    	public virtual bool IsArchived
        {
            get { return _isArchived; }
            set 
    		{ 
    			var isChanged = _isArchived != value; 
    			_isArchived = value; 
    			if (isChanged) OnPropertyChanged("IsArchived");
    		}
        }
        private bool _isArchived = false;

        #endregion

        #region Navigation Properties
    
    	[Association("UserAddresses", "Id", "UserAddressTypeId")]
        public virtual System.Collections.Generic.ICollection<UserAddress> UserAddresses
        {
            get
            {
                if (_userAddresses == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<UserAddress>();
                    newCollection.ItemSet += FixupUserAddressesItemSet;
                    newCollection.ItemRemoved += FixupUserAddressesItemRemoved;
                    _userAddresses = newCollection;
                }
                return _userAddresses;
            }
            set
            {
                if (!ReferenceEquals(_userAddresses, value))
                {
                    var previousValue = _userAddresses as Soaf.Collections.FixupCollection<UserAddress>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupUserAddressesItemSet;
                        previousValue.ItemRemoved -= FixupUserAddressesItemRemoved;
                    }
                    _userAddresses = value;
                    var newValue = value as Soaf.Collections.FixupCollection<UserAddress>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupUserAddressesItemSet;
                        newValue.ItemRemoved += FixupUserAddressesItemRemoved;
                    }
    				OnPropertyChanged("UserAddresses");
                }
            }
        }
        private System.Collections.Generic.ICollection<UserAddress> _userAddresses;

        #endregion

        #region Association Fixup
    
        private void FixupUserAddressesItemSet(object sender, Soaf.EventArgs<UserAddress> e)
        {
            var item = e.Value;
     
            item.UserAddressType = this;
        }
    
        private void FixupUserAddressesItemRemoved(object sender, Soaf.EventArgs<UserAddress> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.UserAddressType, this))
            {
                item.UserAddressType = null;
            }
        }
    

        #endregion

    }
}
