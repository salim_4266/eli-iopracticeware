//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// This entity allows for commerical insurance companies to cross over to Medicare OR other insurnace companies. There is a double assciation between insurer and claimcrossover one points to the WillSend and the other to WillReceive. - This entity allows for any primary insurance company to cross over adjudicated claims to any secondary insurer.  If an insurer both crosses adjudicated claims to other insurers, and receives crossed over claims from another insurer, they will be included in the entity multiple times, both in WillSend and in WillReceive, depending on whether they are primary or secondary
    /// </summary>
    public partial class ClaimCrossOver : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual long Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private long _id;
    
    	public virtual int SendingInsurerId
        {
            get { return _sendingInsurerId; }
            set
            {
                if (_sendingInsurerId != value)
                {
                    if (SendingInsurer != null && SendingInsurer.Id != value)
                    {
                        SendingInsurer = null;
                    }
                    _sendingInsurerId = value;
    
    				OnPropertyChanged("SendingInsurerId");
                }
            }
        }
        private int _sendingInsurerId;
    
    	public virtual int ReceivingInsurerId
        {
            get { return _receivingInsurerId; }
            set
            {
                if (_receivingInsurerId != value)
                {
                    if (ReceivingInsurer != null && ReceivingInsurer.Id != value)
                    {
                        ReceivingInsurer = null;
                    }
                    _receivingInsurerId = value;
    
    				OnPropertyChanged("ReceivingInsurerId");
                }
            }
        }
        private int _receivingInsurerId;

        #endregion

        #region Navigation Properties
    
    	[Association("ReceivingInsurer", "ReceivingInsurerId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Insurer ReceivingInsurer
        {
            get { return _receivingInsurer; }
            set
            {
                if (!ReferenceEquals(_receivingInsurer, value))
                {
                    var previousValue = _receivingInsurer;
                    _receivingInsurer = value;
                    FixupReceivingInsurer(previousValue);
    				OnPropertyChanged("ReceivingInsurer");
                }
            }
        }
        private Insurer _receivingInsurer;
    
    	[Association("SendingInsurer", "SendingInsurerId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Insurer SendingInsurer
        {
            get { return _sendingInsurer; }
            set
            {
                if (!ReferenceEquals(_sendingInsurer, value))
                {
                    var previousValue = _sendingInsurer;
                    _sendingInsurer = value;
                    FixupSendingInsurer(previousValue);
    				OnPropertyChanged("SendingInsurer");
                }
            }
        }
        private Insurer _sendingInsurer;

        #endregion

        #region Association Fixup
    
        private void FixupReceivingInsurer(Insurer previousValue)
        {
            if (previousValue != null && previousValue.ReceivingClaimCrossOvers.Contains(this))
            {
                previousValue.ReceivingClaimCrossOvers.Remove(this);
            }
    
            if (ReceivingInsurer != null)
            {
                if (!ReceivingInsurer.ReceivingClaimCrossOvers.Contains(this))
                {
                    ReceivingInsurer.ReceivingClaimCrossOvers.Add(this);
                }
                if (ReceivingInsurer != null && ReceivingInsurerId != ReceivingInsurer.Id)
                {
                    ReceivingInsurerId = ReceivingInsurer.Id;
                }
            }
        }
    
        private void FixupSendingInsurer(Insurer previousValue)
        {
            if (previousValue != null && previousValue.SendingClaimCrossOvers.Contains(this))
            {
                previousValue.SendingClaimCrossOvers.Remove(this);
            }
    
            if (SendingInsurer != null)
            {
                if (!SendingInsurer.SendingClaimCrossOvers.Contains(this))
                {
                    SendingInsurer.SendingClaimCrossOvers.Add(this);
                }
                if (SendingInsurer != null && SendingInsurerId != SendingInsurer.Id)
                {
                    SendingInsurerId = SendingInsurer.Id;
                }
            }
        }

        #endregion

    }
}
