//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Configuration of screens, questions asked, values recorded and billing codes triggered when a procedure is performed.
    /// </summary>
    public partial class ProcedurePerformed : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;
    
    	public virtual int ScreenId
        {
            get { return _screenId; }
            set
            {
                if (_screenId != value)
                {
                    if (Screen != null && Screen.Id != value)
                    {
                        Screen = null;
                    }
                    _screenId = value;
    
    				OnPropertyChanged("ScreenId");
                }
            }
        }
        private int _screenId;

        #endregion

        #region Navigation Properties
    
    	[Association("ProcedureOrders", "", "")]
        public virtual System.Collections.Generic.ICollection<ProcedureOrder> ProcedureOrders
        {
            get
            {
                if (_procedureOrders == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ProcedureOrder>();
                    newCollection.ItemSet += FixupProcedureOrdersItemSet;
                    newCollection.ItemRemoved += FixupProcedureOrdersItemRemoved;
                    _procedureOrders = newCollection;
                }
                return _procedureOrders;
            }
            set
            {
                if (!ReferenceEquals(_procedureOrders, value))
                {
                    var previousValue = _procedureOrders as Soaf.Collections.FixupCollection<ProcedureOrder>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupProcedureOrdersItemSet;
                        previousValue.ItemRemoved -= FixupProcedureOrdersItemRemoved;
                    }
                    _procedureOrders = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ProcedureOrder>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupProcedureOrdersItemSet;
                        newValue.ItemRemoved += FixupProcedureOrdersItemRemoved;
                    }
    				OnPropertyChanged("ProcedureOrders");
                }
            }
        }
        private System.Collections.Generic.ICollection<ProcedureOrder> _procedureOrders;
    
    	[Association("Screen", "ScreenId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Screen Screen
        {
            get { return _screen; }
            set
            {
                if (!ReferenceEquals(_screen, value))
                {
                    var previousValue = _screen;
                    _screen = value;
                    FixupScreen(previousValue);
    				OnPropertyChanged("Screen");
                }
            }
        }
        private Screen _screen;
    
    	/// <summary>
    	/// Which diagnoses support performing the procedure
    	/// </summary>
    	[Association("ClinicalConditions", "", "")]
        public virtual System.Collections.Generic.ICollection<ClinicalCondition> ClinicalConditions
        {
            get
            {
                if (_clinicalConditions == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ClinicalCondition>();
                    newCollection.ItemSet += FixupClinicalConditionsItemSet;
                    newCollection.ItemRemoved += FixupClinicalConditionsItemRemoved;
                    _clinicalConditions = newCollection;
                }
                return _clinicalConditions;
            }
            set
            {
                if (!ReferenceEquals(_clinicalConditions, value))
                {
                    var previousValue = _clinicalConditions as Soaf.Collections.FixupCollection<ClinicalCondition>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupClinicalConditionsItemSet;
                        previousValue.ItemRemoved -= FixupClinicalConditionsItemRemoved;
                    }
                    _clinicalConditions = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ClinicalCondition>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupClinicalConditionsItemSet;
                        newValue.ItemRemoved += FixupClinicalConditionsItemRemoved;
                    }
    				OnPropertyChanged("ClinicalConditions");
                }
            }
        }
        private System.Collections.Generic.ICollection<ClinicalCondition> _clinicalConditions;

        #endregion

        #region Association Fixup
    
        private void FixupScreen(Screen previousValue)
        {
            if (previousValue != null && previousValue.ProcedurePerformeds.Contains(this))
            {
                previousValue.ProcedurePerformeds.Remove(this);
            }
    
            if (Screen != null)
            {
                if (!Screen.ProcedurePerformeds.Contains(this))
                {
                    Screen.ProcedurePerformeds.Add(this);
                }
                if (Screen != null && ScreenId != Screen.Id)
                {
                    ScreenId = Screen.Id;
                }
            }
        }
    
        private void FixupProcedureOrdersItemSet(object sender, Soaf.EventArgs<ProcedureOrder> e)
        {
            var item = e.Value;
     
            if (!item.ProcedurePerformeds.Contains(this))
            {
                item.ProcedurePerformeds.Add(this);
            }
        }
    
        private void FixupProcedureOrdersItemRemoved(object sender, Soaf.EventArgs<ProcedureOrder> e)
        {
            var item = e.Value;
     
            if (item.ProcedurePerformeds.Contains(this))
            {
                item.ProcedurePerformeds.Remove(this);
            }
        }
    
    
        private void FixupClinicalConditionsItemSet(object sender, Soaf.EventArgs<ClinicalCondition> e)
        {
            var item = e.Value;
     
            if (!item.ProcedurePerformeds.Contains(this))
            {
                item.ProcedurePerformeds.Add(this);
            }
        }
    
        private void FixupClinicalConditionsItemRemoved(object sender, Soaf.EventArgs<ClinicalCondition> e)
        {
            var item = e.Value;
     
            if (item.ProcedurePerformeds.Contains(this))
            {
                item.ProcedurePerformeds.Remove(this);
            }
        }
    

        #endregion

    }
}
