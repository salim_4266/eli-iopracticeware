﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    [DisplayName("Crossover")]
    [MetadataType(typeof(ClaimCrossOverMetadata))]
    public partial class ClaimCrossOver
    {
    }

    public class ClaimCrossOverMetadata
    {
        [Display(Name = "Receiving Insurer")]
        public Insurer ReceivingInsurer { get; set; }

        [Display(Name = "Sending Insurer")]
        public int SendingInsurerId { get; set; }
    }
}