﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum ClaimFrequencyTypeCode
    {
        [Display(Name = "Original Claim")]
        OriginalClaim = 1,
        [Display(Name = "Replacement Or Corrected Claim")]
        ReplacementOrCorrectedClaim = 7,
        [Display(Name = "Voided Or Cancelled Claim")]
        VoidedOrCancelledClaim = 8
    }
}
