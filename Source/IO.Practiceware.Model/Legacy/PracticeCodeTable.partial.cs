﻿using System.ComponentModel;

namespace IO.Practiceware.Model.Legacy
{
    /// <summary>
    /// Different well known values for PracticeCodeTable ReferenceTypes.
    /// </summary>
    public enum PracticeCodeTableReferenceType
    {
        [Description("ACTIVITYSTATUS")]
        ActivityStatus,
        [Description("MARITAL")]
        MaritalStatus,
        [Description("LANGUAGE")]
        Language,
        [Description("INSURERREF")]
        Insurer,
        [Description("RELATIONSHIP")]
        Relationship
    }
}
