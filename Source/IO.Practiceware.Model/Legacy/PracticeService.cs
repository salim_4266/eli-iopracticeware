//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PracticeService : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int CodeId
        {
            get { return _codeId; }
            set 
    		{ 
    			var isChanged = _codeId != value; 
    			_codeId = value; 
    			if (isChanged) OnPropertyChanged("CodeId");
    		}
        }
        private int _codeId;
    
        
        [StringLength(16, ErrorMessage="This field cannot be longer than 16 characters")]
    	public virtual string Code
        {
            get { return _code; }
            set 
    		{ 
    			var isChanged = _code != value; 
    			_code = value; 
    			if (isChanged) OnPropertyChanged("Code");
    		}
        }
        private string _code;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string CodeType
        {
            get { return _codeType; }
            set 
    		{ 
    			var isChanged = _codeType != value; 
    			_codeType = value; 
    			if (isChanged) OnPropertyChanged("CodeType");
    		}
        }
        private string _codeType = "";
    
        
        [StringLength(32, ErrorMessage="This field cannot be longer than 32 characters")]
    	public virtual string CodeCategory
        {
            get { return _codeCategory; }
            set 
    		{ 
    			var isChanged = _codeCategory != value; 
    			_codeCategory = value; 
    			if (isChanged) OnPropertyChanged("CodeCategory");
    		}
        }
        private string _codeCategory = "";
    
        
        [StringLength(64, ErrorMessage="This field cannot be longer than 64 characters")]
    	public virtual string Description
        {
            get { return _description; }
            set 
    		{ 
    			var isChanged = _description != value; 
    			_description = value; 
    			if (isChanged) OnPropertyChanged("Description");
    		}
        }
        private string _description = "";
    
    	public virtual Nullable<float> Fee
        {
            get { return _fee; }
            set 
    		{ 
    			var isChanged = _fee != value; 
    			_fee = value; 
    			if (isChanged) OnPropertyChanged("Fee");
    		}
        }
        private Nullable<float> _fee = 0F;
    
        
        [StringLength(32, ErrorMessage="This field cannot be longer than 32 characters")]
    	public virtual string TOS
        {
            get { return _tOS; }
            set 
    		{ 
    			var isChanged = _tOS != value; 
    			_tOS = value; 
    			if (isChanged) OnPropertyChanged("TOS");
    		}
        }
        private string _tOS = "";
    
        
        [StringLength(32, ErrorMessage="This field cannot be longer than 32 characters")]
    	public virtual string POS
        {
            get { return _pOS; }
            set 
    		{ 
    			var isChanged = _pOS != value; 
    			_pOS = value; 
    			if (isChanged) OnPropertyChanged("POS");
    		}
        }
        private string _pOS = "";
    
        
        [StringLength(10, ErrorMessage="This field cannot be longer than 10 characters")]
    	public virtual string StartDate
        {
            get { return _startDate; }
            set 
    		{ 
    			var isChanged = _startDate != value; 
    			_startDate = value; 
    			if (isChanged) OnPropertyChanged("StartDate");
    		}
        }
        private string _startDate = "";
    
        
        [StringLength(10, ErrorMessage="This field cannot be longer than 10 characters")]
    	public virtual string EndDate
        {
            get { return _endDate; }
            set 
    		{ 
    			var isChanged = _endDate != value; 
    			_endDate = value; 
    			if (isChanged) OnPropertyChanged("EndDate");
    		}
        }
        private string _endDate = "";
    
    	public virtual Nullable<float> RelativeValueUnit
        {
            get { return _relativeValueUnit; }
            set 
    		{ 
    			var isChanged = _relativeValueUnit != value; 
    			_relativeValueUnit = value; 
    			if (isChanged) OnPropertyChanged("RelativeValueUnit");
    		}
        }
        private Nullable<float> _relativeValueUnit = 0F;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string OrderDoc
        {
            get { return _orderDoc; }
            set 
    		{ 
    			var isChanged = _orderDoc != value; 
    			_orderDoc = value; 
    			if (isChanged) OnPropertyChanged("OrderDoc");
    		}
        }
        private string _orderDoc = "F";
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string ConsultOn
        {
            get { return _consultOn; }
            set 
    		{ 
    			var isChanged = _consultOn != value; 
    			_consultOn = value; 
    			if (isChanged) OnPropertyChanged("ConsultOn");
    		}
        }
        private string _consultOn = "F";
    
        
        [StringLength(128, ErrorMessage="This field cannot be longer than 128 characters")]
    	public virtual string ClaimNote
        {
            get { return _claimNote; }
            set 
    		{ 
    			var isChanged = _claimNote != value; 
    			_claimNote = value; 
    			if (isChanged) OnPropertyChanged("ClaimNote");
    		}
        }
        private string _claimNote = "";
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string ServiceFilter
        {
            get { return _serviceFilter; }
            set 
    		{ 
    			var isChanged = _serviceFilter != value; 
    			_serviceFilter = value; 
    			if (isChanged) OnPropertyChanged("ServiceFilter");
    		}
        }
        private string _serviceFilter = "F";
    
        
        [StringLength(16, ErrorMessage="This field cannot be longer than 16 characters")]
    	public virtual string NDC
        {
            get { return _nDC; }
            set 
    		{ 
    			var isChanged = _nDC != value; 
    			_nDC = value; 
    			if (isChanged) OnPropertyChanged("NDC");
    		}
        }
        private string _nDC = "";

        #endregion

    }
}
