﻿using Soaf.Collections;

namespace IO.Practiceware.Model.Legacy
{
    public partial class CLInventory
    {
        public string Name
        {
            get { return new[] { Series, Type_, WearTime }.Join(" "); }
        }
    }
}
