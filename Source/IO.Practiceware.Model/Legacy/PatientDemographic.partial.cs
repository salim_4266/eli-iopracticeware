﻿using Soaf;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PatientDemographic
    {
        public string DisplayName
        {
            get { return "{0}, {1}".FormatWith(LastName, FirstName); }
        }
    }
}