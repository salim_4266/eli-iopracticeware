//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class Pharmacy : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        
        [StringLength(7, ErrorMessage="This field cannot be longer than 7 characters")]
        [Key]
    	public virtual string NCPDPID
        {
            get { return _nCPDPID; }
            set 
    		{ 
    			var isChanged = _nCPDPID != value; 
    			_nCPDPID = value; 
    			if (isChanged) OnPropertyChanged("NCPDPID");
    		}
        }
        private string _nCPDPID;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string StoreNumber
        {
            get { return _storeNumber; }
            set 
    		{ 
    			var isChanged = _storeNumber != value; 
    			_storeNumber = value; 
    			if (isChanged) OnPropertyChanged("StoreNumber");
    		}
        }
        private string _storeNumber;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string ReferenceNumberAlt1
        {
            get { return _referenceNumberAlt1; }
            set 
    		{ 
    			var isChanged = _referenceNumberAlt1 != value; 
    			_referenceNumberAlt1 = value; 
    			if (isChanged) OnPropertyChanged("ReferenceNumberAlt1");
    		}
        }
        private string _referenceNumberAlt1;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string ReferenceNumberAlt1Qualifier
        {
            get { return _referenceNumberAlt1Qualifier; }
            set 
    		{ 
    			var isChanged = _referenceNumberAlt1Qualifier != value; 
    			_referenceNumberAlt1Qualifier = value; 
    			if (isChanged) OnPropertyChanged("ReferenceNumberAlt1Qualifier");
    		}
        }
        private string _referenceNumberAlt1Qualifier;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string StoreName
        {
            get { return _storeName; }
            set 
    		{ 
    			var isChanged = _storeName != value; 
    			_storeName = value; 
    			if (isChanged) OnPropertyChanged("StoreName");
    		}
        }
        private string _storeName;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string AddressLine1
        {
            get { return _addressLine1; }
            set 
    		{ 
    			var isChanged = _addressLine1 != value; 
    			_addressLine1 = value; 
    			if (isChanged) OnPropertyChanged("AddressLine1");
    		}
        }
        private string _addressLine1;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string AddressLine2
        {
            get { return _addressLine2; }
            set 
    		{ 
    			var isChanged = _addressLine2 != value; 
    			_addressLine2 = value; 
    			if (isChanged) OnPropertyChanged("AddressLine2");
    		}
        }
        private string _addressLine2;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string City
        {
            get { return _city; }
            set 
    		{ 
    			var isChanged = _city != value; 
    			_city = value; 
    			if (isChanged) OnPropertyChanged("City");
    		}
        }
        private string _city;
    
        
        [StringLength(2, ErrorMessage="This field cannot be longer than 2 characters")]
    	public virtual string State
        {
            get { return _state; }
            set 
    		{ 
    			var isChanged = _state != value; 
    			_state = value; 
    			if (isChanged) OnPropertyChanged("State");
    		}
        }
        private string _state;
    
        
        [StringLength(11, ErrorMessage="This field cannot be longer than 11 characters")]
    	public virtual string Zip
        {
            get { return _zip; }
            set 
    		{ 
    			var isChanged = _zip != value; 
    			_zip = value; 
    			if (isChanged) OnPropertyChanged("Zip");
    		}
        }
        private string _zip;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhonePrimary
        {
            get { return _phonePrimary; }
            set 
    		{ 
    			var isChanged = _phonePrimary != value; 
    			_phonePrimary = value; 
    			if (isChanged) OnPropertyChanged("PhonePrimary");
    		}
        }
        private string _phonePrimary;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string Fax
        {
            get { return _fax; }
            set 
    		{ 
    			var isChanged = _fax != value; 
    			_fax = value; 
    			if (isChanged) OnPropertyChanged("Fax");
    		}
        }
        private string _fax;
    
        
        [StringLength(80, ErrorMessage="This field cannot be longer than 80 characters")]
    	public virtual string Email
        {
            get { return _email; }
            set 
    		{ 
    			var isChanged = _email != value; 
    			_email = value; 
    			if (isChanged) OnPropertyChanged("Email");
    		}
        }
        private string _email;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhoneAlt1
        {
            get { return _phoneAlt1; }
            set 
    		{ 
    			var isChanged = _phoneAlt1 != value; 
    			_phoneAlt1 = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt1");
    		}
        }
        private string _phoneAlt1;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string PhoneAlt1Qualifier
        {
            get { return _phoneAlt1Qualifier; }
            set 
    		{ 
    			var isChanged = _phoneAlt1Qualifier != value; 
    			_phoneAlt1Qualifier = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt1Qualifier");
    		}
        }
        private string _phoneAlt1Qualifier;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhoneAlt2
        {
            get { return _phoneAlt2; }
            set 
    		{ 
    			var isChanged = _phoneAlt2 != value; 
    			_phoneAlt2 = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt2");
    		}
        }
        private string _phoneAlt2;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string PhoneAlt2Qualifier
        {
            get { return _phoneAlt2Qualifier; }
            set 
    		{ 
    			var isChanged = _phoneAlt2Qualifier != value; 
    			_phoneAlt2Qualifier = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt2Qualifier");
    		}
        }
        private string _phoneAlt2Qualifier;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhoneAlt3
        {
            get { return _phoneAlt3; }
            set 
    		{ 
    			var isChanged = _phoneAlt3 != value; 
    			_phoneAlt3 = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt3");
    		}
        }
        private string _phoneAlt3;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string PhoneAlt3Qualifier
        {
            get { return _phoneAlt3Qualifier; }
            set 
    		{ 
    			var isChanged = _phoneAlt3Qualifier != value; 
    			_phoneAlt3Qualifier = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt3Qualifier");
    		}
        }
        private string _phoneAlt3Qualifier;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhoneAlt4
        {
            get { return _phoneAlt4; }
            set 
    		{ 
    			var isChanged = _phoneAlt4 != value; 
    			_phoneAlt4 = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt4");
    		}
        }
        private string _phoneAlt4;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string PhoneAlt4Qualifier
        {
            get { return _phoneAlt4Qualifier; }
            set 
    		{ 
    			var isChanged = _phoneAlt4Qualifier != value; 
    			_phoneAlt4Qualifier = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt4Qualifier");
    		}
        }
        private string _phoneAlt4Qualifier;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string PhoneAlt5
        {
            get { return _phoneAlt5; }
            set 
    		{ 
    			var isChanged = _phoneAlt5 != value; 
    			_phoneAlt5 = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt5");
    		}
        }
        private string _phoneAlt5;
    
        
        [StringLength(3, ErrorMessage="This field cannot be longer than 3 characters")]
    	public virtual string PhoneAlt5Qualifier
        {
            get { return _phoneAlt5Qualifier; }
            set 
    		{ 
    			var isChanged = _phoneAlt5Qualifier != value; 
    			_phoneAlt5Qualifier = value; 
    			if (isChanged) OnPropertyChanged("PhoneAlt5Qualifier");
    		}
        }
        private string _phoneAlt5Qualifier;
    
        
        [StringLength(22, ErrorMessage="This field cannot be longer than 22 characters")]
    	public virtual string ActiveStartTime
        {
            get { return _activeStartTime; }
            set 
    		{ 
    			var isChanged = _activeStartTime != value; 
    			_activeStartTime = value; 
    			if (isChanged) OnPropertyChanged("ActiveStartTime");
    		}
        }
        private string _activeStartTime;
    
        
        [StringLength(22, ErrorMessage="This field cannot be longer than 22 characters")]
    	public virtual string ActiveEndTime
        {
            get { return _activeEndTime; }
            set 
    		{ 
    			var isChanged = _activeEndTime != value; 
    			_activeEndTime = value; 
    			if (isChanged) OnPropertyChanged("ActiveEndTime");
    		}
        }
        private string _activeEndTime;
    
        
        [StringLength(5, ErrorMessage="This field cannot be longer than 5 characters")]
    	public virtual string ServiceLevel
        {
            get { return _serviceLevel; }
            set 
    		{ 
    			var isChanged = _serviceLevel != value; 
    			_serviceLevel = value; 
    			if (isChanged) OnPropertyChanged("ServiceLevel");
    		}
        }
        private string _serviceLevel;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string PartnerAccount
        {
            get { return _partnerAccount; }
            set 
    		{ 
    			var isChanged = _partnerAccount != value; 
    			_partnerAccount = value; 
    			if (isChanged) OnPropertyChanged("PartnerAccount");
    		}
        }
        private string _partnerAccount;
    
        
        [StringLength(22, ErrorMessage="This field cannot be longer than 22 characters")]
    	public virtual string LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set 
    		{ 
    			var isChanged = _lastModifiedDate != value; 
    			_lastModifiedDate = value; 
    			if (isChanged) OnPropertyChanged("LastModifiedDate");
    		}
        }
        private string _lastModifiedDate;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string TwentyFourHourFlag
        {
            get { return _twentyFourHourFlag; }
            set 
    		{ 
    			var isChanged = _twentyFourHourFlag != value; 
    			_twentyFourHourFlag = value; 
    			if (isChanged) OnPropertyChanged("TwentyFourHourFlag");
    		}
        }
        private string _twentyFourHourFlag;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string CrossStreet
        {
            get { return _crossStreet; }
            set 
    		{ 
    			var isChanged = _crossStreet != value; 
    			_crossStreet = value; 
    			if (isChanged) OnPropertyChanged("CrossStreet");
    		}
        }
        private string _crossStreet;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string RecordChange
        {
            get { return _recordChange; }
            set 
    		{ 
    			var isChanged = _recordChange != value; 
    			_recordChange = value; 
    			if (isChanged) OnPropertyChanged("RecordChange");
    		}
        }
        private string _recordChange;
    
        
        [StringLength(5, ErrorMessage="This field cannot be longer than 5 characters")]
    	public virtual string OldServiceLevel
        {
            get { return _oldServiceLevel; }
            set 
    		{ 
    			var isChanged = _oldServiceLevel != value; 
    			_oldServiceLevel = value; 
    			if (isChanged) OnPropertyChanged("OldServiceLevel");
    		}
        }
        private string _oldServiceLevel;
    
        
        [StringLength(100, ErrorMessage="This field cannot be longer than 100 characters")]
    	public virtual string TextServiceLevel
        {
            get { return _textServiceLevel; }
            set 
    		{ 
    			var isChanged = _textServiceLevel != value; 
    			_textServiceLevel = value; 
    			if (isChanged) OnPropertyChanged("TextServiceLevel");
    		}
        }
        private string _textServiceLevel;
    
        
        [StringLength(100, ErrorMessage="This field cannot be longer than 100 characters")]
    	public virtual string TextServiceLevelChange
        {
            get { return _textServiceLevelChange; }
            set 
    		{ 
    			var isChanged = _textServiceLevelChange != value; 
    			_textServiceLevelChange = value; 
    			if (isChanged) OnPropertyChanged("TextServiceLevelChange");
    		}
        }
        private string _textServiceLevelChange;
    
        
        [StringLength(5, ErrorMessage="This field cannot be longer than 5 characters")]
    	public virtual string Version
        {
            get { return _version; }
            set 
    		{ 
    			var isChanged = _version != value; 
    			_version = value; 
    			if (isChanged) OnPropertyChanged("Version");
    		}
        }
        private string _version;
    
        
        [StringLength(10, ErrorMessage="This field cannot be longer than 10 characters")]
    	public virtual string NPI
        {
            get { return _nPI; }
            set 
    		{ 
    			var isChanged = _nPI != value; 
    			_nPI = value; 
    			if (isChanged) OnPropertyChanged("NPI");
    		}
        }
        private string _nPI;
    
        [Required]
    	public virtual System.Guid msrepl_tran_version
        {
            get { return _msrepl_tran_version; }
            set 
    		{ 
    			var isChanged = _msrepl_tran_version != value; 
    			_msrepl_tran_version = value; 
    			if (isChanged) OnPropertyChanged("msrepl_tran_version");
    		}
        }
        private System.Guid _msrepl_tran_version;

        #endregion

    }
}
