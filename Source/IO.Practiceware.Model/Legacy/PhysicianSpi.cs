//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PhysicianSpi : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int PhysicianSpiId
        {
            get { return _physicianSpiId; }
            set 
    		{ 
    			var isChanged = _physicianSpiId != value; 
    			_physicianSpiId = value; 
    			if (isChanged) OnPropertyChanged("PhysicianSpiId");
    		}
        }
        private int _physicianSpiId;
    
        [Required]
    	public virtual int PhysicianId
        {
            get { return _physicianId; }
            set 
    		{ 
    			var isChanged = _physicianId != value; 
    			_physicianId = value; 
    			if (isChanged) OnPropertyChanged("PhysicianId");
    		}
        }
        private int _physicianId;
    
    	public virtual Nullable<int> LocationId
        {
            get { return _locationId; }
            set 
    		{ 
    			var isChanged = _locationId != value; 
    			_locationId = value; 
    			if (isChanged) OnPropertyChanged("LocationId");
    		}
        }
        private Nullable<int> _locationId;
    
        
        [StringLength(35, ErrorMessage="This field cannot be longer than 35 characters")]
    	public virtual string Spi
        {
            get { return _spi; }
            set 
    		{ 
    			var isChanged = _spi != value; 
    			_spi = value; 
    			if (isChanged) OnPropertyChanged("Spi");
    		}
        }
        private string _spi;
    
        
        [StringLength(25, ErrorMessage="This field cannot be longer than 25 characters")]
    	public virtual string Phone
        {
            get { return _phone; }
            set 
    		{ 
    			var isChanged = _phone != value; 
    			_phone = value; 
    			if (isChanged) OnPropertyChanged("Phone");
    		}
        }
        private string _phone;
    
        [Required]
    	public virtual System.Guid msrepl_tran_version
        {
            get { return _msrepl_tran_version; }
            set 
    		{ 
    			var isChanged = _msrepl_tran_version != value; 
    			_msrepl_tran_version = value; 
    			if (isChanged) OnPropertyChanged("msrepl_tran_version");
    		}
        }
        private System.Guid _msrepl_tran_version;
    
    	public virtual Nullable<int> PhoneType
        {
            get { return _phoneType; }
            set 
    		{ 
    			var isChanged = _phoneType != value; 
    			_phoneType = value; 
    			if (isChanged) OnPropertyChanged("PhoneType");
    		}
        }
        private Nullable<int> _phoneType;

        #endregion

    }
}
