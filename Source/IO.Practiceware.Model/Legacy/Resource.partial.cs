﻿using System.ComponentModel;

namespace IO.Practiceware.Model.Legacy
{
    /// <summary>
    /// Corresponds to ServiceCode field.
    /// </summary>
    public enum ResourceServiceCode
    {
        [Description("01")]
        Room,
        [Description("02")]
        Facility,
    }

    /// <summary>
    /// Values for the ResourceType field.
    /// </summary>
    public enum ResourceType
    {
        [Description("A")]
        Administrative,
        [Description("D")]
        Doctor,
        [Description("O")]
        Other,
        [Description("Q")]
        OpticalPrescriber,
        [Description("R")]
        RoomOrFacility,
        [Description("U")]
        OpticalUser,
        [Description("T")]
        Technician,
        [Description("Z")]
        Archived
    }
}