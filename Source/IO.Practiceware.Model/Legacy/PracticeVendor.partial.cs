﻿using System.ComponentModel;

namespace IO.Practiceware.Model.Legacy
{
    /// <summary>
    /// Corresponds to PracticeVendor.VendorType field.
    /// </summary>
    public enum PracticeVendorType
    {
        [Description("~")]
        Contact,
        [Description("D")]
        Doctor,
        [Description("H")]
        Hospital,
        [Description("L")]
        Laboratory
    }
}
