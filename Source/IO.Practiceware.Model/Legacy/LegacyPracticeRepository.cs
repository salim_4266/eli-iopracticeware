﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soaf;
using Soaf.Collections;

namespace IO.Practiceware.Model.Legacy
{
    public partial class LegacyPracticeRepository
    {

        private static readonly Dictionary<Type, string> TypeToDbObjectNameMap = typeof(ILegacyPracticeRepository)
            .GetProperties()
            .Where(p => p.PropertyType.IsGenericTypeFor(typeof(IQueryable<>)))
            .ToDictionary(p => p.PropertyType.GetGenericArguments()[0], p => "dbo." + p.Name);

        private static readonly Dictionary<string, Type> DbObjectNameToTypeMap =
            TypeToDbObjectNameMap
            .ToDictionary(i => i.Value, i => i.Key);
        
        public static Type GetEntityType(string objectName)
        {
            return DbObjectNameToTypeMap.GetValue(objectName);
        }

        public static string GetDbObjectName(Type entityType)
        {
            return TypeToDbObjectNameMap.GetValue(entityType);
        }

    }

    /// <summary>
    ///   Queries against PatientDemographics.
    /// </summary>
    public static class PatientDemographicQueries
    {
        /// <summary>
        ///   Returns a dictionary of patients by prior patient code.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static IDictionary<string, int> ByPriorPatientCode(this IQueryable<PatientDemographic> source)
        {
            return source.Where(i => i.OldPatient != null && i.OldPatient != string.Empty)
                .GroupBy(i => i.OldPatient)
                .Select(i => new {i.Key, i.FirstOrDefault().PatientId})
                .ToDictionary(i => i.Key, i => i.PatientId);
        }
    }
}