﻿using System.ComponentModel;

namespace IO.Practiceware.Model.Legacy
{
    /// <summary>
    /// Corresponds to PracticeName.PracticeType field.
    /// </summary>
    public enum PracticeNameType
    {
        [Description("P")] BillingPractice,
        [Description("T")] DailyScheduleTemplate,
        [Description("W")] WeeklyScheduleTemplate,
        [Description("S")] SuperScheduleTemplate
    }
}