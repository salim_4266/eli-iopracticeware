//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PracticePayable : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int PayableId
        {
            get { return _payableId; }
            set 
    		{ 
    			var isChanged = _payableId != value; 
    			_payableId = value; 
    			if (isChanged) OnPropertyChanged("PayableId");
    		}
        }
        private int _payableId;
    
        
        [StringLength(16, ErrorMessage="This field cannot be longer than 16 characters")]
    	public virtual string PayableInvoice
        {
            get { return _payableInvoice; }
            set 
    		{ 
    			var isChanged = _payableInvoice != value; 
    			_payableInvoice = value; 
    			if (isChanged) OnPropertyChanged("PayableInvoice");
    		}
        }
        private string _payableInvoice;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string PayableType
        {
            get { return _payableType; }
            set 
    		{ 
    			var isChanged = _payableType != value; 
    			_payableType = value; 
    			if (isChanged) OnPropertyChanged("PayableType");
    		}
        }
        private string _payableType;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string PayablePayerType
        {
            get { return _payablePayerType; }
            set 
    		{ 
    			var isChanged = _payablePayerType != value; 
    			_payablePayerType = value; 
    			if (isChanged) OnPropertyChanged("PayablePayerType");
    		}
        }
        private string _payablePayerType;
    
        
        [StringLength(64, ErrorMessage="This field cannot be longer than 64 characters")]
    	public virtual string PayableDescription
        {
            get { return _payableDescription; }
            set 
    		{ 
    			var isChanged = _payableDescription != value; 
    			_payableDescription = value; 
    			if (isChanged) OnPropertyChanged("PayableDescription");
    		}
        }
        private string _payableDescription;
    
    	public virtual Nullable<float> PayableAmount
        {
            get { return _payableAmount; }
            set 
    		{ 
    			var isChanged = _payableAmount != value; 
    			_payableAmount = value; 
    			if (isChanged) OnPropertyChanged("PayableAmount");
    		}
        }
        private Nullable<float> _payableAmount;
    
        
        [StringLength(10, ErrorMessage="This field cannot be longer than 10 characters")]
    	public virtual string PayableDate
        {
            get { return _payableDate; }
            set 
    		{ 
    			var isChanged = _payableDate != value; 
    			_payableDate = value; 
    			if (isChanged) OnPropertyChanged("PayableDate");
    		}
        }
        private string _payableDate;
    
    	public virtual Nullable<int> PayableVendorId
        {
            get { return _payableVendorId; }
            set 
    		{ 
    			var isChanged = _payableVendorId != value; 
    			_payableVendorId = value; 
    			if (isChanged) OnPropertyChanged("PayableVendorId");
    		}
        }
        private Nullable<int> _payableVendorId;
    
        
        [StringLength(32, ErrorMessage="This field cannot be longer than 32 characters")]
    	public virtual string APAcct
        {
            get { return _aPAcct; }
            set 
    		{ 
    			var isChanged = _aPAcct != value; 
    			_aPAcct = value; 
    			if (isChanged) OnPropertyChanged("APAcct");
    		}
        }
        private string _aPAcct;
    
    	public virtual Nullable<float> PayablePaidAmount
        {
            get { return _payablePaidAmount; }
            set 
    		{ 
    			var isChanged = _payablePaidAmount != value; 
    			_payablePaidAmount = value; 
    			if (isChanged) OnPropertyChanged("PayablePaidAmount");
    		}
        }
        private Nullable<float> _payablePaidAmount;
    
        
        [StringLength(10, ErrorMessage="This field cannot be longer than 10 characters")]
    	public virtual string PayablePaidDate
        {
            get { return _payablePaidDate; }
            set 
    		{ 
    			var isChanged = _payablePaidDate != value; 
    			_payablePaidDate = value; 
    			if (isChanged) OnPropertyChanged("PayablePaidDate");
    		}
        }
        private string _payablePaidDate;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string Status
        {
            get { return _status; }
            set 
    		{ 
    			var isChanged = _status != value; 
    			_status = value; 
    			if (isChanged) OnPropertyChanged("Status");
    		}
        }
        private string _status;

        #endregion

    }
}
