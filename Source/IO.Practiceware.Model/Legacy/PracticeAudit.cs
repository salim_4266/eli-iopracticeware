//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PracticeAudit : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int AuditId
        {
            get { return _auditId; }
            set 
    		{ 
    			var isChanged = _auditId != value; 
    			_auditId = value; 
    			if (isChanged) OnPropertyChanged("AuditId");
    		}
        }
        private int _auditId;
    
        
        [StringLength(8, ErrorMessage="This field cannot be longer than 8 characters")]
    	public virtual string Date
        {
            get { return _date; }
            set 
    		{ 
    			var isChanged = _date != value; 
    			_date = value; 
    			if (isChanged) OnPropertyChanged("Date");
    		}
        }
        private string _date;
    
    	public virtual Nullable<int> Time
        {
            get { return _time; }
            set 
    		{ 
    			var isChanged = _time != value; 
    			_time = value; 
    			if (isChanged) OnPropertyChanged("Time");
    		}
        }
        private Nullable<int> _time;
    
    	public virtual Nullable<int> UserId
        {
            get { return _userId; }
            set 
    		{ 
    			var isChanged = _userId != value; 
    			_userId = value; 
    			if (isChanged) OnPropertyChanged("UserId");
    		}
        }
        private Nullable<int> _userId;
    
    	public virtual Nullable<short> TableId
        {
            get { return _tableId; }
            set 
    		{ 
    			var isChanged = _tableId != value; 
    			_tableId = value; 
    			if (isChanged) OnPropertyChanged("TableId");
    		}
        }
        private Nullable<short> _tableId;
    
    	public virtual Nullable<int> RecordId
        {
            get { return _recordId; }
            set 
    		{ 
    			var isChanged = _recordId != value; 
    			_recordId = value; 
    			if (isChanged) OnPropertyChanged("RecordId");
    		}
        }
        private Nullable<int> _recordId;
    
        
        [StringLength(128, ErrorMessage="This field cannot be longer than 128 characters")]
    	public virtual string PrevRec
        {
            get { return _prevRec; }
            set 
    		{ 
    			var isChanged = _prevRec != value; 
    			_prevRec = value; 
    			if (isChanged) OnPropertyChanged("PrevRec");
    		}
        }
        private string _prevRec;
    
        
        [StringLength(128, ErrorMessage="This field cannot be longer than 128 characters")]
    	public virtual string CurrentRec
        {
            get { return _currentRec; }
            set 
    		{ 
    			var isChanged = _currentRec != value; 
    			_currentRec = value; 
    			if (isChanged) OnPropertyChanged("CurrentRec");
    		}
        }
        private string _currentRec;
    
        
        [StringLength(1, ErrorMessage="This field cannot be longer than 1 characters")]
    	public virtual string Action
        {
            get { return _action; }
            set 
    		{ 
    			var isChanged = _action != value; 
    			_action = value; 
    			if (isChanged) OnPropertyChanged("Action");
    		}
        }
        private string _action;

        #endregion

    }
}
