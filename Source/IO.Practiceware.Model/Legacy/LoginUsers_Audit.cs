//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model.Legacy
{
    public partial class LoginUsers_Audit : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int ID
        {
            get { return _iD; }
            set 
    		{ 
    			var isChanged = _iD != value; 
    			_iD = value; 
    			if (isChanged) OnPropertyChanged("ID");
    		}
        }
        private int _iD;
    
        [Key]
    	public virtual int LoginId
        {
            get { return _loginId; }
            set 
    		{ 
    			var isChanged = _loginId != value; 
    			_loginId = value; 
    			if (isChanged) OnPropertyChanged("LoginId");
    		}
        }
        private int _loginId;
    
        [Key]
    	public virtual System.DateTime TimeStamp
        {
            get { return _timeStamp; }
            set 
    		{ 
    			var isChanged = _timeStamp != value; 
    			_timeStamp = value; 
    			if (isChanged) OnPropertyChanged("TimeStamp");
    		}
        }
        private System.DateTime _timeStamp;
    
        
        [StringLength(1000, ErrorMessage="This field cannot be longer than 1000 characters")]
        [Key]
    	public virtual string ActionTaken
        {
            get { return _actionTaken; }
            set 
    		{ 
    			var isChanged = _actionTaken != value; 
    			_actionTaken = value; 
    			if (isChanged) OnPropertyChanged("ActionTaken");
    		}
        }
        private string _actionTaken;
    
    	public virtual Nullable<int> EncounterId
        {
            get { return _encounterId; }
            set 
    		{ 
    			var isChanged = _encounterId != value; 
    			_encounterId = value; 
    			if (isChanged) OnPropertyChanged("EncounterId");
    		}
        }
        private Nullable<int> _encounterId;

        #endregion

    }
}
