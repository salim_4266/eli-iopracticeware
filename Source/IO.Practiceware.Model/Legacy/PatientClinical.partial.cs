﻿using System.ComponentModel;

namespace IO.Practiceware.Model.Legacy
{
    public partial class PatientClinical
    {
        public const string SpectaclePrescriptionIndicator = "DISPENSE SPECTACLE RX";
        public const string ContactLensesPrescriptionIndicator = "DISPENSE CL RX";
    }

    /// <summary>
    ///   Correspond to ClinicalStatus field.
    /// </summary>
    public enum PatientClinicalStatus
    {
        [Description("D")] Deleted,
        [Description("A")] Active,
        [Description("X")] ActiveWithoutBilling
    }

    /// <summary>
    ///   Corresponds to ClinicalType field.
    /// </summary>
    public enum PatientClinicalType
    {
        [Description("A")] Action,
        [Description("B")] Billing,
        [Description("C")] PatientHistory,
        [Description("F")] Test,
        [Description("H")] ClinicalHistory,
        [Description("I")] Image,
        [Description("K")] Diagnosis,
        [Description("P")] Procedure,
        [Description("Q")] Finding,
        [Description("R")] InactiveFinding,
        [Description("U")] Impression,
        [Description("Y")] ReviewOfSystems,
        [Description("Z")] OfficeVisit
    }
}