﻿
namespace IO.Practiceware.Model
{
    public enum InsurerBusinessClassId
    {
        Amerihealth = 1,
        BlueCrossBlueShield = 2,
        Commercial = 3,
        MedicaidFL = 4,
        MedicaidNC = 5,
        MedicaidNJ = 6,
        MedicaidNV = 7,
        MedicaidNY = 8,
        MedicareNJ = 9,
        MedicareNV = 10,
        MedicareNY = 11,
        Template = 12,
    }
}
