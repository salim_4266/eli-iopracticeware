﻿using System.ComponentModel.DataAnnotations;
namespace IO.Practiceware.Model
{
    public enum BodySystem
    {
        [Display(Name = "Constitutional Symptoms")]
        ConstitutionalSymptoms = 1,
        Eyes = 2,
        [Display(Name = "Ears, Nose, Mouth, Throat")]
        EarsNoseMouthThroat = 3,
        Cardiovascular = 4,
        Respiratory = 5,
        Gastrointestinal = 6,
        Genitourinary = 7,
        Musculoskeletal = 8,
        Integumentary = 9,
        Neurological = 10,
        Psychiatric = 11,
        Endocrine = 12,
        [Display(Name = "Hematologic/Lymphatic")]
        HematologicLymphatic = 13,
        [Display(Name = "Allergic/Immunologic")]
        AllergicImmunologic = 14,
    }
}