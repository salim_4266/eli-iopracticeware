﻿
namespace IO.Practiceware.Model
{  
    public enum PersonNameSuffix
    {
        Jr,

        Sr,

        I,

        II,

        III,

        IV,
    }
}
