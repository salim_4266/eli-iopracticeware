﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class FamilyRelationship
    {
    }

    public enum FamilyRelationshipId
    {
        Aunt = 1,
        Brother = 2,
        Child = 3,
        Cousin = 4,
        Father = 5,
        Grandfather = 6,
        Grandmother = 7,
        Grandparent = 8,
        Greataunt = 9,
        Greatuncle = 10,
        Mother = 11,
        Nephew = 12,
        Niece = 13,
        Other = 14,
        Parent = 15,
        Sibling = 16,
        Sister = 17,
        Uncle = 18,
        Son = 19,
        Daughter = 20,
        Spouse=21
    }
}
