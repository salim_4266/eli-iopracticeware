﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class ClinicalCondition
    {
    }

    public partial class PatientDiagnosisDetail
    {
        public ClinicalConditionStatus ClinicalConditionStatus
        {
            get { return (ClinicalConditionStatus) ClinicalConditionStatusId; }
            set { ClinicalConditionStatusId = (int) value; }
        }
    }

    public partial class PatientDiagnosisComment
    {
        public ClinicalConditionStatus ClinicalConditionStatus
        {
            get { return (ClinicalConditionStatus) ClinicalConditionStatusId; }
            set { ClinicalConditionStatusId = (int) value; }
        }
    }

    public partial class PatientAllergenDetail
    {
        public ClinicalConditionStatus ClinicalConditionStatus
        {
            get { return (ClinicalConditionStatus) ClinicalConditionStatusId; }
            set { ClinicalConditionStatusId = (int) value; }
        }
    }

    public partial class PatientAllergenComment
    {
        public ClinicalConditionStatus ClinicalConditionStatus
        {
            get { return (ClinicalConditionStatus) ClinicalConditionStatusId; }
            set { ClinicalConditionStatusId = (int) value; }
        }
    }

    public enum ClinicalConditionStatus
    {
        Active = 1,
        Inactive = 2,
        Resolved = 3
    }
}
