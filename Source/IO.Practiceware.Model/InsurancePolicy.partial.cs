﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace IO.Practiceware.Model
{
    public partial class InsurancePolicy
    {
        public MedicareSecondaryReasonCode? MedicareSecondaryReasonCode
        {
            get { return MedicareSecondaryReasonCodeId.HasValue ? (MedicareSecondaryReasonCode)MedicareSecondaryReasonCodeId : default(MedicareSecondaryReasonCode?); }
            set { MedicareSecondaryReasonCodeId = value.HasValue ? (int)value : default(int?); }
        }



        public PatientInsurance PatientInsuranceForPolicyholder
        {
            get { return PatientInsurances.FirstOrDefault(pi => pi.InsuredPatientId == PolicyholderPatientId); }
        }
    }

    /// <summary>
    /// Helper methods for insurance policies.
    /// </summary>
    public static class InsurancePolicies
    {
        /// <summary>
        /// Determines whether the policy is active for the specified date.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="dateTime">The date.</param>
        /// <returns>
        ///   <c>true</c> if [is active for] [the specified date]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsActiveForDateTime(this PatientInsurance source, DateTime dateTime)
        {
            return source.InsurancePolicy.StartDateTime.Date <= dateTime.Date
                && (source.EndDateTime == null || source.EndDateTime.Value.Date >= dateTime.Date);
        }

        /// <summary>
        /// Validate the policy code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool IsValidPolicyCode(this string code)
        {
            var policyCodeExpression = new Regex(@"^[a-zA-Z0-9]*$");
            return policyCodeExpression.IsMatch(code);
        }
    }

    public enum MedicareSecondaryReasonCode
    {
        [Display(Name = "Medicare Secondary Working Aged Beneficiary or Spouse with Employer Group Health Plan")]
        [Description("Working Aged")]
        WorkingAged = 1,

        [Display(Name = "Medicare Secondary End-Stage Renal Disease Beneficiary in the 12 Month coordination period with an employer's group health plan")]
        [Description("ESRD")]
        EndStageRenalDisease = 2,

        [Display(Name = "Medicare Secondary, No-Fault Insurance including Auto is Primary")]
        [Description("Auto / No Fault")]
        NoFault = 3,

        [Display(Name = "Medicare Secondary Worker's Compensation")]
        [Description("Workers Comp")]
        WorkersCompensation = 4,

        [Display(Name = "Medicare Secondary Public Health Service (PHS) or Other Federal Agency")]
        [Description("Public Health Service")]
        PublicHealthService = 5,

        [Display(Name = "Medicare Secondary Black Lung")]
        [Description("Black Lung")]
        BlackLung = 6,

        [Display(Name = "Medicare Secondary Veteran's Administration")]
        [Description("VA")]
        VeteransAdministration = 7,

        [Display(Name = "Medicare Secondary Disabled Beneficiary Under Age 65 with Large Group Health Plan (LGHP)")]
        [Description("LGHP")]
        LargeGroupHealthPlan = 8,

        [Display(Name = "Medicare Secondary, Other Liability insurance is primary")]
        [Description("Liability")]
        OtherLiabilityInsurance = 9
    }
}