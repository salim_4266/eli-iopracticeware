﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Soaf;
using Soaf.Reflection;

namespace IO.Practiceware.Model
{
    public interface IEmailAddress
    {
        int Id { get; set; }
        int OrdinalId { get; set; }
        EmailAddressType EmailAddressType { get; set; }
        String Value { get; set; }
    }

    public partial class EmailAddress : IEmailAddress
    {
        public EmailAddressType EmailAddressType
        {
            get { return (EmailAddressType)EmailAddressTypeId; }
            set { EmailAddressTypeId = (int)value; }
        }
    }

    [DisplayName("EmailAddress")]
    [MetadataType(typeof(IEmailAddress))]
    public partial class ExternalOrganizationEmailAddress : IEmailAddress
    {
        public EmailAddressType EmailAddressType
        {
            get { return (EmailAddressType)EmailAddressTypeId; }
            set { EmailAddressTypeId = (int)value; }
        }
    }

    [DisplayName("EmailAddress")]
    [MetadataType(typeof(IEmailAddress))]
    public partial class ExternalContactEmailAddress : IEmailAddress, IHasOrdinalId
    {
        public ExternalContactEmailAddress()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => ExternalContact.IfNotNull(p => p.ExternalContactEmailAddresses),
               Reflector.GetMember(() => ExternalContact).Name);
        }
        public EmailAddressType EmailAddressType
        {
            get { return (EmailAddressType)EmailAddressTypeId; }
            set { EmailAddressTypeId = (int)value; }
        }
    }

    public enum EmailAddressType
    {
        [Display(Name ="Personal")]
        Personal = 1,
        [Display(Name = "Business")]
        Business = 2
    }
}
