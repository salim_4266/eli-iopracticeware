﻿namespace IO.Practiceware.Model
{
    public partial class PatientHeightAndWeight
    {
        public MeasurementSystem MeasurementSystem
        {
            get { return (MeasurementSystem) MeasurementSystemId; }
            set { MeasurementSystemId = (int) value; }
        }

        public decimal HeightUnitInMeters
        {
            get { return HeightUnit.GetValueOrDefault()/1000; }
        }

        public decimal WeightUnitInKilograms
        {
            get { return WeightUnit.GetValueOrDefault()/1000; }
        }
    }

    public enum MeasurementSystem
    {
        Metric = 1,
        UsCustomary = 2
    }
}