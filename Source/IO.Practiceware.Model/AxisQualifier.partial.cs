﻿namespace IO.Practiceware.Model
{
    public partial class AxisQualifier
    {
    }

    /// <summary>
    /// Well known axis qualifiers enumeration
    /// </summary>
    public enum AxisQualifierId
    {
        ConfirmationOfNegativeFindings = 1,
        StatusPostSurgery = 2,
        HistoryOf = 3,
        RuleOut = 4
    }
}
