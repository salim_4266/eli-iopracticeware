﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// This class represents data that will be stored as Xml in the ApplicationSettings table.  The Application Settings table can contain 
    /// many different types of Configuration Settings.  This is one of those types.  Particularly this class represents check-in/out terms
    /// settings on a global level for H17 messages processing.
    /// </summary>
    [DataContract]
    public class AppointmentMessageHandlerSettings : IApplicationSetting
    {
        [DataMember]
        public virtual List<string> CheckInTerms { get; set; }

        [DataMember]
        public virtual List<string> CheckOutTerms { get; set; }
    }
}
