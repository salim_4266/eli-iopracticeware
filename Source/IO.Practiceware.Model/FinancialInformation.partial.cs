﻿
namespace IO.Practiceware.Model
{

    public partial class FinancialInformation : IAdjustmentOrFinancialInformation
    {
        public FinancialSourceType FinancialSourceType
        {
            get { return (FinancialSourceType) FinancialSourceTypeId; }
            set { FinancialSourceTypeId = (int)value; }
        }

        public ClaimAdjustmentGroupCode? ClaimAdjustmentGroupCode
        {
            get
            {
                if (ClaimAdjustmentGroupCodeId.HasValue)
                {
                    var groupCodeId = ClaimAdjustmentGroupCodeId.Value;
                    return (ClaimAdjustmentGroupCode) groupCodeId;
                }
                return null;
            }
        }
    }
}