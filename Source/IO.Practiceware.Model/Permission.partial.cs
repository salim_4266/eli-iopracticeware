﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Soaf;
using Soaf.Security;
using Soaf.Validation;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Well known permissions
    /// </summary>
    [PrincipalContextHasPermission]
    public enum PermissionId
    {
        [Display(Name = "Schedule appointments", GroupName = "SchedulingAndPatientAppointments", Order = 3)]
        ScheduleAppointment = 1,

        [Display(Name = "View provider schedules", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ViewSchedule = 2,

        [Display(Name = "Set up appointment type categories", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ConfigureAppointmentCategories = 3,

        [Display(Name = "Insert or delete categories in a schedule", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        InsertDeleteCategoriesInSchedule = 4,

        [Display(Name = "Set up schedule templates", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        CreateScheduleTemplates = 5,

        [Display(Name = "Apply templates to provider schedules", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ApplyTemplatesToSchedules = 6,

        [Display(Name = "Cancel appointments", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        CancelAppointment = 7,

        [Display(Name = "Reschedule appointments", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        RescheduleAppointment = 8,

        [Display(Name = "Add global and provider schedule comments", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        CreateGlobalAndResourceScheduleComments = 9,

        [Display(Name = "Force appointments into schedule", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ForceScheduleAppointment = 10,

        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), "Cannot reschedule appointments that have been checked in.")]
        [Display(Name = "Change provider on discharged or arrived appointments", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ChangeResourceOnDischargedOrArrivedAppointments = 11,

        [Display(Name = "Submit Claims", GroupName = "PatientFinancials", Order = 1)]
        GenerateClaimsAndStatements = 12,

        [Display(Name = "Cancel appointment in exam room", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        CancelAppointmentInExamRoom = 13,

        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), "Add insurance policies where the patient is the policyholder permission is not set.")]
        [Display(Name = "Add insurance policies where the patient is the policyholder", GroupName = "PatientInsurance", Order = 1)]
        AddPatientInsuranceForSelf = 16,

        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), "Add insurance policies where the patient is not the policyholder permission is not set.")]
        [Display(Name = "Add insurance policies where patient is not the policyholder", GroupName = "PatientInsurance", Order = 1)]
        AddPatientInsuranceForOther = 17,

        [Display(Name = "Edit insurance policies where patient is the policyholder", GroupName = "PatientInsurance", Order = 1)]
        EditPatientInsuranceForSelf = 18,

        [Display(Name = "Edit insurance policies where patient is not the policyholder", GroupName = "PatientInsurance", Order = 1)]
        EditPatientInsuranceForOther = 19,

        [Display(Name = "Delete patient insurance policies", GroupName = "PatientInsurance", Order = 1)]
        DeletePatientInsurance = 20,

        [Display(Name = "Delete patient scanned insurance cards", GroupName = "PatientInsurance", Order = 1)]
        DeletePatientInsuranceDocument = 21,

        [Display(Name = "View insurer set up screens", GroupName = "Insurers", Order = 1)]
        ViewInsurersAndPlans = 22,

        [Display(Name = "Add insurance policyholders", GroupName = "PatientInsurance", Order = 1)]
        CreatePolicyholder = 23,

        [Display(Name = "Edit Insurers", GroupName = "Insurers", Order = 1)]
        EditInsurancePlanInformation = 24,

        [Display(Name = "Manage insurance plan patient payments", GroupName = "Insurers", Order = 1)]
        ManageInsurancePlanPatientPayments = 25,

        [Display(Name = "Reactivate Insurers", GroupName = "Insurers", Order = 1)]
        ReactivateInsurancePlans = 26,

        [Display(Name = "Add insurers", GroupName = "Insurers", Order = 1)]
        CreateInsurancePlans = 27,

        [Display(Name = "Merge insurers", GroupName = "Insurers", Order = 1)]
        MergeInsurancePlans = 28,

        [Display(Name = "Insurer group update", GroupName = "Insurers", Order = 1)]
        GroupUpdateOfInsurancePlans = 29,

        [Display(Name = "Archive Insurers", GroupName = "Insurers", Order = 1)]
        ArchiveInsurancePlans = 30,

        [Display(Name = "Delete Insurers", GroupName = "Insurers", Order = 1)]
        DeleteInsurancePlans = 31,

        [Display(Name = "View patient appointments tab", GroupName = "SchedulingAndPatientAppointments", Order = 1)]
        ViewPatientAppointmentSummary = 32,

        [Display(Name = "Set up patient demographics tab", GroupName = "PatientDemographics", Order = 3)]
        AdministerPatientDemographicsFields = 33,

        [Display(Name = "Set up patient tags", GroupName = "PatientDemographics", Order = 4)]
        AdministerPatientTags = 34,

        [Display(Name = "Edit insurance policyholders", GroupName = "PatientInsurance", Order = 1)]
        EditPolicyholder = 35,

        [Display(Name = "Manage clinical decision support alerts", GroupName = "PatientClinical", Order = 1)]
        ClinicalDecisions = 36,

        [Display(Name = "Reconcile clinical information", GroupName = "PatientClinical", Order = 1)]
        CanReconcileClinical = 37,

        [Display(Name = "Export clinical information", GroupName = "PatientClinical", Order = 1)]
        CanExportClinical = 38,

        [Display(Name = "Edit patient problem list", GroupName = "PatientClinical", Order = 1)]
        CanEditProblemList = 39,

        [Display(Name = "View Patient problem list", GroupName = "PatientClinical", Order = 1)]
        CanViewProblemList = 40,

        [Display(Name = "Import clinical information", GroupName = "PatientClinical", Order = 1)]
        CanImportClinical = 41,

        [Display(Name = "View patient demographics tab", GroupName = "PatientDemographics", Order = 1)]
        ViewPatientDemographics = 42,

        [Display(Name = "Edit patient demographics tab", GroupName = "PatientDemographics", Order = 2)]
        EditPatientDemographics = 43,

        [Display(Name = "Link Contact", GroupName = "PatientCommunications", Order = 1)]
        LinkContact = 44,

        [Display(Name = "Edit Contact", GroupName = "PatientCommunications", Order = 1)]
        EditContact = 45,

        [Display(Name = "UnLink Contact", GroupName = "PatientCommunications", Order = 1)]
        UnLinkContact = 46,

        [Display(Name = "Delete Payments, Adjustments, and Informational Transactions", GroupName = "PatientFinancials", Order = 2)]
        DeleteFinancialTransactions = 47,

        [Display(Name = "Add Payments, Adjustments, and Informational Transactions", GroupName = "PatientFinancials", Order = 3)]
        AddFinancialTransactions = 48,

        [Display(Name = "Edit Payments, Adjustments, and Informational Transactions", GroupName = "PatientFinancials", Order = 4)]
        EditFinancialTransactions = 49,

        [Display(Name = "View Patient Financial Summary Information", GroupName = "PatientFinancials", Order = 5)]
        ViewPatientFinancialSummaryInformation = 50,

        [Display(Name = "Edit Invoice", GroupName = "PatientFinancials", Order = 6)]
        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), PrincipalContextHasPermissionAttribute.FunctionalAccessMessage)]
        EditInvoice = 51,

        [Display(Name = "Bill Invoice", GroupName = "PatientFinancials", Order = 7)]
        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), PrincipalContextHasPermissionAttribute.FunctionalAccessMessage)]
        BillInvoice = 52,

        [Display(Name = "Process Pending Claims", GroupName = "PatientFinancials", Order = 8)]
        [AssociatedErrorMessage(typeof(PrincipalContextHasPermissionAttribute), PrincipalContextHasPermissionAttribute.FunctionalAccessMessage)]
        ProcessPendingClaims = 53,

        [Display(Name = "View Pending Claims", GroupName = "PatientFinancials", Order = 10)]
        ViewPendingClaims = 54,

        [Display(Name = "View Manage Claims Widget", GroupName = "PatientFinancials", Order = 10)]
        ViewManageClaimsWidget = 55,
        
        [Display(Name = "View Payments Lookup Widget", GroupName = "PatientFinancials", Order = 10)]
        ViewPaymentsLookupWidget = 56,

        [Display(Name = "View Manage External Providers", GroupName = "ExternalProviders", Order = 1)]
        ViewManageExternalProviders = 57,

        [Display(Name = "Edit External Providers", GroupName = "ExternalProviders", Order = 2)]
        EditExternalProviders = 58,

        [Display(Name = "Create External Providers", GroupName = "ExternalProviders", Order = 3)]
        CreateExternalProviders = 59,

        [Display(Name = "Delete External Providers", GroupName = "ExternalProviders", Order = 4)]
        DeleteExternalProvider = 60,

        [Display(Name = "Delete Standard Comments", GroupName = "PatientComments", Order = 1)]
        DeleteStandardComments = 61,

        [Display(Name = "Statements", GroupName = "PatientFinancials", Order = 11)]
        GenerateStatements = 62,

        [Display(Name = "Access Admin Utilities", GroupName = "GeneralSetup", Order = 1)]
        AccessAdminUtilities = 63,

        [Display(Name = "View Patient Comments", GroupName = "PatientComments", Order = 2)]
        ViewPatientComments = 64,

        [Display(Name = "Edit Patient Comments", GroupName = "PatientComments", Order = 3)]
        EditPatientComments = 65,

        [Display(Name = "Delete Patient Comments", GroupName = "PatientComments", Order = 4)]
        DeletePatientComments = 66,

        [Display(Name = "Merge Patients", GroupName = "PatientDemographics", Order = 4)]
        MergePatients = 67,

        [Display(Name = "Merge Providers", GroupName = "ExternalProviders", Order = 5)]
        MergeProviders = 68,

        [Display(Name = "Close Date Widget", GroupName = "GeneralSetup", Order = 2)]
        CloseDateWidget = 69
    }

    /// <summary>
    ///   Helper/extension methods for Permissions.
    /// </summary>
    public static class Permissions
    {
        /// <summary>
        ///   Gets the Permission of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="type"> The type. </param>
        /// <returns> </returns>
        public static Permission WithPermissionType<T>(this ICollection<T> source, Enum type) where T : Permission, new()
        {
            string permissionName = ((PermissionId)type).GetAttribute<DisplayAttribute>().Name;
            return source.FirstOrDefault(p => p.Name == permissionName);
        }

        public static bool PrincipalContextHasPermission(this PermissionId permission)
        {
            var principal = PrincipalContext.Current.Principal as UserPrincipal;
            if (principal == null)
            {
                Trace.TraceWarning("Principal is not a UserPrincipal. It is {0}. Permission Id: {1}. Stack: {2}",
                    PrincipalContext.Current.Principal.IfNotNull(p => p.GetType().FullName, "null"),
                    permission,
                    new StackTrace());
                return false;
            }

            var user = principal.Identity.User;
            if (user == null)
            {
                Trace.TraceWarning("Principal has no associated User. Permission Id: {0}. Stack: {1}",
                    permission,
                    new StackTrace());
                return false;
            }

            return user.Roles.Any(r => ((UserPermission)r.Tag).PermissionId == (int)permission);
        }
    }

    [AttributeUsage(AttributeTargets.Enum)]
    public class PrincipalContextHasPermissionAttribute : ValidationAttribute
    {
        public const string AreaAccessMessage = "You do not have permission to access this area of the software.  Please contact your local administrator.";
        public const string FunctionalAccessMessage = "Permission is required to perform this action.To perform this action, please ask an administrator to configure your IO permissions.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var permissionId = (PermissionId)value;
            if (permissionId.PrincipalContextHasPermission())
            {
                return ValidationResult.Success;
            }

            string associatedErrorMessage = GetAssociatedErrorMessageAtrribute((PermissionId)value, GetType()).IfNotDefault(a => a.ErrorMessage);

            string errorMessage = String.IsNullOrEmpty(associatedErrorMessage)
                                   ? AreaAccessMessage
                                   : associatedErrorMessage;
            return new ValidationResult(errorMessage, new[] { validationContext.MemberName });
        }

        private static AssociatedErrorMessageAttribute GetAssociatedErrorMessageAtrribute(PermissionId permissionId, Type associatedAttributeType)
        {
            MemberInfo[] members = permissionId.GetType().GetMember(permissionId.ToString());
            if (members.Any())
            {
                object[] attributes = members.First().GetCustomAttributes(typeof(AssociatedErrorMessageAttribute), false);
                if (attributes.Any())
                {
                    return attributes.OfType<AssociatedErrorMessageAttribute>().FirstOrDefault(a => a.AssociatedAttributeType == associatedAttributeType);
                }
            }
            return null;
        }
    }

    public enum PermissionCategory
    {
        General = 1,
        Reports = 2,
        Other = 3,
        [Display(Name = "Patient Insurance")]
        PatientInsurance = 4,
        [Display(Name = "Scheduling and Patient Appointments")]
        SchedulingAndPatientAppointments = 5,
        [Display(Name = "Patient Demographics")]
        PatientDemographics = 6,
        [Display(Name = "Patient Clinical")]
        PatientClinical = 7,
        [Display(Name = "Insurers")]
        Insurers = 8,
        [Display(Name = "Patient Financials")]
        PatientFinancials = 9,
        [Display(Name = "Patient Communications")]
        PatientCommunications = 10,
        [Display(Name = "External Providers")]
        ExternalProviders = 11,
        [Display(Name = "Patient Comments")]
        PatientComments = 12,
        [Display(Name = "General Setup")]
        GeneralSetup = 13
    }
}
