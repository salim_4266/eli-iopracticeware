﻿using Soaf;
using System;
using System.Collections.Generic;
using System.Linq;
using Soaf.Collections;

namespace IO.Practiceware.Model
{
    public partial class Provider
    {
        /// <summary>
        ///   Gets the claim file override provider.
        /// </summary>
        /// <param name="insurerId"> The insurer id. </param>
        /// <returns> </returns>
        public Provider GetClaimFileOverrideProvider(int insurerId)
        {
            IEnumerable<Provider> source;
            if (User != null) source = User.Providers;
            else if (ServiceLocation != null) source = ServiceLocation.Providers;
            else throw new InvalidOperationException("Neither User or ServiceLocation is present for provider.");

            return source.FirstOrDefault(i => i.ClaimFileOverrideInsurers.Any(o => o.Id == insurerId));
        }

        /// <summary>
        ///   Gets the claim file override provider or current if none exists.
        /// </summary>
        /// <param name="insurerId"> The insurer id. </param>
        /// <returns> </returns>
        public Provider GetClaimFileOverrideProviderOrCurrent(int insurerId)
        {
            return GetClaimFileOverrideProvider(insurerId) ?? this;
        }

        /// <summary>
        /// Gets a description of the provider that may be used in place of a provider name.
        /// </summary>
        /// <returns>Returns the provider's User formatted name if it exists.
        /// Otherwise returns the provider's ServiceLocation name.</returns>
        public string GetNameDescription()
        {
            if (User != null && !string.IsNullOrEmpty(User.GetFormattedName()))
            {
                return "whose User is {0}".FormatWith(User.GetFormattedName());
            }

            if (ServiceLocation != null && !string.IsNullOrEmpty(ServiceLocation.Name))
            {
                return "whose ServiceLocation is {0}".FormatWith(ServiceLocation.Name);
            }

            return string.Empty;
        }

        public string DisplayName
        {
            get
            {
                return new[]
                {
                    User.IfNotNull(u => u.UserName), 
                    ServiceLocation.IfNotNull(sl => sl.ShortName), 
                    BillingOrganization.IfNotNull(bo => bo.ShortName)
                }.WhereNotDefault()
                .Join(" - ");
            }
        }
    }
}