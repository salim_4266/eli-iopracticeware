﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Model
{
    public interface IAdjustmentOrFinancialInformation
    {
        int Id { get; set; }

        FinancialSourceType FinancialSourceType { get; set; }

        decimal Amount { get; set; }

        DateTime PostedDateTime { get; set; }

        int? BillingServiceId { get; set; }

        BillingService BillingService { get; set; }

        int? InvoiceReceivableId { get; set; }

        InvoiceReceivable InvoiceReceivable { get; set; }

        int? PatientId { get; set; }

        Patient Patient { get; set; }

        int? InsurerId { get; set; }

        Insurer Insurer { get; set; }

        int FinancialBatchId { get; set; }

        FinancialBatch FinancialBatch { get; set; }

        int? ClaimAdjustmentGroupCodeId { get; set; }

        ClaimAdjustmentGroupCode? ClaimAdjustmentGroupCode { get; }

        int? ClaimAdjustmentReasonCodeId { get; set; }

        ClaimAdjustmentReasonCode ClaimAdjustmentReasonCode { get; set; }

        bool IncludeCommentOnStatement { get; set; }

        string Comment { get; set; }
    }

    public partial class Adjustment : IAdjustmentOrFinancialInformation
    {
        public FinancialSourceType FinancialSourceType
        {
            get { return (FinancialSourceType)FinancialSourceTypeId; }
            set { FinancialSourceTypeId = (int)value; }
        }

        public ClaimAdjustmentGroupCode? ClaimAdjustmentGroupCode
        {
            get
            {
                if (ClaimAdjustmentGroupCodeId.HasValue)
                {
                    var groupCodeId = ClaimAdjustmentGroupCodeId.Value;
                    return (ClaimAdjustmentGroupCode) groupCodeId;
                }
                return null;
            }
        }
    }

    public static class AdjustmentExtensions
    {
        /// <summary>
        /// Calculates total amount taking AdjustmentType.IsDebit into account
        /// </summary>
        public static decimal CalculateTotalAmount(this IEnumerable<Adjustment> collection)
        {
            return collection.Sum(adj => adj.AdjustmentType.IsDebit ? (adj.Amount * -1) : adj.Amount);
        }

        /// <summary>
        /// Calculates total amount taking AdjustmentType.IsDebit into account and whether to exclude the Payment AdjustmentType
        /// </summary>
        /// <param name="collection">The Adjustment collection.</param>
        /// <param name="excludePaymentAdjustmentType">Whether to exclude the Payment adjustment type.</param>
        /// <returns></returns>
        public static decimal CalculateTotalAdjustmentAmount(this IEnumerable<Adjustment> collection, bool excludePaymentAdjustmentType)
        {
            if (excludePaymentAdjustmentType) return collection.Where(adj => adj.AdjustmentTypeId != (int)AdjustmentTypeId.Payment)
                .Sum(adj => adj.Amount);

            return collection.Sum(adj => adj.AdjustmentType.IsDebit ? (adj.Amount * -1) : adj.Amount);
        }

        /// <summary>
        /// Calculates total payment amount by FinancialSourceType, and whether to breakdown by primary insurer.
        /// </summary>
        /// <param name="collection">The Adjustment collection.</param>
        /// <param name="financialSourceType">Type of the financial source.</param>
        /// <param name="paymentsByPrimaryInsurer">if set to <c>true</c> [return only payments by the Primary Insurer].</param>
        /// <returns></returns>
        public static decimal CalculateTotalPaymentAmountBySource(this IEnumerable<Adjustment> collection, FinancialSourceType financialSourceType, bool? paymentsByPrimaryInsurer)
        {
            if (paymentsByPrimaryInsurer.HasValue && paymentsByPrimaryInsurer.Value) return collection.Where(adj => adj.FinancialBatch.FinancialSourceTypeId == (int)financialSourceType
                && adj.AdjustmentTypeId == (int)AdjustmentTypeId.Payment
                && adj.InvoiceReceivable.PatientInsurance == adj.InvoiceReceivable.GetPrimaryPatientInsurance())
                .Sum(adj => adj.Amount);

            if (paymentsByPrimaryInsurer.HasValue) return collection.Where(adj => adj.FinancialBatch.FinancialSourceTypeId == (int)financialSourceType
                && adj.AdjustmentTypeId == (int)AdjustmentTypeId.Payment
                && adj.InvoiceReceivable.PatientInsurance != adj.InvoiceReceivable.GetPrimaryPatientInsurance())
                .Sum(adj => adj.Amount);

            return collection.Where(adj => adj.FinancialBatch.FinancialSourceTypeId == (int) financialSourceType
                && adj.AdjustmentTypeId == (int) AdjustmentTypeId.Payment)
                .Sum(adj => adj.Amount);
        }
    }
}
