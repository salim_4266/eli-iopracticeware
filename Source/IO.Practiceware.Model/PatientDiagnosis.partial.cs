﻿using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class PatientDiagnosis
    {
        public PatientDiagnosisDetail LatestPatientDiagnosisDetail
        {
            get { return PatientDiagnosisDetails.OrderByDescending(d => d.StartDateTime).FirstOrDefault(); }
        }
    }
}