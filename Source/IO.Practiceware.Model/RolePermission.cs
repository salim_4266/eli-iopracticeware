//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// A collection of permissions goes into one Role.  Lots of meta data.
    /// </summary>
    public partial class RolePermission : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return new object[] { RoleId, PermissionId }; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Required]
    	public virtual bool IsDenied
        {
            get { return _isDenied; }
            set 
    		{ 
    			var isChanged = _isDenied != value; 
    			_isDenied = value; 
    			if (isChanged) OnPropertyChanged("IsDenied");
    		}
        }
        private bool _isDenied;
    
        [Key]
    	public virtual int RoleId
        {
            get { return _roleId; }
            set
            {
                if (_roleId != value)
                {
                    if (Role != null && Role.Id != value)
                    {
                        Role = null;
                    }
                    _roleId = value;
    
    				OnPropertyChanged("RoleId");
                }
            }
        }
        private int _roleId;
    
        [Key]
    	public virtual int PermissionId
        {
            get { return _permissionId; }
            set
            {
                if (_permissionId != value)
                {
                    if (Permission != null && Permission.Id != value)
                    {
                        Permission = null;
                    }
                    _permissionId = value;
    
    				OnPropertyChanged("PermissionId");
                }
            }
        }
        private int _permissionId;

        #endregion

        #region Navigation Properties
    
    	[Association("Role", "RoleId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Role Role
        {
            get { return _role; }
            set
            {
                if (!ReferenceEquals(_role, value))
                {
                    var previousValue = _role;
                    _role = value;
                    FixupRole(previousValue);
    				OnPropertyChanged("Role");
                }
            }
        }
        private Role _role;
    
    	[Association("Permission", "PermissionId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Permission Permission
        {
            get { return _permission; }
            set
            {
                if (!ReferenceEquals(_permission, value))
                {
                    var previousValue = _permission;
                    _permission = value;
                    FixupPermission(previousValue);
    				OnPropertyChanged("Permission");
                }
            }
        }
        private Permission _permission;

        #endregion

        #region Association Fixup
    
        private void FixupRole(Role previousValue)
        {
            if (previousValue != null && previousValue.RolePermissions.Contains(this))
            {
                previousValue.RolePermissions.Remove(this);
            }
    
            if (Role != null)
            {
                if (!Role.RolePermissions.Contains(this))
                {
                    Role.RolePermissions.Add(this);
                }
                if (Role != null && RoleId != Role.Id)
                {
                    RoleId = Role.Id;
                }
            }
        }
    
        private void FixupPermission(Permission previousValue)
        {
            if (previousValue != null && previousValue.RolePermissions.Contains(this))
            {
                previousValue.RolePermissions.Remove(this);
            }
    
            if (Permission != null)
            {
                if (!Permission.RolePermissions.Contains(this))
                {
                    Permission.RolePermissions.Add(this);
                }
                if (Permission != null && PermissionId != Permission.Id)
                {
                    PermissionId = Permission.Id;
                }
            }
        }

        #endregion

    }
}
