﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum DrugUnitOfMeasurement
    {
        [Display(Name = "International Unit")] InternationalUnit = 1,
        [Display(Name = "Gram")] Gram = 2,
        [Display(Name = "Milligram")] Milligram = 3,
        [Display(Name = "Milliliter")] Milliliter = 4,
        [Display(Name = "Unit")] Unit = 5
    }
}