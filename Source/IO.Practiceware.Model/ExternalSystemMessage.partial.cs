﻿using Soaf;

namespace IO.Practiceware.Model
{
    public partial class ExternalSystemMessage
    {
        public byte[] PaperClaimMessage
        {
            get
            {
                if (ExternalSystemExternalSystemMessageType != null && ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int) ExternalSystemMessageTypeId.PaperClaim_Outbound)
                {
                    return Value.HexStringToByteArray();
                }

                return null;
            }
        }
    }
}