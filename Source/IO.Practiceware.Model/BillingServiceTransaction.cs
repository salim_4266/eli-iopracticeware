//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Transactional record documenting each time a service is billed, either to an insurer or a patient.
    /// </summary>
    public partial class BillingServiceTransaction : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual System.Guid Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private System.Guid _id;
    
        [Required]
    	public virtual System.DateTime DateTime
        {
            get { return _dateTime; }
            set 
    		{ 
    			var isChanged = _dateTime != value; 
    			_dateTime = value; 
    			if (isChanged) OnPropertyChanged("DateTime");
    		}
        }
        private System.DateTime _dateTime;
    
        [Required]
    	public virtual decimal AmountSent
        {
            get { return _amountSent; }
            set 
    		{ 
    			var isChanged = _amountSent != value; 
    			_amountSent = value; 
    			if (isChanged) OnPropertyChanged("AmountSent");
    		}
        }
        private decimal _amountSent;
    
    	/// <summary>
    	/// This is the number that apears in the claim file. Here to distingush mutiple claims being sent in a single file and a single day.
    	/// </summary>
    	public virtual Nullable<int> ClaimFileNumber
        {
            get { return _claimFileNumber; }
            set 
    		{ 
    			var isChanged = _claimFileNumber != value; 
    			_claimFileNumber = value; 
    			if (isChanged) OnPropertyChanged("ClaimFileNumber");
    		}
        }
        private Nullable<int> _claimFileNumber;
    
        [Required]
    	internal virtual int BillingServiceTransactionStatusId
        {
            get { return _billingServiceTransactionStatusId; }
            set 
    		{ 
    			var isChanged = _billingServiceTransactionStatusId != value; 
    			_billingServiceTransactionStatusId = value; 
    			if (isChanged) OnPropertyChanged("BillingServiceTransactionStatusId");
    		}
        }
        private int _billingServiceTransactionStatusId;
    
    	/// <summary>
    	/// Was transaction sent electronically or by paper?  ENUM
    	/// </summary>
        [Required]
    	internal virtual int MethodSentId
        {
            get { return _methodSentId; }
            set 
    		{ 
    			var isChanged = _methodSentId != value; 
    			_methodSentId = value; 
    			if (isChanged) OnPropertyChanged("MethodSentId");
    		}
        }
        private int _methodSentId;
    
    	public virtual int BillingServiceId
        {
            get { return _billingServiceId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_billingServiceId != value)
                    {
                        if (BillingService != null && BillingService.Id != value)
                        {
                            BillingService = null;
                        }
                        _billingServiceId = value;
        
        				OnPropertyChanged("BillingServiceId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _billingServiceId;
    
    	public virtual int InvoiceReceivableId
        {
            get { return _invoiceReceivableId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_invoiceReceivableId != value)
                    {
                        if (InvoiceReceivable != null && InvoiceReceivable.Id != value)
                        {
                            InvoiceReceivable = null;
                        }
                        _invoiceReceivableId = value;
        
        				OnPropertyChanged("InvoiceReceivableId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _invoiceReceivableId;
    
    	/// <summary>
    	/// Indicates which entity (clearinghouse or insurer) the service is being sent to.
    	/// </summary>
    	public virtual Nullable<int> ClaimFileReceiverId
        {
            get { return _claimFileReceiverId; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_claimFileReceiverId != value)
                    {
                        if (ClaimFileReceiver != null && ClaimFileReceiver.Id != value)
                        {
                            ClaimFileReceiver = null;
                        }
                        _claimFileReceiverId = value;
        
        				OnPropertyChanged("ClaimFileReceiverId");
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _claimFileReceiverId;

        #endregion

        #region Navigation Properties
    
    	[Association("BillingService", "BillingServiceId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual BillingService BillingService
        {
            get { return _billingService; }
            set
            {
                if (!ReferenceEquals(_billingService, value))
                {
                    var previousValue = _billingService;
                    _billingService = value;
                    FixupBillingService(previousValue);
    				OnPropertyChanged("BillingService");
                }
            }
        }
        private BillingService _billingService;
    
    	[Association("InvoiceReceivable", "InvoiceReceivableId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual InvoiceReceivable InvoiceReceivable
        {
            get { return _invoiceReceivable; }
            set
            {
                if (!ReferenceEquals(_invoiceReceivable, value))
                {
                    var previousValue = _invoiceReceivable;
                    _invoiceReceivable = value;
                    FixupInvoiceReceivable(previousValue);
    				OnPropertyChanged("InvoiceReceivable");
                }
            }
        }
        private InvoiceReceivable _invoiceReceivable;
    
    	[Association("ClaimFileReceiver", "ClaimFileReceiverId", "Id")]
        public virtual ClaimFileReceiver ClaimFileReceiver
        {
            get { return _claimFileReceiver; }
            set
            {
                if (!ReferenceEquals(_claimFileReceiver, value))
                {
                    var previousValue = _claimFileReceiver;
                    _claimFileReceiver = value;
                    FixupClaimFileReceiver(previousValue);
    				OnPropertyChanged("ClaimFileReceiver");
                }
            }
        }
        private ClaimFileReceiver _claimFileReceiver;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupBillingService(BillingService previousValue)
        {
            if (previousValue != null && previousValue.BillingServiceTransactions.Contains(this))
            {
                previousValue.BillingServiceTransactions.Remove(this);
            }
    
            if (BillingService != null)
            {
                if (!BillingService.BillingServiceTransactions.Contains(this))
                {
                    BillingService.BillingServiceTransactions.Add(this);
                }
                if (BillingService != null && BillingServiceId != BillingService.Id)
                {
                    BillingServiceId = BillingService.Id;
                }
            }
        }
    
        private void FixupInvoiceReceivable(InvoiceReceivable previousValue)
        {
            if (previousValue != null && previousValue.BillingServiceTransactions.Contains(this))
            {
                previousValue.BillingServiceTransactions.Remove(this);
            }
    
            if (InvoiceReceivable != null)
            {
                if (!InvoiceReceivable.BillingServiceTransactions.Contains(this))
                {
                    InvoiceReceivable.BillingServiceTransactions.Add(this);
                }
                if (InvoiceReceivable != null && InvoiceReceivableId != InvoiceReceivable.Id)
                {
                    InvoiceReceivableId = InvoiceReceivable.Id;
                }
            }
        }
    
        private void FixupClaimFileReceiver(ClaimFileReceiver previousValue)
        {
            if (previousValue != null && previousValue.BillingServiceTransactions.Contains(this))
            {
                previousValue.BillingServiceTransactions.Remove(this);
            }
    
            if (ClaimFileReceiver != null)
            {
                if (!ClaimFileReceiver.BillingServiceTransactions.Contains(this))
                {
                    ClaimFileReceiver.BillingServiceTransactions.Add(this);
                }
                if (ClaimFileReceiver != null && ClaimFileReceiverId != ClaimFileReceiver.Id)
                {
                    ClaimFileReceiverId = ClaimFileReceiver.Id;
                }
            }
            else if (!_settingFK)
            {
                ClaimFileReceiverId = null;
            }
        }

        #endregion

    }
}
