﻿namespace IO.Practiceware.Model
{
    public partial class Question
    {
        /// <summary>
        ///     OrdinalId is obsolete and non-functional.
        /// </summary>
        public int OrdinalId { get; set; }
    }
}