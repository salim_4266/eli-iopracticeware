//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class PatientPhoneNumberCommunicationPreference : PatientCommunicationPreference  
    {
    
        #region Primitive Properties
    
    	public virtual int PatientPhoneNumberId
        {
            get { return _patientPhoneNumberId; }
            set
            {
                if (_patientPhoneNumberId != value)
                {
                    if (PatientPhoneNumber != null && PatientPhoneNumber.Id != value)
                    {
                        PatientPhoneNumber = null;
                    }
                    _patientPhoneNumberId = value;
    
    				OnPropertyChanged("PatientPhoneNumberId");
                }
            }
        }
        private int _patientPhoneNumberId;

        #endregion

        #region Navigation Properties
    
    	[Association("PatientPhoneNumber", "PatientPhoneNumberId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual PatientPhoneNumber PatientPhoneNumber
        {
            get { return _patientPhoneNumber; }
            set
            {
                if (!ReferenceEquals(_patientPhoneNumber, value))
                {
                    var previousValue = _patientPhoneNumber;
                    _patientPhoneNumber = value;
                    FixupPatientPhoneNumber(previousValue);
    				OnPropertyChanged("PatientPhoneNumber");
                }
            }
        }
        private PatientPhoneNumber _patientPhoneNumber;

        #endregion

        #region Association Fixup
    
        private void FixupPatientPhoneNumber(PatientPhoneNumber previousValue)
        {
            if (previousValue != null && previousValue.PatientPhoneNumberCommunicationPreferences.Contains(this))
            {
                previousValue.PatientPhoneNumberCommunicationPreferences.Remove(this);
            }
    
            if (PatientPhoneNumber != null)
            {
                if (!PatientPhoneNumber.PatientPhoneNumberCommunicationPreferences.Contains(this))
                {
                    PatientPhoneNumber.PatientPhoneNumberCommunicationPreferences.Add(this);
                }
                if (PatientPhoneNumber != null && PatientPhoneNumberId != PatientPhoneNumber.Id)
                {
                    PatientPhoneNumberId = PatientPhoneNumber.Id;
                }
            }
        }

        #endregion

    }
}
