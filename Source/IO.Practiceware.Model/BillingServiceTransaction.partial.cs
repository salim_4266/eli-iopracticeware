﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class BillingServiceTransaction
    {
        public BillingServiceTransactionStatus BillingServiceTransactionStatus
        {
            get { return (BillingServiceTransactionStatus)BillingServiceTransactionStatusId; }
            set { BillingServiceTransactionStatusId = (int)value; }
        }

         public MethodSent MethodSent
        {
            get { return (MethodSent)MethodSentId; }
            set { MethodSentId = (int)value; }
        }
    }

    public enum BillingServiceTransactionStatus
    {
        [Display(Name = "Queued")]
        Queued = 1,
        [Display(Name = "Sent")]
        Sent = 2,
        [Display(Name = "Wait")]
        Wait = 3,
        [Display(Name = "Sent, CrossOver")]
        SentCrossOver = 4,
        [Display(Name = "Closed, Not Sent")]
        ClosedNotSent = 5,
        [Display(Name = "Closed, Sent")]
        ClosedSent = 6,
        [Display(Name = "Closed, CrossOver")]
        ClosedCrossOver = 7,
    }

    public static class BillingServiceTransactionStatuses
    {
        public static bool IncludedInPatientBalance(this BillingServiceTransactionStatus s)
        {
            return s == BillingServiceTransactionStatus.Queued || s == BillingServiceTransactionStatus.Sent 
                || s == BillingServiceTransactionStatus.Wait || s == BillingServiceTransactionStatus.SentCrossOver;
        }
    }

    public enum MethodSent
    {
        Electronic = 1,
        Paper = 2
    }
}