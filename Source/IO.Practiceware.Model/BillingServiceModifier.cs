//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    ///                Modifiers communicate additional information about a service rendered to an insurer.  For example, modifer RT means a service was done on the right eye.  50 means a service was done on both eyes.  Four modifiers max can be attached to a service in an electronic claim file.              
    /// </summary>
    public partial class BillingServiceModifier : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual Nullable<int> OrdinalId
        {
            get { return _ordinalId; }
            set 
    		{ 
    			var isChanged = _ordinalId != value; 
    			_ordinalId = value; 
    			if (isChanged) OnPropertyChanged("OrdinalId");
    		}
        }
        private Nullable<int> _ordinalId;
    
    	public virtual int BillingServiceId
        {
            get { return _billingServiceId; }
            set
            {
                if (_billingServiceId != value)
                {
                    if (BillingService != null && BillingService.Id != value)
                    {
                        BillingService = null;
                    }
                    _billingServiceId = value;
    
    				OnPropertyChanged("BillingServiceId");
                }
            }
        }
        private int _billingServiceId;
    
    	public virtual int ServiceModifierId
        {
            get { return _serviceModifierId; }
            set
            {
                if (_serviceModifierId != value)
                {
                    if (ServiceModifier != null && ServiceModifier.Id != value)
                    {
                        ServiceModifier = null;
                    }
                    _serviceModifierId = value;
    
    				OnPropertyChanged("ServiceModifierId");
                }
            }
        }
        private int _serviceModifierId;

        #endregion

        #region Navigation Properties
    
    	[Association("BillingService", "BillingServiceId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual BillingService BillingService
        {
            get { return _billingService; }
            set
            {
                if (!ReferenceEquals(_billingService, value))
                {
                    var previousValue = _billingService;
                    _billingService = value;
                    FixupBillingService(previousValue);
    				OnPropertyChanged("BillingService");
                }
            }
        }
        private BillingService _billingService;
    
    	[Association("ServiceModifier", "ServiceModifierId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ServiceModifier ServiceModifier
        {
            get { return _serviceModifier; }
            set
            {
                if (!ReferenceEquals(_serviceModifier, value))
                {
                    var previousValue = _serviceModifier;
                    _serviceModifier = value;
                    FixupServiceModifier(previousValue);
    				OnPropertyChanged("ServiceModifier");
                }
            }
        }
        private ServiceModifier _serviceModifier;

        #endregion

        #region Association Fixup
    
        private void FixupBillingService(BillingService previousValue)
        {
            if (previousValue != null && previousValue.BillingServiceModifiers.Contains(this))
            {
                previousValue.BillingServiceModifiers.Remove(this);
            }
    
            if (BillingService != null)
            {
                if (!BillingService.BillingServiceModifiers.Contains(this))
                {
                    BillingService.BillingServiceModifiers.Add(this);
                }
                if (BillingService != null && BillingServiceId != BillingService.Id)
                {
                    BillingServiceId = BillingService.Id;
                }
            }
        }
    
        private void FixupServiceModifier(ServiceModifier previousValue)
        {
            if (previousValue != null && previousValue.BillingServiceModifiers.Contains(this))
            {
                previousValue.BillingServiceModifiers.Remove(this);
            }
    
            if (ServiceModifier != null)
            {
                if (!ServiceModifier.BillingServiceModifiers.Contains(this))
                {
                    ServiceModifier.BillingServiceModifiers.Add(this);
                }
                if (ServiceModifier != null && ServiceModifierId != ServiceModifier.Id)
                {
                    ServiceModifierId = ServiceModifier.Id;
                }
            }
        }

        #endregion

    }
}
