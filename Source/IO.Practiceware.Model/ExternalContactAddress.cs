//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Addresses associated with Contacts
    /// </summary>
    public partial class ExternalContactAddress : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
    	public virtual int ExternalContactId
        {
            get { return _externalContactId; }
            set
            {
                if (_externalContactId != value)
                {
                    if (ExternalContact != null && ExternalContact.Id != value)
                    {
                        ExternalContact = null;
                    }
                    _externalContactId = value;
    
    				OnPropertyChanged("ExternalContactId");
                }
            }
        }
        private int _externalContactId;
    
    	public virtual int ContactAddressTypeId
        {
            get { return _contactAddressTypeId; }
            set
            {
                if (_contactAddressTypeId != value)
                {
                    if (ExternalContactAddressType != null && ExternalContactAddressType.Id != value)
                    {
                        ExternalContactAddressType = null;
                    }
                    _contactAddressTypeId = value;
    
    				OnPropertyChanged("ContactAddressTypeId");
                }
            }
        }
        private int _contactAddressTypeId;
    
        [Key]
    	public virtual long Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private long _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Line1
        {
            get { return _line1; }
            set 
    		{ 
    			var isChanged = _line1 != value; 
    			_line1 = value; 
    			if (isChanged) OnPropertyChanged("Line1");
    		}
        }
        private string _line1;
    
    	public virtual string Line2
        {
            get { return _line2; }
            set 
    		{ 
    			var isChanged = _line2 != value; 
    			_line2 = value; 
    			if (isChanged) OnPropertyChanged("Line2");
    		}
        }
        private string _line2;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string City
        {
            get { return _city; }
            set 
    		{ 
    			var isChanged = _city != value; 
    			_city = value; 
    			if (isChanged) OnPropertyChanged("City");
    		}
        }
        private string _city;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string PostalCode
        {
            get { return _postalCode; }
            set 
    		{ 
    			var isChanged = _postalCode != value; 
    			_postalCode = value; 
    			if (isChanged) OnPropertyChanged("PostalCode");
    		}
        }
        private string _postalCode;
    
    	public virtual string Line3
        {
            get { return _line3; }
            set 
    		{ 
    			var isChanged = _line3 != value; 
    			_line3 = value; 
    			if (isChanged) OnPropertyChanged("Line3");
    		}
        }
        private string _line3;
    
    	public virtual int StateOrProvinceId
        {
            get { return _stateOrProvinceId; }
            set
            {
                if (_stateOrProvinceId != value)
                {
                    if (StateOrProvince != null && StateOrProvince.Id != value)
                    {
                        StateOrProvince = null;
                    }
                    _stateOrProvinceId = value;
    
    				OnPropertyChanged("StateOrProvinceId");
                }
            }
        }
        private int _stateOrProvinceId;

        #endregion

        #region Navigation Properties
    
    	[Association("ExternalContact", "ExternalContactId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ExternalContact ExternalContact
        {
            get { return _externalContact; }
            set
            {
                if (!ReferenceEquals(_externalContact, value))
                {
                    var previousValue = _externalContact;
                    _externalContact = value;
                    FixupExternalContact(previousValue);
    				OnPropertyChanged("ExternalContact");
                }
            }
        }
        private ExternalContact _externalContact;
    
    	[Association("ExternalContactAddressType", "ContactAddressTypeId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual ExternalContactAddressType ExternalContactAddressType
        {
            get { return _externalContactAddressType; }
            set
            {
                if (!ReferenceEquals(_externalContactAddressType, value))
                {
                    var previousValue = _externalContactAddressType;
                    _externalContactAddressType = value;
                    FixupExternalContactAddressType(previousValue);
    				OnPropertyChanged("ExternalContactAddressType");
                }
            }
        }
        private ExternalContactAddressType _externalContactAddressType;
    
    	[Association("StateOrProvince", "StateOrProvinceId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual StateOrProvince StateOrProvince
        {
            get { return _stateOrProvince; }
            set
            {
                if (!ReferenceEquals(_stateOrProvince, value))
                {
                    var previousValue = _stateOrProvince;
                    _stateOrProvince = value;
                    FixupStateOrProvince(previousValue);
    				OnPropertyChanged("StateOrProvince");
                }
            }
        }
        private StateOrProvince _stateOrProvince;

        #endregion

        #region Association Fixup
    
        private void FixupExternalContact(ExternalContact previousValue)
        {
            if (previousValue != null && previousValue.ExternalContactAddresses.Contains(this))
            {
                previousValue.ExternalContactAddresses.Remove(this);
            }
    
            if (ExternalContact != null)
            {
                if (!ExternalContact.ExternalContactAddresses.Contains(this))
                {
                    ExternalContact.ExternalContactAddresses.Add(this);
                }
                if (ExternalContact != null && ExternalContactId != ExternalContact.Id)
                {
                    ExternalContactId = ExternalContact.Id;
                }
            }
        }
    
        private void FixupExternalContactAddressType(ExternalContactAddressType previousValue)
        {
            if (previousValue != null && previousValue.ExternalContactAddresses.Contains(this))
            {
                previousValue.ExternalContactAddresses.Remove(this);
            }
    
            if (ExternalContactAddressType != null)
            {
                if (!ExternalContactAddressType.ExternalContactAddresses.Contains(this))
                {
                    ExternalContactAddressType.ExternalContactAddresses.Add(this);
                }
                if (ExternalContactAddressType != null && ContactAddressTypeId != ExternalContactAddressType.Id)
                {
                    ContactAddressTypeId = ExternalContactAddressType.Id;
                }
            }
        }
    
        private void FixupStateOrProvince(StateOrProvince previousValue)
        {
            if (previousValue != null && previousValue.ExternalContactAddresses.Contains(this))
            {
                previousValue.ExternalContactAddresses.Remove(this);
            }
    
            if (StateOrProvince != null)
            {
                if (!StateOrProvince.ExternalContactAddresses.Contains(this))
                {
                    StateOrProvince.ExternalContactAddresses.Add(this);
                }
                if (StateOrProvince != null && StateOrProvinceId != StateOrProvince.Id)
                {
                    StateOrProvinceId = StateOrProvince.Id;
                }
            }
        }

        #endregion

    }
}
