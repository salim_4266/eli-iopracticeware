//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Modifiers communicate additional information to the insurer about a service rendered.  ServiceModifiers are the master list that BillingServices and ClinicalServices will pull from.
    /// </summary>
    public partial class ServiceModifier : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Code
        {
            get { return _code; }
            set 
    		{ 
    			var isChanged = _code != value; 
    			_code = value; 
    			if (isChanged) OnPropertyChanged("Code");
    		}
        }
        private string _code;
    
    	public virtual string Description
        {
            get { return _description; }
            set 
    		{ 
    			var isChanged = _description != value; 
    			_description = value; 
    			if (isChanged) OnPropertyChanged("Description");
    		}
        }
        private string _description;
    
    	/// <summary>
    	/// The order of display in the exam software.
    	/// </summary>
    	public virtual Nullable<int> ClinicalOrdinalId
        {
            get { return _clinicalOrdinalId; }
            set 
    		{ 
    			var isChanged = _clinicalOrdinalId != value; 
    			_clinicalOrdinalId = value; 
    			if (isChanged) OnPropertyChanged("ClinicalOrdinalId");
    		}
        }
        private Nullable<int> _clinicalOrdinalId;
    
    	/// <summary>
    	/// The order to display to the billing department.
    	/// </summary>
    	public virtual Nullable<int> BillingOrdinalId
        {
            get { return _billingOrdinalId; }
            set 
    		{ 
    			var isChanged = _billingOrdinalId != value; 
    			_billingOrdinalId = value; 
    			if (isChanged) OnPropertyChanged("BillingOrdinalId");
    		}
        }
        private Nullable<int> _billingOrdinalId;
    
        [Required]
    	public virtual bool IsArchived
        {
            get { return _isArchived; }
            set 
    		{ 
    			var isChanged = _isArchived != value; 
    			_isArchived = value; 
    			if (isChanged) OnPropertyChanged("IsArchived");
    		}
        }
        private bool _isArchived = false;

        #endregion

        #region Navigation Properties
    
    	[Association("BillingServiceModifiers", "Id", "ServiceModifierId")]
        public virtual System.Collections.Generic.ICollection<BillingServiceModifier> BillingServiceModifiers
        {
            get
            {
                if (_billingServiceModifiers == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<BillingServiceModifier>();
                    newCollection.ItemSet += FixupBillingServiceModifiersItemSet;
                    newCollection.ItemRemoved += FixupBillingServiceModifiersItemRemoved;
                    _billingServiceModifiers = newCollection;
                }
                return _billingServiceModifiers;
            }
            set
            {
                if (!ReferenceEquals(_billingServiceModifiers, value))
                {
                    var previousValue = _billingServiceModifiers as Soaf.Collections.FixupCollection<BillingServiceModifier>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupBillingServiceModifiersItemSet;
                        previousValue.ItemRemoved -= FixupBillingServiceModifiersItemRemoved;
                    }
                    _billingServiceModifiers = value;
                    var newValue = value as Soaf.Collections.FixupCollection<BillingServiceModifier>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupBillingServiceModifiersItemSet;
                        newValue.ItemRemoved += FixupBillingServiceModifiersItemRemoved;
                    }
    				OnPropertyChanged("BillingServiceModifiers");
                }
            }
        }
        private System.Collections.Generic.ICollection<BillingServiceModifier> _billingServiceModifiers;
    
    	[Association("ClinicalServiceModifiers", "Id", "ServiceModifierId")]
        public virtual System.Collections.Generic.ICollection<ClinicalServiceModifier> ClinicalServiceModifiers
        {
            get
            {
                if (_clinicalServiceModifiers == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ClinicalServiceModifier>();
                    newCollection.ItemSet += FixupClinicalServiceModifiersItemSet;
                    newCollection.ItemRemoved += FixupClinicalServiceModifiersItemRemoved;
                    _clinicalServiceModifiers = newCollection;
                }
                return _clinicalServiceModifiers;
            }
            set
            {
                if (!ReferenceEquals(_clinicalServiceModifiers, value))
                {
                    var previousValue = _clinicalServiceModifiers as Soaf.Collections.FixupCollection<ClinicalServiceModifier>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupClinicalServiceModifiersItemSet;
                        previousValue.ItemRemoved -= FixupClinicalServiceModifiersItemRemoved;
                    }
                    _clinicalServiceModifiers = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ClinicalServiceModifier>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupClinicalServiceModifiersItemSet;
                        newValue.ItemRemoved += FixupClinicalServiceModifiersItemRemoved;
                    }
    				OnPropertyChanged("ClinicalServiceModifiers");
                }
            }
        }
        private System.Collections.Generic.ICollection<ClinicalServiceModifier> _clinicalServiceModifiers;

        #endregion

        #region Association Fixup
    
        private void FixupBillingServiceModifiersItemSet(object sender, Soaf.EventArgs<BillingServiceModifier> e)
        {
            var item = e.Value;
     
            item.ServiceModifier = this;
        }
    
        private void FixupBillingServiceModifiersItemRemoved(object sender, Soaf.EventArgs<BillingServiceModifier> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ServiceModifier, this))
            {
                item.ServiceModifier = null;
            }
        }
    
    
        private void FixupClinicalServiceModifiersItemSet(object sender, Soaf.EventArgs<ClinicalServiceModifier> e)
        {
            var item = e.Value;
     
            item.ServiceModifier = this;
        }
    
        private void FixupClinicalServiceModifiersItemRemoved(object sender, Soaf.EventArgs<ClinicalServiceModifier> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ServiceModifier, this))
            {
                item.ServiceModifier = null;
            }
        }
    

        #endregion

    }
}
