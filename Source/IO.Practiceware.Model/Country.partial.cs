﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class Country
    {
        /// <summary>
        ///   Gets a value indicating whether this instance is the US.
        /// </summary>
        /// <value> <c>true</c> if this instance is US; otherwise, <c>false</c> . </value>
        public bool IsUnitedStates
        {
            get { return NumberCode == (int) CountryNumberCode.UnitedStates || Id == (int) CountryId.UnitedStates; }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is Canada.
        /// </summary>
        /// <value> <c>true</c> if this instance is CA; otherwise, <c>false</c> . </value>
        public bool IsCanada
        {
            get { return NumberCode == (int)CountryNumberCode.Canada || Id == (int) CountryId.Canada; }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is Mexico.
        /// </summary>
        /// <value> <c>true</c> if this instance is MX; otherwise, <c>false</c> . </value>
        public bool IsMexico
        {
            get { return NumberCode == (int)CountryNumberCode.Mexico|| Id == (int)CountryId.Mexico; }
        }
    }

    /// <summary>
    ///   Well known countries list.
    /// </summary>
    public enum CountryId
    {
        [Display(Name = "United States")]
        UnitedStates = 225,
        [Display(Name = "Canada")]
        Canada = 38,
        [Display(Name = "Mexico")]
        Mexico = 140
    }

    /// <summary>
    /// The ISO 3166 country number code.  These are the well known ones
    /// </summary>
    public enum CountryNumberCode
    {
        UnitedStates = 840,
        Canada = 124,
        Mexico = 484
    }

    public sealed class WellKnownCountryLetterAbbreviations
    {
        public const string UnitedStates3LetterAbbreviation = "USA";
        public const string UnitedStates2LetterAbbreviation = "US";

        public const string Canada3LetterAbbreviation = "CAN";
        public const string Canada2LetterAbbreviation = "CA";

        public const string Mexico3LetterAbbreviation = "MEX";
        public const string Mexico2LetterAbbreviation = "MX";
    }
}