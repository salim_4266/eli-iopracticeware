﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum ClaimFormTypeId
    {
        [Display(Name = "CMS-1500")]
        Cms1500 = 1,

        [Display(Name = "UB-04")]
        UB04 = 2,

        [Display(Name = "837 Professional")]
        Professional837 = 3,

        [Display(Name = "837 Institutional")]
        Institutional837 = 4
    }

    public partial class ClaimFormType
    {
        public MethodSent MethodSent
        {
            get { return (MethodSent) MethodSentId; }
            set { MethodSentId = (int)value; }
        }
    }
}