﻿namespace IO.Practiceware.Model
{
    /// <summary>
    ///     A contract for an entity that links to another entity dynamically based on type and key.
    /// </summary>
    public interface IEntityLink
    {
        int PracticeRepositoryEntityId { get; }

        string PracticeRepositoryEntityKey { get; }
    }

    public partial class ExternalSystemEntityMapping : IEntityLink { }

    public partial class LinkedQuestionAnswerValue : IEntityLink { }
}