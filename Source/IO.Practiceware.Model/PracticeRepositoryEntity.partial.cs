﻿namespace IO.Practiceware.Model
{
    /// <summary>
    ///   Well known practice repository entity ids.
    /// </summary>
    public enum PracticeRepositoryEntityId
    {
        /// <summary>
        /// </summary>
        Appointment = 1,

        /// <summary>
        /// </summary>
        AppointmentType = 2,

        /// <summary>
        /// </summary>
        Patient = 3,

        /// <summary>
        /// </summary>
        PatientInsurance = 4,

        /// <summary>
        /// </summary>
        Insurer = 5,

        /// <summary>
        /// </summary>
        User = 6,

        /// <summary>
        /// </summary>
        ExternalContact = 7,

        /// <summary>
        /// </summary>
        ServiceLocation = 8,

        /// <summary>
        /// </summary>
        Language = 9,

        /// <summary>
        /// </summary>
        BillingServiceTransaction = 10,

        /// <summary>
        /// </summary>
        Ethnicity = 11,

        /// <summary>
        /// </summary>
        ClaimFilingIndicatorCode = 12,

        /// <summary>
        /// </summary>
        MaritalStatus = 13,
        Allergen = 14,
        AllergenReactionType = 15,
        AllergySource = 16,
        AxisQualifier = 17,
        ClinicalAttribute = 18,
        ClinicalCondition = 19,
        ClinicalConditionStatus = 20,
        ClinicalProcedure = 21,
        ClinicalQualifier = 22,
        DrugDosageAction = 23,
        DrugDosageForm = 24,
        DrugDosageNumber = 25,
        DrugDispenseForm = 26,
        LaboratoryTest = 27,
        Medication = 28,
        ObservableEntity = 29,
        RelevancyQualifier = 30,
        RouteOfAdministration = 31,
        SmokingCondition = 32,
        TreatmentGoal = 33,
        UnitOfMeasurement = 34,
        Vaccine = 35,
        AccuracyQualifier = 36,
        Race = 37,
        FamilyRelationship = 38,
        Encounter = 39,
        EncounterDiagnosticTestOrder = 40,
        EncounterLaboratoryTestOrder = 41,
        EncounterTreatmentGoalAndInstruction = 42,
        PatientAllergen = 43,
        PatientBloodPressure = 45,
        PatientCognitiveStatusAssessment = 46,
        PatientFunctionalStatusAssessment = 47,
        PatientHeightAndWeight = 48,
        PatientLaboratoryTestResult = 49,
        PatientMedication = 50,
        PatientProblemConcernLevel = 51,
        PatientProcedurePerformed = 52,
        PatientSmokingStatus = 53,
        PatientVaccination = 54,
        TreatmentGoalAndInstruction = 55,
        EncounterTransitionOfCareOrder = 56,
        EncounterProcedureOrder = 57,
        EncounterAdministeredMedication = 58,
        PatientDiagnosisDetail = 59,
        PatientDiagnosis = 60,
        PatientDiagnosticTestPerformed = 61,
        Gender = 62,
        EncounterService = 63,
        BillingService = 64,
        PatientExamPerformed = 65,
        PatientInterventionPerformed = 66,
        EncounterMedicationOrder = 67,
        Question = 68,
        Choice = 69,
        Laterality = 70,
        QuestionAnswer = 71,
        QuestionAnswerValue = 72,
        EncounterCommunicationWithOtherProviderOrder = 73,
        CommunicationWithOtherProviderOrder = 74,
        ExternalSystemMessage = 75
    }
}