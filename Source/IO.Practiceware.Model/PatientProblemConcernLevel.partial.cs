﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class PatientProblemConcernLevel
    {
        public ConcernLevel ConcernLevel
        {
            get { return (ConcernLevel)ConcernLevelId; }
            set { ConcernLevelId = (int)value; }
        }
    }
    public enum ConcernLevel
    {
        Active = 1,
        Suspended = 2,
        Completed = 3,
        Aborted = 4
    }
}
