//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// For Task Management.  Examples of groups are:  Billing, Drug Refills, Clinical Problem, Scheduling, Surgery Booking.
    /// </summary>
    public partial class Group : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        
        [StringLength(250, ErrorMessage="This field cannot be longer than 250 characters")]
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;

        #endregion

        #region Navigation Properties
    
    	[Association("GroupUsers", "Id", "GroupId")]
        public virtual System.Collections.Generic.ICollection<GroupUser> GroupUsers
        {
            get
            {
                if (_groupUsers == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<GroupUser>();
                    newCollection.ItemSet += FixupGroupUsersItemSet;
                    newCollection.ItemRemoved += FixupGroupUsersItemRemoved;
                    _groupUsers = newCollection;
                }
                return _groupUsers;
            }
            set
            {
                if (!ReferenceEquals(_groupUsers, value))
                {
                    var previousValue = _groupUsers as Soaf.Collections.FixupCollection<GroupUser>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupGroupUsersItemSet;
                        previousValue.ItemRemoved -= FixupGroupUsersItemRemoved;
                    }
                    _groupUsers = value;
                    var newValue = value as Soaf.Collections.FixupCollection<GroupUser>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupGroupUsersItemSet;
                        newValue.ItemRemoved += FixupGroupUsersItemRemoved;
                    }
    				OnPropertyChanged("GroupUsers");
                }
            }
        }
        private System.Collections.Generic.ICollection<GroupUser> _groupUsers;

        #endregion

        #region Association Fixup
    
        private void FixupGroupUsersItemSet(object sender, Soaf.EventArgs<GroupUser> e)
        {
            var item = e.Value;
     
            item.Group = this;
        }
    
        private void FixupGroupUsersItemRemoved(object sender, Soaf.EventArgs<GroupUser> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.Group, this))
            {
                item.Group = null;
            }
        }
    

        #endregion

    }
}
