﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class InvoiceSupplemental
    {
        public ClaimDelayReason? ClaimDelayReason
        {
            get
            {
                if (ClaimDelayReasonId != null) return (ClaimDelayReason)ClaimDelayReasonId.Value;
                return null;
            }

            set
            {
                if (value != null) ClaimDelayReasonId = (int)value;
                else ClaimDelayReasonId = null;
            }
        }

        public bool HasAutoAccidentRelatedCause
        {
            get
            {
                return RelatedCause1Id == (int?)RelatedCause.AutoAccident || RelatedCause2Id == (int?)RelatedCause.AutoAccident || RelatedCause3Id == (int?)RelatedCause.AutoAccident;
            }
        }

        public bool HasEmploymentRelatedCause
        {
            get
            {
                return RelatedCause1Id == (int?)RelatedCause.Employment || RelatedCause2Id == (int?)RelatedCause.Employment || RelatedCause3Id == (int?)RelatedCause.Employment;
            }
        }

        public bool HasOtherAccidentRelatedCause
        {
            get
            {
                return RelatedCause1Id == (int?)RelatedCause.OtherAccident || RelatedCause2Id == (int?)RelatedCause.OtherAccident || RelatedCause3Id == (int?)RelatedCause.OtherAccident;
            }
        }
    }

    public enum RelatedCause
    {
        [Display(Name = "Auto Accident")]
        AutoAccident = 1,
        [Display(Name = "Employment")]
        Employment = 2,
        [Display(Name = "Other Accident")]
        OtherAccident = 3
    }

    public enum ClaimDelayReason
    {
        [Description("Proof of Eligibility Unknown or Unavailable")]
        ProofOfEligibilityUnknown = 1,
        [Description("Litigation")]
        Litigation = 2,
        [Description("Authorization Delays")]
        AuthorizationDelays = 3,
        [Description("Delay in Certifying Provider")]
        DelayInCertifyingProvider = 4,
        [Description("Delay in Supplying Billing Forms")]
        DelayInSupplyingBillingForms = 5,
        [Description("Delay in Delivery of Custom-made Appliances")]
        DelayInDelivery = 6,
        [Description("Third Party Processing Delay")]
        ThirdPartyProcessingDelay = 7,
        [Description("Delay in Eligibility Determination")]
        DelayInEligibilityDetermination = 8,
        [Description("Original claim rejected or Denied Due to a Reason Unrelated to the Billing Limitation Rules")]
        OriginalClaimRejected = 9,
        [Description("Administration Delay in the Prior Approval Process")]
        AdministrationDelay = 10,
        [Description("Other")]
        Other = 11,
        [Description("Natural Disaster")]
        NaturalDisaster = 12
    }
}