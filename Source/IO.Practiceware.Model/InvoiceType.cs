﻿
namespace IO.Practiceware.Model
{
    /// <summary>
    /// All AppointmentTypes have an InvoiceType.  This determines what kind of invoice(s) 
    /// </summary>
    public enum InvoiceType
    {
        Professional = 1,
        Facility = 2,
        Vision = 3,
    }
}
