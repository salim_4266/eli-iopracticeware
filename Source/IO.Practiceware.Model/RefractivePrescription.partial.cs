﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class RefractivePrescription
    {
        public RefractivePrescriptionType RefractivePrescriptionType
        {
            get { return (RefractivePrescriptionType)RefractivePrescriptionTypeId; }
            set { RefractivePrescriptionTypeId = (int)value; }
        }

    }
    public enum RefractivePrescriptionType
    {
        Glasses = 1,
        ContactLenses = 2,
        ContactLensTrials = 3
    }

}
