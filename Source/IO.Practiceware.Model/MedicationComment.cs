//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Pre-fab medication order comments.  They will always be included on script. - Will include tapers and other custom dosages.
    /// </summary>
    public partial class MedicationComment : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Value
        {
            get { return _value; }
            set 
    		{ 
    			var isChanged = _value != value; 
    			_value = value; 
    			if (isChanged) OnPropertyChanged("Value");
    		}
        }
        private string _value;
    
    	/// <summary>
    	/// Prefab comments that can be added to MedicationOrders. - If MedicationId is null, then comment will be available for selection for all medications.  Otherwise, it will be available just for the medication with which it is associated
    	/// </summary>
    	public virtual Nullable<int> MedicationId
        {
            get { return _medicationId; }
            set 
    		{ 
    			var isChanged = _medicationId != value; 
    			_medicationId = value; 
    			if (isChanged) OnPropertyChanged("MedicationId");
    		}
        }
        private Nullable<int> _medicationId;

        #endregion

    }
}
