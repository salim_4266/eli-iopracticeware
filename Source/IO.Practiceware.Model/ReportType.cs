﻿namespace IO.Practiceware.Model
{
    public enum ReportType
    {
        /// <summary>
        ///     A Telerik .trdx file
        /// </summary>
        Trdx = 1,

        /// <summary>
        ///     An IQueryable grid report from the IReportingRepository
        /// </summary>
        ReportingRepository = 2
    }
}