﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum ClinicalDataSourceTypeId
    {
        [Display(Name = "Practice Provider")]
        PracticeProvider = 1,
        [Display(Name = "Patient Or Patient Representative")]
        PatientOrPatientRepresentative = 2,
        [Display(Name = "External Provider")]
        ExternalProvider = 3,
        [Display(Name = "Billing Record")]
        BillingRecord = 4
    }

}
