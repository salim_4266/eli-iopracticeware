﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class PatientComment
    {
        public PatientCommentType PatientCommentType
        {
            get { return (PatientCommentType)PatientCommentTypeId; }
            set { PatientCommentTypeId = (int)value; }
        }
    }

    public enum PatientCommentType
    {
        [Display(Name="General")]
        General = 1,

        [Display(Name="Patient Demographic Summary")]
        PatientDemographicSummary = 2,

        [Display(Name = "Patient Communication Preference Notes")]
        PatientCommunicationPreferenceNotes = 3
    }
}
