﻿using Soaf;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Media;

namespace IO.Practiceware.Model
{
    public partial class EncounterStatusConfiguration
    {
        /// <summary>
        /// Gets the name of this status.
        /// </summary>
        public string Name
        {
            get { return ((EncounterStatus)Id).GetAttribute<DisplayAttribute>().Name; }
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public Color Color
        {
            get
            {
                var hexColor = (HexColor.StartsWith("#") ? HexColor : "#" + HexColor);

                var colorObj = ColorConverter.ConvertFromString(hexColor);
                if (colorObj == null)
                {
                    throw new NullReferenceException("Could not convert the HexColor {0} to a Color".FormatWith(hexColor));
                }
                return (Color) colorObj;
            }
        }
    }
}
