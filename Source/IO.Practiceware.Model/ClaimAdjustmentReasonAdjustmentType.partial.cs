﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class ClaimAdjustmentReasonAdjustmentType
    {
        public ClaimAdjustmentGroupCode ClaimAdjustmentGroupCode
        {
            get { return (ClaimAdjustmentGroupCode)ClaimAdjustmentGroupCodeId; }
            set { ClaimAdjustmentGroupCodeId = (int)value; }
        }
    }

    public enum ClaimAdjustmentGroupCode
    {
        [Display(Name = "Contractual Obligation")]
        ContractualObligation = 1,
        [Display(Name = "Patient Responsibility")]
        PatientResponsibility = 2,
        [Display(Name = "Other Adjustment")]
        OtherAdjustment = 3,
        [Display(Name = "Correction and Reversal")]
        CorrectionReversal = 4,
        [Display(Name = "Payer Initiated Reductions")]
        PayerInitiatedReduction = 5
    }
}