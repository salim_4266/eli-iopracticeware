﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Metadata attributes for TaskActivity.
    /// </summary>
    public class TaskActivityMetadata
    {
        [Description("Created")]
        public DateTime? CreatedDateTime { get; set; }

        [Description("Assigned Individually")]
        public bool AssignedIndividually { get; set; }
    }

    [MetadataType(typeof(TaskActivityMetadata))]
    public partial class TaskActivity
    {
        [Description("Assigned To")]
        public string AssignedTo
        {
            get
            {
                var sb = new StringBuilder();

                foreach (var tar in TaskActivityUsers)
                {
                    if (tar.AssignedTo == null)
                    {
                        throw new InvalidOperationException("User is not loaded.");
                    }

                    sb.Append(tar.AssignedTo.DisplayName);

                    if (tar.AlertActive)
                    {
                        sb.Append(" (Alarm Active)");
                    }
                    else if (tar.Active)
                    {
                        sb.Append(" (Active)");
                    }
                    else
                    {
                        sb.Append(" (Complete)");
                    }

                    if (tar != TaskActivityUsers.LastOrDefault())
                    {
                        sb.Append(", ");
                    }
                }
                return sb.ToString();
            }
        }

        [Description("Complete")]
        public bool IsComplete
        {
            get { return TaskActivityUsers.All(tar => !tar.Active); }
        }

        [Description("Completed")]
        public DateTime? CompletedDateTime
        {
            get { return IsComplete ? TaskActivityUsers.Select(tar => tar.CompletedDateTime).LastOrDefault() : null; }
        }

        [Description("Type")]
        public string TaskActivityTypeName
        {
            get { return TaskActivityType != null ? TaskActivityType.Name : string.Empty; }
        }

        public string Status
        {
            get { return IsComplete ? "Complete" : "Active"; }
        }

        [Description("Assigned By")]
        public string AssignedBy
        {
            get { return CreatedBy.DisplayName; }
        }
    }
}
