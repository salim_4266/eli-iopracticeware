﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum DiagnosisTypeId
    {
        [Display(Name = "ICD 9")]
        Icd9 = 1,
        [Display(Name = "ICD 10")]
        Icd10 = 2
    }
}
