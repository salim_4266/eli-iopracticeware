﻿
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum AdjustmentTypeId
    {
        Payment = 1,
        [Display(Name = "Contractual Adjustment")]
        ContractualAdjustment = 2,
        WriteOff = 3,
        [Display(Name = "Refund Unspec")]
        Refund = 4,
        Reversal = 5
    }
}