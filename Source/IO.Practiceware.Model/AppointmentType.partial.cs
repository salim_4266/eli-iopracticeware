﻿namespace IO.Practiceware.Model
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AppointmentType
    {
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public System.Windows.Media.Color Color
        {
            get
            {

                System.Drawing.Color drawingColor = OleColor.HasValue ? System.Drawing.ColorTranslator.FromOle(OleColor.Value) : System.Drawing.Color.Empty;
                return System.Windows.Media.Color.FromArgb(drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B);
            }
            set
            {
                System.Drawing.Color drawingColor = System.Drawing.Color.FromArgb(value.A, value.R, value.G, value.B);
                OleColor = drawingColor == System.Drawing.Color.Empty ? new int?() : System.Drawing.ColorTranslator.ToOle(drawingColor);
            }
        }
    }
}