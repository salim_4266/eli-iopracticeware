﻿
namespace IO.Practiceware.Model
{
    public enum FinancialSourceType
    {
        Insurer = 1,
        Patient = 2,
        BillingOrganization = 3,
        ExternalOrganization = 4
    }
}
