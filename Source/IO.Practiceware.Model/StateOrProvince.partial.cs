﻿namespace IO.Practiceware.Model
{
    public partial class StateOrProvince
    {
        public bool IsInUnitedStates
        {
            get { return CountryId == (int) Model.CountryId.UnitedStates; }
        }

        public bool IsInCanada
        {
            get { return CountryId == (int) Model.CountryId.Canada; }
        }
    }
}