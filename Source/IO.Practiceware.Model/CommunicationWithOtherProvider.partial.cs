﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum CommunicationReviewStatusId
    {
        [Display(Name = "Not Reviewed")]
        NotReviewed = 1,
        [Display(Name = "Ready to send")]
        ReadyToSend = 2
    }

    public enum CommunicationTransmissionStatus
    {
        Queued = 1,
        Sent = 2,
        Removed = 3
    }

    public partial class CommunicationTransaction
    {
        public CommunicationTransmissionStatus CommunicationTransmissionStatus
        {
            get { return (CommunicationTransmissionStatus) CommunicationTransmissionStatusId; }
            set { CommunicationTransmissionStatusId = (int) value; }
        }
    }
}
