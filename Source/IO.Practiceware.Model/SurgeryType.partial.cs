﻿
namespace IO.Practiceware.Model
{
    public partial class SurgeryType
    {
        public override string ToString()
        {
            return Name;
        }
    }
}
