﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Model
{
    /// <summary>
    ///     This class represents data that will be stored as Xml in the ApplicationSettings table.  The Application Settings table can contain
    ///     many different types of Configuration Settings.  This is one of those types.  Particularly this class represents
    ///     a saved filter settings within the 'Submit Transactions' screen.  Saved filters are at the User level. Users can have more than one saved filter.
    /// </summary>
    [DataContract]
    public class SubmitTransactionsApplicationSetting : IApplicationSetting
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ClaimType { get; set; }

        [DataMember]
        public int? PaperClaimFormId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public List<int> ElectronicClaimFormIds { get; set; }

        [DataMember]
        public virtual List<int> SelectedInsurers { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedScheduledDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedLocations { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingOrganizations { get; set; }
    }
}