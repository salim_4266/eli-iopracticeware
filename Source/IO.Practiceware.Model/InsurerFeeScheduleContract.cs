//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Links an Insurer to a FeeScheduleContract for a particular date range.  Insurers can be linked to multiple fee schedules for different time periods.  Only one schedule for any insurer/date.
    /// </summary>
    public partial class InsurerFeeScheduleContract : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required]
    	public virtual System.DateTime StartDateTime
        {
            get { return _startDateTime; }
            set 
    		{ 
    			var isChanged = _startDateTime != value; 
    			_startDateTime = value; 
    			if (isChanged) OnPropertyChanged("StartDateTime");
    		}
        }
        private System.DateTime _startDateTime;
    
    	public virtual Nullable<System.DateTime> EndDateTime
        {
            get { return _endDateTime; }
            set 
    		{ 
    			var isChanged = _endDateTime != value; 
    			_endDateTime = value; 
    			if (isChanged) OnPropertyChanged("EndDateTime");
    		}
        }
        private Nullable<System.DateTime> _endDateTime;
    
    	public virtual int InsurerId
        {
            get { return _insurerId; }
            set
            {
                if (_insurerId != value)
                {
                    if (Insurer != null && Insurer.Id != value)
                    {
                        Insurer = null;
                    }
                    _insurerId = value;
    
    				OnPropertyChanged("InsurerId");
                }
            }
        }
        private int _insurerId;
    
    	public virtual int FeeScheduleContractId
        {
            get { return _feeScheduleContractId; }
            set
            {
                if (_feeScheduleContractId != value)
                {
                    if (FeeScheduleContract != null && FeeScheduleContract.Id != value)
                    {
                        FeeScheduleContract = null;
                    }
                    _feeScheduleContractId = value;
    
    				OnPropertyChanged("FeeScheduleContractId");
                }
            }
        }
        private int _feeScheduleContractId;

        #endregion

        #region Navigation Properties
    
    	[Association("Insurer", "InsurerId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Insurer Insurer
        {
            get { return _insurer; }
            set
            {
                if (!ReferenceEquals(_insurer, value))
                {
                    var previousValue = _insurer;
                    _insurer = value;
                    FixupInsurer(previousValue);
    				OnPropertyChanged("Insurer");
                }
            }
        }
        private Insurer _insurer;
    
    	[Association("FeeScheduleContract", "FeeScheduleContractId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual FeeScheduleContract FeeScheduleContract
        {
            get { return _feeScheduleContract; }
            set
            {
                if (!ReferenceEquals(_feeScheduleContract, value))
                {
                    var previousValue = _feeScheduleContract;
                    _feeScheduleContract = value;
                    FixupFeeScheduleContract(previousValue);
    				OnPropertyChanged("FeeScheduleContract");
                }
            }
        }
        private FeeScheduleContract _feeScheduleContract;

        #endregion

        #region Association Fixup
    
        private void FixupInsurer(Insurer previousValue)
        {
            if (previousValue != null && previousValue.InsurerFeeScheduleContracts.Contains(this))
            {
                previousValue.InsurerFeeScheduleContracts.Remove(this);
            }
    
            if (Insurer != null)
            {
                if (!Insurer.InsurerFeeScheduleContracts.Contains(this))
                {
                    Insurer.InsurerFeeScheduleContracts.Add(this);
                }
                if (Insurer != null && InsurerId != Insurer.Id)
                {
                    InsurerId = Insurer.Id;
                }
            }
        }
    
        private void FixupFeeScheduleContract(FeeScheduleContract previousValue)
        {
            if (previousValue != null && previousValue.InsurerFeeScheduleContracts.Contains(this))
            {
                previousValue.InsurerFeeScheduleContracts.Remove(this);
            }
    
            if (FeeScheduleContract != null)
            {
                if (!FeeScheduleContract.InsurerFeeScheduleContracts.Contains(this))
                {
                    FeeScheduleContract.InsurerFeeScheduleContracts.Add(this);
                }
                if (FeeScheduleContract != null && FeeScheduleContractId != FeeScheduleContract.Id)
                {
                    FeeScheduleContractId = FeeScheduleContract.Id;
                }
            }
        }

        #endregion

    }
}
