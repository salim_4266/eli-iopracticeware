﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class FacilityEncounterService
    {
        public ServiceUnitOfMeasurement ServiceUnitOfMeasurement
        {
            get { return (ServiceUnitOfMeasurement)ServiceUnitOfMeasurementId; }
            set { ServiceUnitOfMeasurementId = (int)value; }
        }
    }
}
