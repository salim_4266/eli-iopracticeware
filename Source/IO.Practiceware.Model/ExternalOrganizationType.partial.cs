﻿
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum ExternalOrganizationTypeId
    {
        Clinic = 1,
        [Display(Name = "Diagnostic Test Laboratory")]
        DiagnosticTestLaboratory = 2,
        Hospital = 3,
        Institution = 4,
        [Display(Name = "Medical Practice")]
        MedicalPractice = 5,
        [Display(Name = "Nursing Home")]
        NursingHome = 6,
        Vendor = 7,
        Other = 8

    }

}
