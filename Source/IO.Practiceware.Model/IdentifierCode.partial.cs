﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class IdentifierCode
    {
        public IdentifierCodeType IdentifierCodeType
        {
            get { return (IdentifierCodeType) IdentifierCodeTypeId; }
            set { IdentifierCodeTypeId = (int) value; }
        }
    }

    /// <summary>
    ///   Helper methods for identifier codes.
    /// </summary>
    public static class IdentifierCodes
    {
        /// <summary>
        ///   Gets the value of the first identifier code matching the specified type.
        /// </summary>
        /// <param name="source"> The source. </param>
        /// <param name="type"> The type. </param>
        /// <returns> </returns>
        public static string WithIdentifierCodeType(this IEnumerable<IdentifierCode> source, IdentifierCodeType type)
        {
            return source.Where(i => i.IdentifierCodeType == type).Select(i => i.Value).FirstOrDefault();
        }
    }

    public enum IdentifierCodeType
    {
        Taxonomy = 1,
        [StringLength(10)] Npi = 2,
        [StringLength(9)] Ssn = 3,
        [StringLength(6)] Upin = 4,
        License = 5,
        [StringLength(9)] Dea = 6,
        [StringLength(9)] TaxId = 7,
        EValidation = 8,
        [StringLength(10)] CliaCertNumber = 9
    }
}