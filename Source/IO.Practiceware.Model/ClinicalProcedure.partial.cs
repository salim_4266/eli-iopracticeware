﻿using System;
using Soaf.ComponentModel;

namespace IO.Practiceware.Model
{
    public partial class ClinicalProcedure
    {
        public ClinicalProcedureCategory ClinicalProcedureCategory
        {
            get { return (ClinicalProcedureCategory) ClinicalProcedureCategoryId; }
            set { ClinicalProcedureCategoryId = (int) value; }
        }
    }

    public partial class PatientProcedurePerformed : IClinicalProcedurePerformed
    {
    }

    public partial class PatientDiagnosticTestPerformed : IClinicalProcedurePerformed
    {
    }

    /// <summary>
    ///     Describes a type associated with performing a ClinicalProcedure
    /// </summary>
    public interface IClinicalProcedurePerformed : IHasId
    {
        ClinicalProcedure ClinicalProcedure { get; }

        DateTime PerformedDateTime { get; }
    }

    public enum ClinicalProcedureCategory
    {
        /// <summary>
        /// something the provider does to a patient without the intent to alter how the body is working (eg. applying a bandage)
        /// </summary>
        Act = 1, 
        DiagnosticTest = 2,
        /// <summary>
        /// something the provider does to a patient with the intent to alter how the body is working (eg. surgery)
        /// </summary>
        Procedure = 3,
        Exam = 4
    }
}