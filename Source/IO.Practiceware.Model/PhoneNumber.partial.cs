﻿using PhoneNumbers;
using Soaf;
using Soaf.Collections;
using Soaf.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace IO.Practiceware.Model
{
    public interface IPhoneNumber : IHasOrdinalId
    {
        int Id { get; set; }

        [Display(Name = "Exchange and Suffix")]
        string ExchangeAndSuffix { get; set; }

        [Display(Name = "Extension")]
        string Extension { get; set; }

        [Display(Name = "International")]
        bool IsInternational { get; set; }

        [Display(Name = "Area Code")]
        string AreaCode { get; set; }

        [Display(Name = "Country Code")]
        string CountryCode { get; set; }

        [Display(Name = "Phone Type")]
        IPhoneNumberType PhoneNumberType { get; set; }
    }

    public interface IPhoneNumberType
    {
        int Id { get; }
        string Name { get; set; }
        string Abbreviation { get; set; }
        bool IsArchived { get; set; }

        string ToString();
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class BillingOrganizationPhoneNumber : IPhoneNumber
    {
        public BillingOrganizationPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => BillingOrganization.IfNotNull(p => p.BillingOrganizationPhoneNumbers),
                Reflector.GetMember(() => BillingOrganization).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return BillingOrganizationPhoneNumberType; }
            set { BillingOrganizationPhoneNumberType = (BillingOrganizationPhoneNumberType)value; }
        }
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class ExternalContactPhoneNumber : IPhoneNumber
    {
        public ExternalContactPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => ExternalContact.IfNotNull(p => p.ExternalContactPhoneNumbers),
                Reflector.GetMember(() => ExternalContact).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return ExternalContactPhoneNumberType; }
            set { ExternalContactPhoneNumberType = (ExternalContactPhoneNumberType)value; }
        }
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class ExternalOrganizationPhoneNumber : IPhoneNumber
    {
        public ExternalOrganizationPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => ExternalOrganization.IfNotNull(p => p.ExternalOrganizationPhoneNumbers),
                Reflector.GetMember(() => ExternalOrganization).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return ExternalOrganizationPhoneNumberType; }
            set { ExternalOrganizationPhoneNumberType = (ExternalOrganizationPhoneNumberType)value; }
        }
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class InsurerPhoneNumber : IPhoneNumber
    {
        public InsurerPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => Insurer.IfNotNull(p => p.InsurerPhoneNumbers),
                Reflector.GetMember(() => Insurer).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return InsurerPhoneNumberType; }
            set { InsurerPhoneNumberType = (InsurerPhoneNumberType)value; }
        }
    }


    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class PatientPhoneNumber : IPhoneNumber
    {
        public PatientPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => Patient.IfNotNull(p => p.PatientPhoneNumbers),
                Reflector.GetMember(() => Patient).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public PatientPhoneNumberType PatientPhoneNumberType
        {
            get { return PatientPhoneNumberTypeId; }
            set { PatientPhoneNumberTypeId = value; }
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return PatientPhoneNumberType == 0 ? PatientPhoneNumberType = PatientPhoneNumberType.Unknown : PatientPhoneNumberType; }
            set { PatientPhoneNumberType = (PatientPhoneNumberType)value; }
        }
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class ServiceLocationPhoneNumber : IPhoneNumber
    {
        public ServiceLocationPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => ServiceLocation.IfNotNull(p => p.ServiceLocationPhoneNumbers),
                Reflector.GetMember(() => ServiceLocation).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType
        {
            get { return ServiceLocationPhoneNumberType; }
            set { ServiceLocationPhoneNumberType = (ServiceLocationPhoneNumberType)value; }
        }
    }

    [DisplayName("Phone")]
    [MetadataType(typeof(IPhoneNumber))]
    public partial class UserPhoneNumber : IPhoneNumber
    {
        public UserPhoneNumber()
        {
            PropertyChanged += OrdinalIdExtensions.CreateOrdinalIdFixHandler(this, () => User.IfNotNull(p => p.UserPhoneNumbers),
                Reflector.GetMember(() => User).Name);
        }

        public override string ToString()
        {
            return this.GetFormattedValue();
        }

        public IPhoneNumberType PhoneNumberType { get { return UserPhoneNumberType; } set { UserPhoneNumberType = (UserPhoneNumberType)value; } }
    }

    public partial class BillingOrganizationPhoneNumberType : IPhoneNumberType
    {

    }

    public partial class ExternalContactPhoneNumberType : IPhoneNumberType
    {

    }

    public partial class ExternalOrganizationPhoneNumberType : IPhoneNumberType
    {

    }

    public partial class InsurerPhoneNumberType : IPhoneNumberType
    {

    }


    public partial class ServiceLocationPhoneNumberType : IPhoneNumberType
    {

    }

    public partial class UserPhoneNumberType : IPhoneNumberType
    {

    }

    /// <summary>
    ///   Extension and helpers for phone number types.
    /// </summary>
    public static class PhoneNumbers
    {
        /// <summary>
        ///   Gets the phone number of the specified type or adds one.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="type"> The type. </param>
        /// <param name="notFoundBehavior"> The not found behavior. </param>
        /// <returns> </returns>
        public static T WithPhoneNumberType<T>(this ICollection<T> source, Enum type, NotFoundBehavior notFoundBehavior = NotFoundBehavior.None) where T : IPhoneNumber, new()
        {
            return source.WithPhoneNumberType(Convert.ToInt32(type), notFoundBehavior);
        }

        /// <summary>
        /// Gets the phone number of the specified type or adds one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static T WithPhoneNumberType<T>(this ICollection<T> source, int type) where T : IPhoneNumber, new()
        {
            return source.WithPhoneNumberType(type, NotFoundBehavior.None);
        }

        /// <summary>
        ///   Gets the phone number of the specified type or adds one.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="type"> The type. </param>
        /// <param name="notFoundBehavior"> The not found behavior. </param>
        /// <returns> </returns>
        public static T WithPhoneNumberType<T>(this ICollection<T> source, int type, NotFoundBehavior notFoundBehavior) where T : IPhoneNumber, new()
        {
            if (notFoundBehavior == NotFoundBehavior.None && source.IsNullOrEmpty()) return default(T);

            var phoneNumberTypeProperty = typeof(T).GetProperties().FirstOrDefault(p => typeof(IPhoneNumberType).IsAssignableFrom(p.PropertyType) && p.PropertyType != typeof(IPhoneNumberType)).EnsureNotDefault("Could not phone number type for entity {0}.".FormatWith(typeof(T).Name));

            var phoneNumberTypeIdProperty = typeof(T).GetProperty(phoneNumberTypeProperty.Name + "Id");

            T phoneNumber;

            if (phoneNumberTypeIdProperty == null)
            {
                phoneNumber = source.FirstOrDefault(a => ((IPhoneNumberType)phoneNumberTypeProperty.GetValue(a, null)).Id == type);
            }
            else
            {
                phoneNumber = source.FirstOrDefault(a => (int)phoneNumberTypeIdProperty.GetValue(a, null) == type);
            }

            if (Equals(phoneNumber, default(T)))
            {
                if (notFoundBehavior == NotFoundBehavior.Exception)
                    throw new ApplicationException("Could not find phone number with type {0}.".FormatWith(type));
                if (notFoundBehavior == NotFoundBehavior.Add)
                {
                    phoneNumber = Activator.CreateInstance<T>();

                    if (phoneNumberTypeIdProperty != null)
                    {
                        phoneNumberTypeIdProperty.SetValue(phoneNumber, type, null);
                    }
                    else
                    {
                        var phoneNumberType = Activator.CreateInstance(phoneNumberTypeProperty.PropertyType, type);
                        phoneNumber.PhoneNumberType = (IPhoneNumberType)phoneNumberType;
                    }

                    source.Add(phoneNumber);
                }
            }
            return phoneNumber;
        }
    }

    public static class PhoneNumberParserExtensions
    {
        const string UnknownRegion = "ZZ";

        /// <summary>
        /// Gets the nonformatted parsed extension
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string GetParsedExtension(this IPhoneNumber phoneNumber)
        {
            var externalLibPhoneNumber = GetExternalLibPhoneNumber(phoneNumber);
            return externalLibPhoneNumber == null ? string.Empty : externalLibPhoneNumber.Extension;
        }

        /// <summary>
        /// Gets the nonformatted parsed extension
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string GetParsedExtension(string phoneNumber, int countryCode = 1)
        {
            var externalLibPhoneNumber = GetExternalLibPhoneNumber(phoneNumber, countryCode);
            return externalLibPhoneNumber == null ? string.Empty : externalLibPhoneNumber.Extension;
        }

        /// <summary>
        /// Gets the nonformatted parsed national number which includes the area code, exchange and suffix.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string GetParsedNationalNumber(this IPhoneNumber phoneNumber)
        {
            var externalLibPhoneNumber = GetExternalLibPhoneNumber(phoneNumber);
            return externalLibPhoneNumber == null ? string.Empty : externalLibPhoneNumber.NationalNumber.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the nonformatted parsed national number which includes the area code, exchange and suffix.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string GetParsedNationalNumber(string phoneNumber, int countryCode = 1)
        {
            var externalLibPhoneNumber = GetExternalLibPhoneNumber(phoneNumber, countryCode);
            return externalLibPhoneNumber == null ? string.Empty : externalLibPhoneNumber.NationalNumber.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Sets phone number values. Works only with US numbers, since attempts to get area code as first 3 digits
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="phoneNumberValue"></param>
        /// <param name="countryCode"></param>
        public static void SetValuesFromString(this IPhoneNumber phoneNumber, string phoneNumberValue, int countryCode = 1)
        {
            if (GetExternalLibPhoneNumber(phoneNumberValue) == null)
                throw new ArgumentException("Could not parse phone number {0}".FormatWith(phoneNumberValue));

            var areaCode = GetParsedNationalNumber(phoneNumberValue).Substring(0, 3);
            var exchangeAndSuffix = GetParsedNationalNumber(phoneNumberValue).Substring(3);
            var extension = GetParsedExtension(phoneNumberValue);

            phoneNumber.AreaCode = areaCode;
            phoneNumber.CountryCode = countryCode.ToString(CultureInfo.InvariantCulture);
            phoneNumber.ExchangeAndSuffix = exchangeAndSuffix;
            phoneNumber.Extension = extension;
            phoneNumber.IsInternational = countryCode != 1;
        }

        /// <summary>
        /// Validate phone number values if phone number is valid and return true else return false
        /// </summary>
        /// <param name="phoneNumberValue"></param>
        public static bool IsValidPhoneNumber(string phoneNumberValue)
        {
            //Checking for Valid  phone number and must have length greater than 3
            if (GetExternalLibPhoneNumber(phoneNumberValue) == null || (GetParsedNationalNumber(phoneNumberValue).Length <= 3))
                return false;
            return true;
        }

        /// <summary>
        /// Attempts to format the phone number according to it's region format
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string GetFormattedValue(this IPhoneNumber phoneNumber)
        {
            string result = null;

            var externalLibPhoneNumber = phoneNumber.GetExternalLibPhoneNumber();
            if (externalLibPhoneNumber != null)
            {
                var format = phoneNumber.IsInternational ? PhoneNumberFormat.INTERNATIONAL : PhoneNumberFormat.NATIONAL;
                var phoneNumberUtility = PhoneNumberUtil.GetInstance();
                result = phoneNumberUtility.Format(externalLibPhoneNumber, format);
            }

            return result ?? "{0}{1}x{2}".FormatWith(phoneNumber.AreaCode, phoneNumber.ExchangeAndSuffix, phoneNumber.Extension).ToNumeric();
        }

        [DebuggerNonUserCode]
        internal static PhoneNumber GetExternalLibPhoneNumber(this IPhoneNumber phoneNumber)
        {
            int countryCode;
            if (!int.TryParse(phoneNumber.CountryCode, out countryCode))
            {
                // Default country code to North America (US)
                countryCode = 1;
            }

            var phoneNumberString = "{0}{1}x{2}".FormatWith(phoneNumber.AreaCode, phoneNumber.ExchangeAndSuffix, phoneNumber.Extension);
            var phoneNumberUtility = PhoneNumberUtil.GetInstance();
            var region = phoneNumberUtility.GetRegionCodeForCountryCode(countryCode);

            // If region is unknown -> parsing will fail anyway
            if (region == UnknownRegion) return null;

            try
            {
                // Attempt to parse the phone for this region
                return phoneNumberUtility.Parse(phoneNumberString, region);
            }
            catch (Exception)
            {
                return null;
            }
        }

        [DebuggerNonUserCode]
        internal static PhoneNumber GetExternalLibPhoneNumber(string phoneNumber, int countryCode = 1)
        {
            var phoneNumberUtility = PhoneNumberUtil.GetInstance();
            var region = phoneNumberUtility.GetRegionCodeForCountryCode(countryCode);
            // If region is unknown -> parsing will fail anyway
            if (region == UnknownRegion) return null;

            try
            {
                return phoneNumberUtility.Parse(phoneNumber, region);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static PhoneNumber GetParsedPhoneNumber(string phoneNumber, int countryCode = 1)
        {
            return GetExternalLibPhoneNumber(phoneNumber, countryCode);
        }
    }
}