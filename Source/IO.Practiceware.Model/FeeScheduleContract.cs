//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// The is the master list of fee schedules.  These fee schedules are named INSURERYYYYMMXX where INSURER is the insurer name, YYYYMM is the fee schedule start month/year, and XX is the fee schedule end date, if any. -  Examples: AETNA20000101, AETNA200301, OXFORD HEALTHPLANS200301.  There are no dollar amounts, InsurerIds or CPT codes in this entity. Dollar amounts are in FeeScheduleContractAmount.  InsurerIds are in InsurerFeeScheduleContract
    /// </summary>
    public partial class FeeScheduleContract : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;

        #endregion

        #region Navigation Properties
    
    	[Association("InsurerFeeScheduleContracts", "Id", "FeeScheduleContractId")]
        public virtual System.Collections.Generic.ICollection<InsurerFeeScheduleContract> InsurerFeeScheduleContracts
        {
            get
            {
                if (_insurerFeeScheduleContracts == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<InsurerFeeScheduleContract>();
                    newCollection.ItemSet += FixupInsurerFeeScheduleContractsItemSet;
                    newCollection.ItemRemoved += FixupInsurerFeeScheduleContractsItemRemoved;
                    _insurerFeeScheduleContracts = newCollection;
                }
                return _insurerFeeScheduleContracts;
            }
            set
            {
                if (!ReferenceEquals(_insurerFeeScheduleContracts, value))
                {
                    var previousValue = _insurerFeeScheduleContracts as Soaf.Collections.FixupCollection<InsurerFeeScheduleContract>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupInsurerFeeScheduleContractsItemSet;
                        previousValue.ItemRemoved -= FixupInsurerFeeScheduleContractsItemRemoved;
                    }
                    _insurerFeeScheduleContracts = value;
                    var newValue = value as Soaf.Collections.FixupCollection<InsurerFeeScheduleContract>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupInsurerFeeScheduleContractsItemSet;
                        newValue.ItemRemoved += FixupInsurerFeeScheduleContractsItemRemoved;
                    }
    				OnPropertyChanged("InsurerFeeScheduleContracts");
                }
            }
        }
        private System.Collections.Generic.ICollection<InsurerFeeScheduleContract> _insurerFeeScheduleContracts;
    
    	[Association("FeeScheduleContractAmounts", "Id", "FeeScheduleContractId")]
        public virtual System.Collections.Generic.ICollection<FeeScheduleContractAmount> FeeScheduleContractAmounts
        {
            get
            {
                if (_feeScheduleContractAmounts == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<FeeScheduleContractAmount>();
                    newCollection.ItemSet += FixupFeeScheduleContractAmountsItemSet;
                    newCollection.ItemRemoved += FixupFeeScheduleContractAmountsItemRemoved;
                    _feeScheduleContractAmounts = newCollection;
                }
                return _feeScheduleContractAmounts;
            }
            set
            {
                if (!ReferenceEquals(_feeScheduleContractAmounts, value))
                {
                    var previousValue = _feeScheduleContractAmounts as Soaf.Collections.FixupCollection<FeeScheduleContractAmount>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupFeeScheduleContractAmountsItemSet;
                        previousValue.ItemRemoved -= FixupFeeScheduleContractAmountsItemRemoved;
                    }
                    _feeScheduleContractAmounts = value;
                    var newValue = value as Soaf.Collections.FixupCollection<FeeScheduleContractAmount>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupFeeScheduleContractAmountsItemSet;
                        newValue.ItemRemoved += FixupFeeScheduleContractAmountsItemRemoved;
                    }
    				OnPropertyChanged("FeeScheduleContractAmounts");
                }
            }
        }
        private System.Collections.Generic.ICollection<FeeScheduleContractAmount> _feeScheduleContractAmounts;

        #endregion

        #region Association Fixup
    
        private void FixupInsurerFeeScheduleContractsItemSet(object sender, Soaf.EventArgs<InsurerFeeScheduleContract> e)
        {
            var item = e.Value;
     
            item.FeeScheduleContract = this;
        }
    
        private void FixupInsurerFeeScheduleContractsItemRemoved(object sender, Soaf.EventArgs<InsurerFeeScheduleContract> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.FeeScheduleContract, this))
            {
                item.FeeScheduleContract = null;
            }
        }
    
    
        private void FixupFeeScheduleContractAmountsItemSet(object sender, Soaf.EventArgs<FeeScheduleContractAmount> e)
        {
            var item = e.Value;
     
            item.FeeScheduleContract = this;
        }
    
        private void FixupFeeScheduleContractAmountsItemRemoved(object sender, Soaf.EventArgs<FeeScheduleContractAmount> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.FeeScheduleContract, this))
            {
                item.FeeScheduleContract = null;
            }
        }
    

        #endregion

    }
}
