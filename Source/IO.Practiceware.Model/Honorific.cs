﻿namespace IO.Practiceware.Model
{
    public enum Honorific
    {
        DDS,

        DO,

        LVN,

        MD,

        NP,

        PA,

        RN,

        PhD,

        PharmD,

        RPh,

        MA,

        OD,

        CNP,

        CNM,

        RPAC,

        FACC,

        FACP,

        LPN,

        Esq,

        DPM,

        PAC,

        CNS,

        RD,

        CRNP,

        FNP,

        ANP,

        GNP,

        PNP,

        APRN,

        ARNP,

        ABOC,
    }
}
