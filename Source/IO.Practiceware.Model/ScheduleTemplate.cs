//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public partial class ScheduleTemplate : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	/// <summary>
    	/// This is the time at which this template will start. 
    	/// </summary>
    	public virtual Nullable<System.DateTime> StartTime
        {
            get { return _startTime; }
            set 
    		{ 
    			var isChanged = _startTime != value; 
    			_startTime = value; 
    			if (isChanged) OnPropertyChanged("StartTime");
    		}
        }
        private Nullable<System.DateTime> _startTime;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;

        #endregion

        #region Navigation Properties
    
    	[Association("ScheduleTemplateBlocks", "Id", "ScheduleTemplateId")]
        public virtual System.Collections.Generic.ICollection<ScheduleTemplateBlock> ScheduleTemplateBlocks
        {
            get
            {
                if (_scheduleTemplateBlocks == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ScheduleTemplateBlock>();
                    newCollection.ItemSet += FixupScheduleTemplateBlocksItemSet;
                    newCollection.ItemRemoved += FixupScheduleTemplateBlocksItemRemoved;
                    _scheduleTemplateBlocks = newCollection;
                }
                return _scheduleTemplateBlocks;
            }
            set
            {
                if (!ReferenceEquals(_scheduleTemplateBlocks, value))
                {
                    var previousValue = _scheduleTemplateBlocks as Soaf.Collections.FixupCollection<ScheduleTemplateBlock>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupScheduleTemplateBlocksItemSet;
                        previousValue.ItemRemoved -= FixupScheduleTemplateBlocksItemRemoved;
                    }
                    _scheduleTemplateBlocks = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ScheduleTemplateBlock>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupScheduleTemplateBlocksItemSet;
                        newValue.ItemRemoved += FixupScheduleTemplateBlocksItemRemoved;
                    }
    				OnPropertyChanged("ScheduleTemplateBlocks");
                }
            }
        }
        private System.Collections.Generic.ICollection<ScheduleTemplateBlock> _scheduleTemplateBlocks;
    
    	[Association("ScheduleBlocks", "Id", "ScheduleTemplateId")]
        public virtual System.Collections.Generic.ICollection<ScheduleBlock> ScheduleBlocks
        {
            get
            {
                if (_scheduleBlocks == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ScheduleBlock>();
                    newCollection.ItemSet += FixupScheduleBlocksItemSet;
                    newCollection.ItemRemoved += FixupScheduleBlocksItemRemoved;
                    _scheduleBlocks = newCollection;
                }
                return _scheduleBlocks;
            }
            set
            {
                if (!ReferenceEquals(_scheduleBlocks, value))
                {
                    var previousValue = _scheduleBlocks as Soaf.Collections.FixupCollection<ScheduleBlock>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupScheduleBlocksItemSet;
                        previousValue.ItemRemoved -= FixupScheduleBlocksItemRemoved;
                    }
                    _scheduleBlocks = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ScheduleBlock>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupScheduleBlocksItemSet;
                        newValue.ItemRemoved += FixupScheduleBlocksItemRemoved;
                    }
    				OnPropertyChanged("ScheduleBlocks");
                }
            }
        }
        private System.Collections.Generic.ICollection<ScheduleBlock> _scheduleBlocks;

        #endregion

        #region Association Fixup
    
        private void FixupScheduleTemplateBlocksItemSet(object sender, Soaf.EventArgs<ScheduleTemplateBlock> e)
        {
            var item = e.Value;
     
            item.ScheduleTemplate = this;
        }
    
        private void FixupScheduleTemplateBlocksItemRemoved(object sender, Soaf.EventArgs<ScheduleTemplateBlock> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ScheduleTemplate, this))
            {
                item.ScheduleTemplate = null;
            }
        }
    
    
        private void FixupScheduleBlocksItemSet(object sender, Soaf.EventArgs<ScheduleBlock> e)
        {
            var item = e.Value;
     
            item.ScheduleTemplate = this;
        }
    
        private void FixupScheduleBlocksItemRemoved(object sender, Soaf.EventArgs<ScheduleBlock> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ScheduleTemplate, this))
            {
                item.ScheduleTemplate = null;
            }
        }
    

        #endregion

    }
}
