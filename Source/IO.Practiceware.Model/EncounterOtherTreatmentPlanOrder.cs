//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Doctor's order for a practice-defined treatment for the patient, such as "return if needed"
    /// </summary>
    public partial class EncounterOtherTreatmentPlanOrder : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	public virtual int EncounterId
        {
            get { return _encounterId; }
            set
            {
                if (_encounterId != value)
                {
                    if (Encounter != null && Encounter.Id != value)
                    {
                        Encounter = null;
                    }
                    _encounterId = value;
    
    				OnPropertyChanged("EncounterId");
                }
            }
        }
        private int _encounterId;
    
    	public virtual int OtherTreatmentPlanOrderId
        {
            get { return _otherTreatmentPlanOrderId; }
            set
            {
                if (_otherTreatmentPlanOrderId != value)
                {
                    if (OtherTreatmentPlanOrder != null && OtherTreatmentPlanOrder.Id != value)
                    {
                        OtherTreatmentPlanOrder = null;
                    }
                    _otherTreatmentPlanOrderId = value;
    
    				OnPropertyChanged("OtherTreatmentPlanOrderId");
                }
            }
        }
        private int _otherTreatmentPlanOrderId;

        #endregion

        #region Navigation Properties
    
    	[Association("Encounter", "EncounterId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual Encounter Encounter
        {
            get { return _encounter; }
            set
            {
                if (!ReferenceEquals(_encounter, value))
                {
                    var previousValue = _encounter;
                    _encounter = value;
                    FixupEncounter(previousValue);
    				OnPropertyChanged("Encounter");
                }
            }
        }
        private Encounter _encounter;
    
    	[Association("OtherTreatmentPlanOrder", "OtherTreatmentPlanOrderId", "Id")]
    	[PracticeRepository.RequiredAssociation]
        public virtual OtherTreatmentPlanOrder OtherTreatmentPlanOrder
        {
            get { return _otherTreatmentPlanOrder; }
            set
            {
                if (!ReferenceEquals(_otherTreatmentPlanOrder, value))
                {
                    var previousValue = _otherTreatmentPlanOrder;
                    _otherTreatmentPlanOrder = value;
                    FixupOtherTreatmentPlanOrder(previousValue);
    				OnPropertyChanged("OtherTreatmentPlanOrder");
                }
            }
        }
        private OtherTreatmentPlanOrder _otherTreatmentPlanOrder;

        #endregion

        #region Association Fixup
    
        private void FixupEncounter(Encounter previousValue)
        {
            if (previousValue != null && previousValue.EncounterOtherTreatmentPlanOrders.Contains(this))
            {
                previousValue.EncounterOtherTreatmentPlanOrders.Remove(this);
            }
    
            if (Encounter != null)
            {
                if (!Encounter.EncounterOtherTreatmentPlanOrders.Contains(this))
                {
                    Encounter.EncounterOtherTreatmentPlanOrders.Add(this);
                }
                if (Encounter != null && EncounterId != Encounter.Id)
                {
                    EncounterId = Encounter.Id;
                }
            }
        }
    
        private void FixupOtherTreatmentPlanOrder(OtherTreatmentPlanOrder previousValue)
        {
            if (previousValue != null && previousValue.EncounterOtherTreatmentPlanOrders.Contains(this))
            {
                previousValue.EncounterOtherTreatmentPlanOrders.Remove(this);
            }
    
            if (OtherTreatmentPlanOrder != null)
            {
                if (!OtherTreatmentPlanOrder.EncounterOtherTreatmentPlanOrders.Contains(this))
                {
                    OtherTreatmentPlanOrder.EncounterOtherTreatmentPlanOrders.Add(this);
                }
                if (OtherTreatmentPlanOrder != null && OtherTreatmentPlanOrderId != OtherTreatmentPlanOrder.Id)
                {
                    OtherTreatmentPlanOrderId = OtherTreatmentPlanOrder.Id;
                }
            }
        }

        #endregion

    }
}
