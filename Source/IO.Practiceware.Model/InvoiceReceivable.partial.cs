﻿using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class InvoiceReceivable
    {
        /// <summary>
        ///   Gets the primary patient insurance for the invoice's receivables.
        /// </summary>
        /// <returns> </returns>
        public PatientInsurance GetPrimaryPatientInsurance()
        {
            return Invoice.InvoiceReceivables.Where(i => i.PatientInsurance != null)
                .Select(i => i.PatientInsurance)
                .OrderBy(pi => pi.OrdinalId)
                .ThenBy(pi => pi.InsuranceTypeId)
                .FirstOrDefault(pi => pi.IsActiveForDateTime(Invoice.Encounter.StartDateTime)
                        && PatientInsurance != null
                        && pi.InsuranceType == PatientInsurance.InsuranceType);
        }
    }

    public static class InvoiceReceivables
    {
        public static decimal GetPatientBalance(this IEnumerable<InvoiceReceivable> source)
        {
            return source.Where(i => i.PatientInsuranceId == null)
                .SelectMany(x => x.BillingServiceTransactions)
                .Where(bst => bst.BillingServiceTransactionStatus.IncludedInPatientBalance())
                .Sum(x => x.AmountSent);
        }
    }
}