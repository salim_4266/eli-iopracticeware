﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Soaf.Reflection;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Denotes a type that possesses property configuration information.
    /// </summary>
    public interface IHasPropertyConfigurations
    {
        /// <summary>
        /// The configuration information for this view model.
        /// </summary>
        ICollection<PropertyConfiguration> PropertyConfigurations { get; set; }
    }

    /// <summary>
    /// A class that contains configuration information for a type's properties.
    /// </summary>
    [DataContract]
    public class PropertyConfiguration
    {
        /// <summary>
        /// The name of the property being configured.
        /// </summary>
        [DataMember]
        public string PropertyName { get; set; }

        /// <summary>
        /// The visibility and requirements kind for this property. If null, property is visible and not required.
        /// </summary>
        [DataMember]
        public PropertyConfigurationKind Kind { get; set; }

        /// <summary>
        /// Returns true if State is unspecified or if State is Required.
        /// </summary>
        public bool IsVisible
        {
            get { return Kind == PropertyConfigurationKind.Visible || Kind == PropertyConfigurationKind.Required; }
        }

        /// <summary>
        /// Returns true if State is specified and Required.
        /// </summary>
        public bool IsRequired
        {
            get { return Kind == PropertyConfigurationKind.Required; }
        }

        /// <summary>
        /// The default value for the property.
        /// </summary>
        [DataMember]
        public object DefaultValue { get; set; }
    }

    /// <summary>
    /// Describes whether a property is Required, Hidden, or Neither.
    /// </summary>
    [DataContract]
    public enum PropertyConfigurationKind 
    {
        [EnumMember]
        Required,
        [EnumMember]
        Hidden,
        [EnumMember]
        Visible
    }

    /// <summary>
    /// Helpers for PropertyConfigurations.
    /// </summary>
    public static class PropertyConfigurations
    {

        /// <summary>
        /// Sets default values on the target instance using PropertyConfigurations. 
        /// </summary>
        /// <param name="target"></param>
        public static void LoadDefaultValues(this IHasPropertyConfigurations target)
        {
            if (target == null) return;
            target.PropertyConfigurations.LoadDefaultValues(target);
        }

        /// <summary>
        /// Sets default values on the target instance using PropertyConfigurations. 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="configurations"> </param>
        public static void LoadDefaultValues(this IEnumerable<PropertyConfiguration> configurations, object target)
        {
            if (target == null || configurations == null) return;

            var targetType = target.GetType();

            foreach (var configuration in configurations)
            {
               LoadDefaultValue(target, targetType, configuration);
            }
        }

        private static void LoadDefaultValue(object target, Type targetType, PropertyConfiguration configuration)
        {
            var property = targetType.GetProperty(configuration.PropertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            if (property != null && configuration.DefaultValue != null && !Equals(property.GetGetMethod().GetInvoker()(target), configuration.DefaultValue))
            {
                property.GetSetMethod().GetInvoker()(target, Convert.ChangeType(configuration.DefaultValue, property.PropertyType));
            }
        }
    }
}
