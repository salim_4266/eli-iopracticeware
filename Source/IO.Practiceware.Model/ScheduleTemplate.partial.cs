﻿using System;

namespace IO.Practiceware.Model
{
    public partial class ScheduleTemplate
    {
        public DateTime GetStartTime(ScheduleTemplateBlock block, int incrementMinutes)
        {
            return StartTime.GetValueOrDefault() + (TimeSpan.FromMinutes(incrementMinutes * (block.OrdinalId - 1)));
        }
    }
}