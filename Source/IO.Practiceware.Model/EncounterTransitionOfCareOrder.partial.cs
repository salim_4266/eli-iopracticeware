﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Model
{
    public partial class EncounterTransitionOfCareOrder
    {
    }

    public enum TransitionOfCareOrderStatusId
    {
        [Display(Name = "Not Reviewed")]
        NotReviewed = 1,
        [Display(Name = "Ready for doctor review")]
        ReadyForDoctorReview = 2,
        [Display(Name = "Ready to send")]
        ReadyToSend = 3,
        [Display(Name = "Send")]
        Send = 4,
        [Display(Name = "Sent")]
        Sent = 5
    }
}
