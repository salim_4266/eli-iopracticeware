﻿using Soaf.Collections;

namespace IO.Practiceware.Model
{
    public partial class PatientMedicationDetail
    {
        public MedicationEventType MedicationEventType
        {
            get { return (MedicationEventType) MedicationEventTypeId; }
            set { MedicationEventTypeId = (int) value; }
        }

        public string Description
        {
            get
            {
                return new object[] {PatientMedication.Medication.Name, DrugDosageNumber.Value, DrugDispenseForm.Name, Frequency.Name, TimeUnit, TimeUnit == null ? new TimeFrame?() : TimeFrame, AsPrescribedComment}
                    .WhereNotDefault().Join(" ");
            }
        }
    }

    public enum MedicationEventType
    {
        PatientStarted = 1,
        Current = 2,
        LastTaken = 3,
        ProviderRefilled = 4,
        ProviderChanged = 5,
        PatientDiscontinued = 6,
        ExternalProviderDiscontinued = 7,
        ProviderDiscontinued = 8,
        DeactivatedAsErroneous = 9,
        MedicationAdministeredDuringVisit = 10
    }
}