﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Well known screen ids.
    /// </summary>
    public enum ScreenId
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Health-Assessment")]
        HealthAssessment = 1,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Height-Weight")]
        HeightWeight = 2,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Current Medications")]
        CurrentMedications = 3,

        /// <summary>
        /// 
        /// </summary>
        Allergies = 4,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Work-Up")]
        WorkUp = 5,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Operating-Room")]
        OperatingRoom = 6,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Post-OP")]
        PostOp = 7,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Patient Financials")]
        PatientFinancials = 8,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Edit & Bill")]
        EditAndBill = 9,

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Post Adjustments")]
        PostAdjustments = 10,
        
    }

    public enum ScreenType
    {
        Financials = 1
    }
}