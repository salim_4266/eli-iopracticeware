﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    public enum AxisQualifier
    {
        [Description("Confirmation of Negative Findings")]
        ConfirmationOfNegativeFindings = 1,
        [Description("Rule out")]
        RuleOut = 2,
        [Description("History of")]
        HistoryOf = 3,
        [Description("Status Post Surgery")]
        StatusPost = 4
    }
}
