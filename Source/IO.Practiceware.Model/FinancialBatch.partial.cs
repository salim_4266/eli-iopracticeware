﻿namespace IO.Practiceware.Model
{
    public partial class FinancialBatch
    {
        public FinancialSourceType FinancialSourceType
        {
            get { return (FinancialSourceType) FinancialSourceTypeId; }
            set { FinancialSourceTypeId = (int) value; }
        }
    }
}
