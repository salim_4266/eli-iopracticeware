﻿<?xml version="1.0" encoding="utf-8"?>
<Schema xmlns="http://schemas.microsoft.com/ado/2008/09/edm" xmlns:cg="http://schemas.microsoft.com/ado/2006/04/codegeneration" xmlns:store="http://schemas.microsoft.com/ado/2007/12/edm/EntityStoreSchemaGenerator" Namespace="IO.Practiceware.Model.Auditing" Alias="Self" xmlns:annotation="http://schemas.microsoft.com/ado/2009/02/edm/annotation">
  <EntityContainer Name="AuditRepository" annotation:LazyLoadingEnabled="false">
    <EntitySet Name="AuditEntries" EntityType="IO.Practiceware.Model.Auditing.AuditEntry" />
    <EntitySet Name="AuditEntryChanges" EntityType="IO.Practiceware.Model.Auditing.AuditEntryChange" />
    <AssociationSet Name="AuditEntryAuditEntryChange" Association="IO.Practiceware.Model.Auditing.AuditEntryAuditEntryChange">
      <End Role="AuditEntry" EntitySet="AuditEntries" />
      <End Role="AuditEntryChange" EntitySet="AuditEntryChanges" />
    </AssociationSet>
    <EntitySet Name="LogEntries" EntityType="IO.Practiceware.Model.Auditing.LogEntry" />
    <EntitySet Name="LogEntryProperties" EntityType="IO.Practiceware.Model.Auditing.LogEntryProperty" />
    <EntitySet Name="LogCategories" EntityType="IO.Practiceware.Model.Auditing.LogCategory" />
    <AssociationSet Name="LogEntryLogCategory" Association="IO.Practiceware.Model.Auditing.LogEntryLogCategory">
      <End Role="LogEntry" EntitySet="LogEntries" />
      <End Role="LogCategory" EntitySet="LogCategories" />
    </AssociationSet>
    <AssociationSet Name="LogEntryLogEntryProperty" Association="IO.Practiceware.Model.Auditing.LogEntryLogEntryProperty">
      <End Role="LogEntry" EntitySet="LogEntries" />
      <End Role="LogEntryProperty" EntitySet="LogEntryProperties" />
    </AssociationSet>
    <FunctionImport Name="GetAuditAndLogEntries" ReturnType="Collection(IO.Practiceware.Model.Auditing.GetAuditAndLogEntriesResult)">
      <Parameter Name="StartDate" Mode="In" Type="DateTime" />
      <Parameter Name="EndDate" Mode="In" Type="DateTime" />
      <Parameter Name="UserIds" Mode="In" Type="String" />
      <Parameter Name="PatientId" Mode="In" Type="String" />
      <Parameter Name="DetailsChanged" Mode="In" Type="String" />
      <Parameter Name="LogActionTypes" Mode="In" Type="String" />
      <Parameter Name="AuditActionTypes" Mode="In" Type="String" />
      <Parameter Name="PageSize" Mode="In" Type="Int32" />
      <Parameter Name="PageIndex" Mode="In" Type="Int32" />
      <Parameter Name="OrderByTypeId" Mode="In" Type="Int32" />
      <Parameter Name="AdditionalFilters" Mode="In" Type="String" />
      <Parameter Name="IncludeTotalCount" Mode="In" Type="Boolean" />
      <Parameter Name="TotalCount" Mode="InOut" Type="Int64" />
    </FunctionImport>
  </EntityContainer>
  <EntityType Name="AuditEntry">
    <Documentation>
      <Summary>Represents an audited row that has been updated or deleted.</Summary>
    </Documentation>
    <Key>
      <PropertyRef Name="Id" />
    </Key>
    <Property Type="Guid" Name="Id" Nullable="false" annotation:StoreGeneratedPattern="Identity" />
    <Property Type="String" Name="ObjectName" Nullable="false" MaxLength="300" Unicode="true" FixedLength="false" />
    <Property Type="Int32" Name="ChangeTypeId" Nullable="false" cg:GetterAccess="Internal" cg:SetterAccess="Internal" />
    <Property Type="DateTime" Name="AuditDateTime" Nullable="false" Precision="3" />
    <Property Type="String" Name="HostName" Nullable="false" MaxLength="500" Unicode="true" FixedLength="false" />
    <Property Type="Int32" Name="UserId" Nullable="true" />
    <NavigationProperty Name="AuditEntryChanges" Relationship="IO.Practiceware.Model.Auditing.AuditEntryAuditEntryChange" FromRole="AuditEntry" ToRole="AuditEntryChange" />
    <Property Type="String" Name="ServerName" Nullable="false" MaxLength="500" Unicode="true" FixedLength="false" />
    <Property Type="String" Name="KeyNames" MaxLength="400" Unicode="true" FixedLength="false" />
    <Property Type="String" Name="KeyValues" MaxLength="400" Unicode="true" FixedLength="false" />
    <Property Type="Int64" Name="KeyValueNumeric" Nullable="true" annotation:StoreGeneratedPattern="Computed" />
    <Property Type="Int64" Name="PatientId" Nullable="true" />
    <Property Type="Int64" Name="AppointmentId" Nullable="true" />
  </EntityType>
  <EntityType Name="AuditEntryChange">
    <Documentation>
      <Summary>Represents a single column level change for an associated AuditEntry.</Summary>
    </Documentation>
    <Key>
      <PropertyRef Name="AuditEntryId" />
      <PropertyRef Name="ColumnName" />
    </Key>
    <Property Type="Guid" Name="AuditEntryId" Nullable="false" />
    <NavigationProperty Name="AuditEntry" Relationship="IO.Practiceware.Model.Auditing.AuditEntryAuditEntryChange" FromRole="AuditEntryChange" ToRole="AuditEntry" />
    <Property Type="String" Name="ColumnName" Nullable="false" MaxLength="400" Unicode="true" FixedLength="false" />
    <Property Type="String" Name="OldValue" Nullable="true" MaxLength="Max" Unicode="true" FixedLength="false" />
    <Property Type="String" Name="NewValue" Nullable="true" MaxLength="Max" Unicode="true" FixedLength="false" />
    <Property Type="Int64" Name="OldValueNumeric" Nullable="true" annotation:StoreGeneratedPattern="Computed" />
    <Property Type="Int64" Name="NewValueNumeric" Nullable="true" annotation:StoreGeneratedPattern="Computed" />
  </EntityType>
  <Association Name="AuditEntryAuditEntryChange">
    <End Type="IO.Practiceware.Model.Auditing.AuditEntry" Role="AuditEntry" Multiplicity="1" />
    <End Type="IO.Practiceware.Model.Auditing.AuditEntryChange" Role="AuditEntryChange" Multiplicity="*" />
    <ReferentialConstraint>
      <Principal Role="AuditEntry">
        <PropertyRef Name="Id" />
      </Principal>
      <Dependent Role="AuditEntryChange">
        <PropertyRef Name="AuditEntryId" />
      </Dependent>
    </ReferentialConstraint>
  </Association>
  <EntityType Name="LogEntry">
    <Key>
      <PropertyRef Name="Id" />
    </Key>
    <Property Type="Guid" Name="Id" Nullable="false" annotation:StoreGeneratedPattern="None" />
    <Property Type="String" Name="Source" MaxLength="255" FixedLength="false" Unicode="true" Nullable="false" />
    <Property Type="String" Name="Title" MaxLength="255" FixedLength="false" Unicode="true" />
    <Property Type="String" Name="Message" Nullable="false" MaxLength="Max" FixedLength="false" Unicode="true" />
    <Property Type="Int32" Name="SeverityId" Nullable="false" />
    <Property Type="Guid" Name="ActivityId" />
    <Property Type="Guid" Name="RelatedActivityId" />
    <Property Type="Int32" Name="EventId" />
    <Property Type="String" Name="MachineName" MaxLength="50" FixedLength="false" Unicode="true" />
    <Property Type="String" Name="ThreadName" MaxLength="255" FixedLength="false" Unicode="true" />
    <Property Type="Int32" Name="ProcessId" />
    <Property Type="String" Name="ProcessName" MaxLength="50" FixedLength="false" Unicode="true" />
    <Property Type="DateTime" Name="DateTime" Nullable="false" annotation:StoreGeneratedPattern="Computed" Precision="3" />
    <Property Type="String" Name="UserId" Unicode="true" MaxLength="50" FixedLength="false" />
    <Property Type="String" Name="ServerName" annotation:StoreGeneratedPattern="Computed" MaxLength="500" Nullable="false" Unicode="true" FixedLength="false" />
    <NavigationProperty Name="Categories" Relationship="IO.Practiceware.Model.Auditing.LogEntryLogCategory" FromRole="LogEntry" ToRole="LogCategory" />
    <NavigationProperty Name="Properties" Relationship="IO.Practiceware.Model.Auditing.LogEntryLogEntryProperty" FromRole="LogEntry" ToRole="LogEntryProperty" />
  </EntityType>
  <EntityType Name="LogEntryProperty">
    <Key>
      <PropertyRef Name="Id" />
    </Key>
    <Property Type="Guid" Name="Id" Nullable="false" annotation:StoreGeneratedPattern="None" />
    <Property Type="String" Name="Name" Nullable="false" MaxLength="255" Unicode="true" FixedLength="false" />
    <Property Type="String" Name="Value" Nullable="false" MaxLength="Max" Unicode="true" FixedLength="false" />
    <NavigationProperty Name="LogEntry" Relationship="IO.Practiceware.Model.Auditing.LogEntryLogEntryProperty" FromRole="LogEntryProperty" ToRole="LogEntry" />
    <Property Type="Guid" Name="LogEntryId" Nullable="false" />
  </EntityType>
  <EntityType Name="LogCategory">
    <Key>
      <PropertyRef Name="Id" />
    </Key>
    <Property Type="Int32" Name="Id" Nullable="false" annotation:StoreGeneratedPattern="Identity" />
    <Property Type="String" Name="Name" Nullable="false" MaxLength="255" Unicode="true" FixedLength="false" />
  </EntityType>
  <Association Name="LogEntryLogCategory">
    <End Type="IO.Practiceware.Model.Auditing.LogEntry" Role="LogEntry" Multiplicity="*" />
    <End Type="IO.Practiceware.Model.Auditing.LogCategory" Role="LogCategory" Multiplicity="*" />
  </Association>
  <Association Name="LogEntryLogEntryProperty">
    <End Type="IO.Practiceware.Model.Auditing.LogEntry" Role="LogEntry" Multiplicity="1" />
    <End Type="IO.Practiceware.Model.Auditing.LogEntryProperty" Role="LogEntryProperty" Multiplicity="*" />
    <ReferentialConstraint>
      <Principal Role="LogEntry">
        <PropertyRef Name="Id" />
      </Principal>
      <Dependent Role="LogEntryProperty">
        <PropertyRef Name="LogEntryId" />
      </Dependent>
    </ReferentialConstraint>
  </Association>
  <ComplexType Name="GetAuditAndLogEntriesResult">
    <Property Type="Guid" Name="Id" Nullable="false" />
    <Property Type="Boolean" Name="IsLogEntry" Nullable="false" />
    <Property Type="DateTime" Name="DateTime" Nullable="false" />
    <Property Type="String" Name="UserId" Nullable="false" />
    <Property Type="String" Name="AccessLocation" Nullable="true" />
    <Property Type="String" Name="AccessDevice" Nullable="true" />
    <Property Type="String" Name="SourceOfAccess" Nullable="true" />
    <Property Type="String" Name="ActionMessage" Nullable="true" />
    <Property Type="Int64" Name="PatientId" Nullable="true" />
    <Property Type="String" Name="DetailChanged" Nullable="false" />
    <Property Type="String" Name="NewValue" Nullable="true" />
    <Property Type="String" Name="OldValue" Nullable="true" />
    <Property Type="Int32" Name="ContentTypeId" Nullable="true" />
  </ComplexType>
</Schema>