//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Chief complaints are assigned to categories for easy navigation
    /// </summary>
    public partial class ChiefComplaintCategory : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;

        #endregion

        #region Navigation Properties
    
    	[Association("ChiefComplaints", "Id", "ChiefComplaintCategoryId")]
        public virtual System.Collections.Generic.ICollection<ChiefComplaint> ChiefComplaints
        {
            get
            {
                if (_chiefComplaints == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ChiefComplaint>();
                    newCollection.ItemSet += FixupChiefComplaintsItemSet;
                    newCollection.ItemRemoved += FixupChiefComplaintsItemRemoved;
                    _chiefComplaints = newCollection;
                }
                return _chiefComplaints;
            }
            set
            {
                if (!ReferenceEquals(_chiefComplaints, value))
                {
                    var previousValue = _chiefComplaints as Soaf.Collections.FixupCollection<ChiefComplaint>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupChiefComplaintsItemSet;
                        previousValue.ItemRemoved -= FixupChiefComplaintsItemRemoved;
                    }
                    _chiefComplaints = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ChiefComplaint>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupChiefComplaintsItemSet;
                        newValue.ItemRemoved += FixupChiefComplaintsItemRemoved;
                    }
    				OnPropertyChanged("ChiefComplaints");
                }
            }
        }
        private System.Collections.Generic.ICollection<ChiefComplaint> _chiefComplaints;

        #endregion

        #region Association Fixup
    
        private void FixupChiefComplaintsItemSet(object sender, Soaf.EventArgs<ChiefComplaint> e)
        {
            var item = e.Value;
     
            item.ChiefComplaintCategory = this;
        }
    
        private void FixupChiefComplaintsItemRemoved(object sender, Soaf.EventArgs<ChiefComplaint> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.ChiefComplaintCategory, this))
            {
                item.ChiefComplaintCategory = null;
            }
        }
    

        #endregion

    }
}
