﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    public enum PatientDemographicsFieldConfigurationId
    {
        [Display(Name = "Title", GroupName = "Name")]
        Title = 1,

        [Display(Name = "First Name", GroupName = "Name")]
        FirstName = 2,

        [Display(Name = "Middle Name", GroupName = "Name")]
        MiddleName = 3,

        [Display(Name = "Last Name", GroupName = "Name")]
        LastName = 4,

        [Display(Name = "Suffix", GroupName = "Name")]
        Suffix = 5,

        [Display(Name = "Nickname", GroupName = "Name")]
        Nickname = 6,

        [Display(Name = "Honorific", GroupName = "Name")]
        Honorific = 7,

        [Display(Name = "Prior First Name", GroupName = "Name")]
        PriorFirstName = 8,

        [Display(Name = "Prior Last Name", GroupName = "Name")]
        PriorLastName = 9,

        [Display(Name = "Primary Address", GroupName = "Address Area")]
        Addresses = 10,
        
        [Display(Name = "Primary Phone", GroupName = "Phone Area")]
        PhoneNumbers = 11,
        
        [Display(Name = "Primary Email Address", GroupName = "Email Address Area")]
        EmailAddresses = 12,

        [Display(Name = "Date of Birth", GroupName = "General")]
        DateOfBirth = 13,

        [Display(Name = "Gender Identity", GroupName = "General")]
        GenderId = 14,

        [Display(Name = "Marital Status", GroupName = "General")]
        MaritalStatusId = 15,

        [Display(Name = "Race", GroupName = "General")]
        PatientRaces = 16,

        [Display(Name = "Ethnicity", GroupName = "General")]
        EthnicityId = 17,

        [Display(Name = "Language", GroupName = "General")]
        LanguageId = 18,

        [Display(Name = "SSN", GroupName = "General")]
        SocialSecurityNumber = 19,

        [Display(Name = "Prior ID", GroupName = "General")]
        PriorId = 20,

        [Display(Name = "Employment Info", GroupName = "Employment Area")]
        EmploymentArea = 21,

        [Display(Name = "Preferred Physician", GroupName = "Physician Info")]
        PreferredPhysicianId = 22,

        [Display(Name = "Preferred Service Location", GroupName = "Physician Info")]
        PreferredServiceLocationId = 23,

        [Display(Name = "PCP/Other", GroupName = "Physician Info")]
        ExternalPhysicians = 24,

        [Display(Name = "HIPAA Consent", GroupName = "Additional Info")]
        HasHipaaConsent = 25,

        [Display(Name = "Release Medical Records", GroupName = "Additional Info")]
        ReleaseMedicalInformationId = 26,

        [Display(Name = "Signature Date", GroupName = "Additional Info")]
        ReleaseSignatureDate = 27,

        [Display(Name = "Assign Benefits to Provider", GroupName = "Additional Info")]
        AssignBenefitsToProviderId = 28,

        [Display(Name = "Patient Tag", GroupName = "Additional Info")]
        Tags = 29,

        [Display(Name = "Status", GroupName = "Additional Info")]
        PatientStatusId = 30,

        [Display(Name = "Religion", GroupName = "Additional Info")]
        ReligionId = 31,

        [Display(Name = "Referring Contact & Category", GroupName = "Additional Info")]
        ReferralSources = 32,

        [Display(Name = "Billing Organization", GroupName = "Additional Info")]
        BillingOrganization = 33,

        [Display(Name = "Comments", GroupName = "Additional Info")]
        Comments = 34,

        [Display(Name = "Date Of Birth", GroupName = "Non-Clinical Patients")]
        NonClinicalPatientsDateOfBirth = 35,

        [Display(Name = "Social Security Number", GroupName = "Non-Clinical Patients")]
        NonClinicalPatientsSocialSecurityNumber = 36,

        [Display(Name = "Sex", GroupName = "General")]
        SexId = 37,

        [Display(Name = "Sexual Orientation", GroupName = "General")]
        SexualOrientationId = 38,

        [Display(Name = "Race & Ethnicity", GroupName = "General")]
        RaceAndEthnicityId = 39,

       [Display(Name = "ONC Psychology", GroupName = "General")]
        SocialBehaviour = 40
    }
}
