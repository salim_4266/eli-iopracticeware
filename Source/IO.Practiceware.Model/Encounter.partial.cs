﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Soaf.Reflection;

namespace IO.Practiceware.Model
{

    /// <summary>
    /// Metadata for the patient model.
    /// </summary>
    public class EncounterMetadata
    {
        [Display(Name = "Location")]
        public int ServiceLocationId { get; set; }

        [Display(Name = "Insurance Type")]
        public InsuranceType InsuranceType { get; set; }

        [Display(Name = "Status Change Reason")]
        public int? EncounterStatusChangeReasonId { get; set; }

        [Display(Name = "Status Comment")]
        public string EncounterStatusChangeComment { get; set; }

        [Display(Name = "Encounter Status")]
        public EncounterStatus EncounterStatus { get; set; }
    }

    [MetadataType(typeof(EncounterMetadata))]
    public partial class Encounter
    {
        private bool _isSyncingStartAndEndDateTime;

        public Encounter()
        {
            PropertyChanged += SyncStartAndEndDateTime;
            StartDateTime = new DateTime(1753, 1, 1);
        }

        private static readonly string StartDateTimePropertyName = Reflector.GetMember<Encounter>(x => x.StartDateTime).Name;
        private static readonly string EndDateTimePropertyName = Reflector.GetMember<Encounter>(x => x.EndDateTime).Name;

        void SyncStartAndEndDateTime(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (_isSyncingStartAndEndDateTime) return;
            _isSyncingStartAndEndDateTime = true;
            try
            {
                if (e.PropertyName == StartDateTimePropertyName) EndDateTime = StartDateTime.AddHours(1);
                if (e.PropertyName == EndDateTimePropertyName) StartDateTime = EndDateTime.AddHours(-1);
            }
            finally
            {
                _isSyncingStartAndEndDateTime = false;
            }
        }

        public InsuranceType InsuranceType
        {
            get { return (InsuranceType)InsuranceTypeId; }
            set { InsuranceTypeId = (int)value; }
        }

        public EncounterStatus EncounterStatus
        {
            get { return (EncounterStatus) EncounterStatusId; }
            set { EncounterStatusId = (int) value; }
        }
    }


    public static class EncounterStatusIds
    {
        public static bool IsPendingStatus(this EncounterStatus status)
        {
            return status == EncounterStatus.Pending;
        }

        public static bool IsArrivedStatus(this EncounterStatus status)
        {
            return status == EncounterStatus.Questions || status == EncounterStatus.WaitingRoom || status == EncounterStatus.ExamRoom ||
                   status == EncounterStatus.CheckoutExpress || status == EncounterStatus.Checkout || status == EncounterStatus.AdminDisposition;
        }

        public static bool IsDischargedStatus(this EncounterStatus status)
        {
            return status == EncounterStatus.Discharged;
        }

        public static bool IsCancelledStatus(this EncounterStatus status)
        {
            return status == EncounterStatus.CancelLeft || status == EncounterStatus.CancelNoShow || status == EncounterStatus.CancelOffice ||
                   status == EncounterStatus.CancelOther || status == EncounterStatus.CancelPatient || status == EncounterStatus.CancelSameDay ||
                   status == EncounterStatus.CancelSpecial;
        }

        public static bool CanCancel(this EncounterStatus encounterStatus)
        {
            if (encounterStatus == EncounterStatus.Pending
                   || encounterStatus == EncounterStatus.Questions
                   || encounterStatus == EncounterStatus.WaitingRoom
                   || encounterStatus.IsCancelledStatus())
                return true;

            return false;
        }

        public static bool CanReschedule(this EncounterStatus encounterStatus)
        {
            return encounterStatus == EncounterStatus.Pending;
        }
    }

    /// <summary>
    /// Enumeration of known encounter types.
    /// </summary>
    [DataContract]
    public enum EncounterTypeId
    {
        [EnumMember]
        [Display(Name = "Clinician Encounter")]
        ClinicianEncounter = 1,

        [EnumMember]
        [Display(Name = "Facility Encounter")]
        FacilityEncounter = 2,

        [EnumMember]
        [Display(Name = "Clinician Facility Encounter")]
        ClinicianFacilityEncounter = 3,

        [EnumMember]
        [Display(Name = "Vision Encounter")]
        VisionEncounter = 4,

        [EnumMember]
        [Display(Name = "No Invoice Encounter")]
        NoInvoiceEncounter = 5
    }

    /// <summary>
    /// Enumeration of valid encounter status Ids.
    /// </summary>
    [DataContract]
    public enum EncounterStatus
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Pending")] Pending = 1,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Questions")] Questions = 2,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Waiting Room")] WaitingRoom = 3,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Exam Room")] ExamRoom = 4,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Checkout, Express")] CheckoutExpress = 5,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Checkout")] Checkout = 6,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Discharged")] Discharged = 7,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Office")] CancelOffice = 8,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Patient")] CancelPatient = 9,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Same Day")] CancelSameDay = 10,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, No Show")] CancelNoShow = 11,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Left")] CancelLeft = 12,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Special")] CancelSpecial = 13,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Cancelled, Other")] CancelOther = 14,

        /// <summary>
        /// 
        /// </summary>
        [EnumMember] [Display(Name = "Admin Disposition")] AdminDisposition = 15
    }
}
