//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Model
{
    /// <summary>
    /// Document templates used when generating forms for merging and printing
    /// </summary>
    public partial class TemplateDocument : System.ComponentModel.INotifyPropertyChanged , Soaf.ComponentModel.IHasId
    {
    
    	object Soaf.ComponentModel.IHasId.Id { get { return Id; } }
    
    	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		var handler = PropertyChanged;
    		if (handler != null)
    		{
    			handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
        [Key]
    	public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
        [Required]
    	public virtual int TemplateDocumentCategoryId
        {
            get { return _templateDocumentCategoryId; }
            set 
    		{ 
    			var isChanged = _templateDocumentCategoryId != value; 
    			_templateDocumentCategoryId = value; 
    			if (isChanged) OnPropertyChanged("TemplateDocumentCategoryId");
    		}
        }
        private int _templateDocumentCategoryId;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string Name
        {
            get { return _name; }
            set 
    		{ 
    			var isChanged = _name != value; 
    			_name = value; 
    			if (isChanged) OnPropertyChanged("Name");
    		}
        }
        private string _name;
    
        [Required(AllowEmptyStrings = true)]
    	public virtual string DisplayName
        {
            get { return _displayName; }
            set 
    		{ 
    			var isChanged = _displayName != value; 
    			_displayName = value; 
    			if (isChanged) OnPropertyChanged("DisplayName");
    		}
        }
        private string _displayName;
    
    	internal virtual byte[] Content
        {
            get { return _content; }
            set 
    		{ 
    			var isChanged = _content != value; 
    			_content = value; 
    			if (isChanged) OnPropertyChanged("Content");
    		}
        }
        private byte[] _content;
    
    	internal virtual byte[] ContentOriginal
        {
            get { return _contentOriginal; }
            set 
    		{ 
    			var isChanged = _contentOriginal != value; 
    			_contentOriginal = value; 
    			if (isChanged) OnPropertyChanged("ContentOriginal");
    		}
        }
        private byte[] _contentOriginal;
    
        [Required]
    	public virtual int ContentTypeId
        {
            get { return _contentTypeId; }
            set 
    		{ 
    			var isChanged = _contentTypeId != value; 
    			_contentTypeId = value; 
    			if (isChanged) OnPropertyChanged("ContentTypeId");
    		}
        }
        private int _contentTypeId;
    
        [Required]
    	public virtual bool IsEditable
        {
            get { return _isEditable; }
            set 
    		{ 
    			var isChanged = _isEditable != value; 
    			_isEditable = value; 
    			if (isChanged) OnPropertyChanged("IsEditable");
    		}
        }
        private bool _isEditable;

        #endregion

        #region Navigation Properties
    
    	[Association("Printers", "", "")]
        public virtual System.Collections.Generic.ICollection<Printer> Printers
        {
            get
            {
                if (_printers == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<Printer>();
                    newCollection.ItemSet += FixupPrintersItemSet;
                    newCollection.ItemRemoved += FixupPrintersItemRemoved;
                    _printers = newCollection;
                }
                return _printers;
            }
            set
            {
                if (!ReferenceEquals(_printers, value))
                {
                    var previousValue = _printers as Soaf.Collections.FixupCollection<Printer>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupPrintersItemSet;
                        previousValue.ItemRemoved -= FixupPrintersItemRemoved;
                    }
                    _printers = value;
                    var newValue = value as Soaf.Collections.FixupCollection<Printer>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupPrintersItemSet;
                        newValue.ItemRemoved += FixupPrintersItemRemoved;
                    }
    				OnPropertyChanged("Printers");
                }
            }
        }
        private System.Collections.Generic.ICollection<Printer> _printers;
    
    	[Association("PatientRecalls", "Id", "TemplateDocumentId")]
        public virtual System.Collections.Generic.ICollection<PatientRecall> PatientRecalls
        {
            get
            {
                if (_patientRecalls == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<PatientRecall>();
                    newCollection.ItemSet += FixupPatientRecallsItemSet;
                    newCollection.ItemRemoved += FixupPatientRecallsItemRemoved;
                    _patientRecalls = newCollection;
                }
                return _patientRecalls;
            }
            set
            {
                if (!ReferenceEquals(_patientRecalls, value))
                {
                    var previousValue = _patientRecalls as Soaf.Collections.FixupCollection<PatientRecall>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupPatientRecallsItemSet;
                        previousValue.ItemRemoved -= FixupPatientRecallsItemRemoved;
                    }
                    _patientRecalls = value;
                    var newValue = value as Soaf.Collections.FixupCollection<PatientRecall>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupPatientRecallsItemSet;
                        newValue.ItemRemoved += FixupPatientRecallsItemRemoved;
                    }
    				OnPropertyChanged("PatientRecalls");
                }
            }
        }
        private System.Collections.Generic.ICollection<PatientRecall> _patientRecalls;
    
    	[Association("ClaimFormTypes", "Id", "TemplateDocumentId")]
        public virtual System.Collections.Generic.ICollection<ClaimFormType> ClaimFormTypes
        {
            get
            {
                if (_claimFormTypes == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<ClaimFormType>();
                    newCollection.ItemSet += FixupClaimFormTypesItemSet;
                    newCollection.ItemRemoved += FixupClaimFormTypesItemRemoved;
                    _claimFormTypes = newCollection;
                }
                return _claimFormTypes;
            }
            set
            {
                if (!ReferenceEquals(_claimFormTypes, value))
                {
                    var previousValue = _claimFormTypes as Soaf.Collections.FixupCollection<ClaimFormType>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupClaimFormTypesItemSet;
                        previousValue.ItemRemoved -= FixupClaimFormTypesItemRemoved;
                    }
                    _claimFormTypes = value;
                    var newValue = value as Soaf.Collections.FixupCollection<ClaimFormType>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupClaimFormTypesItemSet;
                        newValue.ItemRemoved += FixupClaimFormTypesItemRemoved;
                    }
    				OnPropertyChanged("ClaimFormTypes");
                }
            }
        }
        private System.Collections.Generic.ICollection<ClaimFormType> _claimFormTypes;
    
    	[Association("CommunicationWithOtherProviderOrders", "Id", "TemplateDocumentId")]
        public virtual System.Collections.Generic.ICollection<CommunicationWithOtherProviderOrder> CommunicationWithOtherProviderOrders
        {
            get
            {
                if (_communicationWithOtherProviderOrders == null)
                {
                    var newCollection = new Soaf.Collections.FixupCollection<CommunicationWithOtherProviderOrder>();
                    newCollection.ItemSet += FixupCommunicationWithOtherProviderOrdersItemSet;
                    newCollection.ItemRemoved += FixupCommunicationWithOtherProviderOrdersItemRemoved;
                    _communicationWithOtherProviderOrders = newCollection;
                }
                return _communicationWithOtherProviderOrders;
            }
            set
            {
                if (!ReferenceEquals(_communicationWithOtherProviderOrders, value))
                {
                    var previousValue = _communicationWithOtherProviderOrders as Soaf.Collections.FixupCollection<CommunicationWithOtherProviderOrder>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupCommunicationWithOtherProviderOrdersItemSet;
                        previousValue.ItemRemoved -= FixupCommunicationWithOtherProviderOrdersItemRemoved;
                    }
                    _communicationWithOtherProviderOrders = value;
                    var newValue = value as Soaf.Collections.FixupCollection<CommunicationWithOtherProviderOrder>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupCommunicationWithOtherProviderOrdersItemSet;
                        newValue.ItemRemoved += FixupCommunicationWithOtherProviderOrdersItemRemoved;
                    }
    				OnPropertyChanged("CommunicationWithOtherProviderOrders");
                }
            }
        }
        private System.Collections.Generic.ICollection<CommunicationWithOtherProviderOrder> _communicationWithOtherProviderOrders;

        #endregion

        #region Association Fixup
    
        private void FixupPrintersItemSet(object sender, Soaf.EventArgs<Printer> e)
        {
            var item = e.Value;
     
            if (!item.TemplateDocuments.Contains(this))
            {
                item.TemplateDocuments.Add(this);
            }
        }
    
        private void FixupPrintersItemRemoved(object sender, Soaf.EventArgs<Printer> e)
        {
            var item = e.Value;
     
            if (item.TemplateDocuments.Contains(this))
            {
                item.TemplateDocuments.Remove(this);
            }
        }
    
    
        private void FixupPatientRecallsItemSet(object sender, Soaf.EventArgs<PatientRecall> e)
        {
            var item = e.Value;
     
            item.TemplateDocument = this;
        }
    
        private void FixupPatientRecallsItemRemoved(object sender, Soaf.EventArgs<PatientRecall> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.TemplateDocument, this))
            {
                item.TemplateDocument = null;
            }
        }
    
    
        private void FixupClaimFormTypesItemSet(object sender, Soaf.EventArgs<ClaimFormType> e)
        {
            var item = e.Value;
     
            item.TemplateDocument = this;
        }
    
        private void FixupClaimFormTypesItemRemoved(object sender, Soaf.EventArgs<ClaimFormType> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.TemplateDocument, this))
            {
                item.TemplateDocument = null;
            }
        }
    
    
        private void FixupCommunicationWithOtherProviderOrdersItemSet(object sender, Soaf.EventArgs<CommunicationWithOtherProviderOrder> e)
        {
            var item = e.Value;
     
            item.TemplateDocument = this;
        }
    
        private void FixupCommunicationWithOtherProviderOrdersItemRemoved(object sender, Soaf.EventArgs<CommunicationWithOtherProviderOrder> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.TemplateDocument, this))
            {
                item.TemplateDocument = null;
            }
        }
    

        #endregion

    }
}
