﻿using System;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class PatientAllergen
    {
        /// <summary>
        /// Adds a default Problem Concern Level to the PatientAllergen's list of PatientProblemConcernLevels.  Sets the enum ConcernLevel based on the 
        /// first allergen detail's clinical condition status. Returns the new PatientProblemConcernLevel.  If one already existed, it will return null.
        /// </summary>
        public PatientProblemConcernLevel AddDefaultPatientProblemConcernLevel()
        {
            if (this.PatientProblemConcernLevels.Any()) return null;

            var allergenDetail = this.PatientAllergenDetails.OrderByDescending(pad => pad.EnteredDateTime).FirstOrDefault();
            if (allergenDetail != null)
            {
                this.PatientProblemConcernLevels.Add(new PatientProblemConcernLevel { EnteredDateTime = DateTime.UtcNow, ConcernLevel = GetConcernLevelFromClinicalConditionStatus(allergenDetail.ClinicalConditionStatus) });
            }

            return this.PatientProblemConcernLevels.FirstOrDefault();
        }

        public ConcernLevel GetConcernLevelFromClinicalConditionStatus(ClinicalConditionStatus status)
        {
            if (status == ClinicalConditionStatus.Active) return ConcernLevel.Active;

            if (status == ClinicalConditionStatus.Inactive) return ConcernLevel.Suspended;

            if (status == ClinicalConditionStatus.Resolved) return ConcernLevel.Completed;

            return ConcernLevel.Active;
        }
    }
}
