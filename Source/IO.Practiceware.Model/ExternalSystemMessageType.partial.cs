﻿using System.ComponentModel;

namespace IO.Practiceware.Model
{
    public enum ExternalSystemMessageTypeId
    {
        // ReSharper disable InconsistentNaming
        [Description("Acknowledgement")]
        ACK_V23_Inbound = 1,
        [Description("Add/Update Patient")]
        ADT_A04_V23_Inbound = 2,
        [Description("Add/Update Patient")]
        ADT_A08_V23_Inbound = 3,
        [Description("Add Patient")]
        ADT_A28_V23_Inbound = 4,
        [Description("Add/Update Patient")]
        ADT_A31_V23_Inbound = 5,
        [Description("Add Appointment")]
        SIU_S12_V23_Inbound = 6,
        [Description("Add/Reschedule/Check-In Appointment")]
        SIU_S13_V23_Inbound = 7,
        [Description("Check-in/Check-out Appointment")]
        SIU_S14_V23_Inbound = 8,
        [Description("Cancel Appointment")]
        SIU_S15_V23_Inbound = 9,
        [Description("Cancel Appointment")]
        SIU_S17_V23_Inbound = 10,
        [Description("Cancel Appointment")]
        SIU_S26_V23_Inbound = 11,
        [Description("Master File Add/Update")]
        MFN_M02_V231_Inbound = 12,
        [Description("Master File Add/Update")]
        MFN_M02_V23_Inbound = 13,
        [Description("Charges")]
        DFT_P03_V231_Outbound = 14,
        [Description("Doctor Order")]
        ORM_O01_V231_Outbound = 15,
        [Description("Doctor Order")]
        ORM_O01_V231_Inbound = 16,
        [Description("Claim File")]
        X12_837_Outbound = 17,
        [Description("Add Patient")]
        ADT_A28_V23_Outbound = 18,
        [Description("Confirmation")]
        Confirmation_Outbound = 19,
        [Description("Add/Update Patient")]
        ADT_A31_V23_Outbound = 20,
        [Description("Add/Update Patient")]
        ADT_A04_V231_Inbound = 21,
        [Description("Add/Update Patient")]
        ADT_A08_V231_Inbound = 22,
        [Description("Add Patient")]
        ADT_A28_V231_Inbound = 23,
        [Description("Add/Update Patient")]
        ADT_A31_V231_Inbound = 24,
        [Description("Add Appointment")]
        SIU_S12_V231_Inbound = 25,
        [Description("Paper Claim")]
        PaperClaim_Outbound = 26,
        [Description("Encounter Clinical Summary C-CDA")]
        EncounterClinicalSummaryCCDA_Outbound = 27,
        [Description("Patient Transition of Care C-CDA")]
        PatientTransitionOfCareCCDA_Outbound = 28,
        [Description("Patient Transition of Care C-CDA")]
        PatientTransitionOfCareCCDA_Inbound = 29,
        [Description("Acknowledgement")]
        ACK_V231_Inbound = 30,
        [Description("QRDA Category I")]
        QRDA_Category_I_Outbound = 31,
        [Description("QRDA Category III")]
        QRDA_Category_III_Outbound = 32,
        [Description("Add Appointment")]
        SIU_S12_V23_Outbound = 33,
        [Description("Check-in/Check-out Appointment")]
        SIU_S14_V23_Outbound = 34,
        [Description("Cancel Appointment")]
        SIU_S15_V23_Outbound =35,
        [Description("Electronic 835 remittance file")]
        X12_835_Inbound = 36,
        [Description("Patient statement")]
        PatientStatement_Outbound = 37
        // ReSharper restore InconsistentNaming
    }

}