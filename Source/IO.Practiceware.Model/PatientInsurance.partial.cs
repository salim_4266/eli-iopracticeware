﻿using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Model
{
    public partial class PatientInsurance
    {
        public InsuranceType InsuranceType
        {
            get { return (InsuranceType)InsuranceTypeId; }
            set { InsuranceTypeId = (int)value; }
        }

        public PolicyHolderRelationshipType PolicyholderRelationshipType
        {
            get { return (PolicyHolderRelationshipType)PolicyholderRelationshipTypeId; }
            set { PolicyholderRelationshipTypeId = (int)value; }
        }

        /// <summary>
        ///   Gets the index among a collection of policies.
        /// </summary>
        /// <param name="patientInsurances"> The patient insurances. </param>
        /// <param name="dateTime"> The date time. </param>
        /// <param name="withSameInsuranceType"> if set to <c>true</c> [with same insurance type]. </param>
        /// <returns> </returns>
        public int GetIndex(IEnumerable<PatientInsurance> patientInsurances, DateTime? dateTime = null, bool withSameInsuranceType = true)
        {
            var insurances = patientInsurances as List<PatientInsurance> ?? patientInsurances.ToList();
            if (!insurances.Contains(this)) throw new InvalidOperationException("Collection does not contain the specified patient insurance.");

            return insurances
                .Where(i => dateTime == null || i.IsActiveForDateTime(dateTime.Value))
                .Where(i => !withSameInsuranceType || i.InsuranceType == InsuranceType)
                .OrderBy(pi => pi.OrdinalId)
                .IndexOf(this);
        }
    }

    public static class PatientInsurances
    {
        /// <summary>
        /// Returns the max ordinal id (or rank) out of this list of insurances
        /// </summary>
        /// <param name="patientInsurances"></param>
        /// <param name="insuredPatientId">Optional. Include this to return the max ordinal id of only insurances with an insuredPatientId of this value</param>
        /// <param name="insuranceType">Optional. Include this to return the max ordinal id of only insurances with an insuranceType of this value</param>
        /// <returns></returns>
        public static int GetMaxOrdinalId(this IEnumerable<PatientInsurance> patientInsurances, int? insuredPatientId = null, InsuranceType? insuranceType = null)
        {
            return patientInsurances.Where(pi => (!insuredPatientId.HasValue || pi.InsuredPatientId == insuredPatientId) 
                && (!insuranceType.HasValue || pi.InsuranceType == insuranceType))
                .ToList()
                .Select(pi => pi.OrdinalId).DefaultIfEmpty(0).Max();
        }
    }

    public enum PolicyHolderRelationshipType
    {
        [Display(Name = "Self")]
        Self = 1,
        [Display(Name = "Spouse")]
        Spouse = 2,
        [Display(Name = "Child")]
        Child = 3,
        [Display(Name = "Employee")]
        Employee = 4,
        [Display(Name = "Unknown")]
        Unknown = 5,
        [Display(Name = "Life Partner")]
        LifePartner = 6,
        [Display(Name = "Other Relationship")]
        OtherRelationship = 7,
        [Display(Name = "Organ Donor")]
        OrganDonor = 8,
        [Display(Name = "Cadaver Donor")]
        CadaverDonor = 9
    }
}