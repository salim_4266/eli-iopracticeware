﻿namespace IO.Practiceware.Model
{
    public partial class ApplicationSetting
    {
        public const string CloseDateTime = "CloseDateTime";

        public const string SubmitTransactions = "SubmitTransactions";

        public const string PendingClaimsFilter = "PendingClaimsFilter";

        public const string GenerateStatementsFilter = "GenerateStatementsFilter";

        public const string ClaimsDirectory = "ClaimsDirectory";

        public const string AppointmentMessageHandler = "AppointmentMessageHandler";

        public const string PatientImageImporterCriteria = "PatientImageImporterCriteria";

        public const string PatientImageImporterIndexType = "PatientImageImporterIndexType";

        public const string PatientAppointments = "PatientAppointments";

        public const string RadDockingLayout = "RadDockingLayout ({0})";

        public const string UserFeatureSelection = "UserFeatureSelection";

        public const string UserWidgetLayout = "UserWidgetLayout";

        public const string RemittanceFilesPath = "RemittanceFilesPath";

        public const string PatientSearchAdvancedOptions = "PatientSearchAdvancedOptions";

        public const string RecentlyAccessedPatients = "RecentlyAccessedPatients";

        public const string MessagingConfiguration = "MessagingConfiguration";

        public const string GridReport = "GridReport ({0})";

        public const string ServiceLocation = "ServiceLocation";

        public const string StatementExportFormat = "StatementExportFormat";

        public const string OpenBalanceDeterminer = "OpenBalanceDeterminer";

        public const string ProfessionalEncounterServiceSelections = "ProfessionalEncounterServiceSelections";

        public const string DefaultDiagnosisTypeId = "DefaultDiagnosisTypeId";

        public const string FinancialUserFilters = "FinancialUserFilters";  //Filters On Financial Screen
    }


    /// <summary>
    /// Represents a type that can be serializes/deserialized to a ApplicationSetting.
    /// </summary>
    public interface IApplicationSetting
    {

    }
}
