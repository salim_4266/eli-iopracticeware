using System;

namespace IO.Practiceware.DbSync.Extensions
{
    /// <summary>
    /// This class is a timeout for the Console.Readline method.
    /// It takes in a timespan, then waits for a user to press Enter
    /// until the timespan has expired. If the user does enter 
    /// input before the timespan has expired, it retuns the
    /// user's input as a string.
    /// </summary>
    public static class ConsoleExtensions
    {
        private delegate string ReadLineDelegate();

        public static string ReadLine(TimeSpan? quantifiedTimeSpan = null)
        {
            string readLineResult;
            ReadLineDelegate userAnswerTimed = Console.ReadLine;
            var userEnteredStringBeforeTimeSpanExpiration = userAnswerTimed.BeginInvoke(null, null);
            userEnteredStringBeforeTimeSpanExpiration.AsyncWaitHandle.WaitOne(quantifiedTimeSpan == null ? TimeSpan.MaxValue : quantifiedTimeSpan.Value);

            if (userEnteredStringBeforeTimeSpanExpiration.IsCompleted)
            {
                readLineResult = userAnswerTimed.EndInvoke(userEnteredStringBeforeTimeSpanExpiration);
            }
            else
            {
                Console.WriteLine("Timed out waiting for user input!");
                readLineResult = null;
            }
            return readLineResult;
        }
    }
}