﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using IO.Practiceware.DbSync.Extensions;
using IO.Practiceware.DbSync.Properties;
using IO.Practiceware.DbSync.Synchronizers;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using RedGate.Shared.SQL.ExecutionBlock;
using RedGate.Shared.Utils.IO;
using RedGate.SQLDataCompare.Engine;
using RedGate.SQLDataCompare.Engine.ResultsStore;
using ComparisonOptions = RedGate.SQLCompare.Engine.ComparisonOptions;
using Database = RedGate.SQLCompare.Engine.Database;
using SessionSettings = RedGate.SQLCompare.Engine.SessionSettings;

namespace IO.Practiceware.DbSync
{
    /// <summary>
    /// Loads pre-requisites for DbSync.
    /// </summary>
    public static class DbSyncBootstrapper
    {
        static DbSyncBootstrapper()
        {
            LoadRedGateAssemblies();

            if (!RuntimePolicyHelper.LegacyV2RuntimeEnabledSuccessfully)
            {
                throw new InvalidOperationException("Tried to bind version 2 of the runtime for compatability but failed.");
            }
        }

        private static void LoadRedGateAssemblies()
        {
            foreach (var bytes in typeof(Resources).GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(p => p.PropertyType == typeof(byte[])).Select(p => (byte[])p.GetValue(null, null)).ToArray())
            {
                Assembly.Load(bytes);
            }
            AppDomain.CurrentDomain.AssemblyResolve += OnCurrentDomainAssemblyResolve;
        }

        private static Assembly OnCurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var asm = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName.Split(',')[0] == args.Name.Split(',')[0]);
            return asm;
        }

        public static void Initialize() { }
    }

    /// <summary>
    /// <para>Compares and synchronizes a pair of databases.</para>
    /// <para>Accepts a configuration object which allows client code to specify how the comparison and synchronization is performed.</para>
    /// </summary>
    public class Synchronizer
    {
        private Synchronizer()
        {
            DbSyncBootstrapper.Initialize();
            OverrideTransactionScopeMaximumTimeout(TimeSpan.MaxValue);
        }

        /// <summary>
        /// Set the transaction timeout for the current session.
        /// </summary>
        /// <param name="timeout">time out</param>
        public static void OverrideTransactionScopeMaximumTimeout(TimeSpan timeout)
        {
            try
            {
                Type transType = typeof(TransactionManager);
                FieldInfo cachedMaxTimeOut = transType.GetField("_cachedMaxTimeout", BindingFlags.NonPublic | BindingFlags.Static);
                FieldInfo maximumTimeOut = transType.GetField("_maximumTimeout", BindingFlags.NonPublic | BindingFlags.Static);

                if (null != cachedMaxTimeOut)
                {
                    cachedMaxTimeOut.SetValue(null, true);
                }
                if (null != maximumTimeOut)
                {
                    maximumTimeOut.SetValue(null, timeout);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IO.Practiceware.DbSync.Synchronizer"/> class.
        /// </summary>
        /// <param name="db1Source">The server source of the first database</param>
        /// <param name="db2Source">The server source of the second database</param>
        /// <param name="db1Name">The name of the first database</param>
        /// <param name="db2Name">The name of the second database</param>
        public Synchronizer(string db1Source, string db2Source, string db1Name, string db2Name)
            : this()
        {
            _dbSyncParameters = new DbSyncParameters();
            _dbSyncParameters.WellDefinedRulesOnly = true;
            _dbSyncParameters.AutoSelectOnly = true;
            _dbSyncParameters.ProductionServer1Name = db1Source;
            _dbSyncParameters.ProductionServer2Name = db2Source;
            _dbSyncParameters.Db1Name = db1Name;
            _dbSyncParameters.Db2Name = db2Name;
        }

        /// <summary>
        /// <para>Initializes a new instance of the <see cref="IO.Practiceware.DbSync.Synchronizer"/> class</para>
        /// <para> and accepts a <paramref name="dbSyncParameters"/> object.</para>
        /// </summary>
        /// <param name="dbSyncParameters">An object which contains configuration options for the synchronization process.</param>
        public Synchronizer(DbSyncParameters dbSyncParameters)
            : this()
        {
            if (dbSyncParameters == null)
            {
                throw new ArgumentNullException("dbSyncParameters", "The dbSyncParameters argument cannot be null");
            }

            _dbSyncParameters = dbSyncParameters;
        }

        #region Members

        /// <summary>
        /// <para>Gets the <paramref cref="DbSyncParameters"/> object.</para>
        /// <para><seealso cref="DbSyncParameters"/></para>
        /// </summary>
        public DbSyncParameters DbSyncParameters
        {
            get
            {
                if (_running)
                {
                    throw new InvalidOperationException("DbSyncParameters are unavailable while the synchronizer is running");
                }
                return _dbSyncParameters;
            }
        }

        private readonly DbSyncParameters _dbSyncParameters;

        /// <summary>
        /// Gets a value that determines whether or not the synchronizer has started execution
        /// </summary>
        public bool IsCurrentlyExecuting
        {
            get
            {
                return _running;
            }
        }

        private bool _running;

        #endregion Members

        private void ThrowIfCurrentlyExecuting()
        {
            if (_running)
            {
                throw new InvalidOperationException("Synchronizer is currently executing");
            }
        }

        /// <summary>
        /// Run the synchronizer based on configuration options specified in the object
        /// </summary>        
        public void Run()
        {
            ThrowIfCurrentlyExecuting();

            _running = true;

            try
            {
                var dbSyncParameters = _dbSyncParameters;

                if (dbSyncParameters == null)
                {
                    // ReSharper disable once NotResolvedInText
                    throw new ArgumentNullException("dbSyncParameters", "The dbSyncParameters argument cannot be null");
                }

                // Initialize dbPairings if DbSync is running in HubMode.
                var dbPairings = SqlCompareHelper.GetDbPairings(dbSyncParameters);
                if (dbPairings == null)
                {
                    // ReSharper disable once NotResolvedInText
                    throw new ArgumentNullException("dbPairings is null");
                }

                var connectionString = new SqlConnectionStringBuilder
                {
                    DataSource = dbPairings[0].Item1.ServerName,
                    InitialCatalog = dbPairings[0].Item1.DatabaseName,
                    IntegratedSecurity = true
                };

                ITableSynchronizer[] synchronizers = { };
                if (dbSyncParameters.DataSync)
                {
                    synchronizers = LoadAndFilterSynchronizers(dbSyncParameters, connectionString);
                }
                // Backup seed values
                SqlCompareHelper.BackupSeedValues(dbPairings);

                // Backup databases
                if (dbSyncParameters.PerformDatabaseBackups)
                {
                    BackupDatabases(dbPairings);
                }

                if (dbSyncParameters.BackupDatabasesOnly)
                {
                    return;
                }

                RunSynchronizers(synchronizers, dbPairings, dbSyncParameters);
            }
            finally
            {
                _running = false;
            }
        }

        /// <summary>
        /// Run the synchronizer based on configuration options specified in the dbSyncParameters object.
        /// Ignores dbSyncParameters.DataSync and applies dbSyncParameters.SchemaSync no matter which values are
        /// specified in the dbSyncParameters
        /// </summary>        
        public void RunSchemaSync()
        {
            DbSyncParameters.SchemaSync = true;
            DbSyncParameters.DataSync = false;
            Run();
        }

        //todo: have method throw an error if backups don't work
        internal static void BackupDatabases(Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[] dbPairings)
        {
            var databasesToBackUp = new List<string>();

            foreach (var pair in dbPairings)
            {
                if (!(databasesToBackUp.Contains(pair.Item1.ConnectionString)))
                    databasesToBackUp.Add(pair.Item1.ConnectionString);

                if (!(databasesToBackUp.Contains(pair.Item2.ConnectionString)))
                    databasesToBackUp.Add(pair.Item2.ConnectionString);
            }

            foreach (var connectionString in databasesToBackUp)
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    BackupDatabase(connection);
                }
            }
        }

        internal static void BackupDatabase(SqlConnection connection)
        {
            var backupDirectoryQuery =
                connection.Execute<DataTable>(
                    @"EXEC  master.dbo.xp_instance_regread
 N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'");

            string backupDirectory = backupDirectoryQuery.Rows[0]["Data"].ToString();

            Console.WriteLine("Backing up database with connection: {0}.", connection.ConnectionString);

            string backupName = GetBackupName(connection, backupDirectory);

            connection.Execute("BACKUP DATABASE " + connection.Database + @" TO DISK ='" + backupName + @"'");
        }

        private static string GetBackupName(SqlConnection connection, string backupDirectoryPath)
        {
            string filePath = backupDirectoryPath + @"\" + connection.Database + @"DbSync" +
                              DateTime.Now.ToString("yyyyMMdd HH.mm");
            if (!(File.Exists(filePath + @".bak")))
            {
                filePath += @".bak";
                return filePath;
            }
            for (int i = 2; i < int.MaxValue; i++)
            {
                bool indexerReplaced = false;

                if (i > 2)
                {
                    if (filePath.LastIndexOf("(" + (i - 1) + ")", StringComparison.Ordinal) >=
                        filePath.Length - 4)
                    {
                        filePath = filePath.Replace("(" + (i - 1) + ")", "(" + i + ")");
                        indexerReplaced = true;
                    }
                }

                if (!(indexerReplaced))
                    filePath += "(" + i + ")";

                if (!(File.Exists(filePath + @".bak")))
                {
                    filePath += @".bak";
                    return filePath;
                }
            }

            throw new ArgumentException("Unable to set a unique backup name.");
        }

        private static ITableSynchronizer[] LoadAndFilterSynchronizers(DbSyncParameters parameters, SqlConnectionStringBuilder connectionStringForGenericTableSynchronizers)
        {
            var synchronizers = Assembly.GetExecutingAssembly()
                                        .GetTypes()
                                        .Where(t => typeof(ITableSynchronizer).IsAssignableFrom(t) && t.IsClass && !(t.IsAbstract) && t.GetConstructor(Type.EmptyTypes) != null)
                                        .Select(t => (ITableSynchronizer)Activator.CreateInstance(t)).ToArray();

            synchronizers = synchronizers.Concat(GetGenericTableSynchronizers(synchronizers, connectionStringForGenericTableSynchronizers)).Distinct().ToArray();

            if (parameters.ObjectSpecificationMode)
            {
                if (parameters.TablesToInclude != null && parameters.TablesToInclude.Any())
                {
                    synchronizers = synchronizers.Where(s => parameters.TablesToInclude.Any(excludeTable => excludeTable.Equals(s.TableName, StringComparison.OrdinalIgnoreCase))).ToArray();
                }

                if (parameters.TablesToExclude != null && parameters.TablesToExclude.Any())
                {
                    synchronizers = synchronizers.Where(s => !parameters.TablesToExclude.Any(excludeTable => excludeTable.Equals(s.TableName, StringComparison.OrdinalIgnoreCase))).ToArray();
                }
            }

            return synchronizers.OrderByDescending(p => p.Priority).ThenBy(p => p.TableName).ToArray();
        }

        private static IEnumerable<ITableSynchronizer> GetGenericTableSynchronizers(IEnumerable<ITableSynchronizer> otherSynchronizers, SqlConnectionStringBuilder connectionString)
        {
            const string getGenericSynchronizersQuery = @"
WITH TablesCTE(SchemaName, TableName, TableID, Ordinal) AS
(
    SELECT
        OBJECT_SCHEMA_NAME(so.object_id) AS SchemaName,
        OBJECT_NAME(so.object_id) AS TableName,
        so.object_id AS TableID,
        0 AS Ordinal
    FROM
        sys.objects AS so
    WHERE
        so.type = 'U'
        AND so.is_ms_Shipped = 0
    UNION ALL
    SELECT
        OBJECT_SCHEMA_NAME(so.object_id) AS SchemaName,
        OBJECT_NAME(so.object_id) AS TableName,
        so.object_id AS TableID,
        tt.Ordinal + 1 AS Ordinal
    FROM
        sys.objects AS so
    INNER JOIN sys.foreign_keys AS f
        ON f.parent_object_id = so.object_id
        AND f.parent_object_id != f.referenced_object_id
    INNER JOIN TablesCTE AS tt
        ON f.referenced_object_id = tt.TableID
    WHERE
        so.type = 'U'
        AND so.is_ms_Shipped = 0
)

SELECT DISTINCT
        t.Ordinal,
        t.SchemaName,
        t.TableName,
        t.TableID
    FROM
        TablesCTE AS t
    INNER JOIN
        (
            SELECT
                itt.SchemaName as SchemaName,
                itt.TableName as TableName,
                itt.TableID as TableID,
                Max(itt.Ordinal) as Ordinal
            FROM
                TablesCTE AS itt
            GROUP BY
                itt.SchemaName,
                itt.TableName,
                itt.TableID
        ) AS tt
        ON t.TableID = tt.TableID
        AND t.Ordinal = tt.Ordinal
WHERE t.SchemaName = 'model' AND t.TableName NOT LIKE 'conflict_%'
ORDER BY
    t.Ordinal,
    t.TableName
";

            using (var connection = new SqlConnection(connectionString.ToString()))
            {
                var orderedTables = connection.Execute<DataTable>(getGenericSynchronizersQuery)
                    .AsEnumerable().Select(r => new
                    {
                        ObjectName = string.Format("[{0}].[{1}]", r["SchemaName"] as string, r["TableName"] as string),
                        Ordinal = (int)r["Ordinal"]
                    }).OrderByDescending(i => i.Ordinal)
                    .Select((o, i) => new
                    {
                        Priority = i - 1000,
                        o.ObjectName
                    })
                    .ToArray();

                var synchronizers = new List<GenericTableSynchronizer>();

                foreach (var table in orderedTables.Where(t => !otherSynchronizers.Any(s => s.TableName.StripBrackets().Equals(t.ObjectName.StripBrackets()))))
                {
                    synchronizers.Add(new GenericTableSynchronizer(table.ObjectName, table.Priority));
                }

                return synchronizers;
            }
        }

        private static void RunDataSynchronizer(Tuple<DbSyncDatabase, DbSyncDatabase> databases, ITableSynchronizer synchronizer, DbSyncParameters dbSyncParameters)
        {
            ExecutionBlock block = RunDataSynchronizerComparison(databases, synchronizer, dbSyncParameters);

            if (block == null)
                return;

            using (block)
            {
                using (var batchedBlock = GetBatchedScript(block, synchronizer))
                {
                    string scriptName = GetScriptName(databases.Item1.Database.ConnectionProperties.ServerName, databases.Item1.Database.ConnectionProperties.DatabaseName, databases.Item2.Database.ConnectionProperties.ServerName, databases.Item2.Database.ConnectionProperties.DatabaseName, synchronizer.TableName,
                        dbSyncParameters.DbSyncGeneratedScriptPath == null || dbSyncParameters.DbSyncGeneratedScriptPath.Trim() == string.Empty ? null : dbSyncParameters.DbSyncGeneratedScriptPath);

                    var saver = new BlockSaver(scriptName, EncodingType.UTF8, batchedBlock);
                    try
                    {
                        saver.SaveToFile();
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(string.Format("Error saving script file to {0}.", scriptName), ex);
                    }

                    if (dbSyncParameters.ExecuteDbSyncScriptAutomatically)
                    {
                        var stopwatch = new Stopwatch();
                        stopwatch.Start();

                        var executor = new BlockExecutor();
                        using (var connection = databases.Item2.Database.CreateConnection())
                        {
                            // RedGate API has a bug with variable scope...so run the script manually if there is more than 1 reference to a variable
                            if (Enumerable.Range(0, (int)batchedBlock.BatchCount).Any(i => new Regex("@pv").Matches(batchedBlock.GetBatch(i).Contents).Count > 1))
                            {
                                var server = new Server(new ServerConnection(connection));
                                server.ConnectionContext.ExecuteNonQuery(batchedBlock.GetString());
                            }
                            else
                            {
                                executor.ExecuteBlock(batchedBlock, connection);
                            }
                        }

                        Trace.TraceInformation("Ran script against {0}.{1} in {2}ms.", databases.Item2.Database.ConnectionProperties.ServerName, databases.Item2.Database.ConnectionProperties.DatabaseName, stopwatch.ElapsedMilliseconds);
                    }
                }
            }
        }

        private static void RunSynchronizers(ITableSynchronizer[] synchronizers, IEnumerable<Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>> dbPairings, DbSyncParameters dbSyncParameters)
        {
            foreach (var pair in dbPairings)
            {
                Trace.TraceInformation("Running DbSync from {0} to {1}.", pair.Item1, pair.Item2);

                using (var db1 = new DbSyncDatabase(pair.Item1.ConnectionString, pair.Item1.ProductionServerName, dbSyncParameters))
                using (var db2 = new DbSyncDatabase(pair.Item2.ConnectionString, pair.Item2.ProductionServerName, dbSyncParameters))
                {
                    var dbPair = Tuple.Create(db1, db2);

                    if (dbSyncParameters.SchemaSync)
                    {
                        RunSchemaSynchronizers(dbPair, dbSyncParameters);
                    }

                    if (!dbSyncParameters.DataSync)
                        continue;

                    synchronizers
                        .GroupBy(s => s.Priority).OrderByDescending(g => g.Key).ToList().ForEach(g =>
                            g.OrderBy(s => s.TableName).AsParallel().WithDegreeOfParallelism(12)
                            .ForAll(synchronizer =>
                            {
                                Trace.TraceInformation("Synchronizing {0}.", synchronizer.TableName);

                                RunDataSynchronizer(dbPair, synchronizer, dbSyncParameters);

                                if (dbSyncParameters.RunScript == RunScriptOption.TwoWay)
                                {
                                    RunDataSynchronizer(dbPair.Reverse(), synchronizer, dbSyncParameters);
                                }

                            }));
                }
            }
        }

        private static void RunSchemaSynchronizers(Tuple<DbSyncDatabase, DbSyncDatabase> databases, DbSyncParameters dbSyncParameters)
        {
            var db1 = databases.Item1.Database;
            var db2 = databases.Item2.Database;

            var schemaDifferences = db1.CompareWith(db2, dbSyncParameters.CompareOptions);
            foreach (var diff in schemaDifferences)
            {
                if (dbSyncParameters.ObjectTypesToInclude.All(x => diff.DatabaseObjectType != x))
                {
                    diff.Selected = false;
                }

                if (dbSyncParameters.TablesToInclude != null && diff.DatabaseObjectType == RedGate.SQLCompare.Engine.ObjectType.Table)
                {
                    if (dbSyncParameters.TablesToInclude.All(x => !diff.Name.Equals(x, StringComparison.OrdinalIgnoreCase)))
                    {
                        diff.Selected = false;
                    }
                }

                if (dbSyncParameters.TablesToExclude != null && diff.DatabaseObjectType == RedGate.SQLCompare.Engine.ObjectType.Table)
                {
                    if (dbSyncParameters.TablesToExclude.Any(x => diff.Name.Equals(x, StringComparison.OrdinalIgnoreCase)))
                    {
                        diff.Selected = false;
                    }
                }

                var replicationObjectPrefixes = new[] { "sp_MSupd_", "sp_MSdel_", "sp_MSins_" };

                if (replicationObjectPrefixes.Any(o => diff.Name.Contains(o)))
                {
                    diff.Selected = false;
                }
            }

            var work = new RedGate.SQLCompare.Engine.Work();

            work.BuildFromDifferences(schemaDifferences, dbSyncParameters.CompareOptions, true);
            var executionBlock = work.ExecutionBlock;

            using (executionBlock)
            {
                var scriptPath = dbSyncParameters.DbSyncGeneratedScriptPath == null || dbSyncParameters.DbSyncGeneratedScriptPath.Trim() == string.Empty ? null : dbSyncParameters.DbSyncGeneratedScriptPath;
                var scriptFullPathAndName = GetScriptName(databases.Item1.Database.ConnectionProperties.ServerName,
                                                    databases.Item1.Database.ConnectionProperties.DatabaseName,
                                                    databases.Item2.Database.ConnectionProperties.ServerName,
                                                    databases.Item2.Database.ConnectionProperties.DatabaseName, "SchemaSync", scriptPath);

                var saver = new BlockSaver(scriptFullPathAndName, EncodingType.UTF8, executionBlock);
                try
                {
                    saver.SaveToFile();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Error saving script file to {0}.", scriptFullPathAndName), ex);
                }


                if (dbSyncParameters.ExecuteDbSyncScriptAutomatically)
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    var executor = new BlockExecutor();
                    using (var connection = databases.Item2.Database.CreateConnection())
                    {
                        // RedGate API has a bug with variable scope...so run the script manually if there is more than 1 reference to a variable
                        if (Enumerable.Range(0, (int)executionBlock.BatchCount).Any(i => new Regex("@pv").Matches(executionBlock.GetBatch(i).Contents).Count > 1))
                        {
                            var server = new Server(new ServerConnection(connection));
                            server.ConnectionContext.ExecuteNonQuery(executionBlock.GetString());
                        }
                        else
                        {
                            executor.ExecuteBlock(executionBlock, connection);
                        }
                    }

                    Trace.TraceInformation("Ran script against {0}.{1} in {2}ms.", databases.Item2.Database.ConnectionProperties.ServerName, databases.Item2.Database.ConnectionProperties.DatabaseName, stopwatch.ElapsedMilliseconds);
                }
            }
        }

        private static string GetScriptName(string sourceServer, string sourceDb, string targetServer, string targetDb,
                                            string tableName, string path = null)
        {
            var pathToSaveScripts = path ?? "scripts";

            if (!Directory.Exists(pathToSaveScripts))
            {
                Directory.CreateDirectory(pathToSaveScripts);
            }

            return string.Format("{0}\\{1}.{2} To {3}.{4} {5}.sql", pathToSaveScripts, sourceServer.Replace("\\", "_"), sourceDb,
                                 targetServer.Replace("\\", "_"), targetDb, tableName);
        }

        private static ExecutionBlock GetBatchedScript(ExecutionBlock block, ITableSynchronizer synchronizer)
        {
            string batchPrefix = @"

BEGIN TRANSACTION
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON;
";

            batchPrefix += string.Format(
                @"
IF OBJECT_ID(N'tempdb..#TriggersAndConstraints') IS NOT NULL
    DROP TABLE #TriggersAndConstraints

DECLARE @triggerNames{{0}} xml
EXEC dbo.DisableEnabledTriggers '{0}', @names = @triggerNames{{0}} OUTPUT
DECLARE @constraintNames{{0}} xml
EXEC dbo.DisableEnabledCheckConstraints '{0}', @names = @constraintNames{{0}} OUTPUT

SELECT @triggerNames{{0}} AS TriggerNames, @constraintNames{{0}} AS ConstraintNames
INTO #TriggersAndConstraints
",
                synchronizer.TableName);

            batchPrefix += string.Format(@"
IF OBJECT_ID(N'tempdb..#CurrentIdentity') IS NOT NULL
    DROP TABLE #CurrentIdentity

IF (OBJECTPROPERTY(object_id('{0}'), 'TableHasIdentity')=1)
BEGIN
    DECLARE @currentIdentity{{0}} bigint

    SET @currentIdentity{{0}}=ident_current('{0}')
    SET IDENTITY_INSERT {0} ON

    SELECT @currentIdentity{{0}} AS CurrentIdentity
    INTO #CurrentIdentity

END

", synchronizer.TableName);

            if (synchronizer.ScriptPrefix != null)
            {
                batchPrefix += Environment.NewLine + synchronizer.ScriptPrefix + Environment.NewLine;
            }

            string batchSuffix =
                string.Format(@"
IF (OBJECTPROPERTY(object_id('{0}'), 'TableHasIdentity')=1)
BEGIN
    DECLARE @currentIdentity{{0}} bigint

    SELECT @currentIdentity{{0}} = CurrentIdentity
    FROM #CurrentIdentity

    DBCC CHECKIDENT('{0}', RESEED, @currentIdentity{{0}})
END
", synchronizer.TableName);

            batchSuffix += string.Format(@"

DECLARE @disabledTriggerNames{{0}} xml
DECLARE @disabledConstraintNames{{0}} xml

SELECT @disabledTriggerNames{{0}} = TriggerNames, @disabledConstraintNames{{0}} = ConstraintNames
FROM #TriggersAndConstraints

EXEC dbo.EnableTriggers @disabledTriggerNames{{0}}
EXEC dbo.EnableCheckConstraints @disabledConstraintNames{{0}}
");

            if (synchronizer.ScriptSuffix != null)
            {
                batchSuffix += Environment.NewLine + synchronizer.ScriptSuffix + Environment.NewLine;
            }

            batchSuffix += @"

COMMIT TRANSACTION

";

            var batchedBlock = new ExecutionBlock(true);

            if (block.ByteCount >= 50000000)
            {
                batchedBlock.AddBatch(GetDisableAllIndexesSql(synchronizer.TableName));
            }

            bool batchHasActions = false;
            long lastPvBatch = 0;
            bool? hasPv = null;

            for (long batchIndex = 0; batchIndex < block.BatchCount; batchIndex++)
            {
                var batch = block.GetBatch(batchIndex);


                // this script may use @pv
                if (!hasPv.HasValue && batch.Contents.Contains("DECLARE @pv binary(16)\r\n"))
                {
                    hasPv = true;
                }
                else if (!hasPv.HasValue && batchHasActions) // if we've encountered any actions, there's no pv
                {
                    hasPv = false;
                }

                if (batch.Contents.Trim() == "BEGIN TRANSACTION" || batch.Contents.Trim() == "COMMIT TRANSACTION")
                    continue;

                if (!batch.Marker)
                {
                    if (!batchHasActions && new[] { "UPDATE(TEXT)? \\[", "INSERT INTO \\[", "DELETE FROM \\[", "EXEC\\(" }.Select(i => "(^|\\r\\n)" + i)
                        .Any(i => Regex.IsMatch(batch.Contents, i)))
                    {
                        batchHasActions = true;
                        batchedBlock.AddBatch(string.Format(batchPrefix, batchIndex));
                        lastPvBatch = batchIndex;

                        if (hasPv == true)
                        {
                            batchedBlock.AddBatch(string.Format("DECLARE @pv{0} binary(16)", batchIndex));
                        }
                    }

                    var contents = batch.Contents.Replace("DECLARE @pv binary(16)", string.Empty)
                        .Replace("@pv", string.Format("@pv{0}", lastPvBatch))
                        .Replace(string.Format("'SELECT @pv{0}=",lastPvBatch), string.Format("'DECLARE @pv{0} binary(16); SELECT @pv{0}=", lastPvBatch));
                    

                    batchedBlock.AddBatch(contents);
                }
                else
                {
                    batchedBlock.AddBatchMarker();

                    if (batchHasActions)
                    {
                        batchedBlock.AddBatch(string.Format(batchSuffix, batchIndex));

                        batchHasActions = false;
                    }
                }
            }

            if (batchHasActions)
            {
                batchedBlock.AddBatch(batchSuffix);
            }
            batchedBlock.AddBatchMarker();

            if (block.ByteCount >= 50000000)
            {
                batchedBlock.AddBatch(GetRebuildAllIndexesSql(synchronizer.TableName));
            }

            return batchedBlock;
        }

        private static string GetDisableAllIndexesSql(string tableName)
        {
            return string.Format(
@"DECLARE @sql nvarchar(max)
SET @sql = ''
SELECT @sql = @sql + 'ALTER INDEX [' + i.name + '] ON {0} DISABLE;
'
FROM sys.indexes i WHERE i.object_id = OBJECT_ID('{0}') AND i.is_primary_key = 0

EXEC sp_executesql @sql

SET @sql = ''

;WITH DependentObjectCTE (DependentObjectId, DependentObjectName, ReferencedObjectName, ReferencedObjectId) AS 
(
SELECT DISTINCT
	sd.object_id,
	ds.name + '.' + do.name AS Name,
	ReferencedObject = rs.name + '.' + ro.name,
	ReferencedObjectId = sd.referenced_major_id
FROM    
	sys.sql_dependencies sd
	JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
	JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
	JOIN sys.objects do ON sd.object_id = do.object_id
	JOIN sys.schemas ds ON do.schema_id = ds.schema_id			
WHERE   
	sd.referenced_major_id = OBJECT_ID('{0}')
UNION ALL
SELECT
	sd.object_id,
	ds.name + '.' + do.name,
	rs.name + '.' + ro.name,
	sd.referenced_major_id
FROM    
	sys.sql_dependencies sd
	JOIN sys.objects ro ON sd.referenced_major_id = ro.object_id
	JOIN sys.schemas rs ON ro.schema_id = rs.schema_id
	JOIN sys.objects do ON sd.object_id = do.object_id
	JOIN sys.schemas ds ON do.schema_id = ds.schema_id	
	JOIN DependentObjectCTE doCTE ON sd.referenced_major_id = doCTE.DependentObjectId       
WHERE
	sd.referenced_major_id <> sd.object_id
)
SELECT DISTINCT 
@sql = @sql + 'ALTER INDEX [' + i.name + '] ON '+ QUOTENAME(OBJECT_SCHEMA_NAME(c.DependentObjectId)) + '.' + QUOTENAME(OBJECT_NAME(c.DependentObjectId))+' DISABLE;
'
FROM DependentObjectCTE c
JOIN sys.objects o ON o.object_id = c.DependentObjectId
JOIN sys.indexes i ON i.object_id = c.DependentObjectId
WHERE OBJECTPROPERTY(c.DependentObjectId,'IsIndexed') = 1
    AND o.type = 'V'

EXEC sp_executesql @sql

IF OBJECT_ID('tempdb..#DependentIndexesToRebuild') IS NOT NULL 
	DROP TABLE #DependentIndexesToRebuild

SELECT @sql AS Sql INTO #DependentIndexesToRebuild

", tableName);
        }


        private static string GetRebuildAllIndexesSql(string tableName)
        {
            return string.Format(
@"DECLARE @sql nvarchar(max)
SET @sql = ''
SELECT @sql = @sql + 'ALTER INDEX [' + i.name + '] ON {0} REBUILD;
'
FROM sys.indexes i WHERE i.object_id = OBJECT_ID('{0}') AND i.is_primary_key = 0

EXEC sp_executesql @sql

SELECT @sql = REPLACE(Sql, 'DISABLE;', 'REBUILD;') FROM #DependentIndexesToRebuild

EXEC sp_executesql @sql
", tableName);
        }

        private static ExecutionBlock RunDataSynchronizerComparison(Tuple<DbSyncDatabase, DbSyncDatabase> databases, ITableSynchronizer synchronizer, DbSyncParameters dbSyncParameters)
        {
            var db1 = databases.Item1.Database;
            var db2 = databases.Item2.Database;
            using (var ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted, Timeout = TimeSpan.MaxValue }))
            using (ComparisonSession session = SqlCompareHelper.CreateComparisonSession())
            {
                // Don't run data sync if not a table
                var isTableSql = String.Format(@"SELECT OBJECT_ID('{0}','U')", synchronizer.TableName);
                if (!db1.CreateConnection().ExecuteAndDispose<int?>(isTableSql).HasValue)
                {
                    Trace.TraceInformation("On database {1}.{2}, {0} is not a table.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName);
                    return null;
                }
                if (!db2.CreateConnection().ExecuteAndDispose<int?>(isTableSql).HasValue)
                {
                    Trace.TraceInformation("On database {1}.{2}, {0} is not a table.", synchronizer.TableName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);
                    return null;
                }

                session.Options.ComparisonOptions = ComparisonOptions.CompressTemporaryFiles;

                synchronizer.Initialize(db1, db2);

                var getRowCountSql = "SELECT COUNT(*) FROM " + synchronizer.TableName + " WITH(NOLOCK)";

                if (synchronizer.ExcludeIfRowCountSame
                    && db1.CreateConnection().ExecuteAndDispose<int>(getRowCountSql) == db2.CreateConnection().ExecuteAndDispose<int>(getRowCountSql))
                {
                    Trace.TraceInformation("Skipped Table {0} on {1}.{2} to {3}.{4} due to matching row count.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);
                    return null;
                }

                var mappings = new SchemaMappings();
                mappings.CreateMappings(db1, db2);
                ConfigureMappings(db1, db2, synchronizer, mappings, dbSyncParameters.IgnoreMsreplComparison);

                var mapping = mappings.TableMappings.SingleOrDefault(m => m.Include);

                if (mapping == null)
                {
                    return null;
                }

                var stopwatch = new Stopwatch();

                stopwatch.Reset();
                stopwatch.Start();

                RetryExtensions.ExecuteWithRetry(
                    () => session.CompareDatabases(db1, db2, mappings,
                                                   SessionSettings.IncludeRecordsInOne
                                                   | SessionSettings.IncludeRecordsInTwo
                                                   | SessionSettings.IncludeDifferentRecords));

                Trace.TraceInformation("Ran comparison for {0} on {1}.{2} to {3}.{4} in {5}ms.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName, stopwatch.ElapsedMilliseconds);

                TableDifference tableDifference = session.TableDifferences[synchronizer.TableName];

                if (tableDifference != null && tableDifference.ResultsStore != null &&
                    (tableDifference.DifferencesSummary.DifferenceCount(Row.RowType.In1) > 0 || tableDifference.DifferencesSummary.DifferenceCount(Row.RowType.In2) > 0 || tableDifference.DifferencesSummary.DifferenceCount(Row.RowType.Different) > 0))
                {
                    stopwatch.Reset();
                    stopwatch.Start();

                    try
                    {
                        using (Reader reader = tableDifference.ResultsStore.GetReader())
                        {
                            using (var connection1 = new DbSyncConnection(session.Database1.CreateConnection(), databases.Item1.ProductionServerName))
                            using (var connection2 = new DbSyncConnection(session.Database2.CreateConnection(), databases.Item2.ProductionServerName))
                            {
                                var provider = new SqlProvider { Options = session.Options };
                                provider.MaxByteSizeOfTransactions = 1048576;

                                // null out where clause to exclude unnecessary conditions in generated script
                                tableDifference.ResultsStore.Where = new WhereClause();


                                ExecutionBlock migration;
                                if (dbSyncParameters.Database1DataAlwaysWins)
                                {
                                    migration = provider.GetMigrationSQL(session, tableDifference, true);
                                }
                                else
                                {
                                    var choices = new List<bool>();

                                    migration = provider.GetMigrationSQL(session, tableDifference,
                                        r =>
                                        {
                                            var choice = synchronizer.SelectRecord(session, r, tableDifference, reader.GetRow(r.Bookmark), dbSyncParameters, connection1, connection2);
                                            choices.Add(choice);
                                            return choice;
                                        }, true);

                                    if (choices.All(c => c == false))
                                    {
                                        Trace.TraceInformation("Skipped Table {0} on {1}.{2} to {3}.{4} after full comparison because there is no data to synchronize based on choices.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);

                                        return null;
                                    }
                                }
                                Trace.TraceInformation("Generated synchronization script for {0} on {1}.{2} to {3}.{4} in {5}ms.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName, stopwatch.ElapsedMilliseconds);

                                return migration;
                            }
                        }
                    }
                    finally
                    {
                        tableDifference.Dispose();
                    }
                }
                ts.Complete();
            }

            Trace.TraceInformation("{0} on {1}.{2} and {3}.{4} are identical.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);

            return null;
        }

        private static void ConfigureMappings(Database db1, Database db2, ITableSynchronizer synchronizer, SchemaMappings mappings, bool ignoreMsreplComparison)
        {
            const string checksumQuery =
                @"
SELECT CHECKSUM_AGG(BINARY_CHECKSUM(*)) AS [Checksum], MIN(__RangeColumn__) AS RangeMin, MAX(__RangeColumn__) AS RangeMax
FROM (
	SELECT ({2}) / {3} AS __GroupId__, HASHBYTES('MD5', {1})) AS [Hash], {2} AS __RangeColumn__
	FROM {0} x WITH(NOLOCK)
    {4}
	) groups
GROUP BY groups.__GroupId__
";

            const string keyColumnsQuery =
                @"
SELECT ccu.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS tc
INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
INNER JOIN sys.objects o ON o.object_id = OBJECT_ID(@tableName) AND tc.TABLE_NAME = o.name AND tc.TABLE_SCHEMA = SCHEMA_NAME(o.schema_id)
WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY' AND ccu.TABLE_SCHEMA = SCHEMA_NAME(o.schema_id)";

            string columnsQuery =
                @"
SELECT c.Name, t.Name FROM sys.columns c join sys.types t on c.system_type_id = t.system_type_id WHERE object_id = OBJECT_ID(@tableName)";

            if (ignoreMsreplComparison)
            {
                columnsQuery += Environment.NewLine + string.Format("AND c.Name <> '{0}'", "msrepl_tran_version");
            }
            columnsQuery += Environment.NewLine + "ORDER BY c.Name";

            const string rangeColumnAndDivisorQuery = @"
--DECLARE @divisor int
--DECLARE @rangeColumn nvarchar(max)

IF @rangeColumn IS NULL AND OBJECTPROPERTY(OBJECT_ID(@tableName), 'TableHasIdentity') = 1
BEGIN
	SELECT TOP 1 @rangeColumn = ccu.COLUMN_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS tc
	INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
	INNER JOIN sys.objects o ON o.object_id = OBJECT_ID(@tableName) AND tc.TABLE_NAME = o.name AND tc.TABLE_SCHEMA = SCHEMA_NAME(o.schema_id)
	WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY' AND ccu.TABLE_SCHEMA = SCHEMA_NAME(o.schema_id)
END
ELSE IF @rangeColumn IS NULL
BEGIN
    IF @divisor IS NULL
	    SET @divisor = 5

	SELECT TOP 1 @rangeColumn = 'CONVERT(nvarchar, ' + c.name + ', 112)'
	FROM sys.columns c
	JOIN sys.types t ON c.system_type_id = t.system_type_id
	WHERE t.name LIKE 'datetime%' AND c.object_id = OBJECT_ID(@tableName) AND c.is_nullable = 0
END

IF @divisor IS NULL
    SET @divisor = 8000

SELECT @rangeColumn, @divisor
";

            foreach (TableMapping mapping in mappings.TableMappings)
            {
                if (ignoreMsreplComparison && mapping.FieldMappings["msrepl_tran_version"] != null)
                {
                    mapping.FieldMappings["msrepl_tran_version"].Include = false;
                }

                if (mapping.Obj1 == null || mapping.Obj2 == null || !mapping.Obj1.FullyQualifiedName.Equals(synchronizer.TableName, StringComparison.OrdinalIgnoreCase))
                {
                    mapping.Include = false;
                }
                else
                {
                    var sw = new Stopwatch();
                    sw.Start();

                    var checksumComparisonInfo = synchronizer.ChecksumComparisonInfo;

                    Func<Dictionary<string, object>> getQueryParameters = () => new Dictionary<string, object>
                                                                                    {
                                                                                        {"tableName", synchronizer.TableName},
                                                                                        {"divisor", checksumComparisonInfo.Divisor as object ?? DBNull.Value},
                                                                                        {"join", checksumComparisonInfo.Join as object ?? DBNull.Value},
                                                                                        {"rangeColumn", checksumComparisonInfo.RangeColumn as object ?? DBNull.Value},
                                                                                    };

                    var keyColumns = db1.CreateConnection().ExecuteAndDispose<DataTable>(keyColumnsQuery, getQueryParameters()).AsEnumerable().Select(r => r[0] as string).ToArray();

                    var columns = db1.CreateConnection().ExecuteAndDispose<DataTable>(columnsQuery, getQueryParameters()).AsEnumerable().Select(r => Tuple.Create((string)r[0], (string)r[1])).ToArray();
                    var convertedConcatenatedColumns = string.Format("CONVERT(varbinary(8000), {0}", string.Join(" + ", columns.Select(c => string.Format("COALESCE(CONVERT(varbinary(4000), {0}),'')",
                        c.Item2 == "text" || c.Item2 == "ntext" ? string.Format("CONVERT(varchar(4000), {0})", "x." + c.Item1) : "x." + c.Item1))));

                    var rangeColumnAndDivisor = db1.CreateConnection().ExecuteAndDispose<DataTable>(rangeColumnAndDivisorQuery, getQueryParameters())
                        .AsEnumerable().Select(r => new { RangeColumn = r[0] as string, Divisor = r[1] as string }).First();

                    if (checksumComparisonInfo.RangeColumn == null)
                    {
                        checksumComparisonInfo.RangeColumn = rangeColumnAndDivisor.RangeColumn;
                    }

                    if (checksumComparisonInfo.Divisor == null)
                    {
                        checksumComparisonInfo.Divisor = int.Parse(rangeColumnAndDivisor.Divisor);
                    }

                    var parameters = getQueryParameters();
                    parameters["convertedConcatenatedColumns"] = convertedConcatenatedColumns;

                    string query = null;
                    var requiredArguments = new object[] { synchronizer.TableName, convertedConcatenatedColumns, checksumComparisonInfo.RangeColumn, checksumComparisonInfo.Divisor };
                    if (!requiredArguments.Any(a => a == null || (a is string && (string)a == string.Empty)))
                    {
                        query = string.Format(checksumQuery, synchronizer.TableName, convertedConcatenatedColumns, checksumComparisonInfo.RangeColumn, checksumComparisonInfo.Divisor, checksumComparisonInfo.Join);
                    }

                    var whereClause = new StringBuilder();

                    // find checksum ranges that need comparing

                    // get server 1 checksums (checksums1)
                    var checksums1 = Task.Factory.StartNew(() =>
                    {
                        using (var connection = db1.CreateConnection())
                        {
                            if (query == null)
                                return new List<DataRow>();
                            var dt = connection.Execute<DataTable>(query);
                            if (dt == null)
                                throw new InvalidOperationException(string.Format("Query {0} returned null.", query));
                            return dt.AsEnumerable().ToList();
                        }
                    });

                    // get server 2 checksums (checksums2)
                    var checksums2 = Task.Factory.StartNew(() =>
                    {
                        using (var connection = db2.CreateConnection())
                        {
                            if (query == null)
                                return new List<DataRow>();
                            var dt = connection.Execute<DataTable>(query);
                            if (dt == null)
                                throw new InvalidOperationException(string.Format("Query {0} returned null.", query));
                            return dt.AsEnumerable().ToList();
                        }
                    });

                    int exclusionCount = 0;
                    int missingRangeCount = 0;
                    int differentChecksumCount = 0;

                    AddChecksumWhereClause(checksums1.Result, checksums2.Result, whereClause, checksumComparisonInfo, ref exclusionCount, ref missingRangeCount, ref differentChecksumCount);
                    AddChecksumWhereClause(checksums2.Result, checksums1.Result, whereClause, checksumComparisonInfo, ref exclusionCount, ref missingRangeCount, ref differentChecksumCount);

                    Trace.TraceInformation("Performed checksum analysis for table {0} on {1}.{2} to {3}.{4} in {5}ms. Excluded {6} ranges with identical checksums from comparison. {7} ranges are missing. {8} ranges had different checksums.{9}", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName, sw.ElapsedMilliseconds, exclusionCount, missingRangeCount, differentChecksumCount, exclusionCount > 0 ? string.Empty : " A full comparison is required!");

                    if (whereClause.Length > 0)
                    {
                        mapping.Include = true;

                        // don't add a where clauses if it literally would include the entire table
                        if (exclusionCount > 0)
                        {
                            var finalWhereClause = string.Format("EXISTS(SELECT * FROM {0} x WITH(NOLOCK) {1} WHERE {2} AND ({3}))",
                                                                 synchronizer.TableName,
                                                                 checksumComparisonInfo.Join,
                                                                 string.Join(" AND ", keyColumns.Select(k => string.Format("x.{0} = {1}.{0}", k, synchronizer.TableName))),
                                                                 whereClause);

                            mapping.Where = new WhereClause(finalWhereClause);
                        }
                    }
                    else
                    {
                        var rowCountQuery = String.Format("SELECT COUNT(*) FROM {0} WITH(NOLOCK)", synchronizer.TableName);

                        var rowCountDatabase1Task = Task.Factory.StartNew(() =>
                        {
                            using (var connection = db1.CreateConnection())
                            {
                                var numberOfRows = connection.Execute<int>(rowCountQuery);
                                return numberOfRows;
                            }
                        });

                        var rowCountDatabase2Task = Task.Factory.StartNew(() =>
                        {
                            using (var connection = db2.CreateConnection())
                            {
                                var numberOfRows = connection.Execute<int>(rowCountQuery);
                                return numberOfRows;
                            }
                        });

                        if (rowCountDatabase1Task.Result <= 1000 && rowCountDatabase2Task.Result <= 1000)
                        {
                            // if less than 1000 rows, don`t rely on checksum (maybe inaccurate!)
                            Trace.TraceInformation("Table {0} to for {1}.{2} to {3}.{4} has less than 1000 rows, so will always do a full comparison.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);

                            mapping.Include = true;
                        }
                        else
                        {
                            Trace.TraceInformation("Skipping Table {0} on {1}.{2} to {3}.{4} because checksum determined there are no differences.", synchronizer.TableName, db1.ConnectionProperties.ServerName, db1.ConnectionProperties.DatabaseName, db2.ConnectionProperties.ServerName, db2.ConnectionProperties.DatabaseName);

                            mapping.Include = false;
                        }
                    }
                }
            }
        }

        private static void AddChecksumWhereClause(IEnumerable<DataRow> checksums1, List<DataRow> checksums2, StringBuilder whereClause, ChecksumComparisonInfo checksumComparisonInfo, ref int exclusionCount, ref int missingRangeCount, ref int differentChecksumCount)
        {
            // loop through checksums1
            // find items in checksums2 with matching min and max values
            //     if exists and checksum value is different, remove item from checksums2 and add to whereClauses...
            //     if doesn't exists, add to whereClauses...

            foreach (var c in checksums1)
            {
                var checksum = c;

                var matchingChecksumInOther =
                    checksums2.AsEnumerable()
                        .FirstOrDefault(r =>
                                        Equals(r["RangeMin"], checksum["RangeMin"]) && Equals(r["RangeMax"], checksum["RangeMax"]));

                if (matchingChecksumInOther == null || !Equals(matchingChecksumInOther["Checksum"], checksum["Checksum"]))
                {
                    if (matchingChecksumInOther == null)
                        missingRangeCount++;
                    else
                        differentChecksumCount++;

                    // whereClause syntax will be: WHERE CONVERT(nvarchar(max), {RangeColumn}) (first column in PK) >= RangeMin AND CONVERT(nvarchar(max), {RangeColumn}) <= RangeMax

                    var filter = string.Format("({0} BETWEEN '{1}' AND '{2}')",
                                               checksumComparisonInfo.RangeColumn,
                                               checksum["RangeMin"],
                                               checksum["RangeMax"]);

                    if (whereClause.Length > 0)
                    {
                        whereClause.Append(" OR ");
                    }
                    whereClause.Append(filter);
                }
                else
                {
                    exclusionCount++;
                }

                if (matchingChecksumInOther != null)
                {
                    checksums2.Remove(matchingChecksumInOther);
                }
            }
        }
    }
}