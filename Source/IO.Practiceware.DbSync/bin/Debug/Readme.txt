﻿
======================= Available Command Line Options ==========

*Notes: you may specify a comma separated list of key value pairs in the form Key=Value (e.g. DebugMode=true)


*example:  To run a twoway data comparison ==> "DbSync.exe Server1Name=MyServer, Db1Name=MyDatabaseName, SchemaSync=false, DataSync=true, RunScript=TwoWay"


Server1Name 
Server2Name
Db1Name
Db2Name
ProductionServer1Name
ProductionServer2Name
DistributionServer
SchemaSync
DataSync
AutoSelectOnly
WellDefinedRulesOnly
ExclusionSpecificationMode
DebugMode
RunScript (specify one of these values ==> NoSync, OneWay, TwoWay)
PerformDatabaseBackups
BackupDatabasesOnly
RunningInHubMode




---- Include Options -----

*Note: tables and types to include/exclude. Make sure to enclose the Value part of the KeyValuePair in single quotes

TablesToInclude='Table1,Table2,Table3'....

TablesToExclude='Table1,Table2'....

ObjectTypesToInclude=

*Specify one or more of the following

        None = 0,
        Table = 1,
        StoredProcedure = 2,
        View = 3,
        Default = 4,
        FullTextCatalog = 5,
        Function = 6,
        Role = 7,
        Rule = 8,
        User = 9,
        UserDefinedType = 10,
        Trigger = 11,
        DdlTrigger = 12,
        Assembly = 13,
        Synonym = 14,
        XmlSchemaCollection = 15,
        MessageType = 16,
        Contract = 17,
        Queue = 18,
        Service = 19,
        Route = 20,
        EventNotification = 21,
        PartitionScheme = 22,
        PartitionFunction = 23,
        Field = 24,
        Index = 25,
        Schema = 26,
        ServiceBinding = 27,
        Certificate = 28,
        SymmetricKey = 29,
        AsymmetricKey = 30,
        CheckConstraint = 31,
        FullTextStoplist = 32,
        ExtendedProperty = 33,
        Data = 34,

---- DbComparisonOptions Options ----

Notes: When running a schema comparison (i.e. SchemaSync=true), you may specify any number of 
potential flags to customize how the comparison is performed. To specify them, just add them as
a command line option in the form (e.g. DbComparisonOptions='Option1,Option2'). If you do not specify an option
it will not be used.

 
*example: DbSync.exe Server1Name=MyServer, Db1Name=MyDatabaseName, SchemaSync=true, DbComparisonOptions='ForceColumnOrder,IgnoreFillFactor,IgnoreCollations'

ForceColumnOrder  
IgnoreFillFactor  
IgnorePermissions  
IgnoreWhiteSpace  
IgnoreBindings  
IgnoreQuotedIdentifiersAndAnsiNullSettings  
CaseSensitiveObjectDefinition  
IgnoreExtendedProperties  
IgnoreFullTextIndexing  
NoSQLPlumbing  
IgnoreCollations  
IgnoreComments  
IgnoreIndexes  
IgnoreKeys  
IgnoreChecks  
IgnoreTriggers  
IncludeDependencies  
IgnoreInsteadOfTriggers  
IgnoreFileGroups  
IgnoreIdentitySeedAndIncrement  
IgnoreWithNocheck  
IgnoreConstraintNames  
IgnoreStatistics  
DoNotOutputCommentHeader  
AddWithEncryption  
IgnoreSynonymDependencies  
SeparateTriggers  
IgnoreOwners  
IgnoreQueueEventNotifications  
TargetIsPre2005  
UseClrUdtToStringForClrMigration  
ConsiderNextFilegroupInPartitionSchemes  
IgnoreCertificatesAndCryptoKeys  
IgnoreTriggerOrder  
IgnoreUsers  
IgnoreUserProperties  
DisableAndReenableDdlTriggers  
IgnoreWithElementOrder  
IgnoreIndexLockProperties  
IgnoreReplicationTriggers  
IgnoreIdentityProperties  
IgnoreNotForReplication  
TargetIsPre2008  
IgnoreDataCompression  
IgnoreDatabaseAndServerName  
AddDatabaseUseStatement  
IgnoreSchemaObjectAuthorization  
DecryptPost2kEncryptedObjects  
ForceSyncScriptGeneration  
IgnoreStatisticsNorecompute  
IgnoreSquareBrackets  
ThrowOnFileParseFailed  
EnableDeployNow  
TargetIsSqlAzure  
ObjectExistenceChecks  
DropAndCreateInsteadOfAlter  
IgnoreMigrationScripts  
IgnoreTSQLT  
IgnoreSystemNamedConstraintNames  
IgnoreInternallyUsedMicrosoftExtendedProperties  
TargetIsPre2012  
InlineTableObjects  
WriteAssembliesAsDlls  
InlineFulltextFields  
ReadAsDataToolsPermissions  
WriteAsDataToolsPermissions  
InferTypeForSqlVariant  
UseSetStatementsInScriptDatabaseInfo  
ForbidDuplicateTableStorageSettings  
IgnoreWithEncryption  
CloneDatabaseLevelPropertiesForMigrations  
UseMigrationsV2  
DisableSocForLiveDbs 