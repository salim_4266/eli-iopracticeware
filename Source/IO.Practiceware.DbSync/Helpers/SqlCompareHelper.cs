using System.Collections.Concurrent;
using System.Reflection;
using System.Runtime.Serialization;
using IO.Practiceware.DbSync.Extensions;
using RedGate.Migrations.Core.Database;
using RedGate.SQLDataCompare.Engine;
using RedGate.SQLDataCompare.Engine.ResultsStore;
using Database = RedGate.SQLCompare.Engine.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace IO.Practiceware.DbSync
{
    internal static class SqlCompareHelper
    {
        private static readonly Dictionary<string, Tuple<int, int>> IdentityRanges =
            new Dictionary<string, Tuple<int, int>>();

        private static readonly ConcurrentDictionary<ExecuteSqlCacheKey, object> ExecuteSqlCache = new ConcurrentDictionary<ExecuteSqlCacheKey, object>();

        internal static ComparisonSession CreateComparisonSession()
        {
            var comparisonSession = (ComparisonSession)FormatterServices.GetUninitializedObject(typeof(ComparisonSession));

            foreach (string name in new[] { "#c", "#i" })
            {
                FieldInfo field = typeof(ComparisonSession).GetField(name,
                    BindingFlags.Instance | BindingFlags.NonPublic);

                if (field == null)
                    throw new InvalidOperationException();

                field.SetValue(comparisonSession, Activator.CreateInstance(field.FieldType, true));
            }

            return comparisonSession;
        }



        internal static Database CreateDatabase()
        {
            var database = (Database)FormatterServices.GetUninitializedObject(typeof(Database));

            foreach (var f in database.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                if (f.FieldType.IsValueType)
                {
                    f.SetValue(database, Activator.CreateInstance(f.FieldType));
                }
            }

            var ahud = FormatterServices.GetUninitializedObject(typeof(Database).Assembly.GetTypes().First(t => t.Name == "#AHud"));

            foreach (var f in ahud.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                if (f.FieldType.IsValueType)
                {
                    f.SetValue(ahud, Activator.CreateInstance(f.FieldType));
                }
            }

            var factory = new LocalDbFactory<Database>();

            // ReSharper disable PossibleNullReferenceException

            ahud.GetType().GetField("#n", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(ahud, factory);
            ahud.GetType().GetProperty("EncryptionAlgorithm", BindingFlags.Instance | BindingFlags.Public).SetValue(ahud, RedGate.SQLCompare.Engine.DatabaseEncryption.None, null);
            ahud.GetType().GetField("#i", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(ahud, Guid.Empty);
            ahud.GetType().GetField("#l", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(ahud, new RedGate.SQLCompare.Engine.SourceControlInfo(null as string, null, null));
            ahud.GetType().GetField("#p", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(ahud, new RedGate.Shared.Utils.IO.TemporaryFileManager());
            ahud.GetType().GetProperty("DatabaseOld", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(ahud, database, null);


            InitializeObjectCollections(ahud);

            typeof(Database).GetField("#a", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(database, ahud);

            // ReSharper restore PossibleNullReferenceException

            return database;
        }

        private static void InitializeObjectCollections(object ahud)
        {

            // ReSharper disable ConditionIsAlwaysTrueOrFalse            
            // ReSharper disable InconsistentNaming
            // ReSharper disable SuggestUseVarKeywordEvident
            // ReSharper disable ConvertToConstant.Local

            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func2 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func3 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func4 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func5 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func6 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func7 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func8 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func9 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func10 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func11 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func12 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func13 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func14 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func15 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func16 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func17 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func18 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func19 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func20 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func21 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func22 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func23 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func24 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func25 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func26 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func27 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func28 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func29 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func30 = null;
            Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection> func31 = null;
            SetField("m_DependenciesBuilder", ahud, CreateDependenciesBuilder());
            SetField("m_AssemblyDependenciesBuilder", ahud, CreateDependenciesBuilder());

            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> dictionary = new Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>>();
            dictionary.Add(RedGate.SQLCompare.Engine.ObjectType.Table, () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)GetField("m_Tables", ahud));
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_AB_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_AB_1 = RedGate.SQLCompare.Engine.ObjectType.Function;
            if (func == null)
            {
                func = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#njd", ahud);
            }
            arg_AB_0.Add(arg_AB_1, func);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_C3_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_C3_1 = RedGate.SQLCompare.Engine.ObjectType.Default;

            if (func2 == null)
            {
                func2 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Nsj", ahud);
            }
            arg_C3_0.Add(arg_C3_1, func2);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_DE_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_DE_1 = RedGate.SQLCompare.Engine.ObjectType.Rule;
            if (func3 == null)
            {
                func3 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Osj", ahud);
            }
            arg_DE_0.Add(arg_DE_1, func3);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_FA_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_FA_1 = RedGate.SQLCompare.Engine.ObjectType.User;
            if (func4 == null)
            {
                func4 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Pgq", ahud);
            }
            arg_FA_0.Add(arg_FA_1, func4);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_115_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_115_1 = RedGate.SQLCompare.Engine.ObjectType.Role;

            if (func5 == null)
            {
                func5 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#sHv", ahud);
            }
            arg_115_0.Add(arg_115_1, func5);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_131_0 = dictionary;

            RedGate.SQLCompare.Engine.ObjectType arg_131_1 = RedGate.SQLCompare.Engine.ObjectType.UserDefinedType;
            if (func6 == null)
            {
                func6 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#tHv", ahud);
            }
            arg_131_0.Add(arg_131_1, func6);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_14C_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_14C_1 = RedGate.SQLCompare.Engine.ObjectType.View;
            if (func7 == null)
            {
                func7 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#uHv", ahud);
            }
            arg_14C_0.Add(arg_14C_1, func7);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_167_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_167_1 = RedGate.SQLCompare.Engine.ObjectType.StoredProcedure;
            if (func8 == null)
            {
                func8 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Snw", ahud);
            }
            arg_167_0.Add(arg_167_1, func8);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_183_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_183_1 = RedGate.SQLCompare.Engine.ObjectType.Trigger;
            if (func9 == null)
            {
                func9 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Tnw", ahud);
            }
            arg_183_0.Add(arg_183_1, func9);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_19F_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_19F_1 = RedGate.SQLCompare.Engine.ObjectType.DdlTrigger;
            if (func10 == null)
            {
                func10 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Onw", ahud);
            }
            arg_19F_0.Add(arg_19F_1, func10);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_1BB_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_1BB_1 = RedGate.SQLCompare.Engine.ObjectType.Schema;
            if (func11 == null)
            {
                func11 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Pnw", ahud);
            }
            arg_1BB_0.Add(arg_1BB_1, func11);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_1D7_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_1D7_1 = RedGate.SQLCompare.Engine.ObjectType.Assembly;
            if (func12 == null)
            {
                func12 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Qnw", ahud);
            }
            arg_1D7_0.Add(arg_1D7_1, func12);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_1F3_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_1F3_1 = RedGate.SQLCompare.Engine.ObjectType.MessageType;
            if (func13 == null)
            {
                func13 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#vHv", ahud);
            }
            arg_1F3_0.Add(arg_1F3_1, func13);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_20F_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_20F_1 = RedGate.SQLCompare.Engine.ObjectType.Synonym;
            if (func14 == null)
            {
                func14 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#wHv", ahud);
            }
            arg_20F_0.Add(arg_20F_1, func14);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_22B_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_22B_1 = RedGate.SQLCompare.Engine.ObjectType.Contract;
            if (func15 == null)
            {
                func15 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#xHv", ahud);
            }
            arg_22B_0.Add(arg_22B_1, func15);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_247_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_247_1 = RedGate.SQLCompare.Engine.ObjectType.Queue;
            if (func16 == null)
            {
                func16 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#yHv", ahud);
            }
            arg_247_0.Add(arg_247_1, func16);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_263_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_263_1 = RedGate.SQLCompare.Engine.ObjectType.XmlSchemaCollection;
            if (func17 == null)
            {
                func17 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#zHv", ahud);
            }
            arg_263_0.Add(arg_263_1, func17);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_27F_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_27F_1 = RedGate.SQLCompare.Engine.ObjectType.Service;
            if (func18 == null)
            {
                func18 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Rnw", ahud);
            }
            arg_27F_0.Add(arg_27F_1, func18);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_29B_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_29B_1 = RedGate.SQLCompare.Engine.ObjectType.Route;
            if (func19 == null)
            {
                func19 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#f5y", ahud);
            }
            arg_29B_0.Add(arg_29B_1, func19);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_2B7_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_2B7_1 = RedGate.SQLCompare.Engine.ObjectType.EventNotification;
            if (func20 == null)
            {
                func20 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#g5y", ahud);
            }
            arg_2B7_0.Add(arg_2B7_1, func20);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_2D3_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_2D3_1 = RedGate.SQLCompare.Engine.ObjectType.PartitionFunction;
            if (func21 == null)
            {
                func21 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#h5y", ahud);
            }
            arg_2D3_0.Add(arg_2D3_1, func21);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_2EF_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_2EF_1 = RedGate.SQLCompare.Engine.ObjectType.PartitionScheme;
            if (func22 == null)
            {
                func22 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#i5y", ahud);
            }
            arg_2EF_0.Add(arg_2EF_1, func22);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_30B_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_30B_1 = RedGate.SQLCompare.Engine.ObjectType.ServiceBinding;
            if (func23 == null)
            {
                func23 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#j5y", ahud);
            }
            arg_30B_0.Add(arg_30B_1, func23);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_327_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_327_1 = RedGate.SQLCompare.Engine.ObjectType.Certificate;
            if (func24 == null)
            {
                func24 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#k5y", ahud);
            }
            arg_327_0.Add(arg_327_1, func24);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_343_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_343_1 = RedGate.SQLCompare.Engine.ObjectType.SymmetricKey;
            if (func25 == null)
            {
                func25 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#6Bf", ahud);
            }
            arg_343_0.Add(arg_343_1, func25);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_35F_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_35F_1 = RedGate.SQLCompare.Engine.ObjectType.AsymmetricKey;
            if (func26 == null)
            {
                func26 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#l5y", ahud);
            }
            arg_35F_0.Add(arg_35F_1, func26);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_37A_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_37A_1 = RedGate.SQLCompare.Engine.ObjectType.FullTextCatalog;
            if (func27 == null)
            {
                func27 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#piQ", ahud);
            }
            arg_37A_0.Add(arg_37A_1, func27);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_396_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_396_1 = RedGate.SQLCompare.Engine.ObjectType.FullTextStoplist;
            if (func28 == null)
            {
                func28 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#qiQ", ahud);
            }
            arg_396_0.Add(arg_396_1, func28);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_3B2_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_3B2_1 = RedGate.SQLCompare.Engine.ObjectType.ExtendedProperty;
            if (func29 == null)
            {
                func29 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#riQ", ahud);
            }
            arg_3B2_0.Add(arg_3B2_1, func29);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_3CE_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_3CE_1 = RedGate.SQLCompare.Engine.ObjectType.Sequence;
            if (func30 == null)
            {
                func30 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#siQ", ahud);
            }
            arg_3CE_0.Add(arg_3CE_1, func30);
            Dictionary<RedGate.SQLCompare.Engine.ObjectType, Func<RedGate.SQLCompare.Engine.INonReadOnlyCollection>> arg_3EA_0 = dictionary;
            RedGate.SQLCompare.Engine.ObjectType arg_3EA_1 = RedGate.SQLCompare.Engine.ObjectType.SearchPropertyList;
            if (func31 == null)
            {
                func31 = () => (RedGate.SQLCompare.Engine.INonReadOnlyCollection)InvokeMethodOnBase("#Dkx", ahud);
            }
            arg_3EA_0.Add(arg_3EA_1, func31);
            SetField("#a", ahud, dictionary);

            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper restore InconsistentNaming
            // ReSharper restore SuggestUseVarKeywordEvident
            // ReSharper restore ConvertToConstant.Local


        }

        private static object InvokeMethodOnBase(string name, object o)
        {
            var baseType = o.GetType().BaseType;
            if (baseType != null) return baseType.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Invoke(o, new object[0]);
            return null;
        }

        private static void SetField(string name, object instance, object value)
        {
            // ReSharper disable once PossibleNullReferenceException
            var t = instance.GetType();
            while (t != null && t != typeof(object))
            {
                if (SetField(name, t, instance, value))
                {
                    return;
                }
                t = t.BaseType;
            }
        }

        private static bool SetField(string name, Type t, object instance, object value)
        {
            // ReSharper disable once PossibleNullReferenceException
            var f = t.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (f != null)
            {
                f.SetValue(instance, value);
                return true;
            }
            return false;
        }

        private static object GetField(string name, object instance)
        {
            var t = instance.GetType();
            while (t != null && t != typeof(object))
            {
                object value;
                if (GetField(name, t, instance, out value))
                {
                    return value;
                }
                t = t.BaseType;
            }
            return null;
        }

        private static bool GetField(string name, Type t, object instance, out object value)
        {
            // ReSharper disable once PossibleNullReferenceException
            var f = t.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            if (f != null)
            {
                value = f.GetValue(instance);
                return true;
            }
            value = null;
            return false;
        }

        private static object CreateDependenciesBuilder()
        {
            return typeof(RedGate.SQLCompare.Engine.DependenciesBuilder).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, Type.EmptyTypes, null).Invoke(new object[0]);
        }

        public static string ReadAllText(string path)
        {
            using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var streamReader = new StreamReader(fileStream))
                    return streamReader.ReadToEnd();
            }
        }

        public static string StripBrackets(this string s)
        {
            return s.Replace("[", String.Empty).Replace("]", String.Empty);
        }

        public static void Execute(this RedGate.SQLCompare.Engine.ConnectionProperties connectionProperties, string sql)
        {
            var cs = new SqlConnectionStringBuilder
                     {
                         DataSource = connectionProperties.ServerName,
                         InitialCatalog = connectionProperties.DatabaseName,
                         IntegratedSecurity = true
                     };

            using (var connection = new SqlConnection(cs.ToString()))
            {
                connection.Execute(sql);
            }
        }

        public static bool IsMainServer(this RedGate.SQLCompare.Engine.IDatabase db, ComparisonSession session)
        {
            return GetIdentityRange(db, session, "[dbo].[PracticeName]").Item1 == 0;
        }

        public static bool DoesDatabaseAppointmentHaveAnAppointmentTrackInDatabaseIdentityRange(this RedGate.SQLCompare.Engine.IDatabase database, ComparisonSession session, Row row,
            Store store)
        {
            Tuple<int, int> tuple = row.GetFieldValueInResults<int>("AppointmentId", store);

            int appointmentId = tuple.Item1;

            Tuple<int, int> appointmentTrackRange = GetIdentityRange(database, session, "AppointmentTrack");

            var result = CreateConnection(database).ExecuteAndDispose<int?>(
                @"
SELECT TOP 1 CASE
WHEN q.ApptTrackId between " + appointmentTrackRange.Item1 + " and " +
                appointmentTrackRange.Item2 +
                @" THEN 1
ELSE 0
END
FROM
(SELECT * FROM AppointmentTrack aptr WITH(NOLOCK)
WHERE aptr.AppointmentId = " +
                appointmentId + @") q
ORDER BY q.ApptTrackDate DESC, q.ApptTrackTime DESC");

            if (result != null)
                return result.Value == 1;

            return IsMainServer(database, session);
        }

        /// <summary>
        /// Determines if the specified database has the best data for the specified row, based on audit entries.
        /// </summary>
        /// <param name="syncConnection"></param>
        /// <param name="tableName"></param>
        /// <param name="row"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        public static bool DatabaseHasBetterForRow(this DbSyncConnection syncConnection, string tableName, Row row, Store store)
        {
            var connection = syncConnection.Connection;

            const string keyColumnsQuery =
                @"
SELECT ccu.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS tc
INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE AS ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
INNER JOIN sys.objects o ON o.object_id = OBJECT_ID(@tableName) AND tc.TABLE_NAME = o.name AND tc.TABLE_SCHEMA = SCHEMA_NAME(o.schema_id)
WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY'";

            var keyColumns = connection.Execute<DataTable>(keyColumnsQuery, new Dictionary<string, object> { { "tableName", tableName } }).AsEnumerable().Select(r => r[0] as string).ToArray();

            var keyValues = keyColumns.Select(c => row.GetFieldValueInResults<object>(c, store)).Select(i => i.Item1 ?? i.Item2).ToArray();

            var queryToGetNewestAuditServerName = string.Format(
                @"SELECT TOP 1
ServerName
FROM AuditEntries ae
WHERE ObjectName = '{0}' AND {1} = {3}{2}{3}
ORDER BY AuditDateTime DESC", tableName.StripBrackets(),
                keyColumns.Length == 1 && (keyValues[0] is int || keyValues[0] is long) ? "KeyValueNumeric" : "KeyValues",
                string.Join(", ", keyValues),
                keyColumns.Length == 1 && (keyValues[0] is int || keyValues[0] is long) ? "" : "'");

            var bestServerName = connection.Execute<string>(queryToGetNewestAuditServerName);

            return String.Equals(bestServerName, syncConnection.ProductionServerName, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the db pairings and returns them as a tuple array. If two databases are not explicitly specified, this method will query the distribution server to get the pairings.
        /// If two databases are specified, it will return a single-element tuple array containing the pairing of the specified databases.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        public static Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[] GetDbPairings(DbSyncParameters parameters)
        {
            if (parameters.RunningInHubMode)
            {
                var distributionConnection = new SqlConnectionStringBuilder
                                             {
                                                 DataSource = parameters.DistributionServer,
                                                 InitialCatalog = "distribution",
                                                 IntegratedSecurity = true
                                             };

                using (
                    SqlConnection connection1 = new SqlConnection(distributionConnection.ConnectionString),
                        connection2 = new SqlConnection(distributionConnection.ConnectionString),
                        connection3 = new SqlConnection(distributionConnection.ConnectionString))
                {
                    var publisherAnalysisQuery = connection1.Execute<DataTable>(
                        @"SELECT DISTINCT data_source AS PublisherServerName
	,publisher_db AS PublisherDatabasename
FROM sys.servers legend
INNER JOIN MSsubscriptions mss ON mss.publisher_id = legend.server_id
WHERE legend.server_id IN (
		SELECT publisher_id
		FROM mssubscriptions
		)");

                    int publisherCount = publisherAnalysisQuery.Rows.Count;

                    bool replicationMode72 = publisherCount == 1;
                    // A publisher count of 1 indicates that the 7.2 replication model has been implemented.

                    SqlConnectionStringBuilder hubInstanceConnectionString;

                    var nodeServerDatabasePairings =
                        connection1.ExecuteAndDispose<DataTable>(
                            @"SELECT DISTINCT data_source AS SubscriberServerName
	,subscriber_db AS SubscriberDatabasename
FROM sys.servers legend
INNER JOIN MSsubscriptions mss ON mss.subscriber_id = legend.server_id
");

                    int serverPairingCount = replicationMode72
                        ? nodeServerDatabasePairings.Rows.Count
                        : nodeServerDatabasePairings.Rows.Count - 1;

                    if (replicationMode72)
                    {
                        hubInstanceConnectionString = new SqlConnectionStringBuilder
                                                      {
                                                          DataSource =
                                                              publisherAnalysisQuery.Rows[0]["PublisherServerName"].
                                                              ToString(),
                                                          InitialCatalog =
                                                              publisherAnalysisQuery.Rows[0]["PublisherDatabaseName"].
                                                              ToString(),
                                                          IntegratedSecurity = true
                                                      };
                    }
                    else
                    {
                        var hubResult = connection2.ExecuteAndDispose<DataTable>(
                            @"SELECT TOP 1 legendPub.NAME AS PublisherServerName, mss.publisher_db As PublisherDatabaseName
FROM MSsubscriptions mss
INNER JOIN sys.servers legendPub ON legendPub.server_id = mss.publisher_id
GROUP BY legendPub.NAME
	,publisher_db
ORDER BY COUNT(*) DESC");


                        hubInstanceConnectionString = new SqlConnectionStringBuilder
                                                      {
                                                          DataSource =
                                                              hubResult.Rows[0]["PublisherServerName"].ToString(),
                                                          InitialCatalog =
                                                              hubResult.Rows[0]["PublisherDatabaseName"].ToString(),
                                                          IntegratedSecurity = true
                                                      };
                    }


                    bool hasMoreThan2Nodes = serverPairingCount > 1;


                    var dbPairings = new Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[
                        serverPairingCount];

                    int i = 0;

                    if (hasMoreThan2Nodes)
                    {
                        foreach (DataRow row in nodeServerDatabasePairings.Rows)
                        {
                            var spokeInstanceConnectionString = new SqlConnectionStringBuilder
                                                                {
                                                                    DataSource = row["SubscriberServerName"].ToString(),
                                                                    InitialCatalog =
                                                                        row["SubscriberDatabaseName"].ToString(),
                                                                    IntegratedSecurity = true
                                                                };

                            if (spokeInstanceConnectionString.DataSource != hubInstanceConnectionString.DataSource ||
                                spokeInstanceConnectionString.InitialCatalog !=
                                hubInstanceConnectionString.InitialCatalog)
                            {
                                dbPairings[i] =
                                    Tuple.Create(new DbSyncConnectionProperties(hubInstanceConnectionString.ConnectionString, null), new DbSyncConnectionProperties(spokeInstanceConnectionString.ConnectionString, null));
                                i++;
                            }
                        }
                    }
                    else
                    {
                        var hubResult = connection3.Execute<DataTable>(
                            @"SELECT DISTINCT legendPub.NAME AS PublisherServerName
	,publisher_db As PublisherDatabaseName
FROM MSsubscriptions mss
INNER JOIN sys.servers legendPub ON legendPub.server_id = mss.publisher_id
INNER JOIN sys.servers legendSub ON legendSub.server_id = mss.subscriber_id");

                        var hubConnectionString = new SqlConnectionStringBuilder
                                                  {
                                                      DataSource = hubResult.Rows[0]["PublisherServerName"].ToString()
                                                      ,
                                                      InitialCatalog = hubResult.Rows[0]["PublisherDatabaseName"].ToString()
                                                      ,
                                                      IntegratedSecurity = true
                                                  };

                        var spokeResult =
                            connection3.ExecuteAndDispose<DataTable>(
                                @"SELECT DISTINCT data_source AS SubscriberServerName
	,subscriber_db AS SubscriberDatabasename
FROM sys.servers legend
INNER JOIN MSsubscriptions mss ON mss.subscriber_id = legend.server_id
");


                        var spokeConnectionString = new SqlConnectionStringBuilder
                                                    {
                                                        DataSource = spokeResult.Rows[0]["SubscriberServerName"].ToString()
                                                        ,
                                                        InitialCatalog = spokeResult.Rows[0]["SubscriberDatabaseName"].ToString()
                                                        ,
                                                        IntegratedSecurity = true
                                                    };

                        dbPairings = new Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[1];

                        dbPairings[0] = Tuple.Create(
                            new DbSyncConnectionProperties(hubConnectionString.ConnectionString, null),
                            new DbSyncConnectionProperties(spokeConnectionString.ConnectionString, null));
                    }


                    return dbPairings;
                }
            }

            if (!String.IsNullOrEmpty(parameters.Server1Name) && !String.IsNullOrEmpty(parameters.Server2Name) && !String.IsNullOrEmpty(parameters.Db1Name) && !String.IsNullOrEmpty(parameters.Db2Name))
            {
                var dbPairings = new Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[1];

                var db1ConnectionString = new SqlConnectionStringBuilder
                                          {
                                              DataSource = parameters.Server1Name
                                              ,
                                              InitialCatalog = parameters.Db1Name
                                              ,
                                              IntegratedSecurity = true
                                          };

                var db2ConnectionString = new SqlConnectionStringBuilder
                                          {
                                              DataSource = parameters.Server2Name,
                                              InitialCatalog = parameters.Db2Name,
                                              IntegratedSecurity = true
                                          };

                dbPairings[0] = Tuple.Create(new DbSyncConnectionProperties(db1ConnectionString.ConnectionString, parameters.ProductionServer1Name), new DbSyncConnectionProperties(db2ConnectionString.ConnectionString, parameters.ProductionServer2Name));

                return dbPairings;
            }

            throw new ArgumentException("One or more connection string elements were not supplied.");
        }


        public static bool IsLocationOfLastDischargedAppointment(this RedGate.SQLCompare.Engine.IDatabase database, ComparisonSession session,
            Row row, Store store)
        {
            Tuple<int, int> tuple = row.GetFieldValueInResults<int>("PatientId", store);

            int patientId = session.Database2.Equals(database) ? tuple.Item1 : tuple.Item2;

            var result = CreateConnection(database).ExecuteAndDispose<int?>(
                @"
SELECT TOP 1 CASE
WHEN q.ResourceId2 = 0
THEN 1
ELSE 0
END
FROM
(SELECT * FROM Appointments appts WITH(NOLOCK)
WHERE appts.PatientId = " +
                patientId +
                @" AND appts.ScheduleStatus = 'D'
AND appts.AppTime > 0
AND (appts.ResourceId2 = 0
OR appts.ResourceId2 >= 1000)) q
ORDER BY q.AppDate DESC, q.AppTime DESC");

            if (result != null)
            {
                if (result.Value == 1)
                    return IsMainServer(database, session);

                if (result.Value == 0)
                    return !(IsMainServer(database, session));
            }


            return IsMainServer(database, session);
        }

        public static Tuple<int, int> GetIdentityRange(this RedGate.SQLCompare.Engine.IDatabase database, ComparisonSession session,
            string tableName)
        {
            string compositeKey = database.ConnectionProperties.ConnectionString + "_" + tableName;
            Tuple<int, int> range;
            if (IdentityRanges.TryGetValue(compositeKey, out range))
                return range;

            var identities = new Dictionary<Database, int>();
            foreach (Database sessionDatabase in new[] { session.Database1, session.Database2 })
            {
                identities[sessionDatabase] =
                    CreateConnection(sessionDatabase).ExecuteAndDispose<int>("select ident_current('" + tableName +
                                                                              "')");
            }

            foreach (var identity in identities)
            {
                int max = identity.Value;
                int min =
                    identities.Where(i => i.Value < identity.Value)
                        .Select(i => i.Value)
                        .DefaultIfEmpty(0)
                        .Max();

                IdentityRanges[identity.Key.ConnectionProperties.ConnectionString + "_" + tableName] =
                    new Tuple<int, int>(min, max);
            }

            return IdentityRanges[compositeKey];
        }


        public static object GetFieldValue(Row row, string fieldName, Store store, bool useResults1)
        {
            if (store.Fields[fieldName].Field1.Computed || store.Fields[fieldName].Field2.Computed) return null;

            return
                row.Values[
                    useResults1 ? store.Fields[fieldName].OrdinalInResults1 : store.Fields[fieldName].OrdinalInResults2];
        }

        public static object GetFieldValueInResults1(this Row row, string fieldName, Store store)
        {
            return GetFieldValue(row, fieldName, store, true);
        }

        public static object GetFieldValueInResults2(this Row row, string fieldName, Store store)
        {
            return GetFieldValue(row, fieldName, store, false);
        }

        public static Tuple<T, T> Reverse<T>(this Tuple<T, T> couple)
        {
            return new Tuple<T, T>(couple.Item2, couple.Item1);
        }

        public static Tuple<T, T> GetFieldValueInResults<T>(this Row row, string fieldName, Store store)
        {
            return new Tuple<T, T>(GetFieldValueInResults1<T>(row, fieldName, store),
                GetFieldValueInResults2<T>(row, fieldName, store));
        }

        public static Tuple<T, T> CloneWithDefault<T>(this Tuple<T, T> tuple, T defaultValue) where T : class
        {
            T value1 = tuple.Item1;
            T value2 = tuple.Item2;

            if (typeof(T) == typeof(string))
            {
                if (tuple.Item1 == null)
                {
                    value1 = defaultValue;
                }

                if (tuple.Item2 == null)
                {
                    value2 = defaultValue;
                }
            }

            return new Tuple<T, T>(value1, value2);
        }


        public static T GetFieldValueInResults1<T>(this Row row, string fieldName, Store store)
        {
            var type = typeof(T);
            type = Nullable.GetUnderlyingType(type) ?? type;

            object value = GetFieldValue(row, fieldName, store, true);
            if ((value == null || value == DBNull.Value || value is RedGate.SQLDataCompare.Engine.ResultsStore.Missing) ||
                (value is string && value.ToString() == String.Empty && type != typeof(string)))
                return default(T);
            if (!(value is T))
            {
                try
                {
                    value = Convert.ChangeType(value, type);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(String.Format("Could not convert value of type {0} to {1}.",
                        value == null ? "null" : value.GetType().Name,
                        type.Name), ex);
                }
            }
            return (T)value;
        }

        public static T GetFieldValueInResults2<T>(this Row row, string fieldName, Store store)
        {
            var type = typeof(T);
            type = Nullable.GetUnderlyingType(type) ?? type;

            object value = GetFieldValue(row, fieldName, store, false);
            if ((value == null || value == DBNull.Value || value is RedGate.SQLDataCompare.Engine.ResultsStore.Missing) ||
                (value is string && value.ToString() == String.Empty && type != typeof(string)))
                return default(T);
            if (!(value is T))
            {
                try
                {
                    value = Convert.ChangeType(value, type);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(String.Format("Could not convert value of type {0} to {1}.",
                        value == null ? "null" : value.GetType().Name,
                        type.Name), ex);
                }
            }
            return (T)value;
        }

        public static bool AreItemsEqual<T>(this Tuple<T, T> tuple)
        {
            return Equals(tuple.Item1, tuple.Item2);
        }

        public static IEnumerable<string> GetDifferentFields(this Row row, Store store)
        {
            for (int i = 0; i < store.Fields.Count; i++)
            {
                if (row.FieldDifferent(i))
                    yield return store.Fields[i].EscapedFieldName(false);
            }
        }

        public static bool HasOnlyReplicationFieldDifferent(this Row row, Store store)
        {
            return row.GetDifferentFields(store).SequenceEqual(new[] { "[msrepl_tran_version]" });
        }

        public static SqlConnection CreateConnection(this RedGate.SQLCompare.Engine.IDatabase database)
        {
            return new SqlConnection(database.ConnectionProperties.ConnectionString);
        }

        public static void RunScript(this IDbConnection connection, string sql, IDictionary<string, object> parameters = null)
        {
            foreach (var s in sql.Split(new[] { "GO" + Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            {
                connection.Execute(s, parameters);
            }
        }

        public static void Execute(this IDbConnection connection, string sql,
            IDictionary<string, object> parameters = null)
        {
            try
            {
                RetryExtensions.ExecuteWithRetry(() =>
                                              {
                                                  if (connection.State != ConnectionState.Open)
                                                      connection.Open();

                                                  using (IDbCommand command = connection.CreateCommand())
                                                  {
                                                      command.CommandText = sql;

                                                      if (parameters != null)
                                                      {
                                                          foreach (var p in parameters)
                                                          {
                                                              IDbDataParameter dbParameter = command.CreateParameter();
                                                              dbParameter.ParameterName = p.Key;
                                                              dbParameter.Value = p.Value;
                                                              command.Parameters.Add(dbParameter);
                                                          }
                                                      }

                                                      command.CommandTimeout = 6000;

                                                      command.ExecuteNonQuery();
                                                  }
                                              });
            }
            catch (Exception ex)
            {
                throw new ApplicationException(String.Format("Error executing command {0} against server {1}.", sql, new SqlConnectionStringBuilder(connection.ConnectionString).DataSource), ex);
            }
        }

        public static T Execute<T>(this IDbConnection connection, string sql,
            IDictionary<string, object> parameters = null, bool useCache = true)
        {
            object cachedResult;
            var cacheKey = new ExecuteSqlCacheKey(sql, connection.ConnectionString, parameters);
            if (useCache && ExecuteSqlCache.TryGetValue(cacheKey, out cachedResult))
            {
                return cachedResult == null ? default(T) : (T)cachedResult;
            }

            object result = null;
            try
            {
                RetryExtensions.ExecuteWithRetry(() =>
                                              {
                                                  if (connection.State != ConnectionState.Open)
                                                      connection.Open();


                                                  using (IDbCommand command = connection.CreateCommand())
                                                  {
                                                      command.CommandText = sql;
                                                      command.CommandTimeout = 600;

                                                      if (parameters != null)
                                                      {
                                                          foreach (var p in parameters)
                                                          {
                                                              IDbDataParameter dbParameter = command.CreateParameter();
                                                              dbParameter.ParameterName = p.Key;
                                                              dbParameter.Value = p.Value;
                                                              command.Parameters.Add(dbParameter);
                                                          }
                                                      }

                                                      if (typeof(T) == typeof(DataTable))
                                                      {
                                                          using (IDataReader reader = command.ExecuteReader())
                                                          {
                                                              var dt = new DataTable();
                                                              dt.Load(reader);
                                                              result = (T)(object)dt;
                                                          }
                                                      }
                                                      else
                                                      {
                                                          result = command.ExecuteScalar();
                                                      }

                                                      if (result == DBNull.Value || result == null)
                                                      {
                                                          result = default(T);
                                                      }

                                                      if (result != null && !(result is T))
                                                      {
                                                          result = (T)Convert.ChangeType(result, typeof(T));
                                                      }
                                                  }
                                              });
            }
            catch (Exception ex)
            {
                throw new ApplicationException(String.Format("Error executing command \"{0}\" on server {1}.", sql, new SqlConnectionStringBuilder(connection.ConnectionString).DataSource), ex);
            }

            ExecuteSqlCache[cacheKey] = result;

            return result == null ? default(T) : (T)result;
        }

        public static string ToTraceString(this IDbCommand command)
        {
            string text = command.CommandText;

            foreach (IDataParameter parameter in command.Parameters)
            {
                string parameterValue = parameter.Value.ToString();

                if (
                    new[] { typeof(string), typeof(DateTime), typeof(Guid), typeof(SqlGuid) }.Contains(
                        parameter.Value.GetType()))
                {
                    parameterValue = "'" + parameterValue + "'";
                }
                else if (parameter.Value == DBNull.Value)
                {
                    parameterValue = "NULL";
                }
                else if (parameter.Value is bool)
                {
                    parameterValue = (bool)parameter.Value ? "1" : "0";
                }


                text = text.Replace("@" + parameter.ParameterName, parameterValue);
            }

            return text;
        }


        public static T ExecuteAndDispose<T>(this IDbConnection connection, string sql,
            IDictionary<string, object> parameters = null)
        {
            using (connection)
                return Execute<T>(connection, sql, parameters);
        }

        /// <summary>
        /// Batches the source sequence into sized buckets.
        /// </summary>
        /// <typeparam name="TSource">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <returns>A sequence of equally sized buckets containing elements of the source collection.</returns>
        /// <remarks> This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
        {
            return Batch(source, size, x => x);
        }


        /// <summary>
        /// Batches the source sequence into sized buckets and applies a projection to each bucket.
        /// </summary>
        /// <typeparam name="TSource">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <typeparam name="TResult">Type of result returned by <paramref name="resultSelector"/>.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <param name="resultSelector">The projection to apply to each bucket.</param>
        /// <returns>A sequence of projections on equally sized buckets containing elements of the source collection.</returns>
        /// <remarks> This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public static IEnumerable<TResult> Batch<TSource, TResult>(this IEnumerable<TSource> source, int size,
            Func<IEnumerable<TSource>, TResult> resultSelector)
        {
            return BatchImpl(source, size, resultSelector);
        }

        private static IEnumerable<TResult> BatchImpl<TSource, TResult>(this IEnumerable<TSource> source, int size,
            Func<IEnumerable<TSource>, TResult> resultSelector)
        {
            TSource[] bucket = null;
            int count = 0;

            foreach (TSource item in source)
            {
                if (bucket == null)
                {
                    bucket = new TSource[size];
                }

                bucket[count++] = item;

                // The bucket is fully buffered before it's yielded
                if (count != size)
                {
                    continue;
                }

                // Select is necessary so bucket contents are streamed too
                yield return resultSelector(bucket.Select(x => x));

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
            {
                yield return resultSelector(bucket.Take(count));
            }
        }

        public static string ReadLine(this StreamReader reader, bool onlyCrLf)
        {
            if (!onlyCrLf)
            {
                return reader.ReadLine();
            }

            var builder = new StringBuilder();
            bool encounteredCr = false;
            int current = reader.Read();
            while (current >= 0)
            {
                if (encounteredCr && current == 10)
                {
                    return builder.ToString();
                }

                if (encounteredCr)
                {
                    builder.Append((char)13);
                }

                encounteredCr = current == 13;

                if (!encounteredCr)
                {
                    builder.Append((char)current);
                }

                current = reader.Read();
            }

            return null;
        }

        /// <summary>
        /// This method obtains a list of distribution servers. It does so by
        /// enumerating the sql instances as a row in a datatable,
        /// formulating a connection string based on the row, connecting to the 
        /// database, and then checking to see if there is a distribution database
        /// on the instance.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListOfDistributionServers()
        {
            Console.WriteLine("Finding available replicating servers.");

            var instance = SqlDataSourceEnumerator.Instance;
            DataTable tableOfAllSqlInstances = instance.GetDataSources();
            var servers = new List<string>();

            var instanceNames = tableOfAllSqlInstances.AsEnumerable().Select(r => new { ServerName = r[0] as string, InstanceName = r[1] as string })
                .Select(i => String.Join("\\", new[] { i.ServerName, i.InstanceName })).ToArray();


            Console.WriteLine("Found the following SQL instances: {0}.", String.Join(", ", instanceNames));

            instanceNames
                .AsParallel().WithDegreeOfParallelism(10)
                .ForAll(n =>
                        {
                            var connectionString = String.Format("Data Source={0};Initial Catalog=master;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=5", n);

                            TryAddServer(connectionString, servers);
                        });

            return servers;
        }

        [DebuggerNonUserCode]
        private static void TryAddServer(string connectionString, List<string> servers)
        {
            const string query = "SELECT * FROM master.dbo.sysdatabases WHERE Name = 'distribution'";

            using (var conn = new SqlConnection { ConnectionString = connectionString })
            {
                try
                {
                    conn.Open();
                    using (var queryCommand = conn.CreateCommand())
                    {
                        queryCommand.CommandText = query;
                        var queryCommandReader = queryCommand.ExecuteReader();
                        var resultsOfSelectStatement = new DataTable();
                        resultsOfSelectStatement.Load(queryCommandReader);
                        if (resultsOfSelectStatement.Rows.Count > 0)
                        {
                            servers.Add(conn.DataSource);
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Could not connect to: " + conn.DataSource);
                }
            }
        }

        /// <summary>
        /// This method backs up all seed values from the database
        /// parameters passed to DBSync above. It connects to the 
        /// databases, gets the result of the sql query to a datatable,
        /// adds the datatable to a dataset, writes the 
        /// dataset to an xml file, and finally saves the xml file
        /// in the seeds directory.
        /// </summary>
        /// <param name="dbPairings"></param>
        internal static void BackupSeedValues(Tuple<DbSyncConnectionProperties, DbSyncConnectionProperties>[] dbPairings)
        {
            //make directories to dump output
            if (!(Directory.Exists("seeds")) && !(Directory.Exists(@"seeds\Archived")))
            {
                Directory.CreateDirectory(@"seeds");
                Directory.CreateDirectory(@"seeds\Archived");
            }
            if (File.Exists(@"seeds\BackedUpSeedValues.xml"))
            {
                var moveThisOldSeedFile = String.Format(@"seeds\BackedUpSeedValues.xml");
                var intoThisSeedArchivedFolder = String.Format(@"seeds\Archived\BackedUpSeedValues-{0:yyyy-MM-dd_hh-mm-ss-tt}.xml", DateTime.Now);
                File.Move(moveThisOldSeedFile, intoThisSeedArchivedFolder);
            }

            //create a dataset
            var movebackedUpSeedsDtToDs = new DataSet("BackupSeeds");

            var databaseSeedValuesToBackup = new List<string>();

            foreach (var pair in dbPairings)
            {
                if (!(databaseSeedValuesToBackup.Contains(pair.Item1.ConnectionString)))
                    databaseSeedValuesToBackup.Add(pair.Item1.ConnectionString);
                if (!(databaseSeedValuesToBackup.Contains(pair.Item2.ConnectionString)))
                    databaseSeedValuesToBackup.Add(pair.Item2.ConnectionString);
            }

            foreach (var connectionString in databaseSeedValuesToBackup)
            {
                using (var connection = new SqlConnection(connectionString))
                {

                    var addBackupSeedValueDTtoBs = BackupSeedValue(connection);
                    if (addBackupSeedValueDTtoBs != null)
                    {
                        addBackupSeedValueDTtoBs = FormatAddBackupSeedValueDt(connection, addBackupSeedValueDTtoBs);
                        movebackedUpSeedsDtToDs.Tables.Add(addBackupSeedValueDTtoBs);
                    }

                }
            }

            movebackedUpSeedsDtToDs.WriteXml(@"seeds\BackedUpSeedValues.xml");
        }

        /// <summary>
        /// This method executes the sql query to backup the
        /// seed values and stores the result to a datatable.
        /// If there is more than 1 row, it returns the
        /// datatable.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        internal static DataTable BackupSeedValue(SqlConnection connection)
        {
            var databaseToGetSeedsFrom = connection.Database;
            var seedValuesDt =
                connection.Execute<DataTable>(
                    @"USE [" + databaseToGetSeedsFrom + "]" +
                    @"
                    SELECT
                    IDENT_SEED(IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME) AS Seed,
                    IDENT_INCR(IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME) AS Increment,
                    CASE Counts.RowCnt
                    WHEN 0 THEN 0 ELSE IDENT_CURRENT(IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME) END AS CurrentIdentity,
                    IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME AS [Schema.TableName]
                    FROM
                    INFORMATION_SCHEMA.TABLES IST
                    JOIN
                    (
                    SELECT
                    sc.name +'.'+ ta.name TableName
                    ,SUM(pa.rows) RowCnt
                    FROM
                    sys.tables ta
                    INNER JOIN sys.partitions pa
                    ON pa.OBJECT_ID = ta.OBJECT_ID
                    INNER JOIN sys.schemas sc
                    ON ta.schema_id = sc.schema_id
                    WHERE
                    ta.is_ms_shipped = 0 AND pa.index_id IN (1,0)
                    GROUP BY
                    sc.name,ta.name
                    ) Counts ON Counts.TableName = IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME
                    WHERE
                    OBJECTPROPERTY(OBJECT_ID(IST.TABLE_SCHEMA + '.' + IST.TABLE_NAME), 'TableHasIdentity') = 1
                    AND IST.TABLE_TYPE = 'BASE TABLE'"
                    , null, false);
            return seedValuesDt != null ? (seedValuesDt.Rows.Count > 0 ? seedValuesDt : null) : null;
        }

        /// <summary>
        /// This method takes the datatable, adds columns to the
        /// datatable, and stores the name of the server,
        /// instance and database. This is done to allow 
        /// the datatset to be formatted correctly and easily
        /// identified.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="addBackupSeedValueDTtoBs"></param>
        /// <returns></returns>
        internal static DataTable FormatAddBackupSeedValueDt(SqlConnection connection, DataTable addBackupSeedValueDTtoBs)
        {
            if (connection.DataSource != null && connection.DataSource.Contains(@"\"))
            {
                var serverCol = addBackupSeedValueDTtoBs.Columns.Add("Server",
                    typeof(string));
                serverCol.Unique = false;
                var instanceCol = addBackupSeedValueDTtoBs.Columns.Add("Instance",
                    typeof(string));
                instanceCol.Unique = false;
                var databaseCol = addBackupSeedValueDTtoBs.Columns.Add("Database",
                    typeof(string));
                databaseCol.Unique = false;

                var splitDataSourceIntoDbAndInstance = connection.DataSource.Split('\\');
                serverCol.DefaultValue = splitDataSourceIntoDbAndInstance[0];
                instanceCol.DefaultValue = splitDataSourceIntoDbAndInstance[1];
                databaseCol.DefaultValue = connection.Database;
            }
            else
            {
                var serverCol = addBackupSeedValueDTtoBs.Columns.Add("Server",
                    typeof(string));
                serverCol.Unique = false;
                serverCol.DefaultValue = connection.DataSource;
                var databaseCol = addBackupSeedValueDTtoBs.Columns.Add("Database",
                    typeof(string));
                databaseCol.Unique = false;
                databaseCol.DefaultValue = connection.Database;
            }
            addBackupSeedValueDTtoBs.TableName = connection.DataSource + @"." + connection.Database;
            return addBackupSeedValueDTtoBs.Rows.Count > 0 ? addBackupSeedValueDTtoBs : null;
        }

        #region Nested type: ExecuteSqlCacheKey

        private class ExecuteSqlCacheKey
        {
            private readonly string _connectionString;
            private readonly IDictionary<string, object> _parameters;
            private readonly string _sql;

            public ExecuteSqlCacheKey(string sql, string connectionString, IDictionary<string, object> parameters)
            {
                _sql = sql;
                _connectionString = connectionString;
                _parameters = parameters ?? new Dictionary<string, object>();
            }

            public override bool Equals(object obj)
            {
                var other = obj as ExecuteSqlCacheKey;
                if (other == null)
                    return false;
                return other._sql == _sql && other._connectionString == _connectionString &&
                       other._parameters.SequenceEqual(_parameters);
            }

            public override int GetHashCode()
            {
                return _sql.GetHashCode() ^ _connectionString.GetHashCode();
            }
        }

        #endregion
    }
}