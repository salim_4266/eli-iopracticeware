using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace IO.Practiceware.DbSync
{
    public class EmailInfo
    {
        public string EmailSubject = String.Empty;
        public string EmailMessage = String.Empty;
        public string EmailAddress = String.Empty;
        private static string EmailAddressFrom = "sqlreplication@iopracticeware.com";

        // <summary>
        // Assigns Email subject and Body
        // </summary>        
        public void SendEmail()
        {
            var emailMailMessage = new MailMessage { Subject = EmailSubject + " (" + Environment.MachineName + ")", Body = EmailMessage };
            emailMailMessage.To.Add(EmailAddress);
            emailMailMessage.CC.Add("techservices@iopracticeware.com");
            emailMailMessage.From = new MailAddress(EmailAddressFrom);

            try
            {
                // hard coding all the mail settings in the place of a config file
                var smtpClient = new SmtpClient("smtp.gmail.com", 25)
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(EmailAddressFrom, "replication417"),
                    EnableSsl = true
                };

                smtpClient.Send(emailMailMessage);

                Trace.TraceInformation("Email send successfully to  {" + EmailAddress + "} ");
            }
            catch (Exception ex)
            {
                Trace.TraceError("Email send failed to  {" + EmailAddress + "} " + ex.Message);
            }
        }

    }
}