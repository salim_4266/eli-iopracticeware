namespace IO.Practiceware.DbSync.Synchronizers
{
    public class ChecksumComparisonInfo
    {
        public int? Divisor { get; set; }
        public string Join { get; set; }
        public string RangeColumn { get; set; }
    }
}