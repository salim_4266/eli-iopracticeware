﻿using RedGate.SQLDataCompare.Engine;
using RedGate.SQLDataCompare.Engine.ResultsStore;
using Database = RedGate.SQLCompare.Engine.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.DbSync.Synchronizers
{
    public interface ITableSynchronizer
    {
        string TableName { get; }

        string AuditedObjectName { get; }

        string[] DisallowedFieldsDifferent { get; }
        int Priority { get; }

        bool SelectRecord(ComparisonSession session, SynchronizationRecord synchronizationRecord, TableDifference tableDifference, Row getRow, DbSyncParameters options, DbSyncConnection connection1, DbSyncConnection connection2);

        void Initialize(Database db1, Database db2);

        bool ExcludeIfRowCountSame { get; }

        ChecksumComparisonInfo ChecksumComparisonInfo { get; }

        string ScriptPrefix { get; }

        string ScriptSuffix { get; }
    }

    public abstract class TableSynchronizer : ITableSynchronizer
    {
        public virtual bool AutoInsertWhenIn1
        {
            get { return true; }
        }

        public virtual bool NeverDelete
        {
            get { return false; }
        }

        #region ITableSynchronizer Members

        public abstract string TableName { get; }
        public virtual string AuditedObjectName { get; protected set; }
        public virtual string[] DisallowedFieldsDifferent
        {
            get { return new string[0]; }
        }
        public virtual int Priority
        {
            get
            {
                return 0;
            }
        }
        bool ITableSynchronizer.SelectRecord(ComparisonSession session, SynchronizationRecord record, TableDifference tableDifference, Row row, DbSyncParameters dbSyncSessionArguments, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            IEnumerable<string> fieldsDifferent = row.GetDifferentFields(tableDifference.ResultsStore);
            if (row.HasOnlyReplicationFieldDifferent(tableDifference.ResultsStore))
                return true;
            if (record.ResultsStoreType == Row.RowType.In1 && AutoInsertWhenIn1)
                return true;
            if (record.ResultsStoreType == Row.RowType.In2 && NeverDelete)
                return false;
            if (dbSyncSessionArguments.AutoSelectOnly)
                return false;
            if (record.ResultsStoreType == Row.RowType.Different &&
                DisallowedFieldsDifferent.Intersect(fieldsDifferent).Any() && dbSyncSessionArguments.WellDefinedRulesOnly)
                return false;
            if (record.ResultsStoreType == Row.RowType.Different || !AutoInsertWhenIn1)
                return SelectRecord(session, record, tableDifference, row, dbSyncSessionArguments, connection1, connection2);

            return false;
        }
        public virtual void Initialize(Database db1, Database db2)
        {

        }
        public virtual bool ExcludeIfRowCountSame { get { return false; } }
        public virtual ChecksumComparisonInfo ChecksumComparisonInfo
        {
            get { return new ChecksumComparisonInfo(); }
        }
        public virtual string ScriptPrefix { get; private set; }
        public virtual string ScriptSuffix { get; private set; }

        #endregion

        protected virtual bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                            TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            return true;
        }

        protected virtual bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                    TableDifference tableDifference, Row row, DbSyncParameters options, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            return SelectRecord(session, record, tableDifference, row, options);
        }
    }

    public abstract class WellDefinedRulesTableSynchronizer : TableSynchronizer
    {
        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                           TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }

    }

    public class GenericTableSynchronizer : TableSynchronizer
    {
        private readonly string _tableName;
        private readonly int _priority;

        public GenericTableSynchronizer(string tableName, int priority)
        {
            _tableName = tableName;
            _priority = priority;
        }

        public override string TableName
        {
            get { return _tableName; }
        }

        public override int Priority
        {
            get { return _priority; }
        }

        public override bool NeverDelete
        {
            get { return true; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record, TableDifference tableDifference, Row row, DbSyncParameters options, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            return connection1.DatabaseHasBetterForRow(AuditedObjectName ?? TableName, row, tableDifference.ResultsStore);
        }
    }

    public class PatientClinicalTableSynchronizer : TableSynchronizer
    {
        private const string HasRowsForClinicalTypeSql = @"
SELECT CASE
	WHEN COUNT(*) = 0
		THEN 0
	ELSE 1
	END
--SELECT *
FROM PatientClinical WITH(NOLOCK)
WHERE AppointmentId = @AppointmentId
AND COALESCE(ClinicalType, '') = COALESCE(@ClinicalType, '')
                ";

        private const string DatabaseHasBetterByAuditEntrySql = @"SELECT TOP 1 CASE 
		WHEN ServerName = '{0}'
			THEN 1
		ELSE 0
		END
FROM AuditEntries ae WITH(NOLOCK)
INNER JOIN PatientClinical pc WITH(NOLOCK) ON pc.ClinicalId = ae.KeyValueNumeric
WHERE ObjectName = 'dbo.PatientClinical'
	AND pc.AppointmentId = @AppointmentId
ORDER BY AuditDateTime DESC";


        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[PatientId]", "[AppointmentId]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientClinical]"; }
        }

        public override bool AutoInsertWhenIn1
        {
            get { return false; }
        }

        public override int Priority
        {
            get
            {
                return 2;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options, DbSyncConnection connection1, DbSyncConnection connection2)
        {


            long patientid = record.ResultsStoreType == Row.RowType.In1
                                 ? row.GetFieldValueInResults1<long>("PatientId", tableDifference.ResultsStore)
                                 : row.GetFieldValueInResults2<long>("AppointmentId", tableDifference.ResultsStore);

            long appointmentid = record.ResultsStoreType == Row.RowType.In1
                                     ? row.GetFieldValueInResults1<long>("AppointmentId", tableDifference.ResultsStore)
                                     : row.GetFieldValueInResults2<long>("AppointmentId", tableDifference.ResultsStore);

            if (appointmentid == 0 || patientid == 0)
                return row.Type == Row.RowType.In2;

            if (record.ResultsStoreType == Row.RowType.In1 || record.ResultsStoreType == Row.RowType.In2)
            {
                if (OtherDatabaseHasBetter(session, row, record.ResultsStoreType, tableDifference.ResultsStore, connection1, connection2))
                {
                    return record.ResultsStoreType != Row.RowType.In1;
                }
                if (record.ResultsStoreType == Row.RowType.In1)
                    return true;

                return false;
            }

            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> status =
                    row.GetFieldValueInResults<string>("Status", tableDifference.ResultsStore).CloneWithDefault<string>(
                        string.Empty);

                if (!(status.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> statusCondition = p => (p.Item1 == "D" || p.Item1 == "X");

                    if (statusCondition(status))
                    {
                        return true;
                    }

                    if (statusCondition(status.Reverse()))
                    {
                        return false;
                    }
                }

                Tuple<string, string> imageDescriptor = row.GetFieldValueInResults<string>("ImageDescriptor",
                                                                                           tableDifference.ResultsStore)
                    .CloneWithDefault<string>(string.Empty);



                if (!(imageDescriptor.AreItemsEqual()))
                {


                    Func<Tuple<string, string>, bool> imageDescriptorCondition = p => (p.Item1.Length > p.Item2.Length);

                    if (imageDescriptorCondition(imageDescriptor))
                    {
                        return true;
                    }

                    if (imageDescriptorCondition(imageDescriptor.Reverse()))
                    {
                        return false;
                    }
                }

                Tuple<string, string> surgery = row.GetFieldValueInResults<string>("Surgery", tableDifference.ResultsStore).CloneWithDefault<string>(string.Empty);

                if (!(surgery.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> surgeryCondition = p => (p.Item1 != "U");

                    if (surgeryCondition(surgery))
                    {
                        return true;
                    }

                    if (surgeryCondition(surgery.Reverse()))
                    {
                        return false;
                    }
                }

                Tuple<string, string> followUp = row.GetFieldValueInResults<string>("FollowUp", tableDifference.ResultsStore).CloneWithDefault<string>(string.Empty);

                if (!(followUp.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> followUpCondition = p => (p.Item1 != "U");

                    if (followUpCondition(followUp))
                    {
                        return true;
                    }

                    if (followUpCondition(followUp.Reverse()))
                    {
                        return false;
                    }
                }
            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }

        private static bool OtherDatabaseHasBetter(ComparisonSession session, Row row, Row.RowType rowType, Store store, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            IDictionary<string, object> parameters = GetParameters(row, store, session, rowType == Row.RowType.In2);

            var connection = rowType == Row.RowType.In1 ? connection2 : connection1;

            // this block of code is to decide whether it's safe to insert...so there's nothing to do when In2 (since that's only deciding whether or not to delete)
            var hasRows = connection.Connection.Execute<int?>(HasRowsForClinicalTypeSql, parameters);

            if (hasRows == 0)
                // In1 and DB2 has no rows for that clinical type, then DB2 doesn't have better (safe to insert)
                // In2 and DB1 has no rows for that clinical type, then it will be safe to insert when we sync the other way...so don't delete
                return false;

            string productionServerName = rowType == Row.RowType.In2 ? connection2.ProductionServerName : connection1.ProductionServerName;

            // use other connecion for audit entries query (the DB that actually has the row in question)
            connection = rowType == Row.RowType.In1 ? connection1 : connection2;

            var auditTableQueryCondition = connection.Connection.Execute<int?>(string.Format(DatabaseHasBetterByAuditEntrySql, productionServerName), parameters);

            return !auditTableQueryCondition.HasValue || auditTableQueryCondition == 0;
        }

        private static IDictionary<string, object> GetParameters(Row row, Store store, ComparisonSession session,
                                                          bool from2 = false)
        {
            Dictionary<string, object> result = store.Fields
                .Where(f => new[] { "AppointmentId", "ClinicalType" }.Contains(f.EscapedFieldName(from2).Replace("[", string.Empty).Replace("]", string.Empty)))
                .ToDictionary(
                f => f.EscapedFieldName(from2).Replace("[", string.Empty).Replace("]", string.Empty),
                f =>
                from2
                    ? row.GetFieldValueInResults2<object>(f.Field2.Name, store)
                    : row.GetFieldValueInResults1<object>(f.Field1.Name, store))
                .ToDictionary(i => i.Key, i => i.Value ?? DBNull.Value);

            RedGate.SQLCompare.Engine.IDatabase dbScope = from2 ? session.Database2 : session.Database1;

            Tuple<int, int> range = dbScope.GetIdentityRange(session, "AppointmentTrack");

            result["MinAuditId"] = range.Item1;
            result["MaxAuditId"] = range.Item2;

            return result;
        }
    }

    public class AppointmentsTableSynchronizer : TableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 10;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[PatientId]", "[AppDate]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[Appointments]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                var differentFields = row.GetDifferentFields(tableDifference.ResultsStore);
                if (differentFields.SequenceEqual(new[] { "[EncounterId]" }))
                {
                    // this is a basically computed column, always sync
                    return true;
                }

                Tuple<string, string> scheduleStatus = row.GetFieldValueInResults<string>("ScheduleStatus",
                                                                                          tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if (!(scheduleStatus.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> scheduleStatusCondition = p => (p.Item1 == "D");


                    if (scheduleStatusCondition(scheduleStatus))
                    {
                        return true;
                    }

                    if (scheduleStatusCondition(scheduleStatus.Reverse()))
                    {
                        return false;
                    }

                    scheduleStatusCondition = p => (p.Item2 == "P");

                    if (scheduleStatusCondition(scheduleStatus))
                    {
                        return true;
                    }

                    if (scheduleStatusCondition(scheduleStatus.Reverse()))
                    {
                        return false;
                    }

                    scheduleStatusCondition = p => (p.Item1 == "R");

                    if (scheduleStatusCondition(scheduleStatus))
                    {
                        return true;
                    }

                    if (scheduleStatusCondition(scheduleStatus.Reverse()))
                    {
                        return false;
                    }
                }

                Tuple<long, long> resourceId8 = row.GetFieldValueInResults<long>("ResourceId8",
                                                                                 tableDifference.ResultsStore);

                if (!(resourceId8.AreItemsEqual()))
                {
                    if (resourceId8.Item1 == 999)
                        return true;
                    if (resourceId8.Item2 == 999)
                        return false;
                }

                Tuple<string, string> activityStatus = row.GetFieldValueInResults<string>("ActivityStatus",
                                                                                          tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if (!(activityStatus.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> activityStatusCondition = p => (p.Item1 == "D");

                    if (activityStatusCondition(activityStatus))
                        return true;
                    if (activityStatusCondition(activityStatus.Reverse()))
                        return false;
                }

                Tuple<string, string> confirmStatus = row.GetFieldValueInResults<string>("ConfirmStatus",
                                                                                         tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if ((!(confirmStatus.AreItemsEqual())))
                {
                    if (confirmStatus.Item1 != "")
                        return true;
                    if (confirmStatus.Item2 != "")
                        return false;
                }

                if (scheduleStatus.Item1 == scheduleStatus.Item2)
                {
                    Tuple<long, long> setDate = row.GetFieldValueInResults<long>("SetDate", tableDifference.ResultsStore);

                    if (setDate.Item1 > setDate.Item2)
                        return true;
                    if (setDate.Item2 < setDate.Item1)
                        return false;
                }
            }

            return !(options.WellDefinedRulesOnly) && session.Database1.DoesDatabaseAppointmentHaveAnAppointmentTrackInDatabaseIdentityRange(session, row, tableDifference.ResultsStore);

        }
    }

    public class AppointmentTrackTableSynchronizer : WellDefinedRulesTableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 100;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[AppointmentTrack]"; }
        }
    }

    public class AppointmentTypeTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 11;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[AppointmentType]"; }
        }
    }

    public class PracticeCodeTableTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 5;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeCodeTable]"; }
        }
    }

    public class PracticeCalendarTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 5;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeCalendar]"; }
        }
    }

    public class PracticeFavoritesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 5;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeFavorites]"; }
        }
    }

    public class PracticeVendorsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override bool AutoInsertWhenIn1
        {
            get { return false; }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeVendors]"; }
        }
    }

    public class PracticeMessagesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeMessages]"; }
        }
    }

    public class AuditEntriesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {

        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override int Priority
        {
            get
            {
                return 10001;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[AuditEntries]"; }
        }
    }

    public class AuditEntryChangesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {

        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override int Priority
        {
            get
            {
                return 10000;
            }
        }

        public override ChecksumComparisonInfo ChecksumComparisonInfo
        {
            get
            {
                return new ChecksumComparisonInfo
                           {
                               Divisor = 1,
                               Join = " JOIN AuditEntries a WITH(NOLOCK) ON AuditEntryId = a.Id ",
                               RangeColumn = "CONVERT(nvarchar, AuditDateTime, 112)"
                           };
            }
        }

        public override string TableName
        {
            get { return "[dbo].[AuditEntryChanges]"; }
        }

        public override void Initialize(Database db1, Database db2)
        {
            db1.CreateConnection().Execute(@"SELECT 1 AS Value INTO #NoCheck
DELETE FROM AuditEntryChanges WHERE AuditEntryId NOT IN (SELECT Id FROM AuditEntries)
DROP TABLE #NoCheck");
            db2.CreateConnection().Execute(@"SELECT 1 AS Value INTO #NoCheck
DELETE FROM AuditEntryChanges WHERE AuditEntryId NOT IN (SELECT Id FROM AuditEntries)
DROP TABLE #NoCheck");

            base.Initialize(db1, db2);
        }

        public override string ScriptPrefix
        {
            get
            {
                return @"
ALTER TABLE dbo.AuditEntryChanges NOCHECK CONSTRAINT ALL
";
            }
        }

        public override string ScriptSuffix
        {
            get
            {
                return @"
SELECT 1 AS Value INTO #NoCheck
DELETE FROM dbo.AuditEntryChanges WHERE AuditEntryId NOT IN (SELECT Id FROM AuditEntries)
ALTER TABLE dbo.AuditEntryChanges WITH NOCHECK CHECK CONSTRAINT ALL
DROP TABLE #NoCheck
";
            }
        }
    }

    public class LogCategoriesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 100;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[LogCategories]"; }
        }
    }

    public class LogEntriesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 100;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[LogEntries]"; }
        }
    }

    ///// <summary>
    ///// DO NOT SYNC. VERY SLOW AND BENEFIT OF RETAINING IS NEGLIGIBLE.
    ///// </summary>
    //public class LogEntryLogCategoryTableSynchronizer : TableSynchronizer
    //{

    //    public override int Priority
    //    {
    //        get
    //        {
    //            return 5;
    //        }
    //    }

    //    protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
    //                                         TableDifference tableDifference, Row row, SynchronizerArguments options)
    //    {
    //        return !(options.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
    //    }

    //    public override string TableName
    //    {
    //        get { return "[dbo].[LogEntryLogCategory]"; }
    //    }
    //}

    public class LogEntryPropertiesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 90;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[LogEntryProperties]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class Encounter_IE_OverridesTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Encounter_IE_Overrides]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class EPrescription_CredentialsTableSynchronizer : TableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> modifiedDate = row.GetFieldValueInResults<DateTime?>("ModifiedDate",
                                                                                                 tableDifference.
                                                                                                     ResultsStore);

                if (!(modifiedDate.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> modifiedDateCondition =
                        p => (p.Item1 == null || p.Item1 > p.Item2);

                    if (modifiedDateCondition(modifiedDate))
                    {
                        return true;
                    }

                    if (modifiedDateCondition(modifiedDate.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);

        }


        public override string TableName
        {
            get { return "[dbo].[EPrescription_Credentials]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class Patient_EPrescription_RenewalRequestTableSynchronizer : TableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> lastModifiedDate = row.GetFieldValueInResults<DateTime?>(
                    "LastModifiedDate", tableDifference.ResultsStore);

                if (!(lastModifiedDate.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> lastModifiedDateCondition = p => (p.Item1 == null || p.Item1 > p.Item2);

                    if (lastModifiedDateCondition(lastModifiedDate))
                    {
                        return true;
                    }

                    if (lastModifiedDateCondition(lastModifiedDate.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);

        }

        public override string TableName
        {
            get { return "[dbo].[Patient_EPrescription_RenewalRequest]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class Patient_ImagesTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Patient_Images]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class Patient_ImmunizationsTableSynchronizer : TableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> immTimeStamp = row.GetFieldValueInResults<DateTime?>("ImmTimeStamp",
                                                                                               tableDifference.
                                                                                                   ResultsStore);

                if (!(immTimeStamp.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> immTimeStampCondition = p => (p.Item1 == null || p.Item1 > p.Item2);

                    if (immTimeStampCondition(immTimeStamp))
                    {
                        return true;
                    }

                    if (immTimeStampCondition(immTimeStamp.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);

        }

        public override string TableName
        {
            get { return "[dbo].[Patient_Immunizations]"; }
        }
    }


    // ReSharper disable InconsistentNaming
    public class Encounter_SurveillanceTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Encounter_Surveillance]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class HL7_LabInterfaceTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[HL7_LabInterface]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class HL7_LabReportsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[HL7_LabReports]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class HL7_ObservationTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[HL7_Observation]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class HL7_Observation_DetailsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[HL7_Observation_Details]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class IE_EventsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[IE_Events]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class IE_FieldParamsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[IE_FieldParams]"; }
        }
    }


    // ReSharper disable InconsistentNaming
    public class IE_RulesTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[IE_Rules]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class IE_RulesPhrasesTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[IE_RulesPhrases]"; }
        }
    }

    public class PatientClinicalSurgeryPlanTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientClinicalSurgeryPlan]"; }
        }
    }

    public class PatientDemoAltTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientDemoAlt]"; }
        }
    }

    public class EventCodesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[EventCodes]"; }
        }
    }

    public class PracticeAuditTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 10;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeAudit]"; }
        }
    }

    public class PracticeReportsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeReports]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class CMSReportsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[CMSReports]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class CMSReports_ParamsTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }
        public override string TableName
        {
            get { return "[dbo].[CMSReports_Params]"; }
        }
    }

    public class LookupsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Lookups]"; }
        }
    }

    public class LookupValuesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[LookupValues]"; }
        }
    }

    public class MedicaidLocationCodesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[MedicaidLocationCodes]"; }
        }
    }

    public class MessagingTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Messaging]"; }
        }
    }

    public class Messaging2TableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Messaging2]"; }
        }
    }

    public class PayerMappingTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PayerMapping]"; }
        }
    }

    public class PhysicianSpiTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PhysicianSpi]"; }
        }
    }

    public class PracticeAffiliationsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeAffiliations]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class Patient_MedicationsXMlTableSynchronizer : TableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }


        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> lastModifiedDate = row.GetFieldValueInResults<DateTime?>("LastModifiedDate",
                                                                                               tableDifference.
                                                                                                   ResultsStore);

                if (!(lastModifiedDate.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> lastModifiedDateCondition = p => (p.Item1 == null || p.Item1 > p.Item2);

                    if (lastModifiedDateCondition(lastModifiedDate))
                    {
                        return true;
                    }

                    if (lastModifiedDateCondition(lastModifiedDate.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);

        }

        public override string TableName
        {
            get { return "[dbo].[Patient_MedicationsXMl]"; }
        }
    }

    public class PharmaciesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Pharmacies]"; }
        }
    }

    public class ValuesetsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Valuesets]"; }
        }
    }

    public class PatientPrecertsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientPreCerts]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class tblMsgTableSynchronizer : TableSynchronizer
    // ReSharper restore InconsistentNaming
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }


        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> lastModifiedDate = row.GetFieldValueInResults<DateTime?>("LastModifiedDate",
                                                                                               tableDifference.
                                                                                                   ResultsStore);

                if (!(lastModifiedDate.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> lastModifiedDateCondition = p => (p.Item1 == null || p.Item1 > p.Item2);

                    if (lastModifiedDateCondition(lastModifiedDate))
                    {
                        return true;
                    }

                    if (lastModifiedDateCondition(lastModifiedDate.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);

        }

        public override string TableName
        {
            get { return "[dbo].[tblMsg]"; }
        }
    }

    public class PracticeNameTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 5;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeName]"; }
        }
    }

    public class PracticePayablesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticePayables]"; }
        }
    }

    public class ImmunizationsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Immunizations]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class tblGrowthChartDetailTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[tblGrowthChartDetail]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class tblGrowthChartMasterTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[tblGrowthChartMaster]"; }
        }
    }

    public class PatientReferralTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override string TableName
        {
            get { return "[dbo].[PatientReferral]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class REPORTFILTERSTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[REPORTFILTERS]"; }
        }
    }

    public class PracticeServicesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override bool AutoInsertWhenIn1
        {
            get { return false; }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeServices]"; }
        }
    }

    public class PracticeInsurersTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 9;
            }
        }

        public override bool NeverDelete
        {
            get
            {
                return true;
            }
        }

        public override bool AutoInsertWhenIn1
        {
            get { return true; }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeInsurers]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
            TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> insurerCrossOver = row.GetFieldValueInResults<string>("InsurerCrossOver",
                    tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);
                if (!(insurerCrossOver.AreItemsEqual()))
                {
                    return false;
                }
            }
            return true;
        }
    }

    public class PatientNotesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientNotes]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> noteHighlight = row.GetFieldValueInResults<string>("NoteHighlight",
                                                                                         tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if (!(noteHighlight.AreItemsEqual()))
                {
                    Tuple<string, string> noteType = row.GetFieldValueInResults<string>("NoteType",
                                                                                        tableDifference.ResultsStore).
                        CloneWithDefault<string>(string.Empty);
                    if ((noteType.Item1 == "C") && (noteType.Item2 == "C"))
                    {
                        Tuple<string, string> noteSystem = row.GetFieldValueInResults<string>("NoteSystem",
                                                                                              tableDifference.
                                                                                                  ResultsStore).
                            CloneWithDefault<string>(string.Empty);

                        if ((string.IsNullOrEmpty(noteSystem.Item1)) && (string.IsNullOrEmpty(noteSystem.Item2)))
                        {
                            Func<Tuple<string, string>, bool> noteHighlightCondition =
                                p => (string.IsNullOrEmpty(p.Item1) && (!(string.IsNullOrEmpty(p.Item2))));

                            if (noteHighlightCondition(noteHighlight))
                                return true;

                            if (noteHighlightCondition(noteHighlight.Reverse()))
                                return false;
                        }

                        if ((!(string.IsNullOrEmpty(noteSystem.Item1))) && (!(string.IsNullOrEmpty(noteSystem.Item2))))
                        {
                            Func<Tuple<string, string>, bool> noteHighlightCondition =
                                p => ((!(string.IsNullOrEmpty(p.Item1))) && string.IsNullOrEmpty(p.Item2));

                            if (noteHighlightCondition(noteHighlight))
                                return true;

                            if (noteHighlightCondition(noteHighlight.Reverse()))
                                return false;
                        }
                    }
                }


                Tuple<string, string> noteOffDate = row.GetFieldValueInResults<string>("NoteOffDate",
                                                                                       tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if (!(noteOffDate.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> noteOffDateCondition =
                        p => (string.IsNullOrEmpty(p.Item1) && (!(string.IsNullOrEmpty(p.Item2))));

                    if (noteOffDateCondition(noteOffDate))
                        return true;

                    if (noteOffDateCondition(noteOffDate))
                        return false;
                }

                Tuple<string, string> note1 =
                    row.GetFieldValueInResults<string>("Note1", tableDifference.ResultsStore).CloneWithDefault<string>(
                        string.Empty);

                if (!(note1.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> note1Condition = p => (p.Item1.Length > p.Item2.Length);

                    if (note1Condition(note1))
                        return true;

                    if (note1Condition(note1.Reverse()))
                        return false;
                }
                else
                {
                    Tuple<string, string> note2 =
                        row.GetFieldValueInResults<string>("Note2", tableDifference.ResultsStore).CloneWithDefault
                            <string>(string.Empty);

                    if (!(note2.AreItemsEqual()))
                    {
                        Func<Tuple<string, string>, bool> note2Condition = p => (p.Item1.Length > p.Item2.Length);

                        if (note2Condition(note2))
                            return true;

                        if (note2Condition(note2.Reverse()))
                            return false;
                    }
                    else
                    {
                        Tuple<string, string> note3 = row.GetFieldValueInResults<string>("Note3",
                                                                                         tableDifference.ResultsStore).
                            CloneWithDefault<string>(string.Empty);

                        if (!(note3.AreItemsEqual()))
                        {
                            Func<Tuple<string, string>, bool> note3Condition = p => (p.Item1.Length > p.Item2.Length);

                            if (note3Condition(note3))
                                return true;

                            if (note3Condition(note3.Reverse()))
                                return false;
                        }
                        else
                        {
                            Tuple<string, string> note4 = row.GetFieldValueInResults<string>("Note4",
                                                                                             tableDifference.
                                                                                                 ResultsStore).
                                CloneWithDefault<string>(string.Empty);

                            if (!(note4.AreItemsEqual()))
                            {
                                Func<Tuple<string, string>, bool> note4Condition =
                                    p => (p.Item1.Length > p.Item2.Length);

                                if (note4Condition(note4))
                                    return true;

                                if (note4Condition(note4.Reverse()))
                                    return false;
                            }
                        }
                    }
                }
            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }

    public class PracticeActivityTableSynchronizer : TableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 8;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[PatientId]", "[AppointmentId]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeActivity]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {

                Tuple<string, string> status =
                    row.GetFieldValueInResults<string>("Status", tableDifference.ResultsStore).CloneWithDefault<string>(
                        string.Empty);

                if (!(status.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> statusCondition = p => (p.Item1 == "D");

                    if (statusCondition(status))
                        return true;
                    if (statusCondition(status.Reverse()))
                        return false;
                }


                Tuple<float, float> activityStatusTRef = row.GetFieldValueInResults<float>("ActivityStatusTRef",
                                                                                           tableDifference.ResultsStore);

                if (!(activityStatusTRef.AreItemsEqual()))
                {
                    Func<Tuple<float, float>, bool> activityStatusTRefCondition =
                        p => (p.Item1 > p.Item2);

                    if (activityStatusTRefCondition(activityStatusTRef))
                        return true;

                    if (activityStatusTRefCondition(activityStatusTRef.Reverse()))
                        return false;
                }
            }

            return !(options.WellDefinedRulesOnly) && session.Database1.DoesDatabaseAppointmentHaveAnAppointmentTrackInDatabaseIdentityRange(session, row, tableDifference.ResultsStore);

        }
    }

    public class ResourcesTableSynchronizer : TableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 11;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Resources]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {

            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> resourceType = row.GetFieldValueInResults<string>("ResourceType",
                                                                                        tableDifference.ResultsStore).
                    CloneWithDefault<string>(string.Empty);

                if (!(resourceType.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> resourceTypeCondition =
                        p => (new[] { "X", "Y", "Z" }.Contains(p.Item1));

                    if (resourceTypeCondition(resourceType))
                        return true;

                    if (resourceTypeCondition(resourceType.Reverse()))
                    {
                        return false;
                    }
                }
            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }

    public class DrugTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[Drug]"; }
        }
    }

    public class PracticeFeeSchedulesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 3;
            }
        }


        public override string TableName
        {
            get { return "[dbo].[PracticeFeeSchedules]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> endDate = row.GetFieldValueInResults<string>("EndDate",
                                                                                   tableDifference.ResultsStore).CloneWithDefault<string>(string.Empty);

                if (!(endDate.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> endDateCondition =
                        p => (string.IsNullOrEmpty(p.Item2) && (!(string.IsNullOrEmpty(p.Item1))));

                    if (endDateCondition(endDate))
                        return true;
                    if (endDateCondition(endDate.Reverse()))
                        return false;
                }
            }

            return !options.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }
    public class PatientsTableSynchronizer : TableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 11;
            }
        }

        public override string TableName
        {
            get { return "[model].[Patients]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<int, int> ethnicity = row.GetFieldValueInResults<int>("EthnicityId",
                                                                                     tableDifference.ResultsStore);

                if (!(ethnicity.AreItemsEqual()))
                {
                    Func<Tuple<int, int>, bool> ethnicityCondition =
                        p => (string.IsNullOrEmpty((p.Item2.ToString())) && (!string.IsNullOrEmpty(p.Item1.ToString())));

                    if (ethnicityCondition(ethnicity))
                    {
                        return true;
                    }

                    if (ethnicityCondition(ethnicity.Reverse()))
                    {
                        return false;
                    }
                }

                Tuple<int, int> language = row.GetFieldValueInResults<int>("LanguageId",
                                                                                    tableDifference.ResultsStore);

                if (!(language.AreItemsEqual()))
                {
                    Func<Tuple<int, int>, bool> languageCondition =
                        p => (string.IsNullOrEmpty((p.Item2.ToString())) && (!string.IsNullOrEmpty(p.Item1.ToString())));

                    if (languageCondition(language))
                    {
                        return true;
                    }

                    if (languageCondition(language.Reverse()))
                    {
                        return false;
                    }
                }
            }

            return !parameters.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }

    public class FinancialBatchesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 6;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[PatientId]", "[InsurerId]" }; }
        }

        public override string TableName
        {
            get { return "[model].[FinancialBatches]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }

    }

    public class PatientInsurancesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 5;
            }
        }

        public override string TableName
        {
            get { return "[model].[PatientInsurances]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<bool, bool> status = row.GetFieldValueInResults<bool>("IsDeleted", tableDifference.ResultsStore);

                if (!(status.AreItemsEqual()))
                {
                    Func<Tuple<bool, bool>, bool> statusCondition = p => (p.Item1 == false);

                    if (statusCondition(status))
                    {
                        return true;
                    }

                    if (statusCondition(status.Reverse()))
                    {
                        return false;
                    }
                }

            }
            return !parameters.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }

    public class InsurancePoliciesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 7;
            }
        }
        
        public override string TableName
        {
            get { return "[model].[InsurancePolicies]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            return !parameters.WellDefinedRulesOnly && session.Database1.IsMainServer(session);
        }
    }

    public class PatientFinancialDependentsTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[DependentId]", "[PatientId]", "[PolicyHolderId]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientFinancialDependents]"; }
        }
    }

    public class InvoicesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 4;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[EncounterId]" }; }
        }

        public override string TableName
        {
            get { return "[model].[Invoices]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    public class InvoiceReceivablesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 2;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[InvoiceId]" }; }
        }

        public override string TableName
        {
            get { return "[model].[InvoiceReceivables]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<bool, bool> receivableType = row.GetFieldValueInResults<bool>("OpenForReview",
                                                                                          tableDifference.ResultsStore);

                if (!(receivableType.AreItemsEqual()))
                {
                    Func<Tuple<bool, bool>, bool> receivableTypeCondition = p => (p.Item1 != true);

                    if (receivableTypeCondition(receivableType))
                    {
                        return true;
                    }

                    if (receivableTypeCondition(receivableType.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    public class PatientReceivableServicesTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 9;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[Invoice]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[PatientReceivableServices]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> status = row.GetFieldValueInResults<string>("Status", tableDifference.ResultsStore).CloneWithDefault<string>(string.Empty);

                if (!(status.AreItemsEqual()))
                {
                    Func<Tuple<string, string>, bool> statusCondition = p => (p.Item1 == "X");

                    if (statusCondition(status))
                    {
                        return true;
                    }

                    if (statusCondition(status.Reverse()))
                    {
                        return false;
                    }
                }

            }
            return !(options.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    // ReSharper disable InconsistentNaming
    public class CLInventoryTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[CLInventory]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class CLOrdersTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[CLOrders]"; }
        }
    }

    // ReSharper disable InconsistentNaming
    public class PCInventoryTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[PCInventory]"; }
        }
    }

    public class LoginUsersTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 100;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                            TableDifference tableDifference, Row row, DbSyncParameters options)
        {

            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<DateTime?, DateTime?> logoutTime = row.GetFieldValueInResults<DateTime?>("LogoutTime", tableDifference.ResultsStore);

                if (!(logoutTime.AreItemsEqual()))
                {
                    Func<Tuple<DateTime?, DateTime?>, bool> logoutTimeCondition = p => (p.Item1 == null);

                    if (logoutTimeCondition(logoutTime))
                    {
                        return true;
                    }

                    if (logoutTimeCondition(logoutTime.Reverse()))
                    {
                        return false;
                    }
                }

            }

            return !(options.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }

        public override string TableName
        {
            get { return "[dbo].[LoginUsers]"; }
        }
    }

    // LoginUsers_Audit is the actual name of the table, so I want to leave the name of the table synchronizer as is.
    // ReSharper disable InconsistentNaming

    public class LoginUsers_AuditTableSynchronizer : WellDefinedRulesTableSynchronizer
    // ReSharper restore InconsistentNaming
    {
        public override int Priority
        {
            get
            {
                return 9;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[LoginUsers_Audit]"; }
        }
    }

    public class ModifierRulesTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string TableName
        {
            get { return "[dbo].[ModifierRules]"; }
        }
    }

    #region Nested type: AdjustmentsTableSynchronizer

    public class AdjustmentsTableSynchronizer : TableSynchronizer
    {

        public override int Priority
        {
            get
            {
                return 0;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[InvoiceReceivableId]", "[BillingServiceId]" }; }
        }

        public override string TableName
        {
            get { return "[model].[Adjustments]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters)
        {
            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }

    }

    #endregion

    // ReSharper disable InconsistentNaming

    public class PracticeTransactionJournalTableSynchronizer : TableSynchronizer
    {

        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override string[] DisallowedFieldsDifferent
        {
            get { return new[] { "[TransactionTypeId]", "[TransactionType]" }; }
        }

        public override string TableName
        {
            get { return "[dbo].[PracticeTransactionJournal]"; }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters options)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<string, string> transactionType = row.GetFieldValueInResults<string>("TransactionType",
                                                                                           tableDifference.
                                                                                               ResultsStore).CloneWithDefault<string>(string.Empty);

                if (new[] { "L", "Q", "R", "F", "S", "Y" }.Contains(transactionType.Item1))
                {
                    if (transactionType.Item1 == "L" && transactionType.AreItemsEqual())
                    {
                        Tuple<string, string> transactionRemark =
                            row.GetFieldValueInResults<string>("TransactionRemark", tableDifference.ResultsStore).CloneWithDefault<string>(string.Empty);
                        if (!(transactionRemark.AreItemsEqual()))
                        {
                            Func<Tuple<string, string>, bool> transactionRemarkCondition =
                                p => (p.Item1 != "" && p.Item2 == "");

                            if (transactionRemarkCondition(transactionRemark))
                                return true;
                            if (transactionRemarkCondition(transactionRemark.Reverse()))
                                return false;
                        }
                    }

                    Tuple<string, string> transactionStatus = row.GetFieldValueInResults<string>(
                        "TransactionStatus",
                        tableDifference.
                            ResultsStore).CloneWithDefault<string>(string.Empty);

                    if (!(transactionStatus.AreItemsEqual()))
                    {
                        Func<Tuple<string, string>, bool> transactionStatusCondition =
                            p => (p.Item1 == "Z" || p.Item2 == "P");

                        if (transactionStatusCondition(transactionStatus))
                            return true;
                        if (transactionStatusCondition(transactionStatus.Reverse()))
                            return false;
                    }
                }

            }

            return !(options.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    #region Nested type: BillingServiceTransactionsTableSynchronizer

    public class ExternalSystemMessagesTableSynchronizer : TableSynchronizer
    {

        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override int Priority
        {
            get
            {
                return 1;
            }
        }

        public override string TableName
        {
            get { return "[model].[ExternalSystemMessages]"; }
        }


        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    public class BillingServiceTransactionsTableSynchronizer : TableSynchronizer
    {

        public override bool ExcludeIfRowCountSame
        {
            get
            {
                return true;
            }
        }

        public override int Priority
        {
            get
            {
                return 0;
            }
        }

        public override string TableName
        {
            get { return "[model].[BillingServiceTransactions]"; }
        }


        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record,
                                             TableDifference tableDifference, Row row, DbSyncParameters parameters, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            if (record.ResultsStoreType == Row.RowType.Different)
            {
                Tuple<int, int> billingServiceTransactionStatus = row.GetFieldValueInResults<int>("BillingServiceTransactionStatusId",
                                                                                           tableDifference.
                                                                                               ResultsStore);

                if (!(billingServiceTransactionStatus.AreItemsEqual()))
                {
                    Func<Tuple<int, int>, bool> billingServiceTransactionStatusCondition =
                            p => (p.Item1 > p.Item2);

                    if (billingServiceTransactionStatusCondition(billingServiceTransactionStatus))
                        return true;
                    if (billingServiceTransactionStatusCondition(billingServiceTransactionStatus.Reverse()))
                        return false;
                }
            }

            return !(parameters.WellDefinedRulesOnly) && session.Database1.IsMainServer(session);
        }
    }

    #endregion

    public class UpdateConfigurationTableSynchronizer : WellDefinedRulesTableSynchronizer
    {
        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override bool AutoInsertWhenIn1
        {
            get { return false; }
        }

        public override string TableName
        {
            get { return "[dbo].[UpdateConfiguration]"; }
        }
    }

    public class ScheduleBlocksTableSynchronizer : TableSynchronizer
    {
        public override string TableName
        {
            get { return "[model].[ScheduleBlocks]"; }
        }

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        protected override bool SelectRecord(ComparisonSession session, SynchronizationRecord record, TableDifference tableDifference, Row row, DbSyncParameters options, DbSyncConnection connection1, DbSyncConnection connection2)
        {
            if (record.ResultsStoreType == Row.RowType.In2)
            {
                var startDateTime = row.GetFieldValueInResults<string>("StartDateTime",
                                                                                           tableDifference.
                                                                                               ResultsStore);

                var userId = row.GetFieldValueInResults<string>("UserId",
                                                                                           tableDifference.
                                                                                               ResultsStore);

                var result1 = connection1.Connection.Execute<string>(
                @"SELECT * FROM model.ScheduleBlocks WHERE StartDateTime = '" + startDateTime.Item2 + "' and UserId = " + userId.Item2);

                return result1 != null;
            }
            return true;
        }
    }
}