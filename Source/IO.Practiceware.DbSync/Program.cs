﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IO.Practiceware.DbSync.Extensions;
using System.Collections.Generic;
using Options = RedGate.SQLCompare.Engine.Options;

namespace IO.Practiceware.DbSync
{
    class Program
    {
        public static readonly TimeSpan UserInputTimeout = TimeSpan.FromMinutes(10);

        static Program()
        {
            DbSyncBootstrapper.Initialize();
        }

        static void Main(string[] args)
        {
            var parameters = GetDbSyncParameters(args);
            var synchronizer = new Synchronizer(parameters);

            var sw = new Stopwatch();
            sw.Start();

            var emailInfo = new EmailInfo();
            try
            {
                emailInfo.EmailAddress = AskForEmailAddress();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }

            Environment.SetEnvironmentVariable("RGTEMP", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp"));

            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }

            string fileName = DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss");
            Trace.AutoFlush = true;
            Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.Listeners.Add(new TextWriterTraceListener(Path.Combine("logs", string.Format("{0}{1}", fileName, ".log"))));

            try
            {
                synchronizer.Run();

                Trace.Listeners.OfType<TextWriterTraceListener>().ToList().ForEach(tl => tl.Close());
                try
                {
                    var path = Path.Combine("logs", string.Format("{0}{1}", fileName, ".log"));
                    emailInfo.EmailMessage = SqlCompareHelper.ReadAllText(path);
                    emailInfo.EmailSubject = "Database sync completed without errors.";
                }
                catch (Exception e)
                {
                    emailInfo.EmailSubject = "There was a problem with reading the log file.";
                    emailInfo.EmailMessage = e.ToString();
                }
            }
            catch (Exception ex)
            {
                if (!Directory.Exists("errors"))
                {
                    Directory.CreateDirectory("errors");
                }

                Trace.Listeners.OfType<TextWriterTraceListener>().ToList().ForEach(tl => tl.Close());

                File.WriteAllText("errors\\" + DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss") + ".log", ex.ToString());
                Trace.TraceError(ex.ToString());

                emailInfo.EmailMessage = ex.ToString();
                try
                {
                    var path = Path.Combine("logs", string.Format("{0}{1}", fileName, ".log"));
                    emailInfo.EmailMessage += Environment.NewLine + Environment.NewLine + SqlCompareHelper.ReadAllText(path);
                    emailInfo.EmailSubject = "ATTENTION: Database Sync did not complete - check error logs.";
                }
                catch (Exception e)
                {
                    emailInfo.EmailSubject = "There was a problem with reading the log file.";
                    emailInfo.EmailMessage = e.ToString();
                }
            }

            var totalRunTime = sw.ElapsedMilliseconds;
            Trace.TraceInformation("Total run time: {0}ms.", totalRunTime);
            emailInfo.EmailMessage += "Total run time: " + totalRunTime + "ms.";
            emailInfo.SendEmail();
        }

        private static string AskForEmailAddress()
        {
            Console.WriteLine("Please enter the e-mail address where you would like the results of the sync to be sent.");
            return ConsoleExtensions.ReadLine(UserInputTimeout);
        }

        private static string GetStringArgument(IEnumerable<string> args, string name, string defaultValue = null)
        {
            var arg = 
                 args
                .Where(a => a.Contains('=') && a.Split('=')[0].Equals(name, StringComparison.OrdinalIgnoreCase))
                .Select(a => a.Split('=')[1].ToLower()).FirstOrDefault();

            if (arg == null || arg == string.Empty && defaultValue != null)
            {
                return defaultValue;
            }

            return arg;
        }

        private static bool GetBoolArgument(IEnumerable<string> args, string name, bool? defaultValue = null)
        {
            bool parameter;
            if (!Boolean.TryParse(GetStringArgument(args, name), out parameter))
            {
                if (defaultValue == null)
                    throw new InvalidOperationException(string.Format("Could not get a valid boolean value from parameter '{0}' in the list of arguments", name));
                return defaultValue.Value;
            }

            return parameter;
        }

        private static T GetEnumArgument<T>(IEnumerable<string> args, string name, T? defaultValue = null) where T : struct
        {
            if (!typeof(T).IsEnum)
                throw new InvalidOperationException("GetEnumArgument generic method is only valid for enumeration types");

            var arg = GetStringArgument(args, name);

            if (string.IsNullOrEmpty(arg))
            {
                if (defaultValue != null) return defaultValue.Value;
            }

            T e;
            if (arg != null && Enum.TryParse(arg.Trim(), true, out e) )
            {
                return e;
            }

            if (defaultValue != null)
            {
                return defaultValue.Value;
            }

            throw new InvalidOperationException(string.Format("Could not get a valid enum value from parameter '{0}' in the list of arguments", name));
        }

        private static List<string> GetValuesFromArgument(IEnumerable<string> args, string argumentKeyToQuery, IEnumerable<string> defaultValues = null)
        {
            if (argumentKeyToQuery == null)
                return null;

            var list = new List<string>();
            string argValues = GetStringArgument(args, argumentKeyToQuery);

            if (string.IsNullOrEmpty(argValues))
            {
                if (defaultValues != null) return defaultValues.ToList();
            }

            if (argValues != null) 
            {
                argValues = argValues.Trim("'".ToCharArray());

                foreach (var v in argValues.Split(",".ToCharArray())) 
                { 
                    list.Add(v.Trim()); 
                }
            }

            return list;
        }

        private static List<T> GetEnumValuesFromArgument<T>(IEnumerable<string> args, string argumentKeyToQuery, IEnumerable<T> defaultValues = null) where T : struct
        {
            if (!typeof(T).IsEnum)
                return null;

            if (argumentKeyToQuery == null)
                return null;

            var list = new List<T>();

            string argValues = GetStringArgument(args, argumentKeyToQuery);

            if (string.IsNullOrEmpty(argValues))
            {
                if (defaultValues != null) return defaultValues.ToList();
            }

            if (argValues != null) 
            {
                argValues = argValues.Trim("'".ToCharArray());

                foreach (var v in argValues.Split(",".ToCharArray()))
                {
                    list.Add(GetEnumArgument<T>(args, v));
                }
            }

            return list;
        }        

        private static DbSyncParameters GetDbSyncParameters(string[] args)
        {
            var parameters = new DbSyncParameters { DistributionServer = GetStringArgument(args, "distributionServer") };

            parameters.DebugMode = GetBoolArgument(args, "debugMode", DbSyncParameters.Default.DebugMode);

            if (String.IsNullOrEmpty(parameters.DistributionServer))
            {
                parameters.Server1Name = GetStringArgument(args, "server1");
                parameters.Db1Name = GetStringArgument(args, "db1");
                parameters.Server2Name = GetStringArgument(args, "server2");
                parameters.Db2Name = GetStringArgument(args, "db2");
                parameters.ProductionServer1Name = GetStringArgument(args, "productionServer1Name");
                parameters.ProductionServer2Name = GetStringArgument(args, "productionServer2Name");

                if (string.Equals(parameters.ProductionServer1Name, parameters.ProductionServer2Name) && !string.IsNullOrEmpty(parameters.ProductionServer1Name))
                {
                    Console.WriteLine("WARNING: Production server 1 and 2 are both the same. This can cause problems, are you sure you want to continue?");
                }
            }

            TryGetServerParameters(parameters);                       

            parameters.RunScript = GetEnumArgument(args, "runScript", new RunScriptOption?(DbSyncParameters.Default.RunScript)); 
            parameters.AutoSelectOnly = GetBoolArgument(args, "autoSelectOnly", DbSyncParameters.Default.AutoSelectOnly);            
            parameters.WellDefinedRulesOnly = GetBoolArgument(args, "wellDefinedRulesOnly", DbSyncParameters.Default.WellDefinedRulesOnly);            
            parameters.PerformDatabaseBackups = GetBoolArgument(args, "performDatabaseBackups", DbSyncParameters.Default.PerformDatabaseBackups);
            parameters.BackupDatabasesOnly = GetBoolArgument(args, "backupDatabasesOnly", DbSyncParameters.Default.BackupDatabasesOnly);
            parameters.SchemaSync = GetBoolArgument(args, "syncSchema", DbSyncParameters.Default.SchemaSync);
            parameters.ExecuteDbSyncScriptAutomatically = DbSyncParameters.Default.ExecuteDbSyncScriptAutomatically;

            if (parameters.SchemaSync)
            {
                var defaultOptions = new List<Options>();
                defaultOptions.Add( DbSyncParameters.Default.CompareOptions);

                var dbOptions = GetEnumValuesFromArgument(args, "DbComparisonOptions", defaultOptions);
                var objectsToInclude = GetEnumValuesFromArgument<RedGate.SQLCompare.Engine.ObjectType>(args, "ObjectTypesToInclude");

                parameters.CompareOptions = dbOptions.Aggregate((x, y) => x.Plus(y));

                parameters.ObjectTypesToInclude = objectsToInclude.ToList();
            }

            var includeTables = GetValuesFromArgument(args, "TablesToInclude");
            var excludeTables = GetValuesFromArgument(args, "TablesToExclude");
            parameters.TablesToInclude = includeTables;
            parameters.TablesToExclude = excludeTables;

            parameters.DataSync = GetBoolArgument(args, "syncData", DbSyncParameters.Default.DataSync);

            return parameters;
        }

        private static void TryGetServerParameters(DbSyncParameters parameters)
        {
            if (String.IsNullOrEmpty(parameters.DistributionServer)
                && String.IsNullOrEmpty(parameters.Server1Name)
                && String.IsNullOrEmpty(parameters.Server2Name))
            {
                var distributionServers = SqlCompareHelper.GetListOfDistributionServers();

                if (distributionServers.Count != 1)
                {
                    string userAnswer;
                    var alert = distributionServers.Count > 1 ? "DbSync has found more than one possible distribution server. They are: "
                                    + string.Join(", ", distributionServers)
                                    : "Unable to locate any viable distribution servers.";

                    Console.WriteLine(alert);

                    do
                    {
                        const string question = "Would you like to specify a distribution server now? (Y/N).";
                        Console.WriteLine(question);

                        userAnswer = ConsoleExtensions.ReadLine(TimeSpan.FromMinutes(1));

                        if (userAnswer != null)
                            userAnswer = userAnswer.ToLower();

                        if (userAnswer == null)
                        {
                            var emailInfo = new EmailInfo();
                            emailInfo.EmailMessage = @"DBSync timed out waiting on user input. DBSync asked:" + question;
                            emailInfo.EmailSubject = "DBSync timed out waiting for user input.";
                            emailInfo.SendEmail();
                            Environment.Exit(-1);
                        }
                        else if (userAnswer == "y")
                        {
                            Console.WriteLine("\r\nPlease enter the name of the distribution server, and press 'Enter'.");
                            parameters.DistributionServer = Console.ReadLine();
                            parameters.RunningInHubMode = true;
                        }
                        else if (userAnswer == "n")
                        {
                            Environment.Exit(-1);
                        }

                    } while (userAnswer != "y" && userAnswer != "n");
                }

                if (distributionServers.Count == 1)
                {
                    parameters.DistributionServer = distributionServers.First();
                    parameters.RunningInHubMode = true;
                }
            }

        }
    }
}