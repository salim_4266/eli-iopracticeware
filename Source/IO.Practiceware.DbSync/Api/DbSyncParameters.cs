using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace IO.Practiceware.DbSync
{
    /// <summary>
    /// User provided arguments to define how the sync engine performs comparisons and synchronizations.
    /// </summary>    
    public class DbSyncParameters
    {
        static DbSyncParameters()
        {
            DefaultSqlCompareOptions = CreateOptions(RedGate.SQLCompare.Engine.Options.AddDatabaseUseStatement
                                       , RedGate.SQLCompare.Engine.Options.ForceSyncScriptGeneration
                                       , RedGate.SQLCompare.Engine.Options.IgnoreBindings
                                       , RedGate.SQLCompare.Engine.Options.IgnoreDatabaseAndServerName
                                       , RedGate.SQLCompare.Engine.Options.IgnoreFileGroups
                                       , RedGate.SQLCompare.Engine.Options.IgnoreFillFactor
                                       , RedGate.SQLCompare.Engine.Options.DropAndCreateInsteadOfAlter
                                       , RedGate.SQLCompare.Engine.Options.ObjectExistenceChecks
                                       , RedGate.SQLCompare.Engine.Options.IgnoreUserProperties
                                       , RedGate.SQLCompare.Engine.Options.IgnoreReplicationTriggers
                                       , RedGate.SQLCompare.Engine.Options.NoSQLPlumbing
                                       , RedGate.SQLCompare.Engine.Options.SeparateTriggers);

            Default = new DbSyncParameters
                      {
                          Server1Name = "Server1",
                          Server2Name = "Server2",
                          Db1Name = "Database1",
                          Db2Name = "Database2",
                          DistributionServer = "",
                          SchemaSync = false,
                          CompareOptions = DefaultSqlCompareOptions,
                          DataSync = true,
                          AutoSelectOnly = false,
                          WellDefinedRulesOnly = false,
                          RunScript = RunScriptOption.TwoWay,
                          ExecuteDbSyncScriptAutomatically = true,
                          PerformDatabaseBackups = true,
                          BackupDatabasesOnly = false,
                          DebugMode = false,
                          RunningInHubMode = false
                      };

            TableSchemaSyncOnlyDefaultParameters = new DbSyncParameters
            {
                BackupDatabasesOnly = false,
                DataSync = false,
                SchemaSync = true,
                RunScript = RunScriptOption.OneWay,
                ExecuteDbSyncScriptAutomatically = true,
                TablesToInclude = null,
                ObjectTypesToInclude = new List<RedGate.SQLCompare.Engine.ObjectType>
                {
                    RedGate.SQLCompare.Engine.ObjectType.User,
                    RedGate.SQLCompare.Engine.ObjectType.Schema,
                    RedGate.SQLCompare.Engine.ObjectType.Function,
                    RedGate.SQLCompare.Engine.ObjectType.Table,
                },
                TablesToExclude = null,
                PerformDatabaseBackups = false,
                CompareOptions = CreateOptions(
                    RedGate.SQLCompare.Engine.Options.AddDatabaseUseStatement
                    , RedGate.SQLCompare.Engine.Options.IgnoreReplicationTriggers
                    , RedGate.SQLCompare.Engine.Options.IgnoreFillFactor
                    , RedGate.SQLCompare.Engine.Options.ForceSyncScriptGeneration
                    , RedGate.SQLCompare.Engine.Options.IgnoreConstraintNames
                    , RedGate.SQLCompare.Engine.Options.IgnoreSchemaObjectAuthorization
                    , RedGate.SQLCompare.Engine.Options.IgnoreUserProperties
                    , RedGate.SQLCompare.Engine.Options.IgnoreKeys
                    , RedGate.SQLCompare.Engine.Options.IgnoreChecks
                    , RedGate.SQLCompare.Engine.Options.IgnoreTriggers
                    , RedGate.SQLCompare.Engine.Options.IgnoreInsteadOfTriggers
                    , RedGate.SQLCompare.Engine.Options.IgnoreConstraintNames
                    , RedGate.SQLCompare.Engine.Options.NoSQLPlumbing
                    , RedGate.SQLCompare.Engine.Options.IgnoreExtendedProperties
                    , RedGate.SQLCompare.Engine.Options.ForceColumnOrder
                    , RedGate.SQLCompare.Engine.Options.IncludeDependencies)
            };

            TableDataSyncOnlyDefaultParameters = new DbSyncParameters
            {
                BackupDatabasesOnly = false,
                DataSync = true,
                SchemaSync = false,
                RunScript = RunScriptOption.OneWay,
                ExecuteDbSyncScriptAutomatically = true,
                TablesToInclude = null,
                ObjectTypesToInclude = new List<RedGate.SQLCompare.Engine.ObjectType>
                                                          {
                                                              RedGate.SQLCompare.Engine.ObjectType.Table
                                                          },
                TablesToExclude = null,
                PerformDatabaseBackups = false,
                CompareOptions = CreateOptions(RedGate.SQLCompare.Engine.Options.ObjectExistenceChecks
                                 , RedGate.SQLCompare.Engine.Options.AddDatabaseUseStatement
                                 , RedGate.SQLCompare.Engine.Options.IgnoreReplicationTriggers
                                 , RedGate.SQLCompare.Engine.Options.IgnoreFillFactor
                                 , RedGate.SQLCompare.Engine.Options.ForceSyncScriptGeneration
                                 , RedGate.SQLCompare.Engine.Options.IgnoreConstraintNames
                                 , RedGate.SQLCompare.Engine.Options.IgnoreSchemaObjectAuthorization
                                 , RedGate.SQLCompare.Engine.Options.IgnoreUserProperties
                                 , RedGate.SQLCompare.Engine.Options.IgnoreKeys
                                 , RedGate.SQLCompare.Engine.Options.IgnoreChecks
                                 , RedGate.SQLCompare.Engine.Options.IgnoreTriggers
                                 , RedGate.SQLCompare.Engine.Options.IgnoreInsteadOfTriggers
                                 , RedGate.SQLCompare.Engine.Options.IgnoreConstraintNames
                                 , RedGate.SQLCompare.Engine.Options.NoSQLPlumbing),
                AutoSelectOnly = false,
                WellDefinedRulesOnly = false,
                DebugMode = false,
                RunningInHubMode = false
            };

            NonTableObjectsSyncOnlyDefaultParameters = new DbSyncParameters
            {
                BackupDatabasesOnly = false,
                DataSync = false,
                SchemaSync = true,
                RunScript = RunScriptOption.OneWay,
                ExecuteDbSyncScriptAutomatically = true,
                TablesToInclude = null,
                ObjectTypesToInclude = new List<RedGate.SQLCompare.Engine.ObjectType>
                {
                    RedGate.SQLCompare.Engine.ObjectType.CheckConstraint,
                    RedGate.SQLCompare.Engine.ObjectType.Index,
                    RedGate.SQLCompare.Engine.ObjectType.Table,
                    RedGate.SQLCompare.Engine.ObjectType.Function,
                    RedGate.SQLCompare.Engine.ObjectType.Trigger,
                    RedGate.SQLCompare.Engine.ObjectType.StoredProcedure,
                    RedGate.SQLCompare.Engine.ObjectType.View
                },
                TablesToExclude = null,
                PerformDatabaseBackups = false,
                CompareOptions = CreateOptions(RedGate.SQLCompare.Engine.Options.ObjectExistenceChecks
                    , RedGate.SQLCompare.Engine.Options.AddDatabaseUseStatement
                    , RedGate.SQLCompare.Engine.Options.IgnoreReplicationTriggers
                    , RedGate.SQLCompare.Engine.Options.IgnoreExtendedProperties
                    , RedGate.SQLCompare.Engine.Options.ForceSyncScriptGeneration
                    , RedGate.SQLCompare.Engine.Options.IgnoreSchemaObjectAuthorization
                    , RedGate.SQLCompare.Engine.Options.NoSQLPlumbing
                    ),
                AutoSelectOnly = false,
                WellDefinedRulesOnly = false,
                DebugMode = false,
                RunningInHubMode = false
            };
        }

        public static DbSyncParameters Default { get; private set; }

        public static DbSyncParameters TableSchemaSyncOnlyDefaultParameters { get; private set; }

        public static DbSyncParameters TableDataSyncOnlyDefaultParameters { get; private set; }

        public static DbSyncParameters NonTableObjectsSyncOnlyDefaultParameters { get; private set; }


        /// <summary>
        /// The default options used if none were passed through the args parameter
        /// </summary>
        public static RedGate.SQLCompare.Engine.Options DefaultSqlCompareOptions;

        /// <summary>
        /// The server name of the first source
        /// </summary>
        [DataMember]
        public string Server1Name { get; set; }

        /// <summary>
        /// The server name of the second source
        /// </summary>
        [DataMember]
        public string Server2Name { get; set; }

        /// <summary>
        /// The database name of the first source
        /// </summary>
        [DataMember]
        public string Db1Name { get; set; }

        /// <summary>
        /// The database name of the second source
        /// </summary>
        [DataMember]
        public string Db2Name { get; set; }

        public string ProductionServer1Name { get; set; }

        public string ProductionServer2Name { get; set; }

        [DataMember]
        public string DistributionServer { get; set; }

        /// <summary>
        /// Set to true to perform comparison and synchronization of the database object data.
        /// </summary>
        [DataMember]
        public bool DataSync { get; set; }

        /// <summary>
        /// Set to true to have the engine auto select which objects to compare and sync.
        /// </summary>
        [DataMember]
        public bool AutoSelectOnly { get; set; }

        /// <summary>
        /// Set to true to ignore all other options and only use a set of pre-defined rules to perform the comparison and synchronization
        /// </summary>
        [DataMember]
        public bool WellDefinedRulesOnly { get; set; }

        /// <summary>
        /// True to specify to only use the db object names in the TablesToInclude/Exclude list.
        /// </summary>
        [DataMember]
        public bool ObjectSpecificationMode
        {
            get
            {
                if ((TablesToExclude != null && TablesToExclude.Any())
                    || (TablesToInclude != null && TablesToInclude.Any()))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use DebugMode
        /// </summary>
        [DataMember]
        public bool DebugMode { get; set; }

        /// <summary>
        /// Specify how the synchronization is rendered
        /// </summary>
        [DataMember]
        public RunScriptOption RunScript { get; set; }

        /// <summary>
        /// Set to true to backup the databases before synchronization is performed
        /// </summary>
        [DataMember]
        public bool PerformDatabaseBackups { get; set; }

        /// <summary>
        /// Set to true to only back up the databases.  Synchronization is NOT performed.
        /// </summary>
        [DataMember]
        public bool BackupDatabasesOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we are running in hub mode.
        /// </summary>
        [DataMember]
        public bool RunningInHubMode { get; set; }

        /// <summary>
        /// Gets or sets a value to indicate whether or not the DbSync utility's generated scripts get executed automatically
        /// </summary>
        /// <remarks>
        /// If <see cref="ExecuteDbSyncScriptAutomatically"/> is set to true
        /// it will execute the generated scripts automatically rather than saving them
        /// to a directory for future manual execution.
        /// </remarks>
        [DataMember]
        public bool ExecuteDbSyncScriptAutomatically { get; set; }

        /// <summary>
        /// Gets or sets a value representing a path to use to create and save
        /// the synchronizer scripts.
        /// </summary>
        /// <remarks>
        /// If a valid path is provided here,
        /// the DbSync utility will save the generated scripts in this location. If no path is
        /// provided, the scripts will be saved in the same directory as the executing process.
        /// </remarks>
        [DataMember]
        public string DbSyncGeneratedScriptPath { get; set; }

        /// <summary>
        /// Set to true to perform comparison and synchronization of the database schemas
        /// </summary>
        [DataMember]
        public bool SchemaSync { get; set; }

        /// <summary>
        /// List of tables to exclude from the synchronizer
        /// </summary>
        [DataMember]
        public List<string> TablesToExclude { get; set; }

        /// <summary>
        /// List of tables to include in the synchronizer
        /// </summary>
        [DataMember]
        public List<string> TablesToInclude { get; set; }

        /// <summary>
        /// List of object types (e.g. 'Views', 'Triggers', 'Tables') to include in the synchronizer
        /// </summary>
        [DataMember]
        public List<RedGate.SQLCompare.Engine.ObjectType> ObjectTypesToInclude { get; set; }

        /// <summary>
        /// A flags enumeration specifying how to perform the comparison and synchronization
        /// </summary>
        [DataMember]
        public RedGate.SQLCompare.Engine.Options CompareOptions { get; set; }

        /// <summary>
        /// If true, no choicing logic will ever be performed.
        /// </summary>
        /// <value>
        /// <c>true</c> if [database1 data always wins]; otherwise, <c>false</c>.
        /// </value>
        public bool Database1DataAlwaysWins { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to ignore msrepl for data comparison.
        /// </summary>
        /// <value>
        /// <c>true</c> if [disable msrepl comparison]; otherwise, <c>false</c>.
        /// </value>
        public bool IgnoreMsreplComparison { get; set; }

        public DbSyncParameters GetTableSchemaSyncOnly()
        {
            return TableSchemaSyncOnlyDefaultParameters;
        }

        public DbSyncParameters GetTableDataSyncOnly()
        {
            return TableDataSyncOnlyDefaultParameters;
        }

        public DbSyncParameters GetNonTableObjectsSyncOnly()
        {
            return NonTableObjectsSyncOnlyDefaultParameters;
        }

        public void SetSqlVersion(int majorVersion)
        {
            /*
             * Sql Server 2012 = Version 11
             * 
             * Sql Server 2008 = 10
             * 
             * Sql Server 2005 = 9
             */

            if (majorVersion < 10)
            {
                CompareOptions = CompareOptions.Plus(RedGate.SQLCompare.Engine.Options.TargetIsPre2008);
            }
        }

        public void SaveAsConfigFile(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path", "The path cannot be null or empty.  You must supply a valid path");
            }

            var content = ToFormattedConfigString();
            System.IO.File.WriteAllText(path, content);
        }

        /// <summary>
        /// Returns the values of this object represented as a configuration file that may be used as input to the DbSync tool
        /// </summary>
        /// <returns></returns>
        public string ToFormattedConfigString()
        {
            var currentProperties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.GetCustomAttributes(typeof(DataMemberAttribute), false).Any());
            var content = currentProperties.Select(p => string.Format("{0}={1}", p.Name, p.GetGetMethod().Invoke(this, null))).Aggregate((x, y) => x + Environment.NewLine + y);
            return content;
        }

        #region Available SqlCompareOptions Options

        //ForceColumnOrder
        //IgnoreFillFactor
        //IgnorePermissions
        //IgnoreWhiteSpace
        //IgnoreBindings
        //IgnoreQuotedIdentifiersAndAnsiNullSettings
        //CaseSensitiveObjectDefinition
        //IgnoreExtendedProperties
        //IgnoreFullTextIndexing
        //NoSQLPlumbing
        //IgnoreCollations
        //IgnoreComments
        //IgnoreIndexes
        //IgnoreKeys
        //IgnoreChecks
        //IgnoreTriggers
        //IncludeDependencies
        //IgnoreInsteadOfTriggers
        //IgnoreFileGroups
        //IgnoreIdentitySeedAndIncrement
        //IgnoreWithNocheck
        //IgnoreConstraintNames
        //IgnoreStatistics
        //DoNotOutputCommentHeader
        //AddWithEncryption
        //IgnoreSynonymDependencies
        //SeparateTriggers
        //IgnoreOwners
        //IgnoreQueueEventNotifications
        //TargetIsPre2005
        //UseClrUdtToStringForClrMigration
        //ConsiderNextFilegroupInPartitionSchemes
        //IgnoreCertificatesAndCryptoKeys
        //IgnoreTriggerOrder
        //IgnoreUsers
        //IgnoreUserProperties
        //DisableAndReenableDdlTriggers
        //IgnoreWithElementOrder
        //IgnoreIndexLockProperties
        //IgnoreReplicationTriggers
        //IgnoreIdentityProperties
        //IgnoreNotForReplication
        //TargetIsPre2008
        //IgnoreDataCompression
        //IgnoreDatabaseAndServerName
        //AddDatabaseUseStatement
        //IgnoreSchemaObjectAuthorization
        //DecryptPost2kEncryptedObjects
        //ForceSyncScriptGeneration
        //IgnoreStatisticsNorecompute
        //IgnoreSquareBrackets
        //ThrowOnFileParseFailed
        //EnableDeployNow
        //TargetIsSqlAzure
        //ObjectExistenceChecks
        //DropAndCreateInsteadOfAlter
        //IgnoreMigrationScripts
        //IgnoreTSQLT
        //IgnoreSystemNamedConstraintNames
        //IgnoreInternallyUsedMicrosoftExtendedProperties
        //TargetIsPre2012
        //InlineTableObjects
        //WriteAssembliesAsDlls
        //InlineFulltextFields
        //ReadAsDataToolsPermissions
        //WriteAsDataToolsPermissions
        //InferTypeForSqlVariant
        //UseSetStatementsInScriptDatabaseInfo
        //ForbidDuplicateTableStorageSettings
        //IgnoreWithEncryption
        //CloneDatabaseLevelPropertiesForMigrations
        //UseMigrationsV2
        //DisableSocForLiveDbs
        //| Options.UseClrUdtToStringForClrMigration

        #endregion Available SqlCompareOptions Options

        public static RedGate.SQLCompare.Engine.Options CreateOptions(params RedGate.SQLCompare.Engine.Options[] options)
        {
            var result = options.Aggregate((x, y) => x.Plus(y));
            return result;
        }

    }
}