﻿namespace IO.Practiceware.DbSync
{
    /// <summary>
    ///     Enum DatabaseVersionOptions
    ///     This parameter affects which auditing tables DbSync will attempt to use when making decisions. It defaults to
    ///     AutoDetect, which will
    ///     set the mode to Current if the newest auditing tables exist. Otherwise, it will set
    /// </summary>
    public enum DatabaseVersionOptions
    {
        Legacy,
        Current,
        AutoDetect
    };
}