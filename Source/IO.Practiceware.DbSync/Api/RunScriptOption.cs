namespace IO.Practiceware.DbSync
{
    public enum RunScriptOption
    {
        NoSync,
        OneWay,
        TwoWay
    };
}