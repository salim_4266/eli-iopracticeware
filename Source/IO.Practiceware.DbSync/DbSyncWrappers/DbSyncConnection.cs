using System;
using System.Data;
using System.Data.SqlClient;

namespace IO.Practiceware.DbSync
{
    public class DbSyncConnection : IDisposable
    {
        private readonly string _productionServerName;
        private readonly IDbConnection _connection;

        public DbSyncConnection(IDbConnection connection, string productionServerName)
        {
            _connection = connection;
            _productionServerName = string.IsNullOrEmpty(productionServerName) ? new SqlConnectionStringBuilder(connection.ConnectionString).DataSource : productionServerName;
        }

        public IDbConnection Connection
        {
            get { return _connection; }
        }

        public string ProductionServerName
        {
            get { return _productionServerName; }
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}