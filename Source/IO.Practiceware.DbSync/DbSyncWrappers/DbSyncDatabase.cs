using IO.Practiceware.DbSync.Extensions;
using Database = RedGate.SQLCompare.Engine.Database;
using Options = RedGate.SQLCompare.Engine.Options;
using ConnectionProperties = RedGate.SQLCompare.Engine.ConnectionProperties;
using System;
using System.Data.SqlClient;

namespace IO.Practiceware.DbSync
{
    public class DbSyncDatabase : IDisposable
    {
        private readonly string _connectionString;
        private readonly string _productionServerName;
        private readonly bool _syncSchema;
        private readonly bool _syncData;
        private readonly Options _compareOptions;
        private bool _hasRegistered;

        private Database _database;

        public DbSyncDatabase(string connectionString, string productionServerName, DbSyncParameters dbSyncParameters)
        {
            _connectionString = connectionString;
            _productionServerName = string.IsNullOrEmpty(productionServerName) ? new SqlConnectionStringBuilder(connectionString).DataSource : productionServerName;
            _syncData = dbSyncParameters.DataSync;
            _syncSchema = dbSyncParameters.SchemaSync;
            _compareOptions = dbSyncParameters.CompareOptions;
        }

        /// <summary>
        /// Attempts to make a connection to the datasource and retrieve information on the database.
        /// </summary>
        public void Register()
        {
            if (_hasRegistered)
            {
                throw new InvalidOperationException("This instance has already been registered.");
            }

            var db = SqlCompareHelper.CreateDatabase();
            var builder = new SqlConnectionStringBuilder(ConnectionString);

            var connectionProperties = !String.IsNullOrEmpty(builder.UserID) ? new ConnectionProperties(builder.DataSource, builder.InitialCatalog, builder.UserID, builder.Password) : new ConnectionProperties(builder.DataSource, builder.InitialCatalog);
            connectionProperties.ServerName = builder.DataSource;
            connectionProperties.DatabaseName = builder.InitialCatalog;
            connectionProperties.Password = builder.Password;
            connectionProperties.UserName = builder.UserID;
            connectionProperties.IntegratedSecurity = builder.IntegratedSecurity;

            try
            {
                if (_syncSchema)
                {
                    RetryExtensions.ExecuteWithRetry(() => db.Register(connectionProperties, _compareOptions), 2);
                }

                if (_syncData)
                {
                    RetryExtensions.ExecuteWithRetry(() => db.RegisterForDataCompare(connectionProperties, Options.Default.Plus(Options.CaseSensitiveObjectDefinition)), 2);
                }

                _database = db;
                _hasRegistered = true;
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("Could not connect to {0}.", ConnectionString), ex);
            }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        public string ProductionServerName
        {
            get { return _productionServerName; }
        }

        public Database Database
        {
            get
            {
                if (!_hasRegistered)
                {
                    Register();
                }

                return _database;
            }
        }

        public void Dispose()
        {
            if (_database != null)
                _database.Dispose();
        }
    }
}