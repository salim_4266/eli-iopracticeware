using System.Data.SqlClient;

namespace IO.Practiceware.DbSync
{
    public class DbSyncConnectionProperties
    {
        public DbSyncConnectionProperties(string connectionString, string productionServerName)
        {
            ConnectionString = connectionString;
            ProductionServerName = productionServerName;

            var builder = new SqlConnectionStringBuilder(connectionString);
            ServerName = builder.DataSource;
            DatabaseName = builder.InitialCatalog;

            ProductionServerName = string.IsNullOrEmpty(productionServerName) ? ServerName : productionServerName;
        }

        public string ConnectionString { get; private set; }

        public string ServerName { get; private set; }

        public string DatabaseName { get; private set; }

        public string ProductionServerName { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}.{1}{2}", ServerName, DatabaseName, ProductionServerName == ServerName ? string.Empty : string.Format(" (production server {0})", ProductionServerName));
        }
    }
}