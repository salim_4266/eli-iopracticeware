﻿#pragma checksum "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "F21F82864EB36B4BC08EE4ED259BACC950D18BA16DF13EF8E7A35F0A73F7BB2B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IO.Practiceware.Presentation.Views.Common.Behaviors;
using Microsoft.Expression.Interactivity.Core;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder {
    
    
    /// <summary>
    /// LanguageFinderView
    /// </summary>
    public partial class LanguageFinderView : Soaf.Presentation.View, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 16 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder.LanguageFinderView RootView;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox search;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SelectedGridItems;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbAreas;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock selectedItems;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgProducts;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IO.Practiceware.Presentation;component/views/patientinfo/patientdemographics/lan" +
                    "guagefinder/languagefinderview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.RootView = ((IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder.LanguageFinderView)(target));
            return;
            case 2:
            this.search = ((System.Windows.Controls.TextBox)(target));
            
            #line 56 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            this.search.KeyUp += new System.Windows.Input.KeyEventHandler(this.search_KeyUp);
            
            #line default
            #line hidden
            return;
            case 3:
            this.SelectedGridItems = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.cmbAreas = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            
            #line 63 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.selectedItems = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.dgProducts = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 9:
            
            #line 94 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Save);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 95 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Cancel);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 8:
            
            #line 76 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            ((System.Windows.Controls.RadioButton)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.chkLanguage_Unchecked);
            
            #line default
            #line hidden
            
            #line 76 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\LanguageFinder\LanguageFinderView.xaml"
            ((System.Windows.Controls.RadioButton)(target)).Checked += new System.Windows.RoutedEventHandler(this.chkLanguage_Checked);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

