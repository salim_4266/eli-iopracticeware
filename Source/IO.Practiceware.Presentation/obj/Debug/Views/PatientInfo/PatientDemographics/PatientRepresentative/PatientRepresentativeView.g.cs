﻿#pragma checksum "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "BBDE9D4691ABA0D575D4EC2C3AAA942F11EA4C3FC7684AEF19B3840A18B8375B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.PatientRepresentative;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Common.Converters;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative;
using Microsoft.Expression.Interactivity.Core;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative {
    
    
    /// <summary>
    /// PatientRepresentativeView
    /// </summary>
    public partial class PatientRepresentativeView : Soaf.Presentation.View, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative.PatientRepresentativeView RootView;
        
        #line default
        #line hidden
        
        
        #line 297 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox Title;
        
        #line default
        #line hidden
        
        
        #line 303 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox FirstName;
        
        #line default
        #line hidden
        
        
        #line 311 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox MiddleName;
        
        #line default
        #line hidden
        
        
        #line 318 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox LastName;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox AddressLine1;
        
        #line default
        #line hidden
        
        
        #line 331 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox AddressLine2;
        
        #line default
        #line hidden
        
        
        #line 334 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox City;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox StateComboBox;
        
        #line default
        #line hidden
        
        
        #line 359 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadMaskedTextInput MaskedZip;
        
        #line default
        #line hidden
        
        
        #line 369 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox ZipCodes;
        
        #line default
        #line hidden
        
        
        #line 399 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox CountryComboBox;
        
        #line default
        #line hidden
        
        
        #line 423 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox HomePhone;
        
        #line default
        #line hidden
        
        
        #line 443 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox WorkPhone;
        
        #line default
        #line hidden
        
        
        #line 455 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox CellPhone;
        
        #line default
        #line hidden
        
        
        #line 469 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox Email;
        
        #line default
        #line hidden
        
        
        #line 482 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox RelationshipComboBox;
        
        #line default
        #line hidden
        
        
        #line 512 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDatePicker BirthDate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IO.Practiceware.Presentation;component/views/patientinfo/patientdemographics/pat" +
                    "ientrepresentative/patientrepresentativeview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\Views\PatientInfo\PatientDemographics\PatientRepresentative\PatientRepresentativeView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.RootView = ((IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative.PatientRepresentativeView)(target));
            return;
            case 2:
            this.Title = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 3:
            this.FirstName = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 4:
            this.MiddleName = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 5:
            this.LastName = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 6:
            this.AddressLine1 = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 7:
            this.AddressLine2 = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 8:
            this.City = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 9:
            this.StateComboBox = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 10:
            this.MaskedZip = ((Telerik.Windows.Controls.RadMaskedTextInput)(target));
            return;
            case 11:
            this.ZipCodes = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 12:
            this.CountryComboBox = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 13:
            this.HomePhone = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 14:
            this.WorkPhone = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 15:
            this.CellPhone = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 16:
            this.Email = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 17:
            this.RelationshipComboBox = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 18:
            this.BirthDate = ((Telerik.Windows.Controls.RadDatePicker)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

