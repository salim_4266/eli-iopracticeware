﻿#pragma checksum "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "938E8061FD5E182A830F3842F4529E563AA6A61A00F0C32E8FE764CFF9A52EF1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Common.Converters;
using IO.Practiceware.Presentation.Views.Financials.PostAdjustments;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments {
    
    
    /// <summary>
    /// PostAdjustmentsView
    /// </summary>
    public partial class PostAdjustmentsView : Soaf.Presentation.View, System.Windows.Markup.IComponentConnector {
        
        
        #line 39 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 531 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid MainGrid;
        
        #line default
        #line hidden
        
        
        #line 535 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadGridView AccountsGridView;
        
        #line default
        #line hidden
        
        
        #line 1064 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid NextPatientSearchInterface;
        
        #line default
        #line hidden
        
        
        #line 1072 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Run NextPatientText;
        
        #line default
        #line hidden
        
        
        #line 1074 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox NextPatientSearchBox;
        
        #line default
        #line hidden
        
        
        #line 1087 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LoadingInterface;
        
        #line default
        #line hidden
        
        
        #line 1122 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PaymentDetails;
        
        #line default
        #line hidden
        
        
        #line 1169 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox EditPaymentType;
        
        #line default
        #line hidden
        
        
        #line 1197 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal IO.Practiceware.Presentation.Views.Common.Controls.RadAutoCompleteBoxWithClearItemButtonUserControl InsurerPayerAutoCompleteBox;
        
        #line default
        #line hidden
        
        
        #line 1221 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal IO.Practiceware.Presentation.Views.Common.Controls.RadAutoCompleteBoxWithClearItemButtonUserControl EditPatientOrContactPayer;
        
        #line default
        #line hidden
        
        
        #line 1231 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton InsurerRadioButton;
        
        #line default
        #line hidden
        
        
        #line 1232 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton OfficeRadioButton;
        
        #line default
        #line hidden
        
        
        #line 1233 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton PatientOrContactRadioButton;
        
        #line default
        #line hidden
        
        
        #line 1242 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid EditAmount;
        
        #line default
        #line hidden
        
        
        #line 1243 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox AmountTextBox;
        
        #line default
        #line hidden
        
        
        #line 1253 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDatePicker EditPaymentDate;
        
        #line default
        #line hidden
        
        
        #line 1262 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadWatermarkTextBox EditReferenceNumber;
        
        #line default
        #line hidden
        
        
        #line 1270 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDatePicker EditReferenceDate;
        
        #line default
        #line hidden
        
        
        #line 1292 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup EditPaymentInstrumentGrid;
        
        #line default
        #line hidden
        
        
        #line 1302 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadToggleButton ToggleEditModeButton;
        
        #line default
        #line hidden
        
        
        #line 1326 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid UpdatePaymentInstrumentInterface;
        
        #line default
        #line hidden
        
        
        #line 1336 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CancelAndSavePaymentInstrumentStackPanel;
        
        #line default
        #line hidden
        
        
        #line 1339 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelPaymentInstrumentChangesButton;
        
        #line default
        #line hidden
        
        
        #line 1348 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SavePaymentInstrumentChangesButton;
        
        #line default
        #line hidden
        
        
        #line 1354 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SavePaymentInstrumentAndBeginPostingButton;
        
        #line default
        #line hidden
        
        
        #line 1389 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox InvoiceFilterComboBox;
        
        #line default
        #line hidden
        
        
        #line 1406 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PostAndNextButton;
        
        #line default
        #line hidden
        
        
        #line 1416 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PostAndDoneButton;
        
        #line default
        #line hidden
        
        
        #line 1423 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PostButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IO.Practiceware.Presentation;component/views/financials/postadjustments/postadju" +
                    "stmentsview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\Financials\PostAdjustments\PostAdjustmentsView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.MainGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.AccountsGridView = ((Telerik.Windows.Controls.RadGridView)(target));
            return;
            case 4:
            this.NextPatientSearchInterface = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.NextPatientText = ((System.Windows.Documents.Run)(target));
            return;
            case 6:
            this.NextPatientSearchBox = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 7:
            this.LoadingInterface = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.PaymentDetails = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.EditPaymentType = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 10:
            this.InsurerPayerAutoCompleteBox = ((IO.Practiceware.Presentation.Views.Common.Controls.RadAutoCompleteBoxWithClearItemButtonUserControl)(target));
            return;
            case 11:
            this.EditPatientOrContactPayer = ((IO.Practiceware.Presentation.Views.Common.Controls.RadAutoCompleteBoxWithClearItemButtonUserControl)(target));
            return;
            case 12:
            this.InsurerRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 13:
            this.OfficeRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 14:
            this.PatientOrContactRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 15:
            this.EditAmount = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.AmountTextBox = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 17:
            this.EditPaymentDate = ((Telerik.Windows.Controls.RadDatePicker)(target));
            return;
            case 18:
            this.EditReferenceNumber = ((Telerik.Windows.Controls.RadWatermarkTextBox)(target));
            return;
            case 19:
            this.EditReferenceDate = ((Telerik.Windows.Controls.RadDatePicker)(target));
            return;
            case 20:
            this.EditPaymentInstrumentGrid = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 21:
            this.ToggleEditModeButton = ((Telerik.Windows.Controls.RadToggleButton)(target));
            return;
            case 22:
            this.UpdatePaymentInstrumentInterface = ((System.Windows.Controls.Grid)(target));
            return;
            case 23:
            this.CancelAndSavePaymentInstrumentStackPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 24:
            this.CancelPaymentInstrumentChangesButton = ((System.Windows.Controls.Button)(target));
            return;
            case 25:
            this.SavePaymentInstrumentChangesButton = ((System.Windows.Controls.Button)(target));
            return;
            case 26:
            this.SavePaymentInstrumentAndBeginPostingButton = ((System.Windows.Controls.Button)(target));
            return;
            case 27:
            this.InvoiceFilterComboBox = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 28:
            this.PostAndNextButton = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.PostAndDoneButton = ((System.Windows.Controls.Button)(target));
            return;
            case 30:
            this.PostButton = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

