﻿#pragma checksum "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "7E6DC741062A6A4B6134B89F6133876ADE69F3AAB3A153F432E963AE451D4944"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Presentation.Views.Common.Converters;
using IO.Practiceware.Presentation.Views.GlaucomaMonitor;
using Microsoft.Expression.Controls;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using Microsoft.Expression.Media;
using Microsoft.Expression.Shapes;
using Microsoft.Windows.Themes;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.ChartView;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Examples.ScheduleView.CustomAppointment;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;


namespace IO.Practiceware.Presentation.Views.GlaucomaMonitor {
    
    
    /// <summary>
    /// GlaucomaMonitorView
    /// </summary>
    public partial class GlaucomaMonitorView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal IO.Practiceware.Presentation.Views.GlaucomaMonitor.GlaucomaMonitorView UserControl;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadCartesianChart chart;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid DateExtrasGrid;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid DateExtras1;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Shapes.RegularPolygon Medications1;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel DateExtras2;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Shapes.RegularPolygon Medications2;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel DateExtras3;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Shapes.RegularPolygon Medications3;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel DateExtras4;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Shapes.RegularPolygon Medications4;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border HighlightsPanel;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChartIOP;
        
        #line default
        #line hidden
        
        
        #line 263 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChartVSC;
        
        #line default
        #line hidden
        
        
        #line 264 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChartVCC;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChartBVA;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChartCD;
        
        #line default
        #line hidden
        
        
        #line 323 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadToggleButton ViewToggleButton;
        
        #line default
        #line hidden
        
        
        #line 324 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border DemoNoticeOverlay;
        
        #line default
        #line hidden
        
        
        #line 347 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NoticeButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IO.Practiceware.Presentation;component/views/glaucomamonitor/glaucomamonitorview" +
                    ".xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\GlaucomaMonitor\GlaucomaMonitorView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.UserControl = ((IO.Practiceware.Presentation.Views.GlaucomaMonitor.GlaucomaMonitorView)(target));
            return;
            case 2:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.chart = ((Telerik.Windows.Controls.RadCartesianChart)(target));
            return;
            case 4:
            this.DateExtrasGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.DateExtras1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.Medications1 = ((Microsoft.Expression.Shapes.RegularPolygon)(target));
            return;
            case 7:
            this.DateExtras2 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.Medications2 = ((Microsoft.Expression.Shapes.RegularPolygon)(target));
            return;
            case 9:
            this.DateExtras3 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.Medications3 = ((Microsoft.Expression.Shapes.RegularPolygon)(target));
            return;
            case 11:
            this.DateExtras4 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 12:
            this.Medications4 = ((Microsoft.Expression.Shapes.RegularPolygon)(target));
            return;
            case 13:
            this.HighlightsPanel = ((System.Windows.Controls.Border)(target));
            return;
            case 14:
            this.ChartIOP = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.ChartVSC = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 16:
            this.ChartVCC = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.ChartBVA = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.ChartCD = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 19:
            this.ViewToggleButton = ((Telerik.Windows.Controls.RadToggleButton)(target));
            return;
            case 20:
            this.DemoNoticeOverlay = ((System.Windows.Controls.Border)(target));
            return;
            case 21:
            this.NoticeButton = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

