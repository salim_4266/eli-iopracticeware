﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.ViewServices.RescheduleAppointment;
using IO.Practiceware.Presentation.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AppointmentViewModel = IO.Practiceware.Presentation.ViewModels.RescheduleAppointment.AppointmentViewModel;

[assembly: Component(typeof(RescheduleAppointmentViewService), typeof(IRescheduleAppointmentViewService))]
[assembly: Component(typeof(AppointmentViewModelMap), typeof(IMap<UserAppointment, AppointmentViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.RescheduleAppointment
{
    [SupportsUnitOfWork]
    public class RescheduleAppointmentViewService : BaseViewService, IRescheduleAppointmentViewService
    {
        private readonly IMapper<List<UserAppointment>, List<AppointmentViewModel>> _appointmentMapper;        

        public RescheduleAppointmentViewService(IMapper<List<UserAppointment>, List<AppointmentViewModel>> appointmentMapper)
        {
            
            _appointmentMapper = appointmentMapper;
        }

        #region IRescheduleAppointmentViewService Members

        public List<AppointmentViewModel> LoadAppointments(IEnumerable<int> appointmentIds)
        {
            List<UserAppointment> appointments = PracticeRepository.Appointments.OfType<UserAppointment>().Include(x => x.Encounter.Patient).Where(x => appointmentIds.Contains(x.Id)).ToList();

            return _appointmentMapper.Map(appointments);
        }

        public List<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons()
        {
            return PracticeRepository.EncounterStatusChangeReasons.Select(r => new EncounterStatusChangeReasonViewModel { Id = r.Id, Value = r.Value, EncounterStatusId = r.EncounterStatusId }).ToList();
        }

        public ObservableCollection<NamedViewModel> LoadAppointmentRescheduleTypes()
        {
            return Enums.GetValues<EncounterStatus>().Where(i => i.IsPendingStatus()).Select(i => new NamedViewModel((int)i, i.GetDisplayName())).ToExtendedObservableCollection();
        }



        public virtual void RescheduleAppointments(List<int> appointmentIds, RescheduleAppointmentViewModel selectedScheduleBlock, int appointmentRescheduleTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment)
        {
            foreach (UserAppointment appointment in PracticeRepository.Appointments.OfType<UserAppointment>().Include(a => a.Encounter).Include(a => a.ScheduleBlockAppointmentCategories).Where(a => appointmentIds.Contains(a.Id)).ToArray())
            {
                appointment.Encounter.EncounterStatusChangeComment = comment;
                if (changeReason != null) appointment.Encounter.EncounterStatusChangeReasonId = changeReason.Id;
                PracticeRepository.Save(appointment.Encounter);

                var categories = appointment.ScheduleBlockAppointmentCategories.ToList();
                categories.ForEach(c => c.AppointmentId = null);
                categories.ForEach(PracticeRepository.Save);

                var requeriedAppointment = PracticeRepository.Appointments.OfType<UserAppointment>().Include(a => a.Encounter).Include(a => a.ScheduleBlockAppointmentCategories).WithId(appointment.Id);

                var categoryId = selectedScheduleBlock.ScheduleBlockCategoryId;
                if (categoryId.HasValue && categoryId.Value != Guid.Empty)
                {
                     ScheduleBlockAppointmentCategory newScheduleBlockApptCategory = PracticeRepository.ScheduleBlockAppointmentCategories.Single(x => x.Id == selectedScheduleBlock.ScheduleBlockCategoryId);
                    requeriedAppointment.ScheduleBlockAppointmentCategories.Add(newScheduleBlockApptCategory);
                }

                if (requeriedAppointment.Encounter.EncounterStatus.IsPendingStatus())
                {
                    requeriedAppointment.Encounter.EncounterStatus = (EncounterStatus)appointmentRescheduleTypeId;
                }
                
                requeriedAppointment.Encounter.ServiceLocationId = selectedScheduleBlock.LocationId;
                requeriedAppointment.Encounter.StartDateTime = selectedScheduleBlock.StartDateTime;
                requeriedAppointment.DateTime = selectedScheduleBlock.StartDateTime;
                requeriedAppointment.UserId = selectedScheduleBlock.ResourceId;
                if (selectedScheduleBlock.AppointmentTypeId.HasValue)
                {
                    requeriedAppointment.AppointmentTypeId = selectedScheduleBlock.AppointmentTypeId.Value;
                }

                PracticeRepository.Save(requeriedAppointment);

                try
                {
                    var isPortalActive = PracticeRepository.ApplicationSettings.Where(a => a.Name == "IsPortalActive").SingleOrDefault().Value.ToString();
                    if (isPortalActive.ToLower() == "true")
                    {
                        var prtSrv = new PortalServices();
                        AppointmentEntity objTemp = new AppointmentEntity()
                        {
                            PracticeToken = PracticeRepository.ApplicationSettings.Where(a => a.Name == "PracticeAuthToken").SingleOrDefault().Value.ToString(),
                            PatientExternalId = requeriedAppointment.Encounter.PatientId.ToString(),
                            LocationExternalId = requeriedAppointment.Encounter.ServiceLocationId.ToString(),
                            DoctorExternalId = requeriedAppointment.ResourceId.ToString(),
                            AppointmentStatusExternalId = requeriedAppointment.Encounter.EncounterStatusId.ToString(),
                            Start = requeriedAppointment.Encounter.StartDateTime,
                            End = requeriedAppointment.Encounter.EndDateTime,
                            ExternalId = requeriedAppointment.Id.ToString()
                        };
                        prtSrv.UpdateAppointment(objTemp);
                    }
                }
                catch { }
            }
        }

        #endregion
    }

    public interface IRescheduleAppointmentViewService
    {
        /// <summary>
        ///   Loads the appointment detail related to the reschedule appointment view.
        /// </summary>
        /// <param name="appointmentIds"> </param>
        /// <returns> </returns>
        List<AppointmentViewModel> LoadAppointments(IEnumerable<int> appointmentIds);

        /// <summary>
        ///   Loads the change reasons
        /// </summary>
        /// <returns> </returns>
        List<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons();

        /// <summary>
        ///   Loads the appointment cancellation types.
        /// </summary>
        /// <returns> </returns>
        ObservableCollection<NamedViewModel> LoadAppointmentRescheduleTypes();

        /// <summary>
        ///   Sets the status of the appointments to 'Pending' and records a new ChangeHistory for each
        /// </summary>
        /// <param name="appointmentIds"> </param>
        /// <param name="selectedScheduleBlock"> </param>
        /// <param name="appointmentRescheduleTypeId"> </param>
        /// <param name="changeReason"> </param>
        /// <param name="comment"> </param>
        void RescheduleAppointments(List<int> appointmentIds, RescheduleAppointmentViewModel selectedScheduleBlock, int appointmentRescheduleTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment);
    }

    public class AppointmentViewModelMap : IMap<UserAppointment, AppointmentViewModel>
    {
        private readonly IEnumerable<IMapMember<UserAppointment, AppointmentViewModel>> _members;

        public AppointmentViewModelMap()
        {
            _members = this.CreateMembers(appointment => new AppointmentViewModel
                                                             {
                                                                 Id = appointment.Id,
                                                                 PatientName = appointment.Encounter.Patient.DisplayName,
                                                                 StartDateTime = appointment.DateTime,
                                                             }).ToArray();
        }

        #region IMap<UserAppointment,RescheduleAppointmentViewModel> Members

        public IEnumerable<IMapMember<UserAppointment, AppointmentViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}