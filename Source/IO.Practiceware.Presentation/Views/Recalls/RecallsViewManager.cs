﻿using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Recalls
{
    public class RecallsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public RecallsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public RecallsReturnArguments ShowRecalls(RecallLoadArguments loadArguments)
        {
            if (loadArguments.PatientId <= 0)
            {
                throw new ArgumentException("Load Arguments must contain a valid patient id");
            }

            var view = new RecallsView();
            var dataContext = view.DataContext.EnsureType<RecallsViewContext>();

            dataContext.LoadArguments = loadArguments;
            dataContext.ShowPendingRecallsOnly = loadArguments.ShowPendingRecallsOnly;
            dataContext.ShowRecallListOnly = loadArguments.ShowRecallListOnly;

            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = "Patient Recalls" });

            var returnArguments = new RecallsReturnArguments();
            if (dataContext.RecallIds.IsNotNullOrEmpty()) returnArguments.RecallIds = dataContext.RecallIds;
            return returnArguments;
        }
    }

    public class RecallLoadArguments
    {
        public int PatientId { get; set; }
        public int? AppointmentType { get; set; }
        public int? LocationId { get; set; }
        public int? ResourceId { get; set; }
        public DateTime? RecallDate { get; set; }

        /// <summary>
        /// Set to true to hide the 'Create Recall' form and only show the Recall Listing
        /// </summary>
        public bool ShowRecallListOnly { get; set; }

        /// <summary>
        /// Set to true to show only pending recalls in the recalls listing
        /// </summary>
        public bool ShowPendingRecallsOnly { get; set; }
    }

    public class RecallsReturnArguments
    {
        public IEnumerable<int> RecallIds { get; set; }
    }
}
