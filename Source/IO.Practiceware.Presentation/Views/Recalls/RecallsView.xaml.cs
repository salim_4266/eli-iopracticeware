﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Recalls;
using IO.Practiceware.Presentation.ViewServices.Recalls;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Recalls
{
    /// <summary>
    /// Interaction logic for Recalls.xaml
    /// </summary>
    public partial class RecallsView
    {
        public RecallsView()
        {
            InitializeComponent();
        }
    }

    public class RecallsViewContext : IViewContext
    {

        private readonly IRecallsViewService _recallsViewService;
        private readonly Func<RecallDetailViewModel> _createRecallDetailViewModel;
        private ObservableCollection<RecallDisplayViewModel> _patientRecalls;

        public RecallsViewContext(IRecallsViewService referralsViewService, Func<RecallDetailViewModel> createRecallDetailViewModel)
        {
            _recallsViewService = referralsViewService;
            _createRecallDetailViewModel = createRecallDetailViewModel;

            Cancel = Command.Create(ExecuteCancel);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Create = Command.Create(ExecuteCreate).Async(() => InteractionContext);

            CloseSelected = Command.Create<ObservableCollection<object>>(ExecuteCloseSelected, CloseSelectedCanExecute).Async(() => InteractionContext);
            CloseAll = Command.Create(ExecuteCloseAll).Async(() => InteractionContext);

            RecallIds = new List<int>();
        }

        private static bool CloseSelectedCanExecute(ObservableCollection<object> arg)
        {
            return arg.IsNotNullOrEmpty();
        }

        private void ExecuteCloseAll()
        {
            CloseRecalls(FilteredPatientRecalls.Where(x => x.RecallStatus == RecallStatus.Pending));
            if (ShowRecallListOnly)
            {
                InteractionContext.Complete(false);
            }
        }

        private void ExecuteCloseSelected(ObservableCollection<object> selectedItems)
        {
            CloseRecalls(selectedItems.Select(x => (RecallDisplayViewModel)x));
        }

        private void CloseRecalls(IEnumerable<RecallDisplayViewModel> selectedItems)
        {
            var localSelectedItems = selectedItems.ToArray();
            if (localSelectedItems.Any(x => x.RecallStatus == RecallStatus.Sent || x.RecallStatus == RecallStatus.Closed))
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionManager.Current.Alert("You may only close pending items."));
                return;
            }

            // close all selected items   
            _recallsViewService.CloseRecalls(localSelectedItems.Select(x => x.RecallId));
            ReloadGrid();
        }

        private void ExecuteCreate()
        {
            if (SaveData())
            {
                if (RecallIds.IsNotNullOrEmpty())
                {
                    ReloadGrid();
                    PatientRecallDetail = _createRecallDetailViewModel();
                    PatientRecallDetail.PatientId = PatientId;
                    PatientRecallDetail.RecallDate = DateTime.Now.ToClientTime();
                }
            }
        }

        private void ReloadGrid()
        {
            var newHistoryList = _recallsViewService.LoadRecallHistory(PatientRecallDetail.PatientId);
            FilteredPatientRecalls = new ObservableCollection<RecallDisplayViewModel>(newHistoryList);
        }

        private bool SaveData()
        {
            ErrorMessage = null;

            var errorMessages = PatientRecallDetail.Validate();

            ErrorMessage = errorMessages.Join(x => x.Value, Environment.NewLine);  

            if (string.IsNullOrEmpty(ErrorMessage))
            {
                var recallsToSave = ExtrapolateRecalls(PatientRecallDetail).ToArray();
                var recallIds = _recallsViewService.Save(recallsToSave).ToArray();
                for (int i = 0; i < recallIds.Length; i++)
                {
                    recallsToSave[i].RecallId = recallIds[i];
                }

                var idsToAdd = recallIds.Where(i => !RecallIds.Contains(i));
                RecallIds.AddRangeWithSinglePropertyChangedNotification(idsToAdd);

                return true;
            }

            return false;
        }

        private IEnumerable<RecallDetailViewModel> ExtrapolateRecalls(RecallDetailViewModel recall)
        {
            var recalls = new List<RecallDetailViewModel> {recall};

            if (recall.AppointmentTypeId.HasValue && recall.RecallDate.HasValue)
            {
                var appointmentType = SelectLists.AppointmentTypes.WithId(recall.AppointmentTypeId.Value);

                // Create new recalls if max recalls is greater then 1
                for (var i = 1; i < appointmentType.MaximumRecallsPerEncounter; i++)
                {
                    var newRecall = _createRecallDetailViewModel();
                    newRecall.AppointmentTypeId = recall.AppointmentTypeId;
                    newRecall.LocationId = recall.LocationId;
                    newRecall.PatientId = recall.PatientId;
                    newRecall.RecallId = recall.RecallId;
                    newRecall.ResourceId = recall.ResourceId;

                    var monthCount = appointmentType.FrequencyBetweenRecallsPerEncounter * i;
                    newRecall.RecallDate = recall.RecallDate.Value.AddMonths(monthCount);

                    recalls.Add(newRecall);
                }
            }
            return recalls;
        }

        private void ExecuteLoad()
        {
            if (LoadArguments.PatientId > 0)
            {
                PatientId = LoadArguments.PatientId;

                var selectLists = _recallsViewService.GetRecallSelectLists();
                SelectLists = selectLists;


                var patientInfo = _recallsViewService.LoadPatientInfo(PatientId);

                var patientRecallDetail = _createRecallDetailViewModel();
                patientRecallDetail.PatientId = PatientId;
                patientRecallDetail.AppointmentTypeId = LoadArguments.AppointmentType.HasValue ? LoadArguments.AppointmentType.Value : new int?();
                patientRecallDetail.ResourceId = LoadArguments.ResourceId.HasValue ? LoadArguments.ResourceId.Value : patientInfo.DefaultResourceId; // default to patient's defined practice doctor
                patientRecallDetail.RecallDate = LoadArguments.RecallDate.HasValue ? LoadArguments.RecallDate.Value : DateTime.Now.ToClientTime();

                // Note: location is not in use just yet.  Leave this commented out until then.
                //if (!PatientRecallDetail.LocationId.HasValue)
                //{
                //    // default to office location of workstation being used

                //}

                var recallList = _recallsViewService.LoadRecallHistory(PatientId);
                FilteredPatientRecalls = new ObservableCollection<RecallDisplayViewModel>(recallList);
                
                IsClosedChecked = false;
                IsPendingChecked = true;
                IsSentChecked = false;

                PatientInfo = patientInfo;
                PatientRecallDetail = patientRecallDetail;
            }
            else
            {
                throw new ArgumentException("No valid PatientId found");
            }
        }

        private void ExecuteCancel()
        { 
            InteractionContext.Complete(false);
        }      

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        /// <summary>
        /// Gets or sets the Patient Id
        /// </summary>
        [DispatcherThread]
        public virtual int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the Recall Ids
        /// </summary>
        public IList<int> RecallIds { get; set; }

        public RecallLoadArguments LoadArguments { get; set; }

        /// <summary>
        /// Set to true to hide the 'Create Recall' form
        /// </summary>
        [DispatcherThread]
        public virtual bool ShowRecallListOnly { get; set; }

        /// <summary>
        /// Set to true to show only pending recalls
        /// </summary>
        [DispatcherThread]
        public virtual bool ShowPendingRecallsOnly { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to save the data
        /// </summary>
        public ICommand Create { get; protected set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Cancel { get; protected set; }

        /// <summary>
        /// Command to mark the selected recall items in the list as being closed
        /// </summary>
        public ICommand CloseSelected { get; protected set; }

        /// <summary>
        /// Command to mark all recall items in the list as being closed
        /// </summary>
        public ICommand CloseAll { get; protected set; }       

        /// <summary>
        /// Determines whether or not the form is valid
        /// </summary>
        [DispatcherThread]
        [DependsOn("ErrorMessage")]
        public virtual bool HasError { get { return !ErrorMessage.IsNullOrEmpty(); } }

        /// <summary>
        /// The error message if validation fails
        /// </summary>
        [DispatcherThread]
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        /// A list of Referrals to be displayed in a grid
        /// </summary>
        [DispatcherThread, DependsOn("IsPendingChecked", "IsSentChecked", "IsClosedChecked")]
        public virtual ObservableCollection<RecallDisplayViewModel> FilteredPatientRecalls
        {
            get
            {
                if (_patientRecalls.IsNullOrEmpty())
                {
                    return null;
                }

                var returnList = new ObservableCollection<RecallDisplayViewModel>();

                if (IsPendingChecked)
                {
                    returnList.AddRangeWithSinglePropertyChangedNotification(_patientRecalls.Where(x => x.RecallStatus == RecallStatus.Pending).OrderByDescending(x => Convert.ToDateTime(x.DueDate)));
                }

                if (!ShowPendingRecallsOnly)
                {
                    if (IsSentChecked)
                    {
                        returnList.AddRangeWithSinglePropertyChangedNotification(_patientRecalls.Where(x => x.RecallStatus == RecallStatus.Sent).OrderByDescending(x => Convert.ToDateTime(x.DueDate)));
                    }

                    if (IsClosedChecked)
                    {
                        returnList.AddRangeWithSinglePropertyChangedNotification(_patientRecalls.Where(x => x.RecallStatus == RecallStatus.Closed).OrderByDescending(x => Convert.ToDateTime(x.DueDate)));
                    }
                }

                return returnList;
            }
            set { _patientRecalls = value; }
        }

        [DependsOn("FilteredPatientRecalls")]
        [DispatcherThread]
        public virtual bool PatientHasRecalls
        {
            get { return  _patientRecalls.IsNotNullOrEmpty(); }
        }

        [DispatcherThread]
        public virtual RecallPatientInfo PatientInfo { get; set; }

        /// <summary>
        /// The current recall being editing/created
        /// </summary>
        [DispatcherThread]
        public virtual RecallDetailViewModel PatientRecallDetail { get; set; }

        /// <summary>
        /// Contains the collections for binding to the lists (e.g. dropdowns, listboxes)
        /// </summary>
        [DispatcherThread]
        public virtual RecallSelectListsViewModel SelectLists { get; set; }

        [DispatcherThread]
        public virtual bool IsPendingChecked { get; set; }

        [DispatcherThread]
        public virtual bool IsSentChecked { get; set; }

        [DispatcherThread]
        public virtual bool IsClosedChecked { get; set; }

        [DispatcherThread]
        [DependsOn("ShowRecallHistoryOnly")]
        public virtual Visibility CreateRecallFormVisibility
        {
            get
            {
                if (ShowRecallListOnly)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }
    }
}
