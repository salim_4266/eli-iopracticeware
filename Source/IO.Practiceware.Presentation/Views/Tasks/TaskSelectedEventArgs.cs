﻿using IO.Practiceware.Model;
using System;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public class TaskSelectedEventArgs : EventArgs
    {
        private readonly string _screenToOpen;
        private readonly Task _task;

        public TaskSelectedEventArgs(Task task, string screenToOpen)
        {
            _task = task;
            _screenToOpen = screenToOpen;
        }

        public Task Task
        {
            get { return _task; }
        }

        public string ScreenToOpen
        {
            get { return _screenToOpen; }
        }
    }
}