﻿using IO.Practiceware.Application;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using Soaf.Data;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml;
using Point = System.Drawing.Point;
using Screen = System.Windows.Forms.Screen;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class TaskIconView
    {
// ReSharper disable InconsistentNaming
        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        [DllImport("user32.dll")]
        static extern IntPtr SetFocus(IntPtr hWnd);
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
// ReSharper restore InconsistentNaming

        public static void MakeTopMost(Form form)
        {
            SetFocus(form.Handle);
            SetWindowPos(form.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }


        public static void MakeNormal(Form form)
        {
            SetWindowPos(form.Handle, HWND_NOTOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }

        // ReSharper disable InconsistentNaming
        private const int WM_MOUSEACTIVATE = 0x21;
        private const int MA_NOACTIVATE = 0x3;
        // ReSharper restore InconsistentNaming

        private bool _isDragging;
        private Point _startDragPosition;
        DateTime CurrentTime;
        public TaskIconView()
        {
            InitializeComponent();

            PictureBox.MouseMove += OnMouseMove;
            PictureBox.MouseDown += OnMouseDown;
            PictureBox.MouseUp += OnMouseUp;
            PictureBox.MouseDoubleClick += OnDoubleClick;
            DispatcherTimer timers = new DispatcherTimer();
            PictureBox.MouseClick += OnSingleClick;
            timers.Interval = TimeSpan.FromMinutes(30);
            timers.Tick += timer_Ticks;
            timers.Start(); 
        }
        private void OnSingleClick(object sender, MouseEventArgs e)
        {
            if (PictureBox.BackColor == System.Drawing.Color.Red)
            {
                PictureBox.Image = Resources.Images.Message;
                PictureBox.BackColor = System.Drawing.Color.White;
                PictureBox.BorderStyle = BorderStyle.None;
                string responseXml = string.Empty;
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string queryErxResponseLogs = "SELECT  TOP 1 *  FROM [DrFirst].[XmlQueueResponse] WHERE ActionTypeId=9 ORDER BY RecordDateTime desc";

                    DataTable dataTable = dbConnection.Execute<DataTable>(queryErxResponseLogs);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        responseXml = dataTable.Rows[0]["XmlResponse"].ToString();
                    }

                    if (!string.IsNullOrEmpty(responseXml))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(responseXml);
                        PendingErxView view = new PendingErxView();
                        view.Notifications = dataTable;
                        view.ShowDialog();
                        //XmlNodeList xnList = doc.SelectNodes("RCExtResponse/Response/NotificationCountList/NotificationCount");
                        //StringBuilder sb = new StringBuilder();
                        //sb.AppendLine("Notification details:");
                        //sb.AppendLine("------------------------------");
                        //foreach (XmlNode xn in xnList)
                        //{
                        //    string type = xn["Type"].InnerText;
                        //    string number = xn["Number"].InnerText;
                        //    sb.Append(type + " is  ");
                        //    sb.AppendLine(number);
                        //}
                        //System.Windows.MessageBox.Show(sb.ToString());
                    }
                }
                
            }
        }
        void timer_Ticks(object sender, EventArgs e)
        {
            PictureBox.Image = Resources.Images.Message;
            CurrentTime = DateTime.UtcNow;
            string notificationXml = GetNotification();
            notificationXml = "xml=" + notificationXml;
            string responseXml = string.Empty;

            string url = "https://update201.staging.drfirst.com/servlet/rcopia.servlet.EngineServlet";

            notificationXml = notificationXml.Trim('?').Replace("&", " and ").Replace("%", " percent");
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            byte[] byteArray = Encoding.UTF8.GetBytes(notificationXml);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            //get response
            WebResponse response = request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string xml = reader.ReadToEnd();
            responseXml = xml;

            //Inserting values into Database
            LogErxActivity(CurrentTime, responseXml, 9, notificationXml, UserContext.Current.UserDetails.Id);

            PictureBox.Image = Resources.Images.Message;
            PictureBox.BackColor = System.Drawing.Color.Red;
            PictureBox.BorderStyle = BorderStyle.FixedSingle;
            
            
        }
        public void LogErxActivity(DateTime dateTime, string xmlResponse, int ActionTypeId, string XmlValue, int UserId)
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string qryQueueLogs = "INSERT INTO [DrFirst].[XmlQueue](  XmlValue,ActionTypeId,RecordDatetime,UserId) VALUES ( @XmlValue,@ActionTypeId,@RecordDatetime,@UserId);SELECT SCOPE_IDENTITY() as queueId";
                string queryErxResponseLogs = "INSERT INTO [DrFirst].[XmlQueueResponse](XmlQueueId,XmlResponse,RecordDateTime,ActionTypeId,UserId) VALUES (@XmlQueueId,@XmlResponse,@RecordDateTime,@ActionTypeId,@UserId)";

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@RecordDatetime", dateTime);
                param.Add("@ActionTypeId", ActionTypeId);
                param.Add("@XmlValue", XmlValue);
                param.Add("@UserId", UserId);

                int queueId = 0;
                DataTable dataTable = dbConnection.Execute<DataTable>(qryQueueLogs, param);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    queueId = int.Parse(dataTable.Rows[0]["queueId"].ToString());
                }
                Dictionary<string, object> paramRes = new Dictionary<string, object>();
                paramRes.Add("@XmlQueueId", queueId);
                paramRes.Add("@RecordDatetime", dateTime);
                paramRes.Add("@ActionTypeId", ActionTypeId);
                paramRes.Add("@XmlResponse", xmlResponse);
                paramRes.Add("@UserId", UserId);

                dbConnection.Execute(queryErxResponseLogs, paramRes);
            }
        }
        private string GetNotification()
        {

            DataTable dataTable = new DataTable();
            string queryErxCredentials = " SELECT * FROM [DrFirst].[PracticeConfiguration] With (nolock) WHERE IsActive=1 Order by 1 desc";
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var xmlDoc = new XmlDocument();
                var documentElement = xmlDoc.DocumentElement;
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);
                dataTable = dbConnection.Execute<DataTable>(queryErxCredentials);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    
                    foreach (DataRow row in dataTable.Rows)
                    {
                        

                        var rootNode = xmlDoc.CreateElement("RCExtRequest");
                        xmlDoc.AppendChild(rootNode);

                        XmlAttribute version = xmlDoc.CreateAttribute("version");
                        version.Value = row["Version"].ToString();
                        rootNode.Attributes.Append(version);

                        var caller = xmlDoc.CreateElement("Caller");
                        rootNode.AppendChild(caller);

                        var systemName = xmlDoc.CreateElement("SystemName");
                        rootNode.AppendChild(systemName);
                        systemName.InnerText = row["SystemName"].ToString();

                        var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
                        rootNode.AppendChild(rcopiaPracticeUsername);
                        rcopiaPracticeUsername.InnerText = row["PracticeUserName"].ToString(); 

                        var vendorName = xmlDoc.CreateElement("VendorName");
                        caller.AppendChild(vendorName);
                        vendorName.InnerText = row["VendorName"].ToString();

                        var vendorPassword = xmlDoc.CreateElement("VendorPassword");
                        caller.AppendChild(vendorPassword);
                        vendorPassword.InnerText = row["VendorPassword"].ToString(); ;

                        var request = xmlDoc.CreateElement("Request");
                        rootNode.AppendChild(request);



                        var command = xmlDoc.CreateElement("Command");
                        request.AppendChild(command);
                        command.InnerText = "get_notification_count";
                        var returnPrescriptionIDs = xmlDoc.CreateElement("ReturnPrescriptionIDs");
                        request.AppendChild(returnPrescriptionIDs);
                        returnPrescriptionIDs.InnerText = "y";

                        var type = xmlDoc.CreateElement("Type");
                        request.AppendChild(type);
                        type.InnerText = "all";
                   }
                }
                return xmlDoc.InnerXml;
            }
        }
        public TaskIconPresenter Presenter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is close pending.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is close pending; otherwise, <c>false</c>.
        /// </value>
        public bool IsClosePending { get; set; }

        protected override bool ShowWithoutActivation
        {
            get { return false; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            PictureBox.Image = Resources.Images.Message;
            PictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

            Width = 40;
            Height = 40;

            Location = new Point(Screen.PrimaryScreen.WorkingArea.Right - Width - 10, Screen.PrimaryScreen.WorkingArea.Top + 10);

            UpdateIcon();

            TopMost = true;
            MakeTopMost(this);

            if (Presenter == null)
            {
                throw new InvalidOperationException("Presenter should be set.");
            }

            Presenter.TaskActivityUsersChanged += OnTaskActivityUsersChanged;
        }

        private void OnTaskActivityUsersChanged(object sender, EventArgs e)
        {
            if (IsClosePending) return; 

            if ((InvokeRequired))
            {
                // set from a bg thread
                BeginInvoke(new Action(UpdateIcon));
            }
            else
            {
                UpdateIcon();
            }
        }

        public void UpdateIcon()
        {
            if (!IsHandleCreated || IsDisposed || Disposing) return;

            if ((Presenter.TaskActivityUsers == null || !Presenter.TaskActivityUsers.Any(tar => tar.Active | tar.AlertActive)))
            {
                // No active tasks or alerts
                PictureBox.Image = Resources.Images.Message;
            }
            else if (Presenter.TaskActivityUsers.Any(tar => tar.AlertActive))
            {
                // Active alerts
                PictureBox.Image = Resources.Images.MessageAlert;
            }
            else if (Presenter.TaskActivityUsers.Any(tar => tar.Active))
            {
                // Active tasks
                PictureBox.Image = Resources.Images.MessageOpen;
            }
            else
            {
                PictureBox.Image = Resources.Images.Message;
            }
        }

        private void OnMouseDown(Object sender, MouseEventArgs e)
        {
            if ((e.Clicks == 1 & e.Button == MouseButtons.Left))
            {
                _isDragging = true;
                _startDragPosition = e.Location;
            }
        }

        private void OnMouseMove(Object sender, MouseEventArgs e)
        {
            if ((_isDragging))
            {
                var p1 = new Point(e.X, e.Y);
                var p2 = PointToScreen(p1);
                var p3 = new Point(p2.X - _startDragPosition.X, p2.Y - _startDragPosition.Y);
                Location = p3;
            }
        }

        private void OnMouseUp(Object sender, MouseEventArgs e)
        {
            _isDragging = false;
        }

        private void OnDoubleClick(Object sender, EventArgs e)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                var reviewTasksView = new ReviewTasksView();

                reviewTasksView.Load += delegate
                {
                    var window = System.Windows.Application.Current.Windows.OfType<System.Windows.Window>().Last();
                    window.Deactivated += delegate { window.Activate(); };
                };
                reviewTasksView.Presenter = Presenter.ReviewTasksPresenter;
                reviewTasksView.Presenter.Arguments.PatientId = null;
                InteractionManager.Current.ShowModal(new WindowInteractionArguments
                {
                    Content = reviewTasksView,
                    WindowState = System.Windows.WindowState.Maximized,
                    ResizeMode = ResizeMode.CanResize,
                    DialogButtons = DialogButtons.Ok
                });
            }));
        }

        protected override void WndProc(ref Message m)
        {
            //If we're being activated because the mouse clicked on us...
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                //Then refuse to be activated, but allow the click event to pass through (don't use MA_NOACTIVATEEAT)
                m.Result = (IntPtr)MA_NOACTIVATE;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
    }

    public class TaskIconPresenter
    {
        private readonly Func<ReviewTasksPresenter> _reviewTasksPresenterResolver;

        private IEnumerable<TaskActivityUser> _taskActivityResources;

        private readonly Func<TaskActivityViewPresenter> _taskActivityViewPresenterResolver;

        public TaskIconPresenter(Func<ReviewTasksPresenter> reviewTasksPresenterResolver, Func<TaskActivityViewPresenter> taskActivityViewPresenterResolver)
        {
            _reviewTasksPresenterResolver = reviewTasksPresenterResolver;
            _taskActivityViewPresenterResolver = taskActivityViewPresenterResolver;
        }

        public IEnumerable<TaskActivityUser> TaskActivityUsers
        {
            get { return _taskActivityResources; }
            set
            {
                _taskActivityResources = value;
                if (TaskActivityUsersChanged != null)
                {
                    TaskActivityUsersChanged(this, new EventArgs());
                }
            }
        }

        public ReviewTasksPresenter ReviewTasksPresenter
        {
            get { return _reviewTasksPresenterResolver(); }
        }

        public TaskActivityViewPresenter TaskActivityViewPresenter
        {
            get { return _taskActivityViewPresenterResolver(); }
        }

        public event EventHandler<EventArgs> TaskActivityUsersChanged;
    }
}