﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.Linq;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class TaskActivityView
    {
        private readonly PresentationBackgroundWorker _worker;

        public TaskActivityView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            // Add any initialization after the InitializeComponent() call.

            Header = "Task Management";

            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            TaskGrid.IsReadOnly = true;
            TaskGrid.AutoGenerateColumns = false;
            TaskGrid.AutoGenerateRelations = false;

            TaskGrid.PreviewMouseDown += OnGridViewClick;
            TaskGrid.SelectionChanged += OnGridSelectionChanged;

            CreateNewTaskButton.Click += OnCreateNewTaskClick;
            ViewOtherTaskButton.Click += OnViewOtherTaskClick;
        }

        public TaskActivityViewPresenter Presenter { get; set; }

        private void LoadPresenter()
        {
            TaskActivityViewPresenter presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var taskColumns = new List<GridViewColumn>
                                  {
                                      new GridViewColumn(new Binding("TaskActivityTypeName"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Type",
                                              Width = 170
                                          },
                                      new GridViewColumn(new Binding("Title"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Title",
                                              Width = 100
                                          },
                                      new GridViewColumn(new Binding("Status"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Status",
                                              Width = 170
                                          },
                                      new GridViewColumn(new Binding("PatientDisplayName"))
                                          {
                                              IsReadOnly = true,
                                              Header = "PatientName",
                                              Width = 200
                                          },
                                      new GridViewColumn(new Binding("CreatedDateTime"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Createdon",
                                              Width = 200
                                          },
                                      new GridViewColumn(new Binding("AssignedTo"))
                                          {
                                              IsReadOnly = true,
                                              Header = "AssignedTo",
                                              Width = 150
                                          }
                                  };
            taskColumns.ToList().ForEach(TaskGrid.Columns.Add);
            _worker.RunWorkerAsync();
        }


        public void OnPresenterLoaded()
        {
            PatientOptions.ItemsSource = Presenter.PatientChoices;
            object selectedItem = TaskGrid.SelectedItem;

            TaskGrid.ItemsSource = Presenter.Tasks;
            TaskGrid.SelectedItem = selectedItem;

            PatientOptions.SelectedIndex = 0;
            PatientOptions.Enabled = false;
        }

        private void OnGridViewClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;
            object selectedItem = source == null ? null : source.DataContext;

            if (e.ClickCount == 2 && e.ChangedButton == MouseButton.Left)
            {
                var taskActivity = selectedItem as TaskActivity;
                if (taskActivity != null)
                {
                    Presenter.ViewTask(taskActivity);
                    _worker.RunWorkerAsync();
                }
                else
                {
                    var task = selectedItem as TaskViewModel;
                    if ((task != null))
                    {
                        taskActivity = task.TaskActivities.FirstOrDefault(ta => !ta.IsComplete);
                        if ((taskActivity != null))
                        {
                            Presenter.ViewTask(taskActivity);
                            _worker.RunWorkerAsync();
                        }
                    }
                }
            }
        }

        private void OnGridSelectionChanged(object sender, EventArgs e)
        {
            PatientOptions.Enabled = false;
            PatientOptions.SelectedIndex = 0;
            if ((TaskGrid.SelectedItem != null))
            {
                var task = (TaskViewModel) TaskGrid.SelectedItem;
                if (task.Patient != null)
                {
                    if (task.PatientDisplayName != "<No Patient>")
                    {
                        PatientOptions.Enabled = true;
                    }
                }
            }
        }


        private void OnCreateNewTaskClick(object sender, EventArgs e)
        {
            Presenter.CreateTask();
            _worker.RunWorkerAsync();
        }

        private void OnViewOtherTaskClick(object sender, EventArgs e)
        {
            Presenter.ViewOtherTasks();
        }
    }

    public class TaskActivityViewPresenter
    {
        private readonly TaskActivityArguments _arguments = new TaskActivityArguments();
        private readonly IPracticeRepository _practiceRepository;
        private readonly TaskViewManager _taskViewManager;
        public string[] PatientChoices = new string[4];
        private IEnumerable<TaskViewModel> _tasks;

        public TaskActivityViewPresenter(IPracticeRepository practiceRepository, TaskViewManager taskViewManager)
        {
            _practiceRepository = practiceRepository;
            _taskViewManager = taskViewManager;
            PatientChoices[0] = "Go to Patient--";
            PatientChoices[1] = "Chart";
            PatientChoices[2] = "Financials";
            PatientChoices[3] = "Info";
        }

        public TaskViewManager TaskViewManager
        {
            get { return _taskViewManager; }
        }

        
        public IEnumerable<TaskViewModel> Tasks
        {
            get { return _tasks; }
        }

        public TaskActivityArguments Arguments
        {
            get { return _arguments; }
        }

        public void OnRequestingOpenTaskPatient(Task patientTask, string patientScreenToOpen)
        {
            TaskViewManager.OnRequestingOpenTaskPatient(patientTask, patientScreenToOpen);
        }


        public void Load()
        {
            IQueryable<Task> tasks = _practiceRepository.Tasks.Include(t => t.Patient)
                .Include(t => t.TaskActivities.Select(ta => new object[]
                                                                {
                                                                    ta.TaskActivityUsers.Select(tau => tau.AssignedTo),
                                                                    ta.TaskActivityType,
                                                                    ta.CreatedBy
                                                                }));

            List<int> assignedToUserIds = null;
            if (Arguments.AssignedToUserIds != null)
            {
                assignedToUserIds = Arguments.AssignedToUserIds.Distinct().ToList();
                tasks = tasks.Where(t => t.TaskActivities
                                             .Any(ta => ta.TaskActivityUsers
                                                            .Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId))));
            }

            if (Arguments.Active.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities
                                             .Any(ta => ta.TaskActivityUsers
                                                            .Any(tar => Arguments.Active != null && tar.Active == Arguments.Active.Value)));
            }
            
            _tasks = tasks.ToArray().OrderBy(t => t.CreatedDateTime).ToArray()
                .Select(i => new TaskViewModel
                                 {
                                     Id = i.Id,
                                     Patient = i.Patient,
                                     PatientId = i.PatientId,
                                     TaskActivities = i.TaskActivities,
                                     Title = i.Title,
                                     IsComplete = i.IsComplete,
                                     AlertActive = i.AlertActive,
                                     Status = i.Status,
                                     PatientDisplayName = i.PatientDisplayName,
                                     TaskActivityTypeName = i.TaskActivityTypeName,
                                     CreatedDateTime = i.CreatedDateTime,
                                     AssignedTo = i.TaskActivities.SelectMany(j => j.AssignedTo).Contains(',') ? "Many" : i.TaskActivities.Select(j => j.AssignedTo).FirstOrDefault()
                                 }).ToExtendedObservableCollection();


            if (assignedToUserIds != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities
                                                .Where(ta => ta.TaskActivityUsers
                                                    .Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId)))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.Active.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities
                                                .Where(ta => ta.TaskActivityUsers
                                                                 .Any(tar => Arguments.Active != null && tar.Active == Arguments.Active.Value))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }
        }

        public void CreateTask()
        {
            _taskViewManager.CreateTask();
        }

        public void ViewTask(TaskActivity taskActivity)
        {
            _taskViewManager.ViewTask(taskActivity);
        }

        public void ViewOtherTasks()
        {
            _taskViewManager.OtherTasksView();
        }
    }
    public class TaskActivityArguments
    {
        public bool? Active;

        public bool? AlertActive;
        public int[] AssignedByUserIds;
        public int[] AssignedToGroupIds;
        public int[] AssignedToUserIds;

        public DateTime? EndDate;
        public int? PatientId;
        public DateTime? StartDate;
        public int[] TaskActivityTypeIDs;

        public TaskActivityArguments()
        {

            PatientId = ApplicationContext.Current.PatientId;
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };

            Active = true;
        }
    }

    public class TaskViewModel
    {
        public int Id { get; set; }
        public Patient Patient { get; set; }
        public int? PatientId { get; set; }
        public ICollection<TaskActivity> TaskActivities { get; set; }
        public string Title { get; set; }
        public bool IsComplete { get; set; }
        public bool AlertActive { get; set; }
        public string Status { get; set; }
        public string PatientDisplayName { get; set; }
        public string TaskActivityTypeName { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public string AssignedTo { get; set; }
    }
}
