﻿namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class ViewTaskView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.CompleteTaskButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.DeactivateAlarmButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.OKButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.ForwardTaskButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.TitleText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.DescriptionText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.TitleLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.DescriptionLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.CreatedTitleLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.CreatedLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.TaskActivityTypesList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.TypeLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.AssignedToUsersLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.AssignedToText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.SuspendLayout();
            // 
            // CompleteTaskButton
            // 
            this.CompleteTaskButton.Location = new System.Drawing.Point(47, 266);
            this.CompleteTaskButton.Name = "CompleteTaskButton";
            this.CompleteTaskButton.Size = new System.Drawing.Size(92, 53);
            this.CompleteTaskButton.TabIndex = 0;
            this.CompleteTaskButton.Text = "Complete Task";
            // 
            // DeactivateAlarmButton
            // 
            this.DeactivateAlarmButton.Location = new System.Drawing.Point(145, 266);
            this.DeactivateAlarmButton.Name = "DeactivateAlarmButton";
            this.DeactivateAlarmButton.Size = new System.Drawing.Size(103, 53);
            this.DeactivateAlarmButton.TabIndex = 1;
            this.DeactivateAlarmButton.Text = "Deactivate Alarm";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(351, 266);
            this.OKButton.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(92, 53);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            // 
            // ForwardTaskButton
            // 
            this.ForwardTaskButton.Location = new System.Drawing.Point(254, 266);
            this.ForwardTaskButton.Name = "ForwardTaskButton";
            this.ForwardTaskButton.Size = new System.Drawing.Size(92, 53);
            this.ForwardTaskButton.TabIndex = 3;
            this.ForwardTaskButton.Text = "Forward Task";
            // 
            // TitleText
            // 
            this.TitleText.Location = new System.Drawing.Point(103, 9);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(341, 32);
            this.TitleText.TabIndex = 4;
            // 
            // DescriptionText
            // 
            this.DescriptionText.Location = new System.Drawing.Point(103, 56);
            this.DescriptionText.Multiline = true;
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(341, 78);
            this.DescriptionText.TabIndex = 5;
            // 
            // TitleLabel
            // 
            this.TitleLabel.Location = new System.Drawing.Point(5, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(49, 20);
            this.TitleLabel.TabIndex = 6;
            this.TitleLabel.Text = "Title:";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.Location = new System.Drawing.Point(5, 56);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(72, 27);
            this.DescriptionLabel.TabIndex = 7;
            this.DescriptionLabel.Text = "Description";
            // 
            // CreatedTitleLabel
            // 
            this.CreatedTitleLabel.Location = new System.Drawing.Point(5, 141);
            this.CreatedTitleLabel.Name = "CreatedTitleLabel";
            this.CreatedTitleLabel.Size = new System.Drawing.Size(49, 23);
            this.CreatedTitleLabel.TabIndex = 8;
            this.CreatedTitleLabel.Text = "Created:";
            // 
            // CreatedLabel
            // 
            this.CreatedLabel.Location = new System.Drawing.Point(103, 141);
            this.CreatedLabel.Name = "CreatedLabel";
            this.CreatedLabel.Size = new System.Drawing.Size(161, 26);
            this.CreatedLabel.TabIndex = 9;
            this.CreatedLabel.Text = "01/01/2000 -12:00 AM";
            // 
            // TaskActivityTypesList
            // 
            this.TaskActivityTypesList.DisplayMemberPath = "";
            this.TaskActivityTypesList.Location = new System.Drawing.Point(326, 141);
            this.TaskActivityTypesList.Name = "TaskActivityTypesList";
            this.TaskActivityTypesList.SelectedIndex = -1;
            this.TaskActivityTypesList.SelectedValue = "";
            this.TaskActivityTypesList.SelectedValuePath = "";
            this.TaskActivityTypesList.Size = new System.Drawing.Size(118, 27);
            this.TaskActivityTypesList.TabIndex = 18;
            // 
            // TypeLabel
            // 
            this.TypeLabel.Location = new System.Drawing.Point(270, 140);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(58, 15);
            this.TypeLabel.TabIndex = 17;
            this.TypeLabel.Text = "Type";
            // 
            // AssignedToUsersLabel
            // 
            this.AssignedToUsersLabel.Location = new System.Drawing.Point(5, 177);
            this.AssignedToUsersLabel.Name = "AssignedToUsersLabel";
            this.AssignedToUsersLabel.Size = new System.Drawing.Size(78, 23);
            this.AssignedToUsersLabel.TabIndex = 19;
            this.AssignedToUsersLabel.Text = "Assigned To:";
            // 
            // AssignedToText
            // 
            this.AssignedToText.Location = new System.Drawing.Point(102, 177);
            this.AssignedToText.Multiline = true;
            this.AssignedToText.Name = "AssignedToText";
            this.AssignedToText.Size = new System.Drawing.Size(341, 74);
            this.AssignedToText.TabIndex = 20;
            // 
            // ViewTaskView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.AssignedToText);
            this.Controls.Add(this.ForwardTaskButton);
            this.Controls.Add(this.AssignedToUsersLabel);
            this.Controls.Add(this.DeactivateAlarmButton);
            this.Controls.Add(this.TaskActivityTypesList);
            this.Controls.Add(this.CompleteTaskButton);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.CreatedLabel);
            this.Controls.Add(this.CreatedTitleLabel);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.DescriptionText);
            this.Controls.Add(this.TitleText);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ViewTaskView";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(460, 331);
            this.ResumeLayout(false);

        }
        internal Soaf.Presentation.Controls.WindowsForms.Button CompleteTaskButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button DeactivateAlarmButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button OKButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button ForwardTaskButton;
        internal Soaf.Presentation.Controls.WindowsForms.TextBox TitleText;
        internal Soaf.Presentation.Controls.WindowsForms.TextBox DescriptionText;
        internal Soaf.Presentation.Controls.WindowsForms.Label TitleLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label DescriptionLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label CreatedTitleLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label CreatedLabel;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox TaskActivityTypesList;

        internal Soaf.Presentation.Controls.WindowsForms.Label TypeLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label AssignedToUsersLabel;
        internal Soaf.Presentation.Controls.WindowsForms.TextBox AssignedToText;
    }

}
