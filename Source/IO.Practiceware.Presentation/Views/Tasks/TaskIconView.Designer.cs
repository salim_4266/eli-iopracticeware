﻿namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class TaskIconView : System.Windows.Forms.Form
    {

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.PictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)this.PictureBox).BeginInit();
            this.SuspendLayout();
            //
            //PictureBox
            //
            this.PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox.Location = new System.Drawing.Point(0, 0);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(40, 40);
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            //
            //TaskIconForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);

            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(40, 40);
            this.ControlBox = false;
            this.Controls.Add(this.PictureBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TaskIconForm";
            this.Opacity = 0.95;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.White;
            ((System.ComponentModel.ISupportInitialize)this.PictureBox).EndInit();
            this.ResumeLayout(false);

        }
        internal System.Windows.Forms.PictureBox PictureBox;
    }

}
