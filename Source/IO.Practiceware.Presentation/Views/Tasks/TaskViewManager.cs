﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    [Singleton]
    public class TaskViewManager
    {
        private readonly Func<AssignTaskPresenter> _assignTaskPresenterResolver;
        private readonly IInteractionManager _interactionManager;
        private readonly Func<ReviewTasksPresenter> _reviewTasksPresenterResolver;
        private readonly Func<TaskAdministrationPresenter> _taskAdministrationPresenterResolver;
        private readonly Func<ViewTaskPresenter> _viewTaskPresenterResolver;
        private readonly Func<TaskActivityViewPresenter> _taskActivityViewPresenterResolver;
        private readonly Func<OtherTasksViewPresenter> _otherTasksViewPresenterResolver;
        private readonly Func<GroupAdministrationPresenter> _groupAdministrationPresenterResolver;
        private readonly Func<TaskActivityTypeAdministrationPresenter> _taskActivityTypeAdministrationPresenterResolver;
        private readonly Func<UserAdministrationViewPresenter> _userAdministrationPresenterResolver;

        public TaskViewManager(IInteractionManager interactionManager, Func<ReviewTasksPresenter> reviewTasksPresenterResolver, Func<ViewTaskPresenter> viewTaskPresenterResolver, Func<AssignTaskPresenter> assignTaskPresenterResolver, Func<TaskAdministrationPresenter> taskAdministrationPresenterResolver, Func<TaskActivityViewPresenter> taskActivityViewPresenter, Func<OtherTasksViewPresenter> otherTasksViewPresenter, Func<GroupAdministrationPresenter> groupAdministrationPresenterResolver, Func<TaskActivityTypeAdministrationPresenter> taskActivityTypeAdministrationPresenterResolver, Func<UserAdministrationViewPresenter> userAdministrationPresenterResolver)
        {
            _interactionManager = interactionManager;
            _reviewTasksPresenterResolver = reviewTasksPresenterResolver;
            _viewTaskPresenterResolver = viewTaskPresenterResolver;
            _assignTaskPresenterResolver = assignTaskPresenterResolver;
            _taskAdministrationPresenterResolver = taskAdministrationPresenterResolver;
            _taskActivityViewPresenterResolver = taskActivityViewPresenter;
            _otherTasksViewPresenterResolver = otherTasksViewPresenter;
            _groupAdministrationPresenterResolver = groupAdministrationPresenterResolver;
            _taskActivityTypeAdministrationPresenterResolver = taskActivityTypeAdministrationPresenterResolver;
            _userAdministrationPresenterResolver = userAdministrationPresenterResolver;
        }

        public event EventHandler<TaskSelectedEventArgs> RequestingOpenTaskPatient;

        public void CreateTask()
        {
            var view = new AssignTaskView {Presenter = _assignTaskPresenterResolver()};
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  ResizeMode = ResizeMode.NoResize
                                              });
        }

        public void ViewTask(TaskActivity taskActivity)
        {
            var view = new ViewTaskView {Presenter = _viewTaskPresenterResolver()};
            view.Presenter.TaskActivity = taskActivity;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  ResizeMode = ResizeMode.NoResize
                                              });
        }

        public void AdministerTasks()
        {
            var view = new TaskAdministrationView {Presenter = _taskAdministrationPresenterResolver()};
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  ResizeMode = ResizeMode.NoResize
                                              });
        }

        public void ReviewTasks()
        {
            var view = new ReviewTasksView {Presenter = _reviewTasksPresenterResolver()};
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void ReviewCurrentUserActiveTasks()
        {
            var view = new ReviewTasksView {Presenter = _reviewTasksPresenterResolver()};
            view.Presenter.Arguments.ConfigureForCurrentUserActiveTasks();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.NoResize,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void ReviewCurrentUserTasksWithActiveAlert()
        {
            var view = new ReviewTasksView {Presenter = _reviewTasksPresenterResolver()};
            view.Presenter.Arguments.ConfigureForCurrentUserTasksWithActiveAlerts();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.NoResize,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void ReviewActivePatientActiveTasks()
        {
            var view = new ReviewTasksView {Presenter = _reviewTasksPresenterResolver()};
            view.Presenter.Arguments.ConfigureForActivePatientActiveTasks();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.NoResize,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void ReviewActivePatientTasksWithActiveAlert()
        {
            var view = new ReviewTasksView {Presenter = _reviewTasksPresenterResolver()};
            view.Presenter.Arguments.ConfigureForActivePatientTasksWithActiveAlerts();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.NoResize,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void AssignTask(TaskActivity taskActivity)
        {
            var view = new AssignTaskView {Presenter = _assignTaskPresenterResolver()};
            view.Presenter.TaskActivity = taskActivity;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  ResizeMode = ResizeMode.NoResize
                                              });
        }


        public void OnRequestingOpenTaskPatient(Task patientTask, string patientScreenToOpen)
        {
            if (RequestingOpenTaskPatient != null)
            {
                RequestingOpenTaskPatient(this, new TaskSelectedEventArgs(patientTask, patientScreenToOpen));
            }
        }

        public void TaskActivityView()
        {
            var view = new TaskActivityView {Presenter = _taskActivityViewPresenterResolver()};
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.NoResize,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void OtherTasksView()
        {
            var view = new OtherTasksView {Presenter = _otherTasksViewPresenterResolver()};
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  DialogButtons = DialogButtons.Ok
                                              });
        }

        public void ShowTaskActivityTypeAdministration()
        {
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = new TaskActivityTypeAdministrationView { Presenter = _taskActivityTypeAdministrationPresenterResolver() },
                DialogButtons = DialogButtons.Ok | DialogButtons.Cancel,
                ResizeMode = ResizeMode.NoResize
            });
        }

        public void ShowGroupAdministration()
        {
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = new GroupAdministrationView { Presenter = _groupAdministrationPresenterResolver() },
                DialogButtons = DialogButtons.Ok | DialogButtons.Cancel,
                ResizeMode = ResizeMode.NoResize
            });
        }

        public void ShowUserAdministration()
        {
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = new UserAdministrationView { Presenter = _userAdministrationPresenterResolver() },
                DialogButtons = DialogButtons.Ok | DialogButtons.Cancel,
                ResizeMode = ResizeMode.NoResize
            });
        }
    }
}