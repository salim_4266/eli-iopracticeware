﻿using Soaf.Presentation.Controls.WindowsForms;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class TaskActivityView : Soaf.Presentation.Controls.WindowsForms.UserControl 
    {
        #region Component Designer generated code
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.FilterPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
            this.PatientOptionsLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.PatientOptions = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.ViewOtherTaskButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.CreateNewTaskButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.TaskGrid = new Soaf.Presentation.Controls.WindowsForms.GridView();
            this.FilterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // FilterPanel
            // 
            this.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.FilterPanel.Controls.Add(this.PatientOptionsLabel);
            this.FilterPanel.Controls.Add(this.PatientOptions);
            this.FilterPanel.Controls.Add(this.ViewOtherTaskButton);
            this.FilterPanel.Controls.Add(this.CreateNewTaskButton);
            this.FilterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.FilterPanel.Location = new System.Drawing.Point(0, 0);
            this.FilterPanel.Name = "FilterPanel";
            this.FilterPanel.Size = new System.Drawing.Size(788, 70);
            this.FilterPanel.TabIndex = 0;
            // 
            // PatientOptionsLabel
            // 
            this.PatientOptionsLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PatientOptionsLabel.Location = new System.Drawing.Point(228, 14);
            this.PatientOptionsLabel.Name = "PatientOptionsLabel";
            this.PatientOptionsLabel.Size = new System.Drawing.Size(80, 12);
            this.PatientOptionsLabel.TabIndex = 4;
            this.PatientOptionsLabel.Text = "Patient Options";
            this.PatientOptionsLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            // 
            // PatientOptions
            // 
            this.PatientOptions.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PatientOptions.DisplayMemberPath = "";
            this.PatientOptions.Location = new System.Drawing.Point(312, 7);
            this.PatientOptions.Name = "PatientOptions";
            this.PatientOptions.SelectedIndex = -1;
            this.PatientOptions.SelectedValuePath = "";
            this.PatientOptions.Size = new System.Drawing.Size(168, 30);
            this.PatientOptions.TabIndex = 3;
            this.PatientOptions.Text = "comboBox1";
            // 
            // ViewOtherTaskButton
            // 
            this.ViewOtherTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ViewOtherTaskButton.Location = new System.Drawing.Point(685, 5);
            this.ViewOtherTaskButton.Name = "ViewOtherTaskButton";
            this.ViewOtherTaskButton.Size = new System.Drawing.Size(100, 60);
            this.ViewOtherTaskButton.TabIndex = 1;
            this.ViewOtherTaskButton.Text = "View Other Tasks";
            // 
            // CreateNewTaskButton
            // 
            this.CreateNewTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateNewTaskButton.Location = new System.Drawing.Point(561, 6);
            this.CreateNewTaskButton.Name = "CreateNewTaskButton";
            this.CreateNewTaskButton.Size = new System.Drawing.Size(100, 60);
            this.CreateNewTaskButton.TabIndex = 0;
            this.CreateNewTaskButton.Text = "Create New Task";
            // 
            // TaskGrid
            // 
            this.TaskGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TaskGrid.Location = new System.Drawing.Point(0, 70);
            this.TaskGrid.Name = "TaskGrid";
            this.TaskGrid.Size = new System.Drawing.Size(788, 371);
            this.TaskGrid.TabIndex = 1;
            // 
            // TaskActivityView
            // 
            this.Controls.Add(this.TaskGrid);
            this.Controls.Add(this.FilterPanel);
            this.Name = "TaskActivityView";
            this.Size = new System.Drawing.Size(788, 441);
            this.FilterPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal Panel FilterPanel;
        private Button ViewOtherTaskButton;
        private Button CreateNewTaskButton;
        internal GridView TaskGrid;
        private ComboBox PatientOptions;
        private Label PatientOptionsLabel;

    }
}
