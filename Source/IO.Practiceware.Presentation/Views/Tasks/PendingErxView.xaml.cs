﻿using IO.Practiceware.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    /// <summary>
    /// Interaction logic for PendingErxView.xaml
    /// </summary>
    public partial class PendingErxView : Window
    {
        public DataTable Notifications { get; set; }
        int ErxPendingCount;
        int RefillCount;
        public PendingErxView()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string responseXml = string.Empty;

            if (Notifications != null && Notifications.Rows.Count > 0)
            {
                responseXml = Notifications.Rows[0]["XmlResponse"].ToString();
                if (!string.IsNullOrEmpty(responseXml))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(responseXml);

                    XmlNodeList xnList = doc.SelectNodes("RCExtResponse/Response/NotificationCountList/NotificationCount");
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Notification details:");
                    sb.AppendLine("------------------------------");
                    foreach (XmlNode xn in xnList)
                    {
                        string type = xn["Type"].InnerText;
                        string number = xn["Number"].InnerText;
                        
                        if (type == "refill")
                        {
                            btnRefill.Content = type;
                            lblRefill.Content = number;
                            if (!string.IsNullOrEmpty(number))
                                RefillCount = int.Parse(number);
                        }
                        if (type == "rx_pending")
                        {
                            btnRxPending.Content = type;
                            lblRxPending.Content = number;
                            if(!string.IsNullOrEmpty(number))
                                ErxPendingCount = int.Parse(number);
                        }
                        if (type == "note")
                        {
                            btnNote.Content = type;
                            lblNote.Content = number;
                        }
                        if (type == "error")
                        {
                            btnError.Content = type;
                            lblError.Content = number;
                        }
                        if (type == "share")
                        {
                            btnShare.Content = type;
                            lblShare.Content = number;
                        }
                        if (type == "rx_need_signing")
                        {
                            btnSigning.Content = type;
                            lblSigning.Content = number;
                        }
                        if (type == "rx_need_dual_sign")
                        {
                            btnDualSign.Content = type;
                            lblDualSign.Content = number;
                        }
                    }
                }
            }
        }

        private void btnRxPending_Click(object sender, RoutedEventArgs e)
        {
            if(ErxPendingCount>0)
            UtilitiesViewManager.OpenErxNotification( "report");
        }

        private void btnRefill_Click(object sender, RoutedEventArgs e)
        {
            if (RefillCount > 0)
                UtilitiesViewManager.OpenErxNotification("message");
        }
    }
}
