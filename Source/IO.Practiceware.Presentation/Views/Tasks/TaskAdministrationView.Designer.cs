﻿using IO.Practiceware.Presentation.Views.Common.Controls;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class TaskAdministrationView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LayoutPanel = new FlowLayoutPanel();
            this.GroupsButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.TypesButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.OKButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.TitleLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.Usersbutton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.LayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LayoutPanel
            // 
            this.LayoutPanel.Controls.Add(this.GroupsButton);
            this.LayoutPanel.Controls.Add(this.TypesButton);
            this.LayoutPanel.Controls.Add(this.Usersbutton);
            this.LayoutPanel.Controls.Add(this.OKButton);
            this.LayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LayoutPanel.Location = new System.Drawing.Point(0, 55);
            this.LayoutPanel.Name = "LayoutPanel";
            this.LayoutPanel.Size = new System.Drawing.Size(419, 68);
            this.LayoutPanel.TabIndex = 0;
            // 
            // GroupsButton
            // 
            this.GroupsButton.Location = new System.Drawing.Point(3, 3);
            this.GroupsButton.Name = "GroupsButton";
            this.GroupsButton.Size = new System.Drawing.Size(100, 60);
            this.GroupsButton.TabIndex = 0;
            this.GroupsButton.Text = "Groups";
            // 
            // TypesButton
            // 
            this.TypesButton.Location = new System.Drawing.Point(109, 3);
            this.TypesButton.Name = "TypesButton";
            this.TypesButton.Size = new System.Drawing.Size(100, 60);
            this.TypesButton.TabIndex = 1;
            this.TypesButton.Text = "Task Types";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(100, 69);
            this.OKButton.Margin = new System.Windows.Forms.Padding(100, 3, 3, 3);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(100, 60);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            // 
            // TitleLabel
            // 
            this.TitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleLabel.Location = new System.Drawing.Point(0, 0);
            this.TitleLabel.Margin = new System.Windows.Forms.Padding(10, 10, 5, 5);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Padding = new System.Windows.Forms.Padding(8);
            this.TitleLabel.Size = new System.Drawing.Size(419, 35);
            this.TitleLabel.TabIndex = 1;
            this.TitleLabel.Text = "Manage...";
            // 
            // Usersbutton
            // 
            this.Usersbutton.Location = new System.Drawing.Point(215, 3);
            this.Usersbutton.Name = "Usersbutton";
            this.Usersbutton.Size = new System.Drawing.Size(100, 60);
            this.Usersbutton.TabIndex = 3;
            this.Usersbutton.Text = "Users";
            // 
            // TaskAdministrationView
            // 
            this.AutoSize = true;
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.LayoutPanel);
            this.Name = "TaskAdministrationView";
            this.Size = new System.Drawing.Size(419, 123);
            this.LayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal FlowLayoutPanel LayoutPanel;
        internal Soaf.Presentation.Controls.WindowsForms.Button GroupsButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button TypesButton;
        internal Soaf.Presentation.Controls.WindowsForms.Label TitleLabel;

        internal Soaf.Presentation.Controls.WindowsForms.Button OKButton;
        private Soaf.Presentation.Controls.WindowsForms.Button Usersbutton;
    }

}
