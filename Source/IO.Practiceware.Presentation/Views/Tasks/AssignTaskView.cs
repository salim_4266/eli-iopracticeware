﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Tasks;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class AssignTaskView
    {
        private readonly PresentationBackgroundWorker _worker;


        public AssignTaskView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            Header = "Create New Task";

            OKButton.Click += OnOkButtonClick;
            CancelButton.Click += OnCancelButtonClick;
            TaskActivityTypesList.SelectionChanged += OnTaskActivityTypesListSelectionChanged;
            GroupsList.SelectionChanged += OnGroupsListSelectionChanged;
            UsersList.SelectionChanged += OnUsersListSelectionChanged;
            AssignIndividuallyButton.StateChanged += OnAssignIndividuallyStateChanged;

            ReqAssignTaskTolabel.ForeColor = Color.Red;
            ReqTaskTitlelabel.ForeColor = Color.Red;
            ReqTaskTypeLabel.ForeColor = Color.Red;

            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();
        }

        public AssignTaskPresenter Presenter { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _worker.RunWorkerAsync();
        }

        private void LoadPresenter()
        {
            var presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        public void SetTaskInformation()
        {
            TitleText.Text = Presenter.TaskActivity.Title;
            DescriptionText.Text = Presenter.TaskActivity.Description;
        }

        public void OnPresenterLoaded()
        {
            if ((ApplicationContext.Current.PatientId == null ||
                 (Presenter.TaskActivity != null && Presenter.TaskActivity.Task.PatientId.HasValue)))
            {
                // hide button if no patient present in application context
                // OR if the task already has a patient associated with it

                 LinkToPatientButton.Visible = false;
            }
            else
            {
                if (Presenter.ActivePatient != null)
                {
                    LinkToPatientButton.Visible = true;
                    LinkToPatientButton.Text = string.Format("Link To Patient ({0})",
                                                             Presenter.ActivePatient.DisplayName);
                }
            }

            AssignIndividuallyButton.CurrentState = AssignIndividuallyButton.States.First();

            UsersList.ItemsSource = Presenter.Users;
            UsersList.DisplayMemberPath = "UserName";
            UsersList.UnselectAll();

            GroupsList.ItemsSource = Presenter.Groups;
            GroupsList.DisplayMemberPath = "Name";
            GroupsList.UnselectAll();

            TaskActivityTypesList.ItemsSource = Presenter.TaskActivityTypes;
            TaskActivityTypesList.DisplayMemberPath = "Name";
            var taskActivityType = Presenter.TaskActivityTypes.FirstOrDefault();
            if ((Presenter.TaskActivity != null))
            {
                taskActivityType =
                    Presenter.TaskActivityTypes.FirstOrDefault(i => i.Id == Presenter.TaskActivity.TaskActivityTypeId);
            }
            if ((taskActivityType == null))
            {
                taskActivityType = Presenter.TaskActivityTypes.FirstOrDefault();
            }

            TaskActivityTypesList.SelectedValue = taskActivityType;
            TaskActivityTypesList.Refresh();

            OnTaskActivityTypesListSelectionChanged(this, new EventArgs());

            // existing task, so load up the title and description
            if ((Presenter.TaskActivity != null))
            {
                SetTaskInformation();
            }
        }

        /// <summary>
        ///   Creates AssignTaskArguments and invokes the TaskManager to assign the task
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        /// <remarks>
        /// </remarks>
        private void OnOkButtonClick(object sender, EventArgs e)
        {
            var arguments = new AssignTaskArguments();

            if ((Presenter.TaskActivity != null))
            {
                // re-assigning an existing task
                arguments.Task = Presenter.TaskActivity.Task;
            }
            arguments.Alert = AlertButton.IsChecked.GetValueOrDefault();
            arguments.AssignedBy = UserContext.Current.UserDetails.Id;
            arguments.AssignIndivudally = AssignIndividuallyButton.CurrentState != "Single user can complete";
            arguments.Title = TitleText.Text;
            arguments.Description = DescriptionText.Text;
            arguments.AssignToUserIds = UsersList.SelectedItems.Cast<User>().Select(user => user.Id);
            arguments.AssignToGroupIds = GroupsList.SelectedItems.Cast<Group>().Select(@group => @group.Id);
            if (((TaskActivityTypesList.SelectedValue) is TaskActivityType))
            {
                arguments.TaskActivityTypeId = ((TaskActivityType)TaskActivityTypesList.SelectedValue).Id;
            }

            if ((ApplicationContext.Current.PatientId != null & LinkToPatientButton.IsChecked.GetValueOrDefault()))
            {
                arguments.PatientId = ApplicationContext.Current.PatientId;
            }
            if ((Validate(arguments)))
            {
                var bw = new PresentationBackgroundWorker(this);
                bw.DoWork += delegate { Presenter.AssignTask(arguments); };
                bw.RunWorkerCompleted += delegate { InteractionContext.Complete(true); };
                bw.RunWorkerAsync();
            }
        }

        public bool Validate(AssignTaskArguments arguments)
        {
            if ((arguments.AssignToUserIds.IsNullOrEmpty() && arguments.AssignToGroupIds.IsNullOrEmpty()) || TaskActivityTypesList.SelectedValue == null || TitleText.Text == string.Empty)
            {
                return false;
            }
            return true;
        }

        private void OnCancelButtonClick(object sender, EventArgs e)
        {
            InteractionContext.Complete(false);
        }

        private void OnTaskActivityTypesListSelectionChanged(object sender, EventArgs e)
        {
            // Set the default content
            var taskActivityType = TaskActivityTypesList.SelectedValue as TaskActivityType;
            if ((taskActivityType != null))
            {
                // Set defaults
                DescriptionText.Text = taskActivityType.DefaultContent;
                AlertButton.IsChecked = taskActivityType.AlertDefault;
                AlertButton.Text = AlertButton.IsChecked == true ? "Alarm Set" : "Set Alarm";
                if ((LinkToPatientButton.Visible))
                {
                    // Only apply default if button is visible
                    LinkToPatientButton.IsChecked = taskActivityType.LinkToPatientDefault;
                }
                AssignIndividuallyButton.CurrentState = taskActivityType.AssignIndividuallyDefault ? AssignIndividuallyButton.States.Last() : AssignIndividuallyButton.States.First();
            }
        }

        private void OnUsersListSelectionChanged(object sender, EventArgs e)
        {
            ValidateAssignIndividuallyButton();
        }

        private void OnGroupsListSelectionChanged(object sender, EventArgs e)
        {
            ValidateAssignIndividuallyButton();
        }

        private void ValidateAssignIndividuallyButton()
        {
            AssignIndividuallyButton.Visible = GroupsList.SelectedItems.Count > 0 || UsersList.SelectedItems.Count > 0;
        }

        private void OnAssignIndividuallyStateChanged(object sender, EventArgs e)
        {
            string args = AssignIndividuallyButton.CurrentState;
            AssignIndividuallyButton.Text = args;
        }
    }

    public class AssignTaskPresenter
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly ITaskService _taskService;
        private Patient _activePatient;
        private IEnumerable<Group> _groups;
        private IEnumerable<TaskActivityType> _taskActivityTypes;
        private IEnumerable<User> _users;


        public AssignTaskPresenter(IPracticeRepository practiceRepository, ITaskService taskService)
        {
            _practiceRepository = practiceRepository;
            _taskService = taskService;
        }

        public TaskActivity TaskActivity { get; set; }


        public IEnumerable<User> Users
        {
            get { return _users; }
        }


        public IEnumerable<Group> Groups
        {
            get { return _groups; }
        }


        public IEnumerable<TaskActivityType> TaskActivityTypes
        {
            get { return _taskActivityTypes; }
        }


        public Patient ActivePatient
        {
            get { return _activePatient; }
        }




        public void Load()
        {
            _users = _practiceRepository.Users.Where(r => !r.IsArchived).OrderBy(r => r.UserName).ThenBy(r => r.LastName).ToArray();
            _groups = _practiceRepository.Groups.OrderBy(g => g.Name).ToArray();
            _taskActivityTypes = _practiceRepository.TaskActivityTypes.OrderBy(tat => tat.OrdinalId).ToArray();
            if (ApplicationContext.Current.PatientId.HasValue)
            {
                _activePatient =
                    _practiceRepository.Patients.FirstOrDefault(pd => pd.Id == ApplicationContext.Current.PatientId);
            }
        }

        public void AssignTask(AssignTaskArguments assignTaskArguments)
        {
            var newTaskActivity = _taskService.AssignTask(assignTaskArguments);
            _practiceRepository.Save(newTaskActivity);

            if ((TaskActivity != null))
            {
                _taskService.CompleteTask(TaskActivity);
                _practiceRepository.Save(TaskActivity);
            }
        }
    }
}