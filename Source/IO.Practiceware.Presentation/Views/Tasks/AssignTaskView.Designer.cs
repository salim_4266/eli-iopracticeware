﻿namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class AssignTaskView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {

        //Required by the Windows Form Designer

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.AssignTaskLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.OKButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.UsersList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.ResourcesLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.GroupsLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.GroupsList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.TitleText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.TitleLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.DescriptionLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.DescriptionText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.CancelButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.TypeLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.TaskActivityTypesList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.AlertButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.LinkToPatientButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.AssignIndividuallyButton = new Soaf.Presentation.Controls.WindowsForms.StateButton();
            this.ReqAssignTaskTolabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ReqTaskTypeLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ReqTaskTitlelabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.SuspendLayout();
            // 
            // AssignTaskLabel
            // 
            this.AssignTaskLabel.Location = new System.Drawing.Point(32, 12);
            this.AssignTaskLabel.Name = "AssignTaskLabel";
            this.AssignTaskLabel.Size = new System.Drawing.Size(97, 25);
            this.AssignTaskLabel.TabIndex = 0;
            this.AssignTaskLabel.Text = "Assign Task To:";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(522, 401);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(88, 53);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            // 
            // UsersList
            // 
            this.UsersList.DisplayMemberPath = "";
            this.UsersList.ItemsSource = new string[] {
        "Loading..."};
            this.UsersList.Location = new System.Drawing.Point(15, 72);
            this.UsersList.Name = "UsersList";
            this.UsersList.SelectedIndex = -1;
            this.UsersList.SelectedValuePath = "";
            this.UsersList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.UsersList.Size = new System.Drawing.Size(152, 291);
            this.UsersList.TabIndex = 3;
            // 
            // ResourcesLabel
            // 
            this.ResourcesLabel.Location = new System.Drawing.Point(15, 51);
            this.ResourcesLabel.Name = "ResourcesLabel";
            this.ResourcesLabel.Size = new System.Drawing.Size(150, 15);
            this.ResourcesLabel.TabIndex = 5;
            this.ResourcesLabel.Text = "User(s)";
            // 
            // GroupsLabel
            // 
            this.GroupsLabel.Location = new System.Drawing.Point(173, 51);
            this.GroupsLabel.Name = "GroupsLabel";
            this.GroupsLabel.Size = new System.Drawing.Size(150, 28);
            this.GroupsLabel.TabIndex = 6;
            this.GroupsLabel.Text = "Group(s)";
            // 
            // GroupsList
            // 
            this.GroupsList.DisplayMemberPath = "";
            this.GroupsList.Location = new System.Drawing.Point(173, 73);
            this.GroupsList.Name = "GroupsList";
            this.GroupsList.SelectedIndex = -1;
            this.GroupsList.SelectedValuePath = "";
            this.GroupsList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GroupsList.Size = new System.Drawing.Size(152, 233);
            this.GroupsList.TabIndex = 7;
            // 
            // TitleText
            // 
            this.TitleText.Location = new System.Drawing.Point(458, 117);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(152, 27);
            this.TitleText.TabIndex = 9;
            // 
            // TitleLabel
            // 
            this.TitleLabel.Location = new System.Drawing.Point(371, 121);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(60, 25);
            this.TitleLabel.TabIndex = 10;
            this.TitleLabel.Text = "Task Title";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.Location = new System.Drawing.Point(371, 152);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(76, 28);
            this.DescriptionLabel.TabIndex = 11;
            this.DescriptionLabel.Text = "Message";
            // 
            // DescriptionText
            // 
            this.DescriptionText.Location = new System.Drawing.Point(458, 153);
            this.DescriptionText.Multiline = true;
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(152, 153);
            this.DescriptionText.TabIndex = 12;
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(630, 401);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(88, 53);
            this.CancelButton.TabIndex = 13;
            this.CancelButton.Text = "Cancel";
            // 
            // TypeLabel
            // 
            this.TypeLabel.Location = new System.Drawing.Point(371, 75);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(60, 40);
            this.TypeLabel.TabIndex = 15;
            this.TypeLabel.Text = "Task Type";
            // 
            // TaskActivityTypesList
            // 
            this.TaskActivityTypesList.DisplayMemberPath = "";
            this.TaskActivityTypesList.Location = new System.Drawing.Point(458, 68);
            this.TaskActivityTypesList.Name = "TaskActivityTypesList";
            this.TaskActivityTypesList.SelectedIndex = -1;
            this.TaskActivityTypesList.SelectedValue = "";
            this.TaskActivityTypesList.SelectedValuePath = "";
            this.TaskActivityTypesList.Size = new System.Drawing.Size(152, 37);
            this.TaskActivityTypesList.TabIndex = 16;
            // 
            // AlertButton
            // 
            this.AlertButton.Location = new System.Drawing.Point(630, 68);
            this.AlertButton.Name = "AlertButton";
            this.AlertButton.Size = new System.Drawing.Size(88, 84);
            this.AlertButton.TabIndex = 17;
            this.AlertButton.Text = "Set Alarm";
            // 
            // LinkToPatientButton
            // 
            this.LinkToPatientButton.Location = new System.Drawing.Point(630, 222);
            this.LinkToPatientButton.Name = "LinkToPatientButton";
            this.LinkToPatientButton.Size = new System.Drawing.Size(88, 84);
            this.LinkToPatientButton.TabIndex = 19;
            this.LinkToPatientButton.Text = "Link To Patient";
            this.LinkToPatientButton.Visible = false;
            // 
            // AssignIndividuallyButton
            // 
            this.AssignIndividuallyButton.CurrentState = "Single user can complete";
            this.AssignIndividuallyButton.Location = new System.Drawing.Point(173, 312);
            this.AssignIndividuallyButton.Name = "AssignIndividuallyButton";
            this.AssignIndividuallyButton.Size = new System.Drawing.Size(153, 51);
            this.AssignIndividuallyButton.States = new string[] {
        "Single user can complete",
        "Each selected user must complete"};
            this.AssignIndividuallyButton.TabIndex = 20;
            this.AssignIndividuallyButton.Text = "Single user can complete";
            this.AssignIndividuallyButton.Visible = false;
            // 
            // ReqAssignTaskTolabel
            // 
            this.ReqAssignTaskTolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReqAssignTaskTolabel.Location = new System.Drawing.Point(19, 14);
            this.ReqAssignTaskTolabel.Name = "ReqAssignTaskTolabel";
            this.ReqAssignTaskTolabel.Size = new System.Drawing.Size(10, 16);
            this.ReqAssignTaskTolabel.TabIndex = 21;
            this.ReqAssignTaskTolabel.Text = "*";
            this.ReqAssignTaskTolabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            // 
            // ReqTaskTypeLabel
            // 
            this.ReqTaskTypeLabel.AllowDrop = true;
            this.ReqTaskTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReqTaskTypeLabel.Location = new System.Drawing.Point(359, 77);
            this.ReqTaskTypeLabel.Name = "ReqTaskTypeLabel";
            this.ReqTaskTypeLabel.Size = new System.Drawing.Size(10, 16);
            this.ReqTaskTypeLabel.TabIndex = 22;
            this.ReqTaskTypeLabel.Text = "*";
            this.ReqTaskTypeLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            // 
            // ReqTaskTitlelabel
            // 
            this.ReqTaskTitlelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReqTaskTitlelabel.Location = new System.Drawing.Point(359, 122);
            this.ReqTaskTitlelabel.Name = "ReqTaskTitlelabel";
            this.ReqTaskTitlelabel.Size = new System.Drawing.Size(10, 16);
            this.ReqTaskTitlelabel.TabIndex = 23;
            this.ReqTaskTitlelabel.Text = "*";
            this.ReqTaskTitlelabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            // 
            // AssignTaskView
            // 
            this.Controls.Add(this.ReqTaskTitlelabel);
            this.Controls.Add(this.ReqTaskTypeLabel);
            this.Controls.Add(this.ReqAssignTaskTolabel);
            this.Controls.Add(this.AssignIndividuallyButton);
            this.Controls.Add(this.AlertButton);
            this.Controls.Add(this.LinkToPatientButton);
            this.Controls.Add(this.TaskActivityTypesList);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.DescriptionText);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.TitleText);
            this.Controls.Add(this.GroupsList);
            this.Controls.Add(this.GroupsLabel);
            this.Controls.Add(this.ResourcesLabel);
            this.Controls.Add(this.UsersList);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.AssignTaskLabel);
            this.Name = "AssignTaskView";
            this.Size = new System.Drawing.Size(745, 471);
            this.ResumeLayout(false);

        }
        internal Soaf.Presentation.Controls.WindowsForms.Label AssignTaskLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Button OKButton;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox UsersList;
        internal Soaf.Presentation.Controls.WindowsForms.Label ResourcesLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label GroupsLabel;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox GroupsList;
        internal Soaf.Presentation.Controls.WindowsForms.TextBox TitleText;
        internal Soaf.Presentation.Controls.WindowsForms.Label TitleLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label DescriptionLabel;
        internal Soaf.Presentation.Controls.WindowsForms.TextBox DescriptionText;
        internal Soaf.Presentation.Controls.WindowsForms.Button CancelButton;
        internal Soaf.Presentation.Controls.WindowsForms.Label TypeLabel;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox TaskActivityTypesList;
        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton AlertButton;

        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton LinkToPatientButton;
        private Soaf.Presentation.Controls.WindowsForms.StateButton AssignIndividuallyButton;
        private Soaf.Presentation.Controls.WindowsForms.Label ReqAssignTaskTolabel;
        private Soaf.Presentation.Controls.WindowsForms.Label ReqTaskTypeLabel;
        private Soaf.Presentation.Controls.WindowsForms.Label ReqTaskTitlelabel;
    }
}
