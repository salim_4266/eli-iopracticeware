﻿using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class TaskActivityTypeAdministrationView
    {
        private readonly PresentationBackgroundWorker _worker;

        public TaskActivityTypeAdministrationView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            DefaultContentTextBox.TextChanged += OnDefaultContentTextBoxTextChanged;
            TypesListBox.SelectionChanged += OnTypesListBoxSelectionChanged;
            AddTypeButton.Click += OnAddTypeButtonClick;
            DeleteTypeButton.Click += OnDeleteTypeButtonClick;
            LinkToPatientButton.Checked += OnLinkToPatientButtonCheckedChanged;
            LinkToPatientButton.Unchecked += OnLinkToPatientButtonCheckedChanged;
            AlertButton.Checked += OnAlertButtonCheckedChanged;
            AlertButton.Unchecked += OnAlertButtonCheckedChanged;
            AssignIndividuallyButton.Checked += OnAssignIndividuallyButtonCheckedChanged;
            AssignIndividuallyButton.Unchecked += OnAssignIndividuallyButtonCheckedChanged;
            TypesListBox.OrderChanged += OnOrderChanged;
        }

        public TaskActivityTypeAdministrationPresenter Presenter { get; set; }

        private void LoadPresenter()
        {
            var presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        private void OnPresenterLoaded()
        {
            TypesListBox.ItemsSource = (IList) Presenter.TaskActivityTypes;
            TypesListBox.DisplayMemberPath = "Name";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InteractionContext.Completed += OnClosing;

            _worker.RunWorkerAsync();
        }

        private void OnClosing(object sender, EventArgs<bool?> e)
        {
            if (e.Value == true)
            {
                Presenter.Save();
            }
        }

        private void OnTypesListBoxSelectionChanged(object sender, EventArgs e)
        {
            DeleteTypeButton.Enabled = false;

            var type = TypesListBox.SelectedItem as TaskActivityType;
            if ((type != null))
            {
                DefaultContentTextBox.Text = type.DefaultContent;
                AlertButton.IsChecked = type.AlertDefault;
                AssignIndividuallyButton.IsChecked = type.AssignIndividuallyDefault;
                LinkToPatientButton.IsChecked = type.LinkToPatientDefault;

                // only allow delete if not previously used with any tasks
                if ((type.TaskActivities.Count == 0))
                {
                    DeleteTypeButton.Enabled = true;
                }
            }
        }

        private void OnAddTypeButtonClick(object sender, EventArgs e)
        {
            var types = TypesListBox.ItemsSource as ICollection<TaskActivityType>;
            if (types != null)
            {
                var result = InteractionManager.Current.Prompt("Please enter a name");
                var name = result.Input;
                if ((name != null))
                {
                    var newType = new TaskActivityType {Name = name};
                    types.Add(newType);
                    TypesListBox.SelectedItem = newType;
                }
            }
        }

        private void OnDeleteTypeButtonClick(object sender, EventArgs e)
        {
            var types = TypesListBox.ItemsSource as ICollection<TaskActivityType>;
            var type = TypesListBox.SelectedItem as TaskActivityType;
            if (types != null && type != null)
            {
                Presenter.Delete(type,types);
            }
        }

        /// <summary>
        ///   Update DefaultContent
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        /// <remarks>
        /// </remarks>
        private void OnDefaultContentTextBoxTextChanged(object sender, EventArgs e)
        {
            var type = TypesListBox.SelectedItem as TaskActivityType;
            if ((type != null))
            {
                type.DefaultContent = DefaultContentTextBox.Text;
            }
        }

        private void OnAlertButtonCheckedChanged(object sender, EventArgs e)
        {
            var type = TypesListBox.SelectedItem as TaskActivityType;
            if ((type != null))
            {
                type.AlertDefault = AlertButton.IsChecked.GetValueOrDefault();
            }
        }

        private void OnAssignIndividuallyButtonCheckedChanged(object sender, EventArgs e)
        {
            var type = TypesListBox.SelectedItem as TaskActivityType;
            if ((type != null))
            {
                type.AssignIndividuallyDefault = AssignIndividuallyButton.IsChecked.GetValueOrDefault();
            }
        }

        private void OnLinkToPatientButtonCheckedChanged(object sender, EventArgs e)
        {
            var type = TypesListBox.SelectedItem as TaskActivityType;
            if ((type != null))
            {
                type.LinkToPatientDefault = LinkToPatientButton.IsChecked.GetValueOrDefault();
            }
        }

        private void OnOrderChanged(object sender, EventArgs e)
        {
            var types = TypesListBox.ItemsSource as ICollection<TaskActivityType>;
            if (types != null)
            {
                int i = 0;
                foreach (var type in types)
                {
                    type.OrdinalId = i;
                    i++;
                }
            }
        }
    }

    public class TaskActivityTypeAdministrationPresenter
    {
        private readonly IPracticeRepository _practiceRepository;

        private IList<TaskActivityType> _taskActivityTypes;

        public TaskActivityTypeAdministrationPresenter(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public IList<TaskActivityType> TaskActivityTypes
        {
            get { return _taskActivityTypes; }
        }

        public void Load()
        {
            var taskActivityTypes = _practiceRepository.TaskActivityTypes.OrderBy(i => i.OrdinalId).ToList().ToExtendedObservableCollection();

            _taskActivityTypes = taskActivityTypes;
        }

        public void Delete(TaskActivityType tat,ICollection<TaskActivityType> activityTypes)
        {
            var tasksCount =
                _practiceRepository.TaskActivities.Where(i => i.TaskActivityTypeId == tat.Id).Select(i => i.Task).Count();
            if (tasksCount > 0)
            {
                InteractionManager.Current.Alert("Cannot delete task type that is in use");
                return;
            }
            _practiceRepository.Delete(tat);
            activityTypes.Remove(tat);
        }

        public void Save()
        {
            var bw = new PresentationBackgroundWorker(this);
            bw.DoWork += delegate { TaskActivityTypes.ToList().ForEach(_practiceRepository.Save); };
            bw.RunWorkerAsync();
        }
    }
}