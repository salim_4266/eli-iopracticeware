﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.Linq;
using Soaf.Presentation;
using Soaf.Presentation.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GridViewColumn = Soaf.Presentation.Controls.GridViewColumn;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class ReviewTasksView
    {
        private readonly PresentationBackgroundWorker _worker;

        private bool _suspendEvents;


        public ReviewTasksView()
        {
            _suspendEvents = true;
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            StartDatePicker.Text = string.Empty;
            EndDatePicker.Text = string.Empty;
            Grid.IsReadOnly = true;
            Grid.AutoGenerateColumns = false;
            Grid.AutoGenerateRelations = false;

            ClearButton.Click += OnClearButtonClick;
            CreateTaskButton.Click += OnCreateTaskButtonClick;
            CurrentPatientButton.StateChanged += OnCurrentPatientButtonStateChanged;
            CurrentUserButton.StateChanged += OnCurrentUserButtonStateChanged;
            StartDatePicker.SelectionChanged += OnStartDatePickerValueChanged;
            EndDatePicker.SelectionChanged += OnEndDatePickerValueChanged;
            Grid.SelectionChanged += OnGridSelectionChanged;
            Grid.PreviewMouseDown += OnGridViewClick;


            GroupsFilterList.SelectionChanged += OnGroupsFilterListSelectionChanged;
            PatientOptions.SelectionChanged += OnPatientOptionsSelectionChanged;
            StatusButton.StateChanged += OnStatusButtonStateChanged;
            TaskActivityTypesFilterList.SelectionChanged += OnTaskActivityTypesFilterListSelectionChanged;
            UsersFilterList.SelectionChanged += OnUsersFilterListSelectionChanged;
        }

        public ReviewTasksPresenter Presenter { get; set; }


        private void LoadPresenter()
        {
            ReviewTasksPresenter presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _worker.RunWorkerAsync();
        }


        public void OnPresenterLoaded()
        {
            _suspendEvents = true;

            if ((Presenter.Arguments.PatientId.HasValue))
            {
                // if there is a patient in context, display active for current patient
                CurrentPatientButton.Text = string.Format("Current Patient ({0})", Presenter.ActivePatient.DisplayName);
            }
            else
            {
                // otherwise show current user active tasks

                if ((ApplicationContext.Current.PatientId.HasValue))
                {
                    CurrentPatientButton.CurrentState = "Any/No Patient";
                }
                else
                {
                    CurrentPatientButton.Visible = false;
                }
            }

            if (Presenter.ActiveUser == null)
            {
                CurrentUserButton.CurrentState = "All Users";
                CurrentUserButton.Visible = false;
                ForLabel.Visible = false;
            }
            else if (Presenter.Arguments.AssignedToUserIds != null && Presenter.Arguments.AssignedToUserIds.Contains(Presenter.ActiveUser.Id))
            {
                // Current User state already set by default, just add text
                CurrentUserButton.Text = string.Format("Current User ({0})", Presenter.ActiveUser.UserName);
            }
            else
            {
                CurrentUserButton.CurrentState = "All Users";
            }

            UsersFilterList.ItemsSource = Presenter.Users;
            UsersFilterList.DisplayMemberPath = "UserName";
            UsersFilterList.UnselectAll();
            if ((Presenter.Arguments.AssignedToUserIds != null))
            {
                foreach (User user in UsersFilterList.ItemsSource)
                {
                    if ((Presenter.Arguments.AssignedToUserIds.Contains(user.Id)))
                    {
                        UsersFilterList.SelectedItems.Add(user);
                    }
                }
            }

            GroupsFilterList.ItemsSource = Presenter.Groups;
            GroupsFilterList.DisplayMemberPath = "Name";
            GroupsFilterList.UnselectAll();

            if ((Presenter.Arguments.AssignedToGroupIds != null))
            {
                foreach (Group @group in GroupsFilterList.ItemsSource)
                {
                    if ((Presenter.Arguments.AssignedToGroupIds.Contains(@group.Id)))
                    {
                        GroupsFilterList.SelectedItems.Add(@group);
                    }
                }
            }

            if ((Presenter.Arguments.Active.HasValue))
            {
                StatusButton.CurrentState = "Active";
            }

            if ((Presenter.Arguments.AlertActive.HasValue))
            {
                StatusButton.CurrentState = "Alarm";
            }

            TaskActivityTypesFilterList.ItemsSource = Presenter.TaskActivityTypes;
            TaskActivityTypesFilterList.DisplayMemberPath = "Name";
            TaskActivityTypesFilterList.UnselectAll();
            if ((Presenter.Arguments.TaskActivityTypeIDs != null))
            {
                foreach (TaskActivityType taskActivityType in TaskActivityTypesFilterList.ItemsSource)
                {
                    if ((Presenter.Arguments.TaskActivityTypeIDs.Contains(taskActivityType.Id)))
                    {
                        TaskActivityTypesFilterList.SelectedItems.Add(taskActivityType);
                    }
                }
            }

            PatientOptions.ItemsSource = Presenter.PatientChoices;

            if ((Presenter.Arguments.StartDate.HasValue))
            {
                StartDatePicker.SelectedValue = Convert.ToDateTime(Presenter.Arguments.StartDate);
            }
            if ((Presenter.Arguments.EndDate.HasValue))
            {
                EndDatePicker.SelectedValue = Convert.ToDateTime(Presenter.Arguments.EndDate);
            }

            object selectedItem = Grid.SelectedItem;

            IEnumerable<GridViewColumn> taskColumns = GridViewColumn.For<Task>(t => t.TaskActivityTypeName, t => t.Title, t => t.Status, t => t.PatientDisplayName, t => t.CreatedDateTime);
            var taskActivitiesRelation = new GridViewRelation("TaskActivities", GridViewColumn.For<TaskActivity>(i => i.TaskActivityTypeName, i => i.Status, i => i.CreatedDateTime, i => i.AssignedTo, i => i.AssignedBy, i => i.AssignedIndividually, i => i.CompletedDateTime));

            taskColumns.ToList().ForEach(Grid.Columns.Add);
            Grid.Relations.Add(taskActivitiesRelation);

            Grid.ItemsSource = Presenter.Tasks;

            Grid.SelectedItem = selectedItem;

            _suspendEvents = false;
            PatientOptions.SelectedIndex = 0;
            PatientOptions.Enabled = false;
        }


        private void OnGridViewClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;
            object selectedItem = source == null ? null : source.DataContext;

            if (e.ClickCount == 2 && e.ChangedButton == MouseButton.Left)
            {
                var taskActivity = selectedItem as TaskActivity;
                if (taskActivity != null)
                {
                    Presenter.ViewTask(taskActivity);
                    _worker.RunWorkerAsync();
                }
                else
                {
                    var task = selectedItem as Task;
                    if ((task != null))
                    {
                        taskActivity = task.TaskActivities.FirstOrDefault(ta => !ta.IsComplete);
                        if ((taskActivity != null))
                        {
                            Presenter.ViewTask(taskActivity);
                            _worker.RunWorkerAsync();
                        }
                    }
                }
            }
        }

        private void OnGroupsFilterListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                Presenter.Arguments.AssignedToGroupIds = GroupsFilterList.SelectedItems.Count > 0 ? GroupsFilterList.SelectedItems.Cast<Group>().Select(g => g.Id).ToArray() : null;
                _worker.RunWorkerAsync();
            }
        }

        private void OnUsersFilterListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                Presenter.Arguments.AssignedToUserIds = UsersFilterList.SelectedItems.Count > 0 ? UsersFilterList.SelectedItems.Cast<User>().Select(r => r.Id).ToArray() : null;
                _worker.RunWorkerAsync();
            }
        }

        private void OnTaskActivityTypesFilterListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                Presenter.Arguments.TaskActivityTypeIDs = TaskActivityTypesFilterList.SelectedItems.Count > 0 ? TaskActivityTypesFilterList.SelectedItems.Cast<TaskActivityType>().Select(tat => tat.Id).ToArray() : null;
                _worker.RunWorkerAsync();
            }
        }

        private void OnStartDatePickerValueChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                if (StartDatePicker.SelectedValue.HasValue)
                {
                    Presenter.Arguments.StartDate = StartDatePicker.SelectedValue.Value.Date;
                }
                else
                {
                    Presenter.Arguments.StartDate = null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnEndDatePickerValueChanged(object sender, EventArgs e)
        {
            if ((!_suspendEvents))
            {
                if ((EndDatePicker.SelectedValue.HasValue))
                {
                    Presenter.Arguments.EndDate = EndDatePicker.SelectedValue.Value.Date;
                }
                else
                {
                    Presenter.Arguments.EndDate = null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnClearButtonClick(object sender, EventArgs e)
        {
            _suspendEvents = true;
            StartDatePicker.SelectedValue = null;
            EndDatePicker.SelectedValue = null;
            Presenter.Arguments.StartDate = null;
            Presenter.Arguments.EndDate = null;
            _suspendEvents = false;
            _worker.RunWorkerAsync();
        }

        private void OnCurrentUserButtonStateChanged(object sender, EventArgs e)
        {
            string args = CurrentUserButton.CurrentState;
            if (!_suspendEvents)
            {
                switch ((args))
                {
                    case "Current User":
                        Presenter.Arguments.AssignedToUserIds = new[] { UserContext.Current.UserDetails.Id };
                        Presenter.Arguments.AssignedToGroupIds = null;
                        // display user's name
                        CurrentUserButton.Text = string.Format("Current User ({0})", Presenter.ActiveUser.UserName);
                        break;
                    case "All Users":
                        Presenter.Arguments.AssignedToUserIds = null;
                        Presenter.Arguments.AssignedToGroupIds = null;
                        break;
                }
                _worker.RunWorkerAsync();
            }
        }


        private void OnCurrentPatientButtonStateChanged(object sender, EventArgs e)
        {
            string args = CurrentPatientButton.CurrentState;
            if (!_suspendEvents)
            {
                switch ((args))
                {
                    case "Current Patient":
                        // display patient name

                        if (ApplicationContext.Current.PatientId != null)
                        {
                            Presenter.Arguments.PatientId = ApplicationContext.Current.PatientId;
                            CurrentPatientButton.Text = string.Format("Current Patient ({0})", Presenter.ActivePatient.DisplayName);
                        }


                        break;
                    case "Any/No Patient":
                        Presenter.Arguments.PatientId = null;
                        break;
                }
                _worker.RunWorkerAsync();
            }
        }


        private void OnStatusButtonStateChanged(object sender, EventArgs e)
        {
            string args = StatusButton.CurrentState;
            if (!_suspendEvents)
            {
                switch ((args))
                {
                    case "Any":
                        Presenter.Arguments.Active = null;
                        Presenter.Arguments.AlertActive = null;
                        break;
                    case "Active":
                        Presenter.Arguments.Active = true;
                        Presenter.Arguments.AlertActive = null;
                        break;
                    case "Alarm":
                        Presenter.Arguments.AlertActive = true;
                        Presenter.Arguments.Active = true;
                        break;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnCreateTaskButtonClick(object sender, EventArgs e)
        {
            Presenter.CreateTask();
            _worker.RunWorkerAsync();
        }

        private void OnPatientOptionsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Grid.SelectedItem != null && (Grid.SelectedItem) is TaskViewModel))
            {
                var selectedTask = Grid.SelectedItem as TaskViewModel;
                {
                    var task = selectedTask.TaskActivities.Select(i => i.Task).FirstOrDefault();
                    var patientOptionSelected = (string)PatientOptions.SelectedValue;

                    if (patientOptionSelected != "Go to Patient...")
                    {
                        Presenter.OnRequestingOpenTaskPatient(task, patientOptionSelected);
                        PatientOptions.SelectedIndex = 0;
                    }
                }
            }
        }


        private void OnGridSelectionChanged(object sender, EventArgs e)
        {
            PatientOptions.Enabled = false;
            PatientOptions.SelectedIndex = 0;
            if ((Grid.SelectedItem != null))
            {
                var task = Grid.SelectedItem as TaskViewModel;
                if (task != null && task.Patient != null)
                {
                    if (task.PatientDisplayName != "<No Patient>")
                    {
                        PatientOptions.Enabled = true;
                    }
                }
            }
        }
    }


    public class ReviewTasksPresenter
    {
        private readonly ReviewTasksArguments _arguments = new ReviewTasksArguments();
        private readonly IPracticeRepository _practiceRepository;
        private readonly TaskViewManager _taskViewManager;
        public string[] PatientChoices = new string[4];
        private Patient _activePatient;
        private User _activeUser;
        private IEnumerable<Group> _groups;
        private IEnumerable<TaskActivityType> _taskActivityTypes;
        private IEnumerable<TaskViewModel> _tasks;
        private IEnumerable<User> _users;

        public ReviewTasksPresenter(IPracticeRepository practiceRepository, TaskViewManager taskViewManager)
        {
            _practiceRepository = practiceRepository;
            _taskViewManager = taskViewManager;
            PatientChoices[0] = "Go to Patient...";
            PatientChoices[1] = "Chart";
            PatientChoices[2] = "Financials";
            PatientChoices[3] = "Info";
        }

        public TaskViewManager TaskViewManager
        {
            get { return _taskViewManager; }
        }

        public Patient ActivePatient
        {
            get { return _activePatient; }
        }

        public User ActiveUser
        {
            get { return _activeUser; }
        }

        public IEnumerable<TaskViewModel> Tasks
        {
            get { return _tasks; }
        }

        public IEnumerable<User> Users
        {
            get { return _users; }
        }

        public IEnumerable<Group> Groups
        {
            get { return _groups; }
        }

        public IEnumerable<TaskActivityType> TaskActivityTypes
        {
            get { return _taskActivityTypes; }
        }

        public ReviewTasksArguments Arguments
        {
            get { return _arguments; }
        }

        public void OnRequestingOpenTaskPatient(Task patientTask, string patientScreenToOpen)
        {
            TaskViewManager.OnRequestingOpenTaskPatient(patientTask, patientScreenToOpen);
        }


        public void Load()
        {
            if (UserContext.Current.UserDetails.Id != 0)
            {
                _activeUser = _practiceRepository.Users.Where(r => !r.IsArchived).FirstOrDefault(r => r.Id == UserContext.Current.UserDetails.Id);
            }
            if (ApplicationContext.Current.PatientId.HasValue)
            {
                _activePatient = _practiceRepository.Patients.FirstOrDefault(pd => ApplicationContext.Current.PatientId != null && pd.Id == ApplicationContext.Current.PatientId.Value);
            }
            _users = _practiceRepository.Users.Where(r => !r.IsArchived).OrderBy(r => r.UserName).ThenBy(r => r.LastName).ToArray();
            _groups = _practiceRepository.Groups.OrderBy(g => g.Name).ToArray();
            _taskActivityTypes = _practiceRepository.TaskActivityTypes.OrderBy(tat => tat.Name).ToArray();


            IQueryable<Task> tasks = _practiceRepository.Tasks.Include(t => t.TaskActivities.Select(ta =>
                                                                                                    new object[] { ta.TaskActivityUsers.Select(tar => tar.AssignedTo), ta.TaskActivityType, ta.CreatedBy})).Include(t => t.Patient);

            if (Arguments.PatientId.HasValue)
            {
                tasks = tasks.Where(t => Arguments.PatientId != null && (t.PatientId.HasValue && t.PatientId.Value == Arguments.PatientId.Value));
            }

            if (Arguments.AssignedByUserIds != null)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => Arguments.AssignedByUserIds.Contains(ta.CreatedByUserId)));
            }

            List<int> assignedToUserIds = null;
            if (Arguments.AssignedToUserIds != null)
            {
                assignedToUserIds = Arguments.AssignedToUserIds.ToList();
            }

            if ((Arguments.AssignedToGroupIds != null))
            {
                if ((assignedToUserIds == null))
                {
                    assignedToUserIds = new List<int>();
                }
                assignedToUserIds.AddRange(_practiceRepository.Groups.Where(g => Arguments.AssignedToGroupIds.Contains(g.Id)).SelectMany(g => g.GroupUsers).Select(gr => gr.UserId));
            }

            if (assignedToUserIds != null)
            {
                assignedToUserIds = assignedToUserIds.Distinct().ToList();

                tasks = tasks.Where(t => t.TaskActivities.Any(ta => ta.TaskActivityUsers.Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId))));
            }

            if (Arguments.Active.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => ta.TaskActivityUsers.Any(tar => Arguments.Active != null && tar.Active == Arguments.Active.Value)));
            }

            if (Arguments.AlertActive.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => ta.TaskActivityUsers.Any(tar => Arguments.AlertActive != null && tar.AlertActive == Arguments.AlertActive.Value)));
            }

            if (Arguments.TaskActivityTypeIDs != null)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => Arguments.TaskActivityTypeIDs.Contains(ta.TaskActivityTypeId)));
            }

            if (Arguments.StartDate.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => CommonQueries.GetDatePart(ta.CreatedDateTime) >= Arguments.StartDate.Value));
            }

            if (Arguments.EndDate.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => CommonQueries.GetDatePart(ta.CreatedDateTime) <= Arguments.EndDate.Value));
            }            
            _tasks = tasks.ToArray().OrderBy(t => t.CreatedDateTime)
                .ToArray()
                .Select(i => new TaskViewModel
                                 {

                                     Id = i.Id,
                                     Patient = i.Patient,
                                     PatientId = i.PatientId,
                                     TaskActivities = i.TaskActivities,
                                     Title = i.Title,
                                     IsComplete = i.IsComplete,
                                     AlertActive = i.AlertActive,
                                     Status = i.Status,
                                     PatientDisplayName = i.PatientDisplayName,
                                     TaskActivityTypeName = i.TaskActivityTypeName,
                                     CreatedDateTime = i.CreatedDateTime
                                 })
                .ToExtendedObservableCollection();

            if (Arguments.AssignedByUserIds != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => Arguments.AssignedByUserIds.Contains(ta.CreatedByUserId))).ToList().ForEach(ta => task.TaskActivities.Remove(ta));
                }
            }

            if (assignedToUserIds != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.TaskActivityUsers.Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId)))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.Active.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.TaskActivityUsers.Any(tar => Arguments.Active != null && tar.Active == Arguments.Active.Value))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.AlertActive.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.TaskActivityUsers.Any(tar => Arguments.AlertActive != null && tar.AlertActive == Arguments.AlertActive.Value))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.TaskActivityTypeIDs != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => Arguments.TaskActivityTypeIDs.Contains(ta.TaskActivityTypeId))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.StartDate.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.CreatedDateTime.Date >= Arguments.StartDate.GetValueOrDefault().Date)).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.EndDate.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.CreatedDateTime.Date <= Arguments.EndDate.GetValueOrDefault().Date)).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }
        }

        public void CreateTask()
        {
            _taskViewManager.CreateTask();
        }

        public void ViewTask(TaskActivity taskActivity)
        {
            _taskViewManager.ViewTask(taskActivity);
        }
    }

    public class ReviewTasksArguments
    {
        public bool? Active;

        public bool? AlertActive;
        public int[] AssignedByUserIds;
        public int[] AssignedToGroupIds;
        public int[] AssignedToUserIds;

        public DateTime? EndDate;
        public int? PatientId;
        public DateTime? StartDate;
        public int[] TaskActivityTypeIDs;

        public ReviewTasksArguments()
        {

            PatientId = ApplicationContext.Current.PatientId;
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };

            Active = true;
        }

        public void ConfigureForCurrentUserActiveTasks()
        {
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };
            Active = true;
            PatientId = null;
        }

        public void ConfigureForCurrentUserTasksWithActiveAlerts()
        {
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };
            AlertActive = true;
            PatientId = null;
        }

        public void ConfigureForActivePatientActiveTasks()
        {
            PatientId = ApplicationContext.Current.PatientId;
            Active = true;
            AssignedByUserIds = null;
        }

        public void ConfigureForActivePatientTasksWithActiveAlerts()
        {
            PatientId = ApplicationContext.Current.PatientId;
            AlertActive = true;
            AssignedToUserIds = null;
        }
    }
}