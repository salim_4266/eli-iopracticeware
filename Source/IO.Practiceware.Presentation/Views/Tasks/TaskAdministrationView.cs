﻿using System;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class TaskAdministrationView
    {
        public TaskAdministrationView()
        {
            InitializeComponent();

            GroupsButton.Click += OnGroupsButtonClick;
            TypesButton.Click += OnTypesButtonClick;
            OKButton.Click += OnOkButtonClick;
            Usersbutton.Click += OnUsersButtonClick;
        }

        public TaskAdministrationPresenter Presenter { get; set; }

        private void OnGroupsButtonClick(object sender, EventArgs e)
        {
            Presenter.ShowGroupAdministration();
        }

        private void OnTypesButtonClick(object sender, EventArgs e)
        {
            Presenter.ShowTaskActivityTypeAdministration();
        }


        private void OnUsersButtonClick(object sender,EventArgs e)
        {
            Presenter.ShowUserAdministration();
        }

        private void OnOkButtonClick(object sender, EventArgs e)
        {
            InteractionContext.Complete(true);
        }
    }

    public class TaskAdministrationPresenter
    {
        private readonly TaskViewManager _taskViewManager;

        public TaskAdministrationPresenter(TaskViewManager taskViewManager)
        {
            _taskViewManager = taskViewManager;
        }

        public void ShowTaskActivityTypeAdministration()
        {
            _taskViewManager.ShowTaskActivityTypeAdministration();
        }

        public void ShowGroupAdministration()
        {
            _taskViewManager.ShowGroupAdministration();
        }

        public void ShowUserAdministration()
        {
            _taskViewManager.ShowUserAdministration();
        }
    }
}