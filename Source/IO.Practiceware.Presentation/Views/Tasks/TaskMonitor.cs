﻿using Hardcodet.Wpf.TaskbarNotification;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf.ComponentModel;
using Soaf.Presentation.Controls.WindowsForms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    [Singleton]
    public class TaskMonitor
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly object _syncRoot = new object();

        private readonly TaskIconPresenter _taskIconPresenter;
        private readonly TaskViewManager _taskViewManager;

        private List<int> _lastTaskActivityUserIdsWithAlert = new List<int>();
        private TaskIconView _taskIconView;
        private Timer _timer;
        private TaskbarIcon _taskBarIcon;
        private FancyBalloon _fancyBalloon;

        public TaskMonitor(IPracticeRepository practiceRepository, TaskViewManager taskViewManager, TaskIconPresenter taskIconPresenter)
        {
            _practiceRepository = practiceRepository;
            _taskViewManager = taskViewManager;
            _taskIconPresenter = taskIconPresenter;
                     
        }

        private const int PollingPeriod = 10000;

        public void Start()
        {            
            try
            {
                lock (_syncRoot)
                {
                    if (UserContext.Current.UserDetails != null)
                    {
                        if (_timer != null) Stop();

                        var t = new Thread(() =>
                        {
                            var openedTaskView = new TaskIconView {Presenter = _taskIconPresenter};
                            _taskIconView = openedTaskView;
                            System.Windows.Forms.Application.Run(openedTaskView);
                        });
                        t.SetApartmentState(ApartmentState.STA);
                        t.Start();

                        while (!_taskIconView.IsReady()) Thread.Sleep(20);

                        _timer = new Timer(OnTimerTick, null, 0, Timeout.Infinite);
                        _timer.Change(0, Timeout.Infinite);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        public void Stop()
        {
            try
            {
                lock (_syncRoot)
                {
                    if (_timer != null)
                    {
                        _timer.Dispose();
                        _timer = null;
                        _lastTaskActivityUserIdsWithAlert.Clear();

                        _taskIconView.IsClosePending = true;
                        var taskIconViewToClose = _taskIconView;
                        _taskIconView = null;

                        System.Threading.Tasks.Task.Factory.StartNew(() =>
                        {
                            taskIconViewToClose.Invoke(new Action(taskIconViewToClose.Close));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        public void Refresh()
        {
            lock (_syncRoot)
            {
                if (_timer != null) _timer.Change(0, Timeout.Infinite);
            }
        }

        private void OnTimerTick(object state)
        {
            try
            {
                Update();
            }
            catch(Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private void Update()
        {
            // Get on a dispatcher thread before acquiring lock to prevent deadlocks
            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                lock (_syncRoot)
                {
                    if (_timer == null) return;

                    if (UserContext.Current == null || UserContext.Current.UserDetails == null)
                    {
                        Stop();
                        return;
                    }

                    // Get active alerts for current user
                    var userActivities = System.Threading.Tasks.Task.Factory
                        .StartNew(() => _practiceRepository.TaskActivityUsers
                            .Where(tar => tar.AssignedToUserId == UserContext.Current.UserDetails.Id
                                            && (tar.AlertActive || tar.Active))
                            .ToArray())
                        .Result;

                    var userActiveAlerts = userActivities.Where(a => a.AlertActive).ToArray();
                    var userTasks = userActivities.Where(a => a.Active).ToArray();

                    // Save current list of alerts we got
                    var previousUserActiveAlerts = _lastTaskActivityUserIdsWithAlert;
                    _lastTaskActivityUserIdsWithAlert = userActiveAlerts.Select(tar => tar.Id).ToList();

                    if (userActiveAlerts.Any())
                    {
                        // Some new alerts? -> trigger ballon
                        if (userActiveAlerts.Any(tar => !previousUserActiveAlerts.Contains(tar.Id)))
                        {
                            UpdateAlertFormText(userActiveAlerts);
                        }
                    }
                    else
                    {
                        // Remove alert since no more tasks with alerts
                        RemoveAlertForm();
                    }

                    // Update 
                    _taskIconPresenter.TaskActivityUsers = userTasks;

                    _timer.Change(PollingPeriod, Timeout.Infinite);
                }

            }));
        }

        private void RemoveAlertForm()
        {
            if (_taskBarIcon != null)
            {
                _taskBarIcon.CloseBalloon();
            }
        }

        private void UpdateAlertFormText(IEnumerable<TaskActivityUser> taskActivityResources)
        {
            if (_fancyBalloon == null)
            {
                _fancyBalloon = new FancyBalloon();
                _fancyBalloon.PreviewMouseLeftButtonDown += FancyBalloonPreviewMouseLeftButtonDown;
            }
            _fancyBalloon.BalloonText = string.Format("You have {0} active tasks with alarms.", taskActivityResources.Count());
            if (_taskBarIcon == null)
            {
                _taskBarIcon = new TaskbarIcon();
            }
            //show balloon and close it after 8 seconds
            if (!_fancyBalloon.IsVisible)
            {
                _taskBarIcon.ShowCustomBalloon(_fancyBalloon, PopupAnimation.Slide, 8000);
                _fancyBalloon = null;
            }
        }

        private void FancyBalloonPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Close alert
            RemoveAlertForm();

            // Clicked close button? -> do nothing
            if(e.OriginalSource is System.Windows.Controls.Image 
                && ((System.Windows.Controls.Image)(e.OriginalSource)).Name.Equals("imgClose"))
            {
                return;
            }

            _taskViewManager.ReviewCurrentUserTasksWithActiveAlert();
        }
    }
}