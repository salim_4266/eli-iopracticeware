﻿
namespace IO.Practiceware.Presentation.Views.Tasks
{    
    partial class TaskActivityTypeAdministrationView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {
        
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.DefaultContentTextBox = new Soaf.Presentation.Controls.WindowsForms.TextBox();
            this.DefaultContentLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.TypesListBox = new Soaf.Presentation.Controls.WindowsForms.OrderableListBox();
            this.ListPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
            this.DeleteTypeButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.AddTypeButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.AlertButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.LinkToPatientButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.AssignIndividuallyButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.DefaultsLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ListPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DefaultContentTextBox
            // 
            this.DefaultContentTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DefaultContentTextBox.Location = new System.Drawing.Point(247, 33);
            this.DefaultContentTextBox.Multiline = true;
            this.DefaultContentTextBox.Name = "DefaultContentTextBox";
            this.DefaultContentTextBox.Size = new System.Drawing.Size(297, 296);
            this.DefaultContentTextBox.TabIndex = 1;
            // 
            // DefaultContentLabel
            // 
            this.DefaultContentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DefaultContentLabel.Location = new System.Drawing.Point(247, 6);
            this.DefaultContentLabel.Name = "DefaultContentLabel";
            this.DefaultContentLabel.Size = new System.Drawing.Size(81, 20);
            this.DefaultContentLabel.TabIndex = 2;
            this.DefaultContentLabel.Text = "Default Text";
            // 
            // TypesListBox
            // 
            this.TypesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TypesListBox.DisplayMemberPath = "";
            this.TypesListBox.Location = new System.Drawing.Point(2, 79);
            this.TypesListBox.Margin = new System.Windows.Forms.Padding(0);
            this.TypesListBox.Name = "TypesListBox";
            this.TypesListBox.SelectedIndex = -1;
            this.TypesListBox.SelectedValuePath = "";
            this.TypesListBox.Size = new System.Drawing.Size(218, 250);
            this.TypesListBox.TabIndex = 3;
            // 
            // ListPanel
            // 
            this.ListPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ListPanel.Controls.Add(this.DeleteTypeButton);
            this.ListPanel.Controls.Add(this.AddTypeButton);
            this.ListPanel.Controls.Add(this.TypesListBox);
            this.ListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListPanel.Location = new System.Drawing.Point(0, 0);
            this.ListPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ListPanel.Name = "ListPanel";
            this.ListPanel.Size = new System.Drawing.Size(231, 336);
            this.ListPanel.TabIndex = 4;
            // 
            // DeleteTypeButton
            // 
            this.DeleteTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteTypeButton.Location = new System.Drawing.Point(120, 7);
            this.DeleteTypeButton.Name = "DeleteTypeButton";
            this.DeleteTypeButton.Size = new System.Drawing.Size(100, 60);
            this.DeleteTypeButton.TabIndex = 5;
            this.DeleteTypeButton.Text = "Delete Type";
            // 
            // AddTypeButton
            // 
            this.AddTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddTypeButton.Location = new System.Drawing.Point(5, 7);
            this.AddTypeButton.Name = "AddTypeButton";
            this.AddTypeButton.Size = new System.Drawing.Size(100, 60);
            this.AddTypeButton.TabIndex = 4;
            this.AddTypeButton.Text = "Add Type";
            // 
            // AlertButton
            // 
            this.AlertButton.Location = new System.Drawing.Point(550, 37);
            this.AlertButton.Name = "AlertButton";
            this.AlertButton.Size = new System.Drawing.Size(111, 30);
            this.AlertButton.TabIndex = 5;
            this.AlertButton.Text = "Alarm";
            // 
            // LinkToPatientButton
            // 
            this.LinkToPatientButton.Location = new System.Drawing.Point(550, 111);
            this.LinkToPatientButton.Name = "LinkToPatientButton";
            this.LinkToPatientButton.Size = new System.Drawing.Size(111, 30);
            this.LinkToPatientButton.TabIndex = 6;
            this.LinkToPatientButton.Text = "Link To Patient";
            // 
            // AssignIndividuallyButton
            // 
            this.AssignIndividuallyButton.Location = new System.Drawing.Point(550, 74);
            this.AssignIndividuallyButton.Name = "AssignIndividuallyButton";
            this.AssignIndividuallyButton.Size = new System.Drawing.Size(111, 30);
            this.AssignIndividuallyButton.TabIndex = 7;
            this.AssignIndividuallyButton.Text = "Assign Individually";
            // 
            // DefaultsLabel
            // 
            this.DefaultsLabel.Location = new System.Drawing.Point(550, 6);
            this.DefaultsLabel.Name = "DefaultsLabel";
            this.DefaultsLabel.Size = new System.Drawing.Size(114, 21);
            this.DefaultsLabel.TabIndex = 8;
            this.DefaultsLabel.Text = "Default Options:";
            // 
            // TaskActivityTypeAdministrationView
            // 
            this.Controls.Add(this.DefaultsLabel);
            this.Controls.Add(this.AssignIndividuallyButton);
            this.Controls.Add(this.LinkToPatientButton);
            this.Controls.Add(this.AlertButton);
            this.Controls.Add(this.ListPanel);
            this.Controls.Add(this.DefaultContentLabel);
            this.Controls.Add(this.DefaultContentTextBox);
            this.MinimumSize = new System.Drawing.Size(674, 336);
            this.Name = "TaskActivityTypeAdministrationView";
            this.Size = new System.Drawing.Size(674, 336);
            this.ListPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        internal Soaf.Presentation.Controls.WindowsForms.TextBox DefaultContentTextBox;
        internal Soaf.Presentation.Controls.WindowsForms.Label DefaultContentLabel;
        internal Soaf.Presentation.Controls.WindowsForms.OrderableListBox TypesListBox;
        internal Soaf.Presentation.Controls.WindowsForms.Panel ListPanel;
        internal Soaf.Presentation.Controls.WindowsForms.Button DeleteTypeButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button AddTypeButton;
        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton AlertButton;
        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton LinkToPatientButton;
        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton AssignIndividuallyButton;

        internal Soaf.Presentation.Controls.WindowsForms.Label DefaultsLabel;
    }
}
