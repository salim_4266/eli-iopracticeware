﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.Tasks;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class ViewTaskView
    {
        private readonly PresentationBackgroundWorker _worker;


        public ViewTaskView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            OKButton.Click += OnOkButtonClick;
            ForwardTaskButton.Click += OnForwardTaskButtonClick;
            DeactivateAlarmButton.Click += OnDeactivateAlarmButtonClick;
            CompleteTaskButton.Click += OnCompleteTaskButtonClick;
            DescriptionText.TextChanged += OnDescriptionTextChanged;
            TitleText.TextChanged += OnTitleTextChanged;
            TaskActivityTypesList.SelectionChanged += OnTaskTypeChanged;
        }

        public ViewTaskPresenter Presenter { get; set; }

        private void OnPresenterLoaded()
        {
            TaskActivityTypesList.ItemsSource = Presenter.TaskActivityTypes;
            TaskActivityTypesList.DisplayMemberPath = "Name";
            TaskActivityTypesList.SelectedItem =
                Presenter.TaskActivityTypes.FirstOrDefault(i => Presenter.TaskActivity.TaskActivityTypeId == i.Id) ??
                Presenter.TaskActivityTypes.FirstOrDefault();
            TaskActivityTypesList.Refresh();

            TitleText.Text = Presenter.TaskActivity.Title;
            DescriptionText.Text = Presenter.TaskActivity.Description;
            CreatedLabel.Text = Presenter.TaskActivity.CreatedDateTime.ToString();

            foreach (var user in Presenter.AssignedToUsers)
            {
                AssignedToText.Text += string.Format("{0},", user);
            }

            // hide buttons as appropriate
            if (
                (!Presenter.TaskActivity.TaskActivityUsers.Any(
                    tar => tar.Active & tar.AssignedToUserId == UserContext.Current.UserDetails.Id)))
            {
                CompleteTaskButton.Visible = false;
                ForwardTaskButton.Visible = false;
            }
            if (
                (!Presenter.TaskActivity.TaskActivityUsers.Any(
                    tar => tar.AlertActive & tar.AssignedToUserId == UserContext.Current.UserDetails.Id)))
            {
                DeactivateAlarmButton.Visible = false;
            }
        }

        private void LoadPresenter()
        {
            var presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _worker.RunWorkerAsync();
        }

        private void OnOkButtonClick(Object sender, EventArgs e)
        {
            var bw = new PresentationBackgroundWorker(this);
            bw.DoWork += delegate { Presenter.Save(); };
            bw.RunWorkerCompleted += delegate { InteractionContext.Complete(true); };
            bw.RunWorkerAsync();
        }

        /// <summary>
        ///   Complete the task and re-asssign it to someone else
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        /// <remarks>
        /// </remarks>
        private void OnForwardTaskButtonClick(Object sender, EventArgs e)
        {
            Presenter.AssignTask();
            InteractionContext.Complete(true);
        }

        /// <summary>
        ///   Don't complete the task but deactivate the alarm
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        /// <remarks>
        /// </remarks>
        private void OnDeactivateAlarmButtonClick(Object sender, EventArgs e)
        {
            var bw = new PresentationBackgroundWorker(this);
            bw.DoWork += delegate { Presenter.DeactivateTaskAlarm(); };
            bw.RunWorkerCompleted += delegate { InteractionContext.Complete(true); };
            bw.RunWorkerAsync();

        }

        /// <summary>
        ///   Complete the task (and deactivate the alarm)
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        /// <remarks>
        /// </remarks>
        private void OnCompleteTaskButtonClick(Object sender, EventArgs e)
        {
            var bw = new PresentationBackgroundWorker(this);
            bw.DoWork += delegate { Presenter.CompleteTask(); };
            bw.RunWorkerCompleted += delegate { InteractionContext.Complete(true); };
            bw.RunWorkerAsync();

        }

        private void OnDescriptionTextChanged(Object sender, EventArgs e)
        {
            Presenter.TaskActivity.Description = DescriptionText.Text;

        }

        private void OnTitleTextChanged(Object sender, EventArgs e)
        {
            Presenter.TaskActivity.Title = TitleText.Text;
            Presenter.TaskActivity.Task.Title = TitleText.Text;
        }

        private void OnTaskTypeChanged(object sender, EventArgs e)
        {
            var selectedTaskActivityType = TaskActivityTypesList.SelectedItem as TaskActivityType;

            if (selectedTaskActivityType != null && Presenter.TaskActivity != null && selectedTaskActivityType.Id != Presenter.TaskActivity.TaskActivityTypeId)
            {
                Presenter.TaskActivity.TaskActivityTypeId = selectedTaskActivityType.Id;
            }
        }
    }

    public class ViewTaskPresenter
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly ITaskService _taskService;
        private readonly TaskViewManager _taskViewManager;

        private IEnumerable<TaskActivityType> _taskActivityTypes;

        public ViewTaskPresenter(IPracticeRepository practiceRepository, ITaskService taskService, TaskViewManager taskViewManager)
        {
            _practiceRepository = practiceRepository;
            _taskService = taskService;
            _taskViewManager = taskViewManager;
        }

        public IEnumerable<TaskActivityType> TaskActivityTypes
        {
            get { return _taskActivityTypes; }
        }

        public IEnumerable<string> AssignedToUsers
        {
            get { return TaskActivity.TaskActivityUsers.Select(tar => tar.AssignedTo.DisplayName); }
        }

        public TaskActivity TaskActivity { get; set; }

        public void Load()
        {
            _taskActivityTypes = _practiceRepository.TaskActivityTypes.OrderBy(tat => tat.Name).ToArray();
        }

        public void Save()
        {
            _practiceRepository.Save(TaskActivity);
        }

        public void AssignTask()
        {
            _taskViewManager.AssignTask(TaskActivity);
        }

        public void DeactivateTaskAlarm()
        {
            _taskService.DeactivateTaskAlarm(TaskActivity);
            _practiceRepository.Save(TaskActivity);
        }

        public void CompleteTask()
        {
            _taskService.CompleteTask(TaskActivity);
            _practiceRepository.Save(TaskActivity);
        }
    }
}