﻿using Soaf.Presentation.Controls.WindowsForms;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    partial class OtherTasksView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.Grid = new Soaf.Presentation.Controls.WindowsForms.GridView();
            this.FilterPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
            this.StatusFilterList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.StatusLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ClearButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.EndDatePicker = new Soaf.Presentation.Controls.WindowsForms.DatePicker();
            this.EndDateFilterLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.StartDatePicker = new Soaf.Presentation.Controls.WindowsForms.DatePicker();
            this.StartDateFilterLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.TaskActivityTypesFilterLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.TaskActivityTypesFilterList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.UsersFilterLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.UsersFilterList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.GroupsFilterLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.GroupsFilterList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.FilterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grid
            // 
            this.Grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grid.Location = new System.Drawing.Point(0, 137);
            this.Grid.Name = "Grid";
            this.Grid.Size = new System.Drawing.Size(788, 304);
            this.Grid.TabIndex = 0;
            // 
            // FilterPanel
            // 
            this.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.FilterPanel.Controls.Add(this.StatusFilterList);
            this.FilterPanel.Controls.Add(this.StatusLabel);
            this.FilterPanel.Controls.Add(this.ClearButton);
            this.FilterPanel.Controls.Add(this.EndDatePicker);
            this.FilterPanel.Controls.Add(this.EndDateFilterLabel);
            this.FilterPanel.Controls.Add(this.StartDatePicker);
            this.FilterPanel.Controls.Add(this.StartDateFilterLabel);
            this.FilterPanel.Controls.Add(this.TaskActivityTypesFilterLabel);
            this.FilterPanel.Controls.Add(this.TaskActivityTypesFilterList);
            this.FilterPanel.Controls.Add(this.UsersFilterLabel);
            this.FilterPanel.Controls.Add(this.UsersFilterList);
            this.FilterPanel.Controls.Add(this.GroupsFilterLabel);
            this.FilterPanel.Controls.Add(this.GroupsFilterList);
            this.FilterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.FilterPanel.Location = new System.Drawing.Point(0, 0);
            this.FilterPanel.Name = "FilterPanel";
            this.FilterPanel.Size = new System.Drawing.Size(788, 137);
            this.FilterPanel.TabIndex = 1;
            // 
            // StatusFilterList
            // 
            this.StatusFilterList.DisplayMemberPath = "";
            this.StatusFilterList.Location = new System.Drawing.Point(355, 31);
            this.StatusFilterList.Name = "StatusFilterList";
            this.StatusFilterList.SelectedIndex = -1;
            this.StatusFilterList.SelectedValuePath = "";
            this.StatusFilterList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.StatusFilterList.Size = new System.Drawing.Size(110, 100);
            this.StatusFilterList.TabIndex = 19;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Location = new System.Drawing.Point(355, 8);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(75, 20);
            this.StatusLabel.TabIndex = 17;
            this.StatusLabel.Text = "With Status";
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(618, 32);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(79, 60);
            this.ClearButton.TabIndex = 10;
            this.ClearButton.Text = "Clear Dates";
            // 
            // EndDatePicker
            // 
            this.EndDatePicker.Location = new System.Drawing.Point(520, 66);
            this.EndDatePicker.Name = "EndDatePicker";
            this.EndDatePicker.Size = new System.Drawing.Size(81, 21);
            this.EndDatePicker.TabIndex = 9;
            // 
            // EndDateFilterLabel
            // 
            this.EndDateFilterLabel.Location = new System.Drawing.Point(478, 67);
            this.EndDateFilterLabel.Name = "EndDateFilterLabel";
            this.EndDateFilterLabel.Size = new System.Drawing.Size(36, 20);
            this.EndDateFilterLabel.TabIndex = 8;
            this.EndDateFilterLabel.Text = "To";
            // 
            // StartDatePicker
            // 
            this.StartDatePicker.Location = new System.Drawing.Point(520, 31);
            this.StartDatePicker.Name = "StartDatePicker";
            this.StartDatePicker.Size = new System.Drawing.Size(81, 21);
            this.StartDatePicker.TabIndex = 7;
            // 
            // StartDateFilterLabel
            // 
            this.StartDateFilterLabel.Location = new System.Drawing.Point(478, 32);
            this.StartDateFilterLabel.Name = "StartDateFilterLabel";
            this.StartDateFilterLabel.Size = new System.Drawing.Size(46, 20);
            this.StartDateFilterLabel.TabIndex = 6;
            this.StartDateFilterLabel.Text = "From";
            // 
            // TaskActivityTypesFilterLabel
            // 
            this.TaskActivityTypesFilterLabel.Location = new System.Drawing.Point(237, 5);
            this.TaskActivityTypesFilterLabel.Name = "TaskActivityTypesFilterLabel";
            this.TaskActivityTypesFilterLabel.Size = new System.Drawing.Size(54, 23);
            this.TaskActivityTypesFilterLabel.TabIndex = 5;
            this.TaskActivityTypesFilterLabel.Text = "Types";
            // 
            // TaskActivityTypesFilterList
            // 
            this.TaskActivityTypesFilterList.DisplayMemberPath = "";
            this.TaskActivityTypesFilterList.ItemsSource = new string[] {
        "Loading..."};
            this.TaskActivityTypesFilterList.Location = new System.Drawing.Point(237, 31);
            this.TaskActivityTypesFilterList.Name = "TaskActivityTypesFilterList";
            this.TaskActivityTypesFilterList.SelectedIndex = -1;
            this.TaskActivityTypesFilterList.SelectedValuePath = "";
            this.TaskActivityTypesFilterList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.TaskActivityTypesFilterList.Size = new System.Drawing.Size(110, 100);
            this.TaskActivityTypesFilterList.TabIndex = 4;
            // 
            // UsersFilterLabel
            // 
            this.UsersFilterLabel.Location = new System.Drawing.Point(121, 5);
            this.UsersFilterLabel.Name = "UsersFilterLabel";
            this.UsersFilterLabel.Size = new System.Drawing.Size(66, 23);
            this.UsersFilterLabel.TabIndex = 3;
            this.UsersFilterLabel.Text = "Users";
            // 
            // UsersFilterList
            // 
            this.UsersFilterList.DisplayMemberPath = "";
            this.UsersFilterList.ItemsSource = new string[] {
        "Loading..."};
            this.UsersFilterList.Location = new System.Drawing.Point(121, 31);
            this.UsersFilterList.Name = "UsersFilterList";
            this.UsersFilterList.SelectedIndex = -1;
            this.UsersFilterList.SelectedValuePath = "";
            this.UsersFilterList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.UsersFilterList.Size = new System.Drawing.Size(110, 100);
            this.UsersFilterList.TabIndex = 2;
            // 
            // GroupsFilterLabel
            // 
            this.GroupsFilterLabel.Location = new System.Drawing.Point(5, 5);
            this.GroupsFilterLabel.Name = "GroupsFilterLabel";
            this.GroupsFilterLabel.Size = new System.Drawing.Size(47, 23);
            this.GroupsFilterLabel.TabIndex = 1;
            this.GroupsFilterLabel.Text = "Groups";
            // 
            // GroupsFilterList
            // 
            this.GroupsFilterList.DisplayMemberPath = "";
            this.GroupsFilterList.ItemsSource = new string[] {
        "Loading..."};
            this.GroupsFilterList.Location = new System.Drawing.Point(5, 31);
            this.GroupsFilterList.Name = "GroupsFilterList";
            this.GroupsFilterList.SelectedIndex = -1;
            this.GroupsFilterList.SelectedValuePath = "";
            this.GroupsFilterList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GroupsFilterList.Size = new System.Drawing.Size(110, 100);
            this.GroupsFilterList.TabIndex = 0;
            // 
            // OtherTasksView
            // 
            this.Controls.Add(this.Grid);
            this.Controls.Add(this.FilterPanel);
            this.Name = "OtherTasksView";
            this.Size = new System.Drawing.Size(788, 441);
            this.FilterPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal Soaf.Presentation.Controls.WindowsForms.GridView Grid;
        internal Soaf.Presentation.Controls.WindowsForms.Panel FilterPanel;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox GroupsFilterList;
        internal Soaf.Presentation.Controls.WindowsForms.Label UsersFilterLabel;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox UsersFilterList;
        internal Soaf.Presentation.Controls.WindowsForms.Label GroupsFilterLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label StartDateFilterLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label TaskActivityTypesFilterLabel;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox TaskActivityTypesFilterList;
        internal DatePicker EndDatePicker;
        internal Soaf.Presentation.Controls.WindowsForms.Label EndDateFilterLabel;
        internal DatePicker StartDatePicker;
        internal Soaf.Presentation.Controls.WindowsForms.Button ClearButton;
        internal Soaf.Presentation.Controls.WindowsForms.Label StatusLabel;
        private ListBox StatusFilterList;
    }

}
