﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using GridViewColumn = Soaf.Presentation.Controls.GridViewColumn;

namespace IO.Practiceware.Presentation.Views.Tasks
{
    public partial class OtherTasksView
    {
        private readonly PresentationBackgroundWorker _worker;

        private bool _suspendEvents;


        public OtherTasksView()
        {
            _suspendEvents = true;
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            Header = "View Tasks";
            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            StartDatePicker.Text = string.Empty;
            EndDatePicker.Text = string.Empty;
            Grid.IsReadOnly = true;
            Grid.AutoGenerateColumns = false;
            Grid.AutoGenerateRelations = false;

            ClearButton.Click += OnClearButtonClick;
            StartDatePicker.SelectionChanged += OnStartDatePickerValueChanged;
            EndDatePicker.SelectionChanged += OnEndDatePickerValueChanged;
            Grid.PreviewMouseDown += OnGridViewClick;


            GroupsFilterList.SelectionChanged += OnGroupsFilterListSelectionChanged;
            TaskActivityTypesFilterList.SelectionChanged += OnTaskActivityTypesFilterListSelectionChanged;
            UsersFilterList.SelectionChanged += OnUsersFilterListSelectionChanged;
            StatusFilterList.SelectionChanged += OnStatusFilterListSelectionChanged;
            StatusFilterList.SelectedIndex = 0;
        }

        public OtherTasksViewPresenter Presenter { get; set; }


        private void LoadPresenter()
        {
            OtherTasksViewPresenter presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var taskColumns = new List<GridViewColumn>
                                  {
                                      new GridViewColumn(new Binding("TaskActivityTypeName"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Type",
                                              Width = 170
                                          },
                                      new GridViewColumn(new Binding("Title"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Title",
                                              Width = 100
                                          },
                                      new GridViewColumn(new Binding("Status"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Status",
                                              Width = 170
                                          },
                                      new GridViewColumn(new Binding("PatientDisplayName"))
                                          {
                                              IsReadOnly = true,
                                              Header = "PatientName",
                                              Width = 200
                                          },
                                      new GridViewColumn(new Binding("CreatedDateTime"))
                                          {
                                              IsReadOnly = true,
                                              Header = "Createdon",
                                              Width = 200
                                          },
                                      new GridViewColumn(new Binding("AssignedTo"))
                                          {
                                              IsReadOnly = true,
                                              Header = "AssignedTo",
                                              Width = 150
                                          }
                                  };
            taskColumns.ToList().ForEach(Grid.Columns.Add);
            _worker.RunWorkerAsync();
        }


        public void OnPresenterLoaded()
        {
            _suspendEvents = true;

            GroupsFilterList.ItemsSource = Presenter.Groups;
            GroupsFilterList.DisplayMemberPath = "Name";
            GroupsFilterList.UnselectAll();

            if ((Presenter.Arguments.AssignedToGroupIds != null))
            {
                foreach (Group @group in GroupsFilterList.ItemsSource)
                {
                    if ((Presenter.Arguments.AssignedToGroupIds.Contains(@group.Id)))
                    {
                        GroupsFilterList.SelectedItems.Add(@group);
                    }
                }
            }

            UsersFilterList.ItemsSource = Presenter.Users;
            UsersFilterList.DisplayMemberPath = "UserName";
            UsersFilterList.UnselectAll();
            if ((Presenter.Arguments.AssignedToUserIds != null))
            {
                if (Presenter.Arguments.AssignedToUserIds.Contains(0))
                {
                    UsersFilterList.UnselectAll();
                    UsersFilterList.SelectedItems.Add(
                        UsersFilterList.ItemsSource.Cast<User>().FirstOrDefault(user => user.Id == 0));
                }
                else
                {
                    foreach (var user in UsersFilterList.ItemsSource.Cast<User>().Where(user => (Presenter.Arguments.AssignedToUserIds.Contains(user.Id))))
                    {
                        UsersFilterList.SelectedItems.Add(user);
                    }
                }
            }
            

            StatusFilterList.ItemsSource = Presenter.StatusChoices.AsQueryable();
            StatusFilterList.UnselectAll();
            if (Presenter.Arguments.AssignedStatus != null)
            {
                if (Presenter.Arguments.AssignedStatus.Contains("All"))
                {
                    StatusFilterList.SelectedItems.Add(
                        StatusFilterList.ItemsSource.Cast<string>().FirstOrDefault(s => s == "All"));
                }
                else
                {
                    foreach (var item in
                        StatusFilterList.ItemsSource.Cast<string>()
                            .Where(s => Presenter.Arguments.AssignedStatus.Contains(s)))
                    {
                        StatusFilterList.SelectedItems.Add(item);
                    }
                }
            }

            TaskActivityTypesFilterList.ItemsSource = Presenter.TaskActivityTypes;
            TaskActivityTypesFilterList.DisplayMemberPath = "Name";
            TaskActivityTypesFilterList.UnselectAll();
            if ((Presenter.Arguments.TaskActivityTypeIDs != null))
            {
                foreach (TaskActivityType taskActivityType in TaskActivityTypesFilterList.ItemsSource)
                {
                    if ((Presenter.Arguments.TaskActivityTypeIDs.Contains(taskActivityType.Id)))
                    {
                        TaskActivityTypesFilterList.SelectedItems.Add(taskActivityType);
                    }
                }
            }

            if ((Presenter.Arguments.StartDate.HasValue))
            {
                StartDatePicker.SelectedValue = Convert.ToDateTime(Presenter.Arguments.StartDate);
            }
            if ((Presenter.Arguments.EndDate.HasValue))
            {
                EndDatePicker.SelectedValue = Convert.ToDateTime(Presenter.Arguments.EndDate);
            }

            object selectedItem = Grid.SelectedItem;

            Grid.ItemsSource = Presenter.Tasks;

            Grid.SelectedItem = selectedItem;

            _suspendEvents = false;
        }


        private void OnGridViewClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;
            object selectedItem = source == null ? null : source.DataContext;

            if (e.ClickCount == 2 && e.ChangedButton == MouseButton.Left)
            {
                var taskActivity = selectedItem as TaskActivity;
                if (taskActivity != null)
                {
                    Presenter.ViewTask(taskActivity);
                    _worker.RunWorkerAsync();
                }
                else
                {
                    var task = selectedItem as TaskViewModel;
                    if ((task != null))
                    {
                        taskActivity = task.TaskActivities.FirstOrDefault(ta => !ta.IsComplete);
                        if ((taskActivity != null))
                        {
                            Presenter.ViewTask(taskActivity);
                            _worker.RunWorkerAsync();
                        }
                    }
                }
            }
        }

        private void OnGroupsFilterListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                Presenter.Arguments.AssignedToGroupIds = GroupsFilterList.SelectedItems.Count > 0
                                                             ? GroupsFilterList.SelectedItems.Cast<Group>().Select(g => g.Id).ToArray()
                                                             : null;

                var groupUsers = GroupsFilterList.SelectedItems.Count > 0
                                     ? GroupsFilterList.SelectedItems.Cast<Group>().Select(g => g.GroupUsers)
                                     : null;

                Presenter.Arguments.AssignedToUserIds = groupUsers != null ? groupUsers.SelectMany(gu => gu.Select(u => u.UserId)).ToArray() : UsersFilterList.ItemsSource.Cast<User>().Select(user => user.Id).ToArray();

                _worker.RunWorkerAsync();
            }
        }

        private void OnUsersFilterListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_suspendEvents)
            {
                var firstElement = e.AddedItems.Cast<User>().FirstOrDefault(user => user.Id == 0);
                if (firstElement != null)
                {
                    Presenter.Arguments.AssignedToUserIds = UsersFilterList.ItemsSource.Cast<User>().Select(user => user.Id).ToArray();
                }
                else
                {
                    Presenter.Arguments.AssignedToUserIds = UsersFilterList.SelectedItems.Count > 0
                                                                ? UsersFilterList.SelectedItems.Cast<User>()
                                                                      .Where(r => r.Id != 0).Select(r => r.Id).ToArray()
                                                                : null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnStatusFilterListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_suspendEvents)
            {
                var firstElement = e.AddedItems.Cast<string>().FirstOrDefault(s => s == "All");
                if (firstElement != null)
                {
                    foreach (var item in StatusFilterList.ItemsSource)
                    {
                        Presenter.Arguments.AssignedStatus.Add(item.ToString());
                    }
                }
                else
                {
                    Presenter.Arguments.AssignedStatus = StatusFilterList.SelectedItems.Count > 0
                                                             ? StatusFilterList.SelectedItems.Cast<string>()
                                                                   .Where(s => s != "All").ToList()
                                                             : null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnTaskActivityTypesFilterListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                Presenter.Arguments.TaskActivityTypeIDs = TaskActivityTypesFilterList.SelectedItems.Count > 0
                                                              ? TaskActivityTypesFilterList.SelectedItems.Cast<TaskActivityType>()
                                                                    .Select(tat => tat.Id).ToArray()
                                                              : null;
                _worker.RunWorkerAsync();
            }
        }

        private void OnStartDatePickerValueChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                if (StartDatePicker.SelectedValue.HasValue)
                {
                    Presenter.Arguments.StartDate = StartDatePicker.SelectedValue.Value.Date;
                }
                else
                {
                    Presenter.Arguments.StartDate = null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnEndDatePickerValueChanged(object sender, EventArgs e)
        {
            if ((!_suspendEvents))
            {
                if ((EndDatePicker.SelectedValue.HasValue))
                {
                    Presenter.Arguments.EndDate = EndDatePicker.SelectedValue.Value.Date;
                }
                else
                {
                    Presenter.Arguments.EndDate = null;
                }
                _worker.RunWorkerAsync();
            }
        }

        private void OnClearButtonClick(object sender, EventArgs e)
        {
            _suspendEvents = true;
            StartDatePicker.SelectedValue = null;
            EndDatePicker.SelectedValue = null;
            Presenter.Arguments.StartDate = null;
            Presenter.Arguments.EndDate = null;
            _suspendEvents = false;
            _worker.RunWorkerAsync();
        }
    }


    public class OtherTasksViewPresenter
    {
        private readonly OtherTasksArguments _arguments = new OtherTasksArguments();
        private readonly IPracticeRepository _practiceRepository;
        private readonly TaskViewManager _taskViewManager;
        public string[] PatientChoices = new string[4];
        public string[] StatusChoices;
        private Patient _activePatient;
        private User _activeUser;
        private IEnumerable<Group> _groups;
        private IEnumerable<TaskActivityType> _taskActivityTypes;
        private IEnumerable<TaskViewModel> _tasks;
        private IEnumerable<User> _users;

        public OtherTasksViewPresenter(IPracticeRepository practiceRepository, TaskViewManager taskViewManager)
        {
            _practiceRepository = practiceRepository;
            _taskViewManager = taskViewManager;
            PatientChoices[0] = "Go to Patient--";
            PatientChoices[1] = "Chart";
            PatientChoices[2] = "Financials";
            PatientChoices[3] = "Info";

            StatusChoices = new[]
                                {
                                    "Active",
                                    "Alarm Active",
                                    "Complete",
                                    "All"
                                };
        }

        public TaskViewManager TaskViewManager
        {
            get { return _taskViewManager; }
        }

        public Patient ActivePatient
        {
            get { return _activePatient; }
        }

        public User ActiveUser
        {
            get { return _activeUser; }
        }

        public IEnumerable<TaskViewModel> Tasks
        {
            get { return _tasks; }
        }

        public IEnumerable<User> Users
        {
            get { return _users; }
        }

        public IEnumerable<Group> Groups
        {
            get { return _groups; }
        }

        public IEnumerable<TaskActivityType> TaskActivityTypes
        {
            get { return _taskActivityTypes; }
        }

        public OtherTasksArguments Arguments
        {
            get { return _arguments; }
        }

        public void OnRequestingOpenTaskPatient(Task patientTask, string patientScreenToOpen)
        {
            TaskViewManager.OnRequestingOpenTaskPatient(patientTask, patientScreenToOpen);
        }


        public void Load()
        {
            if (UserContext.Current.UserDetails.Id != 0)
            {
                _activeUser = _practiceRepository.Users.Where(u => !u.IsArchived).FirstOrDefault(r => r.Id == UserContext.Current.UserDetails.Id);
            }
            if (ApplicationContext.Current.PatientId.HasValue)
            {
                _activePatient = _practiceRepository.Patients.FirstOrDefault(pd => ApplicationContext.Current.PatientId != null && pd.Id == ApplicationContext.Current.PatientId.Value);
            }
            _users = new List<User>
                    {
                        new User {UserName = "All Users", Id = 0}
                    }
                    .Union(_practiceRepository.Users.Where(u => !u.IsArchived).OrderBy(r => r.UserName)).ToArray();
                         
            _groups = _practiceRepository.Groups.Include(gu => gu.GroupUsers).OrderBy(g => g.Name).ToArray();

            _taskActivityTypes = _practiceRepository.TaskActivityTypes.OrderBy(tat => tat.Name).ToArray();


            IQueryable<Task> tasks = _practiceRepository.Tasks.Include(t => t.Patient)
                .Include(t => t.TaskActivities.Select(ta => new object[]
                                                                {
                                                                    ta.TaskActivityUsers.Select(tar => tar.AssignedTo), ta.TaskActivityType, ta.CreatedBy
                                                                }));

            if (Arguments.PatientId.HasValue)
            {
                tasks = tasks.Where(t => Arguments.PatientId != null && (t.PatientId.HasValue && t.PatientId.Value == Arguments.PatientId.Value));
            }

            if (Arguments.AssignedByUserIds != null)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => Arguments.AssignedByUserIds.Contains(ta.CreatedByUserId)));
            }

            List<int> assignedToUserIds = null;
            if (Arguments.AssignedToUserIds != null)
            {
                assignedToUserIds = Arguments.AssignedToUserIds.ToList();
            }

            if ((Arguments.AssignedToGroupIds != null))
            {
                if ((assignedToUserIds == null))
                {
                    assignedToUserIds = new List<int>();
                }
                assignedToUserIds.AddRange(_practiceRepository.Groups.Where(g => Arguments.AssignedToGroupIds.Contains(g.Id)).SelectMany(g => g.GroupUsers).Select(gr => gr.UserId));
            }

            if (assignedToUserIds != null)
            {
                assignedToUserIds = assignedToUserIds.Distinct().ToList();

                tasks = tasks.Where(t => t.TaskActivities.Any(ta => ta.TaskActivityUsers.Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId))));
            }
            if (Arguments.AssignedStatus != null)
            {
                List<String> assignedStatus = Arguments.AssignedStatus.Distinct().ToList();
                if (!assignedStatus.Contains("All"))
                {
                    IQueryable<Task> tasksActive = null;
                    IQueryable<Task> tasksAlarmActive = null;
                    IQueryable<Task> tasksComplete = null;
                    IQueryable<Task> tasksTemp = null;
                    if (assignedStatus.Contains("Active"))
                    {
                        tasksActive = tasks.Where(t => t.TaskActivities
                                                           .Any(ta => ta.TaskActivityUsers
                                                                          .Any(tar => tar.Active && !tar.AlertDeactivatedDateTime.HasValue && !tar.AlertActive)));
                    }
                    if (assignedStatus.Contains("Alarm Active"))
                    {
                        tasksAlarmActive = tasks.Where(t => t.TaskActivities
                                                                .Any(ta => ta.TaskActivityUsers
                                                                               .Any(tar => tar.AlertActive && !tar.AlertDeactivatedDateTime.HasValue)));
                    }
                    if (assignedStatus.Contains("Complete"))
                    {
                        tasksComplete = tasks.Where(t => t.TaskActivities
                                                             .Any(ta => ta.TaskActivityUsers
                                                                            .Any(tar => tar.CompletedDateTime.HasValue && tar.AlertDeactivatedDateTime.HasValue && !tar.AlertActive && !tar.Active)));
                    }
                    if (tasksActive != null)
                        tasksTemp = tasksActive;
                    if (tasksAlarmActive != null)
                        tasksTemp = tasksTemp != null ? tasksTemp.Union(tasksAlarmActive) : tasksAlarmActive;
                    if (tasksComplete != null)
                        tasksTemp = tasksTemp != null ? tasksTemp.Union(tasksComplete) : tasksComplete;
                    if (tasksTemp != null)
                        tasks = tasksTemp;
                }
            }

            if (Arguments.TaskActivityTypeIDs != null)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => Arguments.TaskActivityTypeIDs.Contains(ta.TaskActivityTypeId)));
            }

            if (Arguments.StartDate.HasValue)
            {
                tasks = tasks.Where(t => t.TaskActivities.Any(ta => Arguments.StartDate != null && ta.CreatedDateTime >= Arguments.StartDate.Value));
            }

            _tasks = tasks.ToArray().OrderBy(t => t.CreatedDateTime)
                .Select(i => new TaskViewModel
                                 {
                                     Id = i.Id,
                                     Patient = i.Patient,
                                     PatientId = i.PatientId,
                                     TaskActivities = i.TaskActivities,
                                     Title = i.Title,
                                     IsComplete = i.IsComplete,
                                     AlertActive = i.AlertActive,
                                     Status = i.Status,
                                     PatientDisplayName = i.PatientDisplayName,
                                     TaskActivityTypeName = i.TaskActivityTypeName,
                                     CreatedDateTime = i.CreatedDateTime,
                                     AssignedTo = i.TaskActivities.SelectMany(j => j.AssignedTo).Contains(',') ? "Many" : i.TaskActivities.Select(j => j.AssignedTo).FirstOrDefault()
                                 }).ToExtendedObservableCollection();

            if (Arguments.AssignedByUserIds != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => Arguments.AssignedByUserIds.Contains(ta.CreatedByUserId))).ToList().ForEach(ta => task.TaskActivities.Remove(ta));
                }
            }

            if (assignedToUserIds != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.TaskActivityUsers.Any(tar => assignedToUserIds.Contains(tar.AssignedToUserId)))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }


            if (Arguments.TaskActivityTypeIDs != null)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => Arguments.TaskActivityTypeIDs.Contains(ta.TaskActivityTypeId))).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.StartDate.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.CreatedDateTime.Date >= Arguments.StartDate.GetValueOrDefault().Date)).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }

            if (Arguments.EndDate.HasValue)
            {
                foreach (var t in _tasks)
                {
                    var task = t;
                    t.TaskActivities.Except(t.TaskActivities.Where(ta => ta.CreatedDateTime.Date <= Arguments.EndDate.GetValueOrDefault().Date)).ToList().ForEach(i => task.TaskActivities.Remove(i));
                }
            }
        }

        public void CreateTask()
        {
            _taskViewManager.CreateTask();
        }

        public void ViewTask(TaskActivity taskActivity)
        {
            _taskViewManager.ViewTask(taskActivity);
        }
    }

    public class OtherTasksArguments
    {
        public bool? Active;

        public bool? AlertActive;
        public int[] AssignedByUserIds;
        public int[] AssignedToGroupIds;
        public int[] AssignedToUserIds;
        public List<string> AssignedStatus = new List<string>();

        public DateTime? EndDate;
        public int? PatientId;
        public DateTime? StartDate;
        public int[] TaskActivityTypeIDs;

        public OtherTasksArguments()
        {

            PatientId = ApplicationContext.Current.PatientId;
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };

            Active = true;
            AssignedStatus.Add("Active");
        }

        public void ConfigureForCurrentUserActiveTasks()
        {
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };
            Active = true;
            PatientId = null;
        }

        public void ConfigureForCurrentUserTasksWithActiveAlerts()
        {
            AssignedToUserIds = new[]
                                    {
                                        UserContext.Current.UserDetails.Id
                                    };
            AlertActive = true;
            PatientId = null;
        }

        public void ConfigureForActivePatientActiveTasks()
        {
            PatientId = ApplicationContext.Current.PatientId;
            Active = true;
            AssignedByUserIds = null;
        }

        public void ConfigureForActivePatientTasksWithActiveAlerts()
        {
            PatientId = ApplicationContext.Current.PatientId;
            AlertActive = true;
            AssignedToUserIds = null;
        }
    }

   
}