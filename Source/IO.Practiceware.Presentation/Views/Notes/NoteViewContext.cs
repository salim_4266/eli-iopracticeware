﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.Notes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Notes
{
    public class NoteViewContext : IViewContext
    {

        #region Private Members
        private readonly INoteViewService _noteViewService;
        private bool _stopExitingWindow;

        #endregion

        #region Properties

        [Dependency]
        public virtual NoteViewModel Note { get; set; }

        public virtual ExtendedObservableCollection<NameAndAbbreviationAndDetailViewModel> StandardNotes { get; set; }

        public virtual ExtendedObservableCollection<NamedViewModel> AlertableScreens { get; set; }

        public NoteLoadArguments NoteLoadArguments { get; set; }

        public ICommand InsertNote { get; set; }

        public ICommand DeleteNoteTemplate { get; protected set; }

        public ICommand Load { get; protected set; }

        public ICommand SaveStandardTemplate { get; protected set; }

        public ICommand Save { get; protected set; }

        public virtual bool ConfirmExitWithUnsavedChangesTrigger { get; set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        public ICommand Close { get; protected set; }

        #endregion

        #region Constructor
        public NoteViewContext(INoteViewService noteViewService)
        {
            _noteViewService = noteViewService;
            StandardNotes = new ExtendedObservableCollection<NameAndAbbreviationAndDetailViewModel>();
            InsertNote = Command.Create<NameAndAbbreviationAndDetailViewModel>(ExecuteInsertNote);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            DeleteNoteTemplate = Command.Create<int>(ExecuteDeleteNoteTemplate).Async(() => InteractionContext);
            SaveStandardTemplate = Command.Create<NoteViewModel>(ExecuteSaveStandardTemplate, delegate { return Note != null && !string.IsNullOrEmpty(Note.Value); }).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, HasChanges).Async(() => InteractionContext);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
            Close = Command.Create(() => InteractionContext.Complete(true));
        }

        #endregion

        #region Private Methods

        private void ExecuteCancelExitWithUnsavedChanges()
        {
            _stopExitingWindow = true;
        }

        void InteractionContextCompleting(object sender, CancelEventArgs e)
        {
            //Need to check for both the Saving comment and Sending Alert.
            if (HasChanges())
            {
                ConfirmExitWithUnsavedChangesTrigger = true;
                e.Cancel = _stopExitingWindow;
                _stopExitingWindow = false;
            }
        }

        private bool HasChanges()
        {
            return Note.As<IEditableObjectExtended>().IsChanged;
        }

        private void ExecuteSave()
        {
            //Define separate method to support the unit testing without closing the screen.
            SaveComment();
            //This check is specific for test cases as calling this method for multiple times.
            InteractionContext.Complete(true);
        }

        protected internal void SaveComment()
        {
            _noteViewService.SaveAndSendCommentAlert(Note, NoteLoadArguments);

            Note.As<IEditableObjectExtended>().AcceptCustomEdits();
        }

        private void ExecuteSaveStandardTemplate(NoteViewModel note)
        {
            var newNoteTemplate = new NameAndAbbreviationAndDetailViewModel
            {
                Name = note.Name,
                Detail = note.Value
            };
            newNoteTemplate.Id = _noteViewService.SaveNoteTemplate(newNoteTemplate);
            StandardNotes.Add(newNoteTemplate);
        }

        private void ExecuteLoad()
        {
            var info = _noteViewService.LoadNoteInformation(NoteLoadArguments);
            AlertableScreens = info.AlertableScreens.ToExtendedObservableCollection();

            if (info.StandardNotes != null)
            {
                StandardNotes = info.StandardNotes;
            }
            if (info.Note != null)
            {
                Note = info.Note;
            }

            Note.As<IEditableObjectExtended>().InitiateCustomEdit();
            InteractionContext.Completing += new EventHandler<CancelEventArgs>(InteractionContextCompleting).MakeWeak();
        }

        private void ExecuteInsertNote(NameAndAbbreviationAndDetailViewModel noteToInsert)
        {
            if (noteToInsert == null) return;
            Note.Value += noteToInsert.Detail;
        }

        private void ExecuteDeleteNoteTemplate(int noteTemplateId)
        {
            if (PermissionId.DeleteStandardComments.EnsurePermission())
            {
                StandardNotes.RemoveWhere(s => s.Id == noteTemplateId);
                _noteViewService.DeleteCommentTemplate(noteTemplateId);
            }
        }


        #endregion
    }
}

