﻿using System.Windows;
using IO.Practiceware.Presentation.ViewModels.Notes;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Notes
{
    public class NoteViewManager
    {
        private readonly IInteractionManager _interactionManager;
        public NoteViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void LaunchNoteView(NoteLoadArguments noteLoadArguments)
        {
            var view = new NoteView();
            var argument = view.DataContext.EnsureType<NoteViewContext>();
            argument.NoteLoadArguments = noteLoadArguments;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                          {
                                              Content = view,
                                              WindowState = WindowState.Normal,
                                              ResizeMode = ResizeMode.CanResizeWithGrip
                                          });
        }

        public void LaunchManageNotesView(NoteLoadArguments noteLoadArguments)
        {
            var view = new ManageNotesView();
            var argument = view.DataContext.EnsureType<ManageNotesViewContext>();
            argument.NoteLoadArguments = noteLoadArguments;
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}
