﻿
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Notes
{
    /// <summary>
    /// Interaction logic for NoteEntryUserControl.xaml
    /// </summary>
    public partial class NoteEntryUserControl
    {


        public static readonly DependencyProperty IsTouchScreenDeviceProperty =
            DependencyProperty.Register("IsTouchScreenDevice", typeof(bool), typeof(NoteEntryUserControl));


        public bool IsTouchScreenDevice
        {
            get { return (bool)GetValue(IsTouchScreenDeviceProperty); }
            set { SetValue(IsTouchScreenDeviceProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
         DependencyProperty.Register("Value", typeof(string), typeof(NoteEntryUserControl));

        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty IsIncludedOnStatementProperty =
           DependencyProperty.Register("IsIncludedOnStatement", typeof(bool), typeof(NoteEntryUserControl));


        public bool IsIncludedOnStatement
        {
            get { return (bool)GetValue(IsIncludedOnStatementProperty); }
            set { SetValue(IsIncludedOnStatementProperty, value); }
        }


        public NoteEntryUserControl()
        {
            InitializeComponent();
        }
    }
}
