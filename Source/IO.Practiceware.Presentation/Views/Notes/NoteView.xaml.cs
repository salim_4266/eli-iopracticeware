﻿
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Notes
{
    /// <summary>
    /// Interaction logic for NoteView.xaml
    /// </summary>
    public partial class NoteView 
    {
        public NoteView()
        {
            InitializeComponent();
        }
    }

    public class NoteViewContextReference : DataContextReference
    {
        public new NoteViewContext DataContext
        {
            get { return (NoteViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
