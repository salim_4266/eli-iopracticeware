﻿
using System;
using System.Collections.ObjectModel;
using System.Windows;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.Views.Notes
{
    /// <summary>
    /// Interaction logic for CreateNoteUserControl.xaml
    /// </summary>
    public partial class CreateNoteUserControl
    {

        public static readonly DependencyProperty IsTouchScreenDeviceProperty =
            DependencyProperty.Register("IsTouchScreenDevice", typeof(bool), typeof(CreateNoteUserControl));


        public bool IsTouchScreenDevice
        {
            get { return (bool)GetValue(IsTouchScreenDeviceProperty); }
            set { SetValue(IsTouchScreenDeviceProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
           DependencyProperty.Register("Value", typeof(string), typeof(CreateNoteUserControl));

        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty IsIncludedOnStatementProperty =
           DependencyProperty.Register("IsIncludedOnStatement", typeof(bool), typeof(CreateNoteUserControl));


        public bool IsIncludedOnStatement
        {
            get { return (bool)GetValue(IsIncludedOnStatementProperty); }
            set { SetValue(IsIncludedOnStatementProperty, value); }
        }

        public static readonly DependencyProperty AlertableScreensProperty =
           DependencyProperty.Register("AlertableScreens", typeof(ObservableCollection<NamedViewModel>), typeof(CreateNoteUserControl));

        public static readonly DependencyProperty SelectedAlertableScreensProperty =
          DependencyProperty.Register("SelectedAlertableScreens", typeof(ObservableCollection<NamedViewModel>), typeof(CreateNoteUserControl));

        public static readonly DependencyProperty ExpirationDateTimeProperty =
         DependencyProperty.Register("ExpirationDateTime", typeof(DateTime?), typeof(CreateNoteUserControl));

        public ObservableCollection<NamedViewModel> AlertableScreens
        {
            get { return (ObservableCollection<NamedViewModel>)GetValue(AlertableScreensProperty); }
            set { SetValue(AlertableScreensProperty, value); }
        }

        public ObservableCollection<NamedViewModel> SelectedAlertableScreens
        {
            get { return (ObservableCollection<NamedViewModel>)GetValue(SelectedAlertableScreensProperty); }
            set { SetValue(SelectedAlertableScreensProperty, value); }
        }

        public DateTime? ExpirationDateTime
        {
            get { return (DateTime?)GetValue(ExpirationDateTimeProperty); }
            set { SetValue(ExpirationDateTimeProperty, value); }
        }

        public CreateNoteUserControl()
        {
            InitializeComponent();
        }
    }


}
