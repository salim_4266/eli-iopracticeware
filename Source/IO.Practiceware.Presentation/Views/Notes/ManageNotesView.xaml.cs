﻿
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Notes
{
    /// <summary>
    /// Interaction logic for ManageNotesView.xaml
    /// </summary>
    public partial class ManageNotesView
    {
        public ManageNotesView()
        {
            InitializeComponent();
        }
    }

    public class ManageNotesViewContextReference : DataContextReference
    {
        public new ManageNotesViewContext DataContext
        {
            get { return (ManageNotesViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
