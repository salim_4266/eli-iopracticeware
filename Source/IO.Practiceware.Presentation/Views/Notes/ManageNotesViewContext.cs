﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.Notes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Notes
{
    public class ManageNotesViewContext : IViewContext
    {
        #region Private Members

        private readonly INoteViewService _noteViewService;
        private readonly Func<NoteViewModel> _noteViewModel;
        private readonly Func<NameAndAbbreviationAndDetailViewModel> _nameAndAbbreviationAndDetailViewModel;

        private bool _stopExitingWindow;

        #endregion

        #region Properties
        public IInteractionContext InteractionContext { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand AddNote { get; protected set; }

        public ICommand DeleteComment { get; protected set; }

        public ICommand Save { get; protected set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }

        public ICommand InsertNote { get; protected set; }

        public ICommand SaveStandardTemplate { get; protected set; }

        public ICommand DeleteNoteTemplate { get; protected set; }

        public ICommand Close { get; protected set; }

        public virtual ExtendedObservableCollection<NoteViewModel> Notes { get; set; }

        [Dependency, DispatcherThread]
        public virtual NoteViewModel Note { get; set; }

        public NoteLoadArguments NoteLoadArguments { get; set; }

        public virtual ExtendedObservableCollection<NamedViewModel> AlertableScreens { get; set; }

        [Dependency, DispatcherThread]
        public virtual ExtendedObservableCollection<NamedViewModel> SelectedAlertableScreens { get; set; }

        public virtual DateTime? ExpirationDateTime { get; set; }

        public virtual ExtendedObservableCollection<NameAndAbbreviationAndDetailViewModel> StandardNotes { get; set; }

        public virtual bool ConfirmExitWithUnsavedChangesTrigger { get; set; }

        #endregion

        #region Constructor

        public ManageNotesViewContext(INoteViewService noteViewService, Func<NoteViewModel> noteViewModel, Func<NameAndAbbreviationAndDetailViewModel> nameAndAbbreviationAndDetailViewModel)
        {
            _noteViewService = noteViewService;
            _noteViewModel = noteViewModel;
            _nameAndAbbreviationAndDetailViewModel = nameAndAbbreviationAndDetailViewModel;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, HasChange).Async(() => InteractionContext);
            AddNote = Command.Create(ExecuteAddNote);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
            SaveStandardTemplate = Command.Create<NoteViewModel>(ExecuteSaveStandardTemplate).Async(() => InteractionContext);
            InsertNote = Command.Create<NameAndAbbreviationAndDetailViewModel>(ExecuteInsertNote);
            DeleteNoteTemplate = Command.Create<int>(ExecuteDeleteNoteTemplate).Async(() => InteractionContext);
            DeleteComment = Command.Create<NoteViewModel>(ExecuteDeleteComment).Async(() => InteractionContext);
            Close = Command.Create(() => InteractionContext.Complete(true));
        }

        #endregion

        #region Private Methods

        private void ExecuteDeleteNoteTemplate(int noteTemplateId)
        {
            if (PermissionId.DeleteStandardComments.EnsurePermission())
            {
                StandardNotes.RemoveWhere(s => s.Id == noteTemplateId);
                _noteViewService.DeleteCommentTemplate(noteTemplateId);
            }
        }

        private void ExecuteDeleteComment(NoteViewModel note)
        {
            if (note.Id.GetValueOrDefault() == 0)
            {
                Notes.Remove(note);
            }
            else
            {
                _noteViewService.DeleteComment(note.Id.Value);

                NoteViewModel deletedNote = Notes.FirstOrDefault(x => x.Id == note.Id.Value);
                if (deletedNote != null)
                {
                    Notes.Remove(deletedNote);
                }
            }
        }

        private void ExecuteInsertNote(NameAndAbbreviationAndDetailViewModel noteToInsert)
        {
            if (noteToInsert == null) return;
            Note.Value = noteToInsert.Detail;
            Note.Id = noteToInsert.Id;
            Note.Name = noteToInsert.Name;
        }

        private void ExecuteSaveStandardTemplate(NoteViewModel note)
        {
            if (string.IsNullOrWhiteSpace(note.Name))
            {
                InteractionManager.Current.Alert("Please provide a valid Name.");
                return;
            }
            if (note != null && string.IsNullOrEmpty(note.Value))
            {
                InteractionManager.Current.Alert("There is no comment text entered.  Please enter a comment.");
                return;
            }

            var isNewNote = note.Id == null || note.Id == 0;

            NameAndAbbreviationAndDetailViewModel existingNote = StandardNotes.IfNotNull(x => x.FirstOrDefault(y => y.Name == note.Name));

            if ((existingNote != null && isNewNote) || (existingNote != null && existingNote.Id != note.Id))
            {
                InteractionManager.Current.Alert("Standard Text with same Name already exists. Please provide some another Name.");
                return;
            }

            var newNoteTemplate = _nameAndAbbreviationAndDetailViewModel();
            newNoteTemplate.Id = isNewNote ? 0 : note.Id.Value;
            newNoteTemplate.Name = note.Name;
            newNoteTemplate.Detail = note.Value;
            newNoteTemplate.Id = _noteViewService.SaveNoteTemplate(newNoteTemplate);
            note.Id = newNoteTemplate.Id;

            if (isNewNote)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() => StandardNotes.Add(newNoteTemplate)));
            }
            else
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() =>
                {

                    NameAndAbbreviationAndDetailViewModel stdNote = StandardNotes.IfNotNull(x => x.FirstOrDefault(y => y.Id == newNoteTemplate.Id));
                    if (stdNote != null)
                    {
                        stdNote.Name = newNoteTemplate.Name;
                        stdNote.Detail = newNoteTemplate.Detail;
                    }
                }));
            }
        }

        private bool HasChange()
        {
            if (Note.As<IEditableObjectExtended>().IsChanged || Notes.Any(x => x.As<IEditableObjectExtended>().IsChanged))
            {
                return true;
            }
            return false;
        }

        private void ExecuteSave()
        {
            bool isValid = Validate();
            if (!isValid)
            {
                InteractionManager.Current.Alert("There is no comment text entered.  Please enter a comment.");
                return;
            }

            //Define separate method to support the unit testing without closing the screen.
            SaveComments();
            InteractionContext.Complete(true);
        }

        private bool Validate()
        {
            bool isValid = true;

            if (HasChange())
            {
                isValid = Notes.Any(note => !(string.IsNullOrEmpty(note.Value)));
            }

            return isValid;
        }

        protected internal void SaveComments()
        {
            var notesToBeSaved = Notes.Where(x => x.As<IEditableObjectExtended>().IsChanged).ToArray();
            _noteViewService.SaveAndSendMultipleCommentAlert(notesToBeSaved, NoteLoadArguments);

            Notes.ForEach(x => x.As<IEditableObjectExtended>().AcceptCustomEdits());

            Note.As<IEditableObjectExtended>().AcceptCustomEdits();
        }

        private void ExecuteAddNote()
        {
            if (Note != null && string.IsNullOrEmpty(Note.Value))
            {
                InteractionManager.Current.Alert("There is no comment text entered.  Please enter a comment.");
                return;
            }
            Note.Id = null;
            Notes.Add(Note);
            Note = _noteViewModel();
        }

        private void ExecuteLoad()
        {
            //This screen will always launch in patient context, so entity id will have patient only.
            var info = _noteViewService.LoadManageNotesInformation(NoteLoadArguments.EntityId.GetValueOrDefault().EnsureNotDefault());
            Notes = info.Notes;
            AlertableScreens = info.AlertableScreens;
            StandardNotes = info.StandardNotes;
            Notes.ForEach(x => x.As<IEditableObjectExtended>().InitiateCustomEdit());
            Note.As<IEditableObjectExtended>().InitiateCustomEdit();

            InteractionContext.Completing += new EventHandler<CancelEventArgs>(InteractionContextCompleting).MakeWeak();
        }

        void InteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (HasChange())
            {
                ConfirmExitWithUnsavedChangesTrigger = true;
                e.Cancel = _stopExitingWindow;
                _stopExitingWindow = false;
            }
        }

        private void ExecuteCancelExitWithUnsavedChanges()
        {
            _stopExitingWindow = true;
        }

        #endregion

    }
}
