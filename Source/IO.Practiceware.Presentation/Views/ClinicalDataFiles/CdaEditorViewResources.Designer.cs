﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CdaEditorViewResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CdaEditorViewResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IO.Practiceware.Presentation.Views.ClinicalDataFiles.CdaEditorViewResources", typeof(CdaEditorViewResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.6.1&quot; /&gt;
        ///          &lt;code code=&quot;48765-2&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;Allergies, Adverse Reactions, Alerts&quot; /&gt;
        ///          &lt;title&gt;Allergies&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Type&lt;/th&gt;
        ///                  &lt;th&gt;Substance&lt;/th&gt;
        ///                  &lt;th&gt;Reaction&lt;/th&gt;
        ///                  &lt;th&gt;Status&lt;/th&gt;
        ///     [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string AllergiesNullFlavor {
            get {
                return ResourceManager.GetString("AllergiesNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.2.1&quot; /&gt;
        ///          &lt;code code=&quot;11369-6&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;History of immunizations&quot; /&gt;
        ///          &lt;title&gt;Immunizations&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;td&gt;Vaccine&lt;/td&gt;
        ///                  &lt;td&gt;Date&lt;/td&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/thead&gt;
        ///              &lt;tbody&gt;
        ///                 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ImmunizationsNullFlavor {
            get {
                return ResourceManager.GetString("ImmunizationsNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.38&quot; /&gt;
        ///          &lt;code code=&quot;29549-3&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;MEDICATIONS ADMINISTERED&quot; /&gt;
        ///          &lt;title&gt;Medications Administered&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Medications&lt;/th&gt;
        ///                  &lt;th&gt;Date Administered&lt;/th&gt;
        ///                  &lt;th&gt;Medication Info&lt;/th&gt;
        ///                [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MedicationsAdministeredNullFlavor {
            get {
                return ResourceManager.GetString("MedicationsAdministeredNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.1.1&quot; /&gt;
        ///          &lt;code code=&quot;10160-0&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;HISTORY OF MEDICATION USE&quot; /&gt;
        ///          &lt;title&gt;Medications&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Medication&lt;/th&gt;
        ///                  &lt;th&gt;Start Date&lt;/th&gt;
        ///                  &lt;th&gt;Medication Details&lt;/th&gt;
        ///                  &lt;th&gt;Status&lt;/t [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MedicationsNullFlavor {
            get {
                return ResourceManager.GetString("MedicationsNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.10&quot; /&gt;
        ///          &lt;code code=&quot;18776-5&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;Treatment Plan&quot; /&gt;
        ///          &lt;title&gt;Plan Of Care&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Name&lt;/th&gt;
        ///                  &lt;th&gt;Type&lt;/th&gt;
        ///                  &lt;th&gt;Date&lt;/th&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/thead&gt;
        ///              &lt;tbody&gt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PlanOfCareNullFlavor {
            get {
                return ResourceManager.GetString("PlanOfCareNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.5&quot; /&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.5.1&quot; /&gt;
        ///          &lt;code code=&quot;11450-4&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;PROBLEM LIST&quot; /&gt;
        ///          &lt;title&gt;Problems&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Type&lt;/th&gt;
        ///                  &lt;th&gt;Name And Code&lt;/th&gt;
        ///                  &lt;th&gt;Date Range&lt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ProblemsNullFlavor {
            get {
                return ResourceManager.GetString("ProblemsNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.7&quot; /&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.7.1&quot; /&gt;
        ///          &lt;code code=&quot;47519-4&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;PROCEDURES&quot; /&gt;
        ///          &lt;title&gt;Procedures&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Name&lt;/th&gt;
        ///                  &lt;th&gt;Date&lt;/th&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/the [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ProceduresNullFlavor {
            get {
                return ResourceManager.GetString("ProceduresNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.3.1&quot; /&gt;
        ///          &lt;code code=&quot;30954-2&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;RESULTS&quot; /&gt;
        ///          &lt;title&gt;Results&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Name&lt;/th&gt;
        ///                  &lt;th&gt;Result Values&lt;/th&gt;
        ///                  &lt;th&gt;Date&lt;/th&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/thead&gt;
        ///              &lt;tbody&gt;
        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ResultsNullFlavor {
            get {
                return ResourceManager.GetString("ResultsNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.17&quot; /&gt;
        ///          &lt;code code=&quot;29762-2&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; displayName=&quot;Social History&quot; /&gt;
        ///          &lt;title&gt;Social History&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th&gt;Social History Element&lt;/th&gt;
        ///                  &lt;th&gt;Description&lt;/th&gt;
        ///                  &lt;th&gt;Effective Dates&lt;/th&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/thead&gt;
        ///       [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SocialHistoryNullFlavor {
            get {
                return ResourceManager.GetString("SocialHistoryNullFlavor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;section&gt;
        ///          &lt;templateId root=&quot;2.16.840.1.113883.10.20.22.2.4.1&quot; /&gt;
        ///          &lt;code code=&quot;8716-3&quot; codeSystem&quot;2.16.840.1.113883.6.1&quot; codeSystemName=&quot;LOINC&quot; displayName=&quot;VITAL SIGNS&quot; /&gt;
        ///          &lt;title&gt;Vital Signs&lt;/title&gt;
        ///          &lt;text&gt;
        ///            &lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;
        ///              &lt;thead&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th align=&quot;right&quot;&gt;Date / Time:&lt;/th&gt;
        ///                &lt;/tr&gt;
        ///              &lt;/thead&gt;
        ///              &lt;tbody&gt;
        ///                &lt;tr&gt;
        ///                  &lt;th al [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string VitalSignsNullFlavor {
            get {
                return ResourceManager.GetString("VitalSignsNullFlavor", resourceCulture);
            }
        }
    }
}
