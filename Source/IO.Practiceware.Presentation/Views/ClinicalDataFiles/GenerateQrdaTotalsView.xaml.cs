﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    /// <summary>
    ///     Interaction logic for GenerateQrdaTotalsView.xaml
    /// </summary>
    public partial class GenerateQrdaTotalsView
    {
        public GenerateQrdaTotalsView()
        {
            InitializeComponent();
        }
    }


    public class GenerateQrdaTotalsViewContext : IViewContext
    {
        private readonly IGenerateQrdaTotalsViewService _generateQrdaTotalsViewService;
        private readonly IGenerateQrdaViewService _generateQrdaViewService;


        public GenerateQrdaTotalsViewContext(IGenerateQrdaTotalsViewService generateQrdaTotalsViewService, IGenerateQrdaViewService generateQrdaViewService)
        {
            _generateQrdaTotalsViewService = generateQrdaTotalsViewService;
            _generateQrdaViewService = generateQrdaViewService;

            SearchResults = new ObservableCollection<QrdaSearchResultViewModel>();
            //This is just copied over from the QrdaViewService - I don't think it needs to be changed though - it should work 
            //exactly as the QrdaViewService currently works
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Search = Command.Create(ExecuteSearch, () => SearchCriteria.IsValid()).Async(() => InteractionContext);
        }


        /// <summary>
        ///     The following data members should be populated exactly as they're already implemented on GenerateQrdaViewContext,
        ///     except that the SearchResults are now a collection of objects with Name, Id, AND a boolean value
        /// </summary>
        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> Users { get; set; }

        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> Measures { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<QrdaSearchResultViewModel> SearchResults { get; set; }

        [Dependency]
        public virtual GenerateQrdaSearchCriteria SearchCriteria { get; set; }


        /// <summary>
        ///     These values need to update whenever the SearchResults are updated
        /// </summary>
        [DependsOn("SearchResults")]
        public virtual int NumeratorTotal
        {
            get { return SearchResults.Count(result => result.IsInNumerator); }
        }

        [DependsOn("SearchResults")]
        public virtual int DenominatorTotal
        {
            get { return SearchResults.Count; }
        }

        [DependsOn("NumeratorTotal", "DenominatorTotal", "DenominatorExclusionTotal", "DenominatorExceptionTotal")]
        public virtual decimal Percentage
        {
            get
            {
                if (DenominatorTotal > 0)
                {
                    var percentageFactor = (DenominatorTotal - DenominatorExclusionTotal - DenominatorExceptionTotal);
                    if ((percentageFactor) > 0)
                        return Math.Round((decimal)(NumeratorTotal * 100) / percentageFactor, 2);
                }
                return 0;
            }
        }

        [DependsOn("SearchResults")]
        public virtual int NumeratorExclusionTotal
        {
            //get { return SearchResults.Count(result => result.NumeratorExclusions); }
            get { return 0; }
        }

        [DependsOn("SearchResults")]
        public virtual int DenominatorExclusionTotal
        {
            get { return SearchResults.Count(result => result.IsInDenominatorExclusion); }
        }

        [DependsOn("SearchResults")]
        public virtual int DenominatorExceptionTotal
        {
            get { return SearchResults.Count(result => result.IsInDenominatorException && !result.IsInNumerator); }
        }

        /// <summary>
        ///     The following Load and Search commands should be implemented exactly as already implemented on
        ///     GenerateQrdaViewContext
        ///     These are the only commands required to be implemented at this time
        /// </summary>
        public virtual ICommand Load { get; protected set; }

        public virtual ICommand Search { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        public void ExecuteLoad()
        {
            GenerateQrdaViewLoadInformation info = _generateQrdaViewService.GetLoadInformation();
            Users = info.Users;
            Measures = info.Measures;

            // Ensure every next change of search criteria will remove the results
            SearchCriteria.As<INotifyPropertyChanged>().PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(
                (sender, args) => { if (SearchResults != null) SearchResults.Clear(); });
        }

        private void ExecuteSearch()
        {
            SearchResults = _generateQrdaTotalsViewService.FindResults(SearchCriteria.Measure.Id, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate).ToObservableCollection();
        }
    }
}