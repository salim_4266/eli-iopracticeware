﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    public struct StructAllergy
    {
        public string Name { get; set; }
        public string Reaction { get; set; }
        public string ReactionDate { get; set; }
        public string XML { get; set; }
        public string Severity { get; set; }
        public string RxNorm { get; set; }
        public string Status { get; set; }

        public void SaveIntoDB(string PatientId)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileAllergy(PatientId, AllergyType, AllergySubstance, AllergyReaction, Status, AllergySeverity, RxNorm, ReactionDate, AllergyStatus) Values ('" + PatientId + "', 'Drug', '" + Name + "', '" + Reaction + "', '0', '" + Severity + "','" + RxNorm + "','" + ReactionDate + "','"+ Status+"')";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }

    }
}
