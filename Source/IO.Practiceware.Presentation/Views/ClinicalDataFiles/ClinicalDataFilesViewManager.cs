using System.Linq;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.Views.AppointmentSelector;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Data;
using IO.Practiceware.Presentation.Views.Setup.AdminUtilities;
using System.Net;
using IO.Practiceware.Application;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    public class ClinicalDataFilesViewManager
    {
        private readonly Func<AppointmentSelectorViewManager> _createAppointmentSelectorViewManager;
        private readonly Func<IClinicalDataFilesEditAndTransmitViewService> _clinicalDataFilesEditAndTransmitViewService;
        private readonly IInteractionManager _interactionManager;


        public ClinicalDataFilesViewManager(IInteractionManager interactionManager, Func<AppointmentSelectorViewManager> createAppointmentSelectorViewManager, Func<IClinicalDataFilesEditAndTransmitViewService> clinicalDataFilesEditAndTransmitViewService)
        {
            _interactionManager = interactionManager;
            _createAppointmentSelectorViewManager = createAppointmentSelectorViewManager;
            _clinicalDataFilesEditAndTransmitViewService = clinicalDataFilesEditAndTransmitViewService;
        }

        public void ShowClinicalDataFilesGenerator()
        {
            string[] ResourceDetail = null;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select ResourceFirstName + ' ' + ResourceLastName + ',' + ResourceType as ResourceDetail From dbo.Resources with(nolock) Where ResourceId = " + UserContext.Current.UserDetails.Id;
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    ResourceDetail = cmd.ExecuteScalar().ToString().Split(',');
                }
                dbConnection.Close();
            }

            RequestTokenEntity objToken = new RequestTokenEntity(ResourceDetail[0], ResourceDetail[1]);
            if (objToken.UserName != "")
            {
                string result = string.Empty;
                string apiUrl = "https://portal.iopracticeware.com/api/ACI/GenerateToken/";
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objToken);
                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;
                try
                {
                    result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                    ResponseTokenEntity objResponseEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTokenEntity>(result);
                    if (objResponseEntity.TokenStatus == "1")
                    {
                        string tempURL = objToken.BaseURL + "?" + "TokenValue=" + objResponseEntity.TokenDetail + "&ProductKey=" + objToken.ProductKey;
                        System.Diagnostics.Process.Start(tempURL);
                    }
                    else
                    {
                        MessageBox.Show(objResponseEntity.TokenDetail);
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                }
            }
        }

        public void ShowClinicalDataFilesImportView()
        {
            var view = new ClinicalDataFilesLinkFilesView();            

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip                
            });
        }

       public void ShowGenerateQrdaCategory1View()
        {
            var view = new GenerateQrdaView();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }

       public void ShowGenerateQrdaTotalsView()
       {
           var view = new GenerateQrdaTotalsView();
           _interactionManager.ShowModal(new WindowInteractionArguments
           {
               Content = view,
               WindowState = WindowState.Normal,
               ResizeMode = ResizeMode.CanResizeWithGrip
           });
       }

        public void TransmitClinicalDataFiles(int appointmentId)
        {
            SelectedAppointmentViewModel appointment = _clinicalDataFilesEditAndTransmitViewService().GetAppointments(new List<int> { appointmentId }).FirstOrDefault();
            if (appointment == null) return;

            //Generating Encounter Clinical Summary
            var encounterClinicalSummaryCdaViewModel = new CdaViewModel { Id = appointment.Id, CdaXml = "", IsClinicalSummary = true };
            _clinicalDataFilesEditAndTransmitViewService().TransmitClinicalDataFile(encounterClinicalSummaryCdaViewModel);

            //Generating Patient Transition of care
            var patientTransitionOfCareCdaViewModel = new CdaViewModel { Id = appointment.PatientId, CdaXml ="", IsClinicalSummary = false };
            _clinicalDataFilesEditAndTransmitViewService().TransmitClinicalDataFile(patientTransitionOfCareCdaViewModel);
        }
    }

    public class ClinicalDataFilesEditAndTransmitLoadArguments
    {
        public List<int> ListOfAppointmentId { get; set; }
    }
}