﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Data;

namespace ClinicalDocument
{


    [XmlRoot(ElementName = "realmCode", Namespace = "urn:hl7-org:v3")]
    public class RealmCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }


    }

    [XmlRoot(ElementName = "typeId", Namespace = "urn:hl7-org:v3")]
    public class TypeId
    {
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
    }

    [XmlRoot(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
    public class TemplateId
    {
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
    }

    [XmlRoot(ElementName = "id", Namespace = "urn:hl7-org:v3")]
    public class Id
    {
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
        [XmlAttribute(AttributeName = "assigningAuthorityName")]
        public string AssigningAuthorityName { get; set; }
    }

    [XmlRoot(ElementName = "code", Namespace = "urn:hl7-org:v3")]
    public class Code
    {
        [XmlAttribute(AttributeName = "code")]
        public string _code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlElement(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
        public OriginalText OriginalText { get; set; }
        [XmlElement(ElementName = "translation", Namespace = "urn:hl7-org:v3")]
        public Translation Translation { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
    public class EffectiveTime
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlElement(ElementName = "low", Namespace = "urn:hl7-org:v3")]
        public Low Low { get; set; }
        [XmlElement(ElementName = "high", Namespace = "urn:hl7-org:v3")]
        public High High { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "confidentialityCode", Namespace = "urn:hl7-org:v3")]
    public class ConfidentialityCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
    }

    [XmlRoot(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
    public class LanguageCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "setId", Namespace = "urn:hl7-org:v3")]
    public class SetId
    {
        [XmlAttribute(AttributeName = "extension")]
        public string Extension { get; set; }
        [XmlAttribute(AttributeName = "root")]
        public string Root { get; set; }
    }

    [XmlRoot(ElementName = "versionNumber", Namespace = "urn:hl7-org:v3")]
    public class VersionNumber
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
    public class Addr
    {
        [XmlElement(ElementName = "streetAddressLine", Namespace = "urn:hl7-org:v3")]
        public string StreetAddressLine { get; set; }
        [XmlElement(ElementName = "city", Namespace = "urn:hl7-org:v3")]
        public string City { get; set; }
        [XmlElement(ElementName = "state", Namespace = "urn:hl7-org:v3")]
        public string State { get; set; }
        [XmlElement(ElementName = "country", Namespace = "urn:hl7-org:v3")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlElement(ElementName = "postalCode", Namespace = "urn:hl7-org:v3")]
         //public PostalCode PostalCode { get; set; }
        public string PostalCode { get; set; }
    }

    [XmlRoot(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
    public class Telecom
    {
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "name", Namespace = "urn:hl7-org:v3")]
    public class Name
    {
        [XmlElement(ElementName = "given", Namespace = "urn:hl7-org:v3")]
        public string Given { get; set; }
        [XmlElement(ElementName = "family", Namespace = "urn:hl7-org:v3")]
        public string Family { get; set; }
        [XmlAttribute(AttributeName = "use")]
        public string Use { get; set; }
        [XmlElement(ElementName = "suffix", Namespace = "urn:hl7-org:v3")]
        public string Suffix { get; set; }
    }

    [XmlRoot(ElementName = "administrativeGenderCode", Namespace = "urn:hl7-org:v3")]
    public class AdministrativeGenderCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "birthTime", Namespace = "urn:hl7-org:v3")]
    public class BirthTime
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "maritalStatusCode", Namespace = "urn:hl7-org:v3")]
    public class MaritalStatusCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "religiousAffiliationCode", Namespace = "urn:hl7-org:v3")]
    public class ReligiousAffiliationCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "raceCode", Namespace = "urn:hl7-org:v3")]
    public class RaceCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "ethnicGroupCode", Namespace = "urn:hl7-org:v3")]
    public class EthnicGroupCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "place", Namespace = "urn:hl7-org:v3")]
    public class Place
    {
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public string Addr { get; set; }
    }

    [XmlRoot(ElementName = "birthplace", Namespace = "urn:hl7-org:v3")]
    public class Birthplace
    {
        [XmlElement(ElementName = "place", Namespace = "urn:hl7-org:v3")]
        public Place Place { get; set; }
    }

    [XmlRoot(ElementName = "modeCode", Namespace = "urn:hl7-org:v3")]
    public class ModeCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "proficiencyLevelCode", Namespace = "urn:hl7-org:v3")]
    public class ProficiencyLevelCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "preferenceInd", Namespace = "urn:hl7-org:v3")]
    public class PreferenceInd
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "languageCommunication", Namespace = "urn:hl7-org:v3")]
    public class LanguageCommunication
    {
        [XmlElement(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
        public LanguageCode LanguageCode { get; set; }
        [XmlElement(ElementName = "modeCode", Namespace = "urn:hl7-org:v3")]
        public ModeCode ModeCode { get; set; }
        [XmlElement(ElementName = "proficiencyLevelCode", Namespace = "urn:hl7-org:v3")]
        public ProficiencyLevelCode ProficiencyLevelCode { get; set; }
        [XmlElement(ElementName = "preferenceInd", Namespace = "urn:hl7-org:v3")]
        public PreferenceInd PreferenceInd { get; set; }
    }

    [XmlRoot(ElementName = "patient", Namespace = "urn:hl7-org:v3")]
    public class Patient
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "administrativeGenderCode", Namespace = "urn:hl7-org:v3")]
        public AdministrativeGenderCode AdministrativeGenderCode { get; set; }
        [XmlElement(ElementName = "birthTime", Namespace = "urn:hl7-org:v3")]
        public BirthTime BirthTime { get; set; }
        [XmlElement(ElementName = "maritalStatusCode", Namespace = "urn:hl7-org:v3")]
        public MaritalStatusCode MaritalStatusCode { get; set; }
        [XmlElement(ElementName = "religiousAffiliationCode", Namespace = "urn:hl7-org:v3")]
        public ReligiousAffiliationCode ReligiousAffiliationCode { get; set; }
        [XmlElement(ElementName = "raceCode", Namespace = "urn:hl7-org:v3")]
        public RaceCode RaceCode { get; set; }
        [XmlElement(ElementName = "ethnicGroupCode", Namespace = "urn:hl7-org:v3")]
        public EthnicGroupCode EthnicGroupCode { get; set; }
        [XmlElement(ElementName = "birthplace", Namespace = "urn:hl7-org:v3")]
        public Birthplace Birthplace { get; set; }
        [XmlElement(ElementName = "languageCommunication", Namespace = "urn:hl7-org:v3")]
        public LanguageCommunication LanguageCommunication { get; set; }
    }

    [XmlRoot(ElementName = "providerOrganization", Namespace = "urn:hl7-org:v3")]
    public class ProviderOrganization
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "standardIndustryClassCode", Namespace = "urn:hl7-org:v3")]
        public string StandardIndustryClassCode { get; set; }
        [XmlElement(ElementName = "asOrganizationPartOf", Namespace = "urn:hl7-org:v3")]
        public string AsOrganizationPartOf { get; set; }
    }

    [XmlRoot(ElementName = "patientRole", Namespace = "urn:hl7-org:v3")]
    public class PatientRole
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "patient", Namespace = "urn:hl7-org:v3")]
        public Patient Patient { get; set; }
        [XmlElement(ElementName = "providerOrganization", Namespace = "urn:hl7-org:v3")]
        public ProviderOrganization ProviderOrganization { get; set; }
    }

    [XmlRoot(ElementName = "recordTarget", Namespace = "urn:hl7-org:v3")]
    public class RecordTarget
    {
        [XmlElement(ElementName = "patientRole", Namespace = "urn:hl7-org:v3")]
        public PatientRole PatientRole { get; set; }
    }

    [XmlRoot(ElementName = "time", Namespace = "urn:hl7-org:v3")]
    public class Time
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlElement(ElementName = "low", Namespace = "urn:hl7-org:v3")]
        public Low Low { get; set; }
    }

    [XmlRoot(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
    public class AssignedPerson
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public Name Name { get; set; }
    }

    [XmlRoot(ElementName = "assignedAuthor", Namespace = "urn:hl7-org:v3")]
    public class AssignedAuthor
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
        public AssignedPerson AssignedPerson { get; set; }
    }

    [XmlRoot(ElementName = "author", Namespace = "urn:hl7-org:v3")]
    public class Author
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "assignedAuthor", Namespace = "urn:hl7-org:v3")]
        public AssignedAuthor AssignedAuthor { get; set; }
    }

    [XmlRoot(ElementName = "postalCode", Namespace = "urn:hl7-org:v3")]
    public class PostalCode
    {
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
    public class AssignedEntity
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "assignedPerson", Namespace = "urn:hl7-org:v3")]
        public AssignedPerson AssignedPerson { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "representedOrganization", Namespace = "urn:hl7-org:v3")]
        public RepresentedOrganization RepresentedOrganization { get; set; }
    }

    [XmlRoot(ElementName = "dataEnterer", Namespace = "urn:hl7-org:v3")]
    public class DataEnterer
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "informant", Namespace = "urn:hl7-org:v3")]
    public class Informant
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "representedCustodianOrganization", Namespace = "urn:hl7-org:v3")]
    public class RepresentedCustodianOrganization
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "telecom", Namespace = "urn:hl7-org:v3")]
        public Telecom Telecom { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
    }

    [XmlRoot(ElementName = "assignedCustodian", Namespace = "urn:hl7-org:v3")]
    public class AssignedCustodian
    {
        [XmlElement(ElementName = "representedCustodianOrganization", Namespace = "urn:hl7-org:v3")]
        public RepresentedCustodianOrganization RepresentedCustodianOrganization { get; set; }
    }

    [XmlRoot(ElementName = "custodian", Namespace = "urn:hl7-org:v3")]
    public class Custodian
    {
        [XmlElement(ElementName = "assignedCustodian", Namespace = "urn:hl7-org:v3")]
        public AssignedCustodian AssignedCustodian { get; set; }
    }

    [XmlRoot(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
    public class InformationRecipient
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "receivedOrganization", Namespace = "urn:hl7-org:v3")]
    public class ReceivedOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "intendedRecipient", Namespace = "urn:hl7-org:v3")]
    public class IntendedRecipient
    {
        [XmlElement(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
        public InformationRecipient InformationRecipient { get; set; }
        [XmlElement(ElementName = "receivedOrganization", Namespace = "urn:hl7-org:v3")]
        public ReceivedOrganization ReceivedOrganization { get; set; }
    }

    [XmlRoot(ElementName = "signatureCode", Namespace = "urn:hl7-org:v3")]
    public class SignatureCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "legalAuthenticator", Namespace = "urn:hl7-org:v3")]
    public class LegalAuthenticator
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "signatureCode", Namespace = "urn:hl7-org:v3")]
        public SignatureCode SignatureCode { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "low", Namespace = "urn:hl7-org:v3")]
    public class Low
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "performer", Namespace = "urn:hl7-org:v3")]
    public class Performer
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "serviceEvent", Namespace = "urn:hl7-org:v3")]
    public class ServiceEvent
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public List<Id> Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "performer", Namespace = "urn:hl7-org:v3")]
        public Performer Performer { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "documentationOf", Namespace = "urn:hl7-org:v3")]
    public class DocumentationOf
    {
        [XmlElement(ElementName = "serviceEvent", Namespace = "urn:hl7-org:v3")]
        public ServiceEvent ServiceEvent { get; set; }
    }

    [XmlRoot(ElementName = "representedOrganization", Namespace = "urn:hl7-org:v3")]
    public class RepresentedOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "responsibleParty", Namespace = "urn:hl7-org:v3")]
    public class ResponsibleParty
    {
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
    }

    [XmlRoot(ElementName = "encounterParticipant", Namespace = "urn:hl7-org:v3")]
    public class EncounterParticipant
    {
        [XmlElement(ElementName = "time", Namespace = "urn:hl7-org:v3")]
        public Time Time { get; set; }
        [XmlElement(ElementName = "assignedEntity", Namespace = "urn:hl7-org:v3")]
        public AssignedEntity AssignedEntity { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "location", Namespace = "urn:hl7-org:v3")]
    public class Location
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlElement(ElementName = "addr", Namespace = "urn:hl7-org:v3")]
        public Addr Addr { get; set; }
    }

    [XmlRoot(ElementName = "healthCareFacility", Namespace = "urn:hl7-org:v3")]
    public class HealthCareFacility
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "location", Namespace = "urn:hl7-org:v3")]
        public Location Location { get; set; }
    }

    [XmlRoot(ElementName = "encompassingEncounter", Namespace = "urn:hl7-org:v3")]
    public class EncompassingEncounter
    {
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "responsibleParty", Namespace = "urn:hl7-org:v3")]
        public ResponsibleParty ResponsibleParty { get; set; }
        [XmlElement(ElementName = "encounterParticipant", Namespace = "urn:hl7-org:v3")]
        public EncounterParticipant EncounterParticipant { get; set; }
        [XmlElement(ElementName = "location", Namespace = "urn:hl7-org:v3")]
        public Location Location { get; set; }
    }

    [XmlRoot(ElementName = "componentOf", Namespace = "urn:hl7-org:v3")]
    public class ComponentOf
    {
        [XmlElement(ElementName = "encompassingEncounter", Namespace = "urn:hl7-org:v3")]
        public EncompassingEncounter EncompassingEncounter { get; set; }
    }

    [XmlRoot(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
    public class Tr
    {
        [XmlElement(ElementName = "td", Namespace = "urn:hl7-org:v3")]
        public List<Td> Td { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlElement(ElementName = "th", Namespace = "urn:hl7-org:v3")]
        public List<Th> Th { get; set; }
    }

    [XmlRoot(ElementName = "thead", Namespace = "urn:hl7-org:v3")]
    public class Thead
    {
        [XmlElement(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
        public Tr Tr { get; set; }
    }

    [XmlRoot(ElementName = "td", Namespace = "urn:hl7-org:v3")]
    public class Td
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlElement(ElementName = "content", Namespace = "urn:hl7-org:v3")]
        public Content Content { get; set; }
    }

    [XmlRoot(ElementName = "tbody", Namespace = "urn:hl7-org:v3")]
    public class Tbody
    {
        [XmlElement(ElementName = "tr", Namespace = "urn:hl7-org:v3")]
        public List<Tr> Tr { get; set; }
    }

    [XmlRoot(ElementName = "table", Namespace = "urn:hl7-org:v3")]
    public class Table
    {
        [XmlElement(ElementName = "thead", Namespace = "urn:hl7-org:v3")]
        public Thead Thead { get; set; }
        [XmlElement(ElementName = "tbody", Namespace = "urn:hl7-org:v3")]
        public Tbody Tbody { get; set; }
        [XmlAttribute(AttributeName = "border")]
        public string Border { get; set; }
        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }
    }

    [XmlRoot(ElementName = "text", Namespace = "urn:hl7-org:v3")]
    public class Text
    {
        [XmlElement(ElementName = "table", Namespace = "urn:hl7-org:v3")]
        public Table Table { get; set; }
        [XmlElement(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
        public Reference Reference { get; set; }
    }

    [XmlRoot(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
    public class StatusCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "high", Namespace = "urn:hl7-org:v3")]
    public class High
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
    public class Reference
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
    public class OriginalText
    {
        [XmlElement(ElementName = "reference", Namespace = "urn:hl7-org:v3")]
        public Reference Reference { get; set; }
    }

    [XmlRoot(ElementName = "value", Namespace = "urn:hl7-org:v3")]
    public class Value
    {
        [XmlElement(ElementName = "originalText", Namespace = "urn:hl7-org:v3")]
        public OriginalText OriginalText { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "playingEntity", Namespace = "urn:hl7-org:v3")]
    public class PlayingEntity
    {
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "participantRole", Namespace = "urn:hl7-org:v3")]
    public class ParticipantRole
    {
        [XmlElement(ElementName = "playingEntity", Namespace = "urn:hl7-org:v3")]
        public PlayingEntity PlayingEntity { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "participant", Namespace = "urn:hl7-org:v3")]
    public class Participant
    {
        [XmlElement(ElementName = "participantRole", Namespace = "urn:hl7-org:v3")]
        public ParticipantRole ParticipantRole { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
    public class Observation
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "value", Namespace = "urn:hl7-org:v3")]
        public Value Value { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
        public EntryRelationship EntryRelationship { get; set; }
        [XmlElement(ElementName = "interpretationCode", Namespace = "urn:hl7-org:v3")]
        public InterpretationCode InterpretationCode { get; set; }

        public Participant Participant { get; set; }
    }

    [XmlRoot(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
    public class EntryRelationship
    {
        [XmlElement(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
        public Observation Observation { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
        [XmlAttribute(AttributeName = "inversionInd")]
        public string InversionInd { get; set; }
    }

    [XmlRoot(ElementName = "act", Namespace = "urn:hl7-org:v3")]
    public class Act
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "entryRelationship", Namespace = "urn:hl7-org:v3")]
        public EntryRelationship EntryRelationship { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    [XmlRoot(ElementName = "entry", Namespace = "urn:hl7-org:v3")]
    public class Entry
    {
        [XmlElement(ElementName = "act", Namespace = "urn:hl7-org:v3")]
        public Act Act { get; set; }
        [XmlAttribute(AttributeName = "typeCode")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "substanceAdministration", Namespace = "urn:hl7-org:v3")]
        public SubstanceAdministration SubstanceAdministration { get; set; }
        [XmlElement(ElementName = "observation", Namespace = "urn:hl7-org:v3")]
        public Observation Observation { get; set; }
        [XmlElement(ElementName = "organizer", Namespace = "urn:hl7-org:v3")]
        public Organizer Organizer { get; set; }
    }



    [XmlRoot(ElementName = "section", Namespace = "urn:hl7-org:v3")]
    public class Section
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }

        [XmlElement(ElementName = "title", Namespace = "urn:hl7-org:v3")]
        public string Title { get; set; }

        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }

        [XmlElement(ElementName = "entry", Namespace = "urn:hl7-org:v3")]
        public List<Object> Entry { get; set; }
    }

    [XmlRoot(ElementName = "component", Namespace = "urn:hl7-org:v3")]
    public class component
    {
        [XmlElement(ElementName = "section", Namespace = "urn:hl7-org:v3")]
        public Section section { get; set; }
    }

    public class Component
    {
        [XmlElement(ElementName = "structuredBody", Namespace = "urn:hl7-org:v3")]
        public StructuredBody structuredBody { get; set; }
    }

    [XmlRoot(ElementName = "content", Namespace = "urn:hl7-org:v3")]
    public class Content
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "doseQuantity", Namespace = "urn:hl7-org:v3")]
    public class DoseQuantity
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "unit")]
        public string Unit { get; set; }
    }

    [XmlRoot(ElementName = "translation", Namespace = "urn:hl7-org:v3")]
    public class Translation
    {
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "displayName")]
        public string DisplayName { get; set; }
        [XmlAttribute(AttributeName = "codeSystemName")]
        public string CodeSystemName { get; set; }
    }

    [XmlRoot(ElementName = "manufacturedMaterial", Namespace = "urn:hl7-org:v3")]
    public class ManufacturedMaterial
    {
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "manufacturerOrganization", Namespace = "urn:hl7-org:v3")]
    public class ManufacturerOrganization
    {
        [XmlElement(ElementName = "name", Namespace = "urn:hl7-org:v3")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "manufacturedProduct", Namespace = "urn:hl7-org:v3")]
    public class ManufacturedProduct
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "manufacturedMaterial", Namespace = "urn:hl7-org:v3")]
        public ManufacturedMaterial ManufacturedMaterial { get; set; }
        [XmlElement(ElementName = "manufacturerOrganization", Namespace = "urn:hl7-org:v3")]
        public ManufacturerOrganization ManufacturerOrganization { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
    }

    [XmlRoot(ElementName = "consumable", Namespace = "urn:hl7-org:v3")]
    public class Consumable
    {
        [XmlElement(ElementName = "manufacturedProduct", Namespace = "urn:hl7-org:v3")]
        public ManufacturedProduct ManufacturedProduct { get; set; }
    }

    [XmlRoot(ElementName = "substanceAdministration", Namespace = "urn:hl7-org:v3")]
    public class SubstanceAdministration
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "text", Namespace = "urn:hl7-org:v3")]
        public Text Text { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "doseQuantity", Namespace = "urn:hl7-org:v3")]
        public DoseQuantity DoseQuantity { get; set; }
        [XmlElement(ElementName = "consumable", Namespace = "urn:hl7-org:v3")]
        public Consumable Consumable { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    [XmlRoot(ElementName = "th", Namespace = "urn:hl7-org:v3")]
    public class Th
    {
        [XmlAttribute(AttributeName = "colspan")]
        public string Colspan { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "align")]
        public string Align { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "interpretationCode", Namespace = "urn:hl7-org:v3")]
    public class InterpretationCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "codeSystem")]
        public string CodeSystem { get; set; }
        [XmlAttribute(AttributeName = "nullFlavor")]
        public string NullFlavor { get; set; }
    }

    [XmlRoot(ElementName = "organizer", Namespace = "urn:hl7-org:v3")]
    public class Organizer
    {
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public TemplateId TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "urn:hl7-org:v3")]
        public StatusCode StatusCode { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public Component Component { get; set; }
        [XmlAttribute(AttributeName = "classCode")]
        public string ClassCode { get; set; }
        [XmlAttribute(AttributeName = "moodCode")]
        public string MoodCode { get; set; }
    }

    [XmlRoot(ElementName = "structuredBody", Namespace = "urn:hl7-org:v3")]
    public class StructuredBody
    {
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public List<component> Component { get; set; }
    }

    [XmlRoot(ElementName = "ClinicalDocument", Namespace = "urn:hl7-org:v3")]
    [Serializable]
    public class ClinicalDocument
    {
        public ClinicalDocument()
        { }

        [XmlElement(ElementName = "realmCode", Namespace = "urn:hl7-org:v3")]
        public RealmCode RealmCode { get; set; }
        [XmlElement(ElementName = "typeId", Namespace = "urn:hl7-org:v3")]
        public TypeId TypeId { get; set; }
        [XmlElement(ElementName = "templateId", Namespace = "urn:hl7-org:v3")]
        public List<TemplateId> TemplateId { get; set; }
        [XmlElement(ElementName = "id", Namespace = "urn:hl7-org:v3")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "code", Namespace = "urn:hl7-org:v3")]
        public Code Code { get; set; }
        [XmlElement(ElementName = "title", Namespace = "urn:hl7-org:v3")]
        public string Title { get; set; }
        [XmlElement(ElementName = "effectiveTime", Namespace = "urn:hl7-org:v3")]
        public EffectiveTime EffectiveTime { get; set; }
        [XmlElement(ElementName = "confidentialityCode", Namespace = "urn:hl7-org:v3")]
        public ConfidentialityCode ConfidentialityCode { get; set; }
        [XmlElement(ElementName = "languageCode", Namespace = "urn:hl7-org:v3")]
        public LanguageCode LanguageCode { get; set; }
        [XmlElement(ElementName = "setId", Namespace = "urn:hl7-org:v3")]
        public SetId SetId { get; set; }
        [XmlElement(ElementName = "versionNumber", Namespace = "urn:hl7-org:v3")]
        public VersionNumber VersionNumber { get; set; }
        [XmlElement(ElementName = "recordTarget", Namespace = "urn:hl7-org:v3")]
        public RecordTarget RecordTarget { get; set; }
        [XmlElement(ElementName = "author", Namespace = "urn:hl7-org:v3")]
        public Author Author { get; set; }
        [XmlElement(ElementName = "dataEnterer", Namespace = "urn:hl7-org:v3")]
        public DataEnterer DataEnterer { get; set; }
        [XmlElement(ElementName = "informant", Namespace = "urn:hl7-org:v3")]
        public Informant Informant { get; set; }
        [XmlElement(ElementName = "custodian", Namespace = "urn:hl7-org:v3")]
        public Custodian Custodian { get; set; }
        [XmlElement(ElementName = "informationRecipient", Namespace = "urn:hl7-org:v3")]
        public InformationRecipient InformationRecipient { get; set; }
        [XmlElement(ElementName = "legalAuthenticator", Namespace = "urn:hl7-org:v3")]
        public LegalAuthenticator LegalAuthenticator { get; set; }
        [XmlElement(ElementName = "documentationOf", Namespace = "urn:hl7-org:v3")]
        public DocumentationOf DocumentationOf { get; set; }
        [XmlElement(ElementName = "componentOf", Namespace = "urn:hl7-org:v3")]
        public ComponentOf ComponentOf { get; set; }
        [XmlElement(ElementName = "component", Namespace = "urn:hl7-org:v3")]
        public Component Component { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "mif", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Mif { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        //[XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        //public string SchemaLocation { get; set; }

        
        //public ClinicalDocument(string patientId, string appointmentId)
        //{

        //    RealmCode = new RealmCode
        //    {
        //        Code = "Test Data"
        //    }; 
        //    TypeId = new TypeId
        //    {
        //        Extension = "Test Data",
        //        Root = "Test Data"
        //    };
        //    TemplateId = new List<CCDA.TemplateId>
        //    {
        //        new CCDA.TemplateId
        //        {
        //            Root = "Test Data"
        //        }
        //    };
        //    Id = new Id
        //            {
        //                Root = "Test Data",
        //                Extension = "Test Data",
        //                AssigningAuthorityName = "Test Data"
        //            };
        //    Code = new Code
        //            {
        //                CodeSystem = "Test Data",
        //                CodeSystemName = "Test Data",
        //                DisplayName = "Test Data",
        //                _code = "Test Data"
        //            };
        //    Title = "Test Data";
        //    EffectiveTime = new EffectiveTime
        //            {
        //                Value = "Test Data"
        //            };
        //    ConfidentialityCode = new ConfidentialityCode
        //            {
        //                Code = "Test Data",
        //                CodeSystem = "Test Data"
        //            };
        //    LanguageCode = new LanguageCode
        //      {
        //                Code = "Test Data"
        //      };
        //    SetId = new SetId
        //            {
        //                Extension = "Test Data",
        //                Root = "Test Data"
        //            };
        //    VersionNumber = new VersionNumber
        //            {
        //                Value = "Test Data"
        //            };
            
        //    RecordTarget = new RecordTarget
        //    {
        //        PatientRole = new PatientRole
        //        {
        //            Id = new List<Id>
        //                       {
        //                           new Id
        //                           {
        //                               AssigningAuthorityName = "Test Data",
        //                               Extension = "Test Data",
        //                               Root = "Test Data"
        //                           }
        //                       },
        //            Addr = new Addr
        //            {
        //                City = "Test Data",
        //                Country = "Test Data",
        //                State = "Test Data",
        //                StreetAddressLine = "Test Data",
        //                Use = "Test Data", 
        //                PostalCode = new PostalCode
        //                {
        //                     NullFlavor = "Test Data"
        //                }
        //            },
        //            Telecom = new Telecom
        //            {
        //                Use = "Test Data",
        //                Value = "Test Data"
        //            },
        //            Patient = new Patient
        //            {
        //                Name = new Name
        //                {
        //                    Given = "Test Data",
        //                    Use = "Test Data",
        //                    Family = "Test Data"
        //                },
        //                AdministrativeGenderCode = new AdministrativeGenderCode
        //                {
        //                    Code = "Test Data",
        //                    CodeSystem = "Test Data",
        //                    CodeSystemName = "Test Data",
        //                    DisplayName = "Test Data"
        //                },
        //                BirthTime = new BirthTime
        //                {
        //                    Value = "Test Data"
        //                },
        //                MaritalStatusCode = new MaritalStatusCode
        //                {
        //                    DisplayName = "Test Data",
        //                    CodeSystemName = "Test Data",
        //                    CodeSystem = "Test Data",
        //                    Code = "Test Data"
        //                },
        //                ReligiousAffiliationCode = new ReligiousAffiliationCode
        //                {
        //                    CodeSystem = "Test Data",
        //                    CodeSystemName = "Test Data",
        //                    NullFlavor = "Test Data"
        //                },
        //                RaceCode = new RaceCode
        //                {
        //                    NullFlavor = "Test Data",
        //                    CodeSystemName = "Test Data",
        //                    CodeSystem = "Test Data",
        //                    DisplayName = "Test Data"
        //                },
        //                Birthplace = new Birthplace
        //                {
        //                    Place = new Place
        //                    {
        //                        Addr = "Test Data"
        //                    }
        //                },
        //                EthnicGroupCode = new EthnicGroupCode
        //                {
        //                    DisplayName = "Test Data",
        //                    CodeSystem = "Test Data",
        //                    CodeSystemName = "Test Data",
        //                    NullFlavor = "Test Data"
        //                },
        //                LanguageCommunication = new LanguageCommunication
        //                {
        //                    LanguageCode = new LanguageCode
        //                    {
        //                        Code = "Test Data"
        //                    },
        //                    ModeCode = new ModeCode
        //                    {
        //                        NullFlavor = "Test Data",
        //                        CodeSystem = "Test Data",
        //                        CodeSystemName = "Test Data"
        //                    },
        //                    PreferenceInd = new PreferenceInd
        //                    {
        //                        Value = "Test Data"
        //                    },
        //                    ProficiencyLevelCode = new ProficiencyLevelCode
        //                    {
        //                        CodeSystem = "Test Data",
        //                        CodeSystemName = "Test Data",
        //                        NullFlavor = "Test Data"
        //                    }
        //                }

        //            },
        //            ProviderOrganization = new ProviderOrganization
        //            {
        //                Addr = new Addr
        //                {
        //                    City = "Test Data",
        //                    Country = "Test Data",
        //                    PostalCode = new PostalCode
        //                    {
        //                        NullFlavor = "Test Data"
        //                    },
        //                    State = "Test Data",
        //                    StreetAddressLine = "Test Data",
        //                    Use = "Test Data"
        //                },
        //                StandardIndustryClassCode = "Test Data",
        //                AsOrganizationPartOf = "Test Data",
        //                Id = new List<Id>
        //                            {
        //                                new Id
        //                                {
        //                                    Extension = "Test Data",
        //                                    Root = "Test Data",
        //                                    AssigningAuthorityName = "Test Data"
        //                                }
        //                            },
        //                Name = "Test Data",
        //                Telecom = new Telecom
        //                {
        //                    NullFlavor = "Test Data",
        //                    Use = "Test Data"
        //                }

        //            }

        //        }
        //    };

        //    Author = new Author
        //    {
        //        AssignedAuthor = new AssignedAuthor
        //        {
        //            AssignedPerson = new AssignedPerson
        //            {
        //                Name = new Name
        //                {
        //                    Family = "Test Data",
        //                    Given = "Test Data",
        //                    Suffix = "Test Data"
        //                }
        //            },
        //            Addr = new Addr
        //                {
        //                    StreetAddressLine = "Test Data",
        //                    City = "Test Data",
        //                    Country = "Test Data",
        //                    PostalCode = new PostalCode
        //                    {
        //                        NullFlavor = "Test Data"
        //                    },
        //                    State = "Test Data"
        //                },
        //            Telecom = new Telecom
        //            {
        //                Use = "Test Data",
        //                NullFlavor = "Test Data"
        //            },
        //            Code = new Code
        //            {
        //                DisplayName = "Test Data",
        //                CodeSystemName = "Test Data",
        //                CodeSystem = "Test Data",
        //                _code = "Test Data"
        //            }
        //        },
        //        Time = new Time
        //            {
        //                Value = "Test Data"
        //            }
        //    };

        //    DataEnterer = new DataEnterer
        //    {
        //        AssignedEntity = new 
        //        AssignedEntity
        //        {
        //            Id = new Id
        //            {
        //                Extension = "Test Data"
        //            },
        //            Addr = new Addr
        //            {
        //                State = "Test Data",
        //                PostalCode = new PostalCode
        //                {
        //                    NullFlavor = "Test Data"
        //                },
        //                Country = "Test Data",
        //                City = "Test Data",
        //                StreetAddressLine = "Test Data",
        //                Use = "Test Data"
        //            }
        //        }
        //    };

        //    Informant = new Informant                                                                                                                   
        //    {
        //        AssignedEntity = new AssignedEntity
        //        {
        //            Addr = new Addr
        //            {
        //                Use = "Test Data",
        //                StreetAddressLine = "Test Data",
        //                City = "Test Data",
        //                Country = "Test Data",
        //                PostalCode = new PostalCode
        //                {
        //                    NullFlavor = "Test Data"
        //                },
        //                State = "Test Data"
        //            },
        //            AssignedPerson = new AssignedPerson
        //            {
        //                Name = new Name
        //                {
        //                    Given = "Test Data",
        //                    Family = "Test Data",
        //                    Suffix = "Test Data"
        //                }
        //            },
        //            Id = new Id
        //            {
        //                Extension = "Test Data",
        //                Root = "Test Data"
        //            },
        //            Telecom = new Telecom
        //                {
        //                    Use = "Test Data",
        //                    NullFlavor = "Test Data"
        //                }
        //        }
        //    };

        //    Custodian = new Custodian
        //    {
        //        AssignedCustodian = new AssignedCustodian
        //        {
        //            RepresentedCustodianOrganization = new RepresentedCustodianOrganization
        //            {
        //                Addr = new Addr
        //                {
        //                    Use = "Test Data",
        //                    StreetAddressLine = "Test Data",
        //                    City = "Test Data",
        //                    Country = "Test Data",
        //                    PostalCode = new PostalCode
        //                    {
        //                        NullFlavor = "Test Data"
        //                    },
        //                    State = "Test Data"
        //                },
        //                Telecom = new Telecom
        //                {
        //                    Use = "Test Data",
        //                    NullFlavor = "Test Data"
        //                },
        //                Name = "Test Data",
        //                Id = new Id
        //                {
        //                    Extension = "Test Data",
        //                    Root = "Test Data"
        //                }
        //            }
        //        }
        //    };

        //    InformationRecipient = new InformationRecipient
        //    {
        //        Name = "Test Data"
        //    };

        //    LegalAuthenticator = new LegalAuthenticator
        //    {
        //        AssignedEntity = new AssignedEntity
        //        {
        //            Addr = new Addr
        //            {
        //                Use = "Test Data",
        //                StreetAddressLine = "Test Data",
        //                City = "Test Data",
        //                Country = "Test Data",
        //                PostalCode = new PostalCode
        //                {
        //                    NullFlavor = "Test Data"
        //                },
        //                State = "Test Data"
        //            },
        //            AssignedPerson = new AssignedPerson
        //            {
        //                Name = new Name
        //                {
        //                    Given = "Test Data",
        //                    Family = "Test Data"
        //                }
        //            },
        //            Code = new Code
        //            {
        //                CodeSystem = "Test Data"
        //            },
        //            Id = new Id
        //            {
        //                AssigningAuthorityName = "Test Data",
        //                Extension = "Test Data",
        //                Root = "Test Data"
        //            },
        //            Telecom = new Telecom
        //            {
        //                Use = "Test Data",
        //                NullFlavor = "Test Data"
        //            }
        //        }
        //    };

        //    DocumentationOf = new DocumentationOf
        //    {
        //        ServiceEvent = new ServiceEvent
        //        {
        //            ClassCode = "Test Data",
        //            Id = new List<Id>
        //                  {
        //                      new Id
        //                      {
        //                          AssigningAuthorityName ="Test Data",
        //                          Extension = "Test Data",
        //                          Root = "Test Data"
        //                      },
        //                      new Id
        //                      {
        //                          AssigningAuthorityName ="Test Data",
        //                          Extension = "Test Data",
        //                          Root = "Test Data"
        //                      }
        //                  },
        //            Code = new Code
        //            {
        //                CodeSystem = "Test Data",
        //                CodeSystemName = "Test Data",
        //                DisplayName = "Test Data"
        //            },
        //            EffectiveTime = new EffectiveTime
        //            {
        //                Low = new Low
        //                {
        //                    Value = "Test Data"
        //                },
        //                High = new High
        //                {
        //                    Value = "Test Data"
        //                }
        //            },
        //            Performer = new Performer
        //            {
        //                TypeCode = "Test Data",
        //                AssignedEntity = new AssignedEntity
        //                {
        //                    Addr = new Addr
        //                    {
        //                        Use = "Test Data",
        //                        StreetAddressLine = "Test Data",
        //                        City = "Test Data",
        //                        Country = "Test Data",
        //                        PostalCode = new PostalCode
        //                        {
        //                            NullFlavor = "Test Data"
        //                        },
        //                        State = "Test Data"
        //                    },
        //                    AssignedPerson = new AssignedPerson
        //                    {
        //                        Name = new Name
        //                        {
        //                            Given = "Test Data",
        //                            Family = "Test Data"
        //                        }
        //                    },
        //                    Code = new Code
        //                    {
        //                        CodeSystem = "Test Data"
        //                    },
        //                    Id = new Id
        //                    {
        //                        AssigningAuthorityName = "Test Data",
        //                        Extension = "Test Data",
        //                        Root = "Test Data"
        //                    },
        //                    Telecom = new Telecom
        //                    {
        //                        Use = "Test Data",
        //                        NullFlavor = "Test Data"
        //                    },
        //                },

        //            }
        //        },

        //    };

        //    Component = new List<Component> 
        //    {
        //        new Component{
        //        section = new section
        //        {
        //                Code = new Code
        //            {
        //                _code = "Test Data",
        //                CodeSystem = "Test Data"
        //            },
        //                Title = "Test Data",
        //                Text = new Text
        //            {
        //                Table = new Table
        //                {
        //                    Width = "100%",
        //                    Border = "1",
        //                    Thead = new Thead
        //                    {
        //                        Tr = new Tr
        //                        {
        //                            Th = new List<Th>
        //                        {
        //                            new Th{ Text="Type"}, new Th{ Text="Substance"}, new Th{ Text="Reaction"}, new Th{ Text="Status"}
        //                        }
        //                        }
        //                    },
        //                    Tbody = new Tbody
        //                    {
        //                        Tr = new List<Tr>
        //                        {
        //                            new Tr
        //                            {
        //                                ID ="ALGSUMMARY_1" ,
        //                                Td =new List<Td>
        //                                {
        //                                    new Td
        //                                    {
        //                                        ID = "ALGTYPE_1",
        //                                        Text = "Drug Allergy"
        //                                    },
        //                                    new Td
        //                                    {
        //                                        ID = "ALGSUB_1",
        //                                        Text = "Sumatriptan, [RxNorm: 37418]"
        //                                    },
        //                                    new  Td
        //                                    {
        //                                        ID ="ALGREACT_1",
        //                                        Text ="Contraindication, Difficulty Breathing"
        //                                    },
        //                                    new Td
        //                                    {
        //                                        ID ="ALGSTATUS_1",
        //                                        Text ="Active"
        //                                    }
        //                                },
        //                            }

        //                         }
        //                    }
        //                }
        //            },
        //                Entry = new List<Entry>
        //                    {
        //                        new Entry
        //                        {
        //                            TypeCode = "Test Data",
        //                            Act= new Act
        //                            {
        //                                ClassCode = "Test Data",
        //                                MoodCode ="Test Data",
        //                                TemplateId= new CCDA.TemplateId
        //                                {
        //                                    Root ="Test Data"
        //                                },
        //                                Id = new Id
        //                                {
        //                                    Root ="Test Data"
        //                                },
        //                                Code= new Code
        //                                {
        //                                    CodeSystem ="Test Data"
        //                                },
        //                                StatusCode  =   new StatusCode
        //                                {
        //                                    Code ="active"
        //                                },
        //                                EffectiveTime = new EffectiveTime
        //                                {
        //                                    High =new High
        //                                    {
        //                                        Value =""
        //                                    },
        //                                    Low = new Low
        //                                    {
        //                                        Value =""
        //                                    }
        //                                },
        //                                EntryRelationship= new EntryRelationship
        //                                {
        //                                    Observation =new Observation
        //                                    {
        //                                        ClassCode ="Test Data",
        //                                        MoodCode ="Test Data",
        //                                        Id =new Id
        //                                        {
        //                                            Root ="Test Data"
        //                                        },
        //                                        Text   =   new Text
        //                                        {
        //                                            Reference = new Reference
        //                                            {
        //                                                Value = "Test Data"
        //                                            }
        //                                        },
        //                                        Code=new Code
        //                                        {
        //                                            _code ="Test Data",
        //                                            CodeSystem ="Test Data"
        //                                        },
        //                                        StatusCode= new StatusCode
        //                                        {
        //                                            Code ="Test Data"
        //                                        },
        //                                        EffectiveTime = new EffectiveTime
        //                                        {
        //                                            High =new High
        //                                            {
        //                                                Value ="Test Data"
        //                                            },
        //                                            Low =new Low
        //                                            {
        //                                                    Value ="Test Data"
        //                                            }
        //                                        },
        //                                        TemplateId= new CCDA.TemplateId
        //                                        {
        //                                            Root = "Test Data"
        //                                        },
        //                                        Value =new Value
        //                                        {
        //                                            Code ="Test Data" ,
        //                                            CodeSystem ="Test Data",
        //                                            CodeSystemName ="Test Data",
        //                                            DisplayName ="Test Data",
        //                                            OriginalText=new OriginalText
        //                                            {
        //                                                Reference =new Reference
        //                                                {
        //                                                    Value = "Test Data"
        //                                                }
        //                                            },
        //                                        },
        //                                        Participant = new Participant
        //                                            {
        //                                                ParticipantRole = new ParticipantRole
        //                                                {
        //                                                    ClassCode = "Test Data",
        //                                                    PlayingEntity = new PlayingEntity
        //                                                    {
        //                                                        ClassCode ="Test Data",
        //                                                        Code =new Code
        //                                                        {
        //                                                            _code =  "Test Data",
        //                                                            CodeSystem = "Test Data",
        //                                                            CodeSystemName = "Test Data",
        //                                                            DisplayName= "Test Data",
        //                                                            NullFlavor= "Test Data",
        //                                                            OriginalText=new OriginalText
        //                                                            {
        //                                                                Reference = new Reference
        //                                                                {
        //                                                                    Value = "Test Data"
        //                                                                }
        //                                                            }
        //                                                        },
        //                                                        Name   =   "Test Data"
        //                                                    }
        //                                                }
        //                                            },
        //                                        EntryRelationship = new EntryRelationship
        //                                        {
        //                                              InversionInd= "Test Data",
        //                                              TypeCode = "Test Data",
        //                                              Observation  = new Observation
        //                                              {
        //                                                ClassCode =    "Test Data",
        //                                                MoodCode =   "Test Data",
        //                                                TemplateId = new CCDA.TemplateId
        //                                                {
        //                                                    Root = "Test Data",
        //                                                },
        //                                                Code = new Code
        //                                                {
        //                                                    _code    =    "Test Data",
        //                                                    CodeSystem  =   "Test Data",
        //                                                    CodeSystemName =   "Test Data",
        //                                                    DisplayName   =   "Test Data",
        //                                                },
        //                                                StatusCode = new StatusCode
        //                                                {
        //                                                    Code  =    "Test Data",
        //                                                },
        //                                                Value =   new Value
        //                                                {
        //                                                    CodeSystem= "Test Data",
        //                                                    DisplayName =   "Test Data",
        //                                                    NullFlavor =    "Test Data",
        //                                                    Type   =   "Test Data"
        //                                                },
        //                                                EffectiveTime   =   new EffectiveTime
        //                                                {
        //                                                    Low    =    new Low
        //                                                    {
        //                                                        Value =   "Test Data"
        //                                                    },
        //                                                    High   =    new High
        //                                                    {
        //                                                        Value =   "Test Data"
        //                                                    }
        //                                                }
        //                                              }
        //                                        }
        //                                    }
        //                                },
        //                            },
        //                            SubstanceAdministration    = new SubstanceAdministration
        //                             {
        //                                   ClassCode    =   "Test Data",
        //                                    MoodCode    =   "Test Data",
        //                                    TemplateId   =   new CCDA.TemplateId
        //                                    {
        //                                        Root = "Test Data"
        //                                    },
        //                                     Id= new Id
        //                                     {
        //                                         Root ="Test Data"
        //                                     },
        //                                     Text   =   new Text
        //                                     {
        //                                         Reference  =   new Reference
        //                                         {
        //                                                Value="Test Data"
        //                                         }
        //                                     },
        //                                     StatusCode =   new StatusCode
        //                                     {
        //                                         Code   =   "completed"
        //                                     },
        //                                     EffectiveTime  =   new EffectiveTime
        //                                     {
        //                                         Low = new Low
        //                                         {
        //                                              Value =   "Test Data"
        //                                         },
        //                                         High   =   new High
        //                                         {
        //                                             Value  =   "Test Data"
        //                                         }
        //                                     },
        //                                     DoseQuantity  =   new DoseQuantity
        //                                      {
        //                                           Value    =   "Test Data",
        //                                            Unit    =   "Test Data"
        //                                      },
        //                                     Consumable   =   new Consumable
        //                                       {
        //                                            ManufacturedProduct =    new ManufacturedProduct
        //                                            {
        //                                                 TemplateId =   new CCDA.TemplateId
        //                                                 {
        //                                                      Root  =   "Test Data"
        //                                                 },
        //                                                  Id    =   new Id
        //                                                  {
        //                                                        Root    =   "Test Data"
        //                                                  },
        //                                                  ManufacturedMaterial  =   new ManufacturedMaterial
        //                                                  {
        //                                                       Code =   new Code
        //                                                       {
        //                                                             CodeSystem =   "Test Data",
        //                                                             CodeSystemName    =   "Test Data",
        //                                                             DisplayName  =   "Test Data",
        //                                                             _code   =   "Test Data",
        //                                                              OriginalText  =   new OriginalText
        //                                                              {
        //                                                                  Reference =   new Reference
        //                                                                  {
        //                                                                       Value    =   "Test Data"
        //                                                                  }
        //                                                              },
        //                                                               Translation  =   new Translation
        //                                                               {
        //                                                                    CodeSystem  =   "Test Data",
        //                                                                     DisplayName    =   "Test Data",
        //                                                                     CodeSystemName =   "Test Data"

        //                                                               }
        //                                                       },
        //                                                       Name   =   "Test Data"
        //                                                  },
        //                                                  ManufacturerOrganization =   new ManufacturerOrganization
        //                                                   {
        //                                                        Name    =   "Test Data"
        //                                                   },
        //                                                  ClassCode = "Test Data"
        //                                            }
        //                                       }
        //                             }

        //                        }
        //                    }

        //        }
        //    }
        //    };
        //}

        //Get DataTable
        //private DataTable GetDataSet(string slctqry, Dictionary<string, object> param)
        //{
        //    DataTable dataTable = new DataTable();
        //    using (var dbConnection = DbConnectionFactory.PracticeRepository)
        //    {
        //        dataTable = dbConnection.Execute<DataTable>(slctqry);
        //    }
        //    return dataTable;
        //}
    }
}

            

            
        
    





