﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using IO.Practiceware.Storage;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    /// <summary>
    ///     Interaction logic for GenerateQrdaView.xaml
    /// </summary>
    public partial class GenerateQrdaView
    {
        public GenerateQrdaView()
        {
            InitializeComponent();
        }
    }

    public class GenerateQrdaViewContext : IViewContext
    {
        private readonly IGenerateQrdaViewService _generateQrdaViewService;
        private readonly string _dropFilesLocation;

        public GenerateQrdaViewContext(IGenerateQrdaViewService generateQrdaViewService)
        {
            _generateQrdaViewService = generateQrdaViewService;
            _dropFilesLocation = Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.Desktop), "IO Practiceware Export");

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Search = Command.Create(ExecuteSearch, () => SearchCriteria.IsValid()).Async(() => InteractionContext);
            GenerateCategory1Files = Command.Create(ExecuteGenerateCategory1Files, () => SearchCriteria.IsValid() && SelectedResults.Any()).Async(() => InteractionContext);
            GenerateCategory3Report = Command.Create(ExecuteGenerateCategory3Report, () => SearchCriteria.IsValid() && SelectedResults.Any()).Async(() => InteractionContext);
            GenerateAllCategory1Files = Command.Create(ExecuteGenerateAllCategory1Files, () => SearchCriteria.IsValid()).Async(() => InteractionContext);
            GenerateAllCategory3Reports = Command.Create(ExecuteGenerateAllCategory3Reports, () => SearchCriteria.IsValid()).Async(() => InteractionContext);
            GenerateCategory3ReportsForAllMeasures = Command.Create(ExecuteGenerateCategory3ReportsForAllMeasures, () => SearchCriteria.IsValid()).Async(() => InteractionContext);
        }

        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> Users { get; set; }

        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> Measures { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> SearchResults { get; set; }

        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedResults { get; set; }

        [Dependency]
        public virtual GenerateQrdaSearchCriteria SearchCriteria { get; set; }

        public virtual ICommand Load { get; protected set; }

        public virtual ICommand Search { get; protected set; }

        public virtual ICommand GenerateCategory1Files { get; protected set; }

        public virtual ICommand GenerateAllCategory1Files { get; protected set; }

        public virtual ICommand GenerateCategory3Report { get; protected set; }

        public virtual ICommand GenerateAllCategory3Reports { get; protected set; }

        public virtual ICommand GenerateCategory3ReportsForAllMeasures { get; protected set; }

        public virtual IInteractionContext InteractionContext { get; set; }

        public void ExecuteLoad()
        {
            GenerateQrdaViewLoadInformation info = _generateQrdaViewService.GetLoadInformation();
            Users = info.Users;
            Measures = info.Measures;

            // TODO: remove after test
            // Search criteria preset for the test
            SearchCriteria.StartDate = new DateTime(2012, 1, 1);
            SearchCriteria.EndDate = new DateTime(2012, 12, 31);
            SearchCriteria.User = Users.FirstOrDefault(u => u.Name == "CQM");
            SearchCriteria.Measure = Measures.First();

            // Ensure every next change of search criteria will remove the results
            SearchCriteria.As<INotifyPropertyChanged>().PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(
                (sender, args) => { if (SearchResults != null) SearchResults.Clear(); });
        }

        private void ExecuteGenerateCategory1Files()
        {
            var patientIds = SelectedResults.Select(r => r.Id).ToArray();
            var results = _generateQrdaViewService.GenerateQrdaCategory1Files(patientIds, SearchCriteria.Measure.Id, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate);

            var exportPath = GetPathNameForNewExport("QRDA Category I", SearchCriteria.User.Name, ToShortMeasureName(SearchCriteria.Measure));
            ExportQrdaCategory1Files(results, exportPath);

            // Open drop location
            if (Directory.Exists(exportPath)) Process.Start(exportPath);
        }

        private void ExecuteGenerateAllCategory1Files()
        {
            var exportPath = GetPathNameForNewExport("QRDA Category I", SearchCriteria.User.Name, "All");

            // Export for every measure
            foreach (var measure in Measures)
            {
                SearchCriteria.Measure = measure;

                try
                {
                    ExecuteSearch();

                    var patientIds = SearchResults.Select(r => r.Id).ToArray();
                    var results = _generateQrdaViewService.GenerateQrdaCategory1Files(patientIds, SearchCriteria.Measure.Id, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate);

                    var measureSpecificExportPath = Path.Combine(exportPath, ToShortMeasureName(SearchCriteria.Measure));
                    ExportQrdaCategory1Files(results, measureSpecificExportPath);
                }
                catch (Exception ex)
                {
                    this.Dispatcher().Invoke(() => InteractionManager.Current.DisplayDetailedException(new InteractiveException(
                        "Export will continue. Error occurred while generating category 1 files for measure: " + SearchCriteria.Measure.Name, 
                        ex)));
                }
            }

            // Open drop location
            if (Directory.Exists(exportPath)) Process.Start(exportPath);
        }

        private void ExecuteGenerateAllCategory3Reports()
        {
            var exportPath = GetPathNameForNewExport("QRDA Category III", SearchCriteria.User.Name, "All");

            // Export for every measure
            foreach (var measure in Measures)
            {
                SearchCriteria.Measure = measure;

                try
                {
                    ExecuteSearch();

                    // Generate report over all patients
                    var patientIds = SearchResults.Select(r => r.Id).ToArray();
                    var result = _generateQrdaViewService.GenerateQrdaCategory3Report(patientIds, new[] { SearchCriteria.Measure.Id }, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate);

                    var measureSpecificExportPath = Path.Combine(exportPath, ToShortMeasureName(SearchCriteria.Measure));

                    // Write Qrda 1 files and Qrda 3 report
                    if (result.PatientsSource != null) ExportQrdaCategory1Files(result.PatientsSource, measureSpecificExportPath);
                    FileSystem.TryWriteFile(Path.Combine(measureSpecificExportPath, "Report.xml"), result.QrdaXml);
                }
                catch (Exception ex)
                {
                    this.Dispatcher().Invoke(() => InteractionManager.Current.DisplayDetailedException(new InteractiveException(
                        "Export will continue. Error occurred while generating category 1 files for measure: " + SearchCriteria.Measure.Name,
                        ex)));
                }
            }

            // Open drop location
            if(Directory.Exists(exportPath)) Process.Start(exportPath);
        }

        private void ExecuteGenerateCategory3Report()
        {
            // Generate report over all patients
            var patientIds = SelectedResults.Select(r => r.Id).ToArray();
            var result = _generateQrdaViewService.GenerateQrdaCategory3Report(patientIds, new[] { SearchCriteria.Measure.Id }, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate);

            // Write Qrda 1 files and Qrda 3 report
            var exportPath = GetPathNameForNewExport("QRDA Category III", SearchCriteria.User.Name, ToShortMeasureName(SearchCriteria.Measure));
            if (result.PatientsSource != null) ExportQrdaCategory1Files(result.PatientsSource, exportPath);
            FileSystem.TryWriteFile(Path.Combine(exportPath, "Report.xml"), result.QrdaXml);

            // Open drop location
            if (Directory.Exists(exportPath)) Process.Start(exportPath);
        }

        private void ExecuteSearch()
        {
            SearchResults = _generateQrdaViewService.FindPatients(SearchCriteria.Measure.Id, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate).ToObservableCollection();
        }

        private void ExecuteGenerateCategory3ReportsForAllMeasures()
        {
            var patientIds = Measures.SelectMany(m =>
                _generateQrdaViewService.FindPatients(m.Id, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate))
                .Select(p => p.Id).Distinct().ToArray();

            var measureIds = Measures.Select(m => m.Id).ToArray();

            var result = _generateQrdaViewService.GenerateQrdaCategory3Report(patientIds, measureIds, SearchCriteria.User.Id, SearchCriteria.StartDate, SearchCriteria.EndDate);

            var exportPath = GetPathNameForNewExport("QRDA Category III", SearchCriteria.User.Name, "All Measures");

            FileSystem.TryWriteFile(Path.Combine(exportPath, "Report.xml"), result.QrdaXml);

            if (result.PatientsSource != null) ExportQrdaCategory1Files(result.PatientsSource, exportPath);

            // Open drop location
            if (Directory.Exists(exportPath)) Process.Start(exportPath);
        }

        private static string ToShortMeasureName(NamedViewModel measure)
        {
            return typeof(ElectronicClinicalQualityMeasure).GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(f => new { MeasureId = ((ElectronicClinicalQualityMeasure)f.GetValue(null)).Id, ShortName = f.Name })
                .Where(m => m.MeasureId == measure.Id)
                .Select(m => m.ShortName)
                .First();
        }

        private void ExportQrdaCategory1Files(IEnumerable<QrdaCategory1ViewModel> qrdaCategory1ViewModels, string exportPath)
        {
            foreach (var r in qrdaCategory1ViewModels)
            {
                var result = r;
                var patientName = SearchResults == null ? r.PatientId.ToString() : SearchResults.Where(p => p.Id == result.PatientId).Select(p => p.Name.Trim()).FirstOrDefault() ?? r.PatientId.ToString();
                
                var filePath = Path.Combine(exportPath, string.Format("{0}_{1}.xml", result.PatientId, patientName));
                FileSystem.TryWriteFile(filePath, result.QrdaXml);
            }
        }

        private string GetPathNameForNewExport(string exportName, string userName, string measureName)
        {
            var result = Path.Combine(_dropFilesLocation, string.Format("{0}--{1}--{2}--{3}",
                exportName.ToSafePathName(),
                DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss"),
                userName.ToSafePathName(),
                measureName.ToSafePathName()));

            return result;
        }
    }

    public class GenerateQrdaSearchCriteria : IViewModel
    {
        public GenerateQrdaSearchCriteria()
        {
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
        }

        [RequiredWithDisplayName(AllowDefaultValuesForValueTypes = false)]
        public virtual DateTime StartDate { get; set; }

        [RequiredWithDisplayName(AllowDefaultValuesForValueTypes = false)]
        public virtual DateTime EndDate { get; set; }

        [RequiredWithDisplayName(AllowDefaultValuesForValueTypes = false)]
        public virtual NamedViewModel User { get; set; }

        [RequiredWithDisplayName(AllowDefaultValuesForValueTypes = false)]
        public virtual NamedViewModel Measure { get; set; }
    }
}