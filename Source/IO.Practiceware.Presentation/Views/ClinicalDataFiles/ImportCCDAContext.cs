﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    public class ImportCCDAContext
    {
        public DataTable SearchPatient(string FirstName, string LastName, string PatientDOB)
        {
            DataTable dtPatient = new DataTable();
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select Id, LastName, FirstName from model.Patients Where LastName = '" + LastName + "' and FirstName = '" + FirstName + "' " +
                    " and DateOfBirth = '" + PatientDOB + "'";
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatient.Load(reader);
                    }
                }
                dbConnection.Close();
            }
            return dtPatient;
        }

        public int CreateNewPatient(string FirstName, string LastName, string DOB, string LanguageCode, string RaceCode, string EthnicityCode, string GenderCode, string AddressLine1, string AddressLine2, string City, string State, string ZipCode,string ProviderName)
        {
            int retPatientId = 0;
            DataTable dtPatient = new DataTable();
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                string _selectQry = "Exec Model.CCDA_CreateNewPatient '" + FirstName + "', '" + LastName + "', '" + DOB + "', '" + LanguageCode + "', '" + RaceCode + "', '" + EthnicityCode + "', '" + GenderCode + "', '" + AddressLine1 + "', '" + AddressLine2 + "', '" + City + "', '" + State + "', '" + ZipCode + "'";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatient.Load(reader);
                        if (dtPatient.Rows.Count > 0)
                        {
                            retPatientId = int.Parse(dtPatient.Rows[0]["PatientId"].ToString());
                        }
                    }
                }
                dbConnection.Close();
            }
            return retPatientId;
        }

        public void InsertReconcileAllergy(int PatientId, string AllergyType, string AllergySubstance, string AllergyReaction)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileAllergy(PatientId, AllergyType, AllergySubstance, AllergyReaction, Status) Values ('" + PatientId + "','" + AllergyType + "', '" + AllergySubstance + "', '" + AllergyReaction + "', '0')";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }

        public void InsertReconcileMedication(int PatientId, string StartDate, string Frequency, string DrugName)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileMedications(PatientId, StartDate, Frequency, DrugName, Status) Values ('" + PatientId + "','" + StartDate + "', '" + Frequency + "', '" + DrugName + "', 0)";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }

        public void InsertReconcileProblems(int PatientId, string EffectiveDate, string SnomedCT, string DiagnosisCode, string DiagnosisDescription)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileProblems(PatientId, EffectiveDate, SnomedCT, DiagnosisCode, DiagnosisDescription, Status) Values ('" + PatientId + "','" + EffectiveDate + "', '" + SnomedCT + "', '" + DiagnosisCode + "', '" + DiagnosisDescription + "', 0);";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }

        public void InsertReconcilePractice(int PatientId, string ProviderName,string PhoneNumber, string AddressLine1, string City, string State, string Zip, string DocumentId,string TypeOfSummaryOfCare, string providerFirstName, string providerLastName, string providerPracticeName)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcilePracticeInformation(PatientId, ProviderName, PhoneNumber, AddressLine1, City, State, Zip, CreatedDateTime, ExternalSystemMessagesId, PatientType, Sender_FirstName, Sender_LastName, Sender_PracticeName) " +
                    "Values(@PatientId, @ProviderName, @PhoneNumber, @AddressLine1, @City, @State, @Zip, @CreateDateTime, @ExternalSystemMessagesId, @TypeOfSummaryOfCare, @providerFirstName, @providerLastName, @providerPracticeName);";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {

                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = PatientId;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parProviderName = cmd.CreateParameter();
                    parProviderName.Direction = ParameterDirection.Input;
                    parProviderName.Value = ProviderName;
                    parProviderName.ParameterName = "@ProviderName";
                    parProviderName.DbType = DbType.String;
                    cmd.Parameters.Add(parProviderName);

                    IDbDataParameter parPhoneNumber = cmd.CreateParameter();
                    parPhoneNumber.Direction = ParameterDirection.Input;
                    parPhoneNumber.Value = PhoneNumber;
                    parPhoneNumber.ParameterName = "@PhoneNumber";
                    parPhoneNumber.DbType = DbType.String;
                    cmd.Parameters.Add(parPhoneNumber);

                    IDbDataParameter parAddressLine1 = cmd.CreateParameter();
                    parAddressLine1.Direction = ParameterDirection.Input;
                    parAddressLine1.Value = AddressLine1;
                    parAddressLine1.ParameterName = "@AddressLine1";
                    parAddressLine1.DbType = DbType.String;
                    cmd.Parameters.Add(parAddressLine1);

                    IDbDataParameter parCity = cmd.CreateParameter();
                    parCity.Direction = ParameterDirection.Input;
                    parCity.Value = City;
                    parCity.ParameterName = "@City";
                    parCity.DbType = DbType.String;
                    cmd.Parameters.Add(parCity);

                    IDbDataParameter parState = cmd.CreateParameter();
                    parState.Direction = ParameterDirection.Input;
                    parState.Value = State;
                    parState.ParameterName = "@State";
                    parState.DbType = DbType.String;
                    cmd.Parameters.Add(parState);

                    IDbDataParameter parZip = cmd.CreateParameter();
                    parZip.Direction = ParameterDirection.Input;
                    parZip.Value = string.IsNullOrEmpty(Zip)?string.Empty:Zip;
                    parZip.ParameterName = "@Zip";
                    parZip.DbType = DbType.String;
                    cmd.Parameters.Add(parZip);

                    IDbDataParameter parCreateDateTime= cmd.CreateParameter();
                    parCreateDateTime.Direction = ParameterDirection.Input;
                    parCreateDateTime.Value = DateTime.Now;
                    parCreateDateTime.ParameterName = "@CreateDateTime";
                    parCreateDateTime.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parCreateDateTime);

                    IDbDataParameter paramExternalSystemMessagesId = cmd.CreateParameter();
                    paramExternalSystemMessagesId.Direction = ParameterDirection.Input;
                    paramExternalSystemMessagesId.Value = DocumentId;
                    paramExternalSystemMessagesId.ParameterName = "@ExternalSystemMessagesId";
                    paramExternalSystemMessagesId.DbType = DbType.String;
                    cmd.Parameters.Add(paramExternalSystemMessagesId);

                    IDbDataParameter paramTypeOfSummaryOfCare = cmd.CreateParameter();
                    paramTypeOfSummaryOfCare.Direction = ParameterDirection.Input;
                    paramTypeOfSummaryOfCare.Value = TypeOfSummaryOfCare;
                    paramTypeOfSummaryOfCare.ParameterName = "@TypeOfSummaryOfCare";
                    paramTypeOfSummaryOfCare.DbType = DbType.String;
                    cmd.Parameters.Add(paramTypeOfSummaryOfCare);

                    IDbDataParameter paramProviderFName = cmd.CreateParameter();
                    paramProviderFName.Direction = ParameterDirection.Input;
                    paramProviderFName.Value = providerFirstName;
                    paramProviderFName.ParameterName = "@providerFirstName";
                    paramProviderFName.DbType = DbType.String;
                    cmd.Parameters.Add(paramProviderFName);

                    IDbDataParameter paramProviderLName = cmd.CreateParameter();
                    paramProviderLName.Direction = ParameterDirection.Input;
                    paramProviderLName.Value = providerLastName;
                    paramProviderLName.ParameterName = "@providerLastName";
                    paramProviderLName.DbType = DbType.String;
                    cmd.Parameters.Add(paramProviderLName);

                    IDbDataParameter paramProviderPName = cmd.CreateParameter();
                    paramProviderPName.Direction = ParameterDirection.Input;
                    paramProviderPName.Value = providerPracticeName;
                    paramProviderPName.ParameterName = "@providerPracticeName";
                    paramProviderPName.DbType = DbType.String;
                    cmd.Parameters.Add(paramProviderPName);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }
    }
}
