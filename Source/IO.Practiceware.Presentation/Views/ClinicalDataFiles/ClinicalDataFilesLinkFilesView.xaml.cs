﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Services.Utilities;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Data;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using IO.Practiceware.Services.PatientSearch;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using ClinicalDocument;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using IO.Practiceware.Data;
using IO.Practiceware.Utilities;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    /// <summary>
    /// Interaction logic for ClinicalDataFilesLinkFiles.xaml
    /// </summary>
    public partial class ClinicalDataFilesLinkFilesView
    {
        public ClinicalDataFilesLinkFilesView()
        {
            InitializeComponent();
        }
    }

    public class ClinicalDataFilesLinkFilesViewContext : IViewContext
    {
        private readonly IClinicalDataFilesLinkFilesViewService _viewService;
        private readonly Func<PatientSearchViewManager> _createPatientSearchViewManager;
        private readonly Func<ImportAndLinkClinicalDataFileViewModel> _createImportClinicalDataFileViewModel;

        public ClinicalDataFilesLinkFilesViewContext(IClinicalDataFilesLinkFilesViewService viewService,
                                                     Func<PatientSearchViewManager> createPatientSearchViewManager,
                                                     Func<ImportAndLinkClinicalDataFileViewModel> createImportClinicalDataFileViewModel)
        {
            _viewService = viewService;
            _createPatientSearchViewManager = createPatientSearchViewManager;
            _createImportClinicalDataFileViewModel = createImportClinicalDataFileViewModel;
            LinkToPatient = Command.Create(ExecuteLinkToPatient, () => SelectedImportAndLinkClinicalDataFileViewModel != null);
            Load = Command.Create<bool>(x => ExecuteLoad()).Async(() => InteractionContext);
            SelectedCCDA = Command.Create<bool>(x => ExecuteCCDAViewer()).Async(() => InteractionContext);
            ImportFiles = Command.Create<Tuple<FileInfo[], object>>(ExecuteImportFiles);
            Save = Command.Create(ExecuteSave, CanExecuteSave).Async(() => InteractionContext);
            ImportAndLinkClinicalDataFileViewModels = new ExtendedObservableCollection<ImportAndLinkClinicalDataFileViewModel>();
            Cancel = Command.Create(ExecuteCancel);
        }

        private void ExecuteCCDAViewer()
        {
            if (SelectedImportAndLinkClinicalDataFileViewModel != null)
            {
                string TempFilePath = SelectedImportAndLinkClinicalDataFileViewModel.FullPath;
                //UtilitiesViewManager.OpenCCDAView(TempFilePath);
            }
        }
        private void ExecuteLoad()
        {
            //DisplayCCDAViewer = "Hidden";
            LoadTypeOfSummaryOfCare();
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select Top(1) Value From model.ApplicationSettings Where Name = 'DisplayCCDA'";
                DataTable dataTable = dbConnection.Execute<DataTable>(_selectQry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    var value = dataTable.Rows[0]["Value"].ToString();
                    DisplayCCDAViewer = value != null && value.ToString().ToUpper().Equals("TRUE") ? "Visible" : "Hidden";
                }
            }
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        public virtual ICommand ImportFiles { get; protected set; }

        public virtual ICommand LinkToPatient { get; protected set; }

        public virtual ICommand Save { get; protected set; }

        public virtual ICommand Cancel { get; protected set; }

        public ICommand Load { get; protected set; }

        public virtual ICommand SelectedCCDA { get; protected set; }

        public virtual ImportAndLinkClinicalDataFileViewModel SelectedImportAndLinkClinicalDataFileViewModel { get; set; }

        [DispatcherThread]
        public virtual string DisplayCCDAViewer { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ImportAndLinkClinicalDataFileViewModel> ImportAndLinkClinicalDataFileViewModels { get; set; }

        private bool CanExecuteSave()
        {
            return ImportAndLinkClinicalDataFileViewModels.Any(f => f.PatientId.HasValue);
        }

        private void ExecuteSave()
        {
            var listOfFilesToLinkAndSave = new List<ImportAndLinkClinicalDataFileViewModel>();
            var filePath = string.Empty;
            var patientId = string.Empty;
            using (new SuppressPropertyChangedScope())
            {
                // for all the rows that have had a patient set, make sure the xml content is loaded and create a pdf
                foreach (var fileViewModel in ImportAndLinkClinicalDataFileViewModels)
                {
                    if (!fileViewModel.PatientId.HasValue) continue;

                    // read file xml, create pdf from xml
                    try
                    {
                        // Not using file manager, since selected files are on user's computer
                        fileViewModel.XmlContent = FileSystem.TryReadAllText(fileViewModel.FullPath);
                        filePath = fileViewModel.FullPath;
                        patientId = fileViewModel.PatientId.ToString();
                        var htmlContent = CdaXmlToHtmlHelper.CdaXmlToHtmlString(fileViewModel.XmlContent);
                        fileViewModel.PdfDocument = WkHtmlToPdfWrapperService.GeneratePdf(new[] { htmlContent });
                        fileViewModel.ClinicalDataFileType = CdaXmlToHtmlHelper.DetermineClinicalDataFileType(fileViewModel.XmlContent);
                        listOfFilesToLinkAndSave.Add(fileViewModel);
                    }
                    catch (Exception ex)
                    {
                        var alertText = string.Format("There was an issue linking {0} to the patient '{1}'.  No files were linked at this time.  Please try again.", fileViewModel.FileName, fileViewModel.PatientSelectedFullName);
                        Trace.TraceError(string.Format("Application Message: {0}{1}Exception: {2}", alertText, Environment.NewLine, ex));
                        System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionManager.Current.Alert(alertText, "Error Linking Files"));
                        return;
                    }
                }

                _viewService.Save(listOfFilesToLinkAndSave);
                string DocumentId = "";
                SaveComponents(filePath, patientId, DocumentId);
                System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionManager.Current.Alert(string.Format("You have successfully linked {0} files", listOfFilesToLinkAndSave.Count), "Link Successful"));
            }

            // remove the saved items from the list
            listOfFilesToLinkAndSave.ForEach(savedItem => ImportAndLinkClinicalDataFileViewModels.Remove(savedItem));

        }

        private void SaveComponents(string fullPath, string patientId, string DocumentId)
        {
            ImportCCDAContext importCtx = new ImportCCDAContext();
            string Severity = string.Empty;
            FileStream fs = null;
            fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read);
            StreamReader d = new StreamReader(fs);
            d.BaseStream.Seek(0, SeekOrigin.Begin);
            TextWriter x = null;
            x = new StringWriter();

            string strxml = "";
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreComments = true;
            using (XmlReader reader = XmlReader.Create(fullPath, readerSettings))
            {
                XmlDocument myData = new XmlDocument();
                myData.Load(reader);

                StringBuilder sb = new StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = "  ",
                    NewLineChars = "\r\n",
                    NewLineHandling = NewLineHandling.Replace
                };
                using (XmlWriter writer = XmlWriter.Create(sb, settings))
                {
                    myData.Save(writer);
                }
                strxml = sb.ToString();
                string filter = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)""";
                strxml = Regex.Replace(strxml, filter, "");
                strxml = strxml.Replace("sdtc:", "");
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strxml);
                XmlNodeList xnList = doc.SelectNodes("//ClinicalDocument/component/structuredBody/component");
                XmlNodeList DataRowNodeList;
                int MedicationIncrement = 0;
                //practice reconciliation
                var providerName = string.Empty;
                var phoneNumber = string.Empty;
                var city = string.Empty;
                var practiceName = string.Empty;
                var senderFName = string.Empty;
                var senderLName = string.Empty;
                var state = string.Empty;
                var postalCode = string.Empty;
                var addressLine1 = string.Empty;
                var statusAllergy = string.Empty;
                var statusMedications = string.Empty;
                var statusProblem = string.Empty;

                foreach (XmlNode row in xnList)
                {
                    if (row.InnerText.ToUpper().StartsWith("ALLERGIES"))
                    {
                        XmlDocument sectionAllerigies = new XmlDocument();
                        sectionAllerigies.LoadXml(row.InnerXml);

                        XmlNodeList entryList = sectionAllerigies.SelectNodes("//section/entry");
                        foreach (XmlNode entrynode in entryList)
                        {
                            string displayName = "";

                            XmlDocument EntrySectionDoc = new XmlDocument();
                            EntrySectionDoc.LoadXml(entrynode.InnerXml);

                            XmlNode MainObservationNode = EntrySectionDoc.SelectSingleNode("//entryRelationship/observation");

                            XmlNodeList ObservationNodeList = MainObservationNode.SelectNodes("//entryRelationship/observation/entryRelationship");

                            foreach (XmlNode ObservationNodeListnodeEnt in ObservationNodeList)
                            {
                                XmlDocument ObservationNodeListnode = new XmlDocument();
                                ObservationNodeListnode.LoadXml(ObservationNodeListnodeEnt.InnerXml);

                                XmlNode ObservationNode = ObservationNodeListnode.SelectSingleNode("//observation");
                                XmlNode ObservationTemplateNode = null;
                                if (ObservationNode!=null)
                                {

                                    ObservationTemplateNode = ObservationNode.SelectSingleNode("//templateId");
                                    if (ObservationTemplateNode != null)
                                    {
                                        if (ObservationTemplateNode.Attributes.Count > 0)
                                        {
                                            if (ObservationTemplateNode.Attributes[0].InnerText != "2.16.840.1.113883.10.20.22.4.9")
                                            {
                                                continue;
                                            }
                                        }
                                    } 
                                }
                                XmlNode OnSetdateNode = MainObservationNode.SelectSingleNode("//effectiveTime/low");

                                XmlNode ParticipantNode = MainObservationNode.SelectSingleNode("//participant");
                                XmlNode ParticipantCodeNode = ParticipantNode.SelectSingleNode("//participantRole/playingEntity/code");

                                XmlNode StatusCodeNode = EntrySectionDoc.SelectSingleNode("//statusCode");
                                statusAllergy = StatusCodeNode.Attributes["code"].InnerText;

                                if (ParticipantCodeNode.Attributes["displayName"] != null)
                                    displayName = ParticipantCodeNode.Attributes["displayName"].Value;
                                else
                                    displayName = ParticipantNode.SelectSingleNode("//participantRole/playingEntity/code/translation").Attributes["displayName"].Value;
                                string Rxnormid = string.Empty;
                                if (ParticipantCodeNode.Attributes["displayName"] != null)
                                     Rxnormid = ParticipantCodeNode.Attributes["code"].Value;
                                else
                                    Rxnormid=ParticipantNode.SelectSingleNode("//participantRole/playingEntity/code/translation").Attributes["code"].Value;

                                XmlNode ReactionObservationValueNode = null;
                                if(null!= ObservationTemplateNode)
                                ReactionObservationValueNode = ObservationTemplateNode.SelectSingleNode("//value");
                                string Reaction = string.Empty;
                                if (ParticipantCodeNode.Attributes["displayName"] != null)
                                    Reaction = ReactionObservationValueNode.Attributes["displayName"].InnerText;

                                try
                                {
                                    //XmlNodeList reactionlist = EntrySectionDoc.SelectNodes("//entryRelationship/observation/entryRelationship/observation");
                                    XmlNode SevirityObservationNode = EntrySectionDoc.SelectSingleNode("//entryRelationship/observation/entryRelationship/observation/entryRelationship/observation");
                                    
                                        XmlNode SevirityObservationValueNode = SevirityObservationNode.SelectSingleNode("//entryRelationship/observation/entryRelationship/observation/entryRelationship/observation/value");
                                        Severity = SevirityObservationValueNode.Attributes["displayName"].InnerText;
                                    //}
                                }
                                catch (Exception)
                                {
                                    Severity = "Moderate";
                                }

                                StructAllergy allergy = new StructAllergy();
                                allergy.Name = String.Empty;
                                allergy.Reaction = String.Empty;
                                allergy.ReactionDate = String.Empty;
                                allergy.XML = String.Empty;
                                allergy.Severity = string.Empty;
                                allergy.Status = string.Empty;
                                string reactionDate = string.Empty;

                                if (OnSetdateNode.Attributes["value"] == null || OnSetdateNode.Attributes["value"].InnerText == "NI" || OnSetdateNode.Attributes["value"].InnerText == "NA")
                                {
                                    reactionDate = DateTime.Today.ToShortDateString();
                                }
                                else
                                {
                                    var date1 = DateTime.ParseExact(OnSetdateNode.Attributes["value"].InnerText, "yyyyMMdd", null);
                                    reactionDate = date1.ToShortDateString();
                                }

                                allergy.Name = displayName;
                                allergy.RxNorm = Rxnormid;
                                allergy.Reaction = Reaction;
                                allergy.ReactionDate = reactionDate;
                                allergy.Severity = Severity;
                                allergy.Status = statusAllergy;
                                allergy.SaveIntoDB(patientId);
                            }
                        }
                    }
                    if (row.InnerText.ToUpper().StartsWith("MEDICATIONS"))
                    {
                        XmlDocument sectionMedications = new XmlDocument();
                        sectionMedications.LoadXml(row.InnerXml);
                        string Rxnormid = string.Empty;
                        XmlNodeList entryList = sectionMedications.SelectNodes("//section/entry");
                        foreach (XmlNode entrynode in entryList)
                        {

                            string displayName = "";

                            XmlDocument EntrySectionDoc = new XmlDocument();
                            EntrySectionDoc.LoadXml(entrynode.InnerXml);

                            XmlNode SubstanceNode = EntrySectionDoc.SelectSingleNode("//substanceAdministration");
                            XmlNode StartdateNode = SubstanceNode.SelectSingleNode("//effectiveTime/low");

                            XmlNode ConsumableNode = SubstanceNode.SelectSingleNode("//consumable");
                            XmlNode ManufacturedMaterialCodeNode = ConsumableNode.SelectSingleNode("//manufacturedProduct/manufacturedMaterial/code");

                            if (null != ManufacturedMaterialCodeNode.Attributes["displayName"])
                                displayName = ManufacturedMaterialCodeNode.Attributes["displayName"].Value;

                            if (null != ManufacturedMaterialCodeNode.Attributes["code"])
                                Rxnormid = ManufacturedMaterialCodeNode.Attributes["code"].Value;

                            DataRowNodeList = sectionMedications.SelectNodes("//section/text/table/tbody/tr");
                            XmlNode StatusCodeNode = null;
                            if(EntrySectionDoc!=null)
                            StatusCodeNode = EntrySectionDoc.SelectSingleNode("//statusCode");
                            if(null!= StatusCodeNode.Attributes["code"])
                            statusMedications = StatusCodeNode.Attributes["code"].InnerText;

                            string Directions = "";
                            int loopIterationNo = 0;
                            foreach (XmlNode act in DataRowNodeList)
                            {
                                if (loopIterationNo == MedicationIncrement)
                                {
                                    Directions = act.ChildNodes[1].InnerText;
                                }
                                loopIterationNo = loopIterationNo + 1;
                            }


                            StructMedication medication = new StructMedication();
                            medication.DrugName = String.Empty;
                            medication.Instructions = String.Empty;
                            medication.LengthOfUse = String.Empty;
                            medication.Quantity = String.Empty;
                            medication.Refills = String.Empty;
                            medication.XML = String.Empty;
                            medication.RxDate = string.Empty;
                            string rxDate = string.Empty;

                            medication.DrugName = displayName;
                            medication.RxNorm = Rxnormid;

                            if (StartdateNode.Attributes["value"] == null || StartdateNode.Attributes["value"].InnerText == "NI" || StartdateNode.Attributes["value"].InnerText == "NA")
                            {
                                rxDate = DateTime.Today.ToShortDateString();
                            }
                            else
                            {
                                var date1 = DateTime.ParseExact(StartdateNode.Attributes["value"].InnerText, "yyyyMMdd", null);
                                rxDate = date1.ToShortDateString();
                            }
                            medication.RxDate = rxDate;
                            medication.Instructions = Directions;
                            medication.RxNorm = Rxnormid;
                            medication.Status = statusMedications;
                            medication.SaveIntoDB(patientId);

                            MedicationIncrement = MedicationIncrement + 1;
                        }
                    }
                    else if (row.InnerText.ToUpper().StartsWith("PROBLEMS"))
                    {
                        XmlDocument sectionProblemList = new XmlDocument();
                        sectionProblemList.LoadXml(row.InnerXml);

                        XmlNodeList entryList = sectionProblemList.SelectNodes("//section/entry");
                        foreach (XmlNode entrynode in entryList)
                        {
                            string displayName = "";

                            XmlDocument EntrySectionDoc = new XmlDocument();
                            EntrySectionDoc.LoadXml(entrynode.InnerXml);

                            XmlNode StatusCodeNode = EntrySectionDoc.SelectSingleNode("//statusCode");
                            if(null!= StatusCodeNode.Attributes["code"])
                            statusProblem = StatusCodeNode.Attributes["code"].InnerText;

                            XmlNode ObservationNode = EntrySectionDoc.SelectSingleNode("//entryRelationship/observation");
                            XmlNode OnSetdateNode = ObservationNode.SelectSingleNode("//effectiveTime/low");

                            XmlNode valueNode = ObservationNode.SelectSingleNode("//value");
                            displayName = valueNode.Attributes["displayName"].Value;

                            //string Rxnormid = valueNode.Attributes["code"].Value;

                            string Rxnormid = null != valueNode.Attributes["code"] ? valueNode.Attributes["code"].Value : string.Empty;
                      
                            StructProblem problem = new StructProblem();
                            problem.Code = String.Empty;
                            problem.Description = String.Empty;
                            problem.Eye = String.Empty;
                            problem.OnsetDate = String.Empty;
                            problem.Status = String.Empty;
                            problem.XML = String.Empty;
                            string OnSetDate = string.Empty;

                            if (OnSetdateNode.Attributes["value"] == null || OnSetdateNode.Attributes["value"].InnerText == "NI" || OnSetdateNode.Attributes["value"].InnerText == "NA")
                            {
                                OnSetDate = DateTime.Today.ToShortDateString();
                            }
                            else
                            {
                                var date1 = DateTime.ParseExact(OnSetdateNode.Attributes["value"].InnerText, "yyyyMMdd", null);
                                OnSetDate = date1.ToShortDateString();
                            }

                            problem.Description = displayName;
                            //Check for the laterality.
                            if (problem.Description.ToUpper().Contains("RIGHT")) problem.Eye = "OD";
                            else if (problem.Description.ToUpper().Contains("LEFT")) problem.Eye = "OS";
                            else if (problem.Description.ToUpper().Contains("BILATERAL"))
                                problem.Eye = "OU";
                            else problem.Eye = "NA";
                            problem.OnsetDate = OnSetDate;
                            problem.Status = statusProblem;
                            problem.SnomedCode = Rxnormid;
                            problem.SaveIntoDB(patientId);
                        }
                    }
                }

                string providerFirstName = "";
                string providerLastName = "";
                
                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/city"))
                    city = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/city").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/streetAddressLine"))
                    addressLine1 = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/streetAddressLine").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/state"))
                    state = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/state").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/postalCode"))
                    postalCode = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/addr/postalCode").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/assignedPerson/name/given"))
                    providerFirstName = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/assignedPerson/name/given").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/assignedPerson/name/family"))
                    providerLastName = doc.SelectSingleNode("//ClinicalDocument/informant/assignedEntity/assignedPerson/name/family").InnerText;

                if (null != doc.SelectSingleNode("//ClinicalDocument/informationRecipient/intendedRecipient/receivedOrganization/name"))
                    practiceName = doc.SelectSingleNode("//ClinicalDocument/informationRecipient/intendedRecipient/receivedOrganization/name").InnerText;


                providerName = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization").InnerText;
                XmlNodeList phone = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/providerOrganization/telecom");

                foreach (XmlElement node in phone)
                {
                    phoneNumber = node.GetAttribute("value").Replace("tel","");
                }
                importCtx.InsertReconcilePractice(int.Parse(patientId), providerName, phoneNumber, addressLine1, city, state, postalCode, DocumentId, SelectedTypeofSummaryofCare, providerFirstName, providerLastName, practiceName);
            }
        }
        private void ExecuteLinkToPatient()
        {
            // Show PatientSearchView to select a patient and set the selected patient id back on the SelectedImportAndLinkClinicalDataFileViewModel.PatientId
            string FirstName = string.Empty;
            string LastName = string.Empty;
            string BirthDate = string.Empty;
            string LanguageCode = string.Empty;
            string raceCode = string.Empty;
            string EthnicityCode = string.Empty;
            string GenderCode = string.Empty;
            string AddressLine1 = string.Empty;
            string AddressLine2 = string.Empty;
            string City = string.Empty;
            string State = string.Empty;
            string ZipCode = string.Empty;
            string ProviderName = string.Empty;
            bool BadCCDA = false;

            if (null != ImportAndLinkClinicalDataFileViewModels)
            {
                XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(ImportAndLinkClinicalDataFileViewModels.);
                ImportAndLinkClinicalDataFileViewModels.ForEach<ImportAndLinkClinicalDataFileViewModel>(item =>
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(ClinicalDocument.ClinicalDocument));

                    #region CCDA_XML

                    FileStream fs = null;
                    fs = new FileStream(item.FullPath, FileMode.Open, FileAccess.Read);
                    StreamReader d = new StreamReader(fs);
                    d.BaseStream.Seek(0, SeekOrigin.Begin);
                    TextWriter x = null;
                    x = new StringWriter();

                    string strxml = "";
                    XmlReaderSettings readerSettings = new XmlReaderSettings();
                    readerSettings.IgnoreComments = true;
                    using (XmlReader reader = XmlReader.Create(item.FullPath, readerSettings))
                    {
                        XmlDocument myData = new XmlDocument();
                        myData.Load(reader);

                        StringBuilder sb = new StringBuilder();
                        XmlWriterSettings settings = new XmlWriterSettings
                        {
                            Indent = true,
                            IndentChars = "  ",
                            NewLineChars = "\r\n",
                            NewLineHandling = NewLineHandling.Replace
                        };
                        using (XmlWriter writer = XmlWriter.Create(sb, settings))
                        {
                            myData.Save(writer);
                        }
                        strxml = sb.ToString();
                        string filter = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)""";
                        strxml = Regex.Replace(strxml, filter, "");
                        strxml = strxml.Replace("sdtc:", "");
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(strxml);

                        if (doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/patient/name/given") != null)
                        {
                            FirstName = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/patient/name/given").InnerText;
                            BadCCDA = false;
                        }
                        else
                            BadCCDA = true;

                        if (doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/patient/name/family") != null)
                        {
                            LastName = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/patient/name/family").InnerText;
                            BadCCDA = false;
                        }
                        else
                            BadCCDA = true;

                        XmlNodeList gender = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/administrativeGenderCode");
                        foreach (XmlElement node in gender)
                        {
                            GenderCode = node.GetAttribute("code");
                        }

                        XmlNodeList lang = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/languageCommunication/languageCode");

                        foreach (XmlElement node in lang)
                        {
                            LanguageCode = node.GetAttribute("code");
                        }

                        XmlNodeList race = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/raceCode");

                        foreach (XmlElement node in race)
                        {
                            raceCode = node.GetAttribute("code");
                        }

                        XmlNodeList ethnicity = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/ethnicGroupCode");
                        foreach (XmlElement node in ethnicity)
                        {
                            EthnicityCode = node.GetAttribute("code");
                        }

                        if (null != doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/addr/streetAddressLine"))
                            AddressLine1 = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/addr/streetAddressLine").InnerText;
                        else if (null != doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization/addr/streetAddressLine"))
                        {
                            AddressLine1 = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization/addr/streetAddressLine").InnerText;


                            City = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization/addr/city").InnerText;
                            State = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization/addr/state").InnerText;
                            ZipCode = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization/addr/postalCode").InnerText;
                        }
                        ProviderName = doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization").InnerText;

                        if (null != doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/birthTime"))
                        {
                            XmlNodeList birthDate = doc.SelectNodes("//ClinicalDocument/recordTarget/patientRole/patient/birthTime");
                            foreach (XmlElement node in birthDate)
                            {
                                var offset = node.GetAttribute("value");
                                if (offset.Length >= 8)
                                {
                                    var year = offset.Substring(0, 4);
                                    var month = offset.Substring(4, 2);
                                    var days = offset.Substring(6, 2);
                                    if (year != "" && month != "" && days != "")
                                    {
                                        BirthDate = year + "-" + month + "-" + days;
                                        BadCCDA = false;
                                    }
                                }
                            }

                        }
                        else
                            BadCCDA = true;
                        #endregion
                        if (BadCCDA)
                        {
                            System.Windows.MessageBox.Show("Bad CCDA");
                            return;
                        }


                        ImportCCDAContext importCtx = new ImportCCDAContext();
                        DataTable dataTable = importCtx.SearchPatient(FirstName, LastName, BirthDate);
                        if (dataTable.Rows.Count > 1)
                        {
                            PatientSearchLoadArguments loadArguments = new PatientSearchLoadArguments
                            {
                                CanCreatePatient = false,
                                RunSearchOnLoad = true,
                                SearchText = FirstName + " " + LastName

                            };
                            var returnArgs = _createPatientSearchViewManager().ShowPatientSearchView(loadArguments) as PatientSearchPatientSelectedReturnArguments;

                            if (returnArgs != null)
                            {
                                SelectedImportAndLinkClinicalDataFileViewModel.PatientId = returnArgs.PatientId;
                                SelectedImportAndLinkClinicalDataFileViewModel.PatientSelectedFullName = returnArgs.PatientDisplayName.IsNullOrEmpty() ? returnArgs.PatientDisplayName : string.Format("{0} {1}", returnArgs.PatientFirstName, returnArgs.PatientLastName);
                            }
                        }
                        else if (dataTable.Rows.Count == 1)
                        {
                            SelectedImportAndLinkClinicalDataFileViewModel.PatientId = int.Parse(dataTable.Rows[0]["Id"].ToString());
                            var fullName = dataTable.Rows[0]["FirstName"].ToString() + " " + dataTable.Rows[0]["LastName"].ToString();
                            SelectedImportAndLinkClinicalDataFileViewModel.PatientSelectedFullName = fullName;
                        }
                        else if (dataTable.Rows.Count == 0)
                        {
                            int patientId = importCtx.CreateNewPatient(FirstName, LastName, BirthDate, LanguageCode, raceCode, EthnicityCode, GenderCode, AddressLine1, AddressLine2, City, State, ZipCode, ProviderName);
                            var fullName = FirstName + " " + LastName;
                            SelectedImportAndLinkClinicalDataFileViewModel.PatientId = patientId;
                            SelectedImportAndLinkClinicalDataFileViewModel.PatientSelectedFullName = fullName;
                        }
                    }
                }
             );
            }
        }

        private void ExecuteImportFiles(Tuple<FileInfo[], object> args)
        {
            var files = args.Item1;
            // Take selected files and create and add an ImportAndLinkClinicalDataFileViewModel for each

            if (files.IsNullOrEmpty()) return;

            // ReSharper disable AssignNullToNotNullAttribute
            ImportAndLinkClinicalDataFileViewModels = files
                                                            // ReSharper restore AssignNullToNotNullAttribute                
                                                            .Select(f =>
                                                              {
                                                                  var model = _createImportClinicalDataFileViewModel();
                                                                  model.FileName = f.Name; // name includes extension
                                                                  model.FullPath = f.FullName;
                                                                  model.LastModifiedDateUtc = f.LastWriteTimeUtc;

                                                                  return model;
                                                              })
                                                              .Where(f => ImportAndLinkClinicalDataFileViewModels.All(i => i.FullPath != f.FullPath))
                                                              .ToExtendedObservableCollection().AddRangeWithSinglePropertyChangedNotification(ImportAndLinkClinicalDataFileViewModels);

        }

        public int PatientId { get; set; }

        public virtual IInteractionContext InteractionContext { get; set; }

        private void LoadTypeOfSummaryOfCare()
        {
            TypeOfSummaryOfCare = new ObservableCollection<string>();
            TypeOfSummaryOfCare.Add("New Patient");
            TypeOfSummaryOfCare.Add("Transition of Care");
            TypeOfSummaryOfCare.Add("Referral");
        }
        public virtual ObservableCollection<string> TypeOfSummaryOfCare { get; set; }
        public virtual string SelectedTypeofSummaryofCare  { get; set; }

    }
}
