﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Storage;
using Soaf.Presentation;
using Soaf.Collections;
using IO.Practiceware.Presentation.Common;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    /// <summary>
    /// Interaction logic for ClinicalDataFilesEditAndTransmitView.xaml
    /// </summary>
    public partial class ClinicalDataFilesEditAndTransmitView
    {
        public ClinicalDataFilesEditAndTransmitView()
        {
            InitializeComponent();
        }
    }

    public class ClinicalDataFilesEditAndTransmitViewContext : IViewContext
    {
        private readonly IClinicalDataFilesEditAndTransmitViewService _clinicalDataFilesEditAndTransmitViewService;
        private readonly DocumentViewManager _documentViewManager;

        public ClinicalDataFilesEditAndTransmitViewContext(CdaEditorViewContext cdaEditorViewContext,
            IClinicalDataFilesEditAndTransmitViewService clinicalDataFilesEditAndTransmitViewService,
            DocumentViewManager documentViewManager)
        {
            CdaEditorViewContext = cdaEditorViewContext;
            _clinicalDataFilesEditAndTransmitViewService = clinicalDataFilesEditAndTransmitViewService;
            _documentViewManager = documentViewManager;
            SearchResults = new ObservableCollection<SelectedAppointmentViewModel>();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            ViewTransitionOfCare = Command.Create(ExecuteViewTransitionOfCare, () => SelectedAppointment != null).Async(() => InteractionContext);
            ViewEditVisitSummary = Command.Create(ExecuteViewEditVisitSummary, () => SelectedAppointment != null).Async(() => InteractionContext);
            SaveAs = Command.Create(ExecuteSaveAs, () => SelectedAppointment != null && !IsInEditMode && CdaViewModel != null && !string.IsNullOrWhiteSpace(CdaViewModel.CdaXml)).Async(() => InteractionContext);
            Print = Command.Create(ExecutePrint, () => SelectedAppointment != null && !IsInEditMode && CdaViewModel != null && !string.IsNullOrWhiteSpace(CdaXmlToHtmlHelper.CdaXmlToHtmlString(CdaViewModel.CdaXml))).Async(() => InteractionContext);
            Transmit = Command.Create(ExecuteTransmit, () => SelectedAppointment != null && !IsInEditMode && CdaViewModel != null && !String.IsNullOrWhiteSpace(CdaViewModel.CdaXml)).Async(() => InteractionContext);
            Edit = Command.Create(ExecuteEditCda, () => SelectedAppointment != null && !IsInEditMode && CdaViewModel != null && CdaViewModel.IsClinicalSummary && !string.IsNullOrWhiteSpace(CdaViewModel.CdaXml));
            Save = Command.Create(ExecuteSaveCda, () => IsInEditMode && SelectedAppointment != null);
            CancelEdit = Command.Create(ExecuteCancelCdaEdit, () => IsInEditMode && SelectedAppointment != null);
        }

        [DispatcherThread]
        public virtual CdaViewModel CdaViewModel { set; get; }

        public CdaEditorViewContext CdaEditorViewContext { set; get; }

        public ClinicalDataFilesEditAndTransmitLoadArguments LoadArguments { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<SelectedAppointmentViewModel> SearchResults { get; set; }

        [DispatcherThread]
        public virtual SelectedAppointmentViewModel SelectedAppointment { get; set; }

        [DispatcherThread]
        public virtual bool IsInEditMode { get; set; }

        private readonly List<string> _listOfPathsToDelete = new List<string>();

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            // Subscribe to completing event to act on window closing
            InteractionContext.Completing += OnInteractionContextCompleting;

            SearchResults = LoadArguments.ListOfAppointmentId != null ? _clinicalDataFilesEditAndTransmitViewService.GetAppointments(LoadArguments.ListOfAppointmentId.ToList()).ToExtendedObservableCollection() : _clinicalDataFilesEditAndTransmitViewService.GetAppointments().ToExtendedObservableCollection();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteViewTransitionOfCare()
        {
            CdaViewModel = _clinicalDataFilesEditAndTransmitViewService.GetPatientTransitionOfCareCda(SelectedAppointment);
        }

        private void ExecuteViewEditVisitSummary()
        {
            CdaViewModel = _clinicalDataFilesEditAndTransmitViewService.GetEncounterClinicalSummaryCda(SelectedAppointment);
        }

        private void ExecuteSaveAs()
        {
            bool isSaved = false;
            this.Dispatcher().Invoke(() => isSaved = DoActualSave());
            if (isSaved) _clinicalDataFilesEditAndTransmitViewService.RecordSavingOfClinicalDataFile(CdaViewModel);
        }

        private bool DoActualSave()
        {
            const string extension = "xml";
            var dialog = new SaveFileDialog
                         {
                             DefaultExt = extension,
                             Filter =
                                 String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Xml"),
                             FilterIndex = 1
                         };
            if (dialog.ShowDialog() != DialogResult.OK) return false;
            var xdoc = new XmlDocument();
            xdoc.LoadXml(CdaViewModel.CdaXml);
            xdoc.Save(dialog.FileName);
            return true;
        }

        private void ExecutePrint()
        {
            string path = FileManager.Instance.GetTempPathName();
            _listOfPathsToDelete.Add(path);
            path = path.Replace(".tmp", ".htm");
            var html = CdaXmlToHtmlHelper.CdaXmlToHtmlString(CdaViewModel.CdaXml);
            FileManager.Instance.CommitContents(path, html);
            _listOfPathsToDelete.Add(path);
            try
            {
                this.Dispatcher().Invoke(() => DoActualPrint(path));
                _clinicalDataFilesEditAndTransmitViewService.RecordPrintingOfClinicalDataFile(CdaViewModel);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
                // swallow any exceptions
            }
        }

        private void DoActualPrint(string htmlPath)
        {
            _documentViewManager.HeaderTitleOfCurrentWindow = "Clinical Data Files";
            _documentViewManager.PrintDocument("C-CDA", htmlPath);
        }

        private void ExecuteTransmit()
        {
            _clinicalDataFilesEditAndTransmitViewService.TransmitClinicalDataFile(CdaViewModel);
            string retStatus = _clinicalDataFilesEditAndTransmitViewService.PostHealthSummary2Portal(CdaViewModel);
            if (retStatus.Equals("Success"))
            {
                InteractionManager.Current.Alert("Transmission of the file was successful.");
            }
            else if (retStatus.Equals("InvalidPatient"))
            {
                InteractionManager.Current.Alert("This Patient seems like not registered with Patient Portal, Please register this Patient first and then try to send the PHI !");
            }
            else 
            {
                InteractionManager.Current.Alert("There is some issue while sending PHI to Portal, Please try after some time or contact IO Practiceware support");
            }
        }

        private void ExecuteEditCda()
        {
            IsInEditMode = true;
            CdaEditorViewContext.ViewModel = new CdaEditorViewModel { Id = CdaViewModel.Id, OriginalXml = CdaViewModel.CdaXml };
        }

        private void ExecuteSaveCda()
        {
            IsInEditMode = false;
            CdaViewModel = new CdaViewModel { Id = CdaEditorViewContext.ViewModel.Id, CdaXml = CdaEditorViewContext.ViewModel.CdaXmlDocument.OuterXml, IsClinicalSummary = true };
        }

        private void ExecuteCancelCdaEdit()
        {
            IsInEditMode = false;
        }

        void OnInteractionContextCompleting(object sender, CancelEventArgs e)
        {
            _listOfPathsToDelete.ForEach(FileManager.Instance.Delete);
        }

        #region Commands

        /// <summary>
        /// Command to load the UI
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to close the UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to Save C-CDA files
        /// </summary>
        public ICommand SaveAs { get; protected set; }

        /// <summary>
        /// Command to Print C-CDA files
        /// </summary>
        public ICommand Print { get; protected set; }

        /// <summary>
        /// Command to Transmit C-CDA files
        /// </summary>
        public ICommand Transmit { get; protected set; }

        /// <summary>
        /// Command to View Transition of Care Document
        /// </summary>
        public ICommand ViewTransitionOfCare { get; protected set; }

        /// <summary>
        /// Command to View/Edit Visit Summary
        /// </summary>
        public ICommand ViewEditVisitSummary { get; protected set; }

        /// <summary>
        /// Command to Edit Transition of Care Document/Visit Summary XML
        /// </summary>
        public ICommand Edit { get; protected set; }

        /// <summary>
        /// Command to Save the changes Transition of Care Document/Visit Summary XML and reload CdaViewer
        /// </summary>
        public ICommand Save { get; protected set; }

        /// <summary>
        /// Command to Cancel the changes to Transition of Care Document/Visit Summary XML and reload CdaViewer
        /// </summary>
        public ICommand CancelEdit { get; protected set; }

        #endregion
    }
}