﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    public struct StructMedication
    {
        public string DrugName { get; set; }
        public string Instructions { get; set; }
        public string MyProperty { get; set; }
        public string LengthOfUse { get; set; }
        public string Quantity { get; set; }
        public string Refills { get; set; }
        public string XML { get; set; }
        public string RxDate { get; set; }
        public string RxNorm { get; set; }
        public string Status { get; set; }

        public void SaveIntoDB(string PatientId)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileMedications(PatientId, StartDate, Frequency, DrugName, Status, Instructions, LengthOfUse, Quantity, Refills, RxDate, RxNorm, MedicationStatus) Values ('" + PatientId + "','" + RxDate + "', '', '" + DrugName + "', 0, '" + Instructions + "','" + LengthOfUse + "','" + Quantity + "','" + Refills + "','" + RxDate + "','" + RxNorm + "','" + Status + "')";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }
    }
}
