﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using System.Xml;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    /// <summary>
    /// Interaction logic for CdaEditorView.xaml
    /// </summary>
    public partial class CdaEditorView
    {
        public CdaEditorView()
        {
            InitializeComponent();
        }
    }

    public class CheckBoxXmlNodeBehavior : Behavior<CheckBox>
    {
        private PropertyChangeNotifier _isCheckedNotifier;

        private string _preUncheckInnerXml;

        private readonly Dictionary<string, string> _checkableNodes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                                                                      {
                                                                          {"PROCEDURES", CdaEditorViewResources.ProceduresNullFlavor},
                                                                          {"IMMUNIZATIONS", CdaEditorViewResources.ImmunizationsNullFlavor},
                                                                          {"PLAN OF CARE", CdaEditorViewResources.PlanOfCareNullFlavor},
                                                                          {"SOCIAL HISTORY", CdaEditorViewResources.SocialHistoryNullFlavor},
                                                                          {"VITAL SIGNS", CdaEditorViewResources.VitalSignsNullFlavor},
                                                                          {"INSTRUCTIONS", null},
                                                                          {"MEDICATIONS ADMINISTERED", CdaEditorViewResources.MedicationsAdministeredNullFlavor},
                                                                          {"REASON FOR VISIT", null},
                                                                          {"PROBLEMS", CdaEditorViewResources.ProblemsNullFlavor},
                                                                          {"MEDICATIONS", CdaEditorViewResources.MedicationsNullFlavor},
                                                                          {"ALLERGIES", CdaEditorViewResources.AllergiesNullFlavor},
                                                                          {"RESULTS", CdaEditorViewResources.ResultsNullFlavor},
                                                                      };

        protected override void OnAttached()
        {
            base.OnAttached();

            if (!ShouldAllowEdit(AssociatedObject))
            {
                AssociatedObject.IsEnabled = false;
            }

            _isCheckedNotifier = new PropertyChangeNotifier(AssociatedObject, ToggleButton.IsCheckedProperty).AddValueChanged(OnIsCheckedChanged);
        }

        protected override void OnDetaching()
        {
            _isCheckedNotifier.RemoveValueChanged(OnIsCheckedChanged);
            base.OnDetaching();
        }

        private void OnIsCheckedChanged(object sender, EventArgs e)
        {
            var element = AssociatedObject.DataContext as XmlElement;

            if (element == null) return;

            var title = GetSectionTitle(element).EnsureNotDefault("Node {0} was not expected.".FormatWith(element.OuterXml));

            if (AssociatedObject.IsChecked == false)
            {
                string nullFlavor;
                if (_checkableNodes.TryGetValue(title, out nullFlavor) && nullFlavor != null)
                {
                    _preUncheckInnerXml = element.InnerXml;
                    element.InnerXml = nullFlavor;
                }
                else if (element.ParentNode != null)
                {
                    element.ParentNode.RemoveChild(element);
                }
            }
            else
            {
                element.InnerXml = _preUncheckInnerXml;
            }
        }

        private bool ShouldAllowEdit(CheckBox checkBox)
        {
            var element = checkBox.DataContext as XmlElement;

            if (element != null)
            {
                var title = GetSectionTitle(element);
                if (title != null && _checkableNodes.ContainsKey(title)) return true;
            }
            return false;
        }

        private string GetSectionTitle(XmlElement element)
        {
            var sectionNode = element.SelectSingleNode("*[local-name()='section']");
            if (element.Name == "component" && sectionNode != null)
            {
                var titleNode = sectionNode.SelectSingleNode("*[local-name()='title']");

                if (titleNode != null && titleNode.Name == "title") return titleNode.InnerText;
            }
            return null;
        }
    }

    public class CdaEditorViewContext : IViewContext
    {
        [DispatcherThread]
        public virtual CdaEditorViewModel ViewModel { get; set; }

        public IInteractionContext InteractionContext { get; set; }
    }
}
