﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Views.ClinicalDataFiles
{
    public struct StructProblem
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Eye { get; set; }
        public string OnsetDate { get; set; }
        public string Status { get; set; }
        public string XML { get; set; }
        public string SnomedCode { get; set; }
        
        public void SaveIntoDB(string PatientId)
        {
            string _insertQry = "";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                _insertQry = "Insert into model.PatientReconcileProblems(PatientId, EffectiveDate, SnomedCT, DiagnosisCode, DiagnosisDescription, Status, Eye, OnSetDate,ProblemStatus) Values ('" + PatientId + "','" + OnsetDate + "', '" + SnomedCode + "', '" + Code + "', '" + ReplaceSingleQuote(Description) + "', 0,'" + Eye + "','" + OnsetDate + "','" + Status + "'); ";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                }
                dbConnection.Close();
            }
        }

        private string ReplaceSingleQuote(string field)
        {
            string result = string.Empty;
            result = !string.IsNullOrEmpty(field) ? field.Contains("'") ? field.Replace("'", "''") : string.Empty : string.Empty;

            return result;
        }
    }
}
