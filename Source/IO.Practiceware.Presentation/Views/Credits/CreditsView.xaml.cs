﻿using System;
using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.Credits;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Credits
{
    /// <summary>
    /// Interaction logic for CreditsView.xaml
    /// </summary>
    public partial class CreditsView
    {
        public CreditsView()
        {
            InitializeComponent();
        }
    }

    public class CreditsViewContext : IViewContext
    {


        public virtual ExternalSourcesCreditViewModel Credit { get; set; }

        public virtual ObservableCollection<ExternalSourceNameAndDescriptionViewModel> Sources { get; set; }



        public CreditsViewContext()
        {

            //var sources = new ObservableCollection<ExternalSourceNameAndDescriptionViewModel>();
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel { Name = "National Center for Immunization and Respiratory Diseases, CVX Code Set, Vaccines Administered, 2013_05_01" });
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel { Name = "International Classification of Diseases, 10th Edition, Clinical Modification, 2014" });
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel { Name = "International Classification of Diseases, Ninth Revision, Clinical Modification, 2013_2012_08_06" });
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel
            //{
            //    Name = "Logical Observation Identifier Names and Codes® (LOINC®) 2.46, 2013",
            //    Text = @"This product includes all or a portion of the LOINC® table, LOINC panels and forms file, LOINC document ontology file, and/or LOINC hierarchies file, or is derived from one or more of the foregoing, subject to a license from Regenstrief Institute, Inc. Your use of the LOINC table, LOINC codes, LOINC panels and forms file, LOINC document ontology file, and LOINC hierarchies file also is subject to this license, a copy of which is available at http://loinc.org/terms-of-use. The current complete LOINC table, LOINC Users' Guide, LOINC panels and forms file, LOINC document ontology file, and LOINC hierarchies file are available for download at http://loinc.org. The LOINC table and LOINC codes are copyright © 1995-2013, Regenstrief Institute, Inc. and the Logical Observation Identifiers Names and Codes (LOINC) Committee. The LOINC panels and forms file, LOINC document ontology file, and LOINC hierarchies file are copyright © 1995-2013, Regenstrief Institute, Inc. All rights reserved. THE LOINC TABLE (IN ALL FORMATS), LOINC PANELS AND FORMS FILE, LOINC DOCUMENT ONTOLOGY FILE, AND LOINC HIERARCHIES ARE PROVIDED 'AS IS'. ANY EXPRESS OR IMPLIED WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. LOINC® is a registered United States trademark of Regenstrief Institute, Inc. A small portion of the LOINC table may include content (e.g., survey instruments) that is subject to copyrights owned by third parties. Such content has been mapped to LOINC terms under applicable copyright and terms of use. Notice of such third party copyright and license terms would need to be included if such content is included."
            //});
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel { Name = "NCI Thesaurus, 2013_03D" });
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel { Name = "RxNorm Vocabulary, 13AB_140303F" });
            //sources.Add(new ExternalSourceNameAndDescriptionViewModel
            //{
            //    Name = "SNOMED Clinical Terms® (SNOMED CT®), 2014_03_01",
            //    Text = @"This material includes SNOMED Clinical Terms® (SNOMED CT®) which is used by permission of the International Health Terminology Standards Development Organisation (IHTSDO). All rights reserved. SNOMED CT®, was originally created by The College of American Pathologists. “SNOMED” and “SNOMED CT” are registered trademarks of the IHTSDO"
            //});

            //Credit = new ExternalSourcesCreditViewModel
            //         {
            //             Version = "2014AA",
            //             Text = @"Some material in the UMLS Metathesaurus is from copyrighted sources of the respective copyright holders. Users of the UMLS Metathesaurus are solely responsible for compliance with any copyright, patent or trademark restrictions and are referred to the copyright, patent or trademark notices appearing in the original sources, all of which are hereby incorporated by reference.",
            //             Sources = sources
            //         };
        }

        public IInteractionContext InteractionContext { get; set; }
    }

}
