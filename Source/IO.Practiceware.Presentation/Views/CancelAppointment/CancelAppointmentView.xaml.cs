﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.CancelAppointment;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.CancelAppointment;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.CancelAppointment
{
    /// <summary>
    /// Interaction logic for CancelAppointmentView.xaml
    /// </summary>
    public partial class CancelAppointmentView
    {
        public CancelAppointmentView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// The root view model (the view context) for the MultiCalendarCancelAppointmentViewContext
    /// </summary>
    public class CancelAppointmentViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion
        
        private ObservableCollection<EncounterStatusChangeReasonViewModel> _listOfReason;
        private readonly ICancelAppointmentViewService _cancelAppointmentViewService;

        public CancelAppointmentViewContext(ICancelAppointmentViewService cancelAppointmentViewService)
        {                        
            _cancelAppointmentViewService = cancelAppointmentViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ChangeCancellationType = Command.Create(ExecuteChangeCancellationType);
            Close = Command.Create(ExecuteClose);
            CancelAppointments = Command.Create(ExecuteCancelAppointments, () => SelectedCancellationType != null).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            ProcessLoadArguments();
            _listOfReason = _cancelAppointmentViewService.LoadEncounterStatusChangeReasons().ToExtendedObservableCollection();            
            ListOfCancellationType = _cancelAppointmentViewService.LoadAppointmentCancellationTypes();
            if (HasOnlySingleAppointment)
            {
                SelectedCancellationType = ListOfCancellationType.FirstOrDefault(t => t.Id == Appointments.First().EncounterStatusId);
                SelectedTypeReason = _listOfReason.FirstOrDefault(r => r.Id == Appointments.First().EncounterStatusChangedReasonId);
                if (Appointments.First().IsCancelled) Comment = Appointments.First().EncounterStatusChangedComment;
            }
        }

        private void ProcessLoadArguments()
        {
            if (LoadArguments == null || LoadArguments.ListOfAppointmentId == null || LoadArguments.ListOfAppointmentId.Count == 0) return;
            Appointments = _cancelAppointmentViewService.GetAppointments(LoadArguments.ListOfAppointmentId);

            this.Dispatcher().Invoke(() =>
                {
                    if (Appointments.Any(a => !((EncounterStatus)a.EncounterStatusId).EnsureCanCancel()))
                    {
                        InteractionContext.Complete(false);
                    }
                });
        }

        private void ExecuteChangeCancellationType()
        {
            ListOfSelectedTypeReason = _listOfReason.Where(r => r.EncounterStatusId == SelectedCancellationType.Id).ToExtendedObservableCollection();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteCancelAppointments()
        {           
            _cancelAppointmentViewService.CancelAppointments(Appointments.Select(a => a.Id).ToList(), SelectedCancellationType.Id, SelectedTypeReason, Comment);
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    Appointments.ToList().ForEach(a => a.IsCancelled = true);
                    InteractionContext.Complete(false);
                });

        }

        public CancelAppointmentLoadArguments LoadArguments { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<CancelAppointmentViewModel> Appointments { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ListOfCancellationType { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedCancellationType { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<EncounterStatusChangeReasonViewModel> ListOfSelectedTypeReason { get; set; }

        [DispatcherThread]
        public virtual EncounterStatusChangeReasonViewModel SelectedTypeReason { get; set; }

        [DispatcherThread]
        public virtual string Comment { get; set; }

        [DependsOn("Appointments")]
        public virtual bool HasOnlySingleAppointment
        {
            get { return Appointments != null && Appointments.Count == 1; }
        }

        [DependsOn("Appointments")]
        public virtual String SingleAppointmentDetail
        {
            get
            {
                if (Appointments == null) return null;
                var appointment = Appointments.FirstOrDefault();
                if (appointment == null) return null;
                var date = appointment.StartDateTime.ToString("MM/dd/yyyy");
                var time = appointment.StartDateTime.ToString("hh:mmtt");
                var detail = String.Format("For {0} on {1} at {2}", appointment.PatientName, date, time);
                return detail;
            }
        }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        public ICommand ChangeCancellationType { get; protected set; }

        public ICommand ReasonChanged { get; protected set; }

        public ICommand Close { get; protected set; }

        public ICommand CancelAppointments { get; protected set; }
    }
}