﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.CancelAppointment
{
    public class CancelAppointmentViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public CancelAppointmentViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public CancelAppointmentReturnArguments ShowCancelAppointment(CancelAppointmentLoadArguments loadArgs, bool isModal = false, Soaf.Presentation.WindowStartupLocation startUpLocation = Soaf.Presentation.WindowStartupLocation.CenterScreen)
        {
            bool canShowCancelAppointment = PermissionId.CancelAppointment.EnsurePermission();
            if (!canShowCancelAppointment) { return null; }

            var view = new CancelAppointmentView();
            var dataContext = view.DataContext.EnsureType<CancelAppointmentViewContext>();
            if (loadArgs != null) dataContext.LoadArguments = loadArgs;

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    IsModal = isModal,
                    WindowStartupLocation = startUpLocation,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });

            var ids = dataContext.Appointments.Where(a => a.IsCancelled).Select(a => a.Id).ToList();
            return new CancelAppointmentReturnArguments {ListOfAppointmentId = ids};
        }
    }

    public class CancelAppointmentLoadArguments
    {
        public List<int> ListOfAppointmentId { get; set; }
    }

    public class CancelAppointmentReturnArguments
    {
        public List<int> ListOfAppointmentId { get; set; }
    }
}
