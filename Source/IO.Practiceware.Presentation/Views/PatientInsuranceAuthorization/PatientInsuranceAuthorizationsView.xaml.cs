﻿using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorization;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorization;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization
{
    public partial class PatientInsuranceAuthorizationsView
    {
        public PatientInsuranceAuthorizationsView()
        {
            InitializeComponent();
        }
    }


    public class PatientInsuranceAuthorizationsViewContext : IViewContext
    {
        private readonly IPatientInsuranceAuthorizationsViewService _patientInsuranceAuthorizationsViewService;
        private readonly Func<PatientInsuranceAuthorizationViewModel> _createPatientAuthorizationViewModel;

        public PatientInsuranceAuthorizationsViewContext(IPatientInsuranceAuthorizationsViewService patientInsuranceAuthorizationsViewService, Func<PatientInsuranceAuthorizationViewModel> createPatientAuthorizationViewModel)
        {
            _patientInsuranceAuthorizationsViewService = patientInsuranceAuthorizationsViewService;
            _createPatientAuthorizationViewModel = createPatientAuthorizationViewModel;

            Close = Command.Create(ExecuteClose);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, ButtonsCanExecute).Async(() => InteractionContext);
            Clear = Command.Create(ExecuteClear);
        }

        private void ExecuteSave()
        {
            ErrorMessage = null;

            var errorMessages = PatientInsuranceAuthorization.Validate();

            ErrorMessage = errorMessages.Aggregate("", (current, message) => current + string.Format("{0}{1}", message.Value, Environment.NewLine));

            if (string.IsNullOrEmpty(ErrorMessage))
            {
                PatientInsuranceAuthorization.Id = _patientInsuranceAuthorizationsViewService.Save(PatientInsuranceAuthorization);
                this.Dispatcher().Invoke(() => InteractionContext.Complete(true));
            }
        }

        public bool ButtonsCanExecute()
        {
            return PatientInsuranceAuthorization != null && PatientInsuranceAuthorization.CastTo<IChangeTracking>().IsChanged;
        }

        private void ExecuteClear()
        {
            PatientInsuranceAuthorization.Code = null;
            PatientInsuranceAuthorization.Comments = null;
        }

        private void ExecuteLoad()
        {
            if (LoadArgument.PatientInsuranceAuthorizationId.HasValue)
            {
                PatientInsuranceAuthorization = _patientInsuranceAuthorizationsViewService.LoadPatientInsuranceAuthorization(LoadArgument.PatientInsuranceAuthorizationId.Value);
                PatientInsuranceAuthorization.InvoiceId = LoadArgument.InvoiceId;
                PatientAppointmentInfo = _patientInsuranceAuthorizationsViewService.LoadPatientAppointmentInfo(PatientInsuranceAuthorization.EncounterId);

                PatientInsuranceAuthorization.CastTo<IChangeTracking>().AcceptChanges();
            }
            else if (LoadArgument.EncounterId.HasValue)
            {
                var patientInsuranceAuthorization = _createPatientAuthorizationViewModel();
                PatientAppointmentInfo = _patientInsuranceAuthorizationsViewService.LoadPatientAppointmentInfo(LoadArgument.EncounterId.Value);
                patientInsuranceAuthorization.EncounterId = LoadArgument.EncounterId;
                patientInsuranceAuthorization.InvoiceId = LoadArgument.InvoiceId;
                var firstInsurance = PatientAppointmentInfo.Insurances.FirstOrDefault();
                if (firstInsurance != null) patientInsuranceAuthorization.PatientInsuranceId = firstInsurance.Id;

                PatientInsuranceAuthorization = patientInsuranceAuthorization;

                PatientInsuranceAuthorization.CastTo<IChangeTracking>().AcceptChanges();
            }
            else
            {
                throw new ArgumentException("No PatientInsuranceAuthorizationId or EncounterId found");
            }

            if (PatientAppointmentInfo == null)
            {
                // ReSharper disable NotResolvedInText
                // There is no parameter named PatientAppointmentInfo
                throw new ArgumentNullException("PatientAppointmentInfo", "PatientAppointmentInfo is null");
                // ReSharper restore NotResolvedInText
            }

            if (!PatientAppointmentInfo.Insurances.Any())
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionManager.Current.Alert("No insurances were found for this patient"));
                InteractionContext.Complete(true);
            }
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        /// <summary>
        /// Gets or sets the Patient Id
        /// </summary>
        public int PatientId { get; set; }


        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to save the data
        /// </summary>
        public ICommand Save { get; protected set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to clear the form data
        /// </summary>
        public ICommand Clear { get; protected set; }

        /// <summary>
        /// Determines whether or not the form is valid
        /// </summary>
        [DispatcherThread]
        [DependsOn("ErrorMessage")]
        public virtual bool HasError { get { return !ErrorMessage.IsNullOrEmpty(); } }

        /// <summary>
        /// The error message if validation fails
        /// </summary>
        [DispatcherThread]
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        /// Member containing basic information about the Patient and Appointment
        /// </summary>
        [DispatcherThread]
        public virtual PatientAppointmentInfo PatientAppointmentInfo { get; set; }

        /// <summary>
        /// Member providing 2-way binding for the authorization data
        /// </summary>
        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationViewModel PatientInsuranceAuthorization { get; set; }

        public PatientInsuranceAuthorizationsLoadArguments LoadArgument { get; set; }
    }
}