﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization
{
    public class PatientInsuranceAuthorizationsViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private PatientInsuranceAuthorizationsViewContext _dataContext;

        public PatientInsuranceAuthorizationsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public PatientInsuranceAuthorizationsReturnArguments ShowPatientInsuranceAuthorization(PatientInsuranceAuthorizationsLoadArguments loadArguments)
        {
            if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                return (PatientInsuranceAuthorizationsReturnArguments)System.Windows.Application.Current.Dispatcher.Invoke(new Func<object>(() => ShowPatientInsuranceAuthorization(loadArguments)));

            var view = new PatientInsuranceAuthorizationsView();

            _dataContext = view.DataContext as PatientInsuranceAuthorizationsViewContext;
            if (_dataContext == null) { throw new Exception("The PatientInsuranceAuthorization's DataContext is not of type {0}".FormatWith(typeof(PatientInsuranceAuthorizationsViewContext).Name)); }

            _dataContext.LoadArgument = loadArguments;
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip });
            var returnArguments = new PatientInsuranceAuthorizationsReturnArguments();
            if (_dataContext.PatientInsuranceAuthorization.Id.HasValue) returnArguments.AuthorizationId = _dataContext.PatientInsuranceAuthorization.Id.Value;
            return returnArguments;
        }

    }

    public class PatientInsuranceAuthorizationsLoadArguments
    {
        public int? EncounterId { get; set; }

        public int? PatientInsuranceAuthorizationId { get; set; }

        public int? InvoiceId { get; set; }
    }

    public class PatientInsuranceAuthorizationsReturnArguments
    {
        public int AuthorizationId { get; set; }
    }
}