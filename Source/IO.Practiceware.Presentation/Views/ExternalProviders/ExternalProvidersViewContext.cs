﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ExternalProviders;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Utilities;
using IO.Practiceware.Presentation.ViewServices.ExternalProviders;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.ExternalProviders
{
    public class ExternalProvidersViewContext : IViewContext
    {
        private readonly IExternalProvidersViewService _externalProvidersViewService;
        private readonly Func<ExternalProviderViewModel> _createNewExternalProvider;
        private readonly Func<EmailAddressViewModel> _createNewEmailAddress;
        private readonly Func<PhoneNumberViewModel> _createNewPhoneNumber;
        private readonly WarningsViewManager _warningsViewManager;
        private bool _closeOnSave;
        private bool _isClosing;
        readonly IInteractionManager _interactionManager;
        private readonly UtilitiesViewManager _utilitiesViewManager;

        [DispatcherThread]
        public virtual DataListsViewModel DataLists { get; set; }

        public virtual ObservableCollection<int?> DeletedExternalProviders { get; set; }

        private List<int> LoadedExternalProviders { get; set; }

        [DispatcherThread]
        public virtual ExtendedObservableCollection<ExternalProviderViewModel> ExternalProviders { get; set; }

        [DispatcherThread]
        [DependsOn("ExternalProviders")]
        public virtual ExtendedObservableCollection<ExternalProviderViewModel> FilteredExternalProviders { get; set; }

        public virtual ExternalProviderViewModel SelectedExternalProvider { get; set; }
        
        [DispatcherThread]
        public virtual ExtendedObservableCollection<NamedViewModel> ContactTypes { get; set; }
        
        [DispatcherThread]
        public virtual NamedViewModel SelectedContactType { get; set; }

        public int SelectedContactTypeId { get; set; }
        
        public virtual bool CanCreateNewExternalProvider
        {
            get { return PermissionId.CreateExternalProviders.PrincipalContextHasPermission(); }
        }

        public virtual bool CanDeleteExternalProviders
        {
            get { return PermissionId.DeleteExternalProvider.PrincipalContextHasPermission(); }
        }

        public virtual bool CanEditExternalProvider
        {
            get { return PermissionId.EditExternalProviders.PrincipalContextHasPermission(); }
        }

        public ExternalProvidersViewContext(IExternalProvidersViewService externalProvidersViewService
                                            , Func<ExternalProviderViewModel> createNewExternalProvider
                                            , Func<EmailAddressViewModel> createNewEmailAddress
                                            , Func<PhoneNumberViewModel> createNewPhoneNumber
                                            , IInteractionManager interactionManager
                                            , UtilitiesViewManager utilitiesViewManager
                                            , WarningsViewManager warningsViewManager)
        {
            _externalProvidersViewService = externalProvidersViewService;
            _createNewExternalProvider = createNewExternalProvider;
            _createNewPhoneNumber = createNewPhoneNumber;
            _createNewEmailAddress = createNewEmailAddress;
            _warningsViewManager = warningsViewManager;
            _interactionManager = interactionManager;
            _utilitiesViewManager = utilitiesViewManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            LoadProviders = Command.Create<String>(ExecuteLoadProviders).Async(() => InteractionContext);
            LoadExternalProviderDetails = Command.Create<ExternalProviderViewModel>(ExecuteLoadDetails).Async(() => InteractionContext);
            CreateNew = Command.Create(ExecuteCreateNew);
            DeleteAddress = Command.Create(ExecuteDeleteAddress);
            DeleteEmailAddress = Command.Create<EmailAddressViewModel>(ExecuteDeleteEmailAddress);
            DeletePhoneNumber = Command.Create<PhoneNumberViewModel>(ExecuteDeletePhoneNumber);
            DeleteExternalProvider = Command.Create<ExternalProviderViewModel>(ExecuteDeleteExternalProvider).Async(() => InteractionContext);
            AddNewEmailAddress = Command.Create(ExecuteAddNewEmailAddress);
            AddNewPhoneNumber = Command.Create(ExecuteAddNewPhoneNumber);
            Close = Command.Create<Action>(ExecuteClose);
            CopyExternalProvider = Command.Create(ExecuteCopyExternalProvider);
            Save = Command.Create(ExecuteSave).Async(() => InteractionContext);
            Filter = Command.Create<String>(ExecuteFilter);
            MergeProviders = Command.Create(() => _utilitiesViewManager.ShowMergeProviders(), () => SelectedContactType != null && SelectedContactType.Id == (int) ContactTypeId.ExternalProviders);

            ExternalProviders = new ExtendedObservableCollection<ExternalProviderViewModel>();
            FilteredExternalProviders = new ExtendedObservableCollection<ExternalProviderViewModel>();
            DeletedExternalProviders = new ObservableCollection<int?>();

            LoadedExternalProviders = new List<int>();
        }

        #region private methods

        private void ExecuteLoad()
        {
            ContactTypes = Enum.GetValues(typeof(ContactTypeId)).Cast<ContactTypeId>().Select(x => new NamedViewModel { Id = (int)x, Name = x.GetDisplayName() }).ToExtendedObservableCollection();
            SelectedContactType = ContactTypes.First(c => c.Id == (SelectedContactTypeId > 0 ? SelectedContactTypeId : 1));
            if (!PermissionId.ViewManageExternalProviders.EnsurePermission()) return;
            var loadViewModel = _externalProvidersViewService.GetDataLists((ContactTypeId) SelectedContactType.Id);
            DataLists = loadViewModel.DataLists;
            ExecuteLoadProviders(string.Empty);
        }

        private void ExecuteLoadProviders(String s)
        {
            LoadedExternalProviders = new List<int>();

            // Prevents loss of changes if they start a new search
            CheckForChangesAndSave();

            var loadViewModel = _externalProvidersViewService.GetExternalProviders(s, (ContactTypeId) SelectedContactType.Id);

            if (loadViewModel == null || loadViewModel.ExternalProviders.IsNullOrEmpty() || loadViewModel.ExternalProviders.Count <= 0)
            {
                this.Dispatcher().Invoke(() => _warningsViewManager.ShowWarnings(new List<String>(), "No providers were found. Please try searching again using different criteria."));
                return;
            }
                
            ExternalProviders = loadViewModel.ExternalProviders.ToExtendedObservableCollection();
            FilteredExternalProviders = ExternalProviders.ToExtendedObservableCollection();
        }

        private void ExecuteLoadDetails(ExternalProviderViewModel externalProvider)
        {
            if (!PermissionId.EditExternalProviders.EnsurePermission()) return;
            // External Provider can be null if they alter the search string for filtering after selecting an 
            //external provider and the new search string can't be found in the existing results
            if (externalProvider != null && externalProvider.Id.HasValue
                && !LoadedExternalProviders.Contains(externalProvider.Id.Value))
            {
                var externalProviderWithDetails = _externalProvidersViewService.GetExternalProviderDetails(externalProvider.Id.Value, (ContactTypeId) SelectedContactType.Id);

                // Setting the details this way because we get ItemsCount errors when simply replacing the existing item 
                // Missing Name Info
                externalProvider.MiddleName = externalProviderWithDetails.MiddleName ?? string.Empty;
                externalProvider.Suffix = externalProviderWithDetails.Suffix;
                externalProvider.Title = externalProviderWithDetails.Title;
                externalProvider.NickName = externalProviderWithDetails.NickName;
                externalProvider.DisplayName = externalProviderWithDetails.DisplayName;

                // Missing Phone Numbers and Emails
                externalProvider.EmailAddresses = externalProviderWithDetails.EmailAddresses;
                externalProvider.PhoneNumbers = externalProviderWithDetails.PhoneNumbers;

                // Professional Details
                externalProvider.Npi = externalProviderWithDetails.Npi;
                externalProvider.Taxonomy = externalProviderWithDetails.Taxonomy;
                externalProvider.ExcludeFromClaim = externalProviderWithDetails.ExcludeFromClaim;
                externalProvider.Comment = externalProviderWithDetails.Comment;

                LoadedExternalProviders.Add(externalProvider.Id.Value);
                // To prevent the code from believing this item has been edited just from loading it's details
                externalProvider.CastTo<IChangeTracking>().AcceptChanges();
            }
        }

        private void ExecuteCreateNew()
        {
            if (!PermissionId.CreateExternalProviders.EnsurePermission()) return;
            var newExternalProvider = _createNewExternalProvider();
            newExternalProvider.CastTo<IEditableObject>().BeginEdit();
            if (SelectedContactType.Id != (int) ContactTypeId.ExternalProviders)
            {
                newExternalProvider.IsEntity = true;
            }
            // Add new External Provider to External Providers list which saves
            // and Filtered External Providers which displays
            ExternalProviders.Add(newExternalProvider);
            FilteredExternalProviders.Add(newExternalProvider);
        }

        private void ExecuteDeleteAddress()
        {
            SelectedExternalProvider.Address = null;
        }

        private void ExecuteDeleteEmailAddress(EmailAddressViewModel args)
        {
            SelectedExternalProvider.EmailAddresses.Remove(args);
        }

        private void ExecuteDeletePhoneNumber(PhoneNumberViewModel args)
        {
            SelectedExternalProvider.PhoneNumbers.Remove(args);
        }

        private void ExecuteDeleteExternalProvider(ExternalProviderViewModel args)
        {
            if (!PermissionId.DeleteExternalProvider.EnsurePermission()) return;

            // Check if can be deleted from a db perspective
            var canBeDeleted = args.Id.HasValue && _externalProvidersViewService.CanDelete(args.Id.Value, (ContactTypeId) SelectedContactType.Id);

            // If it can't display message
            // Added check for if  the External Provider Id has value so it doesn't show the warning if a user deletes a newly created external provider
            // that hasn't been saved yet
            if (!canBeDeleted && args.Id.HasValue)
            {
                // Delete what can be deleted from it and set to IsArchived
                args.IsArchived = true;
                this.Dispatcher().Invoke(() => _warningsViewManager.ShowWarnings(new List<String>(), string.Format("This provider ({0}) is in use and connot be deleted. It will be set to Archived.", args.FormattedName)));
            }
            else
            {
                // otherwise remove it
                DeletedExternalProviders.Add(args.Id);
                ExternalProviders.Remove(args);
                FilteredExternalProviders.Remove(args);
            }
        }

        private void ExecuteAddNewEmailAddress()
        {
            SelectedExternalProvider.EmailAddresses.Add(_createNewEmailAddress());
        }

        private void ExecuteAddNewPhoneNumber()
        {
            SelectedExternalProvider.PhoneNumbers.Add(_createNewPhoneNumber());
        }

        private void ExecuteClose(Action cancel)
        {
            if (!_isClosing)
            {
                _isClosing = true;
                if (CheckForChangesAndSave() && cancel != null)
                {
                    cancel();
                    _closeOnSave = true;
                }
                else
                {
                    InteractionContext.Complete(false);
                }
            }
            _isClosing = false;
        }

        private void ExecuteCopyExternalProvider()
        {
            // Check permission
            if (!PermissionId.CreateExternalProviders.EnsurePermission()) return;

            // Make the copy of the current External Provider
            var newExternalProvider = _createNewExternalProvider();
            newExternalProvider.LastNameOrEntityName = SelectedExternalProvider.LastNameOrEntityName;
            newExternalProvider.FirstName = SelectedExternalProvider.FirstName;
            newExternalProvider.MiddleName = SelectedExternalProvider.MiddleName;
            newExternalProvider.DisplayName = SelectedExternalProvider.DisplayName;
            newExternalProvider.NickName = SelectedExternalProvider.NickName;
            newExternalProvider.Honorific = SelectedExternalProvider.Honorific;
            newExternalProvider.IsEntity = SelectedExternalProvider.IsEntity;
            newExternalProvider.SpecialtyType = SelectedExternalProvider.SpecialtyType;
            newExternalProvider.Suffix = SelectedExternalProvider.Suffix;
            newExternalProvider.Title = SelectedExternalProvider.Title;

            if (SelectedExternalProvider.Address != null)
            {
                newExternalProvider.Address = new AddressViewModel();

                newExternalProvider.Address.Line1 = SelectedExternalProvider.Address.Line1;
                newExternalProvider.Address.Line2 = SelectedExternalProvider.Address.Line2;
                newExternalProvider.Address.Line3 = SelectedExternalProvider.Address.Line3;
                newExternalProvider.Address.City =  SelectedExternalProvider.Address.City;

                if (SelectedExternalProvider.Address.StateOrProvince != null)
                {
                    newExternalProvider.Address.StateOrProvince = new StateOrProvinceViewModel()
                    {
                        Id = SelectedExternalProvider.Address.StateOrProvince.Id,
                        Name = SelectedExternalProvider.Address.StateOrProvince.Name,
                        Abbreviation = SelectedExternalProvider.Address.StateOrProvince.Abbreviation,
                    };

                    if (SelectedExternalProvider.Address.StateOrProvince.Country != null)
                    {
                        newExternalProvider.Address.StateOrProvince.Country = new NameAndAbbreviationViewModel()
                        {
                            Id = SelectedExternalProvider.Address.StateOrProvince.Country.Id,
                            Name = SelectedExternalProvider.Address.StateOrProvince.Country.Name,
                            Abbreviation = SelectedExternalProvider.Address.StateOrProvince.Country.Abbreviation,
                        };
                    }
                }

                newExternalProvider.Address.PostalCode = SelectedExternalProvider.Address.PostalCode;
                if (SelectedExternalProvider.Address.AddressType != null)
                {
                    newExternalProvider.Address.AddressType = new NamedViewModel(SelectedExternalProvider.Address.AddressType.Id, SelectedExternalProvider.Address.AddressType.Name);
                }
            }

            newExternalProvider.EmailAddresses = new ExtendedObservableCollection<EmailAddressViewModel>();
            if (SelectedExternalProvider.EmailAddresses != null && SelectedExternalProvider.EmailAddresses.Count > 0)
            {
                SelectedExternalProvider.EmailAddresses.ForEach(email => {
                    EmailAddressViewModel emailAddressViewModel = new EmailAddressViewModel();

                    emailAddressViewModel.Value = email.Value;
                    emailAddressViewModel.OrdinalId = email.OrdinalId;
                    emailAddressViewModel.PatientId = email.PatientId;

                    if (email.EmailAddressType != null)
                    {
                        emailAddressViewModel.EmailAddressType = new NamedViewModel(email.EmailAddressType.Id, email.EmailAddressType.Name);
                    }

                    newExternalProvider.EmailAddresses.Add(emailAddressViewModel);
                });
            }

            newExternalProvider.PhoneNumbers = new ExtendedObservableCollection<PhoneNumberViewModel>();
            if (SelectedExternalProvider.PhoneNumbers != null && SelectedExternalProvider.PhoneNumbers.Count > 0)
            {
                SelectedExternalProvider.PhoneNumbers.ForEach(phone =>
                {
                    PhoneNumberViewModel phoneNumberViewModel = new PhoneNumberViewModel();

                    phoneNumberViewModel.IsInternational = phone.IsInternational;
                    phoneNumberViewModel.CountryCode = phone.CountryCode;
                    phoneNumberViewModel.AreaCode = phone.AreaCode;
                    phoneNumberViewModel.ExchangeAndSuffix = phone.ExchangeAndSuffix;
                    phoneNumberViewModel.Extension = phone.Extension;

                    if (phone.PhoneType != null)
                    {
                        phoneNumberViewModel.PhoneType = new NamedViewModel(phone.PhoneType.Id, phone.PhoneType.Name);
                    }

                    newExternalProvider.PhoneNumbers.Add(phoneNumberViewModel);
                });
            }

            // Add the copied External provider to the existing list
            ExternalProviders.Add(newExternalProvider);
            InteractionManager.Current.Alert("Click Save button to Save Duplicate provider.");
        }

        private void ExecuteSave()
        {
            // Remove Empty External Providers, Emails, And Phone Numbers
            RemoveEmptyItems();

            // Get the items that have changes
            var externalProvidersWithChanges = ExternalProviders.Where(p => p.CastTo<IChangeTracking>().IsChanged).ToList();
            // Check for changes in individual external providers as well as deletions from overall list
            if (!externalProvidersWithChanges.Any() && ExternalProviders.CastTo<IChangeTracking>().IsChanged)
            {
                externalProvidersWithChanges = ExternalProviders.ToList();
            }

            // Validate Items with changes
            var validationResult = externalProvidersWithChanges.ValidateItems();

            if (validationResult.Any())
            {
                var errors = validationResult.ToDictionary(
                    i => string.Format("{0} ({1}): ", i.Key.FormattedName, i.Key.Id),
                    v => v.Value.Values.Join(Environment.NewLine));

                ShowValidationErrorsDialog(errors.ToValidationMessagesList());
            }
            else
            {
                // Save Items
                var idMapping = _externalProvidersViewService.Save(externalProvidersWithChanges, DeletedExternalProviders.Where(x => x.HasValue).Select(x => x.Value).ToList(), (ContactTypeId) SelectedContactType.Id);


                for (int i = 0; i < idMapping.Count; i++)
                {
                    if (!externalProvidersWithChanges[i].Id.HasValue)
                    {
                        externalProvidersWithChanges[i].Id = idMapping[i];
                    }
                }
                ExternalProviders.CastTo<IEditableObjectExtended>().AcceptCustomEdits();

                if (_closeOnSave)
                {
                    InteractionContext.Complete(true);
                }
            }
            _closeOnSave = false;
            _isClosing = false;
        }

        private void ExecuteFilter(String filterTextBoxValue)
        {
            FilteredExternalProviders = ExternalProviders.Where(x => Contains(x, filterTextBoxValue)).ToExtendedObservableCollection();
        }

        private bool Contains(ExternalProviderViewModel filteredExternalProvider, String filterText)
        {
            if (String.IsNullOrEmpty(filterText)) return true;
            return filteredExternalProvider.LastNameOrEntityName.ContainsIgnoreCase(filterText) || filteredExternalProvider.FirstName.ContainsIgnoreCase(filterText);
        }

        private void RemoveEmptyItems()
        {
            ExternalProviders.ForEach(x => x.EmailAddresses.Where(ea => ea.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(ea => x.EmailAddresses.Remove(ea)));
            ExternalProviders.ForEach(x => x.PhoneNumbers.Where(pn => pn.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(pn => x.PhoneNumbers.Remove(pn)));
            ExternalProviders.Where(x => x.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(ep => ExternalProviders.Remove(ep));
        }

        private void ShowValidationErrorsDialog(IEnumerable<string> errors)
        {
            this.Dispatcher().Invoke(() => _warningsViewManager.ShowWarnings(errors, "Cannot Save: "));
        }

        private bool CheckForChangesAndSave()
        {
            // Are there any unsaved changes?
            var unsavedChanges = ExternalProviders.Any(p => p.CastTo<IChangeTracking>().IsChanged) 
                                || ExternalProviders.CastTo<IChangeTracking>().IsChanged;

            if (unsavedChanges)
            {
                // Confirm whether to save them
                bool? saveChanges = false;
                this.Dispatcher().Invoke(() =>
                {
                    saveChanges = _interactionManager.Confirm("Would you like to save your changes?", "No", "Save");
                });

                // Yes?
                // ReSharper disable once PossibleInvalidOperationException
                if (saveChanges == true)
                {
                    // Commit all changes
                    Save.Execute();
                    return true;
                }
            }
            return false;
        }

        #endregion

        public IInteractionContext InteractionContext { get; set; }

        #region Commands

        public ICommand Load { get; set; }

        public ICommand LoadProviders { get; set; }

        public ICommand LoadExternalProviderDetails { get; set; }

        public ICommand CreateNew { get; set; }

        public ICommand DeleteAddress { get; set; }

        public ICommand DeleteEmailAddress { get; set; }

        public ICommand DeleteExternalProvider { get; set; }

        public ICommand DeletePhoneNumber { get; set; }

        public ICommand AddNewEmailAddress { get; set; }

        public ICommand AddNewPhoneNumber { get; set; }

        public ICommand Close { get; set; }

        public ICommand CopyExternalProvider { get; set; }

        public ICommand Save { get; set; }

        public ICommand Filter { get; set; }

        public virtual ICommand MergeProviders { get; set; }
        #endregion
    }
}
