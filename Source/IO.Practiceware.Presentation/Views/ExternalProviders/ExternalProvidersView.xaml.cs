﻿
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ExternalProviders
{
    /// <summary>
    /// Interaction logic for ExternalProvidersView.xaml
    /// </summary>
    public partial class ExternalProvidersView
    {
        public ExternalProvidersView()
        {
            InitializeComponent();
        }
    }

    public class ExternalProvidersViewContextReference : DataContextReference
    {
        public new ExternalProvidersViewContext DataContext
        {
            get { return (ExternalProvidersViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
