﻿using System.Windows;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ExternalProviders
{
    public class ExternalProvidersViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public ExternalProvidersViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowExternalProviders()
        {
            var view = new ExternalProvidersView();
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip });
        }

        public void ShowExternalProviders(int contactTypeId)
        {
            var view = new ExternalProvidersView();
            var dataContext = view.DataContext.EnsureType<ExternalProvidersViewContext>();
            dataContext.SelectedContactTypeId = contactTypeId;
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip });
        }
    }
}
