﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.GenerateStatements;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.Reporting;
using IO.Practiceware.Presentation.ViewServices.GenerateStatements;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace IO.Practiceware.Presentation.Views.GenerateStatements
{
    public class GenerateStatementsViewContext : IViewContext
    {
        readonly IGenerateStatementsViewService _statementsViewService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly Func<FilterViewModel> _filterViewModel;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly ReportViewManager _reportViewManager;
        private readonly DocumentViewManager _documentViewManger;

        public GenerateStatementsViewContext(IGenerateStatementsViewService generateStatementsViewService
            , IApplicationSettingsService applicationSettingsService
            , Func<FilterViewModel> filterViewModel
            , PatientInfoViewManager patientInfoViewManager
            , ReportViewManager reportViewManager
            , DocumentViewManager documentViewManger)
        {
            _statementsViewService = generateStatementsViewService;
            _applicationSettingsService = applicationSettingsService;
            _filterViewModel = filterViewModel;
            _patientInfoViewManager = patientInfoViewManager;
            _reportViewManager = reportViewManager;
            _documentViewManger = documentViewManger;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            SaveFilter = Command.Create(ExecuteSaveFilter, () => SelectedSavedFilter != null).Async(() => InteractionContext);
            DeleteFilter = Command.Create<GenerateStatementsFilter>(ExecuteDeleteFilter).Async(() => InteractionContext);
            ResetFilter = Command.Create(ExecuteResetFilter);
            DisplayResult = Command.Create(ExecuteDisplayResult).Async(() => InteractionContext, supportsCancellation: true);
            LaunchFinancialSummaryScreen = Command.Create<int>(ExecuteLaunchFinancialSummary);
            Print = Command.Create(ExecutePrint).Async(() => InteractionContext, supportsCancellation: true);
            ForwardBillingServiceTransactions = Command.Create<Dictionary<int, byte[]>>(ExecuteForwardBillingServiceTransactions).Async(() => InteractionContext);
            Export = Command.Create(ExecuteExport).Async(() => InteractionContext);
            SelectAllAggregateStatements = Command.Create(ExecuteSelectAllAggregateStatements, () => FilteredAggregateStatements != null && FilteredAggregateStatements.Any());
            DeselectAllAggregateStatements = Command.Create(ExecuteDeselectAllAggregateStatements, () => SelectedAggregateStatements != null && SelectedAggregateStatements.Any());
            Close = Command.Create(ExecuteClose);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete();
        }

        private void ExecuteSelectAllAggregateStatements()
        {
            SelectedAggregateStatements.Clear();
            SelectedAggregateStatements.AddRange(FilteredAggregateStatements);
        }

        private void ExecuteDeselectAllAggregateStatements()
        {
            SelectedAggregateStatements.Clear();
        }

        private void ExecuteExport()
        {
            Guid[] billingServiceTransactionIds = null;
            int[] billingServiceIds = null;
            int[] invoiceIds = null;

            var patientIds = SelectedAggregateStatements
                .GroupBy(s => s.Patient.Id)
                .Select(g => g.Key)
                .ToArray();

            var openBalanceTypeSettings = ApplicationSettings.Cached.GetSetting<NamedViewModel>(ApplicationSetting.OpenBalanceDeterminer);

            var openBalanceType = openBalanceTypeSettings != null ? openBalanceTypeSettings.Value : new NamedViewModel { Id = (int)OpenBalanceType.Service, Name = OpenBalanceType.Service.ToString() };

            if (openBalanceType.Id == (int)OpenBalanceType.Patient)
            {
                var billingServiceTransactions = SelectedAggregateStatements.GroupBy(s => s.BillingServiceTransactions).SelectMany(s => s.Key);
                billingServiceTransactionIds = billingServiceTransactions.Select(bst => bst.Id).ToArray();
            }

            else if (openBalanceType.Id == (int)OpenBalanceType.Invoice)
            {
                var invoices = SelectedAggregateStatements.GroupBy(s => s.InvoiceAdjustments).SelectMany(s => s.Key);
                invoiceIds = invoices.Select(inv => inv.InvoiceId).ToArray();
            }
            else
            {
                var billingServices = SelectedAggregateStatements.GroupBy(s => s.BillingServices).SelectMany(s => s.Key);
                billingServiceIds = billingServices.Select(bs => bs.Id).ToArray();
            }

            var parameters = new Hashtable(new Hashtable
                                           {
                                               {"PatientIds", patientIds},
                                               {"BillingServiceTransactionIds", billingServiceTransactionIds},
                                               {"InvoiceIds", invoiceIds},
                                               {"BillingServiceIds", billingServiceIds},
                                               {"ServiceLocationIds", Filter.ServiceLocations.Select(s => s.Id).Distinct().ToArray()},
                                               {"RenderingProvideIds", Filter.RenderingProviders.Select(s => s.Id).Distinct().ToArray()},
                                           });

            //Make sure only Selected BillingServices or Invoices or Patients gets removed from the list.

            //Commneted below two lines since these are creating issues in cloud.
            /*var filterdPatientIds = _statementsViewService.FilterPatients(parameters);
            parameters["PatientIds"] = filterdPatientIds.Select(i => i.Id).ToArray();*/
            var results = GenerateExportContent(parameters);

            var exportStatement = results.Values.Join("");
            var patientIdsToForward = results.Where(x => x.Value != null).Select(x => x.Key).ToArray();

            var isSaved = false;
            this.Dispatcher().Invoke(() => isSaved = SaveTextFile(exportStatement));

            if (isSaved)
            {
                foreach (var p in patientIdsToForward)
                {
                    var patientId = p;
                    byte[] result = null;
                    this.Dispatcher().Invoke(() =>
                    {
                        var printparameters = new Hashtable(new Hashtable
                                           {
                                               {"BillingServiceTransactionIds", billingServiceTransactionIds},
                                               {"InvoiceIds", invoiceIds},
                                               {"BillingServiceIds", billingServiceIds},
                                               {"PatientId", patientId},
                                               {"ServiceLocations",Filter.ServiceLocations.Select(s => s.Id).Distinct().ToArray()},
                                               {"Providers",Filter.RenderingProviders.Select(s => s.Id).Distinct().ToArray()}
                                           });

                        result = RenderReport(printparameters);
                    });
                    ExecForwardBillingServiceTransactions(new Dictionary<int, byte[]> { { patientId, result } }, parameters, true);
                }
                //Do not remove the rows where patient not having a first name, last name, or address because they have not been processed.
                var idsNotToRemove = results.Where(r => r.Value == null).Select(r => r.Key).ToArray();
                this.Dispatcher().Invoke(() => SelectedAggregateStatements.Where(statement => !idsNotToRemove.Contains(statement.Patient.Id)).ToList().
                     ForEach(statement => FilteredAggregateStatements.Remove(statement)));
            }

            if (results.Any(x => x.Value == null))
            {
                InteractionManager.Current.Alert("Could not generate statements for patients with id(s): {0}. This is most like due to the patient not having a first name, last name, or address.".FormatWith(results.Where(x => x.Value == null).Select(x => x.Key).Join()));
            }
        }

        public Dictionary<int, string> GenerateExportContent(Hashtable parameters)
        {
            return _statementsViewService.GenerateExportContent(parameters, ApplicationSettings.Cached.GetSetting<int>(ApplicationSetting.StatementExportFormat).IfNotNull(x => x.Value));
        }

        private void ExecutePrint()
        {
            Guid[] billingServiceTransactionIds = null;
            int[] billingServiceIds = null;
            int[] invoiceIds = null;

            var patientIds = SelectedAggregateStatements
                .GroupBy(s => s.Patient.Id)
                .Select(g => g.Key)
                .ToArray();

            var openBalanceTypeSettings = ApplicationSettings.Cached.GetSetting<NamedViewModel>(ApplicationSetting.OpenBalanceDeterminer);

            var openBalanceType = openBalanceTypeSettings != null ? openBalanceTypeSettings.Value : new NamedViewModel { Id = (int)OpenBalanceType.Service, Name = OpenBalanceType.Service.ToString() };

            if (openBalanceType.Id == (int)OpenBalanceType.Patient)
            {
                var billingServiceTransactions = SelectedAggregateStatements.GroupBy(s => s.BillingServiceTransactions).SelectMany(s => s.Key);
                billingServiceTransactionIds = billingServiceTransactions.Select(bst => bst.Id).ToArray();
            }

            else if (openBalanceType.Id == (int)OpenBalanceType.Invoice)
            {
                var invoices = SelectedAggregateStatements.GroupBy(s => s.InvoiceAdjustments).SelectMany(s => s.Key);
                invoiceIds = invoices.Select(inv => inv.InvoiceId).ToArray();
            }
            else
            {
                var billingServices = SelectedAggregateStatements.GroupBy(s => s.BillingServices).SelectMany(s => s.Key);
                billingServiceIds = billingServices.Select(bs => bs.Id).ToArray();
            }

            var parameters = new Hashtable(new Hashtable
                                           {
                                               {"BillingServiceTransactionIds", billingServiceTransactionIds},
                                               {"InvoiceIds", invoiceIds},
                                               {"BillingServiceIds", billingServiceIds},
                                               {"PatientIds", patientIds}
                                           });

            //Commneted below three lines since these are creating issues in cloud.
            /*var filteredPatients = _statementsViewService.FilterPatients(parameters);
            var filteredPatientIds = filteredPatients.Select(i => i.Id).Distinct();
            patientIds = filteredPatientIds.ToArray();*/

            var printerName = "";
            foreach (var p in patientIds)
            {
                var patientId = p;
                byte[] result = null;
                this.Dispatcher().Invoke(() =>
                {
                    var printparameters = new Hashtable(new Hashtable
                                           {
                                               {"BillingServiceTransactionIds", billingServiceTransactionIds},
                                               {"InvoiceIds", invoiceIds},
                                               {"BillingServiceIds", billingServiceIds},
                                               {"PatientId", patientId},
                                               {"ServiceLocations",Filter.ServiceLocations.Select(s => s.Id).Distinct().ToArray()},
                                               {"Providers",Filter.RenderingProviders.Select(s => s.Id).Distinct().ToArray()}
                                           });

                    result = RenderReport(printparameters);
                    printerName = _documentViewManger.PrintDocument("Patient Statement", result, "pdf", printerName);
                });
                ExecForwardBillingServiceTransactions(new Dictionary<int, byte[]> { { patientId, result } }, parameters);
            }

            ExecuteDisplayResult();
        }

        public byte[] RenderReport(Hashtable parameters)
        {
            return _reportViewManager.RenderReport("Patient Statement", "pdf", parameters);
        }

        private void ExecuteForwardBillingServiceTransactions(Dictionary<int, byte[]> individualReportPdfs)
        {
            ExecForwardBillingServiceTransactions(individualReportPdfs, new Hashtable { { "PatientIds", individualReportPdfs.Keys.ToArray() } });
        }

        private void ExecForwardBillingServiceTransactions(Dictionary<int, byte[]> individualReportPdfs, Hashtable parameters, bool isExport = false)
        {
            _statementsViewService.ForwardBillingServiceTransactions(individualReportPdfs, parameters, isExport);
        }

        private void ExecuteDisplayResult()
        {
            FilteredAggregateStatements = _statementsViewService.GetStatements(Filter)
                .OrderBy(s => s.Patient.Name)
                .ThenBy(s => s.DateTime)
                .ToObservableCollection();
        }

        private void ExecuteLoad()
        {
            _newStatementsFiltersFilter = new GenerateStatementsFilter { Name = "Create New" };
            _filterApplicationSettings = _applicationSettingsService.GetSettings<GenerateStatementsFilter>(ApplicationSetting.GenerateStatementsFilter,
                ApplicationContext.Current.ComputerName,
                UserContext.Current.UserDetails.Id);
            Filter = _filterViewModel();

            DataLists = _statementsViewService.LoadInformation();
            SavedFilters = new[] { _newStatementsFiltersFilter }
                .Concat(_filterApplicationSettings.OrderByDescending(x => x.Scope).Select(a => a.Value))
                .ToExtendedObservableCollection();
            SelectedSavedFilter = SavedFilters.Count > 1 ? SavedFilters.Skip(1).First() : null;
        }

        private void ExecuteSaveFilter()
        {
            if (SelectedSavedFilter == null)
            {
                return;
            }

            var applicationSettingToSave = _filterApplicationSettings.FirstOrDefault(a => a.Value == SelectedSavedFilter);

            if (applicationSettingToSave == null && SelectedSavedFilter.Name.ToLowerInvariant() == "create new")
            {
                // new filter
                var promptResult = InteractionManager.Current.Prompt("Enter name below to save your selected filters:", "Saved Filters");

                if (promptResult.DialogResult.GetValueOrDefault() && promptResult.Input.IsNullOrEmpty())
                {
                    InteractionManager.Current.Alert("You must specify a name.");
                    return;
                }

                applicationSettingToSave = new ApplicationSettingContainer<GenerateStatementsFilter>
                {
                    Name = ApplicationSetting.GenerateStatementsFilter,
                    UserId = UserContext.Current.UserDetails.Id,
                    MachineName = ApplicationContext.Current.ComputerName,
                    Value = new GenerateStatementsFilter { Name = promptResult.Input }
                };

                SavedFilters.Add(applicationSettingToSave.Value);
            }

            if (applicationSettingToSave == null)
            {
                //try get application setting to save by name
                applicationSettingToSave = _filterApplicationSettings.FirstOrDefault(fas => fas.Name == SelectedSavedFilter.Name);
            }

            // update saved filter settings
            if (applicationSettingToSave != null)
            {
                applicationSettingToSave.Value.BillingCycleDays = Filter.BillingCycleDays;
                applicationSettingToSave.Value.MinimumAmount = Filter.MinimumAmount;
                applicationSettingToSave.Value.PatientLastNameFrom = Filter.PatientLastNameFrom;
                applicationSettingToSave.Value.PatientLastNameTo = Filter.PatientLastNameTo;

                applicationSettingToSave.Value.RenderingProvideIds = Filter.RenderingProviders.Select(rp => rp.Id).ToList();
                applicationSettingToSave.Value.ServiceLocationIds = Filter.ServiceLocations.Select(s => s.Id).ToList();
                applicationSettingToSave.Value.TransactionStatusIds = Filter.TransactionStatuses.Select(s => s.Id).ToList();
                applicationSettingToSave.Value.StatmentCommunicationPreferenceIds = Filter.StatmentCommunicationPreferences.Select(sp => sp.Id).ToList();

                _applicationSettingsService.SetSetting(applicationSettingToSave);

                SavedFilters = SavedFilters.OrderBy(i => i == _newStatementsFiltersFilter ? 0 : 1)
                    .ThenBy(i => i.Name).ToExtendedObservableCollection();

                SelectedSavedFilter = applicationSettingToSave.Value;
            }
        }
        private void ExecuteDeleteFilter(GenerateStatementsFilter filter)
        {
            var applicationSettingToDelete = _filterApplicationSettings.FirstOrDefault(a => a.Value == filter);

            if (applicationSettingToDelete == null)
            {
                return;
            }

            _applicationSettingsService.DeleteSettings(new[] { applicationSettingToDelete.Id.GetValueOrDefault() });

            SavedFilters.Remove(filter);
        }
        private void ExecuteResetFilter()
        {
            Filter.BillingCycleDays = null;
            Filter.MinimumAmount = default(decimal);
            Filter.PatientLastNameFrom = default(string);
            Filter.PatientLastNameTo = default(string);

            Filter.StatmentCommunicationPreferences.Clear();
            Filter.TransactionStatuses.Clear();
            Filter.ServiceLocations.Clear();
            Filter.RenderingProviders.Clear();

            SelectedSavedFilter = _newStatementsFiltersFilter;
        }

        private void ExecuteLaunchFinancialSummary(int patientId)
        {
            _patientInfoViewManager.ShowPatientInfo(new PatientInfoLoadArguments
            {
                PatientId = patientId,
                TabToOpen = PatientInfoTab.FinancialData
            });
            DisplayResult.Execute();
        }

        private static bool SaveTextFile(string text)
        {
            const string extension = "txt";
            var dialog = new SaveFileDialog
            {
                DefaultExt = extension,
                Filter =
                    String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Text"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                FileManager.Instance.CommitContents(dialog.FileName, text);
                return true;
            }
            return false;
        }

        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<GenerateStatementViewModel> FilteredAggregateStatements { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<GenerateStatementViewModel> SelectedAggregateStatements { get; set; }

        [DispatcherThread]
        public virtual DataListsViewModel DataLists { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<GenerateStatementsFilter> SavedFilters { get; set; }

        public virtual FilterViewModel Filter { get; set; }

        private GenerateStatementsFilter _selectedSavedFilter;

        private GenerateStatementsFilter _newStatementsFiltersFilter;

        private ICollection<ApplicationSettingContainer<GenerateStatementsFilter>> _filterApplicationSettings;

        [DispatcherThread]
        public virtual GenerateStatementsFilter SelectedSavedFilter
        {
            get { return _selectedSavedFilter; }
            set
            {
                _selectedSavedFilter = value;

                if (value != null && value != _newStatementsFiltersFilter)
                {
                    Filter.BillingCycleDays = value.BillingCycleDays;
                    Filter.MinimumAmount = value.MinimumAmount;
                    Filter.PatientLastNameFrom = value.PatientLastNameFrom;
                    Filter.PatientLastNameTo = value.PatientLastNameTo;

                    Filter.StatmentCommunicationPreferences = DataLists.StatmentCommunicationPreferences
                        .Where(i => value.StatmentCommunicationPreferenceIds.Contains(i.Id)).ToObservableCollection();

                    Filter.TransactionStatuses = DataLists.TransactionStatuses
                        .Where(i => value.TransactionStatusIds.Contains(i.Id)).ToObservableCollection();

                    Filter.ServiceLocations = DataLists.ServiceLocations
                        .Where(i => value.ServiceLocationIds.Contains(i.Id)).ToObservableCollection();

                    Filter.RenderingProviders = DataLists.RenderingProviders
                        .Where(i => value.RenderingProvideIds.Contains(i.Id)).ToObservableCollection();
                }
            }
        }

        #region Commands
        public ICommand Load { get; protected set; }

        public ICommand SaveFilter { get; set; }

        public ICommand DeleteFilter { get; set; }

        public ICommand ResetFilter { get; set; }

        public ICommand DisplayResult { get; set; }

        public ICommand LaunchFinancialSummaryScreen { get; set; }

        public ICommand Print { get; set; }

        public ICommand Export { get; set; }

        public ICommand ForwardBillingServiceTransactions { get; set; }

        public ICommand SelectAllAggregateStatements { get; set; }

        public ICommand DeselectAllAggregateStatements { get; set; }

        public ICommand PrintList { get; set; }

        public ICommand Close { get; set; }

        #endregion
    }
}
