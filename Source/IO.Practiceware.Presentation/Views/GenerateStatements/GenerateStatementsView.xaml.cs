﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.GenerateStatements
{
    /// <summary>
    /// Interaction logic for Statements.xaml
    /// </summary>
    public partial class GenerateStatementsView
    {
        public GenerateStatementsView()
        {
            InitializeComponent();
        }
    }
    public class GenerateStatementsViewContextReference : DataContextReference
    {
        public new GenerateStatementsViewContext DataContext
        {
            get { return (GenerateStatementsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
