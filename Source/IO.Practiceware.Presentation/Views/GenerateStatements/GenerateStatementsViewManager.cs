﻿using System.Windows;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.GenerateStatements
{
    public class GenerateStatementsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public GenerateStatementsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowStatements()
        {
            if (!PermissionId.GenerateStatements.EnsurePermission())
            {
                return;
            }

            var view = new GenerateStatementsView();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                          {
                                              Content = view,
                                              WindowState = WindowState.Normal,
                                              ResizeMode = ResizeMode.CanResizeWithGrip
                                          });
        }
    }
}
