﻿using System.Collections.Generic;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.ClinicalInfo;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientInfo;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class ReconcileMedicationAllergiesViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        readonly IClinicalInfoViewService _clinicalInfoViewService;
        readonly IInteractionManager _interactionManager;

        public ReconcileMedicationAllergiesViewContext(
            IClinicalInfoViewService clinicalInfoViewService,
            ICommonViewFeatureExchange exchange,
            IInteractionManager interactionManager)
        {
            _clinicalInfoViewService = clinicalInfoViewService;
            _interactionManager = interactionManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);

            LoadImportCdaAllergies = Command
                .Create(ExecuteLoadImportCdaAllergies,
                () => PatientInfoViewModel.PatientId.HasValue
                    && SelectedCdaForImport != null
                    && PermissionId.CanReconcileClinical.EnsurePermission(false))
                .Async(() => InteractionContext);
            Merge = Command.Create(ExecuteMerge,
                () => PatientInfoViewModel.PatientId.HasValue
                    && SelectedImportCdaAllergies != null
                    && SelectedImportCdaAllergies.Count > 0
                    && PermissionId.CanReconcileClinical.EnsurePermission(false)).Async(() => InteractionContext);
            ImportFromCda = Command.Create(ExecuteImportFromCda,
                () => PatientInfoViewModel.PatientId.HasValue
                    && PermissionId.CanReconcileClinical.EnsurePermission(false));

            SelectedImportCdaAllergies = new ExtendedObservableCollection<PatientMedicationAllergyViewModel>();

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void OnRefresh()
        {
            ExecuteLoad();
        }

        void ExecuteLoad()
        {
            if (!PatientInfoViewModel.PatientId.HasValue) return;

            var allergies = _clinicalInfoViewService.LoadPatientMedicationAllergies(PatientInfoViewModel.PatientId.Value);

            MedicationAllergies = allergies.ToExtendedObservableCollection();
            // Nothing selected by default
            SelectedCdaForImport = null;
        }

        void ExecuteLoadImportCdaAllergies()
        {
            if (!PatientInfoViewModel.PatientId.HasValue 
                || SelectedCdaForImport == null
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            var cdaAllergies = _clinicalInfoViewService.LoadMedicationAllergiesFromCda(SelectedCdaForImport);

            ImportCdaAllergies = cdaAllergies.ToExtendedObservableCollection();
        }

        void ExecuteMerge()
        {
            if (!PatientInfoViewModel.PatientId.HasValue
                || SelectedImportCdaAllergies == null 
                || SelectedImportCdaAllergies.Count <= 0
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            if (!_clinicalInfoViewService.HasDischargedAppointments(PatientInfoViewModel.PatientId.Value))
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("The patient must have a discharged encounter in order to reconcile medication allergies"));
                return;
            }
            // Add selected problems in CDA to general list of allergies
            MedicationAllergies.AddRangeWithSinglePropertyChangedNotification(SelectedImportCdaAllergies);

            // Save updated allergies list
            _clinicalInfoViewService.SavePatientMedicationAllergies(MedicationAllergies);
            
            ImportCdaAllergies.RemoveRangeWithSinglePropertyChangedNotification(SelectedImportCdaAllergies);
           
            //Reload allergies after save
            MedicationAllergies = _clinicalInfoViewService.LoadPatientMedicationAllergies(PatientInfoViewModel.PatientId.Value).ToExtendedObservableCollection();

        }

        void ExecuteImportFromCda()
        {
            if (!PatientInfoViewModel.PatientId.HasValue
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            // Initialize selection view
            var view = new ImportClinicalSelectionView();
            var dataContext = view.DataContext.EnsureType<ImportClinicalSelectionViewContext>();
            dataContext.PatientId = PatientInfoViewModel.PatientId.Value;

            var interactionArguments = new WindowInteractionArguments
            {
                Content = view,
                IsModal = true
            };
            _interactionManager.Show(interactionArguments);

            // If completed successfully -> read selected file
            if (interactionArguments.InteractionContext.DialogResult.GetValueOrDefault())
            {
                ImportCdaAllergies = null;
                SelectedImportCdaAllergies.Clear();
                SelectedCdaForImport = dataContext.SelectedCdaImportFile;

                // Load the list of allergies from that file
                LoadImportCdaAllergies.Execute();
            }
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            return null;   
        }

        public ICommand Load { get; set; }

        public ICommand LoadImportCdaAllergies { get; protected set; }

        public ICommand Merge { get; protected set; }

        public ICommand ImportFromCda { get; protected set; }

        [DispatcherThread]
        public virtual IList<PatientMedicationAllergyViewModel> MedicationAllergies { get; set; }

        [DispatcherThread]
        public virtual ImportedCdaViewModel SelectedCdaForImport { get; set; }

        [DispatcherThread]
        public virtual IList<PatientMedicationAllergyViewModel> ImportCdaAllergies { get; set; }

        [DispatcherThread]
        public virtual IList<PatientMedicationAllergyViewModel> SelectedImportCdaAllergies { get; set; } 

        public IInteractionContext InteractionContext { get; set; }

        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        public void SetAlertAndSettingsViewFeatures(ICommonViewFeatureExchange viewFeatures)
        {
            // Common view features are not supported
            viewFeatures.OpenSettings = null;
            viewFeatures.ShowPendingAlert = null;
        }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }
}
