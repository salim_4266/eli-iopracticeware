﻿using System.Collections.Generic;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.ClinicalInfo;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientInfo;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class ReconcilePatientProblemsViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        readonly IClinicalInfoViewService _clinicalInfoViewService;
        readonly IInteractionManager _interactionManager;

        public ReconcilePatientProblemsViewContext(
            IClinicalInfoViewService clinicalInfoViewService,
            ICommonViewFeatureExchange exchange,
            IInteractionManager interactionManager)
        {
            _clinicalInfoViewService = clinicalInfoViewService;
            _interactionManager = interactionManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);

            LoadImportCdaProblems = Command
                .Create(ExecuteLoadImportCdaProblems,
                () => PatientInfoViewModel.PatientId.HasValue
                    && SelectedCdaForImport != null
                    && PermissionId.CanReconcileClinical.EnsurePermission(false))
                .Async(() => InteractionContext);
            Merge = Command.Create(ExecuteMerge,
                () => PatientInfoViewModel.PatientId.HasValue
                    && SelectedImportCdaProblems != null
                    && SelectedImportCdaProblems.Count > 0
                    && PermissionId.CanReconcileClinical.EnsurePermission(false))
                    .Async(() => InteractionContext); 
            ImportFromCda = Command.Create(ExecuteImportFromCda,
                () => PatientInfoViewModel.PatientId.HasValue
                    && PermissionId.CanReconcileClinical.EnsurePermission(false));

            SelectedImportCdaProblems = new ExtendedObservableCollection<PatientProblemViewModel>();

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void OnRefresh()
        {
            ExecuteLoad();
        }

        void ExecuteLoad()
        {
            if (!PatientInfoViewModel.PatientId.HasValue 
                || !PermissionId.CanViewProblemList.EnsurePermission()) return;

            var currentProblems = _clinicalInfoViewService.LoadPatientProblemsList(PatientInfoViewModel.PatientId.Value);

            Problems = currentProblems.ToExtendedObservableCollection();
            // Nothing selected by default
            SelectedCdaForImport = null;
        }

        void ExecuteLoadImportCdaProblems()
        {
            if (!PatientInfoViewModel.PatientId.HasValue
                || SelectedCdaForImport == null
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            var cdaProblems = _clinicalInfoViewService.LoadProblemsFromCda(SelectedCdaForImport);

            ImportCdaProblems = cdaProblems.ToExtendedObservableCollection();
        }

        void ExecuteMerge()
        {
            if (!PatientInfoViewModel.PatientId.HasValue
                || SelectedImportCdaProblems == null
                || SelectedImportCdaProblems.Count <= 0
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            // Add selected problems in CDA to general list of problems
            Problems.AddRangeWithSinglePropertyChangedNotification(SelectedImportCdaProblems);

            // Save updated problems list
            _clinicalInfoViewService.SavePatientProblems(Problems);
            
            ImportCdaProblems.RemoveRangeWithSinglePropertyChangedNotification(SelectedImportCdaProblems);

            //Reload problem list after save
            Problems = _clinicalInfoViewService.LoadPatientProblemsList(PatientInfoViewModel.PatientId.Value).ToExtendedObservableCollection();
        }

        void ExecuteImportFromCda()
        {
            if (!PatientInfoViewModel.PatientId.HasValue
                || !PermissionId.CanReconcileClinical.EnsurePermission()) return;

            // Initialize selection view
            var view = new ImportClinicalSelectionView();
            var dataContext = view.DataContext.EnsureType<ImportClinicalSelectionViewContext>();
            dataContext.PatientId = PatientInfoViewModel.PatientId.Value;

            var interactionArguments = new WindowInteractionArguments
            {
                Content = view,
                IsModal = true
            };
            _interactionManager.Show(interactionArguments);

            // If completed successfully -> read selected file
            if (interactionArguments.InteractionContext.DialogResult.GetValueOrDefault())
            {
                ImportCdaProblems = null;
                SelectedImportCdaProblems.Clear();
                SelectedCdaForImport = dataContext.SelectedCdaImportFile;

                // Load the list of problems from that file
                LoadImportCdaProblems.Execute();
            }
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            return null;
        }

        public ICommand Load { get; set; }

        public ICommand LoadImportCdaProblems { get; protected set; }

        public ICommand Merge { get; protected set; }

        public ICommand ImportFromCda { get; protected set; }

        [DispatcherThread]
        public virtual IList<PatientProblemViewModel> Problems { get; set; }

        [DispatcherThread]
        public virtual ImportedCdaViewModel SelectedCdaForImport { get; set; }

        [DispatcherThread]
        public virtual IList<PatientProblemViewModel> ImportCdaProblems { get; set; }

        [DispatcherThread]
        public virtual IList<PatientProblemViewModel> SelectedImportCdaProblems { get; set; } 

        public IInteractionContext InteractionContext { get; set; }

        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        public void SetAlertAndSettingsViewFeatures(ICommonViewFeatureExchange viewFeatures)
        {
            // Common view features are not supported
            viewFeatures.OpenSettings = null;
            viewFeatures.ShowPendingAlert = null;
        }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }
}
