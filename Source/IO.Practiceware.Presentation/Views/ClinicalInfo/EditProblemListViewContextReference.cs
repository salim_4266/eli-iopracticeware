﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class EditProblemListViewContextReference : DataContextReference
    {
        public new EditProblemListViewContext DataContext
        {
            get { return (EditProblemListViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
