﻿using System.Collections.Generic;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewServices.ClinicalInfo;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class ImportClinicalSelectionViewContext : IViewContext
    {
        readonly IClinicalInfoViewService _clinicalInfoViewService;

        public ImportClinicalSelectionViewContext(
            IClinicalInfoViewService clinicalInfoViewService)
        {
            _clinicalInfoViewService = clinicalInfoViewService;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Cancel = Command.Create(() => InteractionContext.Complete(false));
            Import = Command.Create(() => InteractionContext.Complete(true));
        }

        void ExecuteLoad()
        {
            var fileList = _clinicalInfoViewService.LoadImportedCdaList(PatientId);

            PatientImportedCdaFiles = fileList.ToExtendedObservableCollection();
        }

        public ICommand Load { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public ICommand Import { get; protected set; }

        [DispatcherThread]
        public virtual IList<ImportedCdaViewModel> PatientImportedCdaFiles { get; set; }

        [DispatcherThread]
        public virtual ImportedCdaViewModel SelectedCdaImportFile { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public int PatientId { get; set; }
    }
}
