﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.ClinicalInfo;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientInfo;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class EditProblemListViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        readonly IClinicalInfoViewService _clinicalInfoViewService;
        readonly Func<NamedViewModel> _createNamedViewModel;
        readonly Func<PatientProblemViewModel> _newPatientProblemViewModel;
        readonly IInteractionManager _interactionManager;

        public EditProblemListViewContext(
            IClinicalInfoViewService clinicalInfoViewService,
            Func<NamedViewModel> createNamedViewModel,
            ICommonViewFeatureExchange exchange,
            IInteractionManager interactionManager, 
            Func<PatientProblemViewModel> newPatientProblemViewModel)
        {
            _clinicalInfoViewService = clinicalInfoViewService;
            _createNamedViewModel = createNamedViewModel;
            _interactionManager = interactionManager;
            _newPatientProblemViewModel = newPatientProblemViewModel;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            AddProblem = Command.Create(ExecuteAddProblem,
                () => PatientInfoViewModel.PatientId.HasValue && PermissionId.CanEditProblemList.EnsurePermission(false));
            Save = Command
                .Create(ExecuteSave,
                () => PatientInfoViewModel.PatientId.HasValue && PermissionId.CanEditProblemList.EnsurePermission(false))
                .Async(() => InteractionContext);

            // Delay by 400ms to prevent searching too quickly
            LookupClinicalProblemNames = Command.Create<string>(ExecuteLookupClinicalProblemNames).Async().Delay(400);

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void OnRefresh()
        {
            // Skip if no changes
            if (Problems.CastTo<IChangeTracking>().IsChanged)
            {
                bool? isRefreshConfirmed = false;
                this.Dispatcher().Invoke(() =>
                {
                    isRefreshConfirmed = _interactionManager.Confirm("You have unsaved changes. Are you sure you want to refresh?", "Cancel", "Refresh");
                });

                if ((bool)isRefreshConfirmed)
                {
                    Load.Execute();
                }
            }
            else Load.Execute();
        }


        void ExecuteLoad()
        {
            if (!PatientInfoViewModel.PatientId.HasValue || !PermissionId.CanViewProblemList.EnsurePermission()) return;

            var loadInfo = _clinicalInfoViewService.LoadEditPatientProblemsList(PatientInfoViewModel.PatientId.Value);

            Problems = loadInfo.Problems.ToExtendedObservableCollection();
            Values = loadInfo;
        }

        void ExecuteAddProblem()
        {
            if (!PatientInfoViewModel.PatientId.HasValue || !PermissionId.CanEditProblemList.EnsurePermission()) return;

            // Create problem
            var newProblem = _newPatientProblemViewModel();
            newProblem.Id = null;
            newProblem.PatientId = PatientInfoViewModel.PatientId.Value;
            newProblem.IsDeactivated = false;

            // Set audit information to display in the grid
            newProblem.LoggedBy = _createNamedViewModel().Modify(nm =>
                {
                    nm.Id = UserContext.Current.UserDetails.Id;
                    nm.Name = UserContext.Current.UserDetails.DisplayName;
                });
            newProblem.LoggedDate = DateTime.Now;
            newProblem.LastModifiedBy = _createNamedViewModel().Modify(nm =>
            {
                nm.Id = UserContext.Current.UserDetails.Id;
                nm.Name = UserContext.Current.UserDetails.DisplayName;
            });
            newProblem.LastModifiedDate = DateTime.Now;

            // Add it to the list of problems
            Problems.Add(newProblem);
        }

        void ExecuteSave()
        {
            // Check permission
            if (!PatientInfoViewModel.PatientId.HasValue || !PermissionId.CanEditProblemList.EnsurePermission()) return;

            // Skip if no changes
            if (!Problems.CastTo<IChangeTracking>().IsChanged) return;

            // Save
            _clinicalInfoViewService.SavePatientProblems(Problems);

            // Reload
            var problems = _clinicalInfoViewService.LoadPatientProblemsList(PatientInfoViewModel.PatientId.Value);
            Problems = problems.ToExtendedObservableCollection();
        }

        void ExecuteLookupClinicalProblemNames(string searchText)
        {
            var names = _clinicalInfoViewService.LookupClinicalProblemNames(searchText);

            SuggestedClinicalProblemNames = names.ToExtendedObservableCollection();
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            return null;
        }

        public ICommand Load { get; set; }

        public ICommand AddProblem { get; protected set; }

        public ICommand Save { get; protected set; }

        public ICommand LookupClinicalProblemNames { get; protected set; }

        [DispatcherThread]
        public virtual IList<PatientProblemViewModel> Problems { get; set; }

        [DispatcherThread]
        public virtual PatientProblemsListLoadInformation Values { get; set; }

        [DispatcherThread]
        public virtual IList<NamedViewModel> SuggestedClinicalProblemNames { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        public void SetAlertAndSettingsViewFeatures(ICommonViewFeatureExchange viewFeatures)
        {
            // Common view features are not supported
            viewFeatures.OpenSettings = null;
            viewFeatures.ShowPendingAlert = null;
        }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }
}
