﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.ClinicalInfo
{
    public class ReconcilePatientProblemsViewContextReference : DataContextReference
    {
        public new ReconcilePatientProblemsViewContext DataContext
        {
            get { return (ReconcilePatientProblemsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
