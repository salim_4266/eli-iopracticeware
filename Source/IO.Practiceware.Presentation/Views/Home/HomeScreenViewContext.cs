﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Setup;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenViewContext : IViewContext, ICommonViewFeatureHandler
    {
        readonly ApplicationSetupViewManager _applicationSetupViewManager;

        public HomeScreenViewContext(ICommonViewFeatureExchange exchange, ApplicationSetupViewManager applicationSetupViewManager)
        {
            _applicationSetupViewManager = applicationSetupViewManager;

            // By default we are set to close app
            CloseAction = HomeScreenCloseAction.Exit;

            ShowOldHome = Command.Create(ExecuteShowOldHome);

            // Report capabilities
            ViewFeatureExchange = exchange;
            ViewFeatureExchange.OpenSettings = ExecuteOpenSettings;
            ViewFeatureExchange.ShowPendingAlert = null;
        }

        void ExecuteShowOldHome()
        {
            CloseAction = HomeScreenCloseAction.ShowOldHome;
            InteractionContext.Complete(true);
        }

        void ExecuteOpenSettings()
        {
            _applicationSetupViewManager.ShowApplicationSetup();
        }

        public virtual ICommand ShowOldHome { get; set; }

        /// <summary>
        /// Return result of home screen
        /// </summary>
        public HomeScreenCloseAction CloseAction { get; set; }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; set; }

        public virtual IInteractionContext InteractionContext { get; set; }
    }
}
