﻿using System.Windows;
using Soaf;
using Soaf.Presentation;
using WindowStartupLocation = Soaf.Presentation.WindowStartupLocation;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public HomeScreenViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        private HomeScreenViewContext _viewContext;
        public HomeScreenCloseAction ShowHome()
        {
            var view = new HomeScreenView();

            _viewContext = view.DataContext.EnsureType<HomeScreenViewContext>();

            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Normal,
                                                  ResizeMode = ResizeMode.CanResizeWithGrip,
                                                  Header = "Home",
                                                  WindowStartupLocation = WindowStartupLocation.CenterScreen
                                              });
            return _viewContext.CloseAction;
        }

        public void CloseHome()
        {
            if (_viewContext != null)
            {
                _viewContext.InteractionContext.Complete(true);
                _viewContext.CloseAction = HomeScreenCloseAction.ShowOldHome;
            }
        }
    }

    /// <summary>
    ///   States what happens when Home screen closes
    /// </summary>
    public enum HomeScreenCloseAction
    {
        Exit = 0,
        ShowOldHome = 1
    }
}