using IO.Practiceware.Presentation.ViewModels.Home;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class WidgetInContainerContextReference : DataContextReference
    {
        public new WidgetInContainerContext DataContext
        {
            get { return (WidgetInContainerContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}