﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.PatientSearch;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class PatientSearchWidgetContext : IViewContext, IWidgetViewModelAware
    {
        readonly PatientSearchViewManager _patientSearchViewManager;
        readonly PatientInfoViewManager _patientInfoViewManager;
        UserPatientSearchWidgetSettings _currentSettings;

        public PatientSearchWidgetContext(PatientSearchViewManager patientSearchViewManager,
            PatientInfoViewManager patientInfoViewManager)
        {
            _patientSearchViewManager = patientSearchViewManager;
            _patientInfoViewManager = patientInfoViewManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Search = Command
                .Create(ExecuteSearch)
                .Async(() => InteractionContext);
        }

        void ExecuteLoad()
        {
            InteractionContext.Completing += OnInteractionContextCompleting;

            // Load settings
            _currentSettings = WidgetInstance.CustomWidgetData as UserPatientSearchWidgetSettings
                               ?? new UserPatientSearchWidgetSettings
                                   {
                                       PatientInfoTabToOpen = PatientInfoTab.Demographics
                                   };
            WidgetInstance.CustomWidgetData = _currentSettings;

            // Set settings
            PatientTabToOpen = _currentSettings.PatientInfoTabToOpen;
        }

        void ExecuteSearch()
        {
            var searchText = SearchText;
            SearchText = string.Empty;

            int? patientId;
            if (_patientSearchViewManager.SearchPatient(searchText, true, out patientId))
            {
                ShowPatient(patientId);
            }
        }

        private void ShowPatient(int? patientId)
        {
            this.Dispatcher().BeginInvoke(() => _patientInfoViewManager
                .ShowPatientInfo(new PatientInfoLoadArguments
                {
                    PatientId = patientId,
                    TabToOpen = PatientTabToOpen
                }));
        }

        void OnInteractionContextCompleting(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Save user selected tab to open
            _currentSettings.PatientInfoTabToOpen = PatientTabToOpen;
        }

        /// <summary>
        /// Entered search text
        /// </summary>
        [DispatcherThread]
        public virtual string SearchText { get; set; }

        /// <summary>
        /// Gets or sets the patient tab to open.
        /// </summary>
        /// <value>
        /// The patient tab to open.
        /// </value>
        public virtual PatientInfoTab PatientTabToOpen { get; set; }

        /// <summary>
        /// Performs load
        /// </summary>
        public virtual ICommand Load { get; set; }

        /// <summary>
        /// Performs actual search
        /// </summary>
        public virtual ICommand Search { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public WidgetViewModel WidgetInstance { get; set; }
    }
}
