﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Services.ApplicationSettings;
using System;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class FeatureAccessWidgetContext : FeatureSelectionAndAccessContext, IWidgetViewModelAware
    {
        public FeatureAccessWidgetContext(
            Func<ApplicationFeatureGroupViewModel> createGroup, Func<ApplicationFeatureViewModel> createFeature, 
            IExternalFeatureIntegrationManager externalFeatureIntegrationManager,
            IApplicationSettingsService applicationViewService)
            : base(createGroup, createFeature, externalFeatureIntegrationManager, applicationViewService)
        {

        }

        public virtual WidgetViewModel WidgetInstance { get; set; }

        protected override UserFeatureSelectionSettings GetSettings()
        {
            var currentSettings = WidgetInstance.CustomWidgetData as UserFeatureSelectionSettings
                ?? new UserFeatureSelectionSettings();
            WidgetInstance.CustomWidgetData = currentSettings;
            return currentSettings;
        }

        protected override void SaveSettings(UserFeatureSelectionSettings settings)
        {
            WidgetInstance.CustomWidgetData = settings;
        }
    }
}
