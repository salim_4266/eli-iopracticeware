﻿using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.Views.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class PaymentsLookupWidgetContext : IViewContext
    {
        private readonly IPostAdjustmentsViewService _viewService;
        private readonly PostAdjustmentsViewManager _viewManager;

        public PaymentsLookupWidgetContext(IPostAdjustmentsViewService viewService,
            PostAdjustmentsViewManager viewManager)
        {
            _viewService = viewService;
            _viewManager = viewManager;
            OpenBatch = Command.Create<PaymentInstrumentViewModel>(ExecuteOpenBatch);
            SearchBatch = Command.Create(ExecuteSearchBatch, () => !Reference.IsNullOrEmpty()).Async(() => InteractionContext);
        }

        private void ExecuteSearchBatch()
        {
            if (Reference.IsNullOrEmpty()) return;

            FoundBatches = _viewService.SearchPaymentInstruments(Reference).ToExtendedObservableCollection();
            if (!FoundBatches.Any())
            {
                MessageToUser = "No financial batches found with reference: " + Reference;
            }
        }

        private void ExecuteOpenBatch(PaymentInstrumentViewModel batch)
        {
            _viewManager.ShowPostPaymentsAndAdjustments(new PostAdjustmentsLoadArguments
            {
                PaymentInstrumentId = batch.Id
            });
        }

        public virtual string Reference { get; set; }

        [DispatcherThread]
        public virtual ExtendedObservableCollection<PaymentInstrumentViewModel> FoundBatches { get; set; }

        [DispatcherThread]
        public virtual string MessageToUser { get; set; }

        public virtual ICommand SearchBatch { get; set; }

        public virtual ICommand OpenBatch { get; set; }

        public IInteractionContext InteractionContext { get; set; }
    }
}