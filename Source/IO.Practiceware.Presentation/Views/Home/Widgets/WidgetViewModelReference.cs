using IO.Practiceware.Presentation.ViewModels.Home;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class WidgetViewModelReference : DataContextReference
    {
        public new WidgetViewModel DataContext
        {
            get { return (WidgetViewModel)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}