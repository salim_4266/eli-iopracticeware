﻿using IO.Practiceware.Presentation.ViewModels.Home;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class WidgetRepresentationSelector : DataTemplateSelector
    {
        public Dictionary<string, Template> TemplateMapping { get; set; }

        public WidgetRepresentationSelector()
        {
            TemplateMapping = new Dictionary<string, Template>();
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Template result;
            var widgetViewModel = item as WidgetViewModel;
            if (widgetViewModel == null || !TemplateMapping.TryGetValue(widgetViewModel.WidgetTemplateKey, out result))
            {
                return base.SelectTemplate(item, container);
            }

            return result.DataTemplate;
        }
    }

    /// <summary>
    /// Provides a link between a value and a <see cref="DataTemplate"/>
    /// for the <see cref="WidgetRepresentationSelector"/>
    /// </summary>
    /// <remarks>
    /// In this case, our value is a <see cref="System.Type"/> which we are attempting to match
    /// to a <see cref="DataTemplate"/>
    /// </remarks>
    public class Template : DependencyObject
    {
        /// <summary>
        /// Provides the <see cref="DataTemplate"/> used to render items
        /// </summary>
        public static readonly DependencyProperty DataTemplateProperty =
           DependencyProperty.Register("DataTemplate", typeof(DataTemplate), typeof(Template));

        /// <summary>
        /// Gets or Sets the <see cref="DataTemplate"/> used to render items
        /// </summary>
        public DataTemplate DataTemplate
        { get { return (DataTemplate)GetValue(DataTemplateProperty); } set { SetValue(DataTemplateProperty, value); } }
    }
}
