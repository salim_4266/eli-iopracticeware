﻿using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims;
using IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class ManageClaimsWidgetContext : IViewContext
    {
        private readonly IManageClaimsViewService _manageClaimsViewService;

        public ManageClaimsWidgetContext(IManageClaimsViewService manageClaimsViewService)
        {
            _manageClaimsViewService = manageClaimsViewService;
            Load = Command.Create(ExecuteLoad).Async();
        }

        [DispatcherThread]
        public virtual ClaimsViewModel PendingClaims { get; set; }

        [DispatcherThread]
        public virtual ClaimsViewModel SentClaims { get; set; }

        [DispatcherThread]
        public virtual ClaimsViewModel UnbilledClaims { get; set; }

        /// <summary>
        ///     Performs load
        /// </summary>
        public virtual ICommand Load { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        private void ExecuteLoad()
        {
            ManageClaimsViewModel loadInformation = _manageClaimsViewService.LoadClaimsInformation();
            PendingClaims = loadInformation.PendingClaims;
            SentClaims = loadInformation.SentClaims;
            UnbilledClaims = loadInformation.UnbilledClaims;
        }
    }
}