using IO.Practiceware.Presentation.ViewModels.Home;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class WidgetsContainerViewModelReference : DataContextReference
    {
        public new WidgetsContainerViewModel DataContext
        {
            get { return (WidgetsContainerViewModel)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}