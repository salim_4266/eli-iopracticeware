﻿using IO.Practiceware.Presentation.ViewModels.Home;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    /// <summary>
    /// Allows widget view context to gain access to <see cref="WidgetViewModel"/> instance
    /// </summary>
    public interface IWidgetViewModelAware
    {
        WidgetViewModel WidgetInstance { get; set; }
    }
}
