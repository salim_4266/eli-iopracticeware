﻿using System;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.ViewServices.Home;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Home.Widgets
{
    public class WelcomeWidgetContext : IViewContext
    {
        private readonly ITimeSheetViewService _timeSheetViewService;
        private readonly Func<TimeSheetTimeBlockViewModel> _createTimeSheetTimeBlockViewModel;
        
        public WelcomeWidgetContext(ITimeSheetViewService timeSheetViewService, Func<TimeSheetTimeBlockViewModel> createTimeSheetTimeBlockViewModel)
        {
            _timeSheetViewService = timeSheetViewService;
           
            _createTimeSheetTimeBlockViewModel = createTimeSheetTimeBlockViewModel;

            FillTimeSheet = Command.Create(ExecuteFillTimeSheet).Async();
            Load = Command.Create(ExecuteLoad).Async();            
        }

        private void ExecuteFillTimeSheet()
        {
            var o = Common.Controls.Window.obj;

            o.Invoke((Action)(() => {
                Common.Controls.Window.IsNotBzy = false;
            }));
            IsBusy = true;
            ClockInOutTime.Id = _timeSheetViewService.UpdateTimesheet(ClockInOutTime);
            o.Invoke((Action)(() => {
                Common.Controls.Window.IsNotBzy = true;
            }));
            IsBusy = false;
        }

        private void ExecuteLoad()
        {
            ClockInOutTime = _createTimeSheetTimeBlockViewModel();
            ClockInOutTime.Id = _timeSheetViewService.IsClockedIn();

            if (ClockInOutTime.Id != null)
            {
                ClockInOutTime.IsBreak = true;
            }
        }

        [DispatcherThread]
        public virtual TimeSheetTimeBlockViewModel ClockInOutTime { get; set; }
        [DispatcherThread]
        public virtual bool IsBusy { get; set; }


        /// <summary>
        /// Fills in User clock In and Out time
        /// </summary>
        public virtual ICommand FillTimeSheet { get; set; }
        public virtual ICommand Load { get; set; }

        public IInteractionContext InteractionContext { get; set; }

    }
}