﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System.Collections.ObjectModel;
using System.Windows;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class ToolbarMoreItemsFeatureDragHandler : Freezable, IElementDragHandler
    {
        public static readonly DependencyProperty DragVisualTemplateProperty =
            DependencyProperty.Register("DragVisualTemplate", typeof(DataTemplate), typeof(ToolbarMoreItemsFeatureDragHandler), new PropertyMetadata(default(DataTemplate)));

        public static readonly DependencyProperty FeatureGroupsCollectionProperty =
            DependencyProperty.Register("FeatureGroupsCollection", typeof(ObservableCollection<ApplicationFeatureGroupViewModel>), typeof(ToolbarMoreItemsFeatureDragHandler), new PropertyMetadata(null));

        public DataTemplate DragVisualTemplate
        {
            get { return (DataTemplate)GetValue(DragVisualTemplateProperty); }
            set { SetValue(DragVisualTemplateProperty, value); }
        }

        public DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Move; }
        }

        public void PrepareForDragging(FrameworkElement element, out object payloadData, out DataTemplate visualTemplate)
        {
            var feature = (ApplicationFeatureViewModel)element.DataContext;

            // Record that feature is being dragged
            feature.IsDragging = true;

            visualTemplate = DragVisualTemplate;
            payloadData = feature;
        }

        public void ProcessDragCompleted(FrameworkElement element, bool dropSucceeded, object payloadData,
                                         DragDropCompletedEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            // We are no longer dragging it
            feature.IsDragging = false;
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new System.NotImplementedException();
        }
    }
}