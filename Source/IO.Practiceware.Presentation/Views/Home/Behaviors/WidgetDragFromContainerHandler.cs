﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System.Windows;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class WidgetDragFromContainerHandler : Freezable, IElementDragHandler
    {
        public static readonly DependencyProperty DragVisualTemplateProperty =
            DependencyProperty.Register("DragVisualTemplate", typeof(DataTemplate), typeof(WidgetDragFromContainerHandler), new PropertyMetadata(default(DataTemplate)));

        public static readonly DependencyProperty WidgetsContainerProperty =
            DependencyProperty.Register("WidgetsContainer", typeof(WidgetsContainerViewModel), typeof(WidgetDragFromContainerHandler), new PropertyMetadata(null));

        public WidgetsContainerViewModel WidgetsContainer
        {
            get { return (WidgetsContainerViewModel)GetValue(WidgetsContainerProperty); }
            set { SetValue(WidgetsContainerProperty, value); }
        }

        public DataTemplate DragVisualTemplate
        {
            get { return (DataTemplate)GetValue(DragVisualTemplateProperty); }
            set { SetValue(DragVisualTemplateProperty, value); }
        }

        public DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Move; }
        }

        public void PrepareForDragging(FrameworkElement element, out object payloadData, out DataTemplate visualTemplate)
        {
            var widget = (WidgetViewModel)element.DataContext;

            // Record that feature is being dragged
            widget.IsDragging = true;

            visualTemplate = DragVisualTemplate;
            payloadData = widget;
        }

        public void ProcessDragCompleted(FrameworkElement element, bool dropSucceeded, object payloadData,
                                         DragDropCompletedEventArgs args)
        {
            var widget = (WidgetViewModel)payloadData;

            // We are no longer dragging it
            widget.IsDragging = false;

            // If it changed owner -> ensure it is no longer in our container
            if (WidgetsContainer != widget.OwnerContainer)
            {
                WidgetsContainer.Widgets.Remove(widget);
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new System.NotImplementedException();
        }
    }
}
