﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System;
using System.Windows;
using System.Windows.Controls;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class WidgetDropInTargetContainerHandler : Freezable, IElementDropTargetHandler
    {
        public static readonly DependencyProperty WidgetsContainerProperty =
            DependencyProperty.Register("WidgetsContainer", typeof(WidgetsContainerViewModel), typeof(WidgetDropInTargetContainerHandler), new PropertyMetadata(null));

        /// <summary>
        /// Attached property which allows setting ItemsPanel property on the handler
        /// </summary>
        public static readonly DependencyProperty PanelOwnerHandlerProperty =
            DependencyProperty.RegisterAttached("PanelOwnerHandler", typeof(WidgetDropInTargetContainerHandler), typeof(WidgetDropInTargetContainerHandler), new PropertyMetadata(null, OnPanelOwnerHandlerPropertyChanged));

        /// <summary>
        /// Panel which is used to display all the items
        /// </summary>
        public Panel ItemsPanel { get; private set; }

        /// <summary>
        /// Container which represents the panel
        /// </summary>
        public WidgetsContainerViewModel WidgetsContainer
        {
            get { return (WidgetsContainerViewModel)GetValue(WidgetsContainerProperty); }
            set { SetValue(WidgetsContainerProperty, value); }
        }

        public Type SupportedPayloadType
        {
            get { return typeof(WidgetViewModel); }
        }

        public void ProcessItemDragOver(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var widget = (WidgetViewModel)payloadData;

            var relativeCursorPositionOnPanel = args.GetPosition(ItemsPanel);
            DragDropHelpers.PositionItemInCollectionAccordingToMouseLocation(widget, WidgetsContainer.Widgets, ItemsPanel, relativeCursorPositionOnPanel);
        }

        public void ProcessItemDropped(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            // Own it now
            var widget = (WidgetViewModel)payloadData;
            widget.OwnerContainer = WidgetsContainer;

            // Add it on the board if it is not there
            if (!WidgetsContainer.Widgets.Contains(widget))
            {
                WidgetsContainer.Widgets.Add(widget);
            }
        }

        public void ProcessItemDragLeave(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var widget = (WidgetViewModel)payloadData;

            if (widget.OwnerContainer != WidgetsContainer)
            {
                WidgetsContainer.Widgets.Remove(widget);
            }
        }

        public void ProcessItemDragEnter(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var widget = (WidgetViewModel)payloadData;

            // Add it on the board if it is not there
            if (!WidgetsContainer.Widgets.Contains(widget))
            {
                WidgetsContainer.Widgets.Add(widget);
            }
        }

        static void OnPanelOwnerHandlerPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var panel = (Panel)dependencyObject;

            // Reset panel for previous handler
            var oldHandler = (WidgetDropInTargetContainerHandler)args.OldValue;
            if (oldHandler != null)
            {
                oldHandler.ItemsPanel = null;
            }

            // Update for new one
            var newHandler = (WidgetDropInTargetContainerHandler)args.NewValue;
            if (newHandler != null)
            {
                newHandler.ItemsPanel = panel;
            }
        }

        public static void SetPanelOwnerHandler(UIElement element, WidgetDropInTargetContainerHandler value)
        {
            element.SetValue(PanelOwnerHandlerProperty, value);
        }

        public static WidgetDropInTargetContainerHandler GetPanelOwnerHandler(UIElement element)
        {
            return (WidgetDropInTargetContainerHandler)element.GetValue(PanelOwnerHandlerProperty);
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }
}