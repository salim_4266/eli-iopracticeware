﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class ToolbarDropTargetHandler : Freezable, IElementDropTargetHandler
    {
        public static readonly DependencyProperty ItemsPanelCollectionProperty =
            DependencyProperty.Register("ItemsPanelCollection", typeof(ObservableCollection<ApplicationFeatureViewModel>), typeof(ToolbarDropTargetHandler), new PropertyMetadata(null));

        /// <summary>
        /// Attached property which allows setting ItemsPanel property on the handler
        /// </summary>
        public static readonly DependencyProperty PanelOwnerHandlerProperty =
            DependencyProperty.RegisterAttached("PanelOwnerHandler", typeof (ToolbarDropTargetHandler), typeof (ToolbarDropTargetHandler), new PropertyMetadata(null, OnPanelOwnerHandlerPropertyChanged));

        /// <summary>
        /// Panel which is used to display all the items
        /// </summary>
        public Panel ItemsPanel { get; private set; }

        /// <summary>
        /// Collection which is mapped to panel
        /// </summary>
        public ObservableCollection<ApplicationFeatureViewModel> ItemsPanelCollection
        {
            get { return (ObservableCollection<ApplicationFeatureViewModel>)GetValue(ItemsPanelCollectionProperty); }
            set { SetValue(ItemsPanelCollectionProperty, value); }
        }

        public Type SupportedPayloadType 
        {
            get { return typeof (ApplicationFeatureViewModel); }
        }

        public void ProcessItemDragOver(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            var relativeCursorPositionOnPanel = args.GetPosition(ItemsPanel);
            DragDropHelpers.PositionItemInCollectionAccordingToMouseLocation(feature, ItemsPanelCollection, ItemsPanel, relativeCursorPositionOnPanel, false);
        }

        public void ProcessItemDropped(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            // Feature is now pinned
            feature.IsPinned = true;

            // It should already be on the toolbar, so no need to re-add it
        }

        public void ProcessItemDragLeave(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            // If feature is not yet pinned -> remove it from toolbar when dragging away
            if (!feature.IsPinned)
            {
                ItemsPanelCollection.Remove(feature);    
            }
        }

        public void ProcessItemDragEnter(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            // Add it on the board if it is not there
            if (!ItemsPanelCollection.Contains(feature))
            {
                ItemsPanelCollection.Add(feature);
            }
        }

        static void OnPanelOwnerHandlerPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var panel = (Panel)dependencyObject;

            // Reset panel for previous handler
            var oldHandler = (ToolbarDropTargetHandler)args.OldValue;
            if (oldHandler != null)
            {
                oldHandler.ItemsPanel = null;
            }

            // Update for new one
            var newHandler = (ToolbarDropTargetHandler)args.NewValue;
            if (newHandler != null)
            {
                newHandler.ItemsPanel = panel;
            }
        }

        public static void SetPanelOwnerHandler(UIElement element, ToolbarDropTargetHandler value)
        {
            element.SetValue(PanelOwnerHandlerProperty, value);
        }

        public static ToolbarDropTargetHandler GetPanelOwnerHandler(UIElement element)
        {
            return (ToolbarDropTargetHandler)element.GetValue(PanelOwnerHandlerProperty);
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }
}