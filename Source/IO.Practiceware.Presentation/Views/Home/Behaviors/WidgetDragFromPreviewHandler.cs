﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System;
using System.Windows;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class WidgetDragFromPreviewHandler : Freezable, IElementDragHandler
    {
        public static readonly DependencyProperty DragVisualTemplateProperty =
            DependencyProperty.Register("DragVisualTemplate", typeof(DataTemplate), typeof(WidgetDragFromPreviewHandler), new PropertyMetadata(default(DataTemplate)));

        public static readonly DependencyProperty CloneWidgetDelegateProperty =
            DependencyProperty.Register("CloneWidgetDelegate", typeof(Func<WidgetViewModel, WidgetViewModel>), typeof(WidgetDragFromPreviewHandler), new PropertyMetadata(default(Func<WidgetViewModel>)));

        public Func<WidgetViewModel, WidgetViewModel> CloneWidgetDelegate
        {
            get { return (Func<WidgetViewModel, WidgetViewModel>)GetValue(CloneWidgetDelegateProperty); }
            set { SetValue(CloneWidgetDelegateProperty, value); }
        }

        public DataTemplate DragVisualTemplate
        {
            get { return (DataTemplate)GetValue(DragVisualTemplateProperty); }
            set { SetValue(DragVisualTemplateProperty, value); }
        }

        public DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Copy; }
        }

        public void PrepareForDragging(FrameworkElement element, out object payloadData, out DataTemplate visualTemplate)
        {
            var widget = (WidgetViewModel)element.DataContext;

            widget = CloneWidgetDelegate(widget); // Create a copy, since we allow multiple copies of widget to be present on the panel

            // Record that feature is being dragged
            widget.IsDragging = true;

            visualTemplate = DragVisualTemplate;
            payloadData = widget;
        }

        public void ProcessDragCompleted(FrameworkElement element, bool dropSucceeded, object payloadData,
                                         DragDropCompletedEventArgs args)
        {
            var widget = (WidgetViewModel)payloadData;

            // We are no longer dragging it
            widget.IsDragging = false;
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new System.NotImplementedException();
        }
    }
}