﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class ToolbarFeatureDragHandler : Freezable, IElementDragHandler
    {
        public static readonly DependencyProperty DragVisualTemplateProperty =
            DependencyProperty.Register("DragVisualTemplate", typeof(DataTemplate), typeof(ToolbarFeatureDragHandler), new PropertyMetadata(default(DataTemplate)));


        public static readonly DependencyProperty ToolbarFeaturesCollectionProperty =
            DependencyProperty.Register("ToolbarFeaturesCollection", typeof(ObservableCollection<ApplicationFeatureViewModel>), typeof(ToolbarFeatureDragHandler), new PropertyMetadata(default(IList<ApplicationFeatureViewModel>)));

        public ObservableCollection<ApplicationFeatureViewModel> ToolbarFeaturesCollection
        {
            get { return (ObservableCollection<ApplicationFeatureViewModel>)GetValue(ToolbarFeaturesCollectionProperty); }
            set { SetValue(ToolbarFeaturesCollectionProperty, value); }
        }

        public DataTemplate DragVisualTemplate
        {
            get { return (DataTemplate)GetValue(DragVisualTemplateProperty); }
            set { SetValue(DragVisualTemplateProperty, value); }
        }

        public DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Move; }
        }

        public void PrepareForDragging(FrameworkElement element, out object payloadData, out DataTemplate visualTemplate)
        {
            var feature = (ApplicationFeatureViewModel)element.DataContext;

            // Record that feature is being dragged
            feature.IsDragging = true;

            visualTemplate = DragVisualTemplate;
            payloadData = feature;
        }

        public void ProcessDragCompleted(FrameworkElement element, bool dropSucceeded, object payloadData,
                                         DragDropCompletedEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel)payloadData;

            // We are no longer dragging it
            feature.IsDragging = false;

            // Feature is no longer pinned? Remove it then
            if (dropSucceeded && !feature.IsPinned)
            {
                ToolbarFeaturesCollection.Remove(feature);
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new System.NotImplementedException();
        }
    }
}