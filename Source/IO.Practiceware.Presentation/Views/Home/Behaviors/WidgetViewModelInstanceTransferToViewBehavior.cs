﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Home.Widgets;
using Soaf.Presentation;
using System;
using System.Windows;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class WidgetViewModelInstanceTransferToViewBehavior : Behavior<View>
    {
        /// <summary>
        /// Sets <see cref="WidgetViewModel"/> instance for widget view
        /// </summary>
        public static readonly DependencyProperty WidgetViewModelInstanceProperty =
            DependencyProperty.RegisterAttached("WidgetViewModelInstance", typeof (WidgetViewModel), typeof (WidgetViewModelInstanceTransferToViewBehavior), new PropertyMetadata(default(WidgetViewModel)));

        PropertyChangeNotifier _viewModelInstanceChangeNotifier;
        PropertyChangeNotifier _viewContextChangeNotifier;

        public static void SetWidgetViewModelInstance(UIElement element, WidgetViewModel value)
        {
            element.SetValue(WidgetViewModelInstanceProperty, value);
        }

        public static WidgetViewModel GetWidgetViewModelInstance(UIElement element)
        {
            return (WidgetViewModel) element.GetValue(WidgetViewModelInstanceProperty);
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            _viewModelInstanceChangeNotifier = new PropertyChangeNotifier(AssociatedObject, WidgetViewModelInstanceProperty);
            _viewModelInstanceChangeNotifier.AddValueChanged(OnViewModelValueChanged);

            _viewContextChangeNotifier = new PropertyChangeNotifier(AssociatedObject, View.DataContextProperty);
            _viewContextChangeNotifier.AddValueChanged(OnViewContextChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _viewModelInstanceChangeNotifier.RemoveValueChanged(OnViewModelValueChanged);
            _viewContextChangeNotifier.RemoveValueChanged(OnViewContextChanged);
        }

        void OnViewContextChanged(object sender, EventArgs e)
        {
            RefreshWidgetViewModelInstanceValue();
        }

        void OnViewModelValueChanged(object sender, EventArgs e)
        {
            RefreshWidgetViewModelInstanceValue();
        }

        void RefreshWidgetViewModelInstanceValue()
        {
            var viewContext = AssociatedObject.GetValue(View.DataContextProperty) as IWidgetViewModelAware;
            if (viewContext == null) return;

            var widgetViewModel = AssociatedObject.GetValue(WidgetViewModelInstanceProperty) as WidgetViewModel;
            
            // Do not allow resetting instance to null (occurs when window is closing)
            if (widgetViewModel == null && viewContext.WidgetInstance != null) return;

            viewContext.WidgetInstance = widgetViewModel;
        }
    }
}
