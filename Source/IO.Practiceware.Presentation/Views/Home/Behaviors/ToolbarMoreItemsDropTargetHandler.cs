﻿using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;

namespace IO.Practiceware.Presentation.Views.Home.Behaviors
{
    public class ToolbarMoreItemsDropTargetHandler : Freezable, IElementDropTargetHandler
    {
        public static readonly DependencyProperty FeatureGroupsCollectionProperty =
            DependencyProperty.Register("FeatureGroupsCollection", typeof(ObservableCollection<ApplicationFeatureGroupViewModel>), typeof(ToolbarMoreItemsDropTargetHandler), new PropertyMetadata(default(IList<ApplicationFeatureViewModel>)));

        public Type SupportedPayloadType
        {
            get { return typeof(ApplicationFeatureViewModel); }
        }

        public void ProcessItemDragOver(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {

        }

        public void ProcessItemDropped(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {
            var feature = (ApplicationFeatureViewModel) payloadData;

            // Feature is no longer pinned, since we are adding it to selection
            feature.IsPinned = false;
        }

        public void ProcessItemDragLeave(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {

        }

        public void ProcessItemDragEnter(FrameworkElement frameworkElement, object payloadData, DragEventArgs args)
        {

        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }
}