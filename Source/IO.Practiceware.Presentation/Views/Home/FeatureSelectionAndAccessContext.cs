using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Home
{
    /// <summary>
    /// Shared context for features which can be select
    /// </summary>
    public abstract class FeatureSelectionAndAccessContext : IViewContext
    {
        readonly Func<ApplicationFeatureViewModel> _createFeature;
        readonly IExternalFeatureIntegrationManager _externalFeatureIntegrationManager;
        private readonly IApplicationSettingsService _applicationViewService;
        private bool _isConsentFormActivated;       
        delegate void DelApplicationSettings(string str);

        protected FeatureSelectionAndAccessContext(
            Func<ApplicationFeatureGroupViewModel> createGroup,
            Func<ApplicationFeatureViewModel> createFeature,
            IExternalFeatureIntegrationManager externalFeatureIntegrationManager,
            IApplicationSettingsService applicationViewService)
        {
            _createFeature = createFeature;
            _externalFeatureIntegrationManager = externalFeatureIntegrationManager;
            _applicationViewService = applicationViewService;

            DelApplicationSettings obj = AppSettings;
            IAsyncResult result = obj.BeginInvoke("IsConsentFormActivated", null, null);
            obj.EndInvoke(result);

            var defaultsStorage = System.Windows.Application.Current.SafeLoadComponent<ResourceDictionary>(
                           typeof(FeatureSelectionAndAccessContext).Assembly.BuildComponentUri("/Views/Home/HomescreenDefaultsDefinition.xaml"));

            var featureSelection = ((IEnumerable)defaultsStorage["AllApplicationFeatures"])
                .OfType<ApplicationFeatureGroupViewModel>()
                .ToExtendedObservableCollection();

            // Retrieved models don't have AOP applied to them, so converting
            // AutoMap<,> fails here, need to resolve issue with it first
            featureSelection = featureSelection.Select(fg => createGroup().Modify(newGroup =>
                {
                    newGroup.Name = fg.Name;
                    newGroup.SpecialColor = fg.SpecialColor;
                    newGroup.SpecialGradientColor = fg.SpecialGradientColor;
                    newGroup.Features = fg.Features
                        .Select(CloneFeature)
                        .ToExtendedObservableCollection();
                })).ToExtendedObservableCollection();

            FeaturesSelection = featureSelection;
            PinnedFeatures = new ExtendedObservableCollection<ApplicationFeatureViewModel>();
            Loaded = Command.Create(ExecuteLoaded).Async(() => InteractionContext);
            Closing = Command.Create(ExecuteClosing);
            HandleFeaturePinned = Command.Create<ApplicationFeatureViewModel>(OnHandleFeaturePinned);
            TriggerApplicationFeature = Command.Create<ApplicationFeatureViewModel>(ExecuteTriggerApplicationFeature);
        }

        void AppSettings(string text)
        {
            var applicationService = _applicationViewService.GetSetting<bool>(text);
            IsConsentFormActivated = applicationService != null ? applicationService.Value : false;            
        }

        void ExecuteLoaded()
        {                    
            var features = FeaturesSelection.SelectMany(fs => fs.Features).ToList();

            // Read current settings
            var currentSettings = GetSettings();

            // Restore pinned state
            foreach (var featureSettings in currentSettings.Features)
            {
                var feature = features.FirstOrDefault(f => f.FeatureKey == featureSettings.FeatureTypeKey);
                if (feature != null)
                {
                    feature.IsPinned = true;
                    PinnedFeatures.Add(feature);
                }
            }
        }

        void ExecuteClosing()
        {
            // Get current settings and clear pinned features list
            var currentSettings = GetSettings();
            currentSettings.Features.Clear();

            // Save currently pinned
            foreach (var feature in PinnedFeatures)
            {
                currentSettings.Features.Add(new UserFeatureSelectionSettings.Feature
                {
                    FeatureTypeKey = feature.FeatureKey
                });
            }
            SaveSettings(currentSettings);
        }

        /// <summary>
        /// Gets settings.
        /// </summary>
        /// <returns></returns>
        protected abstract UserFeatureSelectionSettings GetSettings();

        /// <summary>
        /// Saves settings
        /// </summary>
        /// <param name="settings"></param>
        protected abstract void SaveSettings(UserFeatureSelectionSettings settings);

        private void OnHandleFeaturePinned(ApplicationFeatureViewModel feature)
        {
            // Update accordingly to new pinned feature state
            if (feature.IsPinned)
            {
                if (!PinnedFeatures.Contains(feature))
                {
                    PinnedFeatures.Add(feature);
                }
            }
            else
            {
                PinnedFeatures.Remove(feature);
            }
        }

        private void ExecuteTriggerApplicationFeature(ApplicationFeatureViewModel feature)
        {
            if (feature.Command != null && feature.Command.CanExecute()) feature.Command.Execute(feature.CommandParameter);
            else _externalFeatureIntegrationManager.ExecuteFeature(feature.FeatureKey);
        }

        private ApplicationFeatureViewModel CloneFeature(ApplicationFeatureViewModel sourceFeature)
        {
            var result = _createFeature().Modify(newFeature =>
                {
                    newFeature.DisplayName = sourceFeature.DisplayName;
                    newFeature.FeatureKey = sourceFeature.FeatureKey;
                    newFeature.Group = sourceFeature.Group;
                    newFeature.GroupName = sourceFeature.GroupName;
                    newFeature.IsDragging = sourceFeature.IsDragging;
                    newFeature.IsPinned = sourceFeature.IsPinned;
                    newFeature.ShortDisplayName = sourceFeature.ShortDisplayName;
                    newFeature.Command = sourceFeature.Command;
                    newFeature.CommandParameter = sourceFeature.CommandParameter;
                    newFeature.IsVisible = sourceFeature.DisplayName == "Consent Forms" ? IsConsentFormActivated : true;                    
                });

            return result;
        }
       
        /// <summary>
        /// Command to toggle feature's pinned state
        /// </summary>
        public virtual ICommand HandleFeaturePinned { get; set; }

        /// <summary>
        /// Command which trigger's selected application feature action
        /// </summary>
        public virtual ICommand TriggerApplicationFeature { get; set; }

        /// <summary>
        /// Handles loading
        /// </summary>
        public virtual ICommand Loaded { get; set; }

        /// <summary>
        /// Handles closing
        /// </summary>
        public virtual ICommand Closing { get; set; }

        /// <summary>
        /// Provides a list of features for user's selection (in the More drop down)
        /// </summary>
        public virtual ICollection<ApplicationFeatureGroupViewModel> FeaturesSelection { get; set; }

        /// <summary>
        /// Provides a list of features user has pinned
        /// </summary>
        public virtual ICollection<ApplicationFeatureViewModel> PinnedFeatures { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public bool IsConsentFormActivated
        {
            get
            {
                return _isConsentFormActivated;
            }
            set { _isConsentFormActivated = value; }
        }    
    }
}
