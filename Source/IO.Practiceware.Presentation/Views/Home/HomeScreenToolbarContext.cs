﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf.Threading;
using System;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenToolbarContext : FeatureSelectionAndAccessContext
    {
        readonly IApplicationSettingsService _applicationSettingsService;
        ApplicationSettingContainer<UserFeatureSelectionSettings> _toolbarSettings;

        public HomeScreenToolbarContext(
            Func<ApplicationFeatureGroupViewModel> createGroup, Func<ApplicationFeatureViewModel> createFeature,
            IApplicationSettingsService applicationSettingsService,
            IExternalFeatureIntegrationManager externalFeatureIntegrationManager)
            : base(createGroup, createFeature, externalFeatureIntegrationManager, applicationSettingsService)
        {
            _applicationSettingsService = applicationSettingsService;           
        }

        protected override UserFeatureSelectionSettings GetSettings()
        {
            // Loaded? return existing one
            if (_toolbarSettings != null) return _toolbarSettings.Value;

            // Read toolbar settings
            _toolbarSettings = _applicationSettingsService.GetSetting<UserFeatureSelectionSettings>(ApplicationSetting.UserFeatureSelection);           

            // Create new one if not available yet
            if (_toolbarSettings == null)
            {
                _toolbarSettings = new ApplicationSettingContainer<UserFeatureSelectionSettings>
                                   {
                                       UserId = UserContext.Current.UserDetails.Id,
                                       Name = ApplicationSetting.UserFeatureSelection,
                                       Value = new UserFeatureSelectionSettings()
                                   };
            }

            return _toolbarSettings.Value;
        }

        protected override void SaveSettings(UserFeatureSelectionSettings settings)
        {
            _toolbarSettings.Value = settings;

            // Push settings to server
            System.Threading.Tasks.Task.Factory
                .StartNewWithCurrentTransaction(() =>
                    {
                        _applicationSettingsService.SetSetting(_toolbarSettings);
                    })
                .Wait();
        }
    }
}
