using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenWidgetsPanelContextReference : DataContextReference
    {
        public new HomeScreenWidgetsPanelContext DataContext
        {
            get { return (HomeScreenWidgetsPanelContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}