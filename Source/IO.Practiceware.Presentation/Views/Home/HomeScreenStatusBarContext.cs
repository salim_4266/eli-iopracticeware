﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.Views.Common;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenStatusBarContext : IViewContext
    {
        readonly ICommand _startProcessCommand;

        public HomeScreenStatusBarContext()
        {
            _startProcessCommand = new StartProcessCommand();

            OpenIOSupportWebsite = Command.Create(ExecuteOpenIOSupportWebsite);
        }

        void ExecuteOpenIOSupportWebsite()
        {
            var url = ConfigurationManager.SystemAppSettings["SupportSiteUrl"];
            _startProcessCommand.Execute(new StartProcessArgument { Target = url, CustomErrorMessage = string.Format("Visit {0} to get support.",url) });                                                
        }

        public virtual ICommand OpenIOSupportWebsite { get; set; }

        public IInteractionContext InteractionContext { get; set; }
    }
}
