﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using Soaf.Reflection;
using Soaf.Threading;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class HomeScreenWidgetsPanelContext : IViewContext
    {
        readonly IApplicationSettingsService _applicationSettingsService;
        readonly IInteractionManager _interactionManager;
        readonly Func<WidgetViewModel> _createWidget;
        ApplicationSettingContainer<UserWidgetLayoutSettings> _layoutSettings;
        readonly string _centerContainerName;

        public HomeScreenWidgetsPanelContext(IApplicationSettingsService applicationSettingsService, 
            IInteractionManager interactionManager,
            Func<WidgetViewModel> createWidget)
        {
            _applicationSettingsService = applicationSettingsService;
            _interactionManager = interactionManager;
            _createWidget = createWidget;
            _centerContainerName = Reflector.GetMember(() => CenterContainer).Name;

            var defaultsStorage = System.Windows.Application.Current.SafeLoadComponent<ResourceDictionary>(
                typeof(HomeScreenWidgetsPanelContext).Assembly.BuildComponentUri("/Views/Home/HomescreenDefaultsDefinition.xaml"));

            var widgetsSelection = ((IEnumerable)defaultsStorage["AllWidgets"])
                .OfType<WidgetViewModel>().Where(w => w.IsVisible && (w.PermissionId == null || w.PermissionId.Value.PrincipalContextHasPermission()))
                .ToExtendedObservableCollection();

            // Retrieved models don't have AOP applied to them, so converting
            widgetsSelection = widgetsSelection
                .Select(CloneWidgetViewModel)
                .ToExtendedObservableCollection();

            CenterContainer = new WidgetsContainerViewModel();
            CenterContainer.Widgets = new ExtendedObservableCollection<WidgetViewModel>();

            Loaded = Command.Create(ExecuteLoaded).Async(() => InteractionContext);
            
            RemoveWidget = Command.Create<WidgetViewModel>(ExecuteRemoveWidget);
            WidgetsSelection = widgetsSelection;
            CloneWidgetViewModelDelegate = CloneWidgetViewModel;
        }

        void ExecuteLoaded()
        {
            InteractionContext.Completed += delegate { SaveLayout(); };

            // Load settings
            _layoutSettings = _applicationSettingsService.GetSetting<UserWidgetLayoutSettings>(ApplicationSetting.UserWidgetLayout);

            if (_layoutSettings == null)
            {
                _layoutSettings = new ApplicationSettingContainer<UserWidgetLayoutSettings>
                    {
                        UserId = UserContext.Current.UserDetails.Id,
                        Name = ApplicationSetting.UserWidgetLayout,
                        Value = new UserWidgetLayoutSettings()
                    };

                // Add default container
                var container = new UserWidgetLayoutSettings.WidgetContainer
                    {
                        Name = _centerContainerName
                    };
                _layoutSettings.Value.Containers.Add(container);

                // Add default widget selection
                WidgetsSelection
                    .Where(p => p.IsAddedByDefault && p.IsVisible)
                    .Select(CloneWidgetViewModel)
                    .ToList()
                    .ForEach(w => container.Widgets.Add(new UserWidgetLayoutSettings.WidgetSettings
                        {
                            WidgetTemplateKey = w.WidgetTemplateKey,
                            CustomData = null
                        }));
            }

            // Restore center container layout (since we only have this container)
            var centerContainerLayout = _layoutSettings.Value.Containers
                .FirstOrDefault(c => c.Name == _centerContainerName);
            if (centerContainerLayout != null)
            {
                foreach (var widgetSettings in centerContainerLayout.Widgets)
                {
                    UserWidgetLayoutSettings.WidgetSettings settings = widgetSettings;

                    var widgetInstance = WidgetsSelection
                        .Where(wi => wi.WidgetTemplateKey == settings.WidgetTemplateKey)
                        .Select(CloneWidgetViewModel)
                        .FirstOrDefault();
                    if (widgetInstance == null) continue;

                    // Restore custom configuration
                    widgetInstance.CustomWidgetData = widgetSettings.CustomData;

                    // Assign widget into container
                    widgetInstance.OwnerContainer = CenterContainer;
                    CenterContainer.Widgets.Add(widgetInstance);
                }
            }
            else
            {
                // Add container if it doesn't exist
                _layoutSettings.Value.Containers.Add(new UserWidgetLayoutSettings.WidgetContainer
                {
                    Name = _centerContainerName
                });
            }
        }

        void SaveLayout()
        {
            // Load center container layout settings
            var centerContainerLayout = _layoutSettings.Value.Containers
                .First(c => c.Name == _centerContainerName);

            // Clear current widget
            centerContainerLayout.Widgets.Clear();

            // Store settings of every widget
            foreach (var widget in CenterContainer.Widgets)
            {
                var widgetSettings = new UserWidgetLayoutSettings.WidgetSettings
                    {
                        WidgetTemplateKey = widget.WidgetTemplateKey,
                        CustomData = widget.CustomWidgetData
                    };
                centerContainerLayout.Widgets.Add(widgetSettings);
            }

            // Push to server
            System.Threading.Tasks.Task.Factory
                .StartNewWithCurrentTransaction(() =>
                    {
                        _applicationSettingsService.SetSetting(_layoutSettings);
                    })
                .Wait();
        }

        WidgetViewModel CloneWidgetViewModel(WidgetViewModel w)
        {
            return _createWidget().Modify(newWidget =>
                {
                    newWidget.Description = w.Description;
                    newWidget.DisplayName = w.DisplayName;
                    newWidget.IsAddedByDefault = w.IsAddedByDefault;
                    newWidget.IsDragging = w.IsDragging;
                    newWidget.OwnerContainer = w.OwnerContainer;
                    newWidget.PreviewImageSource = w.PreviewImageSource;
                    newWidget.WidgetTemplateKey = w.WidgetTemplateKey;
                    newWidget.IsVisible = w.IsVisible;
                });
        }

        void ExecuteRemoveWidget(WidgetViewModel widget)
        {
            // Confirm widget removal
            if (_interactionManager.Confirm("Remove this widget and all of its settings?", "Cancel", "Remove")
                .GetValueOrDefault())
            {
                widget.OwnerContainer.Widgets.Remove(widget);
                widget.OwnerContainer = null;   
            }
        }

        public virtual ICommand RemoveWidget { get; set; }

        public virtual ICommand Loaded { get; set; }

        public virtual ICommand Closing { get; set; }

        public virtual ObservableCollection<WidgetViewModel> WidgetsSelection { get; set; }

        public virtual WidgetsContainerViewModel CenterContainer { get; set; }

        public virtual Func<WidgetViewModel, WidgetViewModel> CloneWidgetViewModelDelegate { get; set; } 

        public IInteractionContext InteractionContext { get; set; }
    }
}
