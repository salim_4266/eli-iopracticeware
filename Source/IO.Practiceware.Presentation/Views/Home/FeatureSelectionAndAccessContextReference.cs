using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Home
{
    public class FeatureSelectionAndAccessContextReference : DataContextReference
    {
        public new FeatureSelectionAndAccessContext DataContext
        {
            get { return (FeatureSelectionAndAccessContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}