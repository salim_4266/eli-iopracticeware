﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.SubmitTransactions;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.ComponentModel.DataAnnotations;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.SubmitTransactions
{
    public class SubmitTransactionsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public SubmitTransactionsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowSubmitTransactions(SubmitTransactionsLoadArguments loadArguments)
        {
            if (!PermissionId.GenerateClaimsAndStatements.EnsurePermission())
            {
                return;
            }

            loadArguments.Validate(true);

            var view = new SubmitTransactionsView();
            var dataContext = view.DataContext.EnsureType<SubmitTransactionsViewContext>();

            dataContext.LoadArguments = loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = "Submit Claims" });
        }

        public void ShowSubmitTransactionsErrorReport(SubmitTransactionsErrorReportArguments errorReportArguments)
        {
           var view = new SubmitTransactionsErrorReportView();
           var dataContext = view.DataContext.EnsureType<SubmitTransactionsErrorReportViewContext>();
           dataContext.ReportHeader = errorReportArguments.Header;
           dataContext.ReportBody = errorReportArguments.Body;

           var viewManager = new PrintingDialogViewManager();
           viewManager.PrintPreviewPreparedElement(view, "Submit Transactions Error Report");
        }
    }

    public class SubmitTransactionsLoadArguments
    {
        [Required(ErrorMessage = "You must supply a default claim type")]
        public ClaimType? DefaultClaimType { get; set; }
    }

    public class SubmitTransactionsErrorReportArguments
    {
        public string Header { get; set; }

        public string Body { get; set; }
    }
}
