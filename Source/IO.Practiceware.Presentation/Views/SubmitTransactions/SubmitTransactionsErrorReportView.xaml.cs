﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.SubmitTransactions
{
    /// <summary>
    /// Interaction logic for SubmitTransactionsErrorReportView.xaml
    /// </summary>
    public partial class SubmitTransactionsErrorReportView 
    {
        public SubmitTransactionsErrorReportView()
        {
            InitializeComponent();
        }
    }

    public class SubmitTransactionsErrorReportViewContext : IViewContext
    {
        public virtual string ReportHeader { get; set; }
        public virtual string ReportBody { get; set; }
        public IInteractionContext InteractionContext { get; set; }
    }
}
