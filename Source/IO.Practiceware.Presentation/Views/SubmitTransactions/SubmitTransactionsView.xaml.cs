﻿using System.IO;
using System.Windows.Interop;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.SubmitTransactions;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Presentation.ViewServices.SubmitTransactions;
using IO.Practiceware.Services.Utilities;
using IO.Practiceware.Storage;
using Microsoft.WindowsAPICodePack.Dialogs;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using ApplicationContext = IO.Practiceware.Application.ApplicationContext;

namespace IO.Practiceware.Presentation.Views.SubmitTransactions
{
    /// <summary>
    /// Interaction logic for SubmitTransactions.xaml
    /// </summary>
    public partial class SubmitTransactionsView
    {
        public SubmitTransactionsView()
        {
            InitializeComponent();
        }
    }

    public class SubmitTransactionsViewContext : IViewContext
    {
        private readonly ISubmitTransactionsViewService _submitTransactionsViewService;
        private readonly Func<SubmitTransactionsFilterSelectionViewModel> _createFilterSelectionViewModel;
        private readonly IMapper<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel> _filterSelectionMapper;
        private readonly DocumentViewManager _documentViewManger;
        private readonly SubmitTransactionsViewManager _submitTransactionsViewManager;

        /// <summary>
        /// Used to cache the selected start date after it has been loaded from the view service.  We cache it because it requires a db call to figure out the default
        /// start date, so when the user hits the reset button we want to reset the start date back to the original default without having to make the db call again.
        /// </summary>
        private DateTime? _startDateForFilter;

        public SubmitTransactionsViewContext(ISubmitTransactionsViewService submitTransactionsViewService,
            Func<SubmitTransactionsFilterSelectionViewModel> createFilterSelectionViewModel,
            IMapper<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel> filterSelectionMapper,
            DocumentViewManager documentViewManger, SubmitTransactionsViewManager submitTransactionsViewManager)
        {
            _submitTransactionsViewService = submitTransactionsViewService;
            _createFilterSelectionViewModel = createFilterSelectionViewModel;
            _filterSelectionMapper = filterSelectionMapper;
            _documentViewManger = documentViewManger;
            _submitTransactionsViewManager = submitTransactionsViewManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            ResetFilter = Command.Create(ExecuteResetFilter);
            Search = Command.Create(ExecuteSearch, CanSearchExecute).Async(() => InteractionContext);
            GenerateClaim = Command.Create<IEnumerable<TransactionSearchResultViewModel>>(ExecuteGenerateClaim, x => x.IsNotNullOrEmpty()).Async(() => InteractionContext);
            SelectAll = Command.Create(ExecuteSelectAll);
            SelectNone = Command.Create(ExecuteSelectNone);
            PrintClaim = Command.Create<IEnumerable<TransactionSearchResultViewModel>>(ExecutePrintClaim, x => x.IsNotNullOrEmpty()).Async(() => InteractionContext);
            SaveFilter = Command.Create(ExecuteSaveFilter, () => SelectedFilter != null && SelectedFilter.Id >= 0);
            DeleteFilter = Command.Create(ExecuteDeleteFilter, () => SelectedFilter != null && SelectedFilter.Id > 0).Async(() => InteractionContext);
            SetToSent = Command.Create<IEnumerable<TransactionSearchResultViewModel>>(ExecuteSetToSent, x => x.IsNotNullOrEmpty()).Async(() => InteractionContext);
            ConfirmPrintOk = Command.Create(ExecuteConfirmPrintOk);
            ConfirmRequeue = Command.Create(ExecuteConfirmRequeue);
            OpenBrowserGotoLink = Command.Create<string>(ExecuteOpenBrowserGotoLink);
        }

        private static void ExecuteOpenBrowserGotoLink(string uri)
        {
            if (uri.IsNotNullOrEmpty())
            {
                Process.Start(new ProcessStartInfo(uri));
            }
        }

        private bool _reprintClaims;
        private void ExecuteConfirmPrintOk()
        {
            _reprintClaims = true;
        }

        private bool _requeueClaims;
        private void ExecuteConfirmRequeue()
        {
            _requeueClaims = true;
        }

        private void ExecuteSetToSent(IEnumerable<TransactionSearchResultViewModel> selectedItems)
        {
            // only paper claims can have their status manually altered
            if (FilterSelectionViewModel.CurrentClaimTypeInSearchResults != ClaimType.PaperClaims) return;

            var transactionIds = selectedItems.SelectMany(x => x.BillingServiceTransactionIds);

            _submitTransactionsViewService.SetTransactionsToSentStatus(transactionIds);

            // reload the grid
            ExecuteSearch();
        }

        private bool CanSearchExecute()
        {
            return FilterSelectionViewModel != null && FilterSelectionViewModel.IsValid();
        }

        private void ExecuteDeleteFilter()
        {
            if (SelectedFilter != null && SelectedFilter.Id.HasValue && SelectedFilter.Id > 0)
            {
                _submitTransactionsViewService.DeleteUserSetting(SelectedFilter.Id.Value);
                var replaceFilter = FilterViewModel.SavedFilters.Single(x => x.Id == SelectedFilter.Id.Value);
                FilterViewModel.SavedFilters.Remove(replaceFilter);
            }
        }

        private void ExecuteSaveFilter()
        {
            var currentName = SelectedFilter.Id == 0 ? "" : SelectedFilter.Name;
            var promptResult = InteractionManager.Current.Prompt("Enter name below to save your selected filters:", "Saved Filters", currentName);

            if (promptResult.DialogResult.HasValue && promptResult.DialogResult.Value)
            {
                if (promptResult.Input.IsNullOrEmpty())
                {
                    InteractionManager.Current.Alert("You must enter a valid name for your filter");
                    return;
                }

                var filterSelection = FilterSelectionViewModel;

                Command.Create(() =>
                {
                    var userSetting = _filterSelectionMapper.Map(filterSelection);
                    userSetting.Name = promptResult.Input;
                    userSetting.UserId = UserContext.Current.UserDetails.Id;

                    userSetting.Id = SelectedFilter.Id == 0 ? new int?() : SelectedFilter.Id;

                    var id = _submitTransactionsViewService.SaveUserSetting(userSetting);

                    if (id.HasValue && SelectedFilter.Id == 0) // means a new one was created.
                    {
                        userSetting.Id = id;
                        FilterViewModel.SavedFilters.Add(userSetting);
                        // ReSharper disable once ConditionIsAlwaysTrueOrFalse (There is an item in this collection withouth an Id (Create New))
                        FilterViewModel.SavedFilters = FilterViewModel.SavedFilters.OrderBy(f => f.Id == 0 ? 0 : 1).ThenBy(f => f.Name).ToExtendedObservableCollection();
                        SelectedFilter = FilterViewModel.SavedFilters.WithId(userSetting.Id);
                    }
                    else
                    {
                        var replaceFilter = FilterViewModel.SavedFilters.Single(x => x.Id == userSetting.Id);
                        FilterViewModel.SavedFilters.Remove(replaceFilter);
                        FilterViewModel.SavedFilters.Add(userSetting);
                        FilterViewModel.SavedFilters = FilterViewModel.SavedFilters.OrderBy(f => f.Id == 0 ? 0 : 1).ThenBy(f => f.Name).ToExtendedObservableCollection();
                        SelectedFilter = FilterViewModel.SavedFilters.WithId(userSetting.Id);
                    }
                }).Async(() => InteractionContext).Execute();
            }



        }

        private void ExecutePrintClaim(IEnumerable<TransactionSearchResultViewModel> selectedItems)
        {
            if (FilterSelectionViewModel.CurrentClaimTypeInSearchResults != ClaimType.PaperClaims) return;

            var listOfErrorMessages = new List<string>();
            var listOfExceptions = new List<string>();
            var producedClaims = new List<TransactionSearchResultViewModel>();
            var listOfPathsToDelete = new List<string>();
            var listOfGeneratedPdfPaths = new List<Tuple<string, string>>();
            var listOfGeneratedMessages = new List<Tuple<string, List<Guid>>>();

            var dict = new Dictionary<long, List<string>>();

            try
            {
                dict = _submitTransactionsViewService.ProduceMessagePaperClaimHtml(selectedItems.ToDictionary(x => x.InvoiceReceivableId, y => y.BillingServiceTransactionIds), selectedItems.First().ClaimFormType.Id);
            }
            catch (Exception ex)
            {
                var validationError = ex.SearchFor(m => new ValidationException(m));
                if (validationError != null)
                {
                    listOfErrorMessages.Add(validationError.Message);
                }
                else
                {
                    listOfExceptions.Add(ex.ToString());
                }
            }


            // Show validation alerts if any
            if (listOfErrorMessages.IsNotNullOrEmpty())
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert(listOfErrorMessages.Join(Environment.NewLine + Environment.NewLine)));
                return;
            }

            // If there were exceptions -> show them as well
            if (listOfExceptions.IsNotNullOrEmpty())
            {
                var exception = new Exception(listOfExceptions.Join(Environment.NewLine + Environment.NewLine));
                Trace.TraceError(exception.ToString());
                this.Dispatcher().Invoke(() => InteractionManager.Current.DisplayDetailedException(exception));
                return;
            }

            var documentName = _submitTransactionsViewService.GetDocumentName(selectedItems.First().ClaimFormType.Id);

            foreach (var ir in selectedItems)
            {
                if (!dict.ContainsKey(ir.InvoiceReceivableId)) continue;

                var path = FileManager.Instance.GetTempPathName();
                listOfPathsToDelete.Add(path);
                path = path.Replace(".tmp", ".pdf");

                var currentMessage = dict[ir.InvoiceReceivableId];

                if (currentMessage.IsNotNullOrEmpty())
                {
                    listOfGeneratedMessages.Add(new Tuple<string, List<Guid>>(currentMessage.Join(Environment.NewLine), ir.BillingServiceTransactionIds));

                    byte[] pdfBytes = WkHtmlToPdfWrapperService.GeneratePdf(currentMessage);

                    FileManager.Instance.CommitContents(path, pdfBytes);
                    listOfPathsToDelete.Add(path);
                    listOfGeneratedPdfPaths.Add(new Tuple<string, string>(path, documentName));

                    // Add as produced claim
                    producedClaims.Add(ir);
                }
            }

            this.Dispatcher().Invoke(() => DoActualPrint(listOfGeneratedPdfPaths));

            if (!_requeueClaims)
            {
                // we're done, set all the successful claims to a status of 'sent'
                _submitTransactionsViewService.SetTransactionsToSentStatus(producedClaims.SelectMany(x => x.BillingServiceTransactionIds));
            }

            // write entry to external system messages              
            _submitTransactionsViewService.SavePaperClaimExternalSystemMessageBulk(listOfGeneratedMessages);

            // delete all paths
            listOfPathsToDelete.ForEach(FileManager.Instance.Delete);

            // reload the grid
            Search.Execute();
        }

        private void DoActualPrint(List<Tuple<string, string>> listOfGeneratedPdfPaths)
        {
            if (listOfGeneratedPdfPaths.IsNullOrEmpty()) return;

            _reprintClaims = true;
            _requeueClaims = false;

            while (_reprintClaims) // if still true, user hit the 'Reprint' button
            {
                _reprintClaims = false;

                _documentViewManger.HeaderTitleOfCurrentWindow = "Submit Claims";
                _documentViewManger.PrintDocuments(listOfGeneratedPdfPaths.First().Item2, listOfGeneratedPdfPaths.Select(x => x.Item1).ToArray());

                PrintFinishedTrigger = true; // ask again.
                PrintFinishedTrigger = false;
            }
            ClaimsRequeuedTrigger = true; //ask confirmation that the claims should be in queue to print again in future
            ClaimsRequeuedTrigger = false;
        }

        private void ExecuteSelectNone()
        {
            SelectedTransactions = new ObservableCollection<TransactionSearchResultViewModel>();
        }

        private void ExecuteSelectAll()
        {
            if (SearchResults.IsNotNullOrEmpty())
            {
                SelectedTransactions = new ObservableCollection<TransactionSearchResultViewModel>(SearchResults);
            }
        }

        private void ExecuteGenerateClaim(IEnumerable<TransactionSearchResultViewModel> selectedItems)
        {
            if (selectedItems.IsNullOrEmpty() || FilterSelectionViewModel.CurrentClaimTypeInSearchResults != ClaimType.EClaims) return;

            // Verify e-claims location
            if (string.IsNullOrWhiteSpace(ClaimsDirectoryPaths.ElectronicClaimsFileDirectory)
                || !FileManager.Instance.DirectoryExists(ClaimsDirectoryPaths.ElectronicClaimsFileDirectory))
            {
                string eClaimsPath = null;
                this.Dispatcher().Invoke(() =>
                {
                    var commonOpenFileDialog = new CommonOpenFileDialog
                    {
                        Title = "Please select folder for electronic claims generation",
                        IsFolderPicker = true,
                        AddToMostRecentlyUsedList = false,
                        AllowNonFileSystemItems = false,
                        EnsureFileExists = true,
                        EnsurePathExists = true,
                        EnsureReadOnly = false,
                        EnsureValidNames = true,
                        Multiselect = false,
                        ShowPlacesList = true
                    };

                    if (ConfigurationManager.ClientApplicationConfiguration.RdsWithSharedAccountMode)
                    {
                        commonOpenFileDialog.DefaultDirectory = "\\\\tsclient";
                    }

                    var window = System.Windows.Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(x => x.IsActive);
                    // User selected location? -> save it
                    if ((window == null ? commonOpenFileDialog.ShowDialog() : commonOpenFileDialog.ShowDialog(new WindowInteropHelper(window).Handle)) == CommonFileDialogResult.Ok)
                    {
                        eClaimsPath = commonOpenFileDialog.FileName;
                    }
                });

                // Quit if path is still not set
                if (string.IsNullOrEmpty(eClaimsPath)) return;

                // Save updated setting
                ClaimsDirectoryPaths.ElectronicClaimsFileDirectory = eClaimsPath;
                ClaimsDirectoryPaths.Id = _submitTransactionsViewService.SetClaimsDirectoryPaths(ClaimsDirectoryPaths);
            }

            var listOfErrorMessages = new List<string>();
            var listOfExceptions = new List<string>();

            // get possible list of items that don't have a clearing house
            var noClearingHouseItems = selectedItems.Where(x => x.ClaimFileReceiver == null).ToList();

            foreach (var item in noClearingHouseItems)
            {
                listOfErrorMessages.Add("The following transaction does not have a Claim File Receiver and therefore could not be processed - PatientId: {0}, ServiceDate: {1}".FormatWith(item.PatientId, item.ServiceDate));
            }

            // group by claim file receiver (clearing house)
            var groupedByClaimFileReceiver = selectedItems.Except(noClearingHouseItems).GroupBy(x => x.ClaimFileReceiver.Id);

            foreach (var transactionsByClaimFileReceiver in groupedByClaimFileReceiver) // produce one message for each set of clearing houses
            {
                // now split each clearing house set of transactions into sets of transactions by Claim Type  --> we want to produce a different message for each claim type.
                var groupedByClaimFormType = transactionsByClaimFileReceiver.GroupBy(x => x.ClaimFormType.Id);

                foreach (var transactionsByClaimFormType in groupedByClaimFormType)
                {
                    var transactionIds = transactionsByClaimFormType.SelectMany(x => x.BillingServiceTransactionIds).ToList();

                    var claimFormTypeName = transactionsByClaimFormType.First().ClaimFormType.Name;

                    IList<string> validationResults = new List<string>();
                    const string errorMessage = "The following Clearing House messages could not be processed - Clearing House: {0},  Claim Form: {1}, Reason(s): {2}";
                    try
                    {
                        IEnumerable<Guid> excludedTransactionIds;
                        X12MessageViewModel message = _submitTransactionsViewService.ProduceMessage837(transactionIds, transactionsByClaimFormType.Key, out validationResults, out excludedTransactionIds);
                        var successfulTransactionIds = transactionIds.Except(excludedTransactionIds.ToList());
                        if (message != null && successfulTransactionIds.Any())
                        {
                            var fullFilePath = Path.Combine(ClaimsDirectoryPaths.ElectronicClaimsFileDirectory, string.Format("{0}_{1}{2}", transactionsByClaimFormType.First().ClaimFileReceiver.Name, claimFormTypeName, Constants.ElectronicClaimsFileNameFormat));

                            FileManager.Instance.CommitContents(fullFilePath, message.Message);

                            // write entry to external system messages
                            _submitTransactionsViewService.SaveElectronicClaimExternalSystemMessage(message, successfulTransactionIds);

                            // set to sent status
                            _submitTransactionsViewService.SetTransactionsToSentStatus(successfulTransactionIds);

                            // set summary display information
                            var clearingHouseInfo = _submitTransactionsViewService.GetClearingHouseInfo(transactionsByClaimFileReceiver.Key);
                            LastGeneratedClearingHouseWebsiteUrl = clearingHouseInfo != null ? clearingHouseInfo.Item3 : "";
                            GeneratedEClaimsFullFilePath = fullFilePath;
                            EClaimsTotalGenerated = transactionsByClaimFormType.Count();
                            EClaimsTotalAmountBilled = transactionsByClaimFormType.Sum(x => x.AmountBilled);

                            // triggers an alert command notifying the user that generation has completed and displays summary information
                            EClaimsGenerationFinishedTrigger = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        listOfExceptions.Add(errorMessage.FormatWith(transactionsByClaimFileReceiver.First().ClaimFileReceiver.Name, claimFormTypeName, ex.ToString()));
                    }
                    if (validationResults.Any())
                    {
                        listOfErrorMessages.Add(errorMessage.FormatWith(transactionsByClaimFileReceiver.First().ClaimFileReceiver.Name, claimFormTypeName, Environment.NewLine + validationResults.Join(Environment.NewLine)));
                    }
                }
            }

            // Show validation alerts if any
            if (listOfErrorMessages.IsNotNullOrEmpty())
            {
                const string header = "There were errors generating the file(s). Please address the following and generate the file(s) again";
                this.Dispatcher().Invoke(() =>
                    _submitTransactionsViewManager.ShowSubmitTransactionsErrorReport(
                    new SubmitTransactionsErrorReportArguments
                    {
                        Header = header,
                        Body = listOfErrorMessages.Join(Environment.NewLine + Environment.NewLine)
                    }));
            }

            // If there were exceptions -> show them as well
            if (listOfExceptions.IsNotNullOrEmpty())
            {
                var exception = new Exception(listOfExceptions.Join(Environment.NewLine + Environment.NewLine));
                Trace.TraceError(exception.ToString());
                this.Dispatcher().Invoke(() => InteractionManager.Current.DisplayDetailedException(exception));
            }

            // reload the grid
            ExecuteSearch();
        }

        private void ExecuteSearch()
        {
            var results = _submitTransactionsViewService.SearchTransactions(FilterSelectionViewModel);

            SearchResults = new ObservableCollection<TransactionSearchResultViewModel>(results);
            FilterSelectionViewModel.CurrentClaimTypeInSearchResults = FilterSelectionViewModel.ClaimType;
        }

        private void ExecuteResetFilter()
        {
            FilterSelectionViewModel = _createFilterSelectionViewModel();
            FilterSelectionViewModel.StartDate = _startDateForFilter;
            FilterSelectionViewModel.EndDate = DateTime.Now.ToClientTime();

            if (LoadArguments.DefaultClaimType.HasValue)
            {
                FilterSelectionViewModel.ClaimType = LoadArguments.DefaultClaimType.Value;
            }
            else
            {
                FilterSelectionViewModel.ClaimType = ClaimType.PaperClaims;
            }

            SelectedFilter = null;
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteLoad()
        {
            var userId = UserContext.Current.UserDetails.Id;

            var loadInfo = _submitTransactionsViewService.LoadInformation(userId, ApplicationContext.Current.ComputerName);

            loadInfo.FilterSelectionViewModel.ClaimType = LoadArguments.DefaultClaimType.HasValue ? LoadArguments.DefaultClaimType.Value : ClaimType.PaperClaims;
            loadInfo.FilterSelectionViewModel.PaperClaimFormId = loadInfo.FilterViewModel.PaperClaimForms.IsNotNullOrEmpty() ? loadInfo.FilterViewModel.PaperClaimForms.First().Id : new int?();

            if (loadInfo.FilterViewModel.ElectronicClaimForms.IsNotNullOrEmpty())
            {
                loadInfo.FilterSelectionViewModel.SelectedElectronicClaims = new ObservableCollection<ClaimSelectionViewModel>(loadInfo.FilterViewModel.ElectronicClaimForms);
            }

            ClaimsDirectoryPaths = loadInfo.ClaimsDirectoryPaths;
            FilterViewModel = loadInfo.FilterViewModel;
            loadInfo.FilterViewModel.SavedFilters.Insert(0, new SubmitTransactionsUserSettingViewModel { Id = 0, Name = "Create New" });

            FilterSelectionViewModel = loadInfo.FilterSelectionViewModel;

            _startDateForFilter = FilterSelectionViewModel.StartDate;
        }

        #region Commands

        /// <summary>
        /// Command to load the UI
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to close the UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to clear the filter selections
        /// </summary>
        public ICommand ResetFilter { get; protected set; }

        /// <summary>
        /// Command to perform the search based on the filter selections
        /// </summary>
        public ICommand Search { get; protected set; }

        /// <summary>
        /// Command to generated the claim based on the selected transactions
        /// </summary>
        public ICommand GenerateClaim { get; protected set; }

        /// <summary>
        /// Command to select all search result rows.
        /// </summary>
        public ICommand SelectAll { get; protected set; }

        /// <summary>
        /// Command to de-select all search result rows.
        /// </summary>
        public ICommand SelectNone { get; protected set; }

        /// <summary>
        /// Command to print the selected paper claim
        /// </summary>
        public ICommand PrintClaim { get; protected set; }

        /// <summary>
        /// Command to save the current filter settings for the logged in user.
        /// </summary>
        public ICommand SaveFilter { get; protected set; }

        /// <summary>
        /// Command to delete the currently selected filter
        /// </summary>
        public ICommand DeleteFilter { get; protected set; }

        /// <summary>
        /// Command to set the selected transactions to 'Sent' status.
        /// </summary>
        public ICommand SetToSent { get; protected set; }

        /// <summary>
        /// Confirms that printing has be successful
        /// </summary>
        public ICommand ConfirmPrintOk { get; protected set; }

        /// <summary>
        /// Leave the cliams on queue for printing
        /// </summary>
        public ICommand ConfirmRequeue { get; protected set; }

        /// <summary>
        /// Command to open a web browser.
        /// </summary>
        public ICommand OpenBrowserGotoLink { get; protected set; }

        #endregion

        [DispatcherThread]
        public virtual SubmitTransactionsFilterViewModel FilterViewModel { get; set; }

        [DispatcherThread]
        public virtual SubmitTransactionsFilterSelectionViewModel FilterSelectionViewModel { get; protected set; }

        [DispatcherThread]
        public virtual ObservableCollection<TransactionSearchResultViewModel> SearchResults { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<TransactionSearchResultViewModel> SelectedTransactions { get; set; }

        [DispatcherThread]
        public virtual bool PrintFinishedTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ClaimsRequeuedTrigger { get; set; }

        [DispatcherThread]
        public virtual bool EClaimsGenerationFinishedTrigger { get; set; }

        [DispatcherThread]
        public virtual ClaimsMachineSettingViewModel ClaimsDirectoryPaths { get; set; }

        [DispatcherThread]
        public virtual string GeneratedEClaimsFullFilePath { get; set; }

        [DispatcherThread]
        public virtual string LastGeneratedClearingHouseWebsiteUrl { get; set; }

        [DispatcherThread]
        public virtual int EClaimsTotalGenerated { get; set; }

        [DispatcherThread]
        public virtual decimal EClaimsTotalAmountBilled { get; set; }

        private SubmitTransactionsUserSettingViewModel _selectedFilter;
        [DispatcherThread]
        public virtual SubmitTransactionsUserSettingViewModel SelectedFilter
        {
            get { return _selectedFilter; }
            set
            {
                if (value == null || value.Id == 0)
                {
                    _selectedFilter = value;
                    return;
                }

                // set the appropriate values to the filter
                _selectedFilter = value;

                FilterSelectionViewModel.StartDate = _selectedFilter.StartDate;
                FilterSelectionViewModel.EndDate = _selectedFilter.EndDate;
                FilterSelectionViewModel.ClaimType = (ClaimType)_selectedFilter.ClaimType;
                FilterSelectionViewModel.PaperClaimFormId = _selectedFilter.PaperClaimFormId;
                FilterSelectionViewModel.SelectedBillingDoctors = new ObservableCollection<NamedViewModel>(FilterViewModel.BillingDoctors.Where(x => _selectedFilter.SelectedBillingDoctors != null && _selectedFilter.SelectedBillingDoctors.Contains(x.Id)));
                FilterSelectionViewModel.SelectedBillingOrganizations = new ObservableCollection<NamedViewModel>(FilterViewModel.BillingOrganizations.Where(x => _selectedFilter.SelectedBillingOrganizations != null && _selectedFilter.SelectedBillingOrganizations.Contains(x.Id)));
                FilterSelectionViewModel.SelectedElectronicClaims = new ObservableCollection<ClaimSelectionViewModel>(FilterViewModel.ElectronicClaimForms.Where(x => _selectedFilter.ElectronicClaimFormIds != null && _selectedFilter.ElectronicClaimFormIds.Contains(x.Id)));
                FilterSelectionViewModel.SelectedInsurers = new ObservableCollection<SubmitTransactionsInsurersViewModel>(FilterViewModel.Insurers.Where(x => _selectedFilter.SelectedInsurers != null && x.InsurerIds.All(y => _selectedFilter.SelectedInsurers.Contains(y))));
                FilterSelectionViewModel.SelectedLocations = new ObservableCollection<NamedViewModel>(FilterViewModel.Locations.Where(x => _selectedFilter.SelectedLocations != null && _selectedFilter.SelectedLocations.Contains(x.Id)));
                FilterSelectionViewModel.SelectedScheduledDoctors = new ObservableCollection<NamedViewModel>(FilterViewModel.ScheduledDoctors.Where(x => _selectedFilter.SelectedScheduledDoctors != null && _selectedFilter.SelectedScheduledDoctors.Contains(x.Id)));
            }
        }

        public IInteractionContext InteractionContext
        {
            get;
            set;
        }

        public SubmitTransactionsLoadArguments LoadArguments
        {
            get;
            set;
        }

    }
}
