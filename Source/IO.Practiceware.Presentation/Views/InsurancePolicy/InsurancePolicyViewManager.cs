﻿using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.Views.NonClinicalPatient;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    public class InsurancePolicyViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly INavigableInteractionManager _navigableInteractionManager;
        private readonly Func<IInteractionContext<InsurancePolicyNavigationViewModel>> _createInteractionContext;

        public InsurancePolicyViewManager(IInteractionManager interactionManager,
                                          INavigableInteractionManager navigableInteractionManager,
                                          Func<IInteractionContext<InsurancePolicyNavigationViewModel>> createInteractionContext)
        {
            _interactionManager = interactionManager;
            _navigableInteractionManager = navigableInteractionManager;
            _createInteractionContext = createInteractionContext;
        }

        public ScanDocumentReturnArguments ShowScanDocumentView()
        {
            var view = new ScanDocumentView();
            var viewContext = view.DataContext.EnsureType<ScanDocumentViewContext>();            
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                WindowStartupLocation = Soaf.Presentation.WindowStartupLocation.CenterOwner
            });
            return new ScanDocumentReturnArguments { ScannedDocuments = viewContext.ScannedDocuments };
        }

        public InsurancePolicyReturnArguments ShowEnterPolicyDetailsView(int insurancePolicyId, int insurerId, int policyHolderId)
        {
            var view = new EnterPolicyDetailsView();
            var viewContext = view.DataContext.EnsureType<EnterPolicyDetailsViewContext>();
            viewContext.InsurancePolicyId = insurancePolicyId;
            viewContext.InsurerId = insurerId;
            viewContext.PolicyholderId = policyHolderId;

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.NoResize,
                WindowStartupLocation = Soaf.Presentation.WindowStartupLocation.CenterOwner,
            });

            return new InsurancePolicyReturnArguments {ExistingPolicyHasBeenEdited = viewContext.ExistingPolicyHasBeenEdited};
        }

        public EditPolicyHolderReturnArguments ShowPolicyholderDetailsView(int policyholderId, int patientIdInContext)
        {
            var view = new NonClinicalPatientDetailsView();
            var viewContext = view.DataContext.EnsureType<NonClinicalPatientDetailsViewContext>();
            viewContext.NonClinicalPatientId = policyholderId;
            viewContext.DefaultAddressPatientId = patientIdInContext;

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                WindowStartupLocation = Soaf.Presentation.WindowStartupLocation.CenterOwner
            });

            return new EditPolicyHolderReturnArguments {PolicyHolderHasBeenEdited = viewContext.NonClinicalPatientSuccessfullySaved};
        }

        public InsurancePolicySearchReturnArguments ShowSearchInsurancePlanView()
        {        
            var view = new SearchInsurancePlanView();
            var viewContext = view.DataContext.EnsureType<SearchInsurancePlanViewContext>();
            

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Maximized,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                WindowStartupLocation = Soaf.Presentation.WindowStartupLocation.CenterOwner
            });

            return new InsurancePolicySearchReturnArguments {InsurerId = viewContext.SelectedSearchResult != null ? viewContext.SelectedSearchResult.Id : new int()};
        }
    }

    public class EditPolicyHolderReturnArguments
    {
        public bool PolicyHolderHasBeenEdited { get; set; }
    }

    public class InsurancePolicyReturnArguments
    {
        public bool NewPolicyHasBeenCreated { get; set; }
        public bool ExistingPolicyHasBeenEdited { get; set; }
        public bool NewLinkedPolicyHasBeenAdded { get; set; }
    }

    public class ScanDocumentReturnArguments
    {
        public IEnumerable<Byte[]> ScannedDocuments { get; set; }
    }

    public class InsurancePolicySearchReturnArguments
    {
        public int? InsurerId { get; set; }
    }
}
