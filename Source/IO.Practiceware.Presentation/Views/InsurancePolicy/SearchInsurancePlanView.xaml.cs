﻿using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SearchInsurancePlan;
using IO.Practiceware.Presentation.ViewModels.PatientSearch;
using IO.Practiceware.Presentation.Views.InsurancePlansSetup;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using IO.Practiceware.Services.PatientSearch;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
	/// <summary>
	/// Interaction logic for SearchInsurancePlanView.xaml
	/// </summary>
	public partial class SearchInsurancePlanView
	{
		public SearchInsurancePlanView()
		{
			InitializeComponent();
		}
	}

    public class SearchInsurancePlanViewContext : IViewContext
    {
        readonly IInteractionManager _interactionManager;
        public INavigableInteractionManager NavigableInteractionManager;
        private readonly ISearchInsurancePlanViewService _searchInsurancePlanViewService;

        public SearchInsurancePlanViewContext(
            IInteractionManager interactionManager,
            INavigableInteractionManager navigableInteractionManager,
            ISearchInsurancePlanViewService searchInsurancePlanViewService)
        {
            #region helper func getTextSearchModes

            Func<ObservableCollection<TextSearchModeViewModel>> getTextSearchModes = () => 
                Enums.GetValues<TextSearchMode>().Select(tsm => new TextSearchModeViewModel
                {
                    Id = (int)tsm,
                    Name = tsm.GetDisplayName(),
                    ToolTipText = tsm.GetDescription(),
                }).ToExtendedObservableCollection();

            #endregion

            _interactionManager = interactionManager;
            NavigableInteractionManager = navigableInteractionManager;
            _searchInsurancePlanViewService = searchInsurancePlanViewService;

            CreateOrEditInsurancePlan = Command.Create(ExecuteCreateOrEditInsurancePlan, () => SearchResults != null);
            Search = Command.Create(ExecuteSearch, () => !SearchText.IsNullOrEmpty() && SelectedTextSearchMode.HasValue).Async(() => InteractionContext);
            Back = Command.Create(ExecuteBack,()=> IsInWizardMode);
            Next = Command.Create(ExecuteNext, () => SelectedSearchResult != null);
            Cancel = Command.Create(ExecuteCancel);
            Load = Command.Create(ExecuteLoad);

            TextSearchModes = getTextSearchModes();
            TextSearchModes.First(i => i.Id == (int) TextSearchMode.BeginsWith).IsSelected = true;
        }

        private void ExecuteLoad()
        {
            if (IsInWizardMode)
            {
                var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                navigationViewModel.OcrResult.IfNotNull(i => SearchText = i.InsuranceName);
            }            
        }

        private void ExecuteCreateOrEditInsurancePlan()
        {
            var viewManager = new InsurancePlansSetupViewManager(_interactionManager);
            viewManager.ManageInsurancePlans(new ManageInsurancePlansLoadArguments
                {
                    Filter = SearchText
                });
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteNext()
        {
            if (IsInWizardMode)
            {
                var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                navigationViewModel.InsurerId = SelectedSearchResult.Id;

                var view = new EnterPolicyDetailsView();
                view.InteractionContext = InteractionContext;
                view.DataContext.CastTo<EnterPolicyDetailsViewContext>().InteractionContext = InteractionContext;
                view.DataContext.CastTo<EnterPolicyDetailsViewContext>().NavigableInteractionManager = NavigableInteractionManager;

                var args = new NavigableInteractionArguments
                {
                    Content = new EnterPolicyDetailsView(),
                    Direction = Direction.Forward
                };

                NavigableInteractionManager.Show(args);
                return;
            }

            InteractionContext.Complete(true);
        }

        private void ExecuteBack()
        {
            if (IsInWizardMode)
            {
                NavigableInteractionManager.ShowPrevious();
            }
        }

        private void ExecuteSearch()
        {
            SelectedSearchResult = null;
            HighlightTermList = new[] { SearchText }.ToExtendedObservableCollection();
// ReSharper disable PossibleInvalidOperationException
// Already checked if SelectedTextSearchMode has value in command.canexecute
            SearchResults = _searchInsurancePlanViewService.Search(SelectedTextSearchMode.Value, SearchText);
// ReSharper restore PossibleInvalidOperationException
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the search text
        /// </summary>
        public virtual string SearchText { get; set; }

        /// <summary>
        /// Gets or sets the highlight term list.
        /// </summary>
        /// <remarks>
        /// This is needed for the highlight feature in the 'HighlightingTextBlock'.
        /// </remarks>
        [DispatcherThread]
        public virtual ObservableCollection<string> HighlightTermList { get; set; }

        /// <summary>
        /// Gets or sets the collection of text search modes
        /// </summary>
        public virtual ObservableCollection<TextSearchModeViewModel> TextSearchModes { get; set; }

        /// <summary>
        /// Gets the selected text search mode
        /// </summary>
        public virtual TextSearchMode? SelectedTextSearchMode
        {
            get
            {
                if (TextSearchModes != null && TextSearchModes.Any(tsm => tsm.IsSelected))
                {
                    return (TextSearchMode)TextSearchModes.First(tsm => tsm.IsSelected).Id;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the search results
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<InsurerViewModel> SearchResults { get; set; }

        /// <summary>
        /// Gets or sets the selected search result
        /// </summary>
        [DispatcherThread]
        public virtual InsurerViewModel SelectedSearchResult { get; set; }

        /// <summary>
        /// Gets the command to search for insurance plans
        /// </summary>
        public ICommand Search { get; protected set; }

        /// <summary>
        /// Gets the command to create or edit an insurance plan
        /// </summary>
        public ICommand CreateOrEditInsurancePlan { get; protected set; }

        /// <summary>
        /// Gets the command to navigate back
        /// </summary>
        public ICommand Back { get; protected set; }

        /// <summary>
        /// Gets the command to navigate forward
        /// </summary>
        public ICommand Next { get; protected set; }

        /// <summary>
        /// Gets the command to cancel out of the current instance
        /// </summary>
        public ICommand Cancel { get; protected set; }

        /// <summary>
        /// Gets the command to load
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// To display Search Screen from vb6
        /// </summary>
        public Boolean IsInWizardMode
        {
            get { return InteractionContext is IInteractionContext<InsurancePolicyNavigationViewModel>; }
        }
    }
}