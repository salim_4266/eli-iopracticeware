﻿using System.Windows;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for ScanAndReadUserControl.xaml
    /// </summary>
    public partial class ScanAndReadUserControl
    {
        public static readonly DependencyProperty TargetToClearProperty = DependencyProperty.Register("TargetToClear", typeof(object), typeof(ScanAndReadUserControl));
        public static readonly DependencyProperty TargetPropertyNameProperty = DependencyProperty.Register("TargetPropertyName", typeof(string), typeof(ScanAndReadUserControl));
        public static readonly DependencyProperty LabelTextProperty = DependencyProperty.Register("LabelText", typeof(string), typeof(ScanAndReadUserControl));
        
        public ScanAndReadUserControl()
        {
            InitializeComponent();
        }

        public object TargetToClear
        {
            get { return GetValue(TargetToClearProperty); }
            set { SetValue(TargetToClearProperty, value); }
        }

        public string TargetPropertyName
        {
            get { return (string)GetValue(TargetPropertyNameProperty); }
            set { SetValue(TargetPropertyNameProperty, value); }
        }

        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }
    }
}
