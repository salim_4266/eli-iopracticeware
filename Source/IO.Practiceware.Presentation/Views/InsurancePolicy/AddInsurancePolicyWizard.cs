﻿using System;
using System.Collections.Generic;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.Views.NonClinicalPatient;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Presentation.Views.PatientWizard;
using IO.Practiceware.Presentation.Views.Wizard;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{


    /// <summary>
    /// Wizard to add a new insurance policy to a patient
    /// </summary>
    public class AddInsurancePolicyWizardManager
    {
        private readonly Func<PatientRelationshipViewContext> _createRelationshipViewContext;
        private readonly Func<MiniPatientSearchViewContext> _createMiniPatientSearchViewContext;
        private readonly Func<NonClinicalPatientDetailsViewContext> _createNonClinicalPatientDetailsViewContext;
        private readonly Func<PatientDemographicsViewContext> _createPatientDemographicsViewContext;
        private readonly Func<AddPolicySelectionViewContext> _createAddPolicySelectionViewContext;
        private readonly INavigableInteractionManager _navigableInteractionManager;
        private readonly Func<IInteractionContext<InsurancePolicyNavigationViewModel>> _createInteractionContext;

        public AddInsurancePolicyWizardManager(
            Func<PatientRelationshipViewContext> createRelationshipViewContext,
            Func<MiniPatientSearchViewContext> createMiniPatientSearchViewContext,
            Func<NonClinicalPatientDetailsViewContext> createNonClinicalPatientDetailsViewContext,
            Func<PatientDemographicsViewContext> createPatientDemographicsViewContext,
            Func<AddPolicySelectionViewContext> createAddPolicySelectionViewContext,
            INavigableInteractionManager navigableInteractionManager,
            Func<IInteractionContext<InsurancePolicyNavigationViewModel>> createInteractionContext)
        {
            _createRelationshipViewContext = createRelationshipViewContext;
            _createMiniPatientSearchViewContext = createMiniPatientSearchViewContext;
            _createNonClinicalPatientDetailsViewContext = createNonClinicalPatientDetailsViewContext;
            _createPatientDemographicsViewContext = createPatientDemographicsViewContext;
            _createAddPolicySelectionViewContext = createAddPolicySelectionViewContext;
            _navigableInteractionManager = navigableInteractionManager;
            _createInteractionContext = createInteractionContext;
        }

        /// <summary>
        /// Displays a wizard to search for, create/edit, select insurance policy for patient
        /// </summary>
        /// <param name="patientId"></param>        
        /// <param name="insuranceType"></param>
        /// <returns></returns>
        public InsurancePolicyReturnArguments Show(int patientId, InsuranceType insuranceType)
        {
            var navigationViewModel = new InsurancePolicyNavigationViewModel();

            navigationViewModel.PatientId = patientId;
            navigationViewModel.InsuranceType = insuranceType;

            var interactionContext = _createInteractionContext();
            interactionContext.Context = navigationViewModel;

            var relationshipViewContext = _createRelationshipViewContext();
            var miniPatientSearchViewContext = _createMiniPatientSearchViewContext();
            var nonClinicalPatientDetailsViewContext = _createNonClinicalPatientDetailsViewContext();
            var patientDemographicsViewContext = _createPatientDemographicsViewContext();
            var addPolicySelectionViewContext = _createAddPolicySelectionViewContext();

            patientDemographicsViewContext.Wizard = new PatientDemographicsWizardContext(_navigableInteractionManager, interactionContext, patientDemographicsViewContext);
            nonClinicalPatientDetailsViewContext.Wizard = new NonClinicalPatientDetailsWizardContext(_navigableInteractionManager, interactionContext, nonClinicalPatientDetailsViewContext);
            miniPatientSearchViewContext.Wizard = new MiniPatientSearchWizardContext(_navigableInteractionManager, interactionContext, miniPatientSearchViewContext, nonClinicalPatientDetailsViewContext, patientDemographicsViewContext);
            relationshipViewContext.Wizard = new PatientRelationshipWizardContext(_navigableInteractionManager, interactionContext, relationshipViewContext, miniPatientSearchViewContext);
            addPolicySelectionViewContext.Wizard = new AddPolicySelectionWizardContext(_navigableInteractionManager, interactionContext, addPolicySelectionViewContext, relationshipViewContext);

            var view = new AddPolicySelectionView();
            view.InteractionContext = interactionContext;
            view.DataContext = addPolicySelectionViewContext;

            var navigableInteractionArgs = new NavigableInteractionArguments
            {
                Content = view,
                Direction = Direction.Forward,
                InteractionContext = interactionContext
            };

            _navigableInteractionManager.Show(navigableInteractionArgs);

            return new InsurancePolicyReturnArguments
            {
                NewPolicyHasBeenCreated = interactionContext.Context.PolicySuccessfullyCreated
            };
        }
    }

    /// <summary>
    /// A container class that provides custom logic for the PatientRelationshipView as it pertains to the wizard.
    /// I.E, it contains the logic for passing data onto other screens in the wizard, what to do on the Next, Back, Cancel commands. etc.
    /// </summary>
    public class AddPolicySelectionWizardContext : WizardDataContext<InsurancePolicyNavigationViewModel, AddPolicySelectionViewContext>
    {
        private readonly PatientRelationshipViewContext _patientRelationshipViewContext;

        public AddPolicySelectionWizardContext(INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext<InsurancePolicyNavigationViewModel> interactionContext,
                                        AddPolicySelectionViewContext addPolicySelectionViewContext,
                                        PatientRelationshipViewContext patientRelationshipViewContext
                                        )
            : base(navigableInteractionManager, interactionContext, addPolicySelectionViewContext, interactionContext.Context)
        {
            _patientRelationshipViewContext = patientRelationshipViewContext;
        }


        protected override void ExecuteNext()
        {
            if (ParentViewContext.ScanSelection == AddPolicyScanSelection.None) return;

            if (ParentViewContext.ScanSelection == AddPolicyScanSelection.NoScanManual)
            {
                var view = new PatientRelationshipView();
                view.DataContext = _patientRelationshipViewContext;

                var args = new NavigableInteractionArguments
                {
                    Content = view,
                    Direction = Direction.Forward
                };
                NavigableInteractionManager.Show(args);
            }

            if (ParentViewContext.ScanSelection == AddPolicyScanSelection.ScanAutoEntry)
            {
                var view = new ScanAndReadView();
                view.InteractionContext = InteractionContext;
                view.DataContext.CastTo<ScanAndReadViewContext>().InteractionContext = InteractionContext;
                view.DataContext.CastTo<ScanAndReadViewContext>().NavigableInteractionManager = NavigableInteractionManager;
                view.DataContext.CastTo<ScanAndReadViewContext>().PatientRelationshipViewContext = _patientRelationshipViewContext;
                var args = new NavigableInteractionArguments
                {
                    Content = view,
                    Direction = Direction.Forward,
                };
                NavigableInteractionManager.Show(args);
            }

            if (ParentViewContext.ScanSelection == AddPolicyScanSelection.ScanManual)
            {
                var view = new ScanDocumentView();
                view.InteractionContext = InteractionContext;
                view.DataContext.CastTo<ScanDocumentViewContext>().InteractionContext = InteractionContext;
                view.DataContext.CastTo<ScanDocumentViewContext>().NavigableInteractionManager = NavigableInteractionManager;
                view.DataContext.CastTo<ScanDocumentViewContext>().PatientRelationshipViewContext = _patientRelationshipViewContext;
                var args = new NavigableInteractionArguments
                {
                    Content = view,
                    Direction = Direction.Forward,
                };
                NavigableInteractionManager.Show(args);
            }

        }
    }

    /// <summary>
    /// A container class that provides custom logic for the PatientRelationshipView as it pertains to the wizard.
    /// I.E, it contains the logic for passing data onto other screens in the wizard, what to do on the Next, Back, Cancel commands. etc.
    /// </summary>
    public class PatientRelationshipWizardContext : WizardDataContext<InsurancePolicyNavigationViewModel, PatientRelationshipViewContext>
    {
        private readonly MiniPatientSearchViewContext _miniPatientSearchViewContext;

        public PatientRelationshipWizardContext(INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext<InsurancePolicyNavigationViewModel> interactionContext,
                                        PatientRelationshipViewContext patientRelationshipViewContext,
                                        MiniPatientSearchViewContext miniPatientSearchViewContext
                                        )
            : base(navigableInteractionManager, interactionContext, patientRelationshipViewContext, interactionContext.Context)
        {
            _miniPatientSearchViewContext = miniPatientSearchViewContext;
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientRelationshipType.GetDisplayName();
            ParentViewContext.ShowRelationshipDescription = false;
            ParentViewContext.ShowListOfRelationshipTypes = true;
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.SelectedRelationshipTypeId.HasValue;
        }

        protected override void ExecuteNext()
        {
            NavigationViewModel.RelationshipType = (PolicyHolderRelationshipType)ParentViewContext.SelectedRelationshipTypeId.EnsureNotDefault();

            if (NavigationViewModel.RelationshipType == PolicyHolderRelationshipType.Self)
            {
                NavigationViewModel.PolicyholderId = NavigationViewModel.PatientId;

                var view = new SearchInsurancePlanView();
                view.InteractionContext = InteractionContext;
                view.DataContext.CastTo<SearchInsurancePlanViewContext>().InteractionContext = InteractionContext;
                view.DataContext.CastTo<SearchInsurancePlanViewContext>().NavigableInteractionManager = NavigableInteractionManager;

                var args = new NavigableInteractionArguments
                {
                    Direction = Direction.Forward,
                    Content = view,
                };
                NavigableInteractionManager.Show(args);
            }
            else
            {
                var miniSearchView = new MiniPatientSearchView { AutoCreateDataContext = false };
                miniSearchView.DataContext = _miniPatientSearchViewContext;

                var navigableInteractionArgs = new NavigableInteractionArguments
                {
                    Content = miniSearchView,
                    Direction = Direction.Forward,
                };

                NavigableInteractionManager.Show(navigableInteractionArgs);
            }
        }
    }

    /// <summary>
    /// A container class that provides custom logic for the MiniPatientSearchView as it pertains to the wizard.
    /// I.E, it contains the logic for passing data onto other screens in the wizard, what to do on the Next, Back, Cancel commands. etc.
    /// </summary>
    public class MiniPatientSearchWizardContext : WizardDataContext<InsurancePolicyNavigationViewModel, MiniPatientSearchViewContext>
    {
        private readonly NonClinicalPatientDetailsViewContext _nonClinicalPatientDetailsViewContext;
        private readonly PatientDemographicsViewContext _patientDemographicsViewContext;

        public MiniPatientSearchWizardContext(
                                        INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext<InsurancePolicyNavigationViewModel> interactionContext,
                                        MiniPatientSearchViewContext miniPatientSearchViewContext,
                                        NonClinicalPatientDetailsViewContext nonClinicalPatientDetailsViewContext,
                                        PatientDemographicsViewContext patientDemographicsViewContext)
            : base(navigableInteractionManager, interactionContext, miniPatientSearchViewContext, interactionContext.Context)
        {
            _nonClinicalPatientDetailsViewContext = nonClinicalPatientDetailsViewContext;
            _patientDemographicsViewContext = patientDemographicsViewContext;
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientRelationshipType.GetDisplayName();
            ParentViewContext.OnFilterSearchResults += OnFilterSearchResults;
            Utility = Command.Create(ExecuteCreateEditPolicyholderDetails, CanExecuteCreateEditPolicyholderDetails);

        }

        protected void OnFilterSearchResults(IList<ViewModels.PatientSearch.PatientSearchPatientViewModel> searchResults)
        {
            // current patient should not show up in the list.
            if (Application.ApplicationContext.Current.PatientId.HasValue)
            {
                searchResults.RemoveWhere(sr => sr.Id == Application.ApplicationContext.Current.PatientId);
            }
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.SearchResults.IsNotNullOrEmpty() && ParentViewContext.SelectedSearchResult != null;
        }

        protected override void ExecuteNext()
        {
            NavigationViewModel.PolicyholderId = ParentViewContext.SelectedSearchResult.Id;

            var args = new NavigableInteractionArguments
            {
                Content = new SelectPoliciesView(),
                Direction = Direction.Forward,
            };
            NavigableInteractionManager.Show(args);
        }

        private bool CanExecuteCreateEditPolicyholderDetails()
        {
            return ParentViewContext.SelectedSearchResult == null ? PermissionId.CreatePolicyholder.PrincipalContextHasPermission() : PermissionId.EditPolicyholder.PrincipalContextHasPermission();
        }

        public void ExecuteCreateEditPolicyholderDetails()
        {
            NavigationViewModel.PolicyholderId = ParentViewContext.SelectedSearchResult != null
               ? (int?)ParentViewContext.SelectedSearchResult.Id
               : null;

            View viewToOpen;

            if (NavigationViewModel.PolicyholderId.HasValue && ParentViewContext.SelectedSearchResult != null && ParentViewContext.SelectedSearchResult.IsClinical)
            {
                _patientDemographicsViewContext.SetPatientInfo(new ViewModels.PatientInfo.PatientInfoViewModel { PatientId = NavigationViewModel.PolicyholderId.Value });
                viewToOpen = new PatientDemographicsView { AutoCreateDataContext = false };
                var pdView = (PatientDemographicsView)viewToOpen;
                pdView.Load = _patientDemographicsViewContext.Load; // we have to manually set this up because the Patient Info parent view automatically calls the Demographics Load                 
                viewToOpen.DataContext = _patientDemographicsViewContext;
            }
            else
            {
                _nonClinicalPatientDetailsViewContext.NonClinicalPatientId = NavigationViewModel.PolicyholderId;
                _nonClinicalPatientDetailsViewContext.DefaultAddressPatientId = NavigationViewModel.PatientId;
                viewToOpen = new NonClinicalPatientDetailsView { AutoCreateDataContext = false };
                viewToOpen.DataContext = _nonClinicalPatientDetailsViewContext;
            }

            var navigableInteractionArgs = new NavigableInteractionArguments
            {
                Content = viewToOpen,
                Direction = Direction.Forward,
            };

            NavigableInteractionManager.Show(navigableInteractionArgs);
        }
    }

    /// <summary>
    /// A container class that provides custom logic for the NonClinicalPatientDetailsView as it pertains to the wizard.
    /// I.E, it contains the logic for passing data onto other screens in the wizard, what to do on the Next, Back, Cancel commands. etc.
    /// </summary>
    public class NonClinicalPatientDetailsWizardContext : WizardDataContext<InsurancePolicyNavigationViewModel, NonClinicalPatientDetailsViewContext>
    {
        public NonClinicalPatientDetailsWizardContext(
                                        INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext<InsurancePolicyNavigationViewModel> interactionContext,
                                        NonClinicalPatientDetailsViewContext nonClinicalPatientDetailsViewContext)
            : base(navigableInteractionManager, interactionContext, nonClinicalPatientDetailsViewContext, interactionContext.Context)
        {
            Next = Command.Create(ExecuteNext, CanExecuteNext).Async(() => interactionContext);
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientRelationshipType.GetDisplayName();
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.Save.CanExecute();
        }

        protected override void ExecuteNext()
        {
            bool isEditMode = ParentViewContext.NonClinicalPatientId.HasValue;

            ParentViewContext.SaveNonClinicalPatient();

            if (ParentViewContext.NonClinicalPatientSuccessfullySaved)
            {
                NavigationViewModel.PolicyholderId = ParentViewContext.NonClinicalPatientId;
                ParentViewContext.Dispatcher().Invoke(() =>
                {
                    View view;
                    if (isEditMode)
                    {
                        view = new SelectPoliciesView();
                        view.InteractionContext = InteractionContext;
                        view.DataContext.CastTo<SelectPoliciesViewContext>().InteractionContext = InteractionContext;
                        view.DataContext.CastTo<SelectPoliciesViewContext>().NavigableInteractionManager = NavigableInteractionManager;
                    }
                    else
                    {
                        view = new SearchInsurancePlanView();
                        view.InteractionContext = InteractionContext;
                        view.DataContext.CastTo<SearchInsurancePlanViewContext>().InteractionContext = InteractionContext;
                        view.DataContext.CastTo<SearchInsurancePlanViewContext>().NavigableInteractionManager = NavigableInteractionManager;
                    }


                    var args = new NavigableInteractionArguments
                    {
                        Content = view,
                        Direction = Direction.Forward,
                    };
                    NavigableInteractionManager.Show(args);
                });
            }
        }
    }

    public class PatientDemographicsWizardContext : WizardDataContext<InsurancePolicyNavigationViewModel, PatientDemographicsViewContext>
    {
        public PatientDemographicsWizardContext(INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext<InsurancePolicyNavigationViewModel> interactionContext,
                                        PatientDemographicsViewContext patientDemographicsViewContext)
            : base(navigableInteractionManager, interactionContext, patientDemographicsViewContext, interactionContext.Context)
        {
            ParentViewContext.Load = Command.Composite(true, ParentViewContext.Load, Command.Create(ExecuteLoad));
        }

        private void ExecuteLoad()
        {
            ParentViewContext.PageMode = PatientDemographicsPageMode.EditExistingPatient;
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.Save.CanExecute();
        }

        protected override void ExecuteNext()
        {
            if (ParentViewContext.PatientData == null || ParentViewContext.PatientData.Id <= 0) return;

            var id = ParentViewContext.SavePatient();
            if (!id.HasValue) return;

            if (id.Value != ParentViewContext.PatientData.Id)
            {
                throw new InvalidOperationException("The original selected patient does not match the patient saved");
            }

            NavigationViewModel.PolicyholderId = ParentViewContext.PatientData.Id;

            WizardComplete.Execute();
        }
    }
}
