﻿using IO.Practiceware.Presentation.Views.Imaging;
using IO.Practiceware.Presentation.Views.PatientWizard;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Services.Imaging;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for AddPolicySelectionView.xaml
    /// </summary>
    public partial class AddPolicySelectionView
    {
        public AddPolicySelectionView()
        {
            InitializeComponent();
        }
    }

    public class AddPolicySelectionViewContext : IViewContext, IHasWizardDataContext
    {
        public PatientRelationshipViewContext PatientRelationshipViewContext { get; set; }

        public AddPolicySelectionViewContext(IScanningService scanningService)
        {                        
            ScanManualEntry = Command.Create(ExecuteScanManualEntry, scanningService.CanScanUsingTwain);
            ScanAutoEntry = Command.Create(ExecuteScanAutoEntry, () => IsOcrEnabled);
            NoScanManualEntry = Command.Create(ExecuteNoScanManualEntry);            
            Load = Command.Create(ExecuteLoad);
        }

        private void ExecuteLoad()
        {
            var cardScanner = new CardScanner();
            IsOcrEnabled = cardScanner.CanScan;
        }

        private void ExecuteScanManualEntry()
        {
            ScanSelection = AddPolicyScanSelection.ScanManual;

            if (Wizard != null) Wizard.Next.Execute();
        }

        private void ExecuteNoScanManualEntry()
        {
            ScanSelection = AddPolicyScanSelection.NoScanManual;

            if (Wizard != null) Wizard.Next.Execute();
        }

        private void ExecuteScanAutoEntry()
        {
            ScanSelection = AddPolicyScanSelection.ScanAutoEntry;

            if (Wizard != null) Wizard.Next.Execute();
        }

        /// <summary>
        /// Gets or sets a flag that indicates whether OCR is enabled
        /// </summary>
        public virtual bool IsOcrEnabled { get; set; }
      

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Begins scan with manual entry of insurance card
        /// </summary>
        public ICommand ScanManualEntry { get; protected set; }

        /// <summary>
        /// Begins scan with auto entry of insurance card
        /// </summary>
        public ICommand ScanAutoEntry { get; protected set; }

        /// <summary>
        /// Begins manual entry of insurance card
        /// </summary>
        public ICommand NoScanManualEntry { get; protected set; } 

        /// <summary>
        /// Gets a command to load
        /// </summary>
        public ICommand Load { get; protected set; }         
        public INavigableInteractionManager NavigableInteractionManager
        {
            get;
            set;
        }

        public AddPolicyScanSelection ScanSelection { get; set; }

        [DispatcherThread]
        public IWizardDataContext Wizard { get; set; }

    }

    public enum AddPolicyScanSelection
    {
        None = 0,
        ScanManual,
        NoScanManual,
        ScanAutoEntry,
    }
}