﻿using System.Linq;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanDocument;
using IO.Practiceware.Presentation.Views.PatientWizard;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Direction = Soaf.Presentation.Direction;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for ScanDocumentView.xaml
    /// </summary>
    public partial class ScanDocumentView
    {
        public ScanDocumentView()
        {
            InitializeComponent();
        }
    }

    public class ScanDocumentViewContext : IViewContext
    {
        public PatientRelationshipViewContext PatientRelationshipViewContext { get; set; }
        public INavigableInteractionManager NavigableInteractionManager;
        private readonly IScanningService _scanningService;
        private bool _isLoaded;

        public ScanDocumentViewContext(INavigableInteractionManager navigableInteractionManager,
                                       IScanningService scanningService)
        {
            NavigableInteractionManager = navigableInteractionManager;
            _scanningService = scanningService;
            ScanSettings = GetDefaultScanSettingsViewModel();
            ResetScanSettings = Command.Create(ExecuteResetScanSettings);
            Next = Command.Create(ExecuteNext);
            Back = Command.Create(ExecuteBack);
            Cancel = Command.Create(ExecuteCancel);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            if (!_scanningService.CanScan() && IsInNavigationMode)
            {
                // If we cannot scan, and we 'ShowPrevious' and end up on this screen,
                // do not invoke 'Next' again.  Instead, go 'Back' one more time.
                // (Otherwise we can never navigate back before this screen.)
                if (!_isLoaded)
                {
                    this.Dispatcher().Invoke(() => Next.Execute());
                }
                else
                {
                    this.Dispatcher().Invoke(() => Back.Execute());
                }
            }
            _isLoaded = true;
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteBack()
        {
            NavigableInteractionManager.ShowPrevious();
        }

        private void ExecuteNext()
        {
            if (IsInNavigationMode)
            {
                if (_scanningService.CanScanUsingTwain())
                {
                    var scanSettings = new ScanSettings
                                           {
                                               SelectedScanner = ScanSettings.SelectedScannerName,
                                               ColorSetting = ScanSettings.ColorSetting,
                                               Dpi = ScanSettings.DpiSetting,
                                               DuplexMode = ScanSettings.IsDuplexMode,
                                               PageSize = ScanSettings.PageSize,
                                               Quality = ScanSettings.Quality,
                                               Mode = ScanMode.Twain
                                           };

                    var result = _scanningService.ScanToJpg(scanSettings);
                    if (result.Item1 == ScanReturnStatus.Success)
                    {
                        var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                        navigationViewModel.ScannedDocuments = result.Item2;
                    }
                }

                var view = new PatientRelationshipView();
                view.DataContext = PatientRelationshipViewContext;
                var args = new NavigableInteractionArguments
                               {
                                   Content = view,
                                   Direction = Direction.Forward
                               };
                NavigableInteractionManager.Show(args);
            }
            else
            {
                if (!_scanningService.CanScanUsingTwain())
                {
                    AlertScannerIssueDetectedTrigger = true; // pop up alert message
                    AlertScannerIssueDetectedTrigger = false;

                    InteractionContext.Complete(false);
                    return;
                }

                var scanSettings = new ScanSettings
                {
                    SelectedScanner = ScanSettings.SelectedScannerName,
                    ColorSetting = ScanSettings.ColorSetting,
                    Dpi = ScanSettings.DpiSetting,
                    DuplexMode = ScanSettings.IsDuplexMode,
                    PageSize = ScanSettings.PageSize,
                    Quality = ScanSettings.Quality,
                    Mode = ScanMode.Twain
                };

                var result = _scanningService.ScanToJpg(scanSettings);

                ScannedDocuments = result.Item2;

                InteractionContext.Complete(true);
            }
        }

        private void ExecuteResetScanSettings()
        {
            ScanSettings = GetDefaultScanSettingsViewModel();
        }

        private ScanSettingsViewModel GetDefaultScanSettingsViewModel()
        {
            var list = _scanningService.GetTwainScannerNames().ToObservableCollection();
            return new ScanSettingsViewModel
                {
                    ScannerNames = list,
                    SelectedScannerName = list.FirstOrDefault(),
                    ColorSetting = ColorSetting.Color,
                    DpiSetting = DpiSetting.Medium,
                    IsDuplexMode = false,
                    PageSize = PageSize.Card
                };
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets the scan settings
        /// </summary>
        public virtual ScanSettingsViewModel ScanSettings { get; protected set; }

        public IEnumerable<Byte[]> ScannedDocuments { get; set; }

        /// <summary>
        /// Gets or sets a flag whether this instance is navigable
        /// </summary>
        [DispatcherThread]
        public virtual bool IsInNavigationMode
        {
            get { return InteractionContext.Is<IInteractionContext<InsurancePolicyNavigationViewModel>>(); }
        }

        /// <summary>
        /// Triggers an alert command informing the user that there is a problem with the scanner
        /// </summary>
        [DispatcherThread]
        public virtual bool AlertScannerIssueDetectedTrigger { get; set; }

        /// <summary>
        /// Gets or sets the selected page size.
        /// </summary>
        public virtual PageSize SelectedPageSize { get; set; }

        /// <summary>
        /// Gets the command to reset the scan settings
        /// </summary>
        public virtual ICommand ResetScanSettings { get; protected set; }

        /// <summary>
        /// Gets the command to move to the next screen
        /// </summary>
        public virtual ICommand Next { get; protected set; }

        /// <summary>
        /// Gets the command to move to the previous screen
        /// </summary>
        public virtual ICommand Back { get; protected set; }

        /// <summary>
        /// Gets the command to cancel scanning a document
        /// </summary>
        public virtual ICommand Cancel { get; protected set; }

        /// <summary>
        /// Gets the command to load
        /// </summary>
        public virtual ICommand Load { get; protected set; }
    }
}