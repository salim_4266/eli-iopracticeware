﻿using System.ComponentModel;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SelectPolicies;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for SelectPoliciesView.xaml
    /// </summary>
    public partial class SelectPoliciesView
    {
        public SelectPoliciesView()
        {
            InitializeComponent();
        }
    }

    public class SelectPoliciesViewContext : IViewContext
    {
        public INavigableInteractionManager NavigableInteractionManager {get;set;}
        private readonly ISelectPoliciesViewService _selectPoliciesViewService;

        public SelectPoliciesViewContext(INavigableInteractionManager navigableInteractionManager, ISelectPoliciesViewService selectPoliciesViewService)
        {
            NavigableInteractionManager = navigableInteractionManager;
            _selectPoliciesViewService = selectPoliciesViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            CreateOrEditInsurancePolicy = Command.Create(ExecuteCreateOrEditInsurancePolicy, () => PermissionId.AddPatientInsuranceForOther.PrincipalContextHasPermission());
            Back = Command.Create(ExecuteBack);
            Cancel = Command.Create(ExecuteCancel);
            Next = Command.Create(ExecuteNext, CanExecuteSave).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CanExecuteSave).Async(() => InteractionContext);
        }

        private void ExecuteSave()
        {
            var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
            var patientId = navigationViewModel.PatientId;
            var relationshipType = navigationViewModel.RelationshipType;
            var scannedDocuments = navigationViewModel.ScannedDocuments;

            var changedPatientInsurances = PatientInsurances.Cast<IChangeTracking>().Where(pi => pi.IsChanged).Cast<PatientInsuranceViewModel>();

            var warningMessage = _selectPoliciesViewService.LinkPolicies(patientId, relationshipType, changedPatientInsurances.Select(i => i.InsurancePolicyId), scannedDocuments);
            if (!warningMessage.IsNullOrEmpty())
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert(warningMessage));                
            }

            navigationViewModel.PolicySuccessfullyCreated = true;
        }

        private bool CanExecuteSave()
        {
            return PatientInsurances != null && PatientInsurances.Cast<IChangeTracking>().Any(pi => pi.IsChanged);
        }

        private void ExecuteNext()
        {
            Save.Execute();
            InteractionContext.Complete(true);
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteBack()
        {
            NavigableInteractionManager.ShowPrevious();
        }

        private void ExecuteCreateOrEditInsurancePolicy()
        {
            var args = new NavigableInteractionArguments
            {
                Content = new SearchInsurancePlanView(),
                Direction = Direction.Forward,
            };
            NavigableInteractionManager.Show(args);
        }

        private void ExecuteLoad()
        {
            var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
            var policyholderId = navigationViewModel.PolicyholderId;
            var patientId = navigationViewModel.PatientId;
            if (!policyholderId.HasValue) { throw new Exception("Policyholder Id not set."); }

            var loadInformation = _selectPoliciesViewService.GetLoadInformation(policyholderId.Value, patientId, navigationViewModel.InsuranceType);
            Policyholder = loadInformation.Policyholder;

            if (loadInformation.Policyholder.Id != patientId && !loadInformation.PatientInsurances.Any())
            {
                InteractionManager.Current.Alert(String.Format("We're sorry the insurance for {0} does not cover dependants.", Policyholder.FirstName + " " + Policyholder.LastName));
            }
            else
            {
                PatientInsurances = loadInformation.PatientInsurances.ToExtendedObservableCollection();
            }
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the policyholder
        /// </summary>
        [DispatcherThread]
        public virtual PersonViewModel Policyholder { get; set; }

        /// <summary>
        /// Gets or sets the patient insurances
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<PatientInsuranceViewModel> PatientInsurances { get; set; }

        /// <summary>
        /// Gets the command to load
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Gets the command to create or edit an insurance policy
        /// </summary>
        public ICommand CreateOrEditInsurancePolicy { get; protected set; }

        /// <summary>
        /// Gets or sets the command to navigate to the next screen.
        /// </summary>
        public ICommand Next { get; protected set; }

        /// <summary>
        /// Gets the command to navigate back
        /// </summary>
        public ICommand Back { get; protected set; }

        /// <summary>
        /// Gets the command to cancel
        /// </summary>
        public ICommand Cancel { get; protected set; }

        /// <summary>
        /// Gets the command to save
        /// </summary>
        public ICommand Save { get; protected set; }
    }
}