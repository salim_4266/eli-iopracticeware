﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanAndRead;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using IO.Practiceware.Presentation.Views.PatientWizard;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for ScanAndReadView.xaml
    /// </summary>
    public partial class ScanAndReadView
    {
        public ScanAndReadView()
        {
            InitializeComponent();
        }
    }

    public class ScanAndReadViewContext : IViewContext
    {
        private readonly IScanAndReadViewService _scanAndReadViewService;
        public INavigableInteractionManager NavigableInteractionManager {get;set;}
        private ObservableCollection<CardScannerDataModel> _processedData;
        public PatientRelationshipViewContext PatientRelationshipViewContext { get; set; }

        public ScanAndReadViewContext(IScanAndReadViewService scanAndReadViewService,
                                      INavigableInteractionManager navigableInteractionManager)
        {
            _scanAndReadViewService = scanAndReadViewService;
            NavigableInteractionManager = navigableInteractionManager;
            ProcessScanResults = Command.Create(ExecuteProcessScanResults).Async(() => InteractionContext);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Back = Command.Create(ExecuteBack);
            Next = Command.Create(ExecuteNext);
            Cancel = Command.Create(ExecuteCancel);
        }

        private void ExecuteNext()
        {
            var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
            navigationViewModel.ScannedDocuments = new [] { ScannedImageData };
            navigationViewModel.OcrResult = OcrInsurancePolicyViewModel;


            var view = new PatientRelationshipView();
            view.DataContext = PatientRelationshipViewContext;

            // Not sure how to get relationship from ocr...
            // navigate to "relationship to policyholder screen"
            var args = new NavigableInteractionArguments
            {
                Content = view,
                Direction = Direction.Forward
            };
            NavigableInteractionManager.Show(args);
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteBack()
        {
            NavigableInteractionManager.ShowPrevious();
        }

        private void ExecuteLoad()
        {
            var loadInfo = _scanAndReadViewService.GetLoadInformation();
            PlanTypes = loadInfo.PlanTypes;
            PolicyholderRelationshipTypes = loadInfo.PolicyholderRelationshipTypes;
        }

        private void ExecuteProcessScanResults()
        {
            OcrInsurancePolicyViewModel = _scanAndReadViewService.GetViewModel(ProcessedData);
        }

        /// <summary>
        /// Gets or setes the interaction context
        /// </summary>
        public virtual IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the plan types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> PlanTypes { get; set; }

        /// <summary>
        /// Gets or sets the policyholder relationship types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> PolicyholderRelationshipTypes { get; set; }

        /// <summary>
        /// Gets or sets the view model representing the processed data from the card scanner
        /// </summary>
        [DispatcherThread]
        public virtual OcrInsurancePolicyViewModel OcrInsurancePolicyViewModel { get; set; }

        /// <summary>
        /// Gets or sets the processed data from the card scanner
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<CardScannerDataModel> ProcessedData
        {
            get { return _processedData; }
            set
            {
                _processedData = value;
                ProcessScanResults.Execute();
            }
        }

        /// <summary>
        /// Gets or sets the scanned image data from the card scanner
        /// </summary>
        public virtual byte[] ScannedImageData { get; set; }

        /// <summary>
        /// Gets or sets the scanner name
        /// </summary>
        public virtual string ScannerName { get; set; }

        /// <summary>
        /// Gets a command to process the scan results
        /// </summary>
        public ICommand ProcessScanResults { get; protected set; }

        /// <summary>
        /// Gets the command to load
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Gets the command to navigate back
        /// </summary>
        public ICommand Back { get; protected set; }

        /// <summary>
        /// Gets the command to navigate forward
        /// </summary>
        public ICommand Next { get; protected set; }

        /// <summary>
        /// Gets the command to cancel
        /// </summary>
        public ICommand Cancel { get; protected set; }
    }
}