﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.EnterPolicyDetails;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePolicy
{
    /// <summary>
    /// Interaction logic for EnterPolicyDetails.xaml
    /// </summary>
    public partial class EnterPolicyDetailsView
    {
        public EnterPolicyDetailsView()
        {
            InitializeComponent();
        }
    }

    public class EnterPolicyDetailsViewContext : IViewContext
    {
        public INavigableInteractionManager NavigableInteractionManager;
        private readonly IEnterPolicyDetailsViewService _enterPolicyDetailsViewService;

        public EnterPolicyDetailsViewContext(INavigableInteractionManager navigableInteractionManager,
                                             IEnterPolicyDetailsViewService enterPolicyDetailsViewService)
        {
            NavigableInteractionManager = navigableInteractionManager;
            _enterPolicyDetailsViewService = enterPolicyDetailsViewService;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Back = Command.Create(ExecuteBack);
            Next = Command.Create(ExecuteSave, () => Save.CanExecute(null)).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, () => InsurancePolicy != null && InsurancePolicy.IsValid()).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel);
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteSave()
        {
            // ensure that PolicyCode has no white-spaces
            InsurancePolicy.PolicyCode = InsurancePolicy.PolicyCode.IfNotNull(x => x.Trim());

            if (IsInNavigationMode)
            {
                var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                var patientId = navigationViewModel.PatientId;
                var insuranceType = navigationViewModel.InsuranceType;
                var scannedDocument = navigationViewModel.ScannedDocuments;
                var relationshipType = navigationViewModel.RelationshipType;

                _enterPolicyDetailsViewService.SaveNewInsurancePolicy(InsurancePolicy, patientId, insuranceType, scannedDocument, relationshipType);

                navigationViewModel.PolicySuccessfullyCreated = true;
            }
            else
            {
                _enterPolicyDetailsViewService.SaveExistingInsurancePolicy(InsurancePolicy);
                ExistingPolicyHasBeenEdited = true;
            }

            InteractionContext.Complete(true);
        }

        private void ExecuteBack()
        {
            NavigableInteractionManager.ShowPrevious();
        }

        private void ExecuteLoad()
        {
            IsInNavigationMode = InteractionContext.Is<IInteractionContext<InsurancePolicyNavigationViewModel>>();
            if (IsInNavigationMode)
            {
                var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                if (navigationViewModel.InsurerId.HasValue) { InsurerId = navigationViewModel.InsurerId.Value; }
                if (navigationViewModel.PolicyholderId.HasValue) { PolicyholderId = navigationViewModel.PolicyholderId.Value; }
            }

            if (InsurerId == default(int)) { throw new Exception("Insurer Id not set."); }
            if (PolicyholderId == default(int)) { throw new Exception("Policyholder Id not set."); }

            var loadInformation = _enterPolicyDetailsViewService.GetLoadInformation(PolicyholderId, InsurerId, InsurancePolicyId);
            InsurancePolicy = loadInformation.InsurancePolicy;
            Insurer = loadInformation.Insurer;
            Policyholder = loadInformation.Policyholder;


            if (IsInNavigationMode)
            {
                var navigationViewModel = InteractionContext.EnsureType<IInteractionContext<InsurancePolicyNavigationViewModel>>().Context;
                var ocrResult = navigationViewModel.OcrResult;
                if (ocrResult != null)
                {
                    InsurancePolicy.CoPay = ocrResult.CoPay;
                    InsurancePolicy.Deductible = ocrResult.Deductible;
                    InsurancePolicy.EndDate = ocrResult.EndDate;
                    InsurancePolicy.GroupCode = ocrResult.GroupCode;
                    InsurancePolicy.PolicyCode = ocrResult.PolicyCode;
                    InsurancePolicy.StartDate = ocrResult.StartDate;
                }
            }

            if (InsurancePolicy.StartDate == null) { InsurancePolicy.StartDate = DateTime.Today; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the existing policy has been edited.
        /// Used by the InsurancePolicyReturnArguments.
        /// </summary>
        public bool ExistingPolicyHasBeenEdited { get; set; }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the Insurance Policy Id
        /// </summary>
        [DispatcherThread]
        public virtual int? InsurancePolicyId { get; set; }

        /// <summary>
        /// Gets or sets the Insurer Id
        /// </summary>
        public virtual int InsurerId { get; set; }

        /// <summary>
        /// Gets or sets the Policyholder Id
        /// </summary>
        public virtual int PolicyholderId { get; set; }

        /// <summary>
        /// Gets or sets the insurance policy
        /// </summary>
        [DispatcherThread]
        public virtual InsurancePolicyViewModel InsurancePolicy { get; set; }

        /// <summary>
        /// Gets or sets the policyholder
        /// </summary>
        [DispatcherThread]
        public virtual PersonViewModel Policyholder { get; set; }

        /// <summary>
        /// Gets or sets the insurer
        /// </summary>
        [DispatcherThread]
        public virtual BasicInsurerViewModel Insurer { get; set; }

        /// <summary>
        /// Gets or sets a flag whether this instance is navigable
        /// </summary>
        [DispatcherThread]
        public virtual bool IsInNavigationMode { get; set; }

        /// <summary>
        /// Gets the command to load
        /// </summary>
        public virtual ICommand Load { get; protected set; }

        /// <summary>
        /// Gets the command to navigate back
        /// </summary>
        public virtual ICommand Back { get; protected set; }

        /// <summary>
        /// Gets the command to navigate forward
        /// </summary>
        public virtual ICommand Next { get; protected set; }

        /// <summary>
        /// Gets the command to cancel
        /// </summary>
        public virtual ICommand Cancel { get; protected set; }

        /// <summary>
        /// Gets the command to save
        /// </summary>
        public virtual ICommand Save { get; protected set; }
    }
}