﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Referrals;
using IO.Practiceware.Presentation.ViewServices.Referrals;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Referrals
{
    /// <summary>
    /// Interaction logic for ReferralsView.xaml
    /// </summary>
    public partial class ReferralsView
    {
        public ReferralsView()
        {
            InitializeComponent();
        }
    }


    public class ReferralsViewContext : IViewContext
    {

        private readonly IReferralsViewService _referralsViewService;
        private readonly Func<ReferralDetailsViewModel> _createReferralDetailsViewModel;
        private ReferralDetailsViewModel _referralDetails;

        public ReferralsViewContext(IReferralsViewService referralsViewService, Func<ReferralDetailsViewModel> createReferralDetailsViewModel)
        {
            _referralsViewService = referralsViewService;
            _createReferralDetailsViewModel = createReferralDetailsViewModel;

            EditedReferralIds = new List<int>();
            NewReferralIds = new List<int>();

            Close = Command.Create(ExecuteClose);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, ButtonsCanExecute).Async(() => InteractionContext);
            Apply = Command.Create(ExecuteApply, ButtonsCanExecute).Async(() => InteractionContext);
            // Needs to be Async because it calls AddSelectionListToViewModel which uses the View Service
            Clear = Command.Create(ExecuteClear).Async(() => InteractionContext);
            AcceptDiscardUnsavedChangesOnRowSelection = Command.Create(ExecuteAcceptDiscardUnsavedChangesOnRowSelection);
        }

        private void ExecuteApply()
        {
            bool isNew = !ReferralDetails.ReferralId.HasValue;
            var savedReferral = SaveData();

            SyncDoctorNameToReferralDetail(ReferralDetails);
            ReferralDetails.CastTo<IEditableObject>().EndEdit();
            ReferralDetails.CastTo<IChangeTracking>().AcceptChanges();

            var referralHistory = ReferralHistory.ToList();

            if (isNew && savedReferral)
            {
                AddSelectionListToViewModel(ReferralDetails, referralHistory);
                referralHistory.Add(ReferralDetails);

                ReferralHistory = new ExtendedObservableCollection<ReferralDetailsViewModel>(referralHistory.OrderByDescending(x => x.ExpiresDate));
            }
        }



        private void ExecuteSave()
        {
            if (SaveData())
            {
                LastSavedId = ReferralDetails.ReferralId;
                this.Dispatcher().Invoke(() => InteractionContext.Complete(true));
            }
        }

        private bool SaveData()
        {
            ErrorMessage = null;
            IsExpireDateBeforeStartDate = false;
            HasInvalidStartDate = false;
            HasInvalidExpireDate = false;

            var errorMessages = ReferralDetails.Validate();

            ErrorMessage = errorMessages.Join(x => x.Value, Environment.NewLine);

            IsExpireDateBeforeStartDate = ReferralDetails.StartDate > ReferralDetails.ExpiresDate;
            if (IsExpireDateBeforeStartDate) return false;

            HasInvalidStartDate = _referralsViewService.CheckForInvalidReferralStartDate(ReferralDetails);
            if (HasInvalidStartDate) return false;

            HasInvalidExpireDate = _referralsViewService.CheckForInvalidReferralExpireDate(ReferralDetails);
            if (HasInvalidExpireDate) return false;

            if (string.IsNullOrEmpty(ErrorMessage))
            {
                var savedReferralId = _referralsViewService.Save(ReferralDetails);

                if (!ReferralDetails.ReferralId.HasValue)
                {
                    // this public property is being set as a return argument for screens that need the id of a newly created referral
                    NewReferralIds.Add(savedReferralId);
                }
                else if (!EditedReferralIds.Contains(savedReferralId) && !NewReferralIds.Contains(savedReferralId))
                {
                    EditedReferralIds.Add(savedReferralId);
                }

                ReferralDetails.ReferralId = savedReferralId;

                return true;
            }

            return false;
        }

        public bool ButtonsCanExecute()
        {
            if (ReferralDetails == null) return false;
            // If existing referral then only allow save on edits 
            if (ReferralDetails.ReferralId.HasValue)
            {
                if (NewReferralIds.Any() && ReferralHistory.Count == 1) return true;

                // ReSharper disable once PossibleInvalidOperationException
                var existingReferral = ReferralHistory.FirstOrDefault(r => r.ReferralId.Value == ReferralDetails.ReferralId.Value);
                if (existingReferral != null) return ReferralDetails.CastTo<IChangeTracking>().IsChanged;
            }
            // If new referral all required fields MUST be populated
            return ReferralDetails.IsValid();
        }

        private void ExecuteClear()
        {
            ReferralDetails = null;
            if (ReferralDetails == null && PatientId.HasValue)
            {
                var referralDetails = _createReferralDetailsViewModel();
                referralDetails.PatientId = PatientId.Value;
                AddSelectionListToViewModel(referralDetails, ReferralHistory.ToList());

                ReferralDetails = referralDetails;
            }
        }

        private void ExecuteLoad()
        {
            ExtendedObservableCollection<ReferralDetailsViewModel> referralHistory;
            ReferralDetailsViewModel referralDetails;

            Doctors = _referralsViewService.LoadDoctors();

            // When Editing an existing Referral i.e. Patient Financials > Edit Invoice > Referral > Edit Referral
            // Test Client > Enter Appointment Date > Enter Referral Id
            if (ReferralId.HasValue)
            {
                referralHistory = new ExtendedObservableCollection<ReferralDetailsViewModel>(_referralsViewService.LoadReferralDetailsByReferralId(ReferralId.Value, AppointmentDate).OrderByDescending(x => x.ExpiresDate));
                referralDetails = referralHistory.Single(x => x.ReferralId == ReferralId.Value);
                PatientId = referralDetails.PatientId;
                PatientName = referralDetails.PatientName;
            }
            // When attaching a new referral i.e. Patient Financials > Edit Invoice > Referral >  Add New Referral
            // Patient Insurances > Referrals, 
            else if (PatientId.HasValue)
            {
                referralHistory = new ExtendedObservableCollection<ReferralDetailsViewModel>(_referralsViewService.LoadReferralDetailsByPatientId(PatientId.Value, AppointmentDate).OrderByDescending(x => x.ExpiresDate));

                referralDetails = _createReferralDetailsViewModel();
                referralDetails.PatientId = PatientId.Value;
                PatientName = _referralsViewService.GetPatientDisplayName(PatientId.Value);
                AddSelectionListToViewModel(referralDetails, referralHistory);
            }
            else
            {
                throw new ArgumentException("No ReferralId or PatientId found");
            }

            if (!referralDetails.Insurances.Any())
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionManager.Current.Alert("No insurances were found for this patient"));
                InteractionContext.Complete(true);
            }

            if (!referralDetails.StartDate.HasValue) referralDetails.StartDate = DateTime.Now.ToClientTime().Date;

            var externalProviders = _referralsViewService.LoadExternalProviders();

            referralHistory.ForEachWithSinglePropertyChangedNotification(x => x.ReferredToDoctorName = Doctors.FirstOrDefault(y => x.ReferredToDoctorId.HasValue && x.ReferredToDoctorId == y.Id).IfNotNull(z => z.Name, () => ""));

            ReferralHistory = referralHistory;
            ReferralDetails = referralDetails;
            ExternalProviders = externalProviders;
        }
        

        private void AddSelectionListToViewModel(ReferralDetailsViewModel referralDetails, IEnumerable<ReferralDetailsViewModel> referralHistory)
        {
            var localReferralHistory = referralHistory.ToArray();
            // Used when creating a new Referral so all possible Insurances are loaded for the patient based on their Id
            if (!referralDetails.ReferralId.HasValue && PatientId.HasValue)
            {
                var insurances = _referralsViewService.LoadActiveInsurancesForPatient(PatientId.Value, AppointmentDate);
                referralDetails.Insurances = insurances;
            }
            // used when editing an existing referral so only the insurance linked to the referral is loaded, occurs when referral Id is included in referral Details
            else
            {
                if (localReferralHistory.Any()) referralDetails.Insurances = localReferralHistory.First().Insurances;
            }

        }

        private void SyncDoctorNameToReferralDetail(ReferralDetailsViewModel referralDetail)
        {
            referralDetail.ReferredToDoctorName = Doctors.FirstOrDefault(y => referralDetail.ReferredToDoctorId.HasValue && referralDetail.ReferredToDoctorId == y.Id).IfNotNull(z => z.Name, () => "");
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteAcceptDiscardUnsavedChangesOnRowSelection()
        {
            HasUnsavedChangesOnRowSelection = false;

            _referralDetails.CastTo<IEditableObject>().CancelEdit();
            _referralDetails = _newReferralDetails;
            if (_referralDetails != null)
            {
                _referralDetails.CastTo<IEditableObject>().BeginEdit();
                _referralDetails.CastTo<IChangeTracking>().AcceptChanges();
            }

            _newReferralDetails = null;

        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        /// <summary>
        /// Gets or sets the Patient Id
        /// </summary>
        public int? PatientId { get; set; }

        /// <summary>
        /// Gets or sets the patient name
        /// </summary>
        [DispatcherThread]
        public virtual string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the referral Id
        /// </summary>
        public int? ReferralId { get; set; }

        /// <summary>
        /// The ids of all the newly created referrals.
        /// </summary>
        public List<int> NewReferralIds { get; set; }

        /// <summary>
        /// The ids of all the existing edited referrals
        /// </summary>
        public List<int> EditedReferralIds { get; set; }

        /// <summary>
        /// The id of the last 'saved' referral.  only applies when user hit 'save' button, not 'apply' button.
        /// </summary>
        public int? LastSavedId { get; set; }

        /// <summary>
        /// The appointment date this referral will be applied to
        /// </summary>
        public DateTime AppointmentDate { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to save the data
        /// </summary>
        public ICommand Save { get; protected set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to clear the form data
        /// </summary>
        public ICommand Clear { get; protected set; }

        /// <summary>
        /// Command to save the data but not close the form
        /// </summary>
        public ICommand Apply { get; protected set; }

        public ICommand AcceptDiscardUnsavedChangesOnRowSelection { get; protected set; }

        public ICommand RejectDiscardUnsavedChangesOnRowSelection { get; protected set; }

        /// <summary>
        /// Determines whether or not the form is valid
        /// </summary>
        [DispatcherThread]
        [DependsOn("ErrorMessage")]
        public virtual bool HasError { get { return !ErrorMessage.IsNullOrEmpty(); } }

        /// <summary>
        /// Checks whether the start date is within the insurance coverage dates or not
        /// </summary>
        [DispatcherThread]
        public virtual bool HasInvalidStartDate { get; set; }

        /// <summary>
        /// Checks whether the expire date is within the insurance coverage dates or not
        /// </summary>
        [DispatcherThread]
        public virtual bool HasInvalidExpireDate { get; set; }

        /// <summary>
        /// Checks whether the expire date is before the start date
        /// </summary>
        [DispatcherThread]
        public virtual bool IsExpireDateBeforeStartDate { get; set; }

        /// <summary>
        /// The error message if validation fails
        /// </summary>
        [DispatcherThread]
        public virtual string ErrorMessage { get; set; }

        [DispatcherThread]
        public virtual bool HasUnsavedChangesOnRowSelection { get; set; }

        private bool _isRevertingReferralDetails;

        /// <summary>
        /// Member providing 2-way binding for the referral data
        /// </summary>
        [DispatcherThread]
        public virtual ReferralDetailsViewModel ReferralDetails
        {
            get { return _referralDetails; }
            set
            {
                if (_referralDetails != null && !_isRevertingReferralDetails)
                {

                    if (_referralDetails.CastTo<IChangeTracking>().IsChanged)
                    {
                        _newReferralDetails = value;

                        // setting this will fire a confirm command that when accepted will call the ExecuteAcceptDiscardUnsavedChangesOnRowSelection() 
                        // method and change some state before continuing onto the next line
                        HasUnsavedChangesOnRowSelection = true;

                        if (HasUnsavedChangesOnRowSelection)// will be true still if the user clicked cancel on the confirm command
                        {
                            HasUnsavedChangesOnRowSelection = false;
                            _isRevertingReferralDetails = true;
                            var oldValue = _referralDetails;
                            ReferralDetails = null;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
                                                                                          {
                                                                                              ReferralDetails = oldValue;
                                                                                              _isRevertingReferralDetails = false;
                                                                                          });

                        }

                        return;
                    }

                    _referralDetails.CastTo<IEditableObject>().CancelEdit();
                }
                _referralDetails = value;

                if (_referralDetails != null && !_isRevertingReferralDetails)
                {
                    _referralDetails.CastTo<IEditableObject>().BeginEdit();
                    _referralDetails.CastTo<IChangeTracking>().AcceptChanges();
                }

            }
        }

        private ReferralDetailsViewModel _newReferralDetails;

        /// <summary>
        /// A list of Referrals to be displayed in a grid
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ReferralDetailsViewModel> ReferralHistory { get; set; }

        [DispatcherThread]
        [DependsOn("ReferralHistory")]
        public virtual Visibility ShowReferralHistory
        {
            get
            {
                if (ReferralHistory == null || ReferralHistory.Count == 0)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }

        [DispatcherThread]
        [DependsOn("ReferralHistory")]
        public virtual Visibility ShowReferralHistoryNoData
        {
            get
            {
                if (ReferralHistory == null || ReferralHistory.Count == 0)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
        }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel<int>> ExternalProviders { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel<int>> Doctors { get; set; }
    }
}

