﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Referrals
{
    public class ReferralsViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private ReferralsViewContext _dataContext;

        public ReferralsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public ReferralsReturnArguments ShowReferrals(ReferralLoadArguments loadArguments)
        {
            var view = new ReferralsView();            
            _dataContext =  view.DataContext.EnsureType<ReferralsViewContext>();
            _dataContext.ReferralId = loadArguments.ReferralId;
            _dataContext.PatientId = loadArguments.PatientId;
            if (loadArguments.AppointmentDate.Equals(DateTime.MinValue))
            {
                throw new ArgumentException("The appointment date must be set to a valid value in the load arguments.");
            }
            _dataContext.AppointmentDate = loadArguments.AppointmentDate;
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = "Referrals" });
           
            var returnArguments = new ReferralsReturnArguments
                                      {
                                          NewReferralIds = _dataContext.NewReferralIds,
                                          EditedReferralIds = _dataContext.EditedReferralIds,
                                          LastSavedId = _dataContext.LastSavedId
                                      };
            return returnArguments;          
        }
    }


    public class ReferralLoadArguments
    {
        public int? ReferralId { get; set; }
        public int? PatientId { get; set; }
        public DateTime AppointmentDate { get; set; }
    }

    public class ReferralsReturnArguments
    {
        /// <summary>
        /// Will contain a value if a new referral was created
        /// </summary>
        public List<int> NewReferralIds { get; set; }

        /// <summary>
        /// Contains the list of referrals that were edited on the referrals screen.
        /// </summary>
        public List<int> EditedReferralIds { get; set; }

        /// <summary>
        /// Contains the last id that was saved when the user hit the 'Save' button (not the 'apply' button).
        /// </summary>
        public int? LastSavedId { get; set; }
    }
}
