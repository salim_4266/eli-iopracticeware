﻿using IO.Practiceware.Integration.Omedix;
using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.SecureMessaging
{
    public class SecureMessagingViewManager
    {
        private readonly IOmedixIntegrationService _service;

        public SecureMessagingViewManager(IOmedixIntegrationService service)
        {
            _service = service;
        }

        public void ShowSecureMessagingView()
        {
            // call service, get the url, and show the url in the browser
            var info = _service.GetSecureMessagingInfo();
            var view = new HtmlBrowserView { Height = 600, Width = 1024 };
            view.PostData = System.Text.Encoding.UTF8.GetBytes("guid=" + info.Guid);
            view.HtmlSource = info.Url;
            InteractionManager.Current.Show(new WindowInteractionArguments {Content = view, ResizeMode = System.Windows.ResizeMode.CanResize});
        }
    }
}
