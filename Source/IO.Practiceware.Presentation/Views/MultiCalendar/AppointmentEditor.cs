﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using AppointmentViewModel = IO.Practiceware.Presentation.ViewModels.MultiCalendar.AppointmentViewModel;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    /// <summary>
    /// Provides helper functionality for editing appointments.
    /// </summary>
    public class AppointmentEditor : IViewModel
    {
        private readonly Func<IEnumerable<IList<BlockViewModel>>> _getBlocks;
        private readonly Func<int, BlockViewModel, BlockCategoryViewModel, bool> _rescheduleAppointment;
        private readonly Action _refreshView;
        private readonly TaskQueue _taskQueue;
        private readonly IMultiCalendarViewService _multiCalendarViewService;

        public AppointmentEditor(IMultiCalendarViewService multiCalendarViewService,
                                 Func<IEnumerable<IList<BlockViewModel>>> getBlocks,
                                 Func<int, BlockViewModel, BlockCategoryViewModel, bool> rescheduleAppointment,
                                 Action refreshView, TaskQueue taskQueue,
                                 IInteractionContext interactionContext)
        {
            InteractionContext = interactionContext;
            _multiCalendarViewService = multiCalendarViewService;
            _getBlocks = getBlocks;
            _rescheduleAppointment = rescheduleAppointment;
            _refreshView = refreshView;
            _taskQueue = taskQueue;

            DeleteScheduleBlockCategory = Command.Create<BlockCategoryViewModel>(ExecuteDeleteScheduleBlockCategory).Async(() => InteractionContext);
            MoveAppointment = Command.Create<MoveAppointmentArguments>(ExecuteMoveAppointment);
        }

        private void ExecuteDeleteScheduleBlockCategory(BlockCategoryViewModel blockCategory)
        {
            if (PermissionId.InsertDeleteCategoriesInSchedule.EnsurePermission())
            {
                _taskQueue.CancelAll();
                _taskQueue.Enqueue(() =>
                    {
                        if (blockCategory is UnavailableBlockCategoryViewModel)
                        {
                            _getBlocks().SelectMany(b => b).First(b => b.Categories.Contains(blockCategory)).Comment = null;
                            _multiCalendarViewService.SetBlocksIsUnavailable(new[] {blockCategory.BlockId.Value}, false);
                        }
                        else
                        {
                            _multiCalendarViewService.RemoveScheduleBlockCategories(new[] {blockCategory.Id});
                        }
                    }, () =>
                        {
                            foreach (IList<BlockViewModel> blockCollection in _getBlocks())
                            {
                                BlockViewModel block = blockCollection.FirstOrDefault(b => b.Id == blockCategory.BlockId);
                                if (block != null)
                                {
                                    block.Categories.Remove(blockCategory);
                                }
                            }
                        });
            }
        }

        private void ExecuteMoveAppointment(MoveAppointmentArguments args)
        {
            #region helper func rescheduleAppointmentCommand

            Func<ICommand> rescheduleAppointmentCommand = () =>
                                                          Command.Create(() =>
                                                              {
                                                                  bool hasRescheduled = false;
                                                                  System.Windows.Application.Current.Dispatcher.Invoke(() => hasRescheduled = _rescheduleAppointment(args.Appointment.Id, args.TargetBlock, args.TargetBlockCategory));
                                                                  if (hasRescheduled)
                                                                  {
                                                                      _refreshView();
                                                                  }

                                                              });

            #endregion

            _taskQueue.CancelAll();
            _taskQueue.Enqueue(() => System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    {
                        var isPatientInactive = args.Appointment.Patient.PatientStatus != PatientStatus.Active;
                        if (isPatientInactive)
                        {
                            InteractionManager.Current.Alert("Cannot schedule an appointment for an inactive patient.");
                            return;
                        }

                        var canReschedule = ((EncounterStatus)args.Appointment.EncounterStatusId).CanReschedule();

                        // special case: They can reschedule if an appointment is checked in only if they are changing the doctor (and nothing else) and they have special changeDoctor permission.
                        // or if they are changing the doctor and changing the category and they have both the changeDoctor and forceScheduleAppointment permission.
                        var isCheckedIn = args.Appointment.IsDischarged || args.Appointment.IsArrived;
                        if (isCheckedIn)
                        {
                            if ((IsChangingXOnly(args, isChangingDoctorOrResourceOnly: true) && PermissionId.ChangeResourceOnDischargedOrArrivedAppointments.EnsurePermission())
                                || (IsChangingXOnly(args, isChangingDoctorOrResourceOnly: true, isChangingCategoryOnly: true) && PermissionId.ChangeResourceOnDischargedOrArrivedAppointments.EnsurePermission() && PermissionId.ForceScheduleAppointment.EnsurePermission()))
                            {
                                // let them reschedule the doctor
                                canReschedule = true;
                            }
                        }

                        if (!canReschedule)
                        {
                            InteractionManager.Current.Alert(((EncounterStatus)args.Appointment.EncounterStatusId).GetCanRescheduleAlertMessage());
                            return;
                        }


                        bool hasAppointmentTypeMismatch = args.TargetBlockCategory == null || args.Appointment.AppointmentCategoryId != args.TargetBlockCategory.Category.Id;
                        // ForceAppointments permission implicitly applied.  Not allowed to drag/drop onto non-matching categories if permission is not applied.
                        if (hasAppointmentTypeMismatch)
                        {
                            var confirmCommand = new ConfirmCommand
                                {
                                    Content = "Appointment does not match the defined schedule.  Are you sure you would like to force this appointment into the schedule?",
                                    Command = rescheduleAppointmentCommand()
                                };
                            confirmCommand.Execute();
                        }
                        else
                        {
                            rescheduleAppointmentCommand().Execute();
                        }
                    }
                }));
        }

        /// <summary>
        /// Returns true if the one you specified as true "only" is the only once that has changed.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="isChangingDoctorOrResourceOnly"></param>
        /// <param name="isChangingLocationOnly"></param>
        /// <param name="isChangingDateTimeOnly"></param>
        /// <param name="isChangingCategoryOnly"></param>
        /// <returns></returns>
        private static bool IsChangingXOnly(MoveAppointmentArguments args, bool isChangingDoctorOrResourceOnly = false, bool isChangingLocationOnly = false, bool isChangingDateTimeOnly = false, bool isChangingCategoryOnly = false)
        {
            var isChangingDoctorOrResource = args.Appointment.Resource.Id != args.TargetBlock.Resource.Id;

            var isChangingLocation = args.Appointment.Location.Id != args.TargetBlock.Location.Id;

            var isChangingDateTime = args.Appointment.StartDateTime != args.TargetBlock.StartDateTime;

            var isChangingCategory = args.TargetBlockCategory == null || (args.Appointment.AppointmentCategoryId != args.TargetBlockCategory.Category.Id);

            if ((!isChangingDoctorOrResource || isChangingDoctorOrResourceOnly) && (!isChangingLocation || isChangingLocationOnly) && (!isChangingDateTime || isChangingDateTimeOnly) && (!isChangingCategory || isChangingCategoryOnly))
            {
                return true;
            }

            return false;
        }

        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Command to delete a schedule block category
        /// </summary>
        public ICommand DeleteScheduleBlockCategory { get; protected set; }

        /// <summary>
        /// Command to move an appointment
        /// </summary>
        public ICommand MoveAppointment { get; protected set; }
    }

    public class MoveAppointmentArguments
    {
        public AppointmentViewModel Appointment { get; set; }
        public BlockViewModel TargetBlock { get; set; }
        public BlockCategoryViewModel TargetBlockCategory { get; set; }
    }
}