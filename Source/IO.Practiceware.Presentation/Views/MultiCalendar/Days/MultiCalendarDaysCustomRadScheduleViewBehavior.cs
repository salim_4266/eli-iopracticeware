using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Days
{
    public class MultiCalendarDaysCustomRadScheduleViewBehavior : MultiCalendarCustomRadScheduleViewBehavior
    {
        #region DependencyProperty: DateTimeFormat

        public static readonly DependencyProperty DateTimeFormatProperty = DependencyProperty.Register("DateTimeFormat", typeof(string), typeof(MultiCalendarDaysCustomRadScheduleViewBehavior), new PropertyMetadata("dddd, MMMM dd"));

        public string DateTimeFormat
        {
            get { return GetValue(DateTimeFormatProperty) as string; }
            set { SetValue(DateTimeFormatProperty, value); }
        }

        #endregion

        #region DependencyProperty: Dates

        public static readonly DependencyProperty DatesProperty = DependencyProperty.Register("Dates", typeof(ObservableCollection<MultiCalendarDaysViewModelWrapper>), typeof(MultiCalendarDaysCustomRadScheduleViewBehavior), new PropertyMetadata(new ExtendedObservableCollection<MultiCalendarDaysViewModelWrapper>(), OnPropertyDatesChanged));
        private PropertyChangedEventArgs _lastPropertyChangedEventArgs;

        public ObservableCollection<MultiCalendarDaysViewModelWrapper> Dates
        {
            get { return GetValue(DatesProperty) as ObservableCollection<MultiCalendarDaysViewModelWrapper>; }
            set { SetValue(DatesProperty, value); }
        }

        private static void OnPropertyDatesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarDaysCustomRadScheduleViewBehavior)d;

            var oldPropertyChanged = e.OldValue as INotifyPropertyChanged;
            if (oldPropertyChanged != null)
            {
                oldPropertyChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddDatesFromProperty);
            }

            var newPropertyChanged = e.NewValue as INotifyPropertyChanged;
            if (newPropertyChanged != null)
            {
                newPropertyChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddDatesFromProperty);
            }

            behavior.AddDatesFromProperty(behavior, null);
        }

        private void AddDatesFromProperty(object sender, EventArgs e)
        {
            if (e.As<CascadingPropertyChangedEventArgs>().IfNotNull(a => a.OriginalPropertyChangedEventArgs == _lastPropertyChangedEventArgs)) return;
            
            e.As<CascadingPropertyChangedEventArgs>().IfNotNull(a => _lastPropertyChangedEventArgs = a.OriginalPropertyChangedEventArgs);

            if (AssociatedObject == null || !SpanSize.HasValue) return;

            var args = e.As<CascadingPropertyChangedEventArgs>();
            if (args != null &&
                (args.PropertyName.Equals("count", StringComparison.InvariantCultureIgnoreCase) ||
                args.PropertyName.Equals("item[]", StringComparison.InvariantCultureIgnoreCase) ||
                args.PropertyName.Contains("Item.Blocks") ||
                args.PropertyName.Equals("Item.orphanedAppointments", StringComparison.InvariantCultureIgnoreCase)))
            {
                RefreshGui();
            }

            else if (args == null) RefreshGui();
        }

        #endregion

        #region HelpersDependencyProperty

        public IEnumerable<Appointment> GenerateTelerikOrphanedAppointments(MultiCalendarDaysViewModelWrapper date, List<BlockViewModel> blocks, Resource resource, IList<Appointment> recyclableAppointments)
        {
            var telerikAppointments = new List<Appointment>();
            foreach (AppointmentViewModel orphan in date.OrphanedAppointments.OrderBy(a => a.StartDateTime))
            {
                BlockViewModel block = blocks.FirstOrDefault(b => b.StartDateTime == orphan.StartDateTime && b.EndDateTime == orphan.EndDateTime);

                var recycled = recyclableAppointments.OfType<OrphanedAppointment>().FirstOrDefault();
                if (recycled != null)
                {
                    recyclableAppointments.Remove(recycled);
                    recycled.Initialize(orphan, block);
                    recycled.Resources.Clear();
                    recycled.Resources.Add(resource);
                    telerikAppointments.Add(recycled);
                }
                else
                {
                    var orphanedAppointment = new OrphanedAppointment(orphan, block);
                    orphanedAppointment.Resources.Add(resource);
                    telerikAppointments.Add(orphanedAppointment);
                }
            }
            return telerikAppointments;
        }

        private IEnumerable<Appointment> GenerateTelerikAppointments(MultiCalendarDaysViewModelWrapper day, Resource resource, IList<Appointment> recyclableAppointments)
        {
            var appointments = new List<Appointment>();
            if (!SpanSize.HasValue) return appointments;

            List<BlockViewModel> blocks = day.Blocks.ToList();
            IEnumerable<Appointment> processBlockAppointments = AppointmentUtilities.ProcessBlocksGenerateAppointments(blocks, resource, recyclableAppointments).OrderBy(a => a.Start);
            List<Appointment> orphanedAppointments = GenerateTelerikOrphanedAppointments(day, blocks, resource, recyclableAppointments).OrderBy(a => a.Start).ToList();
            IEnumerable<Appointment> noBlockAppointments = AppointmentUtilities.ProcessBlocksGenerateNoBlockAppointments(blocks, resource, recyclableAppointments);

            List<Appointment> result = processBlockAppointments.Concat(orphanedAppointments).ToList();
            result.AddRange(noBlockAppointments);

            result = result.Except(result.OfType<OnlyBlockAppointment>().OverlappingWith(result.OfType<OrphanedAppointment>())).ToList();
            appointments.AddRange(result);
            // Order by start time, then by appointment id if it exists
            return appointments.OrderBy(a => a.Start).ThenBy(a => a.As<IHasAppointmentViewModel>().IfNotNull(i => i.Appointment).IfNotNull(i => i.Id));
        }

        #endregion

        public override void RefreshGui()
        {
            var formattedDateNames = ListOfTelerikAppointment.All(a => a is NoBlockAppointment) ? new string[0] : ListOfTelerikAppointment.SelectMany(a => a.Resources).Select(r => r.ResourceName).OrderBy(i => i).Distinct().ToArray();
            
            // Days calendar can only select a single resource
            int? oldResourceId = ListOfTelerikAppointment.All(a => a is NoBlockAppointment) ? null : new int?(ListOfTelerikAppointment.OfType<IHasScheduleBlock>().First().Block.Resource.Id);

            RecycledAppointments.AddRange(ListOfTelerikAppointment);

            ListOfTelerikAppointment.Clear();

            var appointments = new List<Appointment>();

            foreach (MultiCalendarDaysViewModelWrapper day in Dates)
            {
                var resource = new Resource(day.Date.ToString(DateTimeFormat), "Dates");
                appointments.AddRange(GenerateTelerikAppointments(day, resource, RecycledAppointments));
            }

            RecycledAppointments.OfType<IRecyclableAppointment>().ForEachWithSinglePropertyChangedNotification(a => a.Clear());

            ListOfTelerikAppointment.AddRangeWithSinglePropertyChangedNotification(appointments);

            bool selectedNewResource = Dates.IsNullOrEmpty() ? oldResourceId.HasValue 
                                                             : (oldResourceId.HasValue && oldResourceId.Value != Dates.First().Resource.Id) || !oldResourceId.HasValue;
            
            if (!ListOfTelerikAppointment.SelectMany(a => a.Resources).Select(r => r.ResourceName).OrderBy(i => i).Distinct().SequenceEqual(formattedDateNames)
                || selectedNewResource)
            {
                ScrollToFirstBlock();
            }

            AssociatedObject.UpdateLayout();
        }

        public override IEnumerable<BlockViewModel> GetBlocks()
        {
            var result = new List<BlockViewModel>();
            if (Dates != null)
            {
                result.AddRange(Dates.SelectMany(d => d.Blocks));
            }
            return result;
        }
    }
}