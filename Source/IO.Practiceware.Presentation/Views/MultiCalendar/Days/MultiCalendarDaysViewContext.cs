using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Days
{
    public class MultiCalendarDaysViewContext : IViewContext, IChildMultiCalendarViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        public void InitializeView(DateTime date, int resourceId)
        {
            SelectedResource = ListOfResource.First(p => p.Id == resourceId);
            GeneratedDates = new ExtendedObservableCollection<DateTime> { date };
            DisplayDate = GeneratedDates.First();
            RefreshView();
        }

        public void RefreshView()
        {
            RefreshView(Dates.Select(d => d.Date).ToArray());
        }

        private void RefreshView(IEnumerable<DateTime> dates)
        {
            if (!dates.Any() && SelectedResource != null) return;

            _taskQueue.Enqueue(() => GeneratedDates.IsNotNullOrEmpty() ? ExecuteQueryBlocks(dates.ToArray()) : null,
                    queryResult =>
                    {
                        if (GeneratedDates.IsNotNullOrEmpty())
                        {
                            ExecutePopulateBlocks(queryResult);
                        }
                    });
        }

        private readonly IMultiCalendarViewService _multiCalendarViewService;
        private readonly Func<MultiCalendarDaysViewModelWrapper> _createMultiCalendarDaysViewModelWrapper;
        private readonly Func<BlockViewModel> _createBlockViewModel;
        private readonly Func<int, BlockViewModel, BlockCategoryViewModel, bool> _showRescheduleAppointment;
        private ResourceViewModel _selectedResource;
        private readonly TaskQueue _taskQueue;

        public MultiCalendarDaysViewContext(IMultiCalendarViewService multiCalendarViewService,
                                            Func<MultiCalendarDaysViewModelWrapper> createMultiCalendarDaysViewModelWrapper,
                                            Func<BlockViewModel> createBlockViewModel, TaskQueue taskQueue,
                                            Func<int, BlockViewModel, BlockCategoryViewModel, bool> showRescheduleAppointment)
        {
            _multiCalendarViewService = multiCalendarViewService;
            _createMultiCalendarDaysViewModelWrapper = createMultiCalendarDaysViewModelWrapper;
            _createBlockViewModel = createBlockViewModel;
            _taskQueue = taskQueue;
            _showRescheduleAppointment = showRescheduleAppointment;
            Init();
            Load = Command.Create<MultiCalendarLoadArguments>(ExecuteLoad);
            SelectDate = Command.Create(ExecuteSelectDate);
        }

        private void Init()
        {
            Dates = new ExtendedObservableCollection<MultiCalendarDaysViewModelWrapper>();
            // To detect is this a local parctice Vs Cloud practice 
            DateTime dtToday = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Today.Hour, DateTime.Today.Minute, DateTime.Today.Second);
            GeneratedDates = new ExtendedObservableCollection<DateTime> { dtToday };
            DisplayDate = GeneratedDates.First();

        }

        private void ExecuteLoad(MultiCalendarLoadArguments loadArgs)
        {
            if (loadArgs != null && loadArgs.Dates != null)
            {
                this.Dispatcher().Invoke(() =>
                    {
                        GeneratedDates.Clear();
                        GeneratedDates.AddRangeWithSinglePropertyChangedNotification(loadArgs.Dates);
                        if (GeneratedDates.Any())
                        {
                            DisplayDate = GeneratedDates.First();
                        }
                    });
            }

            SelectedResource = ListOfResource.Count == 1 ? ListOfResource.FirstOrDefault() : ListOfResource.FirstOrDefault(p => (loadArgs != null && loadArgs.ResourceIds != null && loadArgs.ResourceIds.Contains(p.Id)));
        }

        private void ExecuteSelectDate()
        {
            if (SelectedResource != null)
            {
                var datesToAdd = CreateAndRemoveDateWrappers();
                lock (datesToAdd)
                {
                    RefreshView(datesToAdd);
                }
            }
        }

        /// <summary>
        /// Creates and removes date wrappers using the currently selected dates and resource.
        /// </summary>
        /// <returns>Returns an enumeration of the DateTimes that were added.</returns>
        [DispatcherThread]
        protected virtual DateTime[] CreateAndRemoveDateWrappers()
        {
            var currentlySelectedResource = SelectedResource;
            // If resource is not selected -> clear date wrappers and return
            if (currentlySelectedResource == null)
            {
                Dates.Clear();
                return new DateTime[0];
            }

            // Remove dates not for this resource or that are not selected
            var currentlySelectedDates = GeneratedDates.ToArray();
            var wrappersToRemove = Dates.Where(d => !currentlySelectedDates.Contains(d.Date) || d.Resource != currentlySelectedResource).ToArray();
            wrappersToRemove.ForEachWithSinglePropertyChangedNotification(i => Dates.Remove(i));

            // Calculate which dates should be added
            var datesToAdd = currentlySelectedDates.Where(d => !Dates.Select(i => i.Date).Contains(d)).ToArray();

            // Create new date wrappers
            var wrappers = new ExtendedObservableCollection<MultiCalendarDaysViewModelWrapper>();
            foreach (var date in datesToAdd)
            {
                var wrapper = _createMultiCalendarDaysViewModelWrapper();
                wrapper.Date = date;
                wrapper.Resource = currentlySelectedResource;
                wrappers.Add(wrapper);
            }

            // Insert new date wrappers into correct position
            foreach (var wrapper in wrappers)
            {
                var localWrapper = wrapper;
                var index = Dates.LastIndexOf(d => d.Date < localWrapper.Date);
                if (index == -1)
                {
                    if (Dates.Any())
                    {
                        Dates.Insert(0, localWrapper);
                    }
                    else
                    {
                        Dates.Add(localWrapper);
                    }
                }
                else if (index < Dates.Count - 1)
                {
                    Dates.Insert(index, localWrapper);
                }
                else
                {
                    Dates.Add(localWrapper);
                }
            }

            return datesToAdd;
        }

        private ExtendedObservableCollection<BlockViewModel> InitializeBlockPlaceHolders(ExtendedObservableCollection<BlockViewModel> blocks, ResourceViewModel resource, DateTime date)
        {
            #region helper func createBlockViewModel
            Func<DateTime, ResourceViewModel, BlockViewModel> createBlockViewModel = (blockStartTime, resourceViewModel) =>
            {
                var blockViewModel = _createBlockViewModel();
                blockViewModel.Categories = new ExtendedObservableCollection<BlockCategoryViewModel>();
                blockViewModel.Location = DefaultSpanLocation;
                blockViewModel.StartDateTime = blockStartTime;
                blockViewModel.EndDateTime = blockStartTime.Add(SpanSize);
                blockViewModel.Resource = resourceViewModel;
                return blockViewModel;
            };
            #endregion

            var startTime = date.Date;
            var endTime = startTime.AddDays(1);

            var validBlockStartTimes = new List<DateTime>();
            while (startTime <= endTime.Subtract(SpanSize))
            {
                validBlockStartTimes.Add(startTime);
                startTime = startTime.Add(SpanSize);
            }

            var blockPlaceHolderTimes = validBlockStartTimes.Where(t => blocks.All(b => b.StartDateTime != t));
            foreach (var placeHolderTime in blockPlaceHolderTimes)
            {
                blocks.Add(createBlockViewModel(placeHolderTime, resource));
            }

            return blocks.OrderBy(b => b.StartDateTime).ToExtendedObservableCollection();
        }

        public GetScheduleBlockResults ExecuteQueryBlocks(DateTime[] dates)
        {
            if (dates.Any() && SelectedResource != null)
            {
                var localSelectedResource = SelectedResource;
                return _multiCalendarViewService.GetScheduleBlocks(new[] { localSelectedResource.Id }, dates);
            }
            else
            {
                return null;
            }
        }

        private void ExecutePopulateBlocks(GetScheduleBlockResults queryResults)
        {
            if (queryResults == null) return;

            foreach (var wrapper in Dates.Where(i => queryResults.QueriedDateTimes.Select(t => t.Date).Contains(i.Date) && queryResults.QueriedResourceIds.Contains(i.Resource.Id)))
            {
                var localWrapper = wrapper;
                var newBlocks = queryResults.Blocks.Where(b => b.Resource.Id == localWrapper.Resource.Id && b.StartDateTime.Date == localWrapper.Date).OrderBy(b => b.StartDateTime).ToExtendedObservableCollection();
                newBlocks = InitializeBlockPlaceHolders(newBlocks, wrapper.Resource, wrapper.Date);
                var newOrphanedAppointments = queryResults.OrphanedAppointments.Where(a => a != null && a.Resource.Id == localWrapper.Resource.Id && a.StartDateTime.Date == localWrapper.Date).OrderBy(a => a.StartDateTime).ToExtendedObservableCollection();
                var newResourceComment = queryResults.ResourceComments.First(c => c.UserId.GetValueOrDefault() == localWrapper.Resource.Id && c.Date.Date == localWrapper.Date);
                var newGlobalComment = queryResults.GlobalComments.First(c => c.Date.Date == localWrapper.Date);

                var canUpdateBlocks = !localWrapper.Blocks.SequenceEqual(newBlocks);
                var canUpdateGlobalComment = !Equals(localWrapper.GlobalComment, newGlobalComment);
                var canUpdateOrphanedAppointments = !localWrapper.OrphanedAppointments.SequenceEqual(newOrphanedAppointments);
                var canUpdateResourceComment = !Equals(localWrapper.ResourceComment, newResourceComment);

                using (new SuppressPropertyChangedScope())
                {
                    if (canUpdateBlocks) { localWrapper.Blocks = newBlocks; }
                    if (canUpdateGlobalComment) { localWrapper.GlobalComment = newGlobalComment; }
                    if (canUpdateOrphanedAppointments) { localWrapper.OrphanedAppointments = newOrphanedAppointments; }
                    if (canUpdateResourceComment) { localWrapper.ResourceComment = newResourceComment; }
                }

                // Trigger 'blocks' redraw only once if possible
                var canRedrawBlocks = canUpdateBlocks || canUpdateOrphanedAppointments;
                if (canRedrawBlocks)
                {
                    localWrapper.Blocks = localWrapper.Blocks.ToExtendedObservableCollection();
                }

                // Trigger 'header resource' redraw only once if possible
                // Redrawing blocks implicitly redraws the "header resource" since some values in the "header resource" are calculated from the blocks
                if (!canRedrawBlocks && (canUpdateGlobalComment || canUpdateResourceComment))
                {
                    var resourceComment = localWrapper.ResourceComment;
                    using (new SuppressPropertyChangedScope()) localWrapper.ResourceComment = null;
                    localWrapper.ResourceComment = resourceComment;
                }
            }

            AppointmentEditor = new AppointmentEditor(_multiCalendarViewService,
                                                      () => Dates.Select(d => d.Blocks),
                                                      _showRescheduleAppointment,
                                                      RefreshView, _taskQueue,
                                                      InteractionContext);
        }



        /// <summary>
        /// The default location for blocks
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel DefaultSpanLocation { get; set; }

        /// <summary>
        /// Smallest unit of schedulable time
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan SpanSize { get; set; }

        /// <summary>
        /// Start of range for blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaStart { get; set; }

        /// <summary>
        /// End of range for blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaEnd { get; set; }

        /// <summary>
        /// Series of schedule blocks group by date
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<MultiCalendarDaysViewModelWrapper> Dates { get; set; }

        /// <summary>
        /// List of resources to which templates can be applied to
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ResourceViewModel> ListOfResource { get; set; }

        [DispatcherThread]
        public virtual ResourceViewModel SelectedResource
        {
            get { return _selectedResource; }
            set
            {
                _selectedResource = value;
                var datesToAdd = CreateAndRemoveDateWrappers();
                Dates.ForEachWithSinglePropertyChangedNotification(i => i.Resource = SelectedResource);
                if (datesToAdd.Any())
                {
                    RefreshView(datesToAdd);
                }
            }
        }

        /// <summary>
        /// Hook for custom behavior to store multi selected dates
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<DateTime> GeneratedDates { get; set; }

        [DispatcherThread]
        public virtual DateTime DisplayDate { get; set; }

        /// <summary>
        /// Helper class that contains logic for moving appointments and deleting Categories and Blocks.
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentEditor AppointmentEditor { get; protected set; }

        [DependsOn("Dates")]
        [DispatcherThread]
        public virtual bool CanDisplayCalendar
        {
            get { return Dates.Any(); }
        }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to trigger search via date
        /// </summary>
        public ICommand SelectDate { get; protected set; }
    }
}