﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Soaf.Data;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Days
{
    public class MultiCalendarDaysRadScheduleViewHeaderBehavior : Behavior<RadScheduleView>
    {
        public static readonly string DropDownItemShowThisDayOnly = "Show This Day Only";
        public static readonly string DropDownItemApplyDifferentTemplate = "Apply Different Template";
        public static readonly string DropDownItemCreateNewEditTemplate = "Create New/Edit Template";

        public readonly ResourceTypeCollection ListOftelerikResourceTypes = new ResourceTypeCollection();
        public List<CollectionViewGroup> ListOfCollectionViewGroupInternal = new List<CollectionViewGroup>();

        #region DependencyProperty: DateTimeFormat

        public string DateTimeFormat
        {
            get { return GetValue(DateTimeFormatProperty) as string; }
            set { SetValue(DateTimeFormatProperty, value); }
        }

        public static readonly DependencyProperty DateTimeFormatProperty = DependencyProperty.Register("DateTimeFormat", typeof(string), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior), new PropertyMetadata("dddd, MMMM dd, yyyy"));

        #endregion

        #region DependencyProperty : CanSetDoctorSchedule

        public bool CanSetDoctorSchedule
        {
            get { return (bool)GetValue(CanSetDoctorScheduleProperty); }
            set { SetValue(CanSetDoctorScheduleProperty, value); }
        }

        public static readonly DependencyProperty CanSetDoctorScheduleProperty = DependencyProperty.Register("CanSetDoctorSchedule", typeof(bool), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : CanSetupScheduleTemplates

        public bool CanSetupScheduleTemplates
        {
            get { return (bool)GetValue(CanSetupScheduleTemplatesProperty); }
            set { SetValue(CanSetupScheduleTemplatesProperty, value); }
        }

        public static readonly DependencyProperty CanSetupScheduleTemplatesProperty = DependencyProperty.Register("CanSetupScheduleTemplates", typeof(bool), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty: ShowMultiDoc

        public ICommand ShowMultiDoc
        {
            get { return GetValue(ShowMultiDocProperty) as ICommand; }
            set { SetValue(ShowMultiDocProperty, value); }
        }

        public static readonly DependencyProperty ShowMultiDocProperty = DependencyProperty.Register("ShowMultiDoc", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : ApplyTemplate

        public ICommand ApplyTemplate
        {
            get { return GetValue(ApplyTemplateProperty) as ICommand; }
            set { SetValue(ApplyTemplateProperty, value); }
        }

        public static readonly DependencyProperty ApplyTemplateProperty = DependencyProperty.Register("ApplyTemplate", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : CreateOrEditTemplate

        public ICommand CreateOrEditTemplate
        {
            get { return GetValue(CreateOrEditTemplateProperty) as ICommand; }
            set { SetValue(CreateOrEditTemplateProperty, value); }
        }

        public static readonly DependencyProperty CreateOrEditTemplateProperty = DependencyProperty.Register("CreateOrEditTemplate", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : AddNewComment

        public ICommand AddNewComment
        {
            get { return GetValue(AddNewCommentProperty) as ICommand; }
            set { SetValue(AddNewCommentProperty, value); }
        }

        public static readonly DependencyProperty AddNewCommentProperty = DependencyProperty.Register("AddNewComment", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : EditComment

        public ICommand EditComment
        {
            get { return GetValue(EditCommentProperty) as ICommand; }
            set { SetValue(EditCommentProperty, value); }
        }

        public static readonly DependencyProperty EditCommentProperty = DependencyProperty.Register("EditComment", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty : DeleteComment

        public ICommand DeleteComment
        {
            get { return GetValue(DeleteCommentProperty) as ICommand; }
            set { SetValue(DeleteCommentProperty, value); }
        }

        public static readonly DependencyProperty DeleteCommentProperty = DependencyProperty.Register("DeleteComment", typeof(ICommand), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior));

        #endregion

        #region DependencyProperty: Resource

        public ResourceViewModel Resource
        {
            get { return (ResourceViewModel)GetValue(ResourceProperty); }
            set { SetValue(ResourceProperty, value); }
        }

        public static readonly DependencyProperty ResourceProperty = DependencyProperty.Register("Resource", typeof(ResourceViewModel), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior), new PropertyMetadata(new ResourceViewModel(), OnPropertyResourceChanged));

        private static void OnPropertyResourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarDaysRadScheduleViewHeaderBehavior)d;
            behavior.SetResourceFromProperty();
        }

        private void SetResourceFromProperty()
        {
            if (AssociatedObject == null) { return; }

            foreach (MultiCalendarScheduleViewHeaderResource resource in ListOftelerikResourceTypes[0].Resources)
            {
                if (Resource != null) { resource.ResourceId = Resource.Id; }
            }
        }
        
        #endregion

        #region DependencyProperty: Days
        
        
        public ObservableCollection<MultiCalendarDaysViewModelWrapper> Days
        {
            get { return GetValue(DaysProperty) as ObservableCollection<MultiCalendarDaysViewModelWrapper>; }
            set { SetValue(DaysProperty, value); }
        }

        public static readonly DependencyProperty DaysProperty = DependencyProperty.Register("Days", typeof(ObservableCollection<MultiCalendarDaysViewModelWrapper>), typeof(MultiCalendarDaysRadScheduleViewHeaderBehavior), new PropertyMetadata(new ExtendedObservableCollection<MultiCalendarDaysViewModelWrapper>(), OnPropertyDaysChanged));

        private static void OnPropertyDaysChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarDaysRadScheduleViewHeaderBehavior)d;
            
            var oldCollectionChanged = e.OldValue as INotifyPropertyChanged;
            if (oldCollectionChanged != null)
            {
                oldCollectionChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddDaysFromProperty);
            }

            var newCollectionChanged = e.NewValue as INotifyPropertyChanged;
            if (newCollectionChanged != null)
            {
                newCollectionChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddDaysFromProperty);
            }

            behavior.AddDaysFromProperty(behavior, null);
        }

        private void AddDaysFromProperty(object sender, EventArgs e)
        {
            if (AssociatedObject == null || (!Days.Any(CanAddResource) && !CanRemoveResources(Days))) return;

            if (CanRemoveResources(Days))
            {
                RemoveResources(Days);
            }
            foreach (var resource in Days.Where(CanAddResource))
            {
                AddResource(resource);
            }

            InitializeLocationHourPlaceHolders();

            var telerikResourceType = ListOftelerikResourceTypes[0];
            ListOftelerikResourceTypes[0] = null;
            ListOftelerikResourceTypes[0] = telerikResourceType;

            AssociatedObject.ResourceTypesSource = null;
            AssociatedObject.ResourceTypesSource = ListOftelerikResourceTypes;
        }

        #region Helper methods for DependencyProperty: Resources

        /// <summary>
        /// Method to create location hour place holders.
        /// </summary>
        /// <remarks>
        /// This is needed because the header heights are dynamically sized based on content.
        /// Using this collection of LocationHour place holders, we can size the headers to be the same height.
        /// </remarks>
        private void InitializeLocationHourPlaceHolders()
        {
            if (!Days.Any()) return;

            int maxLocationHours = Days.Select(p => p.LocationHours.Count).Max();
            foreach (MultiCalendarScheduleViewHeaderResource resource in ListOftelerikResourceTypes[0].Resources)
            {
                var locationHourPlaceHolders = new ObservableCollection<object>();
                while (locationHourPlaceHolders.Count + resource.LocationHours.Count < maxLocationHours)
                {
                    locationHourPlaceHolders.Add(new object());
                }
                resource.LocationHourPlaceHolders = locationHourPlaceHolders;
            }
        }

        private bool CanAddResource(MultiCalendarDaysViewModelWrapper resourceWrapper)
        {
            var existingTelerikHeaderResource = ListOftelerikResourceTypes[0].Resources.Cast<MultiCalendarScheduleViewHeaderResource>().FirstOrDefault(r => r.Date == resourceWrapper.Date);
            if (existingTelerikHeaderResource == null) return true;

            if (existingTelerikHeaderResource.ResourceId == resourceWrapper.Resource.Id
                && existingTelerikHeaderResource.EncountersUsed == resourceWrapper.CategoriesUsed
                && existingTelerikHeaderResource.GlobalHeaderComment.CommentViewModel == resourceWrapper.GlobalComment
                && existingTelerikHeaderResource.LocationHours.SequenceEqual(resourceWrapper.LocationHours)
                && existingTelerikHeaderResource.ResourceHeaderComment.CommentViewModel == resourceWrapper.ResourceComment
                && existingTelerikHeaderResource.TotalEncounters == resourceWrapper.TotalCategories)
            {
                return false;
            }

            return true;
        }

        private bool CanRemoveResources(IEnumerable<MultiCalendarDaysViewModelWrapper> resourceWrappers)
        {
            var selectedDates = resourceWrappers.Select(r => r.Date).ToArray();
            return ListOftelerikResourceTypes[0].Resources.Cast<MultiCalendarScheduleViewHeaderResource>().Any(r => !selectedDates.Contains(r.Date));
        }

        private int[] GetScheduleBlockCount(string date1, string date2, int resourceId)
        {
            int[] ScheduleBlocksCount = new int[2];
            string _SelectQuery = "Select SUM(CASE WHEN DATEPART(hour, startdatetime) BETWEEN 0 AND 11 THEN 1 ELSE 0 END) AS  'AMTOT',";
            _SelectQuery += " SUM(CASE WHEN DATEPART(hour, startdatetime) BETWEEN 12 AND 23 THEN 1 ELSE 0 END) AS  'PMTOT' FROM";
            _SelectQuery += " model.scheduleblocks sb with (nolock) inner join model.ScheduleBlockAppointmentCategories sbac with (nolock) on sb.id = sbac.ScheduleBlockId ";
            _SelectQuery += " Where Cast(sb.startdatetime as date) between '" + date1 + "' and '" + date2 + "' and sb.UserId= " + resourceId;

            try
            {
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("AMTOT", typeof(string));
                    dataTable.Columns.Add("PMTOT", typeof(string));
                    dataTable = dbConnection.Execute<DataTable>(_SelectQuery);
                    foreach (DataRow _dataRow in dataTable.Rows)
                    {
                        if (!string.IsNullOrEmpty(_dataRow["AMTOT"].ToString()))
                            ScheduleBlocksCount[0] = int.Parse(_dataRow["AMTOT"].ToString());
                        if (!string.IsNullOrEmpty(_dataRow["PMTOT"].ToString()))
                            ScheduleBlocksCount[1] = int.Parse(_dataRow["PMTOT"].ToString());
                    }
                }
            }
            catch (SqlException)
            {

            }
            return ScheduleBlocksCount;
        }

        private int[] GetAMPMCount(string date1, int resourceId)
        {
            int[] AppCount = new int[2];
            try
            {
                string dYear = Convert.ToDateTime(date1).Year.ToString();
                string dMonth = Convert.ToDateTime(date1).Month.ToString();
                string dDate = Convert.ToDateTime(date1).Day.ToString();
                string inputDate = dYear;
                inputDate += dMonth.Length == 1 ? "0" + dMonth : dMonth;
                inputDate += dDate.Length == 1 ? "0" + dDate : dDate;
                string _SelectQuery = "Select Count(Case When AppTime < 720 Then 1 End) as AM, Count(Case When AppTime >= 720 Then 1 End) as PM From dbo.Appointments a with (nolock) Where a.AppDate = " + inputDate + " and (a.ScheduleStatus = 'P' OR a.ScheduleStatus = 'R' OR a.ScheduleStatus = 'D' OR a.ScheduleStatus = 'A') and a.ResourceId1 = " + resourceId;
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("AM", typeof(string));
                    dataTable.Columns.Add("PM", typeof(string));
                    dataTable = dbConnection.Execute<DataTable>(_SelectQuery);
                    foreach (DataRow _dataRow in dataTable.Rows)
                    {
                        if (!string.IsNullOrEmpty(_dataRow["AM"].ToString()))
                            AppCount[0] = int.Parse(_dataRow["AM"].ToString());
                        if (!string.IsNullOrEmpty(_dataRow["PM"].ToString()))
                            AppCount[1] = int.Parse(_dataRow["PM"].ToString());
                    }

                }
            }
            catch (SqlException)
            {

            }
            return AppCount;
        }

        private string GetAMPMSettings()
        {
            string token = string.Empty;
            string key = "AMPMCounter";
            string _query = "select top 1 value from model.applicationsettings where name = '" + key + "'";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                object AMPMSettings = dbConnection.Execute<object>(_query);
                if (AMPMSettings != null)
                    token = AMPMSettings.ToString();
                else
                    token = "";
            }
            return token;
        }

        private void AddResource(MultiCalendarDaysViewModelWrapper resourceWrapper)
        {
            int amCount = 0;
            int pmCount = 0;
            int scheduleBlockAMCnt = 0;
            int scheduleBlockPMCnt = 0;
            string showDefault = string.Empty;
            string showAMPM = string.Empty;

            if (CanAddResource(resourceWrapper))
            {
                var existingTelerikHeaderResource = ListOftelerikResourceTypes[0].Resources.Cast<MultiCalendarScheduleViewHeaderResource>().FirstOrDefault(r => r.Date == resourceWrapper.Date);
                if (existingTelerikHeaderResource != null)
                {
                    ListOftelerikResourceTypes[0].Resources.Remove(existingTelerikHeaderResource);
                }
                resourceWrapper.Date.AddDays(1);

                string _AMPMSettings = GetAMPMSettings();
                if (_AMPMSettings.Equals("False"))
                {
                    showDefault = "Visible";
                    showAMPM = "Hidden";
                }
                else
                {
                    showDefault = "Hidden";
                    showAMPM = "Visible";
                    int[] AppointmentCounts = GetAMPMCount(resourceWrapper.Date.ToString(), resourceWrapper.Resource.Id);
                    amCount = AppointmentCounts[0];
                    pmCount = AppointmentCounts[1];
                    int[] ScheduleBlocks = GetScheduleBlockCount(resourceWrapper.Date.ToString("yyyy-MM-dd"), resourceWrapper.Date.AddDays(1).ToString("yyyy-MM-dd"), resourceWrapper.Resource.Id);
                    scheduleBlockAMCnt = ScheduleBlocks[0];
                    scheduleBlockPMCnt = ScheduleBlocks[1];
                }

                var telerikHeaderResource = new MultiCalendarScheduleViewHeaderResource
                {
                    ResourceType = "Resource",
                    DisplayName = resourceWrapper.Date.ToString(DateTimeFormat),
                    ResourceName = resourceWrapper.Date.ToString(DateTimeFormat),
                    Date = resourceWrapper.Date,
                    ResourceId = resourceWrapper.Resource.Id,
                    EncountersUsed = resourceWrapper.CategoriesUsed,
                    LocationHours = resourceWrapper.LocationHours,
                    TotalEncounters = resourceWrapper.TotalCategories,
                    AM = amCount,
                    PM  =pmCount,
                    AMScheduleCnt  =  scheduleBlockAMCnt,
                    PMScheduleCnt   =   scheduleBlockPMCnt,
                    ShowAMPM = showAMPM,
                    ShowDefault = showDefault,
                    DropDownItems = new ObservableCollection<Tuple<string, ICommand>> { new Tuple<string, ICommand>(DropDownItemShowThisDayOnly, ShowMultiDoc) }
                };

                var resourceHeaderComment = new HeaderComment(HeaderCommentType.Resource, resourceWrapper.ResourceComment);
                telerikHeaderResource.ResourceHeaderComment = resourceHeaderComment;
                telerikHeaderResource.ResourceHeaderComment.AddNewCommentAtDatabase = AddNewComment;
                telerikHeaderResource.ResourceHeaderComment.EditCommentAtDatabase = EditComment;
                telerikHeaderResource.ResourceHeaderComment.DeleteCommentAtDatabase = DeleteComment;

                var globalHeaderComment = new HeaderComment(HeaderCommentType.Global, resourceWrapper.GlobalComment);
                telerikHeaderResource.GlobalHeaderComment = globalHeaderComment;
                telerikHeaderResource.GlobalHeaderComment.AddNewCommentAtDatabase = AddNewComment;
                telerikHeaderResource.GlobalHeaderComment.EditCommentAtDatabase = EditComment;
                telerikHeaderResource.GlobalHeaderComment.DeleteCommentAtDatabase = DeleteComment;

                if (PermissionId.CreateScheduleTemplates.PrincipalContextHasPermission() || PermissionId.ApplyTemplatesToSchedules.PrincipalContextHasPermission())
                {
                    telerikHeaderResource.DropDownItems.Add(new Tuple<string, ICommand>(DropDownItemApplyDifferentTemplate, ApplyTemplate));
                }

                if (PermissionId.CreateScheduleTemplates.PrincipalContextHasPermission())
                {
                    telerikHeaderResource.DropDownItems.Add(new Tuple<string, ICommand>(DropDownItemCreateNewEditTemplate, CreateOrEditTemplate));
                }

                var telerikResourceCollection = ListOftelerikResourceTypes[0].Resources as ResourceCollection;
                if (telerikResourceCollection == null) throw new Exception("Telerik no longer using ResourceCollection type");
                var indexToInsert = telerikResourceCollection.Concat(new[] { telerikHeaderResource }).Cast < MultiCalendarScheduleViewHeaderResource>().OrderBy(r => r.Date).ToList().IndexOf(telerikHeaderResource);
                telerikResourceCollection.Insert(indexToInsert, telerikHeaderResource);
            }
        }

        private void RemoveResources(IList<MultiCalendarDaysViewModelWrapper> resourceWrappers)
        {
            if (!CanRemoveResources(resourceWrappers)) return;

            var selectedDates = resourceWrappers.Select(r => r.Date).ToArray();
            var resourcesToRemove = ListOftelerikResourceTypes[0].Resources.Cast<MultiCalendarScheduleViewHeaderResource>().Where(r => !selectedDates.Contains(r.Date)).ToArray();
            foreach(var resourceToRemove in resourcesToRemove)
            {
                ListOftelerikResourceTypes[0].Resources.Remove(resourceToRemove);
            }
        }

        #endregion

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            SetGroupHeaderStyle();
            SetResourceGroups();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            RemoveGroupHeaderStyle();
            RemoveResourceGroups();
        }

        #region OnAttachedHelpers

        private void SetGroupHeaderStyle()
        {
            _groupHeaderStyleSelector = AssociatedObject.GroupHeaderStyleSelector;
            var headerStyleSelector = new ResourceGroupHeaderStyleSelector(this);
            AssociatedObject.GroupHeaderStyleSelector = headerStyleSelector;
        }

        private void SetResourceGroups()
        {
            AssociatedObject.ResourceTypesSource = ListOftelerikResourceTypes;
            ListOftelerikResourceTypes.Add(new ResourceType("Dates"));

            var telerikGroupDescription = new GroupDescriptionCollection();
            telerikGroupDescription.Add(new DateGroupDescription());
            telerikGroupDescription.Add(new ResourceGroupDescription { ResourceType = "Dates" });
            AssociatedObject.GroupDescriptionsSource = telerikGroupDescription;
        }

        #endregion

        #region OnDetachingHelpers

        private void RemoveGroupHeaderStyle()
        {
            AssociatedObject.GroupHeaderStyleSelector = _groupHeaderStyleSelector;
        }

        private void RemoveResourceGroups()
        {
            AssociatedObject.ResourceTypesSource = null;
            AssociatedObject.GroupDescriptionsSource = null;
        }

        #endregion

        #region helper variables

        private static ScheduleViewStyleSelector _groupHeaderStyleSelector;

        #endregion

        #region HelpersInnerClasses

        /// <summary>
        /// Style selector that hides DateTime group headers.  This also aligns the group header borders with
        /// its surrounding content.
        /// </summary>
        public class ResourceGroupHeaderStyleSelector : OrientedGroupHeaderStyleSelector
        {
            private readonly MultiCalendarDaysRadScheduleViewHeaderBehavior _behavior;

            public ResourceGroupHeaderStyleSelector(MultiCalendarDaysRadScheduleViewHeaderBehavior behavior)
            {
                _behavior = behavior;
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                if (item is CollectionViewGroup) _behavior.ListOfCollectionViewGroupInternal.Add(item as CollectionViewGroup);

                var groupHeader = container as GroupHeader;
                if (groupHeader != null)
                {
                    if (groupHeader.GroupKey is DateTime)
                    {
                        var style = new Style(typeof(GroupHeaderButton));
                        style.Setters.Add(new Setter(UIElement.VisibilityProperty, Visibility.Collapsed));
                        if (!groupHeader.Resources.Contains(typeof(GroupHeaderButton)))
                        {
                            groupHeader.Resources.Add(typeof(GroupHeaderButton), style);
                        }

                        style = new Style(typeof(GroupHeader));
                        style.Setters.Add(new Setter(Control.BorderThicknessProperty, new Thickness(0)));
                        return style;
                    }
                    groupHeader.Resources.Clear();
                }
                return base.SelectStyle(item, container, activeViewDefinition);
            }
        }

        #endregion
    }
}
