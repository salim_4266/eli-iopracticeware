﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Days
{
    public class MultiCalendarDaysViewModelWrapper : IViewModel
    {
        private ObservableCollection<BlockViewModel> _blocks;

        [DispatcherThread]
        public virtual DateTime Date { get; set; }

        [Dependency]
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentViewModel> OrphanedAppointments { get; set; }

        [DispatcherThread]
        public virtual CalendarCommentViewModel ResourceComment { get; set; }

        [DispatcherThread]
        public virtual CalendarCommentViewModel GlobalComment { get; set; }

        [DependsOn("Blocks")]
        public virtual ObservableCollection<LocationHoursViewModel> LocationHours
        {
            get { return Blocks.GetLocationHours(); }
        }

        [DependsOn("Blocks")]
        public virtual int CategoriesUsed
        {
            get
            {
                int categoriesUsed = Blocks.GetCategoriesUsed();
                if (OrphanedAppointments == null) return categoriesUsed;
                
                categoriesUsed += OrphanedAppointments.Count;
                return categoriesUsed;
            }
        }

        [DependsOn("Blocks")]
        public virtual int TotalCategories
        {
            get { return Blocks.GetTotalCategories(); }
        }

        [DispatcherThread]
        public virtual ResourceViewModel Resource { get; set; }

        [Dependency]
        [DispatcherThread]
        public virtual ObservableCollection<BlockViewModel> Blocks
        {
            get { return _blocks; }
            set
            {
                if (_blocks != null)
                {
                    var collection = _blocks as INotifyPropertyChanged;
                    if (collection != null)
                    {
                        collection.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnBlocksPropertyChanged);
                    }
                }
                _blocks = value;

                if (_blocks != null)
                {
                    var collection = _blocks as INotifyPropertyChanged;
                    if (collection != null)
                    {
                        collection.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnBlocksPropertyChanged);
                    }
                }
            }
        }

        void OnBlocksPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var args = e as CascadingPropertyChangedEventArgs;
            if (args == null) return;

            if (args.PropertyName == "Item.Categories.Item.Appointment.IsOrphaned")
            {
                var appointment = args.OriginalSender as AppointmentViewModel;
                if (appointment != null && appointment.IsOrphaned)
                {
                    OrphanedAppointments.Add(appointment);
                }
            }
            else if (args.PropertyName == "Item.Categories.Count")
            {
                var categories = args.OriginalSender as IEnumerable<BlockCategoryViewModel>;
                if (categories != null)
                {
                    categories.Select(b => b.Appointment).WhereNotDefault().Where(a => !a.IsOrphaned).ToList().ForEach(a => OrphanedAppointments.Remove(a));
                }
            }
        }

        public override bool Equals(object other)
        {
            return Equals(other as MultiCalendarDaysViewModelWrapper);
        }

        public bool Equals(MultiCalendarDaysViewModelWrapper other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Date.Date == other.Date.Date
                   && Blocks.SequenceEqual(other.Blocks)
                   && GlobalComment == other.GlobalComment
                   && OrphanedAppointments.SequenceEqual(other.OrphanedAppointments)
                   && Resource == other.Resource
                   && ResourceComment == other.ResourceComment;
        }

        public override int GetHashCode()
        {
            return Date.Date.GetHashCode()
                   ^ Objects.GetHashCodeWithNullCheck(Blocks)
                   ^ Objects.GetHashCodeWithNullCheck(GlobalComment)
                   ^ Objects.GetHashCodeWithNullCheck(Resource)
                   ^ Objects.GetHashCodeWithNullCheck(ResourceComment)
                   ^ Objects.GetHashCodeWithNullCheck(OrphanedAppointments);
        }
    }
}
