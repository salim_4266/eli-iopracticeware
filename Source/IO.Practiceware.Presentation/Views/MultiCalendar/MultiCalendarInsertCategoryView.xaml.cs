﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    /// <summary>
    /// Interaction logic for MultiCalendarInsertCategoryView.xaml
    /// </summary>
    public partial class MultiCalendarInsertCategoryView
    {
        public MultiCalendarInsertCategoryView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// The root view model (the view context) for the MultiCalendarInsertCategoryView
    /// </summary>
    [SupportsDataErrorInfo]
    public class MultiCalendarInsertCategoryViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IMultiCalendarViewService _multiCalendarViewService;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;
        private readonly Func<BlockCategoryViewModel> _createBlockCategoryViewModel;
        private readonly Func<UnavailableBlockCategoryViewModel> _createUnavailableBlockCategoryViewModel;
        private readonly List<KeyValuePair<BlockCategoryViewModel, BlockViewModel>> _addedBlockCategories;

        public MultiCalendarInsertCategoryViewContext(IMultiCalendarViewService multiCalendarViewService,
                                                      ISchedulingConfigurationViewService schedulingConfigurationViewService,
                                                      Func<BlockCategoryViewModel> createBlockCategoryViewModel,
                                                      Func<UnavailableBlockCategoryViewModel> createUnavailableBlockCategoryViewModel)
        {
            _multiCalendarViewService = multiCalendarViewService;
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _createBlockCategoryViewModel = createBlockCategoryViewModel;
            _createUnavailableBlockCategoryViewModel = createUnavailableBlockCategoryViewModel;
            _addedBlockCategories = new List<KeyValuePair<BlockCategoryViewModel, BlockViewModel>>();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            IncrementCategories = Command.Create(ExecuteIncrementCategories);
            Close = Command.Create(ExecuteClose);
            Save = Command.Create(ExecuteOkClick, () => SelectedLocation.IsValid()).Async(() => InteractionContext);
            ConfirmConflictOkay = Command.Create(ExecuteConfirmConflictOkClick).Async(() => InteractionContext);
            Closing = Command.Create<Action>(ExecuteClosing);
        }

        private void ExecuteClosing(Action cancelClose)
        {
            if (InteractionContext.DialogResult == false)
            {
                RevertIncrementBlockCategories();
            }
        }

        private void ExecuteLoad()
        {
            ListOfCategory = _multiCalendarViewService.LoadCategories();
            ListOfLocation = _multiCalendarViewService.LoadLocations();

            if (SelectedCategory != null)
            {
                SelectedCategory = ListOfCategory.WithId(SelectedCategory.Id);
            }
            if (SelectedLocation != null)
            {
                SelectedLocation = ListOfLocation.WithId(SelectedLocation.Id);
            }
            else
            {
                var defaultLocation = _schedulingConfigurationViewService.GetSchedulingConfiguration().DefaultLocation;
                if (!IsCreatingNewBlock)
                {
                var firstBlock = SelectedBlocks.First();
                SelectedLocation = SelectedBlocks.All(b => b.Location != null && b.Location.Id == firstBlock.Location.Id)
                                        ? ListOfLocation.WithId(firstBlock.Location.Id)
                                           : defaultLocation;
            }
                else
                {
                    SelectedLocation = defaultLocation;
                }
            }

            // Begin tracking changes so we can undo them later if we "cancel"
            SelectedBlocks.CastTo<IEditableObject>().BeginEdit();
        }

        private void ExecuteOkClick()
        {
            if (_addedBlockCategories.Any())
            {
                SavePlaceHolderBlocks();
                SaveScheduleBlockCategories(_addedBlockCategories);
            }
            SaveLocation();
            SelectedBlocks.CastTo<IEditableObject>().EndEdit();
            InteractionContext.Complete(true);
        }

        private void ExecuteConfirmConflictOkClick()
        {
            if (_addedBlockCategories.Any())
            {
                SavePlaceHolderBlocks();
                SaveScheduleBlockCategories(_addedBlockCategories);
            }
            SelectedBlocks.CastTo<IEditableObject>().EndEdit();
            SelectedBlocks.CastTo<IEditableObject>().BeginEdit();

            bool isLocationSaved = false;
            var conflictedBlocks = SelectedBlocksWithLocationConflict;
            var orphanedAppointments = conflictedBlocks.SelectMany(b => b.Categories).Where(c => c.Appointment != null).Select(c => c.Appointment).ToExtendedObservableCollection();
            if (!orphanedAppointments.Any())
            {
                isLocationSaved = SaveLocation();
            }
            else
            {
                isLocationSaved = SaveLocation();

                if (isLocationSaved)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            var args = new ScheduleLocationConflictArguments();
                            args.ConflictedBlocks = conflictedBlocks;
                            args.OrphanedAppointments = orphanedAppointments;
                            args.SelectedLocation = SelectedLocation;
                            ScheduleLocationConflicts.Execute(args);
                        });
                }
            }

            if (isLocationSaved)
            {
                this.Dispatcher().Invoke(() => InteractionContext.Complete(true));
            }
        }

        private void ExecuteIncrementCategories()
        {
            var selectedBlocks = SelectedBlocks;
            IncrementBlockCategories(selectedBlocks);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void SavePlaceHolderBlocks()
        {
            var placeHolderBlocks = SelectedBlocks.Where(b => !b.Id.HasValue).ToArray();
            if (placeHolderBlocks.Any())
            {
                var resultIds = _multiCalendarViewService.SaveNewScheduleBlocks(placeHolderBlocks).ToList();
                foreach (var placeHolderBlock in placeHolderBlocks)
                {
                    var localPlaceHolderBlock = placeHolderBlock;
                    var categories = _addedBlockCategories.Where(i => i.Value == localPlaceHolderBlock).Select(i => i.Key);

                    categories.ForEachWithSinglePropertyChangedNotification(c => c.BlockId = resultIds.First());
                    placeHolderBlock.Id = resultIds.First();
                    resultIds.RemoveAt(0);
                }
            }
        }

        private void SaveScheduleBlockCategories(List<KeyValuePair<BlockCategoryViewModel, BlockViewModel>> toSave)
        {
            if (!toSave.Any()) return;

            if (toSave.Any(kvp => kvp.Key is UnavailableBlockCategoryViewModel))
            {
                var blocksToSetUnavailable = toSave.Select(kvp => kvp.Value);
                // ReSharper disable PossibleInvalidOperationException
                // Already checking if Id has value
                SaveScheduleBlockUnavailableCategories(blocksToSetUnavailable.Where(b => b.Id.HasValue).Select(b => b.Id.Value));
                // ReSharper restore PossibleInvalidOperationException
                return;
            }

            SelectedBlocks.ForEachWithSinglePropertyChangedNotification(b => b.Comment = null);
            // ReSharper disable PossibleInvalidOperationException
            // The warning is in regards to "b.Id.Value". The above block of logic sets Id values to all blocks not yet saved so it is okay.
            var results = _multiCalendarViewService.AddScheduleBlockCategories(SelectedBlocks.Select(b => b.Id.Value), toSave.Select(i => i.Key.Category.Id).ToArray()).ToList();
            // ReSharper restore PossibleInvalidOperationException

            // set keys returned.
            foreach (var item in toSave)
            {
                var result = results.FirstOrDefault(r => r.Category.Id == item.Key.Category.Id && r.BlockId == item.Key.BlockId);
                if (result != null)
                {
                    results.Remove(result);
                    item.Key.Id = result.Id;
                }
            }
        }

        private void SaveScheduleBlockUnavailableCategories(IEnumerable<int> blockIds)
        {
            _multiCalendarViewService.SetBlocksIsUnavailable(blockIds, true);
        }

        private bool SaveLocation()
        {
            if (!SelectedBlocksWithLocationConflict.Any() || !HasLocationChange) return true;

            /* Block Ids are NOT set for time-slots in case if this is new and no category is set */
            if (SelectedCategory == null || (SelectedBlocksWithLocationConflict.Count > 0 && SelectedBlocksWithLocationConflict.First().Categories.Count <= 0))
            {
                InteractionManager.Current.Alert("There is no category added to the selected slot.  Please add the category.");
                return false;
            }

            // ReSharper disable PossibleInvalidOperationException
            // Block Ids are already set at this point
            var viewServiceArgs = SelectedBlocksWithLocationConflict.ToDictionary(b => b.Id.Value, b => SelectedLocation.Id);
            // ReSharper restore PossibleInvalidOperationException

            SelectedBlocksWithLocationConflict.ToList().ForEach(b => b.Location = SelectedLocation);
            _multiCalendarViewService.SetScheduleBlockLocations(viewServiceArgs);

            return true;
        }

        private void IncrementBlockCategories(IEnumerable<BlockViewModel> blocks)
        {
            if (SelectedCategory == null) return;

            if (SelectedCategory is UnavailableCategoryViewModel)
            {
                IncrementUnavailableBlockCategories(blocks);
                return;
            }

            foreach (var block in blocks)
            {
                var blockCategory = _createBlockCategoryViewModel();
                blockCategory.Category = SelectedCategory;
                blockCategory.BlockId = block.Id;

                if (block.Categories.OfType<UnavailableCategoryViewModel>().Any())
                {
                    block.Categories.Clear();
                    _addedBlockCategories.Clear();
                }

                block.Categories.Add(blockCategory);
                _addedBlockCategories.Add(new KeyValuePair<BlockCategoryViewModel, BlockViewModel>(blockCategory, block));
            }
        }

        private void IncrementUnavailableBlockCategories(IEnumerable<BlockViewModel> blocks)
        {
            foreach (var block in blocks)
            {
                var unavailableBlockCategory = _createUnavailableBlockCategoryViewModel();
                unavailableBlockCategory.BlockId = block.Id;

                foreach (var category in block.Categories.Where(c => c.Appointment != null))
                {
                    // ReSharper disable PossibleNullReferenceException
                    category.Appointment.IsOrphaned = true;
                    // ReSharper restore PossibleNullReferenceException
                    category.Appointment = null;
                }

                block.Categories.Clear();
                _addedBlockCategories.Clear();

                block.Categories.Add(unavailableBlockCategory);
                _addedBlockCategories.Add(new KeyValuePair<BlockCategoryViewModel, BlockViewModel>(unavailableBlockCategory, block));
            }
        }

        private void RevertIncrementBlockCategories()
        {
            SelectedBlocks.CastTo<IEditableObject>().CancelEdit();
            _addedBlockCategories.Clear();
        }

        /// <summary>
        /// List of categories that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ColoredViewModel> ListOfCategory { get; set; }

        /// <summary>
        /// Category selected by user to add to schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel SelectedCategory { get; set; }

        /// <summary>
        /// List of locations that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ColoredViewModel> ListOfLocation { get; set; }

        /// <summary>
        /// Location selected by user to add to schedule template
        /// </summary>
        [RequiredIf("IsCreatingNewBlock", ErrorMessage = "A location is required.")]
        [DispatcherThread]
        public virtual ColoredViewModel SelectedLocation { get; set; }

        /// <summary>
        /// Flag that indicates whether this instance needs to create a new block upon saving.
        /// </summary>
        [DispatcherThread]
        [DependsOn("SelectedBlocks")]
        public virtual bool IsCreatingNewBlock { get { return SelectedBlocks != null && SelectedBlocks.Any(b => b.Id == 0); } }

        /// <summary>
        /// Gets or sets the selected blocks
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<BlockViewModel> SelectedBlocks { get; set; }

        [DispatcherThread]
        [DependsOn("SelectedBlocks", "SelectedLocation")]
        public virtual ObservableCollection<BlockViewModel> SelectedBlocksWithLocationConflict
        {
            get
            {
                if (SelectedBlocks != null && SelectedLocation != null)
                {
                    return SelectedBlocks.Where(b => b.Location != null && b.Location.Id != SelectedLocation.Id).ToExtendedObservableCollection();
                }
                return new ExtendedObservableCollection<BlockViewModel>();
            }
        }

        [DispatcherThread]
        [DependsOn("SelectedBlocksWithLocationConflict")]
        public virtual bool HasBlocksWithLocationConflict
        {
            get { return SelectedBlocksWithLocationConflict.Any(); }
        }

        /// <summary>
        /// Gets or sets whether this can save the selected location
        /// </summary>
        public virtual bool HasLocationChange { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command that is executed when this instance is closing
        /// </summary>
        public virtual ICommand Closing { get; protected set; }

        /// <summary>
        /// Command to add a single given category for the selected time interval
        /// </summary>
        public ICommand IncrementCategories { get; private set; }

        /// <summary>
        /// Command to close UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to confirm changing the location for the new appointment category whose location does not match it's block
        /// </summary>
        public ICommand ConfirmConflictOkay { get; protected set; }

        /// <summary>
        /// Command to save the newly added categories containing no location conflicts
        /// </summary>
        public ICommand Save { get; protected set; }

        /// <summary>
        /// Command to show ScheduleLocationConflicts
        /// </summary>
        public ICommand ScheduleLocationConflicts { get; set; }
    }

    public struct ScheduleLocationConflictArguments
    {
        public ColoredViewModel SelectedLocation { get; set; }
        public ObservableCollection<BlockViewModel> ConflictedBlocks { get; set; }
        public ObservableCollection<AppointmentViewModel> OrphanedAppointments { get; set; }
    }
}