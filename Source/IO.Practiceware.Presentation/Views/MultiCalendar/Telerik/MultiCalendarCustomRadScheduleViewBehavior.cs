﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.Views.Common.Converters;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;
using Appointment = Telerik.Windows.Controls.ScheduleView.Appointment;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    public abstract class MultiCalendarCustomRadScheduleViewBehavior : Behavior<RadScheduleView>
    {
        private readonly ObservableCollection<Appointment> _listOfTelerikAppointment = new ExtendedObservableCollection<Appointment>();
        protected ObservableCollection<Appointment> ListOfTelerikAppointment { get { return _listOfTelerikAppointment; } }

        private AppointmentLeftClickHelper _appointmentLeftClickHelper;

        protected List<Appointment> RecycledAppointments { get; private set; }

        protected MultiCalendarCustomRadScheduleViewBehavior()
        {
            RecycledAppointments = new List<Appointment>();
        }

        #region DependencyProperty : SaveComment

        public ICommand SaveComment
        {
            get { return GetValue(SaveCommentProperty) as ICommand; }
            set { SetValue(SaveCommentProperty, value); }
        }

        public static readonly DependencyProperty SaveCommentProperty = DependencyProperty.Register("SaveComment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : RemoveComment

        public ICommand RemoveComment
        {
            get { return GetValue(RemoveCommentProperty) as ICommand; }
            set { SetValue(RemoveCommentProperty, value); }
        }

        public static readonly DependencyProperty RemoveCommentProperty = DependencyProperty.Register("RemoveComment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : CanForceAppointments

        public bool CanForceAppointments
        {
            get { return (bool)GetValue(CanForceAppointmentsProperty); }
            set { SetValue(CanForceAppointmentsProperty, value); }
        }

        public static readonly string ForceNewAppointmentText = "Force New Appointment";
        public static readonly DependencyProperty CanForceAppointmentsProperty = DependencyProperty.Register("CanForceAppointments", typeof(bool), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : CanChangeCategories

        public bool CanChangeCategories
        {
            get { return (bool)GetValue(CanChangeCategoriesProperty); }
            set { SetValue(CanChangeCategoriesProperty, value); }
        }

        public static readonly DependencyProperty CanChangeCategoriesProperty = DependencyProperty.Register("CanChangeCategories", typeof(bool), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : CanMakeAppointments

        public bool CanMakeAppointments
        {
            get { return (bool)GetValue(CanMakeAppointmentsProperty); }
            set { SetValue(CanMakeAppointmentsProperty, value); }
        }

        public static readonly DependencyProperty CanMakeAppointmentsProperty = DependencyProperty.Register("CanMakeAppointments", typeof(bool), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : EditAppointment

        public ICommand EditAppointment
        {
            get { return GetValue(EditAppointmentProperty) as ICommand; }
            set { SetValue(EditAppointmentProperty, value); }
        }

        public static readonly string EditAppointmentText = "Edit Appointment";
        public static readonly DependencyProperty EditAppointmentProperty = DependencyProperty.Register("EditAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : InsertAppointment

        public ICommand InsertAppointment
        {
            get { return GetValue(InsertAppointmentProperty) as ICommand; }
            set { SetValue(InsertAppointmentProperty, value); }
        }

        public static readonly DependencyProperty InsertAppointmentProperty = DependencyProperty.Register("InsertAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : ForceNewAppointment

        public ICommand ForceNewAppointment
        {
            get { return GetValue(ForceNewAppointmentProperty) as ICommand; }
            set { SetValue(ForceNewAppointmentProperty, value); }
        }

        public static readonly string ForceNewAppointmentAppointmentText = "Force New Appointment";
        public static readonly DependencyProperty ForceNewAppointmentProperty = DependencyProperty.Register("ForceNewAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : InsertCategory

        public ICommand InsertCategory
        {
            get { return GetValue(InsertCategoryProperty) as ICommand; }
            set { SetValue(InsertCategoryProperty, value); }
        }

        public static readonly DependencyProperty InsertCategoryProperty = DependencyProperty.Register("InsertCategory", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : RescheduleAppointmentInContext

        public ICommand RescheduleAppointmentInContext
        {
            get { return GetValue(RescheduleAppointmentInContextProperty) as ICommand; }
            set { SetValue(RescheduleAppointmentInContextProperty, value); }
        }

        public static readonly DependencyProperty RescheduleAppointmentInContextProperty = DependencyProperty.Register("RescheduleAppointmentInContext", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        // TODO put back in once implmented
        //#region DependencyProperty : AppointmentDetails

        //public ICommand AppointmentDetails
        //{
        //    get { return GetValue(AppointmentDetailsProperty) as ICommand; }
        //    set { SetValue(AppointmentDetailsProperty, value); }
        //}

        //public static readonly string AppointmentDetailsText = "Appointment Details";
        //public static readonly DependencyProperty AppointmentDetailsProperty = DependencyProperty.Register("AppointmentDetails", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        //#endregion

        // TODO put back in once implmented
        //#region DependencyProperty : PatientInfo

        //public ICommand PatientInfo
        //{
        //    get { return GetValue(PatientInfoProperty) as ICommand; }
        //    set { SetValue(PatientInfoProperty, value); }
        //}

        //public static readonly string PatientInfoText = "Patient Info";
        //public static readonly DependencyProperty PatientInfoProperty = DependencyProperty.Register("PatientInfo", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        //#endregion

        #region DependencyProperty : RescheduleAppointment

        public ICommand RescheduleAppointment
        {
            get { return GetValue(RescheduleAppointmentProperty) as ICommand; }
            set { SetValue(RescheduleAppointmentProperty, value); }
        }

        public static readonly string RescheduleAppointmentText = "Reschedule Appointment";
        public static readonly DependencyProperty RescheduleAppointmentProperty = DependencyProperty.Register("RescheduleAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : CancelAppointment

        public ICommand CancelAppointment
        {
            get { return GetValue(CancelAppointmentProperty) as ICommand; }
            set { SetValue(CancelAppointmentProperty, value); }
        }

        public static readonly string CancelAppointmentText = "Cancel Appointment";
        public static readonly DependencyProperty CancelAppointmentProperty = DependencyProperty.Register("CancelAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : AppointmentSummary

        public ICommand AppointmentSummary
        {
            get { return GetValue(AppointmentSummaryProperty) as ICommand; }
            set { SetValue(AppointmentSummaryProperty, value); }
        }

        public static readonly string AppointmentSummaryText = "Appointment Summary";
        public static readonly DependencyProperty AppointmentSummaryProperty = DependencyProperty.Register("AppointmentSummary", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : PatientDemographics

        public ICommand PatientDemographics
        {
            get { return GetValue(PatientDemographicsProperty) as ICommand; }
            set { SetValue(PatientDemographicsProperty, value); }
        }

        public static readonly string PatientDemographicsText = "Patient Demographics";
        public static readonly DependencyProperty PatientDemographicsProperty = DependencyProperty.Register("PatientDemographics", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : DeleteScheduleBlockCategory

        public ICommand DeleteScheduleBlockCategory
        {
            get { return GetValue(DeleteScheduleBlockCategoryProperty) as ICommand; }
            set { SetValue(DeleteScheduleBlockCategoryProperty, value); }
        }

        public static readonly DependencyProperty DeleteScheduleBlockCategoryProperty = DependencyProperty.Register("DeleteScheduleBlockCategory", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty : MoveAppointment

        public ICommand MoveAppointment
        {
            get { return GetValue(MoveAppointmentProperty) as ICommand; }
            set { SetValue(MoveAppointmentProperty, value); }
        }

        public static readonly DependencyProperty MoveAppointmentProperty = DependencyProperty.Register("MoveAppointment", typeof(ICommand), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty: DefaultSpanLocation

        public ColoredViewModel DefaultSpanLocation
        {
            get { return GetValue(DefaultSpanLocationProperty) as ColoredViewModel; }
            set { SetValue(DefaultSpanLocationProperty, value); }
        }

        public static readonly DependencyProperty DefaultSpanLocationProperty = DependencyProperty.Register("DefaultSpanLocation", typeof(ColoredViewModel), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty: SpanSize

        public TimeSpan? SpanSize
        {
            get { return GetValue(SpanSizeProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(SpanSizeProperty, value); }
        }

        public static readonly DependencyProperty SpanSizeProperty = DependencyProperty.Register("SpanSize", typeof(TimeSpan?), typeof(MultiCalendarCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertySpanSizeChanged));

        private static void OnPropertySpanSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarCustomRadScheduleViewBehavior)d;
            if (behavior.SpanSize.HasValue && behavior.AssociatedObject != null)
            {
                ((DayViewDefinition)behavior.AssociatedObject.ActiveViewDefinition).MinorTickLength = new FixedTickProvider(new DateTimeInterval(behavior.SpanSize.Value.Minutes, 0, 0, 0, 0));
                behavior.UpdateTimeRulerExtents();
            }
        }

        #endregion

        #region DependencyProperty: AreaStart

        public TimeSpan? AreaStart
        {
            get { return GetValue(AreaStartProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(AreaStartProperty, value); }
        }

        public static readonly DependencyProperty AreaStartProperty = DependencyProperty.Register("AreaStart", typeof(TimeSpan?), typeof(MultiCalendarCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertyAreaStartChanged));

        private static void OnPropertyAreaStartChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarCustomRadScheduleViewBehavior)d;
            if (behavior.AreaStart.HasValue && behavior.AssociatedObject != null && behavior.AssociatedObject.ActiveViewDefinition.DayStartTime != behavior.AreaStart.Value)
            {
                behavior.AssociatedObject.ActiveViewDefinition.DayStartTime = behavior.AreaStart.Value;
                behavior.AssociatedObject.ActiveViewDefinition.Dispatcher.BeginInvoke(() => behavior.AssociatedObject.FirstVisibleTime = behavior.AreaStart.Value, DispatcherPriority.Background);
                behavior.UpdateTimeRulerExtents();
            }
        }

        #endregion

        #region DependencyProperty: AreaEnd

        public TimeSpan? AreaEnd
        {
            get { return GetValue(AreaEndProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(AreaEndProperty, value); }
        }

        public static readonly DependencyProperty AreaEndProperty = DependencyProperty.Register("AreaEnd", typeof(TimeSpan?), typeof(MultiCalendarCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertyAreaEndChanged));

        private static void OnPropertyAreaEndChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarCustomRadScheduleViewBehavior)d;
            if (behavior.AreaEnd.HasValue && behavior.AssociatedObject != null && behavior.AssociatedObject.ActiveViewDefinition.DayEndTime != behavior.AreaEnd.Value)
            {
                behavior.AssociatedObject.ActiveViewDefinition.DayEndTime = behavior.AreaEnd.Value;
                if (behavior.AreaStart.HasValue)
                {
                    behavior.AssociatedObject.ActiveViewDefinition.Dispatcher.BeginInvoke(() => behavior.AssociatedObject.FirstVisibleTime = behavior.AreaStart.Value, DispatcherPriority.Background);
                    behavior.UpdateTimeRulerExtents();
                }
            }
        }

        #endregion

        #region DependencyProperty: MinTimeRulerExtent

        public double MinTimeRulerExtent
        {
            get { return (double)GetValue(MinTimeRulerExtentProperty); }
            set { SetValue(MinTimeRulerExtentProperty, value); }
        }

        public static readonly DependencyProperty MinTimeRulerExtentProperty = DependencyProperty.Register("MinTimeRulerExtent", typeof(double), typeof(MultiCalendarCustomRadScheduleViewBehavior));

        #endregion

        #region Helpers

        public abstract IEnumerable<BlockViewModel> GetBlocks();

        public abstract void RefreshGui();

        private void UpdateTimeRulerExtents()
        {
            const int appointmentPixelHeight = 25;

            var extent = AssociatedObject.ActualHeight;
            int appointmentCount = 0;
            if (AreaStart.HasValue && AreaEnd.HasValue && SpanSize.HasValue)
            {
                appointmentCount = Convert.ToInt32(((AreaEnd.Value - AreaStart.Value).TotalMinutes) / SpanSize.Value.TotalMinutes);
            }

            extent = Math.Max(extent, appointmentPixelHeight * appointmentCount);
            MinTimeRulerExtent = extent;
        }

        protected void PromptForSaveComment(BlockViewModel block)
        {
            var prompt = InteractionManager.Current.Prompt("Enter the reason this is unavailable below:", "Comment", block.Comment);
            if (prompt.DialogResult.HasValue && (bool)prompt.DialogResult)
            {
                block.Comment = prompt.Input;
                SaveComment.Execute(block);
            }
        }

        public void ScrollToFirstBlock()
        {
            if (!ListOfTelerikAppointment.Any()) return;

            var firstBlock = ListOfTelerikAppointment.Except(ListOfTelerikAppointment.OfType<NoBlockAppointment>()).OrderBy(a => a.Start).FirstOrDefault();
            if (firstBlock != null)
            {
                AssociatedObject.Dispatcher.BeginInvoke(() => AssociatedObject.FirstVisibleTime = firstBlock.Start.TimeOfDay, DispatcherPriority.Background);
            }
        }

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            SetAppointmentLeftClickHandler();
            SetDayViewDefinition();
            SetTelerikSlotMouseOverHighlight();
            SetHideToolTip();
            SetAppointmentStyle();
            SetAppointmentDragDrop();

            AssociatedObject.SnapAppointments = true;
            AssociatedObject.IsInlineEditingEnabled = false;
            AssociatedObject.NavigationHeaderVisibility = Visibility.Collapsed;
            AssociatedObject.AppointmentsSource = ListOfTelerikAppointment;

            AssociatedObject.ShowDialog += WeakDelegate.MakeWeak<EventHandler<ShowDialogEventArgs>>(ShowDialogHandler);
            AssociatedObject.AppointmentDeleting += WeakDelegate.MakeWeak<EventHandler<AppointmentDeletingEventArgs>>(AppointmentDeletingHandler);

            AssociatedObject.IsDeferredScrollingEnabled = true;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            RemoveAppointmentLeftClickHandler();
            RemoveDayViewDefinition();
            RemoveTelerikMouseOverHighlight();
            RemoveHideToolTip();
            RemoveAppointmentStyle();
            RemoveAppointmentDragDrop();

            AssociatedObject.SnapAppointments = false;
            AssociatedObject.IsInlineEditingEnabled = true;
            AssociatedObject.NavigationHeaderVisibility = Visibility.Visible;
            AssociatedObject.SpecialSlotsSource = null;
            AssociatedObject.AppointmentsSource = null;

            AssociatedObject.ShowDialog -= WeakDelegate.MakeWeak<EventHandler<ShowDialogEventArgs>>(ShowDialogHandler);
            AssociatedObject.AppointmentDeleting -= WeakDelegate.MakeWeak<EventHandler<AppointmentDeletingEventArgs>>(AppointmentDeletingHandler);
        }

        #region OnAttachedHelpers

        private void SetAppointmentLeftClickHandler()
        {
            _appointmentLeftClickHelper = new AppointmentLeftClickHelper(this, ListOfTelerikAppointment);
        }

        private void SetDayViewDefinition()
        {
            var dayViewDefinition = new DayViewDefinition();
            dayViewDefinition.ShowAllDayArea = true;
            dayViewDefinition.MajorTickLength = new FixedTickProvider(new DateTimeInterval(0, 1, 0, 0, 0));

            BindingOperations.SetBinding(dayViewDefinition, MultidayViewDefinition.MinorTickLengthProperty, new Binding("SpanSize") { Source = this, Converter = new TimeSpanToTickLengthConverter() });
            dayViewDefinition.TimerulerMinorTickStringFormat = ":{0:%m}";

            AssociatedObject.ViewDefinitions.Add(dayViewDefinition);
        }

        private void SetTelerikSlotMouseOverHighlight()
        {
            _selectionHighlightStyle = AssociatedObject.SelectionHighlightStyle;
            _mouseOverHighlightStyle = AssociatedObject.MouseOverHighlightStyle;
            var style = new Style();
            style.TargetType = typeof(HighlightItem);
            style.Setters.Add(new Setter(Control.BackgroundProperty, new SolidColorBrush(Colors.Transparent)));
            AssociatedObject.SelectionHighlightStyle = style;
            AssociatedObject.MouseOverHighlightStyle = style;
        }

        private void SetHideToolTip()
        {
            _defaultToolTipTemplate = AssociatedObject.ToolTipTemplate;
            AssociatedObject.ToolTipTemplate = null;
        }

        private void SetAppointmentDragDrop()
        {
            _dragDropBehavior = AssociatedObject.DragDropBehavior;
            AssociatedObject.DragDropBehavior = new AppointmentDragDropBehavior(this);
        }

        private void SetAppointmentStyle()
        {
            _appointmentStyleSelector = AssociatedObject.AppointmentStyleSelector;

            var resourceDictionary = new ResourceDictionary { Source = new Uri("IO.Practiceware.Presentation;component/Views/MultiCalendar/Telerik/MultiCalendarViewResources.xaml", UriKind.Relative) };

            var appointmentStyleSelector = new AppointmentStyleSelector
            {
                BlockCategoryWithAppointmentStyle = (Style)resourceDictionary["BlockCategoryWithAppointmentStyle"],
                UnavailableBlockAppointmentStyle = (Style)resourceDictionary["UnavailableBlockAppointmentStyle"],
                NoBlockStyle = (Style)resourceDictionary["NoBlockStyle"],
                BlockOnlyStyle = (Style)resourceDictionary["BlockOnlyStyle"],
                BlockCategoryNoAppointmentStyle = (Style)resourceDictionary["BlockCategoryNoAppointmentStyle"],
                OrphanedAppointmentStyle = (Style)resourceDictionary["OrphanedAppointmentStyle"]
            };
            AssociatedObject.AppointmentStyleSelector = appointmentStyleSelector;
        }

        #endregion

        #region OnDetachingHelpers

        private void RemoveAppointmentLeftClickHandler()
        {
            _appointmentLeftClickHelper.Dispose();
        }

        private void RemoveDayViewDefinition()
        {
            AssociatedObject.ViewDefinitions.RemoveAll();
        }

        private void RemoveTelerikMouseOverHighlight()
        {
            AssociatedObject.SelectionHighlightStyle = _selectionHighlightStyle;
            AssociatedObject.MouseOverHighlightStyle = _mouseOverHighlightStyle;
        }

        private void RemoveHideToolTip()
        {
            AssociatedObject.ToolTipTemplate = _defaultToolTipTemplate;
        }

        private void RemoveAppointmentDragDrop()
        {
            AssociatedObject.DragDropBehavior = _dragDropBehavior;
        }

        private void RemoveAppointmentStyle()
        {
            AssociatedObject.AppointmentStyleSelector = _appointmentStyleSelector;
        }

        #endregion

        #region HelpersVariables

        private static Style _selectionHighlightStyle;
        private static Style _mouseOverHighlightStyle;
        private static DataTemplate _defaultToolTipTemplate;
        private static ScheduleViewDragDropBehavior _dragDropBehavior;
        private static ScheduleViewStyleSelector _appointmentStyleSelector;

        #endregion

        #region HelpersInnerClasses

        // Used to set appointment to specific color 
        private class AppointmentStyleSelector : OrientedAppointmentItemStyleSelector
        {
            public Style BlockOnlyStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public Style BlockCategoryNoAppointmentStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public Style BlockCategoryWithAppointmentStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public Style OrphanedAppointmentStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public Style UnavailableBlockAppointmentStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public Style NoBlockStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                var appointmentItem = container as AppointmentItem;
                var blockCategoryAppointment = item as BlockCategoryAppointment;

                if (appointmentItem != null)
                {
                    if (blockCategoryAppointment != null)
                    {
                        appointmentItem.IsEnabled = true;

                        if (blockCategoryAppointment.Appointment == null) { return BlockCategoryNoAppointmentStyle; }

                        return blockCategoryAppointment.Appointment.IsOrphaned ? OrphanedAppointmentStyle : BlockCategoryWithAppointmentStyle;
                    }

                    if (item is OnlyBlockAppointment)
                    {
                        appointmentItem.IsEnabled = true;
                        return BlockOnlyStyle;
                    }

                    if (item is OrphanedAppointment)
                    {
                        appointmentItem.IsEnabled = true;
                        return OrphanedAppointmentStyle;
                    }
                    if (item is UnavailableBlockAppointment)
                    {
                        appointmentItem.IsEnabled = true;
                        return UnavailableBlockAppointmentStyle;
                    }

                    if (item is NoBlockAppointment)
                    {
                        appointmentItem.IsEnabled = true;
                        return NoBlockStyle;
                    }
                }

                return base.SelectStyle(item, container, activeViewDefinition);
            }
        }

        #region AppointmentLeftClickHandler

        private class AppointmentLeftClickHelper : IDisposable
        {
            private readonly ObservableCollection<Appointment> _appointments;
            private readonly MultiCalendarCustomRadScheduleViewBehavior _behavior;

            public AppointmentLeftClickHelper(MultiCalendarCustomRadScheduleViewBehavior behavior, ObservableCollection<Appointment> appointments)
            {
                _behavior = behavior;
                _appointments = appointments;

                SetupAppointmentCommands(appointments);
                _appointments.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnListOfTelerikAppointmentCollectionChanged);
            }

            private void SetupAppointmentCommands(IList<Appointment> appointments)
            {
                SetupCanEditAppointmentAppointments(appointments.OfType<ICanEditAppointment>());
                SetupCanInsertAppointmentAppointments(appointments.OfType<ICanInsertAppointment>());
                SetupCanInsertCategoryAppointments(appointments.OfType<ICanInsertCategory>());
                SetupCanRescheduleAppointments(appointments.OfType<ICanRescheduleAppointment>());
                SetupCanViewAppointmentSummary(appointments.OfType<ICanViewAppointmentSummmary>());
                SetupCanViewPatientDemographics(appointments.OfType<ICanViewPatientDemographics>());
                SetupLeftClickDropDownAppointments(appointments.OfType<IHasLeftClickDropDown>());
                SetupUnavailableBlockAppointmentComments(appointments.OfType<UnavailableBlockAppointment>());
            }


            private void OnListOfTelerikAppointmentCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
            {
                SetupAppointmentCommands(_appointments);
            }

            #region helper private methods

            private void SetupLeftClickDropDownAppointments(IEnumerable<IHasLeftClickDropDown> appointments)
            {
                foreach (var appointment in appointments)
                {
                    appointment.LeftClickDropDownItems.RemoveAll();
                    //TODO uncomment after patient info screen is implemented
                    //appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(AppointmentDetailsText, _behavior.AppointmentDetails));

                    // TODO Uncomment the following when the PatientDemographics screen is developed
                    //appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(PatientInfoText, _behavior.PatientInfo));

                    if (PermissionId.ScheduleAppointment.PrincipalContextHasPermission())
                    {
                        appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(EditAppointmentText, _behavior.EditAppointment));
                    }

                    if (PermissionId.RescheduleAppointment.PrincipalContextHasPermission())
                    {
                        appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(RescheduleAppointmentText, _behavior.RescheduleAppointment));
                    }

                    if (PermissionId.CancelAppointment.PrincipalContextHasPermission())
                    {
                        appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(CancelAppointmentText, _behavior.CancelAppointment));
                    }

                    //Need to add PatientInfo permission.Once created we can apply it for AppointmentSummary
                    appointment.LeftClickDropDownItems.Add(new Tuple<String, ICommand>(AppointmentSummaryText, _behavior.AppointmentSummary));


                    if (PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission())
                    {
                        appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(ForceNewAppointmentText, _behavior.ForceNewAppointment));
                    }
                    appointment.LeftClickDropDownItems.Add(new Tuple<string, ICommand>(PatientDemographicsText, _behavior.PatientDemographics));
                }
            }

            private void SetupCanInsertCategoryAppointments(IEnumerable<ICanInsertCategory> appointments)
            {
                if (PermissionId.InsertDeleteCategoriesInSchedule.PrincipalContextHasPermission())
                {
                    appointments.ForEachWithSinglePropertyChangedNotification(a => a.InsertCategory = _behavior.InsertCategory);
                }
            }

            private void SetupCanInsertAppointmentAppointments(IEnumerable<ICanInsertAppointment> appointments)
            {
                if (PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission())
                {
                    appointments.ToList().ForEach(a => a.InsertAppointment = _behavior.InsertAppointment);
                }
                else
                {
                    appointments.Where(a => !(a is UnavailableBlockAppointment)).ToList().ForEach(a => a.InsertAppointment = _behavior.InsertAppointment);
                }
            }

            private void SetupCanEditAppointmentAppointments(IEnumerable<ICanEditAppointment> appointments)
            {
                appointments.ForEachWithSinglePropertyChangedNotification(a => a.EditAppointment = _behavior.EditAppointment);
            }

            private void SetupCanRescheduleAppointments(IEnumerable<ICanRescheduleAppointment> appointments)
            {
                appointments.ToList().ForEach(a => a.RescheduleAppointment = _behavior.RescheduleAppointmentInContext);
            }

            private void SetupUnavailableBlockAppointmentComments(IEnumerable<UnavailableBlockAppointment> appointments)
            {
                foreach (var unavailableAppointment in appointments)
                {
                    unavailableAppointment.PromptForSaveComment = Command.Create<BlockViewModel>(_behavior.PromptForSaveComment);
                    unavailableAppointment.RemoveComment = _behavior.RemoveComment;
                }
            }

            private void SetupCanViewAppointmentSummary(IEnumerable<ICanViewAppointmentSummmary> appointments)
            {
                appointments.ToList().ForEach(a => a.AppointmentSummary = _behavior.AppointmentSummary);
            }


            private void SetupCanViewPatientDemographics(IEnumerable<ICanViewPatientDemographics> appointments)
            {
                appointments.ToList().ForEach(a=>a.PatientDemographics = _behavior.PatientDemographics);
            }
            #endregion

            public void Dispose()
            {
                Dispose(true);
            }

            // ReSharper disable MemberCanBePrivate.Local
            // ReSharper disable UnusedParameter.Local
            // Following microsoft disposal pattern
            protected void Dispose(bool isDisposing)
            // ReSharper restore UnusedParameter.Local
            {
                if (!IsDisposed)
                {
                    _appointments.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnListOfTelerikAppointmentCollectionChanged);
                    IsDisposed = true;
                }
            }

            public bool IsDisposed { get; set; }
            // ReSharper restore MemberCanBePrivate.Local
        }

        #endregion

        #region AppointmentDragDrop

        public class AppointmentDragDropBehavior : ScheduleViewDragDropBehavior
        {
            private readonly MultiCalendarCustomRadScheduleViewBehavior _behavior;

            private Point _dragMousePosition;

            public AppointmentDragDropBehavior(MultiCalendarCustomRadScheduleViewBehavior behavior)
            {
                _behavior = behavior;
                _behavior.AssociatedObject.PreviewDragOver += WeakDelegate.MakeWeak<DragEventHandler>(OnDragOver,
                    handler => _behavior.AssociatedObject.PreviewDragOver -= handler);
            }

            void OnDragOver(object sender, DragEventArgs e)
            {
                _dragMousePosition = _behavior.AssociatedObject.PointToScreen(e.GetPosition(_behavior.AssociatedObject));
            }

            public override bool CanDrop(DragDropState state)
            {
                bool canDrop;

                var draggedAppointment = state.Appointment.As<IHasAppointmentViewModel>().IfNotNull(a => a.Appointment);

                var targetAppointment = GetTargetAppointment(state);
                if (targetAppointment == null) return false;

                if (PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission())
                {
                    if (targetAppointment is OrphanedAppointment)
                    {
                        canDrop = targetAppointment.As<OrphanedAppointment>().Block != null;
                    }
                    else
                    {
                        canDrop = !targetAppointment.Is<NoBlockAppointment>();
                    }
                }
                else
                {
                    if (targetAppointment is OrphanedAppointment)
                    {
                        canDrop = false;
                    }
                    else
                    {
                        canDrop = targetAppointment.As<BlockCategoryAppointment>().IfNotNull(c => c.Appointment == null
                                                                                                  && c.BlockCategory.Category.Id == draggedAppointment.AppointmentCategoryId);
                    }
                }
                return canDrop;
            }

            private Appointment GetTargetAppointment(DragDropState state)
            {
                if (state.DestinationSlots.Count() != 1) return null;

                var draggedAppointment = state.Appointment.As<IHasAppointmentViewModel>().IfNotNull(a => a.Appointment);
                if (draggedAppointment == null) return null;

                var mouseOverAppointments = new List<Appointment>();
                foreach (var element in _behavior.AssociatedObject.ChildrenOfType<AppointmentItem>())
                {
                    var rect = new Rect(element.PointToScreen(new Point(0, 0)), new Size(element.ActualWidth, element.ActualHeight));
                    bool isInside = rect.Contains(_dragMousePosition);

                    if (isInside) mouseOverAppointments.Add((Appointment)element.Appointment);
                }

                if (mouseOverAppointments.Count != 1) return null;

                return mouseOverAppointments.Single();
            }

            public override void Drop(DragDropState state)
            {
                var draggedAppointment = state.Appointment.As<IHasAppointmentViewModel>().IfNotNull(a => a.Appointment);

                var targetAppointment = GetTargetAppointment(state);
                if (targetAppointment == null)
                {
                    return;
                }

                var block = targetAppointment.As<IHasScheduleBlock>().IfNotNull(a => a.Block);
                var blockCategory = targetAppointment.As<IHasScheduleBlockCategory>().IfNotNull(a => a.BlockCategory);

                _behavior.MoveAppointment.Execute(new MoveAppointmentArguments
                {
                    Appointment = draggedAppointment,
                    TargetBlock = block,
                    TargetBlockCategory = blockCategory
                });
            }

            public override bool CanStartDrag(DragDropState state)
            {
                if (state.DraggedAppointments.Count() != 1) return false;

                var hasAppointment = state.DraggedAppointments.First() as IHasAppointmentViewModel;
                return hasAppointment != null && hasAppointment.Appointment != null;
            }

            public override bool CanResize(DragDropState state)
            {
                return false;
            }

            public override bool CanStartResize(DragDropState state)
            {
                return false;
            }
        }

        #endregion

        #endregion

        #region EventHandlers

        private static void ShowDialogHandler(object sender, ShowDialogEventArgs e)
        {
            if (e.DialogViewModel is AppointmentDialogViewModel) e.Cancel = true;
            if (e.DialogViewModel is ConfirmDialogViewModel)
            {
                e.DefaultDialogResult = true;
                e.Cancel = true;
            }
        }

        private void AppointmentDeletingHandler(object sender, AppointmentDeletingEventArgs e)
        {
            var unavailable = e.Appointment as UnavailableBlockAppointment;
            var blockCategoryAppointment = e.Appointment as BlockCategoryAppointment;

            var hasAppointmentInstance = e.Appointment as IHasAppointmentViewModel;

            if (unavailable != null)
            {
                DeleteScheduleBlockCategory.Execute(unavailable.BlockCategory);
            }

            if (blockCategoryAppointment != null && blockCategoryAppointment.Appointment == null)
            {
                DeleteScheduleBlockCategory.Execute(blockCategoryAppointment.BlockCategory);
            }

            if (hasAppointmentInstance != null && hasAppointmentInstance.Appointment != null)
            {
                CancelAppointment.Execute(hasAppointmentInstance);
            }

            e.Cancel = true;
        }

        #endregion

        #region PropertiesWatchers


        #endregion
    }



}
