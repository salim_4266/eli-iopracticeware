﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    public class MultiCalendarScheduleViewHeaderResource : Resource, INotifyPropertyChanged
    {
        /// <summary>
        /// This is needed because the header heights are dynamically sized based on content.
        /// Using this collection of LocationHour place holders, we can size the headers to be the same height.
        /// </summary>
        public virtual ObservableCollection<object> LocationHourPlaceHolders { get; set; }
        public virtual ObservableCollection<LocationHoursViewModel> LocationHours { get; set; }
        public virtual ObservableCollection<Tuple<string, ICommand>> DropDownItems { get; set; }
        public virtual Tuple<string, ICommand> SelectedDropDownItem { get; set; }

        public int ResourceId { get; set; }
        public DateTime Date { get; set; }
        public Tuple<int, DateTime> ResourceDate { get { return Tuple.Create(ResourceId, Date); } }

        public virtual int EncountersUsed { get; set; }
        public virtual int TotalEncounters { get; set; }

        public virtual int AM { get; set; }
        public virtual int PM { get; set; }
        public virtual int AMScheduleCnt { get; set; }
        public virtual int PMScheduleCnt { get; set; }
        public virtual string ShowAMPM { get; set; }
        public virtual string ShowDefault { get; set; }
        public virtual HeaderComment ResourceHeaderComment { get; set; }
        public virtual HeaderComment GlobalHeaderComment { get; set; }
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { }
            remove { }
        }
    }

    public enum HeaderCommentType
    {
        Resource,
        Global
    }

    public class HeaderComment : ViewModelBase
    {
        private readonly HeaderCommentType _type;
        private readonly ICommand _addNewComment;
        private readonly ICommand _editComment;
        private readonly ICommand _deleteComment;
        private CalendarCommentViewModel _calendarCommentViewModel;

        private string PromptMessage
        {
            get { return String.Format("Enter the note you would like to appear as a {0} in schedules below:", TypeDisplayName); }
        }

        public HeaderComment(HeaderCommentType type, CalendarCommentViewModel calendarCommentViewModel)
        {
            _type = type;
            CommentViewModel = calendarCommentViewModel;
            _addNewComment = Command.Create<MultiCalendarScheduleViewHeaderResource>(OnAddNewComment);
            _editComment = Command.Create<MultiCalendarScheduleViewHeaderResource>(OnEditComment);
            _deleteComment = Command.Create<MultiCalendarScheduleViewHeaderResource>(OnDeleteComment);
        }

        public ICommand AddNewCommentAtDatabase { get; set; }
        public ICommand EditCommentAtDatabase { get; set; }
        public ICommand DeleteCommentAtDatabase { get; set; }

        public string NoCommentToolTip
        {
            get
            {
                const string message = "no comment available";
                return PermissionId.CreateGlobalAndResourceScheduleComments.PrincipalContextHasPermission() ? (message + ", click to add a comment") : message;
            }
        }

        public bool NoComment
        {
            get { return String.IsNullOrWhiteSpace(Comment); }
        }

        public bool HasComment
        {
            get { return !String.IsNullOrWhiteSpace(Comment); }
        }

        public string TypeDisplayName
        {
            get { return _type == HeaderCommentType.Resource ? "Doctor Comment" : "Global Comment"; }
        }

        public string Comment
        {
            get { return _calendarCommentViewModel != null ? _calendarCommentViewModel.Value : string.Empty; }
            set
            {
                _calendarCommentViewModel.Value = value;
            }
        }

        public CalendarCommentViewModel CommentViewModel
        {
            get { return _calendarCommentViewModel; }
            private set
            {
                // Disabling Resharper warnings due to the fact that casting IViewModel to INotifyPropertyChanged is not directly apparent.
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                // ReSharper disable ExpressionIsAlwaysNull
                // ReSharper disable SuspiciousTypeConversion.Global
                // ReSharper disable HeuristicUnreachableCode
                // ReSharper disable RedundantAssignment
                var propertyChanged = _calendarCommentViewModel as INotifyPropertyChanged;
                if(propertyChanged != null)
                {
                    propertyChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(CommentViewModelPropertyChanged);
                }
                _calendarCommentViewModel = value;

                propertyChanged = value as INotifyPropertyChanged;

                if (propertyChanged != null)
                {
                    propertyChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(CommentViewModelPropertyChanged);
                }
                // ReSharper restore HeuristicUnreachableCode
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper restore ExpressionIsAlwaysNull
                // ReSharper restore SuspiciousTypeConversion.Global
                // ReSharper restore RedundantAssignment
            }
        }

        public ICommand AddNewComment
        {
            get { return _addNewComment; }
        }

        public ICommand EditComment
        {
            get { return _editComment; }
        }

        public ICommand DeleteComment
        {
            get { return _deleteComment; }
        }

        private void CommentViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Comment");
            OnPropertyChanged("TypeDisplayName");
            OnPropertyChanged("HasComment");
            OnPropertyChanged("NoComment");
            OnPropertyChanged("NoCommentToolTip");
        }

        private void OnAddNewComment(MultiCalendarScheduleViewHeaderResource telerikResource)
        {
            SaveComment();
            if (Comment != null) AddNewCommentAtDatabase.Execute(GetCalendarComment(telerikResource));
        }

        private void OnEditComment(MultiCalendarScheduleViewHeaderResource telerikResource)
        {
            SaveComment();
            if (Comment != null)
                EditCommentAtDatabase.Execute(GetCalendarComment(telerikResource));
            else
                DeleteCommentAtDatabase.Execute(GetCalendarComment(telerikResource));
        }

        private void OnDeleteComment(MultiCalendarScheduleViewHeaderResource telerikResource)
        {
            if (!PermissionId.CreateGlobalAndResourceScheduleComments.PrincipalContextHasPermission()) return;
            Comment = null;
            DeleteCommentAtDatabase.Execute(GetCalendarComment(telerikResource));
        }

        private void SaveComment()
        {
            if (!PermissionId.CreateGlobalAndResourceScheduleComments.PrincipalContextHasPermission()) return;
            var prompt = InteractionManager.Current.Prompt(PromptMessage, TypeDisplayName, Comment);
            if (prompt.DialogResult.HasValue && (bool)prompt.DialogResult)
            {
                Comment = String.IsNullOrWhiteSpace(prompt.Input) ? null : prompt.Input.Trim();
            }
        }

        private CalendarCommentViewModel GetCalendarComment(MultiCalendarScheduleViewHeaderResource telerikResource)
        {
            _calendarCommentViewModel.Date = telerikResource.Date;
            if (_type == HeaderCommentType.Resource) _calendarCommentViewModel.UserId = telerikResource.ResourceId;
            return _calendarCommentViewModel;
        }
    }
}