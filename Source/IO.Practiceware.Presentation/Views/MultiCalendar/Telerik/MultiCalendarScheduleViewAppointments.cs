﻿using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{

    #region TelerikAppointmentWrapper

    public interface IHasAppointmentViewModel
    {
        AppointmentViewModel Appointment { get; }
    }

    public interface ICanInsertCategory
    {
        ICommand InsertCategory { get; set; }
    }

    public interface IHasLeftClickDropDown
    {
        ObservableCollection<Tuple<string, ICommand>> LeftClickDropDownItems { get; }
    }

    public interface ICanEditAppointment
    {
        ICommand EditAppointment { get; set; }
    }

    public interface ICanInsertAppointment
    {
        ICommand InsertAppointment { get; set; }
    }

    public interface ICanRescheduleAppointment
    {
        ICommand RescheduleAppointment { get; set; }
    }

    public interface IHasScheduleBlock
    {
        BlockViewModel Block { get; }
    }

    public interface IHasScheduleBlockCategory : IHasScheduleBlock
    {
        BlockCategoryViewModel BlockCategory { get; }
    }

    public interface IRecyclableAppointment
    {
        void Clear();
    }

    public interface ICanViewAppointmentSummmary
    {
        ICommand AppointmentSummary { get; set; }
    }

    public interface ICanViewPatientDemographics
    {
        ICommand PatientDemographics { get; set; }
    }

    #endregion

    #region NoBlockAppointment

    /// <summary>
    /// No wrapper - Created by behavior
    /// </summary>
    public sealed class NoBlockAppointment : Appointment,
                                             ICanInsertCategory,
                                             IHasScheduleBlock,
                                             IRecyclableAppointment
    {
        private BlockViewModel _block;

        public NoBlockAppointment(BlockViewModel block)
        {
            Subject = String.Empty;
            Category = new Category { CategoryBrush = new SolidColorBrush(Colors.LightGray) };
            Initialize(block);
        }

        public void Initialize(BlockViewModel block)
        {
            Start = DateTime.Today.Add(block.StartDateTime.TimeOfDay);
            End = DateTime.Today.Add(block.EndDateTime.TimeOfDay);
            
            // If the block's EndDateTime ends on the next day (at midnight), then adjust for the difference.
            if(block.EndDateTime.Date.Subtract(block.StartDateTime.Date) == TimeSpan.FromDays(1))
            {
                End = End.AddDays(1);
            }

            _block = block;
            OnPropertyChanged(() => Block);
        }

        public void Clear()
        {
            _block = null;
            OnPropertyChanged(() => Block);
        }

        public ICommand InsertCategory { get; set; }
        public BlockViewModel Block { get { return _block; } }
    }

    #endregion

    #region OnlyBlockAppointment

    /// <summary>
    /// Wraps BlockViewModel
    /// </summary>
    public sealed class OnlyBlockAppointment : Appointment, ICanInsertCategory,
                                               ICanInsertAppointment,
                                               ICanRescheduleAppointment,
                                               IHasScheduleBlock,
                                               IRecyclableAppointment
    {
        private BlockViewModel _block;

        public OnlyBlockAppointment(BlockViewModel block)
        {
            Subject = String.Empty;
            Category = new Category {CategoryBrush = new SolidColorBrush(Colors.White)};
            Initialize(block);
        }

        public void Initialize(BlockViewModel block)
        {
            Start = DateTime.Today.Add(block.StartDateTime.TimeOfDay);
            End = DateTime.Today.Add(block.EndDateTime.TimeOfDay);
            
            // If the block's EndDateTime ends on the next day (at midnight), then adjust for the difference.
            if (block.EndDateTime.Date.Subtract(block.StartDateTime.Date) == TimeSpan.FromDays(1))
            {
                End = End.AddDays(1);
            }

            _block = block;
            OnPropertyChanged(() => Block);
        }

        public void Clear()
        {
            _block = null;
            OnPropertyChanged(() => Block);
        }

        public BlockViewModel Block
        {
            get { return _block; }
        }

        public bool CanRescheduleAppointment { get; set; }
        public ICommand RescheduleAppointment { get; set; }

        public ICommand InsertCategory { get; set; }
        public ICommand InsertAppointment { get; set; }
        public ICommand AppointmentSummary { get; set; }
    }

    #endregion

    #region UnavailableBlockAppointment

    /// <summary>
    /// Wraps UnavailableBlockCategoryViewModel - Can have comment set
    /// </summary>
    public sealed class UnavailableBlockAppointment : Appointment, IHasScheduleBlockCategory,
                                                      ICanInsertAppointment,
                                                      IRecyclableAppointment
    {
        private UnavailableBlockCategoryViewModel _category;
        private BlockViewModel _block;

        public UnavailableBlockAppointment(UnavailableBlockCategoryViewModel category, BlockViewModel block)
        {
            Initialize(category, block);
        }

        public void Initialize(UnavailableBlockCategoryViewModel category, BlockViewModel block)
        {
            Subject = String.IsNullOrWhiteSpace(block.Comment) ? category.Category.Name : String.Format("{0}: {1}", category.Category.Name, block.Comment);
            Start = DateTime.Today.Add(block.StartDateTime.TimeOfDay);
            End = DateTime.Today.Add(block.EndDateTime.TimeOfDay);
            
            // If the block's EndDateTime ends on the next day (at midnight), then adjust for the difference.
            if (block.EndDateTime.Date.Subtract(block.StartDateTime.Date) == TimeSpan.FromDays(1))
            {
                End = End.AddDays(1);
            }

            Category = new Category {CategoryBrush = new SolidColorBrush(category.Category.Color)};
            _category = category;
            _block = block;
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => BlockCategory);
        }

        public void Clear()
        {
            _block = null;
            _category = null;
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => BlockCategory);
        }

        public ICommand PromptForSaveComment { get; set; }
        public ICommand RemoveComment { get; set; }

        public bool SupportsComments
        {
            get { return true; }
        }

        public bool HasComment
        {
            get { return !String.IsNullOrWhiteSpace(_block.Comment); }
        }

        public UnavailableBlockCategoryViewModel BlockCategory
        {
            get { return _category; }
        }

        public BlockViewModel Block
        {
            get { return _block; }
        }

        BlockCategoryViewModel IHasScheduleBlockCategory.BlockCategory
        {
            get { return _category; }
        }

        public ICommand InsertAppointment { get; set; }

    }

    #endregion

    #region BlockCategoryAppointment

    /// <summary>
    /// Wraps BlockCategoryViewModel - Can have IO AppointmentID set
    /// </summary>
    public sealed class BlockCategoryAppointment : Appointment, IHasAppointmentViewModel,
                                                   IHasLeftClickDropDown,
                                                   ICanInsertCategory,
                                                   ICanEditAppointment,
                                                   ICanInsertAppointment,
                                                   ICanRescheduleAppointment,
                                                   ICanViewPatientDemographics,
                                                   ICanViewAppointmentSummmary,
                                                   IHasScheduleBlockCategory,
                                                   IRecyclableAppointment
    {
        private BlockCategoryViewModel _category;
        private BlockViewModel _block;

        public BlockCategoryAppointment(BlockCategoryViewModel category, BlockViewModel block)
        {
            Initialize(category, block);
        }

        public void Initialize(BlockCategoryViewModel category, BlockViewModel block)
        {

            Subject = category.Appointment == null ? category.Category.Name 
                                                   : "{0}, {1}, {2}, {3}, {4}, {5}, {6}".FormatWith(category.Appointment.Patient.LastName,
                                                                                                    category.Appointment.Patient.FirstName,
                                                                                                    category.Appointment.AppointmentType.Name,
                                                                                                    category.Appointment.Location.Name,
                                                                                                    category.Appointment.Comments,
                                                                                                    category.Appointment.Resource.DisplayName,
                                                                                                    category.Appointment.Patient.CellPhone);
            Start = DateTime.Today.Add(block.StartDateTime.TimeOfDay);
            End = DateTime.Today.Add(block.EndDateTime.TimeOfDay);

            // If the block's EndDateTime ends on the next day (at midnight), then adjust for the difference.
            if (block.EndDateTime.Date.Subtract(block.StartDateTime.Date) == TimeSpan.FromDays(1))
            {
                End = End.AddDays(1);
            }

            Category = category.Appointment == null ? new Category {CategoryBrush = new SolidColorBrush(category.Category.Color)} 
                                                    : new Category {CategoryBrush = new SolidColorBrush(Colors.White)};
            _category = category;
            _block = block;
            LeftClickDropDownItems = new ObservableCollection<Tuple<string, ICommand>>();
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => BlockCategory);
            OnPropertyChanged(() => Appointment);
        }


        public void Clear()
        {
            _block = null;
            _category = null;
            InsertCategory = null;
            InsertAppointment = null;
            EditAppointment = null;
            RescheduleAppointment = null;
            AppointmentSummary = null;
            PatientDemographics = null;
            LeftClickDropDownItems.Clear();
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => BlockCategory);
            OnPropertyChanged(() => Appointment);
        }

        public ObservableCollection<Tuple<string, ICommand>> LeftClickDropDownItems { get; private set; }

        public BlockCategoryViewModel BlockCategory
        {
            get { return _category; }
        }

        public BlockViewModel Block
        {
            get { return _block; }
        }

        public AppointmentViewModel Appointment
        {
            get { return _category != null ? _category.Appointment : null; }
        }

        public ICommand InsertCategory { get; set; }
        public ICommand InsertAppointment { get; set; }
        public ICommand EditAppointment { get; set; }
        public ICommand RescheduleAppointment { get; set; }
        public bool CanRescheduleAppointment { get; set; }

        public ICommand AppointmentSummary { get; set; }
        public ICommand PatientDemographics { get; set; }
    }

    #endregion

    #region OrphanedAppointment

    /// <summary>
    /// Wraps AppointmentViewModel
    /// </summary>
    public sealed class OrphanedAppointment : Appointment, IHasAppointmentViewModel,
                                              IHasLeftClickDropDown,
                                              ICanInsertCategory,
                                              ICanEditAppointment,
                                              IHasScheduleBlock,
                                              ICanViewPatientDemographics,
                                              ICanViewAppointmentSummmary,
                                              IRecyclableAppointment
    {
        private AppointmentViewModel _appointmentViewModel;
        private BlockViewModel _block;

        public OrphanedAppointment(AppointmentViewModel appointmentViewModel, BlockViewModel block)
        {
            Category = new Category {CategoryBrush = new SolidColorBrush(Colors.White)};

            Initialize(appointmentViewModel, block);
        }

        public void Initialize(AppointmentViewModel appointmentViewModel, BlockViewModel block)
        {
            Subject = "{0}, {1}, {2}, {3}, {4}, {5}, {6}".FormatWith(appointmentViewModel.Patient.LastName,
                                                                     appointmentViewModel.Patient.FirstName,
                                                                     appointmentViewModel.AppointmentType.Name,
                                                                     appointmentViewModel.Location.Name,
                                                                     appointmentViewModel.Patient.InsurerName,
                                                                     appointmentViewModel.Patient.CellPhone,
                                                                     appointmentViewModel.Comments);
            Start = DateTime.Today.Add(appointmentViewModel.StartDateTime.TimeOfDay);
            End = DateTime.Today.Add(appointmentViewModel.EndDateTime.TimeOfDay);
            
            // If the block's EndDateTime ends on the next day (at midnight), then adjust for the difference.
            if (block.EndDateTime.Date.Subtract(block.StartDateTime.Date) == TimeSpan.FromDays(1))
            {
                End = End.AddDays(1);
            }
            
            _appointmentViewModel = appointmentViewModel;
            _block = block;
            LeftClickDropDownItems = new ObservableCollection<Tuple<string, ICommand>>();
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => Appointment);
        }

        public void Clear()
        {
            _appointmentViewModel = null;
            _block = null;
            LeftClickDropDownItems.Clear();
            OnPropertyChanged(() => Block);
            OnPropertyChanged(() => Appointment);
        }

        public AppointmentViewModel Appointment
        {
            get { return _appointmentViewModel; }
        }

        public BlockViewModel Block
        {
            get { return _block; }
        }

        public ICommand InsertCategory { get; set; }
        public ICommand EditAppointment { get; set; }

        /// <summary>
        /// Gets a collection of Tuples representing the text and command of left-click items.
        /// </summary>
        public ObservableCollection<Tuple<string, ICommand>> LeftClickDropDownItems { get; private set; }

        public ICommand AppointmentSummary { get; set; }
        public ICommand PatientDemographics { get; set; }
    }

    #endregion

    #region CustomAppointmentComparer

    public class CustomAppointmentComparer : IEqualityComparer<Appointment>
    {
        public bool Equals(Appointment x, Appointment y)
        {
            return x.Start == y.Start && x.End == y.End;
        }

        public int GetHashCode(Appointment obj)
        {
            return obj.Start.GetHashCode() ^ obj.End.GetHashCode();
        }
    }

    #endregion

    #region AppointmentUtilities

    public static class AppointmentUtilities
    {
        public static void ClearAll(IEnumerable<Appointment> appointments)
        {
            appointments.OfType<IRecyclableAppointment>().ForEachWithSinglePropertyChangedNotification(a => a.Clear());
        }

        public static IEnumerable<Appointment> OverlappingWith(this IEnumerable<Appointment> source, IEnumerable<Appointment> other)
        {
            return source.Where(i => other.Any(j => i.Resources.SequenceEqual(j.Resources) && i.Start == j.Start && i.End == j.End));
        }

        public static IEnumerable<Appointment> ProcessBlocksGenerateNoBlockAppointments(IEnumerable<BlockViewModel> blocks, Resource resource, IList<Appointment> recyclableAppointments)
        {
            var result = new List<Appointment>();
            foreach (var block in blocks.Where(b => !b.Id.HasValue && !b.Categories.Any()).OrderBy(b => b.StartDateTime))
            {
                var recycled = recyclableAppointments.OfType<NoBlockAppointment>().FirstOrDefault();
                if (recycled != null)
                {
                    recyclableAppointments.Remove(recycled);
                    recycled.Initialize(block);
                    recycled.Resources.Clear();
                    recycled.Resources.Add(resource);
                    result.Add(recycled);
                }
                else
                {
                    var noBlockAppointment = new NoBlockAppointment(block);
                    noBlockAppointment.Resources.Add(resource);
                    result.Add(noBlockAppointment);
                }
            }
            return result;
        }

        public static IEnumerable<Appointment> ProcessBlocksGenerateAppointments(IEnumerable<BlockViewModel> blocks, Resource resource, IList<Appointment> recyclableAppointments)
        {
            var result = new List<Appointment>();
            foreach (var block in blocks.Where(b => b.Id.HasValue || b.Categories.Any()).OrderBy(b => b.StartDateTime))
            {
                if (block.Categories.Count == 0)
                {
                    var recycled = recyclableAppointments.OfType<OnlyBlockAppointment>().FirstOrDefault();
                    if (recycled != null)
                    {
                        recyclableAppointments.Remove(recycled);
                        recycled.Initialize(block);
                        recycled.Resources.RemoveAll();
                        recycled.Resources.Add(resource);
                        result.Add(recycled);
                    }
                    else
                    {
                        var onlyBlockAppointment = new OnlyBlockAppointment(block);
                        onlyBlockAppointment.Resources.Add(resource);
                        result.Add(onlyBlockAppointment);
                    }
                    continue;
                }

                foreach (var unavailable in block.Categories.OfType<UnavailableBlockCategoryViewModel>())
                {
                    var recycled = recyclableAppointments.OfType<UnavailableBlockAppointment>().FirstOrDefault();
                    if (recycled != null)
                    {
                        recyclableAppointments.Remove(recycled);
                        recycled.Initialize(unavailable, block);
                        recycled.Resources.RemoveAll();
                        recycled.Resources.Add(resource);
                        result.Add(recycled);
                    }
                    else
                    {
                        var unavailableBlockAppointment = new UnavailableBlockAppointment(unavailable, block);
                        unavailableBlockAppointment.Resources.Add(resource);
                        result.Add(unavailableBlockAppointment);
                    }
                }

                foreach (var categoryWithAppointment in block.Categories.Where(c => c.Appointment != null && !c.Appointment.IsOrphaned).OrderBy(c => c.Appointment.Id))
                {
                    var recycledBlockCategoryAppointment = recyclableAppointments.OfType<BlockCategoryAppointment>().FirstOrDefault();
                    if (recycledBlockCategoryAppointment != null)
                    {
                        recyclableAppointments.Remove(recycledBlockCategoryAppointment);
                        recycledBlockCategoryAppointment.Initialize(categoryWithAppointment, block);
                        recycledBlockCategoryAppointment.Resources.RemoveAll();
                        recycledBlockCategoryAppointment.Resources.Add(resource);
                        result.Add(recycledBlockCategoryAppointment);
                    }
                    else
                    {
                        var blockCategoryAppointment = new BlockCategoryAppointment(categoryWithAppointment, block);
                        blockCategoryAppointment.Resources.Add(resource);
                        result.Add(blockCategoryAppointment);
                    }
                }

                foreach (var category in block.Categories.Where(c => c.Appointment == null && !c.Is<UnavailableBlockCategoryViewModel>()).OrderBy(c => c.Id))
                {
                    var recycledBlockCategoryAppointment = recyclableAppointments.OfType<BlockCategoryAppointment>().FirstOrDefault();
                    if (recycledBlockCategoryAppointment != null)
                    {
                        recyclableAppointments.Remove(recycledBlockCategoryAppointment);
                        recycledBlockCategoryAppointment.Initialize(category, block);
                        recycledBlockCategoryAppointment.Resources.RemoveAll();
                        recycledBlockCategoryAppointment.Resources.Add(resource);
                        result.Add(recycledBlockCategoryAppointment);
                    }
                    else
                    {
                        var blockCategoryAppointment = new BlockCategoryAppointment(category, block);
                        blockCategoryAppointment.Resources.Add(resource);
                        result.Add(blockCategoryAppointment);
                    }
                }
            }
            return result;
        }
    }

    #endregion

}
