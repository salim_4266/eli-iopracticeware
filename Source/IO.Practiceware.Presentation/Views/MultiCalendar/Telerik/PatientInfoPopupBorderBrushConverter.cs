﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    public class PatientInfoPopupBorderBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values != DependencyProperty.UnsetValue && values.Length == 2)
            {
                var isReferralRequired = values[0] != DependencyProperty.UnsetValue && (bool)values[0];
                var hasPostOp = values[1] != DependencyProperty.UnsetValue && (bool)values[1];

                if (isReferralRequired) { return new SolidColorBrush(Colors.Red); }
                if (hasPostOp) { return new SolidColorBrush(Colors.Blue); }
            }

            return new SolidColorBrush(Colors.Transparent);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
