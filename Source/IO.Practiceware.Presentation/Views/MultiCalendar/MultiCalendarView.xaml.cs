﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Common.Configuration;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.AppointmentSearch;
using IO.Practiceware.Presentation.Views.CancelAppointment;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.MultiCalendar.Days;
using IO.Practiceware.Presentation.Views.MultiCalendar.Docs;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Presentation.Views.Recalls;
using IO.Practiceware.Presentation.Views.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.ScheduleAppointment;
using IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using AppointmentViewModel = IO.Practiceware.Presentation.ViewModels.MultiCalendar.AppointmentViewModel;
using PatientInfoTab = IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoTab;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    /// <summary>
    ///   Interaction logic for MultiCalendarView.xaml
    /// </summary>
    public partial class MultiCalendarView
    {
        public MultiCalendarView()
        {
            InitializeComponent();
        }

        
    }

    /// <summary>
    ///   The root view model (the view context) for the MultiCalendarView
    /// </summary>
    public class MultiCalendarViewContext : IViewContext, ICommonViewFeatureHandler
    {
        private readonly Func<AppointmentSearchViewManager> _createAppointmentSearchViewManager;
        private readonly Func<CalendarCommentViewModel> _createCalendarComment;
        private readonly Func<CancelAppointmentViewManager> _createCancelAppointmentViewManager;
        private readonly Func<ResourceViewModel, MultiCalendarDocsViewModelWrapper> _createDocViewModelWrapper;
        private readonly Func<PatientSearchViewManager> _createPatientSearchViewManager;
        private readonly Func<RecallsViewManager> _createRecallsViewManager;
        private readonly Func<PatientInfoViewManager> _createPatientInfoViewManager;
        private readonly Func<RescheduleAppointmentViewManager> _createRescheduleAppointmentViewManager;
        private readonly Func<ScheduleAppointmentViewManager> _createScheduleAppointmentViewManager;
        private readonly Func<ScheduleTemplateBuilderViewManager> _createScheduleTemplateBuilderViewManager;
        private readonly IImageService _imageService;
        private readonly IMultiCalendarViewService _multiCalendarViewService;
        private readonly IPostOperativePeriodService _postOperativePeriodService;
        private readonly DispatcherTimer _refreshTimer;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;
        private bool _isShowingDays;
        private bool _isBusy;

        /*
Changes from 01/08/2013 meeting and discussed with Matt 
...Tweaks for Doc-Calendar (Multi-Calendar)... 
If Doc-Calendar has the patient passed via the load arguments (set from the VB6 home screen) then that patient is used for each appointment created and each time the recall screen is launched. 
If patient is not passed via the load arguments then each time an appointment is to be created the patient search screen will be launched. Also the patient search screen will be launched each time the recall button is clicked.
        */

        public MultiCalendarViewContext(IMultiCalendarViewService multiCalendarViewService,
                                        IPostOperativePeriodService postOperativePeriodService,
                                        ISchedulingConfigurationViewService schedulingConfigurationViewService,
                                        IMultiCalendarChildViewContextFactory childViewContextFactory,
                                        ICommonViewFeatureExchange exchange,
                                        Func<ResourceViewModel, MultiCalendarDocsViewModelWrapper> createDocViewModelWrapper,
                                        IImageService imageService,
                                        Func<CalendarCommentViewModel> createCalendarComment,
                                        Func<ScheduleTemplateBuilderViewManager> createScheduleTemplateBuilderViewManager,
                                        Func<CancelAppointmentViewManager> createCancelAppointmentViewManager,
                                        Func<AppointmentSearchViewManager> createAppointmentSearchViewManager,
                                        Func<RescheduleAppointmentViewManager> createRescheduleAppointmentViewManager,
                                        Func<ScheduleAppointmentViewManager> createScheduleAppointmentViewManager,
                                        Func<PatientSearchViewManager> createPatientSearchViewManager,
                                        Func<RecallsViewManager> createRecallsViewManager,
                                        Func<PatientInfoViewManager> createPatientInfoViewManager)
        {
            _multiCalendarViewService = multiCalendarViewService;
            _postOperativePeriodService = postOperativePeriodService;
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _createDocViewModelWrapper = createDocViewModelWrapper;
            _createCalendarComment = createCalendarComment;
            _createScheduleTemplateBuilderViewManager = createScheduleTemplateBuilderViewManager;
            _createCancelAppointmentViewManager = createCancelAppointmentViewManager;
            _createAppointmentSearchViewManager = createAppointmentSearchViewManager;
            _createRescheduleAppointmentViewManager = createRescheduleAppointmentViewManager;
            _imageService = imageService;
            _createScheduleAppointmentViewManager = createScheduleAppointmentViewManager;
            PostOpTaskQueue = new SerializedTaskQueue();
            TaskQueue = new SerializedTaskQueue(64);
            TaskQueue.PropertyChanged += new PropertyChangedEventHandler(OnTaskQueuePropertyChanged).MakeWeak();

            DaysViewContext = childViewContextFactory.CreateMultiCalendarDaysViewContext(ShowRescheduleAppointment, TaskQueue);
            DocsViewContext = childViewContextFactory.CreateMultiCalendarDocsViewContext(ShowRescheduleAppointment, TaskQueue);
            _createPatientSearchViewManager = createPatientSearchViewManager;
            _createRecallsViewManager = createRecallsViewManager;
            _createPatientInfoViewManager = createPatientInfoViewManager;
            Init();

            Load = Command.Create(ExecuteLoad).Async(onBegin: c => IsBusy = true, onComplete: c => IsBusy = false);
            RefreshView = Command.Create(ExecuteRefreshView);

            _refreshTimer = new DispatcherTimer(DispatcherPriority.Background);

            InitializeView = Command.Create<Tuple<int, DateTime>>(ExecuteInitializeView);
            Close = Command.Create<IInteractionContext>(ExecuteClose);
            SwitchView = Command.Create<Tuple<int, DateTime>>(ExecuteSwitchView);
            TabSwitch = Command.Create(ExecuteTabSwitch);
            ApplyTemplate = Command.Create<Tuple<int, DateTime>>(ExecuteApplyTemplate);
            ShowRecall = Command.Create(ExecuteShowRecall);
            CreateOrEditTemplate = Command.Create(ExecuteCreateOrEditTemplate);
            AddNewComment = Command.Create<CalendarCommentViewModel>(ExecuteAddNewComment);
            EditComment = Command.Create<CalendarCommentViewModel>(ExecuteEditComment);
            DeleteComment = Command.Create<CalendarCommentViewModel>(ExecuteDeleteComment);
            RescheduleAppointment = Command.Create<object>(ExecuteRescheduleAppointment);
            InsertCategory = Command.Create<BlockViewModel>(ExecuteInsertCategory);
            ScheduleLocationConflicts = Command.Create<ScheduleLocationConflictArguments>(ExecuteScheduleLocationConflicts);
            LoadPatientPhoto = Command.Create<PatientViewModel>(ExecuteLoadPatientPhoto).Async();
            InsertAppointment = Command.Create<object>(ExecuteInsertAppointment);
            EditAppointment = Command.Create<IHasAppointmentViewModel>(ExecuteEditAppointment);
            ForceNewAppointment = Command.Create<IHasScheduleBlock>(ExecuteForceNewAppointment);

            LoadPatientPostOp = Command.Create<PatientViewModel>(ExecuteLoadPatientPostOp).Async();
            SaveComment = Command.Create<BlockViewModel>(ExecuteSaveComment);
            RemoveComment = Command.Create<BlockViewModel>(ExecuteRemoveComment);
            CancelAppointment = Command.Create<object>(ExecuteCancelAppointment);
            RescheduleAppointmentInContext = Command.Create<object>(ExecuteRescheduleAppointmentInContext);
            PatientDemographics = Command.Create<object>(ExecuteShowPatientDemographics);
            AppointmentSummary = Command.Create<object>(ExecuteShowAppointmentSummary);
            AppointmentIds = new HashSet<int>();

            // Report common capabilities
            ViewFeatureExchange = exchange;
            // Show schedule template builder for Settings button
            ViewFeatureExchange.OpenSettings = CreateOrEditTemplate.Execute;
        }

        /// <summary>
        ///   Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        ///   Command to launch the recall screen
        /// </summary>
        public ICommand ShowRecall { get; protected set; }

        /// <summary>
        ///   Command to close UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        ///   Command issued when the view is switched from MultiDay to MultiDoc and vice-versa.
        /// </summary>
        public ICommand SwitchView { get; protected set; }

        /// <summary>
        /// Command when tab is switched by end-user
        /// </summary>
        public ICommand TabSwitch { get; protected set; }

        /// <summary>
        ///   Command to open the Schedule Template Builder and apply a Schedule Template
        /// </summary>
        public ICommand ApplyTemplate { get; protected set; }

        /// <summary>
        ///   Command to open the Schedule Template Builder and create or edit a Schedule Template
        /// </summary>
        public ICommand CreateOrEditTemplate { get; protected set; }

        /// <summary>
        ///   Command to save new comment
        /// </summary>
        public ICommand AddNewComment { get; protected set; }

        /// <summary>
        ///   Command to edit an existing comment
        /// </summary>
        public ICommand EditComment { get; protected set; }

        /// <summary>
        ///   Command to delete an existing comment
        /// </summary>
        public ICommand DeleteComment { get; protected set; }

        /// <summary>
        ///   Command to open the patient appointment list window
        /// </summary>
        public ICommand AppointmentDetails { get; protected set; }

        /// <summary>
        ///   Command to open the patient demographics window
        /// </summary>
        public ICommand PatientInfo { get; protected set; }

        /// <summary>
        ///   Command to open the appointment search window
        /// </summary>
        public ICommand RescheduleAppointment { get; protected set; }

        /// <summary>
        ///   Command to load a patient's photo
        /// </summary>
        public ICommand LoadPatientPhoto { get; protected set; }

        /// <summary>
        ///   Command to load patient post operative info (if one exists).
        /// </summary>
        public ICommand LoadPatientPostOp { get; set; }

        /// <summary>
        ///   Command to load flag indicating whether or not the appointment needs a referral
        /// </summary>
        public ICommand LoadIsReferralRequired { get; set; }

        /// <summary>
        ///   Command to insert a category
        /// </summary>
        public ICommand InsertCategory { get; protected set; }

        /// <summary>
        ///   Command to insert an appointment
        /// </summary>
        public ICommand InsertAppointment { get; protected set; }

        /// <summary>
        ///   Command to edit an appointment
        /// </summary>
        public ICommand EditAppointment { get; protected set; }

        /// <summary>
        /// Command to display AppointmentSummary
        /// </summary>
        public ICommand AppointmentSummary { get; protected set; }

        /// <summary>
        ///   Command to open patient demographics
        /// </summary>
        public ICommand PatientDemographics { get; protected set; }

        /// <summary>
        ///   Command to force a new appointment
        /// </summary>
        public ICommand ForceNewAppointment { get; protected set; }

        /// <summary>
        ///   Command to schedule location conflicts
        /// </summary>
        public ICommand ScheduleLocationConflicts { get; protected set; }

        /// <summary>
        ///   Command to save a comment to an unavailable block
        /// </summary>
        public ICommand SaveComment { get; protected set; }

        /// <summary>
        ///   Command to remove a comment from an unavailable block
        /// </summary>
        public ICommand RemoveComment { get; protected set; }

        /// <summary>
        ///   Command to open the cancel appointment window
        /// </summary>
        public ICommand CancelAppointment { get; protected set; }

        /// <summary>
        ///   Refreshes the currently viewable calendar
        /// </summary>
        private ICommand RefreshView { get; set; }

        /// <summary>
        ///   Initializes the currently viewable calendar with new selected values
        /// </summary>
        private ICommand InitializeView { get; set; }

        /// <summary>
        ///   Command to open the reschedule view when an appointment is in context of the multi calendar window.  i.e. a user chooses to reschedule an appointment, 
        ///   the appointment search window pops up, then they hit the multi-calendar window.  That's when an appointment is in context.
        /// </summary>
        public ICommand RescheduleAppointmentInContext { get; set; }

        /// <summary>
        ///   Helper view context to separate out DOCS functionality
        /// </summary>
        public virtual MultiCalendarDocsViewContext DocsViewContext { get; set; }

        /// <summary>
        ///   Helper view context to separate out DAYS functionality
        /// </summary>
        public virtual MultiCalendarDaysViewContext DaysViewContext { get; set; }

        /// <summary>
        /// The value that indicates the 'zoom level' of both the days and docs calendar.
        /// </summary>
        [DispatcherThread]
        public virtual double MinTimeRulerExtent { get; set; }

        [DispatcherThread]
        public virtual TaskQueue TaskQueue { get; protected set; }

        public virtual TaskQueue PostOpTaskQueue { get; protected set; }


        [DependsOn("TaskQueue.IsBusy")]
        public bool IsBusy
        {
            get { return TaskQueue.IsBusy || _isBusy; }
            set { _isBusy = value; }
        }

        [DispatcherThread]
        public virtual bool IsShowingDocs
        {
            get { return !_isShowingDays; }
            set { _isShowingDays = !value; }
        }

        [DispatcherThread]
        public virtual bool IsShowingDays
        {
            get { return _isShowingDays; }
            set { _isShowingDays = value; }
        }

        [DispatcherThread]
        public virtual bool IsWindowVisible { get; set; }

        public MultiCalendarLoadArguments LoadArguments { get; set; }

        /// <summary>
        ///   Will contain a value if the user came to this window from the appointment search window which is in a "rescheduling" context.
        /// </summary>
        [DispatcherThread]
        public virtual int? AppointmentIdToBeRescheduled { get; set; }

        [DependsOn("AppointmentIdToBeRescheduled")]
        [DispatcherThread]
        public virtual bool CanRescheduleAppointmentInContext
        {
            get { return AppointmentIdToBeRescheduled.HasValue; }
        }

        /// <summary>
        ///   Value that gets set when in a "rescheduling" context of this view (see above AppointmentIdToBeRescheduled property). Set to true when successful.
        /// </summary>
        public bool? HasRescheduledAppointmentsSuccessfully { get; set; }

        public HashSet<int> AppointmentIds { get; set; }

        [DependsOn("CanRescheduleAppointmentInContext")]
        [DispatcherThread]
        public virtual Cursor CalendarCursor
        {
            get
            {
                if (CanRescheduleAppointmentInContext)
                {
                    return Cursors.Cross;
                }
                return Cursors.Arrow;
            }
        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void OnTaskQueuePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy" && _refreshTimer != null)
            {
                if (TaskQueue.IsBusy)
                {
                    _refreshTimer.Stop();
                }
                else
                {
                    _refreshTimer.Start();
                }
            }
        }

        private void Init()
        {
            var defaultSpanLocation = new ColoredViewModel();
            var spanSize = new TimeSpan(0, 30, 0);
            var areaStart = new TimeSpan(0, 0, 0);
            var areaEnd = new TimeSpan(23, 59, 59);

            DaysViewContext.DefaultSpanLocation = defaultSpanLocation;
            DaysViewContext.SpanSize = spanSize;
            DaysViewContext.AreaStart = areaStart;
            DaysViewContext.AreaEnd = areaEnd;

            DocsViewContext.DefaultSpanLocation = defaultSpanLocation;
            DocsViewContext.SpanSize = spanSize;
            DocsViewContext.AreaStart = areaStart;
            DocsViewContext.AreaEnd = areaEnd;
        }



        private void ExecuteLoad()
        {
            #region helper function canShowMultiDay

            Func<bool> canShowMultiDay = () =>
            {
                if (LoadArguments == null) return false;

                if (LoadArguments.LoadMode == MultiCalendarLoadMode.Days) return true;

                if (LoadArguments.ResourceIds.IsNotNullOrEmpty() && (LoadArguments.ResourceIds.Count() > 1 || (LoadArguments.ResourceIds.Count() == 1 && LoadArguments.Dates.Count() <= 1))) return false;

                return true;
            };

            #endregion

            #region helper function canShowCalendar

            Func<bool> canShowCalendar = () =>
            {
                if (LoadArguments != null && LoadArguments.PatientId.HasValue)
                {
                    return _multiCalendarViewService.IsPatientActive(LoadArguments.PatientId.Value);
                }
                return true;
            };

            #endregion

            if (!canShowCalendar())
            {
                this.Dispatcher().Invoke(() =>
                {
                    InteractionManager.Current.Alert("Cannot schedule appointments for inactive patients.");
                    InteractionContext.Complete(false);
                });
            }
            else
            {
                IsWindowVisible = true;
                ObservableCollection<ResourceViewModel> resources = _multiCalendarViewService.LoadResources();
                ScheduleConfigurationViewModel configuration = _schedulingConfigurationViewService.GetSchedulingConfiguration();

                System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() => SetupScheduleView(configuration)));
                DaysViewContext.ListOfResource = resources;
                SetDocsViewContextListOfResource(resources);

                var showMultiDay = canShowMultiDay();

                DocsViewContext.Load.Execute(LoadArguments);

                if (LoadArguments != null)
                {
                    AppointmentIdToBeRescheduled = LoadArguments.AppointmentId;
                    if (LoadArguments.PatientId.HasValue)
                    {
                        var defaultResourceId = _multiCalendarViewService.GetPatientDefaultResourceId(LoadArguments.PatientId.Value);
                        if (defaultResourceId.HasValue)
                        {
                            var resourceIds = LoadArguments.ResourceIds != null ? LoadArguments.ResourceIds.ToList() : new List<int>();
                            resourceIds.Add(defaultResourceId.Value);
                            LoadArguments.ResourceIds = resourceIds;
                        }
                    }
                }

                DaysViewContext.Load.Execute(LoadArguments);

                if (showMultiDay)
                {
                    IsShowingDays = true;
                    DaysViewContext.RefreshView();
                }
                else
                {
                    DocsViewContext.RefreshView();
                }

                _refreshTimer.Tick += OnRefreshTimerTick;
                _refreshTimer.Interval = TimeSpan.FromSeconds(20);

                InteractionContext.Completed += WeakDelegate.MakeWeak<EventHandler<EventArgs<bool?>>>(OnInteractionContextCompleted);
            }
        }

        private void OnInteractionContextCompleted(object sender, EventArgs<bool?> e)
        {
            _refreshTimer.Stop();
            _refreshTimer.Tick -= OnRefreshTimerTick;
            InteractionContext.Completed -= WeakDelegate.MakeWeak<EventHandler<EventArgs<bool?>>>(OnInteractionContextCompleted);
        }

        private void SetDocsViewContextListOfResource(IEnumerable<ResourceViewModel> resources)
        {
            var listOfResource = new List<MultiCalendarDocsViewModelWrapper>();
            var sharedGlobalComment = _createCalendarComment();
            foreach (ResourceViewModel resource in resources)
            {
                MultiCalendarDocsViewModelWrapper wrapper = _createDocViewModelWrapper(resource);
                wrapper.ResourceComment = _createCalendarComment();
                wrapper.GlobalComment = sharedGlobalComment;
                if (resources.Count() == 1) wrapper.IsSelected = true;
                listOfResource.Add(wrapper);
            }
            DocsViewContext.ListOfResource = listOfResource.ToExtendedObservableCollection();
        }

        private void OnRefreshTimerTick(object sender, EventArgs e)
        {
            _refreshTimer.Stop();
            RefreshView.Execute();
        }

        private void ExecuteSaveComment(BlockViewModel block)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() =>
                {
                    if (block.Id != null) _multiCalendarViewService.SetUnavailableBlockComment(block.Id.Value, block.Comment);
                });
        }


        private void ExecuteRemoveComment(BlockViewModel block)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() =>
                {
                    if (!block.Comment.IsNullOrEmpty() && block.Id.HasValue)
                    {
                        block.Comment = null;
                        _multiCalendarViewService.SetUnavailableBlockComment(block.Id.Value, block.Comment);
                    }
                });
        }

        private void ExecuteLoadPatientPhoto(PatientViewModel patient)
        {
            if (patient != null && patient.Photo == null && !patient.HasLoadedPhoto)
            {
                patient.Photo = _imageService.GetPatientPhoto(patient.Id);
                patient.HasLoadedPhoto = true;
            }
        }

        private void ExecuteLoadPatientPostOp(PatientViewModel patientViewModel)
        {
            if (patientViewModel.PatientPostOp == null)
            {
                PostOpTaskQueue.Enqueue(() => patientViewModel.PatientPostOp = _postOperativePeriodService.GetLatestPostOperativePeriodByPatientId(patientViewModel.Id));
            }
        }

        private void ExecuteShowAppointmentSummary(object instance)
        {
            AppointmentViewModel appointmentViewModel;
            if (instance is IHasAppointmentViewModel)
            {
                appointmentViewModel = ((IHasAppointmentViewModel)instance).Appointment;
            }
            else
            {
                appointmentViewModel = instance as AppointmentViewModel;
            }
            if (appointmentViewModel == null) return;

            var loadArguments = new PatientInfoLoadArguments();
            loadArguments.PatientId = appointmentViewModel.Patient.Id;
            loadArguments.TabToOpen = PatientInfoTab.Appointments;

            var patientInfoViewManager = _createPatientInfoViewManager();
            patientInfoViewManager.ShowPatientInfo(loadArguments);
        }

        private void ExecuteShowPatientDemographics(object instance)
        {
            AppointmentViewModel appointmentViewModel;
            if (instance is IHasAppointmentViewModel)
            {
                appointmentViewModel = ((IHasAppointmentViewModel)instance).Appointment;
            }
            else
            {
                appointmentViewModel = instance as AppointmentViewModel;
            }
            if (appointmentViewModel == null) return;

            var loadArguments = new PatientInfoLoadArguments();
            loadArguments.PatientId = appointmentViewModel.Patient.Id;
            loadArguments.TabToOpen = PatientInfoTab.Demographics;

            var patientInfoViewManager = _createPatientInfoViewManager();
            patientInfoViewManager.ShowPatientInfo(loadArguments);
        }

        private void ExecuteRescheduleAppointmentInContext(object item)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    if (!AppointmentIdToBeRescheduled.HasValue)
                    {
                        return;
                    }

                    BlockViewModel blockViewModel;
                    BlockCategoryViewModel blockCategoryViewModel = null;
                    var blockCategoryAppointment = item as BlockCategoryAppointment;
                    if (blockCategoryAppointment == null)
                    {
                        var onlyBlockAppointment = item as OnlyBlockAppointment;
                        if (onlyBlockAppointment == null)
                        {
                            return;
                        }

                        blockViewModel = onlyBlockAppointment.Block;
                    }
                    else
                    {
                        blockViewModel = blockCategoryAppointment.Block;
                        blockCategoryViewModel = blockCategoryAppointment.BlockCategory;
                    }

                    HasRescheduledAppointmentsSuccessfully = ShowRescheduleAppointment(AppointmentIdToBeRescheduled.Value, blockViewModel, blockCategoryViewModel);

                    if (HasRescheduledAppointmentsSuccessfully.Value)
                    {
                        InteractionContext.Complete(false);
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        /// <summary>
        /// Helper method to show the reschedule appointment view.
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <param name="block"></param>
        /// <param name="category"></param>
        /// <returns>Returns a flag that indicates whether the appointment was successfully rescheduled.</returns>
        private bool ShowRescheduleAppointment(int appointmentId, BlockViewModel block, BlockCategoryViewModel category = null)
        {
            var loadArgs = new RescheduleAppointmentLoadArguments
                {
                    AppointmentIds = new List<int> { appointmentId },
                    SelectedBlock = new RescheduleAppointmentViewModel
                        {
                            StartDateTime = block.StartDateTime,
                            LocationId = block.Location.Id,
                            ResourceId = block.Resource.Id,
                            ScheduleBlockCategoryId = category != null ? category.Id : new Guid?(),
                        }
                };

            RescheduleAppointmentViewManager rescheduleAppointmentViewManager = _createRescheduleAppointmentViewManager();
            ReschedulAppointmentReturnArguments returnArgs = rescheduleAppointmentViewManager.ShowRescheduleAppointment(loadArgs);

            if (returnArgs.AppointmentsRescheduledSuccessfully) AppointmentIds.Add(appointmentId);

            return returnArgs.AppointmentsRescheduledSuccessfully;
        }

        private void ExecuteEditAppointment(IHasAppointmentViewModel instance)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        var scheduleAppointmentLoadArguments = new ScheduleAppointmentLoadArguments();

                        if (LoadArguments != null)
                        {
                            scheduleAppointmentLoadArguments.ReasonForVisit = LoadArguments.ReasonForVisit;
                            scheduleAppointmentLoadArguments.Comment = LoadArguments.Comment;
                        }

                        AppointmentViewModel appointment = instance.Appointment;
                        scheduleAppointmentLoadArguments.PatientId = appointment.Patient.Id;
                        scheduleAppointmentLoadArguments.PatientDisplayName = appointment.Patient.DisplayName;

                        scheduleAppointmentLoadArguments.AppointmentId = appointment.Id;
                        scheduleAppointmentLoadArguments.AppointmentTypeId = appointment.AppointmentType.Id;
                        scheduleAppointmentLoadArguments.AppointmentCategoryId = appointment.AppointmentCategoryId;

                        ScheduleAppointmentViewManager scheduleAppointmentViewManager = _createScheduleAppointmentViewManager();
                        var returnArgs = scheduleAppointmentViewManager.ShowScheduleAppointment(scheduleAppointmentLoadArguments);

                        if (returnArgs.AppointmentId.HasValue) AppointmentIds.Add(returnArgs.AppointmentId.Value);
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteForceNewAppointment(IHasScheduleBlock instance)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        BlockViewModel block = instance.Block;
                        ExecuteInsertAppointment(block);
                    }
                }));
        }

        private void ExecuteInsertAppointment(object instance)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        var blockViewModel = instance as BlockViewModel;
                        if (blockViewModel != null && !PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission()) return;

                        bool loadArgsHasPatient = LoadArguments != null && LoadArguments.PatientId.HasValue;

                        NamedViewModel patient = loadArgsHasPatient ? new NamedViewModel { Id = LoadArguments.PatientId.Value, Name = LoadArguments.PatientDisplayName }
                                                     : ShowPatientSearch();

                        if (patient == null)
                        {
                            return;
                        }

                        var scheduleAppointmentLoadArguments = new ScheduleAppointmentLoadArguments();
                        scheduleAppointmentLoadArguments.PatientId = patient.Id;
                        scheduleAppointmentLoadArguments.PatientDisplayName = patient.Name;
                        if (LoadArguments != null)
                        {
                            scheduleAppointmentLoadArguments.ReasonForVisit = LoadArguments.ReasonForVisit;
                            scheduleAppointmentLoadArguments.Comment = LoadArguments.Comment;
                            scheduleAppointmentLoadArguments.OrderAppointmentDate = LoadArguments.OrderAppointmentDate;
                        }

                        if (blockViewModel != null)
                        {
                            scheduleAppointmentLoadArguments.ScheduleBlockId = blockViewModel.Id;
                        }
                        else if (instance is BlockCategoryViewModel)
                        {
                            var blockCategory = instance as BlockCategoryViewModel;
                            scheduleAppointmentLoadArguments.ScheduleBlockId = blockCategory.BlockId;
                            scheduleAppointmentLoadArguments.ScheduleBlockAppointmentCategoryId = blockCategory.Id;
                            scheduleAppointmentLoadArguments.AppointmentCategoryId = blockCategory.Category.Id;
                        }
                        
                        if (scheduleAppointmentLoadArguments.ScheduleBlockAppointmentCategoryId.HasValue && scheduleAppointmentLoadArguments.ScheduleBlockAppointmentCategoryId == Guid.Empty)
                        {
                            return;
                        }

                        ScheduleAppointmentViewManager scheduleAppointmentViewManager = _createScheduleAppointmentViewManager();
                        var returnArgs = scheduleAppointmentViewManager.ShowScheduleAppointment(scheduleAppointmentLoadArguments);

                        if (returnArgs.AppointmentId.HasValue) AppointmentIds.Add(returnArgs.AppointmentId.Value);
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteRefreshView()
        {
            if (IsShowingDays)
            {
                DaysViewContext.RefreshView();
            }
            else
            {
                DocsViewContext.RefreshView();
            }
        }

        private void ExecuteInitializeView(Tuple<int, DateTime> resourceDate)
        {
            if (IsShowingDays)
            {
                DaysViewContext.InitializeView(resourceDate.Item2, resourceDate.Item1);
            }
            else
            {
                DocsViewContext.InitializeView(resourceDate.Item2, resourceDate.Item1);
            }
        }

        private void ExecuteSwitchView(Tuple<int, DateTime> resourceDate)
        {
            if (IsShowingDocs)
            {
                IsShowingDays = true;
            }
            else
            {
                IsShowingDocs = true;
            }
            InitializeView.Execute(resourceDate);
        }

        private void ExecuteTabSwitch()
        {
            if (IsShowingDocs)
            {
                var resource = DocsViewContext.ListOfResource.FirstOrDefault(r => r.IsSelected);
                if (!DaysViewContext.Dates.Any(d => d.Blocks.Any()) || resource == null || !DocsViewContext.SelectedDateFilter.HasValue)
                {
                    IsShowingDays = true;
                    return;
                }
                SwitchView.Execute(new Tuple<int, DateTime>(resource.Resource.Id, DocsViewContext.SelectedDateFilter.Value));
            }
            else
            {
                var resource = DaysViewContext.SelectedResource;
                if (!DocsViewContext.ListOfResource.Any(r => r.Blocks.Any()) || resource == null || DaysViewContext.GeneratedDates == null || DaysViewContext.GeneratedDates.Count == 0)
                {
                    IsShowingDocs = true;
                    return;
                }
                SwitchView.Execute(new Tuple<int, DateTime>(resource.Id, DaysViewContext.GeneratedDates.OrderBy(d => d.Date).First()));
            }
        }

        private void ExecuteRescheduleAppointment(object instance)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        AppointmentViewModel appointmentViewModel;
                        if (instance is IHasAppointmentViewModel)
                        {
                            appointmentViewModel = ((IHasAppointmentViewModel)instance).Appointment;
                        }
                        else
                        {
                            appointmentViewModel = instance as AppointmentViewModel;
                        }
                        if (appointmentViewModel == null) return;

                        bool canReschedule = ((EncounterStatus)appointmentViewModel.EncounterStatusId).EnsureCanReschedule();
                        if (!canReschedule) return;

                        var loadArgs = new AppointmentSearchLoadArguments
                                           {
                                               AppointmentTypeId = appointmentViewModel.AppointmentType.Id,
                                               LocationId = appointmentViewModel.Location.Id,
                                               PatientId = appointmentViewModel.Patient.Id,
                                               ResourceId = appointmentViewModel.Resource.Id,
                                               StartDate = appointmentViewModel.StartDateTime,
                                               AppointmentId = appointmentViewModel.Id
                                           };

                        AppointmentSearchViewManager appointmentSearchViewManager = _createAppointmentSearchViewManager();
                        var returnArgs = appointmentSearchViewManager.ShowAppointmentSearch(loadArgs);

                        foreach (var apptId in returnArgs.AppointmentIds)
                        {
                            AppointmentIds.Add(apptId);
                        }
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteApplyTemplate(Tuple<int, DateTime> resourceDate)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        var loadArgs = new ScheduleTemplateBuilderLoadArguments
                            {
                                SelectedDate = resourceDate.Item2,
                                SelectedMode = ScheduleTemplateBuilderMode.ApplyTemplate,
                                SelectedResourceId = resourceDate.Item1
                            };

                        ScheduleTemplateBuilderViewManager scheduleTemplateBuilderManager = _createScheduleTemplateBuilderViewManager();
                        scheduleTemplateBuilderManager.ShowScheduleTemplateBuilder(loadArgs);
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteCreateOrEditTemplate()
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        ScheduleTemplateBuilderViewManager scheduleTemplateBuilderManager = _createScheduleTemplateBuilderViewManager();
                        scheduleTemplateBuilderManager.ShowScheduleTemplateBuilder();
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteAddNewComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => _multiCalendarViewService.SaveCalendarComment(calendarCommentViewModel));
        }

        private void ExecuteEditComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => _multiCalendarViewService.EditCalendarComment(calendarCommentViewModel));
        }

        private void ExecuteDeleteComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => _multiCalendarViewService.DeleteCalendarComment(calendarCommentViewModel));
        }

        private void ExecuteScheduleLocationConflicts(ScheduleLocationConflictArguments arguments)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        var view = new MultiCalendarLocationConflictView();
                        var context = (MultiCalendarLocationConflictViewContext)view.DataContext;
                        context.NewBlockLocation = arguments.SelectedLocation;
                        context.ConflictedBlocks = arguments.ConflictedBlocks;
                        context.OrphanedAppointments = arguments.OrphanedAppointments;
                        context.ShowViewAppointmentSearch = RescheduleAppointment;
                        context.ShowViewCancelAppointment = CancelAppointment;

                        ShowView(view, WindowStartupLocation.CenterScreen);
                    }
                }));
        }

        private void ExecuteCancelAppointment(object appointments)
        {
            #region parseAppointmentsParameter

            // NOTE: appointments can be a list or a single item
            Func<IList<AppointmentViewModel>> parseAppointmentsParameter = () =>
            {
                var appointmentsCollection = appointments as IEnumerable<AppointmentViewModel>;
                if (appointmentsCollection != null) return appointmentsCollection.ToList();

                var instance = appointments as IHasAppointmentViewModel;
                if (instance == null) throw new ArgumentException("Parameter \"appointments\" is not an IList<AppointmentViewModel> and is not IHasAppointmentViewModel.");

                return instance.Appointment != null ? new List<AppointmentViewModel> { instance.Appointment }
                                                    : new List<AppointmentViewModel>();
            };

            #endregion

            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        var appointmentViewModels = parseAppointmentsParameter();
                        // There will be no appointments if we choose to delete a block category.
                        // This is necessary because our block category view models may optionally hold appointments
                        if (appointmentViewModels.IsNullOrEmpty()) return;

                        var loadArguments = new CancelAppointmentLoadArguments();
                        loadArguments.ListOfAppointmentId = appointmentViewModels.Select(a => a.Id).ToList();
                        CancelAppointmentViewManager cancelAppointmentViewManager = _createCancelAppointmentViewManager();

                        CancelAppointmentReturnArguments returnArguments = cancelAppointmentViewManager.ShowCancelAppointment(loadArguments, true, WindowStartupLocation.CenterOwner);
                        if (returnArguments == null || returnArguments.ListOfAppointmentId == null || returnArguments.ListOfAppointmentId.Count == 0) return;

                        foreach (int appointmentId in returnArguments.ListOfAppointmentId)
                        {
                            AppointmentViewModel appointment = appointmentViewModels.SingleOrDefault(x => x.Id == appointmentId);
                            if (appointment != null)
                            {
                                appointment.IsCancelled = true;
                                AppointmentIds.Remove(appointment.Id);
                            }
                        }
                    }
                }));

            if (RefreshView.CanExecute(null)) RefreshView.Execute();
        }

        private void ExecuteInsertCategory(BlockViewModel block)
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        bool canDisplayInsertCategoryForDocsCalendar = IsShowingDocs && DocsViewContext.ListOfResource != null && DocsViewContext.ListOfResource.Any(p => p.IsSelected);
                        bool canDisplayInsertCategoryForDaysCalendar = IsShowingDays && DaysViewContext.Dates != null && DaysViewContext.Dates.Count > 0;
                        if (block == null || (!canDisplayInsertCategoryForDaysCalendar && !canDisplayInsertCategoryForDocsCalendar))
                        {
                            return;
                        }

                        var view = new MultiCalendarInsertCategoryView();
                        var context = (MultiCalendarInsertCategoryViewContext)view.DataContext;
                        context.SelectedBlocks = new ExtendedObservableCollection<BlockViewModel> { block };
                        context.ScheduleLocationConflicts = ScheduleLocationConflicts;
                        ShowView(view, WindowStartupLocation.MousePosition, true);
                    }
                }));
        }

        private void ExecuteShowRecall()
        {
            TaskQueue.CancelAll();
            TaskQueue.Enqueue(() => this.Dispatcher().Invoke(() =>
                {
                    {
                        bool loadArgsHasPatient = LoadArguments != null && LoadArguments.PatientId.HasValue;
                        NamedViewModel patient = loadArgsHasPatient ? new NamedViewModel { Id = LoadArguments.PatientId.Value, Name = LoadArguments.PatientDisplayName }
                                                     : ShowPatientSearch();
                        if (patient == null) return;

                        var recallLoadArguments = new RecallLoadArguments();
                        recallLoadArguments.PatientId = patient.Id;
                        RecallsViewManager recallsViewManager = _createRecallsViewManager();
                        recallsViewManager.ShowRecalls(recallLoadArguments);
                    }
                }));
        }

        private static void ExecuteClose(IInteractionContext context)
        {
            context.Complete(false);
        }

        public static void ShowView(object view, WindowStartupLocation startupLocation, bool noWindowFrame = false)
        {
            var arguments = new WindowInteractionArguments
            {
                Content = view,
                IsModal = true,
                WindowStartupLocation = startupLocation,
                WindowFrameStyle = WindowFrameStyle.None,
            };
            InteractionManager.Current.ShowModal(arguments);
        }

        private void SetupScheduleView(ScheduleConfigurationViewModel configuration)
        {
            
            ColoredViewModel defaultSpanLocation = configuration.DefaultLocation;
            TimeSpan spanSize = configuration.SpanSize;

            DaysViewContext.DefaultSpanLocation = defaultSpanLocation;

            // we are assigning SpanSize property here twice to raise it's property change event, 
            // that is required to set proper zoom level and to prevent scheduler from crashing
            if (DaysViewContext.SpanSize == spanSize)
            {
                DaysViewContext.SpanSize = new TimeSpan(0, 1, 0);
            }
            DaysViewContext.SpanSize = spanSize;

            // we are assigning SpanSize property here twice to raise it's property change event, 
            // that is required to set proper zoom level and to prevent scheduler from crashing
            DocsViewContext.DefaultSpanLocation = defaultSpanLocation;
            if (DocsViewContext.SpanSize == spanSize)
            {
                DocsViewContext.SpanSize = new TimeSpan(0, 1, 0);
            }
            DocsViewContext.SpanSize = spanSize;
        }

        /// <summary>
        ///   Helper function used by "Show Recall" and "Show Insert/Edit Appointment"
        /// </summary>
        /// <returns> </returns>
        private NamedViewModel ShowPatientSearch()
        {
            PatientSearchViewManager patientSearchViewManager = _createPatientSearchViewManager();
            var returnArguments = patientSearchViewManager.ShowPatientSearchView(new PatientSearchLoadArguments { CanCreatePatient = true });
            var selectedPatientReturnArguments = returnArguments as PatientSearchPatientSelectedReturnArguments;
            if (selectedPatientReturnArguments != null && selectedPatientReturnArguments.PatientId.HasValue)
            {
                var foundPatient = new NamedViewModel
                {
                    Id = selectedPatientReturnArguments.PatientId.Value,
                    Name = selectedPatientReturnArguments.PatientDisplayName
                };
                return foundPatient;
            }
            if (returnArguments is PatientSearchCreatePatientReturnArguments)
            {
                var patientInfoReturnArguments = _createPatientInfoViewManager().ShowPatientInfo(new PatientInfoLoadArguments
                    {
                        PatientId = null,
                        TabToOpen = PatientInfoTab.Demographics
                    });

                if (patientInfoReturnArguments.PatientId.HasValue)
                {
                    return new NamedViewModel
                    {
                        Id = patientInfoReturnArguments.PatientId.Value,
                        Name = string.Empty
                    };
                }
            }
            return null;
        }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; set; }
    }

    public interface IChildMultiCalendarViewContext
    {
        void InitializeView(DateTime date, int resourceId);
        void RefreshView();
    }

    /// <summary>
    ///   Factory interface to create the DocsViewContext and DaysViewContext.
    ///   Provides us with a cleaner interface to create/proxy the children view contexts.
    /// </summary>
    [Factory]
    public interface IMultiCalendarChildViewContextFactory
    {
        MultiCalendarDocsViewContext CreateMultiCalendarDocsViewContext(Func<int, BlockViewModel, BlockCategoryViewModel, bool> showRescheduleAppointment, TaskQueue taskQueue);

        MultiCalendarDaysViewContext CreateMultiCalendarDaysViewContext(Func<int, BlockViewModel, BlockCategoryViewModel, bool> showRescheduleAppointment, TaskQueue taskQueue);
    }

    
}