﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Docs
{
    public class MultiCalendarDocsViewModelWrapper : IViewModel
    {
        private ObservableCollection<BlockViewModel> _blocks;

        public MultiCalendarDocsViewModelWrapper()
        {
            Blocks = new ExtendedObservableCollection<BlockViewModel>();
            OrphanedAppointments = new ExtendedObservableCollection<AppointmentViewModel>();
        }

        public MultiCalendarDocsViewModelWrapper(ResourceViewModel resource) : this()
        {
            Resource = resource;
        }

        private void OnBlocksPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var args = e as CascadingPropertyChangedEventArgs;
            if (args == null) return;

            if (args.PropertyName == "Item.Categories.Item.Appointment.IsOrphaned")
            {
                var appointment = args.OriginalSender as AppointmentViewModel;
                if (appointment != null && appointment.IsOrphaned)
                {
                    OrphanedAppointments.Add(appointment);
                }
            }
            else if (args.PropertyName == "Item.Categories.Count")
            {
                var blocks = args.OriginalSender as IEnumerable<BlockCategoryViewModel>;
                if (blocks != null)
                {
                    blocks.Select(b => b.Appointment).WhereNotDefault().Where(a => !a.IsOrphaned).ToList().ForEach(a => OrphanedAppointments.Remove(a));
                }
            }
        }

        public override bool Equals(object other)
        {
            return Equals(other as MultiCalendarDocsViewModelWrapper);
        }

        public bool Equals(MultiCalendarDocsViewModelWrapper other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Blocks.SequenceEqual(other.Blocks)
                   && GlobalComment == other.GlobalComment
                   && Resource == other.Resource
                   && ResourceComment == other.ResourceComment
                   && IsSelected == other.IsSelected
                   && OrphanedAppointments.SequenceEqual(other.OrphanedAppointments);
        }

        public override int GetHashCode()
        {
            return Objects.GetHashCodeWithNullCheck(IsSelected)
                   ^ Objects.GetHashCodeWithNullCheck(Blocks)
                   ^ Objects.GetHashCodeWithNullCheck(GlobalComment)
                   ^ Objects.GetHashCodeWithNullCheck(Resource)
                   ^ Objects.GetHashCodeWithNullCheck(ResourceComment)
                   ^ Objects.GetHashCodeWithNullCheck(OrphanedAppointments);
        }

        [DispatcherThread]
        public virtual ResourceViewModel Resource { get; set; }

        [DependsOn("Blocks")]
        public virtual ObservableCollection<LocationHoursViewModel> LocationHours
        {
            get { return Blocks.GetLocationHours(); }
        }

        [DependsOn("Blocks")]
        public virtual int CategoriesUsed
        {
            get
            {
                int categoriesUsed = Blocks.GetCategoriesUsed();
                if (OrphanedAppointments == null) return categoriesUsed;

                categoriesUsed += OrphanedAppointments.Count;
                return categoriesUsed;
            }
        }

        [DependsOn("Blocks")]
        public virtual int TotalCategories
        {
            get { return Blocks.GetTotalCategories(); }
        }

        [DispatcherThread]
        public virtual ObservableCollection<BlockViewModel> Blocks
        {
            get { return _blocks; }
            set
            {
                if (_blocks != null)
                {
                    var collection = _blocks as INotifyPropertyChanged;
                    if (collection != null)
                    {
                        collection.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnBlocksPropertyChanged);
                    }
                }
                _blocks = value;

                if (_blocks != null)
                {
                    var collection = _blocks as INotifyPropertyChanged;
                    if (collection != null)
                    {
                        collection.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnBlocksPropertyChanged);
                    }
                }
            }
        }

        [DispatcherThread]
        public virtual bool IsSelected { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentViewModel> OrphanedAppointments { get; set; }

        [DispatcherThread]
        public virtual CalendarCommentViewModel ResourceComment { get; set; }

        [DispatcherThread]
        public virtual CalendarCommentViewModel GlobalComment { get; set; }
    }
}
