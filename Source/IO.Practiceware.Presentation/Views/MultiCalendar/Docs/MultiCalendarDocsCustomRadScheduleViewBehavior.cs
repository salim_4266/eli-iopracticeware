﻿using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Docs
{
    public class MultiCalendarDocsCustomRadScheduleViewBehavior : MultiCalendarCustomRadScheduleViewBehavior
    {
        #region DependencyProperty: Resources

        public ObservableCollection<MultiCalendarDocsViewModelWrapper> Resources
        {
            get { return GetValue(ResourcesProperty) as ObservableCollection<MultiCalendarDocsViewModelWrapper>; }
            set { SetValue(ResourcesProperty, value); }
        }

        public static readonly DependencyProperty ResourcesProperty = DependencyProperty.Register("Resources", typeof(ObservableCollection<MultiCalendarDocsViewModelWrapper>), typeof(MultiCalendarDocsCustomRadScheduleViewBehavior), new PropertyMetadata(new ExtendedObservableCollection<MultiCalendarDocsViewModelWrapper>(), OnPropertyResourcesChanged));
        private PropertyChangedEventArgs _lastPropertyChangedEventArgs;

        private static void OnPropertyResourcesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (MultiCalendarDocsCustomRadScheduleViewBehavior)d;

            var oldCollectionChanged = e.OldValue as INotifyPropertyChanged;
            if (oldCollectionChanged != null)
            {
                oldCollectionChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddResourcesFromProperty);
            }

            var newCollectionChanged = e.NewValue as INotifyPropertyChanged;
            if (newCollectionChanged != null)
            {
                newCollectionChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddResourcesFromProperty);
            }

            if (behavior.AssociatedObject == null || !behavior.SpanSize.HasValue) return;
            behavior.RefreshGui();
        }

        private void AddResourcesFromProperty(object sender, EventArgs e)
        {
            var cascadingPropertyChangeEventArgs = e.As<CascadingPropertyChangedEventArgs>();
            if (cascadingPropertyChangeEventArgs.IfNotNull(a => a.OriginalPropertyChangedEventArgs == _lastPropertyChangedEventArgs)) return;

            cascadingPropertyChangeEventArgs.IfNotNull(a => _lastPropertyChangedEventArgs = a.OriginalPropertyChangedEventArgs);

            if (AssociatedObject == null || !SpanSize.HasValue) return;
            
            if (cascadingPropertyChangeEventArgs.PropertyName.Contains("Item.Blocks") ||
                cascadingPropertyChangeEventArgs.PropertyName == "Item.IsSelected" ||
                cascadingPropertyChangeEventArgs.PropertyName == "Item.OrphanedAppointments")
            {
                RefreshGui();
            }
        }

        #endregion

        #region HelpersDependencyProperty

        public override void RefreshGui()
        {
            var resourceNames = ListOfTelerikAppointment.All(a => a is NoBlockAppointment) ? new string[0] : ListOfTelerikAppointment.SelectMany(a => a.Resources).Select(r => r.ResourceName).OrderBy(i => i).Distinct().ToArray();

            RecycledAppointments.AddRange(ListOfTelerikAppointment);

            ListOfTelerikAppointment.Clear();

            var appointments = new List<Appointment>();
            foreach (var docsViewModelWrapper in Resources.Where(p => p.IsSelected))
            {
                var telerikResource = new Resource
                                          {
                                              ResourceType = "Resource",
                                              DisplayName = docsViewModelWrapper.Resource.DisplayName,
                                              ResourceName = docsViewModelWrapper.Resource.Id.ToString()
                                          };

                appointments.AddRange(GenerateTelerikAppointments(docsViewModelWrapper, telerikResource, RecycledAppointments));
            }

            RecycledAppointments.OfType<IRecyclableAppointment>().ForEachWithSinglePropertyChangedNotification(a => a.Clear());

            ListOfTelerikAppointment.AddRangeWithSinglePropertyChangedNotification(appointments);

            if (!ListOfTelerikAppointment.SelectMany(a => a.Resources).Select(r => r.ResourceName).OrderBy(i => i).Distinct().SequenceEqual(resourceNames))
            {
                ScrollToFirstBlock();
            }

            AssociatedObject.UpdateLayout();
        }

        private static IEnumerable<Appointment> GenerateTelerikOrphanedAppointments(MultiCalendarDocsViewModelWrapper resource, List<BlockViewModel> blocks, Resource telerikResource, IList<Appointment> recyclableAppointments)
        {
            var telerikAppointments = new List<Appointment>();

            foreach (var orphan in resource.OrphanedAppointments.OrderBy(a => a.StartDateTime))
            {
                var block = blocks.FirstOrDefault(b => b.StartDateTime.TimeOfDay == orphan.StartDateTime.TimeOfDay && b.EndDateTime.TimeOfDay == orphan.EndDateTime.TimeOfDay);

                var recycled = recyclableAppointments.OfType<OrphanedAppointment>().FirstOrDefault();
                if (recycled != null)
                {
                    recyclableAppointments.Remove(recycled);
                    recycled.Initialize(orphan, block);
                    recycled.Resources.RemoveAll();
                    recycled.Resources.Add(telerikResource);
                    telerikAppointments.Add(recycled);
                }
                else
                {
                    var orphanedAppointment = new OrphanedAppointment(orphan, block);
                    orphanedAppointment.Resources.Add(telerikResource);
                    telerikAppointments.Add(orphanedAppointment);
                }
            }

            return telerikAppointments;
        }

        private IEnumerable<Appointment> GenerateTelerikAppointments(MultiCalendarDocsViewModelWrapper resource, Resource telerikResource, IList<Appointment> recyclableAppointments)
        {
            var appointments = new List<Appointment>();
            if (!SpanSize.HasValue) return appointments;

            var blocks = resource.Blocks.ToList();
            var processBlockAppointments = AppointmentUtilities.ProcessBlocksGenerateAppointments(blocks, telerikResource, recyclableAppointments).OrderBy(a => a.Start);
            var orphanedAppointments = GenerateTelerikOrphanedAppointments(resource, blocks, telerikResource, recyclableAppointments).OrderBy(a => a.Start).ToList();
            IEnumerable<Appointment> noBlockAppointments = AppointmentUtilities.ProcessBlocksGenerateNoBlockAppointments(blocks, telerikResource, recyclableAppointments);

            List<Appointment> result = processBlockAppointments.Concat(orphanedAppointments).ToList();
            result.AddRange(noBlockAppointments);

            appointments.AddRange(result.Except(result.OfType<OnlyBlockAppointment>().OverlappingWith(result.OfType<OrphanedAppointment>())));
            // Order by start time, then by appointment id if it exists
            return appointments.OrderBy(a => a.Start).ThenBy(a => a.As<IHasAppointmentViewModel>().IfNotNull(i => i.Appointment).IfNotNull(i => i.Id));
        }

        #endregion

        public override IEnumerable<BlockViewModel> GetBlocks()
        {
            var result = new List<BlockViewModel>();
            if (Resources != null)
            {
                result.AddRange(Resources.Where(p => p.IsSelected).SelectMany(p => p.Blocks));
            }
            return result;
        }
    }
}
