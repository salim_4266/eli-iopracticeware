using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.MultiCalendar.Docs
{
    public class MultiCalendarDocsViewContext : IViewContext, IChildMultiCalendarViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        #region IChildMultiCalendarViewContext Members

        public void InitializeView(DateTime date, int resourceId)
        {
            SelectedDateFilter = date;
            foreach (var docsViewModelWrapper in ListOfResource)
            {
                docsViewModelWrapper.IsSelected = (docsViewModelWrapper.Resource.Id == resourceId);
            }
            RefreshView();
        }

        public void RefreshView()
        {
            lock (ListOfResource)
            {
                var resourceIds = ListOfResource.Where(r => r.IsSelected).Select(r => r.Resource.Id).ToArray();
                RefreshView(resourceIds);
            }
        }

        private void RefreshView(int[] resourceIds)
        {
            if (SelectedDateFilter != null && SelectedDateFilter.Value != null && resourceIds.Any())
            {

                _taskQueue.Enqueue(() => ListOfResource.Any(j => j.IsSelected) ? ExecuteQueryBlocks(resourceIds) : null,
                    queryResult =>
                    {
                        if (ListOfResource.Any(j => j.IsSelected))
                        {
                            ExecutePopulateBlocks(queryResult);
                        }
                    });
            }
        }

        #endregion

        private readonly IMultiCalendarViewService _multiCalendarViewService;
        private readonly Func<BlockViewModel> _createBlockViewModel;
        private readonly Func<int, BlockViewModel, BlockCategoryViewModel, bool> _showRescheduleAppointment;
        private DateTime? _selectedDateFilter;
        private readonly SerializedTaskQueue _taskQueue;

        public MultiCalendarDocsViewContext(IMultiCalendarViewService multiCalendarViewService,
                                            Func<BlockViewModel> createBlockViewModel,
                                            Func<int, BlockViewModel, BlockCategoryViewModel, bool> showRescheduleAppointment,
                                            SerializedTaskQueue taskQueue)
        {
            _multiCalendarViewService = multiCalendarViewService;
            _createBlockViewModel = createBlockViewModel;
            _showRescheduleAppointment = showRescheduleAppointment;
            _taskQueue = taskQueue;
            ListOfResource = new ObservableCollection<MultiCalendarDocsViewModelWrapper>();
            Load = Command.Create<MultiCalendarLoadArguments>(ExecuteLoad);
            SelectUser = Command.Create<MultiCalendarDocsViewModelWrapper>(ExecuteSelectUser);
        }

        private void ExecuteLoad(MultiCalendarLoadArguments loadArgs)
        {
            if (loadArgs != null)
            {
                if (loadArgs.ResourceIds != null)
                {
                    ListOfResource.Where(r => loadArgs.ResourceIds.Contains(r.Resource.Id)).ToList().ForEach(r => r.IsSelected = true);
                    ListOfResource.Where(r => !loadArgs.ResourceIds.Contains(r.Resource.Id)).ToList().ForEach(r => r.IsSelected = false);
                }

                if (loadArgs.Dates != null && loadArgs.Dates.Any())
                {
                    SelectedDateFilter = loadArgs.Dates.First();
                    DisplayDate = SelectedDateFilter.Value;
                }
                else
                {

                    SelectedDateFilter = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Today.Hour, DateTime.Today.Minute, DateTime.Today.Second);
                }
            }
            else
            {
                SelectedDateFilter = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Today.Hour, DateTime.Today.Minute, DateTime.Today.Second);
            }
        }

        private void ExecuteSelectUser(MultiCalendarDocsViewModelWrapper resource)
        {
            if (resource != null)
            {
                RefreshView(new[] { resource.Resource.Id });
            }
        }

        private ExtendedObservableCollection<BlockViewModel> InitializeBlockPlaceHolders(ExtendedObservableCollection<BlockViewModel> blocks, ResourceViewModel resource, DateTime date)
        {
            #region helper func createBlockViewModel
            Func<DateTime, ResourceViewModel, BlockViewModel> createBlockViewModel = (blockStartTime, resourceViewModel) =>
            {
                var blockViewModel = _createBlockViewModel();
                blockViewModel.Categories = new ExtendedObservableCollection<BlockCategoryViewModel>();
                blockViewModel.Location = DefaultSpanLocation;
                blockViewModel.StartDateTime = blockStartTime;
                blockViewModel.EndDateTime = blockStartTime.Add(SpanSize);
                blockViewModel.Resource = resourceViewModel;
                return blockViewModel;
            };
            #endregion

            var startTime = date.Date;
            var endTime = startTime.AddDays(1);

            var validBlockStartTimes = new List<DateTime>();
            while (startTime <= endTime.Subtract(SpanSize))
            {
                validBlockStartTimes.Add(startTime);
                startTime = startTime.Add(SpanSize);
            }

            var blockPlaceHolderTimes = validBlockStartTimes.Where(t => blocks.All(b => b.StartDateTime != t));
            foreach (var placeHolderTime in blockPlaceHolderTimes)
            {
                blocks.Add(createBlockViewModel(placeHolderTime, resource));
            }

            return blocks.OrderBy(b => b.StartDateTime).ToExtendedObservableCollection();
        }

        public GetScheduleBlockResults ExecuteQueryBlocks(int[] resourceIds)
        {
            if (resourceIds.Any())
            {
                var selectedDate = SelectedDateFilter.GetValueOrDefault();
                return _multiCalendarViewService.GetScheduleBlocks(resourceIds, new[] { selectedDate });
            }
            else
            {
                return null;
            }
        }

        public void ExecutePopulateBlocks(GetScheduleBlockResults queryResult)
        {
            if (queryResult == null) return;
            var queriedResourceIds = queryResult.QueriedResourceIds;
            var queriedDateTimes = queryResult.QueriedDateTimes;

            if (queriedDateTimes.IsNullOrEmpty() || queriedResourceIds.IsNullOrEmpty()) return;

            var selectedDate = queriedDateTimes.First();

            foreach (var resource in ListOfResource.Where(r => r.IsSelected && queriedResourceIds.Contains(r.Resource.Id)).ToArray())
            {
                var localResource = resource;
                var newBlocks = queryResult.Blocks.Where(b => b.Resource.Id == localResource.Resource.Id).OrderBy(b => b.StartDateTime).ToExtendedObservableCollection();
                newBlocks = InitializeBlockPlaceHolders(newBlocks, localResource.Resource, selectedDate);
                var newOrphanedAppointments = queryResult.OrphanedAppointments.Where(a => a != null && a.Resource.Id == localResource.Resource.Id).OrderBy(a => a.StartDateTime).ToExtendedObservableCollection();
                var newResourceComment = queryResult.ResourceComments.First(c => c.UserId.GetValueOrDefault() == localResource.Resource.Id && c.Date.Date == selectedDate.Date);
                var newGlobalComment = queryResult.GlobalComments.First(c => c.Date.Date == selectedDate.Date);
                var canUpdateBlocks = !localResource.Blocks.SequenceEqual(newBlocks);
                var canUpdateGlobalComment = !Equals(localResource.GlobalComment, newGlobalComment);
                var canUpdateOrphanedAppointments = !localResource.OrphanedAppointments.SequenceEqual(newOrphanedAppointments);
                var canUpdateResourceComment = !Equals(localResource.ResourceComment, newResourceComment);

                using (new SuppressPropertyChangedScope())
                {
                    if (canUpdateBlocks) { resource.Blocks = newBlocks; }
                    if (canUpdateGlobalComment) { resource.GlobalComment = newGlobalComment; }
                    if (canUpdateOrphanedAppointments) { resource.OrphanedAppointments = newOrphanedAppointments; }
                    if (canUpdateResourceComment) { resource.ResourceComment = newResourceComment; }
                }

                // Trigger 'blocks' redraw only once if possible
                var canRedrawBlocks = canUpdateBlocks || canUpdateOrphanedAppointments;
                if (canRedrawBlocks)
                {
                    resource.Blocks = resource.Blocks.ToExtendedObservableCollection();
                }

                // Trigger 'header resource' redraw only once if possible
                // Redrawing blocks implicitly redraws the "header resource" since some values in the "header resource" are calculated from the blocks
                if (!canRedrawBlocks && (canUpdateGlobalComment || canUpdateResourceComment))
                {
                    var resourceComment = resource.ResourceComment;
                    using (new SuppressPropertyChangedScope()) resource.ResourceComment = null;
                    resource.ResourceComment = resourceComment;
                }
            }

            AppointmentEditor = new AppointmentEditor(_multiCalendarViewService,
                                                      () => ListOfResource.Select(d => d.Blocks),
                                                      _showRescheduleAppointment,
                                                      RefreshView, _taskQueue,
                                                      InteractionContext);
        }

        /// <summary>
        /// The default location for blocks
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel DefaultSpanLocation { get; set; }

        /// <summary>
        /// List of resources to which templates can be applied to
        /// Smallest unit of schedulable time
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan SpanSize { get; set; }

        /// <summary>
        /// Start of range for blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaStart { get; set; }

        /// <summary>
        /// End of range for blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaEnd { get; set; }

        /// <summary>
        /// Collection of resources
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<MultiCalendarDocsViewModelWrapper> ListOfResource { get; set; }

        /// <summary>
        /// Selected date to search by
        /// </summary>
        [DispatcherThread]
        public virtual DateTime? SelectedDateFilter
        {
            get { return _selectedDateFilter; }
            set
            {
                _selectedDateFilter = value;
                lock (ListOfResource)
                {
                    RefreshView(ListOfResource.Where(r => r.IsSelected).Select(r => r.Resource.Id).ToArray());
                }
            }
        }

        /// <summary>
        /// Gets or sets the date displayed
        /// </summary>
        [DispatcherThread]
        public virtual DateTime DisplayDate { get; set; }

        /// <summary>
        /// Helper class that contains logic for moving appointments and deleting Categories and Blocks.
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentEditor AppointmentEditor { get; protected set; }

        [DependsOn("SelectedDateFilter", "ListOfResource.Item.IsSelected")]
        [DispatcherThread]
        public virtual bool CanDisplayCalendar
        {
            get { return SelectedDateFilter.HasValue && ListOfResource.Any(r => r.IsSelected); }
        }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to load Blocks upon user selection
        /// </summary>
        public ICommand SelectUser { get; protected set; }



    }
}