﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    public class MultiCalendarViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public MultiCalendarViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowMultiCalendar()
        {
            ShowMultiCalendar(null);
        }

        public MultiCalendarReturnArguments ShowMultiCalendar(MultiCalendarLoadArguments loadArgs)
        {
            var canShowCalendar = PermissionId.ViewSchedule.EnsurePermission();
            if (!canShowCalendar) { return new MultiCalendarReturnArguments(); }

            var view = new MultiCalendarView();

            if (loadArgs != null) view.DataContext.EnsureType<MultiCalendarViewContext>().LoadArguments = loadArgs;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                         {
                                             Content = view,
                                             WindowState = WindowState.Normal,
                                             ResizeMode = ResizeMode.CanResizeWithGrip
                                         });

         
            var returnArgs = new MultiCalendarReturnArguments
                                 {
                                     HasRescheduledAppointmentSuccessfully = view.DataContext.EnsureType<MultiCalendarViewContext>().HasRescheduledAppointmentsSuccessfully,
                                     AppointmentIds =view.DataContext.EnsureType<MultiCalendarViewContext>().AppointmentIds
                                 };

            return returnArgs;
        }
    }

    public enum MultiCalendarLoadMode
    {
        None,
        Docs,
        Days
    }

    public class MultiCalendarLoadArguments
    {
        public MultiCalendarLoadMode LoadMode { get; set; }
        public int? PatientId { get; set; }
        public string PatientDisplayName { get; set; }
        public IEnumerable<int> ResourceIds { get; set; }
        public IEnumerable<DateTime> Dates { get; set; }
        public string ReasonForVisit { get; set; }
        public string Comment { get; set; }

        public int? AppointmentId { get; set; }

        /// <summary>
        /// Gets or sets the date time of the (optional) order appointment.
        /// </summary>
        /// <remarks>
        /// An order appointment is one that orders a Surgery Appointment.
        /// </remarks>
        public DateTime? OrderAppointmentDate { get; set; }
    }

    public class MultiCalendarReturnArguments
    {
        public bool? HasRescheduledAppointmentSuccessfully { get; set; }

        public HashSet<int> AppointmentIds { get; set; }
    }
}