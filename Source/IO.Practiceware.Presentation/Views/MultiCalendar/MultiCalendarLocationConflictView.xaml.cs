﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.MultiCalendar
{
    /// <summary>
    /// Interaction logic for MultiCalendarLocationConflictView.xaml
    /// </summary>
    public partial class MultiCalendarLocationConflictView
    {
        public MultiCalendarLocationConflictView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// The root view model (the view context) for the MultiCalendarLocationConflictView
    /// </summary>
    public class MultiCalendarLocationConflictViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IMultiCalendarViewService _multiCalendarViewService;
        
        public MultiCalendarLocationConflictViewContext(IMultiCalendarViewService multiCalendarViewService)
        {
            _multiCalendarViewService = multiCalendarViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ChangeLocation = Command.Create<AppointmentViewModel>(ExecuteChangeLocation).Async(() => InteractionContext);
            ChangeAllLocation = Command.Create(ExecuteChangeAllLocation).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            UndoBlockLocationChange = Command.Create(ExecuteUndoBlockLocationChange).Async(() => InteractionContext);
            Closing = Command.Create<Action>(ExecuteClosing).AsyncForClosing(() => InteractionContext);
        }

        private void ExecuteClosing(Action cancelClose)
        {
            if (InteractionContext.DialogResult == false)
            {
                ConflictedBlocks.Cast<IEditableObject>().ForEachWithSinglePropertyChangedNotification(b => b.EndEdit());
                OrphanedAppointments.Cast<INotifyPropertyChanged>().ForEachWithSinglePropertyChangedNotification(a =>
                    {
                        a.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                    });

                foreach (var appointment in OrphanedAppointments)
                {
                    appointment.IsOrphaned = true;
                }
                ConflictedBlocks.SelectMany(b => b.Categories).Where(c => OrphanedAppointments.Contains(c.Appointment)).ForEachWithSinglePropertyChangedNotification(c => c.Appointment = null);
                _multiCalendarViewService.MakeAppointmentsOrphaned(OrphanedAppointments.Select(a => a.Id).ToArray());
            }
        }

        private void ExecuteLoad()
        {
            // Execute load is synchronous so that we prevent display this screen all together if there are no orphaned appointments to display.
            var appointmentsToOrphan = OrphanedAppointments.Where(a => !((EncounterStatus)a.EncounterStatusId).CanCancel()).ToArray();
            if (appointmentsToOrphan.Any())
            {
                _multiCalendarViewService.MakeAppointmentsOrphaned(appointmentsToOrphan.Select(a => a.Id).ToArray());
                foreach (var appointment in appointmentsToOrphan)
                {
                    appointment.IsOrphaned = true;
                    OrphanedAppointments.Remove(appointment);
                }
            }

            if(!OrphanedAppointments.Any())
            {
                this.Dispatcher().Invoke(() =>
                    {
                        // Display alert if all appointments cannot have conflict resolution
                        InteractionManager.Current.Alert("No appointment conflicts to resolve.");
                        InteractionContext.Complete(true);
                    });
            }

            OrphanedAppointments.Cast<INotifyPropertyChanged>().ToList().ForEach(a =>
                {
                    a.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                });
        }

        private void OnAppointmentPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsCancelled") return;

            var appointment = sender as AppointmentViewModel;
            if (appointment != null && appointment.IsCancelled)
            {
                appointment.CastTo<INotifyPropertyChanged>().PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                OrphanedAppointments.Remove(appointment);
                if (OrphanedAppointments.Count == 0)
                {
                    ConflictedBlocks.Cast<IEditableObject>().ForEachWithSinglePropertyChangedNotification(b => b.EndEdit());
                    InteractionContext.Complete(true);
                }
            }
        }

        private void ExecuteChangeLocation(AppointmentViewModel appointment)
        {
            appointment.Location = NewBlockLocation;
            _multiCalendarViewService.SetAppointmentsLocation(new[] { appointment.Id }, NewBlockLocation.Id);

            System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    appointment.CastTo<INotifyPropertyChanged>().PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                    OrphanedAppointments.Remove(appointment);
                    if (OrphanedAppointments.Count == 0)
                    {
                        ConflictedBlocks.Cast<IEditableObject>().ForEachWithSinglePropertyChangedNotification(b => b.EndEdit());
                        InteractionContext.Complete(true);
                    }
                });
        }

        private void ExecuteChangeAllLocation()
        {
            foreach (var appointment in OrphanedAppointments) { appointment.Location = NewBlockLocation; }

            var appointmentIds = OrphanedAppointments.Select(a => a.Id).ToList();
            _multiCalendarViewService.SetAppointmentsLocation(appointmentIds, NewBlockLocation.Id);

            ConflictedBlocks.Cast<IEditableObject>().ForEachWithSinglePropertyChangedNotification(b => b.EndEdit());
            OrphanedAppointments.Cast<INotifyPropertyChanged>().ForEachWithSinglePropertyChangedNotification(a =>
                {
                    a.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                });
            System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionContext.Complete(true));
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteUndoBlockLocationChange()
        {
            System.Windows.Application.Current.Dispatcher.Invoke(() => ConflictedBlocks.Cast<IEditableObject>().ForEachWithSinglePropertyChangedNotification(b => b.CancelEdit()));
            _multiCalendarViewService.SetScheduleBlockLocations(ConflictedBlocks.ToDictionary(b => b.Id.Value, b => b.Location.Id));
            OrphanedAppointments.Cast<INotifyPropertyChanged>().ForEachWithSinglePropertyChangedNotification(a =>
                {
                    a.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnAppointmentPropertyChanged);
                });
            System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionContext.Complete(true));
        }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentViewModel> OrphanedAppointments { get; set; }

        public virtual ObservableCollection<BlockViewModel> ConflictedBlocks { get; set; }

        public virtual ColoredViewModel NewBlockLocation { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        public ICommand ShowViewPatientDemographics { get; protected set; }

        public virtual ICommand ShowViewCancelAppointment { get; set; }

        public ICommand ChangeLocation { get; protected set; }

        public ICommand ShowViewAppointmentSearch { get; set; }

        public ICommand ChangeAllLocation { get; protected set; }

        public ICommand Close { get; protected set; }

        public ICommand UndoBlockLocationChange { get; protected set; }

        /// <summary>
        /// Command that is executed when this instance is closing
        /// </summary>
        public virtual ICommand Closing { get; protected set; }
    }
}