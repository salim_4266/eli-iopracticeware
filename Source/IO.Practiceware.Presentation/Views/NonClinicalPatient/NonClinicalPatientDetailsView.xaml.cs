﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.NonClinicalPatient;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.ViewServices.NonClinicalPatient;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.NonClinicalPatient
{
    /// <summary>
    ///     Interaction logic for NonClinicalPatientDetailsView.xaml
    /// </summary>
    public partial class NonClinicalPatientDetailsView
    {
        public NonClinicalPatientDetailsView()
        {
            InitializeComponent();
        }
    }

    public class NonClinicalPatientDetailsViewContext : IViewContext, IHasWizardDataContext
    {
        private readonly Func<PatientInfoViewManager> _createPatientInfoViewManager;
        private readonly INonClinicalPatientViewService _nonClinicalPatientViewService;
        private string _contactTypeName;

        public NonClinicalPatientDetailsViewContext(
            INonClinicalPatientViewService nonClinicalPatientViewService,
            Func<PatientInfoViewManager> createPatientInfoViewManager,
            Func<AddressViewModel> createAddress,
            Func<PhoneNumberViewModel> createPhoneNumber,
            Func<EmailAddressViewModel> createEmailAddress)
        {
            _nonClinicalPatientViewService = nonClinicalPatientViewService;
            _createPatientInfoViewManager = createPatientInfoViewManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ValidateSocialSecurity = Command.Create(ExecuteValidateSocialSecurity, () => NonClinicalPatient != null && NonClinicalPatient.SocialSecurity != null && NonClinicalPatient.SocialSecurity.Length == 9).Async(() => InteractionContext);
            AllowSocialSecurityConflict = Command.Create(ExecuteAllowSocialSecurityConflict);
            UsePatientAddress = Command.Create(ExecuteUsePatientAddress).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CanSaveExecute).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel);
            ConvertToPatient = Command.Create(ExecuteConvertToPatient, CanSaveExecute).Async(() => InteractionContext);
            AddAddress = Command.Create(() => NonClinicalPatient.Address = createAddress().Modify(x => x.AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Home }));
            RemoveAddress = Command.Create(() => NonClinicalPatient.Address = null);
            AddPhoneNumber = Command.Create(() => NonClinicalPatient.PhoneNumber = createPhoneNumber());
            RemovePhoneNumber = Command.Create(() => NonClinicalPatient.PhoneNumber = null);
            AddEmployerAddress = Command.Create(() => NonClinicalPatient.Employer.Address = createAddress().Modify(x => x.AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Business }));
            RemoveEmployerAddress = Command.Create(() => NonClinicalPatient.Employer.Address = null);
            AddEmailAddress = Command.Create(() => NonClinicalPatient.EmailAddress = createEmailAddress());
            RemoveEmailAddress = Command.Create(() => NonClinicalPatient.EmailAddress = null);
        }

        /// <summary>
        ///     The edited or newly created Id of the nonclinical patient (policyholder or patient contact)
        /// </summary>
        public virtual int? NonClinicalPatientId { get; set; }

        /// <summary>
        ///     A flag to determine whether or not the NonClinicalPatient data has been saved on this screen at least once.
        /// </summary>
        public bool NonClinicalPatientSuccessfullySaved { get; set; }

        /// <summary>
        ///     Gets or sets the patient id of the patient in context.
        /// </summary>
        /// <remarks>
        ///     The patient may be different then the NonClinicalPatient
        /// </remarks>
        public virtual int? DefaultAddressPatientId { get; set; }

        /// <summary>
        ///     Gets or sets the NonClinicalPatient
        /// </summary>
        [DispatcherThread]
        public virtual NonClinicalPatientViewModel NonClinicalPatient { get; set; }

        /// <summary>
        ///     Gets or sets the (address) states or provinces.
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<StateOrProvinceViewModel> StateOrProvinces { get; set; }

        /// <summary>
        ///     Gets or sets the phone number types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> PhoneNumberTypes { get; set; }

        /// <summary>
        ///     Gets or sets the email address types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> EmailAddressTypes { get; set; }

        /// <summary>
        ///     Gets or sets the email address types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> GenderTypes { get; set; }

        /// <summary>
        ///     Gets or sets the employment statuses
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> EmploymentStatuses { get; set; }

        /// <summary>
        ///     Gets or sets a flag representing a data trigger for a social security conflict
        /// </summary>
        [DispatcherThread]
        public virtual bool SocialSecurityConflictDataTrigger { get; set; }

        /// <summary>
        ///     Gets or sets the patient that has a social security conflict with the current NonClinicalPatient
        /// </summary>
        [DispatcherThread]
        public virtual NonClinicalPatientViewModel PatientWithSocialSecurityConflict { get; set; }

        /// <summary>
        ///     Gets or sets the header of the associated view
        /// </summary>
        [DispatcherThread]
        [DependsOn("ContactTypeName")]
        public virtual string WindowTitle
        {
            get
            {
                string action = NonClinicalPatientId.HasValue ? "Edit" : "Create";
                return string.Format("{0} {1}", action, ContactTypeName);
            }
        }

        public virtual string ContactTypeName
        {
            get { return _contactTypeName.IsNullOrEmpty() ? "Patient" : _contactTypeName; }
            set { _contactTypeName = value; }
        }

        /// <summary>
        ///     Gets the command to load
        /// </summary>
        public ICommand Load { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public virtual ICommand AddAddress { get; protected set; }

        public virtual ICommand RemoveAddress { get; protected set; }

        public virtual ICommand AddPhoneNumber { get; protected set; }

        public virtual ICommand RemovePhoneNumber { get; protected set; }

        public virtual ICommand AddEmployerAddress { get; protected set; }

        public virtual ICommand RemoveEmployerAddress { get; protected set; }

        public virtual ICommand AddEmailAddress { get; protected set; }

        public virtual ICommand RemoveEmailAddress { get; protected set; }


        /// <summary>
        ///     Gets the command to auto-fill the city and state fields for the NonClinicalPatient
        /// </summary>
        public ICommand AutoFillCityAndStateForNonClinicalPatient { get; protected set; }

        /// <summary>
        ///     Gets the command to auto-fill the city and state fields for the employer
        /// </summary>
        public ICommand AutoFillCityAndStateForEmployer { get; protected set; }

        /// <summary>
        ///     Gets the command to save the NonClinicalPatient
        /// </summary>
        public ICommand Save { get; protected set; }

        /// <summary>
        ///     Gets a command to use the patient address
        /// </summary>
        public ICommand UsePatientAddress { get; protected set; }

        /// <summary>
        ///     Gets a command to validate whether there is a social security number conflict
        /// </summary>
        public ICommand ValidateSocialSecurity { get; protected set; }

        /// <summary>
        ///     Gets a command to allow a social security conflict
        /// </summary>
        public ICommand AllowSocialSecurityConflict { get; protected set; }

        /// <summary>
        ///     Gets a command that converts a non-clinical patient to a full patient
        /// </summary>
        public ICommand ConvertToPatient { get; protected set; }

        [DispatcherThread]
        public virtual IWizardDataContext Wizard { get; set; }

        /// <summary>
        ///     Gets or sets the interaction context.contact
        /// </summary>
        public virtual IInteractionContext InteractionContext { get; set; }

        private void ExecuteConvertToPatient()
        {
            ExecuteSave();
            System.Windows.Application.Current.Dispatcher.Invoke(() => _createPatientInfoViewManager().ShowPatientInfo(new PatientInfoLoadArguments { PatientId = NonClinicalPatient.Id, IsConvertingNonClinicalPatient = true }));
        }

        private bool CanSaveExecute()
        {
            return NonClinicalPatient != null && NonClinicalPatient.IsValid()
                && (NonClinicalPatient.EmailAddress == null || NonClinicalPatient.EmailAddress.IsEmpty || NonClinicalPatient.EmailAddress.IsValid())
                && (NonClinicalPatient.Employer == null || NonClinicalPatient.Employer.Address == null || NonClinicalPatient.Employer.Address.IsEmpty
                    || NonClinicalPatient.Employer.Address.IsValid())
                && (NonClinicalPatient.PhoneNumber == null || NonClinicalPatient.PhoneNumber.IsEmpty || NonClinicalPatient.PhoneNumber.IsValid())
                && (NonClinicalPatient.Address == null || NonClinicalPatient.Address.IsEmpty || NonClinicalPatient.Address.IsValid());
        }

        private void ExecuteAllowSocialSecurityConflict()
        {
            NonClinicalPatient.HasSocialSecurityConflict = false;
        }

        private void ExecuteValidateSocialSecurity()
        {
            PatientWithSocialSecurityConflict = _nonClinicalPatientViewService.GetPatientBySocialSecurity(NonClinicalPatient.SocialSecurity);
            if (PatientWithSocialSecurityConflict != null && PatientWithSocialSecurityConflict.Id != NonClinicalPatient.Id)
            {
                NonClinicalPatient.HasSocialSecurityConflict = true;
                SocialSecurityConflictDataTrigger = true;
            }
            else
            {
                NonClinicalPatient.HasSocialSecurityConflict = false;
            }
        }

        private void ExecuteUsePatientAddress()
        {
            if (DefaultAddressPatientId.HasValue)
            {
                NonClinicalPatient.Address = _nonClinicalPatientViewService.GetPatientAddress(DefaultAddressPatientId.Value);
            }
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        public void SaveNonClinicalPatient()
        {
            NonClinicalPatientId = _nonClinicalPatientViewService.SaveNonClinicalPatient(NonClinicalPatient);
            NonClinicalPatientSuccessfullySaved = true;
        }

        private void ExecuteSave()
        {
            SaveNonClinicalPatient();

            InteractionContext.Complete(true);
        }

        private void ExecuteLoad()
        {
            NonClinicalPatientLoadInformation loadInformation = _nonClinicalPatientViewService.GetLoadInformation(NonClinicalPatientId);
            EmploymentStatuses = loadInformation.EmploymentStatuses;
            PhoneNumberTypes = loadInformation.PhoneNumberTypes;
            StateOrProvinces = loadInformation.StateOrProvinces;
            NonClinicalPatient = loadInformation.NonClinicalPatient;
            EmailAddressTypes = loadInformation.EmailAddressTypes;
            GenderTypes = loadInformation.GenderTypes;
        }
    }
}