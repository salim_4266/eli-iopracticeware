﻿<s:View
	xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
	xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
	xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
	xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
	xmlns:telerik="http://schemas.telerik.com/2008/xaml/presentation"
	xmlns:s="http://soaf/presentation"
    xmlns:nonClinicalPatient="clr-namespace:IO.Practiceware.Presentation.Views.NonClinicalPatient"
	xmlns:Common="clr-namespace:IO.Practiceware.Presentation.Views.Common"
	xmlns:Model="clr-namespace:IO.Practiceware.Model;assembly=IO.Practiceware.Model"
	xmlns:Converters="clr-namespace:IO.Practiceware.Presentation.Views.Common.Converters"
	xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
	xmlns:Behaviors="clr-namespace:IO.Practiceware.Presentation.Views.Common.Behaviors"
	xmlns:ei="http://schemas.microsoft.com/expression/2010/interactions"
	xmlns:Controls="clr-namespace:IO.Practiceware.Presentation.Views.Common.Controls"
    xmlns:wizard="clr-namespace:IO.Practiceware.Presentation.Views.Wizard"
    mc:Ignorable="d"
    xmlns:behaviors="clr-namespace:IO.Practiceware.Presentation.Views.Common.Behaviors"
    x:Class="IO.Practiceware.Presentation.Views.NonClinicalPatient.NonClinicalPatientDetailsView" AutoCreateDataContext="True" 
    ViewContextType="nonClinicalPatient:NonClinicalPatientDetailsViewContext"
	x:Name="RootView" 
    Header="{Binding WindowTitle}"
    Load="{Binding Load}">

    <s:View.Resources>
        <Converters:BooleanToVisibilityConverter IsInverse="True" x:Key="InvertedBooleanToVisibilityConverter"/>
        <Converters:IsNullToBooleanConverter x:Key="IsNullToBooleanConverter" />
        <Converters:IsNullOrEmptyToVisibilityConverter x:Key="NullOrEmptyVisibilityConverter" />
        <Converters:HasValueToVisibilityConverter x:Key="HasValueToVisibilityConverter" />
        <Converters:HasValueToVisibilityConverter IsInverse="True" x:Key="InverseHasValueToVisibilityConverter"/>
        <s:DataContextReference x:Key="ViewDataContextReference" DataContext="{Binding DataContext, ElementName=RootView}" />
    </s:View.Resources>

    <i:Interaction.Triggers>
        <ei:DataTrigger Binding="{Binding SocialSecurityConflictDataTrigger}" Value="True">
            <i:InvokeCommandAction>
                <i:InvokeCommandAction.Command>
                    <s:ConfirmCommand Command="{Binding AllowSocialSecurityConflict}">
                        <s:ConfirmCommand.Content>
                            <StackPanel Orientation="Vertical" DataContext="{Binding Source={StaticResource ViewDataContextReference}, Path=DataContext}">
                                <StackPanel Orientation="Horizontal" Margin="0,0,0,10">
                                    <TextBlock Text="Social security number found in database for the following patient:"/>
                                </StackPanel>
                                <Grid>
                                    <Grid.RowDefinitions>
                                        <RowDefinition Height="*"/>
                                        <RowDefinition Height="*"/>
                                        <RowDefinition Height="*"/>
                                        <RowDefinition Height="*"/>
                                    </Grid.RowDefinitions>
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="Auto"/>
                                        <ColumnDefinition Width="*"/>
                                    </Grid.ColumnDefinitions>
                                    <TextBlock Text="Patient:" TextAlignment="Right" Grid.Row="0"/>
                                    <TextBlock Text="Patient ID:" TextAlignment="Right" Grid.Row="1"/>
                                    <TextBlock Text="Birthdate:" TextAlignment="Right" Grid.Row="2"/>
                                    <TextBlock Text="SSN:" TextAlignment="Right" Grid.Row="3"/>
                                    <TextBlock Grid.Row="0" Grid.Column="1" Margin="5,0,0,0">
                                        <Run Text="{Binding PatientWithSocialSecurityConflict.LastName, Mode=TwoWay}"/>
                                        <Run Text=","/>
                                        <Run Text="{Binding PatientWithSocialSecurityConflict.FirstName, Mode=TwoWay}"/>
                                    </TextBlock>
                                    <TextBlock Grid.Row="1" Grid.Column="1" Margin="5,0,0,0"
                                               Text="{Binding PatientWithSocialSecurityConflict.Id, Mode=TwoWay}"/>
                                    <TextBlock Grid.Row="2" Grid.Column="1" Margin="5,0,0,0"
                                               Text="{Binding PatientWithSocialSecurityConflict.DateOfBirth, Mode=TwoWay, StringFormat=\{0:MM/dd/yyyy\}}"/>
                                    <TextBlock Grid.Row="3" Grid.Column="1" Margin="5,0,0,0"
                                               Text="{Binding PatientWithSocialSecurityConflict.SocialSecurity, Mode=TwoWay}"/>
                                </Grid>
                            </StackPanel>
                        </s:ConfirmCommand.Content>
                        <s:ConfirmCommand.OkButtonContent>
                            Use Anyway
                        </s:ConfirmCommand.OkButtonContent>
                    </s:ConfirmCommand>
                </i:InvokeCommandAction.Command>
            </i:InvokeCommandAction>
            <ei:ChangePropertyAction TargetObject="{Binding}" PropertyName="SocialSecurityConflictDataTrigger" Value="False" />
            <ei:ChangePropertyAction TargetObject="{Binding}" PropertyName="PatientWithSocialSecurityConflict" Value="{x:Null}" />
        </ei:DataTrigger>
    </i:Interaction.Triggers>

    <Grid x:Name="LayoutRoot" Background="White">
        <Grid.Resources>
            <Style TargetType="telerik:RadWatermarkTextBox" BasedOn="{StaticResource DisabledRadWatermarkTextBox}"></Style>
        </Grid.Resources>
        <Grid.RowDefinitions>
            <RowDefinition Height="1*"/>
            <RowDefinition Height="3*"/>
            <RowDefinition Height="44"/>
        </Grid.RowDefinitions>
        <Border Grid.Row="0" x:Name="InfoArea" BorderThickness="0,0,0,1" BorderBrush="{DynamicResource StandardBorderBrush}">
            <TextBlock TextWrapping="Wrap" FontFamily="Segoe UI"
                    FontSize="18" TextAlignment="Center" VerticalAlignment="Center" Margin="0,10,0,0">
                <Run Text="Enter contact details:" />

            </TextBlock>
        </Border>
        <ScrollViewer x:Name="InterfaceArea" Grid.Row="1" VerticalScrollBarVisibility="Auto">
            <StackPanel Margin="10,5">
                <Grid x:Name="MainInterface" HorizontalAlignment="Center">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                    </Grid.RowDefinitions>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>
                    <TextBlock Text="Name:" Grid.Row="0" Grid.Column="0" Margin="5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                    <StackPanel Grid.Row="0" Grid.Column="1" Margin="5,0" Orientation="Horizontal">
                        <telerik:RadWatermarkTextBox WatermarkContent="First Name" Width="190" Height="25" Margin="3,5" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.FirstName, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                                     Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                        <telerik:RadWatermarkTextBox WatermarkContent="Middle Name" Width="100" Height="25" Margin="3,5" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.MiddleName, Mode=TwoWay}"/>
                        <telerik:RadWatermarkTextBox WatermarkContent="Last Name" Width="230" Height="25" Margin="3,5" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.LastName, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                                     Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                        <telerik:RadWatermarkTextBox WatermarkContent="Suffix" Width="50" Height="25" Margin="3,5" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.Suffix, Mode=TwoWay}"/>
                    </StackPanel>

                    <TextBlock Text="Address:" Grid.Row="1" Grid.Column="0" Margin="5,5,5,5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Top"/>
                    <StackPanel Grid.Row="1" Grid.Column="1" Margin="0,-5,0,0">
                        <StackPanel Orientation="Horizontal" Margin="0,0,0,0">
                            <StackPanel Orientation="Vertical">
                                <Button HorizontalAlignment="Left" Visibility="{Binding DefaultAddressPatientId, Converter={StaticResource HasValueToVisibilityConverter}}" Content="Use Patient Address" Height="25" Width="160" Margin="8,5,5,0" FontFamily="Segoe UI" FontSize="13.333" Style="{StaticResource NormalButtonStyle}" IsTabStop="False"
                                    Command="{Binding UsePatientAddress}" />
                                <StackPanel Orientation="Vertical" Visibility="{Binding NonClinicalPatient.Address, Converter={StaticResource HasValueToVisibilityConverter}}">

                                    <Border BorderBrush="{StaticResource BorderBrush}" Width="590" Margin="8,5,0,5" BorderThickness="1" Background="{StaticResource BorderFillBrush}">
                                        <Grid>
                                            <Grid.ColumnDefinitions>
                                                <ColumnDefinition Width="*"/>
                                                <ColumnDefinition Width="Auto"/>
                                            </Grid.ColumnDefinitions>

                                            <Controls:AddressInputView Grid.Column="0" Margin="1,0,1,5" Address="{Binding NonClinicalPatient.Address, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True, NotifyOnSourceUpdated=True, NotifyOnTargetUpdated=True}" AddressTypesVisibility="Collapsed"
                                                           StateOrProvinces="{Binding StateOrProvinces}" IsPrimaryCheckboxVisibility="Collapsed" Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                                            <Button Grid.Column="1" Style="{StaticResource CancelIconButton}" Command="{Binding RemoveAddress}" Height="13" Width="13" Margin="3" Panel.ZIndex="10" IsTabStop="False" VerticalAlignment="Top"/>
                                        </Grid>
                                    </Border>
                                </StackPanel>

                            </StackPanel>
                            <Button Style="{StaticResource NormalButtonStyle}" Command="{Binding AddAddress}" Height="25" Width="25" Margin="7,5" Visibility="{Binding NonClinicalPatient.Address, Converter={StaticResource InverseHasValueToVisibilityConverter}}">
                                <Path Stretch="Fill" Fill="#FF666666" Data="F1 M 337.7,165.82L 331.62,165.82L 331.62,171.9L 328.42,171.9L 328.42,165.82L 322.34,165.82L 322.34,162.62L 328.42,162.62L 328.42,156.54L 331.62,156.54L 331.62,162.62L 337.7,162.62L 337.7,165.82 Z " Height="14" Width="14"/>
                            </Button>
                        </StackPanel>
                    </StackPanel>
                    <TextBlock Text="Phone:" Grid.Row="3" Grid.Column="0" Margin="5,5,5,5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                    <StackPanel Grid.Row="3" Grid.Column="1">
                        <StackPanel Orientation="Horizontal" Margin="0">
                            <StackPanel Orientation="Horizontal" Visibility="{Binding NonClinicalPatient.PhoneNumber, Converter={StaticResource HasValueToVisibilityConverter}}">
                                <Border BorderBrush="{StaticResource BorderBrush}" Width="590" Margin="8,0,0,0" BorderThickness="1" Background="{StaticResource BorderFillBrush}">
                                    <Controls:PhoneNumberInputView Margin="1,3" PhoneNumber="{Binding NonClinicalPatient.PhoneNumber, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True, NotifyOnSourceUpdated=True, NotifyOnTargetUpdated=True}" 
                                                           PhoneTypes="{Binding PhoneNumberTypes}" IsPrimaryCheckboxVisibility="Collapsed" Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                                </Border>
                                <Button Style="{StaticResource CancelIconButton}" Command="{Binding RemovePhoneNumber}" Height="13" Width="13" Margin="-22,-13,0,0" Panel.ZIndex="10" IsTabStop="False"/>
                            </StackPanel>
                            <Button Style="{StaticResource NormalButtonStyle}" Command="{Binding AddPhoneNumber}" Height="25" Width="25" Margin="7,5" Visibility="{Binding NonClinicalPatient.PhoneNumber, Converter={StaticResource InverseHasValueToVisibilityConverter}}">
                                <Path Stretch="Fill" Fill="#FF666666" Data="F1 M 337.7,165.82L 331.62,165.82L 331.62,171.9L 328.42,171.9L 328.42,165.82L 322.34,165.82L 322.34,162.62L 328.42,162.62L 328.42,156.54L 331.62,156.54L 331.62,162.62L 337.7,162.62L 337.7,165.82 Z " Height="14" Width="14"/>
                            </Button>
                        </StackPanel>
                    </StackPanel>
                    <TextBlock Text="Email:" TextWrapping="Wrap" MaxWidth="100" Grid.Row="4" Grid.Column="0" Margin="5,0" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center" LineHeight="13" LineStackingStrategy="BlockLineHeight"/>

                    <StackPanel Grid.Column="1" Grid.Row="4" Orientation="Horizontal">
                        <Border Margin="8,5,0,0" BorderBrush="{StaticResource BorderBrush}" BorderThickness="1" Background="{StaticResource BorderFillBrush}" Width="590" HorizontalAlignment="Left" Visibility="{Binding NonClinicalPatient.EmailAddress, Converter={StaticResource HasValueToVisibilityConverter}}">
                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="*"/>
                                    <ColumnDefinition Width="Auto"/>
                                </Grid.ColumnDefinitions>
                                <Controls:EmailAddressInputView Margin="1,4" Grid.Column="0" 
                                                                EmailAddressTypes="{Binding EmailAddressTypes}" 

                                                                EmailAddress="{Binding NonClinicalPatient.EmailAddress, UpdateSourceTrigger=PropertyChanged, NotifyOnSourceUpdated=True, NotifyOnTargetUpdated=True}" IsPrimaryCheckboxVisibility="Collapsed"/>
                                <Button Grid.Column="1" Style="{StaticResource CancelIconButton}" Command="{Binding RemoveEmailAddress}" Height="13" Width="13" Margin="3" VerticalAlignment="Top" Panel.ZIndex="10" IsTabStop="False"/>
                            </Grid>
                        </Border>
                        <Button Style="{StaticResource NormalButtonStyle}" Command="{Binding AddEmailAddress}" Height="25" Width="25" Margin="7,5" Visibility="{Binding NonClinicalPatient.EmailAddress, Converter={StaticResource InverseHasValueToVisibilityConverter}}">
                            <Path Stretch="Fill" Fill="#FF666666" Data="F1 M 337.7,165.82L 331.62,165.82L 331.62,171.9L 328.42,171.9L 328.42,165.82L 322.34,165.82L 322.34,162.62L 328.42,162.62L 328.42,156.54L 331.62,156.54L 331.62,162.62L 337.7,162.62L 337.7,165.82 Z " Height="14" Width="14"/>
                        </Button>
                    </StackPanel>
                    <TextBlock Text="DOB &amp; SSN &amp; Gender:" VerticalAlignment="Center" TextWrapping="Wrap" MaxWidth="100" Grid.Row="5" Grid.Column="0" Margin="5,3,5,0" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" LineHeight="13" LineStackingStrategy="BlockLineHeight"/>

                    <StackPanel Grid.Row="5" Grid.Column="1" Margin="5,0" Orientation="Horizontal">
                        <telerik:RadDatePicker DateTimeWatermarkContent="Birthdate" Height="25" Margin="3,5" CalendarStyle="{StaticResource  EmbeddedCalendarStyle}" Width="85" HorizontalAlignment="Left" FontSize="13.333"
                                               SelectedDate="{Binding NonClinicalPatient.DateOfBirth, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                               Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                        <telerik:RadWatermarkTextBox WatermarkContent="Social Security #" 
                                                         Text="{Binding NonClinicalPatient.SocialSecurity, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                                                         Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}" Margin="3,5">
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="LostFocus">
                                    <i:InvokeCommandAction Command="{Binding ValidateSocialSecurity}"/>
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </telerik:RadWatermarkTextBox>
                      
                        <telerik:RadComboBox x:Name="Gender" CanAutocompleteSelectItems="True" EmptyText="Select Gender" IsManipulationEnabled="True" IsEditable="True" FontFamily="Segoe UI"
                                            FontSize="13.333" FontStyle="Italic" ItemContainerStyle="{StaticResource  BlueMouseOverRadComboBoxItemContainerStyle}"
                                            CanKeyboardNavigationSelectItems="True" ClearSelectionButtonContent="Collapsed" OpenDropDownOnFocus="True" Height="25" Margin="3,5"
                                            ItemsSource="{Binding GenderTypes}" DisplayMemberPath="Name" SelectedValuePath="Id" MinWidth="150"
                                            SelectedItem="{Binding NonClinicalPatient.SelectedGender, Mode=TwoWay}" 
                                            Visibility="Visible">
                            <i:Interaction.Behaviors>
                                <behaviors:RadComboBoxEmptyTextFontBehavior />
                            </i:Interaction.Behaviors>
                            <telerik:RadComboBox.ItemsPanel>
                                <ItemsPanelTemplate>
                                    <VirtualizingStackPanel />
                                </ItemsPanelTemplate>
                            </telerik:RadComboBox.ItemsPanel>
                        </telerik:RadComboBox>

                    </StackPanel>
                  
                    <Button x:Name="EmploymentInfoButton" Content="Employment Information" Grid.Row="6" FontFamily="Segoe UI" FontSize="13.333" Grid.Column="1" Height="25" Margin="8,0" Style="{StaticResource NormalButtonStyle}" HorizontalAlignment="Left" Width="160">
                        <i:Interaction.Triggers>
                            <i:EventTrigger EventName="Click">
                                <ei:ChangePropertyAction PropertyName="Visibility" Value="Visible" TargetName="EmployerInterface"/>
                                <ei:CallMethodAction MethodName="ScrollToEnd" TargetObject="{Binding RelativeSource={RelativeSource AncestorType=ScrollViewer}}"/>
                            </i:EventTrigger>
                        </i:Interaction.Triggers>
                    </Button>
                </Grid>
                <Grid x:Name="EmployerInterface" HorizontalAlignment="Center" Visibility="Collapsed" Margin="13,0,0,0">
                    <i:Interaction.Triggers>
                        <i:EventTrigger EventName="IsVisibleChanged">
                            <ei:CallMethodAction MethodName="ScrollToEnd" TargetObject="{Binding RelativeSource={RelativeSource AncestorType=ScrollViewer}}"/>
                        </i:EventTrigger>
                    </i:Interaction.Triggers>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                    </Grid.RowDefinitions>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>
                    <TextBlock Text="Occupation:" Width="87" Grid.Row="0" Grid.Column="0" Margin="5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                    <StackPanel Grid.Row="0" Grid.Column="1" Margin="6,0,5,-2" Orientation="Horizontal" Width="590" HorizontalAlignment="Left" >
                        <telerik:RadWatermarkTextBox WatermarkContent="Occupation" Width="275" Height="25" Margin="2,5" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.Occupation, Mode=TwoWay}"/>
                        <TextBlock Text="Employment Status:" Margin="15,5,5,5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                        <telerik:RadComboBox EmptyText="Status" CanAutocompleteSelectItems="True" IsManipulationEnabled="True" IsEditable="True" Margin="3,5" CanKeyboardNavigationSelectItems="True" ClearSelectionButtonVisibility="Collapsed" OpenDropDownOnFocus="True" Height="25" Width="125" FontSize="13.333" FontFamily="Segoe UI" FontStyle="Italic" ItemContainerStyle="{StaticResource  BlueMouseOverRadComboBoxItemContainerStyle}"
                                             DisplayMemberPath="Name"
                                             ItemsSource="{Binding EmploymentStatuses}"
                                             SelectedItem="{Binding NonClinicalPatient.EmploymentStatus, Mode=TwoWay}">
                            <i:Interaction.Behaviors>
                                <Behaviors:RadComboBoxEmptyTextFontBehavior/>
                            </i:Interaction.Behaviors>
                        </telerik:RadComboBox>
                    </StackPanel>
                    <TextBlock Text="Company:" Grid.Row="1" Grid.Column="0" Margin="5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                    <StackPanel Grid.Row="1" Grid.Column="1" Margin="5,0" Orientation="Horizontal">
                        <telerik:RadWatermarkTextBox WatermarkContent="Company Name" Width="275" Height="25" Margin="3,0" FontSize="13.333"
                                                     Text="{Binding NonClinicalPatient.Employer.Name, Mode=TwoWay}"/>
                    </StackPanel>

                    <TextBlock Text="Address:" Grid.Row="2" Grid.Column="0" Margin="5" TextAlignment="Right" FontFamily="Segoe UI" FontWeight="SemiBold" FontSize="13.333" VerticalAlignment="Center"/>
                    <StackPanel Grid.Column="1" Grid.Row="2">
                        <StackPanel Orientation="Horizontal" Margin="0,3,0,0">
                            <StackPanel Orientation="Horizontal" Visibility="{Binding NonClinicalPatient.Employer.Address, Converter={StaticResource HasValueToVisibilityConverter}}">
                                <Border BorderBrush="{StaticResource BorderBrush}" Width="590" Margin="8,1,0,0" BorderThickness="1" Background="{StaticResource BorderFillBrush}">
                                    <Grid>
                                        <Grid.ColumnDefinitions>
                                            <ColumnDefinition Width="*"/>
                                            <ColumnDefinition Width="Auto"/>
                                        </Grid.ColumnDefinitions>
                                        <Controls:AddressInputView Grid.Column="0"  Margin="0,0,0,3" Address="{Binding NonClinicalPatient.Employer.Address, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True, NotifyOnSourceUpdated=True, NotifyOnTargetUpdated=True}" AddressTypesVisibility="Collapsed"
                                                           StateOrProvinces="{Binding StateOrProvinces}" IsPrimaryCheckboxVisibility="Collapsed" Validation.ErrorTemplate="{StaticResource RedCornerErrorTemplateWithPopup}"/>
                                        <Button Grid.Column="1"  Style="{StaticResource CancelIconButton}" Command="{Binding RemoveEmployerAddress}"  Height="13" Width="13" Margin="3" Panel.ZIndex="10" IsTabStop="False" VerticalAlignment="Top"/>
                                    </Grid>
                                </Border>
                            </StackPanel>
                            <Button Style="{StaticResource NormalButtonStyle}" Command="{Binding AddEmployerAddress}" Height="25" Width="25" Margin="7,5" VerticalAlignment="Top" Visibility="{Binding NonClinicalPatient.Employer.Address, Converter={StaticResource InverseHasValueToVisibilityConverter}}">
                                <Path Stretch="Fill" Fill="#FF666666" Data="F1 M 337.7,165.82L 331.62,165.82L 331.62,171.9L 328.42,171.9L 328.42,165.82L 322.34,165.82L 322.34,162.62L 328.42,162.62L 328.42,156.54L 331.62,156.54L 331.62,162.62L 337.7,162.62L 337.7,165.82 Z " Height="14" Width="14"/>
                            </Button>
                        </StackPanel>
                    </StackPanel>


                </Grid>
            </StackPanel>
        </ScrollViewer>
        <Border Grid.Row="2" x:Name="SaveCancelButtons" VerticalAlignment="Stretch" BorderBrush="#FFACACAC" BorderThickness="0,2,0,0" Background="#FFE5E5E5"
                Visibility="{Binding Wizard.IsInWizardMode, Converter={StaticResource InvertedBooleanToVisibilityConverter}}">
            <Grid HorizontalAlignment="Stretch" VerticalAlignment="Center" Margin="5,0" >

                <Button Content="Cancel" Width="65" Style="{StaticResource NormalButtonStyle}" Height="25"
                            Margin="5,0" VerticalAlignment="Center" HorizontalAlignment="Left" ToolTip="Cancel and close window"
                            Command="{Binding Cancel}"/>


                <Button x:Name="ConvertToPatientButton" Margin="5,0" Height="30" Width="110"
                        HorizontalAlignment="Center" VerticalAlignment="Center"
                        Style="{StaticResource ActionButtonStyle}" Command="{Binding ConvertToPatient}"
                        Content="Convert To Patient" Visibility="{Binding DataContext.Wizard, Converter={StaticResource IsNullToBooleanConverter}, RelativeSource={RelativeSource AncestorType={x:Type s:View}}}">
                </Button>

                <Button x:Name="SaveButton" Content="Save" Margin="5,0" Height="30" Width="70" Style="{StaticResource ActionButtonStyle}"
                        HorizontalAlignment="Right" VerticalAlignment="Center"
                            Command="{Binding Save}"/>
            </Grid>
        </Border>
        <wizard:WizardNavigationView Grid.Row="2" UtilityButtonVisibility="Collapsed" DataContext="{Binding Wizard}"
                Visibility="{Binding DataContext.Wizard, Converter={StaticResource NullOrEmptyVisibilityConverter}, RelativeSource={RelativeSource AncestorType={x:Type s:View}}}" />
    </Grid>
</s:View>