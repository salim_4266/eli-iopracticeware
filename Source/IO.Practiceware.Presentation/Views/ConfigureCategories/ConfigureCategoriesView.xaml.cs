﻿using IO.Practiceware.Presentation.ViewModels.ConfigureCategories;
using IO.Practiceware.Presentation.ViewServices.ConfigureCategories;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.ConfigureCategories
{
    /// <summary>
    /// Interaction logic for ConfigureCategoriesView.xaml
    /// </summary>
    public partial class ConfigureCategoriesView
    {
        public ConfigureCategoriesView()
        {
            InitializeComponent();
        }
    }

    public class ConfigureCategoriesViewContext : IViewContext
    {
        private readonly IConfigureCategoriesViewService _configureCategoriesViewService;
        private readonly Func<AppointmentCategoryViewModel> _createCategoryViewModel;
        private bool _isArchivedCategoriesShowing;

        #region IViewContext

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        public ConfigureCategoriesViewContext(IConfigureCategoriesViewService configureCategoriesViewService, Func<AppointmentCategoryViewModel> createCategoryViewModel)
        {
            _configureCategoriesViewService = configureCategoriesViewService;
            _createCategoryViewModel = createCategoryViewModel;

            Close = Command.Create(ExecuteClose);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            
            AddCategory = Command.Create(ExecuteAddCategory);
            SaveNewCategory = Command.Create(ExecuteSaveNewCategory).Async(() => InteractionContext);
            RemoveNewCategory = Command.Create(ExecuteRemoveNewCategory);
            AssociateAppointmentTypeToNewCategory = Command.Create<IList<AssociateAppointmentTypeArgs>>(ExecuteAssociateAppointmentTypeToNewCategory);

            SaveCategory = Command.Create<AppointmentCategoryViewModel>(ExecuteSaveCategory).Async(() => InteractionContext);
            AssociateAppointmentType = Command.Create<IList<AssociateAppointmentTypeArgs>>(ExecuteAssociateAppointmentType).Async(() => InteractionContext);

            LoadArchivedCategories = Command.Create(ExecuteLoadArchivedCategories).Async(() => InteractionContext);
            LoadAssociatedTemplates = Command.Create<AppointmentCategoryViewModel>(ExecuteLoadAssociatedTemplates).Async(() => InteractionContext);
            ArchiveCategory = Command.Create(ExecuteArchiveCategory).Async(() => InteractionContext);
            RestoreCategory = Command.Create<ArchivedAppointmentCategoryViewModel>(ExecuteRestoreCategory).Async(() => InteractionContext);

            LoadPreviouslyAssociatedAppointmentTypes = Command.Create<ArchivedAppointmentCategoryViewModel>(ExecuteLoadPreviouslyAssociatedAppointmentTypes).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            var initData = _configureCategoriesViewService.LoadInitializeData();
            AppointmentTypes = initData.AppointmentTypes.OrderBy(at => at.Name).ToExtendedObservableCollection();
            ArchivedCategories = initData.ArchivedCategories.OrderBy(c => c.Name).ToExtendedObservableCollection();
            ExistingCategories = initData.ExistingCategories.OrderBy(c => c.Name).ToExtendedObservableCollection();
            foreach (var category in ExistingCategories) category.BeginNameEdit();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        public void ExecuteRestoreCategory(ArchivedAppointmentCategoryViewModel category)
        {
            var restoredCategory = _configureCategoriesViewService.RestoreAppointmentCategory(category.Id);
            restoredCategory.BeginNameEdit();

            ArchivedCategories.Remove(category);
            ExistingCategories.Insert(0, restoredCategory);
        }

        public void ExecuteArchiveCategory()
        {
            if (CategoryToArchive != null)
            {
                IsArchivedCategoriesShowing = true;

                var appointmentTypesToClear = AppointmentTypes.Where(at => at.AppointmentCategory == CategoryToArchive).ToList();
                appointmentTypesToClear.ForEach(at => at.AppointmentCategory = null);

                var archivedCategory = _configureCategoriesViewService.ArchiveAppointmentCategory(CategoryToArchive.Id);

                ExistingCategories.Remove(CategoryToArchive);
                ArchivedCategories.Insert(0, archivedCategory);

                CategoryToArchive = null;
            }
        }

        public void ExecuteSaveCategory(AppointmentCategoryViewModel category)
        {
            if (category == null || category.Name.IsNullOrEmpty()) return;

            if (CategoryNameExists(category))
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("Category name already exists.  Please name again."));
                category.CancelNameEdit();
                return;
            }

            _configureCategoriesViewService.SaveAppointmentCategory(category);
            category.BeginNameEdit();
        }

        private void ExecuteAddCategory()
        {
            if (NewAppointmentCategory != null) return;

            var newAppointmentCategory = _createCategoryViewModel();
            var rand = new Random();
            var color = Color.FromArgb(255, (byte)rand.Next(255), (byte)rand.Next(255), (byte)rand.Next(255)); // WE ONLY WANT OPAQUE COLORS - #FF...
            newAppointmentCategory.Color = color;
            NewAppointmentCategory = newAppointmentCategory;
        }

        private void ExecuteSaveNewCategory()
        {
            if (NewAppointmentCategory == null || NewAppointmentCategory.Name.IsNullOrEmpty()) return;

            if (CategoryNameExists(NewAppointmentCategory))
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("Category name already exists.  Please name again."));
                NewAppointmentCategory.CancelNameEdit();
                return;
            }

            int newCategoryId = _configureCategoriesViewService.SaveNewAppointmentCategory(NewAppointmentCategory);
            NewAppointmentCategory.Id = newCategoryId;

            var appointmentTypesToAssociate = AppointmentTypes.Where(at => at.AppointmentCategory == NewAppointmentCategory);
            _configureCategoriesViewService.SaveAppointmentTypes(appointmentTypesToAssociate.ToList());

            NewAppointmentCategory.BeginNameEdit();
            ExistingCategories.Insert(0, NewAppointmentCategory);
            NewAppointmentCategory = null;
        }

        private void ExecuteRemoveNewCategory()
        {
            var appointmentTypesToAssociate = AppointmentTypes.Where(at => at.AppointmentCategory == NewAppointmentCategory).ToList();
            appointmentTypesToAssociate.ForEach(at => at.AppointmentCategory = null);
            NewAppointmentCategory = null;
        }

        private void ExecuteAssociateAppointmentType(IList<AssociateAppointmentTypeArgs> args)
        {
            var appointmentTypesToSave = new List<AppointmentTypeViewModel>();

            foreach (var arg in args.Where(a => a.AppointmentType.AppointmentCategory != a.Category))
            {
                var appointmentType = arg.AppointmentType;
                appointmentType.AppointmentCategory = arg.Category;
                appointmentTypesToSave.Add(appointmentType);
            }

            if (!appointmentTypesToSave.Any()) return;

            _configureCategoriesViewService.SaveAppointmentTypes(args.Select(a => a.AppointmentType).ToList());
        }

        private void ExecuteAssociateAppointmentTypeToNewCategory(IEnumerable<AssociateAppointmentTypeArgs> args)
        {
            if (NewAppointmentCategory == null) return;
            
            foreach(var arg in args)
            {
                arg.AppointmentType.AppointmentCategory = arg.AppointmentType.AppointmentCategory == null ? NewAppointmentCategory : null;
                // Defer saving the appointment types until the New Category is saved.  Appointment types may be 'unhooked' if New Category is removed rather then saved.
            }
        }

        protected void ExecuteLoadPreviouslyAssociatedAppointmentTypes(ArchivedAppointmentCategoryViewModel archivedCategory)
        {
            if (archivedCategory.PreviouslyAssociatedAppointmentTypes == null)
            {
                archivedCategory.PreviouslyAssociatedAppointmentTypes = _configureCategoriesViewService.LoadPreviouslyAssociatedAppointmentTypes(archivedCategory).OrderBy(at => at.Name).ToExtendedObservableCollection();
            }
        }

        public void ExecuteLoadAssociatedTemplates(AppointmentCategoryViewModel category)
        {
            if (category.Templates == null || category.Templates.Count == 0)
            {
                category.Templates = _configureCategoriesViewService.LoadAssociatedTemplates(category.Id).OrderBy(t => t.Name).ToExtendedObservableCollection();
            }
        }

        public void ExecuteLoadArchivedCategories()
        {
            if (ArchivedCategories == null)
            {
                ArchivedCategories = _configureCategoriesViewService.LoadArchivedCategories().OrderBy(c => c.Name).ToExtendedObservableCollection();
            }
        }

        private bool CategoryNameExists(AppointmentCategoryViewModel category)
        {
            var categoriesExceptForSelf = ExistingCategories.Concat(ArchivedCategories).Where(c => c.Id != category.Id);
            if (categoriesExceptForSelf.Any(c => c.Name.Equals(category.Name, StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }
            return false;
        }

        [DispatcherThread]
        public virtual bool IsArchivedCategoriesShowing
        {
            get { return _isArchivedCategoriesShowing; }
            set
            {
                _isArchivedCategoriesShowing = value;
                if (_isArchivedCategoriesShowing && ArchivedCategories == null)
                {
                    LoadArchivedCategories.Execute();
                }
            }
        }

        /// <summary>
        /// A newly created appointment category
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentCategoryViewModel NewAppointmentCategory { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentCategoryViewModel> ExistingCategories { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ArchivedAppointmentCategoryViewModel> ArchivedCategories { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentTypeViewModel> AppointmentTypes { get; set; }

        [DispatcherThread]
        public virtual AppointmentCategoryViewModel CategoryToArchive { get; set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to save an existing appointment category
        /// </summary>
        public ICommand SaveCategory { get; protected set; }

        /// <summary>
        /// Command to associate an appointment type to an appointment category
        /// </summary>
        public ICommand AssociateAppointmentType { get; protected set; }

        /// <summary>
        /// Command to create a new category
        /// </summary>
        public ICommand AddCategory { get; protected set; }

        /// <summary>
        /// Command to save the new category
        /// </summary>
        public ICommand SaveNewCategory { get; protected set; }

        /// <summary>
        /// Command to delete (undo) the newly created category
        /// </summary>
        public ICommand RemoveNewCategory { get; protected set; }

        /// <summary>
        /// Command to associate an appointment type to the new, unsaved, appointment category
        /// </summary>
        public ICommand AssociateAppointmentTypeToNewCategory { get; protected set; }

        /// <summary>
        /// Command to restore a category from archival
        /// </summary>
        public ICommand RestoreCategory { get; protected set; }

        /// <summary>
        /// Command to archive a category
        /// </summary>
        public ICommand ArchiveCategory { get; protected set; }

        /// <summary>
        /// Command to load the templates associated for a given category
        /// </summary>
        public ICommand LoadAssociatedTemplates { get; protected set; }

        /// <summary>
        /// Command to load all archived categories
        /// </summary>
        public ICommand LoadArchivedCategories { get; protected set; }

        /// <summary>
        /// Command to load previously associated appointment types for a given archived category
        /// </summary>
        public ICommand LoadPreviouslyAssociatedAppointmentTypes { get; protected set; }
    }

    public class AssociateAppointmentTypeArgs
    {
        public AppointmentCategoryViewModel Category { get; set; }
        public AppointmentTypeViewModel AppointmentType { get; set; }
    }
}
