﻿using IO.Practiceware.Presentation.ViewModels.ConfigureCategories;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.ConfigureCategories
{
    public class AppointmentTypesListBoxBehavior : Behavior<ListBox>
    {
        private INotifyPropertyChanged _oldValue;
        private StyleSelector _styleSelector;
        private AppointmentCategoryViewModel _category;
        public static readonly DependencyProperty AssociateAppointmentTypeProperty = DependencyProperty.Register("AssociateAppointmentType", typeof (ICommand), typeof (AppointmentTypesListBoxBehavior));
        private PropertyChangeNotifier _itemsSourceNotifier;

        public ICommand AssociateAppointmentType
        {
            get { return (ICommand) GetValue(AssociateAppointmentTypeProperty); }
            set { SetValue(AssociateAppointmentTypeProperty, value); }
        }

        public ICommand InitializeAppointmentTypeSelections
        {
            get;
            set;
        }

        public ICommand ScrollIntoView
        {
            get;
            set;
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            SetAppointmentTypeStyleSelector();
            EnableAppointmentTypeAssociation();
            InitializeAppointmentTypeSelections = Command.Create(ExecuteInitializeAppointmentTypeSelections);
            ScrollIntoView = Command.Create(ExecuteScrollIntoView);

            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourcePropertyChanged);
            
            RegisterItemsSource();
        }

        private void RegisterItemsSource()
        {
            if (AssociatedObject != null)
            {
                if (_oldValue != null)
                {
                    _oldValue.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemsSourcePropertyChanged);
                }
                var collectionChanged = AssociatedObject.ItemsSource as INotifyPropertyChanged;
                if (collectionChanged != null)
                {
                    collectionChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemsSourcePropertyChanged);
                    _oldValue = collectionChanged;
                }

                ExecuteInitializeAppointmentTypeSelections();
            }
        }

        private void OnItemsSourcePropertyChanged(object sender, EventArgs e)
        {
            RegisterItemsSource();
        }

        void OnItemsSourcePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ExecuteInitializeAppointmentTypeSelections();
        }

        private void ExecuteInitializeAppointmentTypeSelections()
        {
            IsInitializing = true;
            AssociatedObject.SelectedItems.Clear();

            var appointmentTypeList = new List<AppointmentTypeViewModel>(AssociatedObject.Items.Cast<AppointmentTypeViewModel>());

            foreach (AppointmentTypeViewModel appointmentType in appointmentTypeList)
            {
                if(appointmentType != null && appointmentType.AppointmentCategory == Category)
                {
                    AssociatedObject.SelectedItems.Add(appointmentType);
                }
            }

            IsInitializing = false;
        }

        private void ExecuteScrollIntoView()
        {
            if (!AssociatedObject.IsLoaded || AssociatedObject.Items.Count == 0) return;

            var activeWindow = System.Windows.Window.GetWindow(AssociatedObject);
            if (!IsElementVisible(AssociatedObject, activeWindow))
            {
                Dispatcher.BeginInvoke(() =>
                {
                    AssociatedObject.UpdateLayout();
                    AssociatedObject.ScrollIntoView(AssociatedObject.Items.Cast<object>().Last());
                    AssociatedObject.UpdateLayout();
                    AssociatedObject.ScrollIntoView(AssociatedObject.Items.Cast<object>().First());
                });
            }
        }

        private static bool IsElementVisible(FrameworkElement element, FrameworkElement container)
        {
            if (!element.IsVisible)
                return false;

            Rect bounds = element.TransformToAncestor(container).TransformBounds(new Rect(0.0, 0.0, element.ActualWidth, element.ActualHeight));
            var rect = new Rect(0.0, 0.0, container.ActualWidth, container.ActualHeight);
            return rect.Contains(bounds.BottomRight);
        }

        private void EnableAppointmentTypeAssociation()
        {
            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
        }

        void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsInitializing) { return; }

            var associateAppointmentTypeArgs = e.RemovedItems.Cast<AppointmentTypeViewModel>().
                                                              Select(i => new AssociateAppointmentTypeArgs {AppointmentType = i, Category = null}).ToList();
            
            associateAppointmentTypeArgs.AddRange(e.AddedItems.Cast<AppointmentTypeViewModel>().
                                                               Select(i => new AssociateAppointmentTypeArgs {AppointmentType = i, Category = Category}));

            AssociateAppointmentType.Execute(associateAppointmentTypeArgs);
        }

        private void SetAppointmentTypeStyleSelector()
        {
            _styleSelector = AssociatedObject.ItemContainerStyleSelector;

            var resourceDictionary = new ResourceDictionary {Source = new Uri("IO.Practiceware.Presentation;component/Views/ConfigureCategories/ConfigureCategoriesResources.xaml", UriKind.Relative)};
            var appointmentTypeStyle = (Style) resourceDictionary["AppointmentTypeStyle"];
            var archivedAppointmentTypeStyle = (Style) resourceDictionary["ArchivedAppointmentTypeStyle"];

            var appointmentTypeStyleSelector = new AppointmentTypeStyleSelector();
            appointmentTypeStyleSelector.Category = Category;
            appointmentTypeStyleSelector.AppointmentTypeStyle = appointmentTypeStyle;
            appointmentTypeStyleSelector.ArchivedAppointmentTypeStyle = archivedAppointmentTypeStyle;
            AssociatedObject.ItemContainerStyleSelector = appointmentTypeStyleSelector;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            RemoveAppointmentTypeStyleSelector();
            DisableAppointmentTypeAssociation();

            if(_oldValue != null)
            {
                _oldValue.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemsSourcePropertyChanged);
                _oldValue = null;
            }

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourcePropertyChanged);
        }

        private void RemoveAppointmentTypeStyleSelector()
        {
            AssociatedObject.ItemContainerStyleSelector = _styleSelector;
        }

        private void DisableAppointmentTypeAssociation()
        {
            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
        }

        public AppointmentCategoryViewModel Category
        {
            get
            {
                if (_category == null && AssociatedObject.DataContext != null)
                {
                    _category = ((AppointmentCategoryViewModel)AssociatedObject.DataContext);
                }
                return _category;
            }
        }

        public bool IsInitializing { get; set; }

        #region Helper Classes

        public class AppointmentTypeStyleSelector : StyleSelector
        {
            public override Style SelectStyle(object item, DependencyObject container)
            {
                var appointmentType = item as AppointmentTypeViewModel;
                if (appointmentType != null)
                {
                    if(appointmentType.AppointmentCategory is ArchivedAppointmentCategoryViewModel)
                    {
                        return ArchivedAppointmentTypeStyle;
                    }

                    return AppointmentTypeStyle;
                }

                return base.SelectStyle(item, container);
            }

            public AppointmentCategoryViewModel Category { get; set; }
            public Style AppointmentTypeStyle { get; set; }
            public Style ArchivedAppointmentTypeStyle { get; set; }
        }

        #endregion
    }
}
