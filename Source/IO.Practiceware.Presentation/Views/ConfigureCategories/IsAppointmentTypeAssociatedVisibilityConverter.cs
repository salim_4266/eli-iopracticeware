﻿using IO.Practiceware.Presentation.ViewModels.ConfigureCategories;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.ConfigureCategories
{
    public class IsAppointmentTypeAssociatedVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var appointmentCategory = values.OfType<AppointmentCategoryViewModel>().FirstOrDefault();
            var appointmentTypeAppointmentCategory = values.OfType<AppointmentCategoryViewModel>().Skip(1).FirstOrDefault();
            if (appointmentCategory != null)
            {
                if (appointmentTypeAppointmentCategory == null || (appointmentCategory.Id == appointmentTypeAppointmentCategory.Id))
                {
                    return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
