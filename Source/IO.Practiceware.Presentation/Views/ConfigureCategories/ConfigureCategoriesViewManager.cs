﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.ConfigureCategories
{
    public class ConfigureCategoriesViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public ConfigureCategoriesViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowConfigureCategories()
        {
            if (!PermissionId.ConfigureAppointmentCategories.EnsurePermission()) return;

            var view = new ConfigureCategoriesView();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}
