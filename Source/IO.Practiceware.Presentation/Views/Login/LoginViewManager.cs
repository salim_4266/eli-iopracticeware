﻿using System.Diagnostics;
using Soaf;
using Soaf.Presentation;
using System;
using System.Windows;
using Soaf.Presentation.Interop;
using IO.Practiceware.Services.ApplicationSettings;

namespace IO.Practiceware.Presentation.Views.Login
{
    public class LoginViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IApplicationSettingsService _applicationViewService;
        private string _consentFormURL;
        delegate void DelApplicationSettings(string str);

        public LoginViewManager(IInteractionManager interactionManager, IApplicationSettingsService applicationViewService)
        {
            _interactionManager = interactionManager;
            _applicationViewService = applicationViewService;
        }

        public int ShowLoginScreen(bool fullScreen = false)
        {
            try
            {
                if (WindowInteropUtility.GetCurrentDpi() != WindowInteropUtility.Dpi.Normal)
                {
                    _interactionManager.Alert(new DpiErrorView());
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                _interactionManager.Alert(new DpiErrorView());
            }

            var view = new LoginView();
            var dataContext = view.DataContext.EnsureType<LoginViewContext>();

            var arguments = new WindowInteractionArguments
            {
                Content = view,
                Header = "Login",
                ResizeMode = ResizeMode.CanResizeWithGrip,
                WindowState = WindowState.Normal
            };

            if (fullScreen)
            {
                arguments.WindowState = WindowState.Maximized;
                arguments.ResizeMode = ResizeMode.NoResize;
            }

            _interactionManager.ShowModal(arguments);

            return dataContext.IoUserId;
        }

        public void RedirectToConsentForms()
        {            
            DelApplicationSettings obj = AppSettings;
            IAsyncResult result = obj.BeginInvoke("ConsentFormURL", null, null);
            obj.EndInvoke(result);
            var serverDataPath = IO.Practiceware.Configuration.ConfigurationManager.ServerDataPath;
            System.Diagnostics.Process.Start(ConsentFormURL + serverDataPath);
        }


        void AppSettings(string text)
        {
            var applicationService = _applicationViewService.GetSetting<string>(text);
            ConsentFormURL = applicationService != null ? applicationService.Value : string.Empty;
        }

        public string ConsentFormURL
        {
            get
            {
                return _consentFormURL;
            }
            set { _consentFormURL = value; }
        }
    }
}
