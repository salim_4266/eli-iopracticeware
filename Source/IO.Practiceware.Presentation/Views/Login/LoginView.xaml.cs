﻿using System.IO;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Logging;
using IO.Practiceware.Presentation.ViewModels.Login;
using IO.Practiceware.Presentation.ViewServices.Login;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace IO.Practiceware.Presentation.Views.Login
{
  
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView
    {
        public LoginView()
        {
            InitializeComponent();
        }






    }

    public class LoginViewContext : IViewContext
    {
        private readonly Func<LoginViewModel> _createLoginViewModel;
        private readonly ILoginViewService _loginViewService;
        private readonly IInteractionManager _interactionManager;

        public LoginViewContext(Func<LoginViewModel> createLoginViewModel, ILoginViewService loginViewService,IInteractionManager interactionManager)
        {
            _createLoginViewModel = createLoginViewModel;
            _loginViewService = loginViewService; 
            Load = Command.Create(ExecuteLoad);
            AddDigit = Command.Create<String>(ExecuteAddDigit);
            RemoveDigit = Command.Create(ExecuteRemoveDigit);
            Close = Command.Create(ExecuteClose);
            Done = Command.Create(ExecuteDone, () => LoginViewModel.IsValid()).Async(() => InteractionContext);
            _interactionManager = interactionManager;
        }

        private void ExecuteDone()
        {
            LoginErrorMessage = null;
            var errorMessages = LoginViewModel.Validate();

            if (LoginViewModel.RemoteFileService && LoginViewModel.IsApplicationServerChecked)
                LoginViewModel.ServerDataPath = ConfigurationManager.ServerDataPath;

            if (!LoginViewModel.RemoteFileService && !ValidatePath(LoginViewModel.ServerDataPath)) { return; }

            // pop up an alert box displaying these messages
            ValidationErrorMessages = errorMessages.Count > 0 ? errorMessages.Select(x => x.Value).Distinct().Join(Environment.NewLine) : "";

            if (errorMessages.IsNotNullOrEmpty())
            {
                return;
            }

            // Save entered configuration
            SaveConfiguration();

            // Run updater now (to support scenarios when client is not compatible with server)
            RunClientUpdate();

            try
            {
                // only want to TryOpenCurrentSqlConnection if user entered Sql mode.  
                if (!ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled
                    && !ConfigurationManager.PracticeRepositoryConnectionString.IsNullOrEmpty()
                    && (ConfigurationManager.PracticeRepositoryConnectionString != ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey))
                {
                    _loginViewService.TryOpenCurrentSqlConnection();
                }

                // do login 
                IoUserId = UserContext.Current.Login(LoginViewModel.PersonalId);
                if (IoUserId > 0)
                {
                    InteractionContext.Complete(true);
                    return;
                }
                else
                {
                    ValidationErrorMessages = null;
                    if (IoUserId == -1)
                    {
                        LoginErrorMessage = "* Your login failed.  Please make sure you've entered the correct Personal Id and try again.";
                        LoginViewModel.PersonalId = string.Empty;
                    }
                    else if (IoUserId == -2)
                    {
                        _interactionManager.Alert("Your are not Authenticated to use IO, For more help please Contact IO Billing");
                    }
                    else if (IoUserId == -3)
                    {
                        _interactionManager.Alert("Your Authentication token is Invalid, Please Contact IO Support");
                    }
                    else if (IoUserId == -4)
                    {
                        _interactionManager.Alert("There is some problem in login, please Contact IO Support");
                    }
                }
            }
            catch (ClientServerAssemblyMismatchException csEx)
            {
                InteractionManager.Current.Alert(csEx.Message);
                return;
            }
            catch (Exception ex)
            {
                var webEx = ex.SearchFor<System.Net.WebException>();
                if (webEx != null && webEx.Message.StartsWith("The remote name could not be resolved", StringComparison.OrdinalIgnoreCase))
                {
                    Trace.TraceError(ex.ToString());
                    ValidationErrorMessages = "The application server you've specified cannot be reached.  Please enter a valid server or wait a few minutes and try again.";
                    return;
                }

                if (webEx != null)
                {
                    Trace.TraceError(ex.ToString());
                    ValidationErrorMessages = "There was an issue connecting to the specified application server.  Please wait a few minutes and try again.  If the problem persists, contact your system administrator.";
                    return;
                }

                var sqlEx = ex.SearchFor<SqlException>();
                if (sqlEx != null)
                {
                    LoggingConfigurationExtensions.TraceError(ex.ToString(), true);
                    ValidationErrorMessages = String.Format("An error occurred while trying to connect to the database.  Please make sure your connection information is correct and try again.  Message: {0}", sqlEx.Message);
                    return;
                }

                var loginEx = ex.SearchFor<InactiveUserLoginException>();
                if (loginEx != null)
                {
                    Trace.TraceError(ex.ToString());
                    ValidationErrorMessages = loginEx.Message;
                    return;
                }

                Trace.TraceError(ex.ToString());
                ValidationErrorMessages = "There was an issue while trying to login.  Please make sure you've entered the correct information and try again.  If the problem persists, contact your system administrator";
            }
        }

        /// <summary>
        /// Validate the path exist.
        /// </summary>
        /// <param name="path">path to validate</param>
        /// <returns></returns>
        private bool ValidatePath(string path)
        {

            if (string.IsNullOrEmpty(path) || !FileSystem.IsValidFilePath(path) || !new DirectoryInfo(path).Exists)
            {
                LoginErrorMessage = "Your login failed. Please make sure you've entered the correct Server data path and try again.";
                return false;
            }

            return true;
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteLoad()
        {
            InteractionContext.Completing += delegate { LoginViewModel = null; };

            IoUserId = -1;

            var loginViewModel = _createLoginViewModel();

            loginViewModel.PersonalId = string.Empty;

            loginViewModel.ApplicationServerName = ConfigurationManager.ApplicationServerClientConfiguration.HostName;
            loginViewModel.RemoteFileService = ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService;

            if (!ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled)
            { // IsEnabled Property in Configuration is set to false or doesn't exist

                // here we should check the Connection String configuration to see if its set --> Because upon initial installation, there will be no connection string
                // and the IsEnabled property will be set to false for ApplicationServerClient.  So we assume we're in Direct Sql Connect mode, and put a PlaceHolder
                // ConnectionString in until the user enters a real one.
                SqlConnectionStringBuilder scb;
                if (ConfigurationManager.PracticeRepositoryConnectionString.IsNullOrEmpty()
                    || ConfigurationManager.PracticeRepositoryConnectionString.Equals(ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey, StringComparison.OrdinalIgnoreCase)
                    || !Soaf.Data.DbConnections.TryGetConnectionStringBuilder(ConfigurationManager.PracticeRepositoryConnectionString, out scb))
                {
                    loginViewModel.SqlConnectionString = new SqlConnectionStringBuilder(ConfigurationManager.DefaultConnectionStringFormat);
                }
                else
                {
                    loginViewModel.SqlConnectionString = scb;
                }
            }

            if (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled)
            {
                loginViewModel.IsApplicationServerChecked = true;
            }
            else
            {
                loginViewModel.IsSqlServerChecked = true;
            }
            loginViewModel.ServerDataPath = ConfigurationManager.ServerDataPath;
            loginViewModel.ApplicationServerToken = ConfigurationManager.ApplicationServerClientConfiguration.AuthenticationToken;
            
            LoginViewModel = loginViewModel;
        }

        private void ExecuteRemoveDigit()
        {
            if (LoginViewModel.PersonalId.Length > 0)
            {
                LoginViewModel.PersonalId = LoginViewModel.PersonalId.Remove(LoginViewModel.PersonalId.Length - 1);
            }
        }

        private void ExecuteAddDigit(string digit)
        {
            if (LoginViewModel.PersonalId.Length < 4)
            {
                LoginViewModel.PersonalId += digit;
            }
        }

        public IInteractionContext InteractionContext
        {
            get;
            set;
        }

        private void SaveConfiguration()
        {
            if (LoginViewModel.IsApplicationServerChecked)
            {
                ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = true;
                ConfigurationManager.PracticeRepositoryConnectionString = ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey; // remove the connection string and set it to a default value
                ConfigurationManager.AuthenticationToken = LoginViewModel.ApplicationServerToken;
                ConfigurationManager.ApplicationServerClientConfiguration.HostName = LoginViewModel.ServerName;
                ConfigurationManager.ApplicationServerClientConfiguration.RemoteFileService = LoginViewModel.RemoteFileService;
            }
            else
            {
                ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled = false;
                ConfigurationManager.PracticeRepositoryConnectionString = LoginViewModel.SqlConnectionString.ToString();
            }

            ConfigurationManager.ServerDataPath = LoginViewModel.ServerDataPath;

            ConfigurationManager.Save();
        }

        private static void RunClientUpdate()
        {
            var isNoUpdate = Environment.GetCommandLineArgs()
                .Any(a => string.Equals(a, "--noupdate", StringComparison.InvariantCultureIgnoreCase));
            var updaterPath = Path.Combine(ConfigurationManager.ApplicationPath, "IOPUpdater.exe");

            // Check whether we should run update and that updater is present
            if (isNoUpdate || !File.Exists(updaterPath))
            {
                return;
            }

            // Run updater and pass current process
            var updaterArgs = Process.GetCurrentProcess().ProcessName + ".exe";
            Process.Start(updaterPath, updaterArgs);
        }

        public ICommand Load { get; protected set; }
        public ICommand AddDigit { get; protected set; }
        public ICommand RemoveDigit { get; protected set; }
        public ICommand Close { get; protected set; }
        public ICommand Done { get; protected set; }

        public virtual LoginViewModel LoginViewModel { get; set; }

        public int IoUserId { get; set; }


        [DispatcherThread]
        public virtual string ValidationErrorMessages { get; set; }

        [DispatcherThread]
        public virtual string LoginErrorMessage { get; set; }
    }
}
