﻿using System.Diagnostics;
using System.Windows.Navigation;

namespace IO.Practiceware.Presentation.Views.Login
{
    /// <summary>
    /// Interaction logic for DpiErrorView.xaml
    /// </summary>
    public partial class DpiErrorView
    {
        public DpiErrorView()
        {
            InitializeComponent();
        }

        private void HyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }

}
