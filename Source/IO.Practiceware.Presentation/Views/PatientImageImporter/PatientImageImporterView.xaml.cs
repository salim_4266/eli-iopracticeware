﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientImageImporter;
using IO.Practiceware.Presentation.ViewServices.PatientImageImporter;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Storage;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.PatientImageImporter
{
    /// <summary>
    ///   Interaction logic for PatientImageImporterView.xaml
    /// </summary>
    public partial class PatientImageImporterView
    {
        public PatientImageImporterView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   View DataContext for patient image importer.
    /// </summary>
    public class PatientImageImporterViewContext : IViewContext
    {
        private readonly DispatcherTimer _dispatcherTimer;
        private readonly IPatientImageImporterViewService _viewService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        readonly Func<PatientImageImporterCriteriaViewModel> _createImageImporterCriteriaViewModel;

        public PatientImageImporterViewContext(
            IPatientImageImporterViewService viewService,
            IApplicationSettingsService applicationSettingsService,
            Func<PatientImageImporterCriteriaViewModel> createImageImporterCriteriaViewModel)
        {
            _viewService = viewService;
            _applicationSettingsService = applicationSettingsService;
            _createImageImporterCriteriaViewModel = createImageImporterCriteriaViewModel;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);

            Run = Command.Create(RunImporter, () => IsValid && !IsRunning).Async(() => InteractionContext);

            Cancel = Command.Create(CancelImporter, () => IsRunning).Async(() => InteractionContext);

            SaveLog = Command.Create<Tuple<string, object>>(t =>
                                                            {
                                                                var s = t.Item1;
                                                                PatientImageImporterJobViewModel job = ActiveJob;
                                                                lock (((ICollection)job.Errors).SyncRoot)
                                                                {
                                                                    if (s != null) FileManager.Instance.CommitContents(s, job.Errors.Join(Environment.NewLine));
                                                                }
                                                            }, s => ActiveJob != null);

            OnClosing = Command.Create<Action>(ExecuteClosing).AsyncForClosing(() => InteractionContext);

            // update active job on timer.
            _dispatcherTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            _dispatcherTimer.Tick += UpdateActiveJob;
            _dispatcherTimer.Start();
        }

        /// <summary>
        ///   Gets or sets the criteria for the job.
        /// </summary>
        /// <value> The criteria. </value>
        [DispatcherThread]
        public virtual PatientImageImporterCriteriaViewModel Criteria { get; protected set; }

        /// <summary>
        ///   Gets or sets the list of available index types.
        /// </summary>
        /// <value> The index types. </value>
        [DispatcherThread]
        public virtual ObservableCollection<PatientImageImporterIndexTypeViewModel> IndexTypes { get; protected set; }

        /// <summary>
        ///   Gets or sets the type of the selected index data to use for the import.
        /// </summary>
        /// <value> The type of the selected index. </value>
        [DispatcherThread]
        public virtual PatientImageImporterIndexTypeViewModel SelectedIndexType { get; set; }

        /// <summary>
        ///   Runs the importer.
        /// </summary>
        /// <value> The run. </value>
        public virtual ICommand Run { get; protected set; }

        /// <summary>
        ///   Cancels the active job.
        /// </summary>
        /// <value> The cancel. </value>
        public virtual ICommand Cancel { get; protected set; }

        /// <summary>
        ///   Loads data on startup.
        /// </summary>
        /// <value> The load. </value>
        public virtual ICommand Load { get; protected set; }

        /// <summary>
        ///   Saves the log file.
        /// </summary>
        /// <value> The save log. </value>
        public virtual ICommand SaveLog { get; protected set; }


        public virtual ICommand OnClosing { get; protected set; }

        /// <summary>
        ///   Gets a value indicating whether there is a job running
        /// </summary>
        /// <value> <c>true</c> if this instance is running; otherwise, <c>false</c> . </value>
        [DependsOn("ActiveJob")]
        public virtual bool IsRunning
        {
            get { return ActiveJob != null && ActiveJob.Status == PatientImageImporterJobStatus.Running; }
        }

        /// <summary>
        ///   Clears the active job log.
        /// </summary>
        /// <value> The active job. </value>
        [DispatcherThread]
        public virtual PatientImageImporterJobViewModel ActiveJob { get; protected set; }

        /// <summary>
        ///   Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value> <c>true</c> if this instance is valid; otherwise, <c>false</c> . </value>
        private bool IsValid
        {
            get { return Criteria.IsValid() && SelectedIndexType != null && SelectedIndexType.IsValid(); }
        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        /// <summary>
        ///   Loads required data for the view.
        /// </summary>
        private void ExecuteLoad()
        {
            var indexTypes = _viewService.GetIndexTypes().ToList();
            var toSelect = indexTypes.FirstOrDefault();

            var criteriaSetting = _applicationSettingsService.GetSetting<PatientImageImporterCriteriaViewModel>(ApplicationSetting.PatientImageImporterCriteria);

            Criteria = criteriaSetting != null ? criteriaSetting.Value : _createImageImporterCriteriaViewModel();

            var indexTypeSetting = _applicationSettingsService.GetSetting<PatientImageImporterIndexTypeViewModel>(ApplicationSetting.PatientImageImporterIndexType);

            if (indexTypeSetting != null && indexTypeSetting.Value != null)
            {
                PatientImageImporterIndexTypeViewModel matchingIndexType = indexTypes.FirstOrDefault(t => t.GetType() == indexTypeSetting.Value.GetType());

                if (matchingIndexType != null)
                {
                    indexTypes.Replace(matchingIndexType, indexTypeSetting.Value);
                    toSelect = indexTypeSetting.Value;
                }
            }

            IndexTypes = indexTypes.ToExtendedObservableCollection();
            SelectedIndexType = toSelect;
        }

        private void ExecuteClosing(Action cancelClosing)
        {
            var criteriaSettings = new List<ApplicationSettingContainer<PatientImageImporterCriteriaViewModel>>
                {
                    new ApplicationSettingContainer<PatientImageImporterCriteriaViewModel>
                        {
                            MachineName = ApplicationContext.Current.ComputerName,
                            Name =ApplicationSetting.PatientImageImporterCriteria,
                            Value = Criteria ?? _createImageImporterCriteriaViewModel()
                        }
                };
            Task.Factory.StartNewWithCurrentTransaction(() => _applicationSettingsService.SetSettings(criteriaSettings));

            if (SelectedIndexType != null)
            {
                // Save index settings
                // TODO: shouldn't settings be deleted when SelectedIndexType == null ?
                // TODO: why not use a single view model for storing both settings?
                var indexSettings = new List<ApplicationSettingContainer<PatientImageImporterIndexTypeViewModel>>
                    {
                        new ApplicationSettingContainer<PatientImageImporterIndexTypeViewModel>
                            {
                                MachineName = ApplicationContext.Current.ComputerName,
                                Name =ApplicationSetting.PatientImageImporterIndexType,
                                Value = SelectedIndexType
                            }
                    };
                Task.Factory.StartNewWithCurrentTransaction(() => _applicationSettingsService.SetSettings(indexSettings));
            }

            if (ActiveJob != null) CancelImporter();

            _dispatcherTimer.Stop();
            _dispatcherTimer.Tick -= UpdateActiveJob;
        }

        /// <summary>
        ///   Updates the active job status.
        /// </summary>
        protected virtual void UpdateActiveJob(object sender, EventArgs e)
        {
            PatientImageImporterJobViewModel job = ActiveJob;

            if (job != null && IsRunning)
            {
                _dispatcherTimer.Stop();

                Task.Factory.StartNewWithCurrentTransaction(
                    () =>
                    {
                        ActiveJob = null;
                        ActiveJob = _viewService.GetJobStatus(job.Id);
                        _dispatcherTimer.Start();
                    });
            }
        }

        /// <summary>
        ///   Cancels the importer job actively running.
        /// </summary>
        private void CancelImporter()
        {
            if (ActiveJob != null)
            {
                _viewService.Cancel(ActiveJob.Id);
            }
        }

        /// <summary>
        ///   Runs the importer.
        /// </summary>
        private void RunImporter()
        {
            Guid jobId = _viewService.Import(Criteria, SelectedIndexType);
            ActiveJob = _viewService.GetJobStatus(jobId);
        }
    }
}