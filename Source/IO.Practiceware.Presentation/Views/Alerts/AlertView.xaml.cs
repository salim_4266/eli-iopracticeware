﻿

using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Alerts
{
    /// <summary>
    /// Interaction logic for AlertView.xaml
    /// </summary>
    public partial class AlertView 
    {
        public AlertView()
        {
            InitializeComponent();
        }
    }

    public class AlertViewContextReference : DataContextReference
    {
        public new AlertViewContext DataContext
        {
            get { return (AlertViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
