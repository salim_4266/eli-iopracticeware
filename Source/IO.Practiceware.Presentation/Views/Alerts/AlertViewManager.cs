﻿using System.Windows;
using IO.Practiceware.Presentation.ViewModels.Notes;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Alerts
{
    public class AlertViewManager
    {
        private readonly IInteractionManager _interactionManager;
        public AlertViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        /// <summary>
        /// Shows the alert.
        /// </summary>
        /// <param name="alertLoadArguments">The alert load arguments.</param>
        public void ShowAlerts(NoteLoadArguments alertLoadArguments)
        {
            var view = new AlertView();
            var argument = view.DataContext.EnsureType<AlertViewContext>();
            argument.LoadArguments = alertLoadArguments;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                          {
                                              Content = view,
                                              WindowState = WindowState.Normal,
                                              ResizeMode = ResizeMode.CanResizeWithGrip
                                          });
        }

    }


}
