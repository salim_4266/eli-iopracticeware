﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Notes;
using IO.Practiceware.Presentation.ViewServices.Alerts;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Alerts
{

    public class AlertViewContext : IViewContext
    {
        #region Private Member
        private readonly NoteViewManager _noteViewManager;
        private readonly IAlertViewService _alertViewService;
        private bool _stopExitingWindow;
        #endregion

        #region Properties
        [DispatcherThread]
        public virtual ObservableCollection<NoteViewModel> Alerts { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public ICommand LaunchNote { get; protected set; }

        public ICommand Load { get; protected set; }

        public ICommand Update { get; protected set; }

        public ICommand Deactivate { get; protected set; }

        public NoteLoadArguments LoadArguments { get; set; }

        public virtual ObservableCollection<NamedViewModel> AlertableScreens { get; set; }

        public virtual bool ConfirmExitWithUnsavedChangesTrigger { get; set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }

        public ICommand Close { get; protected set; }

        #endregion

        #region Constructor
        public AlertViewContext(NoteViewManager noteViewManager, IAlertViewService alertViewService)
        {
            _noteViewManager = noteViewManager;
            _alertViewService = alertViewService;

            LaunchNote = Command.Create<NoteViewModel>(ExecuteLaunchNote);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Update = Command.Create(ExecuteUpdate).Async(() => InteractionContext);
            Deactivate = Command.Create(ExecuteDeactivateAll).Async(() => InteractionContext);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
            Close = Command.Create(() => InteractionContext.Complete(true));
        }

        #endregion

        #region Private Methods
        private void ExecuteCancelExitWithUnsavedChanges()
        {
            _stopExitingWindow = true;
        }

        private void ExecuteDeactivateAll()
        {
            DeactivateAllAlerts();
            InteractionContext.Complete(true);
        }

        /// <summary>
        /// To deactivate all the alerts.
        /// </summary>
        protected internal void DeactivateAllAlerts()
        {
            var alertIds = Alerts.Select(x => x.AlertId.GetValueOrDefault().EnsureNotDefault()).ToList();
            _alertViewService.DeactivateAlerts(alertIds);
        }
        private void ExecuteLoad()
        {
            var alertInformation = _alertViewService.LoadAlerts(LoadArguments);
            Alerts = alertInformation.Notes;
            AlertableScreens = alertInformation.AlertableScreens;
            Alerts.ForEach(x => x.As<IEditableObjectExtended>().InitiateCustomEdit());
            InteractionContext.Completing += new EventHandler<CancelEventArgs>(InteractionContextCompleting).MakeWeak();
        }

        private void InteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (HasChange())
            {
                ConfirmExitWithUnsavedChangesTrigger = true;
                e.Cancel = _stopExitingWindow;
                _stopExitingWindow = false;
            }
        }

        private bool HasChange()
        {
            if (Alerts != null && (Alerts.Any(x => x.As<IEditableObjectExtended>().IsChanged)))
            {
                return true;
            }
            return false;
        }

        private void ExecuteLaunchNote(NoteViewModel note)
        {
            var loadArgument = new NoteLoadArguments();
            loadArgument.NoteType = note.NoteType;
            loadArgument.NoteId = note.Id;
            _noteViewManager.LaunchNoteView(loadArgument);
            Load.Execute();
        }

        private void ExecuteUpdate()
        {
            if (HasChange())
            {
                //Define separate method to support the unit testing without closing the screen.
                UpdateAlerts();
            }

            InteractionContext.Complete(true);
        }

        protected internal void UpdateAlerts()
        {
            var alertToBeUpdated = Alerts.Where(x => x.As<IEditableObjectExtended>().IsChanged).ToArray();
            _alertViewService.UpdateAlerts(alertToBeUpdated);
            Alerts.ForEach(x => x.As<IEditableObjectExtended>().AcceptCustomEdits());
        }

        #endregion
    }
}
