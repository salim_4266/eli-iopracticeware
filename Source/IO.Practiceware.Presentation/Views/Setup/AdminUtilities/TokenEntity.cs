﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Setup.AdminUtilities
{
    public class RequestTokenEntity
    {
        public string UserName { get; set; }
        public string BaseURL { get; set; }
        public string Role { get; set; }
        public string ProductKey { get; set; }
        public string PairKey { get; set; }

        public RequestTokenEntity(string SelectedUserName, string UserType)
        {
            DataTable dtTemp = new DataTable();
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                string _selectQry = "ACI_GET_JSON_DEFAULTS";
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtTemp.Load(reader);
                        if (dtTemp.Rows.Count > 0)
                        {
                            this.UserName = SelectedUserName;
                            this.BaseURL = dtTemp.Rows[0]["ACI_Dashboard_BaseUrl"].ToString();
                            if (UserType == "D")
                            {
                                this.Role = "Eligible Clinician";
                            }
                            else
                            {
                                this.Role = "Administrator";
                            }
                            this.ProductKey = dtTemp.Rows[0]["ACI_ProductKey"].ToString();
                            this.PairKey = dtTemp.Rows[0]["ACI_PairKey"].ToString();
                        }
                    }
                }
                dbConnection.Close();
            }
        }
    }

    public class ResponseTokenEntity
    {
        public string TokenStatus { get; set; }
        public string TokenDetail { get; set; }
    }
}
