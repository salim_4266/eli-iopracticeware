﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Presentation.Views.AuditLog;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientImageImporter;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using LogCategory = IO.Practiceware.Logging.LogCategory;
using IO.Practiceware.Presentation.Views.ClinicalDataFiles;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Utilities;
using Soaf.Logging;
using KeyChart.WinAdmin;
using System.Data;
using System.Net;

namespace IO.Practiceware.Presentation.Views.Setup.AdminUtilities
{
    public class AdminUtilitiesViewContext : IViewContext, IApplicationSetupViewContext, ICommonViewFeatureHandler
    {
        private readonly IInteractionManager _interactionManager;
        readonly AuditLogViewManager _auditLogViewManager;
        readonly ClinicalDataFilesViewManager _clinicalDataFilesViewManager;
        private ILogger _logger;
        private readonly AdminUtilitiesViewManager _adminUtilitiesViewManager;
        private readonly Utilities.UtilitiesViewManager _utilitiesViewManager;
        private bool _ClinicalDecisionAccessPermission { get; set; }
        private bool _RCPDashBoardAccessPermission { get; set; }

        public AdminUtilitiesViewContext(ICommonViewFeatureExchange exchange,
            IInteractionManager interactionManager,
            AuditLogViewManager auditLogViewManager,
            ClinicalDataFilesViewManager clinicalDataFilesViewManager,
            ILogManager logManager,
            AdminUtilitiesViewManager adminUtilitiesViewManager,
            Utilities.UtilitiesViewManager utilitiesViewManager)
        {
            // Report and init supported features
            ViewFeatureExchange = exchange;
            _auditLogViewManager = auditLogViewManager;
            _interactionManager = interactionManager;
            _clinicalDataFilesViewManager = clinicalDataFilesViewManager;
            _logger = logManager.GetCurrentTypeLogger();
            _adminUtilitiesViewManager = adminUtilitiesViewManager;
            _utilitiesViewManager = utilitiesViewManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            AuditLogs = Command.Create(ExecuteAuditLogs);
            DataIntegrity = Command.Create(ExecuteDataIntegrity);
            Encryption = Command.Create(ExecuteEncryption);
            FileConversion = Command.Create(ExecuteFileConversion);
            ImportPatientImages = Command.Create(ExecuteImportPatientImages);
            ImportSPEXUPCs = Command.Create(ExecuteImportSPEXUPCs);
            MachineConfiguration = Command.Create(ExecuteMachineConfiguration);
            RequeueClaims = Command.Create(ExecuteRequeueClaims);
            RequeueRecalls = Command.Create(ExecuteRequeueRecalls);
            EditTemplates = Command.Create(ExecuteEditTemplates);
            NTP = Command.Create(ExecuteNTP);
            ACIDashboard = Command.Create(ExcuteACIDashBoard);
            RCPConfiguration = Command.Create(ExcuteRCPConfiguration);
            UpdoxConfiguration= Command.Create(ExcuteUpdoxConfiguration);
            DrFirstConfiguration = Command.Create(ExcuteDrFirstConfiguration);
            ChangeIncentivePrograms = Command.Create(OnIncentiveProgramsSelectionChanged);

            exchange.Refresh = OnRefresh;
            Close = Command.Create(ExecuteClose);
        }

        private void ExecuteEditTemplates()
        {
            _utilitiesViewManager.ShowEditTemplates();
        }

        private void ExecuteNTP()
        {
            NTPTimeTest objNTPTimeTest = new NTPTimeTest();
            objNTPTimeTest.ShowDialog();
        }
        private void ExcuteUpdoxConfiguration()
        {
            if (_RCPDashBoardAccessPermission)
            {
                UtilitiesViewManager.OpenUpdoxConfiguration();
            }
            else
            {
                _interactionManager.Alert("You (" + UserContext.Current.UserDetails.DisplayName + ") are not authorized to configure.");
            }
        }

        private void ExcuteDrFirstConfiguration()
        {
            if (_RCPDashBoardAccessPermission)
            {
                UtilitiesViewManager.OpenDrFirstConfiguration();
            }
            else
            {
                _interactionManager.Alert("You (" + UserContext.Current.UserDetails.DisplayName + ") are not authorized to configure.");
            }
        }

        private void ExcuteRCPConfiguration()
        {
            if (_RCPDashBoardAccessPermission)
            {
                UtilitiesViewManager.OpenRCPConfiguration();
            }
            else
            {
                _interactionManager.Alert("You (" + UserContext.Current.UserDetails.DisplayName + ") are not authorized to configure.");
            }
        }
        private void ExcuteACIDashBoard()
        {
            string[] ResourceDetail = null;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select ResourceFirstName + ' ' + ResourceLastName + ',' + ResourceType as ResourceDetail From dbo.Resources with(nolock) Where ResourceId = " + UserContext.Current.UserDetails.Id;
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    ResourceDetail = cmd.ExecuteScalar().ToString().Split(',');
                }
                dbConnection.Close();
            }

            RequestTokenEntity objToken = new RequestTokenEntity(ResourceDetail[0], ResourceDetail[1]);
            if (objToken.UserName != "")
            {
                string result = string.Empty;
                string apiUrl = "https://portal.iopracticeware.com/api/ACI/GenerateToken/";
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objToken);
                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;
                try
                {
                    result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                    ResponseTokenEntity objResponseEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTokenEntity>(result);
                    if (objResponseEntity.TokenStatus == "1")
                    {
                        string tempURL = objToken.BaseURL + "?" + "TokenValue=" + objResponseEntity.TokenDetail + "&ProductKey=" + objToken.ProductKey;
                        System.Diagnostics.Process.Start(tempURL);
                    }
                    else
                    {
                        MessageBox.Show(objResponseEntity.TokenDetail);
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                }
            }
        }

        #region private functions

        private void OnRefresh()
        {
            Load.Execute();
        }

        private void ExecuteLoad()
        {
            LoadProgramIncentiveList();
            _ClinicalDecisionAccessPermission = false;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select CDSConfiguration From dbo.CDS_UserPermissions Where ResourceId = " + UserContext.Current.UserDetails.Id;
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    _ClinicalDecisionAccessPermission = Convert.ToBoolean(cmd.ExecuteScalar());
                }
                dbConnection.Close();
            }
            _RCPDashBoardAccessPermission = false;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "SELECT 1 AS RCPDashboardAccess  FROM dbo.Resources WHERE ResourceType='o' AND ResourceId = " + UserContext.Current.UserDetails.Id;
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    _RCPDashBoardAccessPermission = Convert.ToBoolean(cmd.ExecuteScalar());
                }
                dbConnection.Close();
            }
        }
      
        private void LoadProgramIncentiveList()
        {
            IncentiveProgramDataLists = new ObservableCollection<string>();
            IncentiveProgramDataLists.Add(AdminUtilityFeature.ClinicalDecisions);
            IncentiveProgramDataLists.Add(AdminUtilityFeature.MeaningfulUseCalculator);
            IncentiveProgramDataLists.Add(AdminUtilityFeature.PQRS);
            IncentiveProgramDataLists.Add(AdminUtilityFeature.QRDAExport);
            IncentiveProgramDataLists.Add(AdminUtilityFeature.QRDATotals);
        }


        private void OnIncentiveProgramsSelectionChanged()
        {
            if (SelectedIncentiveProgram != null)
            {
                if (SelectedIncentiveProgram == AdminUtilityFeature.ClinicalDecisions)
                {
                    if (_ClinicalDecisionAccessPermission)
                    {
                        UtilitiesViewManager.OpenClinDesc();
                    }
                    else
                    {
                        _interactionManager.Alert("You (" + UserContext.Current.UserDetails.DisplayName + ") are not authorized to configure the intervention.");
                    }
                }
                else if (SelectedIncentiveProgram == AdminUtilityFeature.MeaningfulUseCalculator)
                {
                    UtilitiesViewManager.OpenCms();
                }
                else if (SelectedIncentiveProgram == AdminUtilityFeature.PQRS)
                {
                    UtilitiesViewManager.OpenPqrs();
                }
                else if (SelectedIncentiveProgram == AdminUtilityFeature.QRDAExport)
                {
                    _clinicalDataFilesViewManager.ShowGenerateQrdaCategory1View();
                }
                else if (SelectedIncentiveProgram == AdminUtilityFeature.QRDATotals)
                {
                    _clinicalDataFilesViewManager.ShowGenerateQrdaTotalsView();
                }

            }
        }

        private void ExecuteRequeueRecalls()
        {
            UtilitiesViewManager.OpenRequeueRecalls();
        }

        private void ExecuteRequeueClaims()
        {
            UtilitiesViewManager.OpenRequeueClaims();
        }

        private void ExecuteMachineConfiguration()
        {
            _adminUtilitiesViewManager.ShowMachineConfiguration();
        }

        private void ExecuteImportSPEXUPCs()
        {
            UtilitiesViewManager.OpenSpexupcImport();
        }

        private void ExecuteImportPatientImages()
        {
            var view = new PatientImageImporterView();
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view });
        }

        private void ExecuteDataIntegrity()
        {
            bool _AccessPermission = false;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                string _selectQry = "Select ResourceType From dbo.Resources with(nolock) Where ResourceId = " + UserContext.Current.UserDetails.Id;
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    if (Convert.ToString(cmd.ExecuteScalar()).ToLower() == "d")
                    {
                        _AccessPermission = true;
                    }
                }
                dbConnection.Close();
            }
            if (_AccessPermission)
            {
                UtilitiesViewManager.OpenIntegrity();
            }
            else
            {
                MessageBox.Show("Sorry, Don't have Permission to access this module");
            }
        }

        private void ExecuteAuditLogs()
        {
            _logger.Log("Opened Audit Log Interface", categories: new[] { LogCategory.AuditLog });
            _auditLogViewManager.ShowAuditLogView();
        }

        private void ExecuteEncryption()
        {
            UtilitiesViewManager.OpenEncryption();
        }

        private void ExecuteFileConversion()
        {
            UtilitiesViewManager.OpenFileconversion();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        #endregion

        #region commands
        public ICommand AuditLogs { get; set; }
        public ICommand DataIntegrity { get; set; }
        public ICommand Encryption { get; set; }
        public ICommand FileConversion { get; set; }
        public ICommand ImportPatientImages { get; set; }
        public ICommand ImportSPEXUPCs { get; set; }
        public ICommand MachineConfiguration { get; set; }
        public ICommand RequeueClaims { get; set; }
        public ICommand RequeueRecalls { get; set; }
        public ICommand EditTemplates { get; set; }
        public ICommand NTP { get; set; }
        public ICommand ACIDashboard { get; set; }
        public ICommand RCPConfiguration { get; set; }
        public ICommand UpdoxConfiguration { get; set; }
        public ICommand DrFirstConfiguration { get; set; }
        public ICommand ChangeIncentivePrograms { get; protected set; }
        public ICommand Close { get; protected set; }
        #endregion

        public virtual IInteractionContext InteractionContext { get; set; }
        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
        public virtual string SelectedIncentiveProgram { get; set; }
        public virtual ObservableCollection<string> IncentiveProgramDataLists { get; set; }
        public ICommand Load { get; set; }
    }

}
