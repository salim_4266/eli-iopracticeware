﻿using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IO.Practiceware.Presentation.Views.Setup.AdminUtilities
{
    /// <summary>
    /// Interaction logic for AdminUtilitiesView.xaml
    /// </summary>
    public partial class AdminUtilitiesView 
    {
        public AdminUtilitiesView()
        {
            InitializeComponent();
        }
    }

    public class AdminUtilitiesViewContextReference : DataContextReference
    {
        public new AdminUtilitiesViewContext DataContext
        {
            get { return (AdminUtilitiesViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
