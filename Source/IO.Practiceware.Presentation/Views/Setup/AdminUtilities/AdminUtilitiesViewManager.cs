﻿using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Views.Setup.AdminUtilities
{
    [Singleton]
    public class AdminUtilitiesViewManager
    {
        public event EventHandler OpenMachineConfiguration;

        public void ShowMachineConfiguration()
        {
            if (OpenMachineConfiguration != null)
            {
                OpenMachineConfiguration(this, null);
            }
        }
    }
}
