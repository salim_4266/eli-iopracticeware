﻿using System;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Clinical;
using IO.Practiceware.Presentation.Views.ConfigureCategories;
using IO.Practiceware.Presentation.Views.InsurancePlansSetup;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.Setup.ClinicalSetup;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;


namespace IO.Practiceware.Presentation.Views.Setup.Clinical
{
    public class ClinicalSetupViewContext : IViewContext, IApplicationSetupViewContext
    {
       private readonly Func<ClinicalSetupListViewModel> _createSetupListViewModel;
        
  
        private readonly Func<StringAndIdViewModel> _createString;
        private readonly Func<BoolAndIdViewModel> _createBoolean;

        private readonly Func<ClinicalDetails> _createClinicalDetailsViewModel;
        private readonly Func<CollectionAndSelectedItemAndIdViewModel> _createCollectionAndSelectedItemAndIdViewModel;
        private readonly IClinicalSetupViewService _clinicalSetupService;
        private readonly Func<NamedViewModel> _createNamedViewModel;
     
        public virtual ExtendedObservableCollection<ClinicalSetupListViewModel> ClinicalSetupLists { get; set; }

        public virtual ClinicalSetupListViewModel SelectedList { get; set; }
        public virtual string AddItemText { get; set; }
        private readonly IInteractionManager _interactionManager;

        public ClinicalSetupViewContext(
            Func<ClinicalSetupListViewModel> createSetupListViewModel,
            Func<ClinicalDetails> createItemAndDetailsViewModel,
            Func<StringAndIdViewModel> createString,
            Func<BoolAndIdViewModel> createBoolean,
            Func<NamedViewModel> createNamedViewModel,
            Func<CollectionAndSelectedItemAndIdViewModel> createCollectionAndSelectedItemAndIdViewModel,
            IClinicalSetupViewService clinicalSetupService, IInteractionManager interactionManager
            )
        {
            _createSetupListViewModel = createSetupListViewModel;
            _createClinicalDetailsViewModel = createItemAndDetailsViewModel;
            _createString = createString;
            _createBoolean = createBoolean;
            _createNamedViewModel = createNamedViewModel;
            _createCollectionAndSelectedItemAndIdViewModel = createCollectionAndSelectedItemAndIdViewModel;
            _clinicalSetupService = clinicalSetupService;
            _interactionManager = interactionManager;
            GetClinicalSetupData = Command.Create(ExecuteGetClinicalSetupData).Async(() => InteractionContext);
            AddItemToNamedCollection = Command.Create(ExecuteAddItemToNamedCollection).Async(() => InteractionContext);
            DeleteItemFromNamedCollection = Command.Create<string>(ExecuteDeleteItemFromNamedCollection).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel);
            Save = Command.Create(ExecuteSaveChanges).Async(() => InteractionContext);
            ClinicalSetupLists = InitializeClinicalSetupCollection();
        }

        public IInteractionContext InteractionContext { get; set; }

        #region Commands
        public ICommand GetClinicalSetupData { get; protected set; }
        public ICommand AddItemToNamedCollection { get;protected set; }
        public ICommand DeleteItemFromNamedCollection { get; protected set; }
        public ICommand SaveChanges { get; protected set; }
        public ICommand Cancel { get; protected set; }
        public ICommand Save { get; set; }
        public ICommand Load { get; set; }
        #endregion
        
        public ExtendedObservableCollection<ClinicalSetupListViewModel> InitializeClinicalSetupCollection()
        {
            var clinicalList = new ExtendedObservableCollection<ClinicalSetupListViewModel>();
  
            string[] leftPanelTables = { "Clinical Specialty Types", "Service Modifiers" };

            for (int i = 0; i < leftPanelTables.Length; i++)
            {
                ClinicalSetupListViewModel leftPanelList =  _createSetupListViewModel();
                leftPanelList.Id = i;
                leftPanelList.Name = leftPanelTables[i];
                clinicalList.Add(leftPanelList);
            }

            return clinicalList.ToExtendedObservableCollection();
        }

        public void ExecuteGetClinicalSetupData()
        {
            var clinicalList = SelectedList;
            if (clinicalList != null && !clinicalList.IsLoaded)
            
            {
                clinicalList.List = _clinicalSetupService.GetClinicalSetupData(clinicalList.Name).ToExtendedObservableCollection();
                clinicalList.IsLoaded = true;
            }
        }
       public void ExecuteAddItemToNamedCollection()
        {         
            if (SelectedList.List.Count >0)
            {
                ClinicalDetails nameDetails = _createClinicalDetailsViewModel();
                var nameValueCollection = new ExtendedObservableCollection<StringAndIdViewModel>(); 
                var boolValueCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                var foreignKeyCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                if (SelectedList.List.FirstOrDefault(i => i.Name == AddItemText) != null)
                {
                    InteractionManager.Current.Alert("There is already a property with the same name in this setup type.");
                }
                else
                {
                    var stringValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.StringValues);
                    if (stringValues == null)
                    {
                        nameValueCollection = null;
                    }
                    else
                    {
                        stringValues.ForEach(i =>
                        {
                            var nameValuePair = _createString();
                            nameValuePair.Name = i.Name;
                            nameValuePair.Value = i.Name == "Name" ? AddItemText : "";
                            nameValueCollection.Add(nameValuePair);
                        });
                    }

                    var boolValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.BooleanValues);
                    if (boolValues == null)
                    {
                        boolValueCollection = null;
                    }
                    else
                    {
                        boolValues.ForEach(i =>
                        {
                            var boolValuePair = _createBoolean();
                            boolValuePair.Name = i.Name;
                            boolValuePair.Value = false;
                            boolValueCollection.Add(boolValuePair);
                        });
                    }

                    var foreignKeyValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.ForeignKeyValues);
                    if (foreignKeyValues == null)
                    {
                        foreignKeyCollection = null;
                    }
                    else
                    {
                        foreignKeyValues.ForEach(i =>
                        {
                            var foreignKeys = _createCollectionAndSelectedItemAndIdViewModel();
                            var namedCollection = new ExtendedObservableCollection<NamedViewModel>();
                            i.Collection.ForEach(j =>
                            {
                                var namedValues = _createNamedViewModel();
                                namedValues.Id = j.Id;
                                namedValues.Name = j.Name;
                                namedCollection.Add(namedValues);
                            });

                            foreignKeys.Collection = namedCollection;
                            foreignKeys.SelectedItem = namedCollection[0];
                            foreignKeys.Name = i.Name;
                            foreignKeyCollection.Add(foreignKeys);
                        });
                    }
                            
                    nameDetails.StringValues = nameValueCollection;
                    nameDetails.BooleanValues = boolValueCollection;
                    nameDetails.ForeignKeyValues = foreignKeyCollection;
                    nameDetails.Name = AddItemText;
                    nameDetails.CanDelete = true;
                    SelectedList.List.Add(nameDetails);
                }
            }
            AddItemText = string.Empty;

        }

        public void ExecuteDeleteItemFromNamedCollection(string itemName)
        {
            SelectedList.List.RemoveWhere(i => i.Name == itemName);
            SelectedList.DeletedItems.Add(itemName);
        }

        public void ExecuteSaveChanges()
        {
            
            var loadedList = ClinicalSetupLists.Where(i => i.IsLoaded).ToExtendedObservableCollection();
            _clinicalSetupService.SaveIsLoadedItems(loadedList);
            loadedList.IfNotNull(i => i.ForEach(j =>
            {
                j.IsLoaded = false;
            }));

            ExecuteGetClinicalSetupData();
        }
       

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }      

    }
}
