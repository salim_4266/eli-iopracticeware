﻿
using Soaf;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Setup.Clinical
{
    public class ClinicalSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;
        public ClinicalSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }
        public void ShowApplicationSetup()
        {

            var view = new ClinicalSetupView();
            view.DataContext = view.DataContext.EnsureType<ClinicalSetupViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                Header = "Clinical Setup"
            });
        }
    }
}
