﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Setup.Clinical
{
    /// <summary>
    /// Interaction logic for ClinicalSetup.xaml
    /// </summary>
    public partial class ClinicalSetupView
    {
        public ClinicalSetupView()
        {
            InitializeComponent();
        }
    }

    public class ClinicalViewContextReference : DataContextReference
    {
        public new ClinicalSetupViewContext DataContext
        {
            get { return (ClinicalSetupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

}
