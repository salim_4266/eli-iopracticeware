﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows;
using IO.Practiceware.Presentation.Views.Financials.FeeSchedules.FeeScheduleContracts;
using IO.Practiceware.Presentation.Views.Financials.FeeSchedules.ServiceFeeSchedules;
namespace IO.Practiceware.Presentation.Views.Setup.Billing
{
    public class BillingSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public BillingSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowServiceAndFee()
        {
            var view = new ManageServiceFeeSchedulesView();
            var dataContext = view.DataContext.EnsureType<ManageServiceFeeSchedulesViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }

        public void ShowInsurerContracts()
        {
            var view = new ManageFeeScheduleContractsView();
            var dataContext = view.DataContext.EnsureType<ManageFeeScheduleContractsViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}
