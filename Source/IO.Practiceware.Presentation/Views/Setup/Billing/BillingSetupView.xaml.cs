﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using IO.Practiceware.Presentation.ViewServices.Setup.Billing;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Presentation;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.Setup.Billing
{
    /// <summary>
    /// Interaction logic for BillingSetupView.xaml
    /// </summary>
    public partial class BillingSetupView
    {
        public BillingSetupView()
        {
            InitializeComponent();
        }
    }

    public class BillingSetupViewContextReference : DataContextReference
    {
        public new BillingSetupViewContext DataContext
        {
            get { return (BillingSetupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }   

    /// <summary>
    /// Billing Tab View Context
    /// </summary>
    public class BillingSetupViewContext : IViewContext, IApplicationSetupViewContext
    {
        private readonly IBillingSetupViewService _billingSetupViewService;
        private readonly BillingSetupViewManager _billingSetupViewManager;
        public virtual ExtendedObservableCollection<BillingSetupListViewModel> BillingSetupLists { get; set; }
        private readonly Func<BillingSetupListViewModel> _createSetupListViewModel;
        public virtual BillingSetupListViewModel SelectedList { get; set; }
        private readonly Func<BillingDetails> _createBillingDetailsViewModel;
        private readonly Func<CollectionAndSelectedItemAndIdViewModel> _createCollectionAndSelectedItemAndIdViewModel;
        public virtual string AddItemText { get; set; }
        private readonly Func<StringAndIdViewModel> _createString;
        private readonly Func<BoolAndIdViewModel> _createBoolean;
        private readonly Func<NamedViewModel> _createNamedViewModel;        

        public BillingSetupViewContext(IBillingSetupViewService billingSetupViewService,
            BillingSetupViewManager billingSetupViewManager,
            Func<BillingSetupListViewModel> createSetupListViewModel,
            Func<BillingDetails> createItemAndDetailsViewModel,
            Func<StringAndIdViewModel> createString,
            Func<BoolAndIdViewModel> createBoolean,
            Func<NamedViewModel> createNamedViewModel,
            Func<CollectionAndSelectedItemAndIdViewModel> createCollectionAndSelectedItemAndIdViewModel

            )
        {
            _createBoolean = createBoolean;
            _createNamedViewModel = createNamedViewModel;
            _createSetupListViewModel = createSetupListViewModel;
            _createBillingDetailsViewModel = createItemAndDetailsViewModel;
            _billingSetupViewService = billingSetupViewService;
            _billingSetupViewManager = billingSetupViewManager;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose).Async(() => InteractionContext);
            LoadServicesAndFee = Command.Create(ExecuteLoadServiceAndFee);
            LoadInsurerContracts = Command.Create(ExecuteLoadInsurerContracts);
            GetBillingSetupData = Command.Create(ExecuteGetBillingSetupData).Async(() => InteractionContext);
            BillingSetupLists = InitializeBillingSetupCollection();
            _createCollectionAndSelectedItemAndIdViewModel = createCollectionAndSelectedItemAndIdViewModel;
            _createString = createString;
            AddItemToNamedCollection = Command.Create(ExecuteAddItemToNamedCollection).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);       
        }
        public ExtendedObservableCollection<BillingSetupListViewModel> InitializeBillingSetupCollection()
        {
            var billingList = new ExtendedObservableCollection<BillingSetupListViewModel>();

            string[] leftPanelTables = { "Adjustment Types", "Financial Information Types", "Payment Methods", "Claim Adjustment Reason Codes", "Billing Organization", "Person Billing Organization", "Claim Statuses" };

            for (int i = 0; i < leftPanelTables.Length; i++)
            {
                BillingSetupListViewModel leftPanelList = _createSetupListViewModel();
                leftPanelList.Id = i;
                leftPanelList.Name = leftPanelTables[i];
                billingList.Add(leftPanelList);
            }
            IsEnabledCloseDate = PermissionId.CloseDateWidget.PrincipalContextHasPermission();
            return billingList.ToExtendedObservableCollection();
        }
        public void ExecuteGetBillingSetupData()
        {
            var billingList = SelectedList;
            if (billingList != null && !billingList.IsLoaded)
            {
                billingList.List = _billingSetupViewService.GetBillingSetupData(billingList.Name).ToExtendedObservableCollection();
                billingList.IsLoaded = true;
            }
        }

        public void ExecuteAddItemToNamedCollection()
        {
            if (SelectedList.List.Count > 0)
            {
                BillingDetails nameDetails = _createBillingDetailsViewModel();
                var nameValueCollection = new ExtendedObservableCollection<StringAndIdViewModel>();
                var boolValueCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                var foreignKeyCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                if (SelectedList.List.FirstOrDefault(i => i.Name == AddItemText) != null)
                {
                    InteractionManager.Current.Alert("There is already a property with the same name in this setup type.");
                }
                else
                {
                    var stringValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.StringValues);
                    if (stringValues == null)
                    {
                        nameValueCollection = null;
                    }
                    else
                    {
                        stringValues.ForEach(i =>
                        {
                            var nameValuePair = _createString();
                            nameValuePair.Name = i.Name;
                            nameValuePair.Value = i.Name == "Name" ? AddItemText : "";
                            nameValueCollection.Add(nameValuePair);
                        });
                    }

                    var boolValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.BooleanValues);
                    if (boolValues == null)
                    {
                        boolValueCollection = null;
                    }
                    else
                    {
                        boolValues.ForEach(i =>
                        {
                            var boolValuePair = _createBoolean();
                            boolValuePair.Name = i.Name;
                            boolValuePair.Value = false;
                            boolValueCollection.Add(boolValuePair);
                        });
                    }

                    var foreignKeyValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.ForeignKeyValues);
                    if (foreignKeyValues == null)
                    {
                        foreignKeyCollection = null;
                    }
                    else
                    {
                        foreignKeyValues.ForEach(i =>
                        {
                            var foreignKeys = _createCollectionAndSelectedItemAndIdViewModel();
                            var namedCollection = new ExtendedObservableCollection<NamedViewModel>();
                            i.Collection.ForEach(j =>
                            {
                                var namedValues = _createNamedViewModel();
                                namedValues.Id = j.Id;
                                namedValues.Name = j.Name;
                                namedCollection.Add(namedValues);
                            });

                            foreignKeys.Collection = namedCollection;
                            foreignKeys.SelectedItem = namedCollection[0];
                            foreignKeys.Name = i.Name;
                            foreignKeyCollection.Add(foreignKeys);
                        });
                    }

                    nameDetails.StringValues = nameValueCollection;
                    nameDetails.BooleanValues = boolValueCollection;
                    nameDetails.ForeignKeyValues = foreignKeyCollection;
                    nameDetails.Name = AddItemText;
                    nameDetails.CanDelete = true;
                    SelectedList.List.Add(nameDetails);
                }
            }
            AddItemText = string.Empty;

        }

        private void ExecuteLoad()
        {
            OpenBalanceTypes = Enums.GetValues(typeof(OpenBalanceType)).Select(x => new NamedViewModel { Id = (int)x, Name = x.ToString() });
            var openBalanceTypeSettings = ApplicationSettings.Cached.GetSetting<NamedViewModel>(ApplicationSetting.OpenBalanceDeterminer);
            SelectedOpenBalanceType = openBalanceTypeSettings != null ? openBalanceTypeSettings.Value : OpenBalanceTypes.FirstOrDefault(i => i.Id == (int)OpenBalanceType.Service);

            CloseDate = ApplicationSettings.Cached.GetSetting<DateTime>(ApplicationSetting.CloseDateTime).IfNotNull(s => (DateTime?)s.Value);

            DiagnosisCodeSet = Enums.GetValues(typeof(DiagnosisTypeId)).Select(x => new NamedViewModel { Id = (int)x, Name = x.ToString() }).ToArray();
            var diagnosisCodeSetSettings = ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId);
            SelectedDiagnosisCodeSet = DiagnosisCodeSet.FirstOrDefault(i => i.Id == Convert.ToInt32(diagnosisCodeSetSettings.Value)) ?? DiagnosisCodeSet.FirstOrDefault(i => i.Id == (int)DiagnosisTypeId.Icd9);           
        }

        private void ExecuteSave()
        {

            _billingSetupViewService.SaveOrModifyApplicationSettings(CloseDate, SelectedOpenBalanceType, SelectedDiagnosisCodeSet.Id);

            var loadedList = BillingSetupLists.Where(i => i.IsLoaded).ToExtendedObservableCollection();
            _billingSetupViewService.SaveIsLoadedItems(loadedList);
            loadedList.IfNotNull(i => i.ForEach(j =>
            {
                j.IsLoaded = false;
            }));

            ExecuteGetBillingSetupData();
        }
        private void ExecuteClose()
        {
            InteractionContext.Complete(false);

        }
        private void ExecuteLoadServiceAndFee()
        {
            _billingSetupViewManager.ShowServiceAndFee();
        }        

        private void ExecuteLoadInsurerContracts()
        {
            _billingSetupViewManager.ShowInsurerContracts();
        }      

        public IInteractionContext InteractionContext { get; set; }

        public ICommand Load { get; set; }

        public ICommand Save { get; set; }


        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> OpenBalanceTypes { get; set; }

        private NamedViewModel _selectedOpenBalanceType;
        [DispatcherThread]
        public virtual NamedViewModel SelectedOpenBalanceType
        {
            get { return _selectedOpenBalanceType; }
            set
            {
                if (Equals(_selectedOpenBalanceType, value)) return;
                if (_selectedOpenBalanceType != null) InteractionManager.Current.Alert("Changing this value will take effect only after saving and restarting the software.");
                _selectedOpenBalanceType = value;
            }
        }

        [DispatcherThread]
        public virtual DateTime? CloseDate { get; set; }

        [DispatcherThread]
        public bool IsEnabledCloseDate { get; set; }
        

        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> DiagnosisCodeSet { get; set; }


        private NamedViewModel _selectedDiagnosisCodeSet;
        [DispatcherThread]
        public virtual NamedViewModel SelectedDiagnosisCodeSet
        {
            get { return _selectedDiagnosisCodeSet; }
            set
            {
                if (Equals(_selectedDiagnosisCodeSet, value)) return;

                if (_selectedDiagnosisCodeSet != null) InteractionManager.Current.Alert("Changing this value will take effect only after saving and restarting the software.");
                _selectedDiagnosisCodeSet = value;
            }
        }

        public ICommand LoadServicesAndFee { get; set; }

        public ICommand LoadInsurerContracts { get; set; }
        public ICommand GetBillingSetupData { get; protected set; }
        public ICommand AddItemToNamedCollection { get; protected set; }

        public ICommand Close { get; protected set; }       
    }
}
