﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Setup
{
    class ApplicationSetupViewContextReference : DataContextReference
    {
        public new ApplicationSetupViewContext DataContext
        {
            get { return (ApplicationSetupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
