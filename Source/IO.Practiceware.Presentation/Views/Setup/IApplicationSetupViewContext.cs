﻿using System.Windows.Input;


namespace IO.Practiceware.Presentation.Views.Setup
{
    public interface IApplicationSetupViewContext
    {
        /// <summary>
        /// Command to load the information for the current view context
        /// </summary>
        ICommand Load { get; set; }
    }
}
