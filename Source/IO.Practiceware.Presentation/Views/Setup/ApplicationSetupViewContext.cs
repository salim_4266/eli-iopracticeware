﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup;
using System.Windows.Input;
using IO.Practiceware.Presentation.Views.ExternalProviders;
using System.Windows;
using IO.Practiceware.Presentation.Views.Setup.Administrative;
using IO.Practiceware.Presentation.Views.Setup.Billing;
using IO.Practiceware.Presentation.Views.Setup.Clinical;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.Setup.AdminUtilities;
using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.Views.Setup
{
    /// <summary>
    /// CONTENTS CREATED FOR DESIGN CONCEPT & PROTOTYPING PURPOSES
    /// Developer note: the following is not intended to represent the ideal underlying data structure - it is used for rapid prototyping purposes only and should you 
    /// find any optimizations in the structure of the data presented here, please make those changes to the underlying code structure as you see fit.
    /// </summary>
    public class ApplicationSetupViewContext : IViewContext, IApplicationSetupViewContext
    {
        public AdministrativeSetupViewContext AdministrativeSetupViewContext { get; set; }
        public AdminUtilitiesViewContext AdminUtilitiesViewContext { get; set; }
        private readonly IInteractionManager _interactionManager;
        private readonly IExternalFeatureIntegrationManager _externalFeatureIntegrationManager;

        private readonly Func<ObservableCollection<SetupListViewModel>> _createSetupListViewModelCollection;
        private readonly Func<SetupListViewModel> _createSetupListViewModel;
        private readonly Func<ObservableCollection<ItemAndDetailsViewModel>> _createItemAndDetailsViewModelCollection;
        private readonly Func<ObservableCollection<BoolAndIdViewModel>> _createBoolCollection;
        private readonly Func<ObservableCollection<StringAndIdViewModel>> _createStringCollection;
        private readonly Func<ObservableCollection<CollectionAndSelectedItemAndIdViewModel>> _createCollectionAndSelectedItemAndIdViewModelCollection;
        private readonly Func<ObservableCollection<TwoCollectionAndSelectedItemAndIdViewModel>> _createTwoCollectionAndSelectedItemAndIdViewModelCollection;
        private readonly Func<ObservableCollection<TwoManyToManyViewModel>> _createTwoManyToManyViewModelCollection;
        private readonly Func<TwoCollectionAndSelectedItemAndIdViewModel> _createTwoCollectionAndSelectedItemAndIdViewModel;
        private readonly Func<StringAndIdViewModel> _createString;
        private readonly Func<ObservableCollection<NamedViewModel>> _createNamedViewModelCollection;
        private readonly Func<NamedViewModel> _createNamedViewModel;
        private readonly Func<BoolAndIdViewModel> _createBool;
        private readonly Func<CollectionAndSelectedItemAndIdViewModel> _createCollectionAndSelectedItemAndIdViewModel;
        private readonly Func<ItemAndDetailsViewModel> _createItemAndDetailsViewModel;
        private readonly Func<TwoManyToManyViewModel> _createTwoManyToManyViewModel;
        readonly Func<ViewContextLoadViewModel> _createViewContextLoadViewModel;
        public readonly Dictionary<ApplicationSetupTab, IApplicationSetupViewContext> ApplicationSetupTabs;
        readonly List<ViewContextLoadViewModel> _viewContextModels;
        public virtual Collection<SetupListViewModel> BillingSetupLists { get; set; }
        public virtual Collection<SetupListViewModel> AdminSetupLists { get; set; }
        public virtual Collection<SetupListViewModel> ClinicalSetupLists { get; set; }

        public ICommand OpenExternalProvidersWindow { get; protected set; }
        public ICommand OpenSettings { get; protected set; }

        public ApplicationSetupViewContext(
            Func<ObservableCollection<SetupListViewModel>> createSetupListViewModelCollection,
            Func<SetupListViewModel> createSetupListViewModel,
            Func<ObservableCollection<ItemAndDetailsViewModel>> createItemAndDetailsViewModelCollection,
            Func<ItemAndDetailsViewModel> createItemAndDetailsViewModel,
            Func<ObservableCollection<BoolAndIdViewModel>> createBoolCollection,
            Func<ObservableCollection<StringAndIdViewModel>> createStringCollection,
            Func<ObservableCollection<CollectionAndSelectedItemAndIdViewModel>> createCollectionAndSelectedItemAndIdViewModelCollection,
            Func<ObservableCollection<NamedViewModel>> createNamedViewModelCollection,
            Func<NamedViewModel> createNamedViewModel,
            Func<StringAndIdViewModel> createString,
            Func<BoolAndIdViewModel> createBool,
            Func<CollectionAndSelectedItemAndIdViewModel> createCollectionAndSelectedItemAndIdViewModel,
            Func<ObservableCollection<TwoCollectionAndSelectedItemAndIdViewModel>> createTwoCollectionAndSelectedItemAndIdViewModelCollection,
            Func<TwoCollectionAndSelectedItemAndIdViewModel> createTwoCollectionAndSelectedItemAndIdViewModel,
            Func<ObservableCollection<TwoManyToManyViewModel>> createTwoManyToManyViewModelCollection,
            Func<TwoManyToManyViewModel> createTwoManyToManyViewModel,
            IInteractionManager interactionManager,
            IExternalFeatureIntegrationManager externalFeatureIntegrationManager, Func<ViewContextLoadViewModel> createViewContextLoadViewModel,
            BillingSetupViewContext billingSetupViewContext,
            AdministrativeSetupViewContext administrativeSetupViewContext,
            ClinicalSetupViewContext clinicalSetupViewContext,
            AdminUtilitiesViewContext adminUtilitiesViewContext)
        {
            AdministrativeSetupViewContext = administrativeSetupViewContext;
            AdminUtilitiesViewContext = adminUtilitiesViewContext;
            _createSetupListViewModelCollection = createSetupListViewModelCollection;
            _createSetupListViewModel = createSetupListViewModel;
            _createBoolCollection = createBoolCollection;
            _createStringCollection = createStringCollection;
            _createItemAndDetailsViewModel = createItemAndDetailsViewModel;
            _createItemAndDetailsViewModelCollection = createItemAndDetailsViewModelCollection;
            _createNamedViewModelCollection = createNamedViewModelCollection;
            _createString = createString;
            _createBool = createBool;
            _createNamedViewModel = createNamedViewModel;
            _createCollectionAndSelectedItemAndIdViewModelCollection = createCollectionAndSelectedItemAndIdViewModelCollection;
            _createCollectionAndSelectedItemAndIdViewModel = createCollectionAndSelectedItemAndIdViewModel;
            _createTwoCollectionAndSelectedItemAndIdViewModelCollection = createTwoCollectionAndSelectedItemAndIdViewModelCollection;
            _createTwoCollectionAndSelectedItemAndIdViewModel = createTwoCollectionAndSelectedItemAndIdViewModel;
            _createTwoManyToManyViewModelCollection = createTwoManyToManyViewModelCollection;
            _createTwoManyToManyViewModel = createTwoManyToManyViewModel;


            _interactionManager = interactionManager;
            _externalFeatureIntegrationManager = externalFeatureIntegrationManager;
            _createViewContextLoadViewModel = createViewContextLoadViewModel;

            OpenExternalProvidersWindow = Command.Create(ExecuteOpenExternalProvidersWindow);
            OpenSettings = Command.Create(ExecuteOpenSettings);
            _viewContextModels = new List<ViewContextLoadViewModel>();
            ApplicationSetupTabs = new Dictionary<ApplicationSetupTab, IApplicationSetupViewContext>
                                   {
                                       {ApplicationSetupTab.Administrative, administrativeSetupViewContext},
                                       {ApplicationSetupTab.Billing, billingSetupViewContext},
                                       {ApplicationSetupTab.Clinical, clinicalSetupViewContext},
                                       {ApplicationSetupTab.AdminUtilities, adminUtilitiesViewContext}
                                   };
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            AdminSetupLists = CreateAdminSetupLists();
            BillingSetupLists = CreateBillingSetupLists();
            ClinicalSetupLists = CreateClinicalSetupLists();

            ShowAlltabs = false;

            AdministrativeViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.ViewContext = ApplicationSetupTabs[ApplicationSetupTab.Administrative];
                instance.ApplicationSetupTab = ApplicationSetupTab.Administrative;
                instance.FireCommandOneTimeOnly = true;
                instance.LoadCommand = ApplicationSetupTabs[ApplicationSetupTab.Administrative].Load;
            });
            _viewContextModels.Add(AdministrativeViewContextLoadViewModel);

            BillingViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.ViewContext = ApplicationSetupTabs[ApplicationSetupTab.Billing];
                instance.ApplicationSetupTab = ApplicationSetupTab.Billing;
                instance.FireCommandOneTimeOnly = true;
                instance.LoadCommand = ApplicationSetupTabs[ApplicationSetupTab.Billing].Load;
            });
            _viewContextModels.Add(BillingViewContextLoadViewModel);

            ClinicalViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.ViewContext = ApplicationSetupTabs[ApplicationSetupTab.Clinical];
                instance.ApplicationSetupTab = ApplicationSetupTab.Clinical;
                instance.FireCommandOneTimeOnly = true;
                instance.LoadCommand = ApplicationSetupTabs[ApplicationSetupTab.Clinical].Load;
            });
            _viewContextModels.Add(ClinicalViewContextLoadViewModel);

            AdminUtilitiesViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.ViewContext = ApplicationSetupTabs[ApplicationSetupTab.AdminUtilities];
                instance.ApplicationSetupTab = ApplicationSetupTab.AdminUtilities;
                instance.FireCommandOneTimeOnly = true;
                instance.LoadCommand = ApplicationSetupTabs[ApplicationSetupTab.AdminUtilities].Load;
            });
            _viewContextModels.Add(AdminUtilitiesViewContextLoadViewModel);

            ShowAlltabs = true;

            _viewContextModels.ToList().ForEach(m => m.AllowSelectionChange = true);

            var viewContextModel = _viewContextModels.Single(vcm => vcm.ApplicationSetupTab == ApplicationSetupTab.Administrative);

            viewContextModel.IsSelected = true;
        }

        private void ExecuteOpenSettings()
        {
            _externalFeatureIntegrationManager.ExecuteFeature("Application_Settings");
        }

        private void ExecuteOpenExternalProvidersWindow()
        {
            var view = new ExternalProvidersView();
            view.DataContext = view.DataContext.EnsureType<ExternalProvidersViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                Header = "External Providers"
            });
        }

        private static bool CanAccessAdminUtilities()
        {
            return PermissionId.AccessAdminUtilities.PrincipalContextHasPermission();
        }

        private ObservableCollection<SetupListViewModel> CreateAdminSetupLists()
        {
            ObservableCollection<SetupListViewModel> toReturn = _createSetupListViewModelCollection();

            SetupListViewModel insurerPlanTypes = CreateInsurerPlanTypes();
            SetupListViewModel claimFileReceivers = CreateClaimFileReceivers();
            SetupListViewModel insurerBusinessClasses = CreateInsurerBusinessClasses();

            toReturn.Add(insurerPlanTypes);
            toReturn.Add(claimFileReceivers);
            toReturn.Add(insurerBusinessClasses);

            return toReturn;
        }


        private ObservableCollection<SetupListViewModel> CreateBillingSetupLists()
        {
            ObservableCollection<SetupListViewModel> toReturn = _createSetupListViewModelCollection();

            SetupListViewModel adjustments = CreateAdjustmentTypeSetup(50);
            SetupListViewModel financialInformations = CreateFinancialInformationTypeSetup(20);
            SetupListViewModel paymentMethods = CreatePaymentMethods();
            SetupListViewModel reasonCodes = CreateReasonCodes(100);

            toReturn.Add(adjustments);
            toReturn.Add(financialInformations);
            toReturn.Add(paymentMethods);
            toReturn.Add(reasonCodes);

            return toReturn;
        }


        private ObservableCollection<SetupListViewModel> CreateClinicalSetupLists()
        {
            ObservableCollection<SetupListViewModel> toReturn = _createSetupListViewModelCollection();


            return toReturn;
        }


        private SetupListViewModel CreateClaimFileReceivers()
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            ItemAndDetailsViewModel toAddHM = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesHM = _createStringCollection();
            StringAndIdViewModel namePropertyHM = _createString();
            namePropertyHM.Value = "HM";
            namePropertyHM.Name = "Name";
            stringValuesHM.Add(namePropertyHM);
            StringAndIdViewModel namePropertyGSAppReceiverCode = _createString();
            namePropertyGSAppReceiverCode.Value = "";
            namePropertyGSAppReceiverCode.Name = "GS Application Receiver Code";
            stringValuesHM.Add(namePropertyGSAppReceiverCode);
            StringAndIdViewModel namePropertyGSAppSenderCode = _createString();
            namePropertyGSAppSenderCode.Value = "";
            namePropertyGSAppSenderCode.Name = "GS Application Sender Code";
            stringValuesHM.Add(namePropertyGSAppSenderCode);
            StringAndIdViewModel interchangeControlVersionCode = _createString();
            interchangeControlVersionCode.Value = "00501";
            interchangeControlVersionCode.Name = "Interchange Control Version Code";
            stringValuesHM.Add(interchangeControlVersionCode);
            StringAndIdViewModel interchangeReceiverCode = _createString();
            interchangeReceiverCode.Value = "495971";
            interchangeReceiverCode.Name = "Interchange Receiver Code";
            stringValuesHM.Add(interchangeReceiverCode);
            StringAndIdViewModel interchangeReceiverCodeQualifier = _createString();
            interchangeReceiverCodeQualifier.Value = "33";
            interchangeReceiverCodeQualifier.Name = "Interchange Receiver Code Qualifier";
            stringValuesHM.Add(interchangeReceiverCodeQualifier);
            StringAndIdViewModel interchangeSenderCode = _createString();
            interchangeSenderCode.Value = "54771";
            interchangeSenderCode.Name = "Interchange Sender Code";
            stringValuesHM.Add(interchangeSenderCode);
            StringAndIdViewModel interchangeSenderCodeQualifier = _createString();
            interchangeSenderCodeQualifier.Value = "ZZ";
            interchangeSenderCodeQualifier.Name = "Interchange Sender Code Qualifier";
            stringValuesHM.Add(interchangeSenderCodeQualifier);
            StringAndIdViewModel interchangeUsageIndicator = _createString();
            interchangeUsageIndicator.Value = "P";
            interchangeUsageIndicator.Name = "Interchange Usage Indicator";
            stringValuesHM.Add(interchangeUsageIndicator);
            StringAndIdViewModel loop1000ARecipientCode = _createString();
            loop1000ARecipientCode.Value = "54771 (necessary on the UI???)";
            loop1000ARecipientCode.Name = "Loop 1000A Recipient Code";
            stringValuesHM.Add(loop1000ARecipientCode);
            StringAndIdViewModel loop1000BRecipientCode = _createString();
            loop1000BRecipientCode.Value = "495971 (necessary on the UI???)";
            loop1000BRecipientCode.Name = "Loop 1000B Recipient Code";
            stringValuesHM.Add(loop1000BRecipientCode);
            StringAndIdViewModel elementSeparator = _createString();
            elementSeparator.Value = ":";
            elementSeparator.Name = "Element Separator";
            stringValuesHM.Add(elementSeparator);
            StringAndIdViewModel repetitionSeparator = _createString();
            repetitionSeparator.Value = "^";
            repetitionSeparator.Name = "Repetition Separator";
            stringValuesHM.Add(repetitionSeparator);


            toAddHM.StringValues = stringValuesHM;
            toAddHM.CanDelete = true;
            toAddHM.Name = namePropertyHM.Value;


            ItemAndDetailsViewModel toAddWebMD = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesWebMD = _createStringCollection();
            StringAndIdViewModel namePropertyWebMD = _createString();
            namePropertyWebMD.Value = "WebMD";
            namePropertyWebMD.Name = "Name";
            stringValuesWebMD.Add(namePropertyWebMD);
            StringAndIdViewModel namePropertyGSAppReceiverCodeWebMD = _createString();
            namePropertyGSAppReceiverCodeWebMD.Value = "133052274";
            namePropertyGSAppReceiverCodeWebMD.Name = "GS Application Receiver Code";
            stringValuesWebMD.Add(namePropertyGSAppReceiverCodeWebMD);
            StringAndIdViewModel namePropertyGSAppSenderCodeWebMD = _createString();
            namePropertyGSAppSenderCodeWebMD.Value = "223182777";
            namePropertyGSAppSenderCodeWebMD.Name = "GS Application Sender Code";
            stringValuesWebMD.Add(namePropertyGSAppSenderCodeWebMD);
            StringAndIdViewModel interchangeControlVersionCodeWebMD = _createString();
            interchangeControlVersionCodeWebMD.Value = "00501";
            interchangeControlVersionCodeWebMD.Name = "Interchange Control Version Code";
            stringValuesWebMD.Add(interchangeControlVersionCodeWebMD);
            StringAndIdViewModel interchangeReceiverCodeWebMD = _createString();
            interchangeReceiverCodeWebMD.Value = "133052274";
            interchangeReceiverCodeWebMD.Name = "Interchange Receiver Code";
            stringValuesWebMD.Add(interchangeReceiverCodeWebMD);
            StringAndIdViewModel interchangeReceiverCodeQualifierWebMD = _createString();
            interchangeReceiverCodeQualifierWebMD.Value = "30";
            interchangeReceiverCodeQualifierWebMD.Name = "Interchange Receiver Code Qualifier";
            stringValuesWebMD.Add(interchangeReceiverCodeQualifierWebMD);
            StringAndIdViewModel interchangeSenderCodeWebMD = _createString();
            interchangeSenderCodeWebMD.Value = "223182777";
            interchangeSenderCodeWebMD.Name = "Interchange Sender Code";
            stringValuesWebMD.Add(interchangeSenderCodeWebMD);
            StringAndIdViewModel interchangeSenderCodeQualifierWebMD = _createString();
            interchangeSenderCodeQualifierWebMD.Value = "30";
            interchangeSenderCodeQualifierWebMD.Name = "Interchange Sender Code Qualifier";
            stringValuesWebMD.Add(interchangeSenderCodeQualifierWebMD);
            StringAndIdViewModel interchangeUsageIndicatorWebMD = _createString();
            interchangeUsageIndicatorWebMD.Value = "P";
            interchangeUsageIndicatorWebMD.Name = "Interchange Usage Indicator";
            stringValuesWebMD.Add(interchangeUsageIndicatorWebMD);
            StringAndIdViewModel loop1000ARecipientCodeWebMD = _createString();
            loop1000ARecipientCodeWebMD.Value = "223182777 (necessary on the UI???)";
            loop1000ARecipientCodeWebMD.Name = "Loop 1000A Recipient Code";
            stringValuesWebMD.Add(loop1000ARecipientCodeWebMD);
            StringAndIdViewModel loop1000BRecipientCodeWebMD = _createString();
            loop1000BRecipientCodeWebMD.Value = "133052274 (necessary on the UI???)";
            loop1000BRecipientCodeWebMD.Name = "Loop 1000B Recipient Code";
            stringValuesWebMD.Add(loop1000BRecipientCodeWebMD);
            StringAndIdViewModel elementSeparatorWebMD = _createString();
            elementSeparatorWebMD.Value = ":";
            elementSeparatorWebMD.Name = "Element Separator";
            stringValuesWebMD.Add(elementSeparatorWebMD);
            StringAndIdViewModel repetitionSeparatorWebMD = _createString();
            repetitionSeparatorWebMD.Value = "^";
            repetitionSeparatorWebMD.Name = "Repetition Separator";
            stringValuesWebMD.Add(repetitionSeparatorWebMD);


            toAddWebMD.StringValues = stringValuesWebMD;
            toAddWebMD.CanDelete = true;
            toAddWebMD.Name = namePropertyWebMD.Value;


            ItemAndDetailsViewModel toAddHGSA = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesHGSA = _createStringCollection();
            StringAndIdViewModel namePropertyHGSA = _createString();
            namePropertyHGSA.Value = "HGSA";
            namePropertyHGSA.Name = "Name";
            stringValuesHGSA.Add(namePropertyHGSA);
            StringAndIdViewModel namePropertyGSAppReceiverCodeHGSA = _createString();
            namePropertyGSAppReceiverCodeHGSA.Value = "12502";
            namePropertyGSAppReceiverCodeHGSA.Name = "GS Application Receiver Code";
            stringValuesHGSA.Add(namePropertyGSAppReceiverCodeHGSA);
            StringAndIdViewModel namePropertyGSAppSenderCodeHGSA = _createString();
            namePropertyGSAppSenderCodeHGSA.Value = "1919524";
            namePropertyGSAppSenderCodeHGSA.Name = "GS Application Sender Code";
            stringValuesHGSA.Add(namePropertyGSAppSenderCodeHGSA);
            StringAndIdViewModel interchangeControlVersionCodeHGSA = _createString();
            interchangeControlVersionCodeHGSA.Value = "00501";
            interchangeControlVersionCodeHGSA.Name = "Interchange Control Version Code";
            stringValuesHGSA.Add(interchangeControlVersionCodeHGSA);
            StringAndIdViewModel interchangeReceiverCodeHGSA = _createString();
            interchangeReceiverCodeHGSA.Value = "12502";
            interchangeReceiverCodeHGSA.Name = "Interchange Receiver Code";
            stringValuesHGSA.Add(interchangeReceiverCodeHGSA);
            StringAndIdViewModel interchangeReceiverCodeQualifierHGSA = _createString();
            interchangeReceiverCodeQualifierHGSA.Value = "27";
            interchangeReceiverCodeQualifierHGSA.Name = "Interchange Receiver Code Qualifier";
            stringValuesHGSA.Add(interchangeReceiverCodeQualifierHGSA);
            StringAndIdViewModel interchangeSenderCodeHGSA = _createString();
            interchangeSenderCodeHGSA.Value = "1919524";
            interchangeSenderCodeHGSA.Name = "Interchange Sender Code";
            stringValuesHGSA.Add(interchangeSenderCodeHGSA);
            StringAndIdViewModel interchangeSenderCodeQualifierHGSA = _createString();
            interchangeSenderCodeQualifierHGSA.Value = "ZZ";
            interchangeSenderCodeQualifierHGSA.Name = "Interchange Sender Code Qualifier";
            stringValuesHGSA.Add(interchangeSenderCodeQualifierHGSA);
            StringAndIdViewModel interchangeUsageIndicatorHGSA = _createString();
            interchangeUsageIndicatorHGSA.Value = "P";
            interchangeUsageIndicatorHGSA.Name = "Interchange Usage Indicator";
            stringValuesHGSA.Add(interchangeUsageIndicatorHGSA);
            StringAndIdViewModel loop1000ARecipientCodeHGSA = _createString();
            loop1000ARecipientCodeHGSA.Value = "1919524 (necessary on the UI???)";
            loop1000ARecipientCodeHGSA.Name = "Loop 1000A Recipient Code";
            stringValuesHGSA.Add(loop1000ARecipientCodeHGSA);
            StringAndIdViewModel loop1000BRecipientCodeHGSA = _createString();
            loop1000BRecipientCodeHGSA.Value = "12502 (necessary on the UI???)";
            loop1000BRecipientCodeHGSA.Name = "Loop 1000B Recipient Code";
            stringValuesHGSA.Add(loop1000BRecipientCodeHGSA);
            StringAndIdViewModel elementSeparatorHGSA = _createString();
            elementSeparatorHGSA.Value = ":";
            elementSeparatorHGSA.Name = "Element Separator";
            stringValuesHGSA.Add(elementSeparatorHGSA);
            StringAndIdViewModel repetitionSeparatorHGSA = _createString();
            repetitionSeparatorHGSA.Value = "^";
            repetitionSeparatorHGSA.Name = "Repetition Separator";
            stringValuesHGSA.Add(repetitionSeparatorHGSA);


            toAddHGSA.StringValues = stringValuesHGSA;
            toAddHGSA.CanDelete = true;
            toAddHGSA.Name = namePropertyHGSA.Value;






            ItemAndDetailsViewModel toAddUnknown = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesUnknown = _createStringCollection();
            StringAndIdViewModel namePropertyUnknown = _createString();
            namePropertyUnknown.Value = "";
            namePropertyUnknown.Name = "Name";
            stringValuesUnknown.Add(namePropertyUnknown);
            StringAndIdViewModel namePropertyGSAppReceiverCodeUnknown = _createString();
            namePropertyGSAppReceiverCodeUnknown.Value = "";
            namePropertyGSAppReceiverCodeUnknown.Name = "GS Application Receiver Code";
            stringValuesUnknown.Add(namePropertyGSAppReceiverCodeUnknown);
            StringAndIdViewModel namePropertyGSAppSenderCodeUnknown = _createString();
            namePropertyGSAppSenderCodeUnknown.Value = "UNKNOWN";
            namePropertyGSAppSenderCodeUnknown.Name = "GS Application Sender Code";
            stringValuesUnknown.Add(namePropertyGSAppSenderCodeUnknown);
            StringAndIdViewModel interchangeControlVersionCodeUnknown = _createString();
            interchangeControlVersionCodeUnknown.Value = "00501";
            interchangeControlVersionCodeUnknown.Name = "Interchange Control Version Code";
            stringValuesUnknown.Add(interchangeControlVersionCodeUnknown);
            StringAndIdViewModel interchangeReceiverCodeUnknown = _createString();
            interchangeReceiverCodeUnknown.Value = "";
            interchangeReceiverCodeUnknown.Name = "Interchange Receiver Code";
            stringValuesUnknown.Add(interchangeReceiverCodeUnknown);
            StringAndIdViewModel interchangeReceiverCodeQualifierUnknown = _createString();
            interchangeReceiverCodeQualifierUnknown.Value = "ZZ";
            interchangeReceiverCodeQualifierUnknown.Name = "Interchange Receiver Code Qualifier";
            stringValuesUnknown.Add(interchangeReceiverCodeQualifierUnknown);
            StringAndIdViewModel interchangeSenderCodeUnknown = _createString();
            interchangeSenderCodeUnknown.Value = "";
            interchangeSenderCodeUnknown.Name = "Interchange Sender Code";
            stringValuesUnknown.Add(interchangeSenderCodeUnknown);
            StringAndIdViewModel interchangeSenderCodeQualifierUnknown = _createString();
            interchangeSenderCodeQualifierUnknown.Value = "ZZ";
            interchangeSenderCodeQualifierUnknown.Name = "Interchange Sender Code Qualifier";
            stringValuesUnknown.Add(interchangeSenderCodeQualifierUnknown);
            StringAndIdViewModel interchangeUsageIndicatorUnknown = _createString();
            interchangeUsageIndicatorUnknown.Value = "P";
            interchangeUsageIndicatorUnknown.Name = "Interchange Usage Indicator";
            stringValuesUnknown.Add(interchangeUsageIndicatorUnknown);
            StringAndIdViewModel loop1000ARecipientCodeUnknown = _createString();
            loop1000ARecipientCodeUnknown.Value = "";
            loop1000ARecipientCodeUnknown.Name = "Loop 1000A Recipient Code";
            stringValuesUnknown.Add(loop1000ARecipientCodeUnknown);
            StringAndIdViewModel loop1000BRecipientCodeUnknown = _createString();
            loop1000BRecipientCodeUnknown.Value = "";
            loop1000BRecipientCodeUnknown.Name = "Loop 1000B Recipient Code";
            stringValuesUnknown.Add(loop1000BRecipientCodeUnknown);
            StringAndIdViewModel elementSeparatorUnknown = _createString();
            elementSeparatorUnknown.Value = ":";
            elementSeparatorUnknown.Name = "Element Separator";
            stringValuesUnknown.Add(elementSeparatorUnknown);
            StringAndIdViewModel repetitionSeparatorUnknown = _createString();
            repetitionSeparatorUnknown.Value = "^";
            repetitionSeparatorUnknown.Name = "Repetition Separator";
            stringValuesUnknown.Add(repetitionSeparatorUnknown);


            toAddUnknown.StringValues = stringValuesUnknown;
            toAddUnknown.CanDelete = true;
            toAddUnknown.Name = namePropertyUnknown.Value;

            list.Add(toAddHM);
            list.Add(toAddWebMD);
            list.Add(toAddHGSA);
            list.Add(toAddUnknown);

            toReturn.Id = 0;
            toReturn.Name = "Claim File Receivers";
            toReturn.List = list;
            return toReturn;

        }

        private SetupListViewModel CreateInsurerBusinessClasses()
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            ItemAndDetailsViewModel toAddAmerihealth = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesAmerihealth = _createStringCollection();
            stringValuesAmerihealth.Add(new StringAndIdViewModel { Name = "Name", Value = "Amerihealth" });
            toAddAmerihealth.StringValues = stringValuesAmerihealth;
            toAddAmerihealth.CanDelete = true;
            toAddAmerihealth.CanArchive = true;
            toAddAmerihealth.Name = "Amerihealth";

            ItemAndDetailsViewModel toAddBlueCrossBlueShield = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesBlueCrossBlueShield = _createStringCollection();
            stringValuesBlueCrossBlueShield.Add(new StringAndIdViewModel { Name = "Name", Value = "BlueCrossBlueShield" });
            toAddBlueCrossBlueShield.StringValues = stringValuesBlueCrossBlueShield;
            toAddBlueCrossBlueShield.CanDelete = true;
            toAddBlueCrossBlueShield.CanArchive = true;
            toAddBlueCrossBlueShield.Name = "BlueCrossBlueShield";


            ItemAndDetailsViewModel toAddCommercial = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesCommercial = _createStringCollection();
            stringValuesCommercial.Add(new StringAndIdViewModel { Name = "Name", Value = "Commercial" });
            toAddCommercial.StringValues = stringValuesCommercial;
            toAddCommercial.CanDelete = true;
            toAddCommercial.CanArchive = true;
            toAddCommercial.Name = "Commercial";


            ItemAndDetailsViewModel toAddMedicaidFL = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicaidFL = _createStringCollection();
            stringValuesMedicaidFL.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicaidFL" });
            toAddMedicaidFL.StringValues = stringValuesMedicaidFL;
            toAddMedicaidFL.CanDelete = true;
            toAddMedicaidFL.CanArchive = true;
            toAddMedicaidFL.Name = "MedicaidFL";



            ItemAndDetailsViewModel toAddMedicaidNC = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicaidNC = _createStringCollection();
            stringValuesMedicaidNC.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicaidNC" });
            toAddMedicaidNC.StringValues = stringValuesMedicaidNC;
            toAddMedicaidNC.CanDelete = true;
            toAddMedicaidNC.CanArchive = true;
            toAddMedicaidNC.Name = "MedicaidNC";

            ItemAndDetailsViewModel toAddMedicaidNJ = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicaidNJ = _createStringCollection();
            stringValuesMedicaidNJ.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicaidNJ" });
            toAddMedicaidNJ.StringValues = stringValuesMedicaidNJ;
            toAddMedicaidNJ.CanDelete = true;
            toAddMedicaidNJ.CanArchive = true;
            toAddMedicaidNJ.Name = "MedicaidNJ";

            ItemAndDetailsViewModel toAddMedicaidNV = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicaidNV = _createStringCollection();
            stringValuesMedicaidNV.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicaidNV" });
            toAddMedicaidNV.StringValues = stringValuesMedicaidNV;
            toAddMedicaidNV.CanDelete = true;
            toAddMedicaidNV.CanArchive = true;
            toAddMedicaidNV.Name = "MedicaidNV";

            ItemAndDetailsViewModel toAddMedicaidNY = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicaidNY = _createStringCollection();
            stringValuesMedicaidNY.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicaidNY" });
            toAddMedicaidNY.StringValues = stringValuesMedicaidNY;
            toAddMedicaidNY.CanDelete = true;
            toAddMedicaidNY.CanArchive = true;
            toAddMedicaidNY.Name = "MedicaidNY";


            ItemAndDetailsViewModel toAddMedicareNJ = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicareNJ = _createStringCollection();
            stringValuesMedicareNJ.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicareNJ" });
            toAddMedicareNJ.StringValues = stringValuesMedicareNJ;
            toAddMedicareNJ.CanDelete = true;
            toAddMedicareNJ.CanArchive = true;
            toAddMedicareNJ.Name = "MedicareNJ";

            ItemAndDetailsViewModel toAddMedicareNV = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicareNV = _createStringCollection();
            stringValuesMedicareNV.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicareNV" });
            toAddMedicareNV.StringValues = stringValuesMedicareNV;
            toAddMedicareNV.CanDelete = true;
            toAddMedicareNV.CanArchive = true;
            toAddMedicareNV.Name = "MedicareNV";

            ItemAndDetailsViewModel toAddMedicareNY = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMedicareNY = _createStringCollection();
            stringValuesMedicareNY.Add(new StringAndIdViewModel { Name = "Name", Value = "MedicareNY" });
            toAddMedicareNY.StringValues = stringValuesMedicareNY;
            toAddMedicareNY.CanDelete = true;
            toAddMedicareNY.CanArchive = true;
            toAddMedicareNY.Name = "MedicareNY";

            ItemAndDetailsViewModel toAddBLUENV = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesBLUENV = _createStringCollection();
            stringValuesBLUENV.Add(new StringAndIdViewModel { Name = "Name", Value = "BLUENV" });
            toAddBLUENV.StringValues = stringValuesMedicareNV;
            toAddBLUENV.CanDelete = true;
            toAddBLUENV.CanArchive = true;
            toAddBLUENV.Name = "BLUENV";


            ItemAndDetailsViewModel toAddGHI = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesGHI = _createStringCollection();
            stringValuesGHI.Add(new StringAndIdViewModel { Name = "Name", Value = "GHI" });
            toAddGHI.StringValues = stringValuesMedicareNV;
            toAddGHI.CanDelete = true;
            toAddGHI.CanArchive = true;
            toAddGHI.Name = "GHI";

            list.Add(toAddAmerihealth);
            list.Add(toAddBlueCrossBlueShield);
            list.Add(toAddBLUENV);
            list.Add(toAddCommercial);
            list.Add(toAddGHI);
            list.Add(toAddMedicaidFL);
            list.Add(toAddMedicaidNC);
            list.Add(toAddMedicaidNJ);
            list.Add(toAddMedicaidNV);
            list.Add(toAddMedicaidNY);
            list.Add(toAddMedicareNJ);
            list.Add(toAddMedicareNV);
            list.Add(toAddMedicareNY);


            toReturn.Id = 0;
            toReturn.Name = "Insurer Business Classes";
            toReturn.List = list;
            return toReturn;

        }

        private SetupListViewModel CreateInsurerPlanTypes()
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            ItemAndDetailsViewModel toAddHMO = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesHMO = _createStringCollection();
            StringAndIdViewModel namePropertyHMO = _createString();
            namePropertyHMO.Value = "HMO";
            namePropertyHMO.Name = "Name";
            stringValuesHMO.Add(namePropertyHMO);

            toAddHMO.StringValues = stringValuesHMO;
            toAddHMO.CanDelete = true;
            toAddHMO.Name = namePropertyHMO.Value;

            list.Add(toAddHMO);

            ItemAndDetailsViewModel toAddIndemnmity = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesIndemnity = _createStringCollection();
            StringAndIdViewModel namePropertyIndemnity = _createString();
            namePropertyIndemnity.Value = "Indemnity";
            namePropertyIndemnity.Name = "Name";
            stringValuesIndemnity.Add(namePropertyIndemnity);

            toAddIndemnmity.StringValues = stringValuesIndemnity;
            toAddIndemnmity.CanDelete = true;
            toAddIndemnmity.Name = namePropertyIndemnity.Value;

            list.Add(toAddIndemnmity);

            ItemAndDetailsViewModel toAddOther = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesOther = _createStringCollection();
            StringAndIdViewModel namePropertyOther = _createString();
            namePropertyOther.Value = "Other";
            namePropertyOther.Name = "Name";
            stringValuesOther.Add(namePropertyOther);

            toAddOther.StringValues = stringValuesOther;
            toAddOther.CanDelete = true;
            toAddOther.Name = namePropertyOther.Value;

            list.Add(toAddOther);

            ItemAndDetailsViewModel toAddPOS = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesPOS = _createStringCollection();
            StringAndIdViewModel namePropertyPOS = _createString();
            namePropertyPOS.Value = "POS";
            namePropertyPOS.Name = "Name";
            stringValuesPOS.Add(namePropertyPOS);

            toAddPOS.StringValues = stringValuesPOS;
            toAddPOS.CanDelete = true;
            toAddPOS.Name = namePropertyPOS.Value;

            list.Add(toAddPOS);

            ItemAndDetailsViewModel toAddPPO = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesPPO = _createStringCollection();
            StringAndIdViewModel namePropertyPPO = _createString();
            namePropertyPPO.Value = "PPO";
            namePropertyPPO.Name = "Name";
            stringValuesPPO.Add(namePropertyPPO);

            toAddPPO.StringValues = stringValuesPPO;
            toAddPPO.CanDelete = true;
            toAddPPO.Name = namePropertyPPO.Value;

            list.Add(toAddPPO);

            ItemAndDetailsViewModel toAddEPO = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesEPO = _createStringCollection();
            StringAndIdViewModel namePropertyEPO = _createString();
            namePropertyEPO.Value = "EPO";
            namePropertyEPO.Name = "Name";
            stringValuesEPO.Add(namePropertyEPO);

            toAddEPO.StringValues = stringValuesEPO;
            toAddEPO.CanDelete = true;
            toAddEPO.Name = namePropertyEPO.Value;

            list.Add(toAddEPO);

            ItemAndDetailsViewModel toAddMCARE = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesMCARE = _createStringCollection();
            StringAndIdViewModel namePropertyMCARE = _createString();
            namePropertyMCARE.Value = "MCARE";
            namePropertyMCARE.Name = "Name";
            stringValuesMCARE.Add(namePropertyMCARE);

            toAddMCARE.StringValues = stringValuesMCARE;
            toAddMCARE.CanDelete = true;
            toAddMCARE.Name = namePropertyMCARE.Value;

            list.Add(toAddMCARE);

            ItemAndDetailsViewModel toAddBlank = _createItemAndDetailsViewModel();

            ObservableCollection<StringAndIdViewModel> stringValuesBlank = _createStringCollection();
            StringAndIdViewModel namePropertyBlank = _createString();
            namePropertyBlank.Value = "";
            namePropertyBlank.Name = "Name";
            stringValuesBlank.Add(namePropertyBlank);

            toAddBlank.StringValues = stringValuesBlank;
            toAddBlank.CanDelete = true;
            toAddBlank.Name = namePropertyBlank.Value;

            list.Add(toAddBlank);

            toReturn.Id = 0;
            toReturn.Name = "Insurer Plan Types";
            toReturn.List = list;
            return toReturn;

        }



        private SetupListViewModel CreatePaymentMethods()
        {

            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            ItemAndDetailsViewModel toAddAmEx = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesAmEx = _createStringCollection();
            stringValuesAmEx.Add(new StringAndIdViewModel { Name = "Name", Value = "American Express" });
            toAddAmEx.StringValues = stringValuesAmEx;
            toAddAmEx.CanDelete = true;
            toAddAmEx.CanArchive = true;
            toAddAmEx.Name = "American Express";
            list.Add(toAddAmEx);

            ItemAndDetailsViewModel toAddCash = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesCash = _createStringCollection();
            stringValuesCash.Add(new StringAndIdViewModel { Name = "Name", Value = "Cash" });
            toAddCash.StringValues = stringValuesCash;
            toAddCash.CanDelete = true;
            toAddCash.CanArchive = true;
            toAddCash.Name = "Cash";
            list.Add(toAddCash);

            ItemAndDetailsViewModel toAddCheck = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesCheck = _createStringCollection();
            stringValuesCheck.Add(new StringAndIdViewModel { Name = "Name", Value = "Check" });
            toAddCheck.StringValues = stringValuesCheck;
            toAddCheck.CanDelete = true;
            toAddCheck.CanArchive = true;
            toAddCheck.Name = "Check";
            list.Add(toAddCheck);

            ItemAndDetailsViewModel toAddDebitCard = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesDebitCard = _createStringCollection();
            stringValuesDebitCard.Add(new StringAndIdViewModel { Name = "Name", Value = "Debit Card" });
            toAddDebitCard.StringValues = stringValuesDebitCard;
            toAddDebitCard.CanDelete = true;
            toAddDebitCard.CanArchive = true;
            toAddDebitCard.Name = "Debit Card";
            list.Add(toAddDebitCard);

            ItemAndDetailsViewModel toAddInsCheck = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesInsCheck = _createStringCollection();
            stringValuesInsCheck.Add(new StringAndIdViewModel { Name = "Name", Value = "Endorsed Insurance Check" });
            toAddInsCheck.StringValues = stringValuesInsCheck;
            toAddInsCheck.CanDelete = true;
            toAddInsCheck.CanArchive = true;
            toAddInsCheck.Name = "Endorsed Insurance Check";
            list.Add(toAddInsCheck);

            ItemAndDetailsViewModel toAddMasterCard = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMasterCard = _createStringCollection();
            stringValuesMasterCard.Add(new StringAndIdViewModel { Name = "Name", Value = "MasterCard" });
            toAddMasterCard.StringValues = stringValuesMasterCard;
            toAddMasterCard.CanDelete = true;
            toAddMasterCard.CanArchive = true;
            toAddMasterCard.Name = "MasterCard";
            list.Add(toAddMasterCard);

            ItemAndDetailsViewModel toAddMoneyOrder = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesMoneyOrder = _createStringCollection();
            stringValuesMoneyOrder.Add(new StringAndIdViewModel { Name = "Name", Value = "Money Order" });
            toAddMoneyOrder.StringValues = stringValuesMoneyOrder;
            toAddMoneyOrder.CanDelete = true;
            toAddMoneyOrder.CanArchive = true;
            toAddMoneyOrder.Name = "Money Order";
            list.Add(toAddMoneyOrder);

            ItemAndDetailsViewModel toAddOther = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesOther = _createStringCollection();
            stringValuesOther.Add(new StringAndIdViewModel { Name = "Name", Value = "Other" });
            toAddOther.StringValues = stringValuesOther;
            toAddOther.CanDelete = true;
            toAddOther.CanArchive = true;
            toAddOther.Name = "Other";
            list.Add(toAddOther);

            ItemAndDetailsViewModel toAddVISA = _createItemAndDetailsViewModel();
            ObservableCollection<StringAndIdViewModel> stringValuesVISA = _createStringCollection();
            stringValuesVISA.Add(new StringAndIdViewModel { Name = "Name", Value = "VISA" });
            toAddVISA.StringValues = stringValuesVISA;
            toAddVISA.CanDelete = true;
            toAddVISA.CanArchive = true;
            toAddVISA.Name = "VISA";
            list.Add(toAddVISA);

            toReturn.Id = 0;
            toReturn.Name = "Payment Methods";
            toReturn.List = list;
            return toReturn;
        }

        private SetupListViewModel CreateAdjustmentTypeSetup(int n)
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            ItemAndDetailsViewModel paymentToAdd = _createItemAndDetailsViewModel();

            ObservableCollection<BoolAndIdViewModel> paymentBooleanValues = _createBoolCollection();
            BoolAndIdViewModel paymentBoolProperty1 = _createBool();
            paymentBoolProperty1.Value = true;
            paymentBoolProperty1.Name = "Affects Cash";
            paymentBooleanValues.Add(paymentBoolProperty1);
            BoolAndIdViewModel paymentBoolProperty2 = _createBool();
            paymentBoolProperty2.Value = true;
            paymentBoolProperty2.Name = "Decreases Balance";
            paymentBooleanValues.Add(paymentBoolProperty2);
            BoolAndIdViewModel paymentBoolProperty3 = _createBool();
            paymentBoolProperty3.Value = true;
            paymentBoolProperty3.Name = "Print on Statements";
            paymentBooleanValues.Add(paymentBoolProperty3);

            ObservableCollection<BoolAndIdViewModel> onOffPaymentBooleanValues = _createBoolCollection();
            BoolAndIdViewModel paymentBoolProperty4 = _createBool();
            paymentBoolProperty4.Value = true;
            paymentBoolProperty4.Name = "Insurer Payments";
            onOffPaymentBooleanValues.Add(paymentBoolProperty4);
            BoolAndIdViewModel paymentBoolProperty5 = _createBool();
            paymentBoolProperty5.Value = true;
            paymentBoolProperty5.Name = "Patient Payments";
            onOffPaymentBooleanValues.Add(paymentBoolProperty5);
            BoolAndIdViewModel paymentBoolProperty6 = _createBool();
            paymentBoolProperty6.Value = true;
            paymentBoolProperty6.Name = "Office Payments";
            onOffPaymentBooleanValues.Add(paymentBoolProperty6);

            ObservableCollection<StringAndIdViewModel> paymentStringValues = _createStringCollection();
            StringAndIdViewModel paymenttringProperty1 = _createString();
            paymenttringProperty1.Value = "Payment";
            paymenttringProperty1.Name = "Name";
            paymentStringValues.Add(paymenttringProperty1);
            StringAndIdViewModel paymentStringProperty2 = _createString();
            paymentStringProperty2.Value = "PYMT";
            paymentStringProperty2.Name = "Abbreviation";
            paymentStringValues.Add(paymentStringProperty2);

            ObservableCollection<CollectionAndSelectedItemAndIdViewModel> paymentForeignKeyValues = _createCollectionAndSelectedItemAndIdViewModelCollection();
            CollectionAndSelectedItemAndIdViewModel paymentForeignKeyProperty1 = _createCollectionAndSelectedItemAndIdViewModel();
            paymentForeignKeyProperty1.Collection = CreateFinancialTypeGroups();
            paymentForeignKeyProperty1.SelectedItem = paymentForeignKeyProperty1.Collection[2];
            paymentForeignKeyProperty1.Name = "Financial Type Group";
            paymentForeignKeyValues.Add(paymentForeignKeyProperty1);

            ObservableCollection<TwoManyToManyViewModel> paymentManyToManyObjects = _createTwoManyToManyViewModelCollection();
            TwoManyToManyViewModel paymentManyToManyObject1 = _createTwoManyToManyViewModel();
            paymentManyToManyObject1.Name = "Group Code - Reason Code";

            paymentManyToManyObjects.Add(paymentManyToManyObject1);

            paymentToAdd.BooleanValues = paymentBooleanValues;
            paymentToAdd.OnOffValues = onOffPaymentBooleanValues;
            paymentToAdd.StringValues = paymentStringValues;
            paymentToAdd.ForeignKeyValues = paymentForeignKeyValues;
            paymentToAdd.TwoManyToManyValues = paymentManyToManyObjects;

            paymentToAdd.CanDelete = false;
            paymentToAdd.CanArchive = true;
            paymentToAdd.CanReorder = true;
            paymentToAdd.Name = "Payment";

            list.Add(paymentToAdd);

            ItemAndDetailsViewModel contractualAdjustmentToAdd = _createItemAndDetailsViewModel();

            ObservableCollection<BoolAndIdViewModel> contractualAdjustmentBooleanValues = _createBoolCollection();
            BoolAndIdViewModel contractualAdjustmentBoolProperty1 = _createBool();
            contractualAdjustmentBoolProperty1.Value = true;
            contractualAdjustmentBoolProperty1.Name = "Affects Cash";
            contractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty1);
            BoolAndIdViewModel contractualAdjustmentBoolProperty2 = _createBool();
            contractualAdjustmentBoolProperty2.Value = false;
            contractualAdjustmentBoolProperty2.Name = "Decreases Balance";
            contractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty2);
            BoolAndIdViewModel contractualAdjustmentBoolProperty3 = _createBool();
            contractualAdjustmentBoolProperty3.Value = true;
            contractualAdjustmentBoolProperty3.Name = "Print on Statements";
            contractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty3);

            ObservableCollection<BoolAndIdViewModel> onOffContractualAdjustmentBooleanValues = _createBoolCollection();
            BoolAndIdViewModel contractualAdjustmentBoolProperty4 = _createBool();
            contractualAdjustmentBoolProperty4.Value = true;
            contractualAdjustmentBoolProperty4.Name = "Insurer Payments";
            onOffContractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty4);
            BoolAndIdViewModel contractualAdjustmentBoolProperty5 = _createBool();
            contractualAdjustmentBoolProperty5.Value = true;
            contractualAdjustmentBoolProperty5.Name = "Patient Payments";
            onOffContractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty5);
            BoolAndIdViewModel contractualAdjustmentBoolProperty6 = _createBool();
            contractualAdjustmentBoolProperty6.Value = true;
            contractualAdjustmentBoolProperty6.Name = "Office Payments";
            onOffContractualAdjustmentBooleanValues.Add(contractualAdjustmentBoolProperty6);

            ObservableCollection<StringAndIdViewModel> contractualAdjustmentStringValues = _createStringCollection();
            StringAndIdViewModel contractualAdjustmenttringProperty1 = _createString();
            contractualAdjustmenttringProperty1.Value = "Contractual Adjustment";
            contractualAdjustmenttringProperty1.Name = "Name";
            contractualAdjustmentStringValues.Add(contractualAdjustmenttringProperty1);
            StringAndIdViewModel contractualAdjustmentStringProperty2 = _createString();
            contractualAdjustmentStringProperty2.Value = "CTRWO";
            contractualAdjustmentStringProperty2.Name = "Abbreviation";
            contractualAdjustmentStringValues.Add(contractualAdjustmentStringProperty2);

            ObservableCollection<CollectionAndSelectedItemAndIdViewModel> contractualAdjustmentForeignKeyValues = _createCollectionAndSelectedItemAndIdViewModelCollection();
            CollectionAndSelectedItemAndIdViewModel contractualAdjustmentForeignKeyProperty1 = _createCollectionAndSelectedItemAndIdViewModel();
            contractualAdjustmentForeignKeyProperty1.Collection = CreateFinancialTypeGroups();
            contractualAdjustmentForeignKeyProperty1.SelectedItem = contractualAdjustmentForeignKeyProperty1.Collection[1];
            contractualAdjustmentForeignKeyProperty1.Name = "Financial Type Group";
            contractualAdjustmentForeignKeyValues.Add(contractualAdjustmentForeignKeyProperty1);


            ObservableCollection<TwoManyToManyViewModel> contractualAdjustmentManyToManyObjects = _createTwoManyToManyViewModelCollection();

            ObservableCollection<TwoCollectionAndSelectedItemAndIdViewModel> contractualAdjustmentGroupCodeReasonCodeValues = _createTwoCollectionAndSelectedItemAndIdViewModelCollection();
            TwoCollectionAndSelectedItemAndIdViewModel contractualAdjustmentGroupCodeReasonCodeValue1 = _createTwoCollectionAndSelectedItemAndIdViewModel();
            contractualAdjustmentGroupCodeReasonCodeValue1.Collection1 = CreateGroupCodes();
            contractualAdjustmentGroupCodeReasonCodeValue1.Collection2 = CreateReasonCodes();
            contractualAdjustmentGroupCodeReasonCodeValue1.SelectedItem1 = contractualAdjustmentGroupCodeReasonCodeValue1.Collection1[0];
            contractualAdjustmentGroupCodeReasonCodeValue1.SelectedItem2 = contractualAdjustmentGroupCodeReasonCodeValue1.Collection2[3];
            contractualAdjustmentGroupCodeReasonCodeValues.Add(contractualAdjustmentGroupCodeReasonCodeValue1);

            TwoCollectionAndSelectedItemAndIdViewModel contractualAdjustmentGroupCodeReasonCodeValue2 = _createTwoCollectionAndSelectedItemAndIdViewModel();
            contractualAdjustmentGroupCodeReasonCodeValue2.Collection1 = CreateGroupCodes();
            contractualAdjustmentGroupCodeReasonCodeValue2.Collection2 = CreateReasonCodes();
            contractualAdjustmentGroupCodeReasonCodeValue2.SelectedItem1 = contractualAdjustmentGroupCodeReasonCodeValue1.Collection1[0];
            contractualAdjustmentGroupCodeReasonCodeValue2.SelectedItem2 = contractualAdjustmentGroupCodeReasonCodeValue1.Collection2[8];
            contractualAdjustmentGroupCodeReasonCodeValues.Add(contractualAdjustmentGroupCodeReasonCodeValue2);

            TwoManyToManyViewModel contractualAdjustmentManyToManyObject1 = _createTwoManyToManyViewModel();
            contractualAdjustmentManyToManyObject1.Name = "Group Code - Reason Code";
            contractualAdjustmentManyToManyObject1.LinkedObjects = contractualAdjustmentGroupCodeReasonCodeValues;

            contractualAdjustmentManyToManyObjects.Add(contractualAdjustmentManyToManyObject1);


            contractualAdjustmentToAdd.BooleanValues = contractualAdjustmentBooleanValues;
            contractualAdjustmentToAdd.OnOffValues = onOffContractualAdjustmentBooleanValues;
            contractualAdjustmentToAdd.StringValues = contractualAdjustmentStringValues;
            contractualAdjustmentToAdd.ForeignKeyValues = contractualAdjustmentForeignKeyValues;
            contractualAdjustmentToAdd.TwoManyToManyValues = contractualAdjustmentManyToManyObjects;

            contractualAdjustmentToAdd.CanDelete = false;
            contractualAdjustmentToAdd.CanArchive = true;
            contractualAdjustmentToAdd.CanReorder = true;
            contractualAdjustmentToAdd.Name = "Contractual Adjustment";

            list.Add(contractualAdjustmentToAdd);



            for (int i = 0; i < n; i++)
            {
                ItemAndDetailsViewModel toAdd = _createItemAndDetailsViewModel();

                ObservableCollection<BoolAndIdViewModel> booleanValues = _createBoolCollection();
                BoolAndIdViewModel boolProperty1 = _createBool();
                boolProperty1.Value = false;
                boolProperty1.Name = "Affects Cash";
                booleanValues.Add(boolProperty1);
                BoolAndIdViewModel boolProperty2 = _createBool();
                boolProperty2.Value = (i % 3 == 0);
                boolProperty2.Name = "Decreases Balance";
                booleanValues.Add(boolProperty2);
                BoolAndIdViewModel boolProperty3 = _createBool();
                boolProperty3.Value = true;
                boolProperty3.Name = "Print on Statements";
                booleanValues.Add(boolProperty3);

                ObservableCollection<BoolAndIdViewModel> onOffValues = _createBoolCollection();
                BoolAndIdViewModel boolProperty4 = _createBool();
                boolProperty4.Value = true;
                boolProperty4.Name = "Insurer Payments";
                onOffValues.Add(boolProperty4);
                BoolAndIdViewModel boolProperty5 = _createBool();
                boolProperty5.Value = true;
                boolProperty5.Name = "Patient Payments";
                onOffValues.Add(boolProperty5);
                BoolAndIdViewModel boolProperty6 = _createBool();
                boolProperty6.Value = true;
                boolProperty6.Name = "Office Payments";
                onOffValues.Add(boolProperty6);

                ObservableCollection<StringAndIdViewModel> stringValues = _createStringCollection();
                StringAndIdViewModel stringProperty1 = _createString();
                stringProperty1.Value = "Custom Adjustment " + i;
                stringProperty1.Name = "Name";
                stringValues.Add(stringProperty1);
                StringAndIdViewModel stringProperty2 = _createString();
                stringProperty2.Value = "Adj " + i;
                stringProperty2.Name = "Abbreviation";
                stringValues.Add(stringProperty2);

                ObservableCollection<CollectionAndSelectedItemAndIdViewModel> foreignKeyValues = _createCollectionAndSelectedItemAndIdViewModelCollection();
                CollectionAndSelectedItemAndIdViewModel foreignKeyProperty1 = _createCollectionAndSelectedItemAndIdViewModel();
                foreignKeyProperty1.Collection = CreateFinancialTypeGroups();
                foreignKeyProperty1.SelectedItem = foreignKeyProperty1.Collection[(i % 2)];
                foreignKeyProperty1.Name = "Financial Type Group";
                foreignKeyValues.Add(foreignKeyProperty1);

                ObservableCollection<TwoManyToManyViewModel> objectManyToManyObjects = _createTwoManyToManyViewModelCollection();

                ObservableCollection<TwoCollectionAndSelectedItemAndIdViewModel> objectGroupCodeReasonCodeValues = _createTwoCollectionAndSelectedItemAndIdViewModelCollection();
                TwoCollectionAndSelectedItemAndIdViewModel objectGroupCodeReasonCodeValue1 = _createTwoCollectionAndSelectedItemAndIdViewModel();
                objectGroupCodeReasonCodeValue1.Collection1 = CreateGroupCodes();
                objectGroupCodeReasonCodeValue1.Collection2 = CreateReasonCodes();
                objectGroupCodeReasonCodeValue1.SelectedItem1 = objectGroupCodeReasonCodeValue1.Collection1[(i * 2) % 5];
                objectGroupCodeReasonCodeValue1.SelectedItem2 = objectGroupCodeReasonCodeValue1.Collection2[(i % 123) + 123];
                objectGroupCodeReasonCodeValues.Add(objectGroupCodeReasonCodeValue1);

                TwoCollectionAndSelectedItemAndIdViewModel objectGroupCodeReasonCodeValue2 = _createTwoCollectionAndSelectedItemAndIdViewModel();
                objectGroupCodeReasonCodeValue2.Collection1 = CreateGroupCodes();
                objectGroupCodeReasonCodeValue2.Collection2 = CreateReasonCodes();
                objectGroupCodeReasonCodeValue2.SelectedItem1 = objectGroupCodeReasonCodeValue1.Collection1[(i * 4) % 3 + 1];
                objectGroupCodeReasonCodeValue2.SelectedItem2 = objectGroupCodeReasonCodeValue1.Collection2[(i % 89) + 89];
                objectGroupCodeReasonCodeValues.Add(objectGroupCodeReasonCodeValue2);

                TwoCollectionAndSelectedItemAndIdViewModel objectGroupCodeReasonCodeValue4 = _createTwoCollectionAndSelectedItemAndIdViewModel();
                objectGroupCodeReasonCodeValue4.Collection1 = CreateGroupCodes();
                objectGroupCodeReasonCodeValue4.Collection2 = CreateReasonCodes();
                objectGroupCodeReasonCodeValue4.SelectedItem1 = objectGroupCodeReasonCodeValue1.Collection1[i % 2 + 2];
                objectGroupCodeReasonCodeValue4.SelectedItem2 = objectGroupCodeReasonCodeValue1.Collection2[(i % 234) + 21];
                objectGroupCodeReasonCodeValues.Add(objectGroupCodeReasonCodeValue4);

                TwoManyToManyViewModel manyToManyObject1 = _createTwoManyToManyViewModel();
                manyToManyObject1.Name = "Group Code - Reason Code";
                manyToManyObject1.LinkedObjects = objectGroupCodeReasonCodeValues;

                objectManyToManyObjects.Add(manyToManyObject1);

                toAdd.BooleanValues = booleanValues;
                toAdd.OnOffValues = onOffValues;
                toAdd.StringValues = stringValues;
                toAdd.ForeignKeyValues = foreignKeyValues;
                toAdd.TwoManyToManyValues = objectManyToManyObjects;
                toAdd.CanDelete = true;
                toAdd.CanArchive = true;
                toAdd.CanReorder = true;
                toAdd.Name = "Item" + i;

                list.Add(toAdd);
            }
            toReturn.Id = 0;
            toReturn.Name = "Adjustment Types";
            toReturn.List = list;
            return toReturn;
        }

        private SetupListViewModel CreateFinancialInformationTypeSetup(int n)
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            for (int i = 0; i < n; i++)
            {
                ItemAndDetailsViewModel toAdd = _createItemAndDetailsViewModel();

                ObservableCollection<BoolAndIdViewModel> booleanValues = _createBoolCollection();
                BoolAndIdViewModel boolProperty1 = _createBool();
                boolProperty1.Value = true;
                boolProperty1.Name = "Property 5 name";
                booleanValues.Add(boolProperty1);
                BoolAndIdViewModel boolProperty2 = _createBool();
                boolProperty2.Value = true;
                boolProperty2.Name = "Property 6 name";
                booleanValues.Add(boolProperty2);
                BoolAndIdViewModel boolProperty3 = _createBool();
                boolProperty3.Value = true;
                boolProperty3.Name = "Property 7 name";
                booleanValues.Add(boolProperty3);

                ObservableCollection<StringAndIdViewModel> stringValues = _createStringCollection();
                StringAndIdViewModel stringProperty1 = _createString();
                stringProperty1.Value = "Item" + i;
                stringProperty1.Name = "Name";
                stringValues.Add(stringProperty1);
                StringAndIdViewModel stringProperty2 = _createString();
                stringProperty2.Value = "User's value for this property";
                stringProperty2.Name = "Property 2";
                stringValues.Add(stringProperty2);
                StringAndIdViewModel stringProperty2A = _createString();
                stringProperty2A.Value = "User's value for this property";
                stringProperty2A.Name = "Property 3";
                stringValues.Add(stringProperty2A);

                ObservableCollection<CollectionAndSelectedItemAndIdViewModel> foreignKeyValues = _createCollectionAndSelectedItemAndIdViewModelCollection();
                CollectionAndSelectedItemAndIdViewModel foreignKeyProperty1 = _createCollectionAndSelectedItemAndIdViewModel();
                foreignKeyProperty1.Collection = CreateNamedViewModel(50);
                foreignKeyProperty1.SelectedItem = foreignKeyProperty1.Collection[0];
                foreignKeyProperty1.Name = "Property 3";
                foreignKeyValues.Add(foreignKeyProperty1);
                CollectionAndSelectedItemAndIdViewModel foreignKeyProperty2 = _createCollectionAndSelectedItemAndIdViewModel();
                foreignKeyProperty2.Collection = CreateNamedViewModel(10);
                foreignKeyProperty2.Name = "Property 4";
                foreignKeyValues.Add(foreignKeyProperty2);

                toAdd.BooleanValues = booleanValues;
                toAdd.StringValues = stringValues;
                toAdd.ForeignKeyValues = foreignKeyValues;
                toAdd.CanDelete = true;
                toAdd.Name = "Item" + i;

                list.Add(toAdd);
            }
            toReturn.Id = 0;
            toReturn.Name = "Financial Information Types";
            toReturn.List = list;
            return toReturn;
        }





        private SetupListViewModel CreateReasonCodes(int n)
        {
            SetupListViewModel toReturn = _createSetupListViewModel();
            ObservableCollection<ItemAndDetailsViewModel> list = _createItemAndDetailsViewModelCollection();

            for (int i = 0; i < n; i++)
            {
                ItemAndDetailsViewModel toAdd = _createItemAndDetailsViewModel();

                ObservableCollection<BoolAndIdViewModel> booleanValues = _createBoolCollection();
                BoolAndIdViewModel boolProperty1 = _createBool();
                boolProperty1.Value = true;
                boolProperty1.Name = "Property 3 name";
                booleanValues.Add(boolProperty1);

                ObservableCollection<StringAndIdViewModel> stringValues = _createStringCollection();
                StringAndIdViewModel stringProperty1 = _createString();
                stringProperty1.Value = "Item" + i;
                stringProperty1.Name = "Name";
                stringValues.Add(stringProperty1);
                StringAndIdViewModel stringProperty2 = _createString();
                stringProperty2.Value = "User's value for this property";
                stringProperty2.Name = "Property 2";
                stringValues.Add(stringProperty2);

                ObservableCollection<CollectionAndSelectedItemAndIdViewModel> foreignKeyValues = _createCollectionAndSelectedItemAndIdViewModelCollection();
                CollectionAndSelectedItemAndIdViewModel foreignKeyProperty1 = _createCollectionAndSelectedItemAndIdViewModel();
                foreignKeyProperty1.Collection = CreateNamedViewModel(50);
                foreignKeyProperty1.SelectedItem = foreignKeyProperty1.Collection[0];
                foreignKeyProperty1.Name = "Property 3";
                foreignKeyValues.Add(foreignKeyProperty1);
                CollectionAndSelectedItemAndIdViewModel foreignKeyProperty2 = _createCollectionAndSelectedItemAndIdViewModel();
                foreignKeyProperty2.Collection = CreateNamedViewModel(10);
                foreignKeyProperty2.Name = "Property 4";
                foreignKeyValues.Add(foreignKeyProperty2);

                toAdd.BooleanValues = booleanValues;
                toAdd.StringValues = stringValues;
                toAdd.ForeignKeyValues = foreignKeyValues;

                list.Add(toAdd);
            }

            toReturn.Id = 0;
            toReturn.Name = "Reason Codes";
            toReturn.List = list;
            return toReturn;
        }


        private ObservableCollection<NamedViewModel> CreateFinancialTypeGroups()
        {
            ObservableCollection<NamedViewModel> toReturn = _createNamedViewModelCollection();

            var toAdd1 = _createNamedViewModel();
            toAdd1.Name = "Insurer Adjustments";
            toReturn.Add(toAdd1);

            var toAdd2 = _createNamedViewModel();
            toAdd2.Name = "Office Adjustments";
            toReturn.Add(toAdd2);

            var toAdd3 = _createNamedViewModel();
            toAdd3.Name = "Payment Adjustments";
            toReturn.Add(toAdd3);

            return toReturn;
        }

        private ObservableCollection<NamedViewModel> CreateNamedViewModel(int n)
        {
            ObservableCollection<NamedViewModel> toReturn = _createNamedViewModelCollection();

            for (int i = 0; i < n; i++)
            {
                var toAdd = _createNamedViewModel();
                toAdd.Name = "item" + i;

                toReturn.Add(toAdd);
            }

            return toReturn;
        }


        private ObservableCollection<NamedViewModel> CreateGroupCodes()
        {
            ObservableCollection<NamedViewModel> toReturn = _createNamedViewModelCollection();

            var co = _createNamedViewModel();
            co.Name = "CO";
            toReturn.Add(co);

            var pr = _createNamedViewModel();
            pr.Name = "PR";
            toReturn.Add(pr);

            var oa = _createNamedViewModel();
            oa.Name = "OA";
            toReturn.Add(oa);

            var cr = _createNamedViewModel();
            cr.Name = "CR";
            toReturn.Add(cr);

            var pi = _createNamedViewModel();
            pi.Name = "PI";
            toReturn.Add(pi);

            return toReturn;
        }


        private ObservableCollection<NamedViewModel> CreateReasonCodes()
        {
            ObservableCollection<NamedViewModel> toReturn = _createNamedViewModelCollection();

            for (int i = 0; i < 300; i++)
            {
                var toAdd = _createNamedViewModel();
                toAdd.Name = i.ToString();

                toReturn.Add(toAdd);
            }

            return toReturn;
        }

        public IInteractionContext InteractionContext { get; set; }

        public IApplicationSetupViewContext ViewContext { get; set; }

        public ICommand Load { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel BillingViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel AdministrativeViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel ClinicalViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel AdminUtilitiesViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual bool ShowAlltabs { get; set; }
    }
}
