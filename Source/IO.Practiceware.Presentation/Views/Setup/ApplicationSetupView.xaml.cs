﻿using System.Windows.Input;
using IO.Practiceware.Presentation.Views.PatientInfo;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Setup
{
    /// <summary>
    /// Interaction logic for GlobalSetupView.xaml
    /// </summary>
    public partial class ApplicationSetupView
    {
        public ApplicationSetupView()
        {
            InitializeComponent();
        }
    }
    
    public class AppplicationSetupViewContextReference : DataContextReference
    {
        public new ApplicationSetupViewContext DataContext
        {
            get { return (ApplicationSetupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    public class ViewContextLoadViewModel : IViewModel
    {
        private bool _hasFired;
        private bool _isSelected;

        /// <summary>
        ///   The command to execute to load view context
        /// </summary>
        public ICommand LoadCommand { get; set; }

        /// <summary>
        ///  The command to execute when the IsSelected property is set to true
        /// </summary>
        public ICommand SelectedCommand { get; set; }

        /// <summary>
        ///   The DataContext associated with this instance
        /// </summary>
        public IApplicationSetupViewContext ViewContext { get; set; }

        /// <summary>
        /// The ApplicationSetup Tab assoiated with this context
        /// </summary>
        public ApplicationSetupTab? ApplicationSetupTab { get; set; }

        /// <summary>
        ///   A flag that when set to true checks whether or not the LoadCommand has fired already, and if so do not allow any further executions.
        /// </summary>
        public bool FireCommandOneTimeOnly { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the ViewContext represented by this viewModel has loaded
        /// </summary>
        public bool HasLoaded
        {
            get { return _hasFired; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsSelected can be changed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowSelectionChange { get; set; }

        [DispatcherThread]
        public virtual bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (!AllowSelectionChange) return;

                _isSelected = value;

                // Make a decision whether load command should be called
                if ((_isSelected && FireCommandOneTimeOnly && !_hasFired) || (_isSelected && !FireCommandOneTimeOnly))
                {
                    if (LoadCommand != null)
                    {
                        LoadCommand.Execute();
                    }

                    _hasFired = true;
                }

                // Execute Selected command
                if (_isSelected && SelectedCommand != null)
                {
                    SelectedCommand.Execute(this);
                }
            }
        }
    }
}
