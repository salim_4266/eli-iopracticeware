﻿using System;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Administrative;
using IO.Practiceware.Presentation.Views.ConfigureCategories;
using IO.Practiceware.Presentation.Views.InsurancePlansSetup;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.Setup.Administrative;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Setup.Administrative
{

    public class AdministrativeSetupViewContext : IViewContext, IApplicationSetupViewContext
    {
        
        private readonly Func<AdministrativeSetupListViewModel> _createSetupListViewModel;
        
  
        private readonly Func<StringAndIdViewModel> _createString;
        private readonly Func<BoolAndIdViewModel> _createBoolean;

        private readonly Func<AdministrativeDetails> _createAdministrativeDetailsViewModel;
        private readonly Func<CollectionAndSelectedItemAndIdViewModel> _createCollectionAndSelectedItemAndIdViewModel;
        private readonly IAdminsitrativeSetupViewService _adminsitrativeSetupService;
        private readonly Func<NamedViewModel> _createNamedViewModel;
     
        public virtual ExtendedObservableCollection<AdministrativeSetupListViewModel> AdminSetupLists { get; set; }

        public virtual AdministrativeSetupListViewModel SelectedList { get; set; }
        public virtual string AddItemText { get; set; }
        private readonly IInteractionManager _interactionManager;

        public AdministrativeSetupViewContext(
            Func<AdministrativeSetupListViewModel> createSetupListViewModel,
            Func<AdministrativeDetails> createItemAndDetailsViewModel,
            Func<StringAndIdViewModel> createString,
            Func<BoolAndIdViewModel> createBoolean,
            Func<NamedViewModel> createNamedViewModel,
            Func<CollectionAndSelectedItemAndIdViewModel> createCollectionAndSelectedItemAndIdViewModel,
            IAdminsitrativeSetupViewService adminsitrativeSetupService, IInteractionManager interactionManager)
        {
            _createSetupListViewModel = createSetupListViewModel;
            _createAdministrativeDetailsViewModel = createItemAndDetailsViewModel;
            _createString = createString;
            _createBoolean = createBoolean;
            _createNamedViewModel = createNamedViewModel;
            _createCollectionAndSelectedItemAndIdViewModel = createCollectionAndSelectedItemAndIdViewModel;
            _adminsitrativeSetupService = adminsitrativeSetupService;
            _interactionManager = interactionManager;
            GetAministrativeSetupData = Command.Create(ExecuteGetAministrativeSetupData).Async(() => InteractionContext);
            AddItemToNamedCollection = Command.Create(ExecuteAddItemToNamedCollection).Async(() => InteractionContext);
            DeleteItemFromNamedCollection = Command.Create<string>(ExecuteDeleteItemFromNamedCollection).Async(() => InteractionContext);
            SaveChanges = Command.Create(ExecuteSaveChanges).Async(() => InteractionContext);

            OpenAppointmentCategories = Command.Create(ExecuteOpenAppointmentCategories);
            OpenCalendar = Command.Create(ExecuteOpenCalendar);
            OpenInsurancePlans = Command.Create(ExecuteOpenInsurancePlans);
            OpenPatientDemographics = Command.Create(ExecuteOpenPatientDemographics);
            Close = Command.Create(ExecuteClose);

            AdminSetupLists = InitializeAdminSetupCollection();
        }

        public IInteractionContext InteractionContext { get; set; }

        #region Commands
        public ICommand GetAministrativeSetupData { get; protected set; }
        public ICommand AddItemToNamedCollection { get;protected set; }
        public ICommand DeleteItemFromNamedCollection { get; protected set; }
        public ICommand SaveChanges { get; protected set; }
        public ICommand OpenAppointmentCategories { get; protected set; }
        public ICommand OpenCalendar { get; protected set; }
        public ICommand OpenInsurancePlans { get; protected set; }
        public ICommand OpenPatientDemographics { get; protected set; }
        public ICommand Close { get; protected set; }
        #endregion

        public ExtendedObservableCollection<AdministrativeSetupListViewModel> InitializeAdminSetupCollection()
        {
            var adminList = new ExtendedObservableCollection<AdministrativeSetupListViewModel>();
      
            string[] leftPanelTables = { "Insurer Plan Types", "Claim File Receivers", "Insurer Business Classes", "External Organizations", "Encounter Status Change Reasons", "Service Locations" };

            for (int i = 0; i < leftPanelTables.Length; i++)
            {
                AdministrativeSetupListViewModel leftPanelList = _createSetupListViewModel();
                leftPanelList.Id = i;
                leftPanelList.Name = leftPanelTables[i];
                adminList.Add(leftPanelList);
            }

            return adminList.ToExtendedObservableCollection();
        }

        public void ExecuteGetAministrativeSetupData()
        {
            var adminList = SelectedList;
            if (adminList != null && !adminList.IsLoaded)
            {
                adminList.List = _adminsitrativeSetupService.GetAministrativeSetupData(adminList.Name).ToExtendedObservableCollection();
                adminList.IsLoaded = true;
            }
        }

        public void ExecuteAddItemToNamedCollection()
        {         
            if (SelectedList.List.Count >0)
            {
                AdministrativeDetails nameDetails = _createAdministrativeDetailsViewModel();
                var nameValueCollection = new ExtendedObservableCollection<StringAndIdViewModel>(); 
                var boolValueCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                var foreignKeyCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                if (SelectedList.List.FirstOrDefault(i => i.Name == AddItemText) != null)
                {
                    InteractionManager.Current.Alert("There is already a property with the same name in this setup type.");
                }
                else
                {
                    var stringValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.StringValues);
                    if (stringValues == null)
                    {
                        nameValueCollection = null;
                    }
                    else
                    {
                        stringValues.ForEach(i =>
                        {
                            var nameValuePair = _createString();
                            nameValuePair.Name = i.Name;
                            nameValuePair.Value = i.Name == "Name" ? AddItemText : "";
                            nameValueCollection.Add(nameValuePair);
                        });
                    }

                    var boolValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.BooleanValues);
                    if (boolValues == null)
                    {
                        boolValueCollection = null;
                    }
                    else
                    {
                        boolValues.ForEach(i =>
                        {
                            var boolValuePair = _createBoolean();
                            boolValuePair.Name = i.Name;
                            boolValuePair.Value = false;
                            boolValueCollection.Add(boolValuePair);
                        });
                    }

                    var foreignKeyValues = SelectedList.List.ElementAtOrDefault(0).IfNotNull(i => i.ForeignKeyValues);
                    if (foreignKeyValues == null)
                    {
                        foreignKeyCollection = null;
                    }
                    else
                    {
                        foreignKeyValues.ForEach(i =>
                        {
                            var foreignKeys = _createCollectionAndSelectedItemAndIdViewModel();
                            var namedCollection = new ExtendedObservableCollection<NamedViewModel>();
                            i.Collection.ForEach(j =>
                            {
                                var namedValues = _createNamedViewModel();
                                namedValues.Id = j.Id;
                                namedValues.Name = j.Name;
                                namedCollection.Add(namedValues);
                            });

                            foreignKeys.Collection = namedCollection;
                            foreignKeys.SelectedItem = namedCollection[0];
                            foreignKeys.Name = i.Name;
                            foreignKeyCollection.Add(foreignKeys);
                        });
                    }
                            
                    nameDetails.StringValues = nameValueCollection;
                    nameDetails.BooleanValues = boolValueCollection;
                    nameDetails.ForeignKeyValues = foreignKeyCollection;
                    nameDetails.Name = AddItemText;
                    nameDetails.CanDelete = true;
                    SelectedList.List.Add(nameDetails);
                }
            }
            AddItemText = string.Empty;

        }

        public void ExecuteDeleteItemFromNamedCollection(string itemName)
        {
            SelectedList.List.RemoveWhere(i => i.Name == itemName);
            SelectedList.DeletedItems.Add(itemName);
        }

        public void ExecuteSaveChanges()
        {
            var loadedList = AdminSetupLists.Where(i => i.IsLoaded).ToExtendedObservableCollection();
            _adminsitrativeSetupService.SaveIsLoadedItems(loadedList);
            loadedList.IfNotNull(i => i.ForEach(j =>
            {
                j.IsLoaded = false;
            }));

            ExecuteGetAministrativeSetupData();
        }

        public ICommand Load { get; set; }

        private void ExecuteOpenPatientDemographics()
        {
            var view = new PatientDemographicsSetupView();
            view.DataContext = view.DataContext.EnsureType<PatientDemographicsSetupViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view
            });
        }

        private void ExecuteOpenInsurancePlans()
        {
            var view = new ManageInsurancePlansView();
            view.DataContext = view.DataContext.EnsureType<ManageInsurancePlansViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view
            });
        }

        private void ExecuteOpenCalendar()
        {
            var view = new ScheduleTemplateBuilderView();
            view.DataContext = view.DataContext.EnsureType<ScheduleTemplateBuilderViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view
            });
        }

        private void ExecuteOpenAppointmentCategories()
        {
            var view = new ConfigureCategoriesView();
            view.DataContext = view.DataContext.EnsureType<ConfigureCategoriesViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view
            });
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }
    }
}
