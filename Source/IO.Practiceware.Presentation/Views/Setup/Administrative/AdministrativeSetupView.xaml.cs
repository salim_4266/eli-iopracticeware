﻿using Soaf.Presentation;
using System.Windows.Input;


namespace IO.Practiceware.Presentation.Views.Setup.Administrative
{
    /// <summary>
    /// Interaction logic for AdministrativeView.xaml
    /// </summary>
    public partial class AdministrativeSetupView
    {
        public AdministrativeSetupView()
        {
            InitializeComponent();
        }

    }

    public class AdministrativeSetupViewContextReference : DataContextReference
        {
            public new AdministrativeSetupViewContext DataContext
            {
                get { return (AdministrativeSetupViewContext)base.DataContext; }
                set { base.DataContext = value; }
            }
        }
}
