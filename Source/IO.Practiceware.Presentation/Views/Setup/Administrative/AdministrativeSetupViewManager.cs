﻿using Soaf;
using Soaf.Presentation;
using System.Windows;


namespace IO.Practiceware.Presentation.Views.Setup.Administrative
{
   public  class AdministrativeSetupViewManager
    {
       private readonly IInteractionManager _interactionManager;

       public AdministrativeSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

       public void ShowAdministrativeSetup()
        {

            var view = new AdministrativeSetupView();
            view.DataContext = view.DataContext.EnsureType<AdministrativeSetupViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                Header = "Administrative Setup"
            });
        }
    }
}
