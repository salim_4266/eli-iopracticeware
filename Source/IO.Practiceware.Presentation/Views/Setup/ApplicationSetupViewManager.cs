﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Setup
{
    [Singleton]
    public class ApplicationSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public ApplicationSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowApplicationSetup()
        {
            var view = new ApplicationSetupView();
            view.DataContext = view.DataContext.EnsureType<ApplicationSetupViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
            });
        }

    }

    public enum ApplicationSetupTab
    {
        Administrative,
        Billing,
        Clinical,
        AdminUtilities
    }
}
