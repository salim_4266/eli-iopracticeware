﻿using IO.Practiceware.Presentation.ViewModels.AppointmentSelector;
using IO.Practiceware.Presentation.ViewServices.AppointmentSelector;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.AppointmentSelector
{
    /// <summary>
    /// Interaction logic for AppointmentSelector.xaml
    /// </summary>
    public partial class AppointmentSelectorView
    {
        public AppointmentSelectorView()
        {
            InitializeComponent();
        }
    }

    public class AppointmentSelectorViewContext : IViewContext
    {
        private readonly IAppointmentSelectorViewService _appointmentSelectorViewService;
        private readonly Func<AppointmentSelectorFilterSelectionViewModel> _createFilterSelectionViewModel;

        public AppointmentSelectorViewContext(IAppointmentSelectorViewService appointmentSelectorViewService, Func<AppointmentSelectorFilterSelectionViewModel> createFilterSelectionViewModel)
        {
            _appointmentSelectorViewService = appointmentSelectorViewService;
            _createFilterSelectionViewModel = createFilterSelectionViewModel;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            ResetFilter = Command.Create(ExecuteResetFilter);
            Search = Command.Create(ExecuteSearch, CanSearchExecute).Async(() => InteractionContext);
            SelectAll = Command.Create(ExecuteSelectAll);
            SelectNone = Command.Create(ExecuteSelectNone);
            Next = Command.Create(ExecuteNext);
        }

        public AppointmentSelectorReturnArguments ReturnArguments { get; set; }

        [DispatcherThread]
        public virtual AppointmentSelectorFilterViewModel FilterViewModel { get; set; }

        [DispatcherThread]
        public virtual AppointmentSelectorFilterSelectionViewModel FilterSelectionViewModel { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSelectorResultViewModel> SearchResults { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSelectorResultViewModel> SelectedAppointments { get; set; }

        public bool GetOnlyDischargeEncounters { get; set; }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            FilterViewModel = _appointmentSelectorViewService.LoadSearchFilterInformation();
            ExecuteResetFilter();
            SearchResults = new ObservableCollection<AppointmentSelectorResultViewModel>();
            SelectedAppointments = new ObservableCollection<AppointmentSelectorResultViewModel>();
        }

        private void ExecuteClose()
        {
            ReturnArguments = null;
            InteractionContext.Complete(false);
        }

        private void ExecuteResetFilter()
        {
            FilterSelectionViewModel = _createFilterSelectionViewModel();
        }

        private void ExecuteSearch()
        {
            SearchResults = null;
            SearchResults = _appointmentSelectorViewService.SearchAppointments(FilterSelectionViewModel, GetOnlyDischargeEncounters).ToExtendedObservableCollection();
        }

        private bool CanSearchExecute()
        {
            return FilterSelectionViewModel != null && FilterSelectionViewModel.IsValid();
        }

        private void ExecuteSelectAll()
        {
            if (SearchResults.IsNotNullOrEmpty())
            {
                SelectedAppointments = new ObservableCollection<AppointmentSelectorResultViewModel>(SearchResults);
            }
        }

        private void ExecuteSelectNone()
        {
            SelectedAppointments = new ObservableCollection<AppointmentSelectorResultViewModel>();
        }

        private void ExecuteNext()
        {
            ReturnArguments = new AppointmentSelectorReturnArguments { GoToNextScreen = true };
            if (SelectedAppointments.IsNotNullOrEmpty()) ReturnArguments.ListOfAppointmentId = SelectedAppointments.Select(a => a.Id).ToList();
            else ReturnArguments.ListOfAppointmentId = SearchResults.IsNotNullOrEmpty() ? SearchResults.Select(a => a.Id).ToList() : null;
            InteractionContext.Complete(true);
        }

        #region Commands

        /// <summary>
        /// Command to load the UI
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to close the UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to clear the filter selections
        /// </summary>
        public ICommand ResetFilter { get; protected set; }

        /// <summary>
        /// Command to perform the search based on the filter selections
        /// </summary>
        public ICommand Search { get; protected set; }

        /// <summary>
        /// Command to go to the next screen that will edit and transmit the C-CDA files
        /// </summary>
        public ICommand Next { get; protected set; }

        /// <summary>
        /// Command to select all search result rows.
        /// </summary>
        public ICommand SelectAll { get; protected set; }

        /// <summary>
        /// Command to de-select all search result rows.
        /// </summary>
        public ICommand SelectNone { get; protected set; }

        #endregion
    }
}