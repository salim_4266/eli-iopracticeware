﻿using Soaf;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.AppointmentSelector
{
    public class AppointmentSelectorViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public AppointmentSelectorViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public AppointmentSelectorReturnArguments ShowAppointmentSelector(string parentScreenName = "", bool getOnlyDischargeEncounters = false)
        {
            var view = new AppointmentSelectorView();
            var dataContext = view.DataContext.EnsureType<AppointmentSelectorViewContext>();
            dataContext.GetOnlyDischargeEncounters = getOnlyDischargeEncounters;
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = parentScreenName != "" ? parentScreenName + ": Select Appointments" : "Select Appointments" });
            return dataContext.ReturnArguments;
        }
    }

    public class AppointmentSelectorReturnArguments
    {
        public List<int> ListOfAppointmentId { get; set; }
        public bool GoToNextScreen { get; set; }
    }
}