﻿using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Validation;
using System.Windows.Input;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientInsuranceAuthorizationPopup
{
    public class PatientInsuranceAuthorizationPopupLoadArguments
    {
        public PatientInsuranceAuthorizationPopupOperationMode OperationMode { get; set; } // Default = Instant

        [RequiredIfNull("OnButtonClick")]
        public int? EncounterId { get; set; }

        public int? InvoiceId { get; set; }

        public bool DirectlyLaunchAuthorizationScreen { get; set; }

        [RequiredIfNull("EncounterId")]
        public ICommand OnButtonClick { get; set; } // Provide a hook to perform some outside action when the main button is clicked - OPTIONAL
       
        public PatientInsuranceAuthorizationViewModel AttachedPatientInsuranceAuthorization { get; set; }

    }

    public class PatientInsuranceAuthorizationPopupReturnArguments
    {
        public int? AuthorizationId { get; set; }
    }
}