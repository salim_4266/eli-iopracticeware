﻿using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization;
using IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorizationPopup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PatientInsuranceAuthorizationPopup
{
    /// <summary>
    /// Instant is the default. It results in direct changes to the system will no option to roll back.
    /// Deferred allow usage in other user controls that provide logic for rolling back of changes.
    /// NOTE: Deferred REQUIRES the use of AcceptChanges() and RevertChanges()
    /// </summary>
    public enum PatientInsuranceAuthorizationPopupOperationMode
    {
        Instant,
        Deferred
    }

    /// <summary>
    /// Interaction logic for PatientInsuranceAuthorizationPopupView.xaml
    /// </summary>
    public partial class PatientInsuranceAuthorizationPopupView
    {
        public PatientInsuranceAuthorizationPopupView()
        {
            InitializeComponent();
        }
    }

    public class PatientInsuranceAuthorizationPopupViewContextReference : DataContextReference
    {
        public new PatientInsuranceAuthorizationPopupViewContext DataContext
        {
            get { return (PatientInsuranceAuthorizationPopupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    public class PatientInsuranceAuthorizationPopupViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IPatientInsuranceAuthorizationPopupViewService _patientInsuranceAuthorizationPopupViewService;
        private readonly Func<PatientInsuranceAuthorizationsViewManager> _createPatientInsuranceAuthorizationsViewManager;
        private readonly Func<PatientInsuranceAuthorizationViewModel> _createPatientInsuranceAuthorizationViewModel;

        private readonly ICommand _getAuthorizationFromSystem;
        private readonly ICommand _deleteAuthorizationFromSystem;
        private readonly ICommand _deleteAllAuthorizationFromSystem;
        private readonly ICommand _deleteInvalidAuthorizationsFromSystem;
        private readonly ICommand _editAuthorizationFromSystem;
        private bool _showPopup;
        private bool _showAuthorizationScreen;

        public PatientInsuranceAuthorizationPopupViewContext(IPatientInsuranceAuthorizationPopupViewService patientInsuranceAuthorizationPopupViewService,
                                                                Func<PatientInsuranceAuthorizationsViewManager> createPatientInsuranceAuthorizationsViewManager,
                                                                Func<PatientInsuranceAuthorizationViewModel> createPatientInsuranceAuthorizationViewModel)
        {
            _patientInsuranceAuthorizationPopupViewService = patientInsuranceAuthorizationPopupViewService;
            _createPatientInsuranceAuthorizationsViewManager = createPatientInsuranceAuthorizationsViewManager;
            _createPatientInsuranceAuthorizationViewModel = createPatientInsuranceAuthorizationViewModel;
            DirectLoad = Command.Create(ExecuteDirectLoad);
            AddAuthorization = Command.Create(ExecuteAddAuthorization);
            EditAuthorization = Command.Create(ExecuteEditAuthorization);
            DeleteAuthorization = Command.Create(ExecuteDeleteAuthorization);
            DeleteSignOffGiven = Command.Create(ExecuteDeleteSignOffGiven);
            _getAuthorizationFromSystem = Command.Create<int>(ExecuteGetAuthorization).Async(() => InteractionContext, asyncIf: () => !Thread.CurrentThread.IsBackground);
            _deleteAuthorizationFromSystem = Command.Create<int>(ExecuteDeleteAuthorizationFromSystem).Async(() => InteractionContext, asyncIf: () => !Thread.CurrentThread.IsBackground);
            _deleteAllAuthorizationFromSystem = Command.Create<int>(ExecuteDeleteAllAuthorizationFromSystem).Async(() => InteractionContext, asyncIf: () => !Thread.CurrentThread.IsBackground);
            _deleteInvalidAuthorizationsFromSystem = Command.Create<PatientInsuranceAuthorizationViewModel>(ExecuteDeleteInvalidAuthorizationsFromSystem).Async(() => InteractionContext, asyncIf: () => !Thread.CurrentThread.IsBackground);
            _editAuthorizationFromSystem = Command.Create<PatientInsuranceAuthorizationViewModel>(ExecuteEditAuthorizationFromSystem).Async(() => InteractionContext, asyncIf: () => !Thread.CurrentThread.IsBackground);
        }

        #region USAGE BY OUTER USER CONTROL

        /// <summary>
        /// Flag to outer user control that there are pending changes 
        /// </summary>
        [DispatcherThread]
        public virtual bool HasChanges
        {
            get
            {
                return AttachedPatientInsuranceAuthorization != null && AttachedPatientInsuranceAuthorization.CastTo<IChangeTracking>().IsChanged;
            }
        }

        /// <summary>
        /// For outer user control to load arguments
        /// </summary>
        /// <param name="loadArguments">load args</param>
        public void DirectlyLoadArguments(PatientInsuranceAuthorizationPopupLoadArguments loadArguments)
        {
            LoadArguments = loadArguments;
            DirectLoad.Execute();
        }

        /// <summary>
        /// For outer user control to get arguments
        /// </summary>
        /// <returns>return args</returns>
        public PatientInsuranceAuthorizationPopupReturnArguments DirectlyReturnArguments()
        {
            if (AttachedPatientInsuranceAuthorization != null) return new PatientInsuranceAuthorizationPopupReturnArguments { AuthorizationId = AttachedPatientInsuranceAuthorization.Id };
            return null;
        }

        /// <summary>
        /// if PatientInsuranceAuthorizationPopupOperationMode.Deferred then outer user control must call to save changes
        /// IMPORTANT: IF NOT CALLED THEN INCONSISTENT DATA STATE
        /// Add and Edit are already in the database
        /// Delete needs to do clean up
        /// </summary>
        public void AcceptChanges()
        {
            if (LoadArguments.OperationMode == PatientInsuranceAuthorizationPopupOperationMode.Instant) return;

            if (AttachedPatientInsuranceAuthorization != null)
            {
                AttachedPatientInsuranceAuthorization.CastTo<IEditableObject>().EndEdit();
            }

            if (AttachedPatientInsuranceAuthorization == null || AttachedPatientInsuranceAuthorization.Code.IsNullOrEmpty())
            {
                _deleteAllAuthorizationFromSystem.Execute(LoadArguments.EncounterId);
            }
        }

        /// <summary>
        /// if PatientInsuranceAuthorizationPopupOperationMode.Deferred then outer user control must call to roll back changes
        /// IMPORTANT: IF NOT CALLED THEN INCONSISTENT DATA STATE
        /// Add and Edit are already in the database
        /// Delete needs to do clean up
        /// </summary>
        public void RevertChanges()
        {
            if (LoadArguments.OperationMode == PatientInsuranceAuthorizationPopupOperationMode.Instant) return;

            if (AttachedPatientInsuranceAuthorization != null)
            {
                AttachedPatientInsuranceAuthorization.CastTo<IEditableObject>().CancelEdit();
            }

            if (AttachedPatientInsuranceAuthorization == null || !AttachedPatientInsuranceAuthorization.Id.HasValue)
            {
                _deleteAllAuthorizationFromSystem.Execute(LoadArguments.EncounterId);
            }
            else
            {

                _deleteInvalidAuthorizationsFromSystem.Execute(AttachedPatientInsuranceAuthorization);
                _editAuthorizationFromSystem.Execute(AttachedPatientInsuranceAuthorization);
            }
        }

        #endregion

        private void ExecuteDirectLoad()
        {
            ProcessLoadArguments();
        }

        private void ProcessLoadArguments()
        {
            if (LoadArguments == null) throw new ArgumentNullException();
            if (!LoadArguments.EncounterId.HasValue && LoadArguments.OnButtonClick == null) throw new ArgumentNullException();

            InitializePatientInsuranceAuthorization();

            if (LoadArguments.DirectlyLaunchAuthorizationScreen && LoadArguments.EncounterId.HasValue)
            {
                LaunchPatientInsuranceAuthorizationScreen(LoadArguments.EncounterId, AttachedPatientInsuranceAuthorization.Id, LoadArguments.InvoiceId);
            }
        }

        void InitializePatientInsuranceAuthorization()
        {
            if (AttachedPatientInsuranceAuthorization == null)
            {
                AttachedPatientInsuranceAuthorization = LoadArguments.AttachedPatientInsuranceAuthorization ?? _createPatientInsuranceAuthorizationViewModel();
            }

            AttachedPatientInsuranceAuthorization.EncounterId = LoadArguments.EncounterId;
            AttachedPatientInsuranceAuthorization.InvoiceId = LoadArguments.InvoiceId;

            AttachedPatientInsuranceAuthorization.CastTo<IChangeTracking>().AcceptChanges();
            AttachedPatientInsuranceAuthorization.CastTo<IEditableObject>().BeginEdit();
        }

        /// <summary>
        /// Functionality for when an authorization is not loaded (pop up not used)
        /// Then main button act like other regular button (carry out actions)
        /// Once an authorization is loaded - main button is neutered
        /// </summary>
        private void ExecuteAddAuthorization()
        {

            if (LoadArguments == null) return;

            if (LoadArguments.OnButtonClick != null)
            {
                LoadArguments.OnButtonClick.Execute(null);
                return;
            }
            int? authorizationId = null;
            if (AttachedPatientInsuranceAuthorization != null)
            {
                authorizationId = AttachedPatientInsuranceAuthorization.Id;
            }
            LaunchPatientInsuranceAuthorizationScreen(LoadArguments.EncounterId, authorizationId, LoadArguments.InvoiceId);
        }

        private void ExecuteEditAuthorization()
        {
            if (AttachedPatientInsuranceAuthorization == null) return;
            // To decide to go to authorization screen.
            _showAuthorizationScreen = true;
            LaunchPatientInsuranceAuthorizationScreen(AttachedPatientInsuranceAuthorization.EncounterId, AttachedPatientInsuranceAuthorization.Id, AttachedPatientInsuranceAuthorization.InvoiceId);
        }

        /// <summary>
        /// Confirm dialog used for delete
        /// </summary>
        private void ExecuteDeleteAuthorization()
        {
            if (AttachedPatientInsuranceAuthorization == null) return;
            NeedDeleteSignOff = true;
        }

        private void ExecuteDeleteSignOffGiven()
        {
            ShowPopup = false; // UI tweak - if yes is clicked the pre-auth is deleted and the popup menu is closed. the pre-auth button reverts to the initial state where it reads "ATTACH +"
            if (AttachedPatientInsuranceAuthorization == null) return;

            // An authorization that is removed still has an encounter hence the on button click functionality should still function
            LoadArguments.EncounterId = AttachedPatientInsuranceAuthorization.EncounterId;

            if (LoadArguments.OperationMode == PatientInsuranceAuthorizationPopupOperationMode.Instant)
            {
                _deleteAuthorizationFromSystem.Execute(AttachedPatientInsuranceAuthorization.Id);
            }

            AttachedPatientInsuranceAuthorization = null;
        }

        private void LaunchPatientInsuranceAuthorizationScreen(int? encounterId, int? authorizationId, int? invoiceId)
        {
            if (!_showAuthorizationScreen && AttachedPatientInsuranceAuthorization != null &&
                AttachedPatientInsuranceAuthorization.Code.IsNotNullOrEmpty()) return;

            var manager = _createPatientInsuranceAuthorizationsViewManager();
            var loadArguments = new PatientInsuranceAuthorizationsLoadArguments { EncounterId = encounterId, InvoiceId = invoiceId };
            if (authorizationId.HasValue) loadArguments.PatientInsuranceAuthorizationId = authorizationId;
            var returnArguments = manager.ShowPatientInsuranceAuthorization(loadArguments);

            if (returnArguments == null || returnArguments.AuthorizationId == 0) return;

            _getAuthorizationFromSystem.Execute(returnArguments.AuthorizationId);
            _showAuthorizationScreen = false;
        }

        private void ExecuteGetAuthorization(int authorizationId)
        {
            var attachedPatientInsuranceAuthorization = _patientInsuranceAuthorizationPopupViewService.GetAuthorization(authorizationId);

            //If you add soon after you delete the authorization, AttachedPatientInsuranceAuthorization is null so need to initialize the object again.
            if (AttachedPatientInsuranceAuthorization == null)
            {
                InitializePatientInsuranceAuthorization();
            }

            if (attachedPatientInsuranceAuthorization != null)
            {
                //Mapping the values to not to loose the change tracking of the object.
                // ReSharper disable once PossibleNullReferenceException
                AttachedPatientInsuranceAuthorization.Id = attachedPatientInsuranceAuthorization.Id;
                AttachedPatientInsuranceAuthorization.Location = attachedPatientInsuranceAuthorization.Location;
                AttachedPatientInsuranceAuthorization.Code = attachedPatientInsuranceAuthorization.Code;
                AttachedPatientInsuranceAuthorization.Comments = attachedPatientInsuranceAuthorization.Comments;
                AttachedPatientInsuranceAuthorization.Date = attachedPatientInsuranceAuthorization.Date;
                AttachedPatientInsuranceAuthorization.PatientInsuranceId = attachedPatientInsuranceAuthorization.PatientInsuranceId;
                AttachedPatientInsuranceAuthorization.PatientInsuranceId = attachedPatientInsuranceAuthorization.PatientInsuranceId;

            }

            ShowPopup = false; // UI tweak - once one is created and saved, the pre-auth would be saved to the appointment and the attach+ button updates to show that a pre-auth is attached.  clicking the pre-auth button now would display the pre-auth popup menu displaying the defined details for the attached pre-authorization and the "edit pre-auth" button on the bottom
        }

        private void ExecuteDeleteAuthorizationFromSystem(int authorizationId)
        {
            _patientInsuranceAuthorizationPopupViewService.DeleteAuthorization(authorizationId);
        }

        private void ExecuteDeleteAllAuthorizationFromSystem(int encounterId)
        {
            _patientInsuranceAuthorizationPopupViewService.DeleteAllAuthorization(encounterId);
        }

        private void ExecuteDeleteInvalidAuthorizationsFromSystem(PatientInsuranceAuthorizationViewModel patientInsuranceAuthorizationViewModel)
        {
            if (patientInsuranceAuthorizationViewModel == null) return;
            _patientInsuranceAuthorizationPopupViewService.DeleteInvalidAuthorizations(patientInsuranceAuthorizationViewModel.EncounterId.EnsureNotDefault().GetValueOrDefault(), patientInsuranceAuthorizationViewModel.Id);
        }

        private void ExecuteEditAuthorizationFromSystem(PatientInsuranceAuthorizationViewModel authorization)
        {
            if (authorization == null) return;
            _patientInsuranceAuthorizationPopupViewService.EditAuthorization(authorization);
        }

        /// <summary>
        /// Initial datapoints for the control
        /// </summary>
        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationPopupLoadArguments LoadArguments { get; set; }

        /// <summary>
        /// Direct control of when the pop up shows - Needed because of business requirements
        /// </summary>
        [DispatcherThread]
        [DependsOn("AttachedPatientInsuranceAuthorization")]
        public virtual bool ShowPopup
        {
            get
            {
                if (AttachedPatientInsuranceAuthorization == null || AttachedPatientInsuranceAuthorization.Code.IsNullOrEmpty()) return false;
                return _showPopup;
            }
            set { _showPopup = value; }
        }

        /// <summary>
        /// The indicator on what the main button style shows
        /// </summary>
        [DependsOn("AttachedPatientInsuranceAuthorization"), DispatcherThread]
        public virtual PatientInsuranceAuthorizationMode PatientInsuranceAuthorizationButtonMode
        {
            get
            {
                return (AttachedPatientInsuranceAuthorization != null && AttachedPatientInsuranceAuthorization.Code.IsNotNullOrEmpty()) ? PatientInsuranceAuthorizationMode.Existing : PatientInsuranceAuthorizationMode.None;
            }
        }

        /// <summary>
        /// The text to display on the main button
        /// </summary>
        [DependsOn("AttachedPatientInsuranceAuthorization"), DispatcherThread]
        public virtual string PatientInsuranceAuthorizationButtonText
        {
            get { return (AttachedPatientInsuranceAuthorization == null || AttachedPatientInsuranceAuthorization.Code.IsNullOrEmpty()) ? null : String.Format("# {0}", AttachedPatientInsuranceAuthorization.Code); }
        }

        /// <summary>
        /// The authorization from the system
        /// </summary>
        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationViewModel AttachedPatientInsuranceAuthorization { get; protected set; }

        /// <summary>
        /// Flag to determine if delete config dialog displays or not
        /// </summary>
        public virtual bool NeedDeleteSignOff { get; set; }

        /// <summary>
        /// Command to populate view context
        /// </summary>
        public ICommand DirectLoad { get; protected set; }

        /// <summary>
        /// Command that is tied to the + Attach (main button) button click
        /// </summary>
        public ICommand AddAuthorization { get; protected set; }

        /// <summary>
        /// Command that is tied to the edit button in pop up
        /// </summary>
        public ICommand EditAuthorization { get; protected set; }

        /// <summary>
        /// Command that is tied to the delete button in pop up
        /// </summary>
        public ICommand DeleteAuthorization { get; protected set; }

        /// <summary>
        /// Command that is tied to the ok button in delete confirm dialog
        /// </summary>
        public ICommand DeleteSignOffGiven { get; protected set; }


    }
}