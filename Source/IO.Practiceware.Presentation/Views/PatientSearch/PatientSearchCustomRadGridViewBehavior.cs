﻿using IO.Practiceware.Presentation.ViewModels.PatientSearch;
using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.PatientSearch
{
    public class PatientSearchCustomRadGridViewBehavior : Behavior<RadGridView>
    {
        #region DependencyProperty : LoadPatientPhoto

        public ICommand LoadPatientPhoto
        {
            get { return GetValue(LoadPatientPhotoProperty) as ICommand; }
            set { SetValue(LoadPatientPhotoProperty, value); }
        }

        public static readonly DependencyProperty LoadPatientPhotoProperty = DependencyProperty.Register("LoadPatientPhoto", typeof (ICommand), typeof (PatientSearchCustomRadGridViewBehavior));

        #endregion

        #region DependencyProperty : LoadPatientPostOpPeriod

        public ICommand LoadPatientPostOpPeriod
        {
            get { return GetValue(LoadPatientPostOpPeriodProperty) as ICommand; }
            set { SetValue(LoadPatientPostOpPeriodProperty, value); }
        }

        public static readonly DependencyProperty LoadPatientPostOpPeriodProperty = DependencyProperty.Register("LoadPatientPostOpPeriod", typeof (ICommand), typeof (PatientSearchCustomRadGridViewBehavior));

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.RowLoaded += WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(RowLoadedHandler);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.RowLoaded -= WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(RowLoadedHandler);
        }

        #region HelpersVariables

        private readonly ResourceDictionary _resources = new ResourceDictionary {Source = new Uri("IO.Practiceware.Presentation;component/Views/PatientSearch/Resources.xaml", UriKind.Relative)};

        #endregion

        #region EventHandlers

        private void RowLoadedHandler(object sender, RowLoadedEventArgs e)
        {
            var row = e.Row as GridViewRow;
            if (row == null) return;

            if (row.ToolTip == null)
            {
                var toolTip = new ToolTip
                    {
                        ContentTemplate = (DataTemplate) _resources["IndexCard"]
                    };
                BindingOperations.SetBinding(toolTip, ContentControl.ContentProperty, new Binding("DataContext") { Source = row });
                ToolTipService.SetToolTip(row, toolTip);
                ToolTipService.SetInitialShowDelay(row, 1200);
                ToolTipService.SetShowDuration(row, Int32.MaxValue-1);

                row.ToolTipOpening += ToolTipOpeningHandler;
                row.MouseLeave += OnRowMouseLeave;
            }
        }

        static void OnRowMouseLeave(object sender, MouseEventArgs e)
        {
            var row = sender as GridViewRow;
            if (row == null) return;

            var tooltip = row.ToolTip as ToolTip;
            if(tooltip != null)
            {
                tooltip.IsOpen = false;
            }
        }

        private void ToolTipOpeningHandler(object sender, ToolTipEventArgs e)
        {
            var row = sender as GridViewRow;
            if (row == null) return;
            var patientViewModel = row.DataContext as PatientSearchPatientViewModel;
            if (patientViewModel == null) return;

            if (LoadPatientPhoto != null && patientViewModel.Photo == null && !patientViewModel.HasLoadedPhoto)
            {
                LoadPatientPhoto.Execute(patientViewModel);
            }

            if (LoadPatientPostOpPeriod != null && patientViewModel.PostOp == null && !patientViewModel.HasLoadedPostOp)
            {
                LoadPatientPostOpPeriod.Execute(patientViewModel);
            }
        }

        #endregion

    }
}