using System.Linq;
using IO.Practiceware.Presentation.ViewServices.PatientSearch;
using IO.Practiceware.Services.PatientSearch;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.PatientSearch
{
    [Singleton]
    public class PatientSearchViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IPatientSearchViewService _patientSearchViewService;

        public PatientSearchViewManager(IInteractionManager interactionManager, IPatientSearchViewService patientSearchViewService)
        {
            _interactionManager = interactionManager;
            _patientSearchViewService = patientSearchViewService;
        }

        public PatientSearchReturnArguments ShowPatientSearchView()
        {
            return ShowPatientSearchView(null);
        }

        public PatientSearchReturnArguments ShowPatientSearchView(PatientSearchLoadArguments loadArguments)
        {
            var view = new PatientSearchView();
            var dataContext = view.DataContext as PatientSearchViewContext;
            if (dataContext == null)
            {
                throw new Exception("The PatientSearchView's DataContext is not of type {0}".FormatWith(typeof(PatientSearchViewContext).Name));
            }

            if (loadArguments != null)
            {
                dataContext.LoadArguments = loadArguments;
            }

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });

            if (dataContext.SelectedSearchResult != null && !dataContext.SelectedCreatePatient)
            {
                var returnArguments = new PatientSearchPatientSelectedReturnArguments();

                returnArguments.PatientId = dataContext.SelectedSearchResult.Id;
                returnArguments.PatientPrefix = dataContext.SelectedSearchResult.Prefix;
                returnArguments.PatientFirstName = dataContext.SelectedSearchResult.FirstName;
                returnArguments.PatientMiddleName = dataContext.SelectedSearchResult.MiddleName;
                returnArguments.PatientLastName = dataContext.SelectedSearchResult.LastName;
                returnArguments.PatientSuffix = dataContext.SelectedSearchResult.Suffix;
                returnArguments.PatientDisplayName = dataContext.SelectedSearchResult.DisplayName;

                return returnArguments;
            }
            
            if (dataContext.SelectedCreatePatient)
            {
                return new PatientSearchCreatePatientReturnArguments();
            }
            
            return new PatientSearchPatientSelectedReturnArguments();
        }

        public PatientSearchReturnArguments ShowMiniPatientSearchView(PatientSearchLoadArguments loadArguments)
        {
            var view = new MiniPatientSearchView();
            var dataContext = view.DataContext as MiniPatientSearchViewContext;
            if (dataContext == null)
            {
                throw new Exception("The MiniPatientSearchView's DataContext is not of type {0}".FormatWith(typeof(MiniPatientSearchViewContext).Name));
            }

            if (loadArguments != null)
            {
                dataContext.ContactTypeName = "Patient";                
            }

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });

            if (dataContext.SelectedSearchResult != null)
            {
                var returnArguments = new PatientSearchPatientSelectedReturnArguments();

                returnArguments.PatientId = dataContext.SelectedSearchResult.Id;
                returnArguments.PatientPrefix = dataContext.SelectedSearchResult.Prefix;
                returnArguments.PatientFirstName = dataContext.SelectedSearchResult.FirstName;
                returnArguments.PatientMiddleName = dataContext.SelectedSearchResult.MiddleName;
                returnArguments.PatientLastName = dataContext.SelectedSearchResult.LastName;
                returnArguments.PatientSuffix = dataContext.SelectedSearchResult.Suffix;
                returnArguments.PatientDisplayName = dataContext.SelectedSearchResult.DisplayName;

                return returnArguments;
            }

            return null;
        }

        /// <summary>
        /// Performs quick patient lookup by specified text or shows patient search dialog
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="allowCreateNew"></param>
        /// <param name="patientId"></param>
        /// <returns><c>true</c> if found or create new has been requested (when allowed)</returns>
        public virtual bool SearchPatient(string searchText, bool allowCreateNew, out int? patientId)
        {
            var searchFields = (SearchFields?)GetSearchFields(searchText);

            if (searchFields == SearchFields.PatientId)
            {
                // Try search
                var patientIdText = searchText.Trim();
                var foundPatients = _patientSearchViewService.Search(TextSearchMode.BeginsWith,
                    SearchFields.PatientId, patientIdText);

                patientId = foundPatients
                    .Select(p => (int?)p.Id)
                    .FirstOrDefault(id => id.ToString() == patientIdText);

                // Found patient by id? -> return
                if (patientId != null)
                {
                    return true;
                }

                // Reset search criteria
                searchText = string.Empty;
                searchFields = null;
                _interactionManager.Alert("Patient ID not found. Please enter different search criteria.");
            }

            bool createPatient;
            patientId = SearchPatient(searchText, searchFields, allowCreateNew, out createPatient);
            return patientId.HasValue || createPatient;
        }

        private int? SearchPatient(string searchText, SearchFields? searchFields, bool allowCreateNew, out bool createPatient)
        {
            createPatient = false;

            PatientSearchReturnArguments searchResult = null;

            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                searchResult = ShowPatientSearchView(new PatientSearchLoadArguments
                {
                    SearchText = searchText,
                    CanCreatePatient = allowCreateNew,
                    RunSearchOnLoad = !string.IsNullOrEmpty(searchText),
                    SearchFields = searchFields
                });
            });
            if (searchResult is PatientSearchPatientSelectedReturnArguments)
            {
                return ((PatientSearchPatientSelectedReturnArguments) searchResult).PatientId;
            }
            if (searchResult is PatientSearchCreatePatientReturnArguments)
            {
                createPatient = true;
            }
            return null;
        }

        /// <summary>
        /// Gets the search fields to use based on the input.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        private static SearchFields GetSearchFields(string searchText)
        {
            if (searchText.Trim().IsNumeric())
            {
                return SearchFields.PatientId;
            }
            if (searchText.Contains("/") || searchText.Contains("-"))
            {
                return SearchFields.DateOfBirth;
            }
            return SearchFields.LastName | SearchFields.FirstName | SearchFields.PatientId | SearchFields.IsActiveOnly;
        }
    }

    public class PatientSearchLoadArguments
    {
        public string SearchText { get; set; }

        public bool CanCreatePatient { get; set; }

        public bool RunSearchOnLoad { get; set; }
      
        public SearchFields? SearchFields { get; set; }
    }

    public class PatientSearchReturnArguments
    {

    }

    public class PatientSearchCreatePatientReturnArguments : PatientSearchReturnArguments
    {

    }

    public class PatientSearchPatientSelectedReturnArguments : PatientSearchReturnArguments
    {
        public int? PatientId { get; set; }
        public string PatientPrefix { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMiddleName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientSuffix { get; set; }
        public string PatientDisplayName { get; set; }
    }
}