﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Services.PatientSearch;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.PatientSearch
{
    /// <summary>
    /// Determines if the SearchField set in the 'SearchField' property is selected.
    /// </summary>
    public class SearchFieldsToBooleanConverter : IValueConverter
    {
        public SearchFields SearchFields { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var options = value as IEnumerable<SelectableNamedViewModel>;
            return options != null && options.First(o => o.Id == (int)SearchFields).IsSelected;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
