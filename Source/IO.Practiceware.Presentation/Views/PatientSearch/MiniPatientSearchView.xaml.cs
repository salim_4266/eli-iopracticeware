﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientSearch;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.PatientSearch;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Services.Imaging;
using Soaf;
using IO.Practiceware.Services.PatientSearch;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.PatientSearch
{
    /// <summary>
    /// Interaction logic for MiniPatientSearchView.xaml
    /// </summary>
    public partial class MiniPatientSearchView
    {
        public MiniPatientSearchView()
        {
            InitializeComponent();
        }
    }

    public class MiniPatientSearchViewContext : IViewContext, IHasWizardDataContext
    {
        private readonly IPatientSearchViewService _patientSearchViewService;
        private readonly IPostOperativePeriodService _postOperativePeriodService;
        private readonly IImageService _imageService;

        public MiniPatientSearchViewContext(IPatientSearchViewService patientSearchViewService,
                                            IPostOperativePeriodService postOperativePeriodService,
                                            IImageService imageService)
        {
            _patientSearchViewService = patientSearchViewService;
            _postOperativePeriodService = postOperativePeriodService;
            _imageService = imageService;

            Search = Command.Create(ExecuteSearch, () => !String.IsNullOrWhiteSpace(SearchText) && SearchFields != null && SearchFields.Any(so => so.IsSelected)).Async(() => InteractionContext);
            Reset = Command.Create(ExecuteReset);
            Close = Command.Create(ExecuteClose);
            LoadPatientPhoto = Command.Create<PatientSearchPatientViewModel>(ExecuteLoadPatientPhoto);
            LoadPatientPostOpPeriod = Command.Create<PatientSearchPatientViewModel>(ExecuteLoadPatientPostOpPeriod);

            SearchFields = Enums.GetValues<SearchFields>().Where(sf => sf != Services.PatientSearch.SearchFields.None)
                .Select(sf => new SelectableNamedViewModel
                {
                    Id = (int)sf,
                    Name = sf.GetDisplayName()
                }).ToExtendedObservableCollection();

            TextSearchModes = Enums.GetValues<TextSearchMode>()
                .Select(tsm => new TextSearchModeViewModel
                {
                    Id = (int)tsm,
                    Name = tsm.GetDisplayName(),
                    ToolTipText = tsm.GetDescription()
                }).ToExtendedObservableCollection();


            Init();
        }


        private void Init()
        {
            SetDefaultOptions();
        }

        private void SetDefaultOptions()
        {
            TextSearchModes.ForEachWithSinglePropertyChangedNotification(tsm => tsm.IsSelected = (tsm.Id == (int)TextSearchMode.BeginsWith));

            SearchFields.ForEachWithSinglePropertyChangedNotification(sf => sf.IsSelected = (sf.Id == (int)Services.PatientSearch.SearchFields.LastName ||
                                                         sf.Id == (int)Services.PatientSearch.SearchFields.PatientId ||
                                                         sf.Id == (int)Services.PatientSearch.SearchFields.PriorPatientCode ||
                                                         sf.Id == (int)Services.PatientSearch.SearchFields.DateOfBirth));
        }

        private void ExecuteSearch()
        {
            TermsToHighlight = PatientSearchExtensions.GetSearchTerms(SearchText).ToExtendedObservableCollection();

            var textSearchMode = (TextSearchMode)TextSearchModes.First(tsm => tsm.IsSelected).Id;
            var searchField = SearchFields.Where(sf => sf.IsSelected)
                                          .Select(sf => sf.Id)
                                          .Aggregate(0, (current, sf) => current | sf)
                                          .CastTo<SearchFields>();

            var searchResults = _patientSearchViewService.Search(textSearchMode, searchField, SearchText);

            if (OnFilterSearchResults != null)
            {
                OnFilterSearchResults(searchResults.CastTo<IList>().IfNull(searchResults.ToList));
            }

            SearchResults = searchResults.ToObservableCollection();            
        }

        private void ExecuteReset()
        {
            SetDefaultOptions();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete();
        }

        private void ExecuteLoadPatientPhoto(PatientSearchPatientViewModel patient)
        {
            if (patient.Photo == null && !patient.HasLoadedPhoto)
            {
                patient.HasLoadedPhoto = true;
                Task.Factory.StartNewWithCurrentTransaction(() => patient.Photo = _imageService.GetPatientPhoto(patient.Id));
            }
        }

        private void ExecuteLoadPatientPostOpPeriod(PatientSearchPatientViewModel patient)
        {
            if (patient.PostOp == null && !patient.HasLoadedPostOp)
            {
                patient.HasLoadedPostOp = true;
                Task.Factory.StartNewWithCurrentTransaction(() => patient.PostOp = _postOperativePeriodService.GetLatestPostOperativePeriodByPatientId(patient.Id));
            }
        }

        public virtual string SearchText { get; set; }

        [DependsOn("HistoryResults")]
        public virtual bool CanShowHistory
        {
            get
            {
                return HistoryResults != null && HistoryResults.Count != 0;
            }
        }

        public virtual ObservableCollection<TextSearchModeViewModel> TextSearchModes { get; protected set; }

        public virtual ObservableCollection<SelectableNamedViewModel> SearchFields { get; protected set; }

        public virtual ObservableCollection<PatientSearchPatientViewModel> HistoryResults { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<PatientSearchPatientViewModel> SearchResults { get; set; }

        public virtual PatientSearchPatientViewModel SelectedSearchResult { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<string> TermsToHighlight { get; set; }

        [DependsOn("SearchResults")]
        public virtual bool ShowPreSearchBorder
        {
            get { return SearchResults == null; }
        }

        [DependsOn("SearchResults")]
        public virtual bool ShowNoResultsBorder
        {
            get { return SearchResults != null && SearchResults.Count == 0; }
        }

        [DependsOn("SearchResults")]
        public virtual bool ShowResultsGrid
        {
            get { return SearchResults != null && SearchResults.Count != 0; }
        }       

        [DispatcherThread]
        public virtual string WindowTitle
        {
            get
            {
                return "Search for {0}".FormatWith(ContactTypeName);
            }
        }

        private string _contactTypeName;
        public virtual string ContactTypeName
        {
            get { return _contactTypeName.IsNullOrEmpty() ? "Contact" : _contactTypeName; }
            set { _contactTypeName = value; }
        }

        /// <summary>
        /// Gets a command to execute on load
        /// </summary>
        public ICommand Load { get; set; }

        /// <summary>
        /// Command that is executed when this instance is closing
        /// </summary>
        public ICommand Closing { get; protected set; }

        /// <summary>
        /// Gets a command to perform a search
        /// </summary>
        public ICommand Search { get; protected set; }

        /// <summary>
        /// Gets a command to reset the selected settings
        /// </summary>
        public ICommand Reset { get; protected set; }

        /// <summary>
        /// Gets a command to close the current screen
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Gets a command to load a patient photo
        /// </summary>
        public ICommand LoadPatientPhoto { get; protected set; }

        /// <summary>
        /// Gets a command to load a patient's post op period
        /// </summary>
        public ICommand LoadPatientPostOpPeriod { get; protected set; }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public virtual IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual IWizardDataContext Wizard { get; set; }

        public event Action<IList<PatientSearchPatientViewModel>> OnFilterSearchResults;
    }
}