using IO.Practiceware.Application;
using IO.Practiceware.Logging;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientSearch;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.PatientSearch;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Services.PatientSearch;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Logging;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using LogCategory = IO.Practiceware.Logging.LogCategory;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.PatientSearch
{
    /// <summary>
    /// Interaction logic for PatientSearchView.xaml
    /// </summary>
    public partial class PatientSearchView
    {
        public PatientSearchView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the PatientSearchView.
    /// </summary>
    public class PatientSearchViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly TaskQueue _postOpTaskQueue;
        private readonly TaskQueue _patientPhotoTaskQueue;
        private readonly IPatientSearchViewService _patientSearchViewService;
        private readonly IPostOperativePeriodService _postOperativePeriodService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private ApplicationSettingContainer<PatientSearchAdvancedOptionsSettings> _advancedOptionsUserSetting;
        private readonly IImageService _imageService;
        private readonly ILogger _logger;

        public PatientSearchViewContext(IPatientSearchViewService patientSearchViewService,
                                        IPostOperativePeriodService postOperativePeriodService,
                                        IApplicationSettingsService applicationSettingsService,
                                        IImageService imageService,
                                        Func<SerializedTaskQueue> createTaskQueue,
                                        ILogManager logManager)
        {
            _patientSearchViewService = patientSearchViewService;
            _postOperativePeriodService = postOperativePeriodService;
            _applicationSettingsService = applicationSettingsService;
            _imageService = imageService;
            _logger = logManager.GetCurrentTypeLogger();
            _postOpTaskQueue = createTaskQueue();
            _patientPhotoTaskQueue = createTaskQueue();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Search = Command.Create(ExecuteSearch, () => !String.IsNullOrWhiteSpace(SearchText) && SearchFields != null && SearchFields.Any(so => so.IsSelected)).Async(() => InteractionContext);
            Reset = Command.Create(ExecuteReset);
            Close = Command.Create(ExecuteClose);
            Select = Command.Create(ExecuteSelect, () => SelectedSearchResult != null);
            LoadPatientPhoto = Command.Create<PatientSearchPatientViewModel>(ExecuteLoadPatientPhoto);
            LoadPatientPostOpPeriod = Command.Create<PatientSearchPatientViewModel>(ExecuteLoadPatientPostOpPeriod);
            Closing = Command.Create<Action>(ExecuteClosing);
            CreatePatient = Command.Create(ExecuteCreatePatient, () => LoadArguments.IfNotNull(la => la.CanCreatePatient));

            SearchFields = Enums.GetValues<SearchFields>().Where(sf => sf != Services.PatientSearch.SearchFields.None)
                .Select(sf => new SelectableNamedViewModel
                                  {
                                      Id = (int)sf,
                                      Name = sf.GetDisplayName()
                                  }).ToExtendedObservableCollection();

            TextSearchModes = Enums.GetValues<TextSearchMode>()
                .Select(tsm => new TextSearchModeViewModel
                                   {
                                       Id = (int)tsm,
                                       Name = tsm.GetDisplayName(),
                                       ToolTipText = tsm.GetDescription()
                                   }).ToExtendedObservableCollection();

            SetDefaultOptions();
        }


        private void ExecuteClosing(Action cancelClose)
        {
            SaveAdvancedOptionsSettings();
            if (InteractionContext.DialogResult == false)
            {
                SelectedSearchResult = null;
            }
        }

        private void SetDefaultOptions()
        {
            TextSearchModes.ForEachWithSinglePropertyChangedNotification(tsm => tsm.IsSelected = (tsm.Id == (int)TextSearchMode.BeginsWith));

            SearchFields.ForEachWithSinglePropertyChangedNotification(sf => sf.IsSelected = (sf.Id == (int)Services.PatientSearch.SearchFields.LastName ||
                                                        sf.Id == (int)Services.PatientSearch.SearchFields.PatientId ||
                                                        sf.Id == (int)Services.PatientSearch.SearchFields.PriorPatientCode ||
                                                        sf.Id == (int)Services.PatientSearch.SearchFields.IsActiveOnly ||
                                                        sf.Id == (int)Services.PatientSearch.SearchFields.DateOfBirth));
        }

        private void LoadAdvancedOptionsSettings()
        {
            // Load default advanced options settings from arguments
            if (LoadArguments != null && LoadArguments.SearchFields != null)
            {
                SearchFields = Enums.GetValues<SearchFields>().Where(sf => sf != Services.PatientSearch.SearchFields.None)
                    .Select(sf => new SelectableNamedViewModel
                    {
                        Id = (int)sf,
                        Name = sf.GetDisplayName(),
                        IsSelected = LoadArguments.SearchFields.Value.HasFlag(sf)
                    }).ToExtendedObservableCollection();
            }

            // Load advanced options settings from application settings
            var setting = _applicationSettingsService
                .GetSetting<PatientSearchAdvancedOptionsSettings>(ApplicationSetting.PatientSearchAdvancedOptions);
            _advancedOptionsUserSetting = new ApplicationSettingContainer<PatientSearchAdvancedOptionsSettings>
                                          {
                                              MachineName = ApplicationContext.Current.ComputerName,
                                              Name = ApplicationSetting.PatientSearchAdvancedOptions,
                                              Value = setting != null ? setting.Value.CastTo<PatientSearchAdvancedOptionsSettings>() : new PatientSearchAdvancedOptionsSettings(),
                                              Id = setting != null ? setting.Id : null,
                                              UserId = UserContext.Current.UserDetails.Id
                                          };
            PatientSearchAdvancedOptionsSettings advancedOptionSettings = _advancedOptionsUserSetting.Value ?? new PatientSearchAdvancedOptionsSettings();
            if (advancedOptionSettings.TextSearchModes.Any()) TextSearchModes = advancedOptionSettings.TextSearchModes.ToObservableCollection();
            if (advancedOptionSettings.SearchFields.Any())
            {
                foreach (var selectedSearchField in advancedOptionSettings.SearchFields.ToList())
                {
                    SearchFields.WithId(selectedSearchField.Id).IsSelected = selectedSearchField.IsSelected;
                }
                SearchFields = SearchFields.ToExtendedObservableCollection();
            }
        }

        private void ExecuteLoad()
        {
            if (LoadArguments != null)
            {
                // Set up SearchText
                SearchText = LoadArguments.SearchText;
            }

            // Suggest recently accessed patients
            HistoryResults = _patientSearchViewService.SuggestRecentPatients(UserContext.Current.UserDetails.Id, ApplicationContext.Current.ComputerName).ToExtendedObservableCollection();

            LoadAdvancedOptionsSettings();

            // Run search if specified
            if (LoadArguments.IfNotNull(p => p.RunSearchOnLoad))
            {
                Search.Execute();
            }
        }

        private void ExecuteSearch()
        {
            SearchResults = null;

            TermsToHighlight = PatientSearchExtensions.GetSearchTerms(SearchText).ToExtendedObservableCollection();

            var textSearchMode = (TextSearchMode)TextSearchModes.First(tsm => tsm.IsSelected).Id;
            var searchField = SearchFields.Where(so => so.IsSelected).
                Select(so => so.Id).
                Aggregate(0, (current, so) => current | so)
                .CastTo<SearchFields>();

            SearchResults = _patientSearchViewService.Search(textSearchMode, searchField, SearchText).ToExtendedObservableCollection();
        }

        private void ExecuteReset()
        {
            SetDefaultOptions();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteSelect()
        {
            SelectedCreatePatient = false;

            //Add Patient to Recently Accessed list.
            Task.Factory.StartNewWithCurrentTransaction(() => _patientSearchViewService.RecordPatientAccessed(SelectedSearchResult.Id, UserContext.Current.UserDetails.Id, ApplicationContext.Current.ComputerName));

            //Log the search.
            _logger.Log("Searched for patient {0}.".FormatWith(SelectedSearchResult.Id),
                categories: new[] { LogCategory.Query },
                properties: new[] { new LogEntryProperty { Name = LogEntryPropertyName.PatientId, Value = SelectedSearchResult.Id.ToString() } });

            InteractionContext.Complete(true);
        }

        private void ExecuteCreatePatient()
        {
            SelectedCreatePatient = true;
            InteractionContext.Complete(true);
        }

        private void SaveAdvancedOptionsSettings()
        {
            _advancedOptionsUserSetting.Value = new PatientSearchAdvancedOptionsSettings { TextSearchModes = TextSearchModes.ToList(), SearchFields = SearchFields.ToList() };
            // Push settings to server
            Task.Factory
                .StartNewWithCurrentTransaction(() =>
                    _applicationSettingsService.SetSetting(_advancedOptionsUserSetting))
                .Wait();
        }
        private void ExecuteLoadPatientPhoto(PatientSearchPatientViewModel patient)
        {
            if (patient.Photo == null && !patient.HasLoadedPhoto)
            {
                _patientPhotoTaskQueue.Enqueue(() =>
                                                   {
                                                       patient.Photo = _imageService.GetPatientPhoto(patient.Id);
                                                       patient.HasLoadedPhoto = true;
                                                   });
            }
        }

        private void ExecuteLoadPatientPostOpPeriod(PatientSearchPatientViewModel patient)
        {
            if (patient.PostOp == null && !patient.HasLoadedPostOp)
            {
                _postOpTaskQueue.Enqueue(() =>
                                             {
                                                 patient.PostOp = _postOperativePeriodService.GetLatestPostOperativePeriodByPatientId(patient.Id);
                                                 if (patient.PostOp != null)
                                                 {
                                                     patient.HasLoadedPostOp = true;
                                                 }
                                             });
            }
        }

        public PatientSearchLoadArguments LoadArguments { get; set; }

        public virtual string SearchText { get; set; }

        [DependsOn("HistoryResults")]
        public virtual bool CanShowHistory
        {
            get { return HistoryResults != null && HistoryResults.Count != 0; }
        }

        public virtual ObservableCollection<PatientSearchPatientViewModel> HistoryResults { get; set; }

        public virtual ObservableCollection<TextSearchModeViewModel> TextSearchModes { get; protected set; }

        public virtual ObservableCollection<SelectableNamedViewModel> SearchFields { get; protected set; }

        [DispatcherThread]
        public virtual ObservableCollection<PatientSearchPatientViewModel> SearchResults { get; set; }

        public virtual PatientSearchPatientViewModel SelectedSearchResult { get; set; }

        public virtual bool SelectedCreatePatient { get; private set; }

        [DispatcherThread]
        public virtual ObservableCollection<string> TermsToHighlight { get; set; }

        [DependsOn("SearchResults")]
        public virtual bool ShowPreSearchBorder
        {
            get { return SearchResults == null; }
        }

        [DependsOn("SearchResults")]
        public virtual bool ShowNoResultsBorder
        {
            get { return SearchResults != null && SearchResults.Count == 0; }
        }

        [DependsOn("SearchResults")]
        public virtual bool ShowResultsGrid
        {
            get { return SearchResults != null && SearchResults.Count != 0; }
        }

        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command that is executed when this instance is closing
        /// </summary>
        public virtual ICommand Closing { get; protected set; }

        public ICommand Search { get; protected set; }

        public ICommand Reset { get; protected set; }

        public ICommand Close { get; protected set; }

        public virtual ICommand CreatePatient { get; protected set; }

        public ICommand Select { get; protected set; }

        public ICommand LoadPatientPhoto { get; protected set; }

        public ICommand LoadPatientPostOpPeriod { get; set; }
    }
}