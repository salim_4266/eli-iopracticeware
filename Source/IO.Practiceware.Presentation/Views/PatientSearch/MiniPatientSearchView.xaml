﻿<s:View xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
        xmlns:s="http://soaf/presentation"
        xmlns:telerik="http://schemas.telerik.com/2008/xaml/presentation"
        xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"        
        xmlns:Converters="clr-namespace:IO.Practiceware.Presentation.Views.Common.Converters"
        xmlns:Behaviors="clr-namespace:IO.Practiceware.Presentation.Views.Common.Behaviors"
        xmlns:local="clr-namespace:IO.Practiceware.Presentation.Views.PatientSearch"        
        xmlns:Controls="clr-namespace:IO.Practiceware.Presentation.Views.Common.Controls"        
        xmlns:wizard="clr-namespace:IO.Practiceware.Presentation.Views.Wizard"
        mc:Ignorable="d"
        x:Class="IO.Practiceware.Presentation.Views.PatientSearch.MiniPatientSearchView" 
        AutoCreateDataContext="True"
        ViewContextType="local:MiniPatientSearchViewContext" 
        x:Name="RootView"
        Header="{Binding WindowTitle}"
        d:DesignWidth="640" d:DesignHeight="330" Load="{Binding Load}">

    <s:View.Resources>
        <Converters:BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
        <Converters:SearchScreenScrollBarVisibilityConverter x:Key="SearchScreenScrollBarVisibilityConverter" />
        <Converters:IsNullOrEmptyToVisibilityConverter x:Key="NullOrEmptyVisibilityConverter" />
        <local:PatientSearchCustomRadGridViewBehavior x:Key="PatientSearchCustomRadGridViewBehavior"
                LoadPatientPhoto="{Binding LoadPatientPhoto}"
                LoadPatientPostOpPeriod="{Binding LoadPatientPostOpPeriod}" />
        <Behaviors:ScrollToFirstItemRadGridViewBehavior x:Key="ScrollToFirstItemRadGridViewBehavior" />
    </s:View.Resources>

    <Grid x:Name="LayoutRoot" Background="White">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
            <RowDefinition Height="44" />
        </Grid.RowDefinitions>
        <Border Grid.Row="0" x:Name="InfoArea" BorderThickness="0,0,0,1"
                BorderBrush="{DynamicResource StandardBorderBrush}" Background="{StaticResource  StandardFocusBrush}">
            <Grid HorizontalAlignment="Center">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>
                <StackPanel Grid.Row="0" Orientation="Horizontal" HorizontalAlignment="Center" Margin="0,10,0,0">
                    <telerik:RadWatermarkTextBox WatermarkContent="Enter details to search for here..."
                            s:FocusBehavior.FocusOnLoad="true" Name="SearchText" FontSize="18" Width="400" Height="40"
                            HorizontalAlignment="Left" VerticalAlignment="Center" Margin="0"
                            Text="{Binding SearchText, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
                    <Button Content="Search" HorizontalAlignment="Left" Width="85" Height="30" FontSize="13.333"
                            Style="{StaticResource  ActionButtonStyle}" IsDefault="True" Margin="20,0,0,0"
                            Command="{Binding Search}" />
                </StackPanel>
                <Grid Grid.Row="1" HorizontalAlignment="Center" Margin="10">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <TextBlock Grid.Row="0" Grid.ColumnSpan="5" Text="Advanced Options:" FontFamily="Segoe UI Semibold"
                            Foreground="White" FontSize="14" />
                    <ItemsControl ItemsSource="{Binding TextSearchModes}" Margin="10,0" Grid.Row="1">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <RadioButton Foreground="White" FontFamily="Segoe UI" FontSize="13.333"
                                        GroupName="TextSearchModeGroup" Content="{Binding Name}"
                                        IsChecked="{Binding IsSelected}" ToolTip="{Binding ToolTipText}" />
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                        <ItemsControl.ItemsPanel>
                            <ItemsPanelTemplate>
                                <StackPanel />
                            </ItemsPanelTemplate>
                        </ItemsControl.ItemsPanel>
                    </ItemsControl>

                    <ItemsControl ItemsSource="{Binding SearchFields}" Margin="10,0" Grid.Column="1" Grid.Row="1">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <CheckBox Content="{Binding Name}" IsChecked="{Binding IsSelected}" Foreground="White"
                                        FontFamily="Segoe UI" FontSize="13.333" />
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                        <ItemsControl.ItemsPanel>
                            <ItemsPanelTemplate>
                                <UniformGrid Columns="3" />
                            </ItemsPanelTemplate>
                        </ItemsControl.ItemsPanel>
                    </ItemsControl>
                    <Button Grid.Column="4" Grid.Row="1" Content="Reset" HorizontalAlignment="Right" TabIndex="9"
                            VerticalAlignment="Top" Width="60" Margin="10,0" Background="#FFE2E2E2"
                            Style="{StaticResource  NormalButtonStyle}" Height="25"
                            ToolTip="Reset the Advanced Options to the default settings" FontSize="13.333"
                            Command="{Binding Reset}" />
                </Grid>
            </Grid>
        </Border>
        <Grid Grid.Row="1">
            <Border x:Name="PreSearchBorder"
                    Visibility="{Binding ShowPreSearchBorder, Converter={StaticResource BooleanToVisibilityConverter}}"
                    Grid.Row="1" BorderBrush="{StaticResource  StandardFocusBrush}" BorderThickness="2" Margin="75,15"
                    Height="Auto" VerticalAlignment="Top">
                <TextBlock Text="Search on any information in the area above to find a patient" FontFamily="Segoe UI"
                        TextWrapping="Wrap" FontWeight="Normal" FontSize="18"
                        Foreground="{StaticResource  StandardFocusBrush}" TextAlignment="Center" Margin="10" />
            </Border>
            <Border x:Name="NoResultsBorder"
                    Visibility="{Binding ShowNoResultsBorder, Converter={StaticResource BooleanToVisibilityConverter}}"
                    Grid.Row="1" Background="#FFDDDDDD" BorderBrush="{StaticResource  StandardFocusBrush}"
                    BorderThickness="2" Margin="75,15" Height="Auto" VerticalAlignment="Top">
                <StackPanel Orientation="Vertical" Margin="10">
                    <TextBlock Text="No Results to Display" FontFamily="Segoe UI" FontWeight="Bold" FontSize="18"
                            Foreground="{StaticResource  StandardFocusBrush}" TextAlignment="Center" />
                    <TextBlock Text="Please try searching for different details" FontFamily="Segoe UI"
                            FontWeight="Normal" FontSize="14" Foreground="Black" TextAlignment="Center" />
                </StackPanel>
            </Border>
            <telerik:RadGridView ShowGroupPanel="False"
                    x:Name="ResultsGrid" Grid.Row="1" ScrollMode="Deferred" ColumnWidth="Auto"
                    AutoGenerateColumns="False" SelectionMode="Single" IsReadOnly="True"
                    RowIndicatorVisibility="Collapsed" CanUserFreezeColumns="False" EnableColumnVirtualization="True"
                    EnableRowVirtualization="True"
                    AlternationCount="2" 
                    Visibility="{Binding ShowResultsGrid, Converter={StaticResource BooleanToVisibilityConverter}}"
                    ScrollViewer.HorizontalScrollBarVisibility="{Binding SearchResults, Converter={StaticResource SearchScreenScrollBarVisibilityConverter}}"
                    ScrollViewer.VerticalScrollBarVisibility="{Binding SearchResults, Converter={StaticResource SearchScreenScrollBarVisibilityConverter}}"
                    ItemsSource="{Binding SearchResults}"
                    Behaviors:RadGridViewToggleableSingleSelectBehavior.EnableToggleableSingleSelect="True"
                    SelectedItem="{Binding SelectedSearchResult}">
                <telerik:RadGridView.Resources>
                    <Style TargetType="telerik:GridViewCell">
                        <Setter Property="VerticalContentAlignment" Value="Stretch" />
                    </Style>
                </telerik:RadGridView.Resources>
                <telerik:RadGridView.ScrollPositionIndicatorTemplate>
                    <DataTemplate>
                        <TextBlock Text="{Binding LastName}" />
                    </DataTemplate>
                </telerik:RadGridView.ScrollPositionIndicatorTemplate>
                <i:Interaction.Behaviors>
                    <StaticResource ResourceKey="PatientSearchCustomRadGridViewBehavior" />
                    <StaticResource ResourceKey="ScrollToFirstItemRadGridViewBehavior" />
                </i:Interaction.Behaviors>
                <telerik:RadGridView.Columns>
                    <telerik:GridViewDataColumn Header="ID" Width="50" DataMemberBinding="{Binding Id}">
                       
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Last Name" Width="150" DataMemberBinding="{Binding LastName}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="First Name" Width="150" DataMemberBinding="{Binding FirstName}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Suffix" Width="100" DataMemberBinding="{Binding Suffix}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Middle Name" TextAlignment="Left" DataMemberBinding="{Binding MiddleName}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="DOB" Width="75" DataMemberBinding="{Binding DateOfBirth, StringFormat={}{0:MM/dd/yyyy}}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Patient Type" Width="100" DataMemberBinding="{Binding MultiTypeName}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Next Appt" Width="75" DataMemberBinding="{Binding NextAppointmentDate, StringFormat={}{0:MM/dd/yyyy}}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Last Appt" Width="75" DataMemberBinding="{Binding LastAppointmentDate, StringFormat={}{0:MM/dd/yyyy}}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Phone" Width="120" DataMemberBinding="{Binding MultiPhoneNumber}">
                    </telerik:GridViewDataColumn>
                    <telerik:GridViewDataColumn Header="Prior ID" Width="50" DataMemberBinding="{Binding PriorId}">
                    </telerik:GridViewDataColumn>
                </telerik:RadGridView.Columns>
                <i:Interaction.Triggers>
                    <i:EventTrigger EventName="MouseDoubleClick">
                        <i:InvokeCommandAction Command="{Binding Wizard.Next}" />
                    </i:EventTrigger>
                </i:Interaction.Triggers>
            </telerik:RadGridView>
        </Grid>
        <wizard:WizardNavigationView Grid.Row="2" UtilityButtonVisibility="Visible" DataContext="{Binding Wizard}"
                Visibility="{Binding DataContext.Wizard, Mode=OneWay, Converter={StaticResource NullOrEmptyVisibilityConverter}, RelativeSource={RelativeSource AncestorType={x:Type s:View}}}" 
                                     />
    </Grid>
</s:View>