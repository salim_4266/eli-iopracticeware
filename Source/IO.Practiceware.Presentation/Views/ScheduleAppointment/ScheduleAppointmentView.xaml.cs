﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.ViewModels.ScheduleAppointment;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.PatientReferralPopup;
using IO.Practiceware.Presentation.Views.Recalls;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.ScheduleAppointment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.ScheduleAppointment
{
    public enum EntrySource
    {
        None,
        ScheduleBlock,
        Appointment
    }

    /// <summary>
    /// Interaction logic for ScheduleAppointmentView.xaml
    /// </summary>
    public partial class ScheduleAppointmentView
    {
        public ScheduleAppointmentView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the ScheduleAppointmentView
    /// </summary>
    [SupportsDataErrorInfo]
    public class ScheduleAppointmentViewContext : IViewContext, ICommonViewFeatureHandler
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IScheduleAppointmentViewService _scheduleAppointmentViewService;
        private readonly Func<RecallsViewManager> _createRecallsViewManager;
        private readonly Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> _createPatientInsuranceAuthorizationPopupViewContext;

        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly IPatientInfoViewService _patientInfoViewService;

        private EntrySource _entrySource = EntrySource.None;
        private bool _isPartialSaving;
        private bool _donePartialSave;
        private AppointmentTypeViewModel _selectedAppointmentType;
        private Tuple<string, InsuranceType> _selectedInsuranceType;
        private string _comment;
        private bool _listOfAppointmentTypeActivated;

        public ScheduleAppointmentViewContext(IScheduleAppointmentViewService scheduleAppointmentViewService,
                                              Func<RecallsViewManager> createRecallsViewManager,
                                              Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> createPatientInsuranceAuthorizationPopupViewContext,
                                              ICommonViewFeatureExchange exchange,
                                              PatientInfoViewManager patientInfoViewManager,
                                              IPatientInfoViewService patientInfoViewService)
        {
            _scheduleAppointmentViewService = scheduleAppointmentViewService;
            _createRecallsViewManager = createRecallsViewManager;
            _createPatientInsuranceAuthorizationPopupViewContext = createPatientInsuranceAuthorizationPopupViewContext;
            _patientInfoViewManager = patientInfoViewManager;
            _patientInfoViewService = patientInfoViewService;

            Init();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ListOfAppointmentTypeInteraction = Command.Create(ExecuteListOfAppointmentTypeInteraction);
            SelectedInsuranceTypeChanged = Command.Create(ExecuteSelectedInsuranceTypeChanged).Async(() => InteractionContext);
            PartialSave = Command.Create(ExecutePartialSave, CheckPartialSaveCanExecute).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            DiscardUnsavedChanges = Command.Create(ExecuteDiscardUnsavedChanges).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CheckSaveCanExecute).Async(() => InteractionContext);
            CancelOrderDateSelection = Command.Create(ExecuteCancelOrderDateSelection);
            Closing = Command.Create<Action>(ExecuteClosing);

            // Declare support of features (more in OnLoad)
            ViewFeatureExchange = exchange;
        }

        private void Init()
        {
            ListOfInsuranceType = Enums.GetValues<InsuranceType>().Select(i => Tuple.Create(i.GetDisplayName(), i)).ToExtendedObservableCollection();
        }

        private void ExecuteLoad()
        {
            ProcessLoadArguments();
            if (SelectedPatient != null)
            {
                // Init alerts functionality
                ViewFeatureExchange.HasPendingAlert = _patientInfoViewService.CheckHasPendingAlert(SelectedPatient.Id, (int)PatientAlertType.Schedule);
                ViewFeatureExchange.ShowPendingAlert = () =>
                {
                    // Open Alerts screen for current patient
                    _patientInfoViewManager.ShowPatientAlert(SelectedPatient.Id, PatientAlertType.Schedule);

                    // Opening pending alerts clears the flag
                    ViewFeatureExchange.HasPendingAlert = false;
                };
                Referral.AppointmentStartDateTime = AppointmentStartDateTime;
                Referral.PatientId = SelectedPatient.Id;
                GetExistingReferralInformation();
            }
        }

        private void ProcessLoadArguments()
        {
            if (LoadArguments == null) return;

            IsEditingAppointment = LoadArguments.AppointmentId.HasValue;

            var patientViewModel = _scheduleAppointmentViewService.GetPatient(LoadArguments.PatientId);
            SelectedPatient = patientViewModel;

            // Set up ReasonForVisit
            ReasonForVisit = LoadArguments.ReasonForVisit;

            // Set up Comment
            Comment = LoadArguments.Comment;

            // Set up Appt Type
            int? appointmentCategoryId = LoadArguments.AppointmentCategoryId.HasValue ? LoadArguments.AppointmentCategoryId.Value : 0;
            ConfigureAppointmentType(appointmentCategoryId, LoadArguments.AppointmentTypeId);

            // Set up Appointment
            var appointment = !LoadArguments.AppointmentId.HasValue ? null : _scheduleAppointmentViewService.GetAppointment(LoadArguments.AppointmentId.Value);
            ConfigureAppointment(appointment);

            // Already have a valid entry - You are done here
            if (_entrySource != EntrySource.None) return;

            // Set up ScheduleBlock
            if (LoadArguments.ScheduleBlockId == null && LoadArguments.ScheduleBlockAppointmentCategoryId == null) return;

            SelectedScheduleBlock = _scheduleAppointmentViewService.GetScheduleBlock(LoadArguments.ScheduleBlockId, LoadArguments.ScheduleBlockAppointmentCategoryId);
            if (SelectedScheduleBlock != null) _entrySource = EntrySource.ScheduleBlock;
            ConfigurePatientInsuranceAuthorization(null, null, PartialSave, false);
        }

        private void ConfigureAppointmentType(int? appointmentCategoryId, int? appointmentTypeId)
        {
            bool canForceAppointments = PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission();
            ListOfAppointmentType = _scheduleAppointmentViewService.LoadAppointmentTypes(canForceAppointments ? null : appointmentCategoryId);
            var index = ListOfAppointmentType.ToList().FindIndex(t => t.Id == appointmentTypeId);
            if (index != -1)
            {
                SelectedAppointmentType = null;
                var type = ListOfAppointmentType.ElementAt(index);
                SelectedAppointmentType = type;
            }
        }

        private void ConfigureAppointment(AppointmentViewModel appointment)
        {
            SelectedAppointment = appointment;
            if (SelectedAppointment == null) return;

            ConfigureUserInterface();
            _entrySource = EntrySource.Appointment;
            Referral.SavePending = false;
            SelectedAppointment.CastTo<IEditableObject>().BeginEdit();

        }

        private void ConfigureUserInterface(bool launchAuthorizationScreen = false)
        {
            if (SelectedAppointment != null)
            {
                SelectedScheduleBlock = null;
                SelectedPatient = SelectedAppointment.Patient;
                ConfigureAppointmentType(SelectedAppointment.Type.CategoryId, SelectedAppointment.Type.Id);
                var appointmentInsurance = ListOfInsuranceType.FirstOrDefault(i => (int)i.Item2 == SelectedAppointment.InsuranceTypeId);
                SelectedInsuranceType = appointmentInsurance;
                Referral.ListOfAttachedReferral = SelectedAppointment.Encounter.Referrals.ToExtendedObservableCollection();
                ConfigurePatientInsuranceAuthorization(SelectedAppointment.Encounter.Id, SelectedAppointment.Encounter.Authorization, null, launchAuthorizationScreen);
                Comment = SelectedAppointment.Encounter.Comment;
            }
        }


        private void ConfigurePatientInsuranceAuthorization(int? encounterId, PatientInsuranceAuthorizationViewModel authorization, ICommand onButtonClick, bool launchAuthorizationScreen)
        {
            var loadArguments = new PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupLoadArguments();
            loadArguments.OperationMode = PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupOperationMode.Deferred;
            loadArguments.OnButtonClick = onButtonClick;
            loadArguments.EncounterId = encounterId;
            loadArguments.DirectlyLaunchAuthorizationScreen = launchAuthorizationScreen;
            // Because of the partial save when dealing with schedule block [ExecutePartialSave()]
            // Guard against overwriting the existing view context
            if (PatientInsuranceAuthorizationPopupViewContext == null) PatientInsuranceAuthorizationPopupViewContext = _createPatientInsuranceAuthorizationPopupViewContext();
            if (authorization != null)
            {
                loadArguments.AttachedPatientInsuranceAuthorization = authorization;
            }

            PatientInsuranceAuthorizationPopupViewContext.DirectlyLoadArguments(loadArguments);
        }

        private void GetExistingReferralInformation()
        {
            if (!AppointmentStartDateTime.HasValue) return;
            Referral.IsReferralInformationRequired = _scheduleAppointmentViewService.GetIsReferralInformationRequired(SelectedPatient.Id, SelectedInsuranceType.Item2, AppointmentStartDateTime.Value);
            var referrals = _scheduleAppointmentViewService.GetPatientExistingReferralInformation(SelectedPatient.Id);
            Referral.ExistingReferralInformation = new ExtendedObservableCollection<ReferralViewModel>();
            Referral.ListOfAttachedReferral = Referral.ListOfAttachedReferral.ToExtendedObservableCollection();
            Referral.ValidateReferralAndIncludeInExisting(referrals.Where(x => Referral.ListOfAttachedReferral.All(y => y.Id != x.Id)));
        }

        private void ExecuteListOfAppointmentTypeInteraction()
        {
            _listOfAppointmentTypeActivated = true;
        }

        private void ExecuteSelectedInsuranceTypeChanged()
        {
            if (!AppointmentStartDateTime.HasValue) return;
            Referral.IsReferralInformationRequired = _scheduleAppointmentViewService.GetIsReferralInformationRequired(SelectedPatient.Id, SelectedInsuranceType.Item2, AppointmentStartDateTime.Value);
        }

        private bool CheckPartialSaveCanExecute()
        {
            if (_entrySource == EntrySource.ScheduleBlock && (CheckSaveCanExecute() || SelectedAppointment != null)) return true;
            if (_entrySource == EntrySource.Appointment) return true;
            return false;
        }

        private void ExecutePartialSave()
        {
            _isPartialSaving = true;
            if (_entrySource == EntrySource.Appointment || _donePartialSave) return;
            _donePartialSave = true;
            SelectedAppointment = _scheduleAppointmentViewService.SaveAppointment(SelectedScheduleBlock, SelectedAppointment, SelectedAppointmentType, SelectedInsuranceType.Item2, SelectedPatient, Referral.ListOfAttachedReferral.ToList(), null, ReasonForVisit, Comment, SelectedOrderDate);
            ConfigureUserInterface(true);
            _isPartialSaving = false;
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteClosing(Action cancelClose)
        {
            var authorizationChanges = PatientInsuranceAuthorizationPopupViewContext != null && PatientInsuranceAuthorizationPopupViewContext.HasChanges;
            var hasChanged = InteractionContext.DialogResult.HasValue && InteractionContext.DialogResult.Value;
            if ((Referral.SavePending || authorizationChanges) && !hasChanged)
            {
                DiscardUnsavedChanges.Execute();
            }
        }

        private void ExecuteDiscardUnsavedChanges()
        {
            if (PatientInsuranceAuthorizationPopupViewContext != null)
                PatientInsuranceAuthorizationPopupViewContext.RevertChanges();
            if (SelectedAppointment != null)
            {
                if (_entrySource == EntrySource.ScheduleBlock)
                {
                    _scheduleAppointmentViewService.DeleteAppointment(SelectedAppointment.Id);
                    SelectedAppointment = null;
                }
            }
        }

        private void ExecuteCancelOrderDateSelection()
        {
            SelectedOrderDate = null;
        }

        private bool CheckSaveCanExecute()
        {
            var hasBasicInfo = AppointmentDetailLocation != null && AppointmentDetailResource != null && AppointmentDetailDate != null && AppointmentDetailTime != null;
            var authorizationChanges = PatientInsuranceAuthorizationPopupViewContext != null && PatientInsuranceAuthorizationPopupViewContext.HasChanges;
            return SelectedPatient != null && hasBasicInfo && SelectedAppointmentType != null && SelectedInsuranceType != null && (Referral.SavePending || authorizationChanges);
        }

        private void ExecuteSave()
        {
            if (_entrySource == EntrySource.Appointment && SelectedAppointment != null) SelectedAppointment.CastTo<IEditableObject>().EndEdit();

            if (SelectedAppointmentType.IsOrderRequired)
            {
                if (LoadArguments != null && LoadArguments.OrderAppointmentDate.HasValue)
                {
                    SelectedOrderDate = LoadArguments.OrderAppointmentDate;
                }

                if (!SelectedOrderDate.HasValue)
                {
                    IsOrderRequiredDataTrigger = true;
                    if (!SelectedOrderDate.HasValue)
                    {
                        return;
                    }
                }
            }

            // Grab the authorization id from the user control so it can be saved with the appointment (create or editing)
            PatientInsuranceAuthorizationPopupViewContext.AcceptChanges();
            var patientInsuranceAuthorizationPopupReturnArguments = PatientInsuranceAuthorizationPopupViewContext.DirectlyReturnArguments();
            int? authorizationId = null;
            if (patientInsuranceAuthorizationPopupReturnArguments != null && patientInsuranceAuthorizationPopupReturnArguments.AuthorizationId.HasValue)
            {
                authorizationId = patientInsuranceAuthorizationPopupReturnArguments.AuthorizationId.Value;
            }

            var appointment = _scheduleAppointmentViewService.SaveAppointment(SelectedScheduleBlock, SelectedAppointment, SelectedAppointmentType, SelectedInsuranceType.Item2, SelectedPatient, Referral.ListOfAttachedReferral.ToList(), authorizationId, ReasonForVisit, Comment, SelectedOrderDate);
            if (SelectedAppointment == null) SelectedAppointment = appointment;
            Referral.SavePending = false;

            var hasRecalls = _scheduleAppointmentViewService.HasPendingRecalls(SelectedPatient.Id);
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                // check for pending recalls and display the recall window if any
                if (hasRecalls)
                {
                    var viewManager = _createRecallsViewManager();
                    var loadArgs = new RecallLoadArguments
                    {
                        PatientId = SelectedPatient.Id,
                        ShowRecallListOnly = true
                    };

                    viewManager.ShowRecalls(loadArgs);
                }
                InteractionContext.Complete(true);
            });

        }

        public ScheduleAppointmentLoadArguments LoadArguments { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedPatient { get; set; }

        [DispatcherThread]
        public virtual ScheduleBlockViewModel SelectedScheduleBlock { get; set; }

        [DispatcherThread]
        public virtual AppointmentViewModel SelectedAppointment { get; set; }

        #region AppointmentDetail

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual int AppointmentDetailLocationId
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.Location.Id;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.Location.Id;
                }
                return 0;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual string AppointmentDetailLocation
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.Location.Name;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.Location.Name;
                }
                return null;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual int AppointmentDetailResourceId
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.Resource.Id;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.Resource.Id;
                }
                return 0;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual string AppointmentDetailResource
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.Resource.DisplayName;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.Resource.DisplayName;
                }
                return null;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual string AppointmentDetailDate
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.StartDateTime.Date.ToString("MM/dd/yyyy");
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.StartDateTime.Date.ToString("MM/dd/yyyy");
                }
                return null;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual string AppointmentDetailTime
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.StartDateTime.ToString("t");
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.StartDateTime.ToString("t");
                }
                return null;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual DateTime? AppointmentStartDateTime
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.StartDateTime;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.StartDateTime;
                }
                return null;
            }
        }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock")]
        public virtual DateTime? AppointmentEndDateTime
        {
            get
            {
                if (SelectedAppointment != null)
                {
                    return SelectedAppointment.EndDateTime;
                }
                if (SelectedScheduleBlock != null)
                {
                    return SelectedScheduleBlock.EndDateTime;
                }
                return null;
            }
        }

        #endregion

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentTypeViewModel> ListOfAppointmentType { get; set; }

        [DispatcherThread]
        [Required(ErrorMessage = "Please select an appointment type")]
        public virtual AppointmentTypeViewModel SelectedAppointmentType
        {
            get { return _selectedAppointmentType; }
            set
            {
                _selectedAppointmentType = value;
                if (!_listOfAppointmentTypeActivated) return;
                Referral.SavePending = true;
                if (!_isPartialSaving)
                {
                    bool canForceAppointments = PermissionId.ForceScheduleAppointment.PrincipalContextHasPermission();
                    bool isMismatchType = _selectedAppointmentType == null || (_selectedAppointmentType.CategoryId != LoadArguments.AppointmentCategoryId);
                    isMismatchType = isMismatchType && canForceAppointments;
                    if (isMismatchType) InteractionManager.Current.Alert("Appointment Type does not match the defined schedule.  Are you sure you would like to force this appointment into the schedule?");
                }
            }
        }

        public virtual ObservableCollection<Tuple<string, InsuranceType>> ListOfInsuranceType { get; set; }

        [DispatcherThread]
        [Required(ErrorMessage = "Please select an insurance type")]
        public virtual Tuple<string, InsuranceType> SelectedInsuranceType
        {
            get { return _selectedInsuranceType; }
            set
            {
                _selectedInsuranceType = value;
                Referral.SavePending = true;
            }
        }


        [Dependency]
        public virtual PatientReferralPopupViewContext Referral { get; set; }

        [DependsOn("SelectedAppointment", "SelectedScheduleBlock", "SelectedAppointmentType", "SelectedInsuranceType", "SelectedActiveReferral", "Comment", "PatientInsuranceAuthorizationPopupViewContext")]
        public virtual bool ShowAuthorizationButton
        {
            get { return CheckPartialSaveCanExecute(); }
        }

        /// <summary>
        /// This view context backs another s:view that is used within the parent s:view
        /// </summary>
        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext PatientInsuranceAuthorizationPopupViewContext { get; set; }

        [DispatcherThread]
        public virtual string ReasonForVisit { get; set; }

        [DispatcherThread]
        public virtual string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                Referral.SavePending = true;
            }
        }

        [DispatcherThread]
        public virtual bool IsOrderRequiredDataTrigger { get; set; }

        [DispatcherThread]
        public virtual DateTime? SelectedOrderDate { get; set; }

        [DispatcherThread]
        public virtual bool IsEditingAppointment { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand ListOfAppointmentTypeInteraction { get; protected set; }

        public ICommand SelectedInsuranceTypeChanged { get; protected set; }


        public ICommand PartialSave { get; protected set; }

        public ICommand Close { get; protected set; }

        public ICommand DiscardUnsavedChanges { get; protected set; }

        public ICommand Save { get; protected set; }

        public ICommand CancelOrderDateSelection { get; protected set; }

        /// <summary>
        /// Gets a command that is executed when the associated view is closing.
        /// It is able to cancel the closing event through the passed in bool Action.
        /// </summary>
        public ICommand Closing { get; protected set; }

        public ICommonViewFeatureExchange ViewFeatureExchange { get; private set; }
        public PatientInsuranceAuthorizationViewModel AttachedPatientInsuranceAuthorization { get; set; }
    }
}