﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.ScheduleAppointment;
using Soaf;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.ScheduleAppointment
{
    public class ScheduleAppointmentViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IScheduleAppointmentViewService _viewService;

        public ScheduleAppointmentViewManager(IInteractionManager interactionManager, IScheduleAppointmentViewService viewService)
        {
            _interactionManager = interactionManager;
            _viewService = viewService;
        }

        public ScheduleAppointmentReturnArguments ShowScheduleAppointment(ScheduleAppointmentLoadArguments loadArguments)
        {
            bool isPatientActive = LoadingWindow.Run(() => _viewService.IsPatientActive(loadArguments.PatientId));
            if (!isPatientActive)
            {
                InteractionManager.Current.Alert("Cannot schedule an appointment for an inactive patient.");
                return new ScheduleAppointmentReturnArguments();
            }

            if (!PermissionId.ScheduleAppointment.EnsurePermission()) return new ScheduleAppointmentReturnArguments();

            var view = new ScheduleAppointmentView();

            var dataContext = view.DataContext.EnsureType<ScheduleAppointmentViewContext>();
            if (loadArguments != null) dataContext.LoadArguments = loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });

            return new ScheduleAppointmentReturnArguments { AppointmentId = dataContext.SelectedAppointment == null ? new int?() : dataContext.SelectedAppointment.Id };
        }
    }

    public class ScheduleAppointmentLoadArguments
    {
        public string ReasonForVisit { get; set; }
        public string Comment { get; set; }
        public int PatientId { get; set; }
        public string PatientDisplayName { get; set; }
        public int? AppointmentTypeId { get; set; }
        public int? AppointmentId { get; set; }
        public int? ScheduleBlockId { get; set; }
        public Guid? ScheduleBlockAppointmentCategoryId { get; set; } // Used to save the appointment id from a newly created appointment back to the model.ScheduleBlockAppointmentCategories entry
        public int? AppointmentCategoryId { get; set; } // Used to get the list of model.AppointmentTypes from an appointment category

        /// <summary>
        /// Gets or sets the date time of the (optional) order appointment.
        /// </summary>
        /// <remarks>
        /// An order appointment is one that orders a Surgery Appointment.
        /// </remarks>
        public DateTime? OrderAppointmentDate { get; set; }
    }

    public class ScheduleAppointmentReturnArguments
    {
        public int? AppointmentId { get; set; }
    }
}