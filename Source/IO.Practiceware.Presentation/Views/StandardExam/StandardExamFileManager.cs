﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.ViewModels.StandardExam;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using IO.Practiceware.Data;
using Soaf.Data;
using System.Data;

namespace IO.Practiceware.Presentation.Views.StandardExam
{
    [Singleton]
    public class StandardExamFileManager
    {
        private static string LocalUserAppPath
        {
            get
            {
                return Path.Combine(FileManager.Instance.Configuration.ApplicationDataPath, "DoctorInterface");
        }
        }

        public void SaveOfficeVisitInfo(ServiceViewModel officeVisit, int encounterId, int patientId, string procedureHighLight, bool diagMode, ObservableCollection<DiagnosisViewModel> diagnoses)
        {
            string fileName = string.Format("{0}_{1}_{2}.txt", "EMProc_Icd10", encounterId, patientId);

            string filePath = Path.Combine(LocalUserAppPath, fileName);

            var fileContent = new StringBuilder("");

            fileContent.Append(officeVisit.Code != null ? string.Format("{0}{1}", officeVisit.Code.Code, Environment.NewLine) : Environment.NewLine);

            foreach (var modifier in officeVisit.Modifiers)
            {
                fileContent.Append(modifier.Abbreviation);
            }

            fileContent.Append(Environment.NewLine);

            if (officeVisit.DiagnosisLinks.Count > 0)
            {
                var count = 0;
                foreach (var diagnosis in officeVisit.DiagnosisLinks)
                {
                    if (count >= 4)
                    {
                        break;
                    }
                    if (diagnosis.OrdinalId == 9)
                        fileContent.Append("0");
                    else if (diagnosis.OrdinalId < 9)
                        fileContent.Append(diagnosis.OrdinalId + 1);

                    count = count + 1;
                    
                }
            }
           

            fileContent.Append(Environment.NewLine);

            fileContent.Append(string.Format("{0}{1}", procedureHighLight, Environment.NewLine));

            fileContent.Append(diagMode ? "ICD10" : "IC9");

            FileManager.Instance.CommitContents(filePath, fileContent.ToString());
        }

        public string[] GetTestFiles(int? encounterId, int? patientId)
        {
            var scratchFiles = Path.Combine(LocalUserAppPath, string.Format("T*_{0}_{1}.txt", encounterId, patientId));
            return FileManager.Instance.ListFiles(scratchFiles);
        }

        public string[] GetDiagnosisCodes(int? encounterId, int? patientId, bool isIcd10Mode)
        {
            string diagBillFileName = isIcd10Mode ? string.Format("{0}_{1}_{2}.txt", "DiagBill_Icd10", encounterId, patientId) : string.Format("{0}_{1}_{2}.txt", "DiagBill", encounterId, patientId);
            var diagBillFile = Path.Combine(LocalUserAppPath, diagBillFileName);
            if (FileManager.Instance.FileExists(diagBillFile))
            {
                string fileContent = FileManager.Instance.ReadContentsAsText(diagBillFile);
                string[] diagCodes = fileContent.Split(new[] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                return diagCodes.Where(d => d.Contains("-")).ToArray();
            }
            else
            {
                return null;
            }
        }

        public void SaveBillingDiagnosis(int? patientId, int? encounterId, bool isIcd10Mode, IEnumerable<DiagnosisViewModel> diagnoses)
        {
            string[] diagBillFileNames = new[] { string.Format("{0}_{1}_{2}.txt", "DiagBill", encounterId, patientId), string.Format("{0}_{1}_{2}.txt", "DiagBill_Icd10", encounterId, patientId) };
            string diagBillFilePath = string.Empty;
            foreach (var diagBillFileName in diagBillFileNames)
            {
                diagBillFilePath = Path.Combine(LocalUserAppPath, diagBillFileName);

                if (FileManager.Instance.FileExists(diagBillFilePath))
                {
                    FileManager.Instance.Delete(diagBillFilePath);
                }

                var fileContent = new StringBuilder("");
                FileManager.Instance.CommitContents(diagBillFilePath, fileContent.ToString());
            }

            string diagBillfileName = string.Format("DiagBill_{0}_{1}.txt", encounterId, patientId);
            diagBillFilePath = Path.Combine(LocalUserAppPath, diagBillfileName);
            if (isIcd10Mode)
            {
                //Saving Icd9 codes into DiagBill scratch file                 
                var fileContent = new StringBuilder("");
                foreach (DiagnosisViewModel diagnosis in diagnoses.Where(d => d.Icd9Code != null).ToList())
                {
                    string content = diagnosis.Icd9Code.Code + "-" + diagnosis.Code.Description;
                    fileContent.Append(content);
                    fileContent.Append(Environment.NewLine);
                }
                FileManager.Instance.CommitContents(diagBillFilePath, fileContent.ToString());

                //Saving Icd10 codes into DiagBill_ICD10 scratch file 
                diagBillfileName = string.Format("{0}_{1}_{2}.txt", "DiagBill_Icd10", encounterId, patientId);
                diagBillFilePath = Path.Combine(LocalUserAppPath, diagBillfileName);
                fileContent = new StringBuilder("");
                foreach (DiagnosisViewModel diagnosis in diagnoses)
                {
                    string content = diagnosis.Code.Code + "-" + diagnosis.Code.Description;
                    fileContent.Append(content);
                    fileContent.Append(Environment.NewLine);
                }
                FileManager.Instance.CommitContents(diagBillFilePath, fileContent.ToString());
            }
            else
            {
                var fileContent = new StringBuilder("");
                foreach (DiagnosisViewModel diagnosis in diagnoses)
                {
                    if (diagnosis.Code != null)
                    {
                        string content = diagnosis.Code.Code + "-" + diagnosis.Code.Description;
                        fileContent.Append(content);
                        fileContent.Append(Environment.NewLine);
                    }
                }
                FileManager.Instance.CommitContents(diagBillFilePath, fileContent.ToString());
            }
        }

        public void SaveBillingService(int? patientId, int? encounterId, bool isIcd10Mode, IEnumerable<ServiceViewModel> services)
        {

            //string modifiers = string.Empty;
            string[] procBillFileNames = new[] { string.Format("{0}_{1}_{2}.txt", "ProcBill", encounterId, patientId), string.Format("{0}_{1}_{2}.txt", "ProcBill_Icd10", encounterId, patientId) };
            procBillFileNames = isIcd10Mode ? procBillFileNames : new[] { procBillFileNames[0] };
            string procBillFilePath = string.Empty;
            //To Clear both the files before saving the new content
            foreach (var procBillFileName in procBillFileNames)
            {
                procBillFilePath = Path.Combine(LocalUserAppPath, procBillFileName);

                if (FileManager.Instance.FileExists(procBillFilePath))
                {
                    FileManager.Instance.Delete(procBillFilePath);
                }

                var fileContent = new StringBuilder("");
                FileManager.Instance.CommitContents(procBillFilePath, fileContent.ToString());
            }
            foreach (var procBillFileName in procBillFileNames)
            {
                procBillFilePath = Path.Combine(LocalUserAppPath, procBillFileName);
                var fileContent = new StringBuilder("");
                foreach (ServiceViewModel service in services)
                {
                    if (service.Code != null)
                    {
                        string modifiers = string.Empty;
                        ObservableCollection<DiagnosisViewModel> diagnosisLinks;
                        string content = string.Empty;
                        if (isIcd10Mode && procBillFileName.IndexOf("Icd10") < 0)
                        {
                            diagnosisLinks = service.DiagnosisLinks.Where(i => i.Icd9Code != null).ToObservableCollection();
                            content += diagnosisLinks.Count > 0 ? diagnosisLinks[0].Icd9Code.Code : "?";
                        }
                        else
                        {
                            diagnosisLinks = service.DiagnosisLinks;
                            content += diagnosisLinks.Count > 0 ? diagnosisLinks[0].Code.Code : "?";

                        }
                        content += "-F" + service.Code.Code + ":";
                        foreach (var modifier in service.Modifiers)
                        {
                            content += modifier.Abbreviation;
                            modifiers+= modifier.Abbreviation;
                        }
                        content += "/";

                        var count = 0;
                        foreach (var links in diagnosisLinks)
                        {
                            if (count >= 4)
                            {
                                break;
                            }
                            if (links.OrdinalId == 9)
                                content += "0";
                            else if (links.OrdinalId < 9)
                                content += links.OrdinalId + 1;

                            count = count + 1;
                        }
                        //content += "~" + ((int)service.Units).ToString();
                        fileContent.Append(content);
                        fileContent.Append(Environment.NewLine);

                        
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            //DataTable dtServiceCode = dbConnection.Execute<DataTable>("select * from [dbo].[AppointmentServiceUnit] where PatientId="+ patientId.ToString() + " and AppointmentId="+ encounterId.ToString() + " and Code='"+ service.Code.Code.ToString()+"'");

                            //if (dtServiceCode != null && dtServiceCode.Rows.Count > 0)
                            //{
                            Dictionary<string, object> paramDelete = new Dictionary<string, object>();
                            paramDelete.Add("@PatientId", patientId);
                            //paramDelete.Add("@unit", ((int)service.Units).ToString());
                            //paramDelete.Add("@AppointmentId", encounterId);
                            paramDelete.Add("@Code", service.Code.Code);
                                string qry = "Delete from [dbo].[AppointmentServiceUnit] where PatientId = @PatientId and Code = @Code";
                                dbConnection.Execute(qry, paramDelete);
                            //}
                            
                            Dictionary<string, object> paramInsert = new Dictionary<string, object>();
                                paramInsert.Add("@PatientId", patientId);
                                paramInsert.Add("@AppointmentId", encounterId);
                                paramInsert.Add("@Code", service.Code.Code);
                                paramInsert.Add("@Unit", ((int)service.Units).ToString());
                                paramInsert.Add("@ServiceModifiers", modifiers);
                                string qryServiceCode = " Insert into [dbo].[AppointmentServiceUnit](PatientId,AppointmentId,Code,Unit,ServiceModifiers) values (@PatientId,@AppointmentId,@Code,@Unit,@ServiceModifiers) ";
                                dbConnection.Execute(qryServiceCode, paramInsert);
                           
                        }
                    }
                }
                FileManager.Instance.CommitContents(procBillFilePath, fileContent.ToString());
            }
        }

        public string GetReCalculatedOfficeVisit(int? encounterId, int? patientId)
        {
            string fileContent = string.Empty;
            var doctorInterfacePath = Path.Combine(FileManager.Instance.Configuration.ApplicationDataPath, "DoctorInterface");
            string filename = string.Format("{0}_{1}_{2}.txt", "OfficeVisit", encounterId, patientId);
            var filePath = Path.Combine(doctorInterfacePath, filename);
            if (FileManager.Instance.FileExists(filePath))
            {
                fileContent = FileManager.Instance.ReadContentsAsText(filePath);
                if (!string.IsNullOrEmpty(fileContent)) fileContent = fileContent.Trim();
            }
            return fileContent;
        }

        public void ClearDiagBillContents(int? patientId, int? encounterId)
        {
            var doctorInterfacePath = Path.Combine(FileManager.Instance.Configuration.ApplicationDataPath, "DoctorInterface");
            var diagBillFileNames = new[] { string.Format("{0}_{1}_{2}.txt", "DiagBill", encounterId, patientId), string.Format("{0}_{1}_{2}.txt", "DiagBill_Icd10", encounterId, patientId) };
            foreach (var diagBillFileName in diagBillFileNames)
            {
                string diagBillFilePath = Path.Combine(doctorInterfacePath, diagBillFileName);

                if (FileManager.Instance.FileExists(diagBillFilePath))
                {
                    FileManager.Instance.CommitContents(diagBillFilePath, string.Empty);
                    FileManager.Instance.Delete(diagBillFilePath);
                }
            }
        }
    }
}
