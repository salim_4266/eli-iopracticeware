﻿using IO.Practiceware.Presentation.ViewModels.StandardExam;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.StandardExam
{
    [Singleton]
    public class StandardExamViewManager
    {
        private readonly IInteractionManager _interactionManager;
        public event EventHandler LoadPlanFormHandler;
        public event EventHandler LoadImpressionFormHandler;
        public event EventHandler LoadChartFormHandler;
        public event EventHandler LoadHighlightFormHandler;
        public event EventHandler<OfficeVisitEventArgs> GetOfficeVisitHandler;
        //public event EventHandler LoadSummaryFormHandler;
        public StandardExamReturnArguments returnArguments;
        public CodingCluesLoadArguments CodingCluesLoadArguments { get; set; }
        public StandardExamViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
            returnArguments = new StandardExamReturnArguments();
        }

        public void ShowStandardExam(StandardExamLoadArguments standardExamLoadArguments)
        {

            var view = new StandardExamView();
            var arguments = view.DataContext.EnsureType<StandardExamViewContext>();
            arguments.StandardExamLoadArguments = standardExamLoadArguments;
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip,
                Header = "Exam Billing View"
            });
        }

        public StandardExamReturnArguments GetCheckOutData()
        {
            return returnArguments;
        }


        public void LoadVbForm(string vbFormName)
        {
            returnArguments.FormName = vbFormName;
            if (vbFormName == "Plan")
            {
                if (LoadPlanFormHandler != null)
                {
                    LoadPlanFormHandler(this, null);
                }
            }
            else if (vbFormName == "Impressions")
            {
                if (LoadImpressionFormHandler != null)
                {
                    LoadImpressionFormHandler(this, null);
                }
            }
            else if (vbFormName == "Chart")
            {
                if (LoadChartFormHandler != null)
                {
                    LoadChartFormHandler(this, null);
                }
            }
            else if (vbFormName == "Highlights")
            {
                if (LoadHighlightFormHandler != null)
                {
                    LoadHighlightFormHandler(this, null);
                }
            }

            //else if (vbFormName == "Summary")
            //{
            //    if (LoadSummaryFormHandler != null)
            //    {
            //        LoadSummaryFormHandler(this, null);
            //    }
            //}
        }

        public void RequestOfficeVisitHandler(string selectedDiagnosis)
        {
            if (GetOfficeVisitHandler != null)
            {
                GetOfficeVisitHandler(this, new OfficeVisitEventArgs(selectedDiagnosis));
            }
        }

        public class OfficeVisitEventArgs : EventArgs
        {
            private readonly string _selectedDiagnosis;

            public OfficeVisitEventArgs(String selectedDiagnosis)
            {
                _selectedDiagnosis = selectedDiagnosis;
            }
            public String SelectedDiagnosis
            {
                get { return _selectedDiagnosis; }
            }
        }
    }
}
