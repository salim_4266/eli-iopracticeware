﻿using System.IO;
using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewModels.StandardExam;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.StandardExam;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Task = System.Threading.Tasks.Task;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using IO.Practiceware.Model.Legacy;

namespace IO.Practiceware.Presentation.Views.StandardExam
{
    /// <summary>
    /// Interaction logic for ExamView.xaml
    /// </summary>
    public partial class StandardExamView
    {
        public StandardExamView()
        {
            InitializeComponent();
            Visibility = Visibility.Visible;
        }        
        
    }

    public class StandardExamViewContextReference : DataContextReference
    {
        public new StandardExamViewContext DataContext
        {
            get { return (StandardExamViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
    public class Converter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isVisible = (bool)value;
            return isVisible ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [SupportsDataErrorInfo]
    public class StandardExamViewContext : IViewContext
    {

        private readonly IPatientInfoViewService _patientInfoViewService;
        private readonly IStandardExamViewService _standardExamViewService;
        private readonly IImageService _imageService;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly StandardExamViewManager _standardExamViewManager;
        private readonly Func<DiagnosisViewModel> _createDiagnosisViewModel;
        private readonly Func<ServiceViewModel> _createServiceViewModel;
        private readonly Func<ObservableCollection<NameAndAbbreviationViewModel>> _createNameAndAbbreviationViewModelCollection;
        private readonly Func<ObservableCollection<DiagnosisViewModel>> _createDiagnosisViewModelCollection;
        private readonly StandardExamFileManager _standardExamFileManager;
        private readonly IInteractionManager _interactionManager;
        private readonly ICloner _cloner;
        private bool _isToggled = false;
        private ObservableCollection<DiagnosisCodeViewModel> _icd9Diagnosis = null;
        private ObservableCollection<DiagnosisCodeViewModel> _icd10Diagnosis = null;
        public delegate void AsyncGetBillingServices();
        public delegate void AsyncGetInvoiceInfo();
        public delegate void AsyncGetModifierCodes();
        //public  ObservableCollection<CodingCluesLoadArguments> items;

        //public ObservableCollection<CodingCluesLoadArguments> Items
        //{
        //    get
        //    {
        //        return items;
        //    }
        //    set
        //    {
        //        items = value;
           
        //        // RaisePropertyChanged(ItemsPropertyName);
        //    }
        //}
        public string MDMValue1 { get; set; }
        public DateTime ExamDate { get; set; }



        
        public virtual string ErrorMsg { get; set; }
        public virtual int OfficeTimeVisit { get; set; }

        [NotifyPropertyChanged]
        public virtual bool EnmChangesVisible { get; set; }
        public virtual string MDMValue { get; set; } 

        

        public virtual DataListViewModel DataList { get; set; }
        public virtual PatientInfoViewModel PatientInfo { get; set; }

        [DispatcherThread]
        [Dependency]
        public virtual AppointmentInfoViewModel AppointmentInfo { get; set; }

        public virtual InvoiceInfoViewModel InvoiceInfo { get; set; }

        public virtual NamedViewModel SelectedInvoiceType { get; set; }
        public virtual ObservableCollection<PolicyViewModel> InsurancePolicy { get; set; }

        public virtual ObservableCollection<ServiceViewModel> Services { get; set; }

        public virtual ObservableCollection<ServiceViewModel> OfficeVisit { get; set; }

        [DispatcherThread, Dependency]
        public virtual ExtendedObservableCollection<DiagnosisViewModel> Diagnoses { get; set; }

        [DependsOn("Diagnoses", "Diagnoses.Item.IsQueuedToBill")]
        [DispatcherThread, Dependency]
        public virtual ExtendedObservableCollection<DiagnosisViewModel> SelectedDiagnoses
        {
            get
            {
                return Diagnoses.Where(i => i.IsQueuedToBill && i.Code != null).ToExtendedObservableCollection();
               
            }
        }

        [DependsOn("SelectedDiagnoses")]
        [DispatcherThread, Dependency]
        public virtual ExtendedObservableCollection<DiagnosisViewModel> LinkedDiagnoses
        {
            get { return SelectedDiagnoses.OrderBy(i => i.OrdinalId).Where(i => i.Id <= 10).ToExtendedObservableCollection(); }
        }

        [DispatcherThread]
        public virtual ObservableCollection<DiagnosisCodeViewModel> DiagnosisCodes { get; set; }

        [DispatcherThread]
        public virtual bool CodeSetEnabled { get; set; }

        public virtual ObservableCollection<ServiceCodeViewModel> ServiceCodes { get; set; }

        public virtual ObservableCollection<ServiceCodeViewModel> OfficeVisitCodes { get; set; }

        public virtual ObservableCollection<NameAndAbbreviationViewModel> ModifierCodes { get; set; }

        public virtual ObservableCollection<DiagnosisLinkViewModel> DiagnosisLinks { get; set; }

        public virtual StandardExamLoadArguments StandardExamLoadArguments { get; set; }

        public virtual CodingCluesLoadArguments CodingCluesLoadArguments { get; set; }

        [DispatcherThread]
        public virtual bool IsIcd10DiagnosisTypeSelected { get; set; }

        public StandardExamViewContext(
            Func<ServiceViewModel> createBillingServiceViewModel,
            Func<ObservableCollection<NameAndAbbreviationViewModel>> createNameAndAbbreviationViewModelCollection,
            Func<NameAndAbbreviationViewModel> createNameAndAbbreviationViewModel,
            Func<ObservableCollection<ServiceCodeViewModel>> createServiceCodeViewModelCollection,
            Func<ServiceCodeViewModel> createServiceCodeViewModel,
            Func<ObservableCollection<DiagnosisViewModel>> createDiagnosisViewModelCollection,
            Func<ObservableCollection<PolicyViewModel>> createPolicyViewModel,
            IPatientInfoViewService patientInfoViewService,
            IImageService imageService,
            Func<PatientInfoViewManager> createPatientInfoViewManager,
            IStandardExamViewService standardExamViewService,
            StandardExamViewManager standardExamViewManager,
            Func<DiagnosisViewModel> createDiagnosisViewModel,
            StandardExamFileManager standardExamFileManager,
            IInteractionManager interactionManager,
            ICloner cloner
            )
        {
            _patientInfoViewService = patientInfoViewService;
            _imageService = imageService;
            _patientInfoViewManager = createPatientInfoViewManager();
            _standardExamViewService = standardExamViewService;
            _standardExamViewManager = standardExamViewManager;
            _createDiagnosisViewModel = createDiagnosisViewModel;
            _createServiceViewModel = createBillingServiceViewModel;
            _createNameAndAbbreviationViewModelCollection = createNameAndAbbreviationViewModelCollection;
            _createDiagnosisViewModelCollection = createDiagnosisViewModelCollection;
            _standardExamFileManager = standardExamFileManager;
            _cloner = cloner;
            InsurancePolicy = createPolicyViewModel();
            ErrorMsg = "";
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            GoToPatientInfo = Command.Create<int>(ExecuteRedirectToPatientDemographics).Async(() => InteractionContext);
            FilterInsurers = Command.Create<string>(ExecuteFilterInsurer).Async(() => InteractionContext);
            LoadVbForm = Command.Create<string>(ExecuteLoadVbForm);
            DiagnosisTypeChange = Command.Create(ExecuteDiagnosisTypeChange).Async(() => InteractionContext);
            AddDiagnosis = Command.Create(ExecuteAddDiagnosis);
            RemoveDiagnosis = Command.Create<DiagnosisViewModel>(ExecuteRemoveDiagnosis);
            RemoveService = Command.Create<ServiceViewModel>(ExecuteRemoveService);
            RemoveLinkedDiagnosis = Command.Create<DiagnosisViewModel>(ExecuteRemoveLinkDiagnosis).Async(() => InteractionContext);
            AddService = Command.Create(ExecuteAddService);
            ReloadServiceData = Command.Create<ServiceViewModel>(ExecuteReloadServiceData).Async(() => InteractionContext);
            OfficeVisitSelectionChanged = Command.Create<ServiceCodeViewModel>(ExecuteOfficeVisitSelectionChaged).Async(() => InteractionContext);
            TimeSelectionChanged = Command.Create<ServiceCodeViewModel>(ExecuteTimeVisitSelectionChaged).Async(() => InteractionContext);
            MdmSelectionChanged = Command.Create<ServiceCodeViewModel>(ExecuteMdmVisitSelectionChaged).Async(() => InteractionContext);

            
            GetOfficeVisit = Command.Create(ExecuteGetOfficeVisit).Async(() => InteractionContext);
            _standardExamViewManager.returnArguments.FormName = "Plan";
            _interactionManager = interactionManager;
        }

        internal readonly object GetOfficeVisitLock = new object();

        private void InvoiceInfoLoaded(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            AsyncGetInvoiceInfo caller = (AsyncGetInvoiceInfo)result.AsyncDelegate;

            caller.EndInvoke(ar);
            ExecuteFilterInsurer();
            AsyncGetModifierCodes objModifierCodes = new AsyncGetModifierCodes(LoadModifiersAsync);
            IAsyncResult tag = objModifierCodes.BeginInvoke(new AsyncCallback(ModifierLoaded), null);
        }
        private void ModifierLoaded(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            AsyncGetModifierCodes caller = (AsyncGetModifierCodes)result.AsyncDelegate;
            caller.EndInvoke(ar);

            AsyncGetBillingServices objBillingServices = new AsyncGetBillingServices(LoadBillingServicesAsync);
            IAsyncResult tag = objBillingServices.BeginInvoke(null, null);
        }


        private void ExecuteGetOfficeVisit()
        {            
            string selectedDiagnoses = string.Empty;
            ErrorMsg = "";

            if (Diagnoses != null && Diagnoses.Count > 0)
            {
                var selectedBillingDiagnosis = SelectedDiagnoses.OrderBy(d => d.OrdinalId).ToList();
                if (IsIcd10DiagnosisTypeSelected)
                {
                    foreach (DiagnosisViewModel diagnosis in selectedBillingDiagnosis)
                    {
                        selectedDiagnoses += string.Format("{0}-{1};", diagnosis.Code.Code, diagnosis.Code.Description);
                    }
                }
                else
                {
                    foreach (DiagnosisViewModel diagnosis in selectedBillingDiagnosis.Where(d => d.Code != null).ToList())
                    {
                        selectedDiagnoses += string.Format("{0}-{1};", diagnosis.Code.Code, diagnosis.Code.Description);
                    }
                }
            }



            Task loadOfficeVisit = Task.Factory.StartNewWithCurrentTransaction(() =>
                                                                                {
                                                                                    lock (GetOfficeVisitLock)
                                                                                    {
                                                                                        _standardExamViewManager.RequestOfficeVisitHandler(selectedDiagnoses);
                                                                                    }
                                                                                });
            loadOfficeVisit.Wait();

            LoadOfficeVisitFromFile();
            

            if (ExamDate > new DateTime(2020, 12, 31))
            {               
                //int visitCode = GetServiceCodebyTiming(IsFirstVisit, OfficeTimeVisit);
                int officevistTimecode = GetServiceCodebyTiming(IsFirstVisit, Math.Abs(OfficeTimeVisit));
                int officevistMdmcode = 0;
                
                if (!MDMValue.IsNullOrEmpty())
                {                    
                    officevistMdmcode = GetServiceCodebyMdm(IsFirstVisit, MDMValue);
                }

                int visitCode = officevistMdmcode > officevistTimecode ? officevistMdmcode : officevistTimecode;

                var dat = AppointmentInfo.ScheduleDate;
                OfficeVisit = _standardExamViewService.GetOfficeVisit(visitCode.ToString());
            }
            if (OfficeVisit != null) NewEncounterSerivce = OfficeVisit.Where(i => i.Code != null).Select(i => i.Code).FirstOrDefault();

            CodingCluesLoadArguments = _standardExamViewManager.CodingCluesLoadArguments;
                        
            if (MDMValue.IsNullOrEmpty())
            {
                MDMValue = CodingCluesLoadArguments.MedicalDecisionMaking.ToString()=="Any"? "Straight Forward" : CodingCluesLoadArguments.MedicalDecisionMaking.ToString();
                //MessageBox.Show("Hi  mmmm");
            }
           

            CodingCluesLoadArguments.MedicalDecisionMaking = MDMValue == "Straight Forward" ? "Any" : MDMValue; 

        }

        
        private void LoadOfficeVisitFromFile()
        {

            string officevistcode = _standardExamFileManager.GetReCalculatedOfficeVisit(StandardExamLoadArguments.EncounterId, StandardExamLoadArguments.PatientId);

            OfficeVisit = _standardExamViewService.GetOfficeVisit(officevistcode);

            if (!String.IsNullOrEmpty(officevistcode))
            {
                EmEyeSelector = officevistcode.Length > 2 && officevistcode.Substring(0, 3) == "920";

                IsFirstVisit = officevistcode.Length > 3 && officevistcode.Substring(3, 1) == "0";

                var severityLevel = "1";
                if (officevistcode.Length > 4) severityLevel = officevistcode.Substring(4, 1);

                if (!string.IsNullOrEmpty(severityLevel) && EmEyeSelector)
                {
                    EyeLevel = severityLevel != "4";
                }

                if (!string.IsNullOrEmpty(severityLevel) && !EmEyeSelector)
                {
                    EmLevel1 = severityLevel == "1";
                    EmLevel2 = severityLevel == "2";
                    EmLevel3 = severityLevel == "3";
                    EmLevel4 = severityLevel == "4";
                    EmLevel5 = severityLevel == "5";

                    if (severityLevel != "1" && severityLevel != "2" && severityLevel != "3" && severityLevel != "4" && severityLevel != "5")
                    {
                        EmLevel1 = true;
                    }
                }
            }
        }

        public string[] Alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
        public string[] Colors = { "#FF00CD7F", "#FF00CDCD", "#FF00AFFF", "#FF6478FF", "#FFBB78FF", "#FFF57AE8", "#FFDE6498", "#FFFA7776", "#FFEA7460", "#FFFDA46C", "#FFE5B844", "#FFC0D674" };


        #region Commands
        public virtual ICommand Load { get; set; }
        public virtual ICommand LoadDiagnosis { get; set; }
        public virtual ICommand GoToPatientInfo { get; set; }
        public virtual ICommand FilterInsurers { get; set; }
        public virtual ICommand LoadVbForm { get; set; }
        public virtual ICommand DiagnosisTypeChange { get; set; }
        public virtual ICommand ReloadServiceData { get; set; }
        public virtual ICommand AddDiagnosis { get; set; }
        public virtual ICommand AddService { get; set; }
        public virtual ICommand RemoveDiagnosis { get; set; }
        public virtual ICommand RemoveService { get; set; }
        public virtual ICommand RemoveLinkedDiagnosis { get; set; }

        public virtual ICommand GetOfficeVisit { get; set; }
        #endregion

        private void ExecuteLoad()
        {
            //EnmChangesVisible = "Hidden";
            ErrorMsg = "";
            CodeSetEnabled = false;
            Task loadPatientInfoTask = null;
            IsIcd10DiagnosisTypeSelected = _standardExamViewService.IsDefaultDiagnosisTypeIcd10(StandardExamLoadArguments.PatientId.Value);

            System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(LoadDiagnoses);

            Task readDiagBillFile = Task.Factory.StartNewWithCurrentTransaction(() => { LoadBillingDiagnoses(); });

            if (StandardExamLoadArguments.EncounterId != null)
            {
                loadPatientInfoTask = Task.Factory.StartNewWithCurrentTransaction(() => ExecuteLoadInformation(StandardExamLoadArguments.PatientId.Value, StandardExamLoadArguments.EncounterId.Value));
            }

            Task loadBillingServiceCodes = Task.Factory.StartNewWithCurrentTransaction(() => { ServiceCodes = _standardExamViewService.LoadAllBillingServices(); });

            Task loadOfficeVisit = Task.Factory.StartNewWithCurrentTransaction(LoadOfficeVisitInfo);

            Task loadServiceModifiers = Task.Factory.StartNewWithCurrentTransaction(() => { ModifierCodes = _standardExamViewService.LoadModifierCodes(); });

            if (readDiagBillFile != null) readDiagBillFile.Wait();
        }

        private void LoadBillingDiagnoses()
            {
                string[] diagCodes = _standardExamFileManager.GetDiagnosisCodes(StandardExamLoadArguments.EncounterId, StandardExamLoadArguments.PatientId, IsIcd10DiagnosisTypeSelected);
                Dictionary<string, string> watermark = new Dictionary<string, string>();

                Diagnoses = _standardExamViewService.LoadBillingDiagnosis(StandardExamLoadArguments.PatientId, StandardExamLoadArguments.EncounterId, IsIcd10DiagnosisTypeSelected, StandardExamLoadArguments.DiagnosesLoadArguments, diagCodes, _isToggled, out watermark).ToExtendedObservableCollection();

                ResetDiagnosis(SelectedDiagnoses.OrderBy(i => i.OrdinalId).ToExtendedObservableCollection(), null, null);

                var fileNames = _standardExamFileManager.GetTestFiles(StandardExamLoadArguments.EncounterId, StandardExamLoadArguments.PatientId);
                Services = _standardExamViewService.GetBillingService(IsIcd10DiagnosisTypeSelected, LinkedDiagnoses, fileNames);
        }


        private void LoadBillingServicesAsync()
        {
            ServiceCodes = _standardExamViewService.LoadAllBillingServices();
        }

        private void LoadModifiersAsync()
        {
            ModifierCodes = _standardExamViewService.LoadModifierCodes();
        }

        private void LoadInvoiceInfoAsync()
        {
            InvoiceInfo = _standardExamViewService.LoadInvoiceTypes();
            SelectedInvoiceType = InvoiceInfo.InvoiceType.FirstOrDefault(i => i.Name == "Professional");
        }

        private void ExecuteDiagnosisTypeChange()
        {
            CodeSetEnabled = false;
            _isToggled = !_isToggled;
            Task.Factory.StartNewWithCurrentTransaction(LoadDiagnoses);
            string[] diagCodes = _standardExamFileManager.GetDiagnosisCodes(StandardExamLoadArguments.EncounterId, StandardExamLoadArguments.PatientId, IsIcd10DiagnosisTypeSelected);

            Dictionary<string, string> watermark = new Dictionary<string, string>();
            Diagnoses = _standardExamViewService.LoadBillingDiagnosis(StandardExamLoadArguments.PatientId, StandardExamLoadArguments.EncounterId, IsIcd10DiagnosisTypeSelected, StandardExamLoadArguments.DiagnosesLoadArguments, diagCodes, _isToggled, out watermark).ToExtendedObservableCollection();
            if (_icd9Diagnosis != null && _icd10Diagnosis != null)
            {
                Diagnoses.ForEach(diagnosis =>
                    {
                        if (diagnosis.DiagnosisCodeWaterMark == null && diagnosis.DiagnosisCodes == null)
                        {
                            diagnosis.DiagnosisCodes = DiagnosisCodes;
                        }
                    });
            }

            ResetDiagnosis(SelectedDiagnoses.OrderBy(i => i.OrdinalId).ToExtendedObservableCollection(), null, null);
            Services.ForEach(i =>
            {
                if (i.DiagnosisLinks.Count > 0)
                {
                    i.DiagnosisLinks = _createDiagnosisViewModelCollection();
                }
            });
        }
        private void LoadDiagnoses()
        {
            const int loadingModelId = -100;
            var loadingDiagnoses = new ObservableCollection<DiagnosisCodeViewModel> 
                { 
                    new DiagnosisCodeViewModel
                    {
                        Id = loadingModelId,
                        Code = "Please wait",
                        Description = "Loading..."
                    }
                };

            DiagnosisCodes = loadingDiagnoses;
            if (!IsIcd10DiagnosisTypeSelected)
            {
                if (_icd9Diagnosis == null)
                {
                    _icd9Diagnosis = _standardExamViewService.LoadIcdCodes(false);
                }

                DiagnosisCodes = _icd9Diagnosis;
            }
            else
            {
                if (_icd10Diagnosis == null)
                {
                    _icd10Diagnosis = _standardExamViewService.LoadIcdCodes(true);
                }

                DiagnosisCodes = _icd10Diagnosis;
            }

            // In case "Loading..." was accidentally selected as value
            //foreach (var diagnosis in Diagnoses
            //    .Where(d => d.Code.IfNotNull(c => c.Id == loadingModelId) || d.DiagnosisCodes.Any(i => i.Id == loadingModelId)))
            //{
            //    diagnosis.Code = null;
            //}


            // In case "Loading..." was accidentally selected as value
            Diagnoses.ForEach(diagnosis =>
            {

                if (diagnosis.Code != null)
                {
                    if (diagnosis.Code.IfNotNull(c => c.Id == loadingModelId) || diagnosis.DiagnosisCodes.Any(i => i.Id == loadingModelId))
                    {
                        diagnosis.Code = null;
                    }

                    if ((diagnosis.DiagnosisCodeWaterMark == null && diagnosis.DiagnosisCodes.Count > 1) || diagnosis.Code == null)
                    {
                        diagnosis.DiagnosisCodes = DiagnosisCodes;
                    }
                }
                else if (diagnosis.DiagnosisCodeWaterMark == null && diagnosis.DiagnosisCodes == null)
                {
                    diagnosis.DiagnosisCodes = DiagnosisCodes;
                }
                else if (diagnosis.Code == null && diagnosis.DiagnosisCodes.Count == 1 && diagnosis.DiagnosisCodes.First().Code == "Please wait")
                {
                    diagnosis.DiagnosisCodes = DiagnosisCodes;
                }

            });

            CodeSetEnabled = true;
        }

        private void ExecuteAddDiagnosis()
        {
            var diagnosis = _createDiagnosisViewModel();
            diagnosis.Id = Diagnoses.Count + 1;
            diagnosis.IsQueuedToBill = false;
            diagnosis.OrdinalId = Diagnoses.Count;
            diagnosis.DiagnosisCodes = DiagnosisCodes;
            Diagnoses.Add(diagnosis);
        }

        private void ExecuteRemoveDiagnosis(DiagnosisViewModel diagnosis)
        {
            Diagnoses.Remove(diagnosis);
            Diagnoses = Diagnoses.ToExtendedObservableCollection();
            //ResetLinkingOnRemove(diagnosis, SelectedDiagnoses);
        }

        private void ResetLinkingOnRemove(DiagnosisViewModel diagnosis)
        {
            Services.ForEach(x =>
            {
                if (x.DiagnosisLinks != null)
                {
                    var diagnosisResult = x.DiagnosisLinks.FirstOrDefault(dia => dia.Id == diagnosis.Id);
                    x.DiagnosisLinks.Remove(diagnosisResult);
                }

                var defaultLinked = x.IfNotNull(j => j.DiagnosisLinks.Where(i => i.IsLinked && i.OrdinalId > diagnosis.OrdinalId));

                defaultLinked.ForEach(y =>
                {
                    y.Id = y.Id - 1;
                    y.OrdinalId = y.OrdinalId - 1;
                    y.PointerLetter = Alpha[y.OrdinalId];
                    y.Color = Colors[y.OrdinalId];
                });

            });

            if (OfficeVisit != null)
            {
                OfficeVisit.ForEach(x =>
                {
                    if (x.DiagnosisLinks != null)
                    {
                        var diagnosisResult = x.DiagnosisLinks.FirstOrDefault(dia => dia.Id == diagnosis.Id);
                        x.DiagnosisLinks.Remove(diagnosisResult);
                    }
                });
            }
            var selecteddiagnosis = Diagnoses.FirstOrDefault(i => i.Code == diagnosis.Code && i.OrdinalId == diagnosis.OrdinalId);
            if (selecteddiagnosis != null)
            {
                selecteddiagnosis.OrdinalId = 0;
                selecteddiagnosis.PointerLetter = null;
                selecteddiagnosis.Color = null;
            }

        }

        private void ExecuteRemoveLinkDiagnosis(DiagnosisViewModel diagnosis)
        {
            if (SelectedDiagnoses != null && diagnosis != null)
            {
                var selecteddiagnosis = Diagnoses.FirstOrDefault(i => i.Code == diagnosis.Code && i.OrdinalId == diagnosis.OrdinalId);
                if (diagnosis.Code != null)
                {
                    // ResetDiagnosis(SelectedDiagnoses, diagnosis, true);
                    if (SelectedDiagnoses.Count > 12)
                    {
                        InteractionManager.Current.Alert("Maximum number of billing diagnosis codes are already added i.e 12.");
                        if (selecteddiagnosis != null) selecteddiagnosis.IsQueuedToBill = false;
                        return;
                    }
                    if (SelectedDiagnoses.Count(i => i.Code != null && i.Code.Code == diagnosis.Code.Code) > 1)
                    {
                        InteractionManager.Current.Alert("This diagnosis is already selected.");
                        if (selecteddiagnosis != null) selecteddiagnosis.IsQueuedToBill = false;
                        return;
                    }

                    if (!diagnosis.IsQueuedToBill)
                    {
                        ResetLinkingOnRemove(diagnosis);
                        ResetDiagnosis(SelectedDiagnoses, diagnosis, true);
                    }
                    //Code to link the diagnosis to billing service on selecting the diagnosis
                    else
                    {
                        string code = string.Empty;
                        ResetDiagnosis(SelectedDiagnoses, diagnosis, false);
                        if (LinkedDiagnoses.Any(i => i.Code.Code == diagnosis.Code.Code))
                        {
                            if (IsIcd10DiagnosisTypeSelected)
                            {
                                if (diagnosis.Icd9Code != null && diagnosis.Icd9Code.Code != string.Empty)
                                {
                                    code = diagnosis.Icd9Code.Code;
                                }
                            }
                            else
                            {
                                code = diagnosis.Code.Code;
                            }

                            if (code.IsNotNullOrEmpty())
                            {
                                int r = code.IndexOf('.');
                                if (r > 0)
                                {
                                    int q = code.IndexOf('.', r + 1);
                                    if (q > 0)
                                    {
                                        code = code.Substring(0, q);
                                    }
                                }
                                Services.ForEach(x =>
                                {
                                    if (x.Code != null)
                                    {
                                        var isLinked = _standardExamViewService.IsDiagnosisLinked(x.Code.Code, code);
                                        if (isLinked)
                                        {
                                            diagnosis.IsLinked = true;
                                            if (!(x.DiagnosisLinks.Any(j => j.Code.Code == diagnosis.Code.Code)))
                                            {
                                                x.DiagnosisLinks.Add(((DiagnosisViewModel)_cloner.Clone(diagnosis, diagnosis.GetType(), diagnosis.GetType())));
                                            }
                                        }
                                    }

                                });
                            }
                        }
                    }
                }
                else
                {
                    if (selecteddiagnosis != null) selecteddiagnosis.IsQueuedToBill = false;
                }
            }
        }

        private void ResetDiagnosis(ExtendedObservableCollection<DiagnosisViewModel> diagnoses, DiagnosisViewModel diagnosisModel, bool? isRemoved)
        {
            if (!isRemoved.HasValue)
            {
                for (var i = 0; i < diagnoses.Count; i++)
                {
                    var diagnosis = diagnoses[i];
                    diagnosis.Id = i + 1;
                    diagnosis.OrdinalId = i;
                    diagnosis.PointerLetter = Alpha[i];
                    diagnosis.Color = Colors[i];
                }
            }
            else if (!isRemoved.Value)
            {
                var selectedDiagnosis = diagnoses.FirstOrDefault(i => i.Code == diagnosisModel.Code && i.OrdinalId == diagnosisModel.OrdinalId);
                if (selectedDiagnosis != null)
                {
                    selectedDiagnosis.Id = SelectedDiagnoses.Count;
                    selectedDiagnosis.OrdinalId = SelectedDiagnoses.Count - 1;
                    selectedDiagnosis.PointerLetter = Alpha[SelectedDiagnoses.Count - 1];
                    selectedDiagnosis.Color = Colors[SelectedDiagnoses.Count - 1];
                }
            }
            else
            {
                var selectedDiagnosis = diagnoses.Where(i => i.Id > diagnosisModel.Id).ToObservableCollection();
                for (var i = 0; i < selectedDiagnosis.Count; i++)
                {
                    var diagnosis = selectedDiagnosis[i];
                    diagnosis.Id = diagnosis.Id - 1;
                    diagnosis.OrdinalId = diagnosis.OrdinalId - 1;
                    diagnosis.PointerLetter = Alpha[diagnosis.OrdinalId];
                    diagnosis.Color = Colors[diagnosis.OrdinalId];
                }
            }
        }

        private void ExecuteRemoveService(ServiceViewModel service)
        {
            Services.Remove(service);
            Services = Services.ToObservableCollection();
        }


        private void ExecuteLoadInformation(int patientId, int encounterId)
        {
            DataList = _standardExamViewService.LoadInformation(patientId, encounterId);

            PatientInfo = DataList.PatientInfo;
            AppointmentInfo = DataList.AppointmentInfo;
            ExamDate = AppointmentInfo.ScheduleDate.Date;

            if (ExamDate > new DateTime(2020,12,31))
            {                
                EnmChangesVisible = true;
            }
            else
            {               
                EnmChangesVisible = false;                
            }

            IsIcd10DiagnosisTypeSelected = DataList.DefaultDiagnosisType;
            PatientInfo.Photo = _imageService.GetPatientPhoto(patientId);
            InvoiceInfo = DataList.InvoiceInfo;
            ExecuteFilterInsurer();

            AppointmentTypes = new ObservableCollection<AppointmentTypeViewModel> { AppointmentInfo.AppointmentType };
            SelectedAppointmentType = AppointmentTypes.FirstOrDefault();
            SelectedInvoiceType = InvoiceInfo.InvoiceType.FirstOrDefault(i => i.Name == "Professional");
        }

        private void ExecuteRedirectToPatientDemographics(int patientId)
        {
            this.Dispatcher().BeginInvoke(() => _patientInfoViewManager
                .ShowPatientInfo(new PatientInfoLoadArguments
                {
                    PatientId = patientId,
                    TabToOpen = PatientInfoTab.Demographics
                }));
        }

        private void ExecuteFilterInsurer(string invoiceType = "Professional")
        {
            if (PatientInfo != null && PatientInfo.Insurances != null && invoiceType != null)               
                InsurancePolicy = PatientInfo.Insurances.Where(x => x.InsurerClaimForm != null && x.InsurerClaimForm.Any(y => y.InvoiceType.Name == invoiceType)).Select(x => x).ToObservableCollection();
        }

        private void ExecuteAddService()
        {
            var service = _createServiceViewModel();
            service.Id = Services.Count + 1;
            service.IsQueuedToBill = true;
            service.IsOfficeVisit = false;
            service.ServiceCodes = ServiceCodes;
            service.Modifiers = _createNameAndAbbreviationViewModelCollection();
            service.DiagnosisLinks = _createDiagnosisViewModelCollection();
            service.AllowableAmount = (Decimal)0.00;
            service.Units = 1;
            service.UnitCharge = 0;
            Services.Add(service);
        }

        public void ExecuteLoadVbForm(string formName)
        {
            if (formName != "Highlights")
            {
                InteractionContext.Complete(true);
            }

            if (formName == "Summary")
            {
                var selectedBillingDiagnosis = Diagnoses.Where(d => d.IsQueuedToBill).OrderBy(d => d.OrdinalId).ToList();
                bool? result = true;
                bool? officeVisitResult = true;
                if (!selectedBillingDiagnosis.Any())
                {
                    result = _interactionManager.Confirm("No Diagnosis Selected at CheckOut.", "Cancel", "Continue");
                }
                if (result.GetValueOrDefault() && (NewEncounterSerivce == null || NewEncounterSerivce.Code == null))
                {
                    officeVisitResult = _interactionManager.Confirm("No Office Visit Selected at CheckOut.", "Cancel", "Continue");
                }
                if (result.GetValueOrDefault() && officeVisitResult.GetValueOrDefault())
                {
                    SaveBillingDiagnosisCodes();
                    SaveBillingServiceCodes();
                    SaveOfficeVisitInfo();
                }
                else
                {
                    formName = "Chart";
                }
            }

            if (formName == "Close")
            {
                formName = "Plan";
            }

            _standardExamViewManager.returnArguments.IsIcd10 = IsIcd10DiagnosisTypeSelected;
            _standardExamViewManager.LoadVbForm(formName);
        }

        private void SaveOfficeVisitInfo()
        {
            if (OfficeVisit != null)
            {
                var selectedOfficeVisit = OfficeVisit.FirstOrDefault(i => i.IsQueuedToBill) ?? _createServiceViewModel();

                selectedOfficeVisit.Code = NewEncounterSerivce;
                if (StandardExamLoadArguments.EncounterId != null && StandardExamLoadArguments.PatientId != null)
                {
                    _standardExamFileManager.SaveOfficeVisitInfo(selectedOfficeVisit, StandardExamLoadArguments.EncounterId.Value, StandardExamLoadArguments.PatientId.Value, "F", IsIcd10DiagnosisTypeSelected, Diagnoses);
                }
            }//
        }

        public void ExecuteOfficeVisitSelectionChaged(ServiceCodeViewModel selectedofficevisit)
        {
            if (NewEncounterSerivce == null || NewEncounterSerivce.Code == null|| selectedofficevisit.Code=="99201")
            {
                return;
            }
            if (OfficeVisit != null)
            {
                var selectedDiagnosis = OfficeVisit.FirstOrDefault();
                if (selectedDiagnosis != null)
                {
                    selectedDiagnosis.Modifiers.Clear();
                    selectedDiagnosis.DiagnosisLinks.Clear();
                }
            }
            if (!string.IsNullOrEmpty(NewEncounterSerivce.Code))
            {
                EmEyeSelector = NewEncounterSerivce.Code.Length > 2 && NewEncounterSerivce.Code.Substring(0, 3) == "920";

                IsFirstVisit = NewEncounterSerivce.Code.Length > 3 && NewEncounterSerivce.Code.Substring(3, 1) == "0";

                var severityLevel = "1";
                if (NewEncounterSerivce.Code.Length > 4) severityLevel = NewEncounterSerivce.Code.Substring(4, 1);

                if (!string.IsNullOrEmpty(severityLevel) && EmEyeSelector)
                {
                    EyeLevel = severityLevel != "4";
                }

                if (!string.IsNullOrEmpty(severityLevel) && !EmEyeSelector)
                {
                    EmLevel1 = severityLevel == "1";
                    EmLevel2 = severityLevel == "2";
                    EmLevel3 = severityLevel == "3";
                    EmLevel4 = severityLevel == "4";
                    EmLevel5 = severityLevel == "5";

                    if (severityLevel != "1" && severityLevel != "2" && severityLevel != "3" && severityLevel != "4" && severityLevel != "5")
                    {
                        EmLevel1 = true;
                    }
                }
            }
        }
        public int  GetServiceCodebyMdm(bool IsNewPatient, string  mdm)
        {
            int officevistcode=0;

            if (IsNewPatient)
            {
                if (mdm.Contains("Low"))
                {
                    officevistcode = 99203;
                }
                else if (mdm.Contains("Moderate"))
                {
                    officevistcode = 99204;
                }
                else if (mdm.Contains("High"))
                {
                    officevistcode = 99205;
                }
                else if (mdm.Contains("Straight")|| mdm.Contains("Any"))
                {
                    officevistcode = 99202;
                }

            }
            else
            {
                if (mdm.Contains("Low"))
                {
                    officevistcode = 99213;
                }
                else if (mdm.Contains("Moderate"))
                {
                    officevistcode = 99214;
                }
                else if (mdm.Contains("High"))
                {
                    officevistcode = 99215;
                }
                else if (mdm.Contains("Straight") || mdm.Contains("Any"))
                {
                    officevistcode = 99212;
                }

            }

            return officevistcode;


        }
        public int GetServiceCodebyTiming(bool IsNewPatient, int timesote)
        {
           
            ErrorMsg = "";
            int officevistcode = 0;
            if (timesote >= 0)
            { 
                if (IsNewPatient)
                { 
                    // if(timesote==0)
                    //{ 
                    //    // nothing
                    //}
                    //else
                    if (timesote >= 0 && timesote <= 29)
                    {
                        officevistcode = 99202;
                    }
                    else if (timesote >= 30 && timesote <= 44)
                    {
                        officevistcode = 99203;
                    }
                    else if (timesote >= 45 && timesote <= 59)
                    {
                        officevistcode = 99204;
                    }
                    else if (timesote >= 60 )
                    {
                        officevistcode = 99205;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Minutes", "Visit Time", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    if (timesote == 0)
                    {
                        officevistcode = 99211;
                    }
                    else if (timesote >= 10 && timesote <= 19)
                    {
                        officevistcode = 99212;
                    }
                    else if (timesote >= 20 && timesote <= 29)
                    {
                        officevistcode = 99213;
                    }
                    else if (timesote >= 30 && timesote <= 39)
                    {
                        officevistcode = 99214;
                    }
                    else if (timesote >= 40 )
                    {
                        officevistcode = 99215;
                    }
                    else
                    {
                        ErrorMsg = "Please enter valid minutes";
                    }
                }
            }
            else
            {
                ErrorMsg = "Please enter valid minutes";
            }

            return officevistcode;
        }

        public ICommand TimeSelectionChanged { get; protected set; }
        public void ExecuteTimeVisitSelectionChaged(ServiceCodeViewModel selectedofficevisit)
        {
            ErrorMsg = "";
            
            if (CodingCluesLoadArguments != null)
            {                
                int officevistTimecode = GetServiceCodebyTiming(IsFirstVisit, Math.Abs(OfficeTimeVisit));
                int officevistMdmcode = GetServiceCodebyMdm(IsFirstVisit, MDMValue);


                int officevistcode = officevistMdmcode > officevistTimecode ? officevistMdmcode : officevistTimecode;

                OfficeVisit = _standardExamViewService.GetOfficeVisit(officevistcode.ToString());

                if (OfficeVisit != null) NewEncounterSerivce = OfficeVisit.Where(i => i.Code != null).Select(i => i.Code).FirstOrDefault();


                if (NewEncounterSerivce == null || NewEncounterSerivce.Code == null)
                {
                    return;
                }
                if (OfficeVisit != null)
                {
                    var selectedDiagnosis = OfficeVisit.FirstOrDefault();
                    if (selectedDiagnosis != null)
                    {
                        selectedDiagnosis.Modifiers.Clear();
                        selectedDiagnosis.DiagnosisLinks.Clear();
                    }
                }
                if (!string.IsNullOrEmpty(NewEncounterSerivce.Code))
                {
                    EmEyeSelector = NewEncounterSerivce.Code.Length > 2 && NewEncounterSerivce.Code.Substring(0, 3) == "920";

                    IsFirstVisit = NewEncounterSerivce.Code.Length > 3 && NewEncounterSerivce.Code.Substring(3, 1) == "0";

                    var severityLevel = "1";
                    if (NewEncounterSerivce.Code.Length > 4) severityLevel = NewEncounterSerivce.Code.Substring(4, 1);

                    if (!string.IsNullOrEmpty(severityLevel) && EmEyeSelector)
                    {
                        EyeLevel = severityLevel != "4";
                    }

                    if (!string.IsNullOrEmpty(severityLevel) && !EmEyeSelector)
                    {
                        EmLevel1 = severityLevel == "1";
                        EmLevel2 = severityLevel == "2";
                        EmLevel3 = severityLevel == "3";
                        EmLevel4 = severityLevel == "4";
                        EmLevel5 = severityLevel == "5";

                        if (severityLevel != "1" && severityLevel != "2" && severityLevel != "3" && severityLevel != "4" && severityLevel != "5")
                        {
                            EmLevel1 = true;
                        }
                    }
                }
            }        
            else
            {
                OfficeTimeVisit = 0;
                ErrorMsg = "Please Click on CalculateOfficeVisit button first";                

            }
}

        public ICommand MdmSelectionChanged { get; protected set; }

        public void ExecuteMdmVisitSelectionChaged(ServiceCodeViewModel selectedofficevisit)
        {
            ErrorMsg = "";
            if (CodingCluesLoadArguments != null)
            {
                
                if (MDMValue.IsNullOrEmpty())
                {
                    MDMValue = CodingCluesLoadArguments.MedicalDecisionMaking.ToString() == "Any" ? "Straight Forward" : CodingCluesLoadArguments.MedicalDecisionMaking.ToString();

                }
                //CodingCluesLoadArguments.MedicalDecisionMaking = MDMValue;
                CodingCluesLoadArguments.MedicalDecisionMaking = MDMValue == "Straight Forward" ? "Any" : MDMValue;
                
                int officevistcode = GetServiceCodebyMdm(IsFirstVisit, MDMValue);

                
                OfficeVisit = _standardExamViewService.GetOfficeVisit(officevistcode.ToString());

                if (OfficeVisit != null) NewEncounterSerivce = OfficeVisit.Where(i => i.Code != null).Select(i => i.Code).FirstOrDefault();


                if (NewEncounterSerivce == null || NewEncounterSerivce.Code == null)
                {
                    return;
                }
                if (OfficeVisit != null)
                {
                    var selectedDiagnosis = OfficeVisit.FirstOrDefault();
                    if (selectedDiagnosis != null)
                    {
                        selectedDiagnosis.Modifiers.Clear();
                        selectedDiagnosis.DiagnosisLinks.Clear();
                    }
                }
                if (!string.IsNullOrEmpty(NewEncounterSerivce.Code))
                {
                    EmEyeSelector = NewEncounterSerivce.Code.Length > 2 && NewEncounterSerivce.Code.Substring(0, 3) == "920";

                    IsFirstVisit = NewEncounterSerivce.Code.Length > 3 && NewEncounterSerivce.Code.Substring(3, 1) == "0";

                    var severityLevel = "1";
                    if (NewEncounterSerivce.Code.Length > 4) severityLevel = NewEncounterSerivce.Code.Substring(4, 1);

                    if (!string.IsNullOrEmpty(severityLevel) && EmEyeSelector)
                    {
                        EyeLevel = severityLevel != "4";
                    }

                    if (!string.IsNullOrEmpty(severityLevel) && !EmEyeSelector)
                    {
                        EmLevel1 = severityLevel == "1";
                        EmLevel2 = severityLevel == "2";
                        EmLevel3 = severityLevel == "3";
                        EmLevel4 = severityLevel == "4";
                        EmLevel5 = severityLevel == "5";

                        if (severityLevel != "1" && severityLevel != "2" && severityLevel != "3" && severityLevel != "4" && severityLevel != "5")
                        {
                            EmLevel1 = true;
                        }
                    }
                }
            }
            else
            {
                MDMValue = string.Empty;
                ErrorMsg = "Please Click on CalculateOfficeVisit button first";
               

            }
        }
        private void LoadOfficeVisitInfo()
        {
            OfficeVisitCodes = _standardExamViewService.LoadOfficeVisits();
            
            var index = OfficeVisitCodes.IndexOf(x => x.Code == "99201");
            if (index >= 0) OfficeVisitCodes.RemoveAt(index);

            LoadOfficeVisitFromFile();

            if (OfficeVisit != null) NewEncounterSerivce = OfficeVisit.Where(i => i.Code != null).Select(i => i.Code).FirstOrDefault();
        }

        private void SaveBillingDiagnosisCodes()
        {
            if (Diagnoses != null && Diagnoses.Count > 0)
            {
                var selectedBillingDiagnosis = SelectedDiagnoses.OrderBy(d => d.OrdinalId).ToList();
                if (StandardExamLoadArguments.PatientId != null && StandardExamLoadArguments.EncounterId != null)
                    _standardExamFileManager.SaveBillingDiagnosis(StandardExamLoadArguments.PatientId.Value, StandardExamLoadArguments.EncounterId.Value, IsIcd10DiagnosisTypeSelected, selectedBillingDiagnosis);
            }
        }


        private void SaveBillingServiceCodes()
        {
            if (Services != null && Services.Count > 0)
            {
                var selectedBillingService = Services.Where(d => d.IsQueuedToBill).OrderBy(d => d.Id).ToList();
                if (StandardExamLoadArguments.PatientId != null && StandardExamLoadArguments.EncounterId != null)
                    _standardExamFileManager.SaveBillingService(StandardExamLoadArguments.PatientId.Value, StandardExamLoadArguments.EncounterId.Value, IsIcd10DiagnosisTypeSelected, selectedBillingService);
            }
        }

        private void ExecuteReloadServiceData(ServiceViewModel service)
        {
            if (service != null && service.ServiceCodes.Count > 1 && service.Code != null)
            {
                service = _standardExamViewService.GetServiceUnitCharge(service);
                var serviceToUpdate = Services.FirstOrDefault(i => i.Id == service.Id);
                if (serviceToUpdate != null)
                {
                    serviceToUpdate.Units = service.Units;
                    serviceToUpdate.UnitCharge = service.UnitCharge;
                }


                SelectedDiagnoses.ForEach(x =>
                {
                    string code = string.Empty;
                    if (IsIcd10DiagnosisTypeSelected)
                    {
                        if (x.Icd9Code != null && x.Icd9Code.Code != string.Empty)
                        {
                            code = x.Icd9Code.Code;
                        }
                    }
                    else
                    {
                        code = x.Code.Code;
                    }

                    if (code.IsNotNullOrEmpty())
                    {
                        int r = code.IndexOf('.');
                        if (r > 0)
                        {
                            int q = code.IndexOf('.', r + 1);
                            if (q > 0)
                            {
                                code = code.Substring(0, q);
                            }
                        }
                        var isLinked = _standardExamViewService.IsDiagnosisLinked(service.Code.Code, code);
                        if (isLinked)
                        {
                            x.IsLinked = true;
                            if (!(serviceToUpdate.DiagnosisLinks.Any(j => j.Id == x.Id)))
                            {
                                serviceToUpdate.DiagnosisLinks.Add(x);
                            }
                        }

                    }
                });
            }
        }

        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual bool EmEyeSelector { get; set; }

        public virtual bool IsFirstVisit { get; set; }

        public virtual bool EmLevel1 { get; set; }
        public virtual bool EmLevel2 { get; set; }
        public virtual bool EmLevel3 { get; set; }
        public virtual bool EmLevel4 { get; set; }
        public virtual bool EmLevel5 { get; set; }
        public virtual bool EyeLevel { get; set; }

        public virtual AppointmentTypeViewModel SelectedAppointmentType { get; set; }

        public virtual ObservableCollection<AppointmentTypeViewModel> AppointmentTypes { get; set; }

        public ICommand OfficeVisitSelectionChanged { get; protected set; }
        
        

        [Dependency]
        public virtual ServiceCodeViewModel NewEncounterSerivce { get; set; }
    }
}
