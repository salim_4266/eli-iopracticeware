﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.GlaucomaMonitor
{
    /// <summary>
    ///   Interaction logic for GlaucomaMonitorView.xaml
    /// </summary>
    public partial class GlaucomaMonitorView : UserControl
    {
        public GlaucomaMonitorView()
        {
            InitializeComponent();
        }
    }
}