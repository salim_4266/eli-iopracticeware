﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Interactivity;
using Soaf.Presentation;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.GlaucomaMonitor
{
    public class RadCartesianChartDynamicLineSeriesBehavior : Behavior<RadCartesianChart>
    {
        public IEnumerable<object> ItemsSource
        {
            get { return GetValue(ItemsSourceProperty) as IEnumerable<object>; }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof (IEnumerable<object>), typeof (RadCartesianChartDynamicLineSeriesBehavior), new PropertyMetadata(null, OnItemsSourcePropertyChanged));

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (RadCartesianChartDynamicLineSeriesBehavior)d;

            var oldCollectionChanged = e.OldValue as INotifyPropertyChanged;
            if (oldCollectionChanged != null)
            {
                oldCollectionChanged.PropertyChanged -= behavior.AddLineSeriesFromProperty;
            }

            var newCollectionChanged = e.NewValue as INotifyPropertyChanged;
            if (newCollectionChanged != null)
            {
                newCollectionChanged.PropertyChanged += behavior.AddLineSeriesFromProperty;
            }

            behavior.AddLineSeriesFromProperty(behavior, null);
        }

        private void AddLineSeriesFromProperty(object sender, PropertyChangedEventArgs e)
        {
            if (AssociatedObject != null)
            {
                var lineSeries = AssociatedObject.Series;
                lineSeries.Clear();
            }
        }
    }
}
