﻿using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.GlaucomaMonitor
{
    public class GlaucomaMonitorViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public GlaucomaMonitorViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }
         public void ShowGlaucomaMonitor()
         {
             var view = new GlaucomaMonitorView();
            
            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });
        }
    }
}
