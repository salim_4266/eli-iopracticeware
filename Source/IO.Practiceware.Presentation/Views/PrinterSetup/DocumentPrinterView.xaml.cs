﻿using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PrinterSetup
{
    /// <summary>
    ///   Interaction logic for DocumentPrinterView.xaml
    /// </summary>
    public partial class DocumentPrinterView
    {
        public DocumentPrinterView()
        {
            InitializeComponent();
        }
    }

    [SupportsDataErrorInfo]
    public class DocumentPrinterViewContext : IViewContext
    {
        private readonly IDocumentPrinterViewService _documentPrinterViewService;
        private NamedViewModel _selectedDocumentType;

        public DocumentPrinterViewContext(IDocumentPrinterViewService documentPrinterViewService)
        {
            _documentPrinterViewService = documentPrinterViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Apply = Command.Create<bool>(v => { if (v && this.IsValid()) SaveDocumentPrinter(); }, v => !v || this.IsValid());
        }

        public DialogButtons DialogButtons
        {
            get { return DialogButtons.Ok | DialogButtons.Cancel | DialogButtons.Apply; }
        }

        public ICommand Apply { get; protected set; }

        //Collection of Document Types        
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> DocumentTypes { get; set; }

        //Collection of Templates associated to a document type
        [DispatcherThread]
        public virtual ObservableCollection<DocumentViewModel> Documents { get; protected set; }

        //List of saved IO printers
        [DispatcherThread]
        public virtual ObservableCollection<PrinterViewModel> IoPrinters { get; protected set; }

        public virtual bool IsEditable
        {
            get { return true; }
        }

        //To check the Printer option checked status
        public virtual bool IsPrinterOptionsEnabled
        {
            get { return PrinterConfiguration.OverridePrinterConfiguration; }
            set
            {
                PrinterConfiguration.OverridePrinterConfiguration = value;
                if (value) IsDefaultPrinterEnabled = false;
            }
        }

        //To check the local default Printer checked status
        public virtual bool IsDefaultPrinterEnabled
        {
            get { return PrinterConfiguration.DefaultPrinterConfiguration; }
            set
            {
                PrinterConfiguration.DefaultPrinterConfiguration = value;
                if (value) IsPrinterOptionsEnabled = false;
            }
        }

        // Selected Document Type
        [Required(ErrorMessage = "Please select a document type")]
        public virtual NamedViewModel SelectedDocumentType
        {
            get { return _selectedDocumentType; }
            set
            {
                _selectedDocumentType = value;
                if (value != null)
                    Command.ExecuteAsync(() => { Documents = _documentPrinterViewService.GetTemplates(value.Id).ToExtendedObservableCollection(); }, () => InteractionContext);
                else Documents = new ObservableCollection<DocumentViewModel>();
            }
        }

        //Selected Template
        [Required(ErrorMessage = "Please select a document template")]
        public virtual DocumentViewModel SelectedDocument { get; set; }

        //Selected IO printer
        [Required(ErrorMessage = "Please select a printer")]
        public virtual PrinterViewModel SelectedIoPrinter { get; set; }

        public ICommand Load { get; protected set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            Documents = new ObservableCollection<DocumentViewModel>();
            DocumentTypes = _documentPrinterViewService.GetDocumentTypes().ToExtendedObservableCollection();
            IoPrinters = _documentPrinterViewService.GetIoPrinters().ToExtendedObservableCollection();

            SelectedDocumentType = null;
            SelectedDocument = null;
            SelectedIoPrinter = null;

            IsPrinterOptionsEnabled = PrinterConfiguration.OverridePrinterConfiguration;
            IsDefaultPrinterEnabled = PrinterConfiguration.DefaultPrinterConfiguration;
        }

        private void SaveDocumentPrinter()
        {
            if (SelectedDocumentType != null && SelectedDocument != null && SelectedIoPrinter != null)
            {
                _documentPrinterViewService.SaveDocumentPrinters(SelectedDocumentType.Id, SelectedDocument.Id, SelectedIoPrinter.Id, UserContext.Current.ServiceLocation.Item2);
                SelectedDocumentType = null;
                SelectedDocument = null;
                SelectedIoPrinter = null;
            }
        }
    }
}