﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PrinterSetup
{
    /// <summary>
    ///   Interaction logic for AddPrinterView.xaml
    /// </summary>
    public partial class AddPrinterView
    {
        public AddPrinterView()
        {
            InitializeComponent();
        }
    }

    [SupportsDataErrorInfo]
    public class AddPrinterViewContext : IViewContext
    {
        private readonly IAddPrinterViewService _addPrinterViewService;
        private AddPrinterViewModel _selectedPrinter;

        public AddPrinterViewContext(IAddPrinterViewService addPrinterViewService)
        {
            _addPrinterViewService = addPrinterViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            LoadAllNetworkPrinters();
            SavedPrinters = new ObservableCollection<PrinterViewModel>();
            RefreshPrinters = Command.Create(LoadAllNetworkPrinters).Async(() => InteractionContext);
            Apply = Command.Create<bool>(v => { if (v && this.IsValid()) SavePrinter(); }, v => !v || (this.IsValid()));
        }

        public virtual DialogButtons DialogButtons
        {
            get { return DialogButtons.Ok | DialogButtons.Cancel | DialogButtons.Apply; }
        }

        public ICommand Apply { get; protected set; }

        public ICommand Load { get; protected set; }

        //Command to refresh network printers list
        public ICommand RefreshPrinters { get; set; }

        // The selected network Printer from list
        public virtual AddPrinterViewModel SelectedPrinter
        {
            get { return _selectedPrinter; }
            set
            {
                _selectedPrinter = value;
                if (_selectedPrinter != null) UncPath = _selectedPrinter.UncPath;
            }
        }

        // The List of Network printers displayed on screen
        [DispatcherThread]
        public virtual ObservableCollection<AddPrinterViewModel> Printers { get; protected set; }

        // The user entered UNC path to search a printer from Network printers displayed on screen
        [Required(ErrorMessage = "Please enter a UNC path")]
        [UniqueUncPath]
        public virtual string UncPath { get; set; }

        // The user entered printer name to save a selected Network printer as an IO Printer
        [Required(ErrorMessage = "Please enter a printer name")]
        [UniquePrinterName]
        public virtual string IoPrinterName { get; set; }

        // The selected file server
        public virtual NamedViewModel SelectedServiceLocation { get; set; }

        // The List of File servers/Locations
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }

        // The List of saved IO Printers
        public ObservableCollection<PrinterViewModel> SavedPrinters { get; set; }

        #region Implimenting Validation Attribute for UNC path and Printer name

        #region Nested type: UniquePrinterNameAttribute

        public class UniquePrinterNameAttribute : ValidationAttribute
        {
            private AddPrinterViewContext _context;

            public UniquePrinterNameAttribute()
                : base("Printer name must be unique.")
            {
            }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                _context = (AddPrinterViewContext)validationContext.ObjectInstance;
                return base.IsValid(value, validationContext);
            }

            public override bool IsValid(object value)
            {
                return !(_context.IoPrinterName != null &&
                         _context.SavedPrinters.Any(t => _context.IoPrinterName.Equals(t.Name, StringComparison.OrdinalIgnoreCase)));
            }
        }

        #endregion

        #region Nested type: UniqueUncPathAttribute

        public class UniqueUncPathAttribute : ValidationAttribute
        {
            private AddPrinterViewContext _context;

            public UniqueUncPathAttribute()
                : base("UNC path must be unique.")
            {
            }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                _context = (AddPrinterViewContext)validationContext.ObjectInstance;
                return base.IsValid(value, validationContext);
            }

            public override bool IsValid(object value)
            {
                return !(_context.UncPath != null &&
                         _context.SavedPrinters.Any(t => _context.UncPath.Equals(t.UncPath, StringComparison.OrdinalIgnoreCase)));
            }
        }

        #endregion

        #endregion

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            ServiceLocations = _addPrinterViewService.GetServiceLocations().ToExtendedObservableCollection();
            SelectedServiceLocation = ServiceLocations.FirstOrDefault(s => s.Name.Equals(UserContext.Current.ServiceLocation.Item2, StringComparison.OrdinalIgnoreCase));
            SavedPrinters = _addPrinterViewService.GetIoPrinters().ToExtendedObservableCollection();
            SelectedPrinter = null;
            UncPath = string.Empty;
            IoPrinterName = string.Empty;
        }

        private void LoadAllNetworkPrinters()
        {
            Printers = new ObservableCollection<AddPrinterViewModel> { new AddPrinterViewModel { Name = "Searching..." } };

            Practiceware.Printers.FindNetworkPrinters(
                p => System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() => Printers.Insert(0, new AddPrinterViewModel(p)))),
                () => Printers = Printers.Take(Printers.Count - 1).OrderBy(p => p.Name).ToExtendedObservableCollection());
        }

        private void SavePrinter()
        {
            _addPrinterViewService.SavePrinterDetails(IoPrinterName.Trim(), UncPath, SelectedServiceLocation.Id);
            SelectedPrinter = null;
            UncPath = string.Empty;
            IoPrinterName = string.Empty;
            SavedPrinters = _addPrinterViewService.GetIoPrinters().ToExtendedObservableCollection();
        }
    }
}