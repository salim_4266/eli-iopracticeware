﻿using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.PrinterSetup
{
    public class PrinterSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;
        
        public PrinterSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPrinterOptions()
        {
            var view = new PrinterSetupView();
            _interactionManager.ShowModal(new WindowInteractionArguments {Content = view, WindowState = WindowState.Normal});
        }
    }
}