﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PrinterSetup
{
    /// <summary>
    ///   Interaction logic for PrinterSetupView.xaml
    /// </summary>
    public partial class PrinterSetupView
    {
        public PrinterSetupView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the PrinterSetupView.
    /// </summary>
    public class PrinterSetupViewContext : IViewContext
    {
        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion
    }
}