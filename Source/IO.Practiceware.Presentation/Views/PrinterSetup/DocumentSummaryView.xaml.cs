﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf.Collections;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PrinterSetup
{
    /// <summary>
    ///   Interaction logic for DocumentSummaryView.xaml
    /// </summary>
    public partial class DocumentSummaryView
    {
        public DocumentSummaryView()
        {
            InitializeComponent();
        }
    }

    public class DocumentSummaryViewContext : IViewContext
    {
        private readonly IDocumentSummaryViewService _documentSumamryViewService;

        public DocumentSummaryViewContext(IDocumentSummaryViewService documentSumamryViewService)
        {
            _documentSumamryViewService = documentSumamryViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
        }

        public ICommand Load { get; protected set; }

        public DialogButtons DialogButtons
        {
            get { return DialogButtons.Ok; }
        }

        /// <summary>
        ///   List of documents with associated IO printer names
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<DocumentPrinterViewModel> DocumentPrinters { get; protected set; }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            DocumentPrinters = _documentSumamryViewService.GetDocumentPrinters(UserContext.Current.ServiceLocation.Item2).ToExtendedObservableCollection();
        }
    }
}