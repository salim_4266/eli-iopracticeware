using Soaf.Collections;
using Soaf.Presentation;
using System.Windows.Controls;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    public class ReportViewerViewContext
    {
        private readonly string _documentation;

        public ReportViewerViewContext(string documentation)
        {
            _documentation = documentation;
            ViewDocumentation = Command.Create(ExecuteViewDocumentation);
        }

        private void ExecuteViewDocumentation()
        {
            if (_documentation.IsNotNullOrEmpty())
            {
                var browser = new WebBrowser { Width = 800, Height = 600 };
                browser.NavigateToString(_documentation);
                InteractionManager.Current.Show(new WindowInteractionArguments { Content = browser, ResizeMode = System.Windows.ResizeMode.CanResizeWithGrip });
            }
            else
            {
                InteractionManager.Current.Alert("No documentation exists for this report.");
            }
        }

        public ICommand ViewDocumentation { get; private set; }
    }
}