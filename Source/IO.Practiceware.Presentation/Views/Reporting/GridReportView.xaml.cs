﻿using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Reporting;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Presentation;
using Soaf.Reflection;
using Soaf.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Input;
using Telerik.Windows.Data;
using Enumerable = System.Linq.Enumerable;
using Expression = System.Linq.Expressions.Expression;
using Queryable = System.Linq.Queryable;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    /// <summary>
    ///     Interaction logic for GridReportViewer.xaml
    /// </summary>
    public partial class GridReportView
    {
        public GridReportView()
        {
            InitializeComponent();
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    public class GridReportViewContext : IViewContext
    {
        private const int CacheCleanupTimeout = 90000;
        private static readonly ExpressionEqualityComparer ExpressionEqualityComparer = new ExpressionEqualityComparer();

        private readonly Func<IQueryable> _getQuery;
        private readonly Func<GridReportFilterViewModel> _createFilter;
        private readonly IInteractionManager _interactionManager;
        private readonly IApplicationSettingsService _settingsService;
        private readonly Timer _cacheCleanupTimer;
        private readonly ConcurrentDictionary<Type, object> _emptyResultsRegistry = new ConcurrentDictionary<Type, object>();
        private readonly ConcurrentDictionary<Expression, SemaphoreSlim> _expressionExecutionLocks = new ConcurrentDictionary<Expression, SemaphoreSlim>(ExpressionEqualityComparer);
        private readonly ConcurrentDictionary<Expression, object> _queryCache = new ConcurrentDictionary<Expression, object>(ExpressionEqualityComparer);

        private readonly object _activeQueriesLock = new object();
        private string _lastAppliedFilterKey;
        private ApplicationSettingContainer<GridReportSettings> _settings;
        private int _activeQueryExecutions;
        private bool _pendingRefreshRequest;
        private bool _closed;

        public GridReportViewContext(Func<IQueryable> getQuery, Func<GridReportFilterViewModel> createFilter,
            IInteractionManager interactionManager, IApplicationSettingsService settingsService)
        {
            _getQuery = getQuery;
            _createFilter = createFilter;
            _interactionManager = interactionManager;
            _settingsService = settingsService;

            _cacheCleanupTimer = new Timer(OnCleanupCache);
            _cacheCleanupTimer.Change(Timeout.Infinite, Timeout.Infinite);

            SavedFilters = new ExtendedObservableCollection<GridReportFilterViewModel>();

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            RunReport = Command.Create(() => DataSource = new ReportingQueryableCollectionView(Query), () => Query != null).Async();
            FinalizeQueryExecutions = Command.Create(OnFinalizeQueryExecutions).Delay(800);
            Closing = Command.Create(() =>
            {
                _closed = true;
                _cacheCleanupTimer.Dispose();
            });
            SaveFilter = Command.Create(ExecuteSaveFilter).Async(() => InteractionContext);
            RemoveFilter = Command.Create<GridReportFilterViewModel>(ExecuteRemoveFilter).Async(() => InteractionContext);
        }

        private void LoadFilter(GridReportFilterViewModel filter)
        {
            // Prevent re-loading same filter
            var filterXml = filter.IfNotNull(f => f.SourceFilter.ToXml());
            if (_lastAppliedFilterKey == filterXml)
            {
                return;
            }
            _lastAppliedFilterKey = filterXml;

            ManageGridSettingsImplementation.LoadColumnFilters(filter.IfNotNull(f => f.SourceFilter.Descriptors));
        }

        private void ExecuteRemoveFilter(GridReportFilterViewModel filter)
        {
            // Save settings
            _settings.Value.Filters.Remove(filter.SourceFilter);
            _settingsService.SetSetting(_settings);

            // Remove from UI
            SavedFilters.Remove(filter);
        }

        private void ExecuteSaveFilter()
        {
            PromptResult promptResult = null;

            this.Dispatcher().Invoke(() =>
            {
                // Prompt for filter name
                promptResult = _interactionManager.Prompt("Please provide the name for this filter", null, SelectedFilter.IfNotNull(f => f.Name));
            });

            // Cancelled or empty input?
            if (!promptResult.DialogResult.GetValueOrDefault()
                || string.IsNullOrWhiteSpace(promptResult.Input)) return;

            // Updating existing filter?
            var filter = SavedFilters.FirstOrDefault(f => string.Equals(f.Name, promptResult.Input, StringComparison.OrdinalIgnoreCase));
            if (filter == null)
            {
                filter = _createFilter();
                filter.SourceFilter = new GridReportSettings.GridReportFilter();
                _settings.Value.Filters.Add(filter.SourceFilter);
                SavedFilters.Add(filter);
            }

            // Update filter values
            filter.Name = promptResult.Input;
            filter.SourceFilter.Descriptors = ManageGridSettingsImplementation.SaveColumnFilters().ToList();

            // Store filter
            _settingsService.SetSetting(_settings);

            // Set it as current filter
            SelectedFilter = filter;
        }

        private void ExecuteLoad()
        {
            // Load existing settings
            var settingsKey = string.Format(ApplicationSetting.GridReport, LoadArguments.ReportName);
            _settings = _settingsService.GetSetting<GridReportSettings>(settingsKey,null, null);

            if (_settings == null)
            {
                // Create empty settings for current report and save them
                _settings = new ApplicationSettingContainer<GridReportSettings>
                {
                    Name = settingsKey,
                    Value = new GridReportSettings
                    {
                        Filters = new List<GridReportSettings.GridReportFilter>()
                    }
                };
                _settingsService.SetSetting(_settings);
            }

            // Load settings into view models
            _settings.Value.Filters
                .ForEach(f => SavedFilters
                    .Add(_createFilter()
                        .Modify(cf => cf.SourceFilter = f)));

            // Capture when current filter changes
            this.As<INotifyPropertyChanged>().PropertyChanged += ContextPropertyChanged;

            Query = _getQuery();

            var empty = Queryable.AsQueryable((dynamic)typeof(List<>).MakeGenericType(Query.ElementType).CreateInstance());

            DataSource = new QueryableCollectionView(empty);

            // We want to achieve following workflow (must add fixup logic before caching, since we modify the expression to be cached):
            // GridReportViewContext.Executing -> Cache.Executing -> Cache.Executed -> GridReportViewContext.Executed
            Query.Provider.As<INotifyingQueryProvider>().IfNotNull(p => p.Executing += OnQueryExecuting);
            Query.Provider.As<INotifyingQueryProvider>().IfNotNull(p => p.Cache(_queryCache));
            Query.Provider.As<INotifyingQueryProvider>().IfNotNull(p => p.Executed += OnQueryExecuted);

            Task.Factory.StartNewWithCurrentTransaction(WarmUp);
        }

        private void OnQueryExecuting(object sender, QueryExecutingEventArgs e)
        {
            if (ExpressionExecutionScope.Current.IfNotNull(scope => scope.SkipInterception, false)) return;

            // Record query execution started
            HandleQueryActivated(ExpressionExecutionScope.Current.IfNotNull(scope => scope.ShowSpinner, true));

            // Initiate execution scope
            var executionScope = new ExpressionExecutionScope();

            // Optimize expression Telerik prepared
            e.Expression = PreprocessExpression(e.Expression);

            // Evaluate if query attempts to execute on foreground
            // Note: even with background load mode, Telerik still makes UI thread calls
            if (System.Windows.Application.Current.Dispatcher.CheckAccess())
            {
                // Assign empty result, so that expression is result is either taken from cache or not evaluated
                var emptyResult = MakeEmpty(e.Expression.Type);
                executionScope.EmptyFakeResult = emptyResult;
                executionScope.ForceExecuteInBackground = true;

                e.Result = emptyResult;
                e.Handled = true;
            }
            else
            {
                // Note: do not acquire locks on Main thread, as other threads can dispatch query execution and 
                // either deadlock (when no recursion is allowed) or enter acquired lock recursively and begin executing same query twice
                var executionLock = _expressionExecutionLocks.GetValue(e.Expression, () => new SemaphoreSlim(1, 1));
                executionLock.Wait();
            }
        }

        private void OnQueryExecuted(object sender, QueryExecutedEventArgs e)
        {
            if (ExpressionExecutionScope.Current.SkipInterception) return;

            // Retrieve execution lock
            var executionLock = _expressionExecutionLocks.GetValue(e.Expression, () => new SemaphoreSlim(1, 1));
            var deferredQueryExecution = false;

            // We do not acquire locks for foreground execution, so release it only if originally executed in background
            if (!ExpressionExecutionScope.Current.ForceExecuteInBackground)
            {
                executionLock.Release();
            }

            // Query cache might have contained a valid result and reset it as query result. Keep it if it's the case
            else if (e.Result == ExpressionExecutionScope.Current.EmptyFakeResult)
            {
                var emptyFakeResult = ExpressionExecutionScope.Current.EmptyFakeResult;

                // Run the query we skipped because it tried to execute on the UI thread
                Task.Factory
                    .StartNewWithCurrentTransaction(() =>
                    {
                        using (new ExpressionExecutionScope { SkipInterception = true })
                        {
                            executionLock.Wait();

                            // Validate that query cache still has fake empty result saved for our expression
                            if (_queryCache.ContainsKey(e.Expression) && _queryCache[e.Expression] == emptyFakeResult)
                            {
                                // Remove it then, so that query executes against server
                                object value;
                                _queryCache.TryRemove(e.Expression, out value);
                            }

                            ((IQueryProvider)sender).Execute(e.Expression);

                            executionLock.Release();

                            // Completed deferred query execution -> request refresh
                            HandleQueryCompleted(true);
                        }
                    });

                // Query will continue execution in background
                deferredQueryExecution = true;
            }

            if (!deferredQueryExecution)
            {
                HandleQueryCompleted();
            }

            // Dispose current scope
            ExpressionExecutionScope.Current.Dispose();
        }

        /// <summary>
        /// Finalizes once all queries complete execution
        /// </summary>
        private void OnFinalizeQueryExecutions()
        {
            // Prevent processing after view is closed
            if (_closed) return;

            bool shouldRefresh;
            lock (_activeQueriesLock)
            {
                // Skip this time if another query is now executing
                if (_activeQueryExecutions > 0) return;

                shouldRefresh = _pendingRefreshRequest;
                _pendingRefreshRequest = false;
            }

            // Cleanup after timeout
            _cacheCleanupTimer.Change(CacheCleanupTimeout, Timeout.Infinite);

            // No more queries executing for over 800ms -> remove busy
            // Execute outside of lock, since Remove might lead to Dispatcher.Invoke internally
            InteractionContext.BusyComponents.Remove(_activeQueriesLock);

            if (shouldRefresh)
            {
                this.Dispatcher().Invoke(() => ((ReportingQueryableCollectionView)DataSource).Refresh());
            }
        }

        /// <summary>
        /// Handles the query activated.
        /// </summary>
        /// <param name="allowsSpinner">if set to <c>true</c> [allows spinner].</param>
        private void HandleQueryActivated(bool allowsSpinner = true)
        {
            // Cleanup later
            _cacheCleanupTimer.Change(Timeout.Infinite, Timeout.Infinite);

            lock (_activeQueriesLock)
            {
                _activeQueryExecutions++;
                if (_activeQueryExecutions > 0 && allowsSpinner
                    && !InteractionContext.BusyComponents.Contains(_activeQueriesLock))
                {
                    InteractionContext.BusyComponents.Add(_activeQueriesLock);
                }
            }
        }

        /// <summary>
        /// Handles the query completed.
        /// </summary>
        /// <param name="requiresRefresh">if set to <c>true</c> [requires refresh].</param>
        private void HandleQueryCompleted(bool requiresRefresh = false)
        {
            lock (_activeQueriesLock)
            {
                _activeQueryExecutions--;
                _pendingRefreshRequest |= requiresRefresh;
                if (_activeQueryExecutions == 0)
                {
                    // Defer finalization in case more query executions come in
                    FinalizeQueryExecutions.Execute();
                }
            }
        }

        private void WarmUp()
        {
            Task.Factory.StartNewWithCurrentTransaction(() =>
            {
                using (new ExpressionExecutionScope { ShowSpinner = false })
                {
                    QueryableExtensions.Take(Query, 100).CastTo<IEnumerable>().OfType<object>().ToArray().EnsureNotDefault();
                }
            });

            using (new ExpressionExecutionScope { ShowSpinner = false })
            {
                QueryableExtensions.Count(Query);
            }
        }

        private void OnCleanupCache(object state)
        {
            lock (_activeQueriesLock)
            {
                // Cleanup all we cached so far
                _expressionExecutionLocks.Clear();
                _emptyResultsRegistry.Clear();
                _queryCache.Clear();
            }
        }

        private static Expression PreprocessExpression(Expression source)
        {
            // prevent bringing back ALL items in an AggregateFunctionsGroup just to do an aggregation
            var result = new AggregateFunctionsGroupTakeApplier().Visit(source);

            // only take the page size (Telerik will erroneously omit a Take call when applying aggregates)
            if (result.Type == typeof(IQueryable<AggregateFunctionsGroup>))
            {
                var methodCallResult = result as MethodCallExpression;
                // Ensure expression does not define Take(N) already
                if (!(methodCallResult != null
                    && methodCallResult.Method.Name == "Take"
                    && methodCallResult.Method.DeclaringType == typeof(Queryable)))
                {
                    result = Expression.Call(typeof(Queryable), "Take", new[] { typeof(AggregateFunctionsGroup) }, result, Expression.Constant(100));
                }
            }

            return result;
        }

        private object MakeEmpty(Type type)
        {
            var emptyInstance = _emptyResultsRegistry.GetValue(type, () =>
            {
                if (type.IsGenericTypeFor(typeof(IEnumerable<>)))
                {
                    return (object)Queryable.AsQueryable((dynamic)typeof(List<>).MakeGenericType(type.FindElementType()).CreateInstance());
                }
                return type.CreateInstance();
            });
            return emptyInstance;
        }

        private void ContextPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Interested only in SelectedFilter property
            if (e.PropertyName != Reflector.GetMember<GridReportViewContext>(vc => vc.SelectedFilter).Name)
            {
                return;
            }

            LoadFilter(SelectedFilter);
        }

        public virtual GridReportLoadArguments LoadArguments { get; set; }

        public virtual IEnumerable DataSource { get; protected set; }

        public virtual IInteractionContext InteractionContext { get; set; }

        public virtual ICommand Load { get; private set; }

        public virtual ICommand Closing { get; private set; }

        public virtual IQueryable Query { get; private set; }

        public virtual ICommand RunReport { get; private set; }

        public virtual ICommand FinalizeQueryExecutions { get; set; }

        public virtual ICollection<GridReportFilterViewModel> SavedFilters { get; set; }

        public virtual GridReportFilterViewModel SelectedFilter { get; set; }

        public virtual ICommand SaveFilter { get; private set; }

        public virtual ICommand RemoveFilter { get; private set; }

        /// <summary>
        /// Allows to retrieve current grid filters and set them
        /// </summary>
        public IManageGridSettingsImplementation ManageGridSettingsImplementation { get; set; }

        /// <summary>
        /// Applies Take(100) to the Items and ItemCount of an AggregateFunctionsGroup (otherwise each group will fetch all items from the server regardless of the page size).
        /// </summary>
        private class AggregateFunctionsGroupTakeApplier : ExpressionVisitor
        {
            protected override Expression VisitMemberInit(MemberInitExpression node)
            {
                if (node.NewExpression.Type == typeof(AggregateFunctionsGroup))
                {
                    return node.Update(VisitAndConvert(
                        node.NewExpression, "VisitMemberInit").EnsureNotDefault(),
                        Visit(node.Bindings.Select(ReplaceBinding).ToList().AsReadOnly(), VisitMemberBinding));
                }

                return base.VisitMemberInit(node);
            }

            private static MemberBinding ReplaceBinding(MemberBinding b)
            {
                if (b.Member.Name == "Items")
                    return GetItemsProjection((MemberAssignment)b);
                if (b.Member.Name == "ItemCount")
                    return GetItemCount((MemberAssignment)b);

                return b;
            }

            private static MemberBinding GetItemCount(MemberAssignment a)
            {
                var countCall = (MethodCallExpression)a.Expression;
                var takeCall = Expression.Call(typeof(Enumerable), "Take", new[] { countCall.Arguments[0].Type.FindElementType() }, countCall.Arguments[0], Expression.Constant(100));
                countCall = Expression.Call(countCall.Method, takeCall);

                a = a.Update(countCall);
                return a;
            }

            private static MemberBinding GetItemsProjection(MemberAssignment a)
            {
                var exp = Expression.Call(typeof(Enumerable), "Take", new[] { a.Expression.Type.FindElementType() }, a.Expression, Expression.Constant(100));

                a = a.Update(exp);
                return a;
            }
        }
    }

    public class ReportingQueryableCollectionView : QueryableCollectionView
    {
        public ReportingQueryableCollectionView(IQueryable source)
            : base(source)
        {
            PageSize = 100;
        }

        public override object GetItemAt(int index)
        {
            // sometimes Telerik gets the count wrong...
            if (index < 0 || index >= ItemCount) return null;
            return base.GetItemAt(index);
        }

    }

}