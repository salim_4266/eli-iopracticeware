﻿using System;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    /// <summary>
    /// Allows to capture information regarding executing query
    /// </summary>
    public class ExpressionExecutionScope : IDisposable
    {
        [ThreadStatic]
        private static ExpressionExecutionScope _current;

        private readonly ExpressionExecutionScope _last;

        public ExpressionExecutionScope()
        {
            _last = _current;
            _current = this;

            ShowSpinner = true;
        }

        public static ExpressionExecutionScope Current
        {
            get { return _current; }
        }

        /// <summary>
        /// Indicates if execution on the current query was skipped because it tried to run on the UI thread.
        /// </summary>
        public bool ForceExecuteInBackground { get; set; }

        /// <summary>
        /// Indicates that execution of this query should not be intercepted (no fixing, no background checks)
        /// </summary>
        public bool SkipInterception { get; set; }

        /// <summary>
        /// Stores empty fake result used along with ForceExecuteInBackground flag
        /// </summary>
        public object EmptyFakeResult { get; set; }

        /// <summary>
        /// Indicates whether showing of spinner is allowed
        /// </summary>
        public bool ShowSpinner { get; set; }

        public void Dispose()
        {
            _current = _last;
        }
    }
}