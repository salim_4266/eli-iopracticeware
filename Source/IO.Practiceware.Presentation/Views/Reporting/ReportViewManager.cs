﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using IO.Practiceware.Services.Data;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Reporting;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Telerik.Reporting.XmlSerialization;
using Telerik.ReportViewer.Wpf;
using Report = Telerik.Reporting.Report;
using WindowStartupLocation = Soaf.Presentation.WindowStartupLocation;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    [Singleton]
[Serializable]
public class ReportViewManager
{
    private readonly IInteractionManager _interactionManager;
    private readonly IServiceProvider _serviceProvider;
    private readonly Func<Func<IQueryable>, GridReportViewContext> _createGridReportViewContext;
    private readonly IReportService _reportService;
    private readonly IDocumentGenerationService _documentGenerationService;

    public ReportViewManager(IInteractionManager interactionManager, IServiceProvider serviceProvider, Func<Func<IQueryable>, GridReportViewContext> createGridReportViewContext, IReportService reportService, IDocumentGenerationService documentGenerationService)
    {
        _interactionManager = interactionManager;
        _serviceProvider = serviceProvider;
        _createGridReportViewContext = createGridReportViewContext;
        _reportService = reportService;
        _documentGenerationService = documentGenerationService;
    }

    public void ViewReportList()
    {
        var view = new ReportListView();
        _interactionManager.ShowModal(new WindowInteractionArguments
        {
            Content = view,
            WindowState = WindowState.Normal,
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
            ResizeMode = ResizeMode.CanResizeWithGrip
        });
    }

    private static void PrintReport(ReportContent reportContent, Hashtable parameters)
    {
        var telerikReportContent = reportContent as TelerikReportContent;
        if (telerikReportContent == null)
        {
            throw new ArgumentException("Can only print Telerik reports", "reportContent");
        }
        using (Report report = PrepareReport(telerikReportContent, parameters))
        {
            var printerSettings = new PrinterSettings();

            // The standard print controller comes with no UI
            PrintController standardPrintController = new StandardPrintController();

            var reportProcessor = new ReportProcessor();
            reportProcessor.PrintController = standardPrintController;

            var reportSource = new InstanceReportSource();
            reportSource.ReportDocument = report;

            reportProcessor.PrintReport(reportSource, printerSettings);
        }
    }


    private static List<RenderingResult> RenderReports(ReportContent reportContent, IEnumerable<Hashtable> parameters, string format)
    {
        var results = new List<RenderingResult>();
        LoadingWindow.Run(() =>
        {
            var telerikReportContent = reportContent as TelerikReportContent;
            if (telerikReportContent == null)
            {
                throw new ArgumentException("Can only print Telerik reports", "reportContent");
            }

            foreach (var p in parameters)
            {
                using (var report = PrepareReport(telerikReportContent, p))
                {
                    var reportProcessor = new ReportProcessor();
                    var reportSource = new InstanceReportSource { ReportDocument = report };
                    results.Add(reportProcessor.RenderReport(format, reportSource, p));
                }
            }
        });
        return results;
    }

    private void ViewReport(string reportName, ReportContent reportContent, string documentation, Hashtable parameters)
    {
        if (reportContent == null) return;

        Action<object> viewReport = view => _interactionManager.ShowModal(new WindowInteractionArguments
        {
            Content = view,
            WindowState = WindowState.Maximized,
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
            ResizeMode = ResizeMode.CanResizeWithGrip
        });

        if (reportContent is TelerikReportContent)
        {
            using (var report = PrepareReport(reportContent.CastTo<TelerikReportContent>(), parameters))
            {
                var viewer = new ReportViewer
                {
                    ReportSource = new InstanceReportSource { ReportDocument = report },
                    DataContext = new ReportViewerViewContext(documentation)
                };
                viewer.TextResources = new TextResources { ReportParametersNullText = "All", PreviewButtonText = "Run Report" };
                viewer.ViewMode = ViewMode.PrintPreview;
                viewReport(viewer);
            }
        }
        else if (reportContent is QueryableReportContent)
        {
            var queryableReportContent = reportContent as QueryableReportContent;
            var repositoryType = queryableReportContent.RepositoryType
                .ToType()
                .EnsureNotDefault("Could not resolve repository type {0}.".FormatWith(queryableReportContent.RepositoryType));

            // FYI: since we are currently on main thread -> line below enables optimizations for all calls to IAdoService (even running in background on other threads)
            using (DisposableScope.Create(
                ExtendedAdoService.EnableQueryingOptimizationsContextDataKey,
                k => ContextData.Set(k, new QueryingOptimizationParameters
                {
                    ReuseCachedViewData = true,
                    ViewCacheTimeout = TimeSpan.FromMinutes(20), // Re-use reporting data for 20 minutes,
                    FastViewDataCopy = true,
                    ReadUncommitted = true
                }),
                k => ContextData.Remove(k)))
            {

                var repository = _serviceProvider.GetService(repositoryType);

                var getQuery = new Func<IQueryable>(() =>
                                                    repository.GetType().GetProperty(queryableReportContent.QueryableMemberName).IfNotNull(p => p.GetValue(repository, null) as IQueryable)
                                                        .EnsureNotDefault("Could not find query for report {0}.".FormatWith(queryableReportContent.QueryableMemberName))
                    );

                var gridReportViewContext = _createGridReportViewContext(getQuery);
                gridReportViewContext.LoadArguments = new GridReportLoadArguments
                {
                    ReportName = reportName
                };
                var reportView = new GridReportView { DataContext = gridReportViewContext };
                viewReport(reportView);
            }
        }
    }

    public void PrintReport(string reportName, Hashtable parameters)
    {
        var content = RetrieveContentByName(reportName);
        if (content == null) return;

        PrintReport(content, parameters);
    }

    public void ViewReport(string reportName, string documentation, Hashtable parameters)
    {
        var content = RetrieveContentByName(reportName);
        if (content == null) return;

        ViewReport(reportName, content, documentation, parameters);
    }

    /// <summary>
    /// Saves all rendered reports as a single combined file.
    /// </summary>
    /// <param name="reportName">Name of the report.</param>
    /// <param name="format">The format.</param>
    /// <param name="parameters">The parameters.</param>
    /// <returns></returns>
    public CombinedReportPdfResult RenderCombinedReport(string reportName, string format, params Hashtable[] parameters)
    {
        format = format.ToLower();

        var content = RetrieveContentByName(reportName);

        if (!new[] { "pdf", "csv" }.Contains(format))
        {
            throw new NotSupportedException("Format {0} is not supported for a combined report.".FormatWith(format));
        }

        var results = RenderReports(content, parameters, format);

        var combinedResult = new CombinedReportPdfResult { IndividualReportPdfs = new Dictionary<Hashtable, byte[]>() };

        if (results.Count == 0)
        {
            return combinedResult;
        }

        var encoding = results[0].Encoding;

        for (int i = 0; i < parameters.Length; i++)
        {
            combinedResult.IndividualReportPdfs[parameters[i]] = results[i].DocumentBytes;
        }

        var byteSet = results.Where(r => !r.HasErrors)
            .Select(r => r.DocumentBytes)
            .ToArray();

        switch (format)
        {
            case "pdf":
                combinedResult.CombinedReportPdf = _documentGenerationService.CombinePdfs(byteSet);
                break;
            case "csv":
                combinedResult.CombinedReportPdf = encoding.GetBytes(_documentGenerationService.CombineCsvs(true, byteSet.Select(encoding.GetString).ToArray()));
                break;
        }

        return combinedResult;
    }

    /// <summary>
    /// Renders the report for each set of parameters in the specified format.
    /// </summary>
    /// <param name="reportName">Name of the report.</param>
    /// <param name="format">The format.</param>
    /// <param name="parameters">The parameters.</param>
    /// <returns></returns>
    public byte[] RenderReport(string reportName, string format, Hashtable parameters)
    {
        var content = RetrieveContentByName(reportName);

        var results = RenderReports(content, new[] { parameters }, format);

        return results.Select(r => r.DocumentBytes).FirstOrDefault();
    }

    private ReportContent RetrieveContentByName(string reportName)
    {
        var report = LoadingWindow.Run(() => _reportService.GetReport(reportName));
        if (report == null) return null;

        if (!report.IsPermissioned)
        {
            _interactionManager.Alert("Not permissioned for report {0}.".FormatWith(reportName));
            return null;
        }

        var content = LoadingWindow.Run(() => _reportService.GetReportContent(reportName));
        return content;
    }

    private static Report PrepareReport(TelerikReportContent reportContent, Hashtable parameters)
    {
        using (var ms = new MemoryStream(reportContent.CastTo<TelerikReportContent>().Content))
        {
            var report = (Report)(new ReportXmlSerializer().Deserialize(ms));
            // Assign passed in parameter values
            if (parameters != null)
            {
                foreach (var reportParameter in report.ReportParameters)
                {
                    if (parameters.Contains(reportParameter.Name))
                    {
                        reportParameter.Value = parameters[reportParameter.Name];
                    }
                    // Hide parameter always
                    reportParameter.Visible = false;
                }
            }
            // here updating rptPrmtr.MultiValue = true for paremeters taking multiple values with "," separated
            if (report.Name.Equals("Patient Monthly Statement"))
            {
                foreach (var rptPrmtr in report.ReportParameters)
                {
                    if (Convert.ToString(rptPrmtr.Type) == "String")
                    {
                        rptPrmtr.MultiValue = true;
                    }
                }
            }
            return report;
        }
    }
}

public class CombinedReportPdfResult
{
    public byte[] CombinedReportPdf { get; set; }

    public Dictionary<Hashtable, byte[]> IndividualReportPdfs { get; set; }
}
}