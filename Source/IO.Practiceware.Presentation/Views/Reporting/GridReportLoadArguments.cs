namespace IO.Practiceware.Presentation.Views.Reporting
{
    public class GridReportLoadArguments
    {
        public string ReportName { get; set; }
    }
}