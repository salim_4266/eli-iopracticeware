﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using Soaf;
using Soaf.Threading;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using GridViewColumn = Telerik.Windows.Controls.GridViewColumn;
using Soaf.Linq.Expressions;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    public class ReportingGridViewBehavior : Behavior<RadGridView>
    {
        private readonly Dictionary<string, IEnumerable> _cache = new Dictionary<string, IEnumerable>();

        protected override void OnAttached()
        {
            AssociatedObject.DistinctValuesLoading += OnDistinctValuesLoading;
            AssociatedObject.Columns.CollectionChanged += OnColumnsChanged;
            AssociatedObject.ElementExporting += OnElementExporting;

            base.OnAttached();
        }

        static void OnElementExporting(object sender, GridViewElementExportingEventArgs e)
        {
            // The following allows aggregate function results to be exported in the Grouped Headers
            if (e.Element == ExportElement.GroupHeaderCell)
            {
                var text = e.Value;

                var context = e.Context.As<QueryableCollectionViewGroup>();

                foreach (var aggregateResult in context.AggregateResults)
                {
                    text += " {0} {1}".FormatWith(aggregateResult.Caption, aggregateResult.FormattedValue);
                }

                e.Value = text;
            }
        }

        private void OnColumnsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (AssociatedObject.Columns.Count > 0 && !AssociatedObject.Columns[0].AggregateFunctions.OfType<CountFunction>().Any())
            {
                AssociatedObject.Columns[0].AggregateFunctions.Add(new CountFunction
                {
                    ResultFormatString = "Count: {0}"
                });
            }

            foreach (var column in AssociatedObject.Columns.OfType<GridViewDataColumn>()
                .Where(c => !c.AggregateFunctions.OfType<SumFunction>().Any()
                            && !c.UniqueName.EndsWith("Id", StringComparison.OrdinalIgnoreCase)
                            && new[] { typeof(int), typeof(long), typeof(decimal) }.Contains(c.DataType.AsUnderlyingTypeIfNullable())).ToArray())
            {
                column.AggregateFunctions.Add(new SumFunction
                {
                    ResultFormatString = "Total " + (column.Name.IsNullOrWhiteSpace() ? column.UniqueName : column.Name) + ": {0}"
                });
            }

            foreach (var col in AssociatedObject.Columns.OfType<GridViewDataColumn>())
            {
                col.IsFilteringDeferred = true;
            }
        }

        private void OnDistinctValuesLoading(object sender, GridViewDistinctValuesLoadingEventArgs e)
        {
            IEnumerable result;
            if (_cache.TryGetValue(e.Column.UniqueName, out result))
            {
                e.ItemsSource = result;
            }
            else
            {
                var context = AssociatedObject.DataContext as GridReportViewContext;
                if (context != null)
                {
                    var query = context.Query;
                    SetLoading(e.Column);
                    Task.Factory.StartNewWithCurrentTransaction(() => LoadDistinctValues(query, e.Column.UniqueName)).ContinueWith(t =>
                    {
                        PopulateDistinctValues(e.Column);
                        RemoveLoading(e.Column);
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }
                e.ItemsSource = _cache[e.Column.UniqueName] = new object[] { string.Empty };
            }
        }

        private void RemoveLoading(GridViewColumn column)
        {
            var grid = column.Header as Grid;
            if (grid != null)
            {
                var content = grid.Children.OfType<ContentControl>().FirstOrDefault();
                if (content != null)
                {
                    grid.Children.Remove(content);
                    column.Header = content.Content;
                }
            }
        }

        private static void SetLoading(GridViewColumn column)
        {
            var grid = new Grid();
            grid.Children.Add(new ContentControl { Content = column.Header });
            var bar = new RadProgressBar { IsEnabled = true, Height = 16, IsIndeterminate = true };
            grid.Children.Add(bar);
            grid.Width = column.ActualWidth;
            grid.Height = 16;
            column.Header = grid;

        }

        private void PopulateDistinctValues(GridViewColumn column)
        {
            var popup = AssociatedObject.ChildrenOfType<FilteringDropDown>().Where(f => ReferenceEquals(f.Column, column)).Select(d => d.ChildrenOfType<Popup>().FirstOrDefault()).FirstOrDefault();
            if (popup != null && popup.IsOpen)
            {
                popup.IsOpen = false;
                popup.IsOpen = true;
            }
            var grid = column.Header.CastTo<Grid>();
            grid.Children.Remove(grid.Children.OfType<RadProgressBar>().Single());
        }

        private void LoadDistinctValues(IQueryable query, string columnName)
        {
            if (query != null)
            {
                // remove any Distinct calls since we apply here
                var expression = query.Expression;
                expression = expression.Replace(e => e.As<MethodCallExpression>().IfNotNull(mc => mc.Method.Name == "Distinct" ? mc.Arguments[0] : mc) ?? e);                
                query = query.Provider.CreateQuery(expression);

                ParameterExpression selectParameter = Expression.Parameter(query.ElementType, "x");

                MemberExpression property = Expression.Property(selectParameter, columnName);

                LambdaExpression lambda = Expression.Lambda(property, selectParameter);
                MethodCallExpression select = Expression.Call(typeof(Queryable), "Select", new[] { query.ElementType, property.Type }, query.Expression, lambda);

                MethodCallExpression distinct = Expression.Call(typeof(Queryable), "Distinct", new[] { property.Type }, select);

                var orderParameter = Expression.Parameter(property.Type, "x");
                var order = Expression.Call(typeof(Queryable), property.Type.AsUnderlyingTypeIfNullable() == typeof(DateTime) ? "OrderByDescending" : "OrderBy", new[] { property.Type, property.Type }, distinct, Expression.Lambda(orderParameter, orderParameter));

                var take = Expression.Call(typeof(Queryable), "Take", new[] { property.Type }, order, Expression.Constant(1000));
                query = query.Provider.CreateQuery(take);

                object[] values;

                using (new ExpressionExecutionScope { ShowSpinner = false })
                {
                    values = query.CastTo<IEnumerable>().OfType<object>().ToArray();
                }

                _cache[columnName] = values;
            }
        }

    }

    public class SumFunction : EnumerableSelectorAggregateFunction
    {
        protected override string AggregateMethodName
        {
            get { return "Sum"; }
        }

        public override Expression CreateAggregateExpression(Expression enumerableExpression)
        {
            ParameterExpression sumParameter = Expression.Parameter(enumerableExpression.Type.FindElementType(), "x");
            MethodCallExpression sum = Expression.Call(typeof(Enumerable), "Sum",
                                                       new[] { sumParameter.Type }, new[] { enumerableExpression, Expression.Lambda(Expression.Property(sumParameter, SourceField), sumParameter) });

            return sum;
        }
    }
}