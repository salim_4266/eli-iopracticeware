﻿using System;
using IO.Practiceware.Services.Reporting;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Reporting
{
    public partial class ReportListView
    {
        public ReportListView()
        {
            InitializeComponent();
        }
    }

    public class ReportListViewContext : IViewContext
    {
        private readonly IReportService _reportService;
        private readonly ReportViewManager _reportViewManager;
        private readonly Func<ReportGroupViewModel> _createReportGroupViewModel;

        public ReportListViewContext(IReportService reportService, ReportViewManager reportViewManager, Func<ReportGroupViewModel> createReportGroupViewModel)
        {
            _reportService = reportService;
            _reportViewManager = reportViewManager;
            _createReportGroupViewModel = createReportGroupViewModel;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ViewReport = Command.Create(ExecuteViewReport, () => SelectedReport != null);
        }

        public ICommand Load { get; protected set; }

        public ICommand ViewReport { get; protected set; }

        // List of .trdx report names in ServerDataPath\Reports Folder
        [DispatcherThread]
        public virtual IEnumerable<Report> Reports { get; set; }

        [DependsOn("SearchText", "Reports")]
        [DispatcherThread]
        public virtual IEnumerable<ReportGroupViewModel> ReportsWithGroups
        {
            get
            {
                var returnReports =  new List<ReportGroupViewModel>();
                if (Reports == null) return returnReports;
                
                returnReports.AddRange(Reports.Where(r => SearchText.IsNullOrWhiteSpace() || r.Name.ToLower().Contains(SearchText.ToLower()) || r.Documentation.ToLower().Contains(SearchText.ToLower()))
                            .GroupBy(f => f.Category)
                            .Select(g => _createReportGroupViewModel().Modify(
                            vm => vm.GroupName = g.Key).Modify(
                            vm => vm.Reports = g.ToList())).ToList());
                return returnReports;
            }
        }

        public virtual string SearchText { get; set; }

        public virtual Report SelectedReport { get; set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteViewReport()
        {
            if (SelectedReport != null)
            {
                Report report = SelectedReport;
                _reportViewManager.ViewReport(SelectedReport.Name, SelectedReport.Documentation, null);
                SelectedReport = null;
                SelectedReport = report;
            }
        }

        private void ExecuteLoad()
        {
            Reports = _reportService.GetReports().ToExtendedObservableCollection();
        }
    }
}