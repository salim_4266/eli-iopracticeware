﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    public partial class ScanView
    {
        private readonly string _scanSaveLocation;
        private readonly IScanningService _scanningService;

        private ScanSettings _scanSettings;

        public ScanView(string scanSaveLocation, IScanningService scanningService)
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _scanSaveLocation = scanSaveLocation;
            _scanningService = scanningService;

            CancelButton.Click += OnCancelButtonClick;
            ScanButton.Click += OnScanButtonClick;
            ResetOptionsButton.Click += OnResetOptionsButtonClick;
        }

        protected void OnDisposed(object sender, EventArgs e)
        {
            try
            {
                var path = Directory.GetParent(ConfigurationManager.Configuration.FilePath).FullName + "\\ScanSettings.xml";
                FileManager.Instance.CommitContents(path, _scanSettings.ToXml());
            }
            catch (Exception ex)
            {
                Trace.TraceError(new Exception("Problem saving scan settings.", ex).ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SetUpSettings(false);
            Disposed += OnDisposed;
        }

        private void SetUpSettings(bool shouldResetSettings)
        {
            _scanSettings = new ScanSettings();
            if (!shouldResetSettings)
            {
                try
                {
                    var path = Directory.GetParent(ConfigurationManager.Configuration.FilePath).FullName + "\\ScanSettings.xml";
                    if (FileManager.Instance.FileExists(path))
                    {
                        _scanSettings = FileManager.Instance.ReadContentsAsText(path).FromXml<ScanSettings>();
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(new Exception("Problem loading scan settings.", ex).ToString());
                }
            }
            var scannerNames = _scanningService.GetTwainScannerNames().ToList();
            if (_scanSettings.SelectedScanner.IsNullOrEmpty() || !scannerNames.Contains(_scanSettings.SelectedScanner))
                _scanSettings.SelectedScanner = scannerNames.FirstOrDefault();

            ScannerList.ItemsSource = _scanningService.GetTwainScannerNames();
            if (ScannerList.ItemsSource.IsNullOrEmpty())
                ScanButton.Enabled = false;
            ScannerList.SelectedItem = _scanSettings.SelectedScanner;

            DPIList.ItemsSource = Enums.GetValues<DpiSetting>();
            DPIList.SelectedItem = _scanSettings.Dpi;

            var pageSizeListItemsSource = new List<Tuple<string, PageSize>>();
            foreach (PageSize pageSize in Enum.GetValues(typeof(PageSize)))
            {
                pageSizeListItemsSource.Add(new Tuple<string, PageSize>(pageSize.GetDisplayName(), pageSize));
            }

            PageSizeList.ItemsSource = pageSizeListItemsSource;
            PageSizeList.DisplayMemberPath = "Item1";
            PageSizeList.SelectedValuePath = "Item2";
            PageSizeList.SelectedValue = _scanSettings.PageSize;
            if (PageSizeList.SelectedValue == null) PageSizeList.SelectedIndex = 0;

            ColorSettingList.ItemsSource = Enums.GetValues<ColorSetting>().ToDictionary(i => i.GetDescription());

            ColorSettingList.DisplayMemberPath = "Key";
            ColorSettingList.SelectedValuePath = "Value";
            ColorSettingList.SelectedValue = _scanSettings.ColorSetting;

            DuplexModeCheckButton.IsChecked = _scanSettings.DuplexMode;
        }

        private void OnScanButtonClick(object sender, EventArgs e)
        {
            _scanSettings.SelectedScanner = (String)ScannerList.SelectedItem;

            _scanSettings.Dpi = (DpiSetting)DPIList.SelectedItem;

            _scanSettings.PageSize = (PageSize)PageSizeList.SelectedValue;

            _scanSettings.ColorSetting = ColorSettingList.SelectedValue.ToString().ToEnumOrDefault(_scanSettings.ColorSetting);

            _scanSettings.DuplexMode = DuplexModeCheckButton.IsChecked ?? false;
            Tuple<ScanReturnStatus, byte[]> statusAndContent = _scanningService.ScanToPdf(_scanSettings);
            var status = statusAndContent.Item1;
            var content = statusAndContent.Item2;
            InteractionManager.Current.Alert(status.GetAttribute<DescriptionAttribute>().Description);
            if (status != ScanReturnStatus.Success)
            {
                Trace.TraceError(status.ToString());
            }
            else
            {
                FileManager.Instance.CommitContents(_scanSaveLocation, content);
            }
            InteractionContext.Complete(status == ScanReturnStatus.Success);
        }

        private void OnResetOptionsButtonClick(object sender, EventArgs e)
        {
            SetUpSettings(true);
        }

        private void OnCancelButtonClick(object sender, EventArgs e)
        {
            InteractionContext.Complete(false);
        }
    }
}