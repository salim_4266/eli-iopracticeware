﻿using System.Collections.Generic;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    public class ImagingViewManager
    {
        private readonly IScanningService _scanningService;
        
        public ImagingViewManager(IScanningService scanningService)
        {
            _scanningService = scanningService;
        }

        public byte[] ShowCameraView()
        {
            var view = new CameraView();
            InteractionManager.Current.ShowModal(new WindowInteractionArguments
                                                     {
                                                         Content = view,
                                                         ResizeMode = ResizeMode.NoResize
                                                     });

            return view.ImageData;
        }

        public void ShowScanView(string saveToFilePath)
        {
            var view = new ScanView(saveToFilePath, _scanningService);
            InteractionManager.Current.ShowModal(new WindowInteractionArguments
                                                     {
                                                         Content = view,
                                                         ResizeMode = ResizeMode.NoResize
                                                     });
        }


        public CardScannerReturnArguments ShowCardScannerView(int? patientId)
        {
            var view = new CardScannerView();
            view.DataContext.CastTo<CardScannerViewContext>().PatientId = patientId;

            var result = InteractionManager.Current.ShowModal(new WindowInteractionArguments
                                                              {
                                                                  Content = view,
                                                                  ResizeMode = ResizeMode.NoResize
                                                              });

            if (!result.GetValueOrDefault())
            {
                return null;
            }

            CardScannerReturnArguments returnArgs = null;
            var scannedData = view.DataContext.CastTo<CardScannerViewContext>().ProcessedData;
            var returnPatientId = view.DataContext.CastTo<CardScannerViewContext>().PatientId;
            if (scannedData.IsNotNullOrEmpty())
            {
                returnArgs = new CardScannerReturnArguments {PatientId = returnPatientId, ScannedCardData = scannedData};
            }
            return returnArgs;
        }

        public void ShowImageViewer(byte[] imageToDisplay, bool showModal = false)
        {
            var view = new DisplayImageView();
            var viewContext = view.DataContext.EnsureType<DisplayImageViewContext>();

            viewContext.ImageToDisplay = imageToDisplay;

            var args = new WindowInteractionArguments
                           {
                               Content = view,
                               ResizeMode = ResizeMode.CanResizeWithGrip,
                               WindowStartupLocation = Soaf.Presentation.WindowStartupLocation.CenterOwner,
                               CanClose = true,
                               Header = "Image Viewer"
                           };

            if (showModal)
            {
                InteractionManager.Current.ShowModal(args);
            }
            else
            {
                args.IsModal = false;
                InteractionManager.Current.Show(args);
            }

        }
    }

    public class CardScannerReturnArguments
    {
        public int? PatientId { get; set; }
        public IEnumerable<CardScannerDataModel> ScannedCardData { get; set; }
    }
}