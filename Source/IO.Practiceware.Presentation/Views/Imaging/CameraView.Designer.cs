﻿
namespace IO.Practiceware.Presentation.Views.Imaging
{
    partial class CameraView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.TakePictureButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.VideoPictureBox = new System.Windows.Forms.PictureBox();
            this.PreviewPictureBox = new System.Windows.Forms.PictureBox();
            this.AcceptButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.PreviewPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
            this.CancelButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.VideoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviewPictureBox)).BeginInit();
            this.PreviewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TakePictureButton
            // 
            this.TakePictureButton.Location = new System.Drawing.Point(20, 280);
            this.TakePictureButton.Name = "TakePictureButton";
            this.TakePictureButton.Size = new System.Drawing.Size(320, 60);
            this.TakePictureButton.TabIndex = 0;
            this.TakePictureButton.Text = "Take a Picture";
            // 
            // VideoPictureBox
            // 
            this.VideoPictureBox.Location = new System.Drawing.Point(20, 20);
            this.VideoPictureBox.Name = "VideoPictureBox";
            this.VideoPictureBox.Size = new System.Drawing.Size(320, 240);
            this.VideoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.VideoPictureBox.TabIndex = 1;
            this.VideoPictureBox.TabStop = false;
            // 
            // PreviewPictureBox
            // 
            this.PreviewPictureBox.Location = new System.Drawing.Point(0, 0);
            this.PreviewPictureBox.Name = "PreviewPictureBox";
            this.PreviewPictureBox.Size = new System.Drawing.Size(320, 240);
            this.PreviewPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PreviewPictureBox.TabIndex = 2;
            this.PreviewPictureBox.TabStop = false;
            // 
            // AcceptButton
            // 
            this.AcceptButton.Location = new System.Drawing.Point(171, 260);
            this.AcceptButton.Name = "AcceptButton";
            this.AcceptButton.Size = new System.Drawing.Size(149, 60);
            this.AcceptButton.TabIndex = 3;
            this.AcceptButton.Text = "Save";
            // 
            // PreviewPanel
            // 
            this.PreviewPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PreviewPanel.Controls.Add(this.CancelButton);
            this.PreviewPanel.Controls.Add(this.PreviewPictureBox);
            this.PreviewPanel.Controls.Add(this.AcceptButton);
            this.PreviewPanel.Location = new System.Drawing.Point(20, 20);
            this.PreviewPanel.Margin = new System.Windows.Forms.Padding(0);
            this.PreviewPanel.Name = "PreviewPanel";
            this.PreviewPanel.Size = new System.Drawing.Size(320, 320);
            this.PreviewPanel.TabIndex = 4;
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(0, 260);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(149, 60);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Take Again";
            // 
            // CameraView
            // 
            this.Controls.Add(this.PreviewPanel);
            this.Controls.Add(this.VideoPictureBox);
            this.Controls.Add(this.TakePictureButton);
            this.Name = "CameraView";
            this.Size = new System.Drawing.Size(360, 360);
            ((System.ComponentModel.ISupportInitialize)(this.VideoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviewPictureBox)).EndInit();
            this.PreviewPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal Soaf.Presentation.Controls.WindowsForms.Button TakePictureButton;
        internal System.Windows.Forms.PictureBox VideoPictureBox;
        internal System.Windows.Forms.PictureBox PreviewPictureBox;
        internal Soaf.Presentation.Controls.WindowsForms.Button AcceptButton;
        internal Soaf.Presentation.Controls.WindowsForms.Panel PreviewPanel;

        internal Soaf.Presentation.Controls.WindowsForms.Button CancelButton;
    }

}
