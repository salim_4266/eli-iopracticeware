﻿using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    partial class ScanView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {

        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ScanButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.PageSizeListLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.DPIPickerLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ColorSettingListLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.ScannerList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.CancelButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.OptionsBox = new IO.Practiceware.Presentation.Views.Common.Controls.GroupBox();
            this.ColorSettingList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.PageSizeList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.DPIList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
            this.DuplexModeCheckButton = new Soaf.Presentation.Controls.WindowsForms.ToggleButton();
            this.ResetOptionsButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.DPILabel = new Soaf.Presentation.Controls.WindowsForms.Label();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.OptionsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ScanButton
            // 
            this.ScanButton.Location = new System.Drawing.Point(15, 12);
            this.ScanButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Size = new System.Drawing.Size(107, 82);
            this.ScanButton.TabIndex = 0;
            this.ScanButton.Text = "Scan";
            // 
            // PageSizeListLabel
            // 
            this.PageSizeListLabel.Location = new System.Drawing.Point(13, 94);
            this.PageSizeListLabel.Name = "PageSizeListLabel";
            this.PageSizeListLabel.Size = new System.Drawing.Size(99, 27);
            this.PageSizeListLabel.TabIndex = 20;
            this.PageSizeListLabel.Text = "Page size:";
            // 
            // DPIPickerLabel
            // 
            this.DPIPickerLabel.Location = new System.Drawing.Point(11, 32);
            this.DPIPickerLabel.Name = "DPIPickerLabel";
            this.DPIPickerLabel.Size = new System.Drawing.Size(150, 29);
            this.DPIPickerLabel.TabIndex = 16;
            this.DPIPickerLabel.Text = "Scan  quality:";
            // 
            // ColorSettingListLabel
            // 
            this.ColorSettingListLabel.Location = new System.Drawing.Point(179, 94);
            this.ColorSettingListLabel.Name = "ColorSettingListLabel";
            this.ColorSettingListLabel.Size = new System.Drawing.Size(132, 27);
            this.ColorSettingListLabel.TabIndex = 13;
            this.ColorSettingListLabel.Text = "Color setting:";
            // 
            // SplitContainer
            // 
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.IsSplitterFixed = true;
            this.SplitContainer.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SplitContainer.Name = "SplitContainer";
            this.SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.ScannerList);
            this.SplitContainer.Panel1.Controls.Add(this.label1);
            this.SplitContainer.Panel1.Controls.Add(this.CancelButton);
            this.SplitContainer.Panel1.Controls.Add(this.ScanButton);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.OptionsBox);
            this.SplitContainer.Size = new System.Drawing.Size(496, 370);
            this.SplitContainer.SplitterDistance = 111;
            this.SplitContainer.SplitterWidth = 3;
            this.SplitContainer.TabIndex = 21;
            // 
            // ScannerList
            // 
            this.ScannerList.DisplayMemberPath = "";
            this.ScannerList.Location = new System.Drawing.Point(128, 64);
            this.ScannerList.Name = "ScannerList";
            this.ScannerList.SelectedIndex = -1;
            this.ScannerList.SelectedValuePath = "";
            this.ScannerList.Size = new System.Drawing.Size(193, 30);
            this.ScannerList.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(128, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 29);
            this.label1.TabIndex = 30;
            this.label1.Text = "Scanner";
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(386, 24);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(95, 60);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Cancel";
            // 
            // OptionsBox
            // 
            this.OptionsBox.Controls.Add(this.ColorSettingList);
            this.OptionsBox.Controls.Add(this.PageSizeList);
            this.OptionsBox.Controls.Add(this.DPIList);
            this.OptionsBox.Controls.Add(this.DuplexModeCheckButton);
            this.OptionsBox.Controls.Add(this.ResetOptionsButton);
            this.OptionsBox.Controls.Add(this.DPILabel);
            this.OptionsBox.Controls.Add(this.DPIPickerLabel);
            this.OptionsBox.Controls.Add(this.PageSizeListLabel);
            this.OptionsBox.Controls.Add(this.ColorSettingListLabel);
            this.OptionsBox.Location = new System.Drawing.Point(10, 5);
            this.OptionsBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OptionsBox.Name = "OptionsBox";
            this.OptionsBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OptionsBox.Size = new System.Drawing.Size(471, 240);
            this.OptionsBox.TabIndex = 23;
            this.OptionsBox.TabStop = false;
            this.OptionsBox.Text = "Options";
            // 
            // ColorSettingList
            // 
            this.ColorSettingList.DisplayMemberPath = "";
            this.ColorSettingList.Location = new System.Drawing.Point(179, 116);
            this.ColorSettingList.Name = "ColorSettingList";
            this.ColorSettingList.SelectedIndex = -1;
            this.ColorSettingList.SelectedValuePath = "";
            this.ColorSettingList.Size = new System.Drawing.Size(132, 31);
            this.ColorSettingList.TabIndex = 29;
            // 
            // PageSizeList
            // 
            this.PageSizeList.DisplayMemberPath = "";
            this.PageSizeList.Location = new System.Drawing.Point(11, 117);
            this.PageSizeList.Name = "PageSizeList";
            this.PageSizeList.SelectedIndex = -1;
            this.PageSizeList.SelectedValuePath = "";
            this.PageSizeList.Size = new System.Drawing.Size(135, 30);
            this.PageSizeList.TabIndex = 28;
            // 
            // DPIList
            // 
            this.DPIList.DisplayMemberPath = "";
            this.DPIList.Location = new System.Drawing.Point(12, 57);
            this.DPIList.Name = "DPIList";
            this.DPIList.SelectedIndex = -1;
            this.DPIList.SelectedValuePath = "";
            this.DPIList.Size = new System.Drawing.Size(75, 33);
            this.DPIList.TabIndex = 25;
            // 
            // DuplexModeCheckButton
            // 
            this.DuplexModeCheckButton.Location = new System.Drawing.Point(363, 55);
            this.DuplexModeCheckButton.Name = "DuplexModeCheckButton";
            this.DuplexModeCheckButton.Size = new System.Drawing.Size(90, 35);
            this.DuplexModeCheckButton.TabIndex = 24;
            this.DuplexModeCheckButton.Text = "Duplex Mode";
            // 
            // ResetOptionsButton
            // 
            this.ResetOptionsButton.Location = new System.Drawing.Point(358, 159);
            this.ResetOptionsButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ResetOptionsButton.Name = "ResetOptionsButton";
            this.ResetOptionsButton.Size = new System.Drawing.Size(95, 59);
            this.ResetOptionsButton.TabIndex = 2;
            this.ResetOptionsButton.Text = "Reset options";
            // 
            // DPILabel
            // 
            this.DPILabel.Location = new System.Drawing.Point(100, 60);
            this.DPILabel.Name = "DPILabel";
            this.DPILabel.Size = new System.Drawing.Size(46, 33);
            this.DPILabel.TabIndex = 22;
            this.DPILabel.Text = "DPI";
            // 
            // ToolTip
            // 
            this.ToolTip.IsBalloon = true;
            // 
            // ScanView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.SplitContainer);
            this.Name = "ScanView";
            this.Size = new System.Drawing.Size(496, 370);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            this.OptionsBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal Soaf.Presentation.Controls.WindowsForms.Button ScanButton;
        internal Soaf.Presentation.Controls.WindowsForms.Label PageSizeListLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label DPIPickerLabel;
        internal Soaf.Presentation.Controls.WindowsForms.Label ColorSettingListLabel;
        internal System.Windows.Forms.SplitContainer SplitContainer;
        internal System.Windows.Forms.ToolTip ToolTip;
        internal Soaf.Presentation.Controls.WindowsForms.Label DPILabel;
        internal Soaf.Presentation.Controls.WindowsForms.Button ResetOptionsButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button CancelButton;
        internal Soaf.Presentation.Controls.WindowsForms.ToggleButton DuplexModeCheckButton;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox DPIList;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox ColorSettingList;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox PageSizeList;
        private GroupBox OptionsBox;
        internal Soaf.Presentation.Controls.WindowsForms.ComboBox ScannerList;
        internal Soaf.Presentation.Controls.WindowsForms.Label label1;
    }

}
