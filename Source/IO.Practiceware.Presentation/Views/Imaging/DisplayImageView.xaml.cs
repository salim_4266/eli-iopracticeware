﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    /// <summary>
    /// Interaction logic for ViewImage.xaml
    /// </summary>
    public partial class DisplayImageView
    {
        public DisplayImageView()
        {
            InitializeComponent();
        }
    }


    [SupportsNotifyPropertyChanged(true, false)]  
    public class DisplayImageViewContext : IViewContext
    {
        public DisplayImageViewContext()
        {
            Load = Command.Create(ExecuteLoad);
        }

        private void ExecuteLoad()
        {
            
        }

        public IInteractionContext InteractionContext
        {
            get;
            set;
        }

        public ICommand Load { get; set; }
        
        public virtual byte[] ImageToDisplay { get; set; }
    }
}
