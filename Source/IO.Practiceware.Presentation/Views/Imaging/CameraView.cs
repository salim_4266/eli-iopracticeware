﻿using System.IO;
using Soaf.Presentation;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using Touchless.Vision.Camera;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    public partial class CameraView
    {
        public byte[] ImageData { get; set; }

        private CameraFrameSource _frameSource;
        private Bitmap _latestFrame;

        public CameraView()
        {
            InitializeComponent();
            Load += CameraViewLoad;
            Disposed += CameraViewDisposed;
            TakePictureButton.Click += TakePictureButtonClick;
            CancelButton.Click += CancelButtonClick;
            AcceptButton.Click += AcceptButtonClick;
        }

        private void CameraViewLoad(Object sender, EventArgs e)
        {
            if (CameraService.DefaultCamera == null)
            {
                InteractionManager.Current.Alert("No video capture devices found");
                return;
            }
            StartCapturing();
        }

        private void CameraViewDisposed(object sender, EventArgs e)
        {
            ThrashOldCamera();
        }

        private void StartCapturing()
        {
            try
            {
                Camera c = CameraService.DefaultCamera;
                SetFrameSource(new CameraFrameSource(c));
                _frameSource.Camera.CaptureWidth = 320;
                _frameSource.Camera.CaptureHeight = 240;
                _frameSource.Camera.Fps = 20;
                _frameSource.NewFrame += OnImageCaptured;

                _frameSource.StartFrameCapture();
                PreviewPanel.SendToBack();
            }
            catch (Exception ex)
            {
                InteractionManager.Current.Alert(ex.Message);
            }
        }

        private void SetFrameSource(CameraFrameSource cameraFrameSource)
        {
            if (_frameSource != null && _frameSource == cameraFrameSource) return;
            _frameSource = cameraFrameSource;
        }

        private void OnImageCaptured(Touchless.Vision.Contracts.IFrameSource frameSource, Touchless.Vision.Contracts.Frame frame, double fps)
        {
            _latestFrame = frame.Image;
            VideoPictureBox.Image = _latestFrame;
        }

        private void ThrashOldCamera()
        {
            if (_frameSource != null)
            {
                _frameSource.NewFrame -= OnImageCaptured;
                _frameSource.Camera.Dispose();
                SetFrameSource(null);
            }
        }

        private void TakePictureButtonClick(Object sender, EventArgs e)
        {
            ThrashOldCamera();
            PreviewPictureBox.Image = _latestFrame;
            PreviewPanel.BringToFront();
        }

        private void CancelButtonClick(Object sender, EventArgs e)
        {
            StartCapturing();
            PreviewPictureBox.Image = null;
        }

        private void AcceptButtonClick(Object sender, EventArgs e)
        {
            ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/jpeg");
            var encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
            using (var ms = new MemoryStream())
            {
                _latestFrame.Save(ms, imageCodecInfo, encoderParameters);
                ImageData = ms.ToArray();
            }
            InteractionContext.Complete(true);
        }

        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
    }
}