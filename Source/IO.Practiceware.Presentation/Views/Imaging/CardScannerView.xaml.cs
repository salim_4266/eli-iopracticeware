﻿using IO.Practiceware.Presentation.ViewServices.Imaging;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Imaging
{
    /// <summary>
    ///   Interaction logic for CardScannerView.xaml
    /// </summary>
    public partial class CardScannerView
    {
        public CardScannerView()
        {
            InitializeComponent();
        }
    }

    public class CardScannerViewContext : IViewContext
    {
        private readonly ICardScannerViewService _cardScannerViewService;

        public CardScannerViewContext(ICardScannerViewService cardScannerViewService)
        {
            _cardScannerViewService = cardScannerViewService;

            CardTypes = Enums.GetValues<CardType>().ToArray();

            ProcessedData = new ObservableCollection<CardScannerDataModel>();

            DeleteProcessedDataItem = Command.Create<CardScannerDataModel>(ExecuteDeleteProcessedDataItem);

            Save = Command.Create(ExecuteSave, () => ScannedImageData != null && ProcessedData != null && ProcessedData.Count > 0).Async(() => InteractionContext, false, "Saving...");

            Cancel = Command.Create(() => InteractionContext.Complete());
            ValidationMessage = "";
        }

        /// <summary>
        /// Gets or sets the patient id for which the data will be saved.
        /// </summary>
        /// <value> The patient id. </value>
        public virtual int? PatientId { get; set; }

        /// <summary>
        ///   Gets the card types that can be scanned.
        /// </summary>
        /// <value> The card types. </value>
        public virtual IEnumerable<CardType> CardTypes { get; private set; }

        /// <summary>
        ///   Gets or sets the type of the selected card to scan.
        /// </summary>
        /// <value> The type of the selected card. </value>
        public virtual CardType SelectedCardType { get; set; }

        /// <summary>
        ///   Saves the processed data.
        /// </summary>
        /// <value> The save. </value>
        public ICommand Save { get; private set; }

        /// <summary>
        ///   Gets the cancel.
        /// </summary>
        /// <value> The cancel. </value>
        public ICommand Cancel { get; private set; }

        /// <summary>
        ///   Gets the processed data from the scanned card.
        /// </summary>
        /// <value> The processed data. </value>
        public virtual ICollection<CardScannerDataModel> ProcessedData { get; set; }

        [DispatcherThread]
        public virtual byte[] ScannedImageData { get; set; }

        public ICommand DeleteProcessedDataItem { get; private set; }

        [DispatcherThread]
        [DependsOn("ValidationMessage")]
        public virtual bool IsDataValid { get { return string.IsNullOrEmpty(ValidationMessage); } }

        [DispatcherThread]
        public virtual string ValidationMessage { get; set; }
        
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteDeleteProcessedDataItem(CardScannerDataModel item)
        {
            ProcessedData.Remove(item);
        }

        private void ExecuteSave()
        {
            var response = _cardScannerViewService.ValidateProcessedData(ScannedImageData, SelectedCardType, ProcessedData, PatientId);
            if (response.ValidationMessage.IsNullOrEmpty()) InteractionContext.Complete(true);

            PatientId = response.PatientId;
            ValidationMessage = response.ValidationMessage;
        }
    }
}