﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.ViewServices.RescheduleAppointment;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using AppointmentViewModel = IO.Practiceware.Presentation.ViewModels.RescheduleAppointment.AppointmentViewModel;

namespace IO.Practiceware.Presentation.Views.RescheduleAppointment
{
    /// <summary>
    /// Interaction logic for RescheduleAppointment.xaml
    /// </summary>
    public partial class RescheduleAppointmentView 
    {
        public RescheduleAppointmentView()
        {
            InitializeComponent();
        }
    }

    public class RescheduleAppointmentViewContext : IViewContext
    {

        public IInteractionContext InteractionContext { get; set; }

        public RescheduleAppointmentLoadArguments LoadArguments { get; set; }
        private ObservableCollection<EncounterStatusChangeReasonViewModel> _listOfReason;
        private readonly IRescheduleAppointmentViewService _rescheduleAppointmentViewService;

        public RescheduleAppointmentViewContext(IRescheduleAppointmentViewService rescheduleAppointmentViewService)
        {
            _rescheduleAppointmentViewService = rescheduleAppointmentViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ChangeRescheduleType = Command.Create(ExecuteChangeRescheduleType);
            Close = Command.Create(ExecuteClose);
            RescheduleAppointments = Command.Create(ExecuteRescheduleAppointments, () => SelectedRescheduleType != null).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            LoadArguments.Validate(true);
           
            SelectedBlock = LoadArguments.SelectedBlock;
            
            // load appointment details
            var appointments = _rescheduleAppointmentViewService.LoadAppointments(LoadArguments.AppointmentIds);

            Appointments = appointments.ToExtendedObservableCollection();
                       
            _listOfReason = _rescheduleAppointmentViewService.LoadEncounterStatusChangeReasons().ToExtendedObservableCollection();
            ListOfRescheduledTypes = _rescheduleAppointmentViewService.LoadAppointmentRescheduleTypes();
            SelectedRescheduleType = ListOfRescheduledTypes.FirstOrDefault();
        }

        private void ExecuteChangeRescheduleType()
        {
            ListOfSelectedTypeReason = _listOfReason.Where(r => r.EncounterStatusId == SelectedRescheduleType.Id).ToExtendedObservableCollection();
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteRescheduleAppointments()
        {
            _rescheduleAppointmentViewService.RescheduleAppointments(Appointments.Select(a => a.Id).ToList(), SelectedBlock, SelectedRescheduleType.Id, SelectedTypeReason, Comment);
            AppointmentsSuccessfullyRescheduled = true;
            System.Windows.Application.Current.Dispatcher.Invoke(() => InteractionContext.Complete(false));
        }

        [DispatcherThread]
        public virtual ObservableCollection<AppointmentViewModel> Appointments { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ListOfRescheduledTypes { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedRescheduleType { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<EncounterStatusChangeReasonViewModel> ListOfSelectedTypeReason { get; set; }

        [DispatcherThread]
        public virtual EncounterStatusChangeReasonViewModel SelectedTypeReason { get; set; }

        [DispatcherThread]
        public virtual string Comment { get; set; }

        [DependsOn("Appointments")]
        public virtual bool HasOnlySingleAppointment
        {
            get { return Appointments != null && Appointments.Count == 1; }
        }

        [DependsOn("Appointments")]
        public virtual String SingleAppointmentDetail
        {
            get
            {
                if (Appointments == null) return null;
                var appointment = Appointments.FirstOrDefault();
                if (appointment == null) return null;
                var date = appointment.StartDateTime.ToString("MM/dd/yyyy");
                var time = appointment.StartDateTime.ToString("hh:mmtt");
                var detail = String.Format("For {0} on {1} at {2}", appointment.PatientName, date, time);
                return detail;
            }
        }

        public bool AppointmentsSuccessfullyRescheduled { get; set; }

        public RescheduleAppointmentViewModel SelectedBlock { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        public ICommand ChangeRescheduleType { get; protected set; }

        public ICommand ReasonChanged { get; protected set; }

        public ICommand Close { get; protected set; }

        public ICommand RescheduleAppointments { get; protected set; }
    }
}
