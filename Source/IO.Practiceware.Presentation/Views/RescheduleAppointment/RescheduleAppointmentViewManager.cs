﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Windows;
using WindowStartupLocation = Soaf.Presentation.WindowStartupLocation;

namespace IO.Practiceware.Presentation.Views.RescheduleAppointment
{
    public class RescheduleAppointmentViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public RescheduleAppointmentViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public ReschedulAppointmentReturnArguments ShowRescheduleAppointment(RescheduleAppointmentLoadArguments loadArgs, bool isModal = false, WindowStartupLocation startUpLocation = WindowStartupLocation.CenterScreen)
        {
            loadArgs.EnsureNotDefault("RescheduleAppointmentLoadArguments must be set.");

            var returnArgs = new ReschedulAppointmentReturnArguments();
            bool canShowRescheduleAppointment = PermissionId.RescheduleAppointment.EnsurePermission();
            if (!canShowRescheduleAppointment) { return returnArgs; }

            var view = new RescheduleAppointmentView();

            var dataContext = view.DataContext.EnsureType<RescheduleAppointmentViewContext>(false, "The RescheduleAppointmentViewContext's DataContext is not of type {0}".FormatWith(typeof(RescheduleAppointmentViewContext).Name));
            dataContext.LoadArguments = loadArgs;

            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  IsModal = isModal,
                                                  WindowStartupLocation = startUpLocation,
                                                  WindowState = WindowState.Normal,
                                                  ResizeMode = ResizeMode.CanResizeWithGrip
                                              });

            returnArgs.AppointmentsRescheduledSuccessfully = dataContext.AppointmentsSuccessfullyRescheduled;
            return returnArgs;
        }
    }

    public class RescheduleAppointmentLoadArguments : IViewModel
    {
        [NotEmpty]
        public IEnumerable<int> AppointmentIds { get; set; }        
        public RescheduleAppointmentViewModel SelectedBlock { get; set; }       
    }

    public class ReschedulAppointmentReturnArguments
    {
        public bool AppointmentsRescheduledSuccessfully { get; set; }
    }
}
