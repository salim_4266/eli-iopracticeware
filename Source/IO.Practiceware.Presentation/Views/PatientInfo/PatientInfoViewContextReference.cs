﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    public class PatientInfoViewContextReference : DataContextReference
    {
        public new PatientInfoViewContext DataContext
        {
            get { return (PatientInfoViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
