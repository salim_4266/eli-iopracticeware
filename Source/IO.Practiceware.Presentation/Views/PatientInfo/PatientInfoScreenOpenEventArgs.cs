﻿using System;

namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    public class PatientInfoScreenOpenEventArgs : EventArgs
    {
        public PatientInfoScreenOpenEventArgs(int patientId, string screenToOpen, object parameter = null)
        {
            PatientId = patientId;
            ScreenToOpen = screenToOpen;

            Parameter = parameter;
        }

        public int PatientId { get; private set; }

        public string ScreenToOpen { get; private set; }

        /// <summary>
        /// Universal Parameter based on the screen to open
        /// </summary>
        public object Parameter { get; private set;}
    }
}
