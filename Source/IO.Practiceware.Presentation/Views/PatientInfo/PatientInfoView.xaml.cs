﻿using System.ComponentModel;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.Views.Imaging;
using IO.Practiceware.Presentation.Views.ClinicalInfo;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientFinancialSummary;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Services.Imaging;
using Soaf;
using IO.Practiceware.Utilities;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Soaf.Threading;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    /// <summary>
    ///   Interaction logic for PatientInfo.xaml
    /// </summary>
    public partial class PatientInfoView
    {
        public PatientInfoView()
        {
            InitializeComponent();
        }
    }

    public class PatientInfoViewContext : IViewContext, IPatientInfoViewContext
    {
        public readonly Dictionary<PatientInfoTab, IPatientInfoViewContext> PatientTabs;

        readonly List<ViewContextLoadViewModel> _viewContextModels;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly IPatientInfoViewService _patientInfoViewService;
        readonly Func<ViewContextLoadViewModel> _createViewContextLoadViewModel;
        private readonly Func<PatientInfoViewModel> _createPatientInfoViewModel;
        private readonly ImagingViewManager _imagingViewManager;
        private readonly IImageService _imageService;
        private readonly IPostOperativePeriodService _postOperativePeriodService;

        public PatientInfoViewContext(PatientAppointmentsViewContext patientAppointmentsViewContext,
                                      PatientInsuranceViewContext patientInsuranceViewContext,
                                      EditProblemListViewContext editProblemListViewContext,
                                      ReconcileMedicationAllergiesViewContext reconcileMedicationAllergiesViewContext,
                                      ReconcilePatientProblemsViewContext reconcilePatientProblemsViewContext,
                                      PatientCommunicationPreferencesViewContext patientCommunicationPreferencesViewContext,
                                      PatientDemographicsViewContext patientDemographicsViewContext,
                                      PatientFinancialSummaryViewContext patientFinancialSummaryViewContext,
                                      IPatientInfoViewService patientInfoViewService,
                                      Func<PatientInfoViewManager> createPatientInfoViewManager,
                                      Func<ViewContextLoadViewModel> createViewContextLoadViewModel,
                                      Func<PatientInfoViewModel> createPatientInfoViewModel,
                                      IPostOperativePeriodService postOperativePeriodService,
                                      ImagingViewManager imagingViewManager, IImageService imageService
            )
        {
            PatientTabs = new Dictionary<PatientInfoTab, IPatientInfoViewContext>
                              {
                                  {PatientInfoTab.Appointments, patientAppointmentsViewContext},
                                  {PatientInfoTab.Insurance, patientInsuranceViewContext},
                                  {PatientInfoTab.Communications, patientCommunicationPreferencesViewContext},
                                  {PatientInfoTab.Demographics, patientDemographicsViewContext},
                                  {PatientInfoTab.FinancialData, patientFinancialSummaryViewContext},
                                  {PatientInfoTab.EditProblemList, editProblemListViewContext},
                                  {PatientInfoTab.ReconcilePatientProblems, reconcilePatientProblemsViewContext},
                                  {PatientInfoTab.ReconcileMedicationAllergies, reconcileMedicationAllergiesViewContext}
                              };
            
            _patientInfoViewService = patientInfoViewService;
            _createViewContextLoadViewModel = createViewContextLoadViewModel;
            _createPatientInfoViewModel = createPatientInfoViewModel;
            _imagingViewManager = imagingViewManager;
            _imageService = imageService;
            _patientInfoViewManager = createPatientInfoViewManager();
            _viewContextModels = new List<ViewContextLoadViewModel>();
            _postOperativePeriodService = postOperativePeriodService;
            
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            Closing = Command.Create<Action>(ExecuteClosing);

            TakePatientPhoto = Command.Create(ExecuteTakePatientPhoto);
            DeletePatientPhoto = Command.Create(ExecuteDeletePatientPhoto, () => PatientInfoViewModel != null && PatientInfoViewModel.Photo != null).Async(() => InteractionContext);
        }

        /// <summary>
        ///   The arguments that should be set before initializing/loading this screen
        /// </summary>
        public PatientInfoLoadArguments LoadArguments { get; set; }

        /// <summary>
        ///   Command to close the form
        /// </summary>
        public ICommand Close { get; protected set; }

        public ICommand Closing { get; protected set; }

        /// <summary>
        /// Refreshes which common view features are available for every tab
        /// </summary>
        protected ICommand RefreshCommonViewFeaturesInformation { get; set; }

        public virtual ICommand TakePatientPhoto { get; protected set; }

        public virtual ICommand DeletePatientPhoto { get; protected set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel AppointmentsViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel InsuranceViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel CommunicationsViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel ProblemsListViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel ReconcileProblemsViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel ReconcileAllergiesViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel DemographicsViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel FinancialDataViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel ClinicalDataViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel NotesViewContextLoadViewModel { get; set; }

        [DispatcherThread]
        public virtual ViewContextLoadViewModel DocumentsViewContextLoadViewModel { get; set; }

        /// <summary>
        /// Model to represent the patient's post operative period (if one exists)
        /// </summary>
        [DispatcherThread]
        public virtual PostOperativeViewModel PatientPostOp { get; set; }

        [DispatcherThread]
        [DependsOn("PatientPostOp")]
        public virtual bool ShowPostOp
        {
            get { return PatientPostOp != null && PatientPostOp.CurrentlyInPostOperativePeriod; }
        }
        
        #region IPatientInfoViewContext Members

        /// <summary>
        ///   Contains the patient info for display.
        /// </summary>
        [DispatcherThread]
        public virtual PatientInfoViewModel PatientInfoViewModel { get; set; }

        [DispatcherThread]
        [DependsOn("PatientInfoViewModel", "PatientInfoViewModel.IsClinical")]
        public virtual bool ShowDemographicsOnly
        {
            get { return PatientInfoViewModel == null || PatientInfoViewModel.PatientId == null || !PatientInfoViewModel.IsClinical; }
        }

        public ICommand Load { get; set; }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
            PatientInfoViewModel.CastTo<IEditableObject>().BeginEdit();

            // pass in the patient info data to each of the tabbed views
            PatientTabs.ForEachWithSinglePropertyChangedNotification(x =>
                                    {
                                        x.Value.SetPatientInfo(PatientInfoViewModel, Command.Create<int>(ExecuteLoadPatientInfo).Async());
                                        WindowClosing -= x.Value.OnClosing;
                                        WindowClosing += x.Value.OnClosing;
                                    });
        }

        private delegate PatientInfoClosingReturnArguments PatientInfoWindowClosing();

        private event PatientInfoWindowClosing WindowClosing;

        public PatientInfoClosingReturnArguments OnClosing()
        {
            return null;
        }

        #endregion

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            Task loadPatientTask = null;
            if (LoadArguments.PatientId.HasValue)
            {
                loadPatientTask = Task.Factory.StartNewWithCurrentTransaction(() => ExecuteLoadPatientInfo(LoadArguments.PatientId.Value));
            }
            else
            {
                // can't open any other tabs yet for a brand new patient...other tabs will become enabled upon saving
                LoadArguments.TabToOpen = PatientInfoTab.Demographics;
            }

            var patientInfo = _createPatientInfoViewModel();
            patientInfo.PatientId = LoadArguments.PatientId;
            SetPatientInfo(patientInfo);

            if (patientInfo.PatientId != null)
            {
                Task.Factory.StartNewWithCurrentTransaction(() => PatientPostOp = _postOperativePeriodService.GetLatestPostOperativePeriodByPatientId(patientInfo.PatientId.Value));
            }

            AppointmentsViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.ViewContext = PatientTabs[PatientInfoTab.Appointments];
                    instance.PatientInfoTab = PatientInfoTab.Appointments;
                    instance.LoadCommand = PatientTabs[PatientInfoTab.Appointments].Load;
                });
            _viewContextModels.Add(AppointmentsViewContextLoadViewModel);

            InsuranceViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.ViewContext = PatientTabs[PatientInfoTab.Insurance];
                    instance.PatientInfoTab = PatientInfoTab.Insurance;
                    instance.FireCommandOneTimeOnly = true;
                    instance.SelectedCommand = RefreshCommonViewFeaturesInformation;
                    instance.LoadCommand = PatientTabs[PatientInfoTab.Insurance].Load;
                });
            _viewContextModels.Add(InsuranceViewContextLoadViewModel);

            CommunicationsViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.ViewContext = PatientTabs[PatientInfoTab.Communications];
                    instance.PatientInfoTab = PatientInfoTab.Communications;
                    instance.FireCommandOneTimeOnly = true;
                    instance.LoadCommand = PatientTabs[PatientInfoTab.Communications].Load;
                });
            _viewContextModels.Add(CommunicationsViewContextLoadViewModel);

            DemographicsViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.ViewContext = PatientTabs[PatientInfoTab.Demographics];
                    instance.PatientInfoTab = PatientInfoTab.Demographics;
                    instance.FireCommandOneTimeOnly = true;
                    instance.LoadCommand = PatientTabs[PatientInfoTab.Demographics].Load;
                });
            _viewContextModels.Add(DemographicsViewContextLoadViewModel);

            FinancialDataViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                    instance.ViewContext = PatientTabs[PatientInfoTab.FinancialData];
                    instance.PatientInfoTab = PatientInfoTab.FinancialData;
                    instance.FireCommandOneTimeOnly = true;
                    instance.LoadCommand = PatientTabs[PatientInfoTab.FinancialData].Load;
            });
            _viewContextModels.Add(FinancialDataViewContextLoadViewModel);

            ClinicalDataViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.FireCommandOneTimeOnly = false;
                    instance.PatientInfoTab = PatientInfoTab.ClinicalData;
                    instance.LoadCommand = Command.Create(() => ExecuteOpenLegacyScreen(PatientInfoTab.ClinicalData.ToString()));
               });
            _viewContextModels.Add(ClinicalDataViewContextLoadViewModel);

            NotesViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.FireCommandOneTimeOnly = false;
                    instance.PatientInfoTab = PatientInfoTab.Notes;
                   if (CanShowNotes()) instance.LoadCommand = Command.Create(() => ExecuteOpenLegacyScreen(PatientInfoTab.Notes.ToString()));
                });
            _viewContextModels.Add(NotesViewContextLoadViewModel);

            DocumentsViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.FireCommandOneTimeOnly = false;
                    instance.PatientInfoTab = PatientInfoTab.Documents;
                    instance.LoadCommand = Command.Create(() => ExecuteOpenLegacyScreen(PatientInfoTab.Documents.ToString()));
                });
            _viewContextModels.Add(DocumentsViewContextLoadViewModel);

            ProblemsListViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
                {
                    instance.FireCommandOneTimeOnly = false;
                    instance.PatientInfoTab = PatientInfoTab.EditProblemList;
                    instance.SelectedCommand = RefreshCommonViewFeaturesInformation;
                    instance.ViewContext = PatientTabs[PatientInfoTab.EditProblemList];
                    instance.LoadCommand = PatientTabs[PatientInfoTab.EditProblemList].Load;
                });
            _viewContextModels.Add(ProblemsListViewContextLoadViewModel);

            ReconcileProblemsViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.FireCommandOneTimeOnly = false;
                instance.PatientInfoTab = PatientInfoTab.ReconcilePatientProblems;
                instance.SelectedCommand = RefreshCommonViewFeaturesInformation;
                instance.ViewContext = PatientTabs[PatientInfoTab.ReconcilePatientProblems];
                instance.LoadCommand = PatientTabs[PatientInfoTab.ReconcilePatientProblems].Load;
            });
            _viewContextModels.Add(ReconcileProblemsViewContextLoadViewModel);

            ReconcileAllergiesViewContextLoadViewModel = _createViewContextLoadViewModel().Modify(instance =>
            {
                instance.FireCommandOneTimeOnly = false;
                instance.PatientInfoTab = PatientInfoTab.ReconcileMedicationAllergies;
                instance.SelectedCommand = RefreshCommonViewFeaturesInformation;
                instance.ViewContext = PatientTabs[PatientInfoTab.ReconcileMedicationAllergies];
                instance.LoadCommand = PatientTabs[PatientInfoTab.ReconcileMedicationAllergies].Load;
            });
            _viewContextModels.Add(ReconcileAllergiesViewContextLoadViewModel);

            if (loadPatientTask != null)
            {
                loadPatientTask.Wait();
            }

            _viewContextModels.ToList().ForEach(m => m.AllowSelectionChange = true);

            var viewContextModel = _viewContextModels.Single(vcm => vcm.PatientInfoTab == LoadArguments.TabToOpen);

            viewContextModel.IsSelected = true;
        }

        private static bool CanShowNotes()
        {
            return PermissionId.ViewPatientComments.PrincipalContextHasPermission() && PermissionId.EditPatientComments.PrincipalContextHasPermission();
        }

        private void ExecuteDeletePatientPhoto()
        {
            PatientInfoViewModel.Photo = null;
            // only delete from view service if it's not a new patient...if it is new, demographics will be responsible for saving it
            if (PatientInfoViewModel.PatientId.HasValue)
            {
                var ensureNotDefault = PatientInfoViewModel.PatientId.EnsureNotDefault();
                if (ensureNotDefault != null)
                {
                    _imageService.SavePatientPhoto(ensureNotDefault.Value, null);
                }
            }
        }

        private void ExecuteTakePatientPhoto()
        {
            var data = _imagingViewManager.ShowCameraView();

            if (data != null)
            {
                PatientInfoViewModel.Photo = data;

                // only save to view service if it's not a new patient...if it is new, demographics will be responsible for saving it
                if (PatientInfoViewModel.PatientId.HasValue)
                {
                    Command.Create(() => _imageService.SavePatientPhoto(PatientInfoViewModel.PatientId.Value, data)).Async(() => InteractionContext).Execute();
                }
            }
        }

        private void ExecuteOpenLegacyScreen(string screenToOpen)
        {
            this.Dispatcher().BeginInvoke(() =>
            {
                // always go back to Patient Demographics when done for now, since the user will see the "Under Construction" sign otherwise...
                // must do via BeginInvoke with DispatcherPriority.Background in order to prevent an apparent loop in AvalonDock - if we don't, this method gets called over and over again
                DemographicsViewContextLoadViewModel.IsSelected = true;
                _patientInfoViewManager.ShowLegacyPatientScreen(PatientInfoViewModel.PatientId.GetValueOrDefault(), screenToOpen);
            }, System.Windows.Threading.DispatcherPriority.Background);

        }

        private void ExecuteLoadPatientInfo(int patientId)
        {
            var patientInfo = _patientInfoViewService.LoadPatientInfo(patientId);
            
            patientInfo.Photo = _imageService.GetPatientPhoto(patientId);

            SetPatientInfo(patientInfo);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteClosing(Action cancelClose)
        {
            // check if WindowClosing event is null
            if (WindowClosing != null)
            {
                // get the list of delegates
                var delegateList = WindowClosing.GetInvocationList().Select(d => d.CastTo<PatientInfoWindowClosing>()).ToArray();
                foreach (var closingDelegate in delegateList)
                {
                    // get the tab that corresponds to the current delegate
                    var tab = PatientTabs.SingleOrDefault(x => x.Value.OnClosing == closingDelegate);

                    // get the viewContextModel that corresponds to the tab.                   
                    var viewContextModel = _viewContextModels.SingleOrDefault(vcm => vcm.ViewContext == tab.Value);

                    // if the view context has not yet loaded, do not bother calling it's closing callback method
                    if (viewContextModel != null && !viewContextModel.HasLoaded) continue;

                    // invoke this tab's closing callback method
                    var closingArguments = closingDelegate();

                    // check if the callback method returned some data.  if so, check if the tab is requesting to cancel the closing operation
                    if (closingArguments != null && closingArguments.CancelClose)
                    {

                        // set this tab to be the currently selected one in the Main PatientInfoView.  it's up to the tab itself to handle what happens from there
                        if (viewContextModel != null)
                        {
                            viewContextModel.IsSelected = true;
                        }

                        // call the 'ConfirmCancelClose' callback method if there is one and if the 'Confirm' returns false, 
                        // then we assume that tab decided to proceed with closing after all
                        if (closingArguments.ConfirmCancelClose != null && !closingArguments.ConfirmCancelClose())
                        {
                            continue;
                        }

                        cancelClose();
                        // we only want to show one tab at a time
                        return;
                    }
                }
            }

        }


    }

    public class ViewContextLoadViewModel : IViewModel
    {
        private bool _hasFired;
        private bool _isSelected;

        /// <summary>
        ///   The command to execute to load view context
        /// </summary>
        public ICommand LoadCommand { get; set; }

        /// <summary>
        ///  The command to execute when the IsSelected property is set to true
        /// </summary>
        public ICommand SelectedCommand { get; set; }

        /// <summary>
        ///   The DataContext associated with this instance
        /// </summary>
        public IPatientInfoViewContext ViewContext { get; set; }

        /// <summary>
        /// The Patient Info Tab assoiated with this context
        /// </summary>
        public PatientInfoTab? PatientInfoTab { get; set; } 

        /// <summary>
        ///   A flag that when set to true checks whether or not the LoadCommand has fired already, and if so do not allow any further executions.
        /// </summary>
        public bool FireCommandOneTimeOnly { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the ViewContext represented by this viewModel has loaded
        /// </summary>
        public bool HasLoaded
        {
            get { return _hasFired; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsSelected can be changed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowSelectionChange { get; set; }

        [DispatcherThread]
        public virtual bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (!AllowSelectionChange) return;

                _isSelected = value;

                // Make a decision whether load command should be called
                if ((_isSelected && FireCommandOneTimeOnly && !_hasFired) || (_isSelected && !FireCommandOneTimeOnly))
                {
                    if (LoadCommand != null)
                    {
                        LoadCommand.Execute();
                    }

                    _hasFired = true;
                }

                // Execute Selected command
                if (_isSelected && SelectedCommand != null)
                {
                    SelectedCommand.Execute(this);
                }
            }
        }
    }
}