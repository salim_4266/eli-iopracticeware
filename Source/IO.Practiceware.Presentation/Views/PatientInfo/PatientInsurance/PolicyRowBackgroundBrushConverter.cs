﻿using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientInsurance
{
    public class PolicyRowBackgroundBrushConverter : IValueConverter
    {    
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue)
            {
                var status = (PolicyRowStatus)value;

                if( status == PolicyRowStatus.Current)
                {
                    var bc = new BrushConverter();
                    return bc.ConvertFrom("#FFEBEBEB");
                }

                if (status == PolicyRowStatus.Ended)
                {
                    var bc = new BrushConverter();
                    return bc.ConvertFrom("#FFFFC5C5");
                }

                if (status == PolicyRowStatus.Deleted)
                {
                    var bc = new BrushConverter();
                    return bc.ConvertFrom("#FFCDCDCD");
                }
            }

            return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    
}
