﻿using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Behaviors;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Imaging;
using IO.Practiceware.Presentation.Views.InsurancePlansSetup;
using IO.Practiceware.Presentation.Views.InsurancePolicy;
using IO.Practiceware.Presentation.Views.Referrals;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientInsurance;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientInsurance
{
    /// <summary>
    /// Interaction logic for PatientInsurance.xaml
    /// </summary>
    public partial class PatientInsuranceView
    {
        public PatientInsuranceView()
        {
            InitializeComponent();
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    public class PatientInsuranceViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IPatientInsuranceViewService _patientInsuranceViewService;
        private readonly IImageService _imageService;
        private readonly Func<AddInsurancePolicyWizardManager> _createInsurancePolicyWizardManager;
        private readonly Func<PatientInsuranceViewViewModel> _createPatientInsuranceViewViewModel;
        private readonly ReferralsViewManager _referralsViewManager;
        private readonly InsurancePolicyViewManager _insurancePolicyViewManager;
        private readonly ImagingViewManager _imagingViewManager;

        public PatientInsuranceViewContext(
            IInteractionManager interactionManager,
            IPatientInsuranceViewService patientInsuranceViewService,
            Func<ReferralsViewManager> createReferralsViewManager,
            IImageService imageService, ICommonViewFeatureExchange exchange,
            Func<InsurancePolicyViewManager> createInsurancePolicyViewManager,
            Func<AddInsurancePolicyWizardManager> createInsurancePolicyWizardManager,
            Func<ImagingViewManager> createImagingViewManager,
            Func<PatientInsuranceViewViewModel> createPatientInsuranceViewViewModel)
        {
            _interactionManager = interactionManager;
            _patientInsuranceViewService = patientInsuranceViewService;
            _imageService = imageService;
            _createInsurancePolicyWizardManager = createInsurancePolicyWizardManager;
            _createPatientInsuranceViewViewModel = createPatientInsuranceViewViewModel;
            _imagingViewManager = createImagingViewManager();
            _referralsViewManager = createReferralsViewManager();
            _insurancePolicyViewManager = createInsurancePolicyViewManager();

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ViewReferrals = Command.Create(ExecuteViewReferrals);
            ReorderRows = Command.Create<RowReorderEventArgs>(ExecuteReorderRows);
            RowEditEnded = Command.Create<RowEditEndedArgs>(ExecuteRowEditEnded).Async(() => InteractionContext);
            CellValidating = Command.Create<CellValidatingCommandArgs>(ExecuteCellValidating);
            OpenEditPolicyHolderWindow = Command.Create<PolicyHolderViewModel>(ExecuteOpenEditPolicyHolderWindow);
            OpenInsurersAndPlansWindow = Command.Create<InsurancePlanViewModel>(ExecuteOpenInsurersAndPlansWindow);
            OpenEditPolicyDetailsWindow = Command.Create<PolicyViewModel>(ExecuteOpenEditPolicyDetailsWindow);
            LoadPatientPhoto = Command.Create<PolicyHolderViewModel>(ExecuteLoadPatientPhoto).Async();
            DeletePolicy = Command.Create<PolicyViewModel>(ExecuteDeletePolicy).Async(() => InteractionContext);
            ReactivatePolicy = Command.Create<PolicyViewModel>(ExecuteReactivatePolicy).Async(() => InteractionContext);
            ConfirmEndPolicy = Command.Create(ExecuteConfirmEndPolicy).Async(() => InteractionContext);
            ConfirmEndPolicyForPatientOnly = Command.Create(ExecuteConfirmEndPolicyForPatientOnly);
            ConfirmEndPolicyForAllPatients = Command.Create(ExecuteConfirmEndPolicyForAllPatients);
            ConfirmCancelEndPolicy = Command.Create(ExecuteConfirmCancelEndPolicy);
            CancelConfirmChangeInsuranceType = Command.Create(ExecuteCancelConfirmChangeInsuranceType);
            Close = Command.Create<IInteractionContext>(ExecuteClose);
            ConfirmExitWithoutSavingChanges = Command.Create(ExecuteConfirmExitWithoutSavingChanges);

            ShowScanWizard = Command.Create<PolicyViewModel>(ExecuteShowScanWizard);
            DeleteImage = Command.Create<AttachedImage>(ExecuteDeleteImage).Async(() => InteractionContext);
            ShowImageInPdfViewer = Command.Create<AttachedImage>(ExecuteShowImageInPdfViewer);
            MedicareSecondaryReasonChanged = Command.Create(ExecuteMedicareSecondaryReasonChanged);

            AddMajorMedicalPolicy = Command.Create<InsuranceType>(x => ExecuteAddPolicy(InsuranceType.MajorMedical), x => CanAddPolicy());
            AddVisionPolicy = Command.Create<InsuranceType>(x => ExecuteAddPolicy(InsuranceType.Vision), x => CanAddPolicy());
            AddWorkersCompPolicy = Command.Create<InsuranceType>(x => ExecuteAddPolicy(InsuranceType.WorkersComp), x => CanAddPolicy());
            AddOtherPolicy = Command.Create<InsuranceType>(x => ExecuteAddPolicy(InsuranceType.Ambulatory), x => CanAddPolicy());

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;

        }

        private void OnRefresh()
        {
            Load.Execute();
        }

        #region Command Methods

        private void ExecuteViewReferrals()
        {
            _referralsViewManager.ShowReferrals(new ReferralLoadArguments { PatientId = PatientInfoViewModel.PatientId, AppointmentDate = DateTime.Now.ToClientTime() });
        }

        private void ExecuteLoad()
        {
            if (!PatientInfoViewModel.PatientId.HasValue) throw new InvalidOperationException("PatientInfoViewModel.PatientId must not be null");

            var loadInfo = _patientInsuranceViewService.LoadInformation(PatientInfoViewModel.PatientId.Value);

            // cache the list into a local variable so we can process it without firing PropertyChanged notifications.
            var tempPolicies = loadInfo.Policies.ToList();

            // set a reference to the parent list for each policy. 
            tempPolicies.ForEach(p => p.Parent = tempPolicies.ToList());

            // do not combine the code above that sets each Parent with this foreach.  the Parent is set at that point for a reason.
            tempPolicies.ForEach(policy =>
            {
                policy.CastTo<IEditableObject>().BeginEdit();
                policy.CastTo<IChangeTracking>().AcceptChanges();
                policy.AttachImage = Command.Create<Tuple<FileInfo[], object>>(y => AttachImageToPolicy(y.Item1, policy)).Async(() => InteractionContext);
                policy.PolicyDetails.LinkedPatients = policy.PolicyDetails.LinkedPatients.OrderByDescending(lp => lp.IsPolicyHolder).ThenBy(lp => lp.Name).ToObservableCollection();
            });

            // compress the current ranking of active policies.  we do this to ensure a valid and sequential list of policy ranks.
            tempPolicies.CompactRanking();

            // copy data from load information into our view model.  we use a temp variable to avoid Property Changed notifications from firing until we set the root ViewModel
            var tempPageViewModel = _createPatientInsuranceViewViewModel();
            tempPageViewModel.Policies = tempPolicies.RefreshOrder();
            tempPageViewModel.MedicareSecondaryReasonCodes = loadInfo.MedicareSecondaryReasonCodes;
            tempPageViewModel.Relationships = loadInfo.Relationships;
            tempPageViewModel.ShowCurrentPolicies = true;
            // Show ended/deleted automatically if they have issues
            tempPageViewModel.ShowEndedPolicies = tempPolicies.Any(p => p.PolicyStatus == PolicyRowStatus.Ended && !p.IsValid());
            tempPageViewModel.ShowDeletedPolicies = tempPolicies.Any(p => p.PolicyStatus == PolicyRowStatus.Deleted && !p.IsValid());
            tempPageViewModel.PatientViewModel = PatientInfoViewModel;

            // set DataBound root view model. 
            PatientInsuranceViewViewModel = tempPageViewModel;

            // rearrange policy
            ValidatePolicies(loadInfo.Policies.ToList());
        }

        private void ExecuteCancelConfirmChangeInsuranceType()
        {
            ConfirmChangeInsuranceTypeTrigger = false;
        }

        private void ExecuteClose(IInteractionContext interactionContext)
        {
            if (interactionContext == null)
            {
                InteractionContext.Complete(false);
            }
            else
            {
                interactionContext.Complete(false);
            }
        }

        private void ExecuteOpenEditPolicyDetailsWindow(PolicyViewModel policy)
        {
            if (policy.IsDeleted) return;

            if (policy.PolicyHolderIsNotSelf && !PermissionId.EditPatientInsuranceForOther.PrincipalContextHasPermission())
            {
                return;
            }

            if (policy.PolicyHolderIsSelf && !PermissionId.EditPatientInsuranceForSelf.PrincipalContextHasPermission())
            {
                return;
            }

            var returnArgs = _insurancePolicyViewManager.ShowEnterPolicyDetailsView(policy.InsurancePolicyId, policy.InsurerId, policy.PolicyHolder.Id);
            if (returnArgs != null && returnArgs.ExistingPolicyHasBeenEdited)
            {
                Load.Execute();
            }
        }

        private void ExecuteOpenInsurersAndPlansWindow(InsurancePlanViewModel insurancePlanViewModel)
        {
            var viewManager = new InsurancePlansSetupViewManager(_interactionManager);
            viewManager.ManageInsurancePlans(new ManageInsurancePlansLoadArguments
            {
                InsurerId = insurancePlanViewModel.Id
            });
        }

        private void ExecuteOpenEditPolicyHolderWindow(PolicyHolderViewModel policyHolder)
        {
            var returnArgs = _insurancePolicyViewManager.ShowPolicyholderDetailsView(policyHolder.Id, PatientInfoViewModel.PatientId.GetValueOrDefault());
            if (returnArgs.PolicyHolderHasBeenEdited)
            {
                if (ReloadPatientInfo != null)
                {
                    ReloadPatientInfo.Execute(PatientInfoViewModel.PatientId);
                }

                Load.Execute();
            }
        }

        private void ExecuteAddPolicy(InsuranceType insuranceType)
        {
            if (!CanAddPolicy())
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("You do not have permission to add a new policy"));
            }

            var returnArgs = _createInsurancePolicyWizardManager().Show(PatientInfoViewModel.PatientId.GetValueOrDefault(), insuranceType);

            if (returnArgs.NewPolicyHasBeenCreated)
            {
                Load.Execute();
            }
        }

        private static bool CanAddPolicy()
        {
            return PermissionId.AddPatientInsuranceForSelf.PrincipalContextHasPermission() || PermissionId.AddPatientInsuranceForOther.PrincipalContextHasPermission();
        }

        private static void ExecuteCellValidating(CellValidatingCommandArgs args)
        {
            var policyToValidate = args.DataContext as PolicyViewModel;

            if (policyToValidate == null) return;

            if ("EndDate".Equals(args.PropertyName) || "StartDate".Equals(args.PropertyName))
            {
                // specifically check the enddate here
                if (policyToValidate.EndDate < policyToValidate.StartDate)
                {
                    args.IsValid = false;
                    args.ErrorMessage = "The end date cannot be before the start date. Please correct this before moving on";
                    return;
                }
            }

            // prevent start date from being null
            if ("StartDate".Equals(args.PropertyName))
            {
                if (policyToValidate.StartDate == null)
                {
                    args.IsValid = false;
                    args.ErrorMessage = "The start date cannot be empty. Please correct this before moving on";
                    return;
                }
            }

            // Check if the user Un-Ended the policy
            if ("EndDate".Equals(args.PropertyName))
            {
                var oldDate = args.OldValue.ToString().ToDateTime();

                // check if unending the policy causes a date range validation error
                if (policyToValidate.IsActive() && oldDate.IsBeforeToday() && policyToValidate.ValidatePolicyRankOrderForIntersectingDateRanges(policyToValidate.Parent).IsNotSuccessful())
                {
                    args.Handled = true;
                    args.IsValid = false;
                    args.ErrorMessage = "Whoops! You can't re-activate this policy, the patient already has an active policy of the same rank and type";
                }
            }
        }

        private void ExecuteRowEditEnded(RowEditEndedArgs args)
        {
            if (!args.RowCommitted) return;
            var editedPolicy = args.EditedItem as PolicyViewModel;
            if (editedPolicy == null || !editedPolicy.CastTo<IChangeTracking>().IsChanged) return;

            var oldEndDate = args.OldValues["EndDate"];

            // validate the current row
            var editedPolicyErrorMessages = editedPolicy.Validate();
            if (editedPolicyErrorMessages.IsNotNullOrEmpty())
            {
                ValidationErrorMessages = editedPolicyErrorMessages.Select(x => x.Value).Distinct().Join(Environment.NewLine);
                ValidationErrorsTrigger = true;
                return;
            }

            // validate the whole tab view model
            var insuranceTabErrorMessages = PatientInsuranceViewViewModel.Validate();
            if (insuranceTabErrorMessages.IsNotNullOrEmpty())
            {
                ValidationErrorMessages = insuranceTabErrorMessages.Select(x => x.Value).Distinct().Join(Environment.NewLine);
                ValidationErrorsTrigger = true;
                return;
            }

            // if end date is entered when there was originally no value, null out the IsBilledPolicyHolder property.
            if (oldEndDate == null && editedPolicy.EndDate.HasValue && editedPolicy.IsBilledPolicyHolder)
            {
                editedPolicy.IsBilledPolicyHolder = false;
            }

            if (oldEndDate == null && editedPolicy.EndDate.HasValue && !editedPolicy.IsDeleted)
            {
                if (editedPolicy.Relationship != PolicyHolderRelationshipType.Self)
                {
                    CurrentPolicy = editedPolicy;

                    // trigger a confirmation box to the user asking them whether or not they want to end the policy for everyone else
                    ConfirmEndPolicyForAllPatientsTrigger = true;

                    if (_endForAllPatients)
                    {
                        try
                        {
                            _patientInsuranceViewService.SetEndDateForAllPolicies(editedPolicy.PatientInsuranceId, editedPolicy.EndDate.Value);
                            Policies.Where(x => x.InsurancePolicyId == editedPolicy.InsurancePolicyId && !x.IsDeleted && (x.EndDate > editedPolicy.EndDate)).ForEachWithSinglePropertyChangedNotification(x => x.EndDate = editedPolicy.EndDate);
                        }
                        catch (Exception ex)
                        {
                            ValidationErrorMessages = ex.Message;
                        }

                        _endForAllPatients = false;
                    }
                    else if (_endForPatientOnly)
                    {
                        _endForPatientOnly = false;
                    }
                    else
                    {
                        // change back to null
                        editedPolicy.EndDate = null;
                    }

                    _endForPatientOnly = false;
                    _endForAllPatients = false;
                }
                else
                {
                    // apply this enddate to all linked policies that don't have an enddate
                    _patientInsuranceViewService.SetEndDateForAllPolicies(editedPolicy.PatientInsuranceId, editedPolicy.EndDate.Value);
                    Policies.Where(x => x.InsurancePolicyId == editedPolicy.InsurancePolicyId && !x.IsDeleted && (x.EndDate > editedPolicy.EndDate)).ForEachWithSinglePropertyChangedNotification(x => x.EndDate = editedPolicy.EndDate);
                }
            }

            // check if the user unchecked the 'IsBilledPolicyHolder' for this policy (and did not check any other one)
            var resetBilledPolicyHolder = (bool)args.OldValues["IsBilledPolicyHolder"] && !editedPolicy.IsBilledPolicyHolder
                && PatientInsuranceViewViewModel.BilledPolicyHolderId == null;

            // update policy code to avoid white spaces
            editedPolicy.PolicyCode = editedPolicy.IfNotNull(x => x.PolicyCode.Trim());

            // save the changes to the policy
            SavePolicy(editedPolicy, resetBilledPolicyHolder);

            // policy has been successfully saved
            editedPolicy.CastTo<IEditableObject>().EndEdit();
            editedPolicy.CastTo<IChangeTracking>().AcceptChanges();
            args.RowIsValid = true;
            args.TryRefreshGridView = true;

        }

        private void ExecuteReorderRows(RowReorderEventArgs args)
        {
            if (args != null)
            {
                var draggedItem = args.DraggedItem as PolicyViewModel;
                var draggedOverItem = args.DraggedOverItem as PolicyViewModel;

                if (draggedItem != null && draggedOverItem != null)
                {
                    // check insurance type change
                    if (draggedItem.InsuranceType != draggedOverItem.InsuranceType)
                    {
                        ConfirmChangeInsuranceTypeTrigger = true; // will trigger a confirm command that will ask the user if they really mean to change the insurance type of the policy

                        if (ConfirmChangeInsuranceTypeTrigger) // if still true, then the user hit the yes button.
                        {
                            ConfirmChangeInsuranceTypeTrigger = false;
                            draggedItem.InsuranceType = draggedOverItem.InsuranceType;
                        }
                        else
                        {
                            Policies = Policies.RefreshOrder();
                            return;
                        }
                    }

                    try
                    {
                        ApplyIndexToRankAndSaveAll(Policies);

                        // we now have the ranking correct, so here we have to set the ordering of the physical list according to the new ranking
                        Policies = Policies.RefreshOrder();
                        //if (draggedItem.PolicyRank == 1 && !draggedItem.IsDeleted && draggedItem.IsMedicarePolicy && draggedItem.InsuranceType == InsuranceType.MajorMedical)
                        if (!draggedItem.IsDeleted && draggedItem.InsuranceType == InsuranceType.MajorMedical)
                        {
                            // set the medicare secondary reason back to null
                            draggedItem.MedicareSecondaryReasonCode = null;
                            PatientInsuranceViewViewModel.SelectedMedicareSecondaryReasonCode = null;
                            SaveMedicareSecondaryReason(draggedItem.InsurancePolicyId, default(int?));
                        }

                        // rearrange policy
                        ValidatePolicies(Policies);
                    }
                    catch
                    {
                        // validation failed so restore the originally ranks
                        RevertPolicyRank(Policies);
                        Policies = Policies.RefreshOrder();
                    }
                }
            }
        }

        private void ValidatePolicies(IEnumerable<PolicyViewModel> Policies)
        {
            var tempPolicies = Policies.ToList();

            if (tempPolicies.Count > 1)
            {
                bool isMedicare = false;
                isMedicare = tempPolicies.Any(p => p.IsMedicarePolicy && !((p.PolicyStatus == PolicyRowStatus.Deleted) || (p.PolicyStatus == PolicyRowStatus.Ended)));

                if (isMedicare)
                {
                    tempPolicies.RemoveAll(p => (p.PolicyStatus == PolicyRowStatus.Deleted) || (p.PolicyStatus == PolicyRowStatus.Ended));
                    tempPolicies.RemoveAll(p => (p.InsuranceTypeId == 2)||(p.InsuranceTypeId==3||(p.InsuranceTypeId==4)));
                    tempPolicies = tempPolicies.OrderBy(p => p.PolicyRank).ToList();

                    if (tempPolicies.Count > 1)
                    {
                        foreach (var policies in tempPolicies)
                        {
                            if (policies.PolicyRank == 2 && policies.IsMedicarePolicy && !(policies.PolicyStatus == PolicyRowStatus.Current))
                            {
                                PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = true;
                                break;
                            }
                            else
                            {
                                if (tempPolicies[1].IsMedicarePolicy && tempPolicies[1].PolicyRank != 1 && tempPolicies[1].PolicyRank > tempPolicies[0].PolicyRank)
                                {
                                    PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = true;
                                }
                                else if (tempPolicies[0].IsMedicarePolicy && tempPolicies[0].PolicyRank != 1 && tempPolicies[0].PolicyRank > tempPolicies[1].PolicyRank)
                                {
                                    PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = true;
                                }
                                else
                                    PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = false;
                            }
                        }
                    }
                    else
                    {
                        PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = false;
                    }

                }
                else
                {
                    PatientInsuranceViewViewModel.ShowMedicareSecondaryReason = false;
                }
            }
        }
        private void ExecuteLoadPatientPhoto(PolicyHolderViewModel policyHolder)
        {
            if (policyHolder != null && policyHolder.Photo == null && !policyHolder.PhotoIsLoaded)
            {
                policyHolder.Photo = _imageService.GetPatientPhoto(policyHolder.Id);
                policyHolder.PhotoIsLoaded = true;
            }
        }

        private void ExecuteDeletePolicy(PolicyViewModel policy)
        {
            if (!PermissionId.DeletePatientInsurance.PrincipalContextHasPermission())
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("You do not have permission to delete a patient's insurance"));
                return;
            }

            if (policy.PolicyHolderIsSelf)
            {
                var hasAdjustments = _patientInsuranceViewService.HasAdjustments(policy.PatientInsuranceId);

                if (hasAdjustments)
                {
                    if (!policy.EndDate.HasValue)
                    {
                        // inform user that policy cannot be deleted but give the option of ending it.
                        CurrentPolicy = policy;
                        ConfirmEndPolicyTrigger = true;
                        ConfirmEndPolicyTrigger = false;
                    }
                    else
                    {
                        // inform user that policy cannot be deleted
                        AlertWarnEndPolicyTrigger = true;
                        AlertWarnEndPolicyTrigger = false;
                    }
                }
                else
                {
                    try
                    {

                        _patientInsuranceViewService.DeletePolicy(policy.PatientInsuranceId);
                        Policies.Where(x => x.InsurancePolicyId == policy.InsurancePolicyId).ForEachWithSinglePropertyChangedNotification(x =>
                        {
                            x.IsDeleted = true;
                            if (!x.EndDate.HasValue) x.EndDate = DateTime.Now.ToClientTime().AddDays(-1);
                        });
                    }
                    catch
                    {
                        ValidationErrorMessages = "An error ocurred trying to delete the policy.  Please try again later.";
                        ValidationErrorsTrigger = true;
                    }
                }
            }
            else
            {
                _patientInsuranceViewService.DeletePolicy(policy.PatientInsuranceId);
                if (!policy.EndDate.HasValue)
                {
                    policy.EndDate = DateTime.Now.ToClientTime().AddDays(-1);
                }
                policy.IsDeleted = true;
            }

            // Medicare Policy - primary once other primary policy get deleted
            try
            {
                // we now have the ranking correct, so here we have to set the ordering of the physical list according to the new ranking
                Policies = Policies.RefreshOrder();
                ValidatePolicies(Policies);
            }
            catch
            {
                // validation failed so restore the originally ranks
                RevertPolicyRank(Policies);
                Policies = Policies.RefreshOrder();
            }
        }

        private void ExecuteConfirmEndPolicy()
        {
            if (CurrentPolicy != null && CurrentPolicy.PolicyHolderIsSelf)
            {
                try
                {
                    _patientInsuranceViewService.SetEndDateForAllPolicies(CurrentPolicy.PatientInsuranceId, DateTime.Now.ToClientTime().AddDays(-1));
                    Policies.Where(x => x.InsurancePolicyId == CurrentPolicy.InsurancePolicyId && !x.IsDeleted && (!x.EndDate.HasValue || x.EndDate > CurrentPolicy.EndDate)).ForEachWithSinglePropertyChangedNotification(x => x.EndDate = DateTime.Now.ToClientTime().AddDays(-1));
                }
                catch (Exception ex)
                {
                    ValidationErrorMessages = ex.Message;
                    ValidationErrorsTrigger = true;
                }
            }

            ConfirmEndPolicyTrigger = false;
        }

        private void ExecuteReactivatePolicy(PolicyViewModel policyToReactivate)
        {
            if (policyToReactivate.PolicyHolderIsNotSelf)
            {
                var policyHolderPolicyIsDeleted = _patientInsuranceViewService.IsPolicyHolderPolicyDeleted(policyToReactivate.InsurancePolicyId);
                if (policyHolderPolicyIsDeleted)
                {
                    // inform them that they cannot reactivate
                    AlertPolicyReactivateWarningTrigger = true;
                    return;
                }
            }

            // first reactivate locally and check that validation passes
            policyToReactivate.IsDeleted = false;

            // store away the ranking of each policy
            CachePolicyRank(Policies);

            // reactivate
            Policies.ReactivateAndInsert(policyToReactivate);

            // update the ranking and reorder by that ranking
            Policies.CompactRanking();
            Policies = Policies.RefreshOrder();

            try
            {
                if (!ValidateAllAndSetValidationErrorMessage())
                    throw new ValidationException();

                _patientInsuranceViewService.ReactivatePolicy(policyToReactivate.PatientInsuranceId);
                _patientInsuranceViewService.SavePolicyRankAndInsuranceType(Policies.ToPolicyRankDetails());
            }
            catch (Exception)
            {
                ValidationErrorsTrigger = true;
                RevertPolicyRank(Policies);
                Policies = Policies.RefreshOrder();
                policyToReactivate.IsDeleted = true;
            }
        }

        private bool _endForAllPatients;
        private void ExecuteConfirmEndPolicyForAllPatients()
        {
            _endForAllPatients = true;
        }

        private bool _endForPatientOnly;
        private void ExecuteConfirmEndPolicyForPatientOnly()
        {
            _endForPatientOnly = true;
        }

        private void ExecuteConfirmCancelEndPolicy()
        {
            _endForAllPatients = false;
            _endForPatientOnly = false;
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            if (!ValidateAll()) // if validation fails
            {
                return new PatientInfoClosingReturnArguments { CurrentContext = this, CancelClose = true, ConfirmCancelClose = ConfirmCancelClose };
            }

            return null;
        }

        private bool ConfirmCancelClose()
        {
            if (!ValidateAllAndSetValidationErrorMessage())
            {
                // pops up a confirm command asking whether or not to exit even though there are validation errors
                ValidationErrorsOnClosingTrigger = true;

                if (ValidationErrorsOnClosingTrigger) // if still true, they hit 'no' so we cancel exiting and stay on the screen
                {
                    ValidationErrorsOnClosingTrigger = false;

                    // inform the parent view context to proceed with canceling the close event
                    return true;
                }
            }

            return false;
        }

        private void ExecuteConfirmExitWithoutSavingChanges()
        {
            ValidationErrorsOnClosingTrigger = false; // means user hit the 'yes' button.
        }

        private void ExecuteShowScanWizard(PolicyViewModel policy)
        {
            var returnArgs = _insurancePolicyViewManager.ShowScanDocumentView();

            if (returnArgs.ScannedDocuments.IsNullOrEmpty()) return;

            Command.Create(() => returnArgs.ScannedDocuments.ForEachWithSinglePropertyChangedNotification(sd => SaveNewImageBytes(sd, policy))).Async(() => InteractionContext).Execute();
        }

        private void ExecuteDeleteImage(AttachedImage attachedImage)
        {
            if (!PermissionId.DeletePatientInsuranceDocument.PrincipalContextHasPermission())
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("You do not have permission to perform this action"));
                return;
            }

            _patientInsuranceViewService.DeleteImage(attachedImage.Id);

            var policies = Policies.Where(x => x.PolicyDetails.AttachedImages != null && x.PolicyDetails.AttachedImages.Any(y => y.Id == attachedImage.Id));
            foreach (var p in policies)
            {
                var imageToRemove = p.PolicyDetails.AttachedImages.SingleOrDefault(x => x.Id == attachedImage.Id);
                if (imageToRemove != null)
                {
                    p.PolicyDetails.AttachedImages.Remove(imageToRemove);
                }
            }
        }

        private void ExecuteShowImageInPdfViewer(AttachedImage attachedImage)
        {
            _imagingViewManager.ShowImageViewer(attachedImage.Image);
        }

        private void ExecuteMedicareSecondaryReasonChanged()
        {
            SaveCurrentlySelectedMedicareSecondaryReason();
        }

        #endregion

        #region View Context Bound Properties

        [DispatcherThread]
        public virtual PatientInsuranceViewViewModel PatientInsuranceViewViewModel { get; set; }

        [DispatcherThread]
        public virtual PolicyViewModel CurrentPolicy { get; set; }

        [DispatcherThread]
        public virtual PolicyViewModel SelectedVisionPolicy { get; set; }

        [DispatcherThread]
        public virtual PolicyViewModel SelectedWorkersCompPolicy { get; set; }

        [DispatcherThread]
        public virtual PolicyViewModel SelectedOtherPolicy { get; set; }

        [DispatcherThread]
        public virtual bool ConfirmEndPolicyTrigger { get; set; }

        [DispatcherThread]
        public virtual bool AlertWarnEndPolicyTrigger { get; set; }

        [DispatcherThread]
        public virtual bool AlertPolicyReactivateWarningTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ConfirmEndPolicyForAllPatientsTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ConfirmChangeInsuranceTypeTrigger { get; set; }

        [DispatcherThread]
        public virtual string ValidationErrorMessages { get; set; }

        [DispatcherThread]
        public virtual bool ValidationErrorsTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ValidationErrorsOnClosingTrigger { get; set; }

        [DispatcherThread]
        public virtual bool IsClosing { get; set; }

        [DispatcherThread]
        public virtual string UserIntialDirectory { get; set; }

        #endregion

        #region Commands

        public ICommand Load { get; set; }

        public ICommand ViewReferrals { get; set; }

        public ICommand ReorderRows { get; set; }

        public ICommand RowEditEnded { get; set; }

        public ICommand CellValidating { get; set; }

        public ICommand CellValidated { get; set; }

        public ICommand OpenEditPolicyHolderWindow { get; set; }

        public ICommand OpenInsurersAndPlansWindow { get; set; }

        public ICommand OpenEditPolicyDetailsWindow { get; set; }

        public ICommand LoadPatientPhoto { get; set; }

        public ICommand DeletePolicy { get; set; }

        public ICommand ReactivatePolicy { get; set; }

        public ICommand ConfirmEndPolicy { get; set; }

        public ICommand ConfirmEndPolicyForPatientOnly { get; set; }

        public ICommand ConfirmEndPolicyForAllPatients { get; set; }

        public ICommand ConfirmCancelEndPolicy { get; set; }

        public ICommand Close { get; set; }

        public ICommand CancelConfirmChangeInsuranceType { get; set; }

        public ICommand Closing { get; set; }

        public ICommand AddMajorMedicalPolicy { get; set; }

        public ICommand AddVisionPolicy { get; set; }

        public ICommand AddWorkersCompPolicy { get; set; }

        public ICommand AddOtherPolicy { get; set; }

        public ICommand ShowScanWizard { get; set; }

        public ICommand DeleteImage { get; set; }

        public ICommand ShowImageInPdfViewer { get; set; }

        public ICommand MedicareSecondaryReasonChanged { get; set; }

        public ICommand ConfirmExitWithoutSavingChanges { get; set; }

        public ICommand ReloadPatientInfo { get; set; }

        #endregion

        #region Class Members

        public IInteractionContext InteractionContext
        {
            get;
            set;
        }

        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        /// <summary>
        /// Just a shorthand syntax to access the bound policies list within this view context
        /// </summary>
        private ObservableCollection<PolicyViewModel> Policies
        {
            get { return PatientInsuranceViewViewModel.Policies; }
            set { PatientInsuranceViewViewModel.Policies = value; }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Spawns a background task and saves the current policy to the database.  specify a value for resetBilledPolicyHolder to reset the billed policyholder back to the 
        /// original patient.
        /// </summary>
        /// <param name="policyViewModel"></param>
        /// <param name="resetBilledPolicyHolder"></param>
        private void SavePolicy(PolicyViewModel policyViewModel, bool? resetBilledPolicyHolder = null)
        {
            // save the changes to the policy
            LastTaskCreated = Task.Factory.StartNewWithCurrentTransaction(() =>
            {
                lock (SavePolicyLock)
                {
                    _patientInsuranceViewService.SavePolicy(policyViewModel, resetBilledPolicyHolder.GetValueOrDefault());
                }
            }).OnTaskException(this.Dispatcher());

        }

        /// <summary>
        /// Attempts to set the PolicyRank of each policy based on it's current position in the list (for all insurance types) 
        /// and save the new PolicyRank to the database.  If any validation fails, or the database transaction did not complete successfully, it will
        /// revert the list to the old positions.
        /// </summary>
        private void ApplyIndexToRankAndSaveAll(IEnumerable<PolicyViewModel> policies)
        {
            var localListOfPolicies = policies.ToList();

            // store the current ranks locally
            CachePolicyRank(localListOfPolicies);

            // set new rank based on index location of each item in the list
            localListOfPolicies.ApplyIndexToRank();

            try
            {
                var policiesValidationMessage = localListOfPolicies.ValidatePolicyRankOrderForIntersectingDateRanges();
                if (policiesValidationMessage.IsNotSuccessful()) throw new ValidationException(policiesValidationMessage.ErrorMessage);

                // save to database
                SaveRankAndInsuranceTypeForAll(localListOfPolicies);
            }
            catch (Exception ex)
            {
                var ve = ex.SearchFor<ValidationException>();

                if (ve != null)
                {
                    ValidationErrorMessages = ve.Message;
                    ValidationErrorsTrigger = true;
                }

                // validation failed so restore the original ranks
                RevertPolicyRank(localListOfPolicies);
            }
        }

        private Dictionary<int, int> _ranksByPolicy;

        /// <summary>
        /// Caches the rank of each policy in the list to a dictionary for later use.
        /// </summary>
        private void CachePolicyRank(IEnumerable<PolicyViewModel> policies)
        {
            _ranksByPolicy = policies.ToDictionary(p => p.PatientInsuranceId, p => p.PolicyRank);
        }

        /// <summary>
        /// Reverts the rank of each policy to the previously stored ranks.
        /// </summary>
        private void RevertPolicyRank(IEnumerable<PolicyViewModel> policies)
        {
            foreach (var policy in policies.Where(policy => _ranksByPolicy.ContainsKey(policy.PatientInsuranceId)))
            {
                policy.PolicyRank = _ranksByPolicy[policy.PatientInsuranceId];
            }
        }

        internal readonly object SavePolicyLock = new object();

        internal Task LastTaskCreated { get; private set; }

        /// <summary>
        /// Spawns a background task to save the current list of policies' rank and insurance type
        /// </summary>
        /// <param name="policies"></param>
        private void SaveRankAndInsuranceTypeForAll(IEnumerable<PolicyViewModel> policies)
        {
            LastTaskCreated = Task.Factory.StartNewWithCurrentTransaction(() =>
            {
                lock (SavePolicyLock)
                {
                    _patientInsuranceViewService.SavePolicyRankAndInsuranceType(policies.ToPolicyRankDetails());
                }
            }).OnTaskException(this.Dispatcher());
        }

        /// <summary>
        /// Saves the currently selected reason code as the medicare secondary reason code for the current medicare policy.
        /// If there is no current medicare policy or there is no reason code selected, then it does not save anything.
        /// </summary>
        private void SaveCurrentlySelectedMedicareSecondaryReason()
        {
            if (!PatientInsuranceViewViewModel.ShowMedicareSecondaryReason) return;

            if (PatientInsuranceViewViewModel.SecondaryMedicarePolicy != null)
            {
                SaveMedicareSecondaryReason(PatientInsuranceViewViewModel.SecondaryMedicarePolicy.InsurancePolicyId, PatientInsuranceViewViewModel.SelectedMedicareSecondaryReasonCode);
            }
        }

        /// <summary>
        /// Spawns a new task and saves the medicare secondary reason for the specified insurance policy with the specified reasonCode.
        /// </summary>
        /// <param name="insurancePolicyId"></param>
        /// <param name="reasonCode"></param>
        private void SaveMedicareSecondaryReason(int insurancePolicyId, int? reasonCode)
        {
            LastTaskCreated = Task.Factory.StartNewWithCurrentTransaction(() =>
            {
                lock (SavePolicyLock)
                {
                    _patientInsuranceViewService.SaveMedicareSecondaryReason(insurancePolicyId, reasonCode);
                }
            })
                                                        .OnTaskException(this.Dispatcher());
        }

        private static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPE", ".JPEG", ".BMP", ".GIF", ".PNG" };
        private void AttachImageToPolicy(FileInfo[] files, PolicyViewModel policy)
        {
            if (files.IsNullOrEmpty()) return;

            // need to check if its an actual image or not before we save it
            if (files != null && !ImageExtensions.Contains(files[0].Extension.ToUpperInvariant()))
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("Selected files must of the following format: {0}".FormatWith(ImageExtensions.Join())));
                return;
            }

            // ReSharper disable PossibleNullReferenceException
            // Already checking if "files.IsNullOrEmpty()"
            // We are not using FileManager here, because passed in file is on user's local hard drive
            var imageData = files[0].Contents();
            // ReSharper restore PossibleNullReferenceException

            using (var ms = new MemoryStream(imageData))
            using (var image = Image.FromStream(ms))
            {
                imageData = image.ToByteArray(ImageFormat.Jpeg, 30);
            }
            SaveNewImageBytes(imageData, policy);
        }

        private void SaveNewImageBytes(byte[] image, PolicyViewModel policy)
        {
            var newId = _patientInsuranceViewService.SaveImage(policy.PatientInsuranceId, image);

            policy.PolicyDetails.AttachedImages.Insert(0, new AttachedImage { Id = newId, Image = image });

            // we force a rebind here due to a weird issue in the listbox where 2 items show up upon an initial insert.  
            policy.PolicyDetails.AttachedImages = policy.PolicyDetails.AttachedImages.ToExtendedObservableCollection();
        }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
            ReloadPatientInfo = reloadPatientInfoCommand;
        }

        #endregion

        #region Validation Helper Methods

        /// <summary>
        /// Validates the PatientInsuranceViewViewModel and All policies just returns true or false. No error message is set
        /// </summary>
        /// <returns></returns>
        private bool ValidateAll()
        {
            if (PatientInsuranceViewViewModel != null)
            {
                var isValid = PatientInsuranceViewViewModel.IsValid();

                if (!isValid) return false;

                isValid = Policies.IsNullOrEmpty() || Policies.All(p => p.IsValid());

                return isValid;
            }

            return true;
        }

        /// <summary>
        /// Validates the PatientInsuranceViewViewModel and All policies and sets the error message that can be displayed.
        /// </summary>        
        /// <returns></returns>
        private bool ValidateAllAndSetValidationErrorMessage()
        {
            ValidationErrorMessages = PatientInsuranceViewViewModel != null ? (PatientInsuranceViewViewModel.Validate().ToValidationMessageString() ?? Policies.ValidateToMessage() ?? string.Empty) : string.Empty;
            return ValidationErrorMessages.Trim().IsNullOrEmpty();
        }

        #endregion

        public ICommonViewFeatureExchange ViewFeatureExchange { get; private set; }
    }

    public class PatientInsuranceViewContextDataContextReference : DataContextReference
    {
        public new PatientInsuranceViewContext DataContext
        {
            get
            {
                return (PatientInsuranceViewContext)GetValue(DataContextProperty);
            }
            set
            {
                SetValue(DataContextProperty, value);
            }
        }
    }

    public static class TaskExtensions
    {
        /// <summary>
        /// Invokes any unhandled exception thrown during the task execution onto the main dispatcher thread
        /// </summary>
        /// <param name="t"></param>
        /// <param name="dispatcher"></param>
        /// <returns>The original task</returns>
        public static Task OnTaskException(this Task t, System.Windows.Threading.Dispatcher dispatcher)
        {
            t.ContinueWith(t2 =>
            {
                dispatcher.Invoke(() => t2.Exception.Rethrow());
            }, TaskContinuationOptions.OnlyOnFaulted);
            return t;
        }
    }

    public static class PolicyViewModelExtensionsForInsuranceViewContext
    {
        public static string ValidateToMessage(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.ValidatePolicies().IfNotNull(x => x.ToValidationMessageString());
        }

        /// <summary>
        /// Validates each policy on an individual basis by calling Validate() on the policy object. 
        /// This will return any validation results returned from calling the validation attributes on the policy's properties.                
        /// </summary>
        /// <param name="policies"></param>        
        /// <returns></returns>
        public static List<ValidationResult> ValidatePolicies(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.IfNotNull(pol => pol.FirstOrDefault(p => !p.IsDeleted && !p.IsValid()).IfNotNull(p => p.Validate(true, false)));
        }

        public static ObservableCollection<PolicyViewModel> RefreshOrder(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.OrderByInsuranceTypeThenRank().ToExtendedObservableCollection();
        }
    }
}
