﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.Views.PatientWizard;
using IO.Practiceware.Presentation.ViewsModels.NonClinicalPatient;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientCommunicationPreferences
{
    /// <summary>
    /// Interaction logic for PatientCommunicationPreferencesView.xaml
    /// </summary>
    public partial class PatientCommunicationPreferencesView
    {
        public PatientCommunicationPreferencesView()
        {
            InitializeComponent();
        }
    }

    public class PatientCommunicationPreferencesViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        private readonly IPatientCommunicationPreferencesViewService _patientCommunicationPreferencesViewService;
        private readonly Func<PatientWizardManager> _createPatientWizardManager;
        private readonly Func<PatientCommunicationPreferenceViewModel> _createPatientCommunicationPreferenceViewModel;
        readonly IInteractionManager _interactionManager;

        public PatientCommunicationPreferencesViewContext(IPatientCommunicationPreferencesViewService patientCommunicationPreferencesViewService,
            Func<PatientWizardManager> createPatientWizardManager,
            ICommonViewFeatureExchange exchange,
            IInteractionManager interactionManager,
            Func<PatientCommunicationPreferenceViewModel> createPatientCommunicationPreferenceViewModel)
        {
            _patientCommunicationPreferencesViewService = patientCommunicationPreferencesViewService;
            _createPatientWizardManager = createPatientWizardManager;
            _createPatientCommunicationPreferenceViewModel = createPatientCommunicationPreferenceViewModel;
            _interactionManager = interactionManager;

            Load = Command.Create(ExecuteLoad).Async();
            AddContactPreferenceForCommunicationType = Command.Create<PatientCommunicationPreferenceViewModel>(ExecuteAddContactPreferenceForCommunicationType);
            RemovePreference = Command.Create<PatientCommunicationPreferenceViewModel>(ExecuteRemovePreference).Async(() => InteractionContext);
            AddContact = Command.Create(ExecuteAddContact);
            InsertNewContact = Command.Create<PatientContactViewModel>(ExecuteInsertNewContact).Async(() => InteractionContext);
            UnlinkContact = Command.Create<PatientContactViewModel>(ExecuteUnlinkContact).Async(() => InteractionContext);

            SaveAllPreferences = Command.Create(ExecuteSaveAllPreferences).Async(() => InteractionContext);

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void ExecuteUnlinkContact(PatientContactViewModel contact)
        {
            try
            {
                if (!PermissionId.UnLinkContact.EnsurePermission()) return;

                _patientCommunicationPreferencesViewService.UnlinkContactFromPatient(contact.PatientRelationshipId);
                Contacts.Remove(contact);
            }
            catch (Exception ex)
            {
                if (ex.SearchFor<ValidationException>() != null)
                {
                    AlertCannotUnlinkContactTrigger = true;
                }
            }
        }

        private void ExecuteAddContact()
        {
            if (!PermissionId.LinkContact.EnsurePermission()) return;

            var patientWizard = _createPatientWizardManager();
            var returnArgs = patientWizard.Show(PatientInfoViewModel.PatientId, PatientRelationshipType.Contact);

            // go ahead and add the returned patient to the list of contacts for the Patient in Context.            
            if (returnArgs != null && returnArgs.SelectedId.HasValue)
            {
                InsertNewContact.Execute(new PatientContactViewModel { PatientId = returnArgs.SelectedId.Value, RelationshipDescription = returnArgs.RelationshipDescription });
            }
        }

        private void ExecuteInsertNewContact(PatientContactViewModel contactViewModel)
        {
            if (Contacts.Any(c => c.PatientId == contactViewModel.PatientId))
            {
                AlertCannotLinkContactTrigger = true;
            }
            else
            {
                var newContactViewModel = _patientCommunicationPreferencesViewService.AddContactToPatient(PatientInfoViewModel.PatientId.EnsureNotDefault().Value, contactViewModel.PatientId, contactViewModel.RelationshipDescription);
                Contacts.Add(newContactViewModel);
            }
            Contacts = Contacts.OrderBy(x => x.FullName).ToExtendedObservableCollection();
        }

        private void ExecuteSaveAllPreferences()
        {
            _patientCommunicationPreferencesViewService.SaveAllPatientCommunicationPreferences(PatientInfoViewModel.PatientId.EnsureNotDefault().Value, PatientCommunicationPreferences, Comments);

            _patientCommunicationPreferencesViewService.SaveAllContactInfo(Contacts);

            ExecuteLoad();
        }

        private void ExecuteRemovePreference(PatientCommunicationPreferenceViewModel viewModel)
        {
            if (viewModel.Id.HasValue)
            {
                _patientCommunicationPreferencesViewService.RemovePreference(viewModel.Id.Value);
            }

            PatientCommunicationPreferences.Remove(viewModel);
        }

        private void ExecuteAddContactPreferenceForCommunicationType(PatientCommunicationPreferenceViewModel viewModel)
        {
            var newPreference = _createPatientCommunicationPreferenceViewModel();
            newPreference.CommunicationType = viewModel.CommunicationType;
            newPreference.IsDefault = false;

            PatientCommunicationPreferences.Add(newPreference);

            PatientCommunicationPreferences = PatientCommunicationPreferences.OrderByCommunicationTypeOrdering().ToExtendedObservableCollection();
        }

        private void ExecuteLoad()
        {
            var loadInfo = _patientCommunicationPreferencesViewService.LoadPreferences(PatientInfoViewModel.PatientId.EnsureNotDefault().Value);

            Contacts = loadInfo.Contacts.OrderBy(x => x.FullName).ToExtendedObservableCollection();

            var tempPreferenceList = loadInfo.PatientCommunicationPreferences.ToList();
            foreach (var communication in Enums.GetValues<PatientCommunicationType>())
            {
                var first = tempPreferenceList.FirstOrDefault(x => x.CommunicationType == communication);
                if (first != null)
                {
                    first.IsDefault = true;
                }
                else
                {
                    var patientCommunicationPreferenceViewModel = _createPatientCommunicationPreferenceViewModel();
                    patientCommunicationPreferenceViewModel.IsDefault = true;
                    patientCommunicationPreferenceViewModel.CommunicationType = communication;
                    tempPreferenceList.Add(patientCommunicationPreferenceViewModel);
                }
            }

            PatientCommunicationPreferences = tempPreferenceList.OrderByCommunicationTypeOrdering().ToExtendedObservableCollection();
            Comments = loadInfo.PatientCommunicationPreferencesNotes;
        }


        #region IPatientInfoViewContext Members

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            return null;
        }


        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        #endregion

        [DispatcherThread]
        public virtual ObservableCollection<PatientContactViewModel> Contacts { get; set; }

        [DispatcherThread]
        [DependsOn("Contacts")]
        public virtual ObservableCollection<PatientContactViewModel> ContactsWrapper
        {
            get
            {
                _contactsWrapper = new ExtendedObservableCollection<PatientContactViewModel>(Contacts.OrderBy(x => x.FullName));
                _contactsWrapper.Insert(0, null);

                return _contactsWrapper;
            }
        }
        private ObservableCollection<PatientContactViewModel> _contactsWrapper;

        [DispatcherThread]
        public virtual ObservableCollection<PatientCommunicationPreferenceViewModel> PatientCommunicationPreferences { get; set; }

        [DispatcherThread]
        public virtual string Comments { get; set; }

        [DispatcherThread]
        public virtual bool AlertCannotDeleteContactTrigger { get; set; }

        [DispatcherThread]
        public virtual bool AlertCannotUnlinkContactTrigger { get; set; }

        [DispatcherThread]
        public virtual bool AlertCannotLinkContactTrigger { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        #region Commands

        public ICommand Load { get; set; }

        private void OnRefresh()
        {
            Load.Execute();
        }

        public ICommand AddContactPreferenceForCommunicationType { get; private set; }

        public ICommand RemovePreference { get; private set; }

        public ICommand AddContact { get; private set; }

        private ICommand InsertNewContact { get; set; }

        public ICommand UnlinkContact { get; private set; }

        public ICommand SaveAllPreferences { get; private set; }

        #endregion

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }
}
