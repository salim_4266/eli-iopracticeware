﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.NonClinicalPatient;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Windows;
namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    [Singleton]
    public class PatientInfoViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IPatientInfoViewService _patientInfoViewService;

        public PatientInfoViewManager(IInteractionManager interactionManager, IPatientInfoViewService patientInfoViewService)
        {
            _interactionManager = interactionManager;
            _patientInfoViewService = patientInfoViewService;
        }

        public event EventHandler<PatientInfoScreenOpenEventArgs> OpenLegacyPatientScreen;

        public PatientInfoReturnArguments ShowPatientInfo(PatientInfoLoadArguments loadArguments)
        {
            ApplicationContext.Current.PatientId = loadArguments.PatientId;
            try
            {
                if (loadArguments.PatientId.HasValue && loadArguments.PatientId.Value <= 0)
                {
                    throw new ArgumentException("Could not load the Patient Information screen.  The Id must be a valid value.");
                }

                var loadFullPatientInfoView = !loadArguments.PatientId.HasValue || loadArguments.IsConvertingNonClinicalPatient || LoadingWindow.Run(() => (_patientInfoViewService.IsClinical(loadArguments.PatientId.Value)));
                if (loadFullPatientInfoView)
                {
                    var view = new PatientInfoView();
                    var dataContext = view.DataContext.EnsureType<PatientInfoViewContext>();

                    if (!loadArguments.PatientId.HasValue || !loadArguments.TabToOpen.HasValue)
                    {
                        loadArguments.TabToOpen = PatientInfoTab.Demographics;
                    }

                    dataContext.LoadArguments = loadArguments;

                    _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = "Patient Information" });

                    return new PatientInfoReturnArguments { PatientId = dataContext.PatientInfoViewModel.PatientId, AppointmentIds = dataContext.AppointmentsViewContextLoadViewModel.ViewContext.CastTo<PatientAppointmentsViewContext>().AppointmentIds };
                }
                else
                {
                    var view = new NonClinicalPatientDetailsView();
                    view.DataContext.EnsureType<NonClinicalPatientDetailsViewContext>().NonClinicalPatientId = loadArguments.PatientId;

                    _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip });

                    return new PatientInfoReturnArguments { PatientId = view.DataContext.EnsureType<NonClinicalPatientDetailsViewContext>().NonClinicalPatientId, AppointmentIds = new HashSet<int>() };
                }
            }
            finally
            {
                ApplicationContext.Current.PatientId = null;
            }
        }

        /// <summary>
        /// Called to open legacy patient screen.
        /// </summary>
        /// <param name="patientId">The patient id.</param>
        /// <param name="patientScreenToOpen">The patient screen to open ("FinancialData", "ClinicalData", "Notes", "Documents").</param>
        public void ShowLegacyPatientScreen(int patientId, string patientScreenToOpen)
        {
            if (OpenLegacyPatientScreen != null)
            {
                OpenLegacyPatientScreen(this, new PatientInfoScreenOpenEventArgs(patientId, patientScreenToOpen));
            }
        }

        /// <summary>
        /// Displays patient alert of given type
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="alertType"></param>
        public void ShowPatientAlert(int patientId, PatientAlertType alertType)
        {
            if (OpenLegacyPatientScreen != null)
            {
                OpenLegacyPatientScreen(this, new PatientInfoScreenOpenEventArgs(patientId, "Alerts", (int)alertType));
            }
        }

        /// <summary>
        /// Displays patient Clinical Data for given AppointmentId
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="appointmentId"></param>
        public void ShowPatientClinicalData(int patientId, int appointmentId)
        {
            if (OpenLegacyPatientScreen != null)
            {
                OpenLegacyPatientScreen(this, new PatientInfoScreenOpenEventArgs(patientId, PatientInfoTab.ClinicalData.ToString(), appointmentId));
            }
        }
    }

    public class PatientInfoLoadArguments
    {
        public int? PatientId { get; set; }

        public PatientInfoTab? TabToOpen { get; set; }

        /// <summary>
        /// If the patient info screen is being loaded for a non-clinical patient in the process of being converted to a clinical
        /// patient, then we will show the full patient demographics screen (not just the limited non clinical patient details screen).
        /// </summary>
        /// <value>
        /// <c>true</c> if [is converting non clinical patient]; otherwise, <c>false</c>.
        /// </value>
        public bool IsConvertingNonClinicalPatient { get; set; }

        public int TabToOpenInt
        {
            get { return TabToOpen.HasValue ? (int)TabToOpen : 0; }
            set
            {
                TabToOpen = (PatientInfoTab)value;
            }
        }
    }

    public class PatientInfoReturnArguments
    {
        public HashSet<int> AppointmentIds { get; set; }
        public int? PatientId { get; set; }
    }

    public enum PatientInfoTab
    {
        Appointments = 0,
        Insurance,
        Communications,
        Demographics,
        FinancialData,
        ClinicalData,
        Notes,
        Documents,
        EditProblemList,
        ReconcileMedicationAllergies,
        ReconcilePatientProblems
    }

    /// <summary>
    /// Copy alert types definition in VB6
    /// </summary>
    public enum PatientAlertType
    {
        Demographics = 1,
        Financial = 2,
        Schedule = 3,
        Clinical = 4,
        Checkout = 5,
        Exam = 6,
        Plan = 7,
        Highlight = 8,
        InsurerBatch = 9,
        EM = 10,
        Checkin = 11
    }
}
