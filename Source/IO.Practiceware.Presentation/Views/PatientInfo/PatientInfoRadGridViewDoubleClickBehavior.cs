﻿using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    /// <summary>
    /// Behavior that executes a command on double clicking the associated RadGridView, except when double clicking a RadGridView's popup.
    /// </summary>
    public class PatientInfoRadGridViewDoubleClickBehavior : Behavior<RadGridView>
    {
        public static readonly DependencyProperty DoubleClickCommandProperty = DependencyProperty.Register("DoubleClickCommand", typeof(ICommand), typeof(PatientInfoRadGridViewDoubleClickBehavior));

        public ICommand DoubleClickCommand
        {
            get { return GetValue(DoubleClickCommandProperty) as ICommand; }
            set { SetValue(DoubleClickCommandProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.MouseDoubleClick += OnMouseDoubleClick;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseDoubleClick -= OnMouseDoubleClick;
            base.OnDetaching();
        }

        static void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as Visual;
            if(source != null && source.ParentOfType<Popup>() != null)
            {
                e.Handled = true;
            }
        }
    }
}
