﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.Views.Common.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using IO.Practiceware.Data;
using Soaf.Data;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder
{
    /// <summary>
    /// Interaction logic for ONCPsychologyView.xaml
    /// </summary>
    /// 
    
    public partial class LanguageFinderView:IViewContext
    {
        DataTable dtLanguage = new DataTable();
        List<string> item = new List<string>();
        List<int> LanguageId = new List<int>();
        public ICommand Close { get; protected set; }
        public List<string> SavedSelection = new List<string>();
        ObservableCollection<Language> sel = new ObservableCollection<Language>();
        public int PatientId { get; set; }
        private bool IsSaved;
        public string SavedItems { get; set; }
        public bool IsManipulated { get; set; }
        public LanguageFinderViewManager LanguageMgr { get; set; }
        public LanguageFinderView(int patientId)
        {
            PatientId = patientId;
            InitializeComponent();
            this.DataContext = this;
            FillGrid(DataGetLanguage()); // call this just below initializeComponent
            ShowSelectedLanguages();
            Close = Command.Create(ExecuteClose).Async(() => InteractionContext);
        }

        private void ShowSelectedLanguages()
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.Languages l inner join model.PatientLanguages pl on l.id = pl.LanguageId where pl.patientid = " + PatientId + " FOR xml path('')), 1, 2, '') as Description";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    SelectedGridItems.Text += dataTable.Rows[0]["Description"].ToString();
                }
            }
        }
        //Handler/Methods
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent is valid.  
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child 
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree 
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child.  
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search 
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name 
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found. 
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }
        private void FillGrid(ObservableCollection<Language> language, ObservableCollection<Language> selLanguage = null)
        { 
            if(cmbAreas != null)
            {
                dgProducts.ItemsSource = language;
                cmbAreas.SelectedIndex = 0;
            }
        }

        private ObservableCollection<Language> DataGetLanguage()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("IsSelected", typeof(bool));
            dataTable.Columns.Add("Id", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Shortcut", typeof(string));
            DataTable dtsel = new DataTable();
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                dataTable = dbConnection.Execute<DataTable>("Select Id,Name as Description,Abbreviation as Shortcut From model.Languages");
                string selectedLanguage = "Select * From model.PatientLanguages where patientId=@PatientId";
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@PatientId", PatientId);
                dtsel = dbConnection.Execute<DataTable>(selectedLanguage, param);
            }
            if (dataTable.Rows.Count > 0)
            {
                language = new ObservableCollection<Language>();
                foreach (DataRow row in dataTable.Rows)
                {
                    language.Add(new Language()
                    {
                        Id = (int)row["Id"],
                        Description = (string)row["description"],
                        Shortcut = (string)row["Shortcut"],
                    });
                }
            }
            dtLanguage = dataTable;
            if (!IsManipulated)
            {
                if (dtsel != null && dtsel.Rows.Count > 0)
                {
                    foreach (DataRow row in dtsel.Rows)
                    {
                        foreach (Language item in language)
                        {
                            if (row["LanguageId"].ToString() == item.Id.ToString())
                            {
                                item.IsSelected = true;
                                SavedSelection.Add(item.Description);
                                break;
                            }
                        }
                    }
                }
            }
            OriginialLanguage = language;
            return language;
        }

        [DispatcherThread]
        public ObservableCollection<Language> language { get; set; }

        public ObservableCollection<Language> OriginialLanguage { get; set; }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FilterResults();
        }
        private void FilterResults()
        {
            sel.Clear();
            string filterArea = cmbAreas.SelectionBoxItem.ToString();
            string searchTText = search.Text;
            if (filterArea == "Language Code")
                filterArea = "Shortcut";
            DataRow[] dr = dtLanguage.Select(filterArea + " like '" + searchTText + "%'");
            if (dr.Length > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("IsSelected", typeof(bool));
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Shortcut", typeof(string));
                dt.Columns.Add("Description", typeof(string));
                foreach (DataRow row in dr)
                {
                    sel.Add(new Language()
                    {
                        Id = (int)row["Id"],
                        Description = (string)row["description"],
                        Shortcut = (string)row["Shortcut"]
                        
                    });
                }
                foreach (Language row in sel)
                {
                    foreach (Language item in language)
                    {
                        if (row.Id == item.Id)
                        {
                            row.IsSelected = item.IsSelected;
                            break;
                        }
                    }
                }
                FillGrid(sel);
            }
            else
                FillGrid(language, sel);
        }
        /// <summary>
        /// Returns the first ancester of specified type
        /// </summary>
        public static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            current = VisualTreeHelper.GetParent(current);
            while (current != null)
            {
                if (current is T)
                {
                    return (T)current;
                }

                current = VisualTreeHelper.GetParent(current);
            };
            return null;
        }

        private void cmbAreas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedItems.Text = "Selected :";
            foreach (var row in dgProducts.ItemsSource)
            {
                RadioButton chk = dgProducts.Columns[0].GetCellContent(row) as RadioButton;
                //((RadioButton)dgProducts.Columns[0].GetCellContent(row)).IsChecked=false;
                if (chk != null)
                    chk.IsChecked = false;
            }
        }

        private void chkLanguage_Checked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            RadioButton chk = (RadioButton)sender;
            bool? obj = chk.IsChecked;
            Language objLanguage;
            DataGridRow row = FindAncestor<DataGridRow>(chk);
            //DataRowView rv=  (DataRowView)row.Item;

            if (obj == true)
            {
                objLanguage = (Language)row.Item;
                if (objLanguage.Description == "Declined To Specify")
                {
                    foreach (Language lngg in language)
                    {
                        if (lngg.Description != "Declined To Specify" && lngg.IsSelected == true)
                        {
                            lngg.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Language lgg in sel)
                        {
                            foreach (var itm in language)
                            {
                                if (lgg.Description == "Declined To Specify" && objLanguage.Description == lgg.Description)
                                {
                                    lgg.IsSelected = true;
                                }
                                else if (itm.Description != "Declined To Specify")
                                {
                                    lgg.IsSelected = false;
                                    lgg.IsSelected = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (Language lnge in language)
                    {
                        if (lnge.Description == "Declined To Specify" && lnge.IsSelected == true)
                        {
                            lnge.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Language lnge in sel)
                        {
                            foreach (var itms in language)
                            {
                                if (lnge.Description != "Declined To Specify" && lnge.Description == itms.Description && objLanguage.Description == lnge.Description)
                                {
                                    lnge.IsSelected = true;
                                    itms.IsSelected = true;
                                }
                                else if (lnge.Description == "Declined To Specify" && lnge.Description == itms.Description)
                                {
                                    lnge.IsSelected = false;
                                    itms.IsSelected = false;
                                }
                            }
                        }
                    }
                }
                if (!item.Contains(objLanguage.Description))
                {
                    item.Add(objLanguage.Description);
                    LanguageId.Add(int.Parse(objLanguage.Id.ToString()));
                }
                foreach (Language lang in language)
                {
                    if (lang.Id == objLanguage.Id)
                    {
                        lang.IsSelected = true;
                        break;
                    }
                }
                SelectedGridItems.Text = string.Join(",", item.ToArray());
                SelectedGridItems.ToolTip = string.Join(",", item.ToArray());
                LanguageMgr.selectedlanguage = SelectedGridItems.Text;
                SavedItems = SelectedGridItems.Text;
            }
        }

        private void chkLanguage_Unchecked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            RadioButton chk = (RadioButton)sender;
            bool? obj = chk.IsChecked;
            Language objLanguage;
            DataGridRow row = FindAncestor<DataGridRow>(chk);
            objLanguage = (Language)row.Item;
            if (item.Contains(objLanguage.Description))
            {
                item.Remove(objLanguage.Description);
                LanguageId.Remove(int.Parse(objLanguage.Id.ToString()));
            }
            foreach (Language lan in language)
            {
                if (lan.Id == objLanguage.Id)
                {
                    lan.IsSelected = false;
                    break;
                }
            }
            objLanguage.IsSelected = false;
            SelectedGridItems.Text = string.Join(",", item.ToArray());
            SelectedGridItems.ToolTip = string.Join(",", item.ToArray());
            LanguageMgr.selectedlanguage = SelectedGridItems.Text;
            SavedItems = SelectedGridItems.Text;
        }
        private void ExecuteClose()
        {
            if (IsSaved)
            {
                LanguageMgr.selectedlanguage = SavedItems;
            }
            else
            {
                if (SavedSelection.Count > 0)
                    LanguageMgr.selectedlanguage = string.Join(",", SavedSelection.ToArray());
            }
        }
        private void Save(object sender, RoutedEventArgs e)
        {
            if (PatientId > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    foreach (int item in LanguageId)
                    {
                        if (item == 11000192 && LanguageId.Count > 1)
                        {
                            IInteractionManager intMgr = InteractionManager.Current;
                            intMgr.Alert("Cannot chose other options along with 'Declined To Specify' ELSE only chose Declined To Specify.");
                            return;
                        }
                    }
                    Dictionary<string, object> paramDel = new Dictionary<string, object>();
                    paramDel.Add("@PatientId", PatientId);
                    string queryDel = "delete from model.PatientLanguages where patientId=@PatientId";
                    dbConnection.Execute(queryDel, paramDel);
                    foreach (Language item in language)
                    {
                        if (item.IsSelected)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("@PatientId", PatientId);
                            param.Add("@LanguageId", item.Id);
                            string qryInsert = "Insert into model.PatientLanguages(PatientId,LanguageId) values (@PatientId,@LanguageId)";
                            dbConnection.Execute(qryInsert, param);
                        }
                    }
                    IsSaved = true;
                    LanguageMgr.Existing_Languages = new ObservableCollection<Language>();
                    foreach (var ln in language)
                    {
                        if (ln.IsSelected)
                        {
                            LanguageMgr.Existing_Languages.Add(ln);
                        }
                    }
                    //IInteractionManager Mgr = InteractionManager.Current;
                    //Mgr.Alert("Saved Successfully"); 
                }
            }
            else
            {
                LanguageMgr.Temp_Languages = new ObservableCollection<Language>();
                foreach (var ln in language)
                {
                    if (ln.IsSelected)
                    {
                        LanguageMgr.Temp_Languages.Add(ln);
                    }
                }
            }
                InteractionContext.Complete();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            if (IsSaved)
            {
                LanguageMgr.selectedlanguage = SelectedGridItems.Text; 
            }
            this.InteractionContext.Complete();
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                FilterResults();
            }
        }
    }
}
