﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Soaf.Presentation;
using Soaf;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder
{
    public class LanguageFinderViewManager
    {
        private readonly IInteractionManager _interactionManager;
        IInteractionContext _con;
        public PatientDemographicsViewContext demographicsContext { get; set; }
        public string selectedlanguage { get; set; }
        public ObservableCollection<Language> Temp_Languages { get; set; }
        public ObservableCollection<Language> Existing_Languages { get; set; }
        public LanguageFinderViewManager(IInteractionManager interactionManager, IInteractionContext con)
        {
            _interactionManager = interactionManager;
            _con = con; 
        }
        public void ShowLanguageFinderFeedBack(LanguageFinderLoadArguments loadArgs)
        {
            var view = new LanguageFinderView(loadArgs.PatientId);
            view.LanguageMgr = this;
            view.PatientId = loadArgs.PatientId;
            //var dataContext = view.DataContext.EnsureType<EthnicityFinderView>();
            //if (loadArgs.PatientId != 0 && loadArgs != null)
            //{
            //    dataContext.LoadArguments = loadArgs.PatientId;
                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.NoResize
                });
            if(!string.IsNullOrEmpty(selectedlanguage))
            demographicsContext.selectedlanguage = selectedlanguage;
            demographicsContext.Demo_Languages = Temp_Languages;
            demographicsContext.Existing_Languages = Existing_Languages;
        }


    }
}



