﻿using Soaf;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics
{
    public class PatientDemographicsSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;
        
        public PatientDemographicsSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPatientDemographicsSetup()
        {
            var view = new PatientDemographicsSetupView();
            view.DataContext = view.DataContext.EnsureType<PatientDemographicsSetupViewContext>();
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip, Header = "Patient Demographics Setup" });
        }
    }
}
