﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics
{
    /// <summary>
    /// Interaction logic for PatientDemographicsSetupView.xaml
    /// </summary>
    public partial class PatientDemographicsSetupView
    {
        public PatientDemographicsSetupView()
        {
            InitializeComponent();
        }
    }

    public class PatientDemographicsSetupViewContext : IViewContext
    {
        private readonly IPatientDemographicsManageListViewService _patientDemographicsManageListViewService;
        readonly IInteractionManager _interactionManager;
        private readonly Func<PatientDemographicsConfigureFieldGroupViewModel> _createPatientDemographicsConfigureFieldGroupViewModel;

        public PatientDemographicsSetupViewContext(PatientDemographicsManageListViewContext patientDemographicsManageListViewContext, IPatientDemographicsManageListViewService patientDemographicsManageListViewService, IInteractionManager interactionManager, Func<PatientDemographicsConfigureFieldGroupViewModel> createPatientDemographicsConfigureFieldGroupViewModel)
        {
            PatientDemographicsManageListViewContext = patientDemographicsManageListViewContext;
            _patientDemographicsManageListViewService = patientDemographicsManageListViewService;
            _interactionManager = interactionManager;
            _createPatientDemographicsConfigureFieldGroupViewModel = createPatientDemographicsConfigureFieldGroupViewModel;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel).Async(() => InteractionContext);
        }

        public PatientDemographicsManageListViewContext PatientDemographicsManageListViewContext { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand Save { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public virtual bool CanEditPatientDemographicsFields
        {
            get { return PermissionId.AdministerPatientDemographicsFields.EnsurePermission(); }
        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            // Subscribe to completing event to act on window closing
            InteractionContext.Completing += OnInteractionContextCompleting;

            LoadPatientDemographicsFieldsListsInformation patientDemographicsFieldsListsInformation = _patientDemographicsManageListViewService.LoadPatientDemographicsFieldsListsInformation();
            PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations = patientDemographicsFieldsListsInformation.PatientDemographicsFieldConfigurations
                .GroupBy(f => f.GroupName)
                .Select(g => _createPatientDemographicsConfigureFieldGroupViewModel().Modify(
                    vm => vm.GroupName = g.Key).Modify(
                    vm => vm.ConfigureFields = g.ToExtendedObservableCollection())
                ).ToExtendedObservableCollection();

            PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations = patientDemographicsFieldsListsInformation.PatientDemographicsDefaultCheckBoxValueConfigurations.ToExtendedObservableCollection();
            PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations = patientDemographicsFieldsListsInformation.PatientDemographicsDefaultComboBoxValueConfigurations.ToExtendedObservableCollection();
            PatientDemographicsManageListViewContext.PatientDemographicsLists = patientDemographicsFieldsListsInformation.PatientDemographicsLists.ToExtendedObservableCollection();
            PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.As<IEditableObjectExtended>().InitiateCustomEdit();
            PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.As<IEditableObjectExtended>().InitiateCustomEdit();
            PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.As<IEditableObjectExtended>().InitiateCustomEdit();
            PatientDemographicsManageListViewContext.PatientDemographicsLists.As<IEditableObjectExtended>().InitiateCustomEdit();
        }

        private void ExecuteSave()
        {
            if (CanEditPatientDemographicsFields)
            {
                _patientDemographicsManageListViewService.UpdatePatientDemographicsFieldConfigurations(PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.Where(i => ((IChangeTracking) i).IsChanged).ToArray());
                _patientDemographicsManageListViewService.UpdatePatientDemographicsDefaultValueConfigurations(PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.Where(i => ((IChangeTracking) i).IsChanged).ToArray());
                _patientDemographicsManageListViewService.UpdatePatientDemographicsDefaultValueConfigurations(PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.Where(i => ((IChangeTracking) i).IsChanged).ToArray());
                _patientDemographicsManageListViewService.UpdatePatientDemographicsListsInformation(PatientDemographicsManageListViewContext.PatientDemographicsLists.Where(i => ((IChangeTracking) i).IsChanged).ToArray());
            }
            InteractionContext.Complete(true);
        }

        private void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        void OnInteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (((IInteractionContext)sender).DialogResult == true) return;
            // Are there any unsaved changes?
            var unsavedChanges = (PatientDemographicsManageListViewContext.PatientDemographicsGroupFieldConfigurations.CastTo<IChangeTracking>().IsChanged
                                 || PatientDemographicsManageListViewContext.PatientDemographicsDefaultCheckBoxConfigurations.CastTo<IChangeTracking>().IsChanged
                                 || PatientDemographicsManageListViewContext.PatientDemographicsDefaultComboBoxConfigurations.CastTo<IChangeTracking>().IsChanged
                                 || PatientDemographicsManageListViewContext.PatientDemographicsLists.CastTo<IChangeTracking>().IsChanged);

            if (!unsavedChanges) return;

            if (!CanEditPatientDemographicsFields) return;

            // Confirm whether to save them
            bool? saveChanges = false;
            this.Dispatcher().Invoke(() =>
            {
                saveChanges = _interactionManager.Confirm("Would you like to save your changes?");
            });

            // Yes?
            if ((bool)saveChanges)
            {
                // Save
                ExecuteSave();
            }
        }

    }
}