﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Soaf.Presentation;
using Soaf;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.EthnicityFinder
{
    public class EthnicityFinderViewManager
    {
        private readonly IInteractionManager _interactionManager;
        IInteractionContext _con;
        public PatientDemographicsViewContext demographicsContext { get; set; }
        public string selectedethnicity { get; set; }
        public ObservableCollection<Ethnicity> Temp_Ethnicities { get; set; }
        public ObservableCollection<Ethnicity> Existing_Ethnicities { get; set; }
        public EthnicityFinderViewManager(IInteractionManager interactionManager, IInteractionContext con)
        {
            _interactionManager = interactionManager;
            _con = con; 
        }
        public void ShowEthnicityFinderFeedBack(EthnicityFinderLoadArguments loadArgs)
        {
            var view = new EthnicityFinderView(loadArgs.PatientId);
            view.EthnicityMgr = this;
            view.PatientId = loadArgs.PatientId;
            //var dataContext = view.DataContext.EnsureType<EthnicityFinderView>();
            //if (loadArgs.PatientId != 0 && loadArgs != null)
            //{
            //    dataContext.LoadArguments = loadArgs.PatientId;
                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.NoResize
                });
            demographicsContext.selectedethnicity = selectedethnicity;
            demographicsContext.Demo_Ethnicities = Temp_Ethnicities;
            demographicsContext.Existing_Ethnicities = Existing_Ethnicities;
            //}
            //else
            //{
            //    _interactionManager.Alert("Please register this patient before Ethnicity survey");
            //    return;
            //}
        }


    }
}



