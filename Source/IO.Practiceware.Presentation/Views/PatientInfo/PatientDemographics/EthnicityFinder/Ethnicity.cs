﻿using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.EthnicityFinder
{
    public class Ethnicity : INotifyPropertyChanged
    {
        [DispatcherThread]
        public int Id { get; set; }

        [DispatcherThread]
        public bool IsReadOnly { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private bool isChecked;
        [DispatcherThread]
        public bool IsSelected
        {
            get { return isChecked; }
            set
            {
                if (isChecked != value)
                {
                    isChecked = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        [DispatcherThread]
        public string Code { get; set; }

        [DispatcherThread]
        public string Description { get; set; }

        [DispatcherThread]
        public string OMBCode { get; set; }

        [DispatcherThread]
        public string OMBCategory { get; set; }
    }
}
