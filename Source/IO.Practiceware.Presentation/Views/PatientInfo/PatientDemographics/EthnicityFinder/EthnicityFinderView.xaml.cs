﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.Views.Common.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using IO.Practiceware.Data;
using Soaf.Data;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.EthnicityFinder
{
    /// <summary>
    /// Interaction logic for ONCPsychologyView.xaml
    /// </summary>
    /// 
    
    public partial class EthnicityFinderView:IViewContext
    {
        public bool IsManipulated { get; set; }
        DataTable dtEthnicity = new DataTable();
        List<string> item = new List<string>();
        List<int> EthnicityId = new List<int>();
        ObservableCollection<Ethnicity> sel = new ObservableCollection<Ethnicity>();
        public string SavedItems { get; set; }
        List<int> DeletedEthnicities = new List<int>();
        private bool IsSaved;
        public int PatientId { get; set; }
        public EthnicityFinderViewManager EthnicityMgr { get; set; }
        public List<string> SavedSelection = new List<string>();
        public EthnicityFinderView(int patientId)
        {
            PatientId = patientId;
            InitializeComponent();
            this.DataContext = this;
            FillGrid(DataGetEthnicity()); // call this just below initializeComponent
            ShowSelectedEthnicity();
            Close = Command.Create(ExecuteClose).Async(() => InteractionContext);
        }
        public ICommand Close { get; protected set; }
        private void ShowSelectedEthnicity()
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.ethnicities e inner join model.patientethnicities pe on e.id = pe.ethnicityid where pe.patientid = " + PatientId+ " FOR xml path('')), 1, 2, '') as Description";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    SelectedGridItems.Text += dataTable.Rows[0]["Description"].ToString();
                }
            }
        }
        //Handler/Methods
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent is valid.  
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child 
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree 
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child.  
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search 
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name 
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found. 
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }
        private void FillGrid(ObservableCollection<Ethnicity> ethnicity, ObservableCollection<Ethnicity> selEthnicity=null)
        { 
            if(cmbAreas!=null)
            {
                dgProducts.ItemsSource = ethnicity;
                cmbAreas.SelectedIndex = 1;
            }
        }

        private ObservableCollection<Ethnicity> DataGetEthnicity()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("IsSelected", typeof(bool));
            dataTable.Columns.Add("Code", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("OMBCode", typeof(string));
            dataTable.Columns.Add("OMBCategory", typeof(string));
            DataTable dtsel = new DataTable();

            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string query = "Select Id,Code,Name as Description, OMBCode,OMBCategory From model.Ethnicities";
                dataTable = dbConnection.Execute<DataTable>(query);

                string selectedEthnicity = "Select * From model.PatientEthnicities where patientId=@PatientId";
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@PatientId", PatientId);
                dtsel = dbConnection.Execute<DataTable>(selectedEthnicity,param);
            }
            if (dataTable.Rows.Count > 0)
            {
                ethnicity = new ObservableCollection<Ethnicity>();
                foreach (DataRow row in dataTable.Rows)
                {
                    ethnicity.Add(new Ethnicity()
                    {
                        Id = (int)row["Id"],
                        Code = (string)row["code"],
                        Description = (string)row["description"],
                        OMBCategory = (string)row["OMBCategory"],
                        OMBCode=(string)row["OMBCode"]
                    });
                }
            }
            dtEthnicity = dataTable;
            if (!IsManipulated)
            {
                if (dtsel != null && dtsel.Rows.Count > 0)
                {
                    foreach (DataRow row in dtsel.Rows)
                    {
                        foreach (Ethnicity item in ethnicity)
                        {
                            if (row["EthnicityId"].ToString() == item.Id.ToString())
                            {
                                item.IsSelected = true;
                                SavedSelection.Add(item.Description);
                                break;
                            }
                        }
                    }
                }
            }
            OriginialEthinicity = ethnicity;
            return ethnicity;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FilterResults();
        }
        private void FilterResults()
        {
            sel.Clear();
            string filterArea = cmbAreas.SelectionBoxItem.ToString();
            string searchTText = search.Text;
            if (filterArea == "OMB Category")
                filterArea = "OMBCategory";
            if (filterArea == "OMB Code")
                filterArea = "OMBCode";
            if (!string.IsNullOrEmpty(searchTText))
            {
                DataRow[] dr = dtEthnicity.Select(filterArea + " like '" + searchTText + "%'");
                if (dr.Length > 0)
                {
                    
                    DataTable dt = new DataTable();
                    dt.Columns.Add("IsSelected", typeof(bool));
                    dt.Columns.Add("Id", typeof(int));
                    dt.Columns.Add("Code", typeof(string));
                    dt.Columns.Add("Description", typeof(string));
                    dt.Columns.Add("OMBCode", typeof(string));
                    dt.Columns.Add("OMBCategory", typeof(string));

                    foreach (DataRow row in dr)
                    {
                        sel.Add(new Ethnicity()
                        {
                            Id = (int)row["Id"],
                            Code = (string)row["code"],
                            Description = (string)row["description"],
                            OMBCategory = (string)row["OMBCategory"],
                            OMBCode = (string)row["OMBCode"]
                        });
                    }

                    foreach (Ethnicity row in sel)
                    {
                        foreach (Ethnicity item in ethnicity)
                        {
                            if (row.Id == item.Id)
                            {
                                row.IsSelected = item.IsSelected;
                                break;
                            }
                        }
                    }
                    FillGrid(sel);
                }
            }
            else
                FillGrid(ethnicity, sel);
        }
        /// <summary>
        /// Returns the first ancester of specified type
        /// </summary>
        public static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            current = VisualTreeHelper.GetParent(current);
            while (current != null)
            {
                if (current is T)
                {
                    return (T)current;
                }

                current = VisualTreeHelper.GetParent(current);
            };
            return null;
        }
        
        private void chkEthnicity_Checked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            CheckBox chk = (CheckBox)sender;
            bool? obj = chk.IsChecked;
            Ethnicity objEthnicity;
            DataGridRow row = FindAncestor<DataGridRow>(chk);
            if (obj == true)
                {
                    objEthnicity = (Ethnicity)row.Item;

                if (objEthnicity.Description == "Declined to Specify")
                {
                    foreach (Ethnicity eth in ethnicity)
                    {
                        if (eth.Description != "Declined to Specify" && eth.IsSelected == true)
                        {
                            eth.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Ethnicity eth in sel)
                        {
                            foreach (var itm in ethnicity)
                            {
                                if (eth.Description == "Declined to Specify" && objEthnicity.Description == eth.Description)
                                {
                                    eth.IsSelected = true;
                                }
                                else if (itm.Description != "Declined to Specify")
                                {
                                    eth.IsSelected = false;
                                    itm.IsSelected = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (Ethnicity eth in ethnicity)
                    {
                        if (eth.Description == "Declined to Specify" && eth.IsSelected == true)
                        {
                            eth.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Ethnicity eth in sel)
                        {
                            foreach (var itms in ethnicity)
                            {
                                if (eth.Description != "Declined to Specify" && eth.Description == itms.Description && objEthnicity.Description == eth.Description)
                                {
                                    eth.IsSelected = true;
                                    itms.IsSelected = true;
                                }
                                else if (eth.Description == "Declined to Specify" && eth.Description == itms.Description)
                                {
                                    eth.IsSelected = false;
                                    itms.IsSelected = false;
                                }
                            }
                        }
                    }
                }

                if (!item.Contains(objEthnicity.Description))
                {
                        item.Add(objEthnicity.Description);
                        EthnicityId.Add(int.Parse(objEthnicity.Id.ToString()));
                }

                    foreach (Ethnicity eth in ethnicity)
                    {
                        if (eth.Id == objEthnicity.Id)
                        {
                            eth.IsSelected = true;
                        }
                    }
                SelectedGridItems.Text = string.Join(",", item.ToArray());
                SelectedGridItems.ToolTip = string.Join(",", item.ToArray());
                EthnicityMgr.selectedethnicity = SelectedGridItems.Text;
                SavedItems = SelectedGridItems.Text;
            }
        }

        private void chkEthnicity_Unchecked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            CheckBox chk = (CheckBox)sender;
            bool? obj = chk.IsChecked;
            Ethnicity objEthnicity;
            DataGridRow row = FindAncestor<DataGridRow>(chk);
            objEthnicity = (Ethnicity)row.Item;
            if (objEthnicity.Id == 3150 || objEthnicity.Description.Equals("Declined to Specify"))
            {
                foreach (Ethnicity Row in dgProducts.Items)
                {
                    if (Row.Description != "Declined to Specify")
                    {
                        Row.IsReadOnly = false;
                        break;
                    }
                }
            }

            if (item.Contains(objEthnicity.Description))
            {
                item.Remove(objEthnicity.Description);
                EthnicityId.Remove(int.Parse(objEthnicity.Id.ToString()));
            }
            foreach (Ethnicity ethn in ethnicity)
            {
                if (ethn.Id == objEthnicity.Id)
                {
                    ethn.IsSelected = false;
                    break;
                }
            }

            objEthnicity.IsSelected = false;
            SelectedGridItems.Text = string.Join(",", item.ToArray());
            SelectedGridItems.ToolTip = string.Join(",", item.ToArray());
            EthnicityMgr.selectedethnicity = SelectedGridItems.Text;
            SavedItems = SelectedGridItems.Text;
        }
        private void ExecuteClose()
        {
            if (IsSaved)
            {
                EthnicityMgr.selectedethnicity = SavedItems;
            }
            else
            {
                if (SavedSelection.Count > 0)
                    EthnicityMgr.selectedethnicity = string.Join(",", SavedSelection.ToArray());
            }
        }
        private void Save(object sender, RoutedEventArgs e)
        {
            if (PatientId > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    Dictionary<string, object> paramDel = new Dictionary<string, object>();
                    paramDel.Add("@PatientId", PatientId);

                    foreach (int item in EthnicityId)
                    {
                        if (item == 3150 && EthnicityId.Count > 1)
                        {
                            IInteractionManager intMgr = InteractionManager.Current;
                            intMgr.Alert("Cannot chose other options along with 'Declined to Specify' ELSE only chose Declined to Specify.");
                            return;
                        }
                    }
                    string queryDel = "delete from model.PatientEthnicities where patientId=@PatientId";
                    dbConnection.Execute(queryDel, paramDel);
                    foreach (Ethnicity item in ethnicity)
                    {

                        if (item.IsSelected && item.Description == "Declined to Specify" && EthnicityId.Count == 1 && EthnicityId[0] == 3150)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("@PatientId", PatientId);
                            param.Add("@EthnicityId", item.Id);
                            string qryInsert = "Insert into model.PatientEthnicities(PatientId,EthnicityId) values (@PatientId,@EthnicityId)";
                            dbConnection.Execute(qryInsert, param);
                            break;
                        }
                        else if (item.IsSelected)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("@PatientId", PatientId);
                            param.Add("@EthnicityId", item.Id);
                            string qryInsert = "Insert into model.PatientEthnicities(PatientId,EthnicityId) values (@PatientId,@EthnicityId)";
                            dbConnection.Execute(qryInsert, param);
                        }
                    }
                    IsSaved = true;
                    EthnicityMgr.Existing_Ethnicities = new ObservableCollection<Ethnicity>();
                    foreach (var et in ethnicity)
                    {
                        if (et.IsSelected)
                        {
                            EthnicityMgr.Existing_Ethnicities.Add(et);
                        }
                    }
                    //IInteractionManager Mgr = InteractionManager.Current;
                    //Mgr.Alert("Saved Successfully"); 
                }
            }
            else
            {
                EthnicityMgr.Temp_Ethnicities = new ObservableCollection<Ethnicity>();
                foreach (var et in ethnicity)
                {
                    if (et.IsSelected)
                    {
                        EthnicityMgr.Temp_Ethnicities.Add(et);
                    }
                }
            }
            InteractionContext.Complete();
        }

        [DispatcherThread]
        public ObservableCollection<Ethnicity> ethnicity { get; set; }

        public ObservableCollection<Ethnicity> OriginialEthinicity { get; set; }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            if (IsSaved)
            {
                EthnicityMgr.selectedethnicity = SelectedGridItems.Text; 
            }
            this.InteractionContext.Complete();
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                FilterResults();
            }
        }
    }
}
