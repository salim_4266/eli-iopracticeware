﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics
{
    /// <summary>
    /// Interaction logic for PatientDemographicsManageListView.xaml
    /// </summary>
    public partial class PatientDemographicsManageListView
    {
        public PatientDemographicsManageListView()
        {
            InitializeComponent();
        }
    }

    public class PatientDemographicsManageListViewContext : IViewContext
    {
        public PatientDemographicsManageListViewContext()
        {
            MoveDown = Command.Create(MoveItemDown, () => SelectedPatientDemographicsFavoriteListItem != null);
            MoveUp = Command.Create(MoveItemUp, () => SelectedPatientDemographicsAllListItem != null);
        }

        public ICommand MoveDown { get; protected set; }
        public ICommand MoveUp { get; protected set; }

        [DispatcherThread]
        [Dependency]
        public virtual ExtendedObservableCollection<PatientDemographicsConfigureFieldGroupViewModel> PatientDemographicsGroupFieldConfigurations { get; set; }

        [DispatcherThread]
        [Dependency]
        public virtual ExtendedObservableCollection<PatientDemographicsConfigureDefaultValueViewModel> PatientDemographicsDefaultCheckBoxConfigurations { get; set; }

        [DispatcherThread]
        [Dependency]
        public virtual ExtendedObservableCollection<PatientDemographicsConfigureDefaultValueViewModel> PatientDemographicsDefaultComboBoxConfigurations { get; set; }

        [DispatcherThread]
        [Dependency]
        public virtual ExtendedObservableCollection<PatientDemographicsListViewModel> PatientDemographicsLists { get; set; }

        [DispatcherThread]
        public virtual PatientDemographicsListViewModel SelectedPatientDemographicsListItem { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedPatientDemographicsFavoriteListItem { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedPatientDemographicsAllListItem { get; set; }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private void MoveItemUp()
        {
            SelectedPatientDemographicsListItem.FavoriteItems.Insert(0, SelectedPatientDemographicsAllListItem);
            SelectedPatientDemographicsListItem.NonFavoriteItems.Remove(SelectedPatientDemographicsAllListItem);
            // showing only Top 5 in favorite items
            if (SelectedPatientDemographicsListItem.FavoriteItems.Count > 5)
            {
                //if (SelectedPatientDemographicsListItem.NonFavoriteItems != null)
                //{
                    SelectedPatientDemographicsListItem.NonFavoriteItems.Insert(0, SelectedPatientDemographicsListItem.FavoriteItems[5]);
                //}
                //else
                //{
                //    SelectedPatientDemographicsListItem.NonFavoriteItems = new ExtendedObservableCollection<NamedViewModel>();
                //    SelectedPatientDemographicsListItem.NonFavoriteItems.Add(SelectedPatientDemographicsListItem.FavoriteItems[5]);
                //}
                SelectedPatientDemographicsListItem.FavoriteItems.RemoveAt(5);
            }
            SelectedPatientDemographicsAllListItem = null;
        }

        private void MoveItemDown()
        {
            int currentIndex = SelectedPatientDemographicsListItem.FavoriteItems.IndexOf(SelectedPatientDemographicsFavoriteListItem);

            //Index of the selected item
            if (currentIndex > -1)
            {
                int downIndex = currentIndex + 1;
                if (downIndex == SelectedPatientDemographicsListItem.FavoriteItems.Count)
                {
                    //if (SelectedPatientDemographicsListItem.NonFavoriteItems != null)
                    //{
                        SelectedPatientDemographicsListItem.NonFavoriteItems.Insert(0, SelectedPatientDemographicsFavoriteListItem);
                    //}
                    //else
                    //{
                    //    SelectedPatientDemographicsListItem.NonFavoriteItems = new ExtendedObservableCollection<NamedViewModel>();
                    //    SelectedPatientDemographicsListItem.NonFavoriteItems.Add(SelectedPatientDemographicsFavoriteListItem);
                    //}
                    SelectedPatientDemographicsListItem.FavoriteItems.Remove(SelectedPatientDemographicsFavoriteListItem);
                }
                    //move the items
                else
                {
                    SelectedPatientDemographicsListItem.FavoriteItems.Move(currentIndex, downIndex);
                }
            }
            SelectedPatientDemographicsFavoriteListItem = null;
        }
    }
}