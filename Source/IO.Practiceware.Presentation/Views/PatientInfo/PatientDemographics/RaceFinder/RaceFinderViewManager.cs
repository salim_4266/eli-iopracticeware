﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Soaf.Presentation;
using Soaf;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.RaceFinder
{
    public class RaceFinderViewManager
    {
        private readonly IInteractionManager _interactionManager;
        IInteractionContext _con;
        public string selectedraces { get; set; }
        public ObservableCollection<Race> Temp_Races { get; set; }
        public ObservableCollection<Race> Existing_Races { get; set; }
        public PatientDemographicsViewContext demographicsContext { get; set; }
        public RaceFinderViewManager(IInteractionManager interactionManager, IInteractionContext con)
        {
            _interactionManager = interactionManager;
            _con = con; 
        }
        public void ShowRaceFinderFeedBack(RaceFinderLoadArguments loadArgs)
        {
            var view = new RaceFinderView(loadArgs.PatientId);
            view.RaceMgr = this;
            view.PatientId = loadArgs.PatientId;
            //var dataContext = view.DataContext.EnsureType<EthnicityFinderView>();
            //if (loadArgs.PatientId != 0 && loadArgs != null)
            //{
            //    dataContext.LoadArguments = loadArgs.PatientId;
                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.NoResize
                });
            if (!string.IsNullOrEmpty(selectedraces))
                demographicsContext.selectedraces = selectedraces;
            demographicsContext.Demo_Races = Temp_Races;
            demographicsContext.Existing_Races = Existing_Races;
        }


    }
}



