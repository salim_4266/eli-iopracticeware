﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.Views.Common.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using IO.Practiceware.Data;
using Soaf.Data;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.RaceFinder
{
    /// <summary>
    /// Interaction logic for ONCPsychologyView.xaml
    /// </summary>
    /// 
    
    public partial class RaceFinderView : IViewContext
    {
        public bool IsManipulated { get; set; }
        DataTable dtRace = new DataTable();
        List<string> item = new List<string>();
        List<int> RaceId = new List<int>();
        private bool IsSaved;
        ObservableCollection<Race> sel = new ObservableCollection<Race>();
        public string SavedItems { get; set; }
        public ICommand Close { get; protected set; }
        public RaceFinderViewManager RaceMgr { get; set; }
        public List<string> SavedSelection = new List<string>();
        public int PatientId { get; set; }
        public RaceFinderView(int patientId)
        {
            PatientId = patientId;
            InitializeComponent();
            this.DataContext = this;
            FillGrid(DataGetRace());
            Close = Command.Create(ExecuteClose).Async(() => InteractionContext);
        }
        //Handler/Methods
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent is valid.  
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child 
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree 
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child.  
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search 
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name 
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found. 
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }
        private void FillGrid(ObservableCollection<Race> race, ObservableCollection<Race> selRace = null)
        { 
            if(cmbAreas != null)
            {
                dgProducts.ItemsSource = race;
                cmbAreas.SelectedIndex = 0;
            }
        }

        private ObservableCollection<Race> DataGetRace()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(string));
            dataTable.Columns.Add("Code", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("OMBCode", typeof(string));
            dataTable.Columns.Add("OMBCategory", typeof(string));

            DataTable dtsel = new DataTable();
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                dataTable = dbConnection.Execute<DataTable>("Select Id,Code,OMBCode, OMBCategory,Name as Description From model.Races");
                string selectedRace = "Select * From model.PatientRace where patients_Id=@PatientId";
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@PatientId", PatientId);
                dtsel = dbConnection.Execute<DataTable>(selectedRace, param);
            }

            if (dataTable.Rows.Count > 0)
            {
                race = new ObservableCollection<Race>();
                foreach (DataRow row in dataTable.Rows)
                {
                    race.Add(new Race()
                    {
                        Id = (int)row["Id"],
                        Code = (string)row["code"],
                        Description = (string)row["description"],
                        OMBCategory = (string)row["OMBCategory"],
                        OMBCode = (string)row["OMBCode"]
                    });

                }
            }
            dtRace = dataTable;
            if (!IsManipulated)
            {
                if (dtsel != null && dtsel.Rows.Count > 0)
                {
                    foreach (DataRow row in dtsel.Rows)
                    {
                        foreach (Race item in race)
                        {
                            if (row["Races_Id"].ToString() == item.Id.ToString())
                            {
                                item.IsSelected = true;
                                SavedSelection.Add(item.Description);
                                break;
                            }
                        }
                    }
                }
            }
            OriginialRace = race;
            return race;
        }

        private void ShowSelectedRaces()
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.Races r inner join model.patienRace pr on r.id = pr.Races_Id where pr.Patients_Id = " + PatientId + " FOR xml path('')), 1, 2, '') as Description";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    SelectedGridItems.Text += dataTable.Rows[0]["Description"].ToString();
                }
            }
        }
        [DispatcherThread]
        public ObservableCollection<Race> race { get; set; }

        public ObservableCollection<Race> OriginialRace { get; set; }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FilterResults();
        }
        private void FilterResults()
        {
            sel.Clear();
            string filterArea = cmbAreas.SelectionBoxItem.ToString();
            string searchTText = search.Text;
            if (filterArea == "OMB Category")
                filterArea = "OMBCategory";
            if (filterArea == "OMB Code")
                filterArea = "OMBCode";
            DataRow[] dr = dtRace.Select(filterArea + " like '" + searchTText + "%'");
            if (dr.Length > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Code", typeof(string));
                dt.Columns.Add("Description", typeof(string));
                dt.Columns.Add("OMBCode", typeof(string));
                dt.Columns.Add("OMBCategory", typeof(string));

                foreach (DataRow row in dr)
                {
                    sel.Add(new Race()
                    {
                        Id = (int)row["Id"],
                        Code = (string)row["code"],
                        Description = (string)row["description"],
                        OMBCategory = (string)row["OMBCategory"],
                        OMBCode = (string)row["OMBCode"]
                    });
                }

                foreach (Race row in sel)
                {
                    foreach (Race item in race)
                    {
                        if (row.Id == item.Id)
                        {
                            row.IsSelected = item.IsSelected;
                            break;
                        }
                    }
                }
                FillGrid(sel);
            }
            else
                FillGrid(race, sel);
        }
        
        /// <summary>
        /// Returns the first ancester of specified type
        /// </summary>
        public static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            current = VisualTreeHelper.GetParent(current);
            while (current != null)
            {
                if (current is T)
                {
                    return (T)current;
                }

                current = VisualTreeHelper.GetParent(current);
            };
            return null;
        }
        private void chkRace_Checked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            CheckBox chk = (CheckBox)sender;
            bool? obj = chk.IsChecked;
            Race objRace;
            //DataRowView rv;
            DataGridRow row = FindAncestor<DataGridRow>(chk);

            if (obj == true)
            {
                objRace = (Race)row.Item;
                if (objRace.Description == "Declined to Specify")
                {
                    foreach (Race rc in race)
                    {
                        if (rc.Description != "Declined to Specify" && rc.IsSelected == true)
                        {
                            rc.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Race re in sel)
                        {
                            foreach (var itm in race)
                            {
                                if (re.Description == "Declined to Specify" && objRace.Description == re.Description)
                                {
                                    re.IsSelected = true;
                                }
                                else if (itm.Description != "Declined to Specify")
                                {
                                    re.IsSelected = false;
                                    itm.IsSelected = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (Race rcc in race)
                    {
                        if (rcc.Description == "Declined to Specify" && rcc.IsSelected == true)
                        {
                            rcc.IsSelected = false;
                        }
                    }
                    if (sel.Any())
                    {
                        foreach (Race ra in sel)
                        {
                            foreach (var itms in race)
                            {
                                if (ra.Description != "Declined to Specify" && ra.Description == itms.Description && objRace.Description== ra.Description)
                                {
                                    ra.IsSelected = true;
                                    itms.IsSelected = true;
                                }
                                else if (ra.Description == "Declined to Specify" && ra.Description == itms.Description)
                                {
                                    ra.IsSelected = false;
                                    itms.IsSelected = false;
                                }
                            }
                        }
                    }
                }
                if (!item.Contains(objRace.Description))
                {
                    item.Add(objRace.Description);
                    RaceId.Add(int.Parse(objRace.Id.ToString()));
                }
                foreach (Race rc in race)
                {
                    if (rc.Id == objRace.Id)
                    {
                        rc.IsSelected = true;
                        break;
                    }
                }
                SelectedGridItems.Text = string.Join(",", item.ToArray());
                SelectedGridItems.ToolTip= string.Join(",", item.ToArray());
                RaceMgr.selectedraces = SelectedGridItems.Text;
                SavedItems = SelectedGridItems.Text;
            }
        }

        private void chkRace_Unchecked(object sender, RoutedEventArgs e)
        {
            object a = e.Source;
            CheckBox chk = (CheckBox)sender;
            bool? obj = chk.IsChecked;
            Race objRace;
            //DataRowView rv;
            DataGridRow row = FindAncestor<DataGridRow>(chk);
            objRace = (Race)row.Item;

            //if(objRace.Description!="Declined to Specify")
            //{
            //    objRace
            //}

            if (item.Contains(objRace.Description))
            {
                item.Remove(objRace.Description);
                RaceId.Remove(int.Parse(objRace.Id.ToString()));
            }

            foreach (Race rce in race)
            {
                if (rce.Id == objRace.Id)
                {
                    rce.IsSelected = false;
                    break;
                }
            }
            objRace.IsSelected = false;
            SelectedGridItems.Text = string.Join(",", item.ToArray());
            SelectedGridItems.ToolTip = string.Join(",", item.ToArray());
            RaceMgr.selectedraces = SelectedGridItems.Text;
            SavedItems = SelectedGridItems.Text;
        }
        private void ExecuteClose()
        {
            if (IsSaved)
            {
                RaceMgr.selectedraces = SavedItems;
            }
            else
            {
                if (SavedSelection.Count > 0)
                    RaceMgr.selectedraces = string.Join(",", SavedSelection.ToArray());
            }
        }
        private void Save(object sender, RoutedEventArgs e)
        {
            if (PatientId > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    Dictionary<string, object> paramDel = new Dictionary<string, object>();
                    paramDel.Add("@PatientId", PatientId);

                    foreach (int item in RaceId)
                    {
                        if (item == 4072 && RaceId.Count > 1)
                        {
                            IInteractionManager intMgr = InteractionManager.Current;
                            intMgr.Alert("Cannot chose other options along with 'Declined to Specify' ELSE only chose Declined to Specify.");
                            return;
                        }
                    }
                    string queryDel = "delete from model.PatientRace where patients_Id=@PatientId";
                    dbConnection.Execute(queryDel, paramDel);

                    foreach (Race item in race)
                    {
                        if (item.IsSelected && item.Description == "Declined to Specify" && RaceId.Count == 1 && RaceId[0] == 4072)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("@PatientId", PatientId);
                            param.Add("@RaceId", item.Id);
                            string qryInsert = " Insert into model.PatientRace(Patients_Id,Races_Id) values (@PatientId,@RaceId) ";
                            dbConnection.Execute(qryInsert, param);
                            break;
                        }
                        else if (item.IsSelected)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("@PatientId", PatientId);
                            param.Add("@RaceId", item.Id);
                            string qry = " Insert into model.PatientRace(Patients_Id,Races_Id) values (@PatientId,@RaceId) ";
                            dbConnection.Execute(qry, param);
                        }
                    }
                    IsSaved = true;
                    RaceMgr.Existing_Races = new ObservableCollection<Race>();
                    foreach (var rc in race)
                    {
                        if (rc.IsSelected)
                        {
                            RaceMgr.Existing_Races.Add(rc);
                        }
                    }
                    //IInteractionManager Mgr = InteractionManager.Current;
                    //Mgr.Alert("Saved Successfully"); 
                }
            }
            else
            {
                RaceMgr.Temp_Races = new ObservableCollection<Race>();
                foreach (var rc in race)
                {
                    if (rc.IsSelected)
                    {
                        RaceMgr.Temp_Races.Add(rc);
                    }
                }
            }
            InteractionContext.Complete();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            if (IsSaved)
            {
                RaceMgr.selectedraces = SelectedGridItems.Text; 
            }
            this.InteractionContext.Complete();
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                FilterResults();
            }
        }
    }
}
