﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.ExternalProviders;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.Imaging;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.Imaging;
using IO.Practiceware.Services;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Soaf.Threading;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.ONCPsychology;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.EthnicityFinder;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.LanguageFinder;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.RaceFinder;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Data;
using IO.Practiceware.Utilities;
using System.Data;
using Soaf.Data;
using System.Data.SqlClient;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics
{
    /// <summary>
    /// Interaction logic for PatientDemographicsView.xaml
    /// </summary>
    public partial class PatientDemographicsView
    {
        public PatientDemographicsView()
        {
            InitializeComponent();
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    public class PatientDemographicsViewContext : IViewContext, IPatientInfoViewContext, IHasWizardDataContext, ICommonViewFeatureHandler
    {
        private readonly IPatientDemographicsViewService _patientDemographicsViewService;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly Func<PhoneNumberViewModel> _createPhoneNumberViewModel;
        private readonly Func<AddressViewModel> _createPatientAddressViewModel;
        private readonly Func<EmailAddressViewModel> _createPatientEmailAddressViewModel;
        private readonly Func<PatientReferralSourceViewModel> _createReferralSourceViewModel;
        private readonly Func<PhysicianInformationViewModel> _createPhysicianInfoViewModel;
        private readonly Func<PatientTagViewModel> _createPatientTagViewModel;
        private readonly Func<ImagingViewManager> _createImagingViewManager;
        private readonly Func<ExternalProvidersViewManager> _createExternalProviderViewManager;
        private readonly Func<PatientCommunicationPreferenceViewModel> _patientCommunicationPreferenceViewModel;
        private readonly CardScannerViewModelToPatientViewModelMapper _cardScannerDataToPatientMapper;
        readonly IInteractionManager _interactionManager;
        private readonly IImageService _imageService;
        private readonly IPatientInfoViewService _patientInfoViewService;
        private readonly IPatientCommunicationPreferencesViewService _patientCommunicationPreferenceViewService;
        private readonly IApplicationSettingsService _applicationViewService;
        private readonly ONCPsychologyViewManager _oncPsychologyViewManager;
        private readonly EthnicityFinderViewManager _ethnicityFinderViewManager;
        private readonly LanguageFinderViewManager _languageFinderViewManager;
        private readonly RaceFinderViewManager _raceFinderViewManager;
        private string clientKey = string.Empty;
        private readonly PatientRepresentativeViewManager _patRepresentativeViewManager;
        private bool portalPatientExists = false;

        public virtual bool IsAddressHighlight { get; set; }
        public virtual bool IsPhoneHighlight { get; set; }
        public virtual bool RaceRequired { get; set; }
        public virtual bool AddrReq { get; set; }
        public virtual bool PhoneReq { get; set; }
        public virtual bool EthnicityRequired { get; set; }
        public virtual bool LanguageRequired { get; set; }
        public string selectedethnicity { get; set; }
        public string selectedlanguage { get; set; }
        public string selectedraces { get; set; }

        public ObservableCollection<RaceFinder.Race> Demo_Races { get; set; }
        public ObservableCollection<RaceFinder.Race> Existing_Races { get; set; }
        public ObservableCollection<EthnicityFinder.Ethnicity> Demo_Ethnicities { get; set; }
        public ObservableCollection<EthnicityFinder.Ethnicity> Existing_Ethnicities { get; set; }
        public ObservableCollection<LanguageFinder.Language> Demo_Languages { get; set; }
        public ObservableCollection<LanguageFinder.Language> Existing_Languages { get; set; }
        public PatientDemographicsViewContext(IPatientDemographicsViewService patientDemographicsViewService,
                                              PatientDemographicsSetupViewManager setupViewManager,
                                              PatientInfoViewManager patientInfoViewManager,
                                              Func<PhoneNumberViewModel> createPhoneNumberViewModel,
                                              Func<AddressViewModel> createPatientAddressViewModel,
                                              Func<EmailAddressViewModel> createPatientEmailAddressViewModel,
                                              Func<PatientReferralSourceViewModel> createReferralSourceViewModel,
                                              Func<PhysicianInformationViewModel> createPhysicianInfoViewModel,
                                              Func<PatientTagViewModel> createPatientTagViewModel,
                                              ICommonViewFeatureExchange exchange,
                                              Func<ImagingViewManager> createImagingViewManager,
                                              Func<ExternalProvidersViewManager> createExternalProviderViewManager,
                                              Func<CardScannerViewModelToPatientViewModelMapper> createCardScannerDataToPatientMapper,
                                              IInteractionManager interactionManager,
                                              IImageService imageService,
                                              IPatientInfoViewService patientInfoViewService,
                                              IPatientCommunicationPreferencesViewService patientCommunicationPreferenceViewService,
                                              Func<PatientCommunicationPreferenceViewModel> patientCommunicationPreferenceViewModel,
                                              IApplicationSettingsService applicationViewService,
                                              Func<ONCPsychologyViewManager> createONCPsychologyViewManager,
                                              Func<EthnicityFinderViewManager> createEthnicityFinderViewManager,
                                              Func<LanguageFinderViewManager> createLanguageFinderViewManager,
                                              Func<RaceFinderViewManager> createRaceFinderViewManager,
                                              Func<PatientRepresentativeViewManager> createPatRepresentativeViewManager)
        {
            _patientDemographicsViewService = patientDemographicsViewService;
            _patientInfoViewManager = patientInfoViewManager;
            _createPhoneNumberViewModel = createPhoneNumberViewModel;
            _createPatientAddressViewModel = createPatientAddressViewModel;
            _createPatientEmailAddressViewModel = createPatientEmailAddressViewModel;
            _createReferralSourceViewModel = createReferralSourceViewModel;
            _createPhysicianInfoViewModel = createPhysicianInfoViewModel;

            _createPatientTagViewModel = createPatientTagViewModel;
            _createImagingViewManager = createImagingViewManager;
            _createExternalProviderViewManager = createExternalProviderViewManager;
            _cardScannerDataToPatientMapper = createCardScannerDataToPatientMapper();
            _interactionManager = interactionManager;
            _imageService = imageService;
            _patientInfoViewService = patientInfoViewService;
            _patientCommunicationPreferenceViewService = patientCommunicationPreferenceViewService;
            _patientCommunicationPreferenceViewModel = patientCommunicationPreferenceViewModel;
            _applicationViewService = applicationViewService;
            _oncPsychologyViewManager = createONCPsychologyViewManager();
            _ethnicityFinderViewManager = createEthnicityFinderViewManager();
            _languageFinderViewManager = createLanguageFinderViewManager();
            _raceFinderViewManager = createRaceFinderViewManager();
            _patRepresentativeViewManager = createPatRepresentativeViewManager();

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CanEventExecute).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel, CanEventExecute);
            ConfirmCancelChanges = Command.Create(ExecuteConfirmCancelChanges);
            ConfirmExitWithUnsavedChanges = Command.Create(ExecuteConfirmExitWithUnsavedChanges);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
            AddNewAddress = Command.Create(ExecuteAddNewAddress, CanEventExecute);
            AddNewEmailAddress = Command.Create(ExecuteAddNewEmailAddress, CanEventExecute);
            AddNewPhoneNumber = Command.Create(ExecuteAddNewPhoneNumber, CanEventExecute);
            AddNewOtherPhysician = Command.Create(ExecuteAddNewOtherPhysician, CanEventExecute).Async();
            AddNewReferralSource = Command.Create(ExecuteAddNewReferralSource, CanEventExecute);
            AddPatientTag = Command.Create<PatientTagViewModel>(ExecuteAddPatientTag, x => CanEventExecute() && PatientData.IfNotNull(pd => pd.Tags.IfNotNull(t => t.Count < PatientDemographicsViewViewModel.TagsList.Count)));
            DeleteAddress = Command.Create<AddressViewModel>(ExecuteDeleteAddress);
            DeleteEmailAddress = Command.Create<EmailAddressViewModel>(ExecuteDeleteEmailAddress);
            DeletePhoneNumber = Command.Create<PhoneNumberViewModel>(ExecuteDeletePhoneNumber);
            DeletePhysician = Command.Create<PhysicianInformationViewModel>(ExecuteDeletePhysician);
            DeleteReferralSource = Command.Create<PatientReferralSourceViewModel>(ExecuteDeleteReferralSource);
            DeletePatientTag = Command.Create<PatientTagViewModel>(ExecuteDeletePatientTag);
            RefreshAlertStatus = Command.Create(ExecuteRefreshAlertStatus).Async(() => InteractionContext);
            ShowCardScanner = Command.Create(ExecuteShowCardScanner);
            //RaceChecked = Command.Create<PatientRaceViewModel>(ExecuteRaceChecked);
            ValidateSocialSecurity = Command.Create(ExecuteValidateSocialSecurity, () => PatientData != null).Async(() => InteractionContext);
            AllowSocialSecurityConflict = Command.Create(ExecuteAllowSocialSecurityConflict);
            ShowExternalProviders = Command.Create(ExecuteShowExternalProviders);
            ActivatePatient = Command.Create(ExecuteActivatePatient);
            ONCPsychology = Command.Create(LoadONCPsychology);
            EthnicityFinder = Command.Create(LoadEthnicityFinder);
            LanguageFinder = Command.Create(LoadLanguageFinder);
            RaceFinder = Command.Create(LoadRaceFinder);
            Representative = Command.Create(LoadRepresentative);

            // Report and init supported features
            ViewFeatureExchange = exchange;
            ViewFeatureExchange.OpenSettings = setupViewManager.ShowPatientDemographicsSetup;
            ViewFeatureExchange.ShowPendingAlert = null; // will update on loadPatientDemographicsViewViewModel
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void LoadRaceFinder()
        {
            var loadArguments = new RaceFinderLoadArguments { PatientId = PatientInfoViewModel.PatientId == null ? -1 : PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() };
            _raceFinderViewManager.demographicsContext = this;
            _raceFinderViewManager.ShowRaceFinderFeedBack(loadArguments);
            RacesToolTip = selectedraces;
            if (!string.IsNullOrEmpty(selectedraces))
                SelectedRaces = selectedraces.Length <= 25 ? selectedraces : selectedraces.Length > 25 ? selectedraces.Substring(0, 25) + "..." : selectedraces.Substring(0, 25) + "...";
        }
        private void LoadLanguageFinder()
        {
            var loadArguments = new LanguageFinderLoadArguments { PatientId = PatientInfoViewModel.PatientId == null ? -1 : PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() };
            _languageFinderViewManager.demographicsContext = this;
            _languageFinderViewManager.ShowLanguageFinderFeedBack(loadArguments);
            LanguagesToolTip = selectedlanguage;
            if (!string.IsNullOrEmpty(selectedlanguage))
                SelectedLanguages = selectedlanguage.Length <= 25 ? selectedlanguage : selectedlanguage.Length > 25 ? selectedlanguage.Substring(0, 25) + "..." : selectedlanguage.Substring(0, 25) + "...";
        }
        private void LoadEthnicityFinder()
        {
            var loadArguments = new EthnicityFinderLoadArguments { PatientId = PatientInfoViewModel.PatientId == null ? -1 : PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() };
            _ethnicityFinderViewManager.demographicsContext = this;
            _ethnicityFinderViewManager.ShowEthnicityFinderFeedBack(loadArguments);
            EthnicitiesToolTip = selectedethnicity;
            if (!string.IsNullOrEmpty(selectedethnicity))
                SelectedEthnicities = selectedethnicity.Length <= 25 ? selectedethnicity : selectedethnicity.Length > 25 ? selectedethnicity.Substring(0, 25) + "..." : selectedethnicity.Substring(0, 25) + "...";
        }

        private void LoadRepresentative()
        {
            if (PatientInfoViewModel.PatientId != null)
            {
                var loadArguments = new RepresentativeLoadArguments { PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() };
                _patRepresentativeViewManager.ShowRepresentativeFeedBack(loadArguments);
            }
            else
            {
                _interactionManager.Alert("Please register this patient before adding representative");
                return;
            }
        }

        private void LoadONCPsychology()
        {
            if (PatientInfoViewModel.PatientId != null)
            {
                var loadArguments = new ONCLoadArguments { PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() };
                _oncPsychologyViewManager.ShowONCPsychologyFeedBack(loadArguments);
            }
            else
            {
                _interactionManager.Alert("Please register this patient before ONC Survey");
                return;
            }
        }

        private void ExecuteAllowSocialSecurityConflict()
        {
            PatientData.HasSocialSecurityConflict = false;
        }

        private void ExecuteValidateSocialSecurity()
        {
            if (!PatientData.Validate().Keys.Contains("SocialSecurityNumber"))
            {
                if (PatientData.SocialSecurityNumber.IsNotNullOrEmpty() && PatientData.SocialSecurityNumber.Length == 9)
                {
                    PatientsWithSocialSecurityConflict = _patientDemographicsViewService.GetPatientsBySocialSecurity(PatientData.SocialSecurityNumber).ToExtendedObservableCollection();
                    if (PatientsWithSocialSecurityConflict.Any() && PatientsWithSocialSecurityConflict.Any(p => p.Id != PatientData.Id))
                    {
                        PatientData.HasSocialSecurityConflict = true;
                        SocialSecurityConflictDataTrigger = true;
                    }
                    else
                    {
                        PatientData.HasSocialSecurityConflict = false;
                    }
                }
                else
                {
                    PatientData.HasSocialSecurityConflict = false;
                }
            }
        }


        //private void ExecuteRaceChecked(PatientRaceViewModel race)
        //{
        //    if (PatientData != null && PatientData.PatientRaces != null)
        //    {
        //        var patientRaces = PatientData.PatientRaces.ToList();

        //        if (patientRaces.Any(r => r.Id == race.Id)) // checked value is true (user checked)
        //        {
        //            patientRaces.RemoveAt(patientRaces.IndexOf(pr => pr.Id == race.Id));
        //        }
        //        else // checked value is false (user unchecked)
        //        {
        //            patientRaces.Add(race);
        //        }

        //        PatientData.PatientRaces = patientRaces.ToExtendedObservableCollection();
        //    }
        //}

        private void ExecuteShowCardScanner()
        {
            var returnArgs = _createImagingViewManager().ShowCardScannerView(PatientData.Id);

            if (returnArgs != null)
            {
                // map scanned values to PatientViewModel (don't use default ForEach extension method which surpresses property changes)
                returnArgs.ScannedCardData.ToList().ForEach(data => _cardScannerDataToPatientMapper.MapCardScannerViewModelToPatientViewModel(data, PatientData));

                var photo = returnArgs.ScannedCardData.FirstOrDefault(d => d.Key == CardScannerDataField.Photo);
                if (photo != null && photo.Value is byte[])
                {
                    PatientInfoViewModel.Photo = (byte[])photo.Value;
                }
            }
        }

        private void ExecuteShowExternalProviders()
        {
            _createExternalProviderViewManager().ShowExternalProviders();
        }

        public void ExecuteLoad()
        {
            // Force view mode if user doesn't have permission to edit
            // Otherwise select between editing of new patient vs existing patient
            var pageMode = PermissionId.EditPatientDemographics.PrincipalContextHasPermission()
                ? PatientInfoViewModel == null || !PatientInfoViewModel.PatientId.HasValue
                    ? PatientDemographicsPageMode.EditNewPatient
                    : PatientDemographicsPageMode.EditExistingPatient
                : PatientDemographicsPageMode.View;
            var loadInfo = _patientDemographicsViewService.LoadPatientDemographicsInformation(PatientInfoViewModel == null ? new int?() : PatientInfoViewModel.PatientId);

            var dataListsViewModel = loadInfo.PatientDemographicsDataLists;

            var patientViewModel = loadInfo.PatientViewModel;
            var applicationService = _applicationViewService.GetSetting<bool>("IsPortalActive");
            IsPortalActive = applicationService != null ? applicationService.Value : false;
            if (PatientInfoViewModel.PatientId != null)
            {
                portalPatientExists = _patientDemographicsViewService.IsPortalPatientExist(PatientInfoViewModel.PatientId.Value);
                if (portalPatientExists)
                {
                    RegistrationText = "Update to Patient Portal";
                }
                else
                {
                    RegistrationText = "Register Patient";
                }
            }
            else
            {
                RegistrationText = "Register Patient";
            }

            // todo - display archived values in the main list
            patientViewModel.PatientRaces = patientViewModel.PatientRaces.Select(pr => dataListsViewModel.Races.FirstOrDefault(pr.Equals)).WhereNotDefault().ToExtendedObservableCollection();

            // check the patient's default service location.  If one is not set then check the local computer's config file
            if (!patientViewModel.PreferredServiceLocationId.HasValue)
            {
                patientViewModel.PreferredServiceLocationId = UserContext.Current.ServiceLocation.Item1;
            }

            FixupViewModelDataForPresentation(patientViewModel);

            //Set page mode at the time of intial load only.
            if (PageMode == 0)
            {
                PageMode = pageMode;
            }

            // make sure to bind the list data sources first before binding the actual values.  This is the optimized way of binding.
            // if you do it the other way around, you'll force all of the lists to refresh themselves every time the selectedValue changes.
            // This can cause a huge performance issue, especially on a view like this which contains many lists (each with many items).
            PatientDemographicsViewViewModel = dataListsViewModel;
            PatientData = patientViewModel;

            // Begin loading other physicians in background
            System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(LoadOtherPhysicians);

            // If patient info view model set -> handle showing of pending alerts
            if (PatientInfoViewModel != null)
            {
                ViewFeatureExchange.ShowPendingAlert = () =>
                    _patientInfoViewManager.ShowPatientAlert(PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault(),
                        PatientAlertType.Demographics);
                // Refresh status of current alerts
                RefreshAlertStatus.Execute();
            }

            if (PatientInfoViewModel.PatientId != null)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.ethnicities e inner join model.patientethnicities pe on e.id = pe.ethnicityid where pe.patientid = " + PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() + " FOR xml path('')), 1, 2, '') as Description";
                    DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        SelectedEthnicities = dataTable.Rows[0]["Description"].ToString();
                        EthnicitiesToolTip = SelectedEthnicities;
                        if (!string.IsNullOrEmpty(SelectedEthnicities))
                            SelectedEthnicities = SelectedEthnicities.Length <= 25 ? SelectedEthnicities : SelectedEthnicities.Length > 25 ? SelectedEthnicities.Substring(0, 25) + "..." : SelectedEthnicities.Substring(0, 25) + "...";
                    }
                }
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.languages l inner join model.patientlanguages pl on l.id = pl.languageid where pl.patientid = " + PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() + " FOR xml path('')), 1, 2, '') as Description";
                    DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        SelectedLanguages = dataTable.Rows[0]["Description"].ToString();
                        LanguagesToolTip = SelectedLanguages;
                        if (!string.IsNullOrEmpty(SelectedLanguages))
                            SelectedLanguages = SelectedLanguages.Length <= 25 ? SelectedLanguages : SelectedLanguages.Length > 25 ? SelectedLanguages.Substring(0, 25) + "..." : SelectedLanguages.Substring(0, 25) + "...";
                    }
                }
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string slctqry = "select Stuff((SELECT DISTINCT ', ' + name FROM model.races r inner join model.patientrace pr on r.id = pr.Races_Id where pr.Patients_Id = " + PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault() + " FOR xml path('')), 1, 2, '') as Description";
                    DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        SelectedRaces = dataTable.Rows[0]["Description"].ToString();
                        RacesToolTip = SelectedRaces;
                        if (!string.IsNullOrEmpty(SelectedRaces))
                            SelectedRaces = SelectedRaces.Length <= 25 ? SelectedRaces : SelectedRaces.Length > 25 ? SelectedRaces.Substring(0, 25) + "..." : SelectedRaces.Substring(0, 25) + "...";
                    }
                }
                this.Dispatcher().Invoke(() => PatientData.As<IEditableObjectExtended>().InitiateCustomEdit());
            }
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Isrequired from [model].[PatientDemographicsFieldConfigurations] where Id=16";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0 && string.IsNullOrEmpty(SelectedRaces))
                {
                    RaceRequired = bool.Parse(dataTable.Rows[0]["Isrequired"].ToString());
                }
            }
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Isrequired from [model].[PatientDemographicsFieldConfigurations] where Id=17";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0 && string.IsNullOrEmpty(selectedethnicity))
                {
                    EthnicityRequired = bool.Parse(dataTable.Rows[0]["Isrequired"].ToString());
                }
            }
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Isrequired from [model].[PatientDemographicsFieldConfigurations] where Id=18";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0 && string.IsNullOrEmpty(SelectedLanguages))
                {
                    LanguageRequired = bool.Parse(dataTable.Rows[0]["Isrequired"].ToString());
                }
            }

            //Address & Phone Highlight on Load
            #region MyRegion
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Id as Id ,IsVisibilityConfigurable as IsVisibilityConfigurable,IsVisible as IsVisible,IsRequiredConfigurable as IsRequiredConfigurable,IsRequired as IsRequired from model.PatientDemographicsFieldConfigurations where id=11"; //in (10,11)";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        //if (int.Parse(dr["Id"].ToString()) == 10 && dr["IsVisibilityConfigurable"].ToString() == "False" && dr["IsVisible"].ToString() == "True" && dr["IsRequiredConfigurable"].ToString() == "False" && dr["IsRequired"].ToString() == "True")
                        //{
                            if (PatientData.Addresses != null && PatientData.Addresses.Count > 0)
                            {
                                foreach (var add in PatientData.Addresses)
                                {
                                    if (string.IsNullOrEmpty(add.Line1) && string.IsNullOrEmpty(add.City) && string.IsNullOrEmpty(add.PostalCode) && add.StateOrProvince == null)
                                    {
                                        IsAddressHighlight = true;
                                    }
                                    else
                                    {
                                        IsAddressHighlight = false;
                                    }
                                }
                            }
                            else if (PatientData.Addresses != null && PatientData.Addresses.Count == 0)
                            {
                                AddrReq = true;
                                IsAddressHighlight = true;
                            }
                        //}
                        //else 
                        if (int.Parse(dr["Id"].ToString()) == 11 && dr["IsVisibilityConfigurable"].ToString() == "False" && dr["IsVisible"].ToString() == "True" && dr["IsRequiredConfigurable"].ToString() == "False" && dr["IsRequired"].ToString() == "True")
                        {
                            if (PatientData.PhoneNumbers != null && PatientData.PhoneNumbers.Count > 0)
                            {
                                foreach (var phn in PatientData.PhoneNumbers)
                                {
                                    if (string.IsNullOrEmpty(phn.Extension) && string.IsNullOrEmpty(phn.AreaCode) && string.IsNullOrEmpty(phn.ExchangeAndSuffix) && string.IsNullOrEmpty(phn.PhoneNumber) && phn.PhoneType == null)
                                    {
                                        IsPhoneHighlight = true;
                                    }
                                    else
                                    {
                                        IsPhoneHighlight = false;
                                    }
                                }
                            }
                            else if (PatientData.PhoneNumbers != null && PatientData.PhoneNumbers.Count == 0)
                            {
                                PhoneReq = true;
                                IsPhoneHighlight = true;
                            }
                        }
                    }
                }
            }
            #endregion
        }

        private bool CanEventExecute()
        {
            return IsEditMode;
        }

        private void ExecuteAddNewReferralSource()
        {
            PatientData.ReferralSources.Add(_createReferralSourceViewModel());
        }

        private void ExecuteAddNewOtherPhysician()
        {
            PatientData.ExternalPhysicians.Add(_createPhysicianInfoViewModel());
        }

        private void LoadOtherPhysicians()
        {
            var patientViewModel = PatientData;
            var dataListsViewModel = PatientDemographicsViewViewModel;

            const int loadingModelId = -100;
            var loadingProvider = new ObservableCollection<NameAndAbbreviationAndDetailViewModel>
                {
                    new NameAndAbbreviationAndDetailViewModel
                    {
                        Id = loadingModelId,
                        Name = "Loading..."
                    }
                };

            dataListsViewModel.ExternalProviders = loadingProvider;
            dataListsViewModel.ExternalProviders = _patientDemographicsViewService.LoadExternalProviders().ToObservableCollection();

            // In case "Loading..." physician was accidentally selected
            foreach (var physician in patientViewModel.ExternalPhysicians
                .Where(p => p.Physician.IfNotNull(t => t.Id == loadingModelId)))
            {
                physician.Physician = null;
            }
        }

        private void ExecuteAddNewPhoneNumber()
        {
            PatientData.PhoneNumbers.Add(_createPhoneNumberViewModel());
            if (PatientData.PhoneNumbers.Count == 1) PatientData.PhoneNumbers.First().IsPrimary = true;
        }

        private void ExecuteAddNewEmailAddress()
        {
            PatientData.EmailAddresses.Add(_createPatientEmailAddressViewModel());
            if (PatientData.EmailAddresses.Count == 1) PatientData.EmailAddresses.First().IsPrimary = true;
        }

        private void ExecuteAddNewAddress()
        {
            PatientData.Addresses.Add(_createPatientAddressViewModel());
            if (PatientData.Addresses.Count == 1)
            {
                PatientData.Addresses.First().IsPrimary = true;
                PatientData.Addresses.First().AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Home, Name = PatientAddressTypeId.Home.ToString() };
            }
        }

        private void ExecuteDeletePhysician(PhysicianInformationViewModel physicianViewModel)
        {
            PatientData.ExternalPhysicians.Remove(physicianViewModel);
        }

        private void ExecuteDeletePhoneNumber(PhoneNumberViewModel phoneNumberViewModel)
        {
            ExecuteDeletePatCommunicationPreferences(phoneNumberViewModel.Id);
            PatientData.PhoneNumbers.Remove(phoneNumberViewModel);
            if (phoneNumberViewModel.IsPrimary)
                PatientData.PhoneNumbers.FirstOrDefault().IfNotNull(x => x.IsPrimary = true);
        }


        private void ExecuteDeletePatCommunicationPreferences(int? Id)
        {
            if (Id != null)
            {
                var query = "Delete from Model.PatientCommunicationPreferences where PatientPhoneNumberId =" +Id;
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("@Id", Id);
                    dbConnection.Execute(query);

                }
            }
        }
        private void ExecuteDeleteEmailAddress(EmailAddressViewModel emailViewModel)
        {
            PatientData.EmailAddresses.Remove(emailViewModel);
            if (emailViewModel.IsPrimary)
                PatientData.EmailAddresses.FirstOrDefault().IfNotNull(x => x.IsPrimary = true);
        }

        private void ExecuteDeleteAddress(AddressViewModel addressViewModel)
        {
            PatientData.Addresses.Remove(addressViewModel);
            if (addressViewModel.IsPrimary)
                PatientData.Addresses.FirstOrDefault().IfNotNull(x => x.IsPrimary = true);
        }

        private void ExecuteDeleteReferralSource(PatientReferralSourceViewModel referralViewModel)
        {
            if (referralViewModel.IsEmpty) PatientData.ReferralSources.Remove(referralViewModel);
            else
            {
                // Confirm whether to Delete them
                bool? deleteReferralsource = false;
                this.Dispatcher().Invoke(() =>
                {
                    deleteReferralsource = _interactionManager.Confirm("Are you sure you wish to delete this referral source?", "Cancel", "Delete");
                });

                // Yes?
                if (deleteReferralsource.GetValueOrDefault())
                {
                    // delete
                    PatientData.ReferralSources.Remove(referralViewModel);
                }
            }
        }

        private void ExecuteDeletePatientTag(PatientTagViewModel patientTagViewModel)
        {
            if (patientTagViewModel == null) return;

            PatientData.Tags.Remove(patientTagViewModel);
            PatientData.Tags = PatientData.Tags.ToObservableCollection();

            if (patientTagViewModel.IsPrimary)
            {
                PatientData.Tags.FirstOrDefault().IfNotNull(x => x.IsPrimary = true);
            }
        }

        private void ExecuteCancel()
        {
            // check if user made changes
            if (PatientData.As<IEditableObjectExtended>().IsChanged)
            {
                // trigger pop up to confirm they really want to cancel
                ConfirmCancelEditsTrigger = true;
                return;
            }

            if (IsNewPatientMode)
            {
                // for new patients only, we want to close the PatientInfo view altogether. There is no view mode to go back to.
                InteractionContext.Complete();
            }
        }

        private void ExecuteConfirmCancelChanges()
        {
            PatientData.As<IEditableObjectExtended>().CancelCustomEdits();
        }

        private void ExecuteSave()
        {
            //Capture Previous EmailId
            PreviousEmailAddress = string.Empty;
            UpdatedEmailAddress = string.Empty;
            if (PatientInfoViewModel.PatientId!=null)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string slctqry = "Select Top(1) P.Id as PatientId, pd.email as EmailAddress From model.Patients P inner join patientdemographics pd on p.id=pd.patientid where pd.PatientId=" + PatientInfoViewModel.PatientId;
                    DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        PreviousEmailAddress = dataTable.Rows[0]["EmailAddress"].ToString();
                    }
                } 
            }
            var patientId = SavePatient();

            if (!patientId.HasValue) return;

            // if Photo was changed (from the ocr service most likely) then save it
            if (PatientInfoViewModel != null && PatientInfoViewModel.Photo != null && PatientInfoViewModel.CastTo<IChangeTracking>().IsChanged)
            {
                // need to save photo, since it may have changed
                _imageService.SavePatientPhoto(patientId.Value, PatientInfoViewModel.Photo);
            }

            if (PatientInfoViewModel != null && PageMode == PatientDemographicsPageMode.EditNewPatient)
            {
                PatientInfoViewModel.PatientId = patientId.Value;
            }

            // once we've saved the patient from this screen once, they are forever clinical (this allows access to other funcionality in patient info)
            if (PatientInfoViewModel != null) PatientInfoViewModel.IsClinical = true;

            #region Race_Save
            if (Demo_Races != null && Demo_Races.Count > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    DataTable dtRace = dbConnection.Execute<DataTable>("select count(Patients_Id) as count from model.PatientRace where patients_Id=" + patientId);

                    if (dtRace != null && dtRace.Rows.Count > 0)
                    {
                        if (dtRace.Rows[0]["count"].ToString() == "0")
                        {
                            if (patientId > 0)
                            {
                                foreach (var item in Demo_Races)
                                {
                                    Dictionary<string, object> param = new Dictionary<string, object>();
                                    param.Add("@PatientId", patientId);
                                    param.Add("@RaceId", item.Id);
                                    string qry = " Insert into model.PatientRace(Patients_Id,Races_Id) values (@PatientId,@RaceId) ";
                                    dbConnection.Execute(qry, param);
                                }
                            }
                        }
                    }
                }
                Demo_Races.Clear();
            }
            #endregion

            #region Ethnicity_Save
            if (Demo_Ethnicities != null && Demo_Ethnicities.Count > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    DataTable dtEthnicity = dbConnection.Execute<DataTable>("select count(PatientId) as count from model.PatientEthnicities where PatientId=" + patientId);

                    if (dtEthnicity != null && dtEthnicity.Rows.Count > 0)
                    {
                        if (dtEthnicity.Rows[0]["count"].ToString() == "0")
                        {
                            if (patientId > 0)
                            {
                                foreach (var item in Demo_Ethnicities)
                                {
                                    Dictionary<string, object> param = new Dictionary<string, object>();
                                    param.Add("@PatientId", patientId);
                                    param.Add("@EthnicityId", item.Id);
                                    string qry = "Insert into model.PatientEthnicities(PatientId,EthnicityId) values (@PatientId,@EthnicityId)";
                                    dbConnection.Execute(qry, param);
                                }
                            }
                        }
                    }
                }
                Demo_Ethnicities.Clear();
            }
            #endregion

            #region Language_Save
            if (Demo_Languages != null && Demo_Languages.Count > 0)
            {
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    DataTable dtLanguage = dbConnection.Execute<DataTable>("select count(PatientId) as count from model.PatientLanguages where PatientId=" + patientId);

                    if (dtLanguage != null && dtLanguage.Rows.Count > 0)
                    {
                        if (dtLanguage.Rows[0]["count"].ToString() == "0")
                        {
                            if (patientId > 0)
                            {
                                foreach (var item in Demo_Languages)
                                {
                                    Dictionary<string, object> param = new Dictionary<string, object>();
                                    param.Add("@PatientId", patientId);
                                    param.Add("@LanguageId", item.Id);
                                    string qry = "Insert into model.PatientLanguages(PatientId,LanguageId) values (@PatientId,@LanguageId)";
                                    dbConnection.Execute(qry, param);
                                }
                            }
                        }
                    }
                }
                Demo_Languages.Clear();
            }
            #endregion
            ExecuteLoad();
            
            ReloadPatientInfo.Execute(patientId);

            //Save the prefernce for Recall and Statement on new patient.
            if (IsNewPatientMode)
            {
                PatientCommunicationType[] communctionTypes = { PatientCommunicationType.Recall, PatientCommunicationType.Statement, PatientCommunicationType.Collection };
                _patientCommunicationPreferenceViewService.SaveAllPatientCommunicationPreferences(patientId.Value, ExecuteAddDefaultPreference(communctionTypes), string.Empty);
            }

            if (PageMode != PatientDemographicsPageMode.EditExistingPatient)
            {
                CredentialsEntity objCredentails = PatientActivationNew(patientId.Value, portalPatientExists);
                if (objCredentails._status != null && (objCredentails._status.Equals(ResponseStatus.Success.ToString()) || objCredentails._status.Equals(ResponseStatus.Updated.ToString())))
                {
                    if (PreviousEmailAddress != UpdatedEmailAddress || IsOldPatient == false)
                    {
                        portalPatientExists = true;
                        if (!(PreviousEmailAddress != "" && UpdatedEmailAddress == ""))
                        {
                            UtilitiesViewManager.OpenCredentialConfirmation(objCredentails._externalId, objCredentails._practiceToken, objCredentails._userName, objCredentails._password, PatientInfoViewModel.Name, "Patient", objCredentails._isemailaddress);
                        }
                    }
                }
            }

            //Reset the page mode once the patient has been saved.
            PageMode = PatientDemographicsPageMode.EditExistingPatient;
        }

        private IEnumerable<PatientCommunicationPreferenceViewModel> ExecuteAddDefaultPreference(PatientCommunicationType[] communcationTypes)
        {
            var tempPreferenceList = new List<PatientCommunicationPreferenceViewModel>();
            for (int i = 0; i < communcationTypes.Length; i++)
            {
                {
                    var patientCommunicationPreferenceViewModel = _patientCommunicationPreferenceViewModel();
                    patientCommunicationPreferenceViewModel.IsDefault = true;
                    patientCommunicationPreferenceViewModel.CommunicationType = communcationTypes[i];
                    patientCommunicationPreferenceViewModel.Method = CommunicationMethodType.Letter;
                    patientCommunicationPreferenceViewModel.PatientContact = new PatientContactViewModel
                    {
                        PatientId = PatientData.Id,
                        Addresses = PatientData.Addresses,
                        EmailAddresses = PatientData.EmailAddresses,
                        PhoneNumbers = PatientData.PhoneNumbers
                    };
                    if (patientCommunicationPreferenceViewModel.PatientContact.MainAddress != null &&
                        patientCommunicationPreferenceViewModel.PatientContact.MainAddress.Id.HasValue)
                    {
                        patientCommunicationPreferenceViewModel.DestinationId = patientCommunicationPreferenceViewModel.PatientContact.MainAddress.Id;
                    }
                    tempPreferenceList.Add(patientCommunicationPreferenceViewModel);
                }
            }
            return tempPreferenceList.OrderByCommunicationTypeOrdering().ToExtendedObservableCollection();
        }

        private void ExecuteActivatePatient()
        {
            string status = string.Empty;
            if (PatientInfoViewModel.PatientId.HasValue)
            {
                if (!PatientData.IsValid())
                {
                    _interactionManager.Alert("Please enter emailaddress, DOB, Gender and Save. Then after click Activate");
                }
                else
                {
                    CredentialsEntity objCredentails = PatientActivationNew(PatientInfoViewModel.PatientId.Value, portalPatientExists);
                    if (objCredentails._status.Equals("Information is missing"))
                    {
                        _interactionManager.Alert("A Patient must have email address, DOB, Gender. Please enter and save these details first and then try to register the Patient");
                    }
                    else if (objCredentails._status.Equals(ResponseStatus.Success.ToString()))
                    {
                        portalPatientExists = true;
                        RegistrationText = "Update to Patient Portal";
                        UtilitiesViewManager.OpenCredentialConfirmation(objCredentails._externalId, objCredentails._practiceToken, objCredentails._userName, objCredentails._password, PatientInfoViewModel.Name, "Patient",objCredentails._isemailaddress);
                    }
                    else if (objCredentails._status.Equals(ResponseStatus.Updated.ToString()))
                    {
                        portalPatientExists = true;
                        _interactionManager.Alert("Demographics information has been updated to Patient Portal");
                        RegistrationText = "Update to Patient Portal";
                    }
                    else
                    {
                        _interactionManager.Alert("There is some issue while trying to register this Patient, Please contact IO Practiceware support");
                    }
                }
            }
            else
            {
                _interactionManager.Alert("Please save all required details and then only you can register this patient");
            }
        }

        private CredentialsEntity PatientActivationNew(int patientId, bool isPatientExists)
        {
            CredentialsEntity objCredentials = new CredentialsEntity();
            string _practiceToken = string.Empty;
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select Cast(Value as nvarchar(100)) From model.ApplicationSettings Where Name = 'PracticeAuthToken'");
                _practiceToken = dbConnection.Execute<string>(cmdQuery);
            }

            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                DataTable dtable = new DataTable();
                var command = dbConnection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                dtable = dbConnection.Execute<DataTable>(@"MVE.PP_GetPatientDetailById",
                    new Dictionary<string, object>
                    {
                        { "PatientId", patientId }
                    }
                    , createCommand: () => command);
                
                if (dtable.Rows.Count > 0)
                {
                    PatientEntity parameters = new PatientEntity
                    {
                        PracticeToken = _practiceToken,
                        ExternalId = dtable.Rows[0]["ExternalId"].ToString(),
                        FirstName = dtable.Rows[0]["FirstName"].ToString(),
                        LastName = dtable.Rows[0]["LastName"].ToString(),
                        MiddleName = dtable.Rows[0]["MiddleName"].ToString(),
                        Suffix = dtable.Rows[0]["Suffix"].ToString(),
                        Prefix = dtable.Rows[0]["Prefix"].ToString(),
                        LocationID = dtable.Rows[0]["LocationID"].ToString(),
                        EmailAddresses = dtable.Rows[0]["EmailAddresses"].ToString(),
                        PhoneNumbers = dtable.Rows[0]["PhoneNumbers"].ToString(),
                        Address1 = dtable.Rows[0]["Address1"].ToString(),
                        Address2 = dtable.Rows[0]["Address2"].ToString(),
                        City = dtable.Rows[0]["City"].ToString(),
                        State = dtable.Rows[0]["State"].ToString(),
                        Country = dtable.Rows[0]["Country"].ToString(),
                        Zip = dtable.Rows[0]["Zip"].ToString(),
                        BirthDate = dtable.Rows[0]["BirthDate"].ToString(),
                        Notes = dtable.Rows[0]["Notes"].ToString(),
                        LanguageName = dtable.Rows[0]["LanguageName"].ToString(),
                        UserName = dtable.Rows[0]["UserName"].ToString(),
                        Password = dtable.Rows[0]["Password"].ToString(),
                        Gender = dtable.Rows[0]["Sex"].ToString(),
                        Race=dtable.Rows[0]["Race"].ToString(),
                        Ethnicity= dtable.Rows[0]["Ethnicity"].ToString(),
                        MaritalStatus= dtable.Rows[0]["MaritalStatus"].ToString()
                    };
                    var portalSvc = new PortalServices();
                    if (isPatientExists)
                    {
                        objCredentials._status = portalSvc.UpdateDemographics(parameters);
                        objCredentials._externalId = patientId.ToString();
                        objCredentials._practiceToken = parameters.PracticeToken;
                        objCredentials._userName = parameters.UserName;
                        objCredentials._password = parameters.Password;
                        if (!string.IsNullOrEmpty(parameters.EmailAddresses))
                        {
                            objCredentials._isemailaddress = true;
                        }
                        else
                        {
                            objCredentials._isemailaddress = false;
                        }
                    }
                    else
                    {
                        objCredentials._status = portalSvc.RegisterPatient(parameters);
                        objCredentials._externalId = patientId.ToString();
                        objCredentials._practiceToken = parameters.PracticeToken;
                        objCredentials._userName = parameters.UserName;
                        objCredentials._password = parameters.Password;
                        if (!string.IsNullOrEmpty(parameters.EmailAddresses))
                        {
                            objCredentials._isemailaddress = true;
                        }
                        else
                        {
                            objCredentials._isemailaddress = false;
                        }
                    }
                    dtable.Dispose();
                }
                else
                {
                    objCredentials._status = "Information is missing";
                }
            }
            return objCredentials;
        }

        internal int? SavePatient()
        {
            RemoveEmptyCollections();
            RemoveDuplicateCollections();

            if (PatientData.PatientRaces.Count == 0)
            {
                ObservableCollection<PatientRaceViewModel> races = new ObservableCollection<PatientRaceViewModel>();
                if (Demo_Races != null && Demo_Races.Count > 0)
                {
                    foreach (var race in Demo_Races)
                    {
                        races.Add(new PatientRaceViewModel { Id = race.Id, Name = race.Description });
                    }
                    PatientData.PatientRaces = races;
                }
                else if (Existing_Races != null && Existing_Races.Count > 0)
                {
                    foreach (var race in Existing_Races)
                    {
                        races.Add(new PatientRaceViewModel { Id = race.Id, Name = race.Description });
                    }
                    PatientData.PatientRaces = races;
                }
            }
            if (PatientData.LanguageId == null)
            {
                if (Demo_Languages != null && Demo_Languages.Count > 0)
                {
                    foreach (var lng in Demo_Languages)
                    {
                        PatientData.LanguageId = lng.Id;
                    }
                }
                else if (Existing_Languages != null && Existing_Languages.Count > 0)
                {
                    foreach (var lng in Existing_Languages)
                    {
                        PatientData.LanguageId = lng.Id;
                    }
                }
            }
            if (PatientData.EthnicityId == null)
            {
                if (Demo_Ethnicities != null && Demo_Ethnicities.Count > 0)
                {
                    foreach (var ethn in Demo_Ethnicities)
                    {
                        PatientData.EthnicityId = ethn.Id;
                    }
                }
                else if (Existing_Ethnicities != null && Existing_Ethnicities.Count > 0)
                {
                    foreach (var ethn in Existing_Ethnicities)
                    {
                        PatientData.EthnicityId = ethn.Id;
                    }
                }
            }

            if (PatientData.PhoneNumbers != null)
            {
                foreach (var numbers in PatientData.PhoneNumbers)
                {
                    if (numbers.AreaCode != null && numbers.ExchangeAndSuffix != null)
                    {
                        int PhoneNumberLength = numbers.AreaCode.Length + numbers.ExchangeAndSuffix.ToString().Replace("-", "").Length;
                        if (PhoneNumberLength != 10)
                        {
                            _interactionManager.Alert("Area code and Number should be of 10 digits!");
                            return null;
                        }
                    }
                }
            }

            #region MyRegion
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select Id as Id ,IsVisibilityConfigurable as IsVisibilityConfigurable,IsVisible as IsVisible,IsRequiredConfigurable as IsRequiredConfigurable,IsRequired as IsRequired from model.PatientDemographicsFieldConfigurations where id=11"; //in (10,11)";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        //if (int.Parse(dr["Id"].ToString()) == 10 && dr["IsVisibilityConfigurable"].ToString() == "False" && dr["IsVisible"].ToString() == "True" && dr["IsRequiredConfigurable"].ToString() == "False" && dr["IsRequired"].ToString() == "True")
                        //{
                            if (PatientData.Addresses != null && PatientData.Addresses.Count == 0)
                            {
                                AddrReq = true;
                                // break;
                            }
                            else
                            {
                                AddrReq = false;
                            }
                        //}
                        //else
                        if (int.Parse(dr["Id"].ToString()) == 11 && dr["IsVisibilityConfigurable"].ToString() == "False" && dr["IsVisible"].ToString() == "True" && dr["IsRequiredConfigurable"].ToString() == "False" && dr["IsRequired"].ToString() == "True")
                        {
                            if (PatientData.PhoneNumbers != null && PatientData.PhoneNumbers.Count == 0)
                            {
                                PhoneReq = true;
                            }
                            else
                            {
                                PhoneReq = false;
                            }
                        }
                    }
                }
            }
            #endregion

            if (AddrReq)
            {
                ValidationWarningTrigger = true;
                return null;
            }
            if (PhoneReq)
            {
                ValidationWarningTrigger = true;
                return null;
            }

            // validate the object
            if (!PatientData.IsValid())
            {
                // trigger alert informing the user that there are invalid fields and save cannot proceed
                ValidationWarningTrigger = true;
                return null;
            }
            if(PatientData.Id>0)
            {
                IsOldPatient = true;
            }

            return _patientDemographicsViewService.SavePatientDemographicsInformation(PatientData);
        }

        void RemoveDuplicateCollections()
        {
            PatientData.EmailAddresses = PatientData.EmailAddresses.GroupBy(a => new { a.Value, a.EmailAddressType }).Select(g => g.First()).ToExtendedObservableCollection();
            if (PatientData.EmailAddresses != null && PatientData.EmailAddresses.Count > 0)
            {
                UpdatedEmailAddress = PatientData.EmailAddresses[0].Value;
            }
        }

        void RemoveEmptyCollections()
        {
            PatientData.Addresses.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => PatientData.Addresses.Remove(a));
            PatientData.PhoneNumbers.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => PatientData.PhoneNumbers.Remove(a));
            PatientData.EmailAddresses.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => PatientData.EmailAddresses.Remove(a));
            PatientData.ExternalPhysicians.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => PatientData.ExternalPhysicians.Remove(a));
            PatientData.ReferralSources.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => PatientData.ReferralSources.Remove(a));
        }
        void ExecuteRefreshAlertStatus()
        {
            if (PatientInfoViewModel != null && PatientInfoViewModel.PatientId.HasValue)
            {
                ViewFeatureExchange.HasPendingAlert = _patientInfoViewService.CheckHasPendingAlert(
                    PatientInfoViewModel.PatientId.Value, (int)PatientAlertType.Demographics);
            }
        }

        private void FixupViewModelDataForPresentation(PatientViewModel patientViewModel)
        {
            // don't add default phones/emails/etc if it's not a new patient
            if (patientViewModel.Id <= 0)
            {
                if (patientViewModel.Addresses.IsNullOrEmpty())
                {
                    var a = _createPatientAddressViewModel();
                    a.AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Home, Name = PatientAddressTypeId.Home.ToString() };
                    a.IsPrimary = true;
                    patientViewModel.Addresses = new List<AddressViewModel> { a }.ToExtendedObservableCollection();
                }

                if (patientViewModel.PhoneNumbers.IsNullOrEmpty())
                {
                    var p = _createPhoneNumberViewModel();
                    p.IsPrimary = true;
                    patientViewModel.PhoneNumbers = new List<PhoneNumberViewModel> { p }.ToExtendedObservableCollection();
                }

                if (patientViewModel.EmailAddresses.IsNullOrEmpty())
                {
                    var e = _createPatientEmailAddressViewModel();
                    e.IsPrimary = true;
                    e.PatientId = patientViewModel.Id;
                    patientViewModel.EmailAddresses = new List<EmailAddressViewModel> { e }.ToExtendedObservableCollection();
                }

                if (patientViewModel.ReferralSources.IsNullOrEmpty())
                {
                    patientViewModel.ReferralSources = new List<PatientReferralSourceViewModel> { _createReferralSourceViewModel() }.ToExtendedObservableCollection();
                }

                if (patientViewModel.ExternalPhysicians.IsNullOrEmpty())
                {
                    patientViewModel.ExternalPhysicians = new List<PhysicianInformationViewModel> { _createPhysicianInfoViewModel() }.ToExtendedObservableCollection();

                    var p = _createPhysicianInfoViewModel();
                    p.IsPcp = true;
                    patientViewModel.ExternalPhysicians.Add(p);
                }
            }
        }

        private void ExecuteAddPatientTag(PatientTagViewModel tag)
        {
            if (tag == null || tag.Id == 0) return;

            // check the selected tag the user wants to add doesn't already exist in the patient's list of tags
            if (PatientData.Tags.All(x => x.TagId != tag.Id))
            {
                var newTag = _createPatientTagViewModel();
                newTag.TagId = tag.Id;
                newTag.Name = tag.Name;
                newTag.IsPrimary = PatientData.Tags.Count == 0;
                newTag.HexColor = tag.HexColor;
                PatientData.Tags.Add(newTag);
            }
            PatientData.Tags = PatientData.Tags.ToObservableCollection();
        }

        public virtual bool PatientHasRaceId(int raceId)
        {
            return PatientData.PatientRaces.Any(pr => pr.Id == raceId);
        }
        public virtual string PreviousEmailAddress { get; set; }
        public virtual string UpdatedEmailAddress { get; set; }
        public bool IsOldPatient { get; set; }

        [DispatcherThread]
        public virtual string SelectedEthnicities { get; set; }
        [DispatcherThread]
        public virtual string SelectedLanguages { get; set; }
        [DispatcherThread]
        public virtual string SelectedRaces { get; set; }
        [DispatcherThread]
        public virtual string EthnicitiesToolTip { get; set; }
        [DispatcherThread]
        public virtual string LanguagesToolTip { get; set; }
        [DispatcherThread]
        public virtual string RacesToolTip { get; set; }

        [DispatcherThread]
        public virtual PatientViewModel PatientData { get; set; }

        [DispatcherThread]
        public virtual string RegistrationText { get; set; }

        [DispatcherThread]
        public virtual PatientDemographicsDataListsViewModel PatientDemographicsViewViewModel { get; set; }

        [DispatcherThread]
        public virtual bool IsPortalActive { get; set; }

        [DispatcherThread]
        public virtual PatientInfoViewModel PatientInfoViewModel { get; set; }

        [DispatcherThread]
        public virtual PatientDemographicsPageMode PageMode { get; set; }

        [DispatcherThread]
        [DependsOn("PageMode")]
        public virtual bool IsNewPatientMode
        {
            get { return PageMode == PatientDemographicsPageMode.EditNewPatient; }
        }

        [DispatcherThread]
        [DependsOn("PageMode")]
        public virtual bool IsEditMode
        {
            get { return PageMode == PatientDemographicsPageMode.EditNewPatient || PageMode == PatientDemographicsPageMode.EditExistingPatient; }
        }

        [DispatcherThread]
        public virtual bool ConfirmCancelEditsTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ConfirmExitWithUnsavedChangesTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ValidationWarningTrigger { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<PatientViewModel> PatientsWithSocialSecurityConflict { get; set; }

        [DispatcherThread]
        public virtual bool SocialSecurityConflictDataTrigger { get; set; }

        /// <summary>
        /// This list is synced between the Patient's "Tags" list, which is the list holding the current tags applied to a patient and the 
        /// the general system wide "Tags" list, which is a list of all the system tags.
        /// </summary>
        [DispatcherThread]
        [DependsOn("PatientData.Tags")]
        public virtual ObservableCollection<PatientTagViewModel> SyncedAvailableTagsList
        {
            get
            {
                if (PatientData != null && PatientData.Tags != null)
                {
                    _syncedPatientTagsList = PatientDemographicsViewViewModel.TagsList.Where(x => PatientData.Tags.All(y => y.TagId != x.Id)).ToObservableCollection();
                }

                return _syncedPatientTagsList;
            }
            set { _syncedPatientTagsList = value; }
        }
        private ObservableCollection<PatientTagViewModel> _syncedPatientTagsList;

        public virtual IInteractionContext InteractionContext { get; set; }

        #region Commands
        public ICommand RaceFinder { get; set; }

        public ICommand LanguageFinder { get; set; }

        public ICommand EthnicityFinder { get; set; }

        public ICommand Representative { get; set; }

        public ICommand ONCPsychology { get; set; }

        public ICommand Load { get; set; }

        public ICommand Save { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public ICommand AddNewAddress { get; protected set; }

        public ICommand AddNewEmailAddress { get; protected set; }

        public ICommand AddNewPhoneNumber { get; protected set; }

        public ICommand AddNewOtherPhysician { get; protected set; }

        public ICommand AddNewReferralSource { get; protected set; }

        public ICommand AddPatientTag { get; protected set; }

        public ICommand PrimaryTagChanged { get; protected set; }

        public ICommand ConfirmCancelChanges { get; protected set; }

        public ICommand ConfirmExitWithUnsavedChanges { get; protected set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }

        public ICommand DeleteAddress { get; protected set; }

        public ICommand DeleteEmailAddress { get; protected set; }

        public ICommand DeletePhoneNumber { get; protected set; }

        public ICommand DeletePhysician { get; protected set; }

        public ICommand DeletePatientTag { get; protected set; }

        public ICommand ReloadPatientInfo { get; protected set; }

        public ICommand Closing { get; protected set; }

        public ICommand RefreshAlertStatus { get; protected set; }

        public ICommand ShowCardScanner { get; protected set; }

        public ICommand RaceChecked { get; protected set; }

        public ICommand SetSelectedState { get; protected set; }

        public ICommand ShowExternalProviders { get; protected set; }

        public ICommand ValidateSocialSecurity { get; protected set; }
        public ICommand AllowSocialSecurityConflict { get; protected set; }

        public ICommand ActivatePatient { get; set; }

        #endregion

        #region IPatientInfoViewContext Members

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
            ReloadPatientInfo = reloadPatientInfoCommand;
        }

        public ICommand DeleteReferralSource { get; set; }

        // gets called by parent 'PatientInfoViewContext' when it detects a closing event (i.e. user hit the 'x' in the top right corner)
        public PatientInfoClosingReturnArguments OnClosing()
        {
            // check if user made changes
            if (PatientData != null && PatientData.As<IEditableObjectExtended>().IsChanged)
            {
                // return a cancel close flag to the parent view context
                return new PatientInfoClosingReturnArguments { CurrentContext = this, CancelClose = true, ConfirmCancelClose = ConfirmCancelClose };
            }

            return null;
        }

        private void OnRefresh()
        {
            // check if user made changes
            if (PatientData != null && PatientData.As<IEditableObjectExtended>().IsChanged)
            {
                bool? isRefreshConfirmed = false;
                this.Dispatcher().Invoke(() =>
                {
                    isRefreshConfirmed = _interactionManager.Confirm("You have unsaved changes. Are you sure you want to refresh?", "Cancel", "Refresh");
                });

                if (isRefreshConfirmed.GetValueOrDefault())
                {
                    Reload();
                }
            }
            else
            {
                Reload();
            }
        }

        private void Reload()
        {
            PatientData.As<IEditableObjectExtended>().AcceptChanges();
            PatientData.As<IEditableObjectExtended>().EndEdit();
            Load.Execute();
            if (PatientInfoViewModel != null && PatientInfoViewModel.PatientId.HasValue) ReloadPatientInfo.Execute(PatientInfoViewModel.PatientId);
        }

        // gets called by parent 'PatientInfoViewContext' during the closing event but only if we first sent it a cancel close indication
        private bool ConfirmCancelClose()
        {
            if (PatientData.As<IEditableObjectExtended>().IsChanged)
            {
                // trigger pop up to confirm they really want to exit
                ConfirmExitWithUnsavedChangesTrigger = true;

                // if false, the user hit 'no' and wants to stay on the screen
                if (!_confirmExit)
                {
                    // return true to the parent view context indicating we definitely want to cancel the closing event and stay on the screen.
                    return true;
                }

                // reset the flag
                _confirmExit = false;
            }

            return false;
        }

        private bool _confirmExit;

        private void ExecuteCancelExitWithUnsavedChanges()
        {
            _confirmExit = false;
        }

        private void ExecuteConfirmExitWithUnsavedChanges()
        {
            // user has confirmed to exit the view even though there are unsaved changes
            _confirmExit = true;
        }

        #endregion

        #region IWizardViewContext Members
        [DispatcherThread]
        public virtual IWizardDataContext Wizard { get; set; }
        #endregion

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }

    public enum PatientDemographicsPageMode
    {
        View = 1,
        EditNewPatient = 2,
        EditExistingPatient = 3
    }

    public class ONCLoadArguments
    {
        public int PatientId { get; set; }

    }
    public class EthnicityFinderLoadArguments
    {
        public int PatientId { get; set; }
    }
    public class LanguageFinderLoadArguments
    {
        public int PatientId { get; set; }
    }
    public class RaceFinderLoadArguments
    {
        public int PatientId { get; set; }
    }

    public class RepresentativeLoadArguments
    {
        public int PatientId { get; set; }

    }
}
