﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;
using Soaf;


namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.ONCPsychology
{
   public class ONCPsychologyViewManager
    {
       private readonly IInteractionManager _interactionManager;

       public ONCPsychologyViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        /// <summary>
        /// Invoke ONC view
        /// </summary>
        /// <param name="loadArgs"></param>
        public void ShowONCPsychologyFeedBack(ONCLoadArguments loadArgs)
        {
            var view = new ONCPsychologyView();
            var dataContext = view.DataContext.EnsureType<ONCPsychologyViewContext>();
            if (loadArgs.PatientId != 0 && loadArgs != null)
            {
                dataContext.LoadArguments = loadArgs.PatientId;
                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.NoResize
                });
            }
            else
            { 
                _interactionManager.Alert("Please register this patient before ONC survey");
                return;
            }
        }

    }
}
