﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics.ONCPsychology;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.Views.Common.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.ONCPsychology
{
    /// <summary>
    /// Interaction logic for ONCPsychologyView.xaml
    /// </summary>
    public partial class ONCPsychologyView 
    {
        public ONCPsychologyView()
        {
            InitializeComponent();
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    public class ONCPsychologyViewContext : IViewContext
    {
        private readonly ONCPsychologyViewService _ONCPsychologyViewService;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly ONCPsychologicalAnswersViewModel _oncPsychologicalAnswersViewModel;
        private readonly ONCPsychologyServiceLocationsViewModel _oncPsychologyLocationsViewModel;
        private readonly IInteractionManager _interactionManager;
        Dictionary<string, string> feedBackAns = new Dictionary<string, string>();
        private readonly ConfirmationDialogViewManager _confirmationDialogViewManager;
        private bool _stopExitingWindow;
        int location;
        bool close;
        bool savenclose; 
        ObservableCollection<ONCPsychologicalAnswersViewModel> usereload;

        public ONCPsychologyViewContext(ONCPsychologyViewService ONCPsychologyViewService,
                                              PatientDemographicsSetupViewManager setupViewManager,
                                              PatientInfoViewManager patientInfoViewManager,
                                              ONCPsychologicalAnswersViewModel ONCPsychologicalAnswersViewModel,
                                              IInteractionManager interactionManager,
                                              Func<ONCPsychologicalAnswersViewModel> createONCAnswersViewModel,
                                              Func<ONCPsychologyServiceLocationsViewModel> createONCLocationsViewModel,
                                              ONCPsychologyServiceLocationsViewModel oncPsychologyLocationsViewModel,
                                              ConfirmationDialogViewManager confirmationDialogViewManager
                                               )
        {
            _ONCPsychologyViewService = ONCPsychologyViewService;
            _patientInfoViewManager = patientInfoViewManager;
            _oncPsychologicalAnswersViewModel = ONCPsychologicalAnswersViewModel;
            _oncPsychologyLocationsViewModel = oncPsychologyLocationsViewModel;
            _interactionManager = interactionManager;
            _confirmationDialogViewManager = confirmationDialogViewManager;

            OnAnsComboSelectedItemChangedCommand = Command.Create<Object>(OnAnsChangedHandler);
            OnLocationComboSelectedItemChangedCommand = Command.Create<Object>(OnAnsLocationChangedHandler); 
                Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CanEventExecute).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel, CanEventExecute);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
        }

        /// <summary>
        /// Handles page closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
    
        private void  OnClosing(object sender, CancelEventArgs e)
            {

                //if (!savenclose)
                //{
                    if (!close)
                    {
                        bool popup = false;

                        if (feedBackAns.Any() && usereload == null)
                        {
                            if (LocationId != 0 && !savenclose)
                            {
                                popup = true;
                            }
                            else if (feedBackAns.Any() && savenclose && usereload == null)
                            {
                                popup = false;
                            }
                            else
                            {
                                ShowPopup(e);
                                e.Cancel = _stopExitingWindow;
                                //_stopExitingWindow = true;
                                //e.Cancel = _stopExitingWindow;
                                //_stopExitingWindow = false;
                                // _interactionManager.Confirm("Please select Location");
                            }
                        }
                        if (usereload != null && usereload.Any() && feedBackAns.Count != usereload.Count && !savenclose)
                        {
                            popup = true;
                        }
                        else if (usereload != null && !savenclose)
                        {
                            foreach (var item in usereload)
                            {
                                string qid = item.QuestionIdAnswerId.Split('~')[0];
                                string aid = item.QuestionIdAnswerId.Split('~')[1];

                                if (feedBackAns.ContainsKey(qid) && feedBackAns[qid].ToString() != aid)
                                {
                                    popup = true;
                                    break;
                                }
                            }
                        }
                        else if (!feedBackAns.Any() && usereload == null)
                        {
                            popup = false;
                            e.Cancel = false;
                            _stopExitingWindow = false;
                        }
                        if (popup)
                        {
                            ShowPopup(e);
                        }
                    }
                    else
                    {
                        e.Cancel = false;
                        _stopExitingWindow = false;
                    } 
                //}
            }

        private void ShowPopup(CancelEventArgs e)
        {
            var loadArgument = new ConfirmationDialogLoadArguments
            {
                Cancel = CancelExitWithUnsavedChanges,
                Content = "You have unsaved changes.  Would you like to save your changes?",
                YesContent = "Save",
                NoContent = "Don't Save",
                CancelContent = "Cancel",
                Yes = Save,
                No = Cancel
            };
           
                _confirmationDialogViewManager.ShowConfirmationDialogView(loadArgument);
                    e.Cancel = _stopExitingWindow;
                    _stopExitingWindow = true;
        }
         private void ExecuteCancelExitWithUnsavedChanges()
         {
             _stopExitingWindow = true;
         }
        private void ExecuteLoad()
        {
            InteractionContext.Completing += new EventHandler<CancelEventArgs>(OnClosing).MakeWeak();
            Locations = _ONCPsychologyViewService.GetLocations();
            ObservableCollection<ONCPsychologicalAnswersViewModel>[] obj = new ObservableCollection<ONCPsychologicalAnswersViewModel>[23];
               _ONCPsychologyViewService.GetAnswers(ref obj);
           
                 if(obj!=null && obj.Any())
                 {
                     for (int idx = 0; idx < obj.Length;idx++ )
                     {
                         switch(idx.ToString())
                         {
                             case "0": if(obj.Any())
                                         {
                                             Ans1 = obj[idx];
                                         }
                                        break;
                             case "1": if (obj.Any())
                                        {
                                            Ans2 = obj[idx];
                                        }
                                        break;
                             case "2": if (obj.Any())
                                        {
                                            Ans3 = obj[idx];
                                        }
                                        break;
                             case "3": if (obj.Any())
                                        {
                                            Ans4 = obj[idx];
                                        }
                                        break;
                             case "4": if (obj.Any())
                                        {
                                            Ans5 = obj[idx];
                                        }
                                        break;
                             case "5": if (obj.Any())
                                        {
                                            Ans6 = obj[idx];
                                        }
                                        break;
                             case "6": if (obj.Any())
                                        {
                                            Ans7 = obj[idx];
                                        }
                                        break;
                             case "7": if (obj.Any())
                                        {
                                            Ans8 = obj[idx];
                                        }
                                        break;
                             case "8": if (obj.Any())
                                        {
                                            Ans9 = obj[idx];
                                        }
                                        break;
                             case "9": if (obj.Any())
                                        {
                                            Ans10 = obj[idx];
                                        }
                                        break;
                             case "10": if (obj.Any())
                                        {
                                            Ans11 = obj[idx];
                                        }
                                        break;
                             case "11": if (obj.Any())
                                        {
                                            Ans12 = obj[idx];
                                        }
                                        break;
                             case "12": if (obj.Any())
                                        {
                                            Ans13 = obj[idx];
                                        }
                                        break;
                             case "13": if (obj.Any())
                                        {
                                            Ans14 = obj[idx];
                                        }
                                        break;
                             case "14": if (obj.Any())
                                        {
                                            Ans15 = obj[idx];
                                        }
                                        break;
                             case "15": if (obj.Any())
                                        {
                                            Ans16 = obj[idx];
                                        }
                                        break;
                             case "16": if (obj.Any())
                                        {
                                            Ans17 = obj[idx];
                                        }
                                        break;
                             case "17": if (obj.Any())
                                        {
                                            Ans18 = obj[idx];
                                        }
                                        break;
                             case "18": if (obj.Any())
                                        {
                                            Ans19 = obj[idx];
                                        }
                                        break;
                             case "19": if (obj.Any())
                                        {
                                            Ans20 = obj[idx];
                                        }
                                        break;
                             case "20": if (obj.Any())
                                        {
                                            Ans21 = obj[idx];
                                        }
                                        break;
                             case "21": if (obj.Any())
                                        {
                                            Ans22 = obj[idx];
                                        }
                                        break;
                             case "22": if (obj.Any())
                                        {
                                            Ans23 = obj[idx];
                                        }
                                        break;
                         }
                     }
                 }
            //Reappear !
                 var reload = _ONCPsychologyViewService.GetPatientFeedBackById(LoadArguments,ref location);
                 LocationId = location;
                 if (reload.Any())
                    {
                        usereload = reload;
                        foreach (var item in reload)
                        {
                            string val = item.QuestionIdAnswerId.Split('~')[0];
                            switch (val)
                            {
                                case "1":
                                    Select(Ans1, item);
                                    break;
                                case "2":
                                    Select(Ans2, item);
                                    break;
                                case "3":
                                    Select(Ans3, item);
                                    break;
                                case "4":
                                    Select(Ans4, item);
                                    break;
                                case "5":
                                    Select(Ans5, item);
                                    break;
                                case "6":
                                    Select(Ans6, item);
                                    break;
                                case "7":
                                    Select(Ans7, item);
                                    break;
                                case "8":
                                    Select(Ans8, item);
                                    break;
                                case "9":
                                    Select(Ans9, item);
                                    break;
                                case "10":
                                    Select(Ans10, item);
                                    break;
                                case "11":
                                    Select(Ans11, item);
                                    break;
                                case "12":
                                    Select(Ans12, item);
                                    break;
                                case "13":
                                    Select(Ans13, item);
                                    break;
                                case "14":
                                    Select(Ans14, item);
                                    break;
                                case "15":
                                    Select(Ans15, item);
                                    break;
                                case "16":
                                    Select(Ans16, item);
                                    break;
                                case "17":
                                    Select(Ans17, item);
                                    break;
                                case "18":
                                    Select(Ans18, item);
                                    break;
                                case "19":
                                    Select(Ans19, item);
                                    break;
                                case "20":
                                    Select(Ans20, item);
                                    break;
                                case "21":
                                    Select(Ans21, item);
                                    break;
                                case "22":
                                    Select(Ans22, item);
                                    break;
                                case "23":
                                    Select(Ans23, item);
                                    break;
                            } 
                        }
                    }
        }
        
        /// <summary>
        /// Find selected feedback on page reload
        /// </summary>
        /// <param name="options"></param>
        /// <param name="item"></param>
        private void Select( ObservableCollection<ONCPsychologicalAnswersViewModel>options,ONCPsychologicalAnswersViewModel item)
        {
            foreach (var feed in options)
            {
                if (feed.QuestionIdAnswerId == item.QuestionIdAnswerId)
                {
                    feed.QuestionIdAnswerId = item.QuestionIdAnswerId;
                    QuestionIdAnswerId = feed;
                    break;
                }
            }
        }

        private bool CanEventExecute()
        {
            return true;
        }

        /// <summary>
        /// Handles cancellation
        /// </summary>
        private void ExecuteCancel()
        {
            _stopExitingWindow = false;
            close = true;
            InteractionContext.Complete();
        }


        public void OnAnsLocationChangedHandler(object obj)
        {
            try
            {
                Telerik.Windows.Controls.RadComboBox onClockComboBox = obj as Telerik.Windows.Controls.RadComboBox;
                var itemSelected = onClockComboBox.SelectedItem as ONCPsychologyServiceLocationsViewModel;

                if (itemSelected.Id != 0)
                {
                    LocationRequired = false;
                }
            }
            catch (Exception)
            {
                
            }
        }
        /// <summary>
        /// Handle Answer selection.
        /// </summary>
        /// <param name="obj">control raising the event</param>
        public void OnAnsChangedHandler(object obj)
        {
            savenclose = false;
            try
            {
                Telerik.Windows.Controls.RadComboBox onClockComboBox = obj as Telerik.Windows.Controls.RadComboBox;
                var itemSelected = onClockComboBox.SelectedItem as ONCPsychologicalAnswersViewModel;

                if (!string.IsNullOrEmpty(itemSelected.QuestionIdAnswerId))
                {
                    string quesId = itemSelected.QuestionIdAnswerId.Split('~')[0];
                    string ansId = itemSelected.QuestionIdAnswerId.Split('~')[1];

                        if (feedBackAns.ContainsKey(quesId))
                        {
                            feedBackAns.Remove(quesId);
                        }
                        feedBackAns.Add(quesId, ansId); 
                }
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Save Feedbacks
        /// </summary>
        private void ExecuteSave()
        {
            
            location=LocationId;
            var loc = LocationId;
            if(loc==0)
            {
                _interactionManager.Alert("Please select Location");
                LocationRequired = true;
                _stopExitingWindow = true;
                //InteractionContext.Complete(false);
                //return;
            }
            else
            {
                var patId = LoadArguments;
                if (feedBackAns.Any())
                {
                    foreach (KeyValuePair<string, string> item in feedBackAns)
                    {
                        var qId = item.Key;
                        var aId = item.Value;
                        ONCPsychologicalFeedBackViewModel res = new ONCPsychologicalFeedBackViewModel();
                        res.LocationId = loc;
                        res.PatientId = patId;
                        res.AnswerId = int.Parse(aId);
                        res.QuestionId = int.Parse(qId);
                        _ONCPsychologyViewService.executeInsertQuery(res);
                    }
                    _stopExitingWindow = false;
                    _interactionManager.Alert("Saved Successfully");
                }
                savenclose = true;
            }
        }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans1 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans2 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans3 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans4 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans5 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans6 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans7 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans8 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans9 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans10 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans11 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans12 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans13 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans14 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans15 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans16 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans17 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans18 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans19 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans20 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans21 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans22 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologicalAnswersViewModel> Ans23 { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ONCPsychologyServiceLocationsViewModel> Locations { get; set; }

        [DispatcherThread]
        public virtual int LocationId { get; set; }

        [DispatcherThread]
        public virtual ONCPsychologicalAnswersViewModel QuestionIdAnswerId { get; set; }

        [DispatcherThread]
        public virtual string AnswerId { get; set; }

        [DispatcherThread]
        public virtual bool LocationRequired { get; set; }

        public IInteractionContext InteractionContext { get; set; }
        public virtual int LoadArguments { get; set; }

        #region Commands

        public ICommand ONCPsychology { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand Save { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public ICommand OnAnsComboSelectedItemChangedCommand { get; protected set; }
        public ICommand OnLocationComboSelectedItemChangedCommand { get; protected set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }
        

        #endregion

        #region IPatientInfoViewContext Members

       
        // gets called by parent 'PatientInfoViewContext' during the closing event but only if we first sent it a cancel close indication
        private bool ConfirmCancelClose()
        {
            return false;
        }

        #endregion

       
    }

}
