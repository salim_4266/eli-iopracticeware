﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics
{
    /// <summary>
    /// Interaction logic for PatientDemographicsManagePatientTagsView.xaml
    /// </summary>
    public partial class PatientDemographicsManagePatientTagsView
    {
        public PatientDemographicsManagePatientTagsView()
        {
            InitializeComponent();
        }
    }

    public class PatientDemographicsManagePatientTagsViewContext : IViewContext
    {
        private readonly IPatientDemographicsManagePatientTagViewService _patientDemographicsManagePatientTagViewService;
        private readonly Func<PatientDemographicsPatientTagViewModel> _createPatientTagViewModel;
        private bool _isArchivedPatientTagsShowing;

        public PatientDemographicsManagePatientTagsViewContext(IPatientDemographicsManagePatientTagViewService patientDemographicsManagePatientTagViewService, Func<PatientDemographicsPatientTagViewModel> createPatientTagViewModel)
        {
            _patientDemographicsManagePatientTagViewService = patientDemographicsManagePatientTagViewService;
            _createPatientTagViewModel = createPatientTagViewModel;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            AddPatientTag = Command.Create(ExecuteAddPatientTag);
            SaveNewPatientTag  = Command.Create(ExecuteSaveNewPatientTag).Async(() => InteractionContext);
            RemoveNewPatientTag = Command.Create(ExecuteRemoveNewPatientTag);
            LoadArchivedPatientTags = Command.Create(ExecuteLoadArchivedPatientTags).Async(() => InteractionContext);
            SavePatientTag = Command.Create<PatientDemographicsPatientTagViewModel>(ExecuteSavePatientTag).Async(() => InteractionContext);
            ArchivePatientTag = Command.Create(ExecuteArchivePatientTag).Async(() => InteractionContext);
            RestorePatientTag = Command.Create<PatientDemographicsPatientTagViewModel>(ExecuteRestorePatientTag).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            var patientDemographicsTagsInformation = _patientDemographicsManagePatientTagViewService.LoadPatientDemographicsTagsInformation();
            ArchivedPatientTags = patientDemographicsTagsInformation.ArchivedTags.ToExtendedObservableCollection();
            ExistingPatientTags = patientDemographicsTagsInformation.ExistingTags.ToExtendedObservableCollection();
            foreach (PatientDemographicsPatientTagViewModel patientTag in ExistingPatientTags) patientTag.CastTo<IEditableObject>().BeginEdit();
        }

        private void ExecuteAddPatientTag()
        {
            if (!CanEditPatientTags) return;
            if (NewPatientTag != null) return;

            var newPatientTag = _createPatientTagViewModel();
            var rand = new Random();
            var color = Color.FromArgb(255, (byte)rand.Next(255), (byte)rand.Next(255), (byte)rand.Next(255)); // WE ONLY WANT OPAQUE COLORS - #FF...
            newPatientTag.Color = color;
            NewPatientTag = newPatientTag;
        }

        private void ExecuteSaveNewPatientTag()
        {
            if (!CanEditPatientTags) return;
            if (NewPatientTag == null || NewPatientTag.Name.IsNullOrEmpty()) return;

            if (NewPatientTag.Name.Length > 5)
            {
                InteractionManager.Current.Alert("Patient Tag names cannot be longer than 5 characters.  Please enter a Tag Name within the 5 character limit.");
                return;
            }

            if (PatientTagNameExists(NewPatientTag))
            {
                this.Dispatcher().Invoke(() => InteractionManager.Current.Alert("Patient tag name already exists. Please choose another name."));
                NewPatientTag.CastTo<IEditableObject>().CancelEdit();
                return;
            }
            
            int newPatientTagId = _patientDemographicsManagePatientTagViewService.SavePatientTag(NewPatientTag);
            NewPatientTag.Id = newPatientTagId;
            
            NewPatientTag.CastTo<IEditableObject>().BeginEdit();
            ExistingPatientTags.Insert(0, NewPatientTag);
            NewPatientTag = null;
        }

        private void ExecuteRemoveNewPatientTag()
        {
            NewPatientTag = null;
        }

        public void ExecuteLoadArchivedPatientTags()
        {
            if (ArchivedPatientTags == null)
            {
                ArchivedPatientTags = _patientDemographicsManagePatientTagViewService.LoadArchivedTags().OrderBy(c => c.Name).ToExtendedObservableCollection();
            }
        }

        public void ExecuteSavePatientTag(PatientDemographicsPatientTagViewModel patientTag)
        {
            if (patientTag == null || patientTag.Name.IsNullOrEmpty()) return;

            if (patientTag.Name.Length > 5)
            {
                InteractionManager.Current.Alert("Patient Tag names cannot be longer than 5 characters.  Please enter a Tag Name within the 5 character limit.");
                return;
            }

            if (PatientTagNameExists(patientTag))
            {
                this.Dispatcher().Invoke(() =>
                    {
                        InteractionManager.Current.Alert("Patient tag name already exists. Please choose another name.");
                        patientTag.CastTo<IEditableObject>().CancelEdit();
                        patientTag.CastTo<IChangeTracking>().AcceptChanges();

                    });
                return;
            }
            if (!CanEditPatientTags) return;
            _patientDemographicsManagePatientTagViewService.SavePatientTag(patientTag);
            patientTag.CastTo<IEditableObject>().BeginEdit();
        }

        public void ExecuteArchivePatientTag()
        {
            if (PatientTagToArchive != null)
            {
                if (!CanEditPatientTags) return;
                IsArchivedPatientTagsShowing = true;

                var archivedPatientTag = _patientDemographicsManagePatientTagViewService.ArchivePatientTag(PatientTagToArchive.Id);

                ExistingPatientTags.Remove(PatientTagToArchive);
                ArchivedPatientTags.Insert(0, archivedPatientTag);

                PatientTagToArchive = null;
            }
        }

        public void ExecuteRestorePatientTag(PatientDemographicsPatientTagViewModel patientTag)
        {
            if (!CanEditPatientTags) return;
            var restoredPatientTag = _patientDemographicsManagePatientTagViewService.RestorePatientTag(patientTag.Id);
            restoredPatientTag.CastTo<IEditableObject>().BeginEdit();

            ArchivedPatientTags.Remove(patientTag);
            ExistingPatientTags.Insert(0, restoredPatientTag);
        }

        private bool PatientTagNameExists(PatientDemographicsPatientTagViewModel patientTag)
        {
            return ExistingPatientTags.Concat(ArchivedPatientTags).Where(c => c.Id != patientTag.Id).Any(c => c.Name.Equals(patientTag.Name, StringComparison.OrdinalIgnoreCase));
        }
        
        [DispatcherThread]
        public virtual bool IsArchivedPatientTagsShowing
        {
            get { return _isArchivedPatientTagsShowing; }
            set
            {
                _isArchivedPatientTagsShowing = value;
                if (_isArchivedPatientTagsShowing && ArchivedPatientTags == null)
                {
                    LoadArchivedPatientTags.Execute();
                }
            }
        }

        /// <summary>
        /// A newly created Patient Tag
        /// </summary>
        [DispatcherThread]
        public virtual PatientDemographicsPatientTagViewModel NewPatientTag { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<PatientDemographicsPatientTagViewModel> ExistingPatientTags { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<PatientDemographicsPatientTagViewModel> ArchivedPatientTags { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to create a new Patient Tag
        /// </summary>
        public ICommand AddPatientTag { get; protected set; }

        /// <summary>
        /// Command to save the new Patient Tag
        /// </summary>
        public ICommand SaveNewPatientTag { get; protected set; }

        /// <summary>
        /// Command to delete (undo) the newly created Patient Tag
        /// </summary>
        public ICommand RemoveNewPatientTag { get; protected set; }

        /// <summary>
        /// Command to load all archived Patient tags
        /// </summary>
        public ICommand LoadArchivedPatientTags { get; protected set; }

        /// <summary>
        /// Command to save an existing Patient tag
        /// </summary>
        public ICommand SavePatientTag { get; protected set; }

        /// <summary>
        /// Command to restore a Patient tag from archival
        /// </summary>
        public ICommand RestorePatientTag { get; protected set; }

        /// <summary>
        /// Command to archive a Patient tag
        /// </summary>
        public ICommand ArchivePatientTag { get; protected set; }

        [DispatcherThread]
        public virtual PatientDemographicsPatientTagViewModel PatientTagToArchive { get; set; }

        public IInteractionContext InteractionContext { get; set; }

        public virtual bool CanEditPatientTags
        {
            get { return PermissionId.AdministerPatientTags.EnsurePermission(); }
        }
    }
}
