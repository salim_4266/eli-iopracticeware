﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;
using Soaf;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative
{
	public class PatientRepresentativeViewManager
	{
		private readonly IInteractionManager _interactionManager;

		public PatientRepresentativeViewManager(IInteractionManager interactionManager)
		{
			_interactionManager = interactionManager;
		}

		/// <summary>
		/// Invoke ONC view
		/// </summary>
		/// <param name="loadArgs"></param>
		public void ShowRepresentativeFeedBack(RepresentativeLoadArguments loadArgs)
		{
			var view = new PatientRepresentativeView();
			var dataContext = view.DataContext.EnsureType<PatientRepresentativeViewContext>();
			if (loadArgs.PatientId != 0 && loadArgs != null)
			{
				dataContext.LoadArguments = loadArgs.PatientId;
				_interactionManager.ShowModal(new WindowInteractionArguments
				{
					Content = view,
					WindowState = WindowState.Normal,
					ResizeMode = ResizeMode.NoResize
				});
			}
			else
			{
				_interactionManager.Alert("Please register this patient before adding Representative.");
				return;
			}
		}
	}
}
