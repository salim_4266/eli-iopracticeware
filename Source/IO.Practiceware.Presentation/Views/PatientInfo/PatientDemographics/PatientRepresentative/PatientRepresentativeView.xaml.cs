﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.PatientRepresentative;
//using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.Views.Common.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics.PatientRepresentative;
using System.Data;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Model;
using IO.Practiceware.Data;
using Soaf.Data;
using IO.Practiceware.Services.Geocode;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Threading;
using IO.Practiceware.Presentation.ViewServices;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Utilities;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics.PatientRepresentative
{
    /// <summary>
    /// Interaction logic for Representative.xaml
    /// </summary>
    public partial class PatientRepresentativeView
    {
        public PatientRepresentativeView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }


    public class PatientRepresentativeViewContextReference : DataContextReference
    {
        public new PatientRepresentativeViewContext DataContext
        {
            get { return (PatientRepresentativeViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    public class PatientRepresentativeViewContext : IViewContext
    {

        private readonly PatientRepresentativeViewService _patientRepresentativeViewService;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly PatientRepresentativeViewModel _PatientRepresentativeViewModel;
        private readonly IInteractionManager _interactionManager;
        private readonly ConfirmationDialogViewManager _confirmationDialogViewManager;
        private bool _stopExitingWindow;
        public virtual bool IsFirstNameHighlight { get; set; }
        public virtual bool IsAddressLine1Highlight { get; set; }
        public virtual bool IsAddressCityHighlight { get; set; }
        public virtual bool IsAddressStateHighlight { get; set; }
        public virtual bool IsAddressZipCodeHighlight { get; set; }
        public virtual bool IsEmailHighlight { get; set; }
        public virtual bool IsPhoneNumberHighlight { get; set; }
        public virtual bool IsLastNameHighlight { get; set; }
        public virtual bool IsRelationshipHighlight { get; set; }
        public virtual bool IsDOBHighlight { get; set; }
        private static IGeocodingService _geocodingService;
        //bool close;
        //bool savenclose;
        //PatientRepresentativeViewModel usereload;

        #region Commands
        public ICommand Save { get; protected set; }
        public ICommand Show { get; protected set; }
        public ICommand CountryChanged { get; protected set; }
        public ICommand Load { get; protected set; }

        public ICommand Cancel { get; protected set; }

        public ICommand CancelExitWithUnsavedChanges { get; protected set; }
        public ICommand PopulateCityAndStateCommand { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        #endregion


        public virtual int LoadArguments { get; set; }

        [DispatcherThread]
        public virtual PatientRepresentativeViewModel PatRepresViewModel { get; set; }

        public virtual ObservableCollection<NamedViewModel> Relations { get; set; }

        public virtual ObservableCollection<NameAndAbbreviationViewModel> StatesOrProvinces { get; set; }
        public virtual ObservableCollection<NameAndAbbreviationViewModel> Countries { get; set; }
        [DispatcherThread]
        public virtual NameAndAbbreviationViewModel StateOrProvince { get; set; }
        public IEnumerable<StateOrProvinceViewModel> StateOrProvinces { get; set; }

        [DispatcherThread]
        public virtual string City { get; set; }

        [DispatcherThread]
        public virtual string Line1 { get; set; }

        [DispatcherThread]
        public virtual string Line2 { get; set; }
        [DispatcherThread]
        public virtual string Zip { get; set; }

        public virtual int DefaultSelectedCountry { get; set; }

        [DispatcherThread]
        public virtual NameAndAbbreviationViewModel CountryName { get; set; }

        [DispatcherThread]
        public virtual bool ValidationWarningTrigger { get; set; }

        public PatientRepresentativeViewContext(PatientRepresentativeViewService patientRepresentativeViewService,
                                              PatientDemographicsSetupViewManager setupViewManager,
                                              PatientInfoViewManager patientInfoViewManager,
                                              IInteractionManager interactionManager,
                                              PatientRepresentativeViewModel PatientRepresentativeViewModel,
                                              ConfirmationDialogViewManager confirmationDialogViewManager,
                                              IGeocodingService geocodingService
                                               )
        {
            _patientRepresentativeViewService = patientRepresentativeViewService;
            _patientInfoViewManager = patientInfoViewManager;
            _PatientRepresentativeViewModel = PatientRepresentativeViewModel;
            _interactionManager = interactionManager;
            _confirmationDialogViewManager = confirmationDialogViewManager;
            _geocodingService = geocodingService;
            Show = Command.Create<string>(OnShow).Async(() => InteractionContext);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave, CanEventExecute).Async(() => InteractionContext);
            CountryChanged = Command.Create(ExecuteCountryChanged, CanEventExecute).Async(() => InteractionContext);
            Cancel = Command.Create(() => InteractionContext.Complete(true));
            PopulateCityAndStateCommand = Command.Create(ExecutePopulateCityAndStateCommand).Async(() => InteractionContext);
            //CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
        }


        private void OnShow(string par)
        {
           if (string.IsNullOrEmpty(par))
                IsPhoneNumberHighlight = true;
            else 
                IsPhoneNumberHighlight = false;
        }
        private void ExecuteLoad()
        {
            IsFirstNameHighlight = true;
            IsLastNameHighlight = true;
            IsRelationshipHighlight = true;
            IsDOBHighlight = true;
            IsAddressLine1Highlight = true;
            IsAddressCityHighlight = true;
            IsAddressStateHighlight = true;
            IsAddressZipCodeHighlight = true;
            IsEmailHighlight = true;
            PatRepresViewModel = new PatientRepresentativeViewModel();
            PatRepresViewModel.PatientId = LoadArguments;
            //ZipCodes = _patientRepresentativeViewService.GetZipCodesAndCity();
            PatRepresViewModel = _patientRepresentativeViewService.GetPatientRepresentativeDetails(LoadArguments);
            Relations = NamedViewModelExtensions.ToNamedViewModel<FamilyRelationshipId>().OrderBy(x => x.Name).ToObservableCollection();
            //StatesOrProvinces = _patientRepresentativeViewService.GetStateOrProvince();
            //Countries = NamedViewModelExtensions.ToNamedViewModel<CountryId>().OrderBy(x => x.Name).ToObservableCollection();	
            //DefaultSelectedCountry = Countries.LastOrDefault().Id;	
            StatesOrProvinces = _patientRepresentativeViewService.GetStateOrProvince();
            if (string.IsNullOrEmpty(PatRepresViewModel.HomePhone) && string.IsNullOrEmpty(PatRepresViewModel.WorkPhone) && string.IsNullOrEmpty(PatRepresViewModel.CellPhone))
            {
                IsPhoneNumberHighlight = true;
            }
            else
            {
                IsPhoneNumberHighlight = false;
            }
            if (PatRepresViewModel != null && PatRepresViewModel.Addresses != null)
            {
                City = PatRepresViewModel.Addresses.City;
                StateOrProvince = StatesOrProvinces.FirstOrDefault(i => i.Abbreviation == PatRepresViewModel.State);
                Zip = PatRepresViewModel.Zip;
                Line1 = PatRepresViewModel.Addresses.Line1;
                Line2 = PatRepresViewModel.Addresses.Line2;
                CountryName = PatRepresViewModel.Country;
            }
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select * from model.countries where id in (38,140,225)";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    Countries = new ObservableCollection<NameAndAbbreviationViewModel>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        Countries.Add(new NameAndAbbreviationViewModel() { Id = int.Parse(row["Id"].ToString()), Abbreviation = row["Abbreviation3Letters"].ToString() });
                    }
                }
            }
            //CountryName = Countries.FirstOrDefault(i => i.Abbreviation == PatRepresViewModel.Country);
        }
        private void ExecuteCountryChanged()
        {
            if (CountryName!=null)
            {
                PatRepresViewModel.Country = new NameAndAbbreviationViewModel();
                PatRepresViewModel.Country.Abbreviation = CountryName.Abbreviation; 
            }
            Country cty = new Country();

            if (PatRepresViewModel != null && !string.IsNullOrEmpty(PatRepresViewModel.Country.Abbreviation)) //(cty.IsCanada == true || cty.IsUnitedStates == true || cty.IsMexico == true)
            {
                if (PatRepresViewModel.Country.Abbreviation.ToUpper() == "USA")//(IsCountry == cty.IsUnitedStates)
                {
                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        string slctqry = "select * from model.[StateOrProvinces] where countryid =225 "; //+cty.IsUnitedStates.CountryId;
                        DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            StatesOrProvinces = new ObservableCollection<NameAndAbbreviationViewModel>();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                StatesOrProvinces.Add(new NameAndAbbreviationViewModel() { Id = int.Parse(row["Id"].ToString()), Abbreviation = row["Abbreviation"].ToString(), Name = row["Name"].ToString() });
                            }
                        }
                    }
                }
                else if (PatRepresViewModel.Country.Abbreviation.ToUpper() == "CAN") //(IsCountry == cty.IsCanada)
                {
                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        string slctqry = "select * from model.[StateOrProvinces] where countryid =38 "; //+ //cty.IsUnitedStates.CountryId;
                        DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            StatesOrProvinces = new ObservableCollection<NameAndAbbreviationViewModel>();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                StatesOrProvinces.Add(new NameAndAbbreviationViewModel() { Id = int.Parse(row["Id"].ToString()), Abbreviation = row["Abbreviation"].ToString(), Name = row["Name"].ToString() });
                            }
                        }
                    }
                }
                else if (PatRepresViewModel.Country.Abbreviation.ToUpper() == "MEX") //(IsCountry == cty.IsMexico)
                {
                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        string slctqry = "select * from model.[StateOrProvinces] where countryid =140"; //+ //cty.IsUnitedStates.CountryId;
                        DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            StatesOrProvinces = new ObservableCollection<NameAndAbbreviationViewModel>();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                StatesOrProvinces.Add(new NameAndAbbreviationViewModel() { Id = int.Parse(row["Id"].ToString()), Abbreviation = row["Abbreviation"].ToString(), Name = row["Name"].ToString() });
                            }
                        }
                    }
                }
            }
        }

        private void ExecutePopulateCityAndStateCommand()
        {
            if (!string.IsNullOrEmpty(Zip))
                //ZipCode via DB
                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    string slctqry = "select top 1 c.id as CountryId, * from dbo.ZipCodes zc inner join model.StateOrProvinces sp on zc.stateAbbr=sp.Abbreviation inner join model.Countries c on sp.countryid=c.Id where ZIPCode = '" + Zip + "'";
                    DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in dataTable.Rows)
                        {
                            foreach (var item in StatesOrProvinces)
                            {
                                if (item.Abbreviation == row["StateAbbr"].ToString())
                                {
                                    StateOrProvince = item;
                                    break;
                                }
                            }
                            //StateCityData.Add(new AddressViewModel() { Id = int.Parse(row["Id"].ToString()), City = row["CityName"].ToString() });
                            City = row["CityName"].ToString();
                            NameAndAbbreviationViewModel cntry = new NameAndAbbreviationViewModel
                            {
                                Abbreviation = row["Abbreviation3Letters"].ToString(),
                                Id = int.Parse(row["CountryId"].ToString())

                            };
                            CountryName = cntry;
                        }
                    }
                    else
                    {
                        System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() =>
                        {
                            GeocodeInformation geocodeInformation = null;
                            if (!string.IsNullOrEmpty(Zip))
                            {
                                if (!string.IsNullOrEmpty(Zip))
                                    if (Zip.Length >= 5)
                                        Zip = Zip.Substring(0, 5) + "-" + Zip.Substring(5);
                                Zip = Zip.Replace("--", "-");
                            }
                            try
                            {
                                geocodeInformation = _geocodingService.GetGeocodeInformation(Zip);
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError(ex.ToString());
                            }
                            if (geocodeInformation != null && geocodeInformation.IsValid())
                            {
                                if (CountryName == null || !string.Equals(CountryName.Id.ToString(), geocodeInformation.CountryId.ToString(), StringComparison.CurrentCultureIgnoreCase))
                                {
                                        string slctqry2 = "select * from model.[Countries] where Id = " + geocodeInformation.CountryId; //+cty.IsUnitedStates.CountryId;
                                        DataTable dataTable2 = dbConnection.Execute<DataTable>(slctqry2);
                                        if (dataTable2 != null && dataTable2.Rows.Count > 0)
                                        {
                                            NameAndAbbreviationViewModel ctry = new NameAndAbbreviationViewModel
                                            {
                                                Abbreviation = dataTable2.Rows[0]["Abbreviation3Letters"].ToString(),
                                                Id = int.Parse(dataTable2.Rows[0]["Id"].ToString())

                                            };
                                            CountryName = ctry;
                                        }
                                }
                            

                                if (City == null || !string.Equals(City, geocodeInformation.City, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    City = geocodeInformation.City;
                                }

                                if (StatesOrProvinces != null)
                                {
                                    // overwrite state if different
                                    var stateByPostalCode = StatesOrProvinces.FirstOrDefault(i => i.Id == geocodeInformation.StateId);
                                    if (StateOrProvince == null || !Equals(StateOrProvince, stateByPostalCode))
                                    {
                                        StateOrProvince = stateByPostalCode;
                                    }
                                }
                            }
                            else
                            {
                                City = null;
                                StateOrProvince = null;
                            }
                            if (!string.IsNullOrEmpty(City) && StateOrProvince != null)
                            {
                                InsertZipCodeData(Zip, City, StateOrProvince);
                            }
                        });
                    }
                }
        }
        public void InsertZipCodeData(string Zip, string City, NameAndAbbreviationViewModel StateOrProvince)
        {
            //Save ZipCode, State and City in Table
            if (City != null && StateOrProvince != null)
            {
                using (var dbCon = DbConnectionFactory.PracticeRepository)
                {
                    Zip = Zip.Substring(0, 5);
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("@ZipCode", Zip);
                    param.Add("@CityName", City);
                    param.Add("@StateAbbr", StateOrProvince.Abbreviation);
                    string qryInsert = "Insert into dbo.ZipCodes (ZipCode,CityName,StateAbbr) values (@ZipCode,@CityName,@StateAbbr)";
                    dbCon.Execute(qryInsert, param);
                }
            }
        }
        private void ExecuteSave()
        {
            var patientId = SaveRepresentative();
            if (patientId != null)
            {
                InteractionContext.Complete(true);
            }
        }

        internal int? SaveRepresentative()
        {
            if (string.IsNullOrEmpty(PatRepresViewModel.FirstName) || string.IsNullOrEmpty(PatRepresViewModel.LastName) || string.IsNullOrEmpty(PatRepresViewModel.Relationship)
                || string.IsNullOrEmpty(PatRepresViewModel.DateOfBirth) || string.IsNullOrEmpty(Line1) || string.IsNullOrEmpty(City) || (StateOrProvince == null) || string.IsNullOrEmpty(Zip)
                || string.IsNullOrEmpty(PatRepresViewModel.Email) || (string.IsNullOrEmpty(PatRepresViewModel.HomePhone) && string.IsNullOrEmpty(PatRepresViewModel.WorkPhone) 
                && string.IsNullOrEmpty(PatRepresViewModel.CellPhone)))
            {
                // trigger alert informing the user that there are invalid fields and save cannot proceed
                if (string.IsNullOrEmpty(PatRepresViewModel.FirstName))
                {
                    IsFirstNameHighlight = true;
                }
                if (string.IsNullOrEmpty(PatRepresViewModel.LastName))
                {
                    IsLastNameHighlight = true;
                }
                if (string.IsNullOrEmpty(PatRepresViewModel.Relationship))
                {
                    IsRelationshipHighlight = true;
                }
                if (string.IsNullOrEmpty(PatRepresViewModel.DateOfBirth))
                {
                    IsDOBHighlight = true;
                }
                if (string.IsNullOrEmpty(Line1))
                {
                    IsAddressLine1Highlight = true;
                }
                if (string.IsNullOrEmpty(City))
                {
                    IsAddressCityHighlight = true;
                }
                if (StateOrProvince == null)
                {
                    IsAddressStateHighlight = true;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    IsAddressZipCodeHighlight = true;
                }
                if (string.IsNullOrEmpty(PatRepresViewModel.Email))
                {
                    IsEmailHighlight = true;
                }
                if (string.IsNullOrEmpty(PatRepresViewModel.HomePhone) && string.IsNullOrEmpty(PatRepresViewModel.WorkPhone) && string.IsNullOrEmpty(PatRepresViewModel.CellPhone))
                {
                    IsPhoneNumberHighlight = true;
                }
                ValidationWarningTrigger = true;
                return null;
            }
            PatRepresViewModel.PatientStatusId = 1;
            PatRepresViewModel.PatientId = LoadArguments;
            PatRepresViewModel.Zip = Zip;
            //PhoneNumber Validation
            if (string.IsNullOrEmpty(PatRepresViewModel.HomePhone) && string.IsNullOrEmpty(PatRepresViewModel.CellPhone) && string.IsNullOrEmpty(PatRepresViewModel.WorkPhone))
            {
                _interactionManager.Alert("Phone Number is required!");
                return null;
            }
            //if (string.IsNullOrEmpty(PatRepresViewModel.Email))
            //{
            //    _interactionManager.Alert("Email is required!");
            //    return null;
            //}
            if (!string.IsNullOrEmpty(PatRepresViewModel.HomePhone) && PatRepresViewModel.HomePhone.Length < 10)
            {
                _interactionManager.Alert("HomePhone cannot be less than 10 digits!");
                return null;
            }
            if (!string.IsNullOrEmpty(PatRepresViewModel.WorkPhone) && PatRepresViewModel.WorkPhone.Length < 10)
            {
                _interactionManager.Alert("WorkPhone cannot be less than 10 digits!");
                return null;
            }
            if (!string.IsNullOrEmpty(PatRepresViewModel.CellPhone) && PatRepresViewModel.CellPhone.Length < 10)
            {
                _interactionManager.Alert("CellPhone cannot be less than 10 digits!");
                return null;
            }
            if (PatRepresViewModel.Addresses == null)
            {
                PatRepresViewModel.Addresses = new AddressViewModel();
            }
            if (StateOrProvince != null)
            {
                PatRepresViewModel.Addresses.City = City;
            }
            else
            {
                PatRepresViewModel.Addresses.City = string.Empty;
            }
            if (PatRepresViewModel.Zip != null)
            {
                PatRepresViewModel.Zip = Zip;
            }
            else
            {
                PatRepresViewModel.Zip = string.Empty;
            }
            if (StateOrProvince != null)
            {
                PatRepresViewModel.State = StateOrProvince.Abbreviation;
            }
            else
            {
                PatRepresViewModel.State = string.Empty;
            }
            if(PatRepresViewModel.Country==null)
            {
                PatRepresViewModel.Country = null;
            }
            if (Line1 != null)
            {
                PatRepresViewModel.Addresses.Line1 = Line1;
            }
            else
            {
                PatRepresViewModel.Addresses.Line1 = string.Empty;
            }
            if (Line2 != null)
            {
                PatRepresViewModel.Addresses.Line2 = Line2;
            }
            else
            {
                PatRepresViewModel.Addresses.Line2 = string.Empty;
            }
            //Save Empty when all other fields are null
            if (PatRepresViewModel.Title == null)
            {
                PatRepresViewModel.Title = string.Empty;
            }
            if (PatRepresViewModel.MiddleName == null)
            {
                PatRepresViewModel.MiddleName = string.Empty;
            }
            if (PatRepresViewModel.RelationshipCode == null)
            {
                PatRepresViewModel.RelationshipCode = string.Empty;
            }
            if (PatRepresViewModel.WorkPhone == null)
            {
                PatRepresViewModel.WorkPhone = string.Empty;
            }
            if (PatRepresViewModel.CellPhone == null)
            {
                PatRepresViewModel.CellPhone = string.Empty;
            }

            int _patientId = _patientRepresentativeViewService.UpdatePatientRepersentativeDetails(PatRepresViewModel);
            if (_patientId > 0)
            {
                CredentialsEntity objCredential = PostRepersentative2ERP(PatRepresViewModel.PatientId.ToString());
                if (!string.IsNullOrEmpty(objCredential._status))
                {
                    if (objCredential._status.Equals(ResponseStatus.Updated.ToString()) && !string.IsNullOrEmpty(objCredential._practiceToken))
                    {
                        objCredential._isemailaddress = true;
                        UtilitiesViewManager.OpenCredentialConfirmation(objCredential._externalId, objCredential._practiceToken, objCredential._userName, objCredential._password, PatRepresViewModel.FirstName, "Representative", objCredential._isemailaddress);
                    } 
                }
            }
            return _patientId;
        }

        private CredentialsEntity PostRepersentative2ERP(string _patientId)
        {
            bool isPatientRepersentative = false;
            CredentialsEntity objCredential = new CredentialsEntity();
            string _practiceToken = string.Empty;
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select Cast(Value as nvarchar(100)) From model.ApplicationSettings with(nolock) Where Name = 'PracticeAuthToken'");
                _practiceToken = dbConnection.Execute<string>(cmdQuery);
            }

            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select Top(1) P.Id as ExternalId,pd.email as EmailAddress, P.FirstName as PatientFirstName, ISNULL(P.MiddleName, '') as PatientMiddleName, P.LastName as PatientLastName,  ISNULL(Suffix, '') as Suffix, ISNULL(Prefix,'') Prefix, '' as CompanyName, '' as JobTitle, IsNull(Line1,'') as Address1, Isnull(Line2, '') as  Address2, Isnull(PA.City, '') as  City, Isnull(StateOrProvinceId, '') as  State, '' as Country, Isnull(PostalCode, '') as Zip, Cast(P.DateOfBirth as Date) as PatientBirthDate, '' as Notes,PR.Email as Email ,ISNULL((Select top(1) Name From model.languages Where Id = P.LanguageId),'English') as LanguageName, ISNULL(P.PreferredServiceLocationId, (Select Top(1) Id From model.ServiceLocations)) as LocationId, ISNULL(PPN.CountryCode + PPn.AreaCode + PPn.ExchangeAndSuffix,' ') as PhoneNumbers, PR.PatientRepresentativeId, PR.FirstName as RepFirstName, PR.LastName as RepLastName,PR.Zip as  RepZip,REPLACE(CONVERT (CHAR(10), PR.DateOfBirth, 101),'/','') as DOB, ISNULL(PR.IsPortalActivated,0) as RepPortalStatus From model.Patients P inner join patientdemographics pd on p.id=pd.patientid Left join model.PatientAddresses PA on PA.PatientId = P.Id LEFT Join model.PatientPhoneNumbers PPN on P.Id = PPN.PatientId Left join Model.PatientRepresentatives PR on PR.PatientId  = P.Id Where P.Id  = {0}", _patientId);

                var dtable = dbConnection.Execute<DataTable>(cmdQuery);
                if (dtable.Rows.Count > 0)
                {
                    isPatientRepersentative = Convert.ToBoolean(dtable.Rows[0]["RepPortalStatus"].ToString());
                    var parameters = new PatientEntity
                    {
                        PracticeToken = _practiceToken,
                        ExternalId = dtable.Rows[0]["ExternalId"].ToString(),
                        FirstName = dtable.Rows[0]["PatientFirstName"].ToString(),
                        LastName = dtable.Rows[0]["PatientLastName"].ToString(),
                        MiddleName = dtable.Rows[0]["PatientMiddleName"].ToString(),
                        Suffix = dtable.Rows[0]["Suffix"].ToString(),
                        Prefix = dtable.Rows[0]["Prefix"].ToString(),
                        LocationID = dtable.Rows[0]["LocationID"].ToString(),
                        EmailAddresses = dtable.Rows[0]["EmailAddress"].ToString(),
                        PhoneNumbers = dtable.Rows[0]["PhoneNumbers"].ToString(),
                        Address1 = dtable.Rows[0]["Address1"].ToString(),
                        Address2 = dtable.Rows[0]["Address2"].ToString(),
                        City = dtable.Rows[0]["LocationID"].ToString(),
                        State = dtable.Rows[0]["State"].ToString(),
                        Country = dtable.Rows[0]["Country"].ToString(),
                        Zip = dtable.Rows[0]["Zip"].ToString(),
                        BirthDate = dtable.Rows[0]["PatientBirthDate"].ToString(),
                        Notes = dtable.Rows[0]["Notes"].ToString(),
                        LanguageName = dtable.Rows[0]["LanguageName"].ToString(),
                        UserName = null,
                        Password = null,
                        RepersentativeDetail = new PatientRepresentativeDetail()
                        {
                            FirstName = dtable.Rows[0]["RepFirstName"].ToString(),
                            LastName = dtable.Rows[0]["RepLastName"].ToString(),
                            Password = isPatientRepersentative == false ? dtable.Rows[0]["RepLastName"].ToString() + "_" + dtable.Rows[0]["DOB"].ToString() : "",
                            RepresentativeId = dtable.Rows[0]["PatientRepresentativeId"].ToString(),
                            UserName = isPatientRepersentative == false ? dtable.Rows[0]["RepLastName"].ToString() + "" + dtable.Rows[0]["RepFirstName"].ToString() + "-" + _patientId : "",
                            Email = string.IsNullOrEmpty(Convert.ToString(dtable.Rows[0]["Email"])) ? string.Empty : Convert.ToString(dtable.Rows[0]["Email"])
                        }
                    };
                    var portalSvc = new PortalServices();
                    if (!isPatientRepersentative)
                    {
                        cmdQuery = String.Format(@"Update Model.PatientRepresentatives Set IsPortalActivated =  1 Where PatientId = {0}", _patientId);
                        dbConnection.Execute(cmdQuery);

                        objCredential._status = portalSvc.UpdateDemographics(parameters);
                        objCredential._externalId = parameters.RepersentativeDetail.RepresentativeId;
                        objCredential._practiceToken = parameters.PracticeToken;
                        objCredential._userName = parameters.RepersentativeDetail.UserName;
                        objCredential._password = parameters.RepersentativeDetail.Password;
                    }
                    else
                    {
                        objCredential._status = portalSvc.UpdateDemographics(parameters);
                    }
                    dtable.Dispose();
                }
                else
                {
                    objCredential._status = "Information is missing";
                }
            }
            return objCredential;
        }

        private void InteractionContextCompleting(object sender, CancelEventArgs e)
        {
            //we want to show this popup anytime the user tries to leave without saving.  
            //If they leave without saving AND have unbilled services and/or never-billed services, we want 
            //to show both popups.  This should really never happen, though.  Users are expected to click Save, then Bill. 
            //These popups are just precautions/warnings when users leave services in questionable states (unbilled or never billed).

            if (PatRepresViewModel.As<IEditableObjectExtended>().IsChanged)
            {
                var loadArgument = new ConfirmationDialogLoadArguments
                {
                    Cancel = CancelExitWithUnsavedChanges,
                    Content = "You have unsaved changes.  Would you like to save your changes?",
                    YesContent = "Save",
                    NoContent = "Don't Save",
                    CancelContent = "Cancel",
                    Yes = Save
                };
                _confirmationDialogViewManager.ShowConfirmationDialogView(loadArgument);
            }

            e.Cancel = _stopExitingWindow;
            _stopExitingWindow = false;
        }

        private bool CanEventExecute()
        {
            return true;
        }
    }
}