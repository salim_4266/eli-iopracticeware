﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientFinancialSummary
{
    class CommentFilterSelectionChangedBehavior : Behavior<RadGridView>
    {

        private PropertyChangeNotifier _source;
        protected override void OnAttached()
        {
            base.OnAttached();
            _source = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty).AddValueChanged(SourceChanged);

        }

        protected override void OnDetaching()
        {
            _source.RemoveValueChanged(SourceChanged);
            base.OnDetaching();
        }

        private void SourceChanged(object sender, EventArgs e)
        {
            var sourceValue = ((ObservableCollection<TransactionalNoteViewModel>)(((PropertyChangeNotifier)(sender)).Value));

            if (sourceValue != null && sourceValue.Count > 0)
            {
                AssociatedObject.FilterDescriptors.SuspendNotifications();
                SetCommentFilters();
                AssociatedObject.FilterDescriptors.ResumeNotifications();
            }
        }

        private FilterDescriptor<TransactionalNoteViewModel> _descriptorNotesFiltersComboBox;

        public static readonly DependencyProperty CommentFiltersComboBoxProperty = DependencyProperty
          .Register("CommentFiltersComboBox", typeof(RadComboBox), typeof(CommentFilterSelectionChangedBehavior), new PropertyMetadata(OnCommentFiltersComboBoxChanged));

        private static void OnCommentFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (CommentFilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnCommentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnCommentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetCommentFilters();
            }
        }

        public RadComboBox CommentFiltersComboBox
        {
            get { return (RadComboBox)GetValue(CommentFiltersComboBoxProperty); }
            set
            {
                SetValue(CommentFiltersComboBoxProperty, value);
            }
        }

        void OnCommentFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetCommentFilters();
        }

        private void SetCommentFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorNotesFiltersComboBox == null)
            {
                _descriptorNotesFiltersComboBox = new FilterDescriptor<TransactionalNoteViewModel>();
            }
            var filter = CommentFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => (t == null);
                    break;
                // Hide Archived
                case 1:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => (!t.IsArchived);
                    break;
                // Hide Active
                case 2:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => (t.IsArchived);
                    break;
                // Show All .
                case 3:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => (t != null);
                    break;
            }
            AssociatedObject.FilterDescriptors.Add(_descriptorNotesFiltersComboBox);
        }
    }
}
