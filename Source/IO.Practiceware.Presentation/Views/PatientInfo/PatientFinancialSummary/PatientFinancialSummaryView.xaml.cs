﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientFinancialSummary
{
    /// <summary>
    /// Interaction logic for PatientFinancialSummaryView.xaml
    /// </summary>
    public partial class PatientFinancialSummaryView
    {

        public PatientFinancialSummaryView()
        {
            InitializeComponent();
        }
    }

    public class PatientFinancialSummaryViewContextReference : DataContextReference
    {

        public new PatientFinancialSummaryViewContext DataContext
        {
            get { return (PatientFinancialSummaryViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }

    }
}
