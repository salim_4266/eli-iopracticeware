﻿using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary;
using IO.Practiceware.Presentation.Views.Alerts;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Presentation.Views.Financials.EditInvoice;
using IO.Practiceware.Presentation.Views.Financials.PatientStatements;
using IO.Practiceware.Presentation.Views.Financials.PostAdjustments;
using IO.Practiceware.Presentation.Views.Notes;
using IO.Practiceware.Presentation.Views.Reporting;
using IO.Practiceware.Presentation.ViewServices.Alerts;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientFinancialSummary;
using IO.Practiceware.Services.Utilities;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Application;
using thread=System.Threading.Tasks;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientFinancialSummary
{
    public class PatientFinancialSummaryViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        private readonly IPatientFinancialSummaryViewService _patientFinancialSummaryViewService;
        private readonly PostAdjustmentsViewManager _postAdjustmentsViewManager;
        private readonly PatientStatementsViewManager _patientStatementsViewManager;
        private readonly ReportViewManager _reportViewManager;
        private readonly DocumentViewManager _documentViewManger;
        private readonly EditInvoiceViewManager _editInvoiceViewManager;
        private readonly NoteViewManager _noteViewManager;
        private readonly AlertViewManager _alertViewManager;
        private readonly IAlertViewService _alertViewService;
        private readonly IInteractionManager _interactionManager;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private ApplicationSettingContainer<List<int>> userFilter;
        private bool _reprintClaims;

        [DispatcherThread]
        public virtual bool PrintFinishedTrigger { get; set; }

        [DispatcherThread]
        public virtual bool AlertsTrigger { get; set; }

        public virtual bool IsIcd10 { get; set; }

        /// <summary>
        /// The following data is for demonstration purposes only; please delete as necessary while implementing
        /// </summary>
        public PatientFinancialSummaryViewContext(IPatientFinancialSummaryViewService patientFinancialSummaryViewService,
            PostAdjustmentsViewManager postAdjustmentsViewManager,
            PatientStatementsViewManager patientStatementsViewManager,
            ReportViewManager reportViewManager,
            ICommonViewFeatureExchange exchange,
            DocumentViewManager documentViewManager,
            EditInvoiceViewManager editInvoiceViewManager,
            NoteViewManager noteViewManager,
            IInteractionManager interactionManager, AlertViewManager alertViewManager, IAlertViewService alertViewService,IApplicationSettingsService applicationSettingsService)
        {
            _reportViewManager = reportViewManager;
            _patientFinancialSummaryViewService = patientFinancialSummaryViewService;
            _documentViewManger = documentViewManager;
            _editInvoiceViewManager = editInvoiceViewManager;
            _noteViewManager = noteViewManager;
            _interactionManager = interactionManager;
            _alertViewManager = alertViewManager;
            _alertViewService = alertViewService;
            _applicationSettingsService = applicationSettingsService;
            _postAdjustmentsViewManager = postAdjustmentsViewManager;
            _patientStatementsViewManager = patientStatementsViewManager;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            LoadPatientStatement = Command.Create<int>(ExecuteLoadPatientStatement);
            LoadPatientStatementScreen = Command.Create(ExecuteLoadPatientStatementScreen);
            LoadPatientReceipt = Command.Create<int>(ExecuteLoadPatientReceipt);
            ShowPostAdjustments = Command.Create<object>(ExecuteShowPostAdjustments, CanPostAdjustments);
            ShowEditScreen = Command.Create<object>(ExecuteShowEditScreen);
            AddEditInvoice = Command.Create<InvoiceViewModel>(ExecuteAddEditInvoice);
            LoadInvoice = Command.Create<InvoiceViewModel>(ExecuteLoadInvoiceDetails).Async(() => InteractionContext);
            LoadClaim = Command.Create<PaperClaimGenerationInfo>(ExecuteLoadClaim).Async(() => InteractionContext);
            ChangeFilter = Command.Create(ExecuteChangeFilter).Async(() => InteractionContext);
            RefreshAlertStatus = Command.Create(ExecuteRefreshAlertStatus).Async(() => InteractionContext);
            OpenAccountHistory = Command.Create(ExecuteOpenAccountHistory);
            ShowNote = Command.Create<object>(ExecuteShowNoteScreen, CanShowNote);
            ShowBillingServiceNote = Command.Create<object>(ExecuteShowBillingServiceNote, CanShowBillingServiceNote);
            ShowAlertScreen = Command.Create(() => ExecuteShowAlertScreen(true)).Async(() => InteractionContext);
            AttachDescription = Command.Create<DiagnosisViewModel>(ExecuteAttachDescription).Async(() => InteractionContext);
            DeleteComment = Command.Create<object>(ExecuteDeleteComment).Async(() => InteractionContext);

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
            ViewFeatureExchange.ShowPendingAlert = ShowAlertScreen.Execute; // will update on load
        }

        private void ExecuteAttachDescription(DiagnosisViewModel diagnosisViewModel)
        {
            if (diagnosisViewModel.ActiveDiagnosisCode.Description.IsNullOrEmpty())
            {
                DiagnosisCodeViewModel tempdiagnosisCodeViewModel = _patientFinancialSummaryViewService.FetchDiagnosisDescription(diagnosisViewModel);
                diagnosisViewModel.ActiveDiagnosisCode = tempdiagnosisCodeViewModel;
                tempdiagnosisCodeViewModel = null;
            }
        }

        private static bool CanShowNote(object argument)
        {
            if (argument != null && PermissionId.ViewPatientComments.PrincipalContextHasPermission() && PermissionId.EditPatientComments.PrincipalContextHasPermission())
            {
                return true;
            }

            return false;
        }

        private static bool CanShowBillingServiceNote(object argument)
        {
            if (argument is TransactionalNoteViewModel && PermissionId.ViewPatientComments.PrincipalContextHasPermission() && PermissionId.EditPatientComments.PrincipalContextHasPermission())
            {
                return true;
            }

            return false;
        }

        private void ExecuteShowBillingServiceNote(object argument)
        {
            var loadArgument = new NoteLoadArguments();
            if (argument is TransactionalNoteViewModel)
            {
                loadArgument.NoteType = NoteType.BillingServiceComment;
                loadArgument.NoteId = argument.CastTo<TransactionalNoteViewModel>().Id;
            }
            _noteViewManager.LaunchNoteView(loadArgument);
            ChangeFilter.Execute();
            RefreshAlertStatus.Execute();
        }

        private void ExecuteShowNoteScreen(object argument)
        {
            var loadArgument = new NoteLoadArguments();
            if (argument is InvoiceViewModel)
            {
                loadArgument.NoteType = NoteType.InvoiceComment;
                loadArgument.EntityId = argument.CastTo<InvoiceViewModel>().Id;
            }
            else if (argument is TransactionalNoteViewModel)
            {
                loadArgument.NoteType = NoteType.InvoiceComment;
                loadArgument.NoteId = argument.CastTo<TransactionalNoteViewModel>().Id;
            }
            else if (argument is EncounterServiceViewModel)
            {
                loadArgument.NoteType = NoteType.BillingServiceComment;
                loadArgument.EntityId = argument.CastTo<EncounterServiceViewModel>().Id;
            }

            _noteViewManager.LaunchNoteView(loadArgument);
            ChangeFilter.Execute();
            RefreshAlertStatus.Execute();

        }

        private void ExecuteDeleteComment(object argument)
        {
            if (_interactionManager.Confirm("Are you sure you wish to delete this comment?", "Cancel", "Delete") == true)
            {
                if (argument is TransactionalNoteViewModel)
                {
                    var noteViewModelToDelete = argument.CastTo<TransactionalNoteViewModel>();
                    if (noteViewModelToDelete != null)
                    {
                        _patientFinancialSummaryViewService.DeleteComment(noteViewModelToDelete);
                        ChangeFilter.Execute();
                        RefreshAlertStatus.Execute();
                    }
                }
            }
        }

        private void ExecuteShowAlertScreen(bool showNoAlertPopup)
        {
            var loadArgument = new NoteLoadArguments();
            loadArgument.AlertScreen = AlertScreen.PatientFinancials;
            loadArgument.NoteType = NoteType.PatientFinancialComment;
            loadArgument.EntityId = PatientInfoViewModel.PatientId;

            if (!loadArgument.EntityId.HasValue)
            {
                return;
            }

            if (_alertViewService.HasAlerts(loadArgument))
            {
                this.Dispatcher().Invoke(() => _alertViewManager.ShowAlerts(loadArgument));
                ViewFeatureExchange.HasPendingAlert = _alertViewService.HasAlerts(loadArgument);
                ChangeFilter.Execute();
                RefreshAlertStatus.Execute();
            }
            else
            {
                AlertsTrigger = showNoAlertPopup;
            }
        }

        private void ExecuteLoadPatientStatement(int encounterId)
        {
            var parameters = new Hashtable();
            parameters.Add("PatientId", PatientInfoViewModel.PatientId.GetValueOrDefault());
            parameters.Add("EncounterId", encounterId);
            _reportViewManager.ViewReport("Patient Statement", "", parameters);
        }

        private void ExecuteLoadPatientStatementScreen()
        {
            _patientStatementsViewManager.ShowPatientStatements(PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault());
        }

        private void ExecuteLoadPatientReceipt(int encounterId)
        {
            var parameters = new Hashtable();
            parameters.Add("EncounterId", encounterId);
            _reportViewManager.ViewReport("Patient Receipt", "", parameters);
        }

        private void ExecuteOpenAccountHistory()
        {
            var view = new DateRangeInput();
            var openHistory = _interactionManager.Confirm(view, buttons: ConfirmButtons.OkCancel);
            if (!openHistory.GetValueOrDefault()) return;

            var parameters = new Hashtable();
            parameters.Add("PatientId", PatientInfoViewModel.PatientId.GetValueOrDefault());
            parameters.Add("StartDate", view.StartDate);
            parameters.Add("EndDate", view.EndDate);
            _reportViewManager.ViewReport("Patient Account History", string.Empty, parameters);
        }

        private void ExecuteLoadClaim(PaperClaimGenerationInfo claimViewModel)
        {
            var htmlContents = _patientFinancialSummaryViewService.LoadClaim(claimViewModel);

            var pdfBytes = WkHtmlToPdfWrapperService.GeneratePdf(htmlContents.Content);

            var listOfPathsToDelete = new List<string>();
            var listOfGeneratedPdfPaths = new List<Tuple<string, string>>();
            var path = FileManager.Instance.GetTempPathName(); // get a temp file path

            path = path.Replace(".tmp", ".pdf");
            FileManager.Instance.CommitContents(path, pdfBytes); // put the pdf bytes to the path
            listOfPathsToDelete.Add(path); // save the path to delete
            const string documentName = "Insurer Claim"; // set the document name
            listOfGeneratedPdfPaths.Add(new Tuple<string, string>(path, documentName));
            this.Dispatcher().Invoke(() => DoActualPrint(listOfGeneratedPdfPaths)); // print
            // delete all paths
            listOfPathsToDelete.ForEach(FileManager.Instance.Delete); // delete the path
        }

        private void ExecuteLoad()
        {
            if (!PermissionId.ViewPatientFinancialSummaryInformation.PrincipalContextHasPermission()) return;
            var financialUserFilters = _applicationSettingsService.GetSetting<List<int>>(
            ApplicationSetting.FinancialUserFilters, ApplicationContext.Current.ComputerName, UserContext.Current.UserDetails.Id);
            userFilter = financialUserFilters; 

            // Refresh status of current alerts
            RefreshAlertStatus.Execute();
            // UI elements (brushes and etc) must be created on UI thread
            this.Dispatcher().Invoke(() =>
            {
                InvoiceFilters = CreateInvoiceFilterSelections();
                PaymentFilters = CreatePaymentFilterSelections();
                AdjustmentFilters = CreateAdjustmentFilterSelections();
                InformationalFilters = CreateInformationalFilterSelections();
                BillingFilters = CreateBillingFilterSelections();
                NoteFilters = CreateNoteFilterSelections();
                //Default
                InvoiceFilter = InvoiceFilters[0];
                PaymentsFilter = PaymentFilters[3];
                AdjustmentsFilter = AdjustmentFilters[3];
                InformationalFilter = InformationalFilters[0];
                BillingFilter = BillingFilters[3];
                NoteFilter = NoteFilters[1];
            });

            if (null == financialUserFilters)
            {
                int[] arr = new int[6];
                arr[0] = InvoiceFilter.Id;
                arr[1] = PaymentsFilter.Id;
                arr[2] = AdjustmentsFilter.Id;
                arr[3] = InformationalFilter.Id;
                arr[4] = BillingFilter.Id;
                arr[5] = NoteFilter.Id;
                List<int> filters = arr.ToList();
                ApplicationSettingContainer<List<int>> billingFilters = new ApplicationSettingContainer<List<int>>
                {
                    MachineName = ApplicationContext.Current.ComputerName,
                    Name = ApplicationSetting.FinancialUserFilters,
                    Value = filters,
                    Id = null,
                    UserId = UserContext.Current.UserDetails.Id
                };
                userFilter = billingFilters;
                _applicationSettingsService.SetSetting(billingFilters);
            }
            else
            {
                    int[] arr = financialUserFilters.Value.ToArray();
                    InvoiceFilter = InvoiceFilters[arr[0]];
                    PaymentsFilter = PaymentFilters[arr[1]];
                    AdjustmentsFilter = AdjustmentFilters[arr[2]];
                    InformationalFilter = InformationalFilters[arr[3]];
                    BillingFilter = BillingFilters[arr[4]];
                    NoteFilter = NoteFilters[arr[5]];
            }

            ExecuteShowAlertScreen(false);

       }
        
        private void ExecuteChangeFilter()
        {
            var invoicesShowingDetails = Account != null
                ? Account.SelectedInvoices.IfNotNull(x => x.Select(y => y.Id).ToList(), new List<int>())
                : new List<int>();

            Account = _patientFinancialSummaryViewService.LoadPatientFinancialSummaryInformation(PatientInfoViewModel.PatientId.GetValueOrDefault(), InvoiceFilter, invoicesShowingDetails);

            // Restore selected invoices
            if (Account.Invoices != null) Account.SelectedInvoices = Account.Invoices.Where(x => invoicesShowingDetails.Contains(x.Id)).ToObservableCollection();
       
        
        }

        private void ExecuteLoadInvoiceDetails(InvoiceViewModel invoice)
        {
            if (Account.Invoices.Single(i => i.Id == invoice.Id) != null)
            {
                var invoiceReplacement = _patientFinancialSummaryViewService.LoadFullInvoiceViewModel(invoice);
                var originalInvoice = Account.Invoices.Single(i => i.Id == invoiceReplacement.Id);
                originalInvoice.UnassignedTransactions = invoiceReplacement.UnassignedTransactions;
                originalInvoice.Diagnoses = invoiceReplacement.Diagnoses;
                originalInvoice.Notes = invoiceReplacement.Notes;
                foreach (var billingService in originalInvoice.EncounterServices)
                {
                    var encounterServiceReplacement = invoiceReplacement.EncounterServices.Single(es => es.Id == billingService.Id && es.InvoiceId == billingService.InvoiceId);
                    billingService.Transactions = encounterServiceReplacement.Transactions;
                    billingService.Modifiers = encounterServiceReplacement.Modifiers;
                    billingService.Allowed = encounterServiceReplacement.Allowed;
                    billingService.LinkedDiagnoses = encounterServiceReplacement.LinkedDiagnoses;
                    billingService.Date = encounterServiceReplacement.Date;
                    billingService.Service = encounterServiceReplacement.Service;
                    billingService.ClaimNote = encounterServiceReplacement.ClaimNote;
                    encounterServiceReplacement = null;
                }
            }
        }

        private void ExecuteAddEditInvoice(InvoiceViewModel invoice)
        {
            var loadArguments = new EditInvoiceLoadArguments
                                {
                                    PatientId = PatientInfoViewModel.PatientId.GetValueOrDefault(),
                                    InvoiceId = invoice != null ? invoice.Id : (int?)null
                                };
            this.Dispatcher().BeginInvoke(() =>
            {
                _editInvoiceViewManager.ShowEditInvoice(loadArguments);
                ChangeFilter.Execute();
                RefreshAlertStatus.Execute();
            });
        }

        private void ExecuteShowEditScreen(object argument)
        {
            if (argument is FinancialTransactionViewModel)
            {
                ExecuteShowPostAdjustments(argument);
            }

            if (argument is TransactionalNoteViewModel && PermissionId.EditPatientComments.PrincipalContextHasPermission())
            {
                ExecuteShowBillingServiceNote(argument);
            }
        }


        private void ExecuteShowPostAdjustments(object argument)
        {
            if (!CanPostAdjustments(argument)) return;

            // Prepare load arguments (default opens for all invoices of patient)
            var loadArguments = new PostAdjustmentsLoadArguments
            {
                PatientId = PatientInfoViewModel.PatientId
            };

            if (argument is InvoiceViewModel)
            {
                // Filter by invoice id
                loadArguments.InvoiceId = argument.CastTo<InvoiceViewModel>().Id;
            }
            else if (argument is FinancialTransactionViewModel)
            {
                // Load in context of transaction we are interested in
                loadArguments.TransactionOfInterest = argument.CastTo<FinancialTransactionViewModel>();
                loadArguments.InvoiceId = argument.CastTo<FinancialTransactionViewModel>().InvoiceId;
            }

            this.Dispatcher().BeginInvoke(() =>
            {
                _postAdjustmentsViewManager.ShowPostPaymentsAndAdjustments(loadArguments);
                ChangeFilter.Execute();
                RefreshAlertStatus.Execute();
            });
        }

        private bool CanPostAdjustments(object argument)
        {
            // No point in attempting to create new batch without add financials permission
            if (argument == null || argument is InvoiceViewModel)
            {
                return PermissionId.AddFinancialTransactions.PrincipalContextHasPermission();
            }

            return PermissionId.AddFinancialTransactions.PrincipalContextHasPermission()
                   || PermissionId.EditFinancialTransactions.PrincipalContextHasPermission()
                   || PermissionId.DeleteFinancialTransactions.PrincipalContextHasPermission();
        }

        private void DoActualPrint(List<Tuple<string, string>> listOfGeneratedPdfPaths)
        {
            if (listOfGeneratedPdfPaths.IsNullOrEmpty()) return;

            _reprintClaims = true;

            while (_reprintClaims) // if still true, user hit the 'Reprint' button
            {
                _reprintClaims = false;

                _documentViewManger.HeaderTitleOfCurrentWindow = "Submit Claims";
                _documentViewManger.PrintDocuments(listOfGeneratedPdfPaths.First().Item2, listOfGeneratedPdfPaths.Select(x => x.Item1).ToArray());

                PrintFinishedTrigger = true; // ask again.
                PrintFinishedTrigger = false;
            }
        }

        void ExecuteRefreshAlertStatus()
        {
            if (PatientInfoViewModel != null && PatientInfoViewModel.PatientId.HasValue)
            {
                var loadArgument = new NoteLoadArguments();
                loadArgument.AlertScreen = AlertScreen.PatientFinancials;
                loadArgument.NoteType = NoteType.PatientFinancialComment;
                loadArgument.EntityId = PatientInfoViewModel.PatientId;
                ViewFeatureExchange.HasPendingAlert = _alertViewService.HasAlerts(loadArgument);
            }
        }

        private void OnRefresh()
        {
            ChangeFilter.Execute();
        }

        #region Filter
        private ApplicationSettingContainer<List<int>> CreateBillingFilters()
        {
           var financialUserFilters = _applicationSettingsService.GetSetting<List<int>>(
           ApplicationSetting.FinancialUserFilters, ApplicationContext.Current.ComputerName, UserContext.Current.UserDetails.Id);
           return financialUserFilters;
        }
        
        private static ExtendedObservableCollection<FilterSelectionViewModel> CreateInvoiceFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Id = 0, Name = "Open Invoices", Color = Financials.Common.Resources.OpenInvoiceBrush });
            toReturn.Add(new FilterSelectionViewModel { Id = 1, Name = "All Invoices", Color = Financials.Common.Resources.CreateOpenAndClosedInvoiceBrush() });
            return toReturn;
        }

        private static ExtendedObservableCollection<FilterSelectionViewModel> CreatePaymentFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Name = "Hide Payments", Color = Financials.Common.Resources.EmptyInvoiceBrush, Id = 0 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Insurer Payments", Color = Financials.Common.Resources.InsurerPaymentBrush, Id = 1 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Patient Payments", Color = Financials.Common.Resources.PatientPaymentBrush, Id = 2 });
            toReturn.Add(new FilterSelectionViewModel { Name = "All Payments", Color = Financials.Common.Resources.CreateInsurerAndPatientPaymentBrush(), Id = 3 });
            return toReturn;
        }

        private static ExtendedObservableCollection<FilterSelectionViewModel> CreateAdjustmentFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Name = "Hide Adjustments", Color = Financials.Common.Resources.EmptyInvoiceBrush, Id = 0 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Insurer Adjustments", Color = Financials.Common.Resources.InsurerAdjustmentBrush, Id = 1 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Patient & Office Adjustments", Color = Financials.Common.Resources.PatientAdjustmentBrush, Id = 2 });
            toReturn.Add(new FilterSelectionViewModel { Name = "All Adjustments", Color = Financials.Common.Resources.CreateInsurerAndPatientAdjustmentBrush(), Id = 3 });
            return toReturn;
        }

        private static ExtendedObservableCollection<FilterSelectionViewModel> CreateBillingFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Name = "Hide Billing", Color = Financials.Common.Resources.EmptyInvoiceBrush, Id = 0 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Insurer Billing", Color = Financials.Common.Resources.BillingBrush, Id = 1 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Patient Billing", Color = Financials.Common.Resources.BillingBrush, Id = 2 });
            toReturn.Add(new FilterSelectionViewModel { Name = "All Billing", Color = Financials.Common.Resources.BillingBrush, Id = 3 });
            return toReturn;
        }
        private static ExtendedObservableCollection<FilterSelectionViewModel> CreateNoteFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Name = "Hide Comments", Color = Financials.Common.Resources.EmptyInvoiceBrush, Id = 0 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Active Comments", Color = Financials.Common.Resources.NoteBrush, Id = 1 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Archived Comments", Color = Financials.Common.Resources.NoteBrush, Id = 2 });
            toReturn.Add(new FilterSelectionViewModel { Name = "All Comments", Color = Financials.Common.Resources.NoteBrush, Id = 3 });
            return toReturn;
        }

        private static ExtendedObservableCollection<FilterSelectionViewModel> CreateInformationalFilterSelections()
        {
            var toReturn = new ExtendedObservableCollection<FilterSelectionViewModel>();
            toReturn.Add(new FilterSelectionViewModel { Name = "Hide Informational", Color = Financials.Common.Resources.EmptyInvoiceBrush, Id = 0 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Insurer Informational", Color = Financials.Common.Resources.InformationalBrush, Id = 1 });
            toReturn.Add(new FilterSelectionViewModel { Name = "Patient & Office Informational", Color = Financials.Common.Resources.InformationalBrush, Id = 2 });
            toReturn.Add(new FilterSelectionViewModel { Name = "All Informational", Color = Financials.Common.Resources.InformationalBrush, Id = 3 });
            return toReturn;
        }

        #endregion


        public IInteractionContext InteractionContext { get; set; }


        #region IPatientInfoViewContext Members

        public virtual ICommand Load { get; set; }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            int[] arr = new int[6];
            arr[0] = InvoiceFilter.Id;
            arr[1] = PaymentsFilter.Id;
            arr[2] = AdjustmentsFilter.Id;
            arr[3] = InformationalFilter.Id;
            arr[4] = BillingFilter.Id;
            arr[5] = NoteFilter.Id;
            List<int> filters = arr.ToList();
            userFilter.Value = filters;
            thread.Task.Factory.StartNew(SavePreference);
            return null;
        }

        private void SavePreference()
        {
            _applicationSettingsService.SetSetting(userFilter);
        }
        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        #endregion

        [DispatcherThread]
        public virtual PatientAccountViewModel Account { get; set; }

        #region FiltersSelectors
        [DispatcherThread]
        public virtual FilterSelectionViewModel InvoiceFilter
        {
            get { return _invoiceFilterSelected; }
            set
            {
                var changed = !Equals(_invoiceFilterSelected, value);
                if (changed)
                {
                    _invoiceFilterSelected = value;
                    ChangeFilter.Execute();
                }

            }
        }
        private FilterSelectionViewModel _invoiceFilterSelected;

        [DispatcherThread]
        public virtual FilterSelectionViewModel PaymentsFilter { get; set; }
        [DispatcherThread]
        public virtual FilterSelectionViewModel AdjustmentsFilter { get; set; }
        [DispatcherThread]
        public virtual FilterSelectionViewModel BillingFilter { get; set; }
        [DispatcherThread]
        public virtual FilterSelectionViewModel InformationalFilter { get; set; }
        [DispatcherThread]
        public virtual FilterSelectionViewModel NoteFilter { get; set; }

        #endregion
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> InvoiceFilters { get; set; }
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> PaymentFilters { get; set; }
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> AdjustmentFilters { get; set; }
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> BillingFilters { get; set; }
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> NoteFilters { get; set; }
        [DispatcherThread]
        public virtual ExtendedObservableCollection<FilterSelectionViewModel> InformationalFilters { get; set; }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }

        public virtual ICommand LoadPatientStatementScreen { get; protected set; }
        public virtual ICommand RefreshAlertStatus { get; protected set; }
        public virtual ICommand ChangeFilter { get; set; }
        public virtual ICommand AddEditInvoice { get; protected set; }
        public virtual ICommand LoadPatientStatement { get; set; }
        public virtual ICommand LoadPatientReceipt { get; set; }
        public virtual ICommand ShowPostAdjustments { get; set; }
        public virtual ICommand ShowEditScreen { get; set; }
        public virtual ICommand LoadInvoice { get; set; }
        public virtual ICommand LoadClaim { get; set; }
        public virtual ICommand ShowNote { get; protected set; }
        public virtual ICommand ShowBillingServiceNote { get; protected set; }
        public virtual ICommand OpenAccountHistory { get; protected set; }
        public virtual ICommand ShowAlertScreen { get; protected set; }
        public ICommand AttachDescription { get; protected set; }
        public virtual ICommand DeleteComment { get; protected set; }
    }
}
