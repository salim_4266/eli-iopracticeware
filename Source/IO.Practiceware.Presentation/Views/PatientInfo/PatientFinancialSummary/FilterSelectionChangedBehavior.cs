﻿using System;
using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using Soaf;
using System.Windows;
using System.Windows.Interactivity;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using FinancialSourceType = IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction.FinancialSourceType;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientFinancialSummary
{
    public class FilterSelectionChangedBehavior : Behavior<RadGridView>
    {
        private FilterDescriptor<TransactionViewModel> _descriptorBillingFiltersComboBox;

        public static readonly DependencyProperty BillingFiltersComboBoxProperty = DependencyProperty
          .Register("BillingFiltersComboBox", typeof(RadComboBox), typeof(FilterSelectionChangedBehavior), new PropertyMetadata(OnBillingFiltersComboBoxChanged));

        private static void OnBillingFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (FilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnBillingFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnBillingFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetBillingFilters();
            }
        }

        public RadComboBox BillingFiltersComboBox
        {
            get { return (RadComboBox)GetValue(BillingFiltersComboBoxProperty); }
            set
            {
                SetValue(BillingFiltersComboBoxProperty, value);
            }
        }

        void OnBillingFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetBillingFilters();
        }

        private void SetBillingFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorBillingFiltersComboBox == null)
            {
                _descriptorBillingFiltersComboBox = new FilterDescriptor<TransactionViewModel>();
            }
            var filter = BillingFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorBillingFiltersComboBox.FilteringExpression = t => !(t is BillingTransactionViewModel) || (t is BillingTransactionViewModel && (t.CastTo<BillingTransactionViewModel>().Receiver == null));
                    break;
                // Hide Patient .
                case 1:
                    //If receiver id is 0 that means payer is patient.
                    _descriptorBillingFiltersComboBox.FilteringExpression = t => !(t is BillingTransactionViewModel) || (t is BillingTransactionViewModel && (t.CastTo<BillingTransactionViewModel>().Receiver.Id != 0));
                    break;
                //// Hide Insurer .
                case 2:
                    //If receiver id is 0 that means payer is patient.
                    _descriptorBillingFiltersComboBox.FilteringExpression = t => !(t is BillingTransactionViewModel) || (t is BillingTransactionViewModel && (t.CastTo<BillingTransactionViewModel>().Receiver.Id == 0));
                    break;
                // Show All .
                case 3:
                    _descriptorBillingFiltersComboBox.FilteringExpression = t => !(t is BillingTransactionViewModel) || (t is BillingTransactionViewModel && (t.CastTo<BillingTransactionViewModel>().Receiver != null));
                    break;

            }
            AssociatedObject.FilterDescriptors.Add(_descriptorBillingFiltersComboBox);
        }

        private FilterDescriptor<TransactionViewModel> _descriptorNotesFiltersComboBox;

        public static readonly DependencyProperty NoteFiltersComboBoxProperty = DependencyProperty
          .Register("NoteFiltersComboBox", typeof(RadComboBox), typeof(FilterSelectionChangedBehavior), new PropertyMetadata(OnNoteFiltersComboBoxChanged));

        private static void OnNoteFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (FilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnNoteFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnNoteFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetNoteFilters();
            }
        }


        public RadComboBox NoteFiltersComboBox
        {
            get { return (RadComboBox)GetValue(NoteFiltersComboBoxProperty); }
            set
            {
                SetValue(NoteFiltersComboBoxProperty, value);
            }
        }


        void OnNoteFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetNoteFilters();
        }

        private void SetNoteFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorNotesFiltersComboBox == null)
            {
                _descriptorNotesFiltersComboBox = new FilterDescriptor<TransactionViewModel>();
            }
            var filter = NoteFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => !(t is TransactionalNoteViewModel) || (t is TransactionalNoteViewModel && (t.CastTo<TransactionalNoteViewModel>() == null));
                    break;
                // Hide Archived
                case 1:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => !(t is TransactionalNoteViewModel) || (t is TransactionalNoteViewModel && (t.CastTo<TransactionalNoteViewModel>().IsArchived == false));
                    break;
                // Hide Active
                case 2:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => !(t is TransactionalNoteViewModel) || (t is TransactionalNoteViewModel && (t.CastTo<TransactionalNoteViewModel>().IsArchived));
                    break;
                // Show All .
                case 3:
                    _descriptorNotesFiltersComboBox.FilteringExpression = t => !(t is TransactionalNoteViewModel) || (t is TransactionalNoteViewModel && (t.CastTo<TransactionalNoteViewModel>() != null));
                    break;

            }
            AssociatedObject.FilterDescriptors.Add(_descriptorNotesFiltersComboBox);
        }

        private FilterDescriptor<TransactionViewModel> _descriptorPaymentFiltersComboBox;

        public static readonly DependencyProperty PaymentFiltersComboBoxProperty = DependencyProperty
          .Register("PaymentFiltersComboBox", typeof(RadComboBox), typeof(FilterSelectionChangedBehavior), new PropertyMetadata(OnPaymentFiltersComboBoxChanged));

        private static void OnPaymentFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (FilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnPaymentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnPaymentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetPaymentFilters();
            }
        }

        public RadComboBox PaymentFiltersComboBox
        {
            get { return (RadComboBox)GetValue(PaymentFiltersComboBoxProperty); }
            set
            {
                SetValue(PaymentFiltersComboBoxProperty, value);
            }
        }

        void OnPaymentFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetPaymentFilters();
        }

        private void SetPaymentFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorPaymentFiltersComboBox == null)
            {
                _descriptorPaymentFiltersComboBox = new FilterDescriptor<TransactionViewModel>();
            }
            var filter = PaymentFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorPaymentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            !ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash);
                    break;
                // Hide Patient and Offices.
                case 1:
                    _descriptorPaymentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false) ||
                                                                (IsAdjustmentType(t) &&
                                                                ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash && ConvertToFinancialType(t).Source.Id == (int)FinancialSourceType.Insurer);
                    break;
                // Hide Insurer.
                case 2:
                    _descriptorPaymentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash && ConvertToFinancialType(t).Source.Id != (int)FinancialSourceType.Insurer);
                    break;
                // Show All.
                case 3:
                    _descriptorPaymentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash);
                    break;

            }
            AssociatedObject.FilterDescriptors.Add(_descriptorPaymentFiltersComboBox);
        }

        private FilterDescriptor<TransactionViewModel> _descriptorInformationalFiltersComboBox;

        public static readonly DependencyProperty InformationalFiltersComboBoxProperty = DependencyProperty
          .Register("InformationalFiltersComboBox", typeof(RadComboBox), typeof(FilterSelectionChangedBehavior), new PropertyMetadata(OnInformationalFiltersComboBoxChanged));

        private static void OnInformationalFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (FilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnInformationalFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnInformationalFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetInformationalFilters();
            }
        }

        public RadComboBox InformationalFiltersComboBox
        {
            get { return (RadComboBox)GetValue(InformationalFiltersComboBoxProperty); }
            set
            {
                SetValue(InformationalFiltersComboBoxProperty, value);
            }
        }

        void OnInformationalFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetInformationalFilters();
        }

        private void SetInformationalFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorInformationalFiltersComboBox == null)
            {
                _descriptorInformationalFiltersComboBox = new FilterDescriptor<TransactionViewModel>();
            }
            var filter = InformationalFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorInformationalFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || !IsInformationalType(t)
                                                            || (IsInformationalType(t) &&
                                                            ConvertToFinancialType(t).Source == null);
                    break;
                // Hide Patient and Offices.
                case 1:
                    _descriptorInformationalFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || !IsInformationalType(t) ||
                                                            (IsInformationalType(t) &&
                                                            ConvertToFinancialType(t).Source.Id == (int)FinancialSourceType.Insurer);
                    break;
                // Hide Insurer.
                case 2:
                    _descriptorInformationalFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || !IsInformationalType(t)
                                                            || (IsInformationalType(t) &&
                                                                ConvertToFinancialType(t).Source.Id != (int)FinancialSourceType.Insurer);
                    break;
                // Show All.
                case 3:
                    _descriptorInformationalFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || !IsInformationalType(t)
                                                            || (IsInformationalType(t) &&
                                                            ConvertToFinancialType(t).Source != null);
                    break;

            }
            AssociatedObject.FilterDescriptors.Add(_descriptorInformationalFiltersComboBox);
        }


        private FilterDescriptor<TransactionViewModel> _descriptorAdjustmentFiltersComboBox;

        public static readonly DependencyProperty AdjustmentFiltersComboBoxProperty = DependencyProperty
          .Register("AdjustmentFiltersComboBox", typeof(RadComboBox), typeof(FilterSelectionChangedBehavior), new PropertyMetadata(OnAdjustmentFiltersComboBoxChanged));

        private static void OnAdjustmentFiltersComboBoxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (FilterSelectionChangedBehavior)d;
            var comboBox = e.OldValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= new SelectionChangedEventHandler(behavior.OnAdjustmentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
            }

            comboBox = e.NewValue as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += new SelectionChangedEventHandler(behavior.OnAdjustmentFiltersComboBoxSelectionChanged).MakeWeak<SelectionChangedEventHandler>();
                behavior.SetAdjustmentFilters();
            }
        }

        public RadComboBox AdjustmentFiltersComboBox
        {
            get { return (RadComboBox)GetValue(AdjustmentFiltersComboBoxProperty); }
            set
            {
                SetValue(AdjustmentFiltersComboBoxProperty, value);
            }
        }

        void OnAdjustmentFiltersComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetAdjustmentFilters();
        }

        private void SetAdjustmentFilters()
        {
            if (AssociatedObject == null)
            {
                return;
            }
            if (_descriptorAdjustmentFiltersComboBox == null)
            {
                _descriptorAdjustmentFiltersComboBox = new FilterDescriptor<TransactionViewModel>();
            }
            var filter = AdjustmentFiltersComboBox.SelectedItem as FilterSelectionViewModel;
            if (filter == null) return;

            switch (filter.Id)
            {
                // Hide All.
                case 0:
                    _descriptorAdjustmentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash);
                    break;
                // Hide Patient and Offices.
                case 1:
                    _descriptorAdjustmentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash) ||
                                                             (IsAdjustmentType(t) &&
                                                             ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false && ConvertToFinancialType(t).Source.Id == (int)FinancialSourceType.Insurer);
                    break;
                // Hide Insurer.
                case 2:
                    _descriptorAdjustmentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false && ConvertToFinancialType(t).Source.Id != (int)FinancialSourceType.Insurer);
                    break;
                // Show All.
                case 3:
                    _descriptorAdjustmentFiltersComboBox.FilteringExpression = t => !(t is FinancialTransactionViewModel) || (!IsAdjustmentType(t)) ||
                                                            (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash) ||
                                                           (IsAdjustmentType(t) &&
                                                            ConvertToAdjustmentType(ConvertToFinancialType(t).FinancialTransactionType).IsCash == false);
                    break;

            }
            AssociatedObject.FilterDescriptors.Add(_descriptorAdjustmentFiltersComboBox);
        }

        private PropertyChangeNotifier _source;
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.FilterDescriptors.LogicalOperator = FilterCompositionLogicalOperator.And;
            _source = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty).AddValueChanged(SourceChanged);

        }

        protected override void OnDetaching()
        {
            _source.RemoveValueChanged(SourceChanged);
            base.OnDetaching();
        }

        private void SourceChanged(object sender, EventArgs e)
        {
            var sourceValue = ((ObservableCollection<TransactionViewModel>)(((PropertyChangeNotifier)(sender)).Value));

            if (sourceValue != null && sourceValue.Count > 0)
            {
                AssociatedObject.FilterDescriptors.SuspendNotifications();
                SetAdjustmentFilters();
                SetPaymentFilters();
                SetInformationalFilters();
                SetBillingFilters();
                SetNoteFilters();
                AssociatedObject.FilterDescriptors.ResumeNotifications();
            }
        }

        private static FinancialTransactionViewModel ConvertToFinancialType(TransactionViewModel transaction)
        {
            return transaction.CastTo<FinancialTransactionViewModel>();
        }
        private static AdjustmentTypeViewModel ConvertToAdjustmentType(FinancialTransactionTypeViewModel financialTransactionType)
        {
            return financialTransactionType.CastTo<AdjustmentTypeViewModel>();
        }
        private static bool IsAdjustmentType(TransactionViewModel transaction)
        {
            return (transaction is FinancialTransactionViewModel && (ConvertToFinancialType(transaction).FinancialTransactionType is AdjustmentTypeViewModel));
        }
        private static bool IsInformationalType(TransactionViewModel transaction)
        {
            return (transaction is FinancialTransactionViewModel && (ConvertToFinancialType(transaction).FinancialTransactionType is FinancialInformationTypeViewModel));
        }
    }
}
