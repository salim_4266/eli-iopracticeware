﻿using System;
using System.Collections.Generic;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using System.Windows.Input;
using Soaf;

namespace IO.Practiceware.Presentation.Views.PatientInfo
{
    /// <summary>
    /// Represents a view context in the category of Patient Information.  
    /// </summary>
    public interface IPatientInfoViewContext
    {
        /// <summary>
        /// Object representing the basic information for a patient
        /// </summary>
        PatientInfoViewModel PatientInfoViewModel { get; set; }

        /// <summary>
        /// Method to pass in the patient information object and an optional reload command.
        /// </summary>
        /// <param name="patientInfoViewModel"></param>
        /// <param name="reloadPatientInfoCommand"></param>
        void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null);
  
        /// <summary>
        /// Handler that get's called when th parent ViewContext (PatientInfoView) is closing.  
        /// </summary>
        /// <returns>An object containing the current IPatientInfoViewContext, a CancelClose flag, and possible Error Messages to Display</returns>
        PatientInfoClosingReturnArguments OnClosing();

        /// <summary>
        /// Command to load the information for the current view context
        /// </summary>
        ICommand Load { get; set; }
    }

    public class PatientInfoClosingReturnArguments
    {
        public IPatientInfoViewContext CurrentContext { get; set; }

        public bool CancelClose { get; set; }

        public Func<bool> ConfirmCancelClose { get; set; }

        public IDictionary<string, string> ErrorMessagesToDisplay { get; set; }
    }
}
