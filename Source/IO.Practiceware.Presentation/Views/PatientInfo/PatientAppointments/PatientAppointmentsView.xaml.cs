﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.Views.AppointmentSearch;
using IO.Practiceware.Presentation.Views.CancelAppointment;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.MultiCalendar;
using IO.Practiceware.Presentation.Views.PatientReferralPopup;
using IO.Practiceware.Presentation.Views.Recalls;
using IO.Practiceware.Presentation.Views.ScheduleAppointment;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientAppointments;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Soaf.Threading;

namespace IO.Practiceware.Presentation.Views.PatientInfo.PatientAppointments
{
    /// <summary>
    /// Interaction logic for PatientAppointments.xaml
    /// </summary>
    public partial class PatientAppointmentsView
    {
        public PatientAppointmentsView()
        {
            InitializeComponent();
        }
    }
    public class PatientAppointmentsViewContextReference : DataContextReference
    {
        public new PatientAppointmentsViewContext DataContext
        {
            get { return (PatientAppointmentsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    public class PatientAppointmentsViewContext : IViewContext, IPatientInfoViewContext, ICommonViewFeatureHandler
    {
        private readonly IPatientAppointmentsViewService _patientInfoViewService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly Func<AppointmentViewModelWrapper> _createAppointmentViewModelWrapper;
        private readonly Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> _createPatientInsuranceAuthorizationPopupViewContext;
        private readonly Func<PatientReferralPopupViewContext> _createPatientReferralPopupViewContext;
        private readonly RecallsViewManager _recallsViewManger;

        private readonly ScheduleAppointmentViewManager _scheduleAppointmentViewManager;
        private readonly AppointmentSearchViewManager _appointmentSearchViewManager;
        private readonly CancelAppointmentViewManager _cancelAppointmentViewManager;
        private ObservableCollection<AppointmentViewModelWrapper> _patientAppointments;
        private readonly Func<PatientAppointmentsSettingsViewModel> _createPatientAppointmentsSettingsViewModel;
        private readonly MultiCalendarViewManager _createMultiCalendarViewManager;

        public PatientAppointmentsViewContext(IPatientAppointmentsViewService patientInfoViewService, IApplicationSettingsService applicationSettingsService,
                                        Func<AppointmentViewModelWrapper> createAppointmentViewModelWrapper, ICommonViewFeatureExchange exchange,
                                        Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> createPatientInsuranceAuthorizationPopupViewContext,
                                              Func<RecallsViewManager> createRecallsViewManger, Func<ScheduleAppointmentViewManager> createScheduleAppointmentViewManager,
                                              Func<AppointmentSearchViewManager> createAppointmentSearchViewManager, Func<CancelAppointmentViewManager> createCancelAppointmentViewManager,
                                        Func<PatientAppointmentsSettingsViewModel> createPatientAppointmentsSettingsViewModel, Func<MultiCalendarViewManager> createMultiCalendarViewManager,
            Func<PatientReferralPopupViewContext> createPatientReferralPopupViewContext)
        {
            _patientInfoViewService = patientInfoViewService;
            _applicationSettingsService = applicationSettingsService;
            _createAppointmentViewModelWrapper = createAppointmentViewModelWrapper;
            _createPatientInsuranceAuthorizationPopupViewContext = createPatientInsuranceAuthorizationPopupViewContext;
            _recallsViewManger = createRecallsViewManger();
            _scheduleAppointmentViewManager = createScheduleAppointmentViewManager();
            _appointmentSearchViewManager = createAppointmentSearchViewManager();
            _cancelAppointmentViewManager = createCancelAppointmentViewManager();
            _createPatientAppointmentsSettingsViewModel = createPatientAppointmentsSettingsViewModel;
            _createMultiCalendarViewManager = createMultiCalendarViewManager();
            _createPatientReferralPopupViewContext = createPatientReferralPopupViewContext;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);

            CreateAppointment = Command.Create(ExecuteCreateAppointment, () => PermissionId.ViewSchedule.PrincipalContextHasPermission());
            ViewRecalls = Command.Create(ExecuteViewRecalls);
            ViewCalendar = Command.Create(ExecuteViewCalendar);
            RescheduleAppointment = Command.Create<AppointmentViewModelWrapper>(ExecuteRescheduleAppointment);
            ScheduleCancelAppointment = Command.Create(ExecuteScheduleCancelAppointment);
            CancelAppointment = Command.Create<AppointmentViewModelWrapper>(ExecuteCancelAppointment);
            ShowCommentPopup = Command.Create<AppointmentViewModelWrapper>(ExecuteShowCommentPopup);
            EditComment = Command.Create<AppointmentViewModelWrapper>(ExecuteEditComment);
            DeleteComment = Command.Create<AppointmentViewModelWrapper>(ExecuteDeleteComment).Async(() => InteractionContext);
            SetSelectedAppointment = Command.Create<AppointmentViewModelWrapper>(ExecuteSetSelectedAppointment);
            OkAttachReferralRejection = Command.Create(ExecuteOkAttachReferralRejection);
            AppointmentIds = new HashSet<int>();

            ViewFeatureExchange = exchange;
            ViewFeatureExchange.Refresh = OnRefresh;
        }

        private void OnRefresh()
        {
            Load.Execute();
        }

        private void ExecuteViewCalendar()
        {
            var loadArgs = new MultiCalendarLoadArguments
                               {
                                   PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault(),
                               };

            MultiCalendarViewManager multiCalendarViewManager = _createMultiCalendarViewManager;
            multiCalendarViewManager.ShowMultiCalendar(loadArgs);

            // reload the appointments list            
            Load.Execute();
        }

        private void ExecuteOkAttachReferralRejection()
        {
            NoEncountersRemainingDataTrigger = false;
        }


        private void ExecuteViewRecalls()
        {
            var loadArgs = new RecallLoadArguments
                               {
                                   PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault()
                               };

            _recallsViewManger.ShowRecalls(loadArgs);
        }

        private void ExecuteSetSelectedAppointment(AppointmentViewModelWrapper obj)
        {
            SelectedAppointment = obj;
        }

        private void ExecuteScheduleCancelAppointment()
        {
            if (SelectedAppointment == null)
            {
                return;
            }

            var encounterStatus = (EncounterStatus)SelectedAppointment.PatientAppointment.EncounterStatus.Id;
            if (encounterStatus == EncounterStatus.Pending
                || encounterStatus == EncounterStatus.Discharged
                && (PermissionId.ScheduleAppointment.PrincipalContextHasPermission()))
            {
                // show the schedule appointment for the currently selected appointment.
                var loadArgs = new ScheduleAppointmentLoadArguments
                {
                    AppointmentCategoryId = SelectedAppointment.PatientAppointment.AppointmentType.CategoryId,
                    AppointmentId = SelectedAppointment.PatientAppointment.AppointmentId,
                    PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault()
                };

                _scheduleAppointmentViewManager.ShowScheduleAppointment(loadArgs);

                // reload the appointments list            
                Load.Execute();
            }
            else if (encounterStatus.IsCancelledStatus() && PermissionId.CancelAppointment.EnsurePermission())
            {
                // show the cancelled appointment peripheral screen
                ExecuteCancelAppointment(SelectedAppointment);
            }
        }

        private void ExecuteCancelAppointment(AppointmentViewModelWrapper appointmentContext)
        {
            SelectedAppointment = appointmentContext;

            var loadArguments = new CancelAppointmentLoadArguments { ListOfAppointmentId = new List<int> { appointmentContext.PatientAppointment.AppointmentId } };

            _cancelAppointmentViewManager.ShowCancelAppointment(loadArguments);

            AppointmentIds.Remove(appointmentContext.PatientAppointment.AppointmentId);
            // reload the appointments list            
            Load.Execute();
        }

        private void ExecuteRescheduleAppointment(AppointmentViewModelWrapper appointmentContext)
        {
            SelectedAppointment = appointmentContext;

            var encounterStatus = (EncounterStatus)appointmentContext.PatientAppointment.EncounterStatus.Id;
            bool canReschedule = encounterStatus.EnsureCanReschedule();
            if (!canReschedule) return;

            // load appointment search with parameters pre-defined for the selected appointment
            var args = new AppointmentSearchLoadArguments
                           {
                               PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault(),
                               AppointmentTypeId = appointmentContext.PatientAppointment.AppointmentType.Id,
                               StartDate = appointmentContext.PatientAppointment.DateTime,
                               LocationId = appointmentContext.PatientAppointment.AppointmentLocation.Id,
                               ResourceId = appointmentContext.PatientAppointment.ResourceId,
                               AppointmentId = appointmentContext.PatientAppointment.AppointmentId
                           };
            var returnArgs = _appointmentSearchViewManager.ShowAppointmentSearch(args);
            if (returnArgs.HasRescheduledAppointmentsSuccessfully == true)
            {
                foreach (var apptId in returnArgs.AppointmentIds)
                {
                    AppointmentIds.Add(apptId);
                }
                // reload the appointments list            
                Load.Execute();
            }
        }

        private void ExecuteCreateAppointment()
        {
            // load up appointment search window to create a new appointment for this patient            
            var loadArguments = new AppointmentSearchLoadArguments { PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault(), StartDate = DateTime.Now.ToClientTime() };
            var returnArgs = _appointmentSearchViewManager.ShowAppointmentSearch(loadArguments);
            foreach (var apptId in returnArgs.AppointmentIds)
            {
                AppointmentIds.Add(apptId);
            }
            // reload the appointments list            
            Load.Execute();
        }

        private void ExecuteLoad()
        {
            if (!PermissionId.ViewPatientAppointmentSummary.PrincipalContextHasPermission()) return;

            var settingsContainer = _applicationSettingsService.GetSetting<PatientAppointmentsSettingsViewModel>(ApplicationSetting.PatientAppointments);

            if (settingsContainer != null)
            {
                Settings = _createPatientAppointmentsSettingsViewModel();
                Settings.IsPendingChecked = settingsContainer.Value.IsPendingChecked;
                Settings.IsArrivedChecked = settingsContainer.Value.IsArrivedChecked;
                Settings.IsDischargedChecked = settingsContainer.Value.IsDischargedChecked;
                Settings.IsCancelledChecked = settingsContainer.Value.IsCancelledChecked;
                Settings.Id = settingsContainer.Id;
            }
            else
            {
                Settings = _createPatientAppointmentsSettingsViewModel();
                Settings.IsArrivedChecked = true;
                Settings.IsPendingChecked = true;
            }

            var loadInformation = _patientInfoViewService.LoadInformation(PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault());

            AttachAppointmentData(loadInformation.PatientAppointments, loadInformation.AppointmentChanges.ToList(), loadInformation.PatientInsuranceAuthorizations);

            var appointments = new ExtendedObservableCollection<AppointmentViewModelWrapper>();
            foreach (var patientAppointment in loadInformation.PatientAppointments)
            {
                var appointment = _createAppointmentViewModelWrapper();
                appointment.PatientAppointment = patientAppointment;
                appointment.AuthorizationPopupViewContext = _createPatientInsuranceAuthorizationPopupViewContext();

                var loadArguments = new PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupLoadArguments();
                loadArguments.EncounterId = patientAppointment.EncounterId;
                if (patientAppointment.AttachedPatientInsuranceAuthorization != null)
                {
                    loadArguments.AttachedPatientInsuranceAuthorization = patientAppointment.AttachedPatientInsuranceAuthorization;
                }
                appointment.AuthorizationPopupViewContext.DirectlyLoadArguments(loadArguments);

                if (loadInformation.Referrals != null)
                {
                    appointment.Referral = _createPatientReferralPopupViewContext();
                    appointment.Referral.AppointmentId = appointment.PatientAppointment.AppointmentId;
                    appointment.Referral.AppointmentStartDateTime = appointment.PatientAppointment.DateTime;
                    appointment.Referral.PatientId = PatientInfoViewModel.PatientId.EnsureNotDefault().GetValueOrDefault();
                    appointment.Referral.SaveOnNewReferrals = true;
                    appointment.Referral.ListOfAttachedReferral = new ExtendedObservableCollection<ReferralViewModel>(loadInformation.Referrals.Where(x => x.EncounterIds.Contains(appointment.PatientAppointment.AppointmentId)));
                    appointment.Referral.ExistingReferralInformation = new ExtendedObservableCollection<ReferralViewModel>();
                    appointment.Referral.ValidateReferralAndIncludeInExisting(loadInformation.Referrals);
                }

                appointments.Add(appointment);
            }
            PatientAppointments = appointments.ToExtendedObservableCollection();
        }

        public void ExecuteShowCommentPopup(AppointmentViewModelWrapper appointmentContext)
        {
            PromptCommentModal(appointmentContext.PatientAppointment);
        }

        private void ExecuteDeleteComment(AppointmentViewModelWrapper appointmentContext)
        {
            var appointment = appointmentContext.PatientAppointment;

            _patientInfoViewService.DeleteAppointmentComment(appointment.AppointmentId);
            appointment.AppointmentChanges = _patientInfoViewService.LoadAppointmentChanges(new[] { appointment.AppointmentId }).ToExtendedObservableCollection();
            appointment.Comments = string.Empty;
        }

        private void ExecuteEditComment(AppointmentViewModelWrapper appointmentContext)
        {
            PromptCommentModal(appointmentContext.PatientAppointment, appointmentContext.PatientAppointment.Comments);
        }

        private void PromptCommentModal(PatientAppointmentViewModel appointmentContext, string currentCommentText = null)
        {
            var promptResult = InteractionManager.Current.Prompt("Enter the appointment comment for this patient appointment below:", "Comment", currentCommentText);

            if (promptResult.DialogResult.HasValue && promptResult.DialogResult.Value)
            {
                Command.Create(() =>
                                   {
                                       _patientInfoViewService.SaveAppointmentComment(appointmentContext.AppointmentId, promptResult.Input);
                                       appointmentContext.Comments = promptResult.Input ?? string.Empty;
                                       appointmentContext.AppointmentChanges = _patientInfoViewService.LoadAppointmentChanges(new[] { appointmentContext.AppointmentId }).ToExtendedObservableCollection();

                                   }).Async(() => InteractionContext).Execute();
            }
        }

        private static void AttachAppointmentData(IEnumerable<PatientAppointmentViewModel> patientAppointments,
            IEnumerable<AppointmentChangeViewModel> appointmentChanges = null,
            IEnumerable<PatientInsuranceAuthorizationViewModel> patientInsuranceAuthorizations = null)
        {
            foreach (var a in patientAppointments)
            {
                var appointment = a;
                if (appointmentChanges != null)
                {
                    appointment.AppointmentChanges = appointmentChanges.Where(x => x.AppointmentId == appointment.AppointmentId).OrderByDescending(x => x.DateTime).ToExtendedObservableCollection();
                }

                if (patientInsuranceAuthorizations != null)
                {
                    appointment.AttachedPatientInsuranceAuthorization = patientInsuranceAuthorizations.OrderBy(x => x.Id).LastOrDefault(x => x.EncounterId == appointment.AppointmentId);
                }
            }
        }

        public void SetPatientInfo(PatientInfoViewModel patientInfoViewModel, ICommand reloadPatientInfoCommand = null)
        {
            PatientInfoViewModel = patientInfoViewModel;
        }

        /// <summary>
        /// Command to load the UI
        /// </summary>
        public ICommand Load { get; set; }

        /// <summary>
        /// Command to load the appointment search window
        /// </summary>
        public ICommand CreateAppointment { get; protected set; }

        /// <summary>
        /// Command to load the recalls window for this patient
        /// </summary>
        public ICommand ViewRecalls { get; protected set; }

        /// <summary>
        /// Command to load the Calendar window 
        /// </summary>
        public ICommand ViewCalendar { get; protected set; }

        /// <summary>
        /// Command to show the appointment search screen so that the user can reschedule the appointment
        /// </summary>
        public ICommand RescheduleAppointment { get; protected set; }

        /// <summary>
        /// Command to show the cancel appointment peripheral screen.
        /// </summary>
        public ICommand CancelAppointment { get; protected set; }

        /// <summary>
        /// Command to show the schedule appointment peripheral screen.
        /// </summary>
        public ICommand ScheduleCancelAppointment { get; protected set; }

        /// <summary>
        /// Command to show a pop up for entering comments
        /// </summary>
        public ICommand ShowCommentPopup { get; protected set; }

        /// <summary>
        /// Command to show a pop up for entering comments
        /// </summary>
        public ICommand EditComment { get; protected set; }

        /// <summary>
        /// Command to delete current comment
        /// </summary>
        public ICommand DeleteComment { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public ICommand SetSelectedAppointment { get; protected set; }


        /// <summary>
        /// Command when user hits "Ok" upon rejection message when trying to attach a referral.
        /// </summary>
        public ICommand OkAttachReferralRejection { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual PatientInfoViewModel PatientInfoViewModel { get; set; }

        public PatientInfoClosingReturnArguments OnClosing()
        {
            // Settings is null if this view has never loaded
            if (Settings == null) return null;

            var settingsContainer = new ApplicationSettingContainer<PatientAppointmentsSettingsViewModel>
            {
                Id = Settings.Id,
                MachineName = ApplicationContext.Current.ComputerName,
                Name = ApplicationSetting.PatientAppointments,
                Value = Settings
            };
            System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => _applicationSettingsService.SetSetting(settingsContainer));


            return null;
        }

        public HashSet<int> AppointmentIds { get; set; }

        /// <summary>
        /// Contains a list of patient appointments and their details
        /// </summary>
        [DispatcherThread, DependsOn("Settings")]
        public virtual ObservableCollection<AppointmentViewModelWrapper> PatientAppointments
        {
            get
            {
                var returnedAppointments = new ExtendedObservableCollection<AppointmentViewModelWrapper>();

                if (_patientAppointments == null) return returnedAppointments;


                if (Settings.IsPendingChecked)
                {
                    //var pending = _patientAppointments.Select(a => a.PatientAppointment).Where(x => x.EncounterStatus.IsPendingStatus());
                    returnedAppointments.AddRangeWithSinglePropertyChangedNotification(_patientAppointments.Where(a => ((EncounterStatus)a.PatientAppointment.EncounterStatus.Id).IsPendingStatus()));
                }

                if (Settings.IsArrivedChecked)
                {
                    //var arrived = _patientAppointments.Select(a => a.PatientAppointment).Where(x => x.EncounterStatus.IsArrivedStatus());
                    returnedAppointments.AddRangeWithSinglePropertyChangedNotification(_patientAppointments.Where(a => ((EncounterStatus)a.PatientAppointment.EncounterStatus.Id).IsArrivedStatus()));
                }

                if (Settings.IsDischargedChecked)
                {
                    //var discharged = _patientAppointments.Select(a => a.PatientAppointment).Where(x => x.EncounterStatus.IsDischargedStatus());
                    returnedAppointments.AddRangeWithSinglePropertyChangedNotification(_patientAppointments.Where(a => ((EncounterStatus)a.PatientAppointment.EncounterStatus.Id).IsDischargedStatus()));
                }

                if (Settings.IsCancelledChecked)
                {
                    //var cancelled = _patientAppointments.Select(a => a.PatientAppointment).Where(x => x.EncounterStatus.IsCancelledStatus());
                    returnedAppointments.AddRangeWithSinglePropertyChangedNotification(_patientAppointments.Where(a => ((EncounterStatus)a.PatientAppointment.EncounterStatus.Id).IsCancelledStatus()));
                }

                return returnedAppointments;
            }
            set { _patientAppointments = value; }
        }


        [DispatcherThread]
        public virtual PatientAppointmentsSettingsViewModel Settings { get; set; }

        [DispatcherThread]
        public virtual AppointmentViewModelWrapper SelectedAppointment
        {
            get;
            set;
        }

        [DispatcherThread]
        public virtual ObservableCollection<ReferralViewModel> SelectedAppointmentActiveReferrals
        {
            get { return SelectedAppointment.Referral.ListOfAttachedReferral; }
        }


        [DispatcherThread]
        public virtual string NoCommentToolTip
        {
            get
            {
                const string message = "no comment available";
                return PermissionId.ScheduleAppointment.PrincipalContextHasPermission() ? (message + ", click to add a comment") : message;
            }
        }

        /// <summary>
        /// Set this flag to pop up a command informing the user that they cannot select the referral because it has no more encounters left.
        /// </summary>
        [DispatcherThread]
        public virtual bool NoEncountersRemainingDataTrigger
        {
            get;
            set;
        }

        public virtual ICommonViewFeatureExchange ViewFeatureExchange { get; protected set; }
    }


}
