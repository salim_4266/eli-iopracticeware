﻿using System;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientWizard;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.NonClinicalPatient;
using IO.Practiceware.Presentation.Views.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Presentation.Views.Wizard;
using IO.Practiceware.Presentation.ViewsModels.NonClinicalPatient;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientWizard
{
    /// <summary>
    /// Wizard to search and select, or create a "Non-clinical patient contact".  A "Non-clinical patient contact" 
    /// </summary>
    public class PatientWizardManager
    {
        private readonly Func<PatientRelationshipViewContext> _createRelationshipViewContext;
        private readonly Func<MiniPatientSearchViewContext> _createMiniPatientSearchViewContext;
        private readonly Func<NonClinicalPatientDetailsViewContext> _createNonClinicalPatientDetailsViewContext;
        private readonly Func<PatientDemographicsViewContext> _createPatientDemographicsViewContext;
        private readonly INavigableInteractionManager _navigableInteractionManager;
        private readonly Func<IInteractionContext> _createInteractionContext;



        public PatientWizardManager(
            Func<PatientRelationshipViewContext> createRelationshipViewContext,
            Func<MiniPatientSearchViewContext> createMiniPatientSearchViewContext,
            Func<NonClinicalPatientDetailsViewContext> createNonClinicalPatientDetailsViewContext,
            Func<PatientDemographicsViewContext> createPatientDemographicsViewContext,
            INavigableInteractionManager navigableInteractionManager,
            Func<IInteractionContext> createInteractionContext)
        {
            _createRelationshipViewContext = createRelationshipViewContext;
            _createMiniPatientSearchViewContext = createMiniPatientSearchViewContext;
            _createNonClinicalPatientDetailsViewContext = createNonClinicalPatientDetailsViewContext;
            _createPatientDemographicsViewContext = createPatientDemographicsViewContext;
            _navigableInteractionManager = navigableInteractionManager;
            _createInteractionContext = createInteractionContext;
        }

        /// <summary>
        /// Displays a wizard to search for, create/edit, and then select a patient.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="patientRelationshipType"></param>
        /// <returns></returns>
        public PatientWizardReturnArguments Show(int? patientId, PatientRelationshipType patientRelationshipType)
        {
            var navigationViewModel = new PatientWizardNavigationViewModel();
            navigationViewModel.PatientRelationshipType = patientRelationshipType;
            navigationViewModel.PatientId = patientId;

            var interactionContext = _createInteractionContext();

            var relationshipViewContext = _createRelationshipViewContext();
            var miniPatientSearchViewContext = _createMiniPatientSearchViewContext();
            var nonClinicalPatientDetailsViewContext = _createNonClinicalPatientDetailsViewContext();
            var patientDemographicsViewContext = _createPatientDemographicsViewContext();

            patientDemographicsViewContext.Wizard = new PatientDemographicsWizardContext(_navigableInteractionManager, interactionContext, patientDemographicsViewContext, navigationViewModel);
            nonClinicalPatientDetailsViewContext.Wizard = new NonClinicalPatientDetailsWizardContext(_navigableInteractionManager, interactionContext, nonClinicalPatientDetailsViewContext, navigationViewModel);
            miniPatientSearchViewContext.Wizard = new MiniPatientSearchWizardContext(_navigableInteractionManager, interactionContext, miniPatientSearchViewContext, navigationViewModel, nonClinicalPatientDetailsViewContext, patientDemographicsViewContext);
            relationshipViewContext.Wizard = new PatientRelationshipWizardContext(_navigableInteractionManager, interactionContext, relationshipViewContext, navigationViewModel, miniPatientSearchViewContext);

            var relationshipView = new PatientRelationshipView { AutoCreateDataContext = false };
            relationshipView.DataContext = relationshipViewContext;

            var navigableInteractionArgs = new NavigableInteractionArguments
            {
                Content = relationshipView,
                Direction = Direction.Forward,
                InteractionContext = interactionContext
            };

            _navigableInteractionManager.Show(navigableInteractionArgs);

            PatientWizardReturnArguments returnArgs = null;

            if (navigationViewModel.SelectedtId.HasValue)
            {
                returnArgs = new PatientWizardReturnArguments();
                returnArgs.SelectedId = navigationViewModel.SelectedtId;
                returnArgs.RelationshipDescription = navigationViewModel.RelationshipDescription;
                returnArgs.PolicyholderRelationshipTypeId = navigationViewModel.PolicyholderRelationshipTypeId;
            }

            return returnArgs;
        }
    }

    /// <summary>
    /// The patient wizard allows a user to search for or create/edit, and then select, a patient. "Patient" here refers 
    /// to either "ClinicalPatient", "PatientContact" or a "Policyholder", since they are all technicals "Patients"    
    /// </summary>
    public class PatientWizardReturnArguments
    {
        /// <summary>
        /// The id of the patient selected from the wizard
        /// </summary>
        public int? SelectedId { get; set; }

        /// <summary>
        /// User description of the relationship between the patient and the contact. 
        /// </summary>
        public string RelationshipDescription { get; set; }

        /// <summary>
        /// The type of relationship chosen when trying to add a policy holder. 
        /// </summary>
        public int? PolicyholderRelationshipTypeId { get; set; }
    }

    /// <summary>
    /// A container class that provides custom logic for the PatientRelationshipView as it pertains to the wizard.
    /// I.E, it contains the logic for passing data onto other screens in the wizard, what to do on the Next, Back, Cancel commands. etc.
    /// </summary>
    public class PatientRelationshipWizardContext : WizardDataContext<PatientWizardNavigationViewModel, PatientRelationshipViewContext>
    {
        private readonly MiniPatientSearchViewContext _miniPatientSearchViewContext;

        public PatientRelationshipWizardContext(INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext interactionContext,
                                        PatientRelationshipViewContext patientRelationshipViewContext,
                                        PatientWizardNavigationViewModel viewModel,
                                        MiniPatientSearchViewContext miniPatientSearchViewContext
                                        )
            : base(navigableInteractionManager, interactionContext, patientRelationshipViewContext, viewModel)
        {
            _miniPatientSearchViewContext = miniPatientSearchViewContext;
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientTypeName;
            ParentViewContext.ShowRelationshipDescription = !NavigationViewModel.IsPolicyholderRelationship;
            ParentViewContext.ShowListOfRelationshipTypes = NavigationViewModel.IsPolicyholderRelationship;
        }

        protected override bool CanExecuteBack()
        {
            return false;
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.RelationshipDescription.IsNotNullOrEmpty();
        }

        protected override void ExecuteNext()
        {
            NavigationViewModel.RelationshipDescription = ParentViewContext.RelationshipDescription;

            var miniSearchView = new MiniPatientSearchView { AutoCreateDataContext = false };
            miniSearchView.DataContext = _miniPatientSearchViewContext;

            var navigableInteractionArgs = new NavigableInteractionArguments
            {
                Content = miniSearchView,
                Direction = Direction.Forward,
            };

            NavigableInteractionManager.Show(navigableInteractionArgs);
        }
    }

    public class MiniPatientSearchWizardContext : WizardDataContext<PatientWizardNavigationViewModel, MiniPatientSearchViewContext>
    {
        private readonly NonClinicalPatientDetailsViewContext _nonClinicalPatientDetailsViewContext;
        private readonly PatientDemographicsViewContext _patientDemographicsViewContext;

        public MiniPatientSearchWizardContext(
                                        INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext interactionContext,
                                        MiniPatientSearchViewContext miniPatientSearchViewContext,
                                        PatientWizardNavigationViewModel viewModel,
                                        NonClinicalPatientDetailsViewContext nonClinicalPatientDetailsViewContext,
                                        PatientDemographicsViewContext patientDemographicsViewContext)
            : base(navigableInteractionManager, interactionContext, miniPatientSearchViewContext, viewModel)
        {
            _nonClinicalPatientDetailsViewContext = nonClinicalPatientDetailsViewContext;
            _patientDemographicsViewContext = patientDemographicsViewContext;
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientTypeName;
            Utility = Command.Create(ExecuteCreateEditPatientContactDetails, () => CanCreateOrEdit);
        }

        protected override void ExecuteCancelWizard()
        {
            NavigationViewModel.SelectedtId = null;
            base.ExecuteCancelWizard();
        }

        private bool CanCreateOrEdit
        {
            get
            {
                PermissionId? createPermissionId = null;
                PermissionId? editPermissionId = null;

                switch (NavigationViewModel.PatientRelationshipType)
                {
                    case PatientRelationshipType.Contact:
                        break;
                    case PatientRelationshipType.Policyholder:
                        createPermissionId = PermissionId.CreatePolicyholder;
                        editPermissionId = PermissionId.EditPolicyholder;
                        break;
                }

                bool hasSelectedAnItem = ParentViewContext.SelectedSearchResult != null;

                bool canEdit = hasSelectedAnItem && (editPermissionId == null || editPermissionId.Value.PrincipalContextHasPermission());

                bool canCreate = !hasSelectedAnItem && (createPermissionId == null || createPermissionId.Value.PrincipalContextHasPermission());

                return canEdit || canCreate;
            }
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.SearchResults.IsNotNullOrEmpty() && ParentViewContext.SelectedSearchResult != null;
        }

        protected override void ExecuteNext()
        {
            NavigationViewModel.SelectedtId = ParentViewContext.SelectedSearchResult.Id;
            WizardComplete.Execute();
        }

        public void ExecuteCreateEditPatientContactDetails()
        {
            NavigationViewModel.SelectedtId = ParentViewContext.SelectedSearchResult != null
               ? (int?)ParentViewContext.SelectedSearchResult.Id
               : null;


            View viewToOpen;

            if (NavigationViewModel.SelectedtId.HasValue && ParentViewContext.SelectedSearchResult != null && ParentViewContext.SelectedSearchResult.IsClinical)
            {
                if (NavigationViewModel.PatientRelationshipType == PatientRelationshipType.Contact && !PermissionId.EditContact.EnsurePermission()) return;

                _patientDemographicsViewContext.SetPatientInfo(new ViewModels.PatientInfo.PatientInfoViewModel { PatientId = NavigationViewModel.SelectedtId.Value });
                viewToOpen = new PatientDemographicsView { AutoCreateDataContext = false };
                var pdView = (PatientDemographicsView)viewToOpen;
                pdView.Load = _patientDemographicsViewContext.Load; // we have to manually set this up because the Patient Info parent view automatically calls the Demographics Load                 
                viewToOpen.DataContext = _patientDemographicsViewContext;
            }
            else
            {
                _nonClinicalPatientDetailsViewContext.NonClinicalPatientId = NavigationViewModel.SelectedtId;
                _nonClinicalPatientDetailsViewContext.DefaultAddressPatientId = NavigationViewModel.PatientId;
                viewToOpen = new NonClinicalPatientDetailsView { AutoCreateDataContext = false };
                viewToOpen.DataContext = _nonClinicalPatientDetailsViewContext;
            }

            var navigableInteractionArgs = new NavigableInteractionArguments
            {
                Content = viewToOpen,
                Direction = Direction.Forward,
            };

            NavigableInteractionManager.Show(navigableInteractionArgs);
        }
    }

    public class NonClinicalPatientDetailsWizardContext : WizardDataContext<PatientWizardNavigationViewModel, NonClinicalPatientDetailsViewContext>
    {
        public NonClinicalPatientDetailsWizardContext(
                                        INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext interactionContext,
                                        NonClinicalPatientDetailsViewContext nonClinicalPatientDetailsViewContext,
                                        PatientWizardNavigationViewModel viewModel)
            : base(navigableInteractionManager, interactionContext, nonClinicalPatientDetailsViewContext, viewModel)
        {
            ParentViewContext.ContactTypeName = NavigationViewModel.PatientTypeName;
        }

        protected override void ExecuteCancelWizard()
        {
            NavigationViewModel.SelectedtId = null;
            base.ExecuteCancelWizard();
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.Save.CanExecute();
        }

        protected override void ExecuteNext()
        {
            ParentViewContext.SaveNonClinicalPatient();

            NavigationViewModel.SelectedtId = ParentViewContext.NonClinicalPatientId;

            WizardComplete.Execute();
        }
    }

    public class PatientDemographicsWizardContext : WizardDataContext<PatientWizardNavigationViewModel, PatientDemographicsViewContext>
    {
        public PatientDemographicsWizardContext(INavigableInteractionManager navigableInteractionManager,
                                        IInteractionContext interactionContext,
                                        PatientDemographicsViewContext patientDemographicsViewContext,
                                        PatientWizardNavigationViewModel viewModel)
            : base(navigableInteractionManager, interactionContext, patientDemographicsViewContext, viewModel)
        {
            ParentViewContext.Load = Command.Create(ExecuteLoad);
        }

        private void ExecuteLoad()
        {
            ParentViewContext.ExecuteLoad();
            ParentViewContext.PageMode = PatientDemographicsPageMode.EditExistingPatient;
        }

        protected override void ExecuteCancelWizard()
        {
            NavigationViewModel.SelectedtId = null;
            base.ExecuteCancelWizard();
        }

        protected override bool CanExecuteNext()
        {
            return ParentViewContext.Save.CanExecute();
        }

        protected override void ExecuteNext()
        {
            if (ParentViewContext.PatientData == null || ParentViewContext.PatientData.Id <= 0) return;

            var id = ParentViewContext.SavePatient();
            if (!id.HasValue) return;

            if (id.Value != ParentViewContext.PatientData.Id)
            {
                throw new InvalidOperationException("The original selected patient does not match the patient saved");
            }

            NavigationViewModel.SelectedtId = ParentViewContext.PatientData.Id;

            WizardComplete.Execute();
        }
    }

}
