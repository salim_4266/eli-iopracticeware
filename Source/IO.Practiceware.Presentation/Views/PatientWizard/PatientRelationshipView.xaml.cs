﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Wizard;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientWizard
{
    /// <summary>
    /// Interaction logic for RelationshipView.xaml
    /// </summary>
    public partial class PatientRelationshipView
    {
        public PatientRelationshipView()
        {
            InitializeComponent();
        }
    }

    public class PatientRelationshipViewContext : IViewContext, IHasWizardDataContext
    {

        public PatientRelationshipViewContext()
        {
            SelectedRelationshipChanged = Command.Create(ExecuteSelectedRelationshipChanged);            
            RelationshipTypes = NamedViewModelExtensions.ToNamedViewModel<PolicyHolderRelationshipType>().ToObservableCollection();            
        }   

        private void ExecuteSelectedRelationshipChanged()
        {
            if (SelectedRelationshipTypeId != null)
            {
                if (SelectedRelationshipTypeId == (int)PolicyHolderRelationshipType.Self && !PermissionId.AddPatientInsuranceForSelf.EnsurePermission())
                {
                    SelectedRelationshipTypeId = null;
                }
                else if (SelectedRelationshipTypeId != (int)PolicyHolderRelationshipType.Self && !PermissionId.AddPatientInsuranceForOther.EnsurePermission())
                {
                    SelectedRelationshipTypeId = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public virtual IInteractionContext InteractionContext { get; set; }


        /// <summary>
        /// Gets or sets the policyholder relationship types
        /// </summary>
        public virtual ObservableCollection<NamedViewModel> RelationshipTypes { get; set; }

        public virtual int? SelectedRelationshipTypeId { get; set; }

        public virtual string RelationshipDescription { get; set; }

        public virtual bool ShowListOfRelationshipTypes { get; set; }

        public virtual bool ShowRelationshipDescription { get; set; }

        /// <summary>
        /// Gets the command executed on relationship change
        /// </summary>
        public virtual ICommand SelectedRelationshipChanged { get; protected set; }

        /// <summary>
        /// Command to load the view
        /// </summary>
        public virtual ICommand Load { get; protected set; }

        public virtual ICommand Cancel { get; protected set; }

        [DispatcherThread]
        public virtual string ContactTypeName { get; set; }

        [DispatcherThread]
        public virtual IWizardDataContext Wizard { get; set; }     
    }
}