﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.AppointmentSearch;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.AppointmentSearch
{
    [SupportsDispatcherThread]
    [SupportsNotifyPropertyChanged(true, true)]
    public class AppointmentHistoryHelper
    {
        private readonly Func<EncounterStatusFilterTypeViewModel> _createEncounterStatusFilterTypeViewModel;
        private readonly Func<int> _getPatientId;
        private readonly Action _refreshView;
        private readonly Func<HashSet<int>> _getAppointmentIds;
        private readonly Func<AppointmentSearchViewManager> _getAppointmentSearchViewManager;

        public AppointmentHistoryHelper(ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> appointmentHistory,
                                        Func<int> getPatientId,
                                        Action refreshView,
                                        Func<HashSet<int>> getAppointmentIds,
                                        Func<AppointmentSearchViewManager> getAppointmentSearchViewManager,
                                        Func<EncounterStatusFilterTypeViewModel> createEncounterStatusFilterTypeViewModel)
        {
            AppointmentHistory = appointmentHistory;
            _createEncounterStatusFilterTypeViewModel = createEncounterStatusFilterTypeViewModel;
            _getPatientId = getPatientId;
            _refreshView = refreshView;
            _getAppointmentIds = getAppointmentIds;
            _getAppointmentSearchViewManager = getAppointmentSearchViewManager;

            RescheduleAppointment = Command.Create<AppointmentSearchAppointmentHistoryViewModel>(ExecuteRescheduleAppointment);

            EncounterStatusFilterTypes = new ExtendedObservableCollection<EncounterStatusFilterTypeViewModel>();
            foreach(var enumValue in Enum.GetValues(typeof (EncounterStatusFilterType)).Cast<EncounterStatusFilterType>())
            {
                var viewModel = _createEncounterStatusFilterTypeViewModel();
                viewModel.FilterType = enumValue;
                
                if(viewModel.FilterType == EncounterStatusFilterType.Arrived || viewModel.FilterType == EncounterStatusFilterType.Pending)
                {
                    viewModel.IsSelected = true;
                }
                EncounterStatusFilterTypes.Add(viewModel);
            }
        }

        private void ExecuteRescheduleAppointment(AppointmentSearchAppointmentHistoryViewModel appointment)
        {
            bool canReschedule = ((EncounterStatus) appointment.AppointmentStatus.Id).EnsureCanReschedule();
            if (!canReschedule) return;

            var args = new AppointmentSearchLoadArguments
                           {
                               PatientId = _getPatientId(),
                               AppointmentTypeId = appointment.AppointmentType.Id,
                               StartDate = appointment.AppointmentDateTime,
                               LocationId = appointment.Location.Id,
                               ResourceId = appointment.Resource.Id,
                               AppointmentId = appointment.Id
                           };
            var returnArgs = _getAppointmentSearchViewManager().ShowAppointmentSearch(args);

            foreach (var apptId in returnArgs.AppointmentIds)
            {
                _getAppointmentIds().Add(apptId);
            }

            _refreshView();
        }

        private bool AppointmentIsViewable(AppointmentSearchAppointmentHistoryViewModel appointment)
        {
            var encounterStatus = (EncounterStatus) appointment.AppointmentStatus.Id;
            return EncounterStatusFilterTypes.Where(ft => ft.IsSelected).Any(ft => ft.IsViewable(encounterStatus));
        }

        /// <summary>
        /// The patient's appointment history
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> AppointmentHistory { get; set; }


        /// <summary>
        /// The viewable appointment history
        /// </summary>
        [DependsOn("EncounterStatusFilterTypes")]
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> ViewablePatientAppointmentHistory
        {
            get { return AppointmentHistory.Where(AppointmentIsViewable).ToExtendedObservableCollection(); }
        }

        [DispatcherThread]
        public virtual ObservableCollection<EncounterStatusFilterTypeViewModel> EncounterStatusFilterTypes { get; protected set; }

        /// <summary>
        /// Command to toggle the selection of an encounter status filter
        /// </summary>
        public ICommand ToggleEncounterStatusFilter { get; protected set; }

        /// <summary>
        /// Command to begin rescheduling an appointment
        /// </summary>
        public ICommand RescheduleAppointment { get; protected set; }
    }

    public static class EncounterStatusFilterTypeExtensions
    {
        public static bool IsViewable(this EncounterStatusFilterTypeViewModel viewModel, EncounterStatus encounterStatus)
        {
            switch (viewModel.FilterType)
            {
                case EncounterStatusFilterType.Arrived:
                    return encounterStatus.IsArrivedStatus();
                case EncounterStatusFilterType.Cancelled:
                    return encounterStatus.IsCancelledStatus();
                case EncounterStatusFilterType.Discharged:
                    return encounterStatus.IsDischargedStatus();
                case EncounterStatusFilterType.Pending:
                    return encounterStatus.IsPendingStatus();
                default:
                    throw new Exception("No filter type defined for {0}".FormatWith(viewModel.FilterType));
            }
        }
    }
}
