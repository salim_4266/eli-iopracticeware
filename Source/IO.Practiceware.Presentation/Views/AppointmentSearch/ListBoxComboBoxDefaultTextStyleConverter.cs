﻿using Soaf.Presentation.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.AppointmentSearch
{
    public class ListBoxComboBoxDefaultTextStyleBehavior : Behavior<ListBoxComboBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += SelectionChangedEventHandler;
        }

        private void SelectionChangedEventHandler(object sender, SelectionChangedEventArgs e)
        {
            AssociatedObject.DefaultTextFontStyle = FontStyles.Normal;
        }
    }
}
