﻿using IO.Practiceware.Application;
using IO.Practiceware.Presentation.ViewModels.AppointmentSearch;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.MultiCalendar;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.Recalls;
using IO.Practiceware.Presentation.Views.RescheduleAppointment;
using IO.Practiceware.Presentation.Views.ScheduleAppointment;
using IO.Practiceware.Presentation.ViewServices.AppointmentSearch;
using IO.Practiceware.Presentation.ViewServices.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.AppointmentSearch
{
    /// <summary>
    ///   Interaction logic for AppointmentSearch.xaml
    /// </summary>
    public partial class AppointmentSearchView
    {
        public AppointmentSearchView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the AppointmentSearchView
    /// </summary>
    public class AppointmentSearchViewContext : IViewContext
    {
        private readonly IAppointmentSearchViewService _appointmentSearchViewService;
        private readonly Func<IAppointmentHistoryHelperFactory> _createAppointmentHistoryHelperFactory;
        private readonly Func<AppointmentSearchDateTimeFilterViewModel> _createAppointmentSearchDateTimeFilterViewModel;
        private readonly Func<MultiCalendarViewManager> _createMultiCalendarViewManager;
        private readonly Func<PatientInfoViewManager> _createPatientInfoViewManager;
        private readonly Func<RecallsViewManager> _createRecallsViewManager;
        private readonly Func<RescheduleAppointmentViewManager> _createRescheduleAppointmentViewManager;
        private readonly Func<ScheduleAppointmentViewManager> _createScheduleAppointmentViewManager;
        private readonly IPostOperativePeriodService _postOperativePeriodService;

        public AppointmentSearchViewContext(IAppointmentSearchViewService appointmentSearchViewService,
                                            IPostOperativePeriodService postOperativePeriodService,
                                            Func<TimeSelectionType, TimeSelectionViewModel> createTimeSelection,
                                            Func<MultiCalendarViewManager> createMultiCalendarViewManager,
                                            Func<ScheduleAppointmentViewManager> createScheduleAppointmentViewManager,
                                            Func<RescheduleAppointmentViewManager> createRescheduleAppointmentViewManager,
                                            Func<RecallsViewManager> createRecallsViewManager,
                                            Func<AppointmentSearchDateTimeFilterViewModel> createAppointmentSearchDateTimeFilterViewModel,
                                            Func<PatientInfoViewManager> createPatientInfoViewManager,
                                            Func<IAppointmentHistoryHelperFactory> createAppointmentHistoryHelperFactory)
        {
            _appointmentSearchViewService = appointmentSearchViewService;
            _postOperativePeriodService = postOperativePeriodService;
            _createMultiCalendarViewManager = createMultiCalendarViewManager;
            _createScheduleAppointmentViewManager = createScheduleAppointmentViewManager;
            _createRescheduleAppointmentViewManager = createRescheduleAppointmentViewManager;
            _createRecallsViewManager = createRecallsViewManager;
            _createAppointmentSearchDateTimeFilterViewModel = createAppointmentSearchDateTimeFilterViewModel;
            _createPatientInfoViewManager = createPatientInfoViewManager;
            _createAppointmentHistoryHelperFactory = createAppointmentHistoryHelperFactory;

            Close = Command.Create(ExecuteClose);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Find = Command.Create(ExecuteFind, CanFind).Async(() => InteractionContext, false, null, true);

            ScheduleAppointment = Command.Create(ExecuteScheduleAppointment, () => SelectedSearchResult != null);
            ShowMultiCalendar = Command.Create<AppointmentSearchResultsViewModel>(ExecuteShowMultiCalendar);
            ShowRecall = Command.Create(ExecuteShowRecall);
            ShowAppointmentSummary = Command.Create(ExecuteShowAppointmentSummary);

            TimeSelectionOptions = new List<TimeSelectionViewModel>
                {
                    createTimeSelection(TimeSelectionType.AllDay), createTimeSelection(TimeSelectionType.Range), createTimeSelection(TimeSelectionType.Exact),
                    createTimeSelection(TimeSelectionType.Am), createTimeSelection(TimeSelectionType.Pm), createTimeSelection(TimeSelectionType.Before), createTimeSelection(TimeSelectionType.After)
                };
            SearchResults = new ExtendedObservableCollection<AppointmentSearchResultsViewModel>();

            SearchResultsRightClickCommands = new ExtendedObservableCollection<Tuple<string, ICommand>>();
            SearchResultsRightClickCommands.Add(new Tuple<string, ICommand>("View in Calendar", ShowMultiCalendar));
            SearchResultsRightClickCommands.Add(new Tuple<string, ICommand>("Schedule Appointment", ScheduleAppointment));
            AppointmentIds = new HashSet<int>();
        }

        private bool CanFind()
        {
            if (SearchFilter != null && SearchFilter != null)
            {
                bool basicCriteriaFulfilled = SearchFilter.SelectedAppointmentType != null
                                                  && SearchFilter.SelectedLocations != null && SearchFilter.SelectedLocations.Any()
                                                  && SearchFilter.SelectedResources != null && SearchFilter.SelectedResources.Any();

                bool dateCriteriaFulfilled = SelectedDateTimeFilter != null
                                            && SelectedDateTimeFilter.SelectedDateOption.IsValid();

                bool timeCriteriaFulfilled = SelectedDateTimeFilter != null
                                            && SelectedDateTimeFilter.SelectedTimeOption.IsValid();

                return basicCriteriaFulfilled && dateCriteriaFulfilled && timeCriteriaFulfilled;
            }
            return false;
        }

        private void ExecuteScheduleAppointment()
        {
            if (SelectedSearchResult != null)
            {
                Guid categoryIdToSchedule = SelectedSearchResult.ScheduleBlockCategoryIds.First();
                if (AppointmentId.HasValue)
                {
                    if (ShowRescheduleAppointmentView(AppointmentId.Value, categoryIdToSchedule))
                    {
                        HasRescheduledAppointmentsSuccessfully = true;
                        InteractionContext.Complete(false);
                    }
                }
                else
                {
                    if (ShowScheduleAppointmentView(categoryIdToSchedule))
                    {
                        SelectedSearchResult.ScheduleBlockCategoryIds.Remove(categoryIdToSchedule);
                        if (!SelectedSearchResult.ScheduleBlockCategoryIds.Any())
                        {
                            SearchResults.Remove(SelectedSearchResult);
                            SelectedSearchResult = null;
                        }

                        RefreshView();
                        SelectedPatientInfoTab = PatientInfoTab.AppointmentSummary;
                    }
                }
            }
        }

        private bool ShowScheduleAppointmentView(Guid targetCategory)
        {
            var loadArguments = new ScheduleAppointmentLoadArguments
            {
                PatientId = PatientInfo.Id,
                ScheduleBlockAppointmentCategoryId = targetCategory,
                AppointmentCategoryId = SelectedSearchResult.AppointmentType.AppointmentCategory.Id,
                Comment = LoadArguments.Comment,
                ReasonForVisit = LoadArguments.ReasonForVisit,
                AppointmentTypeId = SelectedSearchResult.AppointmentType.Id,
                OrderAppointmentDate = LoadArguments.OrderAppointmentDate
            };

            var manager = _createScheduleAppointmentViewManager();
            var returnArguments = manager.ShowScheduleAppointment(loadArguments);

            if (returnArguments.AppointmentId.HasValue)
            {
                AppointmentIds.Add(returnArguments.AppointmentId.Value);
            }
            return returnArguments.AppointmentId.HasValue;
        }

        private bool ShowRescheduleAppointmentView(int appointmentId, Guid targetBlockCategory)
        {
            var loadArguments = new RescheduleAppointmentLoadArguments
            {
                AppointmentIds = new List<int> { appointmentId },
                SelectedBlock = new RescheduleAppointmentViewModel
                {
                    AppointmentTypeId = SelectedSearchResult.AppointmentType.Id,
                    StartDateTime = SelectedSearchResult.Date.AddTicks(SelectedSearchResult.Time.Ticks),
                    LocationId = SelectedSearchResult.Location.Id,
                    ResourceId = SelectedSearchResult.Resource.Id,
                    ScheduleBlockCategoryId = targetBlockCategory
                }
            };

            RescheduleAppointmentViewManager manager = _createRescheduleAppointmentViewManager();

            var returnArgs = manager.ShowRescheduleAppointment(loadArguments);

            if (returnArgs.AppointmentsRescheduledSuccessfully) AppointmentIds.Add(appointmentId);
            return returnArgs.AppointmentsRescheduledSuccessfully;
        }

        private void ExecuteShowMultiCalendar(AppointmentSearchResultsViewModel selectedSearchResult)
        {
            #region helper func

            Func<DateSelectionViewModel, List<DateTime>> getSelectedDates = dateSelection =>
                {
                    var result = new List<DateTime>();

                    if (!dateSelection.BeginDate.HasValue) return result;

                    if (dateSelection.EndDate == null)
                    {
                        result.Add(dateSelection.BeginDate.Value);
                    }

                    else
                    {
                        ObservableCollection<DayOfWeek> selectedDaysOfWeek = dateSelection.SelectedDays;
                        DateTime startDate = dateSelection.BeginDate.Value.Date;
                        DateTime endDate = dateSelection.EndDate.Value.Date;
                        while (startDate <= endDate)
                        {
                            if (selectedDaysOfWeek.Contains(startDate.DayOfWeek))
                            {
                                result.Add(startDate);
                            }
                            startDate = startDate.AddDays(1);
                        }
                    }
                    return result;
                };

            #endregion

            var loadArgs = new MultiCalendarLoadArguments
                {
                    PatientId = PatientInfo.Id,
                    PatientDisplayName = PatientInfo.Name,
                    Dates = getSelectedDates(SelectedDateTimeFilter.SelectedDateOption),
                    ResourceIds = (SelectedSearchResult != null) ? new List<int> { SelectedSearchResult.Resource.Id }
                                                  : SearchFilter.SelectedResources.Select(r => r.Id).ToList(),
                    AppointmentId = AppointmentId,
                    OrderAppointmentDate = LoadArguments.OrderAppointmentDate,
                    Comment = LoadArguments.Comment
                };

            MultiCalendarViewManager multiCalendarViewManager = _createMultiCalendarViewManager();
            MultiCalendarReturnArguments returnArgs = multiCalendarViewManager.ShowMultiCalendar(loadArgs);

            HasRescheduledAppointmentsSuccessfully = returnArgs.HasRescheduledAppointmentSuccessfully;

            foreach (var apptId in returnArgs.AppointmentIds)
            {
                AppointmentIds.Add(apptId);
            }

            if (HasRescheduledAppointmentsSuccessfully.HasValue && HasRescheduledAppointmentsSuccessfully.Value)
            {
                InteractionContext.Complete(false);
            }
            else
            {
                RefreshView();
                SelectedPatientInfoTab = PatientInfoTab.AppointmentSummary;
            }
        }

        private void RefreshView()
        {
            const AppointmentSearchLoadTypes loadInformationTypes = AppointmentSearchLoadTypes.PatientAppointmentHistory
                                                        | AppointmentSearchLoadTypes.PatientComments
                                                        | AppointmentSearchLoadTypes.PatientInfo
                                                        | AppointmentSearchLoadTypes.PatientReferrals;
            Command.Create(() =>
            {
                AppointmentSearchLoadInformation loadInformation = _appointmentSearchViewService.LoadInformation(LoadArguments.PatientId, DateTime.Now.ToClientTime(), loadInformationTypes);
                AppointmentHistoryHelper.AppointmentHistory = loadInformation.PatientAppointmentHistory;
                PatientComments = loadInformation.PatientComments;
                PatientInfo = loadInformation.PatientInfo;
                PatientReferrals = loadInformation.PatientReferrals;
            }).Async().Execute();

            if (CanFind() && HasPerformedSearch)
            {
                Find.Execute();
            }
        }

        private void ExecuteShowRecall()
        {
            var recallLoadArguments = new RecallLoadArguments();
            recallLoadArguments.PatientId = PatientInfo.Id;
            if (SearchFilter.SelectedAppointmentType != null) recallLoadArguments.AppointmentType = SearchFilter.SelectedAppointmentType.Id;
            if (SearchFilter.SelectedLocations.Count != 0) recallLoadArguments.LocationId = SearchFilter.SelectedLocations.First().Id;
            if (SearchFilter.SelectedResources.Count != 0) recallLoadArguments.ResourceId = SearchFilter.SelectedResources.First().Id;
            recallLoadArguments.RecallDate = SelectedDateTimeFilter.SelectedDateOption.BeginDate;
            RecallsViewManager recallsViewManager = _createRecallsViewManager();
            recallsViewManager.ShowRecalls(recallLoadArguments);
        }

        private void ExecuteShowAppointmentSummary()
        {
            var patientInfoLoadArguments = new PatientInfoLoadArguments();
            patientInfoLoadArguments.PatientId = PatientInfo.Id;
            var patientInfoViewManager = _createPatientInfoViewManager();
            var returnArgs = patientInfoViewManager.ShowPatientInfo(patientInfoLoadArguments);
            foreach (var apptId in returnArgs.AppointmentIds)
            {
                AppointmentIds.Add(apptId);
            }
        }

        private void ExecuteLoad()
        {
            LoadArguments.EnsureNotDefault("LoadArguments not provided.");

            AppointmentId = LoadArguments.AppointmentId;

            var loadInformation = _appointmentSearchViewService.LoadInformation(LoadArguments.PatientId, DateTime.Now.ToClientTime());
            SelectedPatientInfoTab = PatientInfoTab.Comments;
            PatientComments = loadInformation.PatientComments;
            PatientInfo = loadInformation.PatientInfo;
            PatientReferrals = loadInformation.PatientReferrals;
            SearchFilter = loadInformation.SearchFilter;

            AppointmentTypeViewModel appointmentType = SearchFilter.AppointmentTypes.FirstOrDefault(at => at.Id == LoadArguments.AppointmentTypeId);
            var location = SearchFilter.Locations.FirstOrDefault(l => l.Id == LoadArguments.LocationId);

            NamedViewModel resource = null;
            if (LoadArguments.ResourceId.HasValue)
            {
                resource = SearchFilter.Resources.FirstOrDefault(r => r.Id == LoadArguments.ResourceId);
            }
            else if (PatientInfo.DefaultResourceId.HasValue)
            {
                resource = SearchFilter.Resources.FirstOrDefault(r => r.Id == PatientInfo.DefaultResourceId.Value);
            }

            TimeSelectionViewModel timeSelection = TimeSelectionOptions.First(tso => tso.TimeSelectionType == TimeSelectionType.AllDay);

            AppointmentSearchDateTimeFilterViewModel dateCriteria = _createAppointmentSearchDateTimeFilterViewModel();

            DateTime dt_LoadArgument = new DateTime(LoadArguments.StartDate.Date.Year, LoadArguments.StartDate.Date.Month, LoadArguments.StartDate.Date.Day, LoadArguments.StartDate.Date.Hour, LoadArguments.StartDate.Date.Minute, LoadArguments.StartDate.Date.Second);
            dateCriteria.SelectedDateOption.BeginDate = dt_LoadArgument;

            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                dateCriteria.SelectedDateOption.SelectedDays.Add(day);
            }
            dateCriteria.SelectedTimeOption = timeSelection;
            SelectedDateTimeFilter = dateCriteria;

            SearchFilter.SelectedAppointmentType = appointmentType;
            if (location != null)
            {
                SearchFilter.SelectedLocations.Add(location);
            }
            else
            {
                var matchingLocationsByConfiguration = SearchFilter.Locations.Where(l => string.Equals(l.Name, UserContext.Current.ServiceLocation.Item2, StringComparison.OrdinalIgnoreCase)).ToList();
                matchingLocationsByConfiguration.ForEach(SearchFilter.SelectedLocations.Add);
            }
            if (resource != null)
            {
                SearchFilter.SelectedResources.Add(resource);
            }
            if (!DisableLoadOfPatientPostOp) Task.Factory.StartNewWithCurrentTransaction(() => PatientPostOp = _postOperativePeriodService.GetLatestPostOperativePeriodByPatientId(LoadArguments.PatientId));

            var factory = _createAppointmentHistoryHelperFactory();
            AppointmentHistoryHelper = factory.CreateAppointmentHistoryHelper(loadInformation.PatientAppointmentHistory,
                                                                              () => PatientInfo.Id,
                                                                              RefreshView,
                                                                              () => AppointmentIds);
        }

        private void ExecuteFind()
        {
            SearchResults = null;
            SearchResults = _appointmentSearchViewService.FindAppointments(SearchFilter, SelectedDateTimeFilter);
            HasPerformedSearch = true;
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to issue search request
        /// </summary>
        public ICommand Find { get; protected set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Command to show the MultiCalendar
        /// </summary>
        public ICommand ShowMultiCalendar { get; protected set; }

        /// <summary>
        /// Command to show the recall screen
        /// </summary>
        public ICommand ShowRecall { get; protected set; }

        /// <summary>
        /// Schedules an appointment.
        /// </summary>
        public ICommand ScheduleAppointment { get; protected set; }

        /// <summary>
        /// Command to show the appointment summary screen in PatientInfo
        /// </summary>
        public ICommand ShowAppointmentSummary { get; set; }

        /// <summary>
        /// Gets an AppointmentHistoryHelper that assists on performing the logic to control the appointment history table.
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentHistoryHelper AppointmentHistoryHelper { get; protected set; }

        /// <summary>
        /// Flag that indicates whether this control has performed a search yet
        /// </summary>
        /// <remarks>
        /// This was added so that the "Perform a search to see the results here" message can disappear on performing a search.
        /// </remarks>
        [DispatcherThread]
        public virtual bool HasPerformedSearch { get; protected set; }

        /// <summary>
        /// Determin what tab is selected in the UI for the patient info section
        /// </summary>
        [DispatcherThread]
        public virtual PatientInfoTab SelectedPatientInfoTab { get; set; }

        /// <summary>
        /// Member containing basic information about the Patient
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentSearchPatientInfoViewModel PatientInfo { get; set; }

        /// <summary>
        /// Member containing the comments for the patient
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentSearchPatientCommentsViewModel PatientComments { get; set; }

        /// <summary>
        /// Collection of referrals that can be used to display referral information for the patient
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSearchReferralsViewModel> PatientReferrals { get; set; }

        /// <summary>
        /// Search choices to determine what criteria to search by
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentSearchFilterViewModel SearchFilter { get; set; }

        /// <summary>
        /// Populate UI with selection options for time
        /// </summary>
        [DispatcherThread]
        public virtual IEnumerable<TimeSelectionViewModel> TimeSelectionOptions { get; protected set; }

        /// <summary>
        /// Search choice for date and time
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentSearchDateTimeFilterViewModel SelectedDateTimeFilter { get; set; }

        /// <summary>
        /// Collection of available appointment blocks 
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentSearchResultsViewModel> SearchResults { get; set; }

        /// <summary>
        /// Collection of ICommands and their text representation for use when right clicking a search result
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<Tuple<string, ICommand>> SearchResultsRightClickCommands { get; set; }

        /// <summary>
        /// Model to represent the patient's post operative period (if one exists)
        /// </summary>
        [DispatcherThread]
        public virtual PostOperativeViewModel PatientPostOp { get; set; }

        internal bool DisableLoadOfPatientPostOp { get; set; }

        [DispatcherThread]
        [DependsOn("PatientPostOp")]
        public virtual bool ShowPostOp
        {
            get { return PatientPostOp != null && PatientPostOp.CurrentlyInPostOperativePeriod; }
        }

        /// <summary>
        /// The selected search result
        /// </summary>
        [DispatcherThread]
        public virtual AppointmentSearchResultsViewModel SelectedSearchResult { get; set; }

        public virtual AppointmentSearchLoadArguments LoadArguments { get; set; }

        public int? AppointmentId { get; set; }

        public bool? HasRescheduledAppointmentsSuccessfully { get; set; }

        public HashSet<int> AppointmentIds { get; set; }
    }

    public enum PatientInfoTab
    {
        Comments,
        AppointmentSummary,
        Referrals
    }

    /// <summary>
    ///   Factory interface to create the AppointmentHistoryHelper.
    ///   Provides us with a cleaner interface to create/proxy the AppointmentHistoryHelper.
    /// </summary>
    [Factory]
    public interface IAppointmentHistoryHelperFactory
    {
        AppointmentHistoryHelper CreateAppointmentHistoryHelper(ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> appointmentHistory,
                                                                Func<int> getPatientId,
                                                                Action refreshView,
                                                                Func<HashSet<int>> getAppointmentIds);
    }
}
