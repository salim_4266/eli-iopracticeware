﻿using IO.Practiceware.Presentation.ViewServices.AppointmentSearch;
using Soaf;
using Soaf.Presentation;
using System;
using System.Linq;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.AppointmentSearch
{
    public class AppointmentSearchViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IAppointmentSearchViewService _viewService;

        public AppointmentSearchViewManager(IInteractionManager interactionManager, IAppointmentSearchViewService viewService)
        {
            _interactionManager = interactionManager;
            _viewService = viewService;
        }

        public void ShowAppointmentSearch(int patientId)
        {
            ShowAppointmentSearch(new AppointmentSearchLoadArguments {PatientId = patientId, StartDate = DateTime.Today});
        }

        public AppointmentSearchReturnArguments ShowAppointmentSearch(AppointmentSearchLoadArguments loadArgs)
        {
            loadArgs.EnsureNotDefault("LoadArguments not provided.");

            bool isPatientActive = LoadingWindow.Run(() => _viewService.IsPatientActive(loadArgs.PatientId));
            if (!isPatientActive)
            {
                InteractionManager.Current.Alert("Cannot schedule an appointment for an inactive patient.");
                return new AppointmentSearchReturnArguments();
            }

            var view = new AppointmentSearchView();

            var dataContext = view.DataContext.EnsureType<AppointmentSearchViewContext>();
            dataContext.LoadArguments = loadArgs;

            _interactionManager.ShowModal(new WindowInteractionArguments {Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip});

            var returnArgs = new AppointmentSearchReturnArguments();
            returnArgs.HasRescheduledAppointmentsSuccessfully = dataContext.HasRescheduledAppointmentsSuccessfully;
            returnArgs.AppointmentIds = dataContext.AppointmentIds.ToArray();
            
            return returnArgs;
        }
    }

    public class AppointmentSearchLoadArguments
    {
        public int? AppointmentId { get; set; }
        public int AppointmentTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public int LocationId { get; set; }
        public int? ResourceId { get; set; }
        public int PatientId { get; set; }
        public string ReasonForVisit { get; set; }
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the date time of the (optional) order appointment.
        /// </summary>
        /// <remarks>
        /// An order appointment is one that orders a Surgery Appointment.
        /// </remarks>
        public DateTime? OrderAppointmentDate { get; set; }
    }

    public class AppointmentSearchReturnArguments
    {
        public bool? HasRescheduledAppointmentsSuccessfully { get; set; }

        public int[] AppointmentIds { get; set; }
    }
}
