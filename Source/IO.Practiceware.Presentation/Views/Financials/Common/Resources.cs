using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Financials.Common
{
    public class Resources
    {
        #region Brushes
        /// <summary>
        /// This color determines the indicator color on the filtering comboboxes
        /// Designer can change colors for each filter from here
        /// </summary>
        public static SolidColorBrush OpenInvoiceBrush { get { return new SolidColorBrush(Color.FromRgb(31, 132, 181)); } }
        public static SolidColorBrush OpenServiceBrush { get { return new SolidColorBrush(Color.FromRgb(1, 191, 165)); } }
        public static SolidColorBrush EmptyInvoiceBrush { get { return new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)); } }
        public static SolidColorBrush InsurerPaymentBrush { get { return new SolidColorBrush(Color.FromRgb(240, 223, 161)); } }
        public static SolidColorBrush PatientPaymentBrush { get { return new SolidColorBrush(Color.FromRgb(255, 238, 176)); } }
        public static SolidColorBrush InsurerAdjustmentBrush { get { return new SolidColorBrush(Color.FromRgb(244, 169, 162)); } }
        public static SolidColorBrush PatientAdjustmentBrush { get { return new SolidColorBrush(Color.FromRgb(255, 199, 192)); } }
        public static SolidColorBrush BillingBrush { get { return new SolidColorBrush(Color.FromRgb(191, 228, 229)); } }
        public static SolidColorBrush NoteBrush { get { return new SolidColorBrush(Color.FromRgb(195, 151, 151)); } }
        public static SolidColorBrush InformationalBrush { get { return new SolidColorBrush(Color.FromRgb(206, 216, 216)); } }

        public static LinearGradientBrush CreateOpenAndClosedInvoiceBrush()
        {
            var toReturn = new LinearGradientBrush();

            toReturn.StartPoint = new Point(0, 0);
            toReturn.EndPoint = new Point(1, 0);

            var blue = new GradientStop { Color = Color.FromRgb(31, 132, 181), Offset = 0.5 };
            var gray = new GradientStop { Color = Color.FromRgb(160, 160, 160), Offset = 0.5 };

            toReturn.GradientStops.Add(blue);
            toReturn.GradientStops.Add(gray);

            return toReturn;
        }

        public static LinearGradientBrush CreateOpenAndClosedServiceBrush()
        {
            var toReturn = new LinearGradientBrush();

            toReturn.StartPoint = new Point(0, 0);
            toReturn.EndPoint = new Point(1, 0);

            var blue = new GradientStop { Color = Color.FromRgb(1, 191, 165), Offset = 0.5 };
            var gray = new GradientStop { Color = Color.FromRgb(160, 160, 160), Offset = 0.5 };

            toReturn.GradientStops.Add(blue);
            toReturn.GradientStops.Add(gray);

            return toReturn;
        }

        public static LinearGradientBrush CreateOpenAndPayerServiceBrush()
        {
            var toReturn = new LinearGradientBrush();

            toReturn.StartPoint = new Point(0, 0);
            toReturn.EndPoint = new Point(1, 0);

            var blue = new GradientStop { Color = Color.FromRgb(1, 191, 165), Offset = 0.5 };
            var gray = new GradientStop { Color = Color.FromRgb(191, 228, 229), Offset = 0.5 };

            toReturn.GradientStops.Add(blue);
            toReturn.GradientStops.Add(gray);

            return toReturn;
        }

        public static LinearGradientBrush CreateInsurerAndPatientPaymentBrush()
        {
            var toReturn = new LinearGradientBrush();

            toReturn.StartPoint = new Point(0, 0);
            toReturn.EndPoint = new Point(1, 0);

            var blue = new GradientStop { Color = Color.FromRgb(230, 213, 151), Offset = 0.5 };
            var gray = new GradientStop { Color = Color.FromRgb(255, 238, 176), Offset = 0.5 };

            toReturn.GradientStops.Add(blue);
            toReturn.GradientStops.Add(gray);

            return toReturn;
        }
        public static LinearGradientBrush CreateInsurerAndPatientAdjustmentBrush()
        {
            var toReturn = new LinearGradientBrush();

            toReturn.StartPoint = new Point(0, 0);
            toReturn.EndPoint = new Point(1, 0);

            var blue = new GradientStop { Color = Color.FromRgb(234, 159, 152), Offset = 0.5 };
            var gray = new GradientStop { Color = Color.FromRgb(255, 199, 192), Offset = 0.5 };

            toReturn.GradientStops.Add(blue);
            toReturn.GradientStops.Add(gray);

            return toReturn;
        }
        #endregion
    }
}
