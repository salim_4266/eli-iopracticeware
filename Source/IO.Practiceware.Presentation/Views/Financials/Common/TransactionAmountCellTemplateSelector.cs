﻿
using System.Windows;
using System.Windows.Controls;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;

namespace IO.Practiceware.Presentation.Views.Financials.Common
{
    public class AdjustmentAmountCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (!(item is TransactionViewModel)) return null;
            var x = item as TransactionViewModel;

            if (x == item as BillingTransactionViewModel)
            {
                return EmptyTemplate;
            }
            if (x == item as TransactionalNoteViewModel)
            {
                return EmptyTemplate;
            }
            return AmountTemplate;
        }

        public DataTemplate AmountTemplate { get; set; }
        public DataTemplate EmptyTemplate { get; set; }
    }

    public class BillingTransactionAmountCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (!(item is TransactionViewModel)) return null;
            var x = item as TransactionViewModel;

            if (x == item as BillingTransactionViewModel)
            {
                return AmountTemplate;
            }

            return EmptyTemplate;
        }

        public DataTemplate AmountTemplate { get; set; }
        public DataTemplate EmptyTemplate { get; set; }
    }
}
