﻿using System.Windows;
using System.Windows.Controls;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf;

namespace IO.Practiceware.Presentation.Views.Financials.Common
{
    public class TransactionDetailsCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            // Check it's a transaction
            var x = item as TransactionViewModel;
            if (x == null) return null;

            DataTemplate result = null;
            if (x is BillingTransactionViewModel)
            {
                result = BillingTransactionTemplate;
            }
            else if (x is FinancialTransactionViewModel)
            {
                var y = x as FinancialTransactionViewModel;
                if (y.FinancialTransactionType is AdjustmentTypeViewModel)
                {
                    result = y.FinancialTransactionType.CastTo<AdjustmentTypeViewModel>().IsCash 
                        ? PaymentTemplate 
                        : AdjustmentTemplate;
                }
                else if (y.FinancialTransactionType is FinancialInformationTypeViewModel)
                {
                    result = FinancialInformationTemplate;
                }
            }
            else if (x is TransactionalNoteViewModel)
            {
                result = NoteTemplate;
            }

            return result;
        }

        public DataTemplate PaymentTemplate { get; set; }

        public DataTemplate BillingTransactionTemplate { get; set; }

        public DataTemplate FinancialInformationTemplate { get; set; }

        public DataTemplate AdjustmentTemplate { get; set; }

        public DataTemplate NoteTemplate { get; set; }
    }
}
