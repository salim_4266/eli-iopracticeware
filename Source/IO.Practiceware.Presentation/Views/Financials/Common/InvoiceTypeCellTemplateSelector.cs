﻿using System.Windows;
using System.Windows.Controls;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary;

namespace IO.Practiceware.Presentation.Views.Financials.Common
{
    public class InvoiceTypeCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            // Check it's a transaction
            var x = item as InvoiceViewModel;
            if (x == null) return null;

            DataTemplate result = null;
            if (x.InvoiceType == InvoiceType.Professional)
            {
                result = ProfessionalIconTemplate;
            }
            else if (x.InvoiceType == InvoiceType.Facility)
            {
                result = FacilityIconTemplate;
            }
            else if (x.InvoiceType == InvoiceType.Vision)
            {
                result = VisionIconTemplate;
            }

            return result;
        }

        public DataTemplate ProfessionalIconTemplate { get; set; }

        public DataTemplate FacilityIconTemplate { get; set; }

        public DataTemplate VisionIconTemplate { get; set; }
    }
}
