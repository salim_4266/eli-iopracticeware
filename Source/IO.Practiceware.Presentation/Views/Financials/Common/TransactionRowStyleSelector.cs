﻿using System.Windows;
using System.Windows.Controls;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf;

namespace IO.Practiceware.Presentation.Views.Financials.Common
{
    public class TransactionRowStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            // Check it's a transaction
            var x = item as TransactionViewModel;
            if (x == null) return null;

            if (x is BillingTransactionViewModel)
            {
                return BillingTransactionStyle;
            }
            if (x is FinancialTransactionViewModel)
            {
                var y = x as FinancialTransactionViewModel;
                if (y.FinancialTransactionType is AdjustmentTypeViewModel)
                {
                    if (y.Source != null && (FinancialSourceType)y.Source.Id == FinancialSourceType.Insurer)
                    {
                        if (y.FinancialTransactionType.CastTo<AdjustmentTypeViewModel>().IsCash)
                            return InsurerPaymentStyle;
                        return InsurerAdjustmentStyle;
                    }

                    if (y.FinancialTransactionType.CastTo<AdjustmentTypeViewModel>().IsCash)
                        return PatientPaymentStyle;
                    return PatientAdjustmentStyle;

                }
                if (y.FinancialTransactionType is FinancialInformationTypeViewModel)
                    return FinancialInformationStyle;
            }
            else if (x is TransactionalNoteViewModel)
            {
                return NoteStyle;
            }
            return BackupStyle;
        }

        public Style PatientPaymentStyle { get; set; }
        public Style InsurerPaymentStyle { get; set; }
        public Style PatientAdjustmentStyle { get; set; }
        public Style InsurerAdjustmentStyle { get; set; }
        public Style BillingTransactionStyle { get; set; }
        public Style FinancialInformationStyle { get; set; }
        public Style NoteStyle { get; set; }
        public Style BackupStyle { get; set; }
    }
}
