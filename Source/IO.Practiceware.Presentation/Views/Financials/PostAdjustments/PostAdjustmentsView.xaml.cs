﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    /// <summary>
    /// Interaction logic for PostAdjustmentsView.xaml
    /// </summary>
    public partial class PostAdjustmentsView
    {

        public PostAdjustmentsView()
        {
            InitializeComponent();
        }
    }

    public class PostPaymentsViewContextReference : DataContextReference
    {
        public new PostAdjustmentsViewContext DataContext
        {
            get { return (PostAdjustmentsViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
