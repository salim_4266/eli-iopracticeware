using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    public class PostAdjustmentsLoadArguments
    {
        #region Financial Batch existing or new

        /// <summary>
        /// Financial batch Id. Optional, takes priority over TransactionOfInterest. If <c>null</c> - open screen to create new payment instrument
        /// </summary>
        public int? PaymentInstrumentId { get; set; }

        /// <summary>
        /// Transaction user is interested in. Optional, used to load corresponding financial batch or special handling for on-account transactions
        /// </summary>
        public FinancialTransactionViewModel TransactionOfInterest { get; set; }

        #endregion

        /// <summary>
        /// Patient's Id. Optional, allows to select patients if not provided
        /// </summary>
        public int? PatientId { get; set; }

        /// <summary>
        /// Invoice Id. Optional, shows all open invoices if not provided
        /// </summary>
        public int? InvoiceId { get; set; }
    }
}