﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.Views.Alerts;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.PatientSearch;
using IO.Practiceware.Presentation.ViewServices.Alerts;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;
using FinancialSourceType = IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction.FinancialSourceType;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    public class PostAdjustmentsViewContext : IViewContext, ICommonViewFeatureHandler
    {
        private static readonly string PaymentInstrumentPayerFieldName = Reflector.GetMember<PaymentInstrumentViewModel>(pi => pi.Payer).Name;
        private static readonly string PaymentInstrumentCheckDateFieldName = Reflector.GetMember<PaymentInstrumentViewModel>(pi => pi.CheckDate).Name;
        private static readonly string PaymentInstrumentPaymentAmountFieldName = Reflector.GetMember<PaymentInstrumentViewModel>(pi => pi.PaymentAmount).Name;
        private static readonly string PaymentInstrumentReferenceNumberFieldName = Reflector.GetMember<PaymentInstrumentViewModel>(pi => pi.ReferenceNumber).Name;
        private static readonly string PaymentInstrumentFinancialSourceTypeFieldName = Reflector.GetMember<PaymentInstrumentViewModel>(pi => pi.FinancialSourceType).Name;
        private static readonly string FinancialTransactionTypePropertyName = Reflector.GetMember<FinancialTransactionViewModel>(t => t.FinancialTransactionType).Name;
        private static readonly string AccountNewPaidAmountPropertyName = Reflector.GetMember<AccountViewModel>(m => m.NewPaidAmount).Name;

        private readonly IPostAdjustmentsViewService _viewService;
        private readonly IInteractionManager _interactionManager;
        private readonly IImageService _imageService;
        private readonly WarningsViewManager _warningsViewManager;
        private readonly PatientSearchViewManager _patientSearchViewManager;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly Func<AccountViewModel> _createAccount;
        private readonly Func<BillingServiceViewModel> _createBillingService;
        private readonly Func<ServiceViewModel> _createService;
        private readonly Func<FinancialTransactionViewModel> _createFinancialTransaction;
        private readonly List<FinancialTransactionViewModel> _pendingTransactionRemovals;
        private readonly IMapper<FinancialSourceType, NamedViewModel> _financialSourceTypeMapper;
        private readonly RefreshableCachedValue<ObservableCollection<InsurerPayerViewModel>> _insurersSelection = new RefreshableCachedValue<ObservableCollection<InsurerPayerViewModel>>();
        private readonly RefreshableCachedValue<ObservableCollection<PatientPayerViewModel>> _patientsSelection = new RefreshableCachedValue<ObservableCollection<PatientPayerViewModel>>();
        private readonly AlertViewManager _alertViewManager;
        private readonly IAlertViewService _alertViewService;

        public PostAdjustmentsViewContext(
            IPostAdjustmentsViewService viewService,
            IInteractionManager interactionManager,
            IImageService imageService,
            WarningsViewManager warningsViewManager,
            PatientSearchViewManager patientSearchViewManager,
            PatientInfoViewManager patientInfoViewManager,
            Func<AccountViewModel> createAccount,
            Func<BillingServiceViewModel> createBillingService,
            Func<ServiceViewModel> createService,
            Func<FinancialTransactionViewModel> createFinancialTransaction,
            AlertViewManager alertViewManager,
            ICommonViewFeatureExchange viewFeatureExchange,
            IAlertViewService alertViewService)
        {
            _viewService = viewService;
            _interactionManager = interactionManager;
            _imageService = imageService;
            _warningsViewManager = warningsViewManager;
            _patientSearchViewManager = patientSearchViewManager;
            _patientInfoViewManager = patientInfoViewManager;
            _createAccount = createAccount;
            _createBillingService = createBillingService;
            _createService = createService;
            _createFinancialTransaction = createFinancialTransaction;
            _alertViewManager = alertViewManager;
            ViewFeatureExchange = viewFeatureExchange;
            _alertViewService = alertViewService;

            _pendingTransactionRemovals = new List<FinancialTransactionViewModel>();
            _financialSourceTypeMapper = Mapper.Factory.CreateNamedViewModelMapper<FinancialSourceType>();

            // Init UI defaults (while loading)
            PostingInProgress = true;
            IsInBatchMode = false;

            // Init invoice services filters
            InvoiceServicesFilters = new List<FilterSelectionViewModel>
            {
                new FilterSelectionViewModel {Id = (int) InvoiceServicesFilterType.OpenToNextPayer, Name = InvoiceServicesFilterType.OpenToNextPayer.GetDisplayName(), Color = Common.Resources.CreateOpenAndPayerServiceBrush()},
                new FilterSelectionViewModel {Id = (int) InvoiceServicesFilterType.AllOpen, Name = InvoiceServicesFilterType.AllOpen.GetDisplayName(), Color = Common.Resources.OpenServiceBrush},
                new FilterSelectionViewModel {Id = (int) InvoiceServicesFilterType.AllServices, Name = InvoiceServicesFilterType.AllServices.GetDisplayName(), Color = Common.Resources.CreateOpenAndClosedServiceBrush()}
            };

            CheckCanEditTransaction = o => o.As<FinancialTransactionViewModel>().IfNotNull(t => IsTransactionEditable(t, true));
            Close = Command.Create(() => InteractionContext.Complete(false));
            Closing = Command.Create<Action>(ExecuteClosing);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Post = Command.Create(() => ExecutePost(false), CanPost).Async(() => InteractionContext);
            PostAndNext = Command.Create(() => ExecutePost(true), CanPost).Async(() => InteractionContext);
            Suggest = Command.Create(ExecuteSuggest, HasChanges).Async();
            StartPosting = Command.Create(ExecuteStartPosting).Async(() => InteractionContext);
            CheckDuplicatePaymentInstrument = Command.Create(ExecuteCheckDuplicatePaymentInstrument)
                .Async().Delay(700);
            DeleteTransaction = Command.Composite(
                Command.Create<RemoveItemArguments>(ExecuteDeleteTransaction, CanDeleteTransaction),
                CollectionCommands.RemoveItem);
            AddPatient = Command.Create(ExecuteAddPatient, () => !string.IsNullOrWhiteSpace(PatientSearchText)).Async(() => InteractionContext);
            OpenPatientProfile = Command.Create<AccountViewModel>(ExecuteOpenPatientProfile);
            RemoveAccount = Command.Composite(
                Command.Create<RemoveItemArguments>(ExecuteRemoveAccount, CanRemoveAccount),
                CollectionCommands.RemoveItem);
            LoadPatientPhoto = Command.Create<AccountViewModel>(ExecuteLoadPatientPhoto).Async();
            FilterInsurers = Command.Create(ExecuteFilterInsurers).Async();
            CancelPaymentInstrumentEdit = Command.Create(ExecuteCancelPaymentInstrumentEdit);
            SavePaymentInstrumentEdit = Command.Create(ExecuteSavePaymentInstrumentEdit).Async(() => InteractionContext);

            ShowAlertScreen = Command.Create(() => ExecuteShowAlertScreen(true)).Async(() => InteractionContext);
            ViewFeatureExchange.ShowPendingAlert = ShowAlertScreen.Execute;
        }

        private void ExecuteLoadPatientPhoto(AccountViewModel account)
        {
            // Skip if photo already loaded
            if (account == null || account.Photo != null) return;

            account.Photo = _imageService.GetPatientPhoto(account.PatientId);
        }

        private void ExecuteLoad()
        {
            // Get close datetime to check whether transaction can be edited
            CloseDateTime = ApplicationSettings.Cached.GetSetting<DateTime>(ApplicationSetting.CloseDateTime)
                .IfNotNull(s => (DateTime?)s.Value);

            Reload(false);

            ExecuteShowAlertScreen(false);
        }

        private void ExecuteShowAlertScreen(bool showNoAlertPop)
        {
            var loadArgument = new NoteLoadArguments();
            loadArgument.AlertScreen = AlertScreen.PostAdjustments;
            if (LoadArguments.InvoiceId.HasValue)
            {
                loadArgument.NoteType = NoteType.InvoiceComment;
                loadArgument.EntityId = LoadArguments.InvoiceId;
            }
            else if (LoadArguments.PatientId.HasValue)
            {
                loadArgument.NoteType = NoteType.PatientFinancialComment;
                loadArgument.EntityId = LoadArguments.PatientId;
            }

            if (!loadArgument.EntityId.HasValue)
            {
                return;
            }

            if (_alertViewService.HasAlerts(loadArgument))
            {
                this.Dispatcher().Invoke(() => _alertViewManager.ShowAlerts(loadArgument));
                ViewFeatureExchange.HasPendingAlert = _alertViewService.HasAlerts(loadArgument);
            }
            else
            {
                AlertsTrigger = showNoAlertPop;
            }
        }

        private void ExecuteOpenPatientProfile(AccountViewModel account)
        {
            if (account == null) return;

            _patientInfoViewManager.ShowPatientInfo(new PatientInfoLoadArguments
            {
                PatientId = account.PatientId,
                TabToOpen = PatientInfoTab.FinancialData
            });
        }

        private void ExecuteAddPatient()
        {
            var searchText = PatientSearchText;
            PatientSearchText = string.Empty;

            // Search for patient using specified search text
            int? patientId;
            if (!_patientSearchViewManager.SearchPatient(searchText, false, out patientId)
                // Already have the patient? Skip
                || AccountsSource.Any(a => a.PatientId == patientId))
            {
                return;
            }

            ShouldActivateAddedAccount = true;

            // Load selected patient and show on the UI
            var account = _viewService.LoadAccount(patientId.GetValueOrDefault(), LoadArguments.InvoiceId, PaymentInstrument.Id);
            PrepareAccountForDisplay(account, PaymentInstrument, DataLists.BillingActions);
            AccountsSource.Add(account);

            // If we added first account and no payer selected -> preselect it as payer
            if (AccountsSource.Count == 1
                && CanEditPaymentInstrument
                && PaymentInstrument.FinancialSourceType != FinancialSourceType.BillingOrganization
                && PaymentInstrument.FinancialSourceType != FinancialSourceType.ExternalOrganization
                && PaymentInstrument.Payer == null)
            {
                PaymentInstrument.FinancialSourceType = FinancialSourceType.Patient;
                PaymentInstrument.Payer = PatientAndContactsSelection.First();
            }

            SelectedAccount = account;

            // Must post for newly added patient first
            AddPatientAllowed = false;
        }

        private void ExecutePost(bool goToNext)
        {
            if (!CanPost()) return;

            // Validate
            if (ShowValidationWarnings()) return;

            //Check the CloseDateTime before posting.
            if (CloseDateTime.HasValue && DateTime.Compare(CloseDateTime.Value, PaymentInstrument.Date) > 0)
            {
                _interactionManager.Alert("The posting date you have selected, {0}, is before the close date, {1}. Therefore, no changes were made. Either adjust the posting date to be any date after {1}, or adjust the close date."
                    .FormatWith(PaymentInstrument.Date.ToShortDateString(), CloseDateTime.Value.Date.ToShortDateString()));
                return;
            }
            //Check the future date before posting
            if ((PaymentInstrument.CheckDate.HasValue && PaymentInstrument.CheckDate > DateTime.Now.ToClientTime().Date) ||
                PaymentInstrument.Date > DateTime.Now.ToClientTime().Date)
            {
                var checkDate = _interactionManager.Confirm("Are you sure you wish to continue with a future-dated payment?", "Cancel", "Continue");
                if (!checkDate.GetValueOrDefault()) return;
            }

            
           // Code commented after addition ( NO REQUIREMENT NOW)
            //if ( PaymentInstrument.RemainingPaymentBalance < 0 )
            //{
            //    // new alert message - Please ensure Payment should be less than or equal to allocated amount.
            //    _interactionManager.Alert("Please ensure Payment should be less than or equal to allocated amount");
            //    return;
            //}

            // Anything left on a check? Post on-account
            if (!IsInBatchMode
                && PaymentInstrument.RemainingPaymentBalance > 0
                && PaymentInstrument.FinancialSourceType != FinancialSourceType.BillingOrganization)
            {
                var postOnAccount = _interactionManager.Confirm(
                    string.Format("This batch has a remaining balance of {0:C}. Would you like to post {0:C} on-account?", PaymentInstrument.RemainingPaymentBalance),
                    "No", "Ok");
                if (postOnAccount.GetValueOrDefault())
                {
                    // We are in single-patient context, so this works
                    var account = AccountsSource.First();

                    var onAccountTransaction = _createFinancialTransaction();
                    onAccountTransaction.Amount = PaymentInstrument.RemainingPaymentBalance;
                    // Payment type
                    onAccountTransaction.FinancialTransactionType = OnAccountFinancialTransactionTypesSelection.First();

                    // Add it
                    account.OnAccountTransactions.Add(onAccountTransaction);
                }
            }

            var accountsForPosting = CollectActiveAccounts();
            HandleTransactionTypeChanges(accountsForPosting);
            RemoveZeroOnAccountTransactions(accountsForPosting);

            //Create Invoice Receivables for the Insurances added after Bill if not exists before posting
            var invoiceId = accountsForPosting.FirstOrDefault().IfNotNull(x=> x.BillingServices.FirstOrDefault()).IfNotNull(x=>x.InvoiceId);
            if (invoiceId != 0)
            {
                _viewService.InsertInvoiceReceivable(invoiceId);               
            }
            

            // Post
            var result = goToNext
                ? _viewService.PostAndRefresh(PaymentInstrument, accountsForPosting, _pendingTransactionRemovals, LoadArguments.InvoiceId)
                : _viewService.Post(PaymentInstrument, accountsForPosting, _pendingTransactionRemovals);

            // Show posting errors if any
            if (result.Errors.Any())
            {
                var warnings = result.Errors
                    .Select(x => string.Format("Invoice {0}:{1}{2}",
                        x.InvoiceId, Environment.NewLine, x.Messages
                            .Select(v => v.GetDisplayName())
                            .Join(Environment.NewLine)))
                    .Join(Environment.NewLine);

                _interactionManager.Alert(warnings);
                return;
            }

            // Empty transaction removals list
            _pendingTransactionRemovals.Clear();

            // Payment instrument has been saved -> lock it
            PaymentInstrument.Id = result.PaymentInstrumentId;

            var selectedAccountIndex = SelectedAccount != null
                ? AccountsSource.IndexOf(SelectedAccount)
                : AccountsSource.Count;

            if (goToNext)
            {
                // We don't want to activate account automatically when replacing it
                ShouldActivateAddedAccount = false;

                SelectedAccount = null;

                // Refresh posted accounts information on the UI
                foreach (var postedAccount in result.PostedAccounts)
                {
                    var account = postedAccount;
                    var accountBeforePosting = AccountsSource.First(a => a.PatientId == account.PatientId);
                    var selectedPayerOverride = accountBeforePosting.PayerOverride;

                    CleanupAccountAfterDisplay(accountBeforePosting);
                    PrepareAccountForDisplay(account, PaymentInstrument, DataLists.BillingActions);

                    // Restore payer override
                    account.PayerOverride = selectedPayerOverride
                        .IfNotNull(o => account.OverridePayers.FirstOrDefault(p => p.Id == o.Id));

                    // Replace account
                    AccountsSource.Replace(accountBeforePosting, account);
                }
            }

            // Accept all pending UI changes
            PaymentInstrument.CastTo<IEditableObjectExtended>().AcceptCustomEdits();
            AccountsSource.AcceptCustomEdits();

            // Close dialog if not switching to next account
            if (!goToNext)
            {
                InteractionContext.Complete(true);
                return;
            }

            // Select next account
            if (selectedAccountIndex + 1 <= AccountsSource.Count - 1)
            {
                SelectedAccount = AccountsSource[selectedAccountIndex + 1];
            }

            // Unblock patient edits
            AddPatientAllowed = true;
        }

        private void ExecuteSuggest()
        {
            if (!HasChanges()) return;

            // Validate
            if (ShowValidationWarnings()) return;

            // Request suggestion
            var accountsForPosting = CollectActiveAccounts();
            var suggestions = _viewService.Suggest(PaymentInstrument, accountsForPosting);

            foreach (var balanceForwardSuggestion in suggestions)
            {
                // Find service to apply suggestion to
                var account = AccountsSource.First(a => a.PatientId == balanceForwardSuggestion.AccountId);
                var billingService = account.BillingServices.First(bs => bs.Id == balanceForwardSuggestion.BillingServiceId);

                // Use a payer from next payers selection that matches suggestion or add suggestion to it
                var nextPayer = billingService.NextPayersSelection
                    .FirstOrDefault(np => np.Equals(balanceForwardSuggestion.To));
                if (nextPayer == null && balanceForwardSuggestion.To != null)
                {
                    billingService.NextPayersSelection.Add(balanceForwardSuggestion.To);
                    nextPayer = balanceForwardSuggestion.To;
                }

                // Apply suggestion
                billingService.NextPayer = nextPayer;
                billingService.BillingAction = balanceForwardSuggestion.Action;
            }
        }

        private void ExecuteStartPosting()
        {
            // Validate input
            var piErrors = PaymentInstrument.Validate();
            if (piErrors.Any())
            {
                if (AccountsSource.Count == 0)
                {
                    piErrors.Add("Accounts", "Please, add at least one patient to get payers selection");
                }
                var errorMessage = piErrors.Values.Join(Environment.NewLine);
                _interactionManager.Alert(errorMessage);
                return;
            }

            PostingInProgress = true;
        }

        private void ExecuteCheckDuplicatePaymentInstrument()
        {
            var pi = PaymentInstrument;

            // Do not check for duplicates while payment instrument is not yet valid
            if (InteractionContext.IsComplete
                || (!CanEditPaymentInstrument && !CanEditPaymentInstrumentRound1)
                || !pi.IsValid())
            {
                return;
            }

            // Perform duplicate check
            var duplicatePaymentInstrument = _viewService.FindPaymentInstrumentDuplicate(pi);
            if (duplicatePaymentInstrument == null
                || duplicatePaymentInstrument.Id == LoadArguments.PaymentInstrumentId
                || duplicatePaymentInstrument.Id == PaymentInstrument.Id)
            {
                return;
            }

            // Prevent interaction with UI
            using (new DisposableScope<IList<object>>(InteractionContext.BusyComponents,
                bc => bc.Add(this), bc => bc.Remove(this)))
            {
                // Ask user what to do
                var useExisting = _interactionManager.Confirm("{0} dated {1} for {2:C} from {3} is already in use. Continue posting from the existing check?"
                    .FormatWith(
                        duplicatePaymentInstrument.ReferenceNumber.IsNullOrEmpty()
                            ? "(no reference)"
                            : duplicatePaymentInstrument.ReferenceNumber,
                        duplicatePaymentInstrument.CheckDate == null
                            ? "(no date)"
                            : duplicatePaymentInstrument.CheckDate.ToString(),
                        duplicatePaymentInstrument.PaymentAmount,
                        duplicatePaymentInstrument.Payer), null, "Continue");
                if (!useExisting.GetValueOrDefault()) return;

                LoadArguments.PaymentInstrumentId = duplicatePaymentInstrument.Id;
                Reload(true);
            }
        }

        private void ExecuteRemoveAccount(RemoveItemArguments args)
        {
            if (!CanRemoveAccount(args)) return;

            var account = (AccountViewModel)args.Item;
            AccountsSource.Remove(account);
            AddPatientAllowed = true;
        }

        private void ExecuteDeleteTransaction(RemoveItemArguments args)
        {
            if (!CanDeleteTransaction(args)) return;

            var transaction = (FinancialTransactionViewModel)args.Item;
            if (transaction.Id.HasValue)
            {
                // Record all deletes to post them later
                _pendingTransactionRemovals.Add(transaction);
            }
        }

        private void ExecuteFilterInsurers()
        {
            if (!IsInBatchMode) return;

            // Do not suggest on empty text and until at least 3 characters are entered
            if (string.IsNullOrEmpty(InsurersSearchText) || InsurersSearchText.Trim().Length < 3)
            {
                FoundInsurers = new List<InsurerPayerViewModel>();
                return;
            }

            FoundInsurers = _viewService.SearchInsurers(InsurersSearchText).ToList();
        }

        private void ExecuteClosing(Action cancelClosing)
        {
            bool? canExit = true;

            // Prompt user to save changes if any
            if (HasChanges())
            {
                canExit = _interactionManager.Confirm("You have unsaved changes. Are you sure you want to exit?", "Cancel", "Exit");
            }

            // Cancel closing?
            if (!canExit.GetValueOrDefault(false))
            {
                cancelClosing();
                return;
            }

            // Cleanup
            _insurersSelection.Dispose();
            _patientsSelection.Dispose();
        }

        private void Reload(bool skipDataListsLoading)
        {
            // Cleanup
            if (PaymentInstrument != null)
            {
                PaymentInstrument.CastTo<INotifyPropertyChanged>().PropertyChanged -= OnPaymentInstrumentChanged;
            }
            if (AccountsSource != null)
            {
                foreach (var account in AccountsSource)
                {
                    CleanupAccountAfterDisplay(account);
                }
            }

            // Fetch data
            var loadInformation = _viewService.LoadPostAdjustmentsInformation(
                LoadArguments.PaymentInstrumentId,
                LoadArguments.TransactionOfInterest,
                LoadArguments.PatientId,
                LoadArguments.InvoiceId,
                skipDataListsLoading);

            // Prepare accounts
            var billingActions = !skipDataListsLoading ? loadInformation.DataLists.BillingActions : DataLists.BillingActions;
            foreach (var account in loadInformation.Accounts)
            {
                PrepareAccountForDisplay(account, loadInformation.PaymentInstrument, billingActions);
            }

            // Enable calculation of balance on payment instrument
            loadInformation.PaymentInstrument.SetAccountsInContext(loadInformation.Accounts);

            // Initiate editing
            loadInformation.PaymentInstrument.CastTo<IEditableObjectExtended>().InitiateCustomEdit();
            loadInformation.Accounts.InitiateCustomEdit();

            // Init default editing state
            var isNewPaymentInstrument = loadInformation.PaymentInstrument.Id == null;
            IsInBatchMode = LoadArguments.PatientId == null;
            AddPatientAllowed = IsInBatchMode
                && PermissionId.AddFinancialTransactions.PrincipalContextHasPermission();
            PostingInProgress = !isNewPaymentInstrument;

            // Bind data
            InvoiceSelectedInsurers = loadInformation.InvoiceSelectedInsurerIds;
            if (!skipDataListsLoading)
            {
                DataLists = loadInformation.DataLists;
            }
            PaymentInstrument = loadInformation.PaymentInstrument;
            AccountsSource = loadInformation.Accounts;

            // Prepare for duplicate checks
            PaymentInstrument.CastTo<INotifyPropertyChanged>().PropertyChanged += OnPaymentInstrumentChanged;

            // Preselect account when in single patient mode
            if (!IsInBatchMode || AccountsSource.Count == 1)
            {
                SelectedAccount = AccountsSource.FirstOrDefault();
            }
        }

        private void ExecuteCancelPaymentInstrumentEdit()
        {
            PaymentInstrument.CastTo<IEditableObjectExtended>().CancelCustomEdits();
            IsExistingPaymentInstrumentEditInProgress = false;
        }

        private void ExecuteSavePaymentInstrumentEdit()
        {
            if (ShowValidationWarnings(true, false)) return;

            _viewService.SavePaymentInstrument(PaymentInstrument);
            PaymentInstrument.CastTo<IEditableObjectExtended>().AcceptCustomEdits();
            IsExistingPaymentInstrumentEditInProgress = false;
        }

        private void OnTransactionsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add) return;

            e.NewItems
                .Cast<FinancialTransactionViewModel>()
                .Where(t => !t.Id.HasValue) // New only
                .ForEachWithSinglePropertyChangedNotification(t =>
                {
                    //Check either ReasonCode and Group Code is not present, make both empty
                    if (t.FinancialTransactionType != null
                        && (t.FinancialTransactionType.GroupCode == null || t.FinancialTransactionType.ReasonCode == null))
                    {
                        t.FinancialTransactionType.ReasonCode = t.FinancialTransactionType.GroupCode = null;
                    }

                    // Set source for highlighting to work properly
                    t.Source = _financialSourceTypeMapper.Map(PaymentInstrument.FinancialSourceType);

                    // Set date for "is editable" check to work properly
                    t.Date = PaymentInstrument.Date;
                });
        }

        private bool CanPost()
        {
            return HasChanges();
        }

        private bool HasChanges()
        {
            return (!CanEditPaymentInstrument && !CanEditPaymentInstrumentRound1)
                ? AccountsSource.IfNotNull(a => a.IsChanged)
                : PaymentInstrument.IsValid() && AccountsSource.IfNotNull(a => a.IsChanged);
        }

        private bool CanDeleteTransaction(RemoveItemArguments arg)
        {
            if (arg == null) return false;

            var transaction = arg.Item as FinancialTransactionViewModel;

            // Newly added transactions can always be deleted
            if (transaction == null || !transaction.Id.HasValue) return true;

            // Can delete if permissioned and not closed
            var result = PermissionId.DeleteFinancialTransactions.PrincipalContextHasPermission()
                         && (CloseDateTime == null || CloseDateTime <= transaction.Date);
            return result;
        }

        private bool CanRemoveAccount(RemoveItemArguments arg)
        {
            if (arg == null || !IsInBatchMode) return false;

            var account = arg.Item as AccountViewModel;
            if (account == null) return false;

            var hasTransactions = account.BillingServices.Any(b => b.Transactions.Any())
                || account.OnAccountTransactions.Any()
                || account.UnassignedTransactions.Any();

            return !hasTransactions;
        }

        private void OnAccountPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var account = sender as AccountViewModel;
            if (account == null
                || e.PropertyName != AccountNewPaidAmountPropertyName
                || account.OnAccountTransactions.Count < 1) return;

            var onAccountTransaction = account.OnAccountTransactions.First();

            // Recalculate only existing on-account transaction
            if (!onAccountTransaction.Id.HasValue) return;

            var previousAmount = onAccountTransaction.GetPreviousAmount();
            // New paid amount includes change of on-account transaction balance, so prevent infinite change loop
            var newPaidAmountAdjusted = account.NewPaidAmount + previousAmount - onAccountTransaction.Amount;
            // Update on-account balance according to new paid amount
            onAccountTransaction.Amount = previousAmount - newPaidAmountAdjusted;
        }

        private void OnPaymentInstrumentChanged(object sender, PropertyChangedEventArgs e)
        {
            // When changing financial source type -> reset selected payer
            if (e.PropertyName == PaymentInstrumentFinancialSourceTypeFieldName)
            {
                switch (PaymentInstrument.FinancialSourceType)
                {
                    case FinancialSourceType.Insurer:
                        PaymentInstrument.Payer = InsurersSelection.FirstOrDefault();
                        break;
                    case FinancialSourceType.Patient:
                        PaymentInstrument.Payer = PatientAndContactsSelection.FirstOrDefault();
                        break;
                    // Office
                    case FinancialSourceType.BillingOrganization:
                    case FinancialSourceType.ExternalOrganization:
                        PaymentInstrument.Payer = null;
                        PaymentInstrument.PaymentMethod = null;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // Perform duplicate check
            if (new[] 
                {
                    PaymentInstrumentCheckDateFieldName, 
                    PaymentInstrumentFinancialSourceTypeFieldName, 
                    PaymentInstrumentPayerFieldName, 
                    PaymentInstrumentPaymentAmountFieldName, 
                    PaymentInstrumentReferenceNumberFieldName
                }
                .Contains(e.PropertyName))
            {
                CheckDuplicatePaymentInstrument.Execute();
            }
        }

        private List<AccountViewModel> CollectActiveAccounts()
        {
            var result = new List<AccountViewModel>();
            foreach (var account in AccountsSource)
            {
                var modifiedBillingServices = account.BillingServices
                    .Where(es => es.CastTo<IEditableObjectExtended>().IsChanged)
                    .ToList();

                // We are interested only in accounts with modified billing services
                if (modifiedBillingServices.Count <= 0
                    && !account.OnAccountTransactions.IsChanged) continue;

                var updateTransactionsAction = new Action<IEnumerable<FinancialTransactionViewModel>, BillingServiceViewModel>((transactions, billingService) =>
                {
                    foreach (var transaction in transactions)
                    {
                        // Update association to service and invoice if it has been moved on UI
                        transaction.InvoiceId = billingService.IfNotNull(bs => (int?)bs.InvoiceId);
                        transaction.ServiceId = billingService.IfNotNull(bs => bs.Id);

                        // Update instrument-related values for new transactions which might have changed
                        if (!transaction.Id.HasValue)
                        {
                            transaction.Source = _financialSourceTypeMapper.Map(PaymentInstrument.FinancialSourceType);
                            transaction.Reference = PaymentInstrument.ReferenceNumber;
                        }
                    }
                });

                // Execute update
                updateTransactionsAction(account.OnAccountTransactions, null);
                foreach (var billingService in modifiedBillingServices)
                {
                    updateTransactionsAction(billingService.Transactions, billingService);
                }

                // Set only essential properties needed for posting
                using (new SuppressPropertyChangedScope())
                using (new SuppressChangeTrackingScope())
                {
                    var activeAccountInfo = _createAccount();
                    activeAccountInfo.PatientId = account.PatientId;
                    activeAccountInfo.PayerOverride = account.RequiresOverride
                        ? account.PayerOverride
                        : null;
                    activeAccountInfo.OnAccountTransactions = account.OnAccountTransactions;
                    activeAccountInfo.BillingServices.AddRangeWithSinglePropertyChangedNotification(modifiedBillingServices);
                    result.Add(activeAccountInfo);
                }
            }
            return result;
        }

        /// <summary>
        /// Removes on account balance once it reaches 0
        /// </summary>
        /// <param name="accountsForPosting"></param>
        private void RemoveZeroOnAccountTransactions(IEnumerable<AccountViewModel> accountsForPosting)
        {
            foreach (var account in accountsForPosting)
            {
                var zeroOnAccountTransactions = account.OnAccountTransactions.Where(t => t.Amount == 0).ToList();
                account.OnAccountTransactions.RemoveAll(zeroOnAccountTransactions);
                _pendingTransactionRemovals.AddRange(zeroOnAccountTransactions);
            }
        }

        /// <summary>
        /// When transaction type changes we need to recreate them as new ones
        /// </summary>
        /// <param name="accountsForPosting"></param>
        private void HandleTransactionTypeChanges(IEnumerable<AccountViewModel> accountsForPosting)
        {
            foreach (var modifiedTransaction in accountsForPosting
                .SelectMany(a => a.BillingServices)
                .SelectMany(bs => bs.Transactions)
                .Where(t => t.CastTo<IEditableObjectExtended>().IsChanged)
                .ToList())
            {
                var changes = modifiedTransaction
                    .CastTo<IEditableObjectExtended>()
                    .GetOriginalValuesForNamedEdit(EditableObjectExtensions.CustomEditName);
                if (changes == null) continue;

                // Financial transaction type changed to a different type?
                FinancialTransactionTypeViewModel originalType;
                if (!changes.TryGetValue(FinancialTransactionTypePropertyName, out originalType)
                    || originalType == null
                    || originalType.GetType() == modifiedTransaction.FinancialTransactionType.GetType())
                {
                    continue;
                }

                // Submit modified transaction for removal with original type
                var originalTransaction = modifiedTransaction.Clone();
                originalTransaction.FinancialTransactionType = originalType;
                _pendingTransactionRemovals.Add(originalTransaction);

                // Modified transaction with new type will be submitted as new one
                modifiedTransaction.Id = null;
            }
        }

        private void CleanupAccountAfterDisplay(AccountViewModel account)
        {
            // Unsubscribe events
            account.CastTo<INotifyPropertyChanged>().PropertyChanged -= OnAccountPropertyChanged;
            account.OnAccountTransactions.CollectionChanged -= OnTransactionsCollectionChanged;
            foreach (var encounterService in account.BillingServices)
            {
                encounterService.Transactions.CollectionChanged -= OnTransactionsCollectionChanged;
            }

            // End editing session
            var editableAccount = account.As<IEditableObjectExtended>();
            editableAccount.EndNamedEdit(EditableObjectExtensions.CustomEditName);
            editableAccount.AcceptChanges();

            // De-associate from payment instrument
            account.SetPaymentInstrumentInContext(null);
        }

        private void PrepareAccountForDisplay(AccountViewModel account, PaymentInstrumentViewModel paymentInstrument, ObservableCollection<NamedViewModel> billingActions)
        {
            // Enable functionality related to payer overrides
            account.SetPaymentInstrumentInContext(paymentInstrument);

            foreach (var encounterService in account.BillingServices)
            {
                encounterService.SetPreviousBalance();
                encounterService.InitChoices(account.NextPayers, billingActions);
            }

            // Force unassigned transactions to appear for account invoices without billing services (so that grouping occurs)
            var noServiceInvoiceIds = account.UnassignedTransactions
                .GroupBy(ut => ut.InvoiceId)
                .Where(ut => ut.Key != null)
                .Select(ut => ut.Key.Value)
                .Except(account.BillingServices
                    .GroupBy(es => es.InvoiceId)
                    .Select(es => es.Key)
                )
                .ToList();

            var emptyService = _createService();
            emptyService.Description = "No services for this invoice. Please, add them first";

            foreach (var noServiceInvoiceId in noServiceInvoiceIds)
            {
                var emptyEntry = _createBillingService();
                emptyEntry.Id = null;
                emptyEntry.InvoiceId = noServiceInvoiceId;
                emptyEntry.Service = emptyService;
                account.BillingServices.Add(emptyEntry);
            }

            // Start editing session
            account.CastTo<IEditableObjectExtended>().InitiateCustomEdit();

            // Hook to events
            account.CastTo<INotifyPropertyChanged>().PropertyChanged += OnAccountPropertyChanged;
            account.OnAccountTransactions.CollectionChanged += OnTransactionsCollectionChanged;
            foreach (var encounterService in account.BillingServices)
            {
                encounterService.Transactions.CollectionChanged += OnTransactionsCollectionChanged;
            }
        }

        private bool IsTransactionEditable(FinancialTransactionViewModel transaction, bool onAccountCheck)
        {
            // New transactions are always editable if permissioned
            if (!transaction.Id.HasValue)
            {
                return true;
            }

            return
                // Permissioned
                PermissionId.EditFinancialTransactions.PrincipalContextHasPermission()
                // Non-closed
                && (CloseDateTime == null || CloseDateTime <= transaction.Date)
                // Not existing on-account transaction (they are automatically calculated)
                && (!onAccountCheck || transaction.InvoiceReceivableId != null);
        }

        private bool ShowValidationWarnings(bool paymentInstrumentCheck = true, bool accountsCheck = true)
        {
            // Validate entry
            var piErrors = paymentInstrumentCheck
                && (CanEditPaymentInstrument || CanEditPaymentInstrumentRound1)
                ? PaymentInstrument.Validate()
                : new Dictionary<string, string>();

            var accountsErrors = accountsCheck
                ? AccountsSource.ValidateItems()
                : new Dictionary<AccountViewModel, Dictionary<string, string>>();

            // No errors?
            if (!piErrors.Any() && !accountsErrors.Any()) return false;

            var warnings = new List<string>();

            // Add payment instrument warning
            if (piErrors.Any())
            {
                var warning = piErrors.ToValidationMessageString();
                warnings.Add(warning);
            }

            // Add per-account errors
            foreach (var accountErrors in accountsErrors)
            {
                var warning = string.Format("{0} ({1}): {2}", accountErrors.Key.DisplayName,
                    accountErrors.Key.PatientId,
                    accountErrors.Value.Values.Join(Environment.NewLine));
                warnings.Add(warning);
            }

            this.Dispatcher().Invoke(() => _warningsViewManager.ShowWarnings(warnings, "Correct errors first: "));
            return true;
        }

        /// <summary>
        /// Accounts property which delays returning actual list of Accounts until posting is in progress
        /// Used to speed up initial loading of the screen
        /// </summary>
        [DependsOn("PostingInProgress", "AccountsSource")]
        public virtual ExtendedObservableCollection<AccountViewModel> Accounts
        {
            get { return PostingInProgress ? AccountsSource : new ExtendedObservableCollection<AccountViewModel>(); }
        }

        [DependsOn("AccountsSource.Count", "PaymentInstrument.Id", "FoundInsurers")]
        public virtual ObservableCollection<InsurerPayerViewModel> InsurersSelection
        {
            get
            {
                // Skip until loaded
                if (AccountsSource == null || PaymentInstrument == null) return null;

                var dependencies = PaymentInstrument.Id.CastTo<object>().CreateEnumerable()
                    .Concat(AccountsSource.SelectMany(a => a.NextPayers))
                    .Concat(FoundInsurers.CreateEnumerable())
                    .ToArray();

                return _insurersSelection.GetOrRefreshValue(() =>
                {
                    // Reset previously cached values
                    _insurersSelection.Reset();

                    // Build a list of active insurers on the selected filter date
                    var payersQuery = AccountsSource
                        .SelectMany(a => a.NextPayers)
                        .Select(np => np.Payer)
                        .OfType<InsurerPayerViewModel>();

                    // Additional insurers found? -> use them
                    if (FoundInsurers != null && FoundInsurers.Count > 0)
                    {
                        payersQuery = payersQuery.Union(FoundInsurers);
                    }

                    // Filter insurers by allowed list of insurers based on selected invoice
                    var isInvoiceFiltered = LoadArguments.InvoiceId != null && InvoiceSelectedInsurers != null;
                    if (isInvoiceFiltered)
                    {
                        payersQuery = payersQuery
                            .Where(p => InvoiceSelectedInsurers.Contains(p.Id));
                    }

                    var payers = payersQuery.ToList();
                    // Ensure currently selected payer is always in the list
                    var instrumentPayer = PaymentInstrument.Payer as InsurerPayerViewModel;
                    if (PaymentInstrument.FinancialSourceType == FinancialSourceType.Insurer
                        && instrumentPayer != null
                        && payers.All(p => p.Id != instrumentPayer.Id))
                    {
                        // Get rid of all insurers with same name and re-add selected one
                        payers.RemoveAll(p => InsurerPayerViewModel.DisplayNameBasedComparer.Equals(p, instrumentPayer));
                        payers.Add(instrumentPayer);
                    }

                    // Show only distinct insurers by display name
                    payers = payers
                        .GroupBy(p => p, InsurerPayerViewModel.DisplayNameBasedComparer)
                        .Select(g => g.First())
                        .ToList();

                    // Order properly
                    var result = (isInvoiceFiltered
                        ? payers
                            .OrderBy(p => p.As<PatientInsurancePayerViewModel>().IfNotNull(pi => pi.InsuranceType.Id, int.MaxValue))
                            .ThenBy(p => p.As<PatientInsurancePayerViewModel>().IfNotNull(pi => pi.OrdinalId, int.MaxValue))
                        : payers
                            .OrderBy(p => p.DisplayName))
                        .ToObservableCollection();

                    return result;
                }, dependencies);
            }
        }

        [DependsOn("AccountsSource.Count", "PaymentInstrument.Id")]
        public virtual ObservableCollection<PatientPayerViewModel> PatientAndContactsSelection
        {
            get
            {
                if (AccountsSource == null) return null;

                var dependencies = PaymentInstrument.Id.CastTo<object>().CreateEnumerable()
                    .Concat(AccountsSource.SelectMany(a => a.NextPayers).Select(np => (object)np.Payer))
                    .ToArray();

                return _patientsSelection.GetOrRefreshValue(() =>
                {
                    // Reset previously cached values
                    _patientsSelection.Reset();

                    var result = dependencies
                        .OfType<PatientPayerViewModel>()
                        .ToList();

                    // Ensure currently selected payer is always in the list
                    var instrumentPayer = PaymentInstrument.Payer as PatientPayerViewModel;
                    if (PaymentInstrument.FinancialSourceType == FinancialSourceType.Patient
                        && instrumentPayer != null
                        && result.All(p => p.Id != instrumentPayer.Id))
                    {
                        result.Add(instrumentPayer);
                    }

                    return result
                        .OrderBy(p => p.Name)
                        .ToObservableCollection();
                }, dependencies);
            }
        }

        [DependsOn("PaymentInstrument.FinancialSourceType")]
        public virtual ObservableCollection<FinancialTransactionTypeViewModel> OnAccountFinancialTransactionTypesSelection
        {
            get
            {
                // No payment type when office is selected
                var notOffice = PaymentInstrument.IfNotNull(pi => pi.FinancialSourceType != FinancialSourceType.BillingOrganization);

                // On account transaction can be only of Payment adjustment type
                return DataLists
                    .IfNotNull(dl => dl.FinancialTransactionTypes
                        .OfType<AdjustmentTypeViewModel>()
                        .Where(ftt => ftt.Id == (int)AdjustmentTypeId.Payment && notOffice)
                        .Cast<FinancialTransactionTypeViewModel>()
                        .ToObservableCollection());
            }
        }

        [DependsOn("PaymentInstrument.FinancialSourceType")]
        public virtual ObservableCollection<FinancialTransactionTypeViewModel> FinancialTransactionTypesSelection
        {
            get
            {
                // No payment type when office is selected
                var isOffice = PaymentInstrument.IfNotNull(pi => pi.FinancialSourceType == FinancialSourceType.BillingOrganization);

                var types = DataLists.FinancialTransactionTypes
                    // Put adjustments first
                    .OrderByDescending(p => p is AdjustmentTypeViewModel)
                    .ThenBy(p => p is FinancialInformationTypeViewModel)
                    // Then order by name
                    .ThenBy(p => p.OrdinalId)
                    .ToObservableCollection();

                // Filter out payment type when office is selected
                if (isOffice)
                {
                    var paymentType = types
                        .OfType<AdjustmentTypeViewModel>()
                        .FirstOrDefault(ftt => ftt.Id == (int)AdjustmentTypeId.Payment);

                    if (paymentType != null)
                    {
                        types.Remove(paymentType);
                    }
                }

                return types;
            }
        }

        /// <summary>
        /// Whether editing of payment instrument is allowed
        /// </summary>
        [DependsOn("PaymentInstrument.Id")]
        public virtual bool CanEditPaymentInstrument
        {
            get { return PaymentInstrument.IfNotNull(pi => pi.Id == null); }
        }

        /// <summary>
        /// Whether editing of payment instrument is allowed (for Round 1 limited to Payment Method, Amount and Check code)
        /// NOTE: Temporary property until full edit is allowed. Merge with above one once supported
        /// </summary>
        [DependsOn("CanEditPaymentInstrument", "IsExistingPaymentInstrumentEditInProgress")]
        public virtual bool CanEditPaymentInstrumentRound1
        {
            get { return CanEditPaymentInstrument || IsExistingPaymentInstrumentEditInProgress; }
        }

        public PostAdjustmentsLoadArguments LoadArguments { get; set; }

        /// <summary>
        /// Accounts loaded for this view
        /// </summary>
        public virtual ExtendedObservableCollection<AccountViewModel> AccountsSource { get; set; }

        public virtual DataListsViewModel DataLists { get; set; }

        public virtual PaymentInstrumentViewModel PaymentInstrument { get; set; }

        public virtual List<int> InvoiceSelectedInsurers { get; set; }

        public virtual string InsurersSearchText { get; set; }

        /// <summary>
        /// Insurers found in database according to entered <see cref="InsurersSearchText"/>
        /// </summary>
        public virtual List<InsurerPayerViewModel> FoundInsurers { get; set; }

        /// <summary>
        /// Currently selected account on the UI
        /// </summary>
        public virtual AccountViewModel SelectedAccount { get; set; }

        /// <summary>
        /// Specifies whether edit enabled for existing payment instrument
        /// </summary>
        public virtual bool IsExistingPaymentInstrumentEditInProgress { get; set; }

        /// <summary>
        /// Whether the screen is in batch mode
        /// </summary>
        public virtual bool IsInBatchMode { get; set; }

        /// <summary>
        /// Whether posting has started (either user clicked "Start posting" or editing existing payment instrument transactions)
        /// </summary>
        public virtual bool PostingInProgress { get; set; }

        /// <summary>
        /// Indicates whether newly added account should be activated.
        /// </summary>
        public virtual bool ShouldActivateAddedAccount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether view is in searching mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if searching mode; otherwise, <c>false</c>.
        /// </value>
        public virtual bool AddPatientAllowed { get; set; }

        /// <summary>
        /// Gets or sets the invoice filters.
        /// </summary>
        public virtual List<FilterSelectionViewModel> InvoiceServicesFilters { get; set; }

        [DispatcherThread]
        public virtual bool AlertsTrigger { get; set; }

        public virtual string PatientSearchText { get; set; }

        public virtual DateTime? CloseDateTime { get; set; }

        public virtual Func<object, object> CheckCanEditTransaction { get; set; }

        public virtual ICommand OpenPatientProfile { get; set; }

        public virtual ICommand RemoveAccount { get; set; }

        public virtual ICommand LoadPatientPhoto { get; set; }

        public virtual ICommand AddPatient { get; set; }

        public virtual ICommand Load { get; set; }

        public virtual ICommand Post { get; set; }

        public virtual ICommand PostAndNext { get; set; }

        public virtual ICommand Close { get; set; }

        public virtual ICommand Closing { get; set; }

        public virtual ICommand Suggest { get; set; }

        public virtual ICommand StartPosting { get; set; }

        public virtual ICommand CheckDuplicatePaymentInstrument { get; set; }

        public virtual ICommand DeleteTransaction { get; set; }

        public virtual ICommand ShowAlertScreen { get; protected set; }

        public virtual ICommand FilterInsurers { get; protected set; }

        public virtual ICommand CancelPaymentInstrumentEdit { get; protected set; }

        public virtual ICommand SavePaymentInstrumentEdit { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        public ICommonViewFeatureExchange ViewFeatureExchange { get; private set; }
    }
}
