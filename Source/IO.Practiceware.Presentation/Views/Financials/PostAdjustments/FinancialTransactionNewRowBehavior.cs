﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using FocusNavigationDirection = System.Windows.Input.FocusNavigationDirection;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    /// <summary>
    /// Adds new transactions to the list of all transactions once required fields are filled in
    /// </summary>
    public class FinancialTransactionNewRowBehavior : Behavior<RadGridView>
    {
        public NewFinancialTransactionContext NewFinancialTransactionContext
        {
            get { return (NewFinancialTransactionContext)GetValue(NewFinancialTransactionContextProperty); }
            set { SetValue(NewFinancialTransactionContextProperty, value); }
        }

        public static readonly DependencyProperty NewFinancialTransactionContextProperty =
            DependencyProperty.Register("NewFinancialTransactionContext", typeof(NewFinancialTransactionContext), typeof(FinancialTransactionNewRowBehavior), new PropertyMetadata());

        private GridViewNewRow _newRow;

        protected override void OnAttached()
        {
            base.OnAttached();

            // Do not allow adding new rows without permissions
            if (!PermissionId.AddFinancialTransactions.PrincipalContextHasPermission())
            {
                AssociatedObject.NewRowPosition = GridViewNewRowPosition.None;
            }
            else
            {
                // Watch over layout updates until NewRow element shows up
                AssociatedObject.LayoutUpdated += WeakDelegate.MakeWeak<EventHandler>(OnLayoutUpdated);
                OnLayoutUpdated(AssociatedObject, EventArgs.Empty);
            }
        }

        private void OnLayoutUpdated(object sender, EventArgs e)
        {
            _newRow = AssociatedObject.FindChildByType<GridViewNewRow>();
            if (_newRow == null) return;

            AssociatedObject.LayoutUpdated -= WeakDelegate.MakeWeak<EventHandler>(OnLayoutUpdated);

            _newRow.PreviewKeyDown += OnPreviewKeyDown;
            _newRow.IsKeyboardFocusWithinChanged += OnKeyboardFocusWithinChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.LayoutUpdated -= WeakDelegate.MakeWeak<EventHandler>(OnLayoutUpdated);
            if (_newRow == null) return;

            _newRow.PreviewKeyDown -= OnPreviewKeyDown;
            _newRow.IsKeyboardFocusWithinChanged -= OnKeyboardFocusWithinChanged;
        }

        private void OnKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //Only run this method when NewValue is true, i.e.,when KeyboardFocusWithinChanged has changed IsKeyboardFocused to true
            if (e.NewValue != null && (bool)e.NewValue == true) return;
            var element = sender as UIElement;

            var transactionContext = NewFinancialTransactionContext;
            if (transactionContext == null) return;

            //Fix for the case where clicking on empty space in the new row runs ClearTransactionData
            //Get the object with focus, get whether it's a child of the GridViewNewRow (in this case, always the sender).
            //and send keyboard focus back to one of the controls (as if the user hadn't clicked on the empty space)
            if (Keyboard.FocusedElement != null)
            {
                var focusedElement = Keyboard.FocusedElement as UIElement;
                if (element != null && (focusedElement != null && focusedElement.IsAncestorOf(element)))
                {
                    element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Left));
                    return;
                }
            }
            if (!ProcessNewTransactionEdits(transactionContext))
                ClearTransactionData(transactionContext);
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var transaction = NewFinancialTransactionContext;
            if (transaction == null) return;

            //check the keyEventArg and if it's enter or a forward tab out of the last control, validate and add the new object
            if ((e.Key.Equals(Key.Enter) || (IsLastControlFocused() && e.Key.Equals(Key.Tab))) && !Keyboard.Modifiers.Equals(ModifierKeys.Shift))
            {
                ProcessNewTransactionEdits(transaction, amountText =>
                                                        {
                                                            //If it was enter, after we add the item, move to the allowed box
                                                            if (e.Key.Equals(Key.Enter) && !Keyboard.Modifiers.Equals(ModifierKeys.Shift))
                                                            {

                                                                if (amountText != null) amountText.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));

                                                                e.Handled = true;
                                                            }

                                                            //If it was a forward tab out of the last control, move focus back to the combobox to add another row
                                                            if (e.Key.Equals(Key.Tab) && !Keyboard.Modifiers.Equals(ModifierKeys.Shift))
                                                            {
                                                                var nextControl = _newRow.FindChildByType<RadAutoCompleteBox>();
                                                                if (nextControl != null)
                                                                {
                                                                    Keyboard.Focus(nextControl);
                                                                    e.Handled = true;
                                                                }
                                                            }
                                                        });
            }
        }

        private bool IsLastControlFocused()
        {
            Control current = null;
            foreach (Control c in _newRow.GetChildren<Control>().Where(c => c.IsKeyboardFocusWithin))
            {
                current = c;
            }

            if (current == null) return false;
                    var next = current.PredictFocus(FocusNavigationDirection.Right) as Control;
            //first get out of the current control (telerik controls have controls inside controls which can be tab stops)
            while (current != null && next != null && current.IsAncestorOf(next))
            {
                current = current.PredictFocus(FocusNavigationDirection.Right) as Control;
                next = next.PredictFocus(FocusNavigationDirection.Right) as Control;
            }

            //return whether the next control is still within the associated object
            return (next == null || !AssociatedObject.IsAncestorOf(next));
        }

        private bool ProcessNewTransactionEdits(NewFinancialTransactionContext context, Action<RadWatermarkTextBox> afterAddedToItemsSource = null)
        {
            var element = _newRow.FindChildByType<FrameworkElement>();
            if (element == null) return false;

            var transactionAdded = false;
            var amountText = element.FindName("AmountTextBox") as RadWatermarkTextBox;

            //check that the new transaction should be added, then add it
            if ((amountText != null && !(amountText.CurrentText.IsNullOrWhiteSpace()))
                && context.Transaction.FinancialTransactionType != null
                && context.Transaction.FinancialTransactionType.Id > 0)
            {
                transactionAdded = true;
                //Check the amount, make sure it's can be and is parsed before we send it through
                if (context.Transaction.Amount.ToString() != amountText.CurrentText)
                {
                    decimal d;
                    if (!decimal.TryParse(amountText.CurrentText, out d))
                        return false;
                    context.Transaction.Amount = d;
                }
                // Add it
                AssociatedObject.ItemsSource.CastTo<IList<FinancialTransactionViewModel>>().Add(context.Transaction);

                //Clear the current amount text
                amountText.Text = String.Empty;

                if (afterAddedToItemsSource != null)
                {
                    afterAddedToItemsSource(amountText);
                }
            }
            return transactionAdded;
        }

        private void ClearTransactionData(NewFinancialTransactionContext context)
        {
            context.Transaction.Amount = (decimal)0.00;
            context.Transaction.IncludeCommentOnStatement = false;
            context.Transaction.Comment = String.Empty;
            context.Transaction.FinancialTransactionType = context.PreselectFirstTransactionType 
                ? context.TransactionTypes.FirstOrDefault() 
                : null;
        }
    }
}
