﻿using System.Windows;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    public class PostAdjustmentsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public PostAdjustmentsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPostPaymentsAndAdjustments()
        {
            ShowPostPaymentsAndAdjustments(new PostAdjustmentsLoadArguments());
        }

        public void ShowPostPaymentsAndAdjustments(PostAdjustmentsLoadArguments loadArguments)
        {
            // User must be able to add transactions in order to open screen to post new batch
            if (loadArguments.PaymentInstrumentId == null 
                && loadArguments.TransactionOfInterest == null
                && !PermissionId.AddFinancialTransactions.EnsurePermission())
            {
                return;
            }

            var view = new PostAdjustmentsView();
            var dataContext = view.DataContext.EnsureType<PostAdjustmentsViewContext>();
            dataContext.LoadArguments = loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view, 
                    WindowState = WindowState.Normal, 
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });
        }
    }
}