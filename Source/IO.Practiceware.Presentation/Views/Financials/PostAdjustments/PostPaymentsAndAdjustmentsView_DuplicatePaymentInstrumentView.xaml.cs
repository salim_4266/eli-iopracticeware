﻿namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    /// <summary>
    /// Interaction logic for PostPaymentsAndAdjustmentsView_DuplicatePaymentInstrumentView.xaml
    /// </summary>
    public partial class PostPaymentsAndAdjustmentsViewDuplicatePaymentInstrumentView
    {
        public PostPaymentsAndAdjustmentsViewDuplicatePaymentInstrumentView()
        {
            InitializeComponent();
        }
    }
}
