﻿using System.Collections.ObjectModel;
using System.Windows;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;

namespace IO.Practiceware.Presentation.Views.Financials.PostAdjustments
{
    public class NewFinancialTransactionContext : Freezable
    {
        public FinancialTransactionViewModel Transaction
        {
            get { return (FinancialTransactionViewModel)GetValue(TransactionProperty); }
            set { SetValue(TransactionProperty, value); }
        }

        public ObservableCollection<FinancialTransactionTypeViewModel> TransactionTypes
        {
            get { return (ObservableCollection<FinancialTransactionTypeViewModel>)GetValue(TransactionTypesProperty); }
            set { SetValue(TransactionTypesProperty, value); }
        }

        public bool PreselectFirstTransactionType
        {
            get { return (bool)GetValue(PreselectFirstTransactionTypeProperty); }
            set { SetValue(PreselectFirstTransactionTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PreselectFirstTransactionType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreselectFirstTransactionTypeProperty =
            DependencyProperty.Register("PreselectFirstTransactionType", typeof(bool), typeof(NewFinancialTransactionContext), new PropertyMetadata(false));

        

        // Using a DependencyProperty as the backing store for TransactionTypes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransactionTypesProperty =
            DependencyProperty.Register("TransactionTypes", typeof(ObservableCollection<FinancialTransactionTypeViewModel>), typeof(NewFinancialTransactionContext), new PropertyMetadata());
        

        // Using a DependencyProperty as the backing store for Transaction.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransactionProperty =
            DependencyProperty.Register("Transaction", typeof(FinancialTransactionViewModel), typeof(NewFinancialTransactionContext), new PropertyMetadata());

        protected override Freezable CreateInstanceCore()
        {
            return new NewFinancialTransactionContext();
        }
    }
}