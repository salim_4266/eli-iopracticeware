﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.Views.Alerts;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Documents;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.Views.PatientReferralPopup;
using IO.Practiceware.Presentation.ViewServices.Alerts;
using IO.Practiceware.Presentation.ViewServices.Financials.EditInvoice;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.Utilities;
using IO.Practiceware.Storage;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Input;
using Telerik.Windows.Controls;
using EncounterServiceViewModel = IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice.EncounterServiceViewModel;
using InvoiceViewModel = IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice.InvoiceViewModel;

namespace IO.Practiceware.Presentation.Views.Financials.EditInvoice
{
    public class EditInvoiceViewContext : IViewContext, ICommonViewFeatureHandler
    {
        private readonly IEditInvoiceViewService _editInvoiceViewService;
        private readonly Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> _createPatientInsuranceAuthorizationPopupViewContext;
        private readonly Func<EncounterServiceViewModel> _createEncounterServiceViewModel;
        private readonly Func<DiagnosisCodeViewModel> _createDiagnosisCodeViewModel;
        private IMapper<InvoiceViewModel, DiagnosisViewModel> _diagnosisMapper;
        private IMapper<InvoiceViewModel, DiagnosisLinkPointerViewModel> _diagnosisLinkPointerMapper;
        private ObservableCollection<DiagnosisCodeViewModel> _icd9Diagnosis;
        private ObservableCollection<DiagnosisCodeViewModel> _icd10Diagnosis;
        private ObservableCollection<DiagnosisCodeViewModel> _icd10FullDiagnosis;
        private bool _stopExitingWindow;
        private bool _showUnbilledPopUp;
        private bool _showNeverBilledPopUp;
        private readonly AlertViewManager _alertViewManager;
        private readonly IAlertViewService _alertViewService;
        private readonly IInteractionManager _interactionManager;
        private readonly DocumentViewManager _documentViewManger;
        private readonly ConfirmationDialogViewManager _confirmationDialogViewManager;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        public ICommonViewFeatureExchange ViewFeatureExchange { get; private set; }
        public ICommand Load { get; protected set; }
        public ICommand AddDiagnosis { get; protected set; }
        public ICommand DeleteDiganosis { get; protected set; }
        public ICommand DeleteService { get; protected set; }
        public ICommand MoveDiagnosisRight { get; protected set; }
        public ICommand MoveDiagnosisLeft { get; protected set; }
        public ICommand AddNewRow { get; protected set; }
        public ICommand SaveInvoice { get; protected set; }
        public ICommand Billing { get; protected set; }
        public ICommand NextPayerRowSelected { get; protected set; }
        public ICommand CancelExitWithUnsavedChanges { get; protected set; }
        public ICommand OnPatientInsuranceAuthorizationPopUpclick { get; protected set; }
        public ICommand OnPatientInsuranceAuthorizationConfirmOk { get; protected set; }
        public ICommand SetDefaultUnitCharge { get; protected set; }
        public ICommand DeleteEncounterService { get; protected set; }
        public ICommand EnsureUnitChargeSet { get; protected set; }
        public ICommand OpenClinicalData { get; protected set; }
        public ICommand Close { get; protected set; }
        public ICommand DiagnosisTypeChange { get; protected set; }
        public virtual ICommand ShowAlertScreen { get; protected set; }
        public ICommand FilterDiagnosis { get; protected set; }
        private int _diagnosisTypeId;
        private bool isReloading = false;
        public delegate void AsyncGetDiagonsis();

        const int loadingModelId = -100;

        public ICommand AttachDescription { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual bool ValidationWarningTrigger { get; set; }

        [DispatcherThread]
        public virtual string ValidationMessage { get; set; }

        public virtual bool ConfirmMessageTrigger { get; set; }

        [DispatcherThread]
        public virtual bool PrintClaim { get; set; }

        [DispatcherThread]
        public virtual bool AlertsTrigger { get; set; }

        [DispatcherThread]
        public virtual bool CodeSetEnabled { get; set; }

        [DispatcherThread]
        [Dependency]
        [Expression("ValidInvoiceType || IsValidValuesSelected", ErrorMessage = "Value should be from valid list of data")]
        public virtual EditInvoiceViewModel EditInvoice { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel ClaimFrequencyTypeCode { get; set; }

        public virtual bool IsValidValuesSelected
        {
            get
            {
                if (EditInvoice.DataLists != null && ((!EditInvoice.DataLists.RenderingProviders.Contains(EditInvoice.Invoice.CareTeam.RenderingProvider)) ||
                    !EditInvoice.DataLists.AttributeToLocations.Contains(EditInvoice.Invoice.AttributeTo) ||
                    !EditInvoice.DataLists.ServiceLocations.Contains(EditInvoice.Invoice.ServiceLocation)))
                {
                    return false;
                }
                return true;
            }
        }


        [DispatcherThread, DependsOn("EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id")]
        public virtual bool IsRelatedCauseAutoAccident
        {
            get
            {
                return GetRelatedCauseId(RelatedCause.AutoAccident);
            }
            set
            {
                SetRelatedCauseId(value, RelatedCause.AutoAccident);
            }
        }


        [DispatcherThread, DependsOn("EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id")]
        public virtual bool IsRelatedCauseEmployment
        {
            get
            {
                return GetRelatedCauseId(RelatedCause.Employment);
            }
            set
            {
                SetRelatedCauseId(value, RelatedCause.Employment);
            }
        }


        [DispatcherThread, DependsOn("EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id", "EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id")]
        public virtual bool IsRelatedCauseOtherAccident
        {
            get
            {
                return GetRelatedCauseId(RelatedCause.OtherAccident);
            }
            set
            {
                SetRelatedCauseId(value, RelatedCause.OtherAccident);
            }
        }


        private void SetRelatedCauseId(bool value, RelatedCause relatedCause)
        {
            if (!value)
            {
                if (EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.Value == (int)relatedCause)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id = null;
                }
                else if (EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.Value == (int)relatedCause)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id = null;
                }
                else if (EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.Value == (int)relatedCause)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id = null;
                }
            }
            else
            {
                if (!EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.HasValue)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id = (int)relatedCause;
                }
                else if (!EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.HasValue)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id = (int)relatedCause;
                }
                else if (!EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.HasValue)
                {
                    EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id = (int)relatedCause;
                }

            }
            SwitchRelatedCauseIds();
        }

        private bool GetRelatedCauseId(RelatedCause relatedCause)
        {
            return EditInvoice.Invoice.InvoiceSupplemental != null && ((EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.Value == (int)relatedCause)
                       || (EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.Value == (int)relatedCause)
                       || (EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.Value == (int)relatedCause));
        }

        private void SwitchRelatedCauseIds()
        {
            if (!EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.HasValue)
            {
                EditInvoice.Invoice.InvoiceSupplemental.RelatedCause1Id = EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id;
                EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id = null;
            }
            if (!EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id.HasValue && EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id.HasValue)
            {
                EditInvoice.Invoice.InvoiceSupplemental.RelatedCause2Id = EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id;
                EditInvoice.Invoice.InvoiceSupplemental.RelatedCause3Id = null;
            }
        }

        [DispatcherThread, DependsOn("IsRelatedCauseAutoAccident", "IsRelatedCauseEmployment", "IsRelatedCauseOtherAccident")]
        public virtual bool HasAnyRelatedCauses
        {
            get
            {
                return IsRelatedCauseAutoAccident || IsRelatedCauseEmployment || IsRelatedCauseOtherAccident;
            }
        }

        public virtual bool ValidInvoiceType
        {
            get
            {
                if (EditInvoice.DataLists != null && !EditInvoice.DataLists.InvoiceTypes.Contains(EditInvoice.Invoice.InvoiceType))
                {
                    return false;
                }
                return true;
            }
        }

        [Dependency]
        public virtual NextPayerViewModel NextPayer { get; set; }

        [DispatcherThread]
        [Dependency]
        public virtual EncounterServiceViewModel NewEncounterService { get; set; }

        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext PatientInsuranceAuthorizationPopupViewContext { get; set; }

        [Dependency, DispatcherThread]
        public virtual PatientReferralPopupViewContext Referral { get; set; }

        public virtual EditInvoiceLoadArguments LoadArguments { get; set; }


        [DispatcherThread]
        public virtual IEnumerable<NamedViewModel> DiagnosisCodeSet { get; set; }

        [DispatcherThread]
        public virtual bool IsIcd10DiagnosisTypeSelected { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel SelectedDiagnosisCodeSet { get; set; }

        public EditInvoiceViewContext(IEditInvoiceViewService editInvoiceViewService,
            Func<PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupViewContext> createPatientInsuranceAuthorizationPopupViewContext,
            Func<EncounterServiceViewModel> createEncounterServiceViewModel, AlertViewManager alertViewManager, ICommonViewFeatureExchange exchange,
            IAlertViewService alertViewService, IInteractionManager interactionManager, DocumentViewManager documentViewManger, ConfirmationDialogViewManager confirmationDialogViewManager,
            PatientInfoViewManager patientInfoViewManager, Func<DiagnosisCodeViewModel> createDiagnosisCodeViewModel)
        {
            _editInvoiceViewService = editInvoiceViewService;
            _createPatientInsuranceAuthorizationPopupViewContext = createPatientInsuranceAuthorizationPopupViewContext;
            Init();
            _createEncounterServiceViewModel = createEncounterServiceViewModel;
            _alertViewManager = alertViewManager;
            Load = Command.Create<bool>(x => ExecuteLoad()).Async(() => InteractionContext);
            AddDiagnosis = Command.Create(ExecuteAddDiagnosis);
            FilterDiagnosis = Command.Create<string>(ExecuteFilterDiagnoses);
            DeleteDiganosis = Command.Create<DiagnosisViewModel>(ExecuteDeleteDiagnosis);
            DeleteService = Command.Create<EncounterServiceViewModel>(ExecuteDeleteService);
            AddNewRow = Command.Create(ExecuteAddNewRow).Async(() => InteractionContext);
            MoveDiagnosisRight = Command.Create<DiagnosisViewModel>(ExecuteMoveRowRight);
            MoveDiagnosisLeft = Command.Create<DiagnosisViewModel>(ExecuteMoveRowLeft);
            SaveInvoice = Command.Create(() => ExecuteSave()).Async(() => InteractionContext);
            Billing = Command.Create(ExecuteBill).Async(() => InteractionContext);
            CancelExitWithUnsavedChanges = Command.Create(ExecuteCancelExitWithUnsavedChanges);
            NextPayerRowSelected = Command.Create<NextPayerViewModel>(ExecuteNextPayerRowSelected).Async(() => InteractionContext);
            OnPatientInsuranceAuthorizationPopUpclick = Command.Create(ExecutePatientInsuranceAuthorizationClick).Async(() => InteractionContext);
            OnPatientInsuranceAuthorizationConfirmOk = Command.Create(ExecuteOnPatientInsuranceAuthorizationConfirmOk).Async(() => InteractionContext);
            SetDefaultUnitCharge = Command.Create<EncounterServiceViewModel>(ExecuteEnsureUnitChargeAndUpdateEncounterServiceValues).Async(() => InteractionContext);
            EnsureUnitChargeSet = Command.Create<EncounterServiceViewModel>(ExecuteEnsureUnitChargeAndUpdateEncounterServiceValues).Async(() => InteractionContext);
            OpenClinicalData = Command.Create(ExecuteOpenClinicalData, () => EditInvoice.Invoice != null && EditInvoice.Invoice.EncounterId.HasValue);
            Close = Command.Create(() => InteractionContext.Complete(true));
            DeleteEncounterService = Command.Create<EncounterServiceViewModel>(ExecuteDeleteEncounterService);
            ShowAlertScreen = Command.Create(() => ExecuteShowAlertScreen(true)).Async(() => InteractionContext);
            AttachDescription = Command.Create<DiagnosisViewModel>(ExecuteAttachDescription).Async(() => InteractionContext);
            DiagnosisTypeChange = Command.Create(ExecuteDiagnosisTypeChange).Async(() => InteractionContext);
            ViewFeatureExchange = exchange;
            _alertViewService = alertViewService;
            _interactionManager = interactionManager;
            _documentViewManger = documentViewManger;
            _confirmationDialogViewManager = confirmationDialogViewManager;
            _patientInfoViewManager = patientInfoViewManager;
            ViewFeatureExchange.ShowPendingAlert = ShowAlertScreen.Execute;
            _diagnosisTypeId = int.Parse(ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).Value);
            _createDiagnosisCodeViewModel = createDiagnosisCodeViewModel;
        }

        private void ExecuteAttachDescription(DiagnosisViewModel diagnosisViewModel)
        {
            if (diagnosisViewModel.ActiveDiagnosisCode != null && diagnosisViewModel.ActiveDiagnosisCode.Description.IsNullOrEmpty())
            {
                diagnosisViewModel.ActiveDiagnosisCode = _editInvoiceViewService.FetchDiagnosisDescription(diagnosisViewModel);
            }
        }

        private void ExecuteOnPatientInsuranceAuthorizationConfirmOk()
        {
            if (ExecuteSave())
            {
                IntializePatientInsuranceAuthorizationScreen(true);
            }
        }

        private void ExecuteShowAlertScreen(bool showNoAlertPopup)
        {
            var loadArgument = new NoteLoadArguments();
            loadArgument.AlertScreen = AlertScreen.EditAndBill;
            loadArgument.NoteType = NoteType.InvoiceComment;
            loadArgument.EntityId = EditInvoice.Invoice.Id;

            if (!loadArgument.EntityId.HasValue)
            {
                return;
            }

            if (_alertViewService.HasAlerts(loadArgument))
            {
                this.Dispatcher().Invoke(() => _alertViewManager.ShowAlerts(loadArgument));
                ViewFeatureExchange.HasPendingAlert = _alertViewService.HasAlerts(loadArgument);
            }
            else
            {
                AlertsTrigger = showNoAlertPopup;
            }
        }


        private void ExecutePatientInsuranceAuthorizationClick()
        {
            if (!EditInvoice.Invoice.EncounterId.HasValue)
            {
                ConfirmMessageTrigger = true;
            }
        }



        private void ExecuteEnsureUnitChargeAndUpdateEncounterServiceValues(EncounterServiceViewModel encounterService)
        {
            if (encounterService == null
                || encounterService.Service == null) return;

            // We want to essentially reset this billing service - so reset the values we can load from EncounterService, and clear out the rest
            // we'll take care of when this is fired (IsEnabled on trigger) in xaml, rather than relying on any of the data we pass in from the current service.

            var payerId = EditInvoice.Invoice.NextPayers != null && EditInvoice.Invoice.NextPayers.Count > 0 ? EditInvoice.Invoice.NextPayers.FirstOrDefault().Payer.Id : 0;  
            string AdjustmentAllowed = string.Empty;
            AdjustmentAllowed = payerId > 0 ? _editInvoiceViewService.GetAdjustmentRate(payerId, encounterService.Service.Code) : string.Empty;                     
            //encounterService.Charge = encounterService.Service.UnitFee;
            decimal chargefromPracticeCode = _editInvoiceViewService.GetServiceCharge(payerId, encounterService.Service.Code);
            encounterService.Charge = chargefromPracticeCode > 0 ? chargefromPracticeCode : encounterService.Service.UnitFee;
            var details = LoadServiceDetails(encounterService);
            encounterService.NdcCode = details.NdcCode;
            encounterService.Units = details.Units;
            encounterService.Allowed = AdjustmentAllowed.IsNotNullOrEmpty() ?Convert.ToDecimal(AdjustmentAllowed) : encounterService.Service.UnitFee;
            encounterService.UnclassifiedServiceDescription = details.UnclassifiedServiceDescription;
            encounterService.LinkedDiagnoses.RemoveAll();
            encounterService.Modifiers.RemoveAll();
        }

        private void ExecuteOpenClinicalData()
        {
            if (EditInvoice.Invoice.EncounterId != null)
            {
                _patientInfoViewManager.ShowPatientClinicalData(EditInvoice.Invoice.Patient.Id, EditInvoice.Invoice.EncounterId.Value);
            }
        }

        private void ExecuteDeleteEncounterService(EncounterServiceViewModel encounterService)
        {
            // Check for null so you don't accidentally delete a service if someone is just tabbing through everything
            if (encounterService.Service == null)
            {
                EditInvoice.Invoice.EncounterServices.Remove(encounterService);
            }
        }

        //Set the billing services action based on Next payer grid selection.
        private void ExecuteNextPayerRowSelected(NextPayerViewModel obj)
        {
            var nextPayer = obj.Payer as PatientInsurancePayerViewModel;
            foreach (var encounterService in EditInvoice.Invoice.EncounterServices)
            {
                if (encounterService.Balance > 0 || (encounterService.Service.IsZeroChargeAllowedOnClaim && (nextPayer != null && nextPayer.SendZeroCharge)))
                {
                    encounterService.NextPayer = obj;
                    NextPayer = obj;
                }
            }
        }

        private void ExecuteCancelExitWithUnsavedChanges()
        {
            _stopExitingWindow = true;
        }

        //Initialize
        private void Init()
        {
            _diagnosisLinkPointerMapper = Mapper.Factory.CreateMapper<InvoiceViewModel, DiagnosisLinkPointerViewModel>(invoice => new DiagnosisLinkPointerViewModel
            {
                PointerLetter = DiagnosisLetters[invoice.EncounterDiagnoses.Count],
                DiagnosisColor = DiagnosisColors[invoice.EncounterDiagnoses.Count]
            });
            _diagnosisMapper = Mapper.Factory.CreateMapper<InvoiceViewModel, DiagnosisViewModel>(invoice => new DiagnosisViewModel
            {
                DiagnosisLinkPointer = _diagnosisLinkPointerMapper.Map(invoice),
                ActiveDiagnosisCodes = EditInvoice.DataLists.Diagnoses
            });
        }
        private void ExecuteLoad(bool initialLoadOnly = true)
        {
            //DiagnosisCodeSet = Enums.GetValues(typeof(DiagnosisTypeId)).Select(x => new NamedViewModel { Id = (int)x, Name = x.ToString() }).ToArray();
            if (EditInvoice != null && EditInvoice.DataLists != null)
            {
                var sourceEncounterServices = EditInvoice.Invoice.EncounterServices;
                InvoiceViewModel tempInvoice = _editInvoiceViewService.LoadInfo(EditInvoice.Invoice.Id, LoadArguments.PatientId, _diagnosisTypeId, false).Invoice;
                EditInvoice.Invoice = tempInvoice;
                tempInvoice = null;
                SyncActionType(sourceEncounterServices, EditInvoice.Invoice);
            }
            else
            {
                EditInvoiceViewModel tempEditInvoice = _editInvoiceViewService.LoadInfo(LoadArguments.InvoiceId, LoadArguments.PatientId, _diagnosisTypeId);
                EditInvoice = tempEditInvoice;
                tempEditInvoice = null;
            }

            //SelectedDiagnosisCodeSet = EditInvoice.Invoice.CodesetType;

            //BugID: 19858 Check the priority order of Edit Claim Request on 04/11/2015

            if (EditInvoice.Invoice.CodesetType.Id == 2)
            {
                IsIcd10DiagnosisTypeSelected = true;
            }
            else if (EditInvoice.Invoice.CodesetType.Id == 1)
            {
                IsIcd10DiagnosisTypeSelected = false;
            }
            else
                IsIcd10DiagnosisTypeSelected = _editInvoiceViewService.IsDefaultDiagnosisTypeIcd10(LoadArguments.PatientId.Value);

            using (new SuppressPropertyChangedScope())
            {
                // synchronize objects in the collection so they are reference equals
                EditInvoice.Invoice.EncounterDiagnoses.ForEach(ed => EditInvoice.Invoice.EncounterServices.ForEach(es => es.LinkedDiagnoses.Replace(ed.DiagnosisLinkPointer, ed.DiagnosisLinkPointer)));
            }
           
            PatientInsuranceAuthorizationPopupViewContext = _createPatientInsuranceAuthorizationPopupViewContext();
            //No need of patient Authorization and Referrals when no appointment
            Referral.AppointmentStartDateTime = EditInvoice.Invoice.EncounterId.HasValue ? EditInvoice.Invoice.AppointmentStartDateTime : EditInvoice.Invoice.Date;
            Referral.PatientId = LoadArguments.PatientId.HasValue ? LoadArguments.PatientId : EditInvoice.Invoice.Patient.Id;
            Referral.SaveOnNewReferrals = true;
            GetExistingReferralInformation();
            IntializePatientInsuranceAuthorizationScreen(false);
            EditInvoice.Invoice.As<IEditableObjectExtended>().InitiateCustomEdit();

            // Begin loading external providers & diagnoses in background
            System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(LoadExternalProviders);           
                if (!IsIcd10DiagnosisTypeSelected)
                {
                    System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(LoadIcd9Diagnoses);
                }
                else
                {
                    CodeSetEnabled = false;
                    AsyncGetDiagonsis objInvoiceServices = new AsyncGetDiagonsis(AsyncGetFullDiagnosisCodes);
                    IAsyncResult tag = objInvoiceServices.BeginInvoke(new AsyncCallback(DiagnosisInfoLoaded), null);
                }            
            if (initialLoadOnly)
            {
                //Show alert only call at first time load , so the event should only be attached once.
                InteractionContext.Completing += new EventHandler<CancelEventArgs>(InteractionContextCompleting).MakeWeak();
                ExecuteShowAlertScreen(false);
                EditInvoice.Invoice.EncounterServices.ForEach(x => x.As<IEditableObjectExtended>().InitiateCustomEdit());
            }

            if (PermissionId.BillInvoice.EnsurePermission(false) && EditInvoice.Invoice.EncounterServices.Any())
            {
                _showNeverBilledPopUp = false;
                foreach (var s in EditInvoice.Invoice.EncounterServices)
                {
                    if (s.Balance != 0 && !s.TransactionsExist)
                        _showNeverBilledPopUp = true;
                }
            }
            isReloading = true;
        }

        private void ExecuteDiagnosisTypeChange()
        {
            if (isReloading)
            {
                if (EditInvoice == null) return;

                if (EditInvoice.Invoice.EncounterDiagnoses != null)
                {                    
                    //By Default Clear Diagnoses and Diagnosis Links
                    if (IsIcd10DiagnosisTypeSelected)
                    {
                        //var diagnosisCodeCollection = _createDiagnosisViewModelCollection();
                        //var diagnosisCode = _createDiagnosisViewModel();
                        if (EditInvoice.Invoice.EncounterDiagnoses != null)
                        {

                            EditInvoice.Invoice.EncounterDiagnoses.ForEach(i =>
                            {
                                if (i.ActiveDiagnosisCodes !=null && i.ActiveDiagnosisCode!=null)
                                {
                                    i.ActiveDiagnosisCodes = _editInvoiceViewService.GetIcd10Codes(i.ActiveDiagnosisCode.Code);
                                    if (i.ActiveDiagnosisCodes.Count == 1)
                                    {
                                        i.ActiveDiagnosisCode = i.ActiveDiagnosisCodes.First();
                                    }
                                    else
                                    {
                                        i.ActiveDiagnosisCode = null;
                                    }
                                    i.AlternateDiagnosisCode = null; 
                                }

                            });

                        }
                    }
                    else
                    {                        
                        bool? result = _interactionManager.Confirm("We are unable to translate these diagnosis codes from ICD 10 to ICD 9. If you proceed, your diagnosis codes will be cleared.", "Cancel", "Continue");
                        if (result.GetValueOrDefault())
                        {

                            if (EditInvoice.Invoice.EncounterDiagnoses != null) EditInvoice.Invoice.EncounterDiagnoses.Clear();
                            if (EditInvoice.Invoice.EncounterDiagnosisLinks != null) EditInvoice.Invoice.EncounterDiagnosisLinks.Clear();
                            if (EditInvoice.Invoice.EncounterServices != null) EditInvoice.Invoice.EncounterServices.ForEach(es => es.LinkedDiagnoses.Clear());
                        }
                        else
                        {
                            IsIcd10DiagnosisTypeSelected = true;
                        }
                    }

                    if (!IsIcd10DiagnosisTypeSelected)
                    {
                        System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(LoadIcd9Diagnoses);
                    }
                    else
                    {
                        CodeSetEnabled = false;
                        AsyncGetDiagonsis objInvoiceServices = new AsyncGetDiagonsis(AsyncGetFullDiagnosisCodes);
                        IAsyncResult tag = objInvoiceServices.BeginInvoke(new AsyncCallback(DiagnosisInfoLoaded), null);
                    }

                    EditInvoice.Invoice.CodesetType = new NamedViewModel
                    {
                        //code change corresponding to bug id - 19850// for new build creation Date - 02/11/2015
                        Id = IsIcd10DiagnosisTypeSelected ? 2 : 1,
                        Name = Enum.GetName(typeof(DiagnosisTypeId), IsIcd10DiagnosisTypeSelected ? 2 : 1)
                    };

                }
            }

        }

        /// <summary>
        /// Load filtered Diagnosis codes
        /// </summary>
        /// <param name="filterText">filtered codes</param>
        private void LoadFilteredDiagnosis(string filterText)
        {
            //CodeSetEnabled = false;           
            // const int invalid10Code = -101;
            var loadingDiagnoses = new ObservableCollection<DiagnosisCodeViewModel> 
                { 
                    new DiagnosisCodeViewModel
                    {
                        Id = loadingModelId,
                        Code = "Loading.",
                        Description = "Please wait..."
                    }
                };

            EditInvoice.DataLists.Diagnoses = loadingDiagnoses;
            if(IsIcd10DiagnosisTypeSelected)
            {
                if (_icd10FullDiagnosis == null)
                {
                    _icd10Diagnosis = _editInvoiceViewService.LoadDiagnoses(true, filterText);
                    EditInvoice.DataLists.Diagnoses = _icd10Diagnosis;
                    EditInvoice.Invoice.EncounterDiagnoses.ForEach(diag =>
                    {
                        diag.ActiveDiagnosisCodes = EditInvoice.DataLists.Diagnoses;

                        if (diag.ActiveDiagnosisCode.IfNotNull(c => c.Id == loadingModelId))
                        {
                            diag.ActiveDiagnosisCode = null;
                        }
                    });
                }
                else
                {
                    _icd10Diagnosis = _editInvoiceViewService.LoadDiagnoses(true, filterText);
                    EditInvoice.DataLists.Diagnoses = _icd10Diagnosis;
                    EditInvoice.Invoice.EncounterDiagnoses.ForEach(diag =>
                    {
                        diag.ActiveDiagnosisCodes = EditInvoice.DataLists.Diagnoses;

                        if (diag.ActiveDiagnosisCode.IfNotNull(c => c.Id == loadingModelId))
                        {
                            diag.ActiveDiagnosisCode = null;
                        }
                    });
                }
                //else
                //{
                //    if (_icd10Diagnosis != null && _icd10FullDiagnosis!=null)
                //    {
                //        if (!string.IsNullOrEmpty(filterText))
                //        {
                //            EditInvoice.DataLists.Diagnoses = _icd10Diagnosis.Where(x => x.CodeDescription.ToUpper().Contains(filterText.ToUpper())).ToObservableCollection<DiagnosisCodeViewModel>();
                //            if (EditInvoice.DataLists.Diagnoses.Count == 0)
                //            {
                //                var invalidDiagnoses = new ObservableCollection<DiagnosisCodeViewModel> 
                //            { 
                //                new DiagnosisCodeViewModel
                //                {
                //                    Id = invalid10Code,
                //                    Code = "Invalid code... "+ filterText.ToUpper()
                //                }
                //            };
                //                EditInvoice.DataLists.Diagnoses = invalidDiagnoses;
                //            }
                //        }
                //        else
                //        EditInvoice.DataLists.Diagnoses = _icd10Diagnosis;
                //    }
                //}
            }
                       

            //CodeSetEnabled = true;
        }
        private void ExecuteFilterDiagnoses(string filterText)
        {
            if (isReloading)
            {
                if (EditInvoice == null) return;

                if (EditInvoice.Invoice != null)
                {
                    System.Threading.Tasks.Task.Factory.StartNew(() => LoadFilteredDiagnosis(filterText));
                    //By Default Clear Diagnoses and Diagnosis Links
                    //if (EditInvoice.Invoice.EncounterDiagnoses != null) EditInvoice.Invoice.EncounterDiagnoses.Clear();
                    //if (EditInvoice.Invoice.EncounterDiagnosisLinks != null) EditInvoice.Invoice.EncounterDiagnosisLinks.Clear();
                    //if (EditInvoice.Invoice.EncounterServices != null) EditInvoice.Invoice.EncounterServices.ForEach(es => es.LinkedDiagnoses.Clear());
                    
                    EditInvoice.Invoice.CodesetType = new NamedViewModel
                    {
                        //code change corresponding to bug id - 19850// for new build creation Date - 02/11/2015
                        Id = IsIcd10DiagnosisTypeSelected ? 2 : 1,
                        Name = Enum.GetName(typeof(DiagnosisTypeId), IsIcd10DiagnosisTypeSelected ? 2 : 1)
                    };

                }
            }


        }
        private void AsyncGetFullDiagnosisCodes()
        {

            var loadingDiagnoses = new ObservableCollection<DiagnosisCodeViewModel> 
                { 
                    new DiagnosisCodeViewModel
                    {
                        Id = loadingModelId,
                        Code = "Loading.",
                        Description = "Please wait..."
                    }
                };

            EditInvoice.DataLists.Diagnoses = loadingDiagnoses;          

                if (_icd10FullDiagnosis == null)
                {
                    _icd10FullDiagnosis = _editInvoiceViewService.LoadDiagnoses(true, string.Empty);
                }
            EditInvoice.DataLists.Diagnoses = _icd10FullDiagnosis;
        }

        private void DiagnosisInfoLoaded(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            AsyncGetDiagonsis caller = (AsyncGetDiagonsis)result.AsyncDelegate;
            caller.EndInvoke(ar);
            if (_icd10FullDiagnosis != null && _icd10FullDiagnosis.Count > 0)
            {
                _icd10Diagnosis = _icd10FullDiagnosis;
            }
            EditInvoice.Invoice.EncounterDiagnoses.ForEach(diag =>
            {
                diag.ActiveDiagnosisCodes = EditInvoice.DataLists.Diagnoses;

                if (diag.ActiveDiagnosisCode.IfNotNull(c => c.Id == loadingModelId))
                {
                    diag.ActiveDiagnosisCode = null;
                }
            });           

            CodeSetEnabled = true;           
        }

        private void LoadIcd9Diagnoses()
        {
            CodeSetEnabled = false;           
            var loadingDiagnoses = new ObservableCollection<DiagnosisCodeViewModel> 
                { 
                    new DiagnosisCodeViewModel
                    {
                        Id = loadingModelId,
                        Code = "Loading.",
                        Description = "Please wait..."
                    }
                };

            EditInvoice.DataLists.Diagnoses = loadingDiagnoses;          
                if (_icd9Diagnosis == null)
                {
                    _icd9Diagnosis = _editInvoiceViewService.LoadDiagnoses(false,string.Empty);
                }
                EditInvoice.DataLists.Diagnoses = _icd9Diagnosis;                                        
            //EditInvoice.DataLists.Diagnoses = _editInvoiceViewService.LoadDiagnoses(SelectedDiagnosisCodeSet.Id == 2);
            // In case "Loading..." was accidentally selected as value
            EditInvoice.Invoice.EncounterDiagnoses.ForEach(diag =>
            {
                diag.ActiveDiagnosisCodes = EditInvoice.DataLists.Diagnoses;

                if (diag.ActiveDiagnosisCode.IfNotNull(c => c.Id == loadingModelId))
                {
                    diag.ActiveDiagnosisCode = null;
                }
            });
            CodeSetEnabled = true;
        }

        private void LoadExternalProviders()
        {
            var loadingProvider = new ObservableCollection<NameAndAbbreviationAndDetailViewModel> 
                { 
                    new NameAndAbbreviationAndDetailViewModel
                    {
                        Id = loadingModelId,
                        Name = "Loading..."
                    }
                };
            EditInvoice.DataLists.ExternalProviders = loadingProvider;

            ProvidersDataListsViewModel providerList = _editInvoiceViewService.LoadProviders();

            // prepare external providers
            EditInvoice.DataLists.ExternalProviders = providerList.ExternalProviders;
            // In case "Loading..." physician was accidentally selected
            if (EditInvoice != null && EditInvoice.Invoice != null && EditInvoice.Invoice.ReferringProvider != null && EditInvoice.Invoice.ReferringProvider.Id == loadingModelId)
            {
                EditInvoice.Invoice.ReferringProvider = null;
            }

            // prepare ordering providers
            EditInvoice.DataLists.OrderingProviders = providerList.OrderingProviders;
            if (EditInvoice != null && EditInvoice.Invoice != null && EditInvoice.Invoice.InvoiceSupplemental != null && EditInvoice.Invoice.InvoiceSupplemental.OrderingProvider != null && EditInvoice.Invoice.InvoiceSupplemental.OrderingProvider.Id == loadingModelId)
            {
                EditInvoice.Invoice.InvoiceSupplemental.OrderingProvider = null;
            }
        }

        /// <summary>
        /// In case of Saving through the billing button, to keep the billing action and action type in synch with the earlier selection.
        /// </summary>
        /// <param name="sourceModel"></param>
        /// <param name="invoice"></param>
        private static void SyncActionType(IList<EncounterServiceViewModel> sourceModel, InvoiceViewModel invoice)
        {
            foreach (var targetEncounterService in invoice.EncounterServices)
            {
                var source = sourceModel.FirstOrDefault(y =>
                    y.Service.Code == targetEncounterService.Service.Code);
                if (source == null) continue;

                // Restore next payer first (due to billing actions filtering logic)
                if (source.NextPayer != null)
                {
                    var nextPayer = invoice.NextPayers.FirstOrDefault(p =>
                    {
                        if (!p.Relationship.Equals(source.NextPayer.Relationship)) return false;

                        if (source.NextPayer.Payer is PatientPayerViewModel)
                        {
                            return p.Payer is PatientPayerViewModel;
                        }

                        if (source.NextPayer.Payer is PatientInsurancePayerViewModel
                            && p.Payer is PatientInsurancePayerViewModel)
                        {
                            return source.NextPayer.Payer.CastTo<PatientInsurancePayerViewModel>().PatientInsuranceId
                                == p.Payer.CastTo<PatientInsurancePayerViewModel>().PatientInsuranceId;
                        }

                        return false;
                    });

                    targetEncounterService.NextPayer = nextPayer;
                }

                // Restore action
                targetEncounterService.BillingAction = source.BillingAction;
            }
        }

        private void InteractionContextCompleting(object sender, CancelEventArgs e)
        {

            //we want to show this popup anytime the user tries to leave without saving.  
            //If they leave without saving AND have unbilled services and/or never-billed services, we want 
            //to show both popups.  This should really never happen, though.  Users are expected to click Save, then Bill. 
            //These popups are just precautions/warnings when users leave services in questionable states (unbilled or never billed).

            if (EditInvoice.Invoice.As<IEditableObjectExtended>().IsChanged)
            {
                var loadArgument = new ConfirmationDialogLoadArguments
                {
                    Cancel = CancelExitWithUnsavedChanges,
                    Content = "You have unsaved changes.  Would you like to save your changes?",
                    YesContent = "Save",
                    NoContent = "Don't Save",
                    CancelContent = "Cancel",
                    Yes = SaveInvoice
                };
                _confirmationDialogViewManager.ShowConfirmationDialogView(loadArgument);
            }

            if (_showUnbilledPopUp)
            {
                var result = _interactionManager.Confirm("The updated balance will not be billed until you re-bill the service.", "Go back", "Leave unbilled");
                _stopExitingWindow = !result.GetValueOrDefault();
            }

            if (!_showUnbilledPopUp && _showNeverBilledPopUp)
            {
                var result = _interactionManager.Confirm("There are services on this invoice that have never been billed.", "Go back", "Leave unbilled");
                _stopExitingWindow = !result.GetValueOrDefault();
            }
            e.Cancel = _stopExitingWindow;
            _stopExitingWindow = false;
        }

        private void IntializePatientInsuranceAuthorizationScreen(bool launchScreen)
        {
            ConfigurePatientInsuranceAuthorization(EditInvoice.Invoice.EncounterId, EditInvoice.Invoice.PatientInsuranceAuthorization, launchScreen);
        }

        private void ConfigurePatientInsuranceAuthorization(int? encounterId, ViewModels.PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationViewModel authorization, bool launchAuthorizationScreen)
        {
            var loadArguments = new PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupLoadArguments();
            loadArguments.OperationMode = PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationPopupOperationMode.Instant;
            loadArguments.OnButtonClick = encounterId.HasValue ? null : OnPatientInsuranceAuthorizationPopUpclick;
            loadArguments.EncounterId = encounterId;
            loadArguments.InvoiceId = EditInvoice.Invoice.Id;
            if (authorization != null && authorization.Location != null)
            {
                loadArguments.AttachedPatientInsuranceAuthorization = authorization;
                loadArguments.AttachedPatientInsuranceAuthorization.Location = new ColoredViewModel
                {
                    Color = authorization.Location.Color,
                    ColorString = authorization.Location.ColorString,
                    Id = authorization.Location.Id,
                    Name = authorization.Location.Name
                };
            }

            loadArguments.DirectlyLaunchAuthorizationScreen = launchAuthorizationScreen;
            PatientInsuranceAuthorizationPopupViewContext.DirectlyLoadArguments(loadArguments);
        }

        private void GetExistingReferralInformation()
        {
            var referrals = _editInvoiceViewService.GetPatientExistingReferralInformation(EditInvoice.Invoice.Patient.Id);
            Referral.AppointmentId = EditInvoice.Invoice.EncounterId;
            Referral.InvoiceReceivableIds = EditInvoice.Invoice.InvoiceReceivables.ToList();
            Referral.ExistingReferralInformation = new ExtendedObservableCollection<ReferralViewModel>();
            Referral.ListOfAttachedReferral = new ExtendedObservableCollection<ReferralViewModel>(referrals.Where(x => x.InvoiceReceivableIds.Any(y => Referral.InvoiceReceivableIds.Contains(y))));
            //If current invoice does not have any referral attached to the invoice receivables load the encounter one.
            if (Referral.ListOfAttachedReferral.Count == 0)
            {
                Referral.ListOfAttachedReferral.AddRangeWithSinglePropertyChangedNotification(new ExtendedObservableCollection<ReferralViewModel>(referrals.Where(x => x.EncounterIds.Contains(EditInvoice.Invoice.EncounterId.GetValueOrDefault()))));
            }
            Referral.ValidateReferralAndIncludeInExisting(referrals.Where(x => Referral.ListOfAttachedReferral.All(y => y.Id != x.Id)));
        }

        //Saving invoice
        private bool ExecuteSave()
        {
            //Add a row to service if data is present in new row template.
            AddNewRow.Execute(NewEncounterService);


            if (!EditInvoice.Invoice.As<IEditableObjectExtended>().IsChanged)
            {
                return false;
            }

            if (!PermissionId.EditInvoice.EnsurePermission())
            {
                EditInvoice.Invoice.As<IEditableObjectExtended>().CancelEdit();
                return false;
            }

            var errorMessages = EditInvoice.Invoice.Validate();
            if (!errorMessages.Any())
            {
                if (EditInvoice.Invoice.EncounterDiagnoses.Count > 0)
                {
                    for (int i = 0; i < EditInvoice.Invoice.EncounterDiagnoses.Count; i++)
                    {
                        if (EditInvoice.Invoice.EncounterDiagnoses[i].ActiveDiagnosisCode == null)
                        {
                            ValidationMessage = "Diagnosis code is missing, please select it.";
                            ValidationWarningTrigger = true;
                            return false;
                        }
                    }
                }

                //Filling the Refferals before it goes for save;
                if (Referral.ListOfAttachedReferral.Count > 0)
                {
                    EditInvoice.Invoice.Referral = Referral.ListOfAttachedReferral[0];
                }
                if (PatientInsuranceAuthorizationPopupViewContext.AttachedPatientInsuranceAuthorization != null)
                {
                    EditInvoice.Invoice.PatientInsuranceAuthorization = PatientInsuranceAuthorizationPopupViewContext.AttachedPatientInsuranceAuthorization;
                }
                //For removing the diagnosis which does not have values to avoid object reference error
                EditInvoice.Invoice.EncounterDiagnoses.Where(x => x.ActiveDiagnosisCode == null).ToArray().ForEachWithSinglePropertyChangedNotification(x => EditInvoice.Invoice.EncounterDiagnoses.Remove(x));

                EditInvoice.Invoice.Id = _editInvoiceViewService.SaveInvoice(EditInvoice.Invoice);

                EditInvoice.Invoice.EncounterServices.ForEach(x =>
                {
                    var changedItems = x.CastTo<IEditableObjectExtended>()
                         .GetOriginalValuesForNamedEdit(EditableObjectExtensions.CustomEditName);
                    //if Charge has been changed on billing service or a new billing service has been added to invoice , we should show the unbilled pop up.
                    if (PermissionId.BillInvoice.EnsurePermission(false) &&
                        (changedItems != null && changedItems.Any(c => c.Key == EncounterServiceViewModel.ChargePropertyName ||
                                                                        c.Key == EncounterServiceViewModel.UnitsPropertyName)) && x.TransactionsExist)
                    {
                        _showUnbilledPopUp = true;
                    }
                });

                if (PermissionId.BillInvoice.EnsurePermission(false) && EditInvoice.Invoice.EncounterServices.Any())
                {
                    _showNeverBilledPopUp = false;
                    foreach (var s in EditInvoice.Invoice.EncounterServices)
                    {
                        if (s.Balance != 0 && !s.TransactionsExist)
                            _showNeverBilledPopUp = true;
                    }
                }
                //If encounter id is not present , meaning this is a new manual claim.
                if (!EditInvoice.Invoice.EncounterId.HasValue
                    || EditInvoice.Invoice.EncounterServices.Any(x => !x.Id.HasValue)) //if new service has been added while editing the invoice, reload the page to get the service id.
                {
                    ExecuteLoad(false);
                }

                EditInvoice.Invoice.As<IEditableObjectExtended>().AcceptChanges();
                EditInvoice.Invoice.As<IEditableObjectExtended>().EndEdit();
            }
            else
            {
                ValidationMessage = errorMessages.Select(x => x.Value).Join(Environment.NewLine);
                ValidationWarningTrigger = true;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Diagnosis right arrow key pressed
        /// </summary>
        /// <param name="obj"></param>
        private void ExecuteMoveRowRight(DiagnosisViewModel obj)
        {
            ShuffleDiagnosis(obj, Enumerables.MoveDirection.Forward);
        }

        //Diagnosis left arrow key pressed
        private void ExecuteMoveRowLeft(DiagnosisViewModel obj)
        {
            ShuffleDiagnosis(obj, Enumerables.MoveDirection.Backward);
        }

        //used when diagnosis right or left arrow key pressed.
        private void ShuffleDiagnosis(DiagnosisViewModel obj, Enumerables.MoveDirection moveDirection)
        {
            EditInvoice.Invoice.EncounterDiagnoses.Move(obj, moveDirection);
            ResetDiagnosis();
        }

        //Helper function when move the diagnosis.
        private void ResetDiagnosis()
        {
            for (var i = 0; i < EditInvoice.Invoice.EncounterDiagnoses.Count; i++)
            {
                var pointer = EditInvoice.Invoice.EncounterDiagnoses[i].DiagnosisLinkPointer;
                pointer.PointerLetter = DiagnosisLetters[i];
                pointer.DiagnosisColor = DiagnosisColors[i];
            }
        }

        //When delete key is pressed on diagnosis.
        private void ExecuteDeleteDiagnosis(DiagnosisViewModel item)
        {
            EditInvoice.Invoice.EncounterDiagnoses.Remove(item);
            EditInvoice.Invoice.EncounterServices.ForEach(es => es.LinkedDiagnoses.Remove(item.DiagnosisLinkPointer));
            ResetDiagnosis();
        }

        //When plus key is pressed on diagnosis row.
        private void ExecuteAddDiagnosis()
        {
            EditInvoice.Invoice.EncounterDiagnoses.Add(_diagnosisMapper.Map(EditInvoice.Invoice));
        }

        //When delete key is pressed on service row.
        private void ExecuteDeleteService(EncounterServiceViewModel item)
        {
            EditInvoice.Invoice.EncounterServices.Remove(item);
        }

        //When tab out or key down is pressed service new row.
        private void ExecuteAddNewRow()
        {
            if (NewEncounterService.Service != null)
            {
                var details = LoadServiceDetails(NewEncounterService);

                //We should default the units value when select a service (ExecuteEnsureUnitChargeAndUpdateEncounterServiceValues), 
                //not when we add a row.  by the time we add a row, the user has had the option to change the units.  We might  
                //be overwriting their selection here.  
                //It is, however, safe (though I don't think necessary) to update NDC/Unclassified, because the user can't 
                //change these via the UI until after the row's been added.

                //NewEncounterService.Units = details.Units;

                NewEncounterService.NdcCode = details.NdcCode;
                NewEncounterService.UnclassifiedServiceDescription = details.UnclassifiedServiceDescription;

                EditInvoice.Invoice.EncounterServices.Add(NewEncounterService);
                NewEncounterService.AllBillingActions = EditInvoice.DataLists.BillingActions;
                if (NextPayer.Payer != null)
                {
                    NewEncounterService.NextPayer = NextPayer;
                }

                NewEncounterService = _createEncounterServiceViewModel();
            }
        }

        private EncounterServiceViewModel LoadServiceDetails(EncounterServiceViewModel encounterService)
        {
            return _editInvoiceViewService.LoadServiceDetails(encounterService.Service.Id);
        }

        //Run bill command asynch.
        private void ExecuteBill()
        {
            if (EditInvoice.Invoice.As<IEditableObjectExtended>().IsChanged)
            {
                if (!ExecuteSave())
                {
                    return;
                }
            }

            if (!PermissionId.BillInvoice.EnsurePermission())
            {
                EditInvoice.Invoice.As<IEditableObjectExtended>().CancelEdit();
                return;
            }

            if (ValidateBillData(EditInvoice.Invoice))
            {
                //Incase user select the referral after manual claim save and since referral handeling is done by another window, need to assign for billing.
                if (Referral.ListOfAttachedReferral.Count > 0)
                {
                    EditInvoice.Invoice.Referral = Referral.ListOfAttachedReferral[0];
                }
                if (PatientInsuranceAuthorizationPopupViewContext.AttachedPatientInsuranceAuthorization != null)
                {
                    EditInvoice.Invoice.PatientInsuranceAuthorization = PatientInsuranceAuthorizationPopupViewContext.AttachedPatientInsuranceAuthorization;
                }

                _editInvoiceViewService.BillInovice(EditInvoice.Invoice);

                EditInvoice.Invoice.As<IEditableObjectExtended>().AcceptChanges();
                EditInvoice.Invoice.As<IEditableObjectExtended>().EndEdit();
                //In case invoice has been billed, we dont want to show the pop up on close.
                _showUnbilledPopUp = false;
                _showNeverBilledPopUp = false;

                if (PrintClaim)
                {
                    ExecuteLoadClaim(EditInvoice.Invoice.Id.GetValueOrDefault().EnsureNotDefault());
                }
                //Close the current dialogue after billing is done.
                InteractionContext.Complete(true);
            }
        }

        /// <summary>
        /// Load and generate claim
        /// </summary>
        /// <param name="invoiceId"></param>
        private void ExecuteLoadClaim(int invoiceId)
        {
            var htmlContents = _editInvoiceViewService.LoadClaim(invoiceId);

            foreach (var htmlContent in htmlContents)
            {
                var pdfBytes = WkHtmlToPdfWrapperService.GeneratePdf(htmlContent.Content);
                var path = FileManager.Instance.GetTempPathName(); // get a temp file path
                path = path.Replace(".tmp", ".pdf");
                const string documentName = "Insurer Claim"; // set the document name
                try
                {
                    FileManager.Instance.CommitContents(path, pdfBytes); // put the pdf bytes to the path
                    this.Dispatcher().Invoke(() => Print(path, documentName)); // print
                }
                finally
                {
                    // delete the path
                    FileManager.Instance.Delete(path);
                }
            }
        }

        /// <summary>
        /// Printing the claim data
        /// </summary>
        /// <param name="path"></param>
        /// <param name="documentName"></param>
        private void Print(string path, string documentName)
        {
            _documentViewManger.HeaderTitleOfCurrentWindow = "Submit Claims";
            _documentViewManger.PrintDocuments(documentName, new[] { path });
        }

        //Validation when billing
        private bool ValidateBillData(InvoiceViewModel invoice)
        {
            if (invoice.EncounterServices.Any(x => x.Balance > 0 && !x.IsBillingActionValid()))
            {
                ValidationMessage = "Please pick an action for each service.";
                ValidationWarningTrigger = true;
                return false;
            }
            int? referralInsuranceId = null;

            if (Referral.ListOfAttachedReferral.Count > 0)
            {
                referralInsuranceId = Referral.ListOfAttachedReferral[0].Insurance.Id;
            }
            var warningList = _editInvoiceViewService.ValidateBill(invoice.Id.EnsureNotDefault().GetValueOrDefault(), invoice.EncounterServices, referralInsuranceId, invoice.CareTeam.RenderingProvider.Id);
            if (warningList.Any())
            {
                var warnings = warningList.Select(x => x.GetDisplayName()).Join(Environment.NewLine);
                ValidationMessage = warnings;
                ValidationWarningTrigger = true;
                return false;
            }
            return true;
        }

        private static readonly string[] DiagnosisColors =
        {
            "#FF00CD7F", 
            "#FF00CDCD", 
            "#FF00AFFF", 
            "#FF6478FF", 
            "#FFBB78FF",
            "#FFF57AE8",
            "#FFDE6498",
            "#FFFA7776",
            "#FFEA7460",
            "#FFFDA46C",
            "#FFE5B844",
            "#FFC0D674"
        };

        private static readonly String[] DiagnosisLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
    }
}
