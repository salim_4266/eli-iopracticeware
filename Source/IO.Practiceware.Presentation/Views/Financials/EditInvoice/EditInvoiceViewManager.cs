﻿using System.Windows;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.EditInvoice
{
    public class EditInvoiceViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public EditInvoiceViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowEditInvoice(EditInvoiceLoadArguments loadArguments)
        {
            var view = new EditInvoiceView();
            var dataContext = view.DataContext.EnsureType<EditInvoiceViewContext>();
            dataContext.LoadArguments = loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments
                                          {
                                              Content = view,
                                              WindowState = WindowState.Normal,
                                              ResizeMode = ResizeMode.CanResizeWithGrip
                                          });
        }
    }

    public class EditInvoiceLoadArguments
    {
        /// <summary>
        /// Patient's Id.
        /// </summary>
        public int? PatientId { get; set; }

        /// <summary>
        /// Invoice Id.
        /// </summary>
        public int? InvoiceId { get; set; }
    }
}
