﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.EditInvoice
{
    /// <summary>
    /// Interaction logic for EditInvoiceView.xaml
    /// </summary>
    public partial class EditInvoiceView
    {
        public EditInvoiceView()
        {
            InitializeComponent();
        }

    }

    public class EditInvoiceViewContextReference : DataContextReference
    {
        public new EditInvoiceViewContext DataContext
        {
            get { return (EditInvoiceViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
