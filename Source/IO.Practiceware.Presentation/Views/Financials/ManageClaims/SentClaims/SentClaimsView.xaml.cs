﻿
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.SentClaims
{
    /// <summary>
    /// Interaction logic for SentClaimsView.xaml
    /// </summary>
    public partial class SentClaimsView
    {
        public SentClaimsView()
        {
            InitializeComponent();
        }
    }

    public class SentClaimsViewContextReference : DataContextReference
    {
        public new SentClaimsViewContext DataContext
        {
            get { return (SentClaimsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
