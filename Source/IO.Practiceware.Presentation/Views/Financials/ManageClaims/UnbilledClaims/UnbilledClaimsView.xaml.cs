﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.UnbilledClaims
{
    /// <summary>
    /// Interaction logic for SentAndUnbilledClaims.xaml
    /// </summary>
    public partial class UnbilledClaimsView
    {
        public UnbilledClaimsView()
        {
            InitializeComponent();
        }
    }
    public class UnbilledClaimsViewContextReference : DataContextReference
    {
        public new UnbilledClaimsViewContext DataContext
        {
            get { return (UnbilledClaimsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
