﻿using System;
using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.UnbilledClaims
{
    public class UnbilledClaimsViewContext : IViewContext
    {
         public virtual ObservableCollection<InvoiceViewModel> Invoices { get; set; }

        public ObservableCollection<InvoiceViewModel> SelectedInvoices { get; set; }

        public virtual DataListsViewModel DataLists { get; set; }

        public virtual FilterViewModel Filter { get; set; }

        public UnbilledClaimsViewContext()
        {
            DataLists = new DataListsViewModel
            {
                ServiceLocations = CreateNamedViewModels("location", 10),
                RenderingProviders = CreateRenderingProviders(20),
            };
            Invoices = CreateInvoices(50);

            Filter = new FilterViewModel
            {
                StartDate = DateTime.Today,
            };
        }

        public IInteractionContext InteractionContext { get; set; }

        public ExtendedObservableCollection<InvoiceViewModel> CreateInvoices(int n)
        {
            var toReturn = new ExtendedObservableCollection<InvoiceViewModel>();
            for (var i = 1; i <= n; i++)
            {
                toReturn.Add(new InvoiceViewModel
                {
                    Id = i,
                    DateOfService = DateTime.Today.AddDays(-i),
                    CareTeam = CreateCareTeam(),
                    ServiceLocation = new NamedViewModel { Name = "Office" },
                    Patient = new PatientViewModel
                    {
                        FirstName = "FirstName",
                        LastName = "LastName",
                        MiddleName = "MiddleName",
                        Id = i * 123,
                        PrimaryInsurer = new InsurerViewModel
                        {
                            Id = i * 234,
                            InsurerName = "InsurerName",
                            PlanName = "PlanName",
                            PolicyHolder = new PolicyHolderViewModel
                            {
                                FirstName = "FirstName",
                                LastName = "LastName",
                                MiddleName = "MiddleName",
                                Id = i * 45
                            }
                        }
                    },
                });
            }
            return toReturn;
        }



        public CareTeamViewModel CreateCareTeam()
        {
            return new CareTeamViewModel
            {
                ScheduledProvider = new NamedViewModel { Name = "ScheduledProvider" },
                RenderingProvider = new ProviderViewModel { BillingOrganization = new NamedViewModel { Name = "Office" }, Provider = new NamedViewModel { Name = "RenderingProvider" } },
            };
        }

        public ObservableCollection<NamedViewModel> CreateNamedViewModels(String name, int n)
        {
            var toReturn = new ObservableCollection<NamedViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new NamedViewModel { Name = name + i, Id = i });
            }
            return toReturn;
        }
        public ObservableCollection<ProviderViewModel> CreateRenderingProviders(int n)
        {
            var toReturn = new ObservableCollection<ProviderViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new ProviderViewModel { BillingOrganization = new NamedViewModel { Name = "Office" }, Provider = new NamedViewModel { Name = "RenderingProvider" } });
            }
            return toReturn;
        }
    }
}
