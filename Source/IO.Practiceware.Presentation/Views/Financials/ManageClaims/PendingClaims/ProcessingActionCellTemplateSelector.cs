﻿
using System.Windows;
using System.Windows.Controls;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.PendingClaims
{
    public class ProcessingActionCellTemplateSelector : DataTemplateSelector
    {

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is InvoiceViewModel)
            {
                var x = item as InvoiceViewModel;
                if (x.Status.Name == "Error")
                    return ErrorDataTemplate;
                if (x.Status.Name == "Validate")
                    return ValidateDataTemplate;
                return ProcessDataTemplate;
            }
            return null;
        }
        public DataTemplate ErrorDataTemplate { get; set; }
        public DataTemplate ValidateDataTemplate { get; set; }
        public DataTemplate ProcessDataTemplate { get; set; }
    }
}
