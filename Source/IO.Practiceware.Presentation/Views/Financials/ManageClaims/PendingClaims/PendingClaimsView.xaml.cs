﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.PendingClaims
{
    /// <summary>
    /// Interaction logic for PendingBillingView.xaml
    /// </summary>
    public partial class PendingClaimsView 
    {
        public PendingClaimsView()
        {
            InitializeComponent();
        }

        public new PendingClaimsViewContext DataContext
        {
            get { return (PendingClaimsViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    public class PendingClaimsViewContextReference : DataContextReference
    {
        public new PendingClaimsViewContext DataContext
        {
            get { return (PendingClaimsViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
