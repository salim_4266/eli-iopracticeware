﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Financials.EditInvoice;
using IO.Practiceware.Presentation.Views.PatientInfo;
using IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims.PendingClaims;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.PendingClaims
{
    public class PendingClaimsViewContext : IViewContext
    {
        #region Commands
        public ICommand Load { get; protected set; }
        public ICommand Process { get; protected set; }
        public ICommand DisplayResult { get; protected set; }
        public ICommand LaunchFinancialSummaryScreen { get; protected set; }
        public ICommand LaunchPatientScreen { get; protected set; }
        public ICommand LaunchPolicyHolderScreen { get; protected set; }
        public ICommand LaunchPatientInsuranceScreen { get; protected set; }
        public ICommand LaunchEditInvoiceScreen { get; protected set; }
        public ICommand ReloadInvoice { get; protected set; }
        public ICommand CancelExitWithUnsavedChanges { get; protected set; }
        public ICommand ProcessClose { get; protected set; }
        public ICommand ProcessValidate { get; protected set; }
        public ICommand SaveFilter { get; protected set; }
        public ICommand DeleteFilter { get; protected set; }
        public ICommand ResetFilter { get; protected set; }

        #endregion

        #region Properties
        public IInteractionContext InteractionContext { get; set; }

        public virtual FilterViewModel Filter { get; set; }

        [DispatcherThread]
        public virtual PendingClaimsViewModel PendingClaimsData { get; set; }
        public virtual PendingClaimsLoadInformation PendingClaimsLoadInformation { get; set; }
        public virtual bool ConfirmExitWithUnsavedChangesTrigger { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<PendingClaimsFilter> SavedFilters { get; set; }

        [DispatcherThread]
        public virtual PendingClaimsFilter SelectedSavedFilter
        {
            get { return _selectedSavedFilter; }
            set
            {
                _selectedSavedFilter = value;

                if (value != null && value != _newPendingClaimsFilter)
                {
                    Filter.StartDate = value.StartDate;

                    Filter.EndDate = value.EndDate;

                    Filter.ServiceLocations = PendingClaimsLoadInformation.DataLists.ServiceLocations
                        .Where(i => value.ServiceLocationIds.Contains(i.Id)).ToObservableCollection();

                    Filter.RenderingProviders = PendingClaimsLoadInformation.DataLists.RenderingProviders
                        .Where(i => value.RendingProviderIds.Contains(i.Id)).ToObservableCollection();
                }
            }
        }

        #endregion

        #region Constructors
        public PendingClaimsViewContext(IPendingClaimsViewService pendingClaimsViewService,
            Func<PendingClaimsViewModel> pendingClaimsData,
            Func<FilterViewModel> filterViewModel,
            PatientInfoViewManager patientInfoViewManager,
            EditInvoiceViewManager editInvoiceViewManager,
            IApplicationSettingsService applicationSettingsService,
            IInteractionManager interactionManager)
        {
            _pendingClaimsViewService = pendingClaimsViewService;
            _pendingClaimsData = pendingClaimsData;
            _filterViewModel = filterViewModel;
            _patientInfoViewManager = patientInfoViewManager;
            _editInvoiceViewManager = editInvoiceViewManager;
            _applicationSettingsService = applicationSettingsService;
            _interactionManager = interactionManager;

            _newPendingClaimsFilter = new PendingClaimsFilter { Name = "Create New" };

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Process = Command.Create(ExecuteProcess).Async(() => InteractionContext);
            DisplayResult = Command.Create(ExecuteDisplayResult).Async(() => InteractionContext, false, null, true);
            LaunchFinancialSummaryScreen = Command.Create<int>(ExecuteLaunchFinancialSummary);
            LaunchPatientScreen = Command.Create<int>(ExecuteLaunchPatientScreen);
            LaunchPatientInsuranceScreen = Command.Create<int>(ExecuteLaunchPatientInsuranceScreen);
            LaunchEditInvoiceScreen = Command.Create<int>(ExecuteLaunchEditInvoiceScreen);
            CancelExitWithUnsavedChanges = Command.Create(() => _stopExitingWindow = true);
            ProcessClose = Command.Create(() => InteractionContext.Complete(true));
            ProcessValidate = Command.Create(ExecuteProcessValidate).Async(() => InteractionContext, false, null, true);
            ReloadInvoice = Command.Create<int>(ExecuteReloadInvoice).Async(() => InteractionContext);

            SaveFilter = Command.Create(ExecuteSaveFilter, () => SelectedSavedFilter != null).Async(() => InteractionContext);
            DeleteFilter = Command.Create<PendingClaimsFilter>(ExecuteDeleteFilter, CanDeleteFilter).Async(() => InteractionContext);
            ResetFilter = Command.Create(ExecuteResetFilter);
        }

        #endregion

        #region Private Members
        readonly IPendingClaimsViewService _pendingClaimsViewService;
        readonly IInteractionManager _interactionManager;
        private readonly Func<PendingClaimsViewModel> _pendingClaimsData;
        private readonly Func<FilterViewModel> _filterViewModel;
        private readonly PatientInfoViewManager _patientInfoViewManager;
        private readonly EditInvoiceViewManager _editInvoiceViewManager;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private bool _stopExitingWindow;
        private DateTime? _startDate, _endDate;
        private PendingClaimsFilter _selectedSavedFilter;
        private ICollection<ApplicationSettingContainer<PendingClaimsFilter>> _filterApplicationSettings;
        private readonly PendingClaimsFilter _newPendingClaimsFilter;

        #endregion

        #region Private Methods

        private void ExecuteProcessValidate()
        {
            //incase they click on process without any result in grid
            if (PendingClaimsData == null) { return; }

            var invoiceIds = PendingClaimsData.SelectedInvoices.Where(p => !p.ServicesHaveZeroBalance).Select(p => p.Id).ToList();
            var validationMessages = _pendingClaimsViewService.ValidateInvoices(invoiceIds);
            PendingClaimsData.SelectedInvoices.ForEachWithSinglePropertyChangedNotification(x => x.IsValidated = false);
            PendingClaimsData.SelectedInvoices.ForEachWithSinglePropertyChangedNotification(x => x.Errors.Clear());
            //only set the validation of the invoices where there are no errors.Error once will be set later.
            PendingClaimsData.SelectedInvoices.Where(x => !validationMessages.Keys.Contains(x.Id)).ForEachWithSinglePropertyChangedNotification(x => x.IsValidated = true);
            foreach (var validationMessage in validationMessages)
            {
                var messages = validationMessage.Value;
                var invalidInvoice = PendingClaimsData.SelectedInvoices.Single(x => x.Id == validationMessage.Key);
                invalidInvoice.Errors.Clear();
                invalidInvoice.IsValidated = true;
                //in case invoice status is closed , we should not fail the validation for missing diagnosis.
                if (invalidInvoice.Status.Id == (int)InvoiceStatus.Close)
                {
                    messages.Remove(ValidationMessage.MissingDiagnosis);
                }
                foreach (var message in messages)
                {
                    invalidInvoice.Errors.Add(new NamedViewModel { Name = message.GetDisplayName() });
                    //http://stackoverflow.com/questions/7543044/how-to-trigger-celltemplateselector-when-items-changed
                    // Forcing template to reselect because DataGridTemplateColumn isn't part of visual tree as stated in above link
                    this.Dispatcher().Invoke(() =>
                    {
                        var index = PendingClaimsData.Invoices.IndexOf(invalidInvoice);
                        PendingClaimsData.Invoices.Remove(invalidInvoice);
                        // Force refresh of data template
                        PendingClaimsData.Invoices.Insert(index, invalidInvoice);
                    });

                }
            }
        }
        private void ExecuteDisplayResult()
        {
            PendingClaimsData = _pendingClaimsData();
            int count;
            var invoices = _pendingClaimsViewService.GetInvoices(Filter, out count);
            PendingClaimsData.Invoices = invoices;
            if (count > 200)
            {
                _interactionManager.Alert(string.Format("There are {0} pending claims to process. We're about to load the 200 most recent claims. To Process the rest, click \"Display Results\" again after you're done with these 200 claims.", count));
            }
        }
        private void ExecuteProcess()
        {
            //incase they click on process without any result in grid
            if (PendingClaimsData == null) { return; }
            if (PermissionId.ProcessPendingClaims.EnsurePermission())
            {
                var claimToProcess = PendingClaimsData.SelectedInvoices.Where(x => x.Status.Id == (int)InvoiceStatus.Bill || x.Status.Id == (int)InvoiceStatus.Close || ( x.Status.Id == (int)InvoiceStatus.Error && x.Services.Count == 0)).ToArray();
                _pendingClaimsViewService.BillInvoices(claimToProcess);
                claimToProcess.ForEachWithSinglePropertyChangedNotification(invoice => PendingClaimsData.Invoices.Remove(invoice));
            }

        }
        private void ExecuteLoad()
        {
            _filterApplicationSettings = _applicationSettingsService.GetSettings<PendingClaimsFilter>(ApplicationSetting.PendingClaimsFilter, null, UserContext.Current.UserDetails.Id);

            PendingClaimsLoadInformation = _pendingClaimsViewService.LoadInformation();
            Filter = _filterViewModel();
            Filter.EndDate = _endDate = DateTime.Today;
            Filter.StartDate = _startDate = PendingClaimsLoadInformation.StartDate;
            InteractionContext.Completing += new EventHandler<CancelEventArgs>(InteractionContextCompleting).MakeWeak();
            SavedFilters = new[] { _newPendingClaimsFilter }
                .Concat(_filterApplicationSettings.Select(a => a.Value).OrderBy(f => f.Name))
                .ToExtendedObservableCollection();

        }
        void InteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (PendingClaimsData != null
                && PendingClaimsData.SelectedInvoices.Count(x => x.Status.Id == (int)InvoiceStatus.Bill || x.Status.Id == (int)InvoiceStatus.Close || (x.Status.Id == (int)InvoiceStatus.Error && x.Services.Count == 0) ) > 0)
            {
                ConfirmExitWithUnsavedChangesTrigger = true;
                e.Cancel = _stopExitingWindow;
                _stopExitingWindow = false;
            }
        }
        private void ExecuteResetFilter()
        {
            Filter.StartDate = _startDate;
            Filter.EndDate = _endDate;
            Filter.RenderingProviders.Clear();
            Filter.ServiceLocations.Clear();
            SelectedSavedFilter = null;
        }
        private void ExecuteLaunchEditInvoiceScreen(int invoiceId)
        {
            ShowEditInvoiceScreen(invoiceId);
        }
        private void ExecuteLaunchPatientInsuranceScreen(int patientId)
        {
            ShowPatientInformationScreen(patientId, PatientInfoTab.Insurance);
        }
        private void ExecuteLaunchPatientScreen(int patientId)
        {
            ShowPatientInformationScreen(patientId, PatientInfoTab.Demographics);
        }
        private void ExecuteLaunchFinancialSummary(int patientId)
        {
            ShowPatientInformationScreen(patientId, PatientInfoTab.FinancialData);
        }
        private void ShowPatientInformationScreen(int patientId, PatientInfoTab tabToOpen)
        {
            _patientInfoViewManager.ShowPatientInfo(new PatientInfoLoadArguments
            {
                PatientId = patientId,
                TabToOpen = tabToOpen
            });
            DisplayResult.Execute();
        }
        private void ShowEditInvoiceScreen(int invoiceId)
        {
            _editInvoiceViewManager.ShowEditInvoice(new EditInvoiceLoadArguments
            {
                InvoiceId = invoiceId,
            });
            ReloadInvoice.Execute(invoiceId);
        }

        private void ExecuteReloadInvoice(int id)
        {
            var changedInvoice = PendingClaimsData.Invoices.Single(x => x.Id == id);
            var updatedInvoice = _pendingClaimsViewService.LoadInvoice(id);
            this.Dispatcher().Invoke(() =>
            {
                // remove if the invoice is no longer within the parameters of the filter or if all services in the invoice have been billed (!updatedInvoice.OpenForReview)
                if (!updatedInvoice.OpenForReview
                    || (Filter.ServiceLocations.Any() && !Filter.ServiceLocations.Select(x => x.Id).Contains(updatedInvoice.ServiceLocation.Id))
                    || (Filter.RenderingProviders.Any() && !Filter.RenderingProviders.Select(x => x.Id).Contains(updatedInvoice.CareTeam.RenderingProvider.Id))
                    || (Filter.StartDate.HasValue && updatedInvoice.DateOfService.HasValue && updatedInvoice.DateOfService.Value.Date < Filter.StartDate.Value.Date)
                    || (Filter.EndDate.HasValue && updatedInvoice.DateOfService.HasValue && updatedInvoice.DateOfService.Value.Date > Filter.EndDate.Value.Date)
                    )
                {
                    PendingClaimsData.Invoices.Remove(changedInvoice);
                }
                else
                {
                    // else replace the existing invoice
                    PendingClaimsData.Invoices.Replace(changedInvoice, updatedInvoice);
                }
            });
        }

        private void ExecuteSaveFilter()
        {
            if (SelectedSavedFilter == null)
            {
                return;
            }

            // Resolve filter name
            var filterName = SelectedSavedFilter.Name;
            if (filterName == _newPendingClaimsFilter.Name)
            {
                var promptResult = InteractionManager.Current.Prompt("Enter name below to save your selected filters:", "Saved Filters");

                // User cancelled saving?
                if (promptResult.DialogResult == null || promptResult.DialogResult == false)
                {
                    return;
                }

                // Ensure entered filter name is valid
                if (promptResult.Input.IsNullOrEmpty() || promptResult.Input == _newPendingClaimsFilter.Name)
                {
                    InteractionManager.Current.Alert("You must specify a valid name.");
                    return;
                }

                filterName = promptResult.Input;
            }

            // Resolve filter record or create new one
            var isNewFilter = false;
            var filterToSave = _filterApplicationSettings.FirstOrDefault(f => f.Value.Name == filterName);
            if (filterToSave == null)
            {
                filterToSave = new ApplicationSettingContainer<PendingClaimsFilter>
                {
                    Name = ApplicationSetting.PendingClaimsFilter,
                    UserId = UserContext.Current.UserDetails.Id,
                    Value = new PendingClaimsFilter { Name = filterName }
                };

                isNewFilter = true;
            }

            // Store current selected values for this filter
            filterToSave.Value.StartDate = Filter.StartDate;
            filterToSave.Value.EndDate = Filter.EndDate;
            filterToSave.Value.RendingProviderIds = Filter.RenderingProviders.Select(p => p.Id).ToList();
            filterToSave.Value.ServiceLocationIds = Filter.ServiceLocations.Select(s => s.Id).ToList();

            // Save filter
            _applicationSettingsService.SetSetting(filterToSave);

            if (isNewFilter)
            {
                // Add to settings collection
                _filterApplicationSettings.Add(filterToSave);

                // Add to UI filters list
                var itemToInsertBefore = SavedFilters
                    .Where(f => f != _newPendingClaimsFilter)
                    .FirstOrDefault(f => string.CompareOrdinal(f.Name, filterToSave.Value.Name) > 0);
                var insertIndex = itemToInsertBefore == null
                    ? SavedFilters.Count
                    : SavedFilters.IndexOf(itemToInsertBefore);

                SavedFilters.Insert(insertIndex, filterToSave.Value);
            }

            // Select it
            SelectedSavedFilter = filterToSave.Value;
        }

        private bool CanDeleteFilter(PendingClaimsFilter filter)
        {
            return !(filter == null || filter == _newPendingClaimsFilter);
        }

        private void ExecuteDeleteFilter(PendingClaimsFilter filter)
        {
            if (!CanDeleteFilter(filter)) return;

            var toDelete = _filterApplicationSettings
                .Where(a => a.Value == filter && a.Id != null)
                .Select(a => a.Id.GetValueOrDefault())
                .ToArray();

            _applicationSettingsService.DeleteSettings(toDelete);
            SavedFilters.Remove(filter);
        }

        #endregion

    }
}
