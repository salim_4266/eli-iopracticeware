﻿using System.Windows;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.PendingClaims
{
    public class PendingClaimsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public PendingClaimsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPendingClaim()
        {
            var view = new PendingClaimsView();
            _interactionManager.ShowModal(new WindowInteractionArguments
                                          {
                                              Content = view,
                                              WindowState = WindowState.Normal,
                                              ResizeMode = ResizeMode.CanResizeWithGrip
                                          });
        }
    }
}
