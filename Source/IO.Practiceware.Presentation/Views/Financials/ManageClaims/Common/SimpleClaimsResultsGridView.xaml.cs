﻿using System.Collections;
using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Financials.ManageClaims.Common
{
    /// <summary>
    /// Interaction logic for SimpleClaimsResultsGridView.xaml
    /// </summary>
    public partial class SimpleClaimsResultsGridView
    {

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IEnumerable), typeof(SimpleClaimsResultsGridView));

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(SimpleClaimsResultsGridView));

        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof (SolidColorBrush), typeof (SimpleClaimsResultsGridView));

        public static readonly DependencyProperty CanUserSelectProperty =
            DependencyProperty.Register("CanUserSelect", typeof (bool), typeof (SimpleClaimsResultsGridView));

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public IEnumerable SelectedItems
        {
            get { return (IEnumerable)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        public SolidColorBrush SelectedColor
        {
            get { return (SolidColorBrush)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }
        
        public bool CanUserSelect
        {
            get { return (bool)GetValue(CanUserSelectProperty); }
            set { SetValue(CanUserSelectProperty, value); }
        }

        
        
        public SimpleClaimsResultsGridView()
        {
            InitializeComponent();
        }

    }
}
