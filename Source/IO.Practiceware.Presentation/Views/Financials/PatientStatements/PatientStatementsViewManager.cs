﻿using System.Windows;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.PatientStatements
{
    public class PatientStatementsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public PatientStatementsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPatientStatements(int patientId)
        {
            var view = new PatientStatementsView();
            var dataContext = view.DataContext.EnsureType<PatientStatementsViewContext>();
            dataContext.PatientId = patientId;

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}
