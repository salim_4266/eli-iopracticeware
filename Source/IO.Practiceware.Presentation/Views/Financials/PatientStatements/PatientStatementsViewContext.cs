﻿using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Financials.PatientStatements;
using IO.Practiceware.Presentation.ViewServices.Financials.PatientStatements;
using Soaf.Presentation;
using System;
using System.Text;
using IO.Practiceware.Storage;
namespace IO.Practiceware.Presentation.Views.Financials.PatientStatements
{
    public class PatientStatementsViewContext : IViewContext
    {
        private readonly IPatientStatementsViewService _viewService;

        public PatientStatementsViewContext(IPatientStatementsViewService viewService)
        {
            _viewService = viewService;
            Account = new AccountViewModel();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            //Save = Command.Create<object, object>(ExecuteSave); //Command.Create(ExecuteSave);
            Save = Command.Create<object>(ExecuteSave);
            Print = Command.Create<object>(ExecutePrint);
        }

        /// <summary>
        /// Command to load the UI 
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to close the view
        /// </summary>
        public ICommand Close { get; protected set; }

        public ICommand Save { get; set; }
        public ICommand Print { get; set; }

        [DispatcherThread]
        public virtual AccountViewModel Account { get; set; }

        [DispatcherThread]
        public virtual StatementViewModel SelectedStatement { get; set; }

        public int PatientId { get; set; }
        public IInteractionContext InteractionContext { get; set; }

        private void ExecuteLoad()
        {
            if (PatientId > 0) Account = _viewService.LoadPatientStatementsInformation(PatientId);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        private void ExecuteSave(object obj)
        {
            Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
            saveDialog.InitialDirectory = System.Environment.SpecialFolder.Desktop.ToString();
            saveDialog.FileName = "Statement_" + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Minute;
            saveDialog.FilterIndex = -1;
            saveDialog.Filter = "PDF file (*.pdf)|*.pdf";
            if (this.SelectedStatement != null)
            {
                if (saveDialog.ShowDialog() == true)
                {
                    FileManager.Instance.CommitContents(saveDialog.FileName, this.SelectedStatement.Value);
                }
            }
        }

        private void ExecutePrint(object obj)
        {
            Telerik.Windows.Documents.Fixed.Print.PrintSettings setting = new Telerik.Windows.Documents.Fixed.Print.PrintSettings()
            {
                UseDefaultPrinter = true,
                DocumentName = "Statement",
                PageMargins = new System.Windows.Thickness(10)
            };
            Telerik.Windows.Controls.RadPdfViewer pdfViewer = (Telerik.Windows.Controls.RadPdfViewer)obj;
            pdfViewer.Print(setting);
        }
    }
}