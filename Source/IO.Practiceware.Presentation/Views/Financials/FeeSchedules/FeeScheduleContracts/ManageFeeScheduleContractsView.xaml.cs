﻿
using Soaf.Presentation;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Financials.FeeSchedules.FeeScheduleContracts
{
    /// <summary>
    /// Interaction logic for ManageInsurerContractsView.xaml
    /// </summary>
    public partial class ManageFeeScheduleContractsView
    {
        public ManageFeeScheduleContractsView()
        {
            InitializeComponent();
        }
    }

    public class ManageFeeScheduleContractsViewContextReference : DataContextReference
    {
        public new ManageFeeScheduleContractsViewContext DataContext
        {
            get { return (ManageFeeScheduleContractsViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
