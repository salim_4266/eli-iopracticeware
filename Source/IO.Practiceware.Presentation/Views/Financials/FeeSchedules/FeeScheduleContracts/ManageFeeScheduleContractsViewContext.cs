﻿using System;
using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts;
using Soaf.Presentation;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.FeeScheduleContracts;
using System.Linq;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.Financials.FeeSchedules.FeeScheduleContracts
{
    public class ManageFeeScheduleContractsViewContext : IViewContext
    {
        // private variables
        private readonly IInteractionManager _interactionManager;
        private readonly IFeeScheduleContractsViewService _feeScheduleContractsViewService;
        private readonly Func<FeeScheduleContractViewModel> _createFeeScheduleViewModel;


        // commands
        public ICommand Load { get; protected set; }
        public ICommand CreateNewFeeSchedule { get; protected set; }
        public ICommand DeleteFeeSchedule { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual bool ShowExpiredContracts { get; set; }

        [DispatcherThread]
        public virtual FeeScheduleContractViewModel SelectedItem { get; set; }

        [DispatcherThread]
        public virtual ManageFeeScheduleContractsViewModel ManageFeeScheduleContracts { get; set; }



        public ManageFeeScheduleContractsViewContext(IInteractionManager interactionManager, IFeeScheduleContractsViewService feeScheduleContractsViewService,
            Func<FeeScheduleContractViewModel> createFeeScheduleContractViewModel)
        {
            _interactionManager = interactionManager;
            _feeScheduleContractsViewService = feeScheduleContractsViewService;
            _createFeeScheduleViewModel = createFeeScheduleContractViewModel;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            CreateNewFeeSchedule = Command.Create(ExecuteCreateNewFeeSchedule).Async(() => InteractionContext);
            DeleteFeeSchedule = Command.Create(ExecuteDeleteFeeSchedule).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            ManageFeeScheduleContracts = _feeScheduleContractsViewService.LoadInfo();
        }

        private void ExecuteDeleteFeeSchedule()
        {
            // Confirm whether to Delete them
            bool? deleteFeeSchedule = _interactionManager.Confirm("Are you sure you wish to delete this fee schedule?", "Cancel", "Delete");

            if (deleteFeeSchedule.GetValueOrDefault())
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() => ManageFeeScheduleContracts.FeeScheduleContractsView.Contracts.Remove(SelectedItem));
            }
        }

        private void ExecuteCreateNewFeeSchedule()
        {
            if (!string.IsNullOrEmpty(ManageFeeScheduleContracts.FeeScheduleContractsView.NewFeeScheduleName))
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    var newFeeSchedule = _createFeeScheduleViewModel();
                    newFeeSchedule.Name = ManageFeeScheduleContracts.FeeScheduleContractsView.NewFeeScheduleName;
                    newFeeSchedule.StartDate = DateTime.Now.Date;
                    ManageFeeScheduleContracts.FeeScheduleContractsView.Contracts.Add(newFeeSchedule);

                    // select newly created item in grid
                    SelectedItem = newFeeSchedule;

                    // make textbox empty
                    ManageFeeScheduleContracts.FeeScheduleContractsView.NewFeeScheduleName = string.Empty;
                });
            }
        }
    }
}
