﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.FeeSchedules.ServiceFeeSchedules
{
    /// <summary>
    /// Interaction logic for ManageServicesView.xaml
    /// </summary>
    public partial class ManageServiceFeeSchedulesView
    {
        public ManageServiceFeeSchedulesView()
        {
            InitializeComponent();
        }
    }

    public class ManageServicesViewContextReference : DataContextReference
    {
        public new ManageServiceFeeSchedulesViewContext DataContext
        {
            get { return (ManageServiceFeeSchedulesViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
