﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules.Fees;
using IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.ServiceFeeSchedules;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.FeeSchedules.ServiceFeeSchedules
{
    public class ManageServiceFeeSchedulesViewContext : IViewContext
    {
        #region Private Members
        
        private readonly IManageServiceFeeSchedulesViewService _manageServiceFeeSchedulesViewService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private ApplicationSettingContainer<ProfessionalEncounterServiceSelections> _professionalEncounterServiceSelectionsApplicationSetting;
        #endregion

        public int NumServices = 200;

        [DispatcherThread]
        public virtual ExtendedObservableCollection<ServiceDetailsViewModel> Services { get; set; }

        public ObservableCollection<FeeScheduleContractViewModel> ProviderFeeSchedules { get; set; }
        public ObservableCollection<FeeScheduleContractViewModel> SelectedProviderFeeSchedules { get; set; }

        public ObservableCollection<FeeScheduleContractViewModel> InsurerFeeSchedules { get; set; }
        public ObservableCollection<FeeScheduleContractViewModel> SelectedInsurerFeeSchedules { get; set; }

        public FeeScheduleContractViewModel PracticeContract { get; set; }

        public FeeScheduleContractViewModel SelfPayContract { get; set; }

        [DispatcherThread]
        public virtual bool ShowExpired { get; set; }

        [DispatcherThread]
        public virtual ExtendedObservableCollection<NamedViewModel> EncounterServiceCategories { get; set; }

        [DispatcherThread]
        public virtual ExtendedObservableCollection<NamedViewModel> SelectedEncounterServiceCategories { get; set; }

        public ExtendedObservableCollection<NamedViewModel> UnitTypes { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand DisplayServices { get; protected set; }

        public ICommand Close { get; protected set; }

        public ManageServiceFeeSchedulesViewContext(IManageServiceFeeSchedulesViewService manageServiceFeeSchedulesViewService,
                                                    IApplicationSettingsService applicationSettingsService)
        {
            _manageServiceFeeSchedulesViewService = manageServiceFeeSchedulesViewService;
            _applicationSettingsService = applicationSettingsService;

            PracticeContract = new FeeScheduleContractViewModel { Name = "Practice", Fees = CreatePracticeFees(NumServices) };
            SelfPayContract = new FeeScheduleContractViewModel { Name = "Self Pay", Fees = CreatePracticeFees(NumServices) };

            InsurerFeeSchedules = CreateInsurerContracts(30);
            SelectedInsurerFeeSchedules = CreateInsurerContracts(0);

            ProviderFeeSchedules = CreateProviderContracts(30);
            SelectedProviderFeeSchedules = CreateProviderContracts(0);

            EncounterServiceCategories = new ExtendedObservableCollection<NamedViewModel>();
            SelectedEncounterServiceCategories = new ExtendedObservableCollection<NamedViewModel>();
            UnitTypes = new ExtendedObservableCollection<NamedViewModel> { new NamedViewModel { Id = 1, Name = "Minutes" }, new NamedViewModel { Id = 2, Name = "Units" } };
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            DisplayServices = Command.Create(ExecuteDisplayServices).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            var loadInformation = _manageServiceFeeSchedulesViewService.LoadInformation();
            EncounterServiceCategories = loadInformation.EncounterServiceCategories.ToExtendedObservableCollection();

            _professionalEncounterServiceSelectionsApplicationSetting = _applicationSettingsService.GetSetting<ProfessionalEncounterServiceSelections>(ApplicationSetting.ProfessionalEncounterServiceSelections, ApplicationContext.Current.ComputerName, UserContext.Current.UserDetails.Id);
            if (_professionalEncounterServiceSelectionsApplicationSetting != null)
            {
                ShowExpired = _professionalEncounterServiceSelectionsApplicationSetting.Value.ShowExpired;
                IList<int> categoryIds = _professionalEncounterServiceSelectionsApplicationSetting.Value.CategoryIds;
                SelectedEncounterServiceCategories = EncounterServiceCategories.Where(esc => categoryIds.Contains(esc.Id)).ToExtendedObservableCollection();
            }
        }

        private void ExecuteClose()
        {
            if (_professionalEncounterServiceSelectionsApplicationSetting == null)
            {
                _professionalEncounterServiceSelectionsApplicationSetting = new ApplicationSettingContainer<ProfessionalEncounterServiceSelections>
                                                                            {
                                                                                Name = ApplicationSetting.ProfessionalEncounterServiceSelections,
                                                                                UserId = UserContext.Current.UserDetails.Id,
                                                                                MachineName = ApplicationContext.Current.ComputerName,
                                                                                Value = new ProfessionalEncounterServiceSelections {ShowExpired = ShowExpired, CategoryIds = SelectedEncounterServiceCategories.Select(s => s.Id).ToList()}
                                                                            };
            }
            else
            {
                _professionalEncounterServiceSelectionsApplicationSetting.Value = new ProfessionalEncounterServiceSelections {ShowExpired = ShowExpired, CategoryIds = SelectedEncounterServiceCategories.Select(s => s.Id).ToList()};
            }

            _applicationSettingsService.SetSetting(_professionalEncounterServiceSelectionsApplicationSetting);
            InteractionContext.Complete(true);
        }

        private void ExecuteDisplayServices()
        {
            Services = _manageServiceFeeSchedulesViewService.LoadServices(SelectedEncounterServiceCategories, ShowExpired).ToExtendedObservableCollection();
        }
        
        public ObservableCollection<ServiceDetailsViewModel> CreateServices(int n)
        {
            var toReturn = new ObservableCollection<ServiceDetailsViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new ServiceDetailsViewModel { Code = i + i + i + i + i.ToString(), Description = "Name of service " + i, Id = i, RelativeValueUnitCode = new NamedViewModel { Name = "Test" + i, Id = i }, UnitType = new NamedViewModel { Name = "Units", Id = 1 }, Units = (Decimal)1.0, NdcCode = "Test", RevenueCode = "Test", InvoiceType = new NamedViewModel { Name = "Test" + i, Id = i }, UnclassifiedNote = "Test", StartDate = DateTime.Today, EndDate = DateTime.Today, OrderingProviderRequired = true, CliaWaiverRequired = true, AllowZeroAmount = true, ReferringProviderRequired = true, Category = new NamedViewModel{Name = "Category" + i % 10, Id = i % 10} });
            }
            return toReturn;
        }

        public ObservableCollection<FeeScheduleContractViewModel> CreateInsurerContracts(int n)
        {
            var toReturn = new ObservableCollection<FeeScheduleContractViewModel>();
            for (var i = 0; i < n; i++ )
            {
                toReturn.Add(new FeeScheduleContractViewModel { Name = "insurer" + i, Id = i, Fees = CreateInsurerFeeContracts(NumServices, i), EndDate = (i % 8 == 2) ? DateTime.Today.AddMonths(-i) : DateTime.Today.AddMonths(i) });
            }
            return toReturn;
        }

        public ObservableCollection<FeeViewModel> CreateInsurerFeeContracts(int n, int index)
        {
            var toReturn = new ObservableCollection<FeeViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new InsurerFeeViewModel { ServiceId = i, OfficeAllowable = (((i + index) * 100) % 1000), FacilityAllowable = (i % 10 != 0) ? ((i + ((Decimal)(index * 2)) * 123) % 1200) as Decimal? : null });
            }
            return toReturn;
        }

        public ObservableCollection<FeeScheduleContractViewModel> CreateProviderContracts(int n)
        {
            var toReturn = new ObservableCollection<FeeScheduleContractViewModel>();
            for (int i = 0; i < n; i++)
            {
                toReturn.Add(new FeeScheduleContractViewModel { Name = "Provider " + i, Id = i, Fees = CreatePracticeFees(NumServices) });
            }
            return toReturn;
        }

        public ObservableCollection<FeeViewModel> CreatePracticeFees(int n)
        {
            var toReturn = new ObservableCollection<FeeViewModel>();
            for (int i = 0; i < n; i++)
            {
                toReturn.Add(new PracticeFeeViewModel { ServiceId = i, Fee = (i * (Decimal)123) % 1600});
            }
            return toReturn;
        }

        public IInteractionContext InteractionContext { get; set; }
    }
}