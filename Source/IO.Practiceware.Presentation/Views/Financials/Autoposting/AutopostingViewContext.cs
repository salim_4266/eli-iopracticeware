﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Interop;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.Autoposting;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewServices.Financials.Autoposting;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Storage;
using Microsoft.WindowsAPICodePack.Dialogs;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using ApplicationContext = IO.Practiceware.Application.ApplicationContext;

namespace IO.Practiceware.Presentation.Views.Financials.Autoposting
{
    public class AutopostingViewContext : IViewContext
    {
        private readonly IApplicationSettingsService _settingsService;
        private readonly IAutopostingViewService _autopostingViewService;
        private readonly IInteractionManager _interactionManager;
        private ApplicationSettingContainer<string> _remittanceFilesSetting;

        public AutopostingViewContext(
            IApplicationSettingsService settingsService,
            IAutopostingViewService autopostingViewService,
            IInteractionManager interactionManager)
        {
            _settingsService = settingsService;
            _autopostingViewService = autopostingViewService;
            _interactionManager = interactionManager;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Upload = Command.Create(ExecuteUpload).Async(() => InteractionContext);
            LoadRemittanceAdvices = Command.Create(ExecuteLoadRemittanceAdvices).Async(() => InteractionContext);
            Post = Command.Create(ExecutePost, CanPostSelectedMessage).Async(() => InteractionContext);
            ReloadRemittances = Command.Create(ExecuteReloadRemittances).Async(() => InteractionContext);
        }

        private void ExecuteLoad()
        {
            SourcePath = GetSourcePathFromApplicationSettings();

            var loadInformation = _autopostingViewService.LoadRemittanceMessages(ShowingProcessedFiles);
            PaymentMethods = loadInformation.PaymentMethods;
            Remittances = loadInformation.Remittances;

            SelectedPaymentDate = DateTime.Now;
            SelectedPaymentMethod = PaymentMethods.FirstOrDefault(pm => pm.Name == PaymentMethod.Check);
        }

        private void ExecuteReloadRemittances()
        {
            Remittances = _autopostingViewService.LoadRemittanceMessages(ShowingProcessedFiles).Remittances;
        }

        private void ExecuteLoadRemittanceAdvices()
        {
            var selectedMessage = SelectedRemittanceMessage;
            if (selectedMessage == null || selectedMessage.AreRemittanceAdvicesLoaded) return;

            var loadedMessage = _autopostingViewService.LoadRemittanceMessageWithAdvices(selectedMessage.Id);
            UpdateMessage(selectedMessage, loadedMessage);
        }

        private bool CanPostSelectedMessage()
        {
            return SelectedRemittanceMessage != null
                && !SelectedRemittanceMessage.IsProcessed
                && SelectedRemittanceMessage.AreRemittanceAdvicesLoaded;
        }

        private void ExecutePost()
        {
            if (!CanPostSelectedMessage()) return;

            var closeDate = ApplicationSettings.Cached.GetSetting<DateTime?>(ApplicationSetting.CloseDateTime)
                .IfNotNull(s => s.Value);

            if (closeDate == null || closeDate <= SelectedPaymentDate)
            {
                var selectedMessage = SelectedRemittanceMessage;
                var postedMessage = _autopostingViewService.PostRemittanceMessage(selectedMessage.Id, SelectedPaymentDate, SelectedPaymentMethod);
                UpdateMessage(selectedMessage, postedMessage);

                var message = postedMessage.HasErrors
                    ? "Posting completed with errors."
                    : "Posting was successful.";

                _interactionManager.Alert(message);
            }
            else
            {
                _interactionManager.Alert("The posting date you have selected, {0}, is before the close date, {1}. Therefore, no changes were made. Either adjust the posting date to be any date after {1}, or adjust the close date."
                    .FormatWith(SelectedPaymentDate.ToMMDDYYString(), closeDate.ToMMDDYYString()));
            }
        }

        private void ExecuteUpload()
        {
            if (SourcePath.IsNullOrWhiteSpace()
                || !FileManager.Instance.DirectoryExists(SourcePath))
            {
                string remittancesFolder = null;
                this.Dispatcher().Invoke(() =>
                {
                    var commonOpenFileDialog = new CommonOpenFileDialog
                    {
                        Title = "Please select folder for remittances upload",
                        IsFolderPicker = true,
                        AddToMostRecentlyUsedList = false,
                        AllowNonFileSystemItems = false,
                        EnsureFileExists = true,
                        EnsurePathExists = true,
                        EnsureReadOnly = false,
                        EnsureValidNames = true,
                        Multiselect = false,
                        ShowPlacesList = true
                    };

                    if (ConfigurationManager.ClientApplicationConfiguration.RdsWithSharedAccountMode)
                    {
                        commonOpenFileDialog.DefaultDirectory = "\\\\tsclient";
                    }

                    var window = System.Windows.Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(x => x.IsActive);
                    // User selected location? -> save it
                    if ((window == null ? commonOpenFileDialog.ShowDialog() : commonOpenFileDialog.ShowDialog(new WindowInteropHelper(window).Handle)) == CommonFileDialogResult.Ok)
                    {
                        remittancesFolder = commonOpenFileDialog.FileName;
                    }
                });

                // Quit if path is still not set
                if (string.IsNullOrEmpty(remittancesFolder)) return;

                // Store updated path
                _remittanceFilesSetting.Value = SourcePath = remittancesFolder;
                _settingsService.SetSetting(_remittanceFilesSetting);
            }

            // List new remittances
            var remittances = FileManager.Instance.ListFiles(SourcePath)
                .Select((f, id) => new RemittanceMessageUploadArgument
                {
                    FileName = Path.GetFileName(f),
                    Message = FileManager.Instance.ReadContentsAsText(f),
                    PostingDate = DateTime.Now
                })
                .ToArray();
            if (!remittances.Any()) return;

            var uploadedMessages = _autopostingViewService.Upload(remittances);

            // Move remittance files
            MoveUploadedFiles(uploadedMessages);

            Remittances.AddRangeWithSinglePropertyChangedNotification(uploadedMessages);

            // Show error for files with invalid format
            var immediatelyProcessed = uploadedMessages.Where(m => m.IsProcessed).ToList();
            if (immediatelyProcessed.Any())
            {
                _interactionManager.Alert(string.Format("Following files were uploaded and marked as processed, since there was an issue reading their contents: {0}. Select them to see error details.",
                    immediatelyProcessed.Select(f => f.FileName).Join()));
            }
        }

        private static void UpdateMessage(RemittanceMessageViewModel target, RemittanceMessageViewModel updatedSource)
        {
            // Couldn't make it not blink in the grid, so just copying over fields
            target.Errors = updatedSource.Errors;
            target.IsProcessed = updatedSource.IsProcessed;
            target.RemittanceAdvices = updatedSource.RemittanceAdvices;
            target.AreRemittanceAdvicesLoaded = updatedSource.AreRemittanceAdvicesLoaded;
        }

        private void MoveUploadedFiles(IEnumerable<RemittanceMessageViewModel> uploadedMessages)
        {
            // Ensure uploaded directory
            var uploadDirectory = Path.Combine(SourcePath, @"Uploaded");
            if (!FileManager.Instance.DirectoryExists(uploadDirectory))
            {
                FileManager.Instance.CreateDirectory(uploadDirectory);
            }

            // Move uploaded files into target folder
            foreach (var uploadedMessage in uploadedMessages)
            {
                FileManager.Instance.Move(
                    Path.Combine(SourcePath, uploadedMessage.FileName),
                    Path.Combine(uploadDirectory, uploadedMessage.FileName));
            }
        }

        public IInteractionContext InteractionContext { get; set; }
        public virtual bool ShowingProcessedFiles { get; set; }
        public virtual string SourcePath { get; set; }
        public virtual ExtendedObservableCollection<RemittanceMessageViewModel> Remittances { get; set; }
        public virtual ExtendedObservableCollection<PaymentMethodViewModel> PaymentMethods { get; set; }
        public virtual RemittanceMessageViewModel SelectedRemittanceMessage { get; set; }
        public virtual PaymentMethodViewModel SelectedPaymentMethod { get; set; }
        public virtual DateTime SelectedPaymentDate { get; set; }
        public ICommand Load { get; protected set; }
        public ICommand ReloadRemittances { get; protected set; }
        public ICommand Upload { get; protected set; }
        public ICommand Post { get; protected set; }
        public ICommand LoadRemittanceAdvices { get; protected set; }

        #region Methods
        private string GetSourcePathFromApplicationSettings()
        {
            _remittanceFilesSetting = _settingsService.GetOrCreateSetting<string>(
                ApplicationSetting.RemittanceFilesPath,
                ApplicationContext.Current.ComputerName,
                UserContext.Current.UserDetails.Id);

            return _remittanceFilesSetting.Value;
        }
        #endregion
    }
}
