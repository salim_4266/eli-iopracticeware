﻿using System.Windows;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.Autoposting
{
    public class AutopostingViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public AutopostingViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowAutoposting()
        {
            var view = new AutopostingView();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}
