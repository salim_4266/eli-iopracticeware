﻿<presentation:View x:Class="IO.Practiceware.Presentation.Views.Financials.Autoposting.AutopostingView"
      xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
      xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
      xmlns:presentation="http://soaf/presentation"
      xmlns:telerik="http://schemas.telerik.com/2008/xaml/presentation"
      xmlns:autoposting="clr-namespace:IO.Practiceware.Presentation.Views.Financials.Autoposting"
      xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
      xmlns:autopostingViewModels="clr-namespace:IO.Practiceware.Presentation.ViewModels.Financials.Autoposting"
      xmlns:converters="clr-namespace:IO.Practiceware.Presentation.Views.Common.Converters"
      mc:Ignorable="d" Height="550" Width="1030" AutoCreateDataContext="True" ViewContextType="autoposting:AutopostingViewContext"
      d:DataContext="{d:DesignInstance Type=autoposting:AutopostingViewContext}"
	  Load="{Binding Load}" Header="Post Remittances">
    <presentation:View.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="..\Common\Resources.xaml"/>
            </ResourceDictionary.MergedDictionaries>
            <converters:HasValueToVisibilityConverter x:Key="HasValueToVisibilityConverter" />
            <converters:BooleanToDisplayStringConverter x:Key="BooleanToExceptionTextConverter" TrueDisplayString="Posting Exceptions" FalseDisplayString="Errors"/>
            <converters:BooleanToDisplayStringConverter x:Key="BooleanToStringConverter" TrueDisplayString="Show" FalseDisplayString="Hide"/>
            <Style TargetType="telerik:GridViewRow" d:DataContext="{d:DesignInstance Type=autopostingViewModels:RemittanceMessageViewModel}">
                <Style.Resources>
                    <converters:BooleanToObjectConverter x:Key="BooleanToGrayConverter" TrueValue="Gray" FalseValue="Black"/>
                    <converters:BooleanToObjectConverter x:Key="BooleanToItalicConverter" TrueValue="Italic" FalseValue="Normal"/>
                    <converters:BooleanToObjectConverter x:Key="BooleanToSelectedColorConverter" TrueValue="{StaticResource StandardGrayGradient}" FalseValue="{StaticResource StandardAdminGradient}"/>
                    <Style TargetType="Rectangle">
                        <Setter Property="Opacity" Value="0"/>
                        <Setter Property="Fill" Value="Transparent"/>
                    </Style>
                </Style.Resources>
                <Setter Property="Template" Value="{StaticResource LightGridViewRowTemplate}"/>
                <Setter Property="BorderThickness" Value="0,0,0,1"/>
                <Setter Property="MinHeight" Value="0"/>
                <Setter Property="Height" Value="24"/>
                <Setter Property="Margin" Value="0"/>
                <Setter Property="Foreground" Value="{Binding IsProcessed, Converter={StaticResource BooleanToGrayConverter}, FallbackValue=Gray}"/>
                <Setter Property="FontStyle" Value="{Binding IsProcessed, Converter={StaticResource BooleanToItalicConverter}}"/>
                <Style.Triggers>
                    <Trigger Property="IsSelected" Value="True">
                        <Setter Property="Background" Value="{Binding IsProcessed, Converter={StaticResource BooleanToSelectedColorConverter}}"/>
                        <Setter Property="Foreground" Value="#fafafa"/>
                        <Setter Property="BorderThickness" Value="0"/>
                    </Trigger>
                    <Trigger Property="IsEnabled" Value="False">
                        <Setter Property="FontStyle" Value="Italic"/>
                        <Setter Property="ToolTip" Value="This remittance file has already been posted."/>
                    </Trigger>
                    <Trigger Property="IsValid" Value="False">
                        <Setter Property="Background" Value="White"/>
                        <Setter Property="BorderThickness" Value="1"/>
                        <Setter Property="BorderBrush" Value="Red"/>
                    </Trigger>
                    <MultiTrigger>
                        <MultiTrigger.Conditions>
                            <Condition Property="IsValid" Value="False"/>
                            <Condition Property="IsSelected" Value="True"/>
                        </MultiTrigger.Conditions>
                        <Setter Property="Background" Value="Red"/>
                        <Setter Property="Foreground" Value="White"/>
                        <Setter Property="FontWeight" Value="SemiBold"/>
                    </MultiTrigger>
                </Style.Triggers>
            </Style>

            <Style TargetType="telerik:RadGridView">
                <Setter Property="CanUserFreezeColumns" Value="False"/>
                <Setter Property="ScrollViewer.HorizontalScrollBarVisibility" Value="Disabled"/>
                <Setter Property="IsReadOnly" Value="True"/>
            </Style>
        </ResourceDictionary>
    </presentation:View.Resources>
    <Grid>
        <Grid.Resources>
            <autoposting:AutopostingViewContextReference x:Key="ViewContext"/>
        </Grid.Resources>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
            <RowDefinition Height="40"/>
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="3"/>
            <ColumnDefinition Width="2*"/>
        </Grid.ColumnDefinitions>
        <Grid Grid.Column="0" Grid.Row="0" Background="{StaticResource StandardAdminGradient}">
            <TextBlock Text="Remittance files" Margin="5" FontSize="16" FontWeight="Light" Foreground="White" HorizontalAlignment="Left" VerticalAlignment="Center" />
            <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" Height="22">
                <TextBlock Text="Processed files:" Margin="0 0 55 0" Foreground="White" FontSize="11" FontWeight="Light" HorizontalAlignment="Right" VerticalAlignment="Center"/>
            </StackPanel>
        </Grid>
        <telerik:RadToggleButton x:Name="ProcessedToggleButton" 
                                 IsChecked="{Binding ShowingProcessedFiles, Mode=TwoWay}" 
                                 IsThreeState="False" 
                                 Grid.Row="0" Grid.Column="0" 
                                 Height="18" Width="50"
                                 Foreground="White" 
                                 Background="{StaticResource StandardFocusBrush}" 
                                 Style="{StaticResource OnOffToggleButtonStyle}" 
                                 FontSize="11" 
                                 Margin="2 4 2 0" 
                                 Command="{Binding ReloadRemittances}"
                                 Content="{Binding IsChecked, RelativeSource={RelativeSource Self}, Converter={StaticResource BooleanToStringConverter}}" 
                                 HorizontalAlignment="Right"/>        
        <TextBlock Text="Payment details" Grid.Column="2"  Grid.Row="0" Padding="5" FontSize="16" FontWeight="Light" Foreground="White" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Background="{StaticResource PaymentGradient}"/>
        
        <telerik:RadGridView x:Name="RemittancesGridView" IsReadOnly="True" CanUserSelect="True" Grid.Row="1" Grid.Column="0" ShowGroupPanel="False" RowIndicatorVisibility="Collapsed" AutoGenerateColumns="False" SelectionMode="Single" SelectionUnit="FullRow" ColumnWidth="*" ItemsSource="{Binding Remittances}" SelectedItem="{Binding SelectedRemittanceMessage, Mode=TwoWay}">
            <telerik:RadGridView.FilterDescriptors>
                <telerik:CompositeFilterDescriptor LogicalOperator="Or">
                    <telerik:FilterDescriptor Member="IsProcessed" Operator="IsEqualTo" Value="{Binding Path=DataContext.ShowingProcessedFiles, Source={StaticResource ViewContext}, Mode=OneWay}"/>
                    <telerik:FilterDescriptor Member="IsProcessed" Operator="IsEqualTo" Value="False"/>
                </telerik:CompositeFilterDescriptor>
            </telerik:RadGridView.FilterDescriptors>
            <telerik:RadGridView.SortDescriptors>
                <telerik:SortDescriptor Member="PostingDate" SortDirection="Descending" />
            </telerik:RadGridView.SortDescriptors>
            <i:Interaction.Triggers>
                <i:EventTrigger EventName="SelectionChanged" SourceName="RemittancesGridView">
                    <i:InvokeCommandAction Command="{Binding LoadRemittanceAdvices}"/>
                </i:EventTrigger>
            </i:Interaction.Triggers>
            <telerik:RadGridView.Columns>
                <telerik:GridViewDataColumn Header="File Name" IsFilterable="False" IsSortable="True">
                    <telerik:GridViewDataColumn.CellTemplate>
                        <DataTemplate DataType="autopostingViewModels:RemittanceMessageViewModel">
                            <TextBlock Text="{Binding FileName}"/>
                        </DataTemplate>
                    </telerik:GridViewDataColumn.CellTemplate>
                </telerik:GridViewDataColumn>
                <telerik:GridViewDataColumn DataMemberBinding="{Binding PostingDate, StringFormat='{}{0:MM/dd/yyyy}', FallbackValue='---'}" IsFilterable="False" IsSortable="True" Width="Auto" Header="Posting Date"/>
            </telerik:RadGridView.Columns>
        </telerik:RadGridView>

        <GridSplitter Grid.Row="1" Grid.Column="1" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Cursor="SizeWE" IsHitTestVisible="True"/>

        <Border Grid.Row="2" Background="{StaticResource BorderFillBrush}" BorderThickness="0 1 0 0" BorderBrush="{StaticResource BorderBrush}" Grid.ColumnSpan="3" Grid.Column="0" />
        
        <Grid Grid.Row="1" Grid.Column="2">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="152*"/>
                <ColumnDefinition Width="533*"/>
            </Grid.ColumnDefinitions>
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <Grid Grid.ColumnSpan="2">
                <Grid.Resources>
                    <Style TargetType="telerik:GridViewRow">
                        <Style.Resources>
                                <Style TargetType="Rectangle">
                                <Setter Property="Opacity" Value="0"/>
                            </Style>
                        </Style.Resources>
                        <Setter Property="Template" Value="{StaticResource LightGridViewRowTemplate}"/>
                        <Setter Property="BorderThickness" Value="0,0,0,1"/>
                        <Setter Property="MinHeight" Value="0"/>
                        <Setter Property="Height" Value="24"/>
                        <Setter Property="Margin" Value="0"/>
                        <Setter Property="Foreground" Value="Black"/>
                        <Style.Triggers>
                            <Trigger Property="IsSelected" Value="True">
                                <Setter Property="Background" Value="{StaticResource StandardClaimGradient}"/>
                                <Setter Property="Foreground" Value="#fafafa"/>
                                <Setter Property="BorderThickness" Value="0"/>
                            </Trigger>
                            <Trigger Property="IsValid" Value="False">
                                <Setter Property="Background" Value="White"/>
                                <Setter Property="Foreground" Value="Black"/>
                                <Setter Property="BorderThickness" Value="1"/>
                                <Setter Property="BorderBrush" Value="Red"/>
                            </Trigger>
                            <MultiTrigger>
                                <MultiTrigger.Conditions>
                                    <Condition Property="IsValid" Value="False"/>
                                    <Condition Property="IsSelected" Value="True"/>
                                </MultiTrigger.Conditions>
                                <Setter Property="Background" Value="Red"/>
                                <Setter Property="Foreground" Value="White"/>
                                <Setter Property="FontWeight" Value="SemiBold"/>
                            </MultiTrigger>
                        </Style.Triggers>
                    </Style>
                </Grid.Resources>
                <telerik:RadGridView IsReadOnly="True" 
                                     MaxHeight="150" 
                                     HorizontalAlignment="Stretch" 
                                     ShowGroupPanel="False" 
                                     RowIndicatorVisibility="Collapsed" 
                                     AutoGenerateColumns="False" 
                                     SelectionMode="Single" 
                                     SelectionUnit="FullRow" 
                                     ColumnWidth="*" 
                                     ItemsSource="{Binding SelectedRemittanceMessage.RemittanceAdvices}">
                    <telerik:RadGridView.Columns>
                        <telerik:GridViewDataColumn DataMemberBinding="{Binding InsurerName}" Header="Payer"/>
                        <telerik:GridViewDataColumn DataMemberBinding="{Binding RemittanceDate, StringFormat='{}{0:MM/dd/yyyy}'}" Header="Check Date"/>
                        <telerik:GridViewDataColumn DataMemberBinding="{Binding RemittanceCode}" Header="Reference Number"/>
                        <telerik:GridViewDataColumn DataMemberBinding="{Binding TotalAmount}" Header="Amount"/>
                    </telerik:RadGridView.Columns>
                </telerik:RadGridView>
            </Grid>

            <Grid Grid.Column="0" Grid.Row="1" Background="{StaticResource PaymentColor}" Margin="0 5 0 0" Grid.ColumnSpan="2">
                <TextBlock Text="{Binding SelectedRemittanceMessage.IsProcessed, Converter={StaticResource BooleanToExceptionTextConverter}}" Margin="5 2" FontSize="12" FontWeight="Normal" Foreground="White" HorizontalAlignment="Stretch" VerticalAlignment="Stretch"/>
            </Grid>

            <Border Grid.Column="0" BorderThickness="1" Grid.Row="2" Grid.ColumnSpan="2">
                <telerik:RadWatermarkTextBox IsReadOnly="True"  ScrollViewer.VerticalScrollBarVisibility="Auto"
                                             WatermarkContent="No errors or exceptions detected"  VerticalAlignment="Stretch"
                                             Text="{Binding SelectedRemittanceMessage.AllMessages, Mode=OneWay}" 
                                             TextAlignment="Left" VerticalContentAlignment="Top" TextWrapping="Wrap"/>
            </Border>
        </Grid>

        <Border Background="{StaticResource StandardFocusBrush}" CornerRadius="0 0 10 10" Grid.Row="2" Grid.Column="0">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>
                <Button Grid.Column="1" Style="{StaticResource ActionButtonStyle}" Command="{Binding Upload}" Content="Upload Files" Margin="10 0" HorizontalAlignment="Right" Height="30" Width="90"/>
                <telerik:RadWatermarkTextBox Grid.Column="0" VerticalAlignment="Center" WatermarkContent="Enter remittance files location..." Text="{Binding SourcePath}" Margin="10 0 0 0"/>
            </Grid>
        </Border>

        <Button Name="PostButton" Style="{StaticResource ActionButtonStyle}" Grid.Row="2" Grid.Column="2" HorizontalAlignment="Right" VerticalAlignment="Center" Height="30" Width="90" Margin="10 0" Content="Post" 
                IsEnabled="{Binding SelectedRemittanceMessage, Converter={StaticResource HasValueToVisibilityConverter}}">
            <Button.Command>
                <presentation:ConfirmCommand Command="{Binding Post}" OkButtonContent="Post" CancelButtonContent="Cancel">
                    <presentation:ConfirmCommand.Content>
                        <!-- ReSharper disable once Xaml.RedundantResource -->
                        <DataTemplate>
                            <Grid DataContext="{Binding DataContext, Source={StaticResource ViewContext}}">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto"/>
                                    <ColumnDefinition Width="*"/>
                                </Grid.ColumnDefinitions>
                                <Grid.RowDefinitions>
                                    <RowDefinition Height="*"/>
                                    <RowDefinition Height="Auto"/>
                                    <RowDefinition Height="*"/>
                                    <RowDefinition Height="Auto"/>
                                    <RowDefinition Height="Auto"/>
                                    <RowDefinition Height="*"/>
                                </Grid.RowDefinitions>
                                <TextBlock Grid.Column="0" Grid.Row="3" Text="Payment method:" VerticalAlignment="Center" />
                                <telerik:RadComboBox Grid.Column="1" FontFamily="Segoe UI" Grid.Row="3" Margin="2 5" ItemsSource="{Binding PaymentMethods}" SelectedItem="{Binding SelectedPaymentMethod}"/>
                                <TextBlock Grid.Column="0" Grid.Row="4" Text="Posting date:" VerticalAlignment="Center"/>
                                <telerik:RadDatePicker Grid.Column="1" Grid.Row="4" Margin="2 5" SelectedDate="{Binding SelectedPaymentDate, Mode=TwoWay}" />
                            </Grid>
                        </DataTemplate>
                    </presentation:ConfirmCommand.Content>
                </presentation:ConfirmCommand>
            </Button.Command>
        </Button>

    </Grid>
</presentation:View>
