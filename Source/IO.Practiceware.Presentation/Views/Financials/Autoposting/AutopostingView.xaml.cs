﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Financials.Autoposting
{
    /// <summary>
    /// Interaction logic for AutopostingView.xaml
    /// </summary>
    public partial class AutopostingView
    {
        public AutopostingView()
        {
            InitializeComponent();
        }
    }

    public class AutopostingViewContextReference : DataContextReference
    {
        public new AutopostingViewContext DataContext
        {
            get { return (AutopostingViewContext) base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
