﻿using Aspose.Words;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Storage;
using O2S.Components.PDFRender4NET.Printing;
using O2S.Components.PDFRender4NET.WPF;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Document = Aspose.Words.Document;
using Orientation = Aspose.Words.Orientation;

namespace IO.Practiceware.Presentation.Views.Documents
{
    public class DocumentViewManager
    {
        private readonly IEncounterDocumentService _encounterDocumentService;
        private readonly Lazy<IPracticeRepository> _practiceRepository;
        private readonly IPrintingService _printingService;

        /// <summary>
        /// Set the title of the current window to get print dialog to show as a modal on top of that window.
        /// </summary>
        public string HeaderTitleOfCurrentWindow { private get; set; }

        static DocumentViewManager()
        {
            var license = new License();
            license.SetLicense(
                (typeof(IDocumentGenerationService).Assembly.GetManifestResourceStream(
                    "IO.Practiceware.Services.Documents.Aspose.Words.lic")));
        }

        public DocumentViewManager(Func<IPracticeRepository> practiceRepository, IPrintingService printingService, IEncounterDocumentService encounterDocumentService)
        {
            _printingService = printingService;
            _practiceRepository = Lazy.For(practiceRepository);
            _encounterDocumentService = encounterDocumentService;
        }

        public void ViewDocument(string filePath)
        {
            // Note: saving after viewing was a default behavior before introducing File Redirection
            ViewDocument(filePath, true);
        }

        public void ViewDocument(string filePath, bool saveAfterViewing)
        {
            try
            {
                var localFilePath = FileManager.Instance.Read(filePath);
                var fileExtension = Path.GetExtension(localFilePath);

                if (new[] { ".pdf", ".htm" }
                    .All(ex => !ex.Equals(fileExtension, StringComparison.OrdinalIgnoreCase)))
                {
                    RewriteAsWordBinaryDocument(localFilePath);
                }

                if (saveAfterViewing)
                {
                    // Open document and persist any changes made to it
                    Processes
                        .RunProcessAsync(localFilePath)
                        .ContinueWith(t => FileManager.Instance.Commit(filePath));
                }
                else
                {
                    // Save the rewrite of it as binary document
                    FileManager.Instance.Commit(filePath);

                    // Open it
                    Process.Start(localFilePath);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(new Exception("Could not view document", ex).ToString());
                InteractionManager.Current.Alert("Could not view document: {0}".FormatWith(ex.Message));
            }
        }

        /// <summary>
        /// Prints a list of documents. Will try to print to the correct machine based on print configuration and the type of document.
        /// </summary>
        /// <param name="documentName"></param>
        /// <param name="filePaths"></param>
        public void PrintDocuments(string documentName, string[] filePaths)
        {
            if (filePaths.IsNullOrEmpty()) return;
            var settings = TryGetPrinterName(documentName);
            if (settings == null) return;

            var printerName = settings.PrinterName;
            printerName = Printers.TryResolvePrinterName(printerName);

            // if at this point they still don't have a printer, then just return.
            if (printerName.IsNullOrEmpty()) return;

            short noOfCopies = settings.Copies;
            var fileInfos = FileManager.Instance.GetFileInfos(filePaths);

            var allPdfs = fileInfos.Where(x => x.Extension.Equals(".pdf", StringComparison.OrdinalIgnoreCase)).ToList();
            if (allPdfs.IsNotNullOrEmpty())
            {
                TryPrintPdfs(allPdfs, printerName, noOfCopies);
            }

            foreach (var file in fileInfos.Where(x => !x.Extension.Equals(".pdf", StringComparison.OrdinalIgnoreCase)))
            {
                TryPrintDocument(file, printerName, noOfCopies);
            }
        }

        /// <summary>
        /// Prints a document. Will try to print to the correct machine based on print configuration and the type of document.
        /// </summary>
        /// <param name="documentName">Name of the document.</param>
        /// <param name="content">The content.</param>
        /// <param name="extension">The extension.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <returns></returns>
        public string PrintDocument(string documentName, byte[] content, string extension, string printerName = null)
        {
            var tempPath = FileManager.Instance.GetTempPathName();
            var path = !extension.IsNullOrEmpty() ? tempPath.Replace(".tmp", extension.Substring(1) == "." ? extension : @"." + extension) : tempPath;
            try
            {
                FileManager.Instance.CommitContents(path, content);
                return PrintDocument(documentName, path, printerName);
            }
            finally
            {
                FileManager.Instance.Delete(path);
            }
        }

        /// <summary>
        /// Prints a document. Will try to print to the correct machine based on print configuration and the type of document.
        /// </summary>
        /// <param name="documentName">Name of the document.</param>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        public void PrintDocument(string documentName, string filePath)
        {
            PrintDocument(documentName, filePath, null);
        }

        /// <summary>
        /// Prints a document. Will try to print to the correct machine based on print configuration and the type of document.
        /// </summary>
        /// <param name="documentName">Name of the document.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <returns></returns>
        public string PrintDocument(string documentName, string filePath, string printerName)
        {
            short noOfCopies = 1;
            if (printerName.IsNullOrEmpty())
            {
                var settings = TryGetPrinterName(documentName);
                if (settings == null) return string.Empty;

                printerName = settings.PrinterName;
                printerName = Printers.TryResolvePrinterName(printerName);

                noOfCopies = settings.Copies;
            }

            Trace.TraceInformation("Printing document {0} with printer {1}.", documentName, printerName);

            // if at this point they still don't have a printer, then just return.
            if (printerName.IsNullOrEmpty()) return printerName;

            var toPrintFileInfo = FileManager.Instance.GetFileInfo(filePath);
            if (toPrintFileInfo.Extension.Equals(".pdf", StringComparison.OrdinalIgnoreCase)) // special case for pdf's
            {
                TryPrintPdfs(new[] { toPrintFileInfo }, printerName, noOfCopies);
                return printerName;
            }
            if (toPrintFileInfo.Extension.Equals(".htm", StringComparison.OrdinalIgnoreCase)) // special case for html files
            {
                if (!TryPrintFileUsingNewProcess(toPrintFileInfo, printerName, noOfCopies))
                {
                    Trace.TraceError("Could not print document. See previous errors. Document: {0}. File {1}.".FormatWith(toPrintFileInfo.Name, toPrintFileInfo.FullName));
                }
                return printerName;
            }

            TryPrintDocument(toPrintFileInfo, printerName, noOfCopies);
            return printerName;
        }

        /// <summary>
        /// Ensures that passed in document is saved in Word binary format (.doc). It is compatible with older versions of Office
        /// </summary>
        /// <param name="localFilePath"></param>
        private static void RewriteAsWordBinaryDocument(string localFilePath)
        {
            // Aspose can't re-save same file, so create a copy (this is more performant than loading into MemoryStream)
            var tempFileCopy = FileSystem.GetTempPathName();
            FileSystem.TryCopyFile(localFilePath, tempFileCopy);

            // Resave back to original file
            var document = new Document(tempFileCopy);
            document.Save(localFilePath, SaveFormat.Doc);

            // Delete temp file
            FileSystem.TryDeleteFile(tempFileCopy);
        }

        private PrinterSettings TryGetPrinterName(string documentName)
        {
            string printerName = null;

            if (PrinterConfiguration.DefaultPrinterConfiguration)
            {
                using (var server = new LocalPrintServer())
                {
                    printerName = server.DefaultPrintQueue.IfNotNull(q => q.FullName);
                }
            }
            else if (!PrinterConfiguration.OverridePrinterConfiguration)
            {
                printerName = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => _printingService.GetPrinterUncPath(Path.GetFileName(documentName), UserContext.Current.ServiceLocation.Item2)).Result;
            }

            if (printerName.IsNullOrEmpty()) // try getting the printer settings from a popup dialog
            {
                var settings = GetPrinterSettingsFromDialog();
                if (settings == null)
                {
                    return null;
                }
                return settings;
            }

            return new PrinterSettings { PrinterName = printerName };
        }


        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern long SetDefaultPrinter(string pszPrinter);

        private static void TryPrintDocument(FileInfoRecord toPrintFileInfo, string printerName, short noOfCopies)
        {
            if (!toPrintFileInfo.Exists)
            {
                return;
            }

            if (TryPrintTextFile(toPrintFileInfo, printerName, noOfCopies)) return;
            if (TryPrintImageFile(toPrintFileInfo, printerName, noOfCopies)) return;
            if (TryPrintAsposeDocument(toPrintFileInfo, printerName, noOfCopies)) return;
            if (!TryPrintFileUsingNewProcess(toPrintFileInfo, printerName, noOfCopies))
            {
                Trace.TraceError("Could not print document. See previous errors. Document: {0}. File {1}. Printer {2}.".FormatWith(toPrintFileInfo.Name, toPrintFileInfo.FullName, printerName));
            }
        }

        /// <summary>
        /// Prints a file by running the file in a new adobe reader process with the print verb. Returns false and logs an error in case of failure.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        /// <returns></returns>
        private static bool TryPrintFileUsingNewProcess(FileInfoRecord file, string printerName, short noOfCopies)
        {
            try
            {
                PrintFileUsingNewProcess(file, printerName, noOfCopies);
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Prints a file by running the file in a new process with the print verb.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        private static void PrintFileUsingNewProcess(FileInfoRecord file, string printerName, short noOfCopies)
        {
            if (file == null || printerName.IsNullOrEmpty()) return;

            try
            {
                for (int i = 0; i < noOfCopies; i++)
                {
                    using (var process = new Process())
                    {
                        var readFileLocation = FileManager.Instance.Read(file.FullName);
                        process.StartInfo.FileName = readFileLocation;
                        process.StartInfo.Verb = "Print";
                        process.StartInfo.CreateNoWindow = true;
                        process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;


                        if (!string.IsNullOrEmpty(printerName))
                            process.StartInfo.Arguments = string.Format("/D:\"{0}\" \"{1}\"", printerName,
                                readFileLocation);
                        process.Start();
                        process.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                var applicationException = new ApplicationException("Error printing {0} to {1}.".FormatWith(file.FullName, printerName), ex);
                if (!string.IsNullOrWhiteSpace(printerName))
                {
                    Trace.TraceError(applicationException.ToString());
                }
                else throw applicationException;
            }
        }

        /// <summary>
        /// Tries to print a text document (txt file).
        /// </summary>
        /// <param name="file">To print file info.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        /// <returns></returns>
        private static bool TryPrintTextFile(FileInfoRecord file, string printerName, short noOfCopies)
        {
            if (!".txt".Equals(file.Extension, StringComparison.OrdinalIgnoreCase)) return false;

            try
            {
                var builder = new DocumentBuilder();
                builder.Write(FileManager.Instance.ReadContentsAsText(file.FullName));
                return TryPrintAsposeDocument(builder.Document, printerName, noOfCopies);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Tries to print an image. Returns false if not an image.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        /// <returns></returns>
        private static bool TryPrintImageFile(FileInfoRecord file, string printerName, short noOfCopies)
        {
            try
            {
                var builder = new DocumentBuilder();
                builder.InsertImage(FileManager.Instance.ReadContents(file.FullName));
                builder.PageSetup.Orientation = Orientation.Landscape;
                return TryPrintAsposeDocument(builder.Document, printerName, noOfCopies);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Tries to print an aspose compatible document. Returns false if not compatible.
        /// </summary>
        /// <param name="file">To print file info.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        /// <returns></returns>
        private static bool TryPrintAsposeDocument(FileInfoRecord file, string printerName, short noOfCopies)
        {
            try
            {
                using (var ms = new MemoryStream(FileManager.Instance.ReadContents(file.FullName)))
                {
                    return TryPrintAsposeDocument(new Document(ms), printerName, noOfCopies);
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Tries to print an aspose document object to the specified printer. Returns false and writes to log if an error occurs.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        private static bool TryPrintAsposeDocument(Document document, string printerName, short noOfCopies)
        {
            try
            {
                PrintAsposeDocument(document, printerName, noOfCopies);
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Prints an aspose document object to the specified printer.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="printerName">Name of the printer.</param>
        /// <param name="noOfCopies">Number of Copies.</param>
        private static void PrintAsposeDocument(Document document, string printerName, short noOfCopies)
        {
            if (document == null || printerName == null) return;

            try
            {
                for (int i = 0; i < noOfCopies; i++)
                {
                    XpsPrintHelper.Print(document, printerName, null, true);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error printing to {0}.".FormatWith(printerName), ex);
            }
        }

        private static void TryPrintPdfs(IEnumerable<FileInfoRecord> files, string printerName, short noOfCopies)
        {

            using (var printServer = new LocalPrintServer())
            using (var queueToUse = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections }).FirstOrDefault(q => q.FullName == printerName) ?? printServer.DefaultPrintQueue)
            {
                var nameOfCurrentDefaultPrinter = printServer.DefaultPrintQueue.FullName;
                bool needToChangeDefaultPrinter = queueToUse.FullName != nameOfCurrentDefaultPrinter;
                try
                {

                    if (needToChangeDefaultPrinter)
                    {
                        // manually set the printer we're trying to print as the default printer for the user's computer.
                        // We do this because for some reason certain settings (like which printer tray to use) are only respected when using the default printer.
                        SetDefaultPrinter(queueToUse.FullName);
                        printServer.Refresh();
                    }

                    foreach (var file in files)
                    {
                        if (!TryPrintPdf(file, printServer.DefaultPrintQueue, noOfCopies))
                        {
                            Trace.TraceError("Could not print document.  Document: {0}. File {1}. Printer {2}.".FormatWith(file.Name, file.FullName, printerName));
                        }
                    }
                }
                finally
                {
                    if (needToChangeDefaultPrinter) // make sure to set back to original default printer
                    {
                        SetDefaultPrinter(nameOfCurrentDefaultPrinter);
                        printServer.Refresh();
                    }
                }
            }
        }

        private static bool TryPrintPdf(FileInfoRecord file, PrintQueue printQueue, short noOfCopies)
        {
            if (file == null || printQueue == null) return false;

            try
            {
                using (PDFFile pdfFile = FileManager.Instance.ReadAs(file.FullName, PDFFile.Open))
                {
                    // setting this to unknown will cause the printer to use whatever tray is set as the default in the user's local printer settings 
                    printQueue.DefaultPrintTicket.InputBin = InputBin.Unknown;
                    printQueue.DefaultPrintTicket.CopyCount = noOfCopies;
                    pdfFile.SerialNumber = Constants.Pdf4RenderSerialNumber;
                    var pdfPrintSettings = new PDFPrintSettings(printQueue);

                    pdfPrintSettings.PageScaling = PageScaling.None;
                    pdfFile.Print(pdfPrintSettings);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Gets the printer settings for the specified printer name. If the printer name is null or empty, it prompts the user to choose a printer.
        /// </summary>
        /// <param name="document">An optional Aspose document which will be used to set the Max and ToPage of the printer settings</param>
        /// <param name="printerName">Optional.  If supplied, a dialog will not be presented. Instead, a new printersettings object will be created and returned.</param>
        /// <param name="showDialogModal">A flag that specifies whether or not to show the dialog (if it is shown) in modal mode. The default is True.</param>
        /// <returns></returns>
        public PrinterSettings GetPrinterSettingsFromDialog(Document document = null, string printerName = null, bool showDialogModal = true)
        {
            WpfWin32Window win = null;
            if (showDialogModal)
            {
                // try to grab the current active window and wrap it in a helper class which provides its window handle.
                // we do this so we can pass the current window as the parent window when showing the dialog in modal mode.
                foreach (var w in System.Windows.Application.Current.Windows)
                {
                    var window = w as System.Windows.Window;
                    if (window == null) continue;

                    if (window.IsActive || window.Title.Equals(HeaderTitleOfCurrentWindow))
                    {
                        win = new WpfWin32Window(window);
                        break;
                    }
                }
            }

            var printerSettings = new PrinterSettings();
            if (document != null)
            {
                printerSettings.FromPage = 1;
                printerSettings.MinimumPage = 1;
                printerSettings.MaximumPage = document.PageCount;
                printerSettings.ToPage = document.PageCount;
            }

            if (printerName.IsNotNullOrEmpty())
            {
                printerSettings.PrinterName = printerName;
                return printerSettings;
            }

            var dialog = new PrintDialog { AllowSomePages = true, PrinterSettings = printerSettings };

            var dialogResult = win != null ? dialog.ShowDialog(win) : dialog.ShowDialog();

            return dialogResult == DialogResult.OK ? dialog.PrinterSettings : null;
        }

        /// <summary>
        /// Views the selected ImageApplication as determined by the parameter, externalApplicationName.
        /// </summary>
        /// <param name="externalApplicationName">The name of the external application to launch.</param>
        /// <param name="namedExternalApplicationArguments">All arguments that are passed to the image application.</param>
        public void ViewImageApplication(string externalApplicationName, Hashtable namedExternalApplicationArguments)
        {
            string userId = (UserContext.Current.UserDetails != null) ? UserContext.Current.UserDetails.Id.ToString() : null;
            ExternalApplication externalApplication = _practiceRepository.Value.ExternalApplications.First(ea => ea.ApplicationName.ToUpper() == externalApplicationName.ToUpper() &&
                                                                                                                 (ea.UserName == userId || ea.User == null));

            var imageAppProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = externalApplication.ApplicationPath,
                    Arguments = GetExternalApplicationArguments(externalApplication, namedExternalApplicationArguments),
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            try
            {
                imageAppProcess.Start();
            }
            catch (Exception ex)
            {
                string message = string.Format("Could not start the image application \"{0}\", at \"{1}\", whose arguments are \"{2}\"", externalApplicationName, imageAppProcess.StartInfo.FileName, imageAppProcess.StartInfo.Arguments);
                throw new Exception(message, ex);
            }
        }

        public string GetExternalApplicationArguments(ExternalApplication externalApplication, Hashtable namedArgumentValues)
        {
            if (namedArgumentValues.Contains("patientId"))
            {
                int id;
                if (int.TryParse(namedArgumentValues["patientId"].ToString().Trim(), out id))
                {
                    Patient p = _practiceRepository.Value.Patients.WithId(id);
                    if (p != null)
                    {
                        if (!namedArgumentValues.ContainsKey("firstName"))
                        {
                            namedArgumentValues.Add("firstName", p.FirstName);
                        }
                        if (!namedArgumentValues.ContainsKey("lastName"))
                        {
                            namedArgumentValues.Add("lastName", p.LastName);
                        }
                    }
                }
            }

            var argumentString = externalApplication.ArgumentFormatString;
            foreach (var key in namedArgumentValues.Keys)
            {
                if (argumentString.Contains(key.ToString()))
                {
                    argumentString = argumentString.Replace("{" + key + "}", namedArgumentValues[key].ToString());
                }
            }

            return argumentString;
        }

        /// <summary>
        ///     Prints Encounter documents (like Exam Documents and Surgery Documents)
        /// </summary>
        /// <param name="encounterId">EncounterId</param>
        /// <returns></returns>
        public void PrintEncounterDocuments(int encounterId)
        {
            IEnumerable<BinaryDocument> documents = _encounterDocumentService.CreateDocuments(encounterId);
            foreach (BinaryDocument document in documents)
            {
                string mergeFilePath = FileManager.Instance.GetTempPathName();
                FileManager.Instance.CommitContents(mergeFilePath, document.Content);
                PrintDocument(document.Name, mergeFilePath); // uses existing printing fanout code
                FileManager.Instance.Delete(mergeFilePath);
            }
        }
    }
}