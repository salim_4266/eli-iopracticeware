using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Exam
{
	/// <summary>
	/// Class used to call Surgery Control 
	/// </summary>
	public class ExamViewManager
	{
		private readonly Func<SurgeryViewPresenter> _createSurgeryViewPresenter;

		private readonly IInteractionManager _interactionManager;
		static ExamViewManager()
		{

		}

		public ExamViewManager(Func<SurgeryViewPresenter> createSurgeryViewPresenter, IInteractionManager interactionManager)
		{
			_createSurgeryViewPresenter = createSurgeryViewPresenter;
			_interactionManager = interactionManager;
		}

		/// <summary>
		/// Shows the surgery screen.
		/// </summary>
		/// <param name="appointmentId">The app id.</param>
		public void ShowSurgeryScreen(int appointmentId)
		{
			SurgeryView control = new SurgeryView();
			control.Presenter = _createSurgeryViewPresenter();
			control.Presenter.AppointmentId = appointmentId;
			_interactionManager.ShowModal(new WindowInteractionArguments {
				Content = control,
				WindowState = WindowState.Maximized,
				DialogButtons = DialogButtons.Ok
			});
		}

	}
}
