using Soaf.Collections;
using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class SurgeryView : SurgeryViewBase
	{

		//Required by the Windows Form Designer

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.SurgeryTab = new Soaf.Presentation.Controls.WindowsForms.TabControl();
            SurgeryTab.TabStripPlacement = System.Windows.Controls.Dock.Bottom;
			this.AssessTabPage = new System.Windows.Forms.UserControl();
			this.HtWtTabPage = new System.Windows.Forms.UserControl();
			this.CurrMedsTabPage = new System.Windows.Forms.UserControl();
			this.AllergyTabPage = new System.Windows.Forms.UserControl();
			this.WorkupTabPage = new System.Windows.Forms.UserControl();
			this.ORTabPage = new System.Windows.Forms.UserControl();
			this.PostOPTabPage = new System.Windows.Forms.UserControl();
			this.Label11 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.BottomNoteText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Panel2 = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.Panel3 = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.EyeContextList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
			this.Label2 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.SurgeryTypeList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
			this.TopNoteText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.MyProvider = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.MyPatDOB = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.MyPatFullName = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.PatientPicture = new System.Windows.Forms.PictureBox();
			this.Panel1 = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.Panel4 = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.SurgeryTab.SuspendLayout();
			this.Panel2.SuspendLayout();
			this.Panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.PatientPicture).BeginInit();
			this.SuspendLayout();
			//
			//SurgeryTab
			//
			this.SurgeryTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SurgeryTab.Location = new System.Drawing.Point(10, 93);
			this.SurgeryTab.Name = "SurgeryTab";
			this.SurgeryTab.SelectedItem = this.AssessTabPage;
			this.SurgeryTab.Size = new System.Drawing.Size(984, 533);
			this.SurgeryTab.TabIndex = 95;		  
			//
			//AssessTabPage
			//
			this.AssessTabPage.Font = new System.Drawing.Font("Tahoma", 8.25f);
			this.AssessTabPage.Name = "AssessTabPage";
			this.AssessTabPage.Size = new System.Drawing.Size(977, 489);
			this.AssessTabPage.Text = "Health-Assessment";
			//
			//HtWtTabPage
			//
			this.HtWtTabPage.Font = new System.Drawing.Font("Tahoma", 8.25f);
			this.HtWtTabPage.Name = "HtWtTabPage";
			this.HtWtTabPage.Size = new System.Drawing.Size(977, 489);
			this.HtWtTabPage.Text = "Height-Weight";
			//
			//CurrMedsTabPage
			//
			this.CurrMedsTabPage.Name = "CurrMedsTabPage";
			this.CurrMedsTabPage.Size = new System.Drawing.Size(977, 489);
			this.CurrMedsTabPage.Text = "Current Meds    ";
			//
			//AllergyTabPage
			//
			this.AllergyTabPage.Name = "AllergyTabPage";
			this.AllergyTabPage.Size = new System.Drawing.Size(977, 489);
			this.AllergyTabPage.Text = "Allergies      ";
			//
			//WorkupTabPage
			//
			this.WorkupTabPage.Name = "WorkupTabPage";
			this.WorkupTabPage.Size = new System.Drawing.Size(977, 489);
			this.WorkupTabPage.Text = "Work-up      ";
			//
			//ORTabPage
			//
			this.ORTabPage.Name = "ORTabPage";
			this.ORTabPage.Size = new System.Drawing.Size(977, 489);
			this.ORTabPage.Text = "Operating-Room";
			//
			//PostOPTabPage
			//
			this.PostOPTabPage.Name = "PostOPTabPage";
			this.PostOPTabPage.Size = new System.Drawing.Size(977, 489);
			this.PostOPTabPage.Text = "Post-OP      ";
			//
			//Label11
			//
			this.Label11.Location = new System.Drawing.Point(22, 3);
			this.Label11.Name = "Label11";
			this.Label11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.Label11.Size = new System.Drawing.Size(57, 50);
			this.Label11.TabIndex = 10;
			this.Label11.Text = "Notes";
			//
			//BottomNoteText
			//
			this.BottomNoteText.Location = new System.Drawing.Point(85, 3);
			this.BottomNoteText.Name = "BottomNoteText";
			this.BottomNoteText.Size = new System.Drawing.Size(818, 50);
			this.BottomNoteText.TabIndex = 11;
			//
			//Panel2
			//
			this.Panel2.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.Panel2.Controls.Add(this.BottomNoteText);
			this.Panel2.Controls.Add(this.Label11);
			this.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.Panel2.Location = new System.Drawing.Point(10, 626);
			this.Panel2.Name = "Panel2";
			this.Panel2.Size = new System.Drawing.Size(984, 60);
			this.Panel2.TabIndex = 1;
			//
			//Panel3
			//
			this.Panel3.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.Panel3.Controls.Add(this.EyeContextList);
			this.Panel3.Controls.Add(this.Label2);
			this.Panel3.Controls.Add(this.Label1);
			this.Panel3.Controls.Add(this.SurgeryTypeList);
			this.Panel3.Controls.Add(this.TopNoteText);
			this.Panel3.Controls.Add(this.MyProvider);
			this.Panel3.Controls.Add(this.MyPatDOB);
			this.Panel3.Controls.Add(this.MyPatFullName);
			this.Panel3.Controls.Add(this.PatientPicture);
			this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.Panel3.Location = new System.Drawing.Point(10, 0);
			this.Panel3.Name = "Panel3";
			this.Panel3.Size = new System.Drawing.Size(984, 93);
			this.Panel3.TabIndex = 13;
			//
			//EyeContextList
			//
			this.EyeContextList.DisplayMemberPath = "";
			this.EyeContextList.Location = new System.Drawing.Point(217, 49);
			this.EyeContextList.Name = "EyeContextList";
			this.EyeContextList.SelectedIndex = -1;
			this.EyeContextList.SelectedValuePath = "";
			this.EyeContextList.Size = new System.Drawing.Size(85, 33);
			this.EyeContextList.StyleName = "ComboHeaderComboBoxStyle";
			this.EyeContextList.TabIndex = 44;
			//
			//Label2
			//
			this.Label2.Location = new System.Drawing.Point(122, 55);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(92, 21);
			this.Label2.StyleName = "Header";
			this.Label2.TabIndex = 43;
			this.Label2.Text = "Eye Context";
			//
			//Label1
			//
			this.Label1.Location = new System.Drawing.Point(308, 55);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(113, 21);
			this.Label1.StyleName = "Header";
			this.Label1.TabIndex = 42;
			this.Label1.Text = "Surgery Type";
			//
			//SurgeryTypeList
			//
			this.SurgeryTypeList.DisplayMemberPath = "";
			this.SurgeryTypeList.Location = new System.Drawing.Point(421, 49);
			this.SurgeryTypeList.Name = "SurgeryTypeList";
			this.SurgeryTypeList.SelectedIndex = -1;
			this.SurgeryTypeList.SelectedValuePath = "";
			this.SurgeryTypeList.Size = new System.Drawing.Size(151, 33);
			this.SurgeryTypeList.StyleName = "ComboHeaderComboBoxStyle";
			this.SurgeryTypeList.TabIndex = 41;
			//
			//TopNoteText
			//
			this.TopNoteText.Location = new System.Drawing.Point(576, 40);
			this.TopNoteText.Name = "TopNoteText";
			this.TopNoteText.Size = new System.Drawing.Size(403, 50);
			this.TopNoteText.TabIndex = 40;
			//
			//MyProvider
			//
			this.MyProvider.Location = new System.Drawing.Point(689, 8);
			this.MyProvider.Name = "MyProvider";
			this.MyProvider.Size = new System.Drawing.Size(290, 24);
			this.MyProvider.StyleName = "Header";
			this.MyProvider.TabIndex = 5;
			this.MyProvider.Text = "Surgeon";
			this.MyProvider.TextWrapping = System.Windows.TextWrapping.NoWrap;
			//
			//MyPatDOB
			//
			this.MyPatDOB.Location = new System.Drawing.Point(479, 8);
			this.MyPatDOB.Name = "MyPatDOB";
			this.MyPatDOB.Size = new System.Drawing.Size(204, 24);
			this.MyPatDOB.StyleName = "Header";
			this.MyPatDOB.TabIndex = 2;
			this.MyPatDOB.Text = "DOB";
			//
			//MyPatFullName
			//
			this.MyPatFullName.Location = new System.Drawing.Point(123, 8);
			this.MyPatFullName.Name = "MyPatFullName";
			this.MyPatFullName.Size = new System.Drawing.Size(350, 24);
			this.MyPatFullName.StyleName = "Header";
			this.MyPatFullName.TabIndex = 1;
			this.MyPatFullName.Text = "PatientName";
			//
			//PatientPicture
			//
			this.PatientPicture.BackColor = System.Drawing.Color.DarkGray;
			this.PatientPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.PatientPicture.Location = new System.Drawing.Point(3, 3);
			this.PatientPicture.Name = "PatientPicture";
			this.PatientPicture.Size = new System.Drawing.Size(114, 87);
			this.PatientPicture.TabIndex = 0;
			this.PatientPicture.TabStop = false;
			//
			//Panel1
			//
			this.Panel1.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.Panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.Panel1.Location = new System.Drawing.Point(0, 0);
			this.Panel1.Name = "Panel1";
			this.Panel1.Size = new System.Drawing.Size(10, 686);
			this.Panel1.TabIndex = 14;
			//
			//Panel4
			//
			this.Panel4.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.Panel4.Dock = System.Windows.Forms.DockStyle.Right;
			this.Panel4.Location = new System.Drawing.Point(994, 0);
			this.Panel4.Name = "Panel4";
			this.Panel4.Size = new System.Drawing.Size(10, 686);
			this.Panel4.TabIndex = 15;
			//
			//SurgeryControl
			//
			this.Controls.Add(this.SurgeryTab);
			this.Controls.Add(this.Panel2);
			this.Controls.Add(this.Panel3);
			this.Controls.Add(this.Panel4);
			this.Controls.Add(this.Panel1);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "SurgeryControl";
			this.Size = new System.Drawing.Size(1004, 686);
			this.SurgeryTab.ResumeLayout(false);
			this.Panel2.ResumeLayout(false);
			this.Panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.PatientPicture).EndInit();
			this.ResumeLayout(false);

            new System.Windows.Forms.UserControl[]
		        {
		            this.AssessTabPage,
		            this.HtWtTabPage,
		            this.CurrMedsTabPage,
		            this.AllergyTabPage,
		            this.WorkupTabPage,
		            this.ORTabPage,
		            this.PostOPTabPage
		        }.ForEachWithSinglePropertyChangedNotification(this.SurgeryTab.Items.Add);
		}
		internal Soaf.Presentation.Controls.WindowsForms.Label Label11;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BottomNoteText;
		internal Soaf.Presentation.Controls.WindowsForms.Panel Panel2;
		internal Soaf.Presentation.Controls.WindowsForms.Panel Panel3;
		internal Soaf.Presentation.Controls.WindowsForms.Panel Panel1;
		internal Soaf.Presentation.Controls.WindowsForms.Panel Panel4;
		internal System.Windows.Forms.PictureBox PatientPicture;
		internal Soaf.Presentation.Controls.WindowsForms.Label MyPatFullName;
		internal Soaf.Presentation.Controls.WindowsForms.Label MyProvider;
		internal Soaf.Presentation.Controls.WindowsForms.Label MyPatDOB;
		private Soaf.Presentation.Controls.WindowsForms.TabControl withEventsField_SurgeryTab;
		internal Soaf.Presentation.Controls.WindowsForms.TabControl SurgeryTab {
			get { return withEventsField_SurgeryTab; }
			set {
				if (withEventsField_SurgeryTab != null) {
					withEventsField_SurgeryTab.SelectionChanged -= SurgeryTabSelectedPageChanged;
				}
				withEventsField_SurgeryTab = value;
				if (withEventsField_SurgeryTab != null) {
                    withEventsField_SurgeryTab.SelectionChanged += SurgeryTabSelectedPageChanged;
				}
			}
		}
		private System.Windows.Forms.UserControl withEventsField_AssessTabPage;
		internal System.Windows.Forms.UserControl AssessTabPage {
			get { return withEventsField_AssessTabPage; }
			set {
				if (withEventsField_AssessTabPage != null) {
					withEventsField_AssessTabPage.Leave -= OnTabPageLeave;
				}
				withEventsField_AssessTabPage = value;
				if (withEventsField_AssessTabPage != null) {
					withEventsField_AssessTabPage.Leave += OnTabPageLeave;
				}
			}
		}
		private System.Windows.Forms.UserControl withEventsField_HtWtTabPage;
		internal System.Windows.Forms.UserControl HtWtTabPage {
			get { return withEventsField_HtWtTabPage; }
			set {
				if (withEventsField_HtWtTabPage != null) {
					withEventsField_HtWtTabPage.Leave -= OnTabPageLeave;
				}
				withEventsField_HtWtTabPage = value;
				if (withEventsField_HtWtTabPage != null) {
					withEventsField_HtWtTabPage.Leave += OnTabPageLeave;
				}
			}
		}
		internal System.Windows.Forms.UserControl CurrMedsTabPage;
		internal System.Windows.Forms.UserControl AllergyTabPage;
		private System.Windows.Forms.UserControl withEventsField_WorkupTabPage;
		internal System.Windows.Forms.UserControl WorkupTabPage {
			get { return withEventsField_WorkupTabPage; }
			set {
				if (withEventsField_WorkupTabPage != null) {
					withEventsField_WorkupTabPage.Leave -= OnTabPageLeave;
				}
				withEventsField_WorkupTabPage = value;
				if (withEventsField_WorkupTabPage != null) {
					withEventsField_WorkupTabPage.Leave += OnTabPageLeave;
				}
			}
		}
		private System.Windows.Forms.UserControl withEventsField_ORTabPage;
		internal System.Windows.Forms.UserControl ORTabPage {
			get { return withEventsField_ORTabPage; }
			set {
				if (withEventsField_ORTabPage != null) {
					withEventsField_ORTabPage.Leave -= OnTabPageLeave;
				}
				withEventsField_ORTabPage = value;
				if (withEventsField_ORTabPage != null) {
					withEventsField_ORTabPage.Leave += OnTabPageLeave;
				}
			}
		}
		private System.Windows.Forms.UserControl withEventsField_PostOPTabPage;
		internal System.Windows.Forms.UserControl PostOPTabPage {
			get { return withEventsField_PostOPTabPage; }
			set {
				if (withEventsField_PostOPTabPage != null) {
					withEventsField_PostOPTabPage.Leave -= OnTabPageLeave;
				}
				withEventsField_PostOPTabPage = value;
				if (withEventsField_PostOPTabPage != null) {
					withEventsField_PostOPTabPage.Leave += OnTabPageLeave;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.TextBox TopNoteText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label1;
		internal Soaf.Presentation.Controls.WindowsForms.ComboBox EyeContextList;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label2;
		private Soaf.Presentation.Controls.WindowsForms.ComboBox withEventsField_SurgeryTypeList;
		internal Soaf.Presentation.Controls.WindowsForms.ComboBox SurgeryTypeList {
			get { return withEventsField_SurgeryTypeList; }
			set {
				if (withEventsField_SurgeryTypeList != null) {
					withEventsField_SurgeryTypeList.SelectionChanged -= OnSurgeryTypeListSelectionChanged;
				}
				withEventsField_SurgeryTypeList = value;
				if (withEventsField_SurgeryTypeList != null) {
					withEventsField_SurgeryTypeList.SelectionChanged += OnSurgeryTypeListSelectionChanged;
				}
			}

		}
	}

	public class SurgeryViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<SurgeryViewPresenter>
	{
	}
}
