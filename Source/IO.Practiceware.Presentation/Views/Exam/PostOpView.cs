using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Services.Exam;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Exam
{
    /// <summary>
    ///   class PostOpControl
    /// </summary>
    public partial class PostOpView
    {
        private int _gap;
        private int _left;
        private int _width;

        public PostOpView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            PostOpPanel.Visible = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadPresenter();
        }

        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            LoadPostOpControls();
            PostOpPanel.Visible = true;
            if (Presenter.PatientSurgery != null)
            {
                LoadPostOpData();
            }
        }

        /// <summary>
        ///   Loads the post op data.
        /// </summary>
        private void LoadPostOpData()
        {
            if (DynamicPanel.Controls.Count > 0)
            {
                foreach (object questionChoiceControl in DynamicPanel.Controls)
                {
                    int choiceid1 = ((QuestionChoiceView) questionChoiceControl).ChoiceId1;
                    int choiceid2 = ((QuestionChoiceView) questionChoiceControl).ChoiceId2;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                    if (choice != null)
                    {
                        if (((QuestionChoiceView) questionChoiceControl).ChoiceId1 == choice.ChoiceId)
                        {
                            ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                            ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                        }
                    }
                    else
                    {
                        choice = Presenter.GetChoice(choiceid2);
                        if (choice != null)
                        {
                            if (((QuestionChoiceView) questionChoiceControl).ChoiceId2 == choice.ChoiceId)
                            {
                                ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                                ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                            }
                        }
                    }
                }
            }
            if (DynamicBPPanel.Controls.Count > 0)
            {
                foreach (object bloodPressureControl in DynamicBPPanel.Controls)
                {
                    int questionid = ((BloodPressureView) bloodPressureControl).QuestionId;
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(questionid);
                    if (answer != null)
                    {
                        if (((BloodPressureView) bloodPressureControl).QuestionId == answer.QuestionId)
                        {
                            ((BloodPressureView) bloodPressureControl).Value = answer.Value;
                            ((BloodPressureView) bloodPressureControl).Note = answer.Note;
                        }
                    }
                }
            }
            if (IVOutQAControl.QuestionId > 0)
            {
                PatientSurgeryQuestionAnswer qAnswer = Presenter.GetAnswer(IVOutQAControl.QuestionId);
                if (qAnswer != null)
                {
                    IVOutQAControl.Value = qAnswer.Value;
                    IVOutQAControl.Note = qAnswer.Note;
                }
            }
            if (Presenter.IvOutNotesAnswer != null)
            {
                IvOutText.Text = Presenter.IvOutNotesAnswer.Value;
            }
            if (Presenter.RespAnswer != null)
            {
                RespText.Text = Presenter.RespAnswer.Value;
            }
            if (Presenter.PulseAnswer != null)
            {
                PulseText.Text = Presenter.PulseAnswer.Value;
            }
            if (Presenter.O2SaturationAnswer != null)
            {
                O2Text.Text = Presenter.O2SaturationAnswer.Value;
            }
        }

        /// <summary>
        ///   Loads the post op controls.
        /// </summary>
        private void LoadPostOpControls()
        {
            DynamicPanel.Visible = false;
            DynamicBPPanel.Visible = false;
            bool firstControlLoaded = false;
            bool bpFirstControlLoaded = false;
            _gap = 30;
            if (Presenter.ScreenQuestions != null)
            {
                foreach (SurgeryTemplateScreenQuestion sq in Presenter.ScreenQuestions.OrderBy(s => s.Question.OrdinalId))
                {
                    if (sq.Question.Choices.Count() > 1)
                    {
                        if (DynamicPanel.Controls.Count == 1 & firstControlLoaded == false)
                        {
                            QCControl.AddQuestion(sq.Question);
                            firstControlLoaded = true;
                        }
                        else
                        {
                            AddNextQuestion(sq);
                        }
                    }
                }
            }
            if (Presenter.BpQuestions != null)
            {
                foreach (Question question in Presenter.BpQuestions)
                {
                    if (DynamicBPPanel.Controls.Count == 1 & bpFirstControlLoaded == false)
                    {
                        BPControl.AddQuestion(question);
                        bpFirstControlLoaded = true;
                    }
                    else
                    {
                        AddNextBpQuestion(question);
                    }
                }
            }
            AddStaticQuestion();
            if (DynamicPanel.Controls.Count > 10)
            {
                _width = QCControl.Width;
                _left = QCControl.Left;
                NextButton.Visible = true;
                PreviousButton.Visible = true;
                NextButton.Enabled = true;
            }
            if (firstControlLoaded)
            {
                DynamicPanel.Visible = true;
            }
            if (DynamicBPPanel.Controls.Count > 6)
            {
                _width = BPControl.Width;
                _left = BPControl.Left;
                BpNextButton.Visible = true;
                BpPreviousButton.Visible = true;
                BpNextButton.Enabled = true;
            }
            if (bpFirstControlLoaded)
            {
                DynamicBPPanel.Visible = true;
            }
        }

        /// <summary>
        ///   Adds the next question.
        /// </summary>
        /// <param name="screenQuestion"> The screen question. </param>
        private void AddNextQuestion(SurgeryTemplateScreenQuestion screenQuestion)
        {
            int n = DynamicPanel.Controls.Count;
            var nQcControl = new QuestionChoiceView();
            nQcControl.Visible = true;
            if (n%4 == 0)
            {
                nQcControl.Top = DynamicPanel.Controls[n - 1].Top;
                nQcControl.Left = DynamicPanel.Controls[0].Left + DynamicPanel.Controls[0].Width + _gap;
                nQcControl.Width = DynamicPanel.Controls[0].Width;
            }
            else
            {
                nQcControl.Top = DynamicPanel.Controls[0].Top + DynamicPanel.Controls[0].Height + 5;
                nQcControl.Left = DynamicPanel.Controls[0].Left;
                nQcControl.Width = DynamicPanel.Controls[0].Width;
            }
            nQcControl.AddQuestion(screenQuestion.Question);
            nQcControl.Name = QCControl.Name + (n);
            DynamicPanel.Controls.Add(nQcControl);
            nQcControl.BringToFront();
        }

        private void AddNextBpQuestion(Question question)
        {
            int n = DynamicBPPanel.Controls.Count;
            var nBpControl = new BloodPressureView();
            nBpControl.Visible = true;
            if (n%2 == 0)
            {
                nBpControl.Top = DynamicBPPanel.Controls[n - 1].Top;
                nBpControl.Left = DynamicBPPanel.Controls[0].Left + DynamicBPPanel.Controls[0].Width + _gap;
                nBpControl.Width = DynamicBPPanel.Controls[0].Width;
            }
            else
            {
                nBpControl.Top = DynamicBPPanel.Controls[0].Top + DynamicBPPanel.Controls[0].Height + 5;
                nBpControl.Left = DynamicBPPanel.Controls[0].Left;
                nBpControl.Width = DynamicBPPanel.Controls[0].Width;
            }
            nBpControl.AddQuestion(question);
            DynamicBPPanel.Controls.Add(nBpControl);
            nBpControl.BringToFront();
        }

        /// <summary>
        ///   Adds the static question.
        /// </summary>
        private void AddStaticQuestion()
        {
            if (Presenter.IvOutQuestion != null)
            {
                IVOutQAControl.AddQuestion(Presenter.IvOutQuestion);
            }
        }

        private void MoveNext()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left -= (_width + _gap);
            }
            NextButton.Enabled = DynamicPanel.Controls[0].Left > (_width + _gap + _left);
            }

        private void MovePrevious()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left += (_width + _gap);
            }
            PreviousButton.Enabled = QCControl.Left < _left;
            }

        private void NextButtonClick(object sender, EventArgs e)
        {
            MoveNext();
            PreviousButton.Enabled = true;
        }

        private void PreviousButtonClick(object sender, EventArgs e)
        {
            MovePrevious();
            NextButton.Enabled = true;
        }

        /// <summary>
        ///   Gets the post op user selections.
        /// </summary>
        public void GetPostOpSelections()
        {
            GetPostOpSelectedQuestionChoices();
            GetPostOpSelectedQuestionAnswers();
        }

        /// <summary>
        ///   Gets the post op question choices selected by the user.
        /// </summary>
        private void GetPostOpSelectedQuestionChoices()
        {
            var arguments = new List<QuestionChoiceArguments>();
            foreach (object qc in DynamicPanel.Controls)
            {
                object qc1 = qc;
                if ((((QuestionChoiceView) qc1).SelChoiceId != 0))
                {
                    var argument = new QuestionChoiceArguments();
                    int choiceid1 = ((QuestionChoiceView) qc1).ChoiceId1;
                    int choiceid2 = ((QuestionChoiceView) qc1).ChoiceId2;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                    if (choice != null)
                    {
                        argument.PatientSurgeryQuestionChoiceId = choice.Id;
                    }
                    else
                    {
                        choice = Presenter.GetChoice(choiceid2);
                        if (choice != null)
                        {
                            argument.PatientSurgeryQuestionChoiceId = choice.Id;
                        }
                    }
                    argument.QuestionId = ((QuestionChoiceView) qc1).QuestionId;
                    argument.ChoiceId = ((QuestionChoiceView) qc1).SelChoiceId;
                    argument.Note = ((QuestionChoiceView) qc1).Note;
                    arguments.Add(argument);
                }
            }
            Presenter.PostOpChoiceArgs = arguments;
        }

        /// <summary>
        ///   Gets the post op question answers selected by the user.
        /// </summary>
        private void GetPostOpSelectedQuestionAnswers()
        {
            var arguments = new List<QuestionAnswerArguments>();
            //Insert/update data
            if (!string.IsNullOrEmpty(IVOutQAControl.Value.Trim()) & IVOutQAControl.QuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(IVOutQAControl.QuestionId);
                if (answer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = answer.Id;
                }
                argument.QuestionId = IVOutQAControl.QuestionId;
                argument.Value = IVOutQAControl.Value;
                argument.Note = IVOutQAControl.Note;
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(IVOutQAControl.Value.Trim()) & string.IsNullOrEmpty(IVOutQAControl.Note.Trim()) & IVOutQAControl.QuestionId > 0)
            {
                PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(IVOutQAControl.QuestionId);
                if (answer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = answer.Id;
                    argument.QuestionId = IVOutQAControl.QuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(IvOutText.Text.Trim()) & Presenter.IvOutNotesQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.IvOutNotesAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.IvOutNotesAnswer.Id;
                }
                argument.QuestionId = Presenter.IvOutNotesQuestionId;
                argument.Value = IvOutText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(IvOutText.Text.Trim()) & Presenter.IvOutNotesQuestionId > 0)
            {
                if (Presenter.IvOutNotesAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.IvOutNotesAnswer.Id;
                    argument.QuestionId = Presenter.IvOutNotesQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
            }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(RespText.Text.Trim()) & Presenter.RespQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.RespAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.RespAnswer.Id;
                }
                argument.QuestionId = Presenter.RespQuestionId;
                argument.Value = RespText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(RespText.Text.Trim()) & Presenter.RespQuestionId > 0 )
            {
                if (Presenter.RespAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.RespAnswer.Id;
                    argument.QuestionId = Presenter.RespQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
            }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(PulseText.Text.Trim()) & Presenter.PulseQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.PulseAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.PulseAnswer.Id;
                }
                argument.QuestionId = Presenter.PulseQuestionId;
                argument.Value = PulseText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(PulseText.Text.Trim()) & Presenter.PulseQuestionId > 0)
            {
                if (Presenter.PulseAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.PulseAnswer.Id;
                    argument.QuestionId = Presenter.PulseQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(O2Text.Text.Trim()) & Presenter.O2SaturationQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.O2SaturationAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.O2SaturationAnswer.Id;
                }
                argument.QuestionId = Presenter.O2SaturationQuestionId;
                argument.Value = O2Text.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(O2Text.Text.Trim()) & Presenter.O2SaturationQuestionId > 0)
            {
                if (Presenter.O2SaturationAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.O2SaturationAnswer.Id;
                    argument.QuestionId = Presenter.O2SaturationQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            foreach (object bpc in DynamicBPPanel.Controls)
            {
                object bp = bpc;
                //Insert/update data
                if ((!string.IsNullOrEmpty(((BloodPressureView) bp).Value)))
                {
                    var argument = new QuestionAnswerArguments();
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(((BloodPressureView) bp).QuestionId);
                    if (answer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = answer.Id;
                    }
                    argument.QuestionId = ((BloodPressureView) bp).QuestionId;
                    argument.Value = ((BloodPressureView) bp).Value;
                    argument.Note = ((BloodPressureView) bp).Note;
                    arguments.Add(argument);
                }
                //Delete record if the data is empty
                else if ((string.IsNullOrEmpty(((BloodPressureView)bp).Value)))
                {
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(((BloodPressureView)bp).QuestionId);
                    if (answer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = answer.Id;
                        argument.QuestionId = ((BloodPressureView)bp).QuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
            }
            }
            Presenter.PostOpAnswerArgs = arguments;
        }

        private void MoveBpNext()
        {
            foreach (object bpc in DynamicBPPanel.Controls)
            {
                ((BloodPressureView) bpc).Left -= (_width + _gap);
            }
            BpNextButton.Enabled = DynamicBPPanel.Controls[0].Left > (_width + _gap + _left);
            }

        private void MoveBpPrevious()
        {
            foreach (object bpc in DynamicBPPanel.Controls)
            {
                ((BloodPressureView) bpc).Left += (_width + _gap);
            }
            BpPreviousButton.Enabled = BPControl.Left < _left;
            }

        private void BpNextButtonClick(object sender, EventArgs e)
        {
            MoveBpNext();
            BpPreviousButton.Enabled = true;
        }

        private void BpPreviousButtonClick(object sender, EventArgs e)
        {
            MoveBpPrevious();
            BpNextButton.Enabled = true;
        }
    }

    /// <summary>
    ///   class PostOpViewPresenter
    /// </summary>
    public class PostOpViewPresenter : ILoadable
    {
        private const string ScreenName = "Post-OP";
        private readonly IPracticeRepository _practiceRepository;
        public IEnumerable<Question> BpQuestions;
        public PatientSurgeryQuestionAnswer IvOutAnswer;
        public PatientSurgeryQuestionAnswer IvOutNotesAnswer;
        public int IvOutNotesQuestionId;
        public Question IvOutQuestion;
        public PatientSurgeryQuestionAnswer O2SaturationAnswer;
        public int O2SaturationQuestionId;
        public PatientSurgery PatientSurgery;
        public IEnumerable<PatientSurgeryQuestionChoice> PatientSurgeryQuestionChoices;
        public IEnumerable<QuestionAnswerArguments> PostOpAnswerArgs;
        public IEnumerable<QuestionChoiceArguments> PostOpChoiceArgs;
        public PatientSurgeryQuestionAnswer PulseAnswer;
        public int PulseQuestionId;
        public PatientSurgeryQuestionAnswer RespAnswer;
        public int RespQuestionId;
        public int SurgeryTemlateId;
        private IEnumerable<PatientSurgeryQuestionAnswer> _patientSurgeryQuestionAnswers;

        private IEnumerable<Question> _questions;

        public PostOpViewPresenter(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<SurgeryTemplateScreenQuestion> ScreenQuestions { get; set; }

        public IEnumerable<Question> Questions
        {
            get { return _questions; }
            set
            {
                _questions = value;
                if (_questions != null)
                {
                    Question[] questions = _questions.ToArray();
                    IvOutQuestion = questions.First(q => q.Label.ToUpper() == "IV OUT");
                    IvOutNotesQuestionId = questions.First(q => q.Label.ToUpper() == "IV OUT NOTES").Id;
                    RespQuestionId = questions.First(q => q.Label.ToUpper() == "POST-OP RESP").Id;
                    PulseQuestionId = questions.First(q => q.Label.ToUpper() == "POST-OP PULSE").Id;
                    O2SaturationQuestionId = questions.First(q => q.Label.ToUpper() == "POST-OP O2 SATURATION").Id;
                    LoadAnswers();
                    BpQuestions = questions.Where(q => q.Label.ToUpper().Contains("POST-OP BP")).ToArray();
                }
            }
        }

        public IEnumerable<PatientSurgeryQuestionAnswer> PatientSurgeryQuestionAnswers
        {
            get { return _patientSurgeryQuestionAnswers; }
            set
            {
                _patientSurgeryQuestionAnswers = value;
                if (_patientSurgeryQuestionAnswers != null)
                {
                    LoadAnswers();
                }
            }
        }

        #region ILoadable Members

        public IEnumerable<Action> LoadActions
        {
            get
            {
                var actions = new Action[] {() => ScreenQuestions = _practiceRepository.SurgeryTemplateScreenQuestions.Include(s => s.Question, s => s.Screen, s => s.Question.Choices).Where(s => s.SurgeryTemplateId == SurgeryTemlateId & s.Screen.Name.ToUpper() == ScreenName.ToUpper()).ToArray()};

                return actions;
            }
        }

        #endregion

        /// <summary>
        ///   Loads the PatientSurgeryQuestionAnswers.
        /// </summary>
        public void LoadAnswers()
        {
            if (IvOutQuestion != null)
            {
                IvOutAnswer = GetAnswer(IvOutQuestion.Id);
            }
            if (IvOutNotesQuestionId > 0)
            {
                IvOutNotesAnswer = GetAnswer(IvOutNotesQuestionId);
            }
            if (RespQuestionId > 0)
            {
                RespAnswer = GetAnswer(RespQuestionId);
            }
            if (PulseQuestionId > 0)
            {
                PulseAnswer = GetAnswer(PulseQuestionId);
            }
            if (O2SaturationQuestionId > 0)
            {
                O2SaturationAnswer = GetAnswer(O2SaturationQuestionId);
            }
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionAnswer.
        /// </summary>
        /// <param name="questionId"> The question id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionAnswer GetAnswer(int questionId)
        {
            if (PatientSurgeryQuestionAnswers != null)
            {
                return PatientSurgeryQuestionAnswers.FirstOrDefault(ans => ans.QuestionId == questionId);
            }
            return null;
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionChoice.
        /// </summary>
        /// <param name="choiceId"> The choice id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionChoice GetChoice(int choiceId)
        {
            if (PatientSurgeryQuestionChoices != null)
            {
                return PatientSurgeryQuestionChoices.FirstOrDefault(ch => ch.ChoiceId == choiceId);
            }
            return null;
        }
    }
}