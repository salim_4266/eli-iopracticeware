using IO.Practiceware.Model;
using IO.Practiceware.Services.Exam;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Forms;
using Image = System.Drawing.Image;

namespace IO.Practiceware.Presentation.Views.Exam
{
    /// <summary>
    ///   class SurgeryControl
    /// </summary>
    public partial class SurgeryView
    {
        //actual size 1004/1260 x 683

        private HealthAssessmentView _healthAssessment;
        private HeightWeightView _heightWeight;
        private OpRoomView _opRoom;

        private PostOpView _postOp;
        private WorkUpView _workUp;

        public SurgeryView()
        {
            InitializeComponent();
        }

        /// <summary>
        ///   Loads the health assessment tab.
        /// </summary>
        private void LoadHealthAssessmentTab()
        {
            if (_healthAssessment == null)
            {
                _healthAssessment = new HealthAssessmentView();
                _healthAssessment.Presenter = Presenter.HealthAssessmentViewPresenter;
                _healthAssessment.Dock = DockStyle.Fill;
                AssessTabPage.Controls.Add(_healthAssessment);
            }
        }

        /// <summary>
        ///   Loads the height weight tab.
        /// </summary>
        private void LoadHeightWeightTab()
        {
            if (_heightWeight == null)
            {
                _heightWeight = new HeightWeightView();
                _heightWeight.Presenter = Presenter.HeightWeightViewPresenter;
                _heightWeight.Dock = DockStyle.Fill;
                HtWtTabPage.Controls.Add(_heightWeight);
            }
        }

        /// <summary>
        ///   Loads the work up tab.
        /// </summary>
        private void LoadWorkUpTab()
        {
            if (_workUp == null)
            {
                _workUp = new WorkUpView();
                _workUp.Presenter = Presenter.WorkUpViewPresenter;
                _workUp.Dock = DockStyle.Fill;
                WorkupTabPage.Controls.Add(_workUp);
            }
        }

        /// <summary>
        ///   Loads the operating room tab.
        /// </summary>
        private void LoadOpRoomTab()
        {
            if (_opRoom == null)
            {
                _opRoom = new OpRoomView();
                _opRoom.Presenter = Presenter.OpRoomViewPresenter;
                _opRoom.Dock = DockStyle.Fill;
                ORTabPage.Controls.Add(_opRoom);
            }
        }

        /// <summary>
        ///   Loads the post op tab.
        /// </summary>
        private void LoadPostOpTab()
        {
            if (_postOp == null)
            {
                _postOp = new PostOpView();
                _postOp.Presenter = Presenter.PostOpViewPresenter;
                _postOp.Dock = DockStyle.Fill;
                PostOPTabPage.Controls.Add(_postOp);
            }
        }

        /// <summary>
        ///   Raises the Load event.
        /// </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            InteractionContext.Completing += OnInteractionContextCompleting;
            LoadPresenter();
        }

        private void OnInteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (InteractionContext.DialogResult != false && SurgeryTypeList.SelectedItem == null)
            {
                InteractionManager.Current.Alert("Please select surgery type to save the surgery.", "Select Surgery Type");
                e.Cancel = true;
            }
            else if (InteractionContext.DialogResult == true && SurgeryTypeList.SelectedItem != null)
            {
                SaveSurgery();
            }
        }

        /// <summary>
        ///   Called when [presenter loaded].
        /// </summary>
        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            SetPatientBar();
            LoadHealthAssessmentTab();
        }

        /// <summary>
        ///   Sets the top patient bar.
        /// </summary>
        private void SetPatientBar()
        {
            if (Presenter.Patient != null)
            {
                MyPatFullName.Text = Presenter.Patient.DisplayName;
                MyPatDOB.Text = Presenter.Patient.DateOfBirth + ", " + Presenter.Patient.Gender;
            }
            if (Presenter.PatientPhoto != null)
            {
                PatientPicture.BackgroundImage = Presenter.PatientPhoto;
            }
            if (Presenter.Doctor != null)
            {
                MyProvider.Text = Presenter.Doctor.DisplayName;
            }
            if (Presenter.SurgeryTypes != null)
            {
                SurgeryTypeList.ItemsSource = Presenter.SurgeryTypes;
                foreach (object surgeryType in SurgeryTypeList.ItemsSource)
                {
                    if (Presenter.ActiveSurgeryType != null)
                    {
                        if (((SurgeryType)surgeryType).Id == Presenter.ActiveSurgeryType.Id)
                        {
                            SurgeryTypeList.SelectedItem = surgeryType;
                            SurgeryTypeList.Refresh();
                        }
                    }
                }
                //If SurgeryTypeList.SelectedItem Is Nothing Then
                //    SurgeryTypeList.SelectedItem = SurgeryTypeList.ItemsSource.OfType(Of Object)().FirstOrDefault()
                //End If
            }
            if (Presenter.EyeContext != null)
            {
                EyeContextList.ItemsSource = Presenter.EyeContext;
                foreach (object eyeContext in EyeContextList.ItemsSource)
                {
                    if (Presenter.GetChoice(((Choice)eyeContext).Id) != null)
                    {
                        EyeContextList.SelectedItem = eyeContext;
                        EyeContextList.Refresh();
                    }
                }
            }
            Presenter.LoadAnswers();
            if (Presenter.TopNoteAnswer != null)
            {
                TopNoteText.Text = Presenter.TopNoteAnswer.Value;
            }
            if (Presenter.BottomNoteAnswer != null)
            {
                BottomNoteText.Text = Presenter.BottomNoteAnswer.Value;
            }
        }

        /// <summary>
        ///   Gets the surgery control user selections.
        /// </summary>
        private void GetSurgerySelections()
        {
            GetSurgerySelectedQuestionAnswers();
            GetSurgerySelectedQuestionChoices();
        }

        /// <summary>
        ///   Gets the surgery control question choices selected by user .
        /// </summary>
        private void GetSurgerySelectedQuestionChoices()
        {
            var arguments = new List<QuestionChoiceArguments>();
            if (EyeContextList.SelectedItem != null)
            {
                var argument = new QuestionChoiceArguments();
                foreach (object choice in EyeContextList.ItemsSource)
                {
                    if (Presenter.GetChoice(((Choice)choice).Id) != null)
                    {
                        argument.PatientSurgeryQuestionChoiceId = Presenter.GetChoice(((Choice)choice).Id).Id;
                    }
                }
                argument.QuestionId = ((IEnumerable<Choice>)EyeContextList.ItemsSource).First().QuestionId;
                argument.ChoiceId = ((Choice)EyeContextList.SelectedItem).Id;
                argument.Note = "";
                arguments.Add(argument);
            }
            Presenter.SurgeryChoiceArgs = arguments;
        }

        /// <summary>
        ///   Gets the surgery question answers selected by user.
        /// </summary>
        private void GetSurgerySelectedQuestionAnswers()
        {
            var arguments = new List<QuestionAnswerArguments>();
            if (!string.IsNullOrEmpty(TopNoteText.Text.Trim()))
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.TopNoteAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.TopNoteAnswer.Id;
                }
                argument.QuestionId = Presenter.TopNoteQuestionId;
                argument.Value = TopNoteText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            else
            {
                if (Presenter.TopNoteAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.TopNoteAnswer.Id;
                    argument.QuestionId = Presenter.TopNoteQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            if (!string.IsNullOrEmpty(BottomNoteText.Text.Trim()))
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.BottomNoteAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BottomNoteAnswer.Id;
                }
                argument.QuestionId = Presenter.BottomNoteQuestionId;
                argument.Value = BottomNoteText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            else
            {
                if (Presenter.BottomNoteAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BottomNoteAnswer.Id;
                    argument.QuestionId = Presenter.BottomNoteQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            Presenter.SurgeryAnswerArgs = arguments;
        }

        /// <summary>
        ///   Surgeries the tab selected page changed.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void SurgeryTabSelectedPageChanged(object sender, EventArgs e)
        {
            // Lazy load for speed
            if (Presenter != null)
            {
                var surgeryTabAsRadTab = SurgeryTab.SelectedItem.CastTo<Telerik.Windows.Controls.RadTabItem>().Content.CastTo<System.Windows.Forms.Integration.WindowsFormsHost>().Child.CastTo<System.Windows.Forms.UserControl>();
                if (ReferenceEquals(surgeryTabAsRadTab, HtWtTabPage) && _heightWeight == null)
                {
                    LoadHeightWeightTab();
                }
                else if (ReferenceEquals(surgeryTabAsRadTab, AllergyTabPage))
                {
                    InteractionContext.Complete(true);
                }
                else if (ReferenceEquals(surgeryTabAsRadTab, CurrMedsTabPage))
                {
                    InteractionContext.Complete(true);
                }
                else if (ReferenceEquals(surgeryTabAsRadTab, WorkupTabPage) && _workUp == null)
                {
                    LoadWorkUpTab();
                }
                else if (ReferenceEquals(surgeryTabAsRadTab, ORTabPage) && _opRoom == null)
                {
                    LoadOpRoomTab();
                }
                else if (ReferenceEquals(surgeryTabAsRadTab, PostOPTabPage) && _postOp == null)
                {
                    LoadPostOpTab();
                }
            }
        }

        /// <summary>
        ///   Saves the Patient surgery.
        /// </summary>
        private void SaveSurgery()
        {
            if (SurgeryTypeList.SelectedItem != null)
            {
                Presenter.ActiveSurgeryType = (SurgeryType)SurgeryTypeList.SelectedItem;
                Presenter.SurgeryTypeId = ((SurgeryType)SurgeryTypeList.SelectedItem).Id;
            }
            else
            {
                return;
            }
            GetSurgerySelections();
            if (_healthAssessment != null)
            {
                _healthAssessment.GetHealthAssessmentSelectedQuestionChoices();
            }
            if (_heightWeight != null)
            {
                _heightWeight.GetHeightWeightSelectedQuestionAnswers();
            }
            if (_workUp != null)
            {
                _workUp.GetWorkUpSelections();
            }
            if (_opRoom != null)
            {
                _opRoom.GetOpRoomSelections();
            }
            if (_postOp != null)
            {
                _postOp.GetPostOpSelections();
            }
            Presenter.Save();
        }

        private void OnTabPageLeave(object sender, EventArgs e)
        {
            SaveSurgery();
        }

        private void OnSurgeryTypeListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Presenter.ActiveSurgeryType != null & SurgeryTypeList.SelectedItem != null)
            {
                if (Presenter.ActiveSurgeryType.Id != ((SurgeryType)SurgeryTypeList.SelectedItem).Id)
                {
                    if (InteractionManager.Current.Confirm("Changing surgery type can cause loss of data. Are you sure you want to change this surgery?", header: "Confirm Surgery Type").GetValueOrDefault())
                    {
                        Presenter.ActiveSurgeryType = (SurgeryType)SurgeryTypeList.SelectedItem;
                        SaveSurgery();
                        ReLoadSurgeryTemplate();
                    }
                    else
                    {
                        foreach (object surgeryType in SurgeryTypeList.ItemsSource)
                        {
                            if (Presenter.ActiveSurgeryType != null)
                            {
                                if (((SurgeryType)surgeryType).Id == Presenter.ActiveSurgeryType.Id)
                                {
                                    SurgeryTypeList.SelectedItem = surgeryType;
                                    SurgeryTypeList.Refresh();
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (SurgeryTypeList.SelectedItem != null)
                {
                    Presenter.ActiveSurgeryType = (SurgeryType)SurgeryTypeList.SelectedItem;
                    SaveSurgery();
                    ReLoadSurgeryTemplate();
                }
            }
        }

        private void ReLoadSurgeryTemplate()
        {
            AssessTabPage.Controls.Clear();
            HtWtTabPage.Controls.Clear();
            WorkupTabPage.Controls.Clear();
            ORTabPage.Controls.Clear();
            PostOPTabPage.Controls.Clear();
            if (_healthAssessment != null)
            {
                _healthAssessment.Dispose();
                _healthAssessment = null;
            }
            if (_heightWeight != null)
            {
                _heightWeight.Dispose();
                _heightWeight = null;
            }
            if (_workUp != null)
            {
                _workUp.Dispose();
                _workUp = null;
            }
            if (_opRoom != null)
            {
                _opRoom.Dispose();
                _opRoom = null;
            }
            if (_postOp != null)
            {
                _postOp.Dispose();
                _postOp = null;
            }
            LoadPresenter();
        }
    }

    /// <summary>
    ///   class SurgeryViewPresenter
    /// </summary>
    public class SurgeryViewPresenter : ILoadable
    {
        private readonly IImageService _imageService;
        private readonly IPracticeRepository _practiceRepository;
        private readonly ISurgeryService _surgeryManager;
        private Appointment _activeAppointment;
        private IEnumerable<Choice> _choices;
        private Doctor _doctor;
        private Patient _patient;
        private Image _patientPhoto;
        private PatientSurgery _patientSurgery;

        private IEnumerable<Question> _questions;
        private IEnumerable<SurgeryType> _surgeryTypes;

        /// <summary>
        ///   Initializes a new instance of the <see cref="SurgeryViewPresenter" /> class.
        /// </summary>
        /// <param name="practiceRepository"> The practice repository. </param>
        /// <param name="surgeryManager"> The surgery manager. </param>
        /// <param name="imageService"> The image manager. </param>
        /// <param name="healthAssessmentViewPresenter"> The health assessment control presenter. </param>
        /// <param name="heightWeightViewPresenter"> The height weight control presenter. </param>
        /// <param name="workUpViewPresenter"> The work up control presenter. </param>
        /// <param name="opRoomViewPresenter"> The op room control presenter. </param>
        /// <param name="postOpViewPresenter"> The post op control presenter. </param>
        public SurgeryViewPresenter(IPracticeRepository practiceRepository, ISurgeryService surgeryManager, IImageService imageService, HealthAssessmentViewPresenter healthAssessmentViewPresenter, HeightWeightViewPresenter heightWeightViewPresenter, WorkUpViewPresenter workUpViewPresenter, OpRoomViewPresenter opRoomViewPresenter, PostOpViewPresenter postOpViewPresenter)
        {
            _practiceRepository = practiceRepository;
            _surgeryManager = surgeryManager;
            _imageService = imageService;
            HealthAssessmentViewPresenter = healthAssessmentViewPresenter;
            HeightWeightViewPresenter = heightWeightViewPresenter;
            WorkUpViewPresenter = workUpViewPresenter;
            OpRoomViewPresenter = opRoomViewPresenter;
            PostOpViewPresenter = postOpViewPresenter;
        }

        public HealthAssessmentViewPresenter HealthAssessmentViewPresenter { get; set; }

        public HeightWeightViewPresenter HeightWeightViewPresenter { get; set; }

        public WorkUpViewPresenter WorkUpViewPresenter { get; set; }

        public OpRoomViewPresenter OpRoomViewPresenter { get; set; }

        public PostOpViewPresenter PostOpViewPresenter { get; set; }

        public Appointment ActiveAppointment
        {
            get { return _activeAppointment; }
            private set
            {
                _activeAppointment = value;
                if (value != null)
                {
                    // Get the photo so this happens as part of the load
                    _patientPhoto = value.Encounter == null ? null : _imageService.GetPatientPhoto(value.Encounter.Patient.Id).IfNotNull(i => Image.FromStream(new MemoryStream(i)));
                    _doctor = ((UserAppointment)value).User == null ? null : ((UserAppointment)value).User.CastTo<Doctor>();
                    _patient = value.Encounter == null ? null : value.Encounter.Patient;
                }
            }
        }

        public SurgeryType ActiveSurgeryType { get; set; }

        public Image PatientPhoto
        {
            get { return _patientPhoto; }
        }

        public Doctor Doctor
        {
            get { return _doctor; }
        }

        public Patient Patient
        {
            get { return _patient; }
        }

        public IEnumerable<PatientSurgeryQuestionAnswer> PatientSurgeryQuestionAnswers { get; set; }
        public IEnumerable<PatientSurgeryQuestionChoice> PatientSurgeryQuestionChoices { get; set; }

        public PatientSurgery PatientSurgery
        {
            get { return _patientSurgery; }
            set
            {
                _patientSurgery = value;
                if (value != null)
                {
                    SurgeryTemplateId = value.SurgeryType.SurgeryTemplates.First().Id;
                    PatientSurgeryQuestionAnswers = value.PatientSurgeryQuestionAnswers;
                    PatientSurgeryQuestionChoices = value.PatientSurgeryQuestionChoices;
                    HealthAssessmentViewPresenter.SurgeryTemplateId = SurgeryTemplateId;
                    HealthAssessmentViewPresenter.PatientSurgery = value;
                    HealthAssessmentViewPresenter.PatientSurgeryQuestionChoices = PatientSurgeryQuestionChoices;
                    HeightWeightViewPresenter.PatientSurgery = value;
                    HeightWeightViewPresenter.PatientSurgeryQuestionAnswers = PatientSurgeryQuestionAnswers;
                    WorkUpViewPresenter.SurgeryTemlateId = SurgeryTemplateId;
                    WorkUpViewPresenter.PatientSurgery = value;
                    WorkUpViewPresenter.PatientSurgeryQuestionAnswers = PatientSurgeryQuestionAnswers;
                    WorkUpViewPresenter.PatientSurgeryQuestionChoices = PatientSurgeryQuestionChoices;
                    OpRoomViewPresenter.SurgeryTemlateId = SurgeryTemplateId;
                    OpRoomViewPresenter.PatientSurgery = value;
                    OpRoomViewPresenter.PatientSurgeryQuestionAnswers = PatientSurgeryQuestionAnswers;
                    OpRoomViewPresenter.PatientSurgeryQuestionChoices = PatientSurgeryQuestionChoices;
                    PostOpViewPresenter.SurgeryTemlateId = SurgeryTemplateId;
                    PostOpViewPresenter.PatientSurgery = value;
                    PostOpViewPresenter.PatientSurgeryQuestionAnswers = PatientSurgeryQuestionAnswers;
                    PostOpViewPresenter.PatientSurgeryQuestionChoices = PatientSurgeryQuestionChoices;
                    ActiveSurgeryType = value.SurgeryType;
                    LoadAnswers();
                }
            }
        }

        public IEnumerable<SurgeryType> SurgeryTypes
        {
            get { return _surgeryTypes; }
        }

        public IEnumerable<Choice> EyeContext { get; set; }

        public int SurgeryTypeId { get; set; }
        public int SurgeryTemplateId { get; set; }

        public int TopNoteQuestionId { get; set; }
        public int BottomNoteQuestionId { get; set; }

        public PatientSurgeryQuestionAnswer TopNoteAnswer { get; set; }
        public PatientSurgeryQuestionAnswer BottomNoteAnswer { get; set; }

        public IEnumerable<QuestionAnswerArguments> SurgeryAnswerArgs { get; set; }
        public IEnumerable<QuestionChoiceArguments> SurgeryChoiceArgs { get; set; }
        public int AppointmentId { get; set; }

        public IEnumerable<Question> Questions
        {
            get { return _questions; }
            set
            {
                _questions = value;
                if (_questions != null)
                {
                    Question[] questions = _questions.ToArray();
                    TopNoteQuestionId = questions.First(q => q.Label.ToUpper() == "NOTE BOX TOP").Id;
                    BottomNoteQuestionId = questions.First(q => q.Label.ToUpper() == "NOTE BOX BOTTOM").Id;
                    LoadAnswers();
                    HeightWeightViewPresenter.Questions = questions;
                    WorkUpViewPresenter.Questions = questions;
                    PostOpViewPresenter.Questions = questions;
                }
            }
        }

        public IEnumerable<Choice> Choices
        {
            get { return _choices; }
            set
            {
                _choices = value;
                if (_choices != null)
                {
                    Choice[] choices = _choices.ToArray();
                    EyeContext = choices.Where(qc => qc.Question.Label.ToUpper() == "EYE CONTEXT").ToArray();
                    WorkUpViewPresenter.Choices = choices;
                    OpRoomViewPresenter.Choices = choices;
                }
            }
        }

        #region ILoadable Members

        /// <summary>
        ///   Gets the load actions.
        /// </summary>
        public IEnumerable<Action> LoadActions
        {
            get
            {
                var actions = new Action[]
                                  {
                                      () => _surgeryTypes = _practiceRepository.SurgeryTypes.Include(st => st.SurgeryTemplates).OrderBy(st => st.Name).ToArray(),
                                      () => Questions = _practiceRepository.Questions.ToArray(),
                                      () => Choices = _practiceRepository.Choices.Include(ch => ch.Question).ToArray(),
                                      () => ActiveAppointment = _practiceRepository.Appointments.OfType<UserAppointment>().Include(a => a.Encounter.Patient).Include(a => a.Encounter.ServiceLocation).Include(a => a.User).FirstOrDefault(a => a.Id == AppointmentId),
                                      () => PatientSurgery = _practiceRepository.PatientSurgeries.Include(ps => ps.SurgeryType).Include(ps => ps.SurgeryType.SurgeryTemplates).Include(ps => ps.PatientSurgeryQuestionAnswers).Include(ps => ps.PatientSurgeryQuestionChoices).Include(ps => ps.PatientSurgeryQuestionChoices).FirstOrDefault(ps => ps.Encounter.Appointments.FirstOrDefault().Id == AppointmentId)
                                  }
                    ;
                return actions;
            }
        }

        #endregion

        /// <summary>
        ///   Gets the PatientSurgeryQuestionAnswer.
        /// </summary>
        /// <param name="questionId"> The question id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionAnswer GetAnswer(int questionId)
        {
            if (PatientSurgeryQuestionAnswers != null)
            {
                return PatientSurgeryQuestionAnswers.FirstOrDefault(ans => ans.QuestionId == questionId);
            }
            return null;
        }

        public void LoadAnswers()
        {
            if (TopNoteQuestionId > 0)
            {
                TopNoteAnswer = GetAnswer(TopNoteQuestionId);
            }
            if (BottomNoteQuestionId > 0)
            {
                BottomNoteAnswer = GetAnswer(BottomNoteQuestionId);
            }
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionChoice.
        /// </summary>
        /// <param name="choiceId"> The choice id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionChoice GetChoice(int choiceId)
        {
            if (PatientSurgeryQuestionChoices != null)
            {
                return PatientSurgeryQuestionChoices.FirstOrDefault(ch => ch.ChoiceId == choiceId);
            }
            return null;
        }

        /// <summary>
        ///   Saves the PatientSurgery instance.
        /// </summary>
        public void Save()
        {
            var arguments = new PatientSurgeryArguments();
            if (PatientSurgery != null)
            {
                arguments.PatientSurgery = PatientSurgery;
                arguments.SurgeryType = ActiveSurgeryType;
                arguments.SurgeryTypeId = SurgeryTypeId;
            }
            else
            {
                arguments.Encounter = ActiveAppointment.Encounter;
                arguments.EncounterId = ActiveAppointment.Id;
                arguments.SurgeonUserId = ((UserAppointment)ActiveAppointment).User.Id;
                arguments.SurgeryType = ActiveSurgeryType;
                arguments.SurgeryTypeId = SurgeryTypeId;
            }
            arguments.SurgeryQuestionAnswers = SurgeryAnswerArgs;
            arguments.SurgeryQuestionChoices = SurgeryChoiceArgs;
            arguments.HealthAssessmentQuestionChoices = HealthAssessmentViewPresenter.HealthAssessmentArgs;
            arguments.HeightWeightQuestionAnswers = HeightWeightViewPresenter.HeightWeightArgs;
            arguments.WorkUpQuestionAnswers = WorkUpViewPresenter.WorkUpAnswerArgs;
            arguments.WorkUpQuestionChoices = WorkUpViewPresenter.WorkUpChoiceArgs;
            arguments.OpRoomQuestionAnswers = OpRoomViewPresenter.OpRoomAnswerArgs;
            arguments.OpRoomQuestionChoices = OpRoomViewPresenter.OpRoomChoiceArgs;
            arguments.PostOpQuestionAnswers = PostOpViewPresenter.PostOpAnswerArgs;
            arguments.PostOpQuestionChoices = PostOpViewPresenter.PostOpChoiceArgs;

            PatientSurgery newPatientSurgery = _surgeryManager.SaveAndGetPatientSurgery(arguments);
            PatientSurgery = newPatientSurgery;
        }
    }
}