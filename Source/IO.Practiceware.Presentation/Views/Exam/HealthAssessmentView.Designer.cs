using IO.Practiceware.Presentation.Views.Common.Controls;
using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class HealthAssessmentView : HealthAssessmentViewBase
	{

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.AssessmentPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.CustomDynamicPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DynamicPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NavigationPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NextButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.PreviousButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.QCControl = new QuestionChoiceView();
			this.AssessmentPanel.SuspendLayout();
			this.CustomDynamicPanel.SuspendLayout();
			this.DynamicPanel.SuspendLayout();
			this.NavigationPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//AssessmentPanel
			//
			this.AssessmentPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.AssessmentPanel.Controls.Add(this.CustomDynamicPanel);
			this.AssessmentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AssessmentPanel.Location = new System.Drawing.Point(0, 0);
			this.AssessmentPanel.Name = "AssessmentPanel";
			this.AssessmentPanel.Size = new System.Drawing.Size(906, 579);
			this.AssessmentPanel.TabIndex = 17;
			//
			//CustomDynamicPanel
			//
			this.CustomDynamicPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.CustomDynamicPanel.Controls.Add(this.DynamicPanel);
			this.CustomDynamicPanel.Controls.Add(this.NavigationPanel);
			this.CustomDynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CustomDynamicPanel.Location = new System.Drawing.Point(0, 0);
			this.CustomDynamicPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.CustomDynamicPanel.Name = "CustomDynamicPanel";
			this.CustomDynamicPanel.Size = new System.Drawing.Size(906, 579);
			this.CustomDynamicPanel.TabIndex = 87;
			//
			//DynamicPanel
			//
			this.DynamicPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.DynamicPanel.Controls.Add(this.QCControl);
			this.DynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DynamicPanel.Location = new System.Drawing.Point(0, 0);
			this.DynamicPanel.Name = "DynamicPanel";
			this.DynamicPanel.Size = new System.Drawing.Size(906, 509);
			this.DynamicPanel.TabIndex = 0;
			//
			//NavigationPanel
			//
			this.NavigationPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.NavigationPanel.Controls.Add(this.NextButton);
			this.NavigationPanel.Controls.Add(this.PreviousButton);
			this.NavigationPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.NavigationPanel.Location = new System.Drawing.Point(0, 509);
			this.NavigationPanel.Name = "NavigationPanel";
			this.NavigationPanel.Size = new System.Drawing.Size(906, 70);
			this.NavigationPanel.TabIndex = 2;
			this.NavigationPanel.Visible = false;
			//
			//NextButton
			//
			this.NextButton.Enabled = false;
			this.NextButton.Location = new System.Drawing.Point(803, 4);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(100, 60);
			this.NextButton.TabIndex = 1;
			this.NextButton.Text = "Next";
			//
			//PreviousButton
			//
			this.PreviousButton.Enabled = false;
			this.PreviousButton.Location = new System.Drawing.Point(5, 4);
			this.PreviousButton.Name = "PreviousButton";
			this.PreviousButton.Size = new System.Drawing.Size(100, 60);
			this.PreviousButton.TabIndex = 0;
			this.PreviousButton.Text = "Previous";
			//
			//QCControl
			//
			this.QCControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QCControl.ChoiceId1 = 0;
			this.QCControl.ChoiceId2 = 0;
			this.QCControl.InteractionContext = null;
			this.QCControl.Location = new System.Drawing.Point(18, 9);
			this.QCControl.Name = "QCControl";
			this.QCControl.Note = "";
			this.QCControl.QuestionId = 0;
			this.QCControl.SelChoiceId = 0;
			this.QCControl.Size = new System.Drawing.Size(445, 47);
			this.QCControl.TabIndex = 0;
			//
			//HealthAssessmentControl
			//
			this.Controls.Add(this.AssessmentPanel);
			this.Name = "HealthAssessmentControl";
			this.Size = new System.Drawing.Size(906, 579);
			this.AssessmentPanel.ResumeLayout(false);
			this.CustomDynamicPanel.ResumeLayout(false);
			this.DynamicPanel.ResumeLayout(false);
			this.NavigationPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel AssessmentPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel CustomDynamicPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel NavigationPanel;
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_NextButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button NextButton {
			get { return withEventsField_NextButton; }
			set {
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click -= NextButtonClick;
				}
				withEventsField_NextButton = value;
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click += NextButtonClick;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_PreviousButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button PreviousButton {
			get { return withEventsField_PreviousButton; }
			set {
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click -= PreviousButtonClick;
				}
				withEventsField_PreviousButton = value;
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click += PreviousButtonClick;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel DynamicPanel;

		internal QuestionChoiceView QCControl;
	}
	public class HealthAssessmentViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<HealthAssessmentViewPresenter>
	{
	}
}
