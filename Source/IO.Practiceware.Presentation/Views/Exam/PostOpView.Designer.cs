using IO.Practiceware.Presentation.Views.Common.Controls;
using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class PostOpView : PostOpViewBase
	{

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.PostOpPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.CustomBPDynamicControlPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DynamicBPPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.BPControl = new BloodPressureView();
			this.BpPreviousButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.BpNextButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.StaticPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.IVOutQAControl = new QuestionAnswerView();
			this.PulseText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label5 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.RespText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label4 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.O2Text = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label3 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.IvOutText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.CustomDynamicControlPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DynamicPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.QCControl = new QuestionChoiceView();
			this.PreviousButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.NextButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.PostOpPanel.SuspendLayout();
			this.CustomBPDynamicControlPanel.SuspendLayout();
			this.DynamicBPPanel.SuspendLayout();
			this.StaticPanel.SuspendLayout();
			this.CustomDynamicControlPanel.SuspendLayout();
			this.DynamicPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//PostOpPanel
			//
			this.PostOpPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.PostOpPanel.Controls.Add(this.CustomBPDynamicControlPanel);
			this.PostOpPanel.Controls.Add(this.StaticPanel);
			this.PostOpPanel.Controls.Add(this.CustomDynamicControlPanel);
			this.PostOpPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PostOpPanel.Location = new System.Drawing.Point(0, 0);
			this.PostOpPanel.Name = "PostOpPanel";
			this.PostOpPanel.Size = new System.Drawing.Size(980, 578);
			this.PostOpPanel.TabIndex = 0;
			//
			//CustomBPDynamicControlPanel
			//
			this.CustomBPDynamicControlPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.CustomBPDynamicControlPanel.Controls.Add(this.DynamicBPPanel);
			this.CustomBPDynamicControlPanel.Controls.Add(this.BpPreviousButton);
			this.CustomBPDynamicControlPanel.Controls.Add(this.BpNextButton);
			this.CustomBPDynamicControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.CustomBPDynamicControlPanel.Location = new System.Drawing.Point(0, 330);
			this.CustomBPDynamicControlPanel.Name = "CustomBPDynamicControlPanel";
			this.CustomBPDynamicControlPanel.Size = new System.Drawing.Size(980, 110);
			this.CustomBPDynamicControlPanel.TabIndex = 91;
			//
			//DynamicBPPanel
			//
			this.DynamicBPPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.DynamicBPPanel.Controls.Add(this.BPControl);
			this.DynamicBPPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DynamicBPPanel.Location = new System.Drawing.Point(56, 0);
			this.DynamicBPPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.DynamicBPPanel.Name = "DynamicBPPanel";
			this.DynamicBPPanel.Size = new System.Drawing.Size(868, 110);
			this.DynamicBPPanel.TabIndex = 88;
			//
			//BPControl
			//
			this.BPControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.BPControl.Value = null;
			this.BPControl.InteractionContext = null;
			this.BPControl.Location = new System.Drawing.Point(12, 3);
			this.BPControl.Name = "BPControl";
			this.BPControl.Note = "";
			this.BPControl.QuestionId = 0;
			this.BPControl.Size = new System.Drawing.Size(415, 47);
			this.BPControl.TabIndex = 0;
			//
			//BpPreviousButton
			//
			this.BpPreviousButton.Dock = System.Windows.Forms.DockStyle.Left;
			this.BpPreviousButton.Enabled = false;
			this.BpPreviousButton.Location = new System.Drawing.Point(0, 0);
			this.BpPreviousButton.Name = "BpPreviousButton";
			this.BpPreviousButton.Size = new System.Drawing.Size(56, 110);
			this.BpPreviousButton.TabIndex = 0;
			this.BpPreviousButton.Text = "Previous";
			this.BpPreviousButton.Visible = false;
			//
			//BpNextButton
			//
			this.BpNextButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.BpNextButton.Enabled = false;
			this.BpNextButton.Location = new System.Drawing.Point(924, 0);
			this.BpNextButton.Name = "BpNextButton";
			this.BpNextButton.Size = new System.Drawing.Size(56, 110);
			this.BpNextButton.TabIndex = 1;
			this.BpNextButton.Text = "Next";
			this.BpNextButton.Visible = false;
			//
			//StaticPanel
			//
			this.StaticPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.StaticPanel.Controls.Add(this.IVOutQAControl);
			this.StaticPanel.Controls.Add(this.PulseText);
			this.StaticPanel.Controls.Add(this.Label5);
			this.StaticPanel.Controls.Add(this.RespText);
			this.StaticPanel.Controls.Add(this.Label4);
			this.StaticPanel.Controls.Add(this.O2Text);
			this.StaticPanel.Controls.Add(this.Label3);
			this.StaticPanel.Controls.Add(this.IvOutText);
			this.StaticPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.StaticPanel.Location = new System.Drawing.Point(0, 220);
			this.StaticPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.StaticPanel.Name = "StaticPanel";
			this.StaticPanel.Size = new System.Drawing.Size(980, 110);
			this.StaticPanel.TabIndex = 90;
			//
			//IVOutQAControl
			//
			this.IVOutQAControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.IVOutQAControl.InteractionContext = null;
			this.IVOutQAControl.Location = new System.Drawing.Point(12, 5);
			this.IVOutQAControl.Name = "IVOutQAControl";
			this.IVOutQAControl.Note = "";
			this.IVOutQAControl.QuestionId = 0;
			this.IVOutQAControl.Size = new System.Drawing.Size(259, 47);
			this.IVOutQAControl.TabIndex = 76;
			this.IVOutQAControl.Value = "";
			//
			//PulseText
			//
			this.PulseText.Location = new System.Drawing.Point(479, 57);
			this.PulseText.Name = "PulseText";
			this.PulseText.Size = new System.Drawing.Size(61, 46);
			this.PulseText.TabIndex = 75;
			//
			//Label5
			//
			this.Label5.Location = new System.Drawing.Point(417, 67);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(56, 25);
			this.Label5.StyleName = "";
			this.Label5.TabIndex = 74;
			this.Label5.Text = "Pulse";
			//
			//RespText
			//
			this.RespText.Location = new System.Drawing.Point(131, 59);
			this.RespText.Name = "RespText";
			this.RespText.Size = new System.Drawing.Size(61, 46);
			this.RespText.TabIndex = 73;
			//
			//Label4
			//
			this.Label4.Location = new System.Drawing.Point(21, 67);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(56, 25);
			this.Label4.StyleName = "";
			this.Label4.TabIndex = 72;
			this.Label4.Text = "Resp.";
			//
			//O2Text
			//
			this.O2Text.Location = new System.Drawing.Point(858, 57);
			this.O2Text.Name = "O2Text";
			this.O2Text.Size = new System.Drawing.Size(63, 46);
			this.O2Text.TabIndex = 71;
			//
			//Label3
			//
			this.Label3.Location = new System.Drawing.Point(750, 67);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(102, 25);
			this.Label3.StyleName = "";
			this.Label3.TabIndex = 70;
			this.Label3.Text = "O2 Saturation";
			//
			//IvOutText
			//
			this.IvOutText.Location = new System.Drawing.Point(280, 5);
			this.IvOutText.Name = "IvOutText";
			this.IvOutText.Size = new System.Drawing.Size(640, 46);
			this.IvOutText.TabIndex = 55;
			//
			//CustomDynamicControlPanel
			//
			this.CustomDynamicControlPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.CustomDynamicControlPanel.Controls.Add(this.DynamicPanel);
			this.CustomDynamicControlPanel.Controls.Add(this.PreviousButton);
			this.CustomDynamicControlPanel.Controls.Add(this.NextButton);
			this.CustomDynamicControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.CustomDynamicControlPanel.Location = new System.Drawing.Point(0, 0);
			this.CustomDynamicControlPanel.Name = "CustomDynamicControlPanel";
			this.CustomDynamicControlPanel.Size = new System.Drawing.Size(980, 220);
			this.CustomDynamicControlPanel.TabIndex = 89;
			//
			//DynamicPanel
			//
			this.DynamicPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.DynamicPanel.Controls.Add(this.QCControl);
			this.DynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DynamicPanel.Location = new System.Drawing.Point(56, 0);
			this.DynamicPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.DynamicPanel.Name = "DynamicPanel";
			this.DynamicPanel.Size = new System.Drawing.Size(868, 220);
			this.DynamicPanel.TabIndex = 88;
			//
			//QCControl
			//
			this.QCControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QCControl.ChoiceId1 = 0;
			this.QCControl.ChoiceId2 = 0;
			this.QCControl.InteractionContext = null;
			this.QCControl.Location = new System.Drawing.Point(12, 3);
			this.QCControl.Name = "QCControl";
			this.QCControl.Note = "";
			this.QCControl.QuestionId = 0;
			this.QCControl.SelChoiceId = 0;
			this.QCControl.Size = new System.Drawing.Size(415, 47);
			this.QCControl.TabIndex = 0;
			//
			//PreviousButton
			//
			this.PreviousButton.Dock = System.Windows.Forms.DockStyle.Left;
			this.PreviousButton.Enabled = false;
			this.PreviousButton.Location = new System.Drawing.Point(0, 0);
			this.PreviousButton.Name = "PreviousButton";
			this.PreviousButton.Size = new System.Drawing.Size(56, 220);
			this.PreviousButton.TabIndex = 0;
			this.PreviousButton.Text = "Previous";
			this.PreviousButton.Visible = false;
			//
			//NextButton
			//
			this.NextButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.NextButton.Enabled = false;
			this.NextButton.Location = new System.Drawing.Point(924, 0);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(56, 220);
			this.NextButton.TabIndex = 1;
			this.NextButton.Text = "Next";
			this.NextButton.Visible = false;
			//
			//PostOpControl
			//
			this.Controls.Add(this.PostOpPanel);
			this.Name = "PostOpControl";
			this.Size = new System.Drawing.Size(980, 578);
			this.PostOpPanel.ResumeLayout(false);
			this.CustomBPDynamicControlPanel.ResumeLayout(false);
			this.DynamicBPPanel.ResumeLayout(false);
			this.StaticPanel.ResumeLayout(false);
			this.CustomDynamicControlPanel.ResumeLayout(false);
			this.DynamicPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel PostOpPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel DynamicPanel;
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_NextButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button NextButton {
			get { return withEventsField_NextButton; }
			set {
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click -= NextButtonClick;
				}
				withEventsField_NextButton = value;
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click += NextButtonClick;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_PreviousButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button PreviousButton {
			get { return withEventsField_PreviousButton; }
			set {
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click -= PreviousButtonClick;
				}
				withEventsField_PreviousButton = value;
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click += PreviousButtonClick;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel CustomDynamicControlPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel StaticPanel;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox IvOutText;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox PulseText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label5;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox RespText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label4;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox O2Text;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label3;
		internal QuestionChoiceView QCControl;
		internal QuestionAnswerView IVOutQAControl;
		internal Soaf.Presentation.Controls.WindowsForms.Panel CustomBPDynamicControlPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel DynamicBPPanel;
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_BpPreviousButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button BpPreviousButton {
			get { return withEventsField_BpPreviousButton; }
			set {
				if (withEventsField_BpPreviousButton != null) {
					withEventsField_BpPreviousButton.Click -= BpPreviousButtonClick;
				}
				withEventsField_BpPreviousButton = value;
				if (withEventsField_BpPreviousButton != null) {
					withEventsField_BpPreviousButton.Click += BpPreviousButtonClick;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_BpNextButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button BpNextButton {
			get { return withEventsField_BpNextButton; }
			set {
				if (withEventsField_BpNextButton != null) {
					withEventsField_BpNextButton.Click -= BpNextButtonClick;
				}
				withEventsField_BpNextButton = value;
				if (withEventsField_BpNextButton != null) {
					withEventsField_BpNextButton.Click += BpNextButtonClick;
				}
			}
		}

		internal BloodPressureView BPControl;
	}

	public class PostOpViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<PostOpViewPresenter>
	{
	}
}
