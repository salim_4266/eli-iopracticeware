﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Services.Exam;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Exam
{
    /// <summary>
    ///   class WorkUpControl
    /// </summary>
    public partial class WorkUpView
    {
        private int _gap;
        private int _left;
        private int _width;

        public WorkUpView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            WorkUpPanel.Visible = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadPresenter();
        }

        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            LoadWorkUpControls();
            WorkUpPanel.Visible = true;
            if (Presenter.PatientSurgery != null)
            {
                LoadWorkUpData();
            }
        }

        /// <summary>
        ///   Loads the work up data.
        /// </summary>
        private void LoadWorkUpData()
        {
            if (DynamicPanel.Controls.Count > 0)
            {
                foreach (object questionChoiceControl in DynamicPanel.Controls)
                {
                    int choiceid1 = ((QuestionChoiceView) questionChoiceControl).ChoiceId1;
                    int choiceid2 = ((QuestionChoiceView) questionChoiceControl).ChoiceId2;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                    if (choice != null)
                    {
                        if (((QuestionChoiceView) questionChoiceControl).ChoiceId1 == choice.ChoiceId)
                        {
                            ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                            ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                        }
                    }
                    else
                    {
                        choice = Presenter.GetChoice(choiceid2);
                        if (choice != null)
                        {
                            if (((QuestionChoiceView) questionChoiceControl).ChoiceId2 == choice.ChoiceId)
                            {
                                ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                                ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                            }
                        }
                    }
                }
            }
            if (BPTypeList.ItemsSource != null)
            {
                foreach (object item in BPTypeList.ItemsSource)
                {
                    object item1 = item;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(((Choice) item1).Id);
                    if (choice != null)
                    {
                        BPTypeList.SelectedItem = item1;
                        BPTypeList.Refresh();
                    }
                }
            }
            if (IVInQAControl.QuestionId > 0)
            {
                PatientSurgeryQuestionAnswer qAnswer = Presenter.GetAnswer(IVInQAControl.QuestionId);
                if (qAnswer != null)
                {
                    IVInQAControl.Value = qAnswer.Value;
                    IVInQAControl.Note = qAnswer.Note;
                }
            }
            if (Presenter.IvInNotesAnswer != null)
            {
                IVInText.Text = Presenter.IvInNotesAnswer.Value;
            }
            if (Presenter.RespAnswer != null)
            {
                RespText.Text = Presenter.RespAnswer.Value;
            }
            if (Presenter.PulseAnswer != null)
            {
                PulseText.Text = Presenter.PulseAnswer.Value;
            }
            if (Presenter.BpSystolicAnswer != null)
            {
                BPSystolicText.Text = Presenter.BpSystolicAnswer.Value;
            }
            if (Presenter.BpDiastolicAnswer != null)
            {
                BPDiastolicText.Text = Presenter.BpDiastolicAnswer.Value;
            }
            if (Presenter.O2SaturationAnswer != null)
            {
                O2Text.Text = Presenter.O2SaturationAnswer.Value;
            }
        }

        /// <summary>
        ///   Loads the work up controls.
        /// </summary>
        private void LoadWorkUpControls()
        {
            DynamicPanel.Visible = false;
            bool firstControlLoaded = false;
            _gap = 30;
            if (Presenter.ScreenQuestions != null)
            {
                foreach (SurgeryTemplateScreenQuestion sq in Presenter.ScreenQuestions.OrderBy(s => s.Question.OrdinalId))
                {
                    if (sq.Question.Choices.Count() > 1)
                    {
                        if (DynamicPanel.Controls.Count == 1 & firstControlLoaded == false)
                        {
                            QCControl.AddQuestion(sq.Question);
                            firstControlLoaded = true;
                        }
                        else
                        {
                            AddNextQuestion(sq);
                        }
                    }
                }
            }
            AddStaticQuestion();
            if (DynamicPanel.Controls.Count > 6)
            {
                _width = QCControl.Width;
                _left = QCControl.Left;
                NextButton.Visible = true;
                PreviousButton.Visible = true;
                NextButton.Enabled = true;
            }
            if (firstControlLoaded)
            {
                DynamicPanel.Visible = true;
            }
            if (Presenter.BpTypes != null)
            {
                BPTypeList.ItemsSource = Presenter.BpTypes;
                BPTypeList.DisplayMemberPath = "Name";
            }
        }

        /// <summary>
        ///   Adds the next question.
        /// </summary>
        /// <param name="screenQuestion"> The screen question. </param>
        private void AddNextQuestion(SurgeryTemplateScreenQuestion screenQuestion)
        {
            int n = DynamicPanel.Controls.Count;
            var nQcControl = new QuestionChoiceView();
            nQcControl.Visible = true;
            if (n%3 == 0)
            {
                nQcControl.Top = (DynamicPanel.Controls[n - 1]).Top;
                nQcControl.Left = (DynamicPanel.Controls[0]).Left + (DynamicPanel.Controls[0]).Width + _gap;
                nQcControl.Width = (DynamicPanel.Controls[0]).Width;
            }
            else
            {
                nQcControl.Top = (DynamicPanel.Controls[0]).Top + (DynamicPanel.Controls[0]).Height + 5;
                nQcControl.Left = (DynamicPanel.Controls[0]).Left;
                nQcControl.Width = (DynamicPanel.Controls[0]).Width;
            }
            nQcControl.AddQuestion(screenQuestion.Question);
            nQcControl.Name = QCControl.Name + (n);
            DynamicPanel.Controls.Add(nQcControl);
            nQcControl.BringToFront();
            nQcControl.Refresh();
        }

        /// <summary>
        ///   Adds the static question.
        /// </summary>
        private void AddStaticQuestion()
        {
            if (Presenter.IvInQuestion != null)
            {
                IVInQAControl.AddQuestion(Presenter.IvInQuestion);
            }
        }

        /// <summary>
        ///   Moves to the next.
        /// </summary>
        private void MoveNext()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left -= (_width + _gap);
            }
            NextButton.Enabled = DynamicPanel.Controls[0].Left > (_width + _gap + _left);
            }

        /// <summary>
        ///   Moves to the previous.
        /// </summary>
        private void MovePrevious()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left += (_width + _gap);
            }
            PreviousButton.Enabled = QCControl.Left < _left;
            }

        private void NextButtonClick(object sender, EventArgs e)
        {
            MoveNext();
            PreviousButton.Enabled = true;
        }

        private void PreviousButtonClick(object sender, EventArgs e)
        {
            MovePrevious();
            NextButton.Enabled = true;
        }

        /// <summary>
        ///   Gets the work up user selections.
        /// </summary>
        public void GetWorkUpSelections()
        {
            GetWorkUpSelectedQuestionChoices();
            GetWorkUpSelectedQuestionAnswers();
        }

        /// <summary>
        ///   Gets the work up question choices selected by the user.
        /// </summary>
        private void GetWorkUpSelectedQuestionChoices()
        {
            var arguments = new List<QuestionChoiceArguments>();
            foreach (object qc in DynamicPanel.Controls)
            {
                object qc1 = qc;
                if (((QuestionChoiceView) qc1).SelChoiceId != 0)
                {
                    var argument = new QuestionChoiceArguments();
                    int choiceid1 = ((QuestionChoiceView) qc1).ChoiceId1;
                    int choiceid2 = ((QuestionChoiceView) qc1).ChoiceId2;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                    if (choice != null)
                    {
                        argument.PatientSurgeryQuestionChoiceId = choice.Id;
                    }
                    else
                    {
                        choice = Presenter.GetChoice(choiceid2);
                        if (choice != null)
                        {
                            argument.PatientSurgeryQuestionChoiceId = choice.Id;
                        }
                    }
                    argument.QuestionId = ((QuestionChoiceView) qc1).QuestionId;
                    argument.ChoiceId = ((QuestionChoiceView) qc1).SelChoiceId;
                    argument.Note = ((QuestionChoiceView) qc1).Note;
                    arguments.Add(argument);
                }
            }
            if (BPTypeList.SelectedItem != null)
            {
                var argument = new QuestionChoiceArguments();
                foreach (object item in BPTypeList.ItemsSource)
                {
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(((Choice) item).Id);
                    if (choice != null)
                    {
                        argument.PatientSurgeryQuestionChoiceId = choice.Id;
                    }
                }
                argument.QuestionId = ((IEnumerable<Choice>) BPTypeList.ItemsSource).First().QuestionId;
                argument.ChoiceId = ((Choice) BPTypeList.SelectedItem).Id;
                argument.Note = "";
                arguments.Add(argument);
            }
            Presenter.WorkUpChoiceArgs = arguments;
        }

        /// <summary>
        ///   Gets the work up question answers selected by the user.
        /// </summary>
        private void GetWorkUpSelectedQuestionAnswers()
        {
            var arguments = new List<QuestionAnswerArguments>();
            //Insert/update data
            if (!string.IsNullOrEmpty(IVInQAControl.Value.Trim()) & IVInQAControl.QuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(IVInQAControl.QuestionId);
                if (answer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = answer.Id;
                }
                argument.QuestionId = IVInQAControl.QuestionId;
                argument.Value = IVInQAControl.Value;
                argument.Note = IVInQAControl.Note;
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(IVInQAControl.Value.Trim()) & string.IsNullOrEmpty(IVInQAControl.Note.Trim()) & IVInQAControl.QuestionId > 0)
            {
                PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(IVInQAControl.QuestionId);
                if (answer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = answer.Id;
                    argument.QuestionId = IVInQAControl.QuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(IVInText.Text.Trim()) & Presenter.IvInNotesQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.IvInNotesAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.IvInNotesAnswer.Id;
                }
                argument.QuestionId = Presenter.IvInNotesQuestionId;
                argument.Value = IVInText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(IVInText.Text.Trim()) & Presenter.IvInNotesQuestionId > 0)
            {
                if (Presenter.IvInNotesAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.IvInNotesAnswer.Id;
                    argument.QuestionId = Presenter.IvInNotesQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
            }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(RespText.Text.Trim()) & Presenter.RespQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.RespAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.RespAnswer.Id;
                }
                argument.QuestionId = Presenter.RespQuestionId;
                argument.Value = RespText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(RespText.Text.Trim()) & Presenter.RespQuestionId > 0)
            {
                if (Presenter.RespAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.RespAnswer.Id;
                    argument.QuestionId = Presenter.RespQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(PulseText.Text.Trim()) & Presenter.PulseQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.PulseAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.PulseAnswer.Id;
                }
                argument.QuestionId = Presenter.PulseQuestionId;
                argument.Value = PulseText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(PulseText.Text.Trim()) & Presenter.PulseQuestionId > 0)
            {
                if (Presenter.PulseAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.PulseAnswer.Id;
                    argument.QuestionId = Presenter.PulseQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(BPSystolicText.Text.Trim()) & Presenter.BpSystolicQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.BpSystolicAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BpSystolicAnswer.Id;
                }
                argument.QuestionId = Presenter.BpSystolicQuestionId;
                argument.Value = BPSystolicText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(BPSystolicText.Text.Trim()) & Presenter.BpSystolicQuestionId > 0)
            {
                if (Presenter.BpSystolicAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BpSystolicAnswer.Id;
                    argument.QuestionId = Presenter.BpSystolicQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(BPDiastolicText.Text.Trim()) & Presenter.BpDiastolicQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.BpDiastolicAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BpDiastolicAnswer.Id;
                }
                argument.QuestionId = Presenter.BpDiastolicQuestionId;
                argument.Value = BPDiastolicText.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(BPDiastolicText.Text.Trim()) & Presenter.BpDiastolicQuestionId > 0)
            {
                if (Presenter.BpDiastolicAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.BpDiastolicAnswer.Id;
                    argument.QuestionId = Presenter.BpDiastolicQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            //Insert/update data
            if (!string.IsNullOrEmpty(O2Text.Text.Trim()) & Presenter.O2SaturationQuestionId > 0)
            {
                var argument = new QuestionAnswerArguments();
                if (Presenter.O2SaturationAnswer != null)
                {
                    argument.PatientSurgeryQuestionAnswerId = Presenter.O2SaturationAnswer.Id;
                }
                argument.QuestionId = Presenter.O2SaturationQuestionId;
                argument.Value = O2Text.Text;
                argument.Note = "";
                arguments.Add(argument);
            }
            //Delete record if the data is empty
            else if (string.IsNullOrEmpty(O2Text.Text.Trim()) & Presenter.O2SaturationQuestionId > 0)
            {
                if (Presenter.O2SaturationAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.O2SaturationAnswer.Id;
                    argument.QuestionId = Presenter.O2SaturationQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            Presenter.WorkUpAnswerArgs = arguments;
        }
    }

    /// <summary>
    ///   class WorkUpViewPresenter
    /// </summary>
    public class WorkUpViewPresenter : ILoadable
    {
        private const string ScreenName = "Work-Up";
        private readonly IPracticeRepository _practiceRepository;
        public PatientSurgeryQuestionAnswer BpDiastolicAnswer;
        public int BpDiastolicQuestionId;
        public PatientSurgeryQuestionAnswer BpSystolicAnswer;
        public int BpSystolicQuestionId;
        public PatientSurgeryQuestionAnswer IvInAnswer;
        public PatientSurgeryQuestionAnswer IvInNotesAnswer;
        public int IvInNotesQuestionId;
        public Question IvInQuestion;
        public PatientSurgeryQuestionAnswer O2SaturationAnswer;
        public int O2SaturationQuestionId;
        public PatientSurgery PatientSurgery;
        public IEnumerable<PatientSurgeryQuestionChoice> PatientSurgeryQuestionChoices;
        public PatientSurgeryQuestionAnswer PulseAnswer;
        public int PulseQuestionId;
        public PatientSurgeryQuestionAnswer RespAnswer;
        public int RespQuestionId;
        public int SurgeryTemlateId;
        public IEnumerable<QuestionAnswerArguments> WorkUpAnswerArgs;
        public IEnumerable<QuestionChoiceArguments> WorkUpChoiceArgs;
        private IEnumerable<Choice> _choices;
        private IEnumerable<PatientSurgeryQuestionAnswer> _patientSurgeryQuestionAnswers;
        private IEnumerable<Question> _questions;

        public WorkUpViewPresenter(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<Choice> BpTypes { get; set; }

        public IEnumerable<SurgeryTemplateScreenQuestion> ScreenQuestions { get; set; }

        public IEnumerable<Question> Questions
        {
            get { return _questions; }
            set
            {
                _questions = value;
                if (_questions != null)
                {
                    Question[] questions = _questions.ToArray();
                    IvInQuestion = questions.FirstOrDefault(q => q.Label.ToUpper() == "IV IN");
                    IvInNotesQuestionId = questions.First(q => q.Label.ToUpper() == "IV IN NOTES").Id;
                    RespQuestionId = questions.First(q => q.Label.ToUpper() == "WORK-UP RESP").Id;
                    PulseQuestionId = questions.First(q => q.Label.ToUpper() == "WORK-UP PULSE").Id;
                    BpSystolicQuestionId = questions.First(q => q.Label.ToUpper() == "WORK-UP BP SYSTOLIC").Id;
                    BpDiastolicQuestionId = questions.First(q => q.Label.ToUpper() == "WORK-UP BP DIASTOLIC").Id;
                    O2SaturationQuestionId = questions.First(q => q.Label.ToUpper() == "WORK-UP O2 SATURATION").Id;
                    LoadAnswers();
                }
            }
        }

        public IEnumerable<Choice> Choices
        {
            get { return _choices; }
            set
            {
                _choices = value;
                if (_choices != null)
                {
                    BpTypes = _choices.Where(qc => qc.Question.Label.ToUpper() == "WORK-UP BP TYPE").ToArray();
                }
            }
        }

        public IEnumerable<PatientSurgeryQuestionAnswer> PatientSurgeryQuestionAnswers
        {
            get { return _patientSurgeryQuestionAnswers; }
            set
            {
                _patientSurgeryQuestionAnswers = value;
                if (_patientSurgeryQuestionAnswers != null)
                {
                    LoadAnswers();
                }
            }
        }

        #region ILoadable Members

        public IEnumerable<Action> LoadActions
        {
            get
            {
                var actions = new Action[]
                                  {
                                      () => ScreenQuestions = _practiceRepository.SurgeryTemplateScreenQuestions.Include(s => s.Question, s => s.Screen, s => s.Question.Choices).Where(s => s.SurgeryTemplateId == SurgeryTemlateId & s.Screen.Name.ToUpper() == ScreenName.ToUpper()).ToArray()
                                  }
                    ;

                return actions;
            }
        }

        #endregion

        /// <summary>
        ///   Loads the answers.
        /// </summary>
        public void LoadAnswers()
        {
            if (IvInQuestion != null)
            {
                IvInAnswer = GetAnswer(IvInQuestion.Id);
            }
            if (IvInNotesQuestionId > 0)
            {
                IvInNotesAnswer = GetAnswer(IvInNotesQuestionId);
            }
            if (RespQuestionId > 0)
            {
                RespAnswer = GetAnswer(RespQuestionId);
            }
            if (PulseQuestionId > 0)
            {
                PulseAnswer = GetAnswer(PulseQuestionId);
            }
            if (BpSystolicQuestionId > 0)
            {
                BpSystolicAnswer = GetAnswer(BpSystolicQuestionId);
            }
            if (BpDiastolicQuestionId > 0)
            {
                BpDiastolicAnswer = GetAnswer(BpDiastolicQuestionId);
            }
            if (O2SaturationQuestionId > 0)
            {
                O2SaturationAnswer = GetAnswer(O2SaturationQuestionId);
            }
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionAnswer.
        /// </summary>
        /// <param name="questionId"> The question id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionAnswer GetAnswer(int questionId)
        {
            if (PatientSurgeryQuestionAnswers != null)
            {
                return PatientSurgeryQuestionAnswers.FirstOrDefault(ans => ans.QuestionId == questionId);
            }
            return null;
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionChoice.
        /// </summary>
        /// <param name="choiceId"> The choice id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionChoice GetChoice(int choiceId)
        {
            if (PatientSurgeryQuestionChoices != null)
            {
                return PatientSurgeryQuestionChoices.FirstOrDefault(ch => ch.ChoiceId == choiceId);
            }
            return null;
        }
    }
}