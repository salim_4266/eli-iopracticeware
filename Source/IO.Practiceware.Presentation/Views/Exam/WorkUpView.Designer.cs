using IO.Practiceware.Presentation.Views.Common.Controls;
using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class WorkUpView : WorkUpViewBase
	{

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.WorkUpPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.StaticPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.PulseText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label5 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.RespText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label4 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.O2Text = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label3 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.BPTypeList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
			this.Label2 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.BPDiastolicText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.BPSystolicText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.IVInText = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.CustomDynamicControlPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DynamicPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.PreviousButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.NextButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.QCControl = new QuestionChoiceView();
			this.IVInQAControl = new QuestionAnswerView();
			this.WorkUpPanel.SuspendLayout();
			this.StaticPanel.SuspendLayout();
			this.CustomDynamicControlPanel.SuspendLayout();
			this.DynamicPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//WorkUpPanel
			//
			this.WorkUpPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.WorkUpPanel.Controls.Add(this.StaticPanel);
			this.WorkUpPanel.Controls.Add(this.CustomDynamicControlPanel);
			this.WorkUpPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.WorkUpPanel.Location = new System.Drawing.Point(0, 0);
			this.WorkUpPanel.Name = "WorkUpPanel";
			this.WorkUpPanel.Size = new System.Drawing.Size(980, 578);
			this.WorkUpPanel.TabIndex = 0;
			//
			//StaticPanel
			//
			this.StaticPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.StaticPanel.Controls.Add(this.IVInQAControl);
			this.StaticPanel.Controls.Add(this.PulseText);
			this.StaticPanel.Controls.Add(this.Label5);
			this.StaticPanel.Controls.Add(this.RespText);
			this.StaticPanel.Controls.Add(this.Label4);
			this.StaticPanel.Controls.Add(this.O2Text);
			this.StaticPanel.Controls.Add(this.Label3);
			this.StaticPanel.Controls.Add(this.BPTypeList);
			this.StaticPanel.Controls.Add(this.Label2);
			this.StaticPanel.Controls.Add(this.BPDiastolicText);
			this.StaticPanel.Controls.Add(this.BPSystolicText);
			this.StaticPanel.Controls.Add(this.Label1);
			this.StaticPanel.Controls.Add(this.IVInText);
			this.StaticPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.StaticPanel.Location = new System.Drawing.Point(0, 164);
			this.StaticPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.StaticPanel.Name = "StaticPanel";
			this.StaticPanel.Size = new System.Drawing.Size(980, 110);
			this.StaticPanel.TabIndex = 90;
			//
			//PulseText
			//
			this.PulseText.Location = new System.Drawing.Point(858, 59);
			this.PulseText.Name = "PulseText";
			this.PulseText.Size = new System.Drawing.Size(61, 46);
			this.PulseText.TabIndex = 64;
			//
			//Label5
			//
			this.Label5.Location = new System.Drawing.Point(796, 69);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(56, 25);
			this.Label5.StyleName = "";
			this.Label5.TabIndex = 63;
			this.Label5.Text = "Pulse";
			//
			//RespText
			//
			this.RespText.Location = new System.Drawing.Point(858, 6);
			this.RespText.Name = "RespText";
			this.RespText.Size = new System.Drawing.Size(61, 46);
			this.RespText.TabIndex = 62;
			//
			//Label4
			//
			this.Label4.Location = new System.Drawing.Point(796, 15);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(56, 25);
			this.Label4.StyleName = "";
			this.Label4.TabIndex = 61;
			this.Label4.Text = "Resp.";
			//
			//O2Text
			//
			this.O2Text.Location = new System.Drawing.Point(614, 58);
			this.O2Text.Name = "O2Text";
			this.O2Text.Size = new System.Drawing.Size(61, 46);
			this.O2Text.TabIndex = 60;
			//
			//Label3
			//
			this.Label3.Location = new System.Drawing.Point(506, 69);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(102, 25);
			this.Label3.StyleName = "";
			this.Label3.TabIndex = 59;
			this.Label3.Text = "O2 Saturation";
			//
			//BPTypeList
			//
			this.BPTypeList.DisplayMemberPath = "";
			this.BPTypeList.Location = new System.Drawing.Point(272, 61);
			this.BPTypeList.Name = "BPTypeList";
			this.BPTypeList.SelectedIndex = -1;
			this.BPTypeList.SelectedValuePath = "";
			this.BPTypeList.Size = new System.Drawing.Size(143, 33);
			this.BPTypeList.TabIndex = 58;
			//
			//Label2
			//
			this.Label2.Location = new System.Drawing.Point(190, 69);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(11, 25);
			this.Label2.StyleName = "Header";
			this.Label2.TabIndex = 57;
			this.Label2.Text = " / ";
			//
			//BPDiastolicText
			//
			this.BPDiastolicText.Location = new System.Drawing.Point(204, 59);
			this.BPDiastolicText.Name = "BPDiastolicText";
			this.BPDiastolicText.Size = new System.Drawing.Size(61, 46);
			this.BPDiastolicText.TabIndex = 56;
			//
			//BPSystolicText
			//
			this.BPSystolicText.Location = new System.Drawing.Point(128, 59);
			this.BPSystolicText.Name = "BPSystolicText";
			this.BPSystolicText.Size = new System.Drawing.Size(61, 46);
			this.BPSystolicText.TabIndex = 55;
			//
			//Label1
			//
			this.Label1.Location = new System.Drawing.Point(16, 69);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(102, 25);
			this.Label1.StyleName = "";
			this.Label1.TabIndex = 54;
			this.Label1.Text = "Blood Pressure";
			//
			//IVInText
			//
			this.IVInText.Location = new System.Drawing.Point(272, 6);
			this.IVInText.Name = "IVInText";
			this.IVInText.Size = new System.Drawing.Size(403, 46);
			this.IVInText.TabIndex = 53;
			//
			//CustomDynamicControlPanel
			//
			this.CustomDynamicControlPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.CustomDynamicControlPanel.Controls.Add(this.DynamicPanel);
			this.CustomDynamicControlPanel.Controls.Add(this.PreviousButton);
			this.CustomDynamicControlPanel.Controls.Add(this.NextButton);
			this.CustomDynamicControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.CustomDynamicControlPanel.Location = new System.Drawing.Point(0, 0);
			this.CustomDynamicControlPanel.Name = "CustomDynamicControlPanel";
			this.CustomDynamicControlPanel.Size = new System.Drawing.Size(980, 164);
			this.CustomDynamicControlPanel.TabIndex = 89;
			//
			//DynamicPanel
			//
			this.DynamicPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.DynamicPanel.Controls.Add(this.QCControl);
			this.DynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DynamicPanel.Location = new System.Drawing.Point(56, 0);
			this.DynamicPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.DynamicPanel.Name = "DynamicPanel";
			this.DynamicPanel.Size = new System.Drawing.Size(868, 164);
			this.DynamicPanel.TabIndex = 1;
			//
			//PreviousButton
			//
			this.PreviousButton.Dock = System.Windows.Forms.DockStyle.Left;
			this.PreviousButton.Enabled = false;
			this.PreviousButton.Location = new System.Drawing.Point(0, 0);
			this.PreviousButton.Name = "PreviousButton";
			this.PreviousButton.Size = new System.Drawing.Size(56, 164);
			this.PreviousButton.TabIndex = 0;
			this.PreviousButton.Text = "Previous";
			this.PreviousButton.Visible = false;
			//
			//NextButton
			//
			this.NextButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.NextButton.Enabled = false;
			this.NextButton.Location = new System.Drawing.Point(924, 0);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(56, 164);
			this.NextButton.TabIndex = 2;
			this.NextButton.Text = "Next";
			this.NextButton.Visible = false;
			//
			//QCControl
			//
			this.QCControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QCControl.ChoiceId1 = 0;
			this.QCControl.ChoiceId2 = 0;
			this.QCControl.InteractionContext = null;
			this.QCControl.Location = new System.Drawing.Point(12, 3);
			this.QCControl.Name = "QCControl";
			this.QCControl.Note = "";
			this.QCControl.QuestionId = 0;
			this.QCControl.SelChoiceId = 0;
			this.QCControl.Size = new System.Drawing.Size(415, 47);
			this.QCControl.TabIndex = 0;
			//
			//IVInQAControl
			//
			this.IVInQAControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.IVInQAControl.InteractionContext = null;
			this.IVInQAControl.Location = new System.Drawing.Point(7, 6);
			this.IVInQAControl.Name = "IVInQAControl";
			this.IVInQAControl.Note = "";
			this.IVInQAControl.QuestionId = 0;
			this.IVInQAControl.Size = new System.Drawing.Size(259, 47);
			this.IVInQAControl.TabIndex = 65;
			this.IVInQAControl.Value = "";
			//
			//WorkUpControl
			//
			this.Controls.Add(this.WorkUpPanel);
			this.Name = "WorkUpControl";
			this.Size = new System.Drawing.Size(980, 578);
			this.WorkUpPanel.ResumeLayout(false);
			this.StaticPanel.ResumeLayout(false);
			this.CustomDynamicControlPanel.ResumeLayout(false);
			this.DynamicPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel WorkUpPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel DynamicPanel;
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_NextButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button NextButton {
			get { return withEventsField_NextButton; }
			set {
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click -= NextButtonClick;
				}
				withEventsField_NextButton = value;
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click += NextButtonClick;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_PreviousButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button PreviousButton {
			get { return withEventsField_PreviousButton; }
			set {
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click -= PreviousButtonClick;
				}
				withEventsField_PreviousButton = value;
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click += PreviousButtonClick;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel CustomDynamicControlPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel StaticPanel;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox IVInText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label1;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label2;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BPDiastolicText;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BPSystolicText;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox PulseText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label5;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox RespText;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label4;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox O2Text;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label3;
		internal Soaf.Presentation.Controls.WindowsForms.ComboBox BPTypeList;
		internal QuestionAnswerView IVInQAControl;

		internal QuestionChoiceView QCControl;
	}

	public class WorkUpViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<WorkUpViewPresenter>
	{
	}
}
