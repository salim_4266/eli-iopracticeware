using IO.Practiceware.Presentation.Views.Common.Controls;
using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class OpRoomView : OpRoomViewBase
	{
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.ORPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.StaticPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DrugsList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.Label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label5 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label6 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label8 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label7 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label10 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label9 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label11 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label12 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.EquipmentList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.OtherTeamList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.CirculatorList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.AnesthesiologistList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.AnesthesiaList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.ScrubTechList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.AssistantSurgeonList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.SurgeonList = new Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox();
			this.Panel1 = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.DynamicPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NavigationPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NextButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.PreviousButton = new Soaf.Presentation.Controls.WindowsForms.Button();
			this.Label13 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.QAControl = new QuestionAnswerView();
			this.ORPanel.SuspendLayout();
			this.StaticPanel.SuspendLayout();
			this.Panel1.SuspendLayout();
			this.DynamicPanel.SuspendLayout();
			this.NavigationPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//ORPanel
			//
			this.ORPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.ORPanel.Controls.Add(this.StaticPanel);
			this.ORPanel.Controls.Add(this.Panel1);
			this.ORPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ORPanel.Location = new System.Drawing.Point(0, 0);
			this.ORPanel.Name = "ORPanel";
			this.ORPanel.Size = new System.Drawing.Size(980, 514);
			this.ORPanel.TabIndex = 0;
			//
			//StaticPanel
			//
			this.StaticPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.StaticPanel.Controls.Add(this.DrugsList);
			this.StaticPanel.Controls.Add(this.Label1);
			this.StaticPanel.Controls.Add(this.Label5);
			this.StaticPanel.Controls.Add(this.Label6);
			this.StaticPanel.Controls.Add(this.Label8);
			this.StaticPanel.Controls.Add(this.Label7);
			this.StaticPanel.Controls.Add(this.Label10);
			this.StaticPanel.Controls.Add(this.Label9);
			this.StaticPanel.Controls.Add(this.Label11);
			this.StaticPanel.Controls.Add(this.Label12);
			this.StaticPanel.Controls.Add(this.EquipmentList);
			this.StaticPanel.Controls.Add(this.OtherTeamList);
			this.StaticPanel.Controls.Add(this.CirculatorList);
			this.StaticPanel.Controls.Add(this.AnesthesiologistList);
			this.StaticPanel.Controls.Add(this.AnesthesiaList);
			this.StaticPanel.Controls.Add(this.ScrubTechList);
			this.StaticPanel.Controls.Add(this.AssistantSurgeonList);
			this.StaticPanel.Controls.Add(this.SurgeonList);
			this.StaticPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StaticPanel.Location = new System.Drawing.Point(627, 0);
			this.StaticPanel.Name = "StaticPanel";
			this.StaticPanel.Size = new System.Drawing.Size(353, 514);
			this.StaticPanel.TabIndex = 53;
			//
			//DrugsList
			//
			this.DrugsList.DisplayMemberPath = "";
			this.DrugsList.Location = new System.Drawing.Point(177, 350);
			this.DrugsList.Name = "DrugsList";
			this.DrugsList.Size = new System.Drawing.Size(160, 35);
			this.DrugsList.TabIndex = 59;
			//
			//Label1
			//
			this.Label1.Location = new System.Drawing.Point(21, 358);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(75, 21);
			this.Label1.TabIndex = 60;
			this.Label1.Text = "Drugs";
			//
			//Label5
			//
			this.Label5.Location = new System.Drawing.Point(20, 34);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(59, 21);
			this.Label5.TabIndex = 51;
			this.Label5.Text = "Surgeon";
			//
			//Label6
			//
			this.Label6.Location = new System.Drawing.Point(20, 74);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(125, 21);
			this.Label6.TabIndex = 52;
			this.Label6.Text = "Assistant Surgeon";
			//
			//Label8
			//
			this.Label8.Location = new System.Drawing.Point(20, 114);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(76, 21);
			this.Label8.TabIndex = 55;
			this.Label8.Text = "Scrub Tech";
			//
			//Label7
			//
			this.Label7.Location = new System.Drawing.Point(20, 234);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(68, 21);
			this.Label7.TabIndex = 57;
			this.Label7.Text = "Circulator";
			//
			//Label10
			//
			this.Label10.Location = new System.Drawing.Point(20, 154);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(75, 21);
			this.Label10.TabIndex = 54;
			this.Label10.Text = "Anesthesia";
			//
			//Label9
			//
			this.Label9.Location = new System.Drawing.Point(20, 194);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(114, 21);
			this.Label9.TabIndex = 56;
			this.Label9.Text = "Anesthesiologist";
			//
			//Label11
			//
			this.Label11.Location = new System.Drawing.Point(20, 274);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(153, 21);
			this.Label11.TabIndex = 58;
			this.Label11.Text = "Other team members";
			//
			//Label12
			//
			this.Label12.Location = new System.Drawing.Point(21, 316);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(75, 21);
			this.Label12.TabIndex = 53;
			this.Label12.Text = "Equipment";
			//
			//EquipmentList
			//
			this.EquipmentList.DisplayMemberPath = "";
			this.EquipmentList.Location = new System.Drawing.Point(177, 308);
			this.EquipmentList.Name = "EquipmentList";
			this.EquipmentList.Size = new System.Drawing.Size(160, 35);
			this.EquipmentList.TabIndex = 50;
			//
			//OtherTeamList
			//
			this.OtherTeamList.DisplayMemberPath = "";
			this.OtherTeamList.Location = new System.Drawing.Point(177, 268);
			this.OtherTeamList.Name = "OtherTeamList";
			this.OtherTeamList.Size = new System.Drawing.Size(160, 35);
			this.OtherTeamList.TabIndex = 49;
			//
			//CirculatorList
			//
			this.CirculatorList.DisplayMemberPath = "";
			this.CirculatorList.Location = new System.Drawing.Point(177, 228);
			this.CirculatorList.Name = "CirculatorList";
			this.CirculatorList.Size = new System.Drawing.Size(160, 35);
			this.CirculatorList.TabIndex = 48;
			//
			//AnesthesiologistList
			//
			this.AnesthesiologistList.DisplayMemberPath = "";
			this.AnesthesiologistList.Location = new System.Drawing.Point(177, 188);
			this.AnesthesiologistList.Name = "AnesthesiologistList";
			this.AnesthesiologistList.Size = new System.Drawing.Size(160, 35);
			this.AnesthesiologistList.TabIndex = 47;
			//
			//AnesthesiaList
			//
			this.AnesthesiaList.DisplayMemberPath = "";
			this.AnesthesiaList.Location = new System.Drawing.Point(177, 148);
			this.AnesthesiaList.Name = "AnesthesiaList";
			this.AnesthesiaList.Size = new System.Drawing.Size(160, 35);
			this.AnesthesiaList.TabIndex = 46;
			//
			//ScrubTechList
			//
			this.ScrubTechList.DisplayMemberPath = "";
			this.ScrubTechList.Location = new System.Drawing.Point(177, 108);
			this.ScrubTechList.Name = "ScrubTechList";
			this.ScrubTechList.Size = new System.Drawing.Size(160, 35);
			this.ScrubTechList.TabIndex = 45;
			//
			//AssistantSurgeonList
			//
			this.AssistantSurgeonList.DisplayMemberPath = "";
			this.AssistantSurgeonList.Location = new System.Drawing.Point(177, 68);
			this.AssistantSurgeonList.Name = "AssistantSurgeonList";
			this.AssistantSurgeonList.Size = new System.Drawing.Size(160, 35);
			this.AssistantSurgeonList.TabIndex = 44;
			//
			//SurgeonList
			//
			this.SurgeonList.DisplayMemberPath = "";
			this.SurgeonList.Location = new System.Drawing.Point(177, 29);
			this.SurgeonList.Name = "SurgeonList";
			this.SurgeonList.Size = new System.Drawing.Size(160, 35);
			this.SurgeonList.TabIndex = 43;
			//
			//Panel1
			//
			this.Panel1.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.Panel1.Controls.Add(this.DynamicPanel);
			this.Panel1.Controls.Add(this.NavigationPanel);
			this.Panel1.Controls.Add(this.Label13);
			this.Panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.Panel1.Location = new System.Drawing.Point(0, 0);
			this.Panel1.Name = "Panel1";
			this.Panel1.Size = new System.Drawing.Size(627, 514);
			this.Panel1.TabIndex = 54;
			//
			//DynamicPanel
			//
			this.DynamicPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.DynamicPanel.Controls.Add(this.QAControl);
			this.DynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DynamicPanel.Location = new System.Drawing.Point(0, 21);
			this.DynamicPanel.Name = "DynamicPanel";
			this.DynamicPanel.Size = new System.Drawing.Size(627, 425);
			this.DynamicPanel.TabIndex = 52;
			//
			//NavigationPanel
			//
			this.NavigationPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.NavigationPanel.Controls.Add(this.NextButton);
			this.NavigationPanel.Controls.Add(this.PreviousButton);
			this.NavigationPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.NavigationPanel.Location = new System.Drawing.Point(0, 446);
			this.NavigationPanel.Name = "NavigationPanel";
			this.NavigationPanel.Size = new System.Drawing.Size(627, 68);
			this.NavigationPanel.TabIndex = 53;
			this.NavigationPanel.Visible = false;
			//
			//NextButton
			//
			this.NextButton.Enabled = false;
			this.NextButton.Location = new System.Drawing.Point(524, 4);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(100, 60);
			this.NextButton.TabIndex = 1;
			this.NextButton.Text = "Next";
			//
			//PreviousButton
			//
			this.PreviousButton.Enabled = false;
			this.PreviousButton.Location = new System.Drawing.Point(5, 4);
			this.PreviousButton.Name = "PreviousButton";
			this.PreviousButton.Size = new System.Drawing.Size(100, 60);
			this.PreviousButton.TabIndex = 0;
			this.PreviousButton.Text = "Previous";
			//
			//Label13
			//
			this.Label13.Dock = System.Windows.Forms.DockStyle.Top;
			this.Label13.Location = new System.Drawing.Point(0, 0);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(627, 21);
			this.Label13.StyleName = "Header";
			this.Label13.TabIndex = 12;
			this.Label13.Text = "Surgical prep";
			//
			//QAControl
			//
			this.QAControl.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QAControl.InteractionContext = null;
			this.QAControl.Location = new System.Drawing.Point(26, 6);
			this.QAControl.Name = "QAControl";
			this.QAControl.Note = "";
			this.QAControl.QuestionId = 0;
			this.QAControl.Size = new System.Drawing.Size(280, 48);
			this.QAControl.TabIndex = 0;
			this.QAControl.Value = "";
			//
			//OpRoomControl
			//
			this.Controls.Add(this.ORPanel);
			this.Name = "OpRoomControl";
			this.Size = new System.Drawing.Size(980, 514);
			this.ORPanel.ResumeLayout(false);
			this.StaticPanel.ResumeLayout(false);
			this.Panel1.ResumeLayout(false);
			this.DynamicPanel.ResumeLayout(false);
			this.NavigationPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel ORPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label13;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox SurgeonList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox OtherTeamList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox CirculatorList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox AnesthesiologistList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox AnesthesiaList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox ScrubTechList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox AssistantSurgeonList;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox EquipmentList;
		internal Soaf.Presentation.Controls.WindowsForms.Panel DynamicPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel StaticPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Panel Panel1;
		internal Soaf.Presentation.Controls.WindowsForms.Panel NavigationPanel;
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_NextButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button NextButton {
			get { return withEventsField_NextButton; }
			set {
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click -= NextButtonClick;
				}
				withEventsField_NextButton = value;
				if (withEventsField_NextButton != null) {
					withEventsField_NextButton.Click += NextButtonClick;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.Button withEventsField_PreviousButton;
		internal Soaf.Presentation.Controls.WindowsForms.Button PreviousButton {
			get { return withEventsField_PreviousButton; }
			set {
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click -= PreviousButtonClick;
				}
				withEventsField_PreviousButton = value;
				if (withEventsField_PreviousButton != null) {
					withEventsField_PreviousButton.Click += PreviousButtonClick;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.Label Label5;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label6;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label8;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label7;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label10;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label9;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label11;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label12;
		internal Soaf.Presentation.Controls.WindowsForms.ListBoxComboBox DrugsList;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label1;

		internal QuestionAnswerView QAControl;
	}

	public class OpRoomViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<OpRoomViewPresenter>
	{
	}
}
