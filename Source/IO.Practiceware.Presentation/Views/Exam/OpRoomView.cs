using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Services.Exam;
using Soaf.Linq;
using Soaf.Presentation;
using Soaf.Presentation.Controls.WindowsForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Exam
{
    public partial class OpRoomView
    {
        private int _gap;
        private int _left;

        /// <summary>
        ///   class OpRoomControl
        /// </summary>
        private int _width;

        public OpRoomView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            ORPanel.Visible = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadPresenter();
        }

        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            LoadOpRoomControls();
            ORPanel.Visible = true;
            if (Presenter.PatientSurgery != null)
            {
                LoadOpRoomData();
            }
        }

        /// <summary>
        ///   Loads the Operating room data.
        /// </summary>
        private void LoadOpRoomData()
        {
            if (DynamicPanel.Controls.Count > 0)
            {
                foreach (object questionAnswerControl in DynamicPanel.Controls)
                {
                    int questionId = ((QuestionAnswerView) questionAnswerControl).QuestionId;
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(questionId);
                    if (answer != null)
                    {
                        if (((QuestionAnswerView) questionAnswerControl).QuestionId == answer.QuestionId)
                        {
                            ((QuestionAnswerView) questionAnswerControl).Value = answer.Value;
                            ((QuestionAnswerView) questionAnswerControl).Note = answer.Note;
                        }
                    }
                }
            }
            if (SurgeonList.ItemsSource != null)
            {
                foreach (object i in SurgeonList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        SurgeonList.SelectedItems.Add(item);
                    }
                }
            }

            if (AssistantSurgeonList.ItemsSource != null)
            {
                foreach (object i in AssistantSurgeonList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        AssistantSurgeonList.SelectedItems.Add(item);
                    }
                }
            }
            if (ScrubTechList.ItemsSource != null)
            {
                foreach (object i in ScrubTechList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        ScrubTechList.SelectedItems.Add(item);
                    }
                }
            }
            if (AnesthesiaList.ItemsSource != null)
            {
                foreach (object i in AnesthesiaList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        AnesthesiaList.SelectedItems.Add(item);
                    }
                }
            }
            if (AnesthesiologistList.ItemsSource != null)
            {
                foreach (object i in AnesthesiologistList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        AnesthesiologistList.SelectedItems.Add(item);
                    }
                }
            }
            if (CirculatorList.ItemsSource != null)
            {
                foreach (object i in CirculatorList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        CirculatorList.SelectedItems.Add(item);
                    }
                }
            }
            if (OtherTeamList.ItemsSource != null)
            {
                foreach (object i in OtherTeamList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        OtherTeamList.SelectedItems.Add(item);
                    }
                }
            }
            if (EquipmentList.ItemsSource != null)
            {
                foreach (object i in EquipmentList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        EquipmentList.SelectedItems.Add(item);
                    }
                }
            }
            if (DrugsList.ItemsSource != null)
            {
                foreach (object i in DrugsList.ItemsSource)
                {
                    object item = i;
                    if (Presenter.GetChoice(((Choice) item).Id) != null)
                    {
                        DrugsList.SelectedItems.Add(item);
                    }
                }
            }
        }

        /// <summary>
        ///   Loads the Operating room controls.
        /// </summary>
        private void LoadOpRoomControls()
        {
            DynamicPanel.Visible = false;
            bool firstControlLoaded = false;
            _gap = 24;
            if (Presenter.ScreenQuestions != null)
            {
                foreach (SurgeryTemplateScreenQuestion sq in Presenter.ScreenQuestions.OrderBy(s => s.Question.OrdinalId))
                {
                    //If SQ.Question.Choices.Count > 1 Then
                    //Else
                    if (DynamicPanel.Controls.Count == 1 & firstControlLoaded == false)
                    {
                        QAControl.AddQuestion(sq.Question);
                        firstControlLoaded = true;
                    }
                    else
                    {
                        AddNextQuestion(sq);
                    }
                    //End If
                }
            }
            if (DynamicPanel.Controls.Count > 12)
            {
                _width = QAControl.Width;
                _left = QAControl.Left;
                NavigationPanel.Visible = true;
                NextButton.Enabled = true;
            }
            if (firstControlLoaded)
            {
                DynamicPanel.Visible = true;
            }
            if (Presenter.Surgeons != null)
            {
                SurgeonList.ItemsSource = Presenter.Surgeons;
                SurgeonList.DisplayMemberPath = "Name";
            }
            if (Presenter.AssistantSurgeons != null)
            {
                AssistantSurgeonList.ItemsSource = Presenter.AssistantSurgeons;
                AssistantSurgeonList.DisplayMemberPath = "Name";
            }
            if (Presenter.ScrubTechs != null)
            {
                ScrubTechList.ItemsSource = Presenter.ScrubTechs;
                ScrubTechList.DisplayMemberPath = "Name";
            }
            if (Presenter.Anesthesias != null)
            {
                AnesthesiaList.ItemsSource = Presenter.Anesthesias;
                AnesthesiaList.DisplayMemberPath = "Name";
            }
            if (Presenter.Anesthesiologists != null)
            {
                AnesthesiologistList.ItemsSource = Presenter.Anesthesiologists;
                AnesthesiologistList.DisplayMemberPath = "Name";
            }
            if (Presenter.Circulators != null)
            {
                CirculatorList.ItemsSource = Presenter.Circulators;
                CirculatorList.DisplayMemberPath = "Name";
            }
            if (Presenter.Others != null)
            {
                OtherTeamList.ItemsSource = Presenter.Others;
                OtherTeamList.DisplayMemberPath = "Name";
            }
            if (Presenter.Equipments != null)
            {
                EquipmentList.ItemsSource = Presenter.Equipments;
                EquipmentList.DisplayMemberPath = "Name";
            }
            if (Presenter.Drugs != null)
            {
                DrugsList.ItemsSource = Presenter.Drugs;
                DrugsList.DisplayMemberPath = "Name";
            }
        }

        /// <summary>
        ///   Adds the next question.
        /// </summary>
        /// <param name="screenQuestion"> The screen question. </param>
        private void AddNextQuestion(SurgeryTemplateScreenQuestion screenQuestion)
        {
            int n = DynamicPanel.Controls.Count;
            var nQaControl = new QuestionAnswerView();
            nQaControl.Visible = true;
            if (n%6 == 0)
            {
                nQaControl.Top = DynamicPanel.Controls[n - 1].Top;
                nQaControl.Left = DynamicPanel.Controls[0].Left + DynamicPanel.Controls[0].Width + _gap;
                nQaControl.Width = DynamicPanel.Controls[0].Width;
            }
            else
            {
                nQaControl.Top = DynamicPanel.Controls[0].Top + DynamicPanel.Controls[0].Height + 5;
                nQaControl.Left = DynamicPanel.Controls[0].Left;
                nQaControl.Width = DynamicPanel.Controls[0].Width;
            }
            nQaControl.AddQuestion((screenQuestion.Question));
            nQaControl.Name = QAControl.Name + (n);
            DynamicPanel.Controls.Add(nQaControl);
            nQaControl.BringToFront();
        }

        private void MoveNext()
        {
            foreach (object qa in DynamicPanel.Controls)
            {
                ((QuestionAnswerView) qa).Left -= (_width + _gap);
            }
            NextButton.Enabled = DynamicPanel.Controls[0].Left > (_width + _gap + _left);
        }

        private void MovePrevious()
        {
            foreach (object qa in DynamicPanel.Controls)
            {
                ((QuestionAnswerView) qa).Left += (_width + _gap);
            }
            PreviousButton.Enabled = QAControl.Left < _left;
        }

        private void NextButtonClick(object sender, EventArgs e)
        {
            MoveNext();
            PreviousButton.Enabled = true;
        }

        private void PreviousButtonClick(object sender, EventArgs e)
        {
            MovePrevious();
            NextButton.Enabled = true;
        }

        /// <summary>
        ///   Gets the operating room question answers selected by the user.
        /// </summary>
        private void GetOpRoomSelectedQuestionAnswers()
        {
            var arguments = new List<QuestionAnswerArguments>();
            foreach (object qa in DynamicPanel.Controls)
            {
                object qa1 = qa;
                //Insert/update data
                if (!string.IsNullOrEmpty(((QuestionAnswerView) qa1).Value.Trim()) & ((QuestionAnswerView) qa1).QuestionId > 0)
                {
                    var argument = new QuestionAnswerArguments();
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(((QuestionAnswerView) qa1).QuestionId);
                    if (answer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = answer.Id;
                    }
                    argument.QuestionId = ((QuestionAnswerView) qa1).QuestionId;
                    argument.Value = ((QuestionAnswerView) qa1).Value;
                    argument.Note = ((QuestionAnswerView) qa1).Note;
                    arguments.Add(argument);
                }
                //Delete record if the data is empty
                else if (string.IsNullOrEmpty(((QuestionAnswerView) qa1).Value.Trim()) & string.IsNullOrEmpty(((QuestionAnswerView) qa1).Note.Trim()) & ((QuestionAnswerView) qa1).QuestionId > 0)
                {
                    PatientSurgeryQuestionAnswer answer = Presenter.GetAnswer(((QuestionAnswerView) qa1).QuestionId);
                    if (answer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = answer.Id;
                        argument.QuestionId = ((QuestionAnswerView) qa1).QuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
            }
            Presenter.OpRoomAnswerArgs = arguments;
        }

        /// <summary>
        ///   Gets the operating room question choices selected by the user.
        /// </summary>
        private void GetOpRoomSelectedQuestionChoices()
        {
            var arguments = new List<QuestionChoiceArguments>();
            if (SurgeonList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, SurgeonList);
            else DeleteUnselectedChoices(arguments, SurgeonList, null);

            if (AssistantSurgeonList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, AssistantSurgeonList);
            else DeleteUnselectedChoices(arguments, AssistantSurgeonList, null);

            if (ScrubTechList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, ScrubTechList);
            else DeleteUnselectedChoices(arguments, ScrubTechList, null);

            if (AnesthesiaList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, AnesthesiaList);
            else DeleteUnselectedChoices(arguments, AnesthesiaList, null);

            if (AnesthesiologistList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, AnesthesiologistList);
            else DeleteUnselectedChoices(arguments, AnesthesiologistList, null);

            if (CirculatorList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, CirculatorList);
            else DeleteUnselectedChoices(arguments, CirculatorList, null);

            if (OtherTeamList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, OtherTeamList);
            else DeleteUnselectedChoices(arguments, OtherTeamList, null);

            if (EquipmentList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, EquipmentList);
            else DeleteUnselectedChoices(arguments, EquipmentList, null);

            if (DrugsList.SelectedItems.Count > 0)
                AddEditSelectedChoices(arguments, DrugsList);
            else DeleteUnselectedChoices(arguments, DrugsList, null);

            Presenter.OpRoomChoiceArgs = arguments;
        }

        private void AddEditSelectedChoices(List<QuestionChoiceArguments> arguments, ListBoxComboBox comboList)
        {
            foreach (object s in comboList.SelectedItems)
            {
                object selectedItem = s;
                var argument = new QuestionChoiceArguments();
                PatientSurgeryQuestionChoice choice = Presenter.GetChoice(((Choice) selectedItem).Id);
                if (choice != null)
                {
                    argument.PatientSurgeryQuestionChoiceId = choice.Id;
                }
                argument.QuestionId = ((IEnumerable<Choice>) comboList.ItemsSource).First().QuestionId;
                argument.ChoiceId = ((Choice) selectedItem).Id;
                argument.Note = "";
                arguments.Add(argument);
            }
            DeleteUnselectedChoices(arguments, comboList, comboList.SelectedItems);
        }

        private void DeleteUnselectedChoices(List<QuestionChoiceArguments> arguments, ListBoxComboBox comboList, IList selectedItems)
        {
            foreach (object i in comboList.ItemsSource)
            {
                object item = i;
                var argument = new QuestionChoiceArguments();
                PatientSurgeryQuestionChoice choice = Presenter.GetChoice(((Choice) item).Id);
                if ((choice != null && (selectedItems != null && !selectedItems.Contains(item)) || (choice != null && selectedItems == null)))
                {
                    argument.PatientSurgeryQuestionChoiceId = choice.Id;
                    argument.QuestionId = ((IEnumerable<Choice>) comboList.ItemsSource).First().QuestionId;
                    argument.ChoiceId = ((Choice) item).Id;
                    argument.Note = "";
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
        }

        /// <summary>
        ///   Gets the operating room user selections.
        /// </summary>
        public void GetOpRoomSelections()
        {
            GetOpRoomSelectedQuestionAnswers();
            GetOpRoomSelectedQuestionChoices();
        }
    }

    public class OpRoomViewPresenter : ILoadable
    {
        private const string ScreenName = "Operating-Room";
        private readonly IPracticeRepository _practiceRepository;
        public IEnumerable<QuestionAnswerArguments> OpRoomAnswerArgs;
        public IEnumerable<QuestionChoiceArguments> OpRoomChoiceArgs;
        public PatientSurgery PatientSurgery;
        public IEnumerable<PatientSurgeryQuestionAnswer> PatientSurgeryQuestionAnswers;
        public IEnumerable<PatientSurgeryQuestionChoice> PatientSurgeryQuestionChoices;

        /// <summary>
        ///   class OpRoomViewPresenter
        /// </summary>
        public int SurgeryTemlateId;

        private IEnumerable<Choice> _choices;

        public OpRoomViewPresenter(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<SurgeryTemplateScreenQuestion> ScreenQuestions { get; set; }
        public IEnumerable<Choice> Surgeons { get; set; }
        public IEnumerable<Choice> AssistantSurgeons { get; set; }
        public IEnumerable<Choice> ScrubTechs { get; set; }
        public IEnumerable<Choice> Anesthesias { get; set; }
        public IEnumerable<Choice> Anesthesiologists { get; set; }
        public IEnumerable<Choice> Circulators { get; set; }
        public IEnumerable<Choice> Others { get; set; }
        public IEnumerable<Choice> Equipments { get; set; }
        public IEnumerable<Choice> Drugs { get; set; }

        public IEnumerable<Choice> Choices
        {
            get { return _choices; }
            set
            {
                _choices = value;
                if (_choices != null)
                {
                    Choice[] choices = _choices.ToArray();
                    Surgeons = choices.Where(qc => qc.Question.Label.ToUpper() == "SURGEON").ToArray();
                    AssistantSurgeons = choices.Where(qc => qc.Question.Label.ToUpper() == "ASSISTANT SURGEON").ToArray();
                    ScrubTechs = choices.Where(qc => qc.Question.Label.ToUpper() == "SCRUB TECH").ToArray();
                    Anesthesias = choices.Where(qc => qc.Question.Label.ToUpper() == "ANESTHESIA").ToArray();
                    Anesthesiologists = choices.Where(qc => qc.Question.Label.ToUpper() == "ANESTHESIOLOGIST").ToArray();
                    Circulators = choices.Where(qc => qc.Question.Label.ToUpper() == "CIRCULATOR").ToArray();
                    Others = choices.Where(qc => qc.Question.Label.ToUpper() == "OTHER TEAM MEMBERS").ToArray();
                    Equipments = choices.Where(qc => qc.Question.Label.ToUpper() == "EQUIPMENT").ToArray();
                    Drugs = choices.Where(qc => qc.Question.Label.ToUpper() == "DRUGS").ToArray();
                }
            }
        }

        #region ILoadable Members

        public IEnumerable<Action> LoadActions
        {
            get
            {
                var actions = new Action[] {() => ScreenQuestions = _practiceRepository.SurgeryTemplateScreenQuestions.Include(s => s.Question, s => s.Screen, s => s.Question.Choices).Where(s => s.SurgeryTemplateId == SurgeryTemlateId & s.Screen.Name.ToUpper() == ScreenName.ToUpper()).ToArray()};

                return actions;
            }
        }

        #endregion

        /// <summary>
        ///   Gets the PatientSurgeryQuestionAnswer.
        /// </summary>
        /// <param name="questionId"> The question id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionAnswer GetAnswer(int questionId)
        {
            if (PatientSurgeryQuestionAnswers != null)
            {
                return PatientSurgeryQuestionAnswers.FirstOrDefault(ans => ans.QuestionId == questionId);
            }
            return null;
        }

        /// <summary>
        ///   Gets the PatientSurgeryQuestionChoice.
        /// </summary>
        /// <param name="choiceId"> The choice id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionChoice GetChoice(int choiceId)
        {
            if (PatientSurgeryQuestionChoices != null)
            {
                return PatientSurgeryQuestionChoices.FirstOrDefault(ch => ch.ChoiceId == choiceId);
            }
            return null;
        }
    }
}