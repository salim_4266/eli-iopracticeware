using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Services.Exam;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Exam
{
    public partial class HealthAssessmentView
    {
        private int _gap;
        private int _left;

        /// <summary>
        ///   class HealthAssessmentControl
        /// </summary>
        private int _width;

        /// <summary>
        ///   Initializes a new instance of the <see cref="HealthAssessmentView" /> class.
        /// </summary>
        public HealthAssessmentView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        /// <summary>
        ///   Raises the Load event.
        /// </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadPresenter();
        }

        /// <summary>
        ///   Called when [presenter loaded].
        /// </summary>
        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            LoadHealthAssessmentControls();
            if (Presenter.PatientSurgery != null)
            {
                LoadHealthAssessmentData();
            }
        }

        /// <summary>
        ///   Loads the health assessment data.
        /// </summary>
        private void LoadHealthAssessmentData()
        {
            if (Presenter.PatientSurgeryQuestionChoices.Any())
            {
                if (DynamicPanel.Controls.Count > 0)
                {
                    foreach (object questionChoiceControl in DynamicPanel.Controls)
                    {
                        int choiceid1 = ((QuestionChoiceView) questionChoiceControl).ChoiceId1;
                        int choiceid2 = ((QuestionChoiceView) questionChoiceControl).ChoiceId2;
                        PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                        if (choice != null)
                        {
                            if (((QuestionChoiceView) questionChoiceControl).ChoiceId1 == choice.ChoiceId)
                            {
                                ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                                ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                            }
                        }
                        else
                        {
                            choice = Presenter.GetChoice(choiceid2);
                            if (choice != null)
                            {
                                if (((QuestionChoiceView) questionChoiceControl).ChoiceId2 == choice.ChoiceId)
                                {
                                    ((QuestionChoiceView) questionChoiceControl).SelChoiceId = choice.ChoiceId;
                                    ((QuestionChoiceView) questionChoiceControl).Note = choice.Note;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///   Loads the health assessment controls.
        /// </summary>
        private void LoadHealthAssessmentControls()
        {
            bool firstControlLoaded = false;
            _gap = 45;
            if (Presenter.ScreenQuestions != null)
            {
                foreach (SurgeryTemplateScreenQuestion sq in Presenter.ScreenQuestions.OrderBy(s => s.Question.OrdinalId))
                {
                    if (sq.Question.Choices.Count() > 1)
                    {
                        if (DynamicPanel.Controls.Count == 1 & firstControlLoaded == false)
                        {
                            QCControl.AddQuestion(sq.Question);
                            firstControlLoaded = true;
                        }
                        else
                        {
                            AddNextQuestion(sq);
                        }
                    }
                }
            }
            if (DynamicPanel.Controls.Count > 14)
            {
                _width = QCControl.Width;
                _left = QCControl.Left;
                NavigationPanel.Visible = true;
                NextButton.Enabled = true;
            }
            AssessmentPanel.Visible = firstControlLoaded;
        }

        /// <summary>
        ///   Adds the next question.
        /// </summary>
        /// <param name="screenQuestion"> The screen question. </param>
        private void AddNextQuestion(SurgeryTemplateScreenQuestion screenQuestion)
        {
            int n = DynamicPanel.Controls.Count;
            var nQcControl = new QuestionChoiceView();
            nQcControl.Visible = true;
            if (n%7 == 0)
            {
                nQcControl.Top = (DynamicPanel.Controls[n - 1]).Top;
                nQcControl.Left = (DynamicPanel.Controls[0]).Left + (DynamicPanel.Controls[0]).Width + _gap;
                nQcControl.Width = (DynamicPanel.Controls[0]).Width;
            }
            else
            {
                nQcControl.Top = (DynamicPanel.Controls[0]).Top + (DynamicPanel.Controls[0]).Height + 5;
                nQcControl.Left = (DynamicPanel.Controls[0]).Left;
                nQcControl.Width = (DynamicPanel.Controls[0]).Width;
            }
            nQcControl.AddQuestion(screenQuestion.Question);
            nQcControl.Name = QCControl.Name + (n);
            DynamicPanel.Controls.Add(nQcControl);
            nQcControl.BringToFront();
        }

        /// <summary>
        ///   Moves to the next controls.
        /// </summary>
        private void MoveNext()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left -= (_width + _gap);
            }
            NextButton.Enabled = DynamicPanel.Controls[0].Left > (_width + _gap + _left);
        }

        /// <summary>
        ///   Moves the previous controls.
        /// </summary>
        private void MovePrevious()
        {
            foreach (object qc in DynamicPanel.Controls)
            {
                ((QuestionChoiceView) qc).Left += (_width + _gap);
            }
            PreviousButton.Enabled = QCControl.Left < _left;
        }

        private void NextButtonClick(object sender, EventArgs e)
        {
            MoveNext();
            PreviousButton.Enabled = true;
        }

        private void PreviousButtonClick(object sender, EventArgs e)
        {
            MovePrevious();
            NextButton.Enabled = true;
        }

        /// <summary>
        ///   Gets the health assessment question choices selected by the user.
        /// </summary>
        public void GetHealthAssessmentSelectedQuestionChoices()
        {
            var arguments = new List<QuestionChoiceArguments>();
            foreach (object qc1 in DynamicPanel.Controls)
            {
                object qc = qc1;
                if (((QuestionChoiceView) qc).SelChoiceId != 0)
                {
                    var argument = new QuestionChoiceArguments();
                    int choiceid1 = ((QuestionChoiceView) qc).ChoiceId1;
                    int choiceid2 = ((QuestionChoiceView) qc).ChoiceId2;
                    PatientSurgeryQuestionChoice choice = Presenter.GetChoice(choiceid1);
                    if (choice != null)
                    {
                        argument.PatientSurgeryQuestionChoiceId = choice.Id;
                    }
                    else
                    {
                        choice = Presenter.GetChoice(choiceid2);
                        if (choice != null)
                        {
                            argument.PatientSurgeryQuestionChoiceId = choice.Id;
                        }
                    }
                    argument.QuestionId = ((QuestionChoiceView) qc).QuestionId;
                    argument.ChoiceId = ((QuestionChoiceView) qc).SelChoiceId;
                    argument.Note = ((QuestionChoiceView) qc).Note;
                    arguments.Add(argument);
                }
            }
            Presenter.HealthAssessmentArgs = arguments;
        }
    }

    /// <summary>
    ///   class HealthAssessmentViewPresenter
    /// </summary>
    public class HealthAssessmentViewPresenter : ILoadable
    {
        private const string ScreenName = "Health-Assessment";
        private readonly IPracticeRepository _practiceRepository;

        public IEnumerable<QuestionChoiceArguments> HealthAssessmentArgs;
        public PatientSurgery PatientSurgery;

        public IEnumerable<SurgeryTemplateScreenQuestion> ScreenQuestions;
        public int SurgeryTemplateId;
        private IEnumerable<PatientSurgeryQuestionChoice> _patientSurgeryQuestionChoices;

        /// <summary>
        ///   Initializes a new instance of the <see cref="HealthAssessmentViewPresenter" /> class.
        /// </summary>
        /// <param name="practiceRepository"> The practice repository. </param>
        public HealthAssessmentViewPresenter(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<PatientSurgeryQuestionChoice> PatientSurgeryQuestionChoices
        {
            get { return _patientSurgeryQuestionChoices; }
            set
            {
                _patientSurgeryQuestionChoices = value;

                if (_patientSurgeryQuestionChoices != null)
                {
                }
            }
        }

        #region ILoadable Members

        /// <summary>
        ///   Gets the load actions.
        /// </summary>
        public IEnumerable<Action> LoadActions
        {
            get
            {
                var actions = new Action[]
                                  {
                                      () =>
                                      ScreenQuestions = _practiceRepository.SurgeryTemplateScreenQuestions.Include(s => s.Question, s => s.Screen, s => s.Question.Choices).Where(s => s.SurgeryTemplateId == SurgeryTemplateId & s.Screen.Name.ToUpper() == ScreenName.ToUpper()).ToArray()
                                  };
                return actions;
            }
        }

        #endregion

        /// <summary>
        ///   Gets the PatientSurgeryQuestionChoice.
        /// </summary>
        /// <param name="choiceId"> The choice id. </param>
        /// <returns> </returns>
        public PatientSurgeryQuestionChoice GetChoice(int choiceId)
        {
            if (PatientSurgeryQuestionChoices != null)
            {
                return PatientSurgeryQuestionChoices.FirstOrDefault(ch => ch.ChoiceId == choiceId);
            }
            return null;
        }
    }
}