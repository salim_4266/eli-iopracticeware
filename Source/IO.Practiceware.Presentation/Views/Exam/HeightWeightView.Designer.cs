using System;

namespace IO.Practiceware.Presentation.Views.Exam
{
	partial class HeightWeightView : HeightWeightViewBase
	{
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.HeightWeightPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.Label4 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.GramsButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.KgsButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.OuncesButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.LbsButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.WeightTextBox2 = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.WeightTextBox1 = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.BMITextBox = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label3 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.HeightTextBox2 = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.HeightTextBox1 = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.CmButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.MeterButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.InchButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.FeetButton = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.Label2 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.Label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.HeightWeightPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//HeightWeightPanel
			//
			this.HeightWeightPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.HeightWeightPanel.Controls.Add(this.Label4);
			this.HeightWeightPanel.Controls.Add(this.GramsButton);
			this.HeightWeightPanel.Controls.Add(this.KgsButton);
			this.HeightWeightPanel.Controls.Add(this.OuncesButton);
			this.HeightWeightPanel.Controls.Add(this.LbsButton);
			this.HeightWeightPanel.Controls.Add(this.WeightTextBox2);
			this.HeightWeightPanel.Controls.Add(this.WeightTextBox1);
			this.HeightWeightPanel.Controls.Add(this.BMITextBox);
			this.HeightWeightPanel.Controls.Add(this.Label3);
			this.HeightWeightPanel.Controls.Add(this.HeightTextBox2);
			this.HeightWeightPanel.Controls.Add(this.HeightTextBox1);
			this.HeightWeightPanel.Controls.Add(this.CmButton);
			this.HeightWeightPanel.Controls.Add(this.MeterButton);
			this.HeightWeightPanel.Controls.Add(this.InchButton);
			this.HeightWeightPanel.Controls.Add(this.FeetButton);
			this.HeightWeightPanel.Controls.Add(this.Label2);
			this.HeightWeightPanel.Controls.Add(this.Label1);
			this.HeightWeightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HeightWeightPanel.Location = new System.Drawing.Point(0, 0);
			this.HeightWeightPanel.Name = "HeightWeightPanel";
			this.HeightWeightPanel.Size = new System.Drawing.Size(980, 514);
			this.HeightWeightPanel.TabIndex = 0;
			//
			//Label4
			//
			this.Label4.Location = new System.Drawing.Point(391, 396);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(85, 21);
			this.Label4.StyleName = "Header";
			this.Label4.TabIndex = 16;
			this.Label4.Text = "Kg/m2";
			//
			//GramsButton
			//
			this.GramsButton.AllowUncheck = false;
			this.GramsButton.GroupName = "4'";
			this.GramsButton.IsChecked = false;
			this.GramsButton.Location = new System.Drawing.Point(710, 291);
			this.GramsButton.Name = "GramsButton";
			this.GramsButton.Size = new System.Drawing.Size(75, 56);
			this.GramsButton.TabIndex = 13;
			this.GramsButton.TabStop = false;
			this.GramsButton.Text = "Grams";
			//
			//KgsButton
			//
			this.KgsButton.AllowUncheck = false;
			this.KgsButton.GroupName = "3";
			this.KgsButton.IsChecked = false;
			this.KgsButton.Location = new System.Drawing.Point(600, 291);
			this.KgsButton.Name = "KgsButton";
			this.KgsButton.Size = new System.Drawing.Size(75, 56);
			this.KgsButton.TabIndex = 12;
			this.KgsButton.TabStop = false;
			this.KgsButton.Text = "Kgs";
			//
			//OuncesButton
			//
			this.OuncesButton.AllowUncheck = false;
			this.OuncesButton.GroupName = "4";
			this.OuncesButton.IsChecked = true;
			this.OuncesButton.Location = new System.Drawing.Point(710, 192);
			this.OuncesButton.Name = "OuncesButton";
			this.OuncesButton.Size = new System.Drawing.Size(75, 56);
			this.OuncesButton.TabIndex = 11;
			this.OuncesButton.Text = "Ounces";
			//
			//LbsButton
			//
			this.LbsButton.AllowUncheck = false;
			this.LbsButton.GroupName = "3";
			this.LbsButton.IsChecked = true;
			this.LbsButton.Location = new System.Drawing.Point(600, 192);
			this.LbsButton.Name = "LbsButton";
			this.LbsButton.Size = new System.Drawing.Size(75, 56);
			this.LbsButton.TabIndex = 10;
			this.LbsButton.Text = "Lbs";
			//
			//WeightTextBox2
			//
			this.WeightTextBox2.Location = new System.Drawing.Point(710, 98);
			this.WeightTextBox2.Multiline = false;
			this.WeightTextBox2.Name = "WeightTextBox2";
			this.WeightTextBox2.Size = new System.Drawing.Size(75, 56);
			this.WeightTextBox2.TabIndex = 9;
			//
			//WeightTextBox1
			//
			this.WeightTextBox1.Location = new System.Drawing.Point(600, 98);
			this.WeightTextBox1.Multiline = false;
			this.WeightTextBox1.Name = "WeightTextBox1";
			this.WeightTextBox1.Size = new System.Drawing.Size(75, 56);
			this.WeightTextBox1.TabIndex = 8;
			//
			//BMITextBox
			//
			this.BMITextBox.Enabled = false;
			this.BMITextBox.Location = new System.Drawing.Point(288, 383);
			this.BMITextBox.Multiline = false;
			this.BMITextBox.Name = "BMITextBox";
			this.BMITextBox.Size = new System.Drawing.Size(75, 56);
			this.BMITextBox.TabIndex = 15;
			//
			//Label3
			//
			this.Label3.Location = new System.Drawing.Point(193, 396);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(36, 25);
			this.Label3.StyleName = "Header";
			this.Label3.TabIndex = 14;
			this.Label3.Text = "BMI";
			//
			//HeightTextBox2
			//
			this.HeightTextBox2.Location = new System.Drawing.Point(288, 98);
			this.HeightTextBox2.Multiline = false;
			this.HeightTextBox2.Name = "HeightTextBox2";
			this.HeightTextBox2.Size = new System.Drawing.Size(73, 56);
			this.HeightTextBox2.TabIndex = 3;
			//
			//HeightTextBox1
			//
			this.HeightTextBox1.Location = new System.Drawing.Point(178, 98);
			this.HeightTextBox1.Multiline = false;
			this.HeightTextBox1.Name = "HeightTextBox1";
			this.HeightTextBox1.Size = new System.Drawing.Size(75, 56);
			this.HeightTextBox1.TabIndex = 2;
			//
			//CmButton
			//
			this.CmButton.AllowUncheck = false;
			this.CmButton.GroupName = "2";
			this.CmButton.IsChecked = false;
			this.CmButton.Location = new System.Drawing.Point(288, 291);
			this.CmButton.Name = "CmButton";
			this.CmButton.Size = new System.Drawing.Size(75, 56);
			this.CmButton.TabIndex = 7;
			this.CmButton.TabStop = false;
			this.CmButton.Text = "Cms";
			//
			//MeterButton
			//
			this.MeterButton.AllowUncheck = false;
			this.MeterButton.GroupName = "1";
			this.MeterButton.IsChecked = false;
			this.MeterButton.Location = new System.Drawing.Point(178, 291);
			this.MeterButton.Name = "MeterButton";
			this.MeterButton.Size = new System.Drawing.Size(75, 56);
			this.MeterButton.TabIndex = 6;
			this.MeterButton.TabStop = false;
			this.MeterButton.Text = "Meters";
			//
			//InchButton
			//
			this.InchButton.AllowUncheck = false;
			this.InchButton.GroupName = "2";
			this.InchButton.IsChecked = true;
			this.InchButton.Location = new System.Drawing.Point(288, 192);
			this.InchButton.Name = "InchButton";
			this.InchButton.Size = new System.Drawing.Size(75, 56);
			this.InchButton.TabIndex = 5;
			this.InchButton.Text = "Inches";
			//
			//FeetButton
			//
			this.FeetButton.AllowUncheck = false;
			this.FeetButton.GroupName = "1";
			this.FeetButton.IsChecked = true;
			this.FeetButton.Location = new System.Drawing.Point(178, 192);
			this.FeetButton.Name = "FeetButton";
			this.FeetButton.Size = new System.Drawing.Size(75, 56);
			this.FeetButton.TabIndex = 4;
			this.FeetButton.Text = "Feet";
			//
			//Label2
			//
			this.Label2.Location = new System.Drawing.Point(664, 38);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(64, 25);
			this.Label2.StyleName = "Header";
			this.Label2.TabIndex = 1;
			this.Label2.Text = "Weight";
			//
			//Label1
			//
			this.Label1.Location = new System.Drawing.Point(242, 38);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(60, 25);
			this.Label1.StyleName = "Header";
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Height";
			//
			//HeightWeightControl
			//
			this.Controls.Add(this.HeightWeightPanel);
			this.Name = "HeightWeightControl";
			this.Size = new System.Drawing.Size(980, 514);
			this.HeightWeightPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel HeightWeightPanel;
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_GramsButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton GramsButton {
			get { return withEventsField_GramsButton; }
			set {
				if (withEventsField_GramsButton != null) {
					withEventsField_GramsButton.Checked -= GramsButtonCheckedChanged;
					withEventsField_GramsButton.Unchecked -= GramsButtonCheckedChanged;
				}
				withEventsField_GramsButton = value;
				if (withEventsField_GramsButton != null) {
					withEventsField_GramsButton.Checked += GramsButtonCheckedChanged;
					withEventsField_GramsButton.Unchecked += GramsButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_KgsButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton KgsButton {
			get { return withEventsField_KgsButton; }
			set {
				if (withEventsField_KgsButton != null) {
					withEventsField_KgsButton.Checked -= KgsButtonCheckedChanged;
					withEventsField_KgsButton.Unchecked -= KgsButtonCheckedChanged;
				}
				withEventsField_KgsButton = value;
				if (withEventsField_KgsButton != null) {
					withEventsField_KgsButton.Checked += KgsButtonCheckedChanged;
					withEventsField_KgsButton.Unchecked += KgsButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_OuncesButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton OuncesButton {
			get { return withEventsField_OuncesButton; }
			set {
				if (withEventsField_OuncesButton != null) {
					withEventsField_OuncesButton.Checked -= OuncesButtonCheckedChanged;
					withEventsField_OuncesButton.Unchecked -= OuncesButtonCheckedChanged;
				}
				withEventsField_OuncesButton = value;
				if (withEventsField_OuncesButton != null) {
					withEventsField_OuncesButton.Checked += OuncesButtonCheckedChanged;
					withEventsField_OuncesButton.Unchecked += OuncesButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_LbsButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton LbsButton {
			get { return withEventsField_LbsButton; }
			set {
				if (withEventsField_LbsButton != null) {
					withEventsField_LbsButton.Checked -= LbsButtonCheckedChanged;
					withEventsField_LbsButton.Unchecked -= LbsButtonCheckedChanged;
				}
				withEventsField_LbsButton = value;
				if (withEventsField_LbsButton != null) {
					withEventsField_LbsButton.Checked += LbsButtonCheckedChanged;
					withEventsField_LbsButton.Unchecked += LbsButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.TextBox withEventsField_WeightTextBox2;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox WeightTextBox2 {
			get { return withEventsField_WeightTextBox2; }
			set {
				if (withEventsField_WeightTextBox2 != null) {
					withEventsField_WeightTextBox2.KeyPress -= TextBoxKeyPress;
					withEventsField_WeightTextBox2.KeyUp -= HeightTextBox1KeyUp;
					withEventsField_WeightTextBox2.Leave -= TextBoxLeave;
				}
				withEventsField_WeightTextBox2 = value;
				if (withEventsField_WeightTextBox2 != null) {
					withEventsField_WeightTextBox2.KeyPress += TextBoxKeyPress;
					withEventsField_WeightTextBox2.KeyUp += HeightTextBox1KeyUp;
					withEventsField_WeightTextBox2.Leave += TextBoxLeave;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.TextBox withEventsField_WeightTextBox1;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox WeightTextBox1 {
			get { return withEventsField_WeightTextBox1; }
			set {
				if (withEventsField_WeightTextBox1 != null) {
					withEventsField_WeightTextBox1.KeyPress -= TextBoxKeyPress;
					withEventsField_WeightTextBox1.KeyUp -= HeightTextBox1KeyUp;
					withEventsField_WeightTextBox1.Leave -= TextBoxLeave;
				}
				withEventsField_WeightTextBox1 = value;
				if (withEventsField_WeightTextBox1 != null) {
					withEventsField_WeightTextBox1.KeyPress += TextBoxKeyPress;
					withEventsField_WeightTextBox1.KeyUp += HeightTextBox1KeyUp;
					withEventsField_WeightTextBox1.Leave += TextBoxLeave;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BMITextBox;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label3;
		private Soaf.Presentation.Controls.WindowsForms.TextBox withEventsField_HeightTextBox2;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox HeightTextBox2 {
			get { return withEventsField_HeightTextBox2; }
			set {
				if (withEventsField_HeightTextBox2 != null) {
					withEventsField_HeightTextBox2.KeyPress -= TextBoxKeyPress;
					withEventsField_HeightTextBox2.KeyUp -= HeightTextBox1KeyUp;
					withEventsField_HeightTextBox2.Leave -= TextBoxLeave;
				}
				withEventsField_HeightTextBox2 = value;
				if (withEventsField_HeightTextBox2 != null) {
					withEventsField_HeightTextBox2.KeyPress += TextBoxKeyPress;
					withEventsField_HeightTextBox2.KeyUp += HeightTextBox1KeyUp;
					withEventsField_HeightTextBox2.Leave += TextBoxLeave;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.TextBox withEventsField_HeightTextBox1;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox HeightTextBox1 {
			get { return withEventsField_HeightTextBox1; }
			set {
				if (withEventsField_HeightTextBox1 != null) {
					withEventsField_HeightTextBox1.KeyPress -= TextBoxKeyPress;
					withEventsField_HeightTextBox1.KeyUp -= HeightTextBox1KeyUp;
					withEventsField_HeightTextBox1.Leave -= TextBoxLeave;
				}
				withEventsField_HeightTextBox1 = value;
				if (withEventsField_HeightTextBox1 != null) {
					withEventsField_HeightTextBox1.KeyPress += TextBoxKeyPress;
					withEventsField_HeightTextBox1.KeyUp += HeightTextBox1KeyUp;
					withEventsField_HeightTextBox1.Leave += TextBoxLeave;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_CmButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton CmButton {
			get { return withEventsField_CmButton; }
			set {
				if (withEventsField_CmButton != null) {
					withEventsField_CmButton.Checked -= CmButtonCheckedChanged;
					withEventsField_CmButton.Unchecked -= CmButtonCheckedChanged;
				}
				withEventsField_CmButton = value;
				if (withEventsField_CmButton != null) {
					withEventsField_CmButton.Checked += CmButtonCheckedChanged;
					withEventsField_CmButton.Unchecked += CmButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_MeterButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton MeterButton {
			get { return withEventsField_MeterButton; }
			set {
				if (withEventsField_MeterButton != null) {
					withEventsField_MeterButton.Checked -= MeterButtonCheckedChanged;
					withEventsField_MeterButton.Unchecked -= MeterButtonCheckedChanged;
				}
				withEventsField_MeterButton = value;
				if (withEventsField_MeterButton != null) {
					withEventsField_MeterButton.Checked += MeterButtonCheckedChanged;
					withEventsField_MeterButton.Unchecked += MeterButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_InchButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton InchButton {
			get { return withEventsField_InchButton; }
			set {
				if (withEventsField_InchButton != null) {
					withEventsField_InchButton.Checked -= InchButtonCheckedChanged;
					withEventsField_InchButton.Unchecked -= InchButtonCheckedChanged;
				}
				withEventsField_InchButton = value;
				if (withEventsField_InchButton != null) {
					withEventsField_InchButton.Checked += InchButtonCheckedChanged;
					withEventsField_InchButton.Unchecked += InchButtonCheckedChanged;
				}
			}
		}
		private Soaf.Presentation.Controls.WindowsForms.RadioButton withEventsField_FeetButton;
		internal Soaf.Presentation.Controls.WindowsForms.RadioButton FeetButton {
			get { return withEventsField_FeetButton; }
			set {
				if (withEventsField_FeetButton != null) {
					withEventsField_FeetButton.Checked -= FeetButtonCheckedChanged;
					withEventsField_FeetButton.Unchecked -= FeetButtonCheckedChanged;
				}
				withEventsField_FeetButton = value;
				if (withEventsField_FeetButton != null) {
					withEventsField_FeetButton.Checked += FeetButtonCheckedChanged;
					withEventsField_FeetButton.Unchecked += FeetButtonCheckedChanged;
				}
			}
		}
		internal Soaf.Presentation.Controls.WindowsForms.Label Label2;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label1;

		internal Soaf.Presentation.Controls.WindowsForms.Label Label4;
	}

	public class HeightWeightViewBase : Soaf.Presentation.Controls.WindowsForms.PresentationControl<HeightWeightViewPresenter>
	{
	}
}
