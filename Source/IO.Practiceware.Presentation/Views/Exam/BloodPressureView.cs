using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.Views.Exam
{
    public partial class BloodPressureView
    {
        private string _bpAnswer;
        private int _questionId;

        public BloodPressureView()
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            var bpTypes = new BpTypes();
            BPTypeList.ItemsSource = bpTypes.List;
        }

        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; }
        }

        public string Value
        {
            get
            {
                _bpAnswer = "";
                if (!string.IsNullOrEmpty(BPSystolicValue.Text.Trim()) & !string.IsNullOrEmpty(BPDiastolicValue.Text.Trim()))
                {
                    _bpAnswer = BPSystolicValue.Text.Trim() + "|" + BPDiastolicValue.Text.Trim();
                }
                if (!string.IsNullOrEmpty(BPSystolicValue.Text.Trim()) & string.IsNullOrEmpty(BPDiastolicValue.Text.Trim()))
                {
                    _bpAnswer = BPSystolicValue.Text.Trim();
                }
                if (string.IsNullOrEmpty(BPSystolicValue.Text.Trim()) & !string.IsNullOrEmpty(BPDiastolicValue.Text.Trim()))
                {
                    _bpAnswer = "|" + BPDiastolicValue.Text.Trim();
                }
                return _bpAnswer;
            }
            set
            {
                _bpAnswer = value;
                if (_bpAnswer != null)
                {
                    if (_bpAnswer.IndexOf("|") >= 0)
                    {
                        BPSystolicValue.Text = _bpAnswer.Substring(0, _bpAnswer.IndexOf("|"));
                        if ((_bpAnswer.IndexOf("|") + 1) < _bpAnswer.Length)
                        {
                            BPDiastolicValue.Text = _bpAnswer.Substring(_bpAnswer.IndexOf("|") + 1, _bpAnswer.Length - (_bpAnswer.IndexOf("|") + 1));
                        }
                    }
                    else
                    {
                        BPSystolicValue.Text = _bpAnswer;
                    }
                }
            }
        }

        public string Note
        {
            get
            {
                if (BPTypeList.ItemsSource != null)
                {
                    if (BPTypeList.SelectedItem != null)
                    {
                        return BPTypeList.SelectedItem.ToString();
                    }
                    return "";
                }
                return "";
            }
            set
            {
                if (value != null)
                {
                    if (BPTypeList.ItemsSource != null)
                    {
                        foreach (object item in BPTypeList.ItemsSource)
                        {
                            if (item.ToString() == value)
                            {
                                BPTypeList.SelectedItem = item;
                                BPTypeList.Refresh();
                            }
                        }
                    }
                }
            }
        }

        public void AddQuestion(Question question)
        {
            _questionId = question.Id;
        }
    }

    public class BpTypes
    {
        public string[] List = new string[3];

        public BpTypes()
        {
            List[0] = "Sitting";
            List[1] = "Standing";
            List[2] = "Supine";
        }
    }
}