using System;

namespace IO.Practiceware.Presentation.Views.Exam
{

    partial class BloodPressureView : Soaf.Presentation.Controls.WindowsForms.UserControl
	{
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.QuestionPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.BPTypeList = new Soaf.Presentation.Controls.WindowsForms.ComboBox();
			this.BPDiastolicValue = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Label1 = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.BPSystolicValue = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.QuestionLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.QuestionPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//QuestionPanel
			//
			this.QuestionPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QuestionPanel.Controls.Add(this.BPTypeList);
			this.QuestionPanel.Controls.Add(this.BPDiastolicValue);
			this.QuestionPanel.Controls.Add(this.Label1);
			this.QuestionPanel.Controls.Add(this.BPSystolicValue);
			this.QuestionPanel.Controls.Add(this.QuestionLabel);
			this.QuestionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.QuestionPanel.Location = new System.Drawing.Point(0, 0);
			this.QuestionPanel.Name = "QuestionPanel";
			this.QuestionPanel.Size = new System.Drawing.Size(377, 48);
			this.QuestionPanel.TabIndex = 0;
			//
			//BPTypeList
			//
			this.BPTypeList.DisplayMemberPath = "";
			this.BPTypeList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BPTypeList.Location = new System.Drawing.Point(256, 0);
			this.BPTypeList.Name = "BPTypeList";
			this.BPTypeList.SelectedIndex = -1;
			this.BPTypeList.SelectedValuePath = "";
			this.BPTypeList.Size = new System.Drawing.Size(121, 48);
			this.BPTypeList.TabIndex = 4;
			//
			//BPDiastolicValue
			//
			this.BPDiastolicValue.Dock = System.Windows.Forms.DockStyle.Left;
			this.BPDiastolicValue.Location = new System.Drawing.Point(196, 0);
			this.BPDiastolicValue.Name = "BPDiastolicValue";
			this.BPDiastolicValue.Size = new System.Drawing.Size(60, 48);
			this.BPDiastolicValue.TabIndex = 3;
			//
			//Label1
			//
			this.Label1.Dock = System.Windows.Forms.DockStyle.Left;
			this.Label1.Location = new System.Drawing.Point(180, 0);
			this.Label1.Margin = new System.Windows.Forms.Padding(5, 2, 5, 2);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(16, 48);
			this.Label1.StyleName = "";
			this.Label1.TabIndex = 1;
			this.Label1.Text = "/";
			this.Label1.VerticalAlignment = System.Windows.VerticalAlignment.Center;
			//
			//BPSystolicValue
			//
			this.BPSystolicValue.Dock = System.Windows.Forms.DockStyle.Left;
			this.BPSystolicValue.Location = new System.Drawing.Point(120, 0);
			this.BPSystolicValue.Name = "BPSystolicValue";
			this.BPSystolicValue.Size = new System.Drawing.Size(60, 48);
			this.BPSystolicValue.TabIndex = 2;
			//
			//QuestionLabel
			//
			this.QuestionLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.QuestionLabel.Location = new System.Drawing.Point(0, 0);
			this.QuestionLabel.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
			this.QuestionLabel.Name = "QuestionLabel";
			this.QuestionLabel.Size = new System.Drawing.Size(120, 48);
			this.QuestionLabel.TabIndex = 0;
			this.QuestionLabel.Text = "Blood Pressure";
			this.QuestionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
			//
			//BloodPressureUserControl
			//
			this.Controls.Add(this.QuestionPanel);
			this.Name = "BloodPressureUserControl";
			this.Size = new System.Drawing.Size(377, 48);
			this.QuestionPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel QuestionPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Label QuestionLabel;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BPDiastolicValue;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox BPSystolicValue;
		internal Soaf.Presentation.Controls.WindowsForms.Label Label1;

		internal Soaf.Presentation.Controls.WindowsForms.ComboBox BPTypeList;
	}
}
