
using IO.Practiceware.Model;
using IO.Practiceware.Services.Exam;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Exam
{
    /// <summary>
    /// class HeightWeightControl
    /// </summary>
    public partial class HeightWeightView
    {
        private bool _isHeightFtInch = true;

        private bool _isWeightLbsOz = true;

        private bool _heightWeightLoaded;

        public HeightWeightView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadPresenter();
        }

        protected override void OnPresenterLoaded()
        {
            base.OnPresenterLoaded();
            if (Presenter.PatientSurgery != null)
            {
                LoadHeightWeightData();
            }
            _heightWeightLoaded = true;
        }

        /// <summary>
        /// Loads the height weight data.
        /// </summary>
        private void LoadHeightWeightData()
        {
            if (Presenter.HeightFeetAnswer != null)
            {
                HeightTextBox1.Text = Presenter.HeightFeetAnswer.Value;
                FeetButton.IsChecked = true;
            }
            if (Presenter.HeightInchAnswer != null)
            {
                HeightTextBox2.Text = Presenter.HeightInchAnswer.Value;
                InchButton.IsChecked = true;
            }
            if (Presenter.HeightMeterAnswer != null)
            {
                HeightTextBox1.Text = Presenter.HeightMeterAnswer.Value;
                MeterButton.IsChecked = true;
            }
            if (Presenter.HeightCmAnswer != null)
            {
                HeightTextBox2.Text = Presenter.HeightCmAnswer.Value;
                CmButton.IsChecked = true;
            }
            if (Presenter.WeightLbsAnswer != null)
            {
                _isWeightLbsOz = true;
                WeightTextBox1.Text = Presenter.WeightLbsAnswer.Value;
                LbsButton.IsChecked = true;
            }
            if (Presenter.WeightOzAnswer != null)
            {
                _isWeightLbsOz = true;
                WeightTextBox2.Text = Presenter.WeightOzAnswer.Value;
                OuncesButton.IsChecked = true;
            }
            if (Presenter.WeightKgsAnswer != null)
            {
                _isWeightLbsOz = false;
                WeightTextBox1.Text = Presenter.WeightKgsAnswer.Value;
                KgsButton.IsChecked = true;
            }
            if (Presenter.WeightGmsAnswer != null)
            {
                _isWeightLbsOz = false;
                WeightTextBox2.Text = Presenter.WeightGmsAnswer.Value;
                GramsButton.IsChecked = true;
                _isWeightLbsOz = false;
            }
            CalculateBmiValue();
        }

        /// <summary>
        /// Gets the height weight question answers selected by the user.
        /// </summary>
        public void GetHeightWeightSelectedQuestionAnswers()
        {
            var arguments = new List<QuestionAnswerArguments>();
            if (FeetButton.IsChecked.GetValueOrDefault())
            {
                //insert or update data
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.HeightFeetAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightFeetAnswer.Id;
                    }
                    argument.QuestionId = Presenter.HeightFeetQuestionId;
                    argument.Value = HeightTextBox1.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.HeightFeetAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightFeetAnswer.Id;
                        argument.QuestionId = Presenter.HeightFeetQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
                //insert or update data
                if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.HeightInchAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightInchAnswer.Id;
                    }
                    argument.QuestionId = Presenter.HeightInchQuestionId;
                    argument.Value = HeightTextBox2.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.HeightInchAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightInchAnswer.Id;
                        argument.QuestionId = Presenter.HeightInchQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }

                //delete Meter/Cm data after inserting Feet/Inch data
                if (Presenter.HeightMeterAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.HeightMeterAnswer.Id;
                    argument.QuestionId = Presenter.HeightMeterQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }

                if (Presenter.HeightCmAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.HeightCmAnswer.Id;
                    argument.QuestionId = Presenter.HeightCmQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            if (MeterButton.IsChecked.GetValueOrDefault())
            {
                //insert or update data
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.HeightMeterAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightMeterAnswer.Id;
                    }
                    argument.QuestionId = Presenter.HeightMeterQuestionId;
                    argument.Value = HeightTextBox1.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.HeightMeterAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightMeterAnswer.Id;
                        argument.QuestionId = Presenter.HeightMeterQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
                //insert or update data
                if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.HeightCmAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightCmAnswer.Id;
                    }
                    argument.QuestionId = Presenter.HeightCmQuestionId;
                    argument.Value = HeightTextBox2.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.HeightCmAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.HeightCmAnswer.Id;
                        argument.QuestionId = Presenter.HeightCmQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
                //delete Feet/Inch data after inserting Meter/Cm data
                if (Presenter.HeightFeetAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.HeightFeetAnswer.Id;
                    argument.QuestionId = Presenter.HeightFeetQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
                if (Presenter.HeightInchAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.HeightInchAnswer.Id;
                    argument.QuestionId = Presenter.HeightInchQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            if (LbsButton.IsChecked.GetValueOrDefault())
            {
                //insert or update data
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.WeightLbsAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightLbsAnswer.Id;
                    }
                    argument.QuestionId = Presenter.WeightLbsQuestionId;
                    argument.Value = WeightTextBox1.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.WeightLbsAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightLbsAnswer.Id;
                        argument.QuestionId = Presenter.WeightLbsQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
                //insert or update data
                if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.WeightOzAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightOzAnswer.Id;
                    }
                    argument.QuestionId = Presenter.WeightOzQuestionId;
                    argument.Value = WeightTextBox2.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.WeightOzAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightOzAnswer.Id;
                        argument.QuestionId = Presenter.WeightOzQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }

                //delete kgs/grams data after inserting Lbs/Oz data
                if (Presenter.WeightKgsAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.WeightKgsAnswer.Id;
                    argument.QuestionId = Presenter.WeightKgsQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
                if (Presenter.WeightGmsAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.WeightGmsAnswer.Id;
                    argument.QuestionId = Presenter.WeightGmsQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            if (KgsButton.IsChecked.GetValueOrDefault())
            {
                //Insert or update data
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.WeightKgsAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightKgsAnswer.Id;
                    }
                    argument.QuestionId = Presenter.WeightKgsQuestionId;
                    argument.Value = WeightTextBox1.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.WeightKgsAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightKgsAnswer.Id;
                        argument.QuestionId = Presenter.WeightKgsQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }
                //Insert or update data
                if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    var argument = new QuestionAnswerArguments();
                    if (Presenter.WeightGmsAnswer != null)
                    {
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightGmsAnswer.Id;
                    }
                    argument.QuestionId = Presenter.WeightGmsQuestionId;
                    argument.Value = WeightTextBox2.Text;
                    argument.Note = "";
                    arguments.Add(argument);
                }
                else //Delete empty data record
                {
                    if (Presenter.WeightGmsAnswer != null)
                    {
                        var argument = new QuestionAnswerArguments();
                        argument.PatientSurgeryQuestionAnswerId = Presenter.WeightGmsAnswer.Id;
                        argument.QuestionId = Presenter.WeightGmsQuestionId;
                        argument.IsDeleted = true;
                        arguments.Add(argument);
                    }
                }

                //delete Lbs/Oz data after inserting kgs/grams data
                if (Presenter.WeightLbsAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.WeightLbsAnswer.Id;
                    argument.QuestionId = Presenter.WeightLbsQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
                if (Presenter.WeightOzAnswer != null)
                {
                    var argument = new QuestionAnswerArguments();
                    argument.PatientSurgeryQuestionAnswerId = Presenter.WeightOzAnswer.Id;
                    argument.QuestionId = Presenter.WeightOzQuestionId;
                    argument.IsDeleted = true;
                    arguments.Add(argument);
                }
            }
            Presenter.HeightWeightArgs = arguments;
        }

        /// <summary>
        /// Converts the feet to meter and inch to cm.
        /// </summary>
        private void ConvertFeetToMeterAndInchToCm()
        {
            double feet = 0;
            double inch = 0;
            double meter;
            double cm;
            if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()))
            {
                feet = Convert.ToInt32(HeightTextBox1.Text.Trim());
            }
            if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
            {
                inch = Convert.ToDouble(HeightTextBox2.Text.Trim());
            }
            Arithmetic.ConvertFeetInchToMeterCm(feet, inch, out meter, out cm);
            if (_heightWeightLoaded)
            {
                HeightTextBox1.Text = meter.ToString();
                HeightTextBox2.Text = cm.ToString();
            }
        }

        /// <summary>
        /// Converts the meter to feet and cm to inch.
        /// </summary>
        private void ConvertMeterToFeetAndCmToInch()
        {
            double meter = 0;
            double cm = 0;
            double feet;
            double inch;
            if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()))
            {
                meter = Convert.ToInt32(HeightTextBox1.Text.Trim());
            }
            if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
            {
                cm = Convert.ToDouble(HeightTextBox2.Text.Trim());
            }
            Arithmetic.ConvertMeterCmToFeetInch(meter, cm, out feet, out inch);
            if (_heightWeightLoaded)
            {
                HeightTextBox1.Text = feet.ToString();
                HeightTextBox2.Text = inch.ToString();
            }
        }

        /// <summary>
        /// Converts the LBS to KGS and ozs to GMS.
        /// </summary>
        private void ConvertLbsToKgsAndOzsToGms()
        {
            double lbs = 0;
            double ounces = 0;
            double kgs;
            double grams;
            if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
            {
                lbs = Convert.ToInt32(WeightTextBox1.Text.Trim());
            }
            if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
            {
                ounces = Convert.ToInt32(WeightTextBox2.Text.Trim());
            }
            Arithmetic.ConvertLbsOuncesToKgsGrams(lbs, ounces, out kgs, out grams);
            if (_heightWeightLoaded)
            {
                WeightTextBox1.Text = kgs.ToString();
                WeightTextBox2.Text = grams.ToString();
            }
        }

        /// <summary>
        /// Converts the KGS to LBS and GMS to oz.
        /// </summary>
        private void ConvertKgsToLbsAndGmsToOz()
        {
            double kgs = 0;
            double grams = 0;
            double lbs;
            double ounces;
            if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
            {
                kgs = Convert.ToInt32(WeightTextBox1.Text.Trim());
            }
            if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
            {
                grams = Convert.ToInt32(WeightTextBox2.Text.Trim());
            }
            Arithmetic.ConvertKgsGramsToLbsOunces(kgs, grams, out lbs, out ounces);
            if (_heightWeightLoaded)
            {
                WeightTextBox1.Text = lbs.ToString();
                WeightTextBox2.Text = ounces.ToString();
            }
        }

        private void FeetButtonCheckedChanged(object sender, EventArgs e)
        {
            InchButton.IsChecked = FeetButton.IsChecked;
            if (_isHeightFtInch == false & FeetButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    ConvertMeterToFeetAndCmToInch();
                    _isHeightFtInch = true;
                }
            }
        }

        private void InchButtonCheckedChanged(object sender, EventArgs e)
        {
            FeetButton.IsChecked = InchButton.IsChecked;
            if (_isHeightFtInch == false & InchButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    ConvertMeterToFeetAndCmToInch();
                    _isHeightFtInch = true;
                }
            }
        }

        private void MeterButtonCheckedChanged(object sender, EventArgs e)
        {
            CmButton.IsChecked = MeterButton.IsChecked;
            if (_isHeightFtInch & MeterButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    ConvertFeetToMeterAndInchToCm();
                    _isHeightFtInch = false;
                }
            }
        }

        private void CmButtonCheckedChanged(object sender, EventArgs e)
        {
            MeterButton.IsChecked = CmButton.IsChecked;
            if (_isHeightFtInch & CmButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(HeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    ConvertFeetToMeterAndInchToCm();
                    _isHeightFtInch = false;
                }
            }
        }

        private void LbsButtonCheckedChanged(object sender, EventArgs e)
        {
            OuncesButton.IsChecked = LbsButton.IsChecked;
            if (_isWeightLbsOz == false & LbsButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    ConvertKgsToLbsAndGmsToOz();
                    _isWeightLbsOz = true;
                }
            }
        }

        private void OuncesButtonCheckedChanged(object sender, EventArgs e)
        {
            LbsButton.IsChecked = OuncesButton.IsChecked;
            if (_isWeightLbsOz == false & OuncesButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    ConvertKgsToLbsAndGmsToOz();
                    _isWeightLbsOz = true;
                }
            }
        }

        private void KgsButtonCheckedChanged(object sender, EventArgs e)
        {
            GramsButton.IsChecked = KgsButton.IsChecked;
            if (_isWeightLbsOz & KgsButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    ConvertLbsToKgsAndOzsToGms();
                    _isWeightLbsOz = false;
                }
            }
        }

        private void GramsButtonCheckedChanged(object sender, EventArgs e)
        {
            KgsButton.IsChecked = GramsButton.IsChecked;
            if (_isWeightLbsOz & GramsButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()) | !string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                {
                    ConvertLbsToKgsAndOzsToGms();
                    _isWeightLbsOz = false;
                }
            }
        }

        /// <summary>
        /// Calculates the BMI value.
        /// </summary>
        private void CalculateBmiValue()
        {
            double kgs = 0;
            double meters = 0;
            if (!string.IsNullOrEmpty(HeightTextBox1.Text) & FeetButton.IsChecked.GetValueOrDefault())
            {
                meters = Convert.ToDouble(HeightTextBox1.Text).ConvertFeetToMeter();
                if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                    meters = Convert.ToDouble(meters + (Convert.ToDouble(HeightTextBox2.Text).ConvertInchToCm()).ConvertCmToMeter());
            }
            if (!string.IsNullOrEmpty(HeightTextBox1.Text) & MeterButton.IsChecked.GetValueOrDefault())
            {
                meters = Convert.ToDouble(HeightTextBox1.Text);
                if (!string.IsNullOrEmpty(HeightTextBox2.Text.Trim()))
                {
                    meters = meters + Convert.ToDouble(HeightTextBox2.Text).ConvertCmToMeter();
                }
            }

            if (!string.IsNullOrEmpty(WeightTextBox1.Text) & LbsButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
                    kgs = Convert.ToDouble(Convert.ToDouble(WeightTextBox1.Text).ConvertPoundToKg());
                if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                    kgs = Convert.ToDouble(kgs + ((Convert.ToDouble(WeightTextBox2.Text).ConvertOuncesToPound()).ConvertPoundToKg()));
            }
            if (!string.IsNullOrEmpty(WeightTextBox1.Text) & KgsButton.IsChecked.GetValueOrDefault())
            {
                if (!string.IsNullOrEmpty(WeightTextBox1.Text.Trim()))
                    kgs = Convert.ToDouble(WeightTextBox1.Text);
                if (!string.IsNullOrEmpty(WeightTextBox2.Text.Trim()))
                    kgs = Convert.ToDouble(kgs + Convert.ToDouble(WeightTextBox2.Text).ConvertGramsToKgs());
            }
            if (meters > 0)
            {
                string bmiValue = Arithmetic.GetBodyMassIndex(kgs, meters).ToString();
                int k = bmiValue.IndexOf('.');
                if (k > 0)
                {
                    bmiValue = bmiValue.Substring(0, k + 3);
                }
                BMITextBox.Text = bmiValue;
            }
            else
            {
                BMITextBox.Text = "";
            }
        }

        private void TextBoxKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {

            if (char.IsNumber(e.KeyChar) | e.KeyChar == (char)(8) | e.KeyChar == '.')
            {
            }
            else
            {
                e.Handled = true;
            }
        }
        private void HeightTextBox1KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            CalculateBmiValue();
        }
        private void TextBoxLeave(object sender, EventArgs e)
        {
            CalculateBmiValue();
        }
    }

    public class HeightWeightViewPresenter : ILoadable
    {

        private IEnumerable<Question> _questions;
        private IEnumerable<PatientSurgeryQuestionAnswer> _patientSurgeryQuestionAnswers;

        public IEnumerable<PatientSurgeryQuestionAnswer> PatientSurgeryQuestionAnswers
        {
            get { return _patientSurgeryQuestionAnswers; }
            set
            {
                _patientSurgeryQuestionAnswers = value;
                if (_patientSurgeryQuestionAnswers != null)
                {
                    LoadAnswers();
                }
            }
        }
        public IEnumerable<QuestionAnswerArguments> HeightWeightArgs { get; set; }
        public int HeightFeetQuestionId { get; set; }
        public int HeightInchQuestionId { get; set; }
        public int HeightMeterQuestionId { get; set; }
        public int HeightCmQuestionId { get; set; }
        public int WeightLbsQuestionId { get; set; }
        public int WeightOzQuestionId { get; set; }
        public int WeightKgsQuestionId { get; set; }
        public int WeightGmsQuestionId { get; set; }

        public PatientSurgeryQuestionAnswer HeightFeetAnswer { get; set; }
        public PatientSurgeryQuestionAnswer HeightInchAnswer { get; set; }
        public PatientSurgeryQuestionAnswer HeightMeterAnswer { get; set; }
        public PatientSurgeryQuestionAnswer HeightCmAnswer { get; set; }
        public PatientSurgeryQuestionAnswer WeightLbsAnswer { get; set; }
        public PatientSurgeryQuestionAnswer WeightOzAnswer { get; set; }
        public PatientSurgeryQuestionAnswer WeightKgsAnswer { get; set; }
        public PatientSurgeryQuestionAnswer WeightGmsAnswer { get; set; }
        public PatientSurgery PatientSurgery { get; set; }

        public IEnumerable<Question> Questions
        {
            get { return _questions; }
            set
            {
                _questions = value;
                if (_questions != null)
                {
                    var questions = _questions.ToArray();
                    HeightFeetQuestionId = questions.Single(q => q.Label.ToUpper() == "HEIGHT-FEET").Id;
                    HeightInchQuestionId = questions.Single(q => q.Label.ToUpper() == "HEIGHT-INCHES").Id;
                    HeightMeterQuestionId = questions.Single(q => q.Label.ToUpper() == "HEIGHT-M").Id;
                    HeightCmQuestionId = questions.Single(q => q.Label.ToUpper() == "HEIGHT-CM").Id;
                    WeightLbsQuestionId = questions.Single(q => q.Label.ToUpper() == "WEIGHT-LBS").Id;
                    WeightOzQuestionId = questions.Single(q => q.Label.ToUpper() == "WEIGHT-OUNCES").Id;
                    WeightKgsQuestionId = questions.Single(q => q.Label.ToUpper() == "WEIGHT-KGS").Id;
                    WeightGmsQuestionId = questions.Single(q => q.Label.ToUpper() == "WEIGHT-GRAMS").Id;
                    LoadAnswers();
                }
            }
        }
        public IEnumerable<Action> LoadActions
        {
            get
            {
                Action[] actions = {
					
				};
                return actions;
            }
        }
        public void LoadAnswers()
        {
            if (HeightFeetQuestionId > 0)
            {
                HeightFeetAnswer = GetAnswer(HeightFeetQuestionId);
            }
            if (HeightInchQuestionId > 0)
            {
                HeightInchAnswer = GetAnswer(HeightInchQuestionId);
            }
            if (HeightMeterQuestionId > 0)
            {
                HeightMeterAnswer = GetAnswer(HeightMeterQuestionId);
            }
            if (HeightCmQuestionId > 0)
            {
                HeightCmAnswer = GetAnswer(HeightCmQuestionId);
            }
            if (WeightLbsQuestionId > 0)
            {
                WeightLbsAnswer = GetAnswer(WeightLbsQuestionId);
            }
            if (WeightOzQuestionId > 0)
            {
                WeightOzAnswer = GetAnswer(WeightOzQuestionId);
            }
            if (WeightKgsQuestionId > 0)
            {
                WeightKgsAnswer = GetAnswer(WeightKgsQuestionId);
            }
            if (WeightGmsQuestionId > 0)
            {
                WeightGmsAnswer = GetAnswer(WeightGmsQuestionId);
            }
        }
        /// <summary>
        /// Gets the PatientSurgeryQuestionAnswer.
        /// </summary>
        /// <param name="questionId">The question id.</param>
        /// <returns></returns>
        public PatientSurgeryQuestionAnswer GetAnswer(int questionId)
        {
            if (PatientSurgeryQuestionAnswers != null)
            {
                return PatientSurgeryQuestionAnswers.FirstOrDefault(ans => ans.QuestionId == questionId);
            }
            return null;
        }
    }
}
