﻿namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class ManageInsurancePlansLoadArguments
    {
        /// <summary>
        /// Insurance plans filter string. Will search in Insurer and Plan names.
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Specific Insurer record to load
        /// </summary>
        public int? InsurerId { get; set; }
    }
}
