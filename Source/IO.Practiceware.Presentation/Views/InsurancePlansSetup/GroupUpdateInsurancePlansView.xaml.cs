﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for GroupUpdateInsurancePlansView.xaml
	/// </summary>
	public partial class GroupUpdateInsurancePlansView : UserControl
	{
		public GroupUpdateInsurancePlansView()
		{
			this.InitializeComponent();
		}
	}
}