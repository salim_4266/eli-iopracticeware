using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class InsurancePlansSetupViewManager
    {
        private readonly IInteractionManager _interactionManager;
        private ManageInsurancePlansViewContext _dataContext;

        public InsurancePlansSetupViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ManageInsurancePlans(ManageInsurancePlansLoadArguments loadArguments)
        {
            var canShowInsurancePlans = PermissionId.ViewInsurersAndPlans.EnsurePermission();
            if (!canShowInsurancePlans) { return; }

            var view = new ManageInsurancePlansView();
            _dataContext = view.DataContext.EnsureType<ManageInsurancePlansViewContext>();
            _dataContext.LoadArguments = loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view, 
                    WindowState = WindowState.Normal, 
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });
        }
    }
}