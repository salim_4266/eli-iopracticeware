﻿using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Presentation.ViewServices.InsurancePlansSetup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class RemittanceMappingViewContext : IViewContext
    {
        readonly IInteractionManager _interactionManager;
        readonly IInsurancePlansSetupViewService _viewService;

        public RemittanceMappingViewContext(
            IInteractionManager interactionManager,
            IInsurancePlansSetupViewService viewService,
            Func<PayerMappingViewModel> createPayerMapping)
        {
            _interactionManager = interactionManager;
            _viewService = viewService;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            SaveMappings = Command.Create(ExecuteSaveMappings).Async(() => InteractionContext);
            Cancel = Command.Create(ExecuteCancel).Async(() => InteractionContext);

            NewPayerMappingFactory = createPayerMapping;
        }

        void OnInteractionContextCompleting(object sender, CancelEventArgs e)
        {
            // Are there any unsaved changes?
            var unsavedChanges = PayerMappings != null 
                                 && PayerMappings.CastTo<IEditableObjectExtended>().IsChanged;

            if (!unsavedChanges) return;

            // Confirm whether to save them
            bool? saveChanges = false;
            this.Dispatcher().Invoke(() =>
                {
                    saveChanges = _interactionManager.Confirm("Would you like to save your changes?");
                });

            // Yes?
            if ((bool)saveChanges)
            {
                // Save
                _viewService.SavePayerMappings(PayerMappings);
            }
        }

        void ExecuteCancel()
        {
            InteractionContext.Complete(false);
        }

        void ExecuteSaveMappings()
        {
            // Save
            _viewService.SavePayerMappings(PayerMappings);
            PayerMappings.CastTo<IEditableObjectExtended>().AcceptCustomEdits();

            // Quit
            InteractionContext.Complete(false);
        }

        void ExecuteLoad()
        {
            // Subscribe to completing event to act on window closing
            InteractionContext.Completing += OnInteractionContextCompleting;

            PayerMappings = _viewService.GetPayerMappings().ToExtendedObservableCollection();
            PayerMappings.CastTo<IEditableObjectExtended>().InitiateCustomEdit();
        }

        /// <summary>
        /// Load the view
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Saves mapping
        /// </summary>
        public ICommand SaveMappings { get; protected set; }

        /// <summary>
        /// Ends interaction
        /// </summary>
        public ICommand Cancel { get; protected set; }

        /// <summary>
        /// A factory method which can create new payer mappings
        /// </summary>
        public Func<PayerMappingViewModel> NewPayerMappingFactory { get; protected set; }

        [DispatcherThread]
        public virtual ObservableCollection<PayerMappingViewModel> PayerMappings { get; set; }

        public IInteractionContext InteractionContext { get; set; }
    }
}