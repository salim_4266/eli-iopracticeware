using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.Utilities;
using IO.Practiceware.Presentation.ViewServices.InsurancePlansSetup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class ManageInsurancePlansViewContext : IViewContext
    {
        readonly IInsurancePlansSetupViewService _viewService;
        readonly Func<InsurancePlanToManageViewModel> _createInsurancePlanToManage;
        readonly Func<InsurerDoctorAssignmentViewModel> _createInsurerDoctorAssignment;
        readonly IInteractionManager _interactionManager;
        private readonly WarningsViewManager _warningsViewManager;
        private readonly UtilitiesViewManager _utilitiesViewManager;

        public ManageInsurancePlansViewContext(
            IInsurancePlansSetupViewService viewService,
            Func<InsurancePlanToManageViewModel> createInsurancePlanToManage,
            Func<InsurerDoctorAssignmentViewModel> createInsurerDoctorAssignment,
            IInteractionManager interactionManager,
            WarningsViewManager warningsViewManager,
            UtilitiesViewManager utilitiesViewManager)
        {
            _viewService = viewService;
            _createInsurancePlanToManage = createInsurancePlanToManage;
            _createInsurerDoctorAssignment = createInsurerDoctorAssignment;
            _interactionManager = interactionManager;
            _warningsViewManager = warningsViewManager;
            _utilitiesViewManager = utilitiesViewManager;

            // We need to init empty collection here, so that filtering works
            InsurancePlans = new ExtendedObservableCollection<InsurancePlanToManageViewModel>();

            // Just init beforehand
            SelectedInsurancePlans = new ExtendedObservableCollection<InsurancePlanToManageViewModel>();
            IsActiveChecked = true;

            LoadMoreInfo = Command.Create<InsurancePlanToManageViewModel>(ExecuteLoadMoreInfo);
            LoadChangeHistory = Command.Create<InsurancePlanToManageViewModel>(ExecuteLoadChangeHistory).Async(() => InteractionContext);
            LoadClaimCrossovers = Command.Create<InsurancePlanToManageViewModel>(ExecuteLoadClaimCrossovers).Async(() => InteractionContext);
            LoadClaimCrossoverInsurers = Command.Create(ExecuteLoadClaimCrossoverInsurers).Async(() => InteractionContext);
            LoadClaimFormSetup = Command.Create<InsurancePlanToManageViewModel>(ExecuteLoadClaimFormSetup).Async(() => InteractionContext);
            ReactivatePlan = Command.Create<InsurancePlanToManageViewModel>(ExecuteReactivatePlan).Async(() => InteractionContext);
            SavePlanChanges = Command.Create<InsurancePlanToManageViewModel>(ExecuteSavePlanChanges).Async(() => InteractionContext);
            ApplyPlanChanges = Command.Create<InsurancePlanToManageViewModel>(ExecuteApplyPlanChanges).Async(() => InteractionContext);
            CancelPlanChanges = Command.Create<InsurancePlanToManageViewModel>(ExecuteCancelPlanChanges);
            NotUsePlanByDependent = Command.Create<InsurancePlanToManageViewModel>(ExecuteNotUsePlanByDependent).Async(() => InteractionContext);
            AddClaimCrossovers = Command.Create<InsurancePlanToManageViewModel>(ExecuteAddClaimCrossovers);
            AddClaimFormType = Command.Create<InsurancePlanToManageViewModel>(ExecuteAddClaimFormType);

            OpenMergeInsurers = Command.Create(ExecuteOpenMergeInsurers);
            OpenRemittanceMapping = Command.Create(ExecuteOpenRemittanceMapping);
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ReloadPlans = Command.Create(ExecuteReloadPlans).Async(() => InteractionContext);
            NewPlan = Command.Create(ExecuteNewPlan);
            ArchivePlans = Command.Create(ExecuteArchivePlans).Async(() => InteractionContext);
            DeletePlans = Command.Create(ExecuteDeletePlans).Async(() => InteractionContext);
            GroupUpdatePlans = Command.Create(ExecuteGroupUpdatePlans);
            CancelAllChanges = Command.Create(ExecuteCancelAllChanges).Async(() => InteractionContext);
            ApplyAllChanges = Command.Create(ExecuteApplyAllChanges).Async(() => InteractionContext);
            SaveAllChanges = Command.Create(ExecuteSaveAllChanges).Async(() => InteractionContext);
            Closing = Command.Create(ExecuteClosing);

            // Setup callbacks
            UniqueFieldValues = FilterInsurancePlansByField;
            EligableForCrossover = FilterEligableForCrossover;
            CheckCanEditPlan = p => CanEditPlan((InsurancePlanToManageViewModel)p);
        }

        private void ExecuteLoad()
        {
            // Set filter
            Filter = LoadArguments.IfNotNull(p => p.Filter);
            IsSingleInsurerMode = LoadArguments.IfNotNull(p => p.InsurerId) != null;

            // Load data from server
            var response = _viewService.LoadManageInsurancePlansViewData(
                LoadArguments.IfNotNull(p => p.InsurerId), IsArchivedChecked, IsActiveChecked);

            // Load reference types
            PlanTypes = response.PlanTypes.ToExtendedObservableCollection();
            PhoneTypes = response.PhoneTypes.ToExtendedObservableCollection();
            States = response.States.ToExtendedObservableCollection();
            BusinessClasses = response.BusinessClasses.ToExtendedObservableCollection();
            ClaimFileIndicators = response.ClaimFileIndicators.ToExtendedObservableCollection();
            ClaimFileReceivers = response.ClaimFileReceivers.ToExtendedObservableCollection();
            ClaimFormTypes = response.ClaimFormTypes.ToExtendedObservableCollection();
            InvoiceTypes = response.InvoiceTypes.ToExtendedObservableCollection();
            Doctors = response.Doctors.ToExtendedObservableCollection();
            DiagnosisCodeSet = Enums.GetValues<DiagnosisTypeId>().Select(x => new NamedViewModel { Id = (int)x, Name = x.GetDisplayName() }).ToList();
            DiagnosisCodeSet.Insert(0, new NamedViewModel { Id = -1, Name = "Remove Setting" });
            // Load plans
            ReloadInsurancePlansFrom(response.MatchedInsurancePlans, false);
        }

        private void ExecuteArchivePlans()
        {
            if (!CanArchivePlans) return;

            // Get list of active plans we should archive
            var plansToArchive = SelectedInsurancePlans
                .Where(p => !p.IsArchived && p.Id.HasValue)
                .ToList();

            // Check if these plans have pending changes
            var changedPlans = plansToArchive.Where(p => p.CastTo<IChangeTracking>().IsChanged).ToList();
            if (changedPlans.Count > 0)
            {
                bool? forceArchive = false;
                this.Dispatcher().Invoke(() =>
                {
                    forceArchive = _interactionManager.Confirm("Some of the selected plans have unsaved changes. Discard them and then archive?");
                });

                // Cancel operation if "no"
                if (forceArchive == null || !forceArchive.Value)
                {
                    return;
                }

                // Cancel all pending changes
                changedPlans.ForEach(p => p.CastTo<IEditableObjectExtended>().CancelCustomEdits());
            }

            // Archive plans
            _viewService.ArchivePlans(plansToArchive.Select(p => p.Id.Value).ToArray());

            // Update plan archived state and update change tracking
            plansToArchive.ForEach(p => p.IsArchived = true);

            // Remove archived plans from view if not showing them
            if (!IsArchivedChecked)
            {
                InsurancePlans.RemoveAll(plansToArchive);
            }
        }

        private void ExecuteReactivatePlan(InsurancePlanToManageViewModel plan)
        {
            if (!plan.CanReactivatePlan || !plan.Id.HasValue) return;

            // Question reactivation
            bool? reactivate = false;
            this.Dispatcher().Invoke(() =>
                {
                    reactivate = _interactionManager.Confirm("Are you sure you want to reactivate this plan?");
                });

            // Perform it
            if (reactivate != null && reactivate.Value)
            {
                // Reactive the plan
                _viewService.ReactiveInsurancePlan(plan.Id.Value);
                plan.IsArchived = false;

                // If we are not showing active plans -> remove it
                if (!IsActiveChecked)
                {
                    InsurancePlans.Remove(plan);
                }
            }
        }

        private void ExecuteAddClaimCrossovers(InsurancePlanToManageViewModel plan)
        {
            if (!plan.CanEditPlan) return;

            // Filter claim crossover to add, so that we don't insert duplicates or invalid ones
            var insurersToAdd = plan.ClaimCrossoversToAdd
                                    .Where(p => FilterEligableForCrossover(plan, p));

            // Add selected claim crossover
            plan.ClaimCrossovers.AddRangeWithSinglePropertyChangedNotification(insurersToAdd);

            // Clear the selection
            plan.ClaimCrossoversToAdd.Clear();
        }

        private void ExecuteAddClaimFormType(InsurancePlanToManageViewModel plan)
        {
            if (!plan.CanEditPlan) return;

            // Add only valid claim form types
            if (plan.InsurerClaimFormToAdd == null
                || plan.InsurerClaimFormToAdd.FormType == null
                || plan.InsurerClaimFormToAdd.InvoiceType == null)
                return;

            // Actually add it
            plan.InsurerClaimForms.Add(plan.InsurerClaimFormToAdd);
        }

        private void ExecuteCancelPlanChanges(InsurancePlanToManageViewModel plan)
        {
            // Cancel any pending changes and begin editing again
            plan.CastTo<IEditableObjectExtended>().CancelCustomEdits();

            // Hide details
            plan.IsShowingDetails = false;
        }

        private void ExecuteDeletePlans()
        {
            if (!CanDeletePlans) return;

            var plansToDeleteOnServer = SelectedInsurancePlans.Where(p => p.Id.HasValue).ToList();
            var plansToDeleteLocally = SelectedInsurancePlans.Where(p => !p.Id.HasValue).ToList();

            // First delete local plans
            InsurancePlans.RemoveAll(plansToDeleteLocally);

            // TODO: remove below when Telerik fixes bug with SelectedItems for newly added items to the Grid
            SelectedInsurancePlans.RemoveAll(plansToDeleteLocally);

            // No plans to remove on server -> quit
            if (plansToDeleteOnServer.Count <= 0) return;

            // Issue delete on server end
            var result = _viewService.DeleteInsurancePlans(plansToDeleteOnServer
                .Select(p => p.Id.Value).ToArray());

            // Remove those successfully deleted
            if (result.DeletedPlans != null && result.DeletedPlans.Count > 0)
            {
                var deletedSuccessfuly = InsurancePlans
                    .Where(p => p.Id.HasValue && result.DeletedPlans.Contains(p.Id.Value))
                    .ToList();
                InsurancePlans.RemoveAll(deletedSuccessfuly);
            }

            // Alert on failed removal
            if (result.PlanRemovalErrors != null && result.PlanRemovalErrors.Count > 0)
            {
                this.Dispatcher().Invoke(() => _interactionManager.Alert(
                    new AlertArguments
                    {
                        Content = "Plans cannot be deleted if they are in use."
                    }));
            }
        }

        private void ExecuteGroupUpdatePlans()
        {
            this.Dispatcher().Invoke(() => _interactionManager.Alert(
                new AlertArguments
                {
                    Content = "Not implemented yet"
                }));
        }

        private void ExecuteSavePlanChanges(InsurancePlanToManageViewModel insurancePlan)
        {
            if (!insurancePlan.CanEditPlan) return;

            CommitPlanChanges(insurancePlan, true);
        }

        private void ExecuteApplyPlanChanges(InsurancePlanToManageViewModel insurancePlan)
        {
            if (!insurancePlan.CanEditPlan) return;

            CommitPlanChanges(insurancePlan, false);
        }

        private void ExecuteLoadMoreInfo(InsurancePlanToManageViewModel insurancePlan)
        {
            // Ensure doctor assignment lists all doctors
            var listedDoctors = insurancePlan.DoctorAssignments.Select(p => p.Doctor.Id).ToList();
            var notPresentDoctors = Doctors.Where(p => !listedDoctors.Contains(p.Id)).ToList();

            // Ensure we don't call code below multiple times
            if (notPresentDoctors.Count <= 0) return;

            // Add not present doctors
            notPresentDoctors.ForEach(d => insurancePlan.DoctorAssignments.Add(
                _createInsurerDoctorAssignment().Modify(a => a.Doctor = d)));

            insurancePlan.DoctorAssignments.CastTo<IEditableObjectExtended>().AcceptCustomEdits();
        }

        private void ExecuteLoadChangeHistory(InsurancePlanToManageViewModel insurancePlan)
        {
            // Reload every time requested
            insurancePlan.ChangeHistory = !insurancePlan.Id.HasValue
                ? new List<AuditEntryViewModel>()
                : _viewService.LoadChangeHistory(insurancePlan.Id.Value).ToList();
        }

        private void ExecuteLoadClaimCrossovers(InsurancePlanToManageViewModel insurancePlan)
        {
            // Load only first time, so that user's changes are not lost when moving between tabs
            if (insurancePlan.IsClaimCrossoversLoaded) return;

            // Force not cascading editing events, so that if editing plan in the grid is later added for crossover -> it's state won't be affected by editing events on Insurer 
            insurancePlan.ClaimCrossovers.CascadeEditingEventsToChildren = false;
            insurancePlan.ClaimCrossoversToAdd = new ExtendedObservableCollection<InsurancePlanToManageViewModel>();

            Action loadClaimCrossovers = () =>
                {
                    // For existing -> Load information from server
                    if (insurancePlan.Id.HasValue)
                    {
                        // Load it
                        var reply = _viewService.LoadClaimCrossover(insurancePlan.Id.Value);

                        // Populate values
                        insurancePlan.ClaimCrossovers.AddRangeWithSinglePropertyChangedNotification(reply);
                    }
                };

            loadClaimCrossovers();

            // We just fetched it -> change state to as if there were no changes, so we can revert to this state
            insurancePlan.ClaimCrossovers.CastTo<IEditableObjectExtended>().AcceptCustomEdits();

            insurancePlan.IsClaimCrossoversLoaded = true;
        }

        private void ExecuteLoadClaimCrossoverInsurers()
        {
            ClaimCrossoverInsurers = _viewService.LoadInsurancePlans(false, true, ClaimCrossoverFilter).ToExtendedObservableCollection();
        }

        private void ExecuteLoadClaimFormSetup(InsurancePlanToManageViewModel insurancePlan)
        {
            // Load only first time, so that user's changes are not lost when moving between tabs
            if (insurancePlan.IsInsurerClaimFormsLoaded) return;

            // For existing -> Load information from server
            if (insurancePlan.Id.HasValue)
            {
                var reply = _viewService.LoadClaimForms(insurancePlan.Id.Value);
                insurancePlan.InsurerClaimForms.AddRangeWithSinglePropertyChangedNotification(reply);
            }

            // We just fetched it -> change state to as if there were no changes
            insurancePlan.InsurerClaimForms.CastTo<IEditableObjectExtended>().AcceptCustomEdits();

            insurancePlan.IsInsurerClaimFormsLoaded = true;
        }

        private void ExecuteReloadPlans()
        {
            var response = _viewService.LoadInsurancePlans(IsArchivedChecked, IsActiveChecked, Filter);
            ReloadInsurancePlansFrom(response);
        }

        private void ExecuteNewPlan()
        {
            if (!CanCreateNewPlan) return;

            // Create plan
            var newPlan = _createInsurancePlanToManage();

            // Provide empty Id and set temporary Guid Id
            newPlan.Id = null;
            newPlan.NewPlanId = Guid.NewGuid();

            if (SelectedInsurancePlans.Count > 0)
            {
                // Use selected insurance's plan name as default one
                newPlan.InsuranceName = SelectedInsurancePlans.First().InsuranceName;
            }
            else if (!string.IsNullOrEmpty(Filter))
            {
                // If no plan is selected and we have results filtered -> use filter as Insurance name
                // This ensures user will be able to find it
                newPlan.InsuranceName = Filter;
            }

            // Prepare for editing
            PreparePlanForEditing(newPlan);

            // Add it
            InsurancePlans.Add(newPlan);
        }

        private void ExecuteNotUsePlanByDependent(InsurancePlanToManageViewModel insurancePlan)
        {
            // Ok, for newly created plans
            if (!insurancePlan.Id.HasValue) return;

            // Ok, for plans not yet used by dependents
            if (!_viewService.IsPlanUsedByDependents(insurancePlan.Id.Value)) return;

            // Force it to change value back
            insurancePlan.CanBeUsedByDependent = true;

            // Warn user
            this.Dispatcher().Invoke(() => _interactionManager.Alert(
                new AlertArguments
                {
                    Content = "This option cannot be disabled due to existing dependents being linked with this plan. Instead, make a new plan that does not allow for it to be used with dependents, then archive the existing plan if desired."
                }));
        }

        private void ExecuteCancelAllChanges()
        {
            // Since we are closing window by command -> no need to actually cancel changes on plans
            InteractionContext.Complete(false);
        }

        private void ExecuteClosing()
        {
            // Are there any unsaved changes?
            var unsavedChanges = InsurancePlans.Any(
                p => p.CastTo<IChangeTracking>().IsChanged || !p.Id.HasValue);

            if (unsavedChanges)
            {
                // Confirm whether to save them
                bool? saveChanges = false;
                this.Dispatcher().Invoke(() =>
                {
                    saveChanges = _interactionManager.Confirm("Save changes?");
                });

                // Yes?
                if ((bool)saveChanges)
                {
                    // Commit all changes
                    CommitAllChanges(false);
                }
            }
        }

        private void ExecuteApplyAllChanges()
        {
            if (!CanEditPlans) return;

            CommitAllChanges(false);
        }

        private void ExecuteSaveAllChanges()
        {
            if (!CanEditPlans) return;

            CommitAllChanges(true);
        }

        void ExecuteOpenRemittanceMapping()
        {
            this.Dispatcher().Invoke(() =>
            {
                // Show it
                var view = new RemittanceMappingView();
                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });
            });
        }


        private void ExecuteOpenMergeInsurers()
        {
            _utilitiesViewManager.ShowMergeInsurers();
        }

        private bool CanEditPlan(InsurancePlanToManageViewModel insurancePlan)
        {
            return insurancePlan != null && insurancePlan.CanEditPlan;
        }

        private bool FilterEligableForCrossover(object inputModel, object itemToFilter)
        {
            var insurancePlan = (InsurancePlanToManageViewModel)inputModel;
            var insurancePlanToFilter = (InsurancePlanToManageViewModel)itemToFilter;

            // Skip new, archived and itself
            if (!insurancePlanToFilter.Id.HasValue
                || insurancePlanToFilter.IsArchived
                || insurancePlanToFilter.Id == insurancePlan.Id)
            {
                return false;
            }

            // Filter those already selected
            return insurancePlan.ClaimCrossovers == null
                || insurancePlan.ClaimCrossovers.All(p => p.Id != insurancePlanToFilter.Id);
        }

        private IList FilterInsurancePlansByField(object inputModel, string displayMemberPath, IList insurancePlansToFilter)
        {
            Func<object, string> fieldStringValue = o =>
            {
                if (o == null) return null;
                var value = Binder.GetValue(o, displayMemberPath);
                return value == null ? null : value.ToString();
            };

            // Filter collection by "Insurer" group and then return entries with Distinct field values
            var model = (InsurancePlanToManageViewModel)inputModel;
            var currentItems = insurancePlansToFilter.Cast<InsurancePlanToManageViewModel>();
            return currentItems
                .Where(p => p.InsuranceName == model.IfNotNull(m => m.InsuranceName)
                    && !string.IsNullOrWhiteSpace(fieldStringValue(p)))
                .Distinct(EqualityComparer.For<InsurancePlanToManageViewModel>(
                    (p1, p2) => fieldStringValue(p1) == fieldStringValue(p2)))
                .ToList();
        }

        private void ReloadInsurancePlansFrom(ICollection<InsurancePlanToManageViewModel> insurancePlans, bool dropSingleInsurerMode = true)
        {
            // Unsubscribe from events before clearing
            insurancePlans.ForEach(plan => plan.CastTo<INotifyPropertyChanged>()
                .PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnEditedInsurancePlanPropertyChanged));

            // Clear
            SelectedInsurancePlans.Clear();
            InsurancePlans.Clear();

            // Prepare each plan for editing
            insurancePlans.ForEach(PreparePlanForEditing);

            // Drop single insurer mode
            if (dropSingleInsurerMode)
            {
                IsSingleInsurerMode = false;
            }

            // Update plans
            // Collection itself is not changed to keep filtering working
            InsurancePlans.AddRangeWithSinglePropertyChangedNotification(insurancePlans);
        }

        private void PreparePlanForEditing(InsurancePlanToManageViewModel insurancePlan)
        {
            // Turn on edits tracking
            insurancePlan.CastTo<IEditableObjectExtended>().InitiateCustomEdit();

            // Listen to property changes to implement additional business logic
            insurancePlan.CastTo<INotifyPropertyChanged>().PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnEditedInsurancePlanPropertyChanged);
        }

        static readonly string PayedIdPropertyName = Reflector.GetMember<InsurancePlanToManageViewModel>(c => c.PayerId).Name;

        void OnEditedInsurancePlanPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var insurancePlan = sender as InsurancePlanToManageViewModel;

            // Implementing: if only one Claim File Receiver has been set up at the practice, default the Claim File Receiver field 
            // to that receiver if the  payer ID is filled in
            if (e.PropertyName == PayedIdPropertyName
                && insurancePlan != null
                && insurancePlan.Receiver == null
                && ClaimFileReceivers.Count == 1
                && !string.IsNullOrEmpty(insurancePlan.PayerId))
            {
                insurancePlan.Receiver = ClaimFileReceivers.First();
            }

        }

        private void CommitAllChanges(bool endInteraction)
        {
            // Find all changed plans and new ones
            var plansToCommit = InsurancePlans
                .Where(p => p.CastTo<IChangeTracking>().IsChanged || !p.Id.HasValue)
                .ToList();

            //Remove Empty phone numbers
            plansToCommit.ForEach(p => p.InsurerPhones.Where(a => a.IsEmpty).ToArray().ForEachWithSinglePropertyChangedNotification(a => p.InsurerPhones.Remove(a)));

            // Validate all plans to commit
            var validationResult = plansToCommit.ValidateItems();
            if (validationResult.Any())
            {
                var errors = validationResult.ToDictionary(
                    i => string.Format("{0} ({1}): ", i.Key.PlanName, i.Key.Id),
                    v => v.Value.Values.Join(Environment.NewLine));

                ShowValidationErrorsDialog(errors.ToValidationMessagesList());
            }
            else
            {
                // Save all plans
                var idMapping = _viewService.SavePlans(plansToCommit);

                if (endInteraction)
                {
                    // No need to manage the state of plans, since we are ending interaction
                    InteractionContext.Complete(false);
                }
                else
                {
                    foreach (var insurancePlan in plansToCommit)
                    {
                        // Update Ids of newly inserted items
                        if (!insurancePlan.Id.HasValue)
                        {
                            insurancePlan.Id = idMapping[insurancePlan.NewPlanId.Value];
                            insurancePlan.NewPlanId = null;
                        }

                        // Accept changes
                        insurancePlan.CastTo<IEditableObjectExtended>().AcceptCustomEdits();
                    }
                }
            }
        }

        private void CommitPlanChanges(InsurancePlanToManageViewModel insurancePlan, bool hideDetails)
        {
            // Validate first
            var validationResult = insurancePlan.Validate();
            if (validationResult != null && validationResult.Count > 0)
            {
                ShowValidationErrorsDialog(validationResult.ToValidationMessagesList());
            }
            else
            {
                // Save plan and confirm its Id (it could have changed if it was new plan)
                var id = _viewService.SavePlan(insurancePlan);
                insurancePlan.Id = id;

                // Hide details
                if (hideDetails)
                {
                    insurancePlan.IsShowingDetails = false;
                }

                // Continue editing mode
                insurancePlan.CastTo<IEditableObjectExtended>().AcceptCustomEdits();
            }
        }

        private void ShowValidationErrorsDialog(IEnumerable<string> errors)
        {
            this.Dispatcher().Invoke(() => _warningsViewManager.ShowWarnings(errors, "Cannot Save: "));
        }

        public IInteractionContext InteractionContext { get; set; }

        public ManageInsurancePlansLoadArguments LoadArguments { get; set; }

        /// <summary>
        /// Loads the context
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Loads data for more info tab
        /// </summary>
        public ICommand LoadMoreInfo { get; protected set; }

        /// <summary>
        /// Loads change history screen for insurer
        /// </summary>
        public ICommand LoadChangeHistory { get; protected set; }

        /// <summary>
        /// Loads claim form setup screen for insurer
        /// </summary>
        public ICommand LoadClaimFormSetup { get; protected set; }

        /// <summary>
        /// Loads claim crossover screen for insurer
        /// </summary>
        public ICommand LoadClaimCrossovers { get; protected set; }

        /// <summary>
        /// Loads claim crossover insurers upon search
        /// </summary>
        public ICommand LoadClaimCrossoverInsurers { get; protected set; }

        /// <summary>
        /// Reloads insurance plans
        /// </summary>
        public ICommand ReloadPlans { get; protected set; }

        /// <summary>
        /// Group update selected insurance plans
        /// </summary>
        public ICommand GroupUpdatePlans { get; protected set; }

        /// <summary>
        /// Archive selected insurance plans
        /// </summary>
        public ICommand ArchivePlans { get; protected set; }

        /// <summary>
        /// Reactivate plan passed as parameter
        /// </summary>
        public ICommand ReactivatePlan { get; protected set; }

        /// <summary>
        /// Delete selected insurance plans
        /// </summary>
        public ICommand DeletePlans { get; protected set; }

        /// <summary>
        /// Command to cancel all changes and close the window
        /// </summary>
        public ICommand CancelAllChanges { get; protected set; }

        /// <summary>
        /// Gets a command that is executed when the associated view is closing.
        /// It is able to cancel the closing event through the passed in bool Action.
        /// </summary>
        public ICommand Closing { get; protected set; }

        /// <summary>
        /// Command to apply all changes and remain on the window
        /// </summary>
        public ICommand ApplyAllChanges { get; protected set; }

        /// <summary>
        /// Command to save all changes and close the window
        /// </summary>
        public ICommand SaveAllChanges { get; protected set; }

        /// <summary>
        /// Begins creation of new plan
        /// </summary>
        public ICommand NewPlan { get; protected set; }

        /// <summary>
        /// Saves changes made to plan and collapses detail row
        /// </summary>
        public ICommand SavePlanChanges { get; protected set; }

        /// <summary>
        /// Cancels all pending changes made to the plan and collapses detail row
        /// </summary>
        public ICommand CancelPlanChanges { get; protected set; }

        /// <summary>
        /// Applies all pending changes made to the plan and doesn't change the detail row state
        /// </summary>
        public ICommand ApplyPlanChanges { get; protected set; }

        /// <summary>
        /// Triggered when plan is set to be not used by dependent
        /// </summary>
        public ICommand NotUsePlanByDependent { get; protected set; }

        /// <summary>
        /// Adds selected claim crossovers to plan's crossover
        /// </summary>
        public ICommand AddClaimCrossovers { get; protected set; }

        /// <summary>
        /// Adds selected claim form type to plan's claim form types
        /// </summary>
        public ICommand AddClaimFormType { get; protected set; }

        /// <summary>
        /// Opens remittance mapping window
        /// </summary>
        public ICommand OpenRemittanceMapping { get; protected set; }

        /// <summary>
        /// Opens merge screen
        /// </summary>
        public ICommand OpenMergeInsurers { get; protected set; }

        /// <summary>
        /// Suggests possible field values based on current item in the context
        /// </summary>
        [DispatcherThread]
        public virtual Func<object, string, IList, IList> UniqueFieldValues { get; set; }

        /// <summary>
        /// Suggests Insurers which are not yet selected for crossover
        /// </summary>
        [DispatcherThread]
        public virtual Func<object, object, bool> EligableForCrossover { get; set; }

        /// <summary>
        /// Checks whether plan is not in archived state
        /// </summary>
        [DispatcherThread]
        public virtual Func<object, bool> CheckCanEditPlan { get; set; }

        /// <summary>
        /// Contains a list of insurance plans and their details
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<InsurancePlanToManageViewModel> InsurancePlans { get; set; }

        /// <summary>
        /// Claim crossover options
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<InsurancePlanToManageViewModel> ClaimCrossoverInsurers { get; set; }

        /// <summary>
        /// Contains a list of plan types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> PlanTypes { get; set; }

        /// <summary>
        /// Contains a list of phone types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> PhoneTypes { get; set; }

        /// <summary>
        /// Contains a list of states
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<StateOrProvinceViewModel> States { get; set; }

        /// <summary>
        /// Contains a list of business classes
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> BusinessClasses { get; set; }

        /// <summary>
        /// Contains a list of Claim file indicators
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ClaimFileIndicators { get; set; }

        /// <summary>
        /// Contains a list of Claim file receivers
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ClaimFileReceivers { get; set; }

        /// <summary>
        /// Contains a list of Claim form types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ClaimFormTypeViewModel> ClaimFormTypes { get; set; }

        /// <summary>
        /// Contains a list of Invoice types
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> InvoiceTypes { get; set; }

        /// <summary>
        /// Contains a list of doctors
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> Doctors { get; set; }

        [DispatcherThread]
        public virtual List<NamedViewModel> DiagnosisCodeSet { get; set; }

        /// <summary>
        /// Checked insurance plans
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<InsurancePlanToManageViewModel> SelectedInsurancePlans { get; set; }

        /// <summary>
        /// Insurance plans filter string. Will search in Insurer and Plan names.
        /// </summary>
        [DispatcherThread]
        public virtual string Filter { get; set; }

        /// <summary>
        /// Claim Crossover screen Insurance plans filter string. Will search in Insurer and Plan names.
        /// </summary>
        [DispatcherThread]
        public virtual string ClaimCrossoverFilter { get; set; }

        /// <summary>
        /// Determines whether or not the "Active" checkbox is checked.
        /// </summary>
        [DispatcherThread]
        public virtual bool IsActiveChecked { get; set; }

        /// <summary>
        /// Determines whether or not the "Archived" checkbox is checked.
        /// </summary>
        [DispatcherThread]
        public virtual bool IsArchivedChecked { get; set; }

        /// <summary>
        /// Determines whether screen has been loaded for a specific Insurer record.
        /// </summary>
        [DispatcherThread]
        public virtual bool IsSingleInsurerMode { get; set; }

        #region Calculated

        public virtual bool CanCreateNewPlan
        {
            get { return PermissionId.CreateInsurancePlans.PrincipalContextHasPermission(); }
        }

        public virtual bool CanEditPlans
        {
            get { return PermissionId.EditInsurancePlanInformation.PrincipalContextHasPermission(); }
        }

        [DependsOn("SelectedInsurancePlans")]
        public virtual bool CanArchivePlans
        {
            get
            {
                return SelectedInsurancePlans.Any(p => !p.IsArchived && p.Id.HasValue)
                    && PermissionId.ArchiveInsurancePlans.PrincipalContextHasPermission();
            }
        }

        [DependsOn("SelectedInsurancePlans")]
        public virtual bool CanDeletePlans
        {
            get
            {
                return SelectedInsurancePlans.Any()
                    && PermissionId.DeleteInsurancePlans.PrincipalContextHasPermission();
            }
        }

        #endregion
    }
}