﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for InsurancePlansMoreOptionsView.xaml
	/// </summary>
	public partial class InsurancePlansMoreOptionsView : UserControl
	{
		public InsurancePlansMoreOptionsView()
		{
			this.InitializeComponent();
		}
	}
}