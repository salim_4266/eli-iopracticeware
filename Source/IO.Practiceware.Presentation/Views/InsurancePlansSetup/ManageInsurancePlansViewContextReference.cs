﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class ManageInsurancePlansViewContextReference : DataContextReference
    {
        public new ManageInsurancePlansViewContext DataContext {
            get { return (ManageInsurancePlansViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }
}
