﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for InsurancePlansClaimCrossoverView.xaml
	/// </summary>
	public partial class InsurancePlansClaimCrossoverView : UserControl
	{
		public InsurancePlansClaimCrossoverView()
		{
			this.InitializeComponent();
		}
	}
}