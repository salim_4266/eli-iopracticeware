﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for InsurancePlansChangeHistoryView.xaml
	/// </summary>
	public partial class InsurancePlansChangeHistoryView : UserControl
	{
		public InsurancePlansChangeHistoryView()
		{
			this.InitializeComponent();
		}
	}
}