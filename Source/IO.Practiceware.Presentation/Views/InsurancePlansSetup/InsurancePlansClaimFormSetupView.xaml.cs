﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for InsurancePlansClaimFormSetupView.xaml
	/// </summary>
	public partial class InsurancePlansClaimFormSetupView : UserControl
	{
		public InsurancePlansClaimFormSetupView()
		{
			this.InitializeComponent();
		}
	}
}