﻿using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
	/// <summary>
	/// Interaction logic for InsurancePlanRightClickMenu.xaml
	/// </summary>
	public partial class InsurancePlanRightClickMenu : UserControl
	{
		public InsurancePlanRightClickMenu()
		{
			this.InitializeComponent();
		}
	}
}