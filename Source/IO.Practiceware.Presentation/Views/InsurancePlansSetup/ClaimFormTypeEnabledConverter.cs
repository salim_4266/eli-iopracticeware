using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.InsurancePlansSetup
{
    public class ClaimFormTypeEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isElectronic = (bool)values[0];
            string payerId = (string)values[1];
            object receiver = values[2];
            return !isElectronic || (!string.IsNullOrEmpty(payerId) && receiver != null);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}