﻿using Soaf.Collections;
using Soaf.Presentation;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadGridViewGroupDescriptorBehavior : Behavior<RadGridView>
    {
        public static readonly DependencyProperty GroupDescriptorCommandProperty = DependencyProperty.Register("GroupDescriptorCommand", typeof (ICommand), typeof (RadGridViewGroupDescriptorBehavior));

        protected override void OnAttached()
        {
            GroupDescriptorCommand = Command.Create<string>(ExecuteGroupDescriptorCommand);
            base.OnAttached();
        }

        private void ExecuteGroupDescriptorCommand(string groupDescriptorMemberName)
        {
            AssociatedObject.GroupDescriptors.Clear();
            if (groupDescriptorMemberName.IsNotNullOrEmpty())
            {
                AssociatedObject.GroupDescriptors.Add(new GroupDescriptor {Member = groupDescriptorMemberName});
            }
        }

        public ICommand GroupDescriptorCommand
        {
            get { return GetValue(GroupDescriptorCommandProperty) as ICommand; }
            set { SetValue(GroupDescriptorCommandProperty, value); }
        }
    }
}
