﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf;
using Soaf.Collections;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using DragVisual = Telerik.Windows.DragDrop.DragVisual;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class GridViewDragDropBehavior : Behavior<RadGridView>
    {
        #region IsEnabled

        public static bool GetIsEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnabledProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.RegisterAttached("IsEnabled", typeof(bool), typeof(GridViewDragDropBehavior),
                new PropertyMetadata((OnIsEnabledPropertyChanged)));

        public static void OnIsEnabledPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (bool)e.NewValue ? new GridViewDragDropBehavior() : null;
            dependencyObject.AddOrReplaceBehavior(behavior);
        }

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();

            UnsubscribeFromDragDropEvents();
            SubscribeToDragDropEvents();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            UnsubscribeFromDragDropEvents();
        }

        private void SubscribeToDragDropEvents()
        {
            DragDropManager.AddDragInitializeHandler(AssociatedObject, OnDragInitialize);
            DragDropManager.AddGiveFeedbackHandler(AssociatedObject, OnGiveFeedback);
            DragDropManager.AddDropHandler(AssociatedObject, OnDrop);
            if (AssociatedObject.ParentOfType<RadGridView>() != null)
                DragDropManager.AddDropHandler(AssociatedObject.ParentOfType<RadGridView>(), OnDrop);
            DragDropManager.AddDragDropCompletedHandler(AssociatedObject, OnDragDropCompleted);
            DragDropManager.AddDragOverHandler(AssociatedObject, OnDragOver);
        }

        private void UnsubscribeFromDragDropEvents()
        {
            DragDropManager.RemoveDragInitializeHandler(AssociatedObject, OnDragInitialize);
            DragDropManager.RemoveGiveFeedbackHandler(AssociatedObject, OnGiveFeedback);
            DragDropManager.RemoveDropHandler(AssociatedObject, OnDrop);
            if (AssociatedObject.ParentOfType<RadGridView>() != null)
                DragDropManager.RemoveDropHandler(AssociatedObject.ParentOfType<RadGridView>(), OnDrop);
            DragDropManager.RemoveDragDropCompletedHandler(AssociatedObject, OnDragDropCompleted);
            DragDropManager.RemoveDragOverHandler(AssociatedObject, OnDragOver);
        }

        private void OnDragInitialize(object sender, DragInitializeEventArgs e)
        {
            var details = new DropIndicationDetails();

            var row = e.OriginalSource as GridViewRow ?? (e.OriginalSource as FrameworkElement).ParentOfType<GridViewRow>();
            if (row != null)
            {
                // Save item and source collection
                details.CurrentDraggedItem = row.Item;
                details.DraggedItemSourceCollection = row.GridViewDataControl.ItemsSource as IList;

                IDragPayload dragPayload = DragDropPayloadManager.GeneratePayload(null);

                dragPayload.SetData("DropDetails", details);
                e.Data = dragPayload;
            }

            e.DragVisual = new DragVisual
            {
                DataContext = details.CurrentDraggedItem,
                Content = details.CurrentDraggedItem,
                ContentTemplate = AssociatedObject.TryFindResource("DraggedItemTemplate") as DataTemplate
            };

            e.DragVisualOffset = e.RelativeStartPoint;
            e.AllowedEffects = DragDropEffects.All;
        }

        private static void OnGiveFeedback(object sender, Telerik.Windows.DragDrop.GiveFeedbackEventArgs e)
        {
            e.SetCursor(Cursors.Arrow);
            e.Handled = true;
        }

        private static void OnDragDropCompleted(object sender, DragDropCompletedEventArgs e)
        {}

        private void OnDrop(object sender, Telerik.Windows.DragDrop.DragEventArgs e)
        {
            var details = DragDropPayloadManager.GetDataFromObject(e.Data, "DropDetails") as DropIndicationDetails;
            var grid = details.IfNotNull(d => d.CurrentDraggedOverItem as GridViewDataControl);

            if (details != null 
                && grid != null
                // Ensure it's not the same collection
                && !ReferenceEquals(details.DraggedItemSourceCollection, grid.ItemsSource))
            {
                if (details.DraggedItemSourceCollection != null)
                {
                    details.DraggedItemSourceCollection.Remove(details.CurrentDraggedItem);
                }
                ((IList)grid.ItemsSource).Add(details.CurrentDraggedItem);
            }

            e.Handled = true;
        }

        private void OnDragOver(object sender, Telerik.Windows.DragDrop.DragEventArgs e)
        {
            var dropDetails = DragDropPayloadManager.GetDataFromObject(e.Data, "DropDetails") as DropIndicationDetails;
            if (dropDetails == null) return;

            dropDetails.CurrentDropPosition = DropPosition.Inside;
            dropDetails.CurrentDraggedOverItem = null;

            var source = e.OriginalSource as DependencyObject;
            if (source != null)
            {
                var draggedItemType = dropDetails.CurrentDraggedItem.GetType();
                var childGridMatchingType = GetChildGridMatchingType(source, dropDetails.DraggedItemSourceCollection, draggedItemType);
                if (childGridMatchingType == null)
                {
                    e.Effects = DragDropEffects.None;
                }
                else
                {
                    dropDetails.CurrentDraggedOverItem = childGridMatchingType;
                    e.Effects = DragDropEffects.Move;
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }

            e.Handled = true;
        }

        private static GridViewDataControl GetChildGridMatchingType(DependencyObject dependencyObject, IList draggedItemSourceCollection, Type draggedItemType)
        {
            var candidates = new List<GridViewDataControl>();
            if (dependencyObject is GridViewDataControl)
            {
                candidates.Add((GridViewDataControl)dependencyObject);
            }
            else if (dependencyObject is GridViewRow)
            {
                candidates.Add(((GridViewRow)dependencyObject).GridViewDataControl);
            }

            dependencyObject.ParentOfType<GridViewDataControl>().IfNotNull(candidates.Add);
            dependencyObject.ParentOfType<GridViewRow>().IfNotNull(r => candidates.Add(r.GridViewDataControl));

            var result = candidates
                .FirstOrDefault(v => !ReferenceEquals(draggedItemSourceCollection, v.ItemsSource)
                    && draggedItemType.Is(v.ItemsSource.CastTo<IEnumerable>().FindElementType()));

            return result;
        }
    }
}
