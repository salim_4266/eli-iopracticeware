﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public sealed class HandleScrollEventBehavior : Behavior<UIElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseWheel += WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseWheel -= WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);
            base.OnDetaching();
        }

        void AssociatedObjectPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //handle the event on the attached object, then raise the event on the parent.
            var ao = AssociatedObject.GetParents().FirstOrDefault() as UIElement;
            e.Handled = true;
            var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            e2.RoutedEvent = UIElement.MouseWheelEvent;
            if (ao != null) ao.RaiseEvent(e2);
        }
    }
}
