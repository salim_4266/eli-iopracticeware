﻿using System.Collections.Specialized;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Soaf;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    ///     Used on a DataGrid's Items collection so that when an Item is added, the added 
    ///     Item is selected and therefore gains focus and appears within the view.
    /// </summary>
    public class DataGridActivateAddedItemBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Items.CastTo<INotifyCollectionChanged>().CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(SwitchFocusToNewRow);
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Items.CastTo<INotifyCollectionChanged>().CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(SwitchFocusToNewRow);
        }
        protected void SwitchFocusToNewRow(object sender, NotifyCollectionChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.Action != NotifyCollectionChangedAction.Add) return;
            var row = AssociatedObject.SelectedIndex = propertyChangedEventArgs.NewStartingIndex;
            var obj = AssociatedObject.Items[row];
            AssociatedObject.ScrollIntoView(obj);
            AssociatedObject.UpdateLayout();
        }
    }
}
