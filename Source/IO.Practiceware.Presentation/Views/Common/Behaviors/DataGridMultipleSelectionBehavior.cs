﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class DataGridMultipleSelectionBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseDown += WeakDelegate.MakeWeak<MouseButtonEventHandler>(PreviewMouseLeftButtonDown);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseDown -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(PreviewMouseLeftButtonDown);

        }

        private void PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // get the DataGridRow at the clicked point
            var o = TryFindFromPoint<DataGridRow>(AssociatedObject, e.GetPosition(AssociatedObject));
            // only handle this when Ctrl or Shift not pressed 
            ModifierKeys mods = Keyboard.PrimaryDevice.Modifiers;
            if (o != null && ((int) (mods & ModifierKeys.Control) == 0 && (int) (mods & ModifierKeys.Shift) == 0))
            {
                o.IsSelected = !o.IsSelected;
                var toggleButtons = o.ChildrenOfType<RadToggleButton>().ToList();
                var toggleButton = toggleButtons.Any() ? toggleButtons.First() : null;
                if (toggleButton != null) toggleButton.IsChecked = !toggleButton.IsChecked;
                e.Handled = true;
            }
        }

        private static T TryFindFromPoint<T>(UIElement reference, Point point)
            where T : DependencyObject
        {
            var element = reference.InputHitTest(point) as DependencyObject;
            if (element == null) return null;
            if (element is T) return (T) element;
            return element.ParentOfType<T>();
        }
    }

}
