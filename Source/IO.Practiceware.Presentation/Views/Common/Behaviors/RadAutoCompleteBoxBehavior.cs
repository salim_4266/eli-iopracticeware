﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Primitives;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// This is a standard autocompletebox behavior to be attached to autocompleteboxes throughout the software
    /// 
    /// There are three major functions: display the dropdown on focus, move focus automatically on selection changed, and clear 
    /// search text of any autocompletebox where no item has been selected.
    /// 
    /// Note: additionally, we're handling scrolling here.  Autocompleteboxes shouldn't need their contents to scroll, and as is, they handle 
    /// all the mousewheel events (not good when autocompletebox is inside any kind of scrollviewer)
    /// </summary>
    public class RadAutoCompleteBoxBehavior : Behavior<RadAutoCompleteBox>
    {
        public static readonly DependencyProperty ClearSearchTextProperty = DependencyProperty.Register("ClearSearchText", typeof (bool), typeof (RadAutoCompleteBoxBehavior), new PropertyMetadata(true));

        public bool ClearSearchText
        {
            get { return (bool) GetValue(ClearSearchTextProperty); }
            set { SetValue(ClearSearchTextProperty, value); }
        }

        private IFilteringBehavior _oldFilteringBehavior;

        protected override void OnAttached()
        {
            AssociatedObject.GotFocus += WeakDelegate.MakeWeak<RoutedEventHandler>(DisplayDropDownListOnGotFocus);
            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(MoveFocusOnSelectionChanged);
            AssociatedObject.LostFocus += WeakDelegate.MakeWeak<RoutedEventHandler>(ClearSearchTextOnLostFocus);
            _oldFilteringBehavior = AssociatedObject.FilteringBehavior;
            AssociatedObject.FilteringBehavior = new FilteringBehavior();
            AssociatedObject.PreviewMouseWheel += WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);

            base.OnAttached();
        }


        /// <summary>
        /// Don't let the autocompletebox handle all the mousewheel events
        /// </summary>
        private void AssociatedObjectPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var temp = AssociatedObject as UIElement;
            temp = temp.GetChildren<Popup>().First();
            if (temp != null && temp.IsMouseOver) return;

            e.Handled = true;
            var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            e2.RoutedEvent = UIElement.MouseWheelEvent;
            AssociatedObject.RaiseEvent(e2);
        }

        /// <summary>
        /// On got focus:
        /// 
        /// Check the source of the GotFocus event.  When we retemplate the autocompletebox, clicks sent from objects inside the dropdown template
        /// cause the autocompletebox to lose and gain focus.  If the sender is a button, make sure that button is clicked.
        /// 
        /// Check that the source of the GotFocus event is of type RadListBoxItem.  If it is, we need to make sure that item is selected.  I'm not sure the
        /// cause of this issue, but it's documented in TFS 17402.  Some interaction between this behavior and the built-in functionality for clicking on 
        /// a highlighted item occurs with this behavior.  We'll handle that case here by setting the selected item in code.
        /// 
        /// Finally, display the dropdown by calling Populate()
        /// </summary>
        private void DisplayDropDownListOnGotFocus(object sender, RoutedEventArgs e)
        {

            var originalSource = e.OriginalSource as UIElement;

            //If the original source of the event was a button, ensure that the button is clicked.
            //Note: this is the fix for TFS 15942
            if (originalSource != null && originalSource.GetType() == typeof (RadButton))
            {
                var click = new RoutedEventArgs(ButtonBase.ClickEvent);
                originalSource.RaiseEvent(click);
                click.Handled = true;
                return;
            }

            //If the original source of the event was a list item, ensure that the item is selected

            //Note: this is the fix for TFS 17402; for some reason, when we override the GotFocus event here,
            //the RadAutoCompleteBox does not handle clicking directly on a highlighted RadListBoxItem
            if (originalSource != null && originalSource.GetType() == typeof (RadListBoxItem))
            {
                AssociatedObject.SelectedItem = originalSource.ParentOfType<RadListBox>().SelectedItem;
                if (AssociatedObject.SelectionMode == AutoCompleteSelectionMode.Multiple)
                    AssociatedObject.SearchText = "";
                return;
            }

            string searchText = "";

            //If there's no selected item and the drop down isn't already open, Populate(searchText) to open the dropdown
            if (AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Single) && AssociatedObject.IsDropDownOpen == false)
            {
                if (AssociatedObject.SelectedItem != null)
                {
                    searchText = BindingExpressionHelper.GetValue(AssociatedObject.SelectedItem, AssociatedObject.DisplayMemberPath).ToStringIfNotNull();
                }

                var textBox = AssociatedObject.ChildrenOfType<RadWatermarkTextBox>().FirstOrDefault();
                if (textBox != null && textBox.Text.Length > 0 && AssociatedObject.SelectedItem != null)
                {
                    textBox.SelectAll();
                }

                AssociatedObject.Populate(searchText);
            }

            //If it's multi-select, Populate("") to open the dropdown, with all list items
            if (AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Multiple))
            {
                AssociatedObject.Populate("");
            }
        }

        /// <summary>
        /// If it's a single-select autocompletebox, automatically move focus once an item is selected
        /// If it's a multi-select autocompletebox, when an item is selected, attach an event listener for tab and for enter
        /// </summary>
        private void MoveFocusOnSelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!AssociatedObject.IsKeyboardFocusWithin || AssociatedObject.SelectedItem == null) return;
            if (AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Multiple))
            {
                AssociatedObject.PreviewKeyUp += WeakDelegate.MakeWeak<KeyEventHandler>(AssociatedObjectClearAndMoveFocusOnPreviewKeyUp);
                AssociatedObject.SearchText = "";
            }
            if (AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Single))
            {
                AssociatedObject.PreviewKeyUp += WeakDelegate.MakeWeak<KeyEventHandler>(AssociatedObjectMoveFocusOnPreviewKeyUp);
            }
        }

        /// <summary>
        /// Set the SearchText (which serves to display the text of the selected item, when selection mode is single) and send 
        /// focus to the next object (eliminating the need for users to tab twice which is the case as is, once to select, and again to move focus)
        /// </summary>
        private void AssociatedObjectMoveFocusOnPreviewKeyUp(object sender, KeyEventArgs e)
        {
            //if it's multi-select, we want to stay in the current box
            if (!AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Single)) return;

            //if it's single-select, and the user hits tab, we want to go to the next control in the tab order
            if (((e.Key.Equals(Key.Tab) && Keyboard.Modifiers != ModifierKeys.Shift) || (e.Key.Equals(Key.Enter) && Keyboard.Modifiers != ModifierKeys.Shift)) && AssociatedObject.SelectedItem != null)
            {
                var current = AssociatedObject as UIElement;
                var next = AssociatedObject.PredictFocus(FocusNavigationDirection.Right) as UIElement;

                //first get out of the current control (telerik controls have controls inside controls which can be tab stops)
                while (current != null && next != null && current.IsAncestorOf(next))
                {
                    current = current.PredictFocus(FocusNavigationDirection.Right) as UIElement;
                    next = next.PredictFocus(FocusNavigationDirection.Right) as UIElement;
                }
                if (current != null)
                {
                    current.MoveFocus(e.Key.Equals(Key.Enter) ? new TraversalRequest(FocusNavigationDirection.Down) : new TraversalRequest(FocusNavigationDirection.Next));
                }
            }
            AssociatedObject.PreviewKeyUp -= WeakDelegate.MakeWeak<KeyEventHandler>(AssociatedObjectMoveFocusOnPreviewKeyUp);
        }

        /// <summary>
        /// Re-open the dropdown on PreviewKeyUp, to make it clear that the user can search and select again in multi-select mode
        /// </summary>
        private void AssociatedObjectClearAndMoveFocusOnPreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (!AssociatedObject.SelectionMode.Equals(AutoCompleteSelectionMode.Multiple)) return;
            if ((e.Key.Equals(Key.Enter) || e.Key.Equals(Key.Tab) || e.Key.Equals(Key.Back)) && Keyboard.Modifiers != ModifierKeys.Shift)
            {
                if (!AssociatedObject.IsDropDownOpen)
                    AssociatedObject.Populate("");
            }
            AssociatedObject.PreviewKeyUp -= WeakDelegate.MakeWeak<KeyEventHandler>(AssociatedObjectClearAndMoveFocusOnPreviewKeyUp);
        }

        /// <summary>
        /// On lost focus, check if there's a highlighted item.  If there is, select it.  Otherwise, clear any unused search text to make clear to the user that the search text is not a selected item.
        ///
        /// Single-select case: user enters "ABC" as a diagnosis code, which msut be selected from a list.  There is no "ABC" item, so we delete the "ABC" text 
        /// on lost focus, making it clear no selection has been made.
        ///
        /// Multi-select case: user enters "50" as a modifier and hits tab or enter or clicks.  50 is converted into a selected item, displayed in a BoxesItemTemplate, and then the user enters "ABC".  
        /// Regardless of whether there is a selected item, clear any remaining search text to make it clear the search text is not a selected item.
        /// </summary>
        private void ClearSearchTextOnLostFocus(object sender, RoutedEventArgs e)
        {
            if (!ClearSearchText
                || string.IsNullOrEmpty(AssociatedObject.SearchText)
                || AssociatedObject.SelectedItem != null) return;

            //If there's exactly one filtered item, select it automatically on lost focus
            //Note: this is supports the case where a user enters search text and then clicks away from the box before
            //selecting.  In the case where there's one matching list item, we'll provide a shortcut to automatically select that item
            var items = AssociatedObject.FilteredItems is IList<object>
                ? (IList<object>) AssociatedObject.FilteredItems
                : (IList<object>) AssociatedObject.FilteredItems.ToList(typeof (object));

            if (items.Count == 1)
            {
                AssociatedObject.SelectedItem = items[0];

                if (AssociatedObject.SelectionMode == AutoCompleteSelectionMode.Single
                    && AssociatedObject.SelectedItem != null)
                {
                    AssociatedObject.SearchText = BindingExpressionHelper.GetValue(AssociatedObject.SelectedItem, AssociatedObject.DisplayMemberPath).ToString();
                }
                else if (AssociatedObject.SelectionMode == AutoCompleteSelectionMode.Multiple)
                {
                    AssociatedObject.SearchText = string.Empty;
                }

                var textBox = AssociatedObject.FindChildByType<RadWatermarkTextBox>();
                textBox.CurrentText = AssociatedObject.SearchText;
            }
            else
            {
                // Clear selection
                AssociatedObject.SearchText = string.Empty;
                var textBox = AssociatedObject.FindChildByType<RadWatermarkTextBox>();
                textBox.CurrentText = AssociatedObject.SearchText;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.FilteringBehavior = _oldFilteringBehavior;
            AssociatedObject.GotFocus -= WeakDelegate.MakeWeak<RoutedEventHandler>(DisplayDropDownListOnGotFocus);
            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(MoveFocusOnSelectionChanged);
            AssociatedObject.LostFocus -= WeakDelegate.MakeWeak<RoutedEventHandler>(ClearSearchTextOnLostFocus);
            AssociatedObject.PreviewMouseWheel -= WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);

            base.OnDetaching();
        }

        #region Nested type: FilteringBehavior

        private class FilteringBehavior : Telerik.Windows.Controls.FilteringBehavior
        {
            public override IEnumerable<object> FindMatchingItems(string searchText, IList items, IEnumerable<object> escapedItems, string textSearchPath, TextSearchMode textSearchMode)
            {
                IEnumerable<object> result;

                if (string.IsNullOrWhiteSpace(searchText))
                {
                    result = items.OfType<object>().Except(escapedItems).ToArray();
                }
                else
                    result = base.FindMatchingItems(searchText.Trim(), items, escapedItems, textSearchPath, textSearchMode);

                return result;
            }
        }

        #endregion
    }
}