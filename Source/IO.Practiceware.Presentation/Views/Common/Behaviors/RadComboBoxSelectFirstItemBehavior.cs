﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using Soaf.Presentation;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadComboBoxSelectFirstItemBehavior : Behavior<RadComboBox>
    {
        private PropertyChangeNotifier _itemsSourceNotifier;
        private PropertyChangeNotifier _selectedItemNotifier;

        protected override void OnAttached()
        {
            base.OnAttached();

            SelectFirstItem();

            _selectedItemNotifier = new PropertyChangeNotifier(AssociatedObject, Selector.SelectedItemProperty)
                .AddValueChanged(OnSelectedItemChanged);
            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, ItemsControl.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourceChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourceChanged);
            _selectedItemNotifier.RemoveValueChanged(OnSelectedItemChanged);
        }

        private void OnSelectedItemChanged(object sender, EventArgs e)
        {
            SelectFirstItem();
        }

        private void OnItemsSourceChanged(object sender, EventArgs e)
        {
            SelectFirstItem();
        }

        private void SelectFirstItem()
        {
            if (AssociatedObject.SelectedIndex < 0
                && AssociatedObject.ItemsSource != null
                && AssociatedObject.ItemsSource.OfType<object>().Count() == 1)
            {
                AssociatedObject.SelectedIndex = 0;
            }
        }
    }
}