﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Threading;
using Soaf;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Binding = System.Windows.Data.Binding;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    internal class RowClicksRadGridViewBehavior : Behavior<RadGridView>
    {
        #region DependencyProperty : OnRowRightClick

        public static bool GetHandleClicks(DependencyObject obj)
        {
            return (bool)obj.GetValue(HandleClicksProperty);
        }

        public static void SetHandleClicks(DependencyObject obj, bool value)
        {
            obj.SetValue(HandleClicksProperty, value);
        }

        public static readonly DependencyProperty HandleClicksProperty =
            DependencyProperty.RegisterAttached("HandleClicks", typeof(bool), typeof(RowClicksRadGridViewBehavior));

        public ICommand SingleClickCommand
        {
            get { return GetValue(SingleClickCommandProperty) as ICommand; }
            set { SetValue(SingleClickCommandProperty, value); }
        }

        public static readonly DependencyProperty SingleClickCommandProperty = DependencyProperty.Register("SingleClickCommand", typeof(ICommand), typeof(RowClicksRadGridViewBehavior));


        public ICommand DoubleClickCommand
        {
            get { return GetValue(DoubleClickCommandProperty) as ICommand; }
            set { SetValue(DoubleClickCommandProperty, value); }
        }


        public static readonly DependencyProperty DoubleClickCommandProperty = DependencyProperty.Register("DoubleClickCommand", typeof(ICommand), typeof(RowClicksRadGridViewBehavior));

        public Popup RightClickMenuPopup
        {
            get { return GetValue(RightClickMenuPopupProperty) as Popup; }
            set { SetValue(RightClickMenuPopupProperty, value); }
        }

        public static readonly DependencyProperty RightClickMenuPopupProperty = DependencyProperty.Register("RightClickMenuPopup", typeof(Popup), typeof(RowClicksRadGridViewBehavior));

        public Type DataContextTypeFilter
        {
            get { return GetValue(DataContextTypeFilterProperty) as Type; }
            set { SetValue(DataContextTypeFilterProperty, value); }
        }

        public static readonly DependencyProperty DataContextTypeFilterProperty = DependencyProperty.Register("ViewModelType", typeof(Type), typeof(RowClicksRadGridViewBehavior));

        #endregion

        #region EventHandlers

        private void OnRowRightClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;
            _gridViewRow = source.ParentOfType<GridViewRow>();
            if (_gridViewRow == null) return;
            //check the type of the row is what we expect (this is passed through xaml - for now, we just match on strings... seems to works for our purposes here...)
            if (DataContextTypeFilter != null)
            {
                if (!_gridViewRow.Item.Is(DataContextTypeFilter)) return;
            }

            //open the popup and give it the datacontext of the row we clicked on
            RightClickMenuPopup.IsOpen = true;
            RightClickMenuPopup.Focus();
            BindingOperations.SetBinding(RightClickMenuPopup, Popup.IsOpenProperty, new Binding("IsKeyboardFocusWithin") { Source = RightClickMenuPopup, Mode= BindingMode.OneWay });
            if (_gridViewRow != null) RightClickMenuPopup.DataContext = _gridViewRow.DataContext;
        }


        readonly DispatcherTimer _clickTimer = new DispatcherTimer();
        int _clickCounter;
        GridViewRow _gridViewRow;
        readonly EventHandler _handler;

        public RowClicksRadGridViewBehavior()
        {
            _handler = (s, eventArgs) => EvaluateClicks(_gridViewRow);
        }


        private void OnRowClick(object sender, MouseButtonEventArgs e)
        {

            //get doubleClickTime from the user's mouse settings
            int doubleClickTime = SystemInformation.DoubleClickTime;

            //just in case... make it non-zero
            if (doubleClickTime <= 0) doubleClickTime = 300;

            _clickTimer.Interval = TimeSpan.FromMilliseconds(doubleClickTime);

            var originalSource = e.OriginalSource as FrameworkElement;

            if (originalSource != null && (GetHandleClicks(originalSource) || originalSource.ParentOfType<GridViewHeaderCell>() != null) || originalSource.ParentOfType<RadDropDownButton>() != null)
            {
                e.Handled = true;
                return;
            }

            if (originalSource != null && !(originalSource.GetType() == typeof(RadDropDownButton) || originalSource.ParentOfType<RadContextMenu>() != null))
            {

                _gridViewRow = originalSource.ParentOfType<GridViewRow>();

                //start the timer
                _clickTimer.Stop();
                _clickCounter++;
                _clickTimer.Start();

            }
        }

        private void EvaluateClicks(RadRowItem gridViewRow)
        {
            _clickTimer.Stop();
            //if we don't get a row...
            if (gridViewRow == null)
            {
                _clickCounter = 0;
                return;
            }

            //check the type of the row is what we expect (this is passed through xaml - for now, we just match on strings... seems to works for our purposes here...)
            if (DataContextTypeFilter != null)
            {
                if (!gridViewRow.Item.Is(DataContextTypeFilter))
                {
                    _clickCounter = 0;
                    return;
                }
            }
            if (_clickCounter == 1)
            {
                if (!gridViewRow.IsSelected)
                {
                    if (SingleClickCommand != null) SingleClickCommand.Execute(gridViewRow.DataContext);
                    gridViewRow.IsSelected = true;
                }
                else
                    gridViewRow.IsSelected = false;
            }
            if (_clickCounter > 1)
            {
                if (DoubleClickCommand != null) DoubleClickCommand.Execute(gridViewRow.DataContext);
            }
            _clickCounter = 0;
        }


        private static void VisibilityChanged(object sender, GridViewRowDetailsEventArgs e)
        {
            var grid = sender as RadGridView;
            if (grid == null) return;

            if (e.Row != null && e.Row.Item != null && e.Visibility.HasValue)
            {
                //if the row is now expanded, scroll the item into view
                if (e.Visibility == Visibility.Visible)
                    grid.ScrollIntoView(e.Row.Item);

                //if the row is now collapsed, ensure the current row is in view, and check whether the bottom of the current row is lower (greater than) 
                //the scrollableheight - if it is, ScrollToEnd().
                else
                {
                    grid.ScrollIntoView(e.Row.Item);

                    var scrollViewer = grid.FindChildByType<GridViewScrollViewer>();
                    if (scrollViewer.ScrollableHeight <= (scrollViewer.VerticalOffset + e.Row.ActualHeight))
                        scrollViewer.ScrollToEnd();
                }
            }
        }


        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.MouseRightButtonUp += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnRowRightClick);
            AssociatedObject.AddHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(OnRowClick).MakeWeak<MouseButtonEventHandler>(), true);
            AssociatedObject.RowDetailsVisibilityChanged += WeakDelegate.MakeWeak<EventHandler<GridViewRowDetailsEventArgs>>(VisibilityChanged);
            _clickTimer.Tick += _handler.MakeWeak();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.MouseRightButtonUp -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnRowRightClick);
            AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(OnRowClick).MakeWeak<MouseButtonEventHandler>());
            AssociatedObject.RowDetailsVisibilityChanged -= WeakDelegate.MakeWeak<EventHandler<GridViewRowDetailsEventArgs>>(VisibilityChanged);
            _clickTimer.Tick -= _handler.MakeWeak();
        }
    }
}
