﻿using Soaf.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadGridViewLambdaRowFilterBehavior : Behavior<RadGridView>
    {
        FilterDescriptor<object> _filterDescriptor;

        public static readonly DependencyProperty FilteringCallbackProperty = DependencyProperty
            .Register("FilteringCallback", typeof(Func<object, object, bool>), typeof(RadGridViewLambdaRowFilterBehavior));

        public static readonly DependencyProperty ContextProperty = DependencyProperty
            .Register("Context", typeof(object), typeof(RadGridViewLambdaRowFilterBehavior),
                      new PropertyMetadata(ContextPropertyChangedCallback));

        /// <summary>
        /// We subscribe to NotifyPropertyChanged on Context. Properties listed here will force refiltering of the associated grid
        /// </summary>
        public static readonly DependencyProperty ContextPropertiesToRefilterOnProperty = DependencyProperty
            .Register("ContextPropertiesToRefilterOn", typeof(IList), typeof(RadGridViewLambdaRowFilterBehavior));

        public RadGridViewLambdaRowFilterBehavior()
        {
            ContextPropertiesToRefilterOn = new List<string>();
        }

        /// <summary>
        /// Item filtering callback. 
        /// Parameter 1: context
        /// Parameter 2: item to filter
        /// Result: true/false
        /// </summary>
        public Func<object, object, bool> FilteringCallback
        {
            get { return (Func<object, object, bool>)GetValue(FilteringCallbackProperty); }
            set { SetValue(FilteringCallbackProperty, value); }
        }

        public object Context
        {
            get { return GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }

        public IList ContextPropertiesToRefilterOn
        {
            get { return (IList)GetValue(ContextPropertiesToRefilterOnProperty); }
            set { SetValue(ContextPropertiesToRefilterOnProperty, value); }
        }

        protected override void OnAttached()
        {
            // Init filter
            _filterDescriptor = new FilterDescriptor<object>
            {
                FilteringExpression = itemToFilter => FilteringCallback(Context, itemToFilter)
            };

            AssociatedObject.FilterDescriptors.Add(_filterDescriptor);

            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            // Remove filter descriptor
            if (_filterDescriptor != null)
            {
                AssociatedObject.FilterDescriptors.Remove(_filterDescriptor);
            }

            base.OnDetaching();
        }

        void NotifyContextPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            // Ensure its the property we need
            if (ContextPropertiesToRefilterOn == null
                || !ContextPropertiesToRefilterOn.Contains(propertyChangedEventArgs.PropertyName))
            {
                return;
            }

            // Trigger re-filtering
            // TODO: Telerik again broke this: AssociatedObject.FilterDescriptors.Reset(); ??
            AssociatedObject.FilterDescriptors.Remove(_filterDescriptor);
            AssociatedObject.FilterDescriptors.Add(_filterDescriptor);
        }

        static void ContextPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (RadGridViewLambdaRowFilterBehavior) d;
            var oldValueNotify = e.OldValue as INotifyPropertyChanged;
            var newValueNotify = e.NewValue as INotifyPropertyChanged;

            if (oldValueNotify != null)
            {
                oldValueNotify.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(instance.NotifyContextPropertyChanged);
            }

            if (newValueNotify != null)
            {
                newValueNotify.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(instance.NotifyContextPropertyChanged);
            }
        }
    }
}