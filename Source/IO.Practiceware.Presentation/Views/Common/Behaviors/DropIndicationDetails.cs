﻿using System.Collections;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class DropIndicationDetails : ViewModelBase
    {
        private object _currentDraggedItem;
        private DropPosition _currentDropPosition;
        private object _currentDraggedOverItem;
        private IList _draggedItemSourceCollection;

        public object CurrentDraggedOverItem
        {
            get
            {
                return _currentDraggedOverItem;
            }
            set
            {
                if (_currentDraggedOverItem != value)
                {
                    _currentDraggedOverItem = value;
                    OnPropertyChanged("CurrentDraggedOverItem");
                }
            }
        }

        public int DropIndex { get; set; }

        public DropPosition CurrentDropPosition
        {
            get
            {
                return _currentDropPosition;
            }
            set
            {
                if (_currentDropPosition != value)
                {
                    _currentDropPosition = value;
                    OnPropertyChanged("CurrentDropPosition");
                }
            }
        }

        public object CurrentDraggedItem
        {
            get
            {
                return _currentDraggedItem;
            }
            set
            {
                if (_currentDraggedItem != value)
                {
                    _currentDraggedItem = value;
                    OnPropertyChanged("CurrentDraggedItem");
                }
            }
        }

        public IList DraggedItemSourceCollection
        {
            get { return _draggedItemSourceCollection; }
            set
            {
                if (_draggedItemSourceCollection != value)
                {
                    _draggedItemSourceCollection = value;
                    OnPropertyChanged("DraggedItemSourceCollection");
                }
            }
        }

        public override string ToString()
        {
            return string.Empty;
        }
    }
}