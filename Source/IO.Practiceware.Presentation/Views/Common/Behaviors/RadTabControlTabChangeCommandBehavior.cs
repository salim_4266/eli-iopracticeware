﻿using Soaf.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadTabControlTabChangeCommandBehavior : Behavior<RadTabControl>
    {
        static readonly DependencyPropertyKey TabChangeCommandsKey =
            DependencyProperty.RegisterReadOnly(
                "TabChangeCommands", 
                typeof(FreezableCollection<TabChangeCommand>), 
                typeof(RadTabControlTabChangeCommandBehavior), null);

        public static readonly DependencyProperty TabChangeCommandsProperty =
            TabChangeCommandsKey.DependencyProperty;

        public FreezableCollection<TabChangeCommand> TabChangeCommands
        {
            get { return (FreezableCollection<TabChangeCommand>)GetValue(TabChangeCommandsProperty); }
        }

        public RadTabControlTabChangeCommandBehavior()
        {
            SetValue(TabChangeCommandsKey, new FreezableCollection<TabChangeCommand>());
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<RadSelectionChangedEventHandler>(AssociatedObjectOnSelectionChanged);
        }
        
        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<RadSelectionChangedEventHandler>(AssociatedObjectOnSelectionChanged);
        }

        void AssociatedObjectOnSelectionChanged(object sender, RadSelectionChangedEventArgs radSelectionChangedEventArgs)
        {
            // Find command for index
            var selectedTabIndex = AssociatedObject.SelectedIndex;
            var actionForTabSelection = TabChangeCommands.FirstOrDefault(p => p.TabIndex == selectedTabIndex);
            var tabItem = AssociatedObject.SelectedItem as RadTabItem;

            // Execute if found
            if (actionForTabSelection != null && tabItem != null)
            {
                // TODO: replace with CommandParameter when binding works
                actionForTabSelection.Command.Execute(tabItem.DataContext);
            }
        }
    }

    public class TabChangeCommand : DependencyObject
    {
        public static readonly DependencyProperty TabIndexProperty = DependencyProperty
            .Register(
                "TabIndex",
                typeof (int),
                typeof(TabChangeCommand));

        public static readonly DependencyProperty CommandProperty = DependencyProperty
            .Register(
                "Command",
                typeof (ICommand),
                typeof(TabChangeCommand));

        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty
            .Register(
                "CommandParameter",
                typeof (object),
                typeof (TabChangeCommand));

        /// <summary>
        /// Index of tab
        /// </summary>
        public int TabIndex
        {
            get { return (int)GetValue(TabIndexProperty); }
            set { SetValue(TabIndexProperty, value); }
        }

        /// <summary>
        /// Action to execute when tab is selected
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Command parameter
        /// TODO: find out why it doesn't work. For now replaced by reading DataContext
        /// </summary>
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
    }
}
