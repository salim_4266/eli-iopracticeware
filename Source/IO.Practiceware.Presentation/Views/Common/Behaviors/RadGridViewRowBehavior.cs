﻿using System.Linq;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadGridViewRowBehavior : Behavior<RadGridView>
    {
        public static readonly DependencyProperty RowEditEndedCommandProperty = DependencyProperty
            .Register("RowEditEndedCommand", typeof(ICommand), typeof(RadGridViewRowBehavior));

        public static readonly DependencyProperty RowValidatingCommandProperty = DependencyProperty
            .Register("RowValidatingCommand", typeof(ICommand), typeof(RadGridViewRowBehavior));

        public static readonly DependencyProperty CellValidatingCommandProperty = DependencyProperty
           .Register("CellValidatingCommand", typeof(ICommand), typeof(RadGridViewRowBehavior));

        public static readonly DependencyProperty CellValidatedCommandProperty = DependencyProperty
          .Register("CellValidatedCommand", typeof(ICommand), typeof(RadGridViewRowBehavior));

        public static readonly DependencyProperty CheckCanEditRowProperty = DependencyProperty
            .Register("CheckCanEditRow", typeof(Func<object, bool>), typeof(RadGridViewRowBehavior));

        public static readonly DependencyProperty NewRowItemFactoryProperty =
            DependencyProperty.Register("NewRowItemFactory", typeof(Func<object>), typeof(RadGridViewRowBehavior), new PropertyMetadata(default(Func<object>)));

        public static readonly DependencyProperty DataErrorCommandProperty = DependencyProperty
            .Register("DataErrorCommand", typeof(ICommand), typeof(RadGridViewRowBehavior));

        public Func<object> NewRowItemFactory
        {
            get { return (Func<object>)GetValue(NewRowItemFactoryProperty); }
            set { SetValue(NewRowItemFactoryProperty, value); }
        }

        public ICommand RowEditEndedCommand
        {
            get { return GetValue(RowEditEndedCommandProperty) as ICommand; }
            set { SetValue(RowEditEndedCommandProperty, value); }
        }

        public ICommand RowValidatingCommand
        {
            get { return GetValue(RowValidatingCommandProperty) as ICommand; }
            set { SetValue(RowValidatingCommandProperty, value); }
        }

        public ICommand CellValidatingCommand
        {
            get { return GetValue(CellValidatingCommandProperty) as ICommand; }
            set { SetValue(CellValidatingCommandProperty, value); }
        }

        public ICommand CellValidatedCommand
        {
            get { return GetValue(CellValidatedCommandProperty) as ICommand; }
            set { SetValue(CellValidatedCommandProperty, value); }
        }

        public ICommand DataErrorCommand
        {
            get { return GetValue(DataErrorCommandProperty) as ICommand; }
            set { SetValue(DataErrorCommandProperty, value); }
        }

        /// <summary>
        /// Function which decides whether row cells can be edited. First parameter is DataContext
        /// </summary>
        public Func<object, bool> CheckCanEditRow
        {
            get { return GetValue(CheckCanEditRowProperty) as Func<object, bool>; }
            set { SetValue(CheckCanEditRowProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.AddingNewDataItem += WeakDelegate.MakeWeak<EventHandler<GridViewAddingNewEventArgs>>(OnAddingNewDataItem);
            AssociatedObject.RowEditEnded += WeakDelegate.MakeWeak<EventHandler<GridViewRowEditEndedEventArgs>>(OnRowEditEnded);
            AssociatedObject.RowValidating += WeakDelegate.MakeWeak<EventHandler<GridViewRowValidatingEventArgs>>(OnRowValidating);
            AssociatedObject.CellValidating += WeakDelegate.MakeWeak<EventHandler<GridViewCellValidatingEventArgs>>(OnCellValidating);
            AssociatedObject.BeginningEdit += WeakDelegate.MakeWeak<EventHandler<GridViewBeginningEditRoutedEventArgs>>(OnBeginningEdit);
            AssociatedObject.DataError += WeakDelegate.MakeWeak<EventHandler<DataErrorEventArgs>>(OnDataError);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.AddingNewDataItem -= WeakDelegate.MakeWeak<EventHandler<GridViewAddingNewEventArgs>>(OnAddingNewDataItem);
            AssociatedObject.RowEditEnded -= WeakDelegate.MakeWeak<EventHandler<GridViewRowEditEndedEventArgs>>(OnRowEditEnded);
            AssociatedObject.RowValidating -= WeakDelegate.MakeWeak<EventHandler<GridViewRowValidatingEventArgs>>(OnRowValidating);
            AssociatedObject.CellValidating -= WeakDelegate.MakeWeak<EventHandler<GridViewCellValidatingEventArgs>>(OnCellValidating);
            AssociatedObject.CellValidated -= WeakDelegate.MakeWeak<EventHandler<GridViewCellValidatedEventArgs>>(OnCellValidated);
            AssociatedObject.BeginningEdit -= WeakDelegate.MakeWeak<EventHandler<GridViewBeginningEditRoutedEventArgs>>(OnBeginningEdit);
            AssociatedObject.DataError -= WeakDelegate.MakeWeak<EventHandler<DataErrorEventArgs>>(OnDataError);
        }

        private void OnBeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            if (CheckCanEditRow != null)
            {
                e.Cancel = !CheckCanEditRow(e.Row.DataContext);
            }
        }

        private void OnCellValidating(object sender, GridViewCellValidatingEventArgs e)
        {
            if (CellValidatingCommand != null)
            {
                // copy values to custom Args object
                var args = new CellValidatingCommandArgs(e.Row.Item, e.NewValue, e.OldValue, e.Cell.Column.UniqueName);

                // execute custom command
                CellValidatingCommand.Execute(args);

                e.ErrorMessage = args.ErrorMessage ?? e.ErrorMessage;
                e.Handled = args.Handled;                
                e.IsValid = args.IsValid;
            }
        }

        private void OnCellValidated(object sender, GridViewCellValidatedEventArgs e)
        {
            if (CellValidatedCommand != null)
            {
                var args = new CellValidatedCommandArgs(e.Cell.ParentRow.Item) { PropertyName = e.Cell.Column.UniqueName };

                // execute custom command
                CellValidatedCommand.Execute(args);

                e.ValidationResult.ErrorMessage = args.ErrorMessage;
                e.ValidationResult.PropertyName = args.PropertyName;
                e.Handled = true;                
            }
        }

        private void OnRowValidating(object sender, GridViewRowValidatingEventArgs e)
        {
            if (RowValidatingCommand != null)
            {
                // copy values to custom Args object
                var args = new RowValidatingCommandArgs(e.OldValues, e.EditOperationType.ToString(), e.Row.DataContext);

                // execute custom command 
                RowValidatingCommand.Execute(args);

                // copy any validation results provided by the custom command to this EventArg's validationResults
                args.ValidationResults.ForEach(vr => e.ValidationResults.Add(new GridViewCellValidationResult { ErrorMessage = vr.ErrorMessage, PropertyName = vr.MemberNames.FirstOrDefault() }));

                // copy the IsValid property back over
                e.IsValid = args.IsValid;
            }
        }

        private void OnRowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            if (RowEditEndedCommand != null)
            {
                // copy values to custom Args object
                var args = new RowEditEndedArgs
                               {
                                   DataContext = e.Row.DataContext,
                                   EditedItem = e.EditedItem,
                                   RowCommitted = e.EditAction == GridViewEditAction.Commit,
                                   OldValues = e.OldValues,
                                   EditAction = e.EditAction.ToString(),
                                   EditOperationType = e.EditOperationType.ToString()
                               };

                // execute custom command
                RowEditEndedCommand.Execute(args);

                // copy any values that may have potentially changed back to the EventArgs
                e.Handled = args.Handled;

                if (args.RowIsValid && args.TryRefreshGridView)
                {
                    foreach (var cell in e.Row.Cells)
                    {
                        var gridViewCell = cell as GridViewCell;
                        if (gridViewCell != null)
                        {
                            gridViewCell.HasValidationErrors = false;
                            gridViewCell.SetValue(GridViewCell.ErrorsProperty, null);
                        }
                    }
                }
            }
        }

        private void OnDataError(object sender, DataErrorEventArgs e)
        {
            if (DataErrorCommand != null)
            {
                // copy values to custom Args object
                var args = new DataErrorArgs(e.Exception);
                args.KeepRowInEditMode = e.KeepRowInEditMode;

                // execute custom command
                DataErrorCommand.Execute(e);

                // copy any values that may have potentially changed back to the EventArgs
                e.KeepRowInEditMode = args.KeepRowInEditMode;
                e.Handled = args.Handled;
            }
        }

        private void OnAddingNewDataItem(object sender, GridViewAddingNewEventArgs e)
        {
            if (NewRowItemFactory != null)
            {
                e.NewObject = NewRowItemFactory();
            }
        }
    }
}
