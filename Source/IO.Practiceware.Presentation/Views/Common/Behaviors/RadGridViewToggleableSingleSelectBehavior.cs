﻿using Soaf.ComponentModel;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public static class RadGridViewToggleableSingleSelectBehavior
    {
        public static readonly DependencyProperty EnableToggleableSingleSelectProperty = DependencyProperty.RegisterAttached("EnableToggleableSingleSelect", typeof(bool), typeof(RadGridView), new PropertyMetadata(false, OnEnableToggleableSingleSelectPropertyChanged));

        private static void OnEnableToggleableSingleSelectPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var gridView = d as RadGridView;
            if (gridView == null || !(e.NewValue is bool)) { return; }

            if ((bool)e.NewValue)
            {
                gridView.SelectionMode = SelectionMode.Multiple;
                gridView.SelectionChanged += new EventHandler<SelectionChangeEventArgs>(OnGridViewSelectionChanged).MakeWeak();
            }
        }

        static void OnGridViewSelectionChanged(object sender, SelectionChangeEventArgs e)
        {
            var gridView = sender as RadGridView;
            if (gridView != null)
            {
                if (gridView.SelectedItems.Count > 1)
                {
                    var latestAddedItem = e.AddedItems.First();
                    gridView.SelectedItems.Clear();
                    gridView.SelectedItems.Add(latestAddedItem);
                }
            }
        }

        public static void SetEnableToggleableSingleSelect(RadGridView gridView, bool value)
        {
            gridView.SetValue(EnableToggleableSingleSelectProperty, value);
        }
    }
}
