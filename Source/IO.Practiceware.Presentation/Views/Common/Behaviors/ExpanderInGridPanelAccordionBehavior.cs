﻿using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class ExpanderInGridPanelAccordionBehavior : Behavior<Expander>
    {
        DependencyPropertyDescriptor _isExpandedEventSubscription;

        protected override void OnAttached()
        {
            base.OnAttached();

            _isExpandedEventSubscription = DependencyPropertyDescriptor.FromProperty(Expander.IsExpandedProperty, typeof(Expander));
            _isExpandedEventSubscription.AddValueChanged(AssociatedObject, OnIsExpandedChanged);

            AssociatedObject.Loaded += WeakDelegate.MakeWeak<RoutedEventHandler>(OnAssociatedObjectLoaded);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _isExpandedEventSubscription.RemoveValueChanged(AssociatedObject, OnIsExpandedChanged);

            AssociatedObject.Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnAssociatedObjectLoaded);
        }

        void OnAssociatedObjectLoaded(object sender, RoutedEventArgs e)
        {
            // Ensure at least one expander is selected
            OnIsExpandedChanged(sender, e);
        }

        void OnIsExpandedChanged(object sender, EventArgs eventArgs)
        {
            List<Expander> otherExpanders;
            Grid gridParent;
            int associatedObjectGridColumnIndex;

            // Find parent grid and then other expanders depending on visual tree structure
            var parent = VisualTreeHelper.GetParent(AssociatedObject);
            var contentPresenter = parent as ContentPresenter;
            var isInsideContentPresenter = contentPresenter != null;
            if (isInsideContentPresenter)
            {
                associatedObjectGridColumnIndex = Grid.GetColumn(contentPresenter);
                gridParent = (Grid)VisualTreeHelper.GetParent(contentPresenter);
                otherExpanders = gridParent.Children
                    .OfType<ContentPresenter>()
                    .Where(cp => VisualTreeHelper.GetChildrenCount(cp) > 0)
                    .Select(cp => VisualTreeHelper.GetChild(cp, 0))
                    .OfType<Expander>()
                    .Where(e => !Equals(e, AssociatedObject))
                    .ToList();
            }
            else
            {
                associatedObjectGridColumnIndex = Grid.GetColumn(AssociatedObject);
                gridParent = (Grid) parent;
                otherExpanders = gridParent.Children
                    .OfType<Expander>()
                    .Where(e => !Equals(e, AssociatedObject))
                    .ToList();
            }

            if (AssociatedObject.IsExpanded)
            {
                // Ensure others are not expanded
                otherExpanders
                    .Where(e => e.IsExpanded)
                    .ToList()
                    .ForEach(p => p.IsExpanded = false);

                // TODO: add support for accordion orientation and use RowDefinitions instead of column definitions
                // Ensure our column is set to Star where others are set to Auto
                for (int index = 0; index < gridParent.ColumnDefinitions.Count; index++)
                {
                    var columnDefinition = gridParent.ColumnDefinitions[index];
                    columnDefinition.Width = new GridLength(columnDefinition.Width.Value, 
                        index == associatedObjectGridColumnIndex ? GridUnitType.Star : GridUnitType.Auto);
                }
            }
            else
            {
                // Ensure one is opened
                var expandedOne = otherExpanders.FirstOrDefault(e => e.IsExpanded);
                if (expandedOne == null)
                {
                    AssociatedObject.IsExpanded = true;
                }
            }
        }
    }
}