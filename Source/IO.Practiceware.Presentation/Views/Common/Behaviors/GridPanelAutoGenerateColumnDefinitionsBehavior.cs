﻿using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class GridPanelAutoGenerateColumnDefinitionsBehavior : Behavior<Grid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.LayoutUpdated += WeakDelegate.MakeWeak<EventHandler>(AssociatedObjectOnLayoutUpdated);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.LayoutUpdated -= WeakDelegate.MakeWeak<EventHandler>(AssociatedObjectOnLayoutUpdated);
        }

        void AssociatedObjectOnLayoutUpdated(object sender, EventArgs eventArgs)
        {
            // If count is the same -> exit
            if (AssociatedObject.Children.Count == AssociatedObject.ColumnDefinitions.Count) return;

            // Generate new column definitions
            AssociatedObject.ColumnDefinitions.Clear();
            for (int columnIndex = 0; columnIndex < AssociatedObject.Children.Count; columnIndex++)
            {
                AssociatedObject.ColumnDefinitions.Add(new ColumnDefinition
                    {
                        Width = new GridLength(1, GridUnitType.Auto)
                    });
            }

            // Update column index
            for (int i = 0; i < AssociatedObject.Children.Count; i++)
            {
                var child = (FrameworkElement)AssociatedObject.Children[i];
                Grid.SetColumn(child, i);
            }
        }
    }
}
