﻿using IO.Practiceware.Presentation.ViewModels.Common.TelerikChart;
using IO.Practiceware.Presentation.Views.GlaucomaMonitor;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interactivity;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ChartView;
using CollectionExtensions = Telerik.Windows.Controls.CollectionExtensions;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Enumeration of values representing the shapes that a dataset may take
    /// </summary>
    public enum DataSetShape
    {
        Circle,
        Rectangle,
        Diamond
    }

    /// <summary>
    /// Enumeration of values representing the line styles that a dataset may take
    /// </summary>
    public enum DataSetLineStyle
    {
        Solid,
        SmallDash,
        LargeDash
    }

    /// <summary>
    /// Behavior to generate a dynamic number of LineSeries whose points and lines that cross, will displayed as 'merged'.
    /// </summary>
    public class RadCartesianChartDynamicLineSeriesBehavior : Behavior<RadCartesianChart>
    {
        #region DependencyProperty: CombinedShapeBorderColor

        public Color? CombinedShapeBorderColor
        {
            get { return GetValue(CombinedShapeBorderColorProperty) as Color?; }
            set { SetValue(CombinedShapeBorderColorProperty, value); }
        }

        public static readonly DependencyProperty CombinedShapeBorderColorProperty = DependencyProperty.Register("CombinedShapeBorderColor", typeof(Color?), typeof(RadCartesianChartDynamicLineSeriesBehavior));

        #endregion

        #region DependencyProperty: CombinedShapeFillColor

        public Color? CombinedShapeFillColor
        {
            get { return GetValue(CombinedShapeFillColorProperty) as Color?; }
            set { SetValue(CombinedShapeFillColorProperty, value); }
        }

        public static readonly DependencyProperty CombinedShapeFillColorProperty = DependencyProperty.Register("CombinedShapeFillColor", typeof(Color?), typeof(RadCartesianChartDynamicLineSeriesBehavior));

        #endregion

        #region DependencyProperty: ItemsSource

        public IEnumerable<object> ItemsSource
        {
            get { return GetValue(ItemsSourceProperty) as IEnumerable<DatesItem>; }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable<object>), typeof(RadCartesianChartDynamicLineSeriesBehavior), new PropertyMetadata(null, OnItemsSourcePropertyChanged));

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (RadCartesianChartDynamicLineSeriesBehavior)d;

            var oldCollectionChanged = e.OldValue as INotifyPropertyChanged;
            if (oldCollectionChanged != null)
            {
                oldCollectionChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddLineSeriesFromProperty);
            }

            var newCollectionChanged = e.NewValue as INotifyPropertyChanged;
            if (newCollectionChanged != null)
            {
                newCollectionChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.AddLineSeriesFromProperty);
            }

            behavior.AddLineSeriesFromProperty(behavior, null);
        }

        private void AddLineSeriesFromProperty(object sender, PropertyChangedEventArgs e)
        {
            if (AssociatedObject == null || ItemsSource == null) return;

            var lineSeries = AssociatedObject.Series;
            lineSeries.Clear();

            if (!ItemsSource.Any()) return;

            #region temporary funcs to map the temporary viewmodels to IO viewmodels

            Func<DataLinesItem, DataPointViewModel> mapDataLinesItemToDataPointViewModel = dataLinesItem => new DataPointViewModel
            {
                PlotPoint = dataLinesItem.Point,
                ShapeBorderColor = dataLinesItem.ShapeBorderColor,
                ShapeFillColor = dataLinesItem.ShapeFillColor
            };

            Func<DataSetsItem, DataSetViewModel> mapDataSetsItemToDataSet = dataSetsItem => new DataSetViewModel
            {
                LineStyle = dataSetsItem.LineStyle,
                OrdinalId = dataSetsItem.OrdinalId,
                Shape = dataSetsItem.Shape,
                DataPoints = dataSetsItem.DataLines.Select(mapDataLinesItemToDataPointViewModel).ToExtendedObservableCollection()
            };

            Func<MedicationsItem, MedicationViewModel> mapMedicationsItemToMedicationViewModel = medicationsItem => new MedicationViewModel
            {
                Frequency = medicationsItem.Frequency,
                IsNew = medicationsItem.IsNew,
                Name = medicationsItem.Name,
                Qualifier = medicationsItem.Qualifier
            };

            Func<TestsItem, TestViewModel> mapTestsItemToTestViewModel = testsItem => new TestViewModel
            {
                FilePath = testsItem.FilePath,
                Type = testsItem.Type
            };

            Func<ProceduresItem, ProcedureViewModel> mapProceduresItemsToProcedureViewModel = proceduresItem => new ProcedureViewModel
                {
                    FilePath = proceduresItem.FilePath,
                    Qualifier = proceduresItem.Qualifier,
                    Type = proceduresItem.Type
                };

            Func<DatesItem, DateColumnViewModel> mapDatesItemToDateViewModelColumn = datesItem => new DateColumnViewModel
            {
                DataSets = datesItem.DataSets.Select(mapDataSetsItemToDataSet).ToExtendedObservableCollection(),
                Date = datesItem.Date,
                HeaderPlotPoint = MaxChartValue,
                Visibility = datesItem.Visibility,
                Medications = datesItem.Medications.Select(mapMedicationsItemToMedicationViewModel).ToExtendedObservableCollection(),
                Tests = datesItem.Tests.Select(mapTestsItemToTestViewModel).ToExtendedObservableCollection(),
                Procedures = datesItem.Procedures.Select(mapProceduresItemsToProcedureViewModel).ToExtendedObservableCollection()
            };

            #endregion

            var localDates = ItemsSource.ToArray();
            var dateViewModels = localDates.OfType<DateColumnViewModel>().ToList();
            dateViewModels.AddRange(localDates.OfType<DatesItem>().Select(mapDatesItemToDateViewModelColumn));

            lineSeries.Add(CreateHeaderLineSeries(dateViewModels));
            CollectionExtensions.AddRange(lineSeries, CreateLineSeries(dateViewModels));
        }

        #endregion

        private DataTemplate _headerPointTemplate;
        private DataTemplate _circlePointTemplate;
        private DataTemplate _rectanglePointTemplate;
        private DataTemplate _diamondPointTemplate;
        private PropertyChangeNotifier _verticalAxisNotifier;
        private const double MaxChartValue = 100;

        protected override void OnAttached()
        {
            base.OnAttached();

            InitializeStylesFromResourceDictionary();
            AddLineSeriesFromProperty(this, null);
            _verticalAxisNotifier = new PropertyChangeNotifier(AssociatedObject, RadCartesianChart.VerticalAxisProperty)
                .AddValueChanged(OnVerticalAxisChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _verticalAxisNotifier.RemoveValueChanged(OnVerticalAxisChanged);
        }

        private void OnVerticalAxisChanged(object sender, EventArgs e)
        {
            ((LinearAxis)AssociatedObject.VerticalAxis).Maximum = MaxChartValue;

            _verticalAxisNotifier.RemoveValueChanged(OnVerticalAxisChanged);
        }

        private void InitializeStylesFromResourceDictionary()
        {
            const string resourceDictionarySource = "IO.Practiceware.Presentation;component/Views/GlaucomaMonitor/ChartResources.xaml";
            var resourceDictionary = new ResourceDictionary { Source = new Uri(resourceDictionarySource, UriKind.Relative) };

            Func<string, DataTemplate> getDataTemplate = templateName =>
                {
                    var dataTemplate = (DataTemplate)resourceDictionary[templateName];
                    if (dataTemplate == null) throw new Exception("Could not locate {0} at resource dictionary {1}".FormatWith(templateName, resourceDictionarySource));
                    return dataTemplate;
                };

            _headerPointTemplate = getDataTemplate("HeaderPointTemplate");
            _circlePointTemplate = getDataTemplate("CirclePointTemplate");
            _rectanglePointTemplate = getDataTemplate("RectanglePointTemplate");
            _diamondPointTemplate = getDataTemplate("DiamondPointTemplate");
        }

        #region LineSeries builder functions

        /// <summary>
        /// Builds DataPointViewModelProxies keyed by OrdinalId, LineStyle, Shape and Color.
        /// Each Key represents a LineSeries while the DataPointViewModelProxy value represents an item in the LineSeries' respective ItemsSource.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private Dictionary<LineSeriesKey, DataPointViewModelProxy> BuildLineSeriesItemsSource(DateColumnViewModel date)
        {
            var result = new Dictionary<LineSeriesKey, DataPointViewModelProxy>();

            var dateTime = date.Date;
            foreach (var dataSetGroupedByLineStyleAndShape in date.DataSets.GroupBy(ds => new { ds.OrdinalId, ds.LineStyle, ds.Shape }))
            {
                foreach (var dataLinesGroupedByPoint in dataSetGroupedByLineStyleAndShape.SelectMany(i => i.DataPoints).GroupBy(dl => dl.PlotPoint))
                {
                    Color borderColor;
                    Color fillColor;

                    DataPointViewModelProxy dataPoint;

                    if (dataLinesGroupedByPoint.Count() > 1)
                    {
                        borderColor = CombinedShapeBorderColor ?? dataLinesGroupedByPoint.Select(dl => dl.ShapeBorderColor).Distinct().CombineColors();
                        fillColor = CombinedShapeFillColor ?? dataLinesGroupedByPoint.Select(dl => dl.ShapeFillColor).Distinct().CombineColors();
                        dataPoint = DataPointViewModelProxy.CreateNew(dateTime, dataLinesGroupedByPoint.First().PlotPoint, borderColor, fillColor, true);
                    }
                    else
                    {
                        borderColor = dataLinesGroupedByPoint.First().ShapeBorderColor;
                        fillColor = dataLinesGroupedByPoint.First().ShapeFillColor;
                        dataPoint = DataPointViewModelProxy.CreateNew(dateTime, dataLinesGroupedByPoint.First().PlotPoint, borderColor, fillColor);
                    }

                    var key = new LineSeriesKey
                        {
                            OrdinalId = dataSetGroupedByLineStyleAndShape.Key.OrdinalId,
                            LineStyle = dataSetGroupedByLineStyleAndShape.Key.LineStyle,
                            Shape = dataSetGroupedByLineStyleAndShape.Key.Shape,
                            Color = borderColor
                        };

                    result.Add(key, dataPoint);
                }
            }
            return result;
        }

        /// <summary>
        /// Builds complete ItemsSources for all LineSeries.
        /// This also does calculations to link combined LineSeries to matching non-combined LineSeries.
        /// </summary>
        /// <remarks>
        /// A matching non-combined LineSeries to a combined LineSeries are those that share the same key but do not share the same color.
        /// This includes OrdinalId, LineStyle, and Shape.
        /// </remarks>
        /// <param name="dates"></param>
        /// <returns></returns>
        private Dictionary<LineSeriesKey, IList<DataPointViewModelProxy>> BuildAllLineSeriesItemsSources(IEnumerable<DateColumnViewModel> dates)
        {
            var result = new Dictionary<LineSeriesKey, IList<DataPointViewModelProxy>>();

            DateTime previousDate = default(DateTime);

            foreach (var date in dates.OrderBy(d => d.Date))
            {
                foreach (var itemsSource in BuildLineSeriesItemsSource(date))
                {
                    var localItemsSource = itemsSource;
                    var dataPointsToAdd = new List<DataPointViewModelProxy>(new[] { localItemsSource.Value });

                    // If the current ItemsSource to add is not a combined DataPointViewModel, 
                    // and the previous DataPoint (by date) is a combined DataPointViewModel,
                    // then Add the previous combined DataPointViewModel to the current LineSeries ItemsSource
                    if (!localItemsSource.Value.IsCombinedDataPointViewModel)
                    {
                        DateTime localPreviousDate = previousDate;

                        // A matching non-combined LineSeries to a combined LineSeries are those that share the same key but do not share the same color.
                        var matchingItemsSource = result.Where(kvp => kvp.Key.IsEqualToAndIgnoreColor(localItemsSource.Key)).ToArray();
                        if (matchingItemsSource.Any())
                        {
                            var previousDateDataPoints = matchingItemsSource.SelectMany(i => i.Value).Where(dp => dp.Date == localPreviousDate).ToArray();
                            if (previousDateDataPoints.Any())
                            {
                                if (previousDateDataPoints.Any(dp => dp.IsCombinedDataPointViewModel))
                                {
                                    dataPointsToAdd.Add(previousDateDataPoints.First(dp => dp.IsCombinedDataPointViewModel));
                                }
                                else if (previousDateDataPoints.Any(dp => dp.ShapeBorderColor == localItemsSource.Value.ShapeBorderColor))
                                {
                                    dataPointsToAdd.Add(previousDateDataPoints.First(dp => dp.ShapeBorderColor == localItemsSource.Value.ShapeBorderColor));
                                }
                            }
                        }
                    }

                    if (result.ContainsKey(localItemsSource.Key))
                    {
                        dataPointsToAdd = dataPointsToAdd.Where(dp => !result[localItemsSource.Key].Contains(dp)).ToList();
                        CollectionExtensions.AddRange(result[localItemsSource.Key], dataPointsToAdd);
                    }
                    else
                    {
                        result.Add(localItemsSource.Key, new List<DataPointViewModelProxy>(dataPointsToAdd));
                    }
                }

                previousDate = date.Date;
            }

            return result;
        }

        private IEnumerable<LineSeries> CreateLineSeries(IEnumerable<DateColumnViewModel> dateColumns)
        {
            var newLineSeries = new List<LineSeries>();
            var localDateColumns = dateColumns.ToArray();

            var itemsSources = BuildAllLineSeriesItemsSources(localDateColumns).ToArray();
            var ordinalIdToRangeMap = new OrdinalIdToRangeMap(MaxChartValue - 30);
            ordinalIdToRangeMap.InitializeMap(localDateColumns.SelectMany(d => d.DataSets).Select(ds => ds.OrdinalId));

            foreach (var grouping in itemsSources.GroupBy(i => new { i.Key.OrdinalId, i.Key.LineStyle, i.Key.Shape }))
            {
                ExtrapolatePlotPointsByOrdinalId(grouping.SelectMany(g => g.Value).ToArray(), grouping.Key.OrdinalId, ordinalIdToRangeMap);
            }

            foreach (var itemsSourceKeyedByLineSeriesKey in itemsSources)
            {
                var dataPoints = itemsSourceKeyedByLineSeriesKey.Value.ToExtendedObservableCollection();
                var lineStyle = itemsSourceKeyedByLineSeriesKey.Key.LineStyle;
                var shape = itemsSourceKeyedByLineSeriesKey.Key.Shape;

                var dateTimes = localDateColumns.OrderBy(d => d.Date).Select(d => d.Date);
                var contiguousDataPoints = new List<DataPointViewModelProxy>();
                foreach (var date in dateTimes)
                {
                    var localDate = date;
                    var matchingDataPoints = dataPoints.Where(dp => dp.Date == localDate).ToArray();
                    if (matchingDataPoints.Any())
                    {
                        contiguousDataPoints.AddRange(matchingDataPoints);
                    }
                    else if (contiguousDataPoints.Any())
                    {
                        newLineSeries.AddRange(CreateLineSeriesByLineStyleAndShape(contiguousDataPoints.ToExtendedObservableCollection(), lineStyle, shape));
                        contiguousDataPoints.Clear();
                    }
                }
                if (contiguousDataPoints.Any())
                {
                    newLineSeries.AddRange(CreateLineSeriesByLineStyleAndShape(contiguousDataPoints.ToExtendedObservableCollection(), lineStyle, shape));
                }
            }

            return newLineSeries;
        }

        private static void ExtrapolatePlotPointsByOrdinalId(IEnumerable<DataPointViewModelProxy> dataPoints, int ordinalId, OrdinalIdToRangeMap ordinalToRangeMap)
        {
            var localDataPoints = dataPoints.ToArray();

            var minDataPointValue = localDataPoints.Aggregate((a, b) => a.PlotPoint < b.PlotPoint ? a : b).PlotPoint;
            var maxDataPointValue = localDataPoints.Aggregate((a, b) => a.PlotPoint > b.PlotPoint ? a : b).PlotPoint;
            var dataPointRange = maxDataPointValue - minDataPointValue;

            var minAndMaxOfChartSection = ordinalToRangeMap.GetMinAndMax(ordinalId);
            // Give a buffer of '10' between sections where the max value is 100
            var maxOfChartSection = minAndMaxOfChartSection.Item2 - 10;
            // Give a buffer of '3' to the very bottom of the chart so the symbols do not display off screen
            var minOfChartSection = minAndMaxOfChartSection.Item1.Equals(0) ? 3 : minAndMaxOfChartSection.Item1;
            var chartSectionRange = maxOfChartSection - minOfChartSection;

            foreach (var datapoint in localDataPoints)
            {
                datapoint.ExtrapolatedPlotPoint = ((chartSectionRange * (datapoint.PlotPoint - minDataPointValue)) / (dataPointRange)) + minOfChartSection;
            }

            OffsetOverlappingPlotPoints(localDataPoints, maxOfChartSection);
        }

        private static void OffsetOverlappingPlotPoints(IEnumerable<DataPointViewModelProxy> dataPoints, double maxPlotPointValue)
        {
            const double lambda = 8;
            var localDataPoints = dataPoints.ToArray();

            // Group the data points by category (Column/DateTime)
            foreach (var datapointsGroupedByDate in localDataPoints.OrderBy(dp => dp.Date).GroupBy(dp => dp.Date))
            {
                // Order the data points by plot points
                var localDataPointsGroupedByDate = datapointsGroupedByDate.OrderBy(dp => dp.ExtrapolatedPlotPoint).ToList();

                // Compare the position of the current plot point to the next
                while (localDataPointsGroupedByDate.Count > 1)
                {
                    var lowerPoint = localDataPointsGroupedByDate[0];
                    var higherPoint = localDataPointsGroupedByDate[1];
                    if (lowerPoint.IsCombinedDataPointViewModel && higherPoint.IsCombinedDataPointViewModel)
                    {
                        localDataPointsGroupedByDate.Remove(lowerPoint);
                        continue;
                    }

                    var lowerPlotPoint = lowerPoint.ExtrapolatedPlotPoint;
                    var higherPlotPoint = higherPoint.ExtrapolatedPlotPoint;

                    // If the position of the current plot point compared to the next is less then the lambda,
                    // adjust the positions of each where the new difference is equal to the lambda
                    if (Math.Abs(lowerPlotPoint - higherPlotPoint) < lambda)
                    {
                        // The following determines if the higher plot point WITH offset exceeds the maximum height of the chart section.
                        // If so, the higher plot point will be set to the maximum height of the chart section
                        // and the lower plot point will be lowered by the remainder of the offset
                        var offset = lambda - Math.Abs(lowerPlotPoint - higherPlotPoint);
                        if (higherPlotPoint + offset > maxPlotPointValue)
                        {
                            var remainderOffset = higherPlotPoint + offset - maxPlotPointValue;
                            higherPlotPoint = maxPlotPointValue;
                            lowerPlotPoint -= remainderOffset;
                        }
                        else
                        {
                            higherPlotPoint += offset;
                        }

                        lowerPoint.ExtrapolatedPlotPoint = lowerPlotPoint;
                        higherPoint.ExtrapolatedPlotPoint = higherPlotPoint;
                    }
                    localDataPointsGroupedByDate.Remove(lowerPoint);
                }
            }
        }

        private LineSeries CreateHeaderLineSeries(IEnumerable<DateColumnViewModel> dates)
        {
            var transparentBrush = new SolidColorBrush(Colors.Transparent);
            var lineSeries = new LineSeriesWithNoAutomationPeer
                {
                    Background = transparentBrush,
                    Stroke = transparentBrush,
                    StrokeThickness = 0,
                    IsHitTestVisible = true,
                    Visibility = Visibility.Visible,
                    ItemsSource = dates,
                    PointTemplate = _headerPointTemplate,
                    CategoryBinding = new PropertyNameDataPointBinding(DateColumnViewModel.CategoryBindingPropertyName),
                    ValueBinding = new PropertyNameDataPointBinding(DateColumnViewModel.ValueBindingPropertyName),
                };
            return lineSeries;
        }

        private IEnumerable<LineSeries> CreateLineSeriesByLineStyleAndShape(IList<DataPointViewModelProxy> itemsSource, DataSetLineStyle lineStyle, DataSetShape shape)
        {
            #region helper func getPointTemplate

            Func<DataTemplate> getPointTemplate = () =>
                {
                    switch (shape)
                    {
                        case DataSetShape.Circle:
                            return _circlePointTemplate;
                        case DataSetShape.Diamond:
                            return _diamondPointTemplate;
                        case DataSetShape.Rectangle:
                            return _rectanglePointTemplate;
                        default:
                            throw new Exception("No PointTemplate defined for {0}".FormatWith(shape));
                    }
                };

            #endregion

            SolidColorBrush lineBrush;
            if (itemsSource.Count == 1)
            {
                lineBrush = new SolidColorBrush(itemsSource.First().ShapeBorderColor);
            }
            else
            {
                // If all the data points are combined data points, they all share the same color - so take the color of the first item.
                // If there is a mix of combined data points and regular data points in the items source, do not take the combined color - take the color of the regular data point.
                lineBrush = itemsSource.All(i => i.IsCombinedDataPointViewModel) 
                                ? new SolidColorBrush(itemsSource.First().ShapeBorderColor)
                                : new SolidColorBrush(itemsSource.First(i => !i.IsCombinedDataPointViewModel).ShapeBorderColor);
            }

            var lineSeriesForLine = new LineSeriesWithNoAutomationPeer
                {
                    DashArray = lineStyle.GetDashArray(),
                    Stroke = lineBrush,
                    StrokeThickness = 4,
                    ItemsSource = itemsSource,
                    CategoryBinding = new PropertyNameDataPointBinding(DataPointViewModelProxy.CategoryBindingPropertyName),
                    ValueBinding = new PropertyNameDataPointBinding(DataPointViewModelProxy.ValueBindingPropertyName),
                };

            var lineSeriesForPlotPoints = new LineSeriesWithNoAutomationPeer
                {
                    Stroke = new SolidColorBrush(Colors.Transparent),
                    PointTemplate = getPointTemplate(),
                    ItemsSource = itemsSource,
                    CategoryBinding = new PropertyNameDataPointBinding(DataPointViewModelProxy.CategoryBindingPropertyName),
                    ValueBinding = new PropertyNameDataPointBinding(DataPointViewModelProxy.ValueBindingPropertyName),
                };

            lineSeriesForLine.SetVisibilityBinding(shape.GetVisibilityBindingElementName());
            lineSeriesForPlotPoints.SetVisibilityBinding(shape.GetVisibilityBindingElementName());

            return new[] { lineSeriesForLine, lineSeriesForPlotPoints };
        }

        #endregion

        #region helper nested classes

        /// <summary>
        /// Proxy for the DataPointViewModel that handles the value of the ExtrapolatedPlotPoint and whether the DataPoint is a combined DataPoint.
        /// </summary>
        /// <remarks>
        /// The ExtrapolatedPlotPoint is calculated by using the OrdinalId and the Original PlotPoint.
        /// A Combined DataPointViewModel represents 2 or more DataPointViewModels that share the same LineSeriesKey and the same PlotPoint.
        /// </remarks>
        public class DataPointViewModelProxy : DataPointViewModel
        {
            public const string CategoryBindingPropertyName = "Date";
            public const string ValueBindingPropertyName = "ExtrapolatedPlotPoint";

            /// <summary>
            /// Protected constructor so creation of this class is inhibited by the general user.
            /// </summary>
            /// <remarks>
            /// The regular DataPointViewModel is meant to be public for general use, while this class contains calculated information
            /// based on the regular, public DataPointViewModels.  (Data such as ExtrapolatedPlotPoint and whether the proxy represents a combination of multiple DataPointViewModels.)
            /// </remarks>
            protected DataPointViewModelProxy()
            {
            }

            public static DataPointViewModelProxy CreateNew(DateTime date, double plotPoint, Color borderColor, Color fillColor, bool isCombinedDataPoint = false)
            {
                return new DataPointViewModelProxy
                    {
                        Date = date,
                        PlotPoint = plotPoint,
                        ShapeBorderColor = borderColor,
                        ShapeFillColor = fillColor,
                        IsCombinedDataPointViewModel = isCombinedDataPoint
                    };
            }

            /// <summary>
            /// Gets or sets the Extrapolated PlotPoint
            /// </summary>
            /// <remarks>
            /// The ExtrapolatedPlotPoint is calculated by using the OrdinalId and the Original PlotPoint.
            /// </remarks>
            public virtual double ExtrapolatedPlotPoint { get; set; }

            public virtual DateTime Date { get; set; }

            /// <summary>
            /// Gets or sets whether this proxy represents a combined DataPointViewModel.
            /// </summary>
            /// <remarks>
            /// A Combined DataPointViewModel represents 2 or more DataPointViewModels that share the same LineSeriesKey and the same PlotPoint.
            /// </remarks>
            public virtual bool IsCombinedDataPointViewModel { get; set; }
        }

        /// <summary>
        /// Class to help determine the range of values a given ordinal ID may take.
        /// </summary>
        private class OrdinalIdToRangeMap
        {
            private readonly double _maxChartValue;
            private double _rangeOfSection;
            private readonly Dictionary<int, double> _sectionMinValueKeyedByOrdinalId;

            public OrdinalIdToRangeMap(double maxChartValue)
            {
                _maxChartValue = maxChartValue;
                _sectionMinValueKeyedByOrdinalId = new Dictionary<int, double>();
            }

            public void InitializeMap(IEnumerable<int> ordinalIds)
            {
                var localOrdinalIds = ordinalIds.Distinct().OrderBy(i => i).ToArray();
                if (localOrdinalIds.Any())
                {
                    var numberOfSections = localOrdinalIds.Count();
                    _rangeOfSection = _maxChartValue / numberOfSections;

                    for (int i = 0; i < numberOfSections; i++)
                    {
                        var minValueOfSection = i * _rangeOfSection;
                        _sectionMinValueKeyedByOrdinalId.Add(localOrdinalIds[i], minValueOfSection);
                    }
                }
            }

            /// <summary>
            /// Gets the minimum and maximum range of values for the given OrdinalId.
            /// </summary>
            /// <param name="ordinalId"></param>
            /// <returns></returns>
            public Tuple<double, double> GetMinAndMax(int ordinalId)
            {
                var minValue = _sectionMinValueKeyedByOrdinalId[ordinalId];
                var maxValue = minValue + _rangeOfSection;
                return new Tuple<double, double>(minValue, maxValue);
            }
        }

        /// <summary>
        /// Each unique LineSeriesKey represents a LineSeries in the chart.
        /// Keyed by OrdinalId, LineStyle, Shape, and Color.
        /// </summary>
        public class LineSeriesKey
        {
            public int OrdinalId { get; set; }
            public DataSetLineStyle LineStyle { get; set; }
            public DataSetShape Shape { get; set; }
            public Color Color { get; set; }

            public bool IsEqualToAndIgnoreColor(LineSeriesKey other)
            {
                return OrdinalId.Equals(other.OrdinalId) && LineStyle == other.LineStyle && Shape == other.Shape;
            }

            public override bool Equals(object obj)
            {
                var other = obj as LineSeriesKey;
                
                if (ReferenceEquals(this, other)) return true;
                if (ReferenceEquals(null, other)) return false;

                return OrdinalId.Equals(other.OrdinalId)
                       && Color == other.Color
                       && Shape == other.Shape
                       && LineStyle == other.LineStyle;
            }

            public override int GetHashCode()
            {
                return OrdinalId.GetHashCode()
                       ^ Color.GetHashCode()
                       ^ Shape.GetHashCode()
                       ^ LineStyle.GetHashCode();
            }
        }

        #endregion
    }

    #region extension methods

    public static class LineSeriesExtensions
    {
        /// <summary>
        /// Helper method to set the visibility binding to whether the given elementName 'IsChecked'.
        /// </summary>
        /// <param name="lineSeries"></param>
        /// <param name="elementName"></param>
        public static void SetVisibilityBinding(this LineSeries lineSeries, string elementName)
        {
            const string bindingPath = "IsChecked";

            var visibilityBinding = new Binding(bindingPath)
                {
                    ElementName = elementName,
                    Converter = new BooleanToVisibilityConverter()
                };
            BindingOperations.SetBinding(lineSeries, UIElement.VisibilityProperty, visibilityBinding);
        }

        /// <summary>
        /// Helper method to translate the DataSetLineStyle to a DoubleCollection representing the actual DashArray value.
        /// </summary>
        /// <param name="lineStyle"></param>
        /// <returns></returns>
        public static DoubleCollection GetDashArray(this DataSetLineStyle lineStyle)
        {
            switch (lineStyle)
            {
                case DataSetLineStyle.LargeDash:
                    return new DoubleCollection { 4 };
                case DataSetLineStyle.SmallDash:
                    return new DoubleCollection { 1 };
                case DataSetLineStyle.Solid:
                    return null;
                default:
                    throw new Exception("No DashArray defined for {0}".FormatWith(lineStyle));
            }
        }

        /// <summary>
        /// Helper method to get the ElementName for use in binding the visibility of a DataSetShape.
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        public static string GetVisibilityBindingElementName(this DataSetShape shape)
        {
            switch (shape)
            {
                case DataSetShape.Circle:
                    return "ChartIOP";
                case DataSetShape.Diamond:
                    return "ChartCD";
                case DataSetShape.Rectangle:
                    return "ChartVSC";
                default:
                    throw new Exception("No Visibility Binding ElementName defined for {0}".FormatWith(shape));
            }
        }

        /// <summary>
        /// Helper method to add a collection of colors together.
        /// </summary>
        /// <param name="colors"></param>
        /// <returns></returns>
        public static Color CombineColors(this IEnumerable<Color> colors)
        {
            Color resultColor = default(Color);
            foreach (var color in colors)
            {
                resultColor = Color.Add(resultColor, color);
            }

            return resultColor;
        }
    }

    #endregion

    /// <summary>
    /// Basic LineSeries class with no support for AutomationPeer.
    /// </summary>
    /// <remarks>
    /// AutomationPeer is used for WPF unit testing.  This is an innocuous change
    /// that fixes a problem that occurrs in telerik's implementation of LineSeriesAutomationPeer
    /// when using the above behavior from VB6.
    /// </remarks>
    public class LineSeriesWithNoAutomationPeer : LineSeries
    {
        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            return null;
        }
    }
}