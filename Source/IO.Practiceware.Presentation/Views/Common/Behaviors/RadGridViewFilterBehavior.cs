﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadGridViewFilterBehavior : Behavior<RadGridView>
    {
        private readonly Lazy<CustomFilterDescriptor> _textBoxFilterDescriptor;
        private readonly NotifyCollectionChangedEventHandler _onFilterDescriptorsCollectionChanged;
        private readonly TextChangedEventHandler _onTextBoxTextChanged;
        private readonly EventHandler<GridViewFilteredEventArgs> _onAssociatedObjectFiltered;
        private readonly PropertyChangedEventHandler _onTextBoxFilterDescriptorChanged;

        private bool _isSyncingFilters;

        public static readonly DependencyProperty TextBoxProperty = DependencyProperty
            .Register("TextBox", typeof(TextBox), typeof(RadGridViewFilterBehavior), new PropertyMetadata(OnTextBoxPropertyChanged));

        public static readonly DependencyProperty FilterDescriptorsProperty = DependencyProperty
            .Register("FilterDescriptors", typeof(FilterDescriptorCollection), typeof(RadGridViewFilterBehavior), new PropertyMetadata(OnFilterDescriptorsChanged));

        public RadGridViewFilterBehavior()
        {
            _onFilterDescriptorsCollectionChanged = WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnFilterDescriptorsCollectionChanged);
            _onTextBoxTextChanged = WeakDelegate.MakeWeak<TextChangedEventHandler>(OnTextBoxTextChanged);
            _onAssociatedObjectFiltered = WeakDelegate.MakeWeak<EventHandler<GridViewFilteredEventArgs>>(OnAssociatedObjectFiltered);
            _onTextBoxFilterDescriptorChanged = WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnTextBoxFilterDescriptorChanged);
            // Columns are not yet added when behavior is attached, so lazy create the filter
            _textBoxFilterDescriptor = new Lazy<CustomFilterDescriptor>(() =>
            {
                var filterDescriptor = new CustomFilterDescriptor(AssociatedObject.Columns);
                AssociatedObject.FilterDescriptors.Add(filterDescriptor);
                filterDescriptor.PropertyChanged += _onTextBoxFilterDescriptorChanged;
                return filterDescriptor;
            });
        }

        private void OnTextBoxFilterDescriptorChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FilterValue") SyncFilters(AssociatedObject.FilterDescriptors, FilterDescriptors);
        }

        private static void OnFilterDescriptorsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (RadGridViewFilterBehavior)d;

            var value = e.OldValue as FilterDescriptorCollection;
            if (value != null)
            {
                value.CollectionChanged -= behavior._onFilterDescriptorsCollectionChanged;
            }

            value = e.NewValue as FilterDescriptorCollection;
            if (value != null)
            {
                value.CollectionChanged += behavior._onFilterDescriptorsCollectionChanged;
            }

            behavior.SyncFilters(value, behavior.AssociatedObject != null ? behavior.AssociatedObject.FilterDescriptors : null);
            behavior.SyncFilters(behavior.AssociatedObject != null ? behavior.AssociatedObject.FilterDescriptors : null, value);
        }

        private void OnFilterDescriptorsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (AssociatedObject == null) return;

            var source = ReferenceEquals(sender, FilterDescriptors) ? FilterDescriptors : AssociatedObject.FilterDescriptors;
            var target = ReferenceEquals(sender, FilterDescriptors) ? AssociatedObject.FilterDescriptors : FilterDescriptors;

            SyncFilters(source, target);
        }

        private void SyncFilters(FilterDescriptorCollection source, FilterDescriptorCollection target)
        {
            if (_isSyncingFilters) return;
            _isSyncingFilters = true;
            try
            {
                if (FilterDescriptors == null) FilterDescriptors = new CompositeFilterDescriptorCollection();

                if (source != null && target != null)
                {
                    foreach (var filter in source.Where(f => !target.Contains(f)).ToArray())
                    {
                        target.Add(filter);
                    }
                    foreach (var filter in target.Where(f => !source.Contains(f) && (!_textBoxFilterDescriptor.IsValueCreated || !ReferenceEquals(f, _textBoxFilterDescriptor.Value))).ToArray())
                    {
                        target.Remove(filter);
                    }
                }

                // force DataBinding
                if (ReferenceEquals(target, FilterDescriptors)) FilterDescriptors = target;
            }
            finally
            {
                _isSyncingFilters = false;
            }
        }

        public FilterDescriptorCollection FilterDescriptors
        {
            get { return (FilterDescriptorCollection)GetValue(FilterDescriptorsProperty); }
            set { SetValue(FilterDescriptorsProperty, value); }
        }

        public TextBox TextBox
        {
            get { return (TextBox)GetValue(TextBoxProperty); }
            set { SetValue(TextBoxProperty, value); }
        }

        public static void OnTextBoxPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (RadGridViewFilterBehavior)dependencyObject;

            var tb = e.OldValue as TextBox;
            if (tb != null)
            {
                tb.TextChanged -= behavior._onTextBoxTextChanged;
            }

            tb = e.NewValue as TextBox;
            if (tb != null)
            {
                tb.TextChanged += behavior._onTextBoxTextChanged;
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Filtered += _onAssociatedObjectFiltered;

            if (AssociatedObject.FilterDescriptors.Count > 0)
            {
                SyncFilters(AssociatedObject.FilterDescriptors, FilterDescriptors);
            }
            else
            {
                SyncFilters(FilterDescriptors, AssociatedObject.FilterDescriptors);
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.Filtered -= _onAssociatedObjectFiltered;

            if (TextBox != null)
            {
                TextBox.TextChanged -= _onTextBoxTextChanged;
            }

            if (_textBoxFilterDescriptor.IsValueCreated)
            {
                AssociatedObject.FilterDescriptors.Remove(_textBoxFilterDescriptor.Value);
                _textBoxFilterDescriptor.Value.PropertyChanged -= _onTextBoxFilterDescriptorChanged;
            }

            base.OnDetaching();
        }

        private void OnAssociatedObjectFiltered(object sender, GridViewFilteredEventArgs e)
        {
            SyncFilters(AssociatedObject.FilterDescriptors, FilterDescriptors);
        }

        private void OnTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox == null) return;
            Dispatcher.Invoke(() => _textBoxFilterDescriptor.Value.FilterValue = TextBox.Text);
        }

        public class CustomFilterDescriptor : FilterDescriptorBase
        {
            private readonly CompositeFilterDescriptor _compositeFilterDescriptor;
            private static readonly ConstantExpression TrueExpression = System.Linq.Expressions.Expression.Constant(true);
            private string _filterValue;
            private readonly ICommand _updateFilterValue;

            public CompositeFilterDescriptor CompositeFilterDescriptor
            {
                get { return _compositeFilterDescriptor; }
            }

            public CustomFilterDescriptor(IEnumerable<Telerik.Windows.Controls.GridViewColumn> columns)
            {
                _compositeFilterDescriptor = new CompositeFilterDescriptor();
                _compositeFilterDescriptor.LogicalOperator = FilterCompositionLogicalOperator.Or;

                foreach (GridViewDataColumn column in columns.OfType<GridViewDataColumn>().Where(c => c.IsFilterable))
                {
                    _compositeFilterDescriptor.FilterDescriptors.Add(CreateFilterForColumn(column));
                }

                _updateFilterValue = Command.Create(() => Dispatcher.Invoke(() => OnPropertyChanged("FilterValue"))).Delay(1000);
            }

            public string FilterValue
            {
                get
                {
                    return _filterValue;
                }
                set
                {
                    if (_filterValue != value)
                    {
                        _filterValue = value;
                        UpdateCompositeFilterValues();
                        _updateFilterValue.Execute();
                    }
                }
            }

            protected override System.Linq.Expressions.Expression CreateFilterExpression(ParameterExpression parameterExpression)
            {
                if (string.IsNullOrEmpty(FilterValue))
                {
                    return TrueExpression;
                }
                try
                {
                    return _compositeFilterDescriptor.CreateFilterExpression(parameterExpression);
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                }

                return TrueExpression;
            }

            private IFilterDescriptor CreateFilterForColumn(GridViewDataColumn column)
            {
                if (column.DataType == null) { column.DataType = typeof(string); }
                FilterOperator filterOperator = GetFilterOperatorForType(column.DataType);
                var descriptor = new FilterDescriptor(column.DataMemberBinding.Path.Path.Replace(".", string.Empty), filterOperator, _filterValue);
                descriptor.MemberType = column.DataType;

                return descriptor;
            }

            private static FilterOperator GetFilterOperatorForType(Type dataType)
            {
                return dataType == typeof(string) ? FilterOperator.Contains : FilterOperator.IsEqualTo;
            }

            private void UpdateCompositeFilterValues()
            {
                foreach (FilterDescriptor descriptor in _compositeFilterDescriptor.FilterDescriptors)
                {
                    if (descriptor.MemberType == null) continue;

                    var convertedValue = ConvertFilterValue(FilterValue, descriptor.MemberType);
                    if (descriptor.MemberType.IsAssignableFrom(typeof(DateTime)))
                    {
                        DateTime date;
                        if (DateTime.TryParse(FilterValue, out date))
                        {
                            descriptor.Value = date;
                        }
                        else
                        {
                            descriptor.Value = OperatorValueFilterDescriptorBase.UnsetValue;
                        }
                    }
                    else
                    {
                        descriptor.Value = convertedValue;
                    }
                }
            }

            [DebuggerNonUserCode]
            private static object ConvertFilterValue(string filterValue, Type targetType)
            {
                if (targetType == typeof(string)) return filterValue;

                if (targetType.IsAssignableFrom(typeof(DateTime)))
                {
                    DateTime date;
                    if (DateTime.TryParse(filterValue, out date)) { return date; }
                }
                else if (targetType.IsAssignableFrom(typeof(TimeSpan)))
                {
                    TimeSpan time;
                    if (TimeSpan.TryParse(filterValue, out time)) { return time; }
                }

                object convertedValue;
                try
                {
                    convertedValue = Convert.ChangeType(filterValue, targetType, CultureInfo.CurrentCulture);
                }
                catch
                {
                    convertedValue = OperatorValueFilterDescriptorBase.UnsetValue;
                }
                return convertedValue;
            }
        }
    }

    /// <summary>
    /// Converts a CompositeFilterDescriptorCollection to a SQL compatible string format.
    /// </summary>
    public class CompositeFilterDescriptorCollectionToStringConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(System.IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as CompositeFilterDescriptorCollection;
            if (collection != null) return Convert(collection);

            var s = value as string;
            if (string.IsNullOrEmpty(s)) return new CompositeFilterDescriptorCollection();

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as CompositeFilterDescriptorCollection;
            if (collection != null) return Convert(collection);

            var s = value as string;
            if (string.IsNullOrEmpty(s)) return new CompositeFilterDescriptorCollection();

            return value;
        }

        private string Convert(CompositeFilterDescriptorCollection collection)
        {
            if (collection.Count == 0) return null;

            return Convert(collection.LogicalOperator, collection);
        }

        private string Convert(FilterCompositionLogicalOperator @operator, IEnumerable<IFilterDescriptor> collection)
        {
            return "({0})".FormatWith(collection.Select(Convert).WhereNotDefault().Join(" {0} ".FormatWith(@operator.ToString().ToUpper())));
        }

        private string Convert(IFilterDescriptor f)
        {
            var compositeFilterDescriptor = f as CompositeFilterDescriptor;
            if (compositeFilterDescriptor != null)
            {
                return Convert(compositeFilterDescriptor.LogicalOperator, compositeFilterDescriptor.FilterDescriptors);
            }

            var memberColumnFilterDescriptor = f as MemberColumnFilterDescriptor;
            if (memberColumnFilterDescriptor != null)
            {
                return Convert(memberColumnFilterDescriptor);
            }

            var customTextFilter = f as RadGridViewFilterBehavior.CustomFilterDescriptor;
            if (customTextFilter != null)
            {
                return Convert(customTextFilter.CompositeFilterDescriptor);
            }

            var filterDescriptor = f as FilterDescriptor;
            if (filterDescriptor != null)
            {
                return Convert(filterDescriptor, filterDescriptor.Member);
            }

            return null;
        }

        private static string Convert(MemberColumnFilterDescriptor f)
        {
            var cfd = (IColumnFilterDescriptor)f;

            if (!cfd.IsActive) return null;

            var result = new StringBuilder();

            if (cfd.DistinctFilter != null && cfd.DistinctFilter.IsActive)
            {
                var distinctFilter = Convert(cfd.DistinctFilter, f.Member);
                if (distinctFilter != null) result.Append(distinctFilter);
            }
            if (cfd.FieldFilter != null && cfd.FieldFilter.IsActive && (cfd.FieldFilter.Filter1.IsActive || cfd.FieldFilter.Filter2.IsActive))
            {
                var fieldField = Convert(cfd.FieldFilter, f.Member);
                if (fieldField != null && !string.IsNullOrEmpty(result.ToString()))
                {
                    result.Append(" AND ");
                    result.Append(fieldField);
                }
                else
                {
                    result.Append(fieldField);
                }
            }

            return "({0})".FormatWith(result);
        }

        private static string Convert(IDistinctValuesFilterDescriptor f, string member)
        {
            string result = null;
            switch (f.DistinctValuesComparisonOperator)
            {
                case FilterOperator.IsEqualTo:
                    result = "{0} IN ({1})".FormatWith(member, f.DistinctValues.Select(x => "'" + x + "'").Join());
                    break;
                case FilterOperator.Contains:
                    result = f.DistinctValues.Select(v => "{0} LIKE '%{1}%'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.DoesNotContain:
                    result = f.DistinctValues.Select(v => "{0} NOT LIKE '%{1}%'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.EndsWith:
                    result = f.DistinctValues.Select(v => "{0} LIKE '{1}%'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsContainedIn:
                    result = "{0} IN ({1})".FormatWith(member, f.DistinctValues.Select(x => "'" + x + "'").Join());
                    break;
                case FilterOperator.IsEmpty:
                    result = f.DistinctValues.Select(v => "({0} = '' OR {0} IS NULL)".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsGreaterThan:
                    result = f.DistinctValues.Select(v => "{0} > '{1}'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsGreaterThanOrEqualTo:
                    result = f.DistinctValues.Select(v => "{0} >= '{1}'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsLessThan:
                    result = f.DistinctValues.Select(v => "{0} < '{1}'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsLessThanOrEqualTo:
                    result = f.DistinctValues.Select(v => "{0} <= '{1}'".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsNotContainedIn:
                    result = "{0} NOT IN ({1})".FormatWith(member, f.DistinctValues.Select(x => "'" + x + "'").Join());
                    break;
                case FilterOperator.IsNotEmpty:
                    result = f.DistinctValues.Select(v => "{0} IS NOT NULL".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.IsNull:
                    result = f.DistinctValues.Select(v => "{0} IS NULL".FormatWith(member, v)).Join(" OR ");
                    break;
                case FilterOperator.StartsWith:
                    result = f.DistinctValues.Select(v => "{0} LIKE '%{1}'".FormatWith(member, v)).Join(" OR ");
                    break;
            }
            result = "({0})".FormatWith(result);

            return result;
        }

        private static string Convert(IFieldFilterDescriptor f, string member)
        {
            return "({0})".FormatWith(new[] { f.Filter1, f.Filter2 }
                .Where(x => x.IsActive).Select(x => Convert(x, member))
                .WhereNotDefault()
                .Join(" {0} ".FormatWith(f.LogicalOperator.ToString().ToUpper())));
        }

        private static string Convert(OperatorValueFilterDescriptorBase f, string member)
        {
            switch (f.Operator)
            {
                case FilterOperator.IsEqualTo:
                    return "{0} IN ('{1}')".FormatWith(member, f.Value);
                case FilterOperator.Contains:
                    return "{0} LIKE '%{1}%'".FormatWith(member, f.Value);
                case FilterOperator.DoesNotContain:
                    return "{0} NOT LIKE '%{1}%'".FormatWith(member, f.Value);
                case FilterOperator.EndsWith:
                    return "{0} LIKE '{1}%'".FormatWith(member, f.Value);
                case FilterOperator.IsContainedIn:
                    return "{0} IN '({1})'".FormatWith(member, f.Value);
                case FilterOperator.IsEmpty:
                    return "{0} = ''".FormatWith(member, f.Value);
                case FilterOperator.IsGreaterThan:
                    return "{0} > '{1}'".FormatWith(member, f.Value);
                case FilterOperator.IsGreaterThanOrEqualTo:
                    return "{0} >= '{1}'".FormatWith(member, f.Value);
                case FilterOperator.IsLessThan:
                    return "{0} < '{1}'".FormatWith(member, f.Value);
                case FilterOperator.IsLessThanOrEqualTo:
                    return "{0} <= '{1}'".FormatWith(member, f.Value);
                case FilterOperator.IsNotContainedIn:
                    return "{0} NOT IN ('{1}')".FormatWith(member, f.Value);
                case FilterOperator.IsNotEmpty:
                    return "{0} IS NOT NULL".FormatWith(member, f.Value);
                case FilterOperator.IsNull:
                    return "{0} IS NULL".FormatWith(member, f.Value);
                case FilterOperator.StartsWith:
                    return "{0} LIKE '%{1}'".FormatWith(member, f.Value);
                default:
                    return null;
            }
        }

    }
}
