﻿using System;
using System.Globalization;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    ///     Custom  Date/Time parsing for the RadDateTimePicker control.
    /// </summary>
    public class RadDateTimePickerParserBehavior : Behavior<RadDateTimePicker>
    {

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.ParseMode = Telerik.Windows.Controls.Input.ParseMode.Always;
            AssociatedObject.ParseExact = true;
            AssociatedObject.ParseDateTimeValue += new RadDateTimePicker.ParseDateTimeEventHandler(OnParseDateTimeValue).MakeWeak<RadDateTimePicker.ParseDateTimeEventHandler>();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.ParseDateTimeValue -= new RadDateTimePicker.ParseDateTimeEventHandler(OnParseDateTimeValue).MakeWeak<RadDateTimePicker.ParseDateTimeEventHandler>();
        }

        private static void OnParseDateTimeValue(object sender, ParseDateTimeEventArgs args)
        {
            DateTime result;

            if (ParseText(args.TextToParse, out result))
            {
                args.Result = result;
                args.IsParsingSuccessful = true;
            }
        }

        private static bool ParseText(string text, out DateTime result)
        {
            return DateTime.TryParseExact(text.Trim(), DateTimes.DateFormats, CultureInfo.CurrentUICulture, DateTimeStyles.AssumeLocal, out result);
        }
    }
}