﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Primitives;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// A behavior to attach to RadAutoCompleteBoxes where the ItemsSource loads in the background.  Attach this to any RadAutoCompleteBox to force the UI to select the first 
    /// item when the user enters search text while the ItemsSource is loading.  
    /// 
    /// Once the ItemsSource finishes loading (in the UI, this is noted by the dropdown list appearing), this behavior will set the highlighted item to the first item in the dropdown,
    /// ensuring that a value is selected (note - we'll still need to wait for the drop down list to appear)
    /// </summary>
    class RadAutoCompleteBoxBackgroundLoadingBehavior : Behavior<RadAutoCompleteBox>
    {

        public static readonly DependencyProperty SelectionChangedCommandProperty = DependencyProperty.Register("SelectionChangedCommand", typeof(ICommand), typeof(RadAutoCompleteBoxBackgroundLoadingBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand SelectionChangedCommand
        {
            get { return GetValue(SelectionChangedCommandProperty) as ICommand; }
            set { SetValue(SelectionChangedCommandProperty, value); }
        }
        
        protected override void OnAttached()
        {
            
            AssociatedObject.TargetUpdated += new EventHandler<DataTransferEventArgs>(TargetUpdated).MakeWeak<EventHandler<DataTransferEventArgs>>();
            AssociatedObject.PreviewKeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(OnKeyDown);
            base.OnAttached();
        }

        void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (SelectionChangedCommand != null)
                {
                   SelectionChangedCommand.Execute(AssociatedObject.SearchText);
                }
            }
            
        }

        /// <summary>
        /// For the following code to work properly, set UpdateTargetSource to true only on the ItemsSource binding in xaml.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TargetUpdated(object sender, DataTransferEventArgs e)
        {
            if (AssociatedObject.SelectionMode == AutoCompleteSelectionMode.Multiple) return;
            var itemsSource = AssociatedObject.ItemsSource as IEnumerable<object>;
            var filteredItems = AssociatedObject.FilteredItems as IEnumerable<object>;
            var selectedItem = AssociatedObject.SelectedItem;
            if (filteredItems != null)
            {
                var filteredItemsList = filteredItems as IList<object> ?? filteredItems.ToList();
                if (itemsSource != null && selectedItem == null 
                    && AssociatedObject.SearchText != "" && filteredItemsList.Count() < itemsSource.Count() && AssociatedObject.IsDropDownOpen)
                {
                    if (Keyboard.PrimaryDevice != null)
                    {
                        if (Keyboard.PrimaryDevice.ActiveSource != null)
                        {
                            var keydown = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, Key.Down)
                                          {
                                              RoutedEvent = Keyboard.KeyDownEvent
                                          };
                            InputManager.Current.ProcessInput(keydown);
                        }
                    }

                    //this should only need to be called once, so once it's called, and the above code is ran, detatch it.
                    AssociatedObject.TargetUpdated -= new EventHandler<DataTransferEventArgs>(TargetUpdated).MakeWeak<EventHandler<DataTransferEventArgs>>();

                }
            }
        }


        protected override void OnDetaching()
        {
            AssociatedObject.TargetUpdated -= new EventHandler<DataTransferEventArgs>(TargetUpdated).MakeWeak<EventHandler<DataTransferEventArgs>>();
            AssociatedObject.PreviewKeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(OnKeyDown);
            base.OnDetaching();
        }
    }
}
