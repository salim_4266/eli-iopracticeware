﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// RadComboBox behavior that allows the DependencyProperty EmptyText to be italicized, while the ComboBoxItems and the "typed-in" text is normal font.
    /// </summary>
    public class RadComboBoxEmptyTextFontBehavior : Behavior<RadComboBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.SelectionChanged += OnSelectionChanged;
            AssociatedObject.DropDownClosed += OnDropDownClosed;
            AssociatedObject.DropDownOpened += OnDropDownOpened;
            AssociatedObject.KeyDown += OnKeyDown;
            base.OnAttached();
        }

        private void OnSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AssociatedObject.FontStyle = string.IsNullOrEmpty(AssociatedObject.Text) || AssociatedObject.Text == AssociatedObject.EmptyText ? FontStyles.Italic : FontStyles.Normal;
        }

        private void OnDropDownOpened(object sender, EventArgs e)
        {
            AssociatedObject.FontStyle = FontStyles.Normal;
        }

        private void OnDropDownClosed(object sender, EventArgs e)
        {
            AssociatedObject.FontStyle = string.IsNullOrEmpty(AssociatedObject.Text) || AssociatedObject.Text == AssociatedObject.EmptyText ? FontStyles.Italic : FontStyles.Normal;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(AssociatedObject.Text) &&
                e.SystemKey == Key.Delete || e.SystemKey == Key.Back || e.SystemKey == Key.Space)
            {
                AssociatedObject.FontStyle = FontStyles.Italic;
            }
            else
            {
                AssociatedObject.FontStyle = FontStyles.Normal;
            }
        }
    }
}