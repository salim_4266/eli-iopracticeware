﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadAutoCompleteBoxCustomFilterBehavior : Behavior<RadAutoCompleteBox>
    {
        private IFilteringBehavior _oldFilteringBehavior;

        /// <summary>
        /// Specifies callback to pre-filter ItemSource for search on specific field
        /// </summary>
        public static readonly DependencyProperty FilterCallbackProperty = DependencyProperty
            .Register(
                "FilterCallback", 
                typeof(Func<object, string, IList, IList>), 
                typeof(RadAutoCompleteBoxCustomFilterBehavior));

        /// <summary>
        /// An object which owns field AutoComplete is suggesting
        /// </summary>
        public static readonly DependencyProperty ContextProperty = DependencyProperty
            .Register(
                "Context",
                typeof(object),
                typeof(RadAutoCompleteBoxCustomFilterBehavior));

        public object Context
        {
            get { return GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value);}
        }

        public Func<object, string, IList, IList> FilterCallback
        {
            get { return (Func<object, string, IList, IList>)GetValue(FilterCallbackProperty); }
            set { SetValue(FilterCallbackProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.GotFocus += AssociatedObjectOnGotFocus;

            // Update filtering behavior
            _oldFilteringBehavior = AssociatedObject.FilteringBehavior;
            AssociatedObject.FilteringBehavior = new FilteringBehavior(this);

            base.OnAttached();
        }

        private void AssociatedObjectOnGotFocus(object sender, RoutedEventArgs e)
        {
            AssociatedObject.IsDropDownOpen = false;
            AssociatedObject.Populate(AssociatedObject.SearchText);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.FilteringBehavior = _oldFilteringBehavior;
            base.OnDetaching();
        }

        #region Nested type: FilteringBehavior

        private class FilteringBehavior : Telerik.Windows.Controls.FilteringBehavior
        {
            readonly RadAutoCompleteBoxCustomFilterBehavior _owner;

            public FilteringBehavior(RadAutoCompleteBoxCustomFilterBehavior owner)
            {
                _owner = owner;
            }

            public override IEnumerable<object> FindMatchingItems(string searchText, IList items, IEnumerable<object> escapedItems, string textSearchPath, TextSearchMode textSearchMode)
            {
                IEnumerable<object> result;

                if (_owner.FilterCallback != null && _owner.AssociatedObject != null)
                {
                    items = _owner.FilterCallback(_owner.Context, _owner.AssociatedObject.DisplayMemberPath, items);
                }

                if (string.IsNullOrWhiteSpace(searchText))
                {
                    result = items.OfType<object>().Except(escapedItems).ToArray();
                }
                else
                    result = base.FindMatchingItems(searchText.Trim(), items, escapedItems, textSearchPath, textSearchMode);

                return result;
            }
        }

        #endregion
    }
}
