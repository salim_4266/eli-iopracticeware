﻿using ICSharpCode.AvalonEdit;
using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class TextEditorBehavior : Behavior<TextEditor>
    {
        public static readonly DependencyProperty CodeProperty =
        DependencyProperty.Register("Code", typeof(string), typeof(TextEditorBehavior), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnCodeChanged));

        private static void OnCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((string)e.NewValue != ((TextEditorBehavior)d).AssociatedObject.Text)
            {
                ((TextEditorBehavior)d).AssociatedObject.Text = (string)e.NewValue;
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.TextChanged += new EventHandler(AssociatedObjectTextChanged).MakeWeak();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.TextChanged -= new EventHandler(AssociatedObjectTextChanged).MakeWeak();
        }

        void AssociatedObjectTextChanged(object sender, EventArgs e)
        {
            SetValue(CodeProperty, AssociatedObject.Text);
        }

        public string Code
        {
            get { return (string)GetValue(CodeProperty); }
            set { SetValue(CodeProperty, value); }
        }

    }
}
