﻿using Soaf.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Behavior that executes a command on double clicking the associated RadGridView, except when double clicking a RadGridView's popup.
    /// </summary>
    public class RadGridViewDoubleClickBehavior : Behavior<RadGridView>
    {
        public static readonly DependencyProperty DoubleClickCommandProperty = DependencyProperty.Register("DoubleClickCommand", typeof(ICommand), typeof(RadGridViewDoubleClickBehavior));
        public static readonly DependencyProperty DoubleClickCommandParameterProperty = DependencyProperty.Register("DoubleClickCommandParameter", typeof(object), typeof(RadGridViewDoubleClickBehavior));

        public ICommand DoubleClickCommand
        {
            get { return GetValue(DoubleClickCommandProperty) as ICommand; }
            set { SetValue(DoubleClickCommandProperty, value); }
        }

        public object DoubleClickCommandParameter
        {
            get { return GetValue(DoubleClickCommandParameterProperty); }
            set { SetValue(DoubleClickCommandParameterProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.MouseDoubleClick += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseDoubleClick);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseDoubleClick -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseDoubleClick);
            base.OnDetaching();
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as DependencyObject;
            if (source != null && DependencyObjectExtensions.ParentOfType<Popup>(source) != null)
            {
                e.Handled = true;
                return;
            }

            if (DoubleClickCommand != null)
            {
                DoubleClickCommand.Execute(DoubleClickCommandParameter);
            }
        }
    }
}
