﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Soaf.Reflection;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Allows a GridViewSelectColumn to trigger the "IsSelected" property on the selected/deselected row's datacontext, and vice versa.
    /// </summary>
    public class RadGridViewSelectColumnBehavior : Behavior<RadGridView>
    {
        private PropertyChangeNotifier _itemsSourceNotifier;

        public static readonly DependencyProperty IsSelectedPropertyNameProperty = DependencyProperty.Register("IsSelectedPropertyName", typeof(string), typeof(RadGridViewSelectColumnBehavior));

        protected override void OnAttached()
        {
            base.OnAttached();

            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty).AddValueChanged(OnItemsSourceChanged);
            
            InitializeSelectedRows(AssociatedObject);
            
            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<EventHandler<SelectionChangeEventArgs>>(OnRadGridViewSelectionChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourceChanged);
            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<EventHandler<SelectionChangeEventArgs>>(OnRadGridViewSelectionChanged);
        }

        private bool GetIsSelected(object item)
        {
            var isSelectedProperty = item.GetType().GetProperty(IsSelectedPropertyName);
            return (bool)isSelectedProperty.GetGetter()(item);
        }

        private void SetIsSelected(object item, bool isSelected)
        {
            var isSelectedProperty = item.GetType().GetProperty(IsSelectedPropertyName);
            isSelectedProperty.GetSetter()(item, isSelected);
        }

        private void InitializeSelectedRows(RadGridView gridView)
        {
            gridView.SelectedItems.Clear();
            if (gridView.ItemsSource != null)
            {
                gridView.SelectedItems.AddRange(gridView.ItemsSource.CastTo<IEnumerable<object>>().Where(GetIsSelected));
            }
        }

        private void OnRadGridViewSelectionChanged(object sender, SelectionChangeEventArgs e)
        {
            e.AddedItems.ToList().ForEach(i => SetIsSelected(i, true));
            e.RemovedItems.ToList().ForEach(i => SetIsSelected(i, false));
        }

        private void OnItemsSourceChanged(object sender, EventArgs e)
        {
            InitializeSelectedRows(AssociatedObject);

            var notifyCollectionChanged = AssociatedObject.ItemsSource.CastTo<INotifyCollectionChanged>();
            notifyCollectionChanged.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnItemsSourceChanged, 
                handler => notifyCollectionChanged.CollectionChanged -= handler);

            var enumerableItemsSource = AssociatedObject.ItemsSource.CastTo<IEnumerable<object>>();
            enumerableItemsSource.Cast<INotifyPropertyChanged>().ToList().ForEach(i =>
                {
                    i.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemPropertyChanged, 
                        handler => i.PropertyChanged -= handler);
                });
        }

        private void OnItemsSourceChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var newItem in e.NewItems)
            {
                if (GetIsSelected(newItem)) { AssociatedObject.SelectedItems.Add(newItem); }
            }

            foreach (var oldItem in e.OldItems)
            {
                if (GetIsSelected(oldItem)) { AssociatedObject.SelectedItems.Remove(oldItem); }
            }
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != IsSelectedPropertyName) return;

            if (GetIsSelected(sender))
            {
                if (!AssociatedObject.SelectedItems.Contains(sender))
                {
                    AssociatedObject.SelectedItems.Add(sender);
                }
            }
            else
            {
                if (AssociatedObject.SelectedItems.Contains(sender))
                {
                    AssociatedObject.SelectedItems.Remove(sender);
                }
            }
        }

        public string IsSelectedPropertyName
        {
            get { return (string)GetValue(IsSelectedPropertyNameProperty); }
            set { SetValue(IsSelectedPropertyNameProperty, value); }
        }
    }
}
