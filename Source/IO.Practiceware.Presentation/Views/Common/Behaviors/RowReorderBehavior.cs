﻿using System.Windows.Interactivity;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Provides row (item) reorder capabilities to RadGridView
    /// </summary>
    public class RowReorderBehavior : Behavior<RadGridView>
    {
        #region DependencyProperty: RowReorderedCommand

        public ICommand RowReorderedCommand
        {
            get { return GetValue(RowReorderedCommandProperty) as ICommand; }
            set { SetValue(RowReorderedCommandProperty, value); }
        }

        public static readonly DependencyProperty RowReorderedCommandProperty = DependencyProperty.Register("RowReorderedCommand", typeof(ICommand), typeof(RowReorderBehavior));

        #endregion

        #region  DependencyProperty : IsEnabled

        // Using a DependencyProperty as the backing store for IsEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(RowReorderBehavior), new FrameworkPropertyMetadata(false, OnIsEnabledPropertyChanged));

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static void OnIsEnabledPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var behavior = dependencyObject as RowReorderBehavior;
            if (behavior == null || e.NewValue == null) return;

            var isEnabledNew = e.NewValue.CastTo<bool>();
            var isEnabledOld = e.OldValue.IfNotNull(v => v.CastTo<bool>(), false);

            behavior.CleanUp();

            if (isEnabledNew && !isEnabledOld) behavior.Initialize();
        }

        #endregion

        private bool _initialized;

        protected override void OnAttached()
        {
            base.OnAttached();
            CleanUp();
            Initialize();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            CleanUp();
        }

        protected virtual void Initialize()
        {
            if (_initialized) return;

            AssociatedObject.RowLoaded += WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(AssociatedObjectRowLoaded);
            SubscribeToDragDropEvents();
            _initialized = true;
        }

        protected virtual void CleanUp()
        {
            if (!_initialized) return;

            AssociatedObject.RowLoaded -= WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(AssociatedObjectRowLoaded);
            UnsubscribeFromDragDropEvents();
            _initialized = false;
        }

        private void AssociatedObjectRowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row is GridViewHeaderRow || e.Row is GridViewNewRow || e.Row is GridViewFooterRow)
                return;

            var row = e.Row as GridViewRow;
            InitializeRowDragAndDrop(row);
        }

        private void InitializeRowDragAndDrop(GridViewRow row)
        {
            if (row == null)
                return;

            DragDropManager.RemoveDragOverHandler(row, OnRowDragOver);
            DragDropManager.AddDragOverHandler(row, OnRowDragOver);
        }

        private void SubscribeToDragDropEvents()
        {
            DragDropManager.AddDragInitializeHandler(AssociatedObject, OnDragInitialize);
            DragDropManager.AddGiveFeedbackHandler(AssociatedObject, OnGiveFeedback);
            DragDropManager.AddDropHandler(AssociatedObject, OnDrop);
        }

        private void UnsubscribeFromDragDropEvents()
        {
            DragDropManager.RemoveDragInitializeHandler(AssociatedObject, OnDragInitialize);
            DragDropManager.RemoveGiveFeedbackHandler(AssociatedObject, OnGiveFeedback);
            DragDropManager.RemoveDropHandler(AssociatedObject, OnDrop);
        }

        private void OnDragInitialize(object sender, DragInitializeEventArgs e)
        {
            var sourceRow = e.OriginalSource as GridViewRow ?? (e.OriginalSource as FrameworkElement).ParentOfType<GridViewRow>();

            if (sourceRow == null || sourceRow.Name == "PART_RowResizer") return;

            var details = new DropIndicationDetails();
            var item = sourceRow.Item;
            details.CurrentDraggedItem = item;

            var dragPayload = DragDropPayloadManager.GeneratePayload(null);

            dragPayload.SetData("DraggedItem", item);
            dragPayload.SetData("DropDetails", details);

            e.Data = dragPayload;

            // find the data template
            var template = AssociatedObject.TryFindResource("DraggedItemTemplate");
            e.DragVisual = new Telerik.Windows.DragDrop.DragVisual
            {
                Content = details,
                ContentTemplate = template as DataTemplate
            };

            e.DragVisualOffset = e.RelativeStartPoint;
            e.AllowedEffects = DragDropEffects.All;
        }

        private static void OnGiveFeedback(object sender, Telerik.Windows.DragDrop.GiveFeedbackEventArgs e)
        {
            e.SetCursor(Cursors.Arrow);
            e.Handled = true;
        }

        private void OnDrop(object sender, Telerik.Windows.DragDrop.DragEventArgs e)
        {
            var draggedItem = DragDropPayloadManager.GetDataFromObject(e.Data, "DraggedItem");
            var details = DragDropPayloadManager.GetDataFromObject(e.Data, "DropDetails") as DropIndicationDetails;

            if (details == null || draggedItem == null || details.CurrentDraggedOverItem == null)
            {
                return;
            }

            var grid = sender as RadGridView;

            if (grid == null) return;

            if (draggedItem == details.CurrentDraggedOverItem) return;

            var draggedItemRemoved = false;
            var originalIndexOfDraggedItem = -1;

            if (e.Effects == DragDropEffects.Move || e.Effects == DragDropEffects.All)
            {
                var itemSource = grid.ItemsSource as IList;
                if (itemSource != null)
                {
                    originalIndexOfDraggedItem = itemSource.IndexOf(draggedItem);
                    itemSource.Remove(draggedItem);
                    draggedItemRemoved = true;
                }
            }

            if (e.Effects != DragDropEffects.None)
            {
                var collection = grid.ItemsSource as IList;

                if (collection == null) return;

                var newIndex = collection.IndexOf(details.CurrentDraggedOverItem);

                if (details.CurrentDropPosition == DropPosition.After)
                {
                    newIndex++;
                }

                try
                {
                    collection.Insert(newIndex, draggedItem);
                }
                catch (IndexOutOfRangeException)
                {
                    if (draggedItemRemoved)
                    {
                        var count = ((ICollection)grid.ItemsSource).Count;
                        if (originalIndexOfDraggedItem == count)
                        {
                            collection.Add(draggedItem);
                        }
                        else
                        {
                            collection.Insert(originalIndexOfDraggedItem, draggedItem);
                        }
                    }
                }

                if (RowReorderedCommand != null && RowReorderedCommand.CanExecute(null))
                {
                    RowReorderedCommand.Execute(new RowReorderEventArgs { DraggedItem = draggedItem, DraggedOverItem = details.CurrentDraggedOverItem, DropIndex = newIndex, DropPosition = details.CurrentDropPosition });
                }
            }

            e.Handled = true;
        }

        private void OnRowDragOver(object sender, Telerik.Windows.DragDrop.DragEventArgs e)
        {
            var row = sender as GridViewRow;
            var details = DragDropPayloadManager.GetDataFromObject(e.Data, "DropDetails") as DropIndicationDetails;

            if (details == null || row == null)
            {
                return;
            }

            details.CurrentDraggedOverItem = row.DataContext;

            if (details.CurrentDraggedItem == details.CurrentDraggedOverItem)
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
                return;
            }

            details.CurrentDropPosition = GetDropPositionFromPoint(e.GetPosition(row), row);
            var dropIndex = (AssociatedObject.Items as IList).IndexOf(row.DataContext);
            var draggedIndex = (AssociatedObject.Items as IList).IndexOf(DragDropPayloadManager.GetDataFromObject(e.Data, "DraggedItem"));

            if (dropIndex >= row.GridViewDataControl.Items.Count - 1 && details.CurrentDropPosition == DropPosition.After)
            {
                details.DropIndex = dropIndex;
                return;
            }

            dropIndex = draggedIndex > dropIndex ? dropIndex : dropIndex - 1;
            details.DropIndex = details.CurrentDropPosition == DropPosition.Before ? dropIndex : dropIndex + 1;
        }

        private static DropPosition GetDropPositionFromPoint(Point absoluteMousePosition, FrameworkElement row)
        {
            if (row != null)
            {
                return absoluteMousePosition.Y < row.ActualHeight / 2 ? DropPosition.Before : DropPosition.After;
            }

            return DropPosition.Inside;
        }
    }
   
    public class RowReorderEventArgs
    {
        /// <summary>
        /// The item the user dragged
        /// </summary>
        public object DraggedItem { get; set; }

        /// <summary>
        /// The item the user dragged over.  "Dragged Over" could mean the item that is ahead of the drop target or before the drop target.  You must check the DropPosition property of the
        /// Event Args to determine which is which.
        /// </summary>
        public object DraggedOverItem { get; set; }

        /// <summary>
        /// The index the dragged item was dropped.
        /// </summary>
        public int DropIndex { get; set; }

        /// <summary>
        /// Either before or after.
        /// </summary>
        public DropPosition DropPosition { get; set; }
    }
}