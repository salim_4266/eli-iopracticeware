using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RowDoubleClickRadGridViewBehavior : Behavior<RadGridView>
    {
        #region DependencyProperty : OnRowDoubleClick

        public ICommand OnRowDoubleClick
        {
            get { return GetValue(OnRowDoubleClickProperty) as ICommand; }
            set { SetValue(OnRowDoubleClickProperty, value); }
        }

        public static readonly DependencyProperty OnRowDoubleClickProperty = DependencyProperty.Register("OnRowDoubleClick", typeof (ICommand), typeof (RowDoubleClickRadGridViewBehavior));

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
          
            
            AssociatedObject.AddHandler(GridViewCellBase.CellDoubleClickEvent, new EventHandler<RadRoutedEventArgs>(OnCellDoubleClick).MakeWeak(), true);
        }

        #region EventHandlers

        private void OnCellDoubleClick(object sender, RadRoutedEventArgs e)
        {
            var gridViewCell = e.OriginalSource as GridViewCell;
            if (gridViewCell != null) OnRowDoubleClick.Execute(gridViewCell.ParentRow.Item);

            var gridViewHeaderCell = e.OriginalSource as GridViewHeaderCell;
            if (gridViewHeaderCell != null) e.Handled = true;
        }

        #endregion

    }
}