﻿using System.Windows;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// A solution to problem of SearchText not being in sync when suggested item is selected
    /// For more, see http://www.telerik.com/community/forums/wpf/autocompletebox/databinding-to-radautocompletebox.aspx
    /// </summary>
    public class RadAutoCompleteBoxSearchTextSyncBehavior : Behavior<RadAutoCompleteBox>
    {
        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(RadAutoCompleteBoxSearchTextSyncBehavior), new PropertyMetadata(true));

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
        }

        void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Do nothing when selected item is null now
            if (!IsEnabled || AssociatedObject.SelectedItem == null) return;

            // Get current value and update Search Text with it
            var currentValue = Binder.GetValue(AssociatedObject.SelectedItem, AssociatedObject.DisplayMemberPath) as string;
            AssociatedObject.SearchText = currentValue;

            var tb = AssociatedObject.FindChildByType<TextBox>();
            if (tb != null)
                tb.CaretIndex = 0;
        }
    }
}
