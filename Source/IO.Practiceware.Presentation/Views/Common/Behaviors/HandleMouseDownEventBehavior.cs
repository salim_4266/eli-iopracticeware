﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public sealed class HandleMouseDownEventBehavior : Behavior<UIElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.MouseDown += WeakDelegate.MakeWeak<MouseButtonEventHandler>(AssociatedObjectMouseDown);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseDown -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(AssociatedObjectMouseDown);
            base.OnDetaching();
        }

        void AssociatedObjectMouseDown(object sender, MouseButtonEventArgs e)
        {
            
            //handle the event on the attached object, then raise the event on the parent.
            //var ao = AssociatedObject.GetParents().FirstOrDefault() as UIElement;
            e.Handled = true;
            //var e2 = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
            //e2.RoutedEvent = UIElement.PreviewMouseDownEvent;
            //if (ao != null) ao.RaiseEvent(e2);
        }
    }
}
