﻿using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Scrolls to the first item of the RadGridView when the ItemsSource has updated.
    /// </summary>
    public class ScrollToFirstItemRadGridViewBehavior : Behavior<RadGridView>
    {
        private PropertyChangeNotifier _itemsSourceNotifier;

        protected override void OnAttached()
        {
            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourcePropertyChanged);
        }

        protected override void OnDetaching()
        {
            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourcePropertyChanged);
            base.OnDetaching();
        }

        private void OnItemsSourcePropertyChanged(object sender, EventArgs e)
        {
            if (AssociatedObject != null)
            {
                var itemsCollection = AssociatedObject.ItemsSource as IEnumerable<object>;
                if (itemsCollection != null)
                {
                    var itemsArray = itemsCollection as object[] ?? itemsCollection.ToArray();
                    if(itemsArray.Any())
                    {
                        var scrollViewer = AssociatedObject.ChildrenOfType<GridViewScrollViewer>().FirstOrDefault();
                        if(scrollViewer != null) scrollViewer.ScrollToHome();
                    }
                }
            }
        }
    }
}
