﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    class MaxSelectedItemsBehavior : Behavior<RadAutoCompleteBox>
    {

        public static int GetMaxSelectedItemsCount(DependencyObject obj)
        {
            return (int)obj.GetValue(MaxSelectedItemsCountProperty);
        }

        public static void SetMaxSelectedItemsCount(DependencyObject obj, int value)
        {
            obj.SetValue(MaxSelectedItemsCountProperty, value);
        }

        public static readonly DependencyProperty MaxSelectedItemsCountProperty =
            DependencyProperty.RegisterAttached("MaxSelectedItemsCount", typeof(int), typeof(MaxSelectedItemsBehavior),
                new UIPropertyMetadata(OnMaxSelectedItemsCountPropertyChanged));

        //attach methods to selectionchanged event
        static void OnMaxSelectedItemsCountPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (int) e.NewValue > 0 ? new MaxSelectedItemsBehavior() : null;
            dependencyObject.AddOrReplaceBehavior(behavior);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(AddItemIfCountLessThanMaxSelectedItemsCount);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(AddItemIfCountLessThanMaxSelectedItemsCount);
        }

        private void AddItemIfCountLessThanMaxSelectedItemsCount(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            var currentMax = GetMaxSelectedItemsCount(AssociatedObject);
            var autocompleteBox = AssociatedObject;
            if (autocompleteBox != null)
            {
                var list = autocompleteBox.SelectedItems as IList;
                if (list != null && list.Count > currentMax)
                {
                    list.RemoveAt(currentMax);
                }
            }
        }
    }
}
