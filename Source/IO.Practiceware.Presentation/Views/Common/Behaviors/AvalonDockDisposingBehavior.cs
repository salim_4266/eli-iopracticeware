﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Xceed.Wpf.AvalonDock;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    ///     Ensures the dock is disposed when the window closes.
    /// </summary>
    public class AvalonDockDisposingBehavior : Behavior<DockingManager>
    {
        public static readonly DependencyProperty WindowProperty = DependencyProperty.Register("Window", typeof (Window), typeof (AvalonDockDisposingBehavior), new PropertyMetadata(OnWindowChanged));

        public Window Window
        {
            get { return GetValue(WindowProperty) as Window; }
            set { SetValue(WindowProperty, value); }
        }

        private static void OnWindowChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (AvalonDockDisposingBehavior) d;

            var window = e.OldValue as Window;
            if (window != null)
            {
                window.Closed -= new EventHandler(behavior.OnWindowClosed).MakeWeak();
            }
            window = e.NewValue as Window;
            if (window != null)
            {
                window.Closed += new EventHandler(behavior.OnWindowClosed).MakeWeak();
            }
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            if (AssociatedObject != null && AssociatedObject.AutoHideWindow != null)
            {
                // AvalonDock has a memory leak and this will be rooted by an HwndTarget
                AssociatedObject.AutoHideWindow.Dispose();
            }
        }
    }
}