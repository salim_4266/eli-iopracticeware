﻿using System.Windows;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Static behavior that allows one to deselect a radiobutton after it has been selected.
    /// </summary>
    public static class RadioButtonUncheckBehavior
    {   
        public static readonly DependencyProperty AllowRadioButtonDeselectionProperty = DependencyProperty.RegisterAttached("AllowRadioButtonDeselection", typeof(bool), typeof(RadioButtonUncheckBehavior), new PropertyMetadata(false, OnAllowRadioButtonDeselectionChanged));
        public static readonly DependencyProperty WasInitiallyCheckedProperty = DependencyProperty.RegisterAttached("WasInitiallyChecked", typeof(bool), typeof(RadioButtonUncheckBehavior));
        
        public static void SetAllowRadioButtonDeselection(DependencyObject radioButton, bool value)
        {
            if (!(radioButton is RadioButton)) return;
            radioButton.SetValue(AllowRadioButtonDeselectionProperty, value);
        }

        private static void OnAllowRadioButtonDeselectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var radioButton = d as RadioButton;
            if (radioButton == null || !(args.NewValue is bool))
            {
                return;
            }

            if ((bool)args.NewValue)
            {
                radioButton.Checked += OnRadioButtonChecked;
                radioButton.Click += OnListBoxClick;
            }
        }

        private static void OnRadioButtonChecked(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;
            if(radioButton != null)
            {
                radioButton.SetValue(WasInitiallyCheckedProperty, true);
            }
        }

        private static void OnListBoxClick(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;
            if (radioButton != null && radioButton.IsChecked.HasValue && radioButton.IsChecked.Value)
            {
                var wasInitiallyChecked = (bool) radioButton.GetValue(WasInitiallyCheckedProperty);
                if (wasInitiallyChecked)
                {
                    radioButton.SetValue(WasInitiallyCheckedProperty, false);
                }
                else
                {
                    radioButton.IsChecked = false;
                }
            }
        }
    }
}
