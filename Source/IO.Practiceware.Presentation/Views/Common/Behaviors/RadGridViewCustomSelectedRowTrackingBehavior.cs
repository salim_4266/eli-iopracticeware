﻿using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Keeps track of single selected row for Telerik's GridView based on how focus moves between it's child elements
    /// </summary>
    /// <remarks>
    /// To be used when native selected row tracking of the grid is not possible (broken via styles)
    /// </remarks>
    public class RadGridViewCustomSelectedRowTrackingBehavior : Behavior<RadGridView>
    {
        private bool _justHandledToggleViaFollowFocus;

        /// <summary>
        /// Gets or sets the name of the control within GridViewRow which is used to toggle its selection.
        /// </summary>
        /// <value>
        /// The name of the toggle selection control.
        /// </value>
        public string ToggleSelectionControlName
        {
            get { return (string)GetValue(ToggleSelectionControlNameProperty); }
            set { SetValue(ToggleSelectionControlNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToggleSelectionControlName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToggleSelectionControlNameProperty =
            DependencyProperty.Register("ToggleSelectionControlName", typeof(string), typeof(RadGridViewCustomSelectedRowTrackingBehavior), new PropertyMetadata());

        
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.GotFocus += WeakDelegate.MakeWeak<RoutedEventHandler>(OnFollowFocus);
            AssociatedObject.PreviewMouseUp += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseUp);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.GotFocus -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnFollowFocus);
            AssociatedObject.PreviewMouseUp -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseUp);
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            RefreshSelectedValue(e, false);
        }

        private void OnFollowFocus(object sender, RoutedEventArgs e)
        {
            RefreshSelectedValue(e, true);
        }

        private void RefreshSelectedValue(RoutedEventArgs e, bool fromFollowFocus)
        {
            var handled = _justHandledToggleViaFollowFocus;
            _justHandledToggleViaFollowFocus = false;

            if ((!fromFollowFocus && handled)
                // If grid itself got focus -> don't reset selected row
                || ReferenceEquals(e.OriginalSource, AssociatedObject))
            {
                return;
            }

            var sourceTargetElement = e.OriginalSource as FrameworkElement;
            var gridItems = AssociatedObject.ItemsSource.As<IEnumerable>().IfNotNull(c => c.OfType<object>().ToList());
            if (sourceTargetElement == null || gridItems == null) return;

            object selectedItem = null;
            var targetElement = sourceTargetElement;
            while (targetElement != null)
            {
                var parentRow = DependencyObjectExtensions.ParentOfType<GridViewRow>(targetElement);
                targetElement = parentRow;

                // Row with datacontext from this grid?
                if (parentRow == null || !gridItems.Contains(parentRow.DataContext))
                {
                    continue;
                }
                
                // Just select current row's datacontexts
                selectedItem = parentRow.DataContext;
                break;
            }

            // Locate toggle control if needed
            var toggleControl = targetElement == null || string.IsNullOrEmpty(ToggleSelectionControlName)
                    ? null 
                    : targetElement.GetVisualChildren<FrameworkElement>()
                        .Select(c => (FrameworkElement)c.FindName(ToggleSelectionControlName))
                        .FirstOrDefault(c => c != null);

            // If event originated from toggle control -> do toggle
            var changedViaToggle = false;
            if (toggleControl != null 
                && (sourceTargetElement is GridViewCell
                    ? sourceTargetElement.IsAncestorOf(toggleControl) 
                    : toggleControl.IsAncestorOf(sourceTargetElement)
               ))
            {
                selectedItem = AssociatedObject.SelectedItem == selectedItem
                    ? null
                    : selectedItem;
                changedViaToggle = true;
                _justHandledToggleViaFollowFocus = fromFollowFocus;
            }

            AssociatedObject.SelectedItem = selectedItem 
                // Only reset selection if changed via toggle
                ?? (changedViaToggle ? selectedItem : AssociatedObject.SelectedItem);
        }
    }
}
