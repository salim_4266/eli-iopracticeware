﻿using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Prefilters checkbox column by either "True" or "False" value
    /// </summary>
    public class RadGridViewCheckBoxColumnPreFilterBehavior : Behavior<GridViewCheckBoxColumn>
    {
        /// <summary>
        /// True or False values to show
        /// </summary>
        public bool ValueToShow { get; set; }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.ColumnFilterDescriptor.DistinctFilter.AddDistinctValue(ValueToShow);
        }
    }
}
