﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Primitives;
using FocusNavigationDirection = System.Windows.Input.FocusNavigationDirection;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// attaches a command on enter KeyDown (shouldn't interfere with built-in enter keyDown or previewKeyDown functionality on selectors)
    /// </summary>
    class AttachCommandOnEnterBehavior : Behavior<UIElement>
    {

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(AttachCommandOnEnterBehavior), new PropertyMetadata());

        public static readonly DependencyProperty CommandParameterProperty =
                        DependencyProperty.Register("CommandParameter", typeof(Object), typeof(AttachCommandOnEnterBehavior), new PropertyMetadata());


        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public Object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.KeyDown += new KeyEventHandler(OnEnterKeydown).MakeWeak<KeyEventHandler>();
            AssociatedObject.KeyDown += new KeyEventHandler(OnTabKeydown).MakeWeak<KeyEventHandler>();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.KeyDown -= new KeyEventHandler(OnEnterKeydown).MakeWeak<KeyEventHandler>();
            AssociatedObject.KeyDown -= new KeyEventHandler(OnTabKeydown).MakeWeak<KeyEventHandler>();
        }

        private void OnEnterKeydown(object sender, KeyEventArgs e)
        {

            //If it's enter, execute the command and move focus
            if (e.Key.Equals(Key.Enter))
            {
                UpdateBindings(e.OriginalSource as Control);
                Command.Execute(CommandParameter);
                if (AssociatedObject != null)
                {
                    AssociatedObject.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    ClearBindings(AssociatedObject.GetChildren<Control>());
                }

                e.Handled = true;
            }
        }

        private void OnTabKeydown(object sender, KeyEventArgs e)
        {
            //If it's tab, check if the current control is the last one
            if (e.Key.Equals(Key.Tab) && !Keyboard.Modifiers.Equals(ModifierKeys.Shift) && IsLastControlFocused())
            {
                UpdateBindings(e.OriginalSource as Control);
                Command.Execute(CommandParameter);
                if (AssociatedObject != null)
                {
                    AssociatedObject.MoveFocus(new TraversalRequest(FocusNavigationDirection.Right));
                    ClearBindings(AssociatedObject.GetChildren<Control>());                    
                }


                e.Handled = true;
            }
        }

        private bool IsLastControlFocused()
        {
            Control current = null;
            foreach (Control c in AssociatedObject.GetChildren<Control>().Where(c => c.IsKeyboardFocusWithin))
            {
                current = c;
            }

            if (current == null) return false;
            var next = current.PredictFocus(FocusNavigationDirection.Right) as Control;

            //first get out of the current control (telerik controls have controls inside controls which can be tab stops)
            while (current != null && next != null && current.IsAncestorOf(next))
            {
                current = current.PredictFocus(FocusNavigationDirection.Right) as Control;
                next = next.PredictFocus(FocusNavigationDirection.Right) as Control;
            }

            //return whether the next control is still within the associated object
            return (next == null || !AssociatedObject.IsAncestorOf(next));
        }


        private static void UpdateBindings(Control e)
        {
            
            var sourceAsTextBox = e as RadWatermarkTextBox;
            var sourceAsAutoCompleteBox = e.ParentOfType<RadAutoCompleteBox>();
            if (sourceAsTextBox != null && sourceAsTextBox.ParentOfType<RadAutoCompleteBox>() == null)
            {
                var bindingExpression = sourceAsTextBox.GetBindingExpression(TextBox.TextProperty);
                if (bindingExpression != null)
                {
                    bindingExpression.UpdateSource();
                }
            }
            if (sourceAsAutoCompleteBox != null)
            {
                if (sourceAsAutoCompleteBox.SelectionMode == AutoCompleteSelectionMode.Single)
                {
                    var bindingExpression = sourceAsAutoCompleteBox.GetBindingExpression(RadAutoCompleteBox.SelectedItemProperty);
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateSource();
                    }
                }
                if (sourceAsAutoCompleteBox.SelectionMode == AutoCompleteSelectionMode.Multiple)
                {
                    var bindingExpression = sourceAsAutoCompleteBox.GetBindingExpression(RadAutoCompleteBox.SelectedItemsProperty);
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateSource();
                    }
                }
            }
        }

        private static void ClearBindings(IEnumerable<Control> controls)
        {
            foreach (var c in controls)
            {
                var originalSource = c;
                var sourceAsTextBox = c as RadWatermarkTextBox;
                var sourceAsAutoCompleteBox = originalSource.ParentOfType<RadAutoCompleteBox>();
                if (sourceAsTextBox != null && sourceAsTextBox.ParentOfType<RadAutoCompleteBox>() == null)
                {
                    var bindingExpression = sourceAsTextBox.GetBindingExpression(TextBox.TextProperty);
                    if (bindingExpression != null)
                    {
                        bindingExpression.UpdateTarget();
                    }
                }
                if (sourceAsAutoCompleteBox != null)
                {
                    if (sourceAsAutoCompleteBox.SelectionMode == AutoCompleteSelectionMode.Single)
                    {

                        var bindingExpression = sourceAsAutoCompleteBox.GetBindingExpression(RadAutoCompleteBox.SelectedItemProperty);
                        if (bindingExpression != null)
                        {
                            bindingExpression.UpdateTarget();
                        }
                    }
                    if (sourceAsAutoCompleteBox.SelectionMode == AutoCompleteSelectionMode.Multiple)
                    {
                        var bindingExpression = sourceAsAutoCompleteBox.GetBindingExpression(RadAutoCompleteBox.SelectedItemsProperty);
                        if (bindingExpression != null)
                        {
                            bindingExpression.UpdateTarget();
                        }
                    }
                }
            }
        }
    }
}
