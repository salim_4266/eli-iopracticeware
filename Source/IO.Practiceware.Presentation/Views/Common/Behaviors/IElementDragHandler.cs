﻿using System.Windows;
using Telerik.Windows.DragDrop;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public interface IElementDragHandler
    {
        DragDropEffects SupportedEffects { get; }

        /// <summary>
        /// Prepares element for dragging and returns data template to be used while element is dragged
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="payloadData">The payload data.</param>
        /// <param name="visualTemplate">The visual template.</param>
        void PrepareForDragging(FrameworkElement element, out object payloadData, out DataTemplate visualTemplate);

        /// <summary>
        /// Handle drag completed operation
        /// </summary>
        /// <param name="element"></param>
        /// <param name="dropSucceeded"></param>
        /// <param name="payloadData"></param>
        /// <param name="args"></param>
        void ProcessDragCompleted(FrameworkElement element, bool dropSucceeded, object payloadData, DragDropCompletedEventArgs args);
    }
}