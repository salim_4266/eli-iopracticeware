﻿using System.Windows;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Allows OneWayToSource binding in cases when it cannot be accomplished using built-in XAML functionality
    /// </summary>
    public class SourceToDestinationPropertyBindingBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", 
            typeof (object), 
            typeof (SourceToDestinationPropertyBindingBehavior), 
            new PropertyMetadata(default(object), OnSourcePropertyChanged));

        public static readonly DependencyProperty DestinationProperty =
            DependencyProperty.Register("Destination", 
            typeof (object), 
            typeof (SourceToDestinationPropertyBindingBehavior), 
            new PropertyMetadata(default(object)));

        public object Source
        {
            get { return GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public object Destination
        {
            get { return GetValue(DestinationProperty); }
            set { SetValue(DestinationProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            Destination = Source;
        }

        static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Carry over updated source value to destination
            d.SetValue(DestinationProperty, e.NewValue);
        }
    }
}
