﻿using Soaf.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// RadComboBox behavior that allows the DependencyProperty EmptyText to be italicized, while the ComboBoxItems and the "typed-in" text is normal font.
    /// </summary>
    public class RadComboBoxBehavior : Behavior<RadComboBox>
    {
        public static readonly DependencyProperty SelectionChangedCommandProperty = DependencyProperty.Register("SelectionChangedCommand", typeof(ICommand), typeof(RadComboBoxBehavior));

        public ICommand SelectionChangedCommand
        {
            get { return GetValue(SelectionChangedCommandProperty) as ICommand; }
            set { SetValue(SelectionChangedCommandProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);           
        }


        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectionChangedCommand != null)
            {
                SelectionChangedCommand.Execute(null);
            }
        }      
    }
}