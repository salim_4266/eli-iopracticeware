﻿using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Allows a button's click behavior to clear a property back to its default value.
    /// </summary>
    public class ClearButtonBehavior : Behavior<Button>
    {
        public static readonly DependencyProperty TargetToClearProperty = DependencyProperty.Register("TargetToClear", typeof(object), typeof(ClearButtonBehavior));
        
        public static readonly DependencyProperty TargetPropertyNameProperty = DependencyProperty.Register("TargetPropertyName", typeof(string), typeof(ClearButtonBehavior));
        
        public static readonly DependencyProperty ResetFocusElementProperty = DependencyProperty.Register("ResetFocusElement", typeof(UIElement), typeof(ClearButtonBehavior));

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(ClearButtonBehavior), new PropertyMetadata(true));

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public object TargetToClear
        {
            get { return GetValue(TargetToClearProperty); }
            set { SetValue(TargetToClearProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the target property. Supports multiple properties separated by commas too
        /// </summary>
        /// <value>
        /// The name of the target property.
        /// </value>
        public string TargetPropertyName
        {
            get { return (string)GetValue(TargetPropertyNameProperty); }
            set { SetValue(TargetPropertyNameProperty, value); }
        }

        public UIElement ResetFocusElement
        {
            get { return (UIElement)GetValue(ResetFocusElementProperty); }
            set { SetValue(ResetFocusElementProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Click += WeakDelegate.MakeWeak<RoutedEventHandler>(OnButtonClick);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Click -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnButtonClick);
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            if (!IsEnabled) return;

            if (TargetToClear != null && TargetPropertyName.IsNotNullOrEmpty())
            {
                var propertyNames = TargetPropertyName.Split(',');
                foreach (var propertyName in propertyNames)
                {
                    var propertyInfo = TargetToClear.GetType().GetProperty(propertyName);
                    var defaultValue = propertyInfo.PropertyType.IsValueType ? Activator.CreateInstance(propertyInfo.PropertyType) : null;
                    propertyInfo.GetSetter()(TargetToClear, defaultValue);
                }
            }
            if (ResetFocusElement != null)
                ResetFocusElement.Focus();
        }
    }
}
