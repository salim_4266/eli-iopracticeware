﻿using Soaf;
using Soaf.ComponentModel;
using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class ParentOfTypeSetValueBehavior : Behavior<FrameworkElement>
    {
        private object _parent;
        private PropertyInfo _propertyInfo;

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(object), typeof(ParentOfTypeSetValueBehavior), new PropertyMetadata(null, OnPropertyValueChanged));
        
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += WeakDelegate.MakeWeak<RoutedEventHandler>(AssociatedObjectLoaded);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(AssociatedObjectLoaded);
        }

        private void AssociatedObjectLoaded(object sender, RoutedEventArgs e)
        {
            _parent = AssociatedObject.GetParents().FirstOrDefault(p => TypeOfParent.IsInstanceOfType(p));
            if (_parent == null) { throw new Exception("Could not find parent of type {0}".FormatWith(TypeOfParent.Name)); }

            _propertyInfo = TypeOfParent.GetProperty(Property, BindingFlags.Public | BindingFlags.Instance);
            if (_propertyInfo == null) { throw new Exception("Could not find property {0} for type {1}".FormatWith(Property, TypeOfParent.Name)); }

            _propertyInfo.SetValue(_parent, Value, null);
        }

        private static void OnPropertyValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ParentOfTypeSetValueBehavior)d;
            if (behavior._propertyInfo != null && behavior._parent != null)
            {
                behavior._propertyInfo.SetValue(behavior._parent, behavior.Value, null);
            }
        }

        /// <summary>
        /// Gets or sets the value of the property to set on the parent
        /// </summary>
        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets the property to set on the parent
        /// </summary>
        public string Property { get; set; }

        /// <summary>
        /// Gets or sets the type of the parent
        /// </summary>
        public Type TypeOfParent { get; set; }
    }
}
