﻿using Soaf;
using Soaf.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class TextBoxBaseEnterKeyCommandBehavior : Behavior<TextBoxBase>
    {
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof (ICommand), typeof (TextBoxBaseEnterKeyCommandBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.PreviewKeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(OnKeyDown);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.PreviewKeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(OnKeyDown);
        }

        void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Command.IfNotNull(c => c.Execute(null));
            }
        }
    }
}
