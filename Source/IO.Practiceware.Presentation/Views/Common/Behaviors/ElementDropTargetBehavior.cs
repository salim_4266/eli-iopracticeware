﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using Telerik.Windows.DragDrop;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    [ContentProperty("ElementDropTargetHandler")]
    public class ElementDropTargetBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty ElementDropTargetHandlerProperty =
            DependencyProperty.Register("ElementDropTargetHandler", typeof (IElementDropTargetHandler), typeof (ElementDropTargetBehavior), new PropertyMetadata(default(IElementDropTargetHandler)));

        public IElementDropTargetHandler ElementDropTargetHandler
        {
            get { return (IElementDropTargetHandler) GetValue(ElementDropTargetHandlerProperty); }
            set { SetValue(ElementDropTargetHandlerProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            // Allow drop operations
            AssociatedObject.SetValue(UIElement.AllowDropProperty, true);

            // Ensure there is some background on drop panel, so it can be "hit tested"
            if (AssociatedObject.GetValue(Control.BackgroundProperty) == null)
            {
                AssociatedObject.SetValue(Control.BackgroundProperty, Brushes.Transparent);
            }

            // Subscribe
            DragDropManager.AddDragEnterHandler(AssociatedObject, DragEnterHandler);
            DragDropManager.AddDragLeaveHandler(AssociatedObject, DragLeaveHandler);
            DragDropManager.AddDragOverHandler(AssociatedObject, DragOverHandler);
            DragDropManager.AddDropHandler(AssociatedObject, DropHandler);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            DragDropManager.RemoveDragEnterHandler(AssociatedObject, DragEnterHandler);
            DragDropManager.RemoveDragLeaveHandler(AssociatedObject, DragLeaveHandler);
            DragDropManager.RemoveDragOverHandler(AssociatedObject, DragOverHandler);
            DragDropManager.RemoveDropHandler(AssociatedObject, DropHandler);
        }

        void DragOverHandler(object sender, DragEventArgs args)
        {
            object payloadData;
            if (!GetSupportedPayload(args, out payloadData))
            {
                return;
            }

            ElementDropTargetHandler.ProcessItemDragOver(AssociatedObject, payloadData, args);
        }

        void DragLeaveHandler(object sender, DragEventArgs args)
        {
            object payloadData;
            if (!GetSupportedPayload(args, out payloadData))
            {
                return;
            }

            ElementDropTargetHandler.ProcessItemDragLeave(AssociatedObject, payloadData, args);
        }

        void DragEnterHandler(object sender, DragEventArgs args)
        {
            object payloadData;
            if (!GetSupportedPayload(args, out payloadData))
            {
                return;
            }

            ElementDropTargetHandler.ProcessItemDragEnter(AssociatedObject, payloadData, args);
        }

        void DropHandler(object sender, DragEventArgs args)
        {
            object payloadData;
            if (!GetSupportedPayload(args, out payloadData))
            {
                return;
            }

            ElementDropTargetHandler.ProcessItemDropped(AssociatedObject, payloadData, args);
        }

        bool GetSupportedPayload(DragEventArgs args, out object payloadData)
        {
            payloadData = null;

            // No effect if not supported
            args.Effects = DragDropEffects.None;
            args.Handled = true;

            if (args.Data == null)
            {
                return false;
            }

            // Get payload
            payloadData = DragDropPayloadManager.GetDataFromObject(args.Data, ElementDragSourceBehavior.DragSourcePayloadKey);
            if (payloadData == null)
            {
                return false;
            }

            // Check if it is a supported payload
            var payloadDataType = payloadData.GetType();
            var supported = ElementDropTargetHandler.SupportedPayloadType.IsAssignableFrom(payloadDataType);
            if (supported)
            {
                // Allowed effect if payload matches
                args.Effects = args.AllowedEffects;
            }

            return supported;
        }
    }
}
