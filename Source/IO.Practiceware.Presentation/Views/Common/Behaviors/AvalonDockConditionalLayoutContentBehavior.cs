﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Xceed.Wpf.AvalonDock.Layout;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class LayoutContentCollection : List<LayoutDocument> { }

    /// <summary>
    /// Conditionally displays certain layout content.
    /// </summary>
    public class AvalonDockConditionalLayoutContentBehavior : Behavior<LayoutDocumentPane>
    {
        public static readonly DependencyProperty IsContentVisibleProperty = DependencyProperty.Register("IsContentVisible", typeof(bool), typeof(AvalonDockConditionalLayoutContentBehavior), new PropertyMetadata(OnIsContentVisibleChanged));

        private static void OnIsContentVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (AvalonDockConditionalLayoutContentBehavior)d;

            if (behavior.AssociatedObject == null) return;

            if ((bool) e.NewValue)
            {
                foreach (var content in behavior.LayoutContents.Where(i => !behavior.AssociatedObject.Children.Contains(i)))
                {
                    behavior.AssociatedObject.Children.Add(content);
                }
            }
            else
            {
                foreach (var content in behavior.LayoutContents)
                {
                    behavior.AssociatedObject.Children.Remove(content);
                }
            }
        }

        public bool IsContentVisible
        {
            get { return (bool)GetValue(IsContentVisibleProperty); }
            set { SetValue(IsContentVisibleProperty, value); }
        }

        public static readonly DependencyProperty LayoutContentsProperty = DependencyProperty.Register("LayoutContents", typeof(LayoutContentCollection), typeof(AvalonDockConditionalLayoutContentBehavior), new PropertyMetadata(new LayoutContentCollection()));

        public LayoutContentCollection LayoutContents
        {
            get { return (LayoutContentCollection)GetValue(LayoutContentsProperty); }
            set { SetValue(LayoutContentsProperty, value); }
        }
    }
}
