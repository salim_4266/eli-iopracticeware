﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Threading;
using Soaf;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;
using Binding = System.Windows.Data.Binding;
using DataGrid = System.Windows.Controls.DataGrid;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    internal class RowClicksDataGridViewBehavior : Behavior<DataGrid>
    {

        #region DependencyProperty : OnRowRightClick

        public ICommand SingleClickCommand
        {
            get { return GetValue(SingleClickCommandProperty) as ICommand; }
            set { SetValue(SingleClickCommandProperty, value); }
        }

        public static readonly DependencyProperty SingleClickCommandProperty = DependencyProperty.Register("SingleClickCommand", typeof(ICommand), typeof(RowClicksDataGridViewBehavior));


        public ICommand DoubleClickCommand
        {
            get { return GetValue(DoubleClickCommandProperty) as ICommand; }
            set { SetValue(DoubleClickCommandProperty, value); }
        }


        public static readonly DependencyProperty DoubleClickCommandProperty = DependencyProperty.Register("DoubleClickCommand", typeof(ICommand), typeof(RowClicksDataGridViewBehavior));

        public Popup RightClickMenuPopup
        {
            get { return GetValue(RightClickMenuPopupProperty) as Popup; }
            set { SetValue(RightClickMenuPopupProperty, value); }
        }

        public static readonly DependencyProperty RightClickMenuPopupProperty = DependencyProperty.Register("RightClickMenuPopup", typeof(Popup), typeof(RowClicksDataGridViewBehavior));

        public Type DataContextTypeFilter
        {
            get { return GetValue(DataContextTypeFilterProperty) as Type; }
            set { SetValue(DataContextTypeFilterProperty, value); }
        }

        public static readonly DependencyProperty DataContextTypeFilterProperty = DependencyProperty.Register("ViewModelType", typeof(Type), typeof(RowClicksDataGridViewBehavior));

        #endregion

        #region EventHandlers

        private void OnRowRightClick(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;
            _gridViewRow = source.ParentOfType<DataGridRow>();
            if (_gridViewRow == null) return;
            //check the type of the row is what we expect (this is passed through xaml - for now, we just match on strings... seems to works for our purposes here...)
            if (DataContextTypeFilter != null)
            {
                if (!_gridViewRow.Item.Is(DataContextTypeFilter)) return;
            }

            //open the popup and give it the datacontext of the row we clicked on
            RightClickMenuPopup.IsOpen = true;
            RightClickMenuPopup.Focus();
            BindingOperations.SetBinding(RightClickMenuPopup, Popup.IsOpenProperty, new Binding("IsKeyboardFocusWithin") { Source = RightClickMenuPopup, Mode = BindingMode.OneWay });
            if (_gridViewRow != null) RightClickMenuPopup.DataContext = _gridViewRow.DataContext;
        }


        readonly DispatcherTimer _clickTimer = new DispatcherTimer();
        int _clickCounter;
        DataGridRow _gridViewRow;
        readonly EventHandler _handler;

        public RowClicksDataGridViewBehavior()
        {
            _handler = (s, eventArgs) => EvaluateClicks(_gridViewRow);
        }


        private void OnRowClick(object sender, MouseButtonEventArgs e)
        {

            //get doubleClickTime from the user's mouse settings
            int doubleClickTime = SystemInformation.DoubleClickTime;

            //just in case... make it non-zero
            if (doubleClickTime <= 0) doubleClickTime = 300;

            _clickTimer.Interval = TimeSpan.FromMilliseconds(doubleClickTime);

            var originalSource = e.OriginalSource as FrameworkElement;
            if (originalSource != null && !(originalSource.GetType() == typeof(RadDropDownButton) || originalSource.ParentOfType<RadContextMenu>() != null))
            {

                _gridViewRow = originalSource.ParentOfType<DataGridRow>();

                //start the timer
                _clickTimer.Stop();
                _clickCounter++;
                _clickTimer.Start();

            }
        }

        private void EvaluateClicks(DataGridRow gridViewRow)
        {
            _clickTimer.Stop();
            //if we don't get a row...
            if (gridViewRow == null)
            {
                _clickCounter = 0;
                return;
            }

            //check the type of the row is what we expect (this is passed through xaml - for now, we just match on strings... seems to works for our purposes here...)
            if (DataContextTypeFilter != null)
            {
                if (!gridViewRow.Item.Is(DataContextTypeFilter))
                {
                    _clickCounter = 0;
                    return;
                }
            }
            if (_clickCounter == 1)
            {
                if (gridViewRow.IsSelected)
                {
                    if (SingleClickCommand != null) SingleClickCommand.Execute(gridViewRow.DataContext);
                    var dataGrid = gridViewRow.GetVisualParent<DataGrid>();
                    dataGrid.SelectedItem = gridViewRow;
                    dataGrid.UpdateLayout();
                }
            }
            if (_clickCounter > 1)
            {
                if (DoubleClickCommand != null) DoubleClickCommand.Execute(gridViewRow.DataContext);
            }
            _clickCounter = 0;
        }




        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.MouseRightButtonUp += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnRowRightClick);
            AssociatedObject.AddHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(OnRowClick).MakeWeak<MouseButtonEventHandler>(), true);
            _clickTimer.Tick += _handler.MakeWeak();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.MouseRightButtonUp -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnRowRightClick);
            AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(OnRowClick).MakeWeak<MouseButtonEventHandler>());
            _clickTimer.Tick -= _handler.MakeWeak();
        }
    }
}
