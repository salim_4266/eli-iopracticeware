﻿using System.Windows;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Marks the item selected, scrolls to it and then enters into edit mode for first data column
    /// </summary>
    public class RadGridViewActivateAddedItemBehavior : Behavior<RadGridView>
    {
        private PropertyChangeNotifier _itemsSourceNotifier;
        INotifyCollectionChanged _subscribedItemsSource;

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.Register("IsEnabled", typeof(bool), typeof(RadGridViewActivateAddedItemBehavior), new PropertyMetadata(true));

        /// <summary>
        /// Gets or sets the name assigned to <see cref="FrameworkElement"/> which should be focused for a newly added row.
        /// </summary>
        /// <value>
        /// The UI element name to focus.
        /// </value>
        public string UIElementNameToFocus
        {
            get { return (string)GetValue(UIElementNameToFocusProperty); }
            set { SetValue(UIElementNameToFocusProperty, value); }
        }

        public static readonly DependencyProperty UIElementNameToFocusProperty =
            DependencyProperty.Register("UIElementNameToFocus", typeof(string), typeof(RadGridViewActivateAddedItemBehavior), new PropertyMetadata(null));

        protected override void OnAttached()
        {
            base.OnAttached();

            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, DataControl.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourcePropertyChanged);

            TryResubscribeNewItemsSource();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.RowLoaded -= WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(OnGridRowLoaded);

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourcePropertyChanged);

            UnsubscribePreviousItemsSource();
        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!IsEnabled || e.Action != NotifyCollectionChangedAction.Add || e.NewItems.Count <= 0) return;

            // End current edit, since scrolling will fail if it is on
            AssociatedObject.CommitEdit();

            var newItem = e.NewItems[0];

            // Make it selected one to assist finding in the grid
            AssociatedObject.SelectedItems.Clear();
            AssociatedObject.SelectedItems.Add(newItem);

            // Scroll to it if it's not in focus
            AssociatedObject.ScrollIntoView(newItem);

            if (!string.IsNullOrEmpty(UIElementNameToFocus))
            {
                // Preselect specified control once new row is loaded
                AssociatedObject.RowLoaded -= WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(OnGridRowLoaded);
                AssociatedObject.RowLoaded += WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(OnGridRowLoaded);
            }
            else
            {
                // Begin editing in first data column
                var firstDataColumn = AssociatedObject.Columns.OfType<GridViewDataColumn>().FirstOrDefault();
                if (firstDataColumn != null)
                {
                    AssociatedObject.CurrentCellInfo = new GridViewCellInfo(newItem, firstDataColumn);
                    AssociatedObject.BeginEdit();
                }
            }
        }

        private void OnGridRowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.DataElement != AssociatedObject.SelectedItem) return;

            // Execute once per new row added
            AssociatedObject.RowLoaded -= WeakDelegate.MakeWeak<EventHandler<RowLoadedEventArgs>>(OnGridRowLoaded);

            TryFocusRequestedElement(e.Row, UIElementNameToFocus, DateTime.Now);
        }

        /// <summary>
        /// Tries to focus requested element which might be not yet in Visual tree
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="propertyToFocus">The property to focus.</param>
        /// <param name="initiated">The initiated.</param>
        private void TryFocusRequestedElement(GridViewRowItem row, string propertyToFocus, DateTime initiated)
        {
            var elementToFocus = row.GetChildren<FrameworkElement>().FirstOrDefault(p => p.Name == propertyToFocus);
            if (elementToFocus == null && (DateTime.Now - initiated <= TimeSpan.FromSeconds(3)))
            {
                Dispatcher.BeginInvoke(new Action<GridViewRowItem, string, DateTime>(TryFocusRequestedElement),
                    System.Windows.Threading.DispatcherPriority.Background, row, propertyToFocus, initiated);
                return;
            }

            elementToFocus.As<UIElement>().IfNotNull(e => e.Focus());
        }

        private void OnItemsSourcePropertyChanged(object sender, EventArgs e)
        {
            TryResubscribeNewItemsSource();
        }

        void TryResubscribeNewItemsSource()
        {
            UnsubscribePreviousItemsSource();

            _subscribedItemsSource = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            if (_subscribedItemsSource != null)
            {
                _subscribedItemsSource.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnCollectionChanged);
            }
        }

        void UnsubscribePreviousItemsSource()
        {
            if (_subscribedItemsSource == null) return;

            _subscribedItemsSource.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnCollectionChanged);
            _subscribedItemsSource = null;
        }
    }
}
