﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Soaf.Presentation;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadAutoCompleteBoxSelectFirstItemBehavior : Behavior<RadAutoCompleteBox>
    {
        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(RadAutoCompleteBoxSelectFirstItemBehavior), new PropertyMetadata(true));

        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        private PropertyChangeNotifier _itemsSourceNotifier;
        private PropertyChangeNotifier _selectedItemNotifier;

        protected override void OnAttached()
        {
            base.OnAttached();

            if (IsEnabled)
            {
                SelectFirstItem();
            }

            _selectedItemNotifier = new PropertyChangeNotifier(AssociatedObject, RadAutoCompleteBox.SelectedItemProperty)
                .AddValueChanged(OnSelectedItemChanged);
            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, RadAutoCompleteBox.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourceChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourceChanged);
            _selectedItemNotifier.RemoveValueChanged(OnSelectedItemChanged);
        }

        private void OnSelectedItemChanged(object sender, EventArgs e)
        {
            if (!IsEnabled) return;

            SelectFirstItem();
        }

        private void OnItemsSourceChanged(object sender, EventArgs e)
        {
            if (!IsEnabled) return;

            SelectFirstItem();
        }

        private void SelectFirstItem()
        {
            if (AssociatedObject.ItemsSource != null
                && AssociatedObject.ItemsSource.OfType<object>().Count() == 1)
            {
                AssociatedObject.SelectedItem = AssociatedObject.ItemsSource.OfType<object>().ElementAt(0);
                AssociatedObject.UpdateLayout();
            }
        }
    }
}
