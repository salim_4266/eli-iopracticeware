﻿using Soaf.ComponentModel;
using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using Window = IO.Practiceware.Presentation.Views.Common.Controls.Window;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Behavior to autoclose a popup if the user clicks away from it.
    /// </summary>
    public class PopupAutoCloseBehavior : Behavior<Popup>
    {
        public static readonly DependencyProperty ElementsToIgnoreProperty = DependencyProperty.Register("ElementsToIgnore", typeof(IList), typeof(PopupAutoCloseBehavior));

        /// <summary>
        /// Gets or sets a list of elements that will not auto close the popup when clicked on.
        /// </summary>
        public IList ElementsToIgnore
        {
            get { return GetValue(ElementsToIgnoreProperty) as IList; }
            set { SetValue(ElementsToIgnoreProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.Opened += WeakDelegate.MakeWeak<EventHandler>(AssociatedObjectOpened);
            base.OnAttached();
        }

        private void AssociatedObjectOpened(object sender, EventArgs e)
        {
            AssociatedObject.Opened -= WeakDelegate.MakeWeak<EventHandler>(AssociatedObjectOpened);

            var parentWindow = DependencyObjectExtensions.ParentOfType<Window>(AssociatedObject);
            if (parentWindow != null)
            {
                parentWindow.PreviewMouseDown += WeakDelegate.MakeWeak<MouseButtonEventHandler>(ParentViewPreviewMouseDown, 
                    handler => parentWindow.PreviewMouseDown -= handler);
            }
        }

        private void ParentViewPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var originalSourceParents = (e.OriginalSource as DependencyObject).GetParents().ToArray();

            if (!originalSourceParents.Any(p => p is Popup))
            {
                if(ElementsToIgnore == null || ElementsToIgnore.Count == 0)
                {
                    AssociatedObject.IsOpen = false;
                }
                else if (!originalSourceParents.Any(p => ElementsToIgnore.Contains(p)))
                {
                    AssociatedObject.IsOpen = false;
                }
            }
        }
    }
}
