﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class PopupParentWindowFocusBehavior : Behavior<Popup>
    {
        private bool _hidden;
        private UIElement _lastPlacementTarget;
        private System.Windows.Window _lastWindow;
        private PropertyChangeNotifier _placementTargetNotifier;

        protected override void OnAttached()
        {
            base.OnAttached();
            InitializeForPlacementTarget();

            _placementTargetNotifier = new PropertyChangeNotifier(AssociatedObject, Popup.PlacementTargetProperty)
                .AddValueChanged(OnPlacementTargetChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            DetachWindowEvents();

            if (_lastPlacementTarget != null)
            {
                ((FrameworkElement)_lastPlacementTarget).Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnPlacementTargetLoaded);
            }
            _placementTargetNotifier.RemoveValueChanged(OnPlacementTargetChanged);
        }

        private void OnPlacementTargetChanged(object sender, EventArgs e)
        {
            InitializeForPlacementTarget();
        }

        private void InitializeForPlacementTarget()
        {
            if (_lastPlacementTarget != null)
            {
                ((FrameworkElement)_lastPlacementTarget).Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnPlacementTargetLoaded);
            }
            if (AssociatedObject.PlacementTarget != null)
            {
                ((FrameworkElement)AssociatedObject.PlacementTarget).Loaded += WeakDelegate.MakeWeak<RoutedEventHandler>(OnPlacementTargetLoaded);
                AttachWindowEvents();
            }
            _lastPlacementTarget = AssociatedObject.PlacementTarget;
        }

        private void OnPlacementTargetLoaded(object sender, RoutedEventArgs e)
        {
            AttachWindowEvents();
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            DetachWindowEvents();
        }

        private void AttachWindowEvents()
        {
            if (_lastWindow != null)
            {
                DetachWindowEvents();
            }

            System.Windows.Window window = System.Windows.Window.GetWindow(AssociatedObject.PlacementTarget);
            if (window != null)
            {
                window.Deactivated += WeakDelegate.MakeWeak<EventHandler>(OnWindowDeativated);
                window.Activated += WeakDelegate.MakeWeak<EventHandler>(OnWindowActivated);
                window.Closed += WeakDelegate.MakeWeak<EventHandler>(OnWindowClosed);
            }

            _lastWindow = window;
        }

        private void DetachWindowEvents()
        {
            if (_lastWindow != null)
            {
                _lastWindow.Deactivated -= WeakDelegate.MakeWeak<EventHandler>(OnWindowDeativated);
                _lastWindow.Activated -= WeakDelegate.MakeWeak<EventHandler>(OnWindowActivated);
                _lastWindow.Closed -= WeakDelegate.MakeWeak<EventHandler>(OnWindowClosed);
            }
        }

        private void OnWindowDeativated(object sender, EventArgs e)
        {
            if (AssociatedObject == null || AssociatedObject.PlacementTarget == null) return;

            System.Windows.Window window = System.Windows.Window.GetWindow(AssociatedObject.PlacementTarget);
            if (window != null && AssociatedObject.IsOpen)
            {
                _hidden = true;
                AssociatedObject.IsOpen = false;
            }
        }

        private void OnWindowActivated(object sender, EventArgs e)
        {
            if (_hidden)
            {
                _hidden = false;
                AssociatedObject.IsOpen = true;
            }
        }
    }
}
