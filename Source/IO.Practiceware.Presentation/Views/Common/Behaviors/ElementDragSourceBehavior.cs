﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using Telerik.Windows.DragDrop;
using GiveFeedbackEventArgs = Telerik.Windows.DragDrop.GiveFeedbackEventArgs;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    [ContentProperty("ElementDragHandler")]
    public class ElementDragSourceBehavior : Behavior<FrameworkElement>
    {
        public const string DragSourcePayloadKey = "DragSourcePayload";

        public static readonly DependencyProperty ElementDragHandlerProperty =
            DependencyProperty.Register("ElementDragHandler", typeof (IElementDragHandler), typeof (ElementDragSourceBehavior), new PropertyMetadata(default(IElementDragHandler)));

        public IElementDragHandler ElementDragHandler
        {
            get { return (IElementDragHandler) GetValue(ElementDragHandlerProperty); }
            set { SetValue(ElementDragHandlerProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            // Allow initiating drag operation
            AssociatedObject.SetValue(DragDropManager.AllowDragProperty, true);

            // Subscribe
            DragDropManager.AddDragInitializeHandler(AssociatedObject, DragInitializeHandler);
            DragDropManager.AddDragDropCompletedHandler(AssociatedObject, DragDropCompletedHandler);
            DragDropManager.AddGiveFeedbackHandler(AssociatedObject, GiveFeedbackHandler);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            DragDropManager.RemoveDragInitializeHandler(AssociatedObject, DragInitializeHandler);
            DragDropManager.RemoveDragDropCompletedHandler(AssociatedObject, DragDropCompletedHandler);
            DragDropManager.RemoveGiveFeedbackHandler(AssociatedObject, GiveFeedbackHandler);
        }

        void DragInitializeHandler(object sender, DragInitializeEventArgs args)
        {
            if (args.Handled) return;

            // Init default effect
            args.AllowedEffects = ElementDragHandler.SupportedEffects;

            // Prepare for dragging
            DataTemplate template;
            object payloadData;
            ElementDragHandler.PrepareForDragging(AssociatedObject, out payloadData, out template);

            // Generate payload with data
            var payload = DragDropPayloadManager.GeneratePayload(null);
            payload.SetData(DragSourcePayloadKey, payloadData);
            args.Data = payload;

            // Construct drag visual
            args.DragVisual = new ContentControl
                {
                    Content = AssociatedObject.DataContext, 
                    ContentTemplate = template,
                };
            
            // Handled
            args.Handled = true;
        }

        void DragDropCompletedHandler(object sender, DragDropCompletedEventArgs args)
        {
            if (args.Data == null || args.Handled) return;

            var payloadData = DragDropPayloadManager.GetDataFromObject(args.Data, DragSourcePayloadKey);

            var dropSucceeded = args.Effects != DragDropEffects.None;
            ElementDragHandler.ProcessDragCompleted(AssociatedObject, dropSucceeded, payloadData, args);

            // Mark as handled
            args.Handled = true;
        }

        static void GiveFeedbackHandler(object sender, GiveFeedbackEventArgs args)
        {
            if (args.Handled) return;

            // Change cursor depending on whether current target supports actions with dragged object
            args.SetCursor(args.Effects == DragDropEffects.None
                ? Cursors.No
                : Cursors.Hand);

            args.Handled = true;
        }
    }
}
