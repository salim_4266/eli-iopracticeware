﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Behavior that adds a confirmation dialog to any selection changes.
    /// Adds a selected item in context that is seperate from the Control's selected item.
    /// This behavior synchronizes the selected item in context and the Control's selected item depending on the Confirmation Dialog result.
    /// </summary>
    public class ConfirmSelectionChangeBehavior : Behavior<Selector>
    {
        #region DependencyProperty: ConfirmCommandContent

        public string ConfirmCommandContent
        {
            get { return (string) GetValue(ConfirmCommandContentProperty); }
            set { SetValue(ConfirmCommandContentProperty, value); }
        }

        public static readonly DependencyProperty ConfirmCommandContentProperty = DependencyProperty.Register("ConfirmCommandContent", typeof (object), typeof (ConfirmSelectionChangeBehavior), new PropertyMetadata("There are unsaved changes. Are you sure you want to discard your changes?"));

        #endregion

        #region DependencyProperty: IsConfirmationRequired

        public bool IsConfirmationRequired
        {
            get { return (bool) GetValue(IsConfirmationRequiredProperty); }
            set { SetValue(IsConfirmationRequiredProperty, value); }
        }

        public static readonly DependencyProperty IsConfirmationRequiredProperty = DependencyProperty.Register("IsConfirmationRequired", typeof (bool), typeof (ConfirmSelectionChangeBehavior), new PropertyMetadata(true));

        #endregion

        #region DependencyProperty: CancelEditOnConfirm

        public bool CancelEditOnConfirm
        {
            get { return (bool) GetValue(CancelEditOnConfirmProperty); }
            set { SetValue(CancelEditOnConfirmProperty, value); }
        }

        public static readonly DependencyProperty CancelEditOnConfirmProperty = DependencyProperty.Register("CancelEditOnConfirm", typeof (bool), typeof (ConfirmSelectionChangeBehavior), new PropertyMetadata(true));

        #endregion

        #region DependencyProperty: SelectedItem

        /// <summary>
        /// Gets or sets the selected item in context.
        /// </summary>
        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof (object), typeof (ConfirmSelectionChangeBehavior), new PropertyMetadata(null, OnPropertySelectedItemChanged));

        private static void OnPropertySelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ConfirmSelectionChangeBehavior) d;
            
            bool hasChangedSelectionFromCode = behavior.AssociatedObject.SelectedItem != behavior.SelectedItem;
            if (hasChangedSelectionFromCode && behavior.SelectedItem != null)
            {
                if (behavior.IsConfirmationRequired 
                    && behavior.SelectedItem.CastTo<IChangeTracking>().IsChanged 
                    && !behavior._ignoreCurrentItemChangedIfHasDefaultSelectedIndex)
                {
                    behavior._contextItemSelectionHelper.ConfirmCommand.Execute();
                }
                else
                {
                    behavior.AssociatedObject.SelectedItem = behavior.SelectedItem;
                }
            }

            if (behavior.ItemSelectedCommand != null && !behavior.SuppressItemSelectedCommand) behavior.ItemSelectedCommand.Execute();
        }

        #endregion

        #region DependencyProperty: ItemSelectedCommand

        /// <summary>
        /// Command that is fired when the item in context has changed.
        /// </summary>
        public ICommand ItemSelectedCommand
        {
            get { return GetValue(ItemSelectedCommandProperty) as ICommand; }
            set { SetValue(ItemSelectedCommandProperty, value); }
        }

        public static readonly DependencyProperty ItemSelectedCommandProperty = DependencyProperty.Register("ItemSelectedCommand", typeof(ICommand), typeof(ConfirmSelectionChangeBehavior));

        #endregion

        #region DependencyProperty: SuppressItemSelectedCommand

        public bool SuppressItemSelectedCommand
        {
            get { return (bool)GetValue(SuppressItemSelectedCommandProperty); }
            set { SetValue(SuppressItemSelectedCommandProperty, value); }
        }

        public static readonly DependencyProperty SuppressItemSelectedCommandProperty = DependencyProperty.Register("SuppressItemSelectedCommand", typeof(bool), typeof(ConfirmSelectionChangeBehavior));

        #endregion

        private PropertyChangeNotifier _selectedItemNotifier;
        private ControlItemChangeHelper _controlItemChangeHelper;
        private ContextItemSelectionHelper _contextItemSelectionHelper;

        // If initialized with a selected item value, then we do not need to a confirmation dialog
        private bool _ignoreCurrentItemChangedIfHasDefaultSelectedIndex;

        protected override void OnAttached()
        {
            // 'CurrentChanging' will be triggered whenever the ItemsSource is changing.
            // This is needed because if the SelectedItem may be initialized before the ItemsSource value changed event.
            // (Order of events) CurrentChanging -> SelectedItemChange (if selectedItem/Index initialized in xaml) -> ItemsSourceChange
            AssociatedObject.Items.CurrentChanging += WeakDelegate.MakeWeak<CurrentChangingEventHandler>(OnItemsCurrentChanging);
            
            _selectedItemNotifier = new PropertyChangeNotifier(AssociatedObject, Selector.SelectedItemProperty)
                .AddValueChanged(OnControlSelectedItemChanged);

            _controlItemChangeHelper = new ControlItemChangeHelper(this);
            _contextItemSelectionHelper = new ContextItemSelectionHelper(this);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.Items.CurrentChanging -= WeakDelegate.MakeWeak<CurrentChangingEventHandler>(OnItemsCurrentChanging);
            _selectedItemNotifier.RemoveValueChanged(OnControlSelectedItemChanged);
            base.OnDetaching();
        }

        /// <summary>
        /// Method called when the ItemsSource is being set.  It suppresses a confirmation dialog when
        /// resetting the ItemsSource causes a Item Selection Change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnItemsCurrentChanging(object sender, CurrentChangingEventArgs e)
        {
            if (!e.IsCancelable)
            {
                _ignoreCurrentItemChangedIfHasDefaultSelectedIndex = true;
            }
        }

        /// <summary>
        /// Method triggered when the Control's selected item has changed.
        /// This will show a confirmation dialog if required, then update the selected item in context if possible.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnControlSelectedItemChanged(object sender, EventArgs e)
        {
            if (SelectedItem != AssociatedObject.SelectedItem)
            {
                if (_ignoreCurrentItemChangedIfHasDefaultSelectedIndex)
                {
                    _ignoreCurrentItemChangedIfHasDefaultSelectedIndex = false;
                    SelectedItem = AssociatedObject.SelectedItem;
                    return;
                }

                if (IsConfirmationRequired && SelectedItem.CastTo<IChangeTracking>().IsChanged)
                {
                    _controlItemChangeHelper.ConfirmCommand.Execute();
                }
                else
                {
                    SelectedItem = AssociatedObject.SelectedItem;
                }
            }
        }

        #region helper classes

        /// <summary>
        /// Helper class that exposes a ConfirmCommand to be used when the Control's selected item has changed.
        /// Reverts to the previous selection if 'Cancel' is selected, 
        /// or sets the "Item in Context" to the new selection, if 'Accept' is selected.
        /// </summary>
        private class ControlItemChangeHelper
        {
            private readonly ConfirmSelectionChangeBehavior _behavior;
            private ConfirmCommand _confirmCommand;

            public ControlItemChangeHelper(ConfirmSelectionChangeBehavior behavior)
            {
                _behavior = behavior;

                // ConfirmCommand is composed of AcceptSelectionChange command and CancelSelectionChange command.
                ConfirmCommand = Command.Create(ExecuteConfirmCommand);
                AcceptSelectionChange = Command.Create(ExecuteAcceptSelectionChange);
                CancelSelectionChange = Command.Create(ExecuteCancelSelectionChange);
            }

            private void ExecuteConfirmCommand()
            {
                if (_confirmCommand == null)
                {
                    _confirmCommand = new ConfirmCommand
                        {
                            CancelCommand = CancelSelectionChange,
                            Command = AcceptSelectionChange
                        };
                }
                _confirmCommand.Content = _behavior.ConfirmCommandContent;
                _confirmCommand.Execute();
            }

            private void ExecuteAcceptSelectionChange()
            {
                if (_behavior.CancelEditOnConfirm)
                {
                    _behavior.SelectedItem.CastTo<IEditableObject>().CancelEdit();
                    _behavior.SelectedItem.CastTo<IChangeTracking>().AcceptChanges();
                }
                _behavior.SelectedItem = _behavior.AssociatedObject.SelectedItem;
            }

            private void ExecuteCancelSelectionChange()
            {
                var isConfirmEnabled = _behavior.IsConfirmationRequired;
                _behavior.IsConfirmationRequired = false;
                _behavior.AssociatedObject.SelectedItem = _behavior.SelectedItem;
                _behavior.IsConfirmationRequired = isConfirmEnabled;
            }

            public ICommand ConfirmCommand { get; private set; }
            private ICommand AcceptSelectionChange { get; set; }
            private ICommand CancelSelectionChange { get; set; }
        }

        /// <summary>
        /// Helper class that exposes a ConfirmCommand to be used when the "Item in Context" has changed.
        /// Reverts to the previous selection if 'Cancel' is selected, 
        /// or sets the Control's Selected Item to the new selection, if 'Accept' is selected.
        /// </summary>
        private class ContextItemSelectionHelper
        {
            private readonly ConfirmSelectionChangeBehavior _behavior;
            private ConfirmCommand _confirmCommand;

            public ContextItemSelectionHelper(ConfirmSelectionChangeBehavior behavior)
            {
                _behavior = behavior;

                // ConfirmCommand is composed of AcceptSelectionChange command and CancelSelectionChange command.
                ConfirmCommand = Command.Create(ExecuteConfirmCommand);
                AcceptSelectionChange = Command.Create(ExecuteAcceptSelectionChange);
                CancelSelectionChange = Command.Create(ExecuteCancelSelectionChange);
            }

            private void ExecuteConfirmCommand()
            {
                if (_confirmCommand == null)
                {
                    _confirmCommand = new ConfirmCommand
                        {
                            CancelCommand = CancelSelectionChange,
                            Command = AcceptSelectionChange
                        };
                }

                _confirmCommand.Content = _behavior.ConfirmCommandContent;
                _confirmCommand.Execute();
            }

            private void ExecuteAcceptSelectionChange()
            {
                if (_behavior.CancelEditOnConfirm)
                {
                    _behavior.AssociatedObject.SelectedItem.CastTo<IEditableObject>().CancelEdit();
                    _behavior.AssociatedObject.SelectedItem.CastTo<IChangeTracking>().AcceptChanges();
                }

                var isConfirmEnabled = _behavior.IsConfirmationRequired;
                _behavior.IsConfirmationRequired = false;
                _behavior.AssociatedObject.SelectedItem = _behavior.SelectedItem;
                _behavior.IsConfirmationRequired = isConfirmEnabled;
                
            }

            private void ExecuteCancelSelectionChange()
            {
                _behavior.SelectedItem = _behavior.AssociatedObject.SelectedItem;
            }

            public ICommand ConfirmCommand { get; private set; }
            private ICommand AcceptSelectionChange { get; set; }
            private ICommand CancelSelectionChange { get; set; }
        }
    }

    #endregion
}
