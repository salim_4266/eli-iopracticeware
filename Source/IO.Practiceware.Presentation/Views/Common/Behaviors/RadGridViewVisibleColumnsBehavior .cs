﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class RadGridViewVisibleColumnsBehavior : Behavior<RadGridView>
    {
        private bool _isUpdatingGridVisibleColumns;

        private readonly List<PropertyChangeNotifier> _notifiers = new List<PropertyChangeNotifier>();

        public static readonly DependencyProperty VisibleColumnsProperty =
          DependencyProperty.Register("VisibleColumns", typeof(ICollection<string>), typeof(RadGridViewVisibleColumnsBehavior), new PropertyMetadata(default(RadGridView), OnVisibleColumnsChanged));

        public ICollection<string> VisibleColumns
        {
            get { return (ICollection<string>)GetValue(VisibleColumnsProperty); }
            set { SetValue(VisibleColumnsProperty, value); }
        }

        private static void OnVisibleColumnsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (RadGridViewVisibleColumnsBehavior)d;

            if (e.OldValue == e.NewValue) return;

            var notifyCollectionChanged = e.OldValue as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(instance.OnVisibleColumnsCollectionChanged);
            }
            else
            {
                var notifyPropertyChanged = e.OldValue as INotifyPropertyChanged;
                if (notifyPropertyChanged != null)
                {
                    notifyPropertyChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(instance.OnVisibleColumnsCollectionChanged);
                }
            }

            notifyCollectionChanged = e.NewValue as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(instance.OnVisibleColumnsCollectionChanged);
            }
            else
            {
                var notifyPropertyChanged = e.NewValue as INotifyPropertyChanged;
                if (notifyPropertyChanged != null)
                {
                    notifyPropertyChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(instance.OnVisibleColumnsCollectionChanged);
                }
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Columns.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnColumnsCollectionChanged);
            UpdateVisibleColumns();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Columns.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnColumnsCollectionChanged);
        }

        private void UpdateVisibleColumns()
        {
            if (_isUpdatingGridVisibleColumns || VisibleColumns == null) return;

            _isUpdatingGridVisibleColumns = true;
            try
            {
                VisibleColumns.Clear();

                foreach (var column in AssociatedObject.Columns)
                {
                    if (column.IsVisible)
                    {
                        VisibleColumns.Add(column.UniqueName);
                    }
                }
            }
            finally
            {
                _isUpdatingGridVisibleColumns = false;
            }
        }

        private void OnColumnsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach(var notifier in _notifiers)
                {
                    notifier.RemoveValueChanged(OnGridColumnIsVisibleChanged);
                }
                _notifiers.Clear();

                foreach (GridViewColumn column in e.NewItems)
                {
                    _notifiers.Add(new PropertyChangeNotifier(column, GridViewColumn.IsVisibleProperty).AddValueChanged(OnGridColumnIsVisibleChanged));
                }
            }
        }

        private void OnGridColumnIsVisibleChanged(object sender, EventArgs e)
        {
            UpdateVisibleColumns();
        }


        private void OnVisibleColumnsCollectionChanged(object sender, EventArgs e)
        {
            UpdateGridVisibleColumns();
        }

        private void UpdateGridVisibleColumns()
        {
            if (_isUpdatingGridVisibleColumns || VisibleColumns == null) return;

            _isUpdatingGridVisibleColumns = true;
            try
            {
                foreach (var column in AssociatedObject.Columns)
                {
                    column.IsVisible = VisibleColumns.Contains(column.UniqueName);
                }
                foreach (var column in VisibleColumns.ToArray())
                {
                    if (AssociatedObject.Columns[column] == null) VisibleColumns.Remove(column);
                }
            }
            finally
            {
                _isUpdatingGridVisibleColumns = false;
            }
        }
    }
}