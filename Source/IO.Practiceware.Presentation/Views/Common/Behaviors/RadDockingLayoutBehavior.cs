﻿using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    // TODO The following is not currently used
    // Just adding it for now until the other issues around the RadDocking control are resolved

    [DataContract]
    public class RadDockingLayoutViewModel : IViewModel
    {
        [DataMember]
        public Byte[] LayoutByteStream { get; set; }
    }

    public class RadDockingLayoutBehavior : Behavior<RadDocking>
    {
        public static readonly DependencyProperty ApplicationSettingsServiceProperty = DependencyProperty.Register("ApplicationSettingsService", typeof(IApplicationSettingsService), typeof(RadDockingLayoutBehavior));
        public static readonly DependencyProperty RadDockingLayoutNameProperty = DependencyProperty.Register("RadDockingLayoutName", typeof(string), typeof(RadDockingLayoutBehavior));

        private ApplicationSettingContainer<RadDockingLayoutViewModel> _settingsContainer;
        private readonly object _syncRoot = new object();

        public object RadDockingLayoutName
        {
            get { return GetValue(RadDockingLayoutNameProperty); }
            set { SetValue(RadDockingLayoutNameProperty, value); }
        }

        protected override void OnAttached()
        {
            if (RadDockingLayoutName == null) throw new ArgumentException("RadDockingLayoutName must be set.");

            AssociatedObject.IsVisibleChanged += OnIsVisibleChanged;
            AssociatedObject.Loaded += OnRadDockingLoaded;
            base.OnAttached();
        }

        void OnRadDockingLoaded(object sender, RoutedEventArgs e)
        {
            var settingName = string.Format(ApplicationSetting.RadDockingLayout, RadDockingLayoutName);
            System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() =>
                {
                    lock (_syncRoot)
                    {
                        var settingsContainer = ApplicationSettings.Cached.GetSetting<RadDockingLayoutViewModel>(settingName, ApplicationContext.Current.ComputerName, UserContext.Current.UserDetails.IfNotNull(ud => ud.Id));

                        if (settingsContainer != null)
                        {
                            _settingsContainer = settingsContainer;
                            using (var ms = new MemoryStream(_settingsContainer.Value.LayoutByteStream))
                            {
                                Dispatcher.Invoke(() => AssociatedObject.LoadLayout(ms));
                            }
                        }
                        else
                        {
                            _settingsContainer = new ApplicationSettingContainer<RadDockingLayoutViewModel>
                                {
                                    Value = new RadDockingLayoutViewModel(),
                                    MachineName = ApplicationContext.Current.ComputerName,
                                    Name = settingName
                                };
                        }
                    }
                });
        }

        void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!AssociatedObject.IsVisible)
            {
                System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() =>
                    {
                        lock (_syncRoot)
                        {
                            using (var ms = new MemoryStream())
                            {
                                Dispatcher.Invoke(() => AssociatedObject.SaveLayout(ms));
                                _settingsContainer.Value.LayoutByteStream = ms.ToArray();
                            }

                            ApplicationSettings.Cached.SetSetting(_settingsContainer);
                        }
                    });
            }
        }
    }
}
