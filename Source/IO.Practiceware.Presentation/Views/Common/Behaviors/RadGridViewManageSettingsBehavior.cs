using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Soaf;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public interface IManageGridSettingsImplementation
    {
        IEnumerable<FilterSetting> SaveColumnFilters();
        void LoadColumnFilters(IEnumerable<FilterSetting> savedSettings);
    }

    /// <summary>
    /// Allows to hook into saving and restoring grid settings (filters, sorting, grouping and etc.)
    /// </summary>
    public class RadGridViewManageSettingsBehavior : Behavior<RadGridView>, IManageGridSettingsImplementation
    {
        public IManageGridSettingsImplementation SetImplementationTo
        {
            get { return (IManageGridSettingsImplementation)GetValue(SetImplementationToProperty); }
            set { SetValue(SetImplementationToProperty, value); }
        }

        public static readonly DependencyProperty SetImplementationToProperty =
            DependencyProperty.Register("SetImplementationTo", typeof(IManageGridSettingsImplementation), typeof(RadGridViewManageSettingsBehavior), new PropertyMetadata());

        protected override void OnAttached()
        {
            base.OnAttached();

            SetImplementationTo = this;
        }

        public void LoadColumnFilters(IEnumerable<FilterSetting> savedSettings)
        {
            ExecuteOnDispatcherThread(() => LoadColumnFiltersInternal(savedSettings));
        }

        public IEnumerable<FilterSetting> SaveColumnFilters()
        {
            IEnumerable<FilterSetting> filterSettings = null;
            ExecuteOnDispatcherThread(() =>
            {
                filterSettings = SaveColumnFiltersInternal();
            });
            return filterSettings;
        }

        private static void ExecuteOnDispatcherThread(Action method)
        {
            // Cannot read AssociatedObject.Dispatcher from background thread
            var dispatcher = System.Windows.Application.Current.Dispatcher;
            if (dispatcher == null || dispatcher.CheckAccess())
            {
                method();
            }
            else
            {
                dispatcher.Invoke(method);
            }
        }

        private IEnumerable<FilterSetting> SaveColumnFiltersInternal()
        {
            IDictionary<string, FilterSetting> settings = new Dictionary<string, FilterSetting>();

            // Save sort order
            foreach (var descriptor in AssociatedObject.SortDescriptors.OfType<ColumnSortDescriptor>())
            {
                settings[descriptor.Column.UniqueName] = new FilterSetting
                {
                    ColumnUniqueName = descriptor.Column.UniqueName,
                    ColumnSortIndex = descriptor.Column.SortingIndex,
                    ColumnSortState = descriptor.Column.SortingState
                };
            }
            
            // Save visible columns
            foreach (var column in AssociatedObject.Columns)
            {
                FilterSetting setting;
                var columnUniqueName = column.UniqueName;
                if (!settings.TryGetValue(columnUniqueName, out setting))
                {
                    setting = new FilterSetting
                    {
                        ColumnUniqueName = columnUniqueName
                    };
                    settings.Add(columnUniqueName, setting);
                }
                setting.IsVisible = column.IsVisible;
            }

            // Save applied filters
            foreach (var columnFilter in AssociatedObject.FilterDescriptors.OfType<IColumnFilterDescriptor>())
            {
                FilterSetting setting;
                var columnUniqueName = columnFilter.Column.UniqueName;
                if (!settings.TryGetValue(columnUniqueName, out setting))
                {
                    setting = new FilterSetting
                    {
                        ColumnUniqueName = columnUniqueName
                    };
                    settings.Add(columnUniqueName, setting);
                }

                setting.SelectedDistinctValues.AddRange(columnFilter.DistinctFilter.DistinctValues);
                setting.SelectedDistinctOperator = columnFilter.DistinctFilter.DistinctValuesComparisonOperator;

                if (columnFilter.FieldFilter.Filter1.IsActive)
                {
                    setting.Filter1 = new FilterDescriptorProxy();
                    setting.Filter1.Operator = columnFilter.FieldFilter.Filter1.Operator;
                    setting.Filter1.Value = columnFilter.FieldFilter.Filter1.Value;
                    setting.Filter1.IsCaseSensitive = columnFilter.FieldFilter.Filter1.IsCaseSensitive;
                }

                setting.FieldFilterLogicalOperator = columnFilter.FieldFilter.LogicalOperator;

                if (columnFilter.FieldFilter.Filter2.IsActive)
                {
                    setting.Filter2 = new FilterDescriptorProxy();
                    setting.Filter2.Operator = columnFilter.FieldFilter.Filter2.Operator;
                    setting.Filter2.Value = columnFilter.FieldFilter.Filter2.Value;
                    setting.Filter2.IsCaseSensitive = columnFilter.FieldFilter.Filter2.IsCaseSensitive;
                }
            }

            return settings.Values;
        }

        private void LoadColumnFiltersInternal(IEnumerable<FilterSetting> savedSettings)
        {
            var filterSettings = savedSettings as IList<FilterSetting> ?? savedSettings.IfNotNull(ss => ss.ToList());

            // First apply sorting (attempting to handle them both at the same time or doing filtering first breaks it)
            AssociatedObject.SortDescriptors.SuspendNotifications();
            AssociatedObject.SortDescriptors.Clear();

            if (filterSettings != null)
            {
                // Restore visibility of columns
                foreach (var setting in filterSettings)
                {
                    var column = AssociatedObject.Columns[setting.ColumnUniqueName];
                    if (column == null) continue;

                    column.IsVisible = setting.IsVisible;
                }

                // Restore sorting
                foreach (var setting in filterSettings
                    .Where(ss => ss.ColumnSortState != SortingState.None)
                    .OrderBy(ss => ss.ColumnSortIndex))
                {
                    var column = AssociatedObject.Columns[setting.ColumnUniqueName];
                    if (column == null) continue;

                    AssociatedObject.SortDescriptors.Add(new ColumnSortDescriptor
                    {
                        Column = column,
                        SortDirection = setting.ColumnSortState == SortingState.Ascending
                            ? ListSortDirection.Ascending
                            : ListSortDirection.Descending
                    });
                }
            }

            AssociatedObject.SortDescriptors.ResumeNotifications();

            // Now apply filtering
            AssociatedObject.FilterDescriptors.SuspendNotifications();
            AssociatedObject.FilterDescriptors.Clear();

            if (filterSettings != null)
            {
                foreach (var setting in filterSettings)
                {
                    var column = AssociatedObject.Columns[setting.ColumnUniqueName];
                    if (column == null) continue;

                    var columnFilter = column.ColumnFilterDescriptor;

                    foreach (object distinctValue in setting.SelectedDistinctValues)
                    {
                        columnFilter.DistinctFilter.AddDistinctValue(distinctValue);
                    }
                    columnFilter.DistinctFilter.DistinctValuesComparisonOperator = setting.SelectedDistinctOperator;

                    if (setting.Filter1 != null)
                    {
                        columnFilter.FieldFilter.Filter1.Operator = setting.Filter1.Operator;
                        columnFilter.FieldFilter.Filter1.Value = setting.Filter1.Value;
                        columnFilter.FieldFilter.Filter1.IsCaseSensitive = setting.Filter1.IsCaseSensitive;
                    }

                    columnFilter.FieldFilter.LogicalOperator = setting.FieldFilterLogicalOperator;

                    if (setting.Filter2 != null)
                    {
                        columnFilter.FieldFilter.Filter2.Operator = setting.Filter2.Operator;
                        columnFilter.FieldFilter.Filter2.Value = setting.Filter2.Value;
                        columnFilter.FieldFilter.Filter2.IsCaseSensitive = setting.Filter2.IsCaseSensitive;
                    }
                }
            }

            AssociatedObject.FilterDescriptors.ResumeNotifications();
        }
    }

    public class FilterDescriptorProxy
    {
        public FilterOperator Operator { get; set; }
        public object Value { get; set; }
        public bool IsCaseSensitive { get; set; }
    }

    public class FilterSetting
    {
        public string ColumnUniqueName { get; set; }
        
        public List<object> SelectedDistinctValues = new List<object>();
        public FilterOperator SelectedDistinctOperator { get; set; }

        public FilterDescriptorProxy Filter1 { get; set; }
        public FilterCompositionLogicalOperator FieldFilterLogicalOperator { get; set; }
        public FilterDescriptorProxy Filter2 { get; set; }

        /// <summary>
        /// Additionally store sort order
        /// </summary>
        public SortingState ColumnSortState { get; set; }
        public int ColumnSortIndex { get; set; }

        /// <summary>
        /// Column visibility state
        /// </summary>
        public bool IsVisible { get; set; }

        public FilterSetting()
        {
            ColumnSortIndex = -1;
            ColumnSortState = SortingState.None;
            SelectedDistinctOperator = FilterOperator.IsEqualTo;
            IsVisible = false;
        }
    }
}