﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public class TextBoxFilterBehavior : Behavior<TextBox>
    {
        public ICommand ChangingTextCommand
        {
            get { return GetValue(ChangingTextCommandProperty) as ICommand; }
            set { SetValue(ChangingTextCommandProperty, value); }
        }

        public static readonly DependencyProperty ChangingTextCommandProperty = DependencyProperty.Register("ChangingTextCommand", typeof(ICommand), typeof(TextBoxFilterBehavior));

        protected override void OnAttached()
        {
            AssociatedObject.PreviewKeyUp += WeakDelegate.MakeWeak<KeyEventHandler>(ProcessCommand);
        }

        private void ProcessCommand(object sender, KeyEventArgs propertyChangedEventArgs)
        {
            if (ChangingTextCommand != null) ChangingTextCommand.Execute(AssociatedObject.Text);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewKeyUp -= WeakDelegate.MakeWeak<KeyEventHandler>(ProcessCommand);
        }
    }
}
