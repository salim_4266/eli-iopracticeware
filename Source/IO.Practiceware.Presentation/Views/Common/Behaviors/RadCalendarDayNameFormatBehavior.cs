﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// RadCalendar behavior that allows the Day Name to be formatted using the enum, DayNameFormat.
    /// </summary>
    /// <remarks>
    /// By default, the RadCalendar will format the Day Names using a 3 letter prefix such as Mon, Tues, Wed etc.
    /// </remarks>
    public class RadCalendarDayNameFormatBehavior : Behavior<RadCalendar>
    {
        private const string CultureRegion = "en-US";

        public static readonly DependencyProperty DayNameFormatProperty =
          DependencyProperty.Register("DayNameFormat", typeof(DayNameFormat), typeof(RadCalendarDayNameFormatBehavior), new PropertyMetadata(DayNameFormat.Short, OnDayNameFormatChanged));

        public DayNameFormat DayNameFormat
        {
            get { return (DayNameFormat)GetValue(DayNameFormatProperty); }
            set { SetValue(DayNameFormatProperty, value); }
        }

        protected override void OnAttached()
        {
            SetCalendarDayNameFormat(DayNameFormat);
            base.OnAttached();
        }

        private static void OnDayNameFormatChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue == e.NewValue) return;

            var instance = (RadCalendarDayNameFormatBehavior)d;
            instance.SetCalendarDayNameFormat((DayNameFormat)e.NewValue);
        }

        private void SetCalendarDayNameFormat(DayNameFormat dayNameFormat)
        {
            if (AssociatedObject != null)
            {
                AssociatedObject.Culture = new CultureInfo(CultureRegion) {DateTimeFormat = new DateTimeFormatInfo {AbbreviatedDayNames = dayNameFormat.DayValues()}};
            }
        }
    }

    #region DayName Helper Classes
    /// <summary>
    /// Attribute for the DayNameFormat enum.  Allows any configuration of day name values for each defined 'DayNameFormat'.
    /// </summary>
    public class DayNameValuesAttribute : Attribute
    {
        public string[] DayValues { get; set; }

        public DayNameValuesAttribute(IEnumerable<string> days)
        {
            DayValues = days.ToArray();
        }

        public DayNameValuesAttribute(string day1, string day2, string day3, string day4, string day5, string day6, string day7)
        {
            DayValues = new[] { day1, day2, day3, day4, day5, day6, day7 };
        }
    }

    public enum DayNameFormat
    {
        [DayNameValuesAttribute("S", "M", "T", "W", "T", "F", "S")]
        FirstLetter,
        [DayNameValuesAttribute("Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")]
        FirstTwoLetters,
        [DayNameValuesAttribute("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")]
        Full,
        [DayNameValuesAttribute("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")]
        Short
    }

    /// <summary>
    /// Extension methods for the DayNameFormat enumeration.
    /// </summary>
    public static class DayNameFormatExtensions
    {
        public static string[] DayValues(this DayNameFormat dayNameFormat)
        {
            DayNameValuesAttribute values = dayNameFormat.GetDayNameValuesAttribute();
            if (values.DayValues.Length != 7)
            {
                throw new Exception(string.Format("Unexpected number of Day Values in the DayNameFormat, {0}", dayNameFormat));
            }
            return values.DayValues;
        }

        private static DayNameValuesAttribute GetDayNameValuesAttribute(this DayNameFormat dayNameFormat)
        {
            try
            {
                var type = typeof(DayNameFormat);
                var memInfo = type.GetMember(dayNameFormat.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(DayNameValuesAttribute), false);
                return ((DayNameValuesAttribute)attributes[0]);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Could not get the {0} attribute from {1}", typeof(DayNameFormat).FullName, dayNameFormat), ex);
            }
        }
    }
    #endregion
}
