﻿using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Calendar;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Behavior for a multi-column, MonthView RadCalendar.  Allows the selected month column to be highlighted while the others are made more transparent.
    /// </summary>
    public class RadCalendarSingleSelectBehavior : Behavior<RadCalendar>
    {
        private CalendarView _selectedCalendarView;

        private static void MakeTransparent(CalendarView calendarView) { calendarView.Opacity = 0.4; }
        private static void MakeOpaque(CalendarView calendarView) { calendarView.Opacity = 1; }

        private bool IsInitialized { get; set; }
        private List<CalendarView> CalendarViews { get; set; }
        
        private CalendarView SelectedCalendarView
        {
            get { return _selectedCalendarView; }
            set
            {
                _selectedCalendarView = value;
                if (_selectedCalendarView != null) { MakeOpaque(_selectedCalendarView); }
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += WeakDelegate.MakeWeak<RoutedEventHandler>(RadCalendarLoaded);
            AssociatedObject.IsVisibleChanged += WeakDelegate.MakeWeak<DependencyPropertyChangedEventHandler>(RadCalendarIsVisibleChanged);
            AssociatedObject.DisplayModeChanged += WeakDelegate.MakeWeak<EventHandler<CalendarModeChangedEventArgs>>(RadCalendarDisplayModeChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            // Just in case it hasn't been initialized
            AssociatedObject.Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(RadCalendarLoaded);
            AssociatedObject.IsVisibleChanged -= WeakDelegate.MakeWeak<DependencyPropertyChangedEventHandler>(RadCalendarIsVisibleChanged);
            AssociatedObject.DisplayModeChanged -= WeakDelegate.MakeWeak<EventHandler<CalendarModeChangedEventArgs>>(RadCalendarDisplayModeChanged);
        }

        private void RadCalendarIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!IsInitialized && AssociatedObject.DisplayMode == DisplayMode.MonthView && AssociatedObject.IsVisible && AssociatedObject.IsLoaded)
            {
                InitializeChildCalendarViews();
            }
        }

        private void RadCalendarDisplayModeChanged(object sender, CalendarModeChangedEventArgs e)
        {
            if (!IsInitialized && AssociatedObject.DisplayMode == DisplayMode.MonthView && AssociatedObject.IsLoaded && AssociatedObject.IsVisible)
            {
                InitializeChildCalendarViews();
            }
        }

        private void RadCalendarLoaded(object sender, RoutedEventArgs e)
        {
            if (!IsInitialized && AssociatedObject.DisplayMode == DisplayMode.MonthView && AssociatedObject.IsVisible)
            {
                InitializeChildCalendarViews();
            }
        }

        private void CalendarViewGotKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            SelectedCalendarView = sender as CalendarView;
            CalendarViews.Where(cv => !cv.Equals(SelectedCalendarView)).ToList().ForEach(MakeTransparent);
        }

        private void InitializeChildCalendarViews()
        {
            #region HelperFunction
            Func<List<CalendarView>> getChildCalendarViews = () => AssociatedObject.ChildrenOfType<CalendarView>().Where(cv => cv.Items.Count > 0).ToList();
            #endregion

            CalendarViews = getChildCalendarViews();
            SelectedCalendarView = GetSelectedCalendarView(CalendarViews);

            CalendarViews.Where(cv => !cv.Equals(SelectedCalendarView)).ToList().ForEach(MakeTransparent);
            CalendarViews.ToList().ForEach(cv =>
                {
                    cv.GotKeyboardFocus += WeakDelegate.MakeWeak<KeyboardFocusChangedEventHandler>(CalendarViewGotKeyboardFocus,
                        handler => cv.GotKeyboardFocus -= handler);
                });
            MakeOpaque(CalendarViews.First());

            // Set as initialized
            IsInitialized = true;
            
            // Remove event subscriptions which trigger initialization
            AssociatedObject.Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(RadCalendarLoaded);
            AssociatedObject.IsVisibleChanged -= WeakDelegate.MakeWeak<DependencyPropertyChangedEventHandler>(RadCalendarIsVisibleChanged);
            AssociatedObject.DisplayModeChanged -= WeakDelegate.MakeWeak<EventHandler<CalendarModeChangedEventArgs>>(RadCalendarDisplayModeChanged);
        }

        private CalendarView GetSelectedCalendarView(List<CalendarView> calendarViews)
        {
            #region HelperFunction
            Func<int?> getSelectedCalendarViewIndex = () =>
            {
                if (AssociatedObject.SelectedDate.HasValue)
                {
                    // Display Date Start represents the first element in the CalendarViews collection.
                    int startMonthIndex = AssociatedObject.DisplayDate.Month;
                    int selectedMonthIndex = AssociatedObject.SelectedDate.Value.Month;

                    int resultIndex = selectedMonthIndex - startMonthIndex;
                    if (resultIndex >= 0 && resultIndex < calendarViews.Count) { return resultIndex; }
                }
                return null;
            };
            #endregion

            if (calendarViews != null && calendarViews.Count > 0)
            {
                var collectionIndex = getSelectedCalendarViewIndex();
                if (collectionIndex.HasValue) { return calendarViews[collectionIndex.Value]; }
            }

            return null;
        }
    }
}
