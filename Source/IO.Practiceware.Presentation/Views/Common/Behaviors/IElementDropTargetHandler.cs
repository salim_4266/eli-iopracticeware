﻿using System;
using System.Windows;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    public interface IElementDropTargetHandler
    {
        /// <summary>
        /// Support payload type of the drop target
        /// </summary>
        Type SupportedPayloadType { get; }

        void ProcessItemDragOver(FrameworkElement frameworkElement, object payloadData, DragEventArgs args);
        void ProcessItemDropped(FrameworkElement frameworkElement, object payloadData, DragEventArgs args);
        void ProcessItemDragLeave(FrameworkElement frameworkElement, object payloadData, DragEventArgs args);
        void ProcessItemDragEnter(FrameworkElement frameworkElement, object payloadData, DragEventArgs args);
    }
}