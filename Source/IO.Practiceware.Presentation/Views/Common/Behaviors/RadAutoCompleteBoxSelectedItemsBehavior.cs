﻿using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    ///   Synchronizes the SelectedItems collection on a RadAutoCompleteBox with an IsSelected property on each item.
    /// </summary>
    public class RadAutoCompleteBoxSelectedItemsBehavior : Behavior<RadAutoCompleteBox>
    {
        private IEnumerable _itemsSource;
        private ObservableCollection<object> _selectedItems;
        private bool _isSelectedItemsChanging;
        private PropertyChangeNotifier _itemsSourceNotifier;

        public string IsSelectedPath { get; set; }

        protected override void OnAttached()
        {
            base.OnAttached();

            _selectedItems = new ObservableCollection<object>();
            _selectedItems.CollectionChanged += OnSelectedItemsChanged;
            AssociatedObject.SelectedItems = _selectedItems;

            _itemsSourceNotifier = new PropertyChangeNotifier(AssociatedObject, RadAutoCompleteBox.ItemsSourceProperty)
                .AddValueChanged(OnItemsSourceChanged);

            OnItemsSourceChanged(null, null);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _itemsSourceNotifier.RemoveValueChanged(OnItemsSourceChanged);
        }

        private void OnSelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_isSelectedItemsChanging) return;

            _isSelectedItemsChanging = true;

            var changedItems = AssociatedObject.ItemsSource.OfType<object>().Where(o => (bool)Binder.GetValue(o, IsSelectedPath) != _selectedItems.Contains(o)).ToArray();

            foreach (object item in changedItems)
            {
                Binder.SetValue(item, IsSelectedPath, _selectedItems.Contains(item));
            }
            _isSelectedItemsChanging = false;

            Dispatcher.BeginInvoke(UpdateSelectedItemsFromItemsSource);
        }

        private void OnItemsSourceChanged(object sender, EventArgs e)
        {
            var collectionChanged = _itemsSource as INotifyCollectionChanged;
            if (collectionChanged != null)
            {
                collectionChanged.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnItemsSourceCollectionChanged);
            }

            _itemsSource = AssociatedObject.ItemsSource;

            collectionChanged = _itemsSource as INotifyCollectionChanged;
            if (collectionChanged != null)
            {
                collectionChanged.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnItemsSourceCollectionChanged);
            }
            
            if (_itemsSource != null)
            {
                RegisterItems();
                UpdateSelectedItemsFromItemsSource();
            }
        }

        private void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RegisterItems();
            UpdateSelectedItemsFromItemsSource();
        }

        private void RegisterItems()
        {
            foreach (INotifyPropertyChanged item in _itemsSource)
            {
                item.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemPropertyChanged);
                item.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnItemPropertyChanged);
            }
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateSelectedItemsForItem(sender);
        }

        private void UpdateSelectedItemsFromItemsSource()
        {
            _selectedItems.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnSelectedItemsChanged);

            var source = AssociatedObject.ItemsSource.OfType<object>().ToList();
            _selectedItems = _selectedItems.OrderBy(source.IndexOf).ToExtendedObservableCollection();

            _selectedItems.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(OnSelectedItemsChanged);

            AssociatedObject.SelectedItems = _selectedItems;
            
            foreach (var item in AssociatedObject.ItemsSource)
            {
                UpdateSelectedItemsForItem(item);
            }
        }

        private void UpdateSelectedItemsForItem(object sender)
        {
          if (_isSelectedItemsChanging) return;

            _isSelectedItemsChanging = true;

            var isSelected = (bool)Binder.GetValue(sender, IsSelectedPath);
            if (!isSelected) _selectedItems.Remove(sender);
            else if (!_selectedItems.Contains(sender)) _selectedItems.Add(sender);

            _isSelectedItemsChanging = false;
        }
    }
}