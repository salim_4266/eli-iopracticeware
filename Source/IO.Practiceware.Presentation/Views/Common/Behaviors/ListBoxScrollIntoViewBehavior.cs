﻿using Soaf.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Behavior to scroll the first selected item into view, for the attached ListBox.
    /// All subsequent selected items will not auto scroll into view.
    /// </summary>
    public static class ListBoxScrollIntoViewBehavior
    {
        public static readonly DependencyProperty ScrollFirstSelectedItemIntoViewProperty = DependencyProperty.RegisterAttached("ScrollFirstSelectedItemIntoView", typeof (bool), typeof (ListBox), new PropertyMetadata(false, OnScrollFirstSelectedItemIntoViewPropertyChanged));

        private static void OnScrollFirstSelectedItemIntoViewPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var listBox = d as ListBox;
            if (listBox == null || !(args.NewValue is bool))
            {
                return;
            }

            if ((bool) args.NewValue)
            {
                listBox.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
            }
        }

        public static void OnSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            var listBox = sender as ListBox;
            if(listBox != null && listBox.SelectedItem != null)
            {
                listBox.ScrollIntoView(listBox.SelectedItem);
                listBox.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnSelectionChanged);
            }
        }

        public static void SetScrollFirstSelectedItemIntoView(ListBox listbox, bool value)
        {
            listbox.SetValue(ScrollFirstSelectedItemIntoViewProperty, value);
        }
    }
}
