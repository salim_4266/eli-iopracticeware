﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;
using Soaf.ComponentModel;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Scrolls Telerik's GridView based on how focus moves between it's child elements
    /// </summary>
    /// <remarks>
    /// To be used when native scrolling of the grid is not possible (or broken via styles)
    /// </remarks>
    public class RadGridViewCustomScrollingBehavior : Behavior<RadGridView>
    {
        private ScrollViewer _scrollViewer;

        /// <summary>
        /// Margin from either top or bottom which triggers manual scrolling to selected row
        /// </summary>
        public int ScrollMargin { get; set; }

        #region VerticalOffset Property

        public static DependencyProperty VerticalOffsetProperty =
            DependencyProperty.RegisterAttached("VerticalOffset",
                typeof(double),
                typeof(RadGridViewCustomScrollingBehavior),
                new UIPropertyMetadata(0.0, OnVerticalOffsetChanged));

        public static void SetVerticalOffset(FrameworkElement target, double value)
        {
            target.SetValue(VerticalOffsetProperty, value);
        }

        public static double GetVerticalOffset(FrameworkElement target)
        {
            return (double)target.GetValue(VerticalOffsetProperty);
        }

        #endregion

        public RadGridViewCustomScrollingBehavior()
        {
            ScrollMargin = 120;
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += WeakDelegate.MakeWeak<RoutedEventHandler>(OnGridLoaded);
            AssociatedObject.GotFocus += WeakDelegate.MakeWeak<RoutedEventHandler>(OnFollowFocus);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Loaded -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnGridLoaded);
            AssociatedObject.GotFocus -= WeakDelegate.MakeWeak<RoutedEventHandler>(OnFollowFocus);
        }

        private void OnGridLoaded(object sender, RoutedEventArgs e)
        {
            _scrollViewer = AssociatedObject.GetChildren<ScrollViewer>().FirstOrDefault();
        }

        private void OnFollowFocus(object sender, RoutedEventArgs e)
        {
            var uiElement = e.OriginalSource as FrameworkElement;
            if (uiElement == null || _scrollViewer == null) return;

            var position = uiElement.TranslatePoint(new Point(uiElement.ActualWidth, uiElement.ActualHeight), _scrollViewer);

            var scrollingUp = position.Y <= ScrollMargin;
            if (scrollingUp
                || _scrollViewer.ActualHeight - position.Y <= ScrollMargin)
            {
                var newOffset = _scrollViewer.VerticalOffset + position.Y;
                newOffset += scrollingUp
                    ? -ScrollMargin
                    : -_scrollViewer.ActualHeight + ScrollMargin;
                AnimateScroll(_scrollViewer, newOffset);
            }
        }

        private static void OnVerticalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var scrollViewer = d as ScrollViewer;
            if (scrollViewer != null)
            {
                scrollViewer.ScrollToVerticalOffset((double)e.NewValue);
            }
        }

        private static void AnimateScroll(ScrollViewer scrollViewer, double newOffset)
        {
            // Bound new offset according to scroll viewer
            if (newOffset < 0)
            {
                newOffset = 0;
            }

            // Don't start animation if we have already scrolled to required position
            if ((int)(newOffset - scrollViewer.VerticalOffset) == 0)
            {
                return;
            }

            var verticalAnimation = new DoubleAnimation
            {
                From = scrollViewer.VerticalOffset,
                To = newOffset,
                Duration = new Duration(TimeSpan.FromMilliseconds(300))
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(verticalAnimation);
            Storyboard.SetTarget(verticalAnimation, scrollViewer);
            Storyboard.SetTargetProperty(verticalAnimation, new PropertyPath(VerticalOffsetProperty));
            storyboard.Begin();
        }
    }
}