﻿using System.Windows;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Binds a specified DependencyProperty on a specified Target to a specified Value. Useful for binding parent properties from a child where the child is not in the naming scope/visibility of the parent.
    /// </summary>
    public class BindPropertyBehavior : Behavior<DependencyObject>
    {
        public static readonly DependencyProperty PropertyProperty = DependencyProperty.Register("Property", typeof(DependencyProperty), typeof(BindPropertyBehavior), new PropertyMetadata(OnPropertyChanged));

        public DependencyProperty Property
        {
            get { return GetValue(PropertyProperty) as DependencyProperty; }
            set { SetValue(PropertyProperty, value); }
        }

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BindPropertyBehavior)d).ClearBinding(null, e.OldValue as DependencyProperty);
            ((BindPropertyBehavior)d).SetBinding(null, e.NewValue as DependencyProperty);
        }

        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(DependencyObject), typeof(BindPropertyBehavior), new PropertyMetadata(OnTargetChanged));

        public DependencyObject Target
        {
            get { return GetValue(TargetProperty) as DependencyObject; }
            set { SetValue(TargetProperty, value); }
        }

        private static void OnTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BindPropertyBehavior)d).ClearBinding(e.OldValue as DependencyObject, null);
            ((BindPropertyBehavior)d).SetBinding(e.NewValue as DependencyObject, null);
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(object), typeof(BindPropertyBehavior), new FrameworkPropertyMetadata());

        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private void ClearBinding(DependencyObject target, DependencyProperty property)
        {
            if (target == null) target = Target;
            if (property == null) property = Property;

            if (target == null || property == null) return;

            BindingOperations.ClearBinding(target, property);
        }

        private void SetBinding(DependencyObject target, DependencyProperty property)
        {
            if (target == null) target = Target;
            if (property == null) property = Property;

            if (target == null || property == null) return;

            BindingOperations.SetBinding(target, property, new Binding("Value") { Source = this, Mode = BindingMode.TwoWay });
        }
    }
}
