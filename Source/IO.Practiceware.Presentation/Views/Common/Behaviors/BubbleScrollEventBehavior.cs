﻿using Soaf.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Used on sub-controls of an expander to bubble the mouse wheel scroll event up 
    /// </summary>
    public sealed class BubbleScrollEventBehavior : Behavior<UIElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseWheel += WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseWheel -= WeakDelegate.MakeWeak<MouseWheelEventHandler>(AssociatedObjectPreviewMouseWheel);
            base.OnDetaching();
        }

        void AssociatedObjectPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            e2.RoutedEvent = UIElement.MouseWheelEvent;
            AssociatedObject.RaiseEvent(e2);
        }
    }
}
