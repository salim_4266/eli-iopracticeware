﻿using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Behaviors
{
    /// <summary>
    /// Static behavior to allow a grid row to be selected on Right Mouse Button Down.
    /// </summary>
    public static class RadGridViewRightClickSelectBehavior
    {
        public static readonly DependencyProperty AllowRightClickSelectProperty = DependencyProperty.RegisterAttached("AllowRightClickSelect", typeof(bool), typeof(RadGridView), new PropertyMetadata(false, OnAllowRightClickSelectChanged));

        public static void SetAllowRightClickSelect(RadGridView radGridView, bool value)
        {
            radGridView.SetValue(AllowRightClickSelectProperty, value);
        }

        private static void OnAllowRightClickSelectChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var radGridView = d as RadGridView;
            if (radGridView == null || !(args.NewValue is bool))
            {
                return;
            }

            if ((bool)args.NewValue)
            {
                radGridView.MouseRightButtonDown += OnMouseRightButtonDown;
            }
        }

        static void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var s = e.OriginalSource as FrameworkElement;
            var parentRow = s.ParentOfType<GridViewRow>();
            if (parentRow != null)
            {
                parentRow.IsSelected = true;
            }
        }
    }
}
