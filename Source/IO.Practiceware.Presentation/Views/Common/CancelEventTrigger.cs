﻿using System;
using System.Windows;
using EventTrigger = System.Windows.Interactivity.EventTrigger;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class CancelEventTrigger : EventTrigger
    {
        protected override void OnEvent(EventArgs eventArgs)
        {
            var routedEventArgs = eventArgs as RoutedEventArgs;
            if (routedEventArgs != null) routedEventArgs.Handled = true;
        }
    }
}