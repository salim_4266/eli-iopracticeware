﻿using Soaf;
using System;
using System.Windows;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class EventTriggerEventArgsProxy : TriggerAction<DependencyObject>
    {
        public EventArgs Args
        {
            get { return GetValue(ArgsProperty) as EventArgs; }
            set { SetValue(ArgsProperty, value); }
        }

        // ReSharper disable StaticFieldInGenericType
        // "This means that for a generic class C<T> which has a static field X, the values of C<int>.X and C<string>.X have completely different, independent values.
        // This is intended.
        public static readonly DependencyProperty ArgsProperty = DependencyProperty.Register("Args", typeof(EventArgs), typeof(EventTriggerEventArgsProxy), new PropertyMetadata(null));
        // ReSharper restore StaticFieldInGenericType

        protected override void Invoke(object parameter)
        {
            var eventArgs = parameter as EventArgs;
            if (eventArgs == null) throw new ArgumentException("\'Args\' {0} is not of Type \'EventArgs\'".FormatWith(Args));

            Args = eventArgs;
        }

        protected override void OnDetaching()
        {
            ClearValue(ArgsProperty);
            base.OnDetaching();
        }
    }
}
