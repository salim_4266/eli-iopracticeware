﻿using IO.Practiceware.Model;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class PermissionExtensions
    {
        /// <summary>
        /// Ensures that user has specified permission.
        /// Will show a UI alert if user doesn't have the permission
        /// </summary>
        /// <param name="permission">The permission.</param>
        /// <param name="alertNoPermission">if set to <c>true</c> displays UI alert of user permission missing.</param>
        /// <returns></returns>
        public static bool EnsurePermission(this PermissionId permission, bool alertNoPermission = true)
        {
            var errorMessages = permission.Validate();
            if(errorMessages.Any())
            {
                if (alertNoPermission)
                {
                    var alertMessage = errorMessages.Join(x => x.Value, Environment.NewLine);
                    InteractionManager.Current.Alert(alertMessage);   
                }
                return false;
            }

            return true;
        }
    }
}
