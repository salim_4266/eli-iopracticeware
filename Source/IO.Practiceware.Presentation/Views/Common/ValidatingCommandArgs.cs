﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// An object used to pass relavent data to 'validating' event handlers subscribed to controls that support validation events.
    /// </summary>
    public class ValidatingCommandArgs
    {
        public ValidatingCommandArgs()
        {
            IsValid = true;
        }

        public ValidatingCommandArgs(object dataContext)
            : this()
        {
            DataContext = dataContext;
        }

        /// <summary>
        /// The data object bound to the current row being validated
        /// </summary>
        public object DataContext { get; protected set; }

        /// <summary>
        /// Set this to notify whether or not the validation context is valid. Default is true.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Set to true to notify the rest of the events in the event route that the current event has been handled.  
        /// Note: it is up to the actual events to decide whether or not they want to respect this value.
        /// Most of the time setting this to true will prevent the rest of the events in the event chain from executing
        /// </summary>
        public bool Handled { get; set; }
    }

    public class RowValidatingCommandArgs : ValidatingCommandArgs
    {
        private void Init()
        {
            ValidationResults = new List<ValidationResult>();
        }

        public RowValidatingCommandArgs()
        {
            Init();
        }

        /// <summary>
        /// Create a new <see cref="RowValidatingCommandArgs"/> object.
        /// </summary>
        /// <param name="oldValues">A dictionary to represent a list of Properties and their old values from the grid's cells</param>
        /// <param name="editOperationType">A string value to describe the type of operation performed that caused the event to fire (e.g. Edit, Insert)</param>
        /// <param name="rowDataContext">An object which represents the data context of the row at the time the event fired</param>
        public RowValidatingCommandArgs(IDictionary<string, object> oldValues, string editOperationType, object rowDataContext)
            : base(rowDataContext)
        {
            Init();
            OldValues = oldValues;
            EditOperationType = editOperationType ?? "Unknown";
        }

        /// <summary>
        /// A list of Properties and their old values from the grid's cells
        /// </summary>
        public IDictionary<string, object> OldValues { get; private set; }

        /// <summary>
        /// Add any validation results here to display to the grid
        /// </summary>
        public IList<ValidationResult> ValidationResults
        {
            get;
            private set;
        }

        public string EditOperationType { get; private set; }
    }

    public class CellValidatingCommandArgs : ValidatingCommandArgs
    {
        public CellValidatingCommandArgs()
        {
        }

        /// <summary>
        /// Create a new <see cref="CellValidatingCommandArgs"/> object.
        /// </summary>
        /// <param name="rowDataContext">An object which represents the data context of the row at the time the event fired</param>
        public CellValidatingCommandArgs(object rowDataContext)
            : base(rowDataContext)
        {
        }

        /// <summary>
        /// Create a new <see cref="CellValidatingCommandArgs"/> object.
        /// </summary>
        /// <param name="rowDataContext">An object which represents the data context of the row at the time the event fired</param>
        /// <param name="newValue"></param>
        /// <param name="oldValue"></param>
        /// <param name="propertyName"></param>
        public CellValidatingCommandArgs(object rowDataContext, object newValue, object oldValue, string propertyName)
            : base(rowDataContext)
        {
            NewValue = newValue;
            OldValue = oldValue;
            PropertyName = propertyName;
        }

        public object OldValue { get; private set; }

        public object NewValue { get; private set; }

        /// <summary>
        /// Custom error message set by the user for use by the original control that fired the event
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The name of the property that is databound to the column this cell belongs to.  If the column is not databound then null will be the value
        /// </summary>
        public string PropertyName { get; private set; }       
    }

    public class CellValidatedCommandArgs : ValidatingCommandArgs
    {
          /// <summary>
        /// Create a new <see cref="CellValidatingCommandArgs"/> object.
        /// </summary>
        /// <param name="rowDataContext">An object which represents the data context of the row at the time the event fired</param>
        public CellValidatedCommandArgs(object rowDataContext)
            : base(rowDataContext)
        {
        }

        /// <summary>
        /// Custom error message set by the user for use by the original control that fired the event
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The name of the property that is databound to the column this cell belongs to.  If the column is not databound then null will be the value
        /// </summary>
        public string PropertyName { get; set; }

    }
}
