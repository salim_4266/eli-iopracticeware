﻿using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common
{
	public class CalendarExtensions
    {
        #region DependencyProperty: TrackCalendarSelectedDates

        public static bool GetTrackCalendarSelectedDates(DependencyObject obj)
		{
			return (bool)obj.GetValue(TrackCalendarSelectedDatesProperty);
		}

		public static void SetTrackCalendarSelectedDates(DependencyObject obj, bool value)
		{
			obj.SetValue(TrackCalendarSelectedDatesProperty, value);
		}

		public static readonly DependencyProperty TrackCalendarSelectedDatesProperty =
			DependencyProperty.RegisterAttached("TrackCalendarSelectedDates", typeof(bool), typeof(CalendarExtensions), new PropertyMetadata(false, OnTrackCalendarSelectedDatesPropertyChanged));

        private static void OnTrackCalendarSelectedDatesPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var calendar = d as RadCalendar;
            if (calendar != null)
            {
                if ((bool)e.NewValue)
                {
                    var extensions = new CalendarExtensions(calendar);
                    SetExtensions(calendar, extensions);
                    extensions.UpdateSelectedDates();
                }
                else
                {
                    GetExtensions(calendar).Detach();
                    calendar.ClearValue(ExtensionsProperty);
                }
            }
        }

        private void Detach()
        {
            _calendar.SelectionChanged -= WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnCalendarSelectionChanged);
        }

        #endregion

        #region DependencyProperty: SelectedDates

        public static IEnumerable<DateTime> GetSelectedDates(DependencyObject obj)
		{
			return (IEnumerable<DateTime>)obj.GetValue(SelectedDatesProperty);
		}

		public static void SetSelectedDates(DependencyObject obj, IEnumerable<DateTime> value)
		{
			obj.SetValue(SelectedDatesProperty, value);
		}

		public static readonly DependencyProperty SelectedDatesProperty =
			DependencyProperty.RegisterAttached("SelectedDates", typeof(IEnumerable<DateTime>), typeof(CalendarExtensions), new PropertyMetadata(Enumerable.Empty<DateTime>(), OnSelectedDatesPropertyChanged));

        private static void OnSelectedDatesPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var extensions = GetExtensions(d);

            if (extensions != null)
            {
                var selectedDates = GetSelectedDates(d);
                if (selectedDates != null)
                {
                    extensions.UpdateCalendarSelectedDates(GetSelectedDates(d).ToList());
                }
                extensions.UpdateSelectedDates();
            }
        }

        private void UpdateCalendarSelectedDates(List<DateTime> dates)
        {
            if (_updatingSelectedDates) return;

            _updatingSelectedDates = true;

            foreach (var selectedDate in _calendar.SelectedDates.ToList())
            {
                if (!dates.Contains(selectedDate))
                {
                    _calendar.SelectedDates.Remove(selectedDate);
                }
            }

            foreach (var date in dates)
            {
                if (!_calendar.SelectedDates.Contains(date))
                {
                    _calendar.SelectedDates.Add(date);
                }
            }

            _updatingSelectedDates = false;
        }

        #endregion

        #region DependencyProperty: Extensions

        private static CalendarExtensions GetExtensions(DependencyObject obj)
		{
			return (CalendarExtensions)obj.GetValue(ExtensionsProperty);
		}

		private static void SetExtensions(DependencyObject obj, CalendarExtensions value)
		{
			obj.SetValue(ExtensionsProperty, value);
		}

		private static readonly DependencyProperty ExtensionsProperty =
			DependencyProperty.RegisterAttached("Extensions", typeof(CalendarExtensions), typeof(CalendarExtensions), null);

        #endregion

		private readonly RadCalendar _calendar;
		private bool _updatingSelectedDates;

		private CalendarExtensions(RadCalendar calendar)
		{
			_calendar = calendar;
            _calendar.SelectionChanged += WeakDelegate.MakeWeak<SelectionChangedEventHandler>(OnCalendarSelectionChanged);
		}

		private void OnCalendarSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			UpdateSelectedDates();
		}

		private void UpdateSelectedDates()
		{
			if (!_updatingSelectedDates)
			{
				_updatingSelectedDates = true;
				SetSelectedDates(_calendar, _calendar.SelectedDates);
				_updatingSelectedDates = false;
			}
		}
	}
}
