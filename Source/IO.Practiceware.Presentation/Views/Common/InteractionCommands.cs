﻿using System;
using System.Linq;
using System.Windows.Input;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// Common commands for interactions.
    /// </summary>
    public class InteractionCommands
    {
        private static readonly ICommand CancelCommand = Command.Create<IInteractionContext>(i => i.Complete(false));

        private static readonly ICommand CloseCommand = Command.Create<IInteractionContext>(i => i.Complete(true));

        /// <summary>
        /// Calls InteractionContext.Complete(false).
        /// </summary>
        /// <value>The cancel.</value>
        public static ICommand Cancel
        {
            get { return CancelCommand; }
        }

        /// <summary>
        /// Gets InteractionContext.Complete(true).
        /// </summary>
        /// <value>The close.</value>
        public static ICommand Close
        {
            get { return CloseCommand; }
        }
    }
}
