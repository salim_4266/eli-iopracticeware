﻿using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class DragDropHelpers
    {
        public static void PositionItemInCollectionAccordingToMouseLocation<TItem>(TItem item, ICollection<TItem> collection, 
            Panel panel, Point relativeCursorPositionOnPanel, bool verticalAlignedChildren = true)
        {
            var currentIndexOnPanel = collection.IndexOf(item);

            // See if we want to drop in the middle
            int insertionIndex = LocateDropInsertionIndexOnPanel(panel, relativeCursorPositionOnPanel);

            // Since we will be moving our current item -> update the index
            var correlatedInsertionIndex = insertionIndex > currentIndexOnPanel
                                               ? insertionIndex - 1
                                               : insertionIndex;

            // If we are close to edge of items panel -> insert it as first item
            if ((!verticalAlignedChildren && relativeCursorPositionOnPanel.X <= 20)
                || (verticalAlignedChildren && relativeCursorPositionOnPanel.Y <= 20))
            {
                // Insert it as the first item
                correlatedInsertionIndex = 0;
            }

            if (correlatedInsertionIndex >= 0 && currentIndexOnPanel != correlatedInsertionIndex)
            {
                var observableCollection = collection as ObservableCollection<TItem>;
                if (observableCollection != null)
                {
                    observableCollection.Move(currentIndexOnPanel, correlatedInsertionIndex);   
                }
                else
                {
                    // TODO: we can also check it with IList and use ArrayListAdapter to move it
                    throw new NotSupportedException("Cannot move item to cursor if collection is not an observable");
                }
            }
        }

        /// <summary>
        /// Calculates the drop insertion index for new item based on where the cursor is
        /// </summary>
        public static int LocateDropInsertionIndexOnPanel(Panel dropPanel, Point relativeCursorPositionOnPanel)
        {
            // By default it is either first or last item
            int insertionIndex = -1;

            // VisualTreeHelper.HitTest doesn't work for us to find the UI item we are hovering mouse on, so what we do is:
            // 1. Map relative position to screen coordinates (this is to compare it with UI item absolution position) 
            // 2. Take absolute coordinates of every UI item and build a relative position using containers absolute position
            // 3. If point is in UI items's box -> insert it after it
            var targetPointOnScreen = dropPanel.PointToScreen(relativeCursorPositionOnPanel);
            foreach (var existingWidget in dropPanel.Children.OfType<FrameworkElement>())
            {
                var relativePosition = targetPointOnScreen - existingWidget.PointToScreen(new Point(0, 0));

                // Ensure it is within bounds
                if (relativePosition.X > 0 && relativePosition.Y > 0
                    && relativePosition.X < existingWidget.ActualWidth
                    && relativePosition.Y < existingWidget.ActualHeight)
                {
                    insertionIndex = dropPanel.Children.IndexOf(existingWidget) + 1;
                    break;
                }
            }
            return insertionIndex;
        } 
    }
}
