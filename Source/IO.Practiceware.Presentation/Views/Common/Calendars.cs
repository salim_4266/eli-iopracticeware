﻿using System.Windows;
using System.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class CustomDayButtonStyleSelector : StyleSelector
    {
        public Style CustomDayButtonStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            return CustomDayButtonStyle ?? base.SelectStyle(item, container);
        }
    }
}
