﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Soaf;
using Soaf.ComponentModel;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class CustomTabNavigation
    {
        #region ATTACHABLE PROPERTIES

        public static int? GetEnableSkipTabStopsKeyNavigation(DependencyObject obj)
        {
            return (int?)obj.GetValue(EnableSkipTabStopsKeyNavigationProperty);
        }

        public static void SetEnableSkipTabStopsKeyNavigation(DependencyObject obj, int? value)
        {
            obj.SetValue(EnableSkipTabStopsKeyNavigationProperty, value);
        }

        public static readonly DependencyProperty EnableSkipTabStopsKeyNavigationProperty =
            DependencyProperty.RegisterAttached("EnableSkipTabStopsKeyNavigation", typeof(int?), typeof(CustomTabNavigation),
            new UIPropertyMetadata(OnEnableSkipTabStopsKeyNavigationPropertyChanged));

        //attach methods to keydown events
        static void OnEnableSkipTabStopsKeyNavigationPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;
               
            KeyEventHandler handler = (sender, eventArgs) => SkipTabStopsOnTabKeydown(sender, eventArgs, (int)e.NewValue);

            if (e.NewValue != null)
            {
                element.KeyDown += handler;
            }
            else
            {
                element.KeyDown -= handler;
            }
        }


        /// <summary>
        /// Enter key navigation - on enter keypress, down; on enter-shift, up (again, in relation to the control with current focus)
        /// </summary>
        public static bool GetEnableCustomEnterKeyNavigation(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableCustomEnterKeyNavigationProperty);
        }

        public static void SetEnableCustomEnterKeyNavigation(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableCustomEnterKeyNavigationProperty, value);
        }

        public static readonly DependencyProperty EnableCustomEnterKeyNavigationProperty =
            DependencyProperty.RegisterAttached("EnableCustomEnterKeyNavigation", typeof(bool), typeof(CustomTabNavigation),
            new UIPropertyMetadata(OnEnableCustomEnterKeyNavigationPropertyChanged));

        //attach methods to keydown events
        static void OnEnableCustomEnterKeyNavigationPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;

            if ((bool)e.NewValue)
            {
                element.KeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.KeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(DownOnEnterKeydown);
            }
            else
            {
                element.KeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.KeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(DownOnEnterKeydown);
            }
        }


        /// <summary>
        /// Enter key navigation - on enter keypress, down; on enter-shift, up (again, in relation to the control with current focus)
        /// </summary>
        public static bool GetEnableCustomReturnKeyNavigation(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableCustomReturnKeyNavigationProperty);
        }

        public static void SetEnableCustomReturnKeyNavigation(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableCustomReturnKeyNavigationProperty, value);
        }

        public static readonly DependencyProperty EnableCustomReturnKeyNavigationProperty =
            DependencyProperty.RegisterAttached("EnableCustomReturnKeyNavigation", typeof(bool), typeof(CustomTabNavigation),
            new UIPropertyMetadata(OnEnableCustomReturnKeyNavigationPropertyChanged));

        //attach methods to keydown events
        static void OnEnableCustomReturnKeyNavigationPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;

            if ((bool)e.NewValue)
            {
                element.KeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.KeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(ReturnOnEnterKeydown);
            }
            else
            {
                element.KeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.KeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(ReturnOnEnterKeydown);
            }
        }

        //Use preview, unless the control you're attaching to already has PreviewKeyDown events defined (i.e., RadAutoCompleteBox)
        public static bool GetEnableCustomEnterKeyNavigationOnPreview(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableCustomEnterKeyNavigationOnPreviewProperty);
        }

        public static void SetEnableCustomEnterKeyNavigationOnPreview(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableCustomEnterKeyNavigationOnPreviewProperty, value);
        }

        public static readonly DependencyProperty EnableCustomEnterKeyNavigationOnPreviewProperty =
            DependencyProperty.RegisterAttached("EnableCustomEnterKeyNavigationOnPreview", typeof(bool), typeof(CustomTabNavigation),
            new UIPropertyMetadata(OnEnableCustomEnterKeyNavigationOnPreviewPropertyChanged));


        //Attach method to PreviewKeyDown 
        static void OnEnableCustomEnterKeyNavigationOnPreviewPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;


            if ((bool)e.NewValue)
            {
                element.PreviewKeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.PreviewKeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(DownOnEnterKeydown);
            }
            else
            {
                element.PreviewKeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
                element.PreviewKeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(DownOnEnterKeydown);
            }
        }



        //Use preview, unless the control you're attaching to already has PreviewKeyDown events defined (i.e., RadAutoCompleteBox)
        public static bool GetEnableCustomShiftEnterKeyNavigationOnPreview(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableCustomShiftEnterKeyNavigationOnPreviewProperty);
        }

        public static void SetEnableCustomShiftEnterKeyNavigationOnPreview(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableCustomShiftEnterKeyNavigationOnPreviewProperty, value);
        }

        public static readonly DependencyProperty EnableCustomShiftEnterKeyNavigationOnPreviewProperty =
            DependencyProperty.RegisterAttached("EnableCustomShiftEnterKeyNavigationOnPreview", typeof(bool), typeof(CustomTabNavigation),
            new UIPropertyMetadata(OnEnableCustomShiftEnterKeyNavigationOnPreviewPropertyChanged));


        //attach method to PreviewKeyDown 
        static void OnEnableCustomShiftEnterKeyNavigationOnPreviewPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;


            if ((bool)e.NewValue)
            {
                element.PreviewKeyDown += WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
            }
            else
            {
                element.PreviewKeyDown -= WeakDelegate.MakeWeak<KeyEventHandler>(UpOnShiftEnterKeyDown);
            }
        }


        #endregion



        #region METHODS
        /// <summary>
        /// look through the elements returned by PredictFocus(right) until you run out of visible elements.  Then set focus to the last element.
        /// </summary>
        private static void SkipTabStopsOnTabKeydown(object sender, KeyEventArgs e, int? n)
        {
            if (e.Key.Equals(Key.Tab) && Keyboard.Modifiers != ModifierKeys.Shift)
            {
                var element = sender as UIElement;
                for (int i = 0; i < n; i++)
                {
                    if (element != null)
                    {
                        element = element.PredictFocus(FocusNavigationDirection.Right) as UIElement;
                    }
                }
                if (element != null) element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                e.Handled = true;
            }
        }


        /// WPF is not perfect at finding the next "down" or "up" control, so if it fails to find anything, go right until null, then next; or go left until null, then prev
        /// this way, this'll work 99% of the time because FocusNavigationDirection.Next and FocusNavigationDirection.Previous will always return a value (whereas, up/down/left/right, might not)
        static void ReturnOnEnterKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                var element = sender as UIElement;
                if (element != null)
                {
                    element = GetTopElement(element);


                    if (GetFarthestElement(FocusNavigationDirection.Right, element).PredictFocus(FocusNavigationDirection.Right) != null)
                        element = GetFarthestElement(FocusNavigationDirection.Right, element).PredictFocus(FocusNavigationDirection.Right) as UIElement;
                    if (element != null) element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    e.Handled = true;
                }
            }
        }

        /// WPF is not perfect at finding the next "down" or "up" control, so if it fails to find anything, go right until null, then next; or go left until null, then prev
        /// this way, this'll work 99% of the time because FocusNavigationDirection.Next and FocusNavigationDirection.Previous will always return a value (whereas, up/down/left/right, might not)
        static void DownOnEnterKeydown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                var element = sender as UIElement;
                if (element != null)
                {
                    element = GetTopElement(element);
                    var testDown = element.PredictFocus(FocusNavigationDirection.Down) as UIElement;

                    //if testDown was not found, or if it's a child of element for whatever reason, find the right-most UI object and send focus to right-most's next object
                    //temporary workaround - we don't want to enter onto a checkbox - FocusNavigation.Down doesn't work quite right with checkboxes...
                    if (testDown == null || testDown.IsDescendantOf(element) || testDown.Is(typeof(CheckBox)))
                    {
                        if (GetFarthestElement(FocusNavigationDirection.Right, element).PredictFocus(FocusNavigationDirection.Right) != null)
                            element = GetFarthestElement(FocusNavigationDirection.Right, element).PredictFocus(FocusNavigationDirection.Right) as UIElement;
                        if (element != null) element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        e.Handled = true;
                        return;
                    }
                    //if testDown was found, send focus there
                    element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    e.Handled = true;
                }
            }
        }


        static void UpOnShiftEnterKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                var element = sender as UIElement;
                if (element != null)
                {
                    var testUp = element.PredictFocus(FocusNavigationDirection.Up) as UIElement;

                    if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                    {

                        if (testUp != null)
                        {
                            if (element.IsAncestorOf(testUp))
                                element = testUp;
                        }
                        if (testUp == null)
                        {
                            if (GetFarthestElement(FocusNavigationDirection.Left, element).PredictFocus(FocusNavigationDirection.Left) as UIElement != null)
                                element = GetFarthestElement(FocusNavigationDirection.Left, element).PredictFocus(FocusNavigationDirection.Left) as UIElement;


                            if (element != null) element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                            e.Handled = true;
                            return;
                        }
                        element.MoveFocus(new TraversalRequest(FocusNavigationDirection.Up));
                        e.Handled = true;
                    }
                }
            }
        }



        /// <summary>
        /// Get the second to farthest element in some direction (this should only be used for FocusNavigationDirection.Left, Right, Up, Down; )
        /// </summary>
        public static UIElement GetFarthestElement(FocusNavigationDirection direction, UIElement reference)
            {
            if (direction == FocusNavigationDirection.Next || direction == FocusNavigationDirection.Previous)
            {
                throw new NotSupportedException("Do not call GetFarthestElement using FocusNavigationDirection.Next or FocusNavigationDirection.Next as the direction");
            }
            var testSkip = reference.PredictFocus(direction) as UIElement;

            if (testSkip == null)
            {
                return reference;
            }
            var test = testSkip.PredictFocus(direction) as UIElement;

            if (test != null)
            {
                
                return GetFarthestElement(direction, testSkip);
            }
            
            return reference;
        }

        public static UIElement GetTopElement(UIElement reference)
        {
            var testSkip = reference.PredictFocus(FocusNavigationDirection.Right) as UIElement;

            if (testSkip == null || !reference.IsAncestorOf(testSkip))
                return reference;
            return GetTopElement(testSkip);
        }
        #endregion
    }
}
