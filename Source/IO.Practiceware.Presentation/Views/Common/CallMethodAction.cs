﻿using System.Windows;
using Soaf.Reflection;
using System.Windows.Interactivity;

namespace IO.Practiceware.Presentation.Views.Common
{
    internal class CallMethodAction : TargetedTriggerAction<object>
    {
        public string MethodName { get; set; }

        public static readonly System.Windows.DependencyProperty MethodParameterProperty = System.Windows.DependencyProperty.Register("MethodParameter", typeof(object), typeof(CallMethodAction), new System.Windows.PropertyMetadata(null));
        public object MethodParameter
        {
            get { return GetValue(MethodParameterProperty); }
            set { SetValue(MethodParameterProperty, value); }
        }

        protected override void Invoke(object parameter)
        {
            var arguments = MethodParameter == null ? new object[0] : new[] { MethodParameter };

            Target.Invoke(MethodName, arguments);
        }
    }
}