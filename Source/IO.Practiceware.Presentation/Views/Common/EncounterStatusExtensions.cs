﻿using IO.Practiceware.Model;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class EncounterStatusExtensions
    {
        public static bool EnsureCanCancel(this EncounterStatus encounterStatus, string alertMessage = null)
        {
            bool canCancel = encounterStatus.CanCancel() || (encounterStatus == EncounterStatus.ExamRoom && PermissionId.CancelAppointmentInExamRoom.PrincipalContextHasPermission());
            if (!canCancel)
            {
                if (alertMessage == null) { alertMessage = encounterStatus.GetCanCancelAlertMessage(); }
                InteractionManager.Current.Alert(alertMessage);
            }

            return canCancel;
        }

        public static bool EnsureCanReschedule(this EncounterStatus encounterStatus, bool displayAlert = true, string alertMessage = null)
        {
            bool canReschedule = encounterStatus.CanReschedule();
            if (!canReschedule)
            {
                if( displayAlert)
                {
                    if (alertMessage == null) { alertMessage = encounterStatus.GetCanRescheduleAlertMessage(); }
                    InteractionManager.Current.Alert(alertMessage);
                }               
            }

            return canReschedule;
        }

        public static string GetCanRescheduleAlertMessage(this EncounterStatus encounterStatus)
        {
            return "Cannot reschedule appointment because it has a status of {0}.".FormatWith(encounterStatus.GetDisplayName());
        }

        public static string GetCanCancelAlertMessage(this EncounterStatus encounterStatus)
        {
            return "Cannot cancel appointment because it has a status of {0}.".FormatWith(encounterStatus.GetDisplayName());
        }
    }
}
