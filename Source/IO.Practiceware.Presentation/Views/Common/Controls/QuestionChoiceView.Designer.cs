using System;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{

	partial class QuestionChoiceView : Soaf.Presentation.Controls.WindowsForms.UserControl
	{

		//Required by the Windows Form Designer

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.QuestionPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NoteTextBox = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.Choice2Button = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.Choice1Button = new Soaf.Presentation.Controls.WindowsForms.RadioButton();
			this.QuestionLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.QuestionPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//QuestionPanel
			//
			this.QuestionPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QuestionPanel.Controls.Add(this.NoteTextBox);
			this.QuestionPanel.Controls.Add(this.Choice2Button);
			this.QuestionPanel.Controls.Add(this.Choice1Button);
			this.QuestionPanel.Controls.Add(this.QuestionLabel);
			this.QuestionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.QuestionPanel.Location = new System.Drawing.Point(0, 0);
			this.QuestionPanel.Name = "QuestionPanel";
			this.QuestionPanel.Size = new System.Drawing.Size(356, 48);
			this.QuestionPanel.TabIndex = 0;
			//
			//NoteTextBox
			//
			this.NoteTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NoteTextBox.Location = new System.Drawing.Point(224, 0);
			this.NoteTextBox.Name = "NoteTextBox";
			this.NoteTextBox.Size = new System.Drawing.Size(132, 48);
			this.NoteTextBox.TabIndex = 3;
			//
			//Choice2Button
			//
			this.Choice2Button.AllowUncheck = false;
			this.Choice2Button.Dock = System.Windows.Forms.DockStyle.Left;
			this.Choice2Button.GroupName = "";
			this.Choice2Button.Location = new System.Drawing.Point(172, 0);
			this.Choice2Button.Name = "Choice2Button";
			this.Choice2Button.Size = new System.Drawing.Size(52, 48);
			this.Choice2Button.TabIndex = 2;
			//
			//Choice1Button
			//
			this.Choice1Button.AllowUncheck = false;
			this.Choice1Button.Dock = System.Windows.Forms.DockStyle.Left;
			this.Choice1Button.GroupName = "";
			this.Choice1Button.Location = new System.Drawing.Point(120, 0);
			this.Choice1Button.Name = "Choice1Button";
			this.Choice1Button.Size = new System.Drawing.Size(52, 48);
			this.Choice1Button.TabIndex = 1;
			//
			//QuestionLabel
			//
			this.QuestionLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.QuestionLabel.Location = new System.Drawing.Point(0, 0);
			this.QuestionLabel.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
			this.QuestionLabel.Name = "QuestionLabel";
			this.QuestionLabel.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
			this.QuestionLabel.Size = new System.Drawing.Size(120, 48);
			this.QuestionLabel.TabIndex = 0;
			this.QuestionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
			//
			//QuestionChoiceUserControl
			//
			this.Controls.Add(this.QuestionPanel);
			this.Name = "QuestionChoiceUserControl";
			this.Size = new System.Drawing.Size(356, 48);
			this.QuestionPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel QuestionPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Label QuestionLabel;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox NoteTextBox;
	    internal Soaf.Presentation.Controls.WindowsForms.RadioButton Choice2Button;
	    internal Soaf.Presentation.Controls.WindowsForms.RadioButton Choice1Button;
	}
}
