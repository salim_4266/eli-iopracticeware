﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for HighlightTextControl.xaml
    /// </summary>
    public partial class HighlightTextControl
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(HighlightTextControl), new PropertyMetadata(null));
        public static readonly DependencyProperty HighlightForegroundProperty = DependencyProperty.Register("HighlightForeground", typeof(Brush), typeof(HighlightTextControl), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
        public static readonly DependencyProperty HighlightBackgroundProperty = DependencyProperty.Register("HighlightBackground", typeof(Brush), typeof(HighlightTextControl), new PropertyMetadata(new SolidColorBrush(Colors.Yellow)));
        public static readonly DependencyProperty HighlightTextCacherProperty = DependencyProperty.Register("HighlightTextCacher", typeof(HighlightTextCacher), typeof(HighlightTextControl), new PropertyMetadata(null));
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(HighlightTextControl), new PropertyMetadata(TextAlignment.Center));
        public static readonly DependencyProperty IsHighlightingEnabledProperty = DependencyProperty.Register("IsHighlightingEnabled", typeof(bool), typeof(HighlightTextControl), new PropertyMetadata(true));
        public static readonly DependencyProperty TermsToHighlightProperty = DependencyProperty.Register("TermsToHighlight", typeof(IEnumerable<string>), typeof(HighlightTextControl));

        public HighlightTextControl()
        {
            InitializeComponent();
        }

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        public Brush HighlightForeground
        {
            get { return GetValue(HighlightForegroundProperty) as Brush; }
            set { SetValue(HighlightForegroundProperty, value); }
        }

        public Brush HighlightBackground
        {
            get { return GetValue(HighlightBackgroundProperty) as Brush; }
            set { SetValue(HighlightBackgroundProperty, value); }
        }

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public bool IsHighlightingEnabled
        {
            get { return (bool)GetValue(IsHighlightingEnabledProperty); }
            set { SetValue(IsHighlightingEnabledProperty, value); }
        }

        public IEnumerable<string> TermsToHighlight
        {
            get { return GetValue(TermsToHighlightProperty) as IEnumerable<string>; }
            set { SetValue(TermsToHighlightProperty, value); }
        }
    }
}
