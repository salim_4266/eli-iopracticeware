﻿using System.Windows;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public class DependencyProperties
    {
        /// <summary>
        /// Denotes that UI Element requires attention
        /// </summary>
        public static readonly DependencyProperty RequiresAttentionProperty =
            DependencyProperty.RegisterAttached("RequiresAttention",
            typeof(bool),
            typeof(DependencyProperties),
            new PropertyMetadata(false));

        public static void SetRequiresAttention(UIElement element, bool value)
        {
            element.SetValue(RequiresAttentionProperty, value);
        }

        public static bool GetRequiresAttention(UIElement element)
        {
            return (bool)element.GetValue(RequiresAttentionProperty);
        }
    }
}
