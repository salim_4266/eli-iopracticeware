﻿using System.Drawing;
using System.IO;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.Resources;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Presentation.Interop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Image = System.Windows.Controls.Image;
using IWindow = Soaf.Presentation.IWindow;
using Rectangle = System.Windows.Shapes.Rectangle;
using Task = System.Threading.Tasks.Task;
using Window = IO.Practiceware.Presentation.Views.Common.Controls.Window;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.ViewServices.Home;
using IO.Practiceware.Presentation.Views.Home.Widgets;

[assembly: Component(typeof(Window), typeof(IWindow), Priority = 1)]

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    internal class Window : Soaf.Presentation.Window, ICommonViewFeatureManager
    {
        readonly IExternalFeatureIntegrationManager _externalFeatureIntegrationManager;
        readonly ITimeSheetViewService _timeSheetViewServiceLogout;
        protected const string PartHelp = "PART_Help";
        protected const string PartPageTitle = "PART_PageTitle";
        protected const string PartMoveRectangle = "PART_MoveRectangle";
        protected const string PartWelcomeUserText = "PART_WelcomeUserText";
        protected const string PartWelcomeLocationText = "PART_WelcomeLocationText";
        protected const string PartLocationDropDownMenu = "PART_LocationDropDownMenu";
        protected const string PartPrintScreen = "PART_Print";
        protected const string PartResizeGrid = "PART_ResizeGrid";
        protected const string PartAlerts = "PART_Alerts";
        protected const string PartSettings = "PART_Settings";
        protected const string PartWelcomeUserDropDown = "PART_WelcomeUserDropDown";
        protected const string PartLogOut = "PART_LogOut";
        protected const string Partclose = "PART_Close";
        protected const string PartRefresh = "PART_RefreshButton";
        protected const string PrintPreview_Text = "Print Preview";
        protected const string PartDateTime = "PART_DateTime";

        private static readonly IDictionary<string, bool> HelpPages = new Dictionary<string, bool>();
        private DispatcherTimer _welcomeGreetingTextRefresh;
        private HwndSource _hwndSource;

        private static bool _isNotBzy = true;
        public static bool IsNotBzy { get { return _isNotBzy; } set { _isNotBzy = value; _closeButton.IsEnabled = value; } }

        Button _helpButton;
        Button _alertsButton;
        Button _printButton;
        Rectangle _moveRectangle;
        TextBlock _welcomeUserText;
        TextBlock _locationText;
        TextBlock _pageTitle;
        Grid _resizeGrid;
        Button _settingsButton;
        RadDropDownButton _welcomeUserDropDown;
        Button _logOutButton;
        Button _refreshButton;
        TextBlock _dateTime;
        public static Button _closeButton;

        public static Dispatcher obj = Dispatcher.CurrentDispatcher;
        // We may sometimes override the base.ResizeMode to NoResize in order to support WindowsFormsHosts without ugly window borders. Store the real ResizeMode here.
        ResizeMode? _realResizeMode;
        ICommonViewFeatureHandler _currentViewFeatureHandler;

        static Window()
        {
            var resourceDictionary = System.Windows.Application.Current.SafeLoadComponent<ResourceDictionary>(
                typeof(PresentationBootstrapper).Assembly.BuildComponentUri("/Resources/Window.xaml"));

            System.Windows.Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);

            DefaultStyleKeyProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata(typeof(Window)));
        }



        public Window(IExternalFeatureIntegrationManager externalFeatureIntegrationManager, ITimeSheetViewService timeSheetViewServiceLogout)
        {
            _externalFeatureIntegrationManager = externalFeatureIntegrationManager;
            _timeSheetViewServiceLogout = timeSheetViewServiceLogout;
            Icon = Icons.IopIcon.ToImageSource();
            Loaded += OnWindowLoaded;
            PreviewMouseMove += OnPreviewMouseMoveResetCursor;

            // Set this window instance as view feature manager
            SetValue(CommonViewFeatures.ViewFeatureManagerProperty, this);
        }

        protected override void OnClosed(EventArgs args)
        {
            if (_welcomeGreetingTextRefresh != null)
            {
                _welcomeGreetingTextRefresh.IsEnabled = false;
            }
            base.OnClosed(args);
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            if (!IsLoaded
                && Content.As<DependencyObject>().IfNotNull(d => d.GetChildren<WindowsFormsHost>().Any() || d.GetChildren<WebBrowser>().Any()))
            {
                // WindowsFormsHosts do not support transparency
                AllowsTransparency = false;
            }
        }

        public override void OnApplyTemplate()
        {
            // Locate template parts and handle actions
            _welcomeUserDropDown = GetTemplateChild(PartWelcomeUserDropDown) as RadDropDownButton;

            _closeButton = GetTemplateChild(Partclose) as Button;
            if (_closeButton != null)
            {
                _closeButton.IsEnabled = IsNotBzy;
            }

            _helpButton = GetTemplateChild(PartHelp) as Button;
            if (_helpButton != null)
            {
                _helpButton.Click += OnHelp;
            }

            _logOutButton = GetTemplateChild(PartLogOut) as Button;
            if (_logOutButton != null)
            {
                _logOutButton.Click += OnLogOut;
            }



            _alertsButton = GetTemplateChild(PartAlerts) as Button;
            if (_alertsButton != null)
            {
                _alertsButton.Click += OnAlerts;
            }

            _settingsButton = GetTemplateChild(PartSettings) as Button;
            if (_settingsButton != null)
            {
                _settingsButton.Click += OnSettings;
            }

            _printButton = GetTemplateChild(PartPrintScreen) as Button;
            if (_printButton != null)
            {
                _printButton.Click += OnPrintScreen;
            }

            _moveRectangle = GetTemplateChild(PartMoveRectangle) as Rectangle;
            if (_moveRectangle != null)
            {
                _moveRectangle.PreviewMouseDown += OnMoveRectanglePreviewMouseDown;
            }

            _welcomeUserText = GetTemplateChild(PartWelcomeUserText) as TextBlock;
            if (_welcomeUserText != null)
            {
                _welcomeUserText.PreviewMouseDown += OnMoveRectanglePreviewMouseDown;
            }

            _locationText = GetTemplateChild(PartWelcomeLocationText) as TextBlock;
            if (_locationText != null)
            {
                _locationText.PreviewMouseDown += OnMoveRectanglePreviewMouseDown;
            }

            _pageTitle = GetTemplateChild(PartPageTitle) as TextBlock;
            if (_pageTitle != null)
            {
                _pageTitle.PreviewMouseDown += OnMoveRectanglePreviewMouseDown;
            }

            _refreshButton = GetTemplateChild(PartRefresh) as Button;
            if (_refreshButton != null)
            {
                _refreshButton.Click += OnRefreshScreen;
            }

            _dateTime = GetTemplateChild(PartDateTime) as TextBlock;
            if (_dateTime != null)
            {
                _dateTime.PreviewMouseDown += OnMoveRectanglePreviewMouseDown;
            }

            _resizeGrid = GetTemplateChild(PartResizeGrid) as Grid;
            // Enable resize grid only with AllowsTransparency
            if (_resizeGrid != null)
            {
                if (!AllowsTransparency)
                {
                    // in order to remove the outer border, we can set the base.ResizeMode to NoResize, then handle the drag events ourselves
                    _realResizeMode = ResizeMode;
                    Dispatcher.BeginInvoke(() =>
                                               {
                                                   ResizeMode = ResizeMode.NoResize;
                                                   UpdateResizeMode();
                                               });
                }
                foreach (var resizeRectangle in _resizeGrid.Children.OfType<Rectangle>())
                {
                    resizeRectangle.PreviewMouseDown += OnResizeRectanglePreviewMouseDown;
                    resizeRectangle.MouseMove += OnResizeRectangleMouseMove;
                }
            }
            else
            {
                // Remove cursor reset handler
                PreviewMouseMove -= OnPreviewMouseMoveResetCursor;
            }

            base.OnApplyTemplate();
        }

        private void UpdateResizeMode()
        {
            switch (_realResizeMode)
            {
                case ResizeMode.NoResize:
                    SetButtonVisibility("PART_Maximize", false);
                    SetButtonVisibility("PART_Minimize", false);
                    break;
                case ResizeMode.CanMinimize:
                    SetButtonVisibility("PART_Maximize", false);
                    SetButtonVisibility("PART_Minimize", true);
                    break;
                case ResizeMode.CanResize:
                case ResizeMode.CanResizeWithGrip:
                    SetButtonVisibility("PART_Maximize", true);
                    SetButtonVisibility("PART_Minimize", true);
                    break;
            }
        }

        void OnRefreshWelcomeText(object sender, EventArgs e)
        {
            if (_locationText != null)
            {
                _locationText.Text = UserContext.Current.IfNotNull(c => c.ServiceLocation.IfNotNull(l => " | {0}".FormatWith(l.Item2))) ?? string.Empty;
            }

            if (_welcomeUserDropDown != null)
            {
                _welcomeUserDropDown.Visibility = UserContext.Current != null && UserContext.Current.UserDetails != null
                    ? Visibility.Visible
                    : Visibility.Collapsed;
            }

            if (_welcomeUserText != null)
            {
                string partOfDay;
                var now = DateTime.Now.ToClientTime();
                if (now.Hour < 12)
                {
                    partOfDay = "morning";
                }
                else if (now.Hour < 17)
                {
                    partOfDay = "afternoon";
                }
                else
                {
                    partOfDay = "evening";
                }

                var userInfo = UserContext.Current != null && UserContext.Current.UserDetails != null
                                   ? string.Format(", {0}", UserContext.Current.UserDetails.DisplayName)
                                   : string.Empty;
                _welcomeUserText.Text = string.Format(" Good {0}{1}", partOfDay, userInfo);
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dateTimer = new DispatcherTimer();
            dateTimer.Interval = TimeSpan.FromMilliseconds(1);
            dateTimer.Tick += timer_Tick;
            dateTimer.Start();

            // Init timers to keep greeting up to date
            _welcomeGreetingTextRefresh = new DispatcherTimer(TimeSpan.FromMinutes(2),
                DispatcherPriority.Background, OnRefreshWelcomeText, Dispatcher);
            _welcomeGreetingTextRefresh.Start();
            OnRefreshWelcomeText(this, EventArgs.Empty);
            UserContext.Current.PropertyChanged += delegate
            {
                Dispatcher.Invoke(() => OnRefreshWelcomeText(this, EventArgs.Empty));
            };

            SetIsAboutEnabled();

            // Update common feature state
            UpdateCommonFeatureState();

            // Hide minimize button by default, since we set window to NoResize
            SetButtonVisibility(PartMinimize, false);

            /* Hide the Print Preview button on Print Preview window itself. */
            SetPrintButtonVisibility();

            //If we want a nonresizable window, we hide the resize grid 
            if (IsWindowSetToNotResize())
            {
                _resizeGrid.Visibility = Visibility.Collapsed;
            }

            //If height > primary screen and width > primary screen, maximize
            else if (ActualHeight >= SystemParameters.WorkArea.Height || ActualWidth >= SystemParameters.WorkArea.Width)
            {
                if (ActualHeight >= SystemParameters.WorkArea.Height && ActualWidth >= SystemParameters.WorkArea.Width)
                    WindowState = WindowState.Maximized;

                //override the set width & height based on the dimensions of the screen 
                Height = Math.Min(SystemParameters.WorkArea.Height, ActualHeight);
                Width = Math.Min(SystemParameters.WorkArea.Width, ActualWidth);

                //center the restored window on the primary monitor 
                Left = (SystemParameters.WorkArea.Width - Width) / 2;
                Top = (SystemParameters.WorkArea.Height - Height) / 2;
            }

            else
            {
                //when the screen is loaded, if it's size is within the bounds of the workarea, catch any cases in which the top or left or right falls off the screen 
                if (Top < 0)
                {
                    Top = 0;
                }
                if (Left < 0)
                {
                    Left = 0;
                }
                if (Left + ActualWidth > (SystemParameters.VirtualScreenWidth))
                {
                    Left = SystemParameters.VirtualScreenWidth - ActualWidth + 2; //account for dropshadow margin
                }
            }


        }

        void timer_Tick(object sender, EventArgs e)
        {
            _dateTime.Text = DateTime.Now.ToClientTime().ToString("MM/dd/yyyy HH:mm:ss ");
        }

        /// <summary>
        /// Print Preview button is a common control on Window, but hide this on 'Print Preview' window itself 
        /// as it is not needed here.
        /// </summary>
        private void SetPrintButtonVisibility()
        {
            string header = this.Header as string;
            if (header != null && header.Contains(PrintPreview_Text))
            {
                _printButton.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void SetIsAboutEnabled()
        {
            string url = GetAboutUrl();
            bool isEnabled;
            if (HelpPages.TryGetValue(url, out isEnabled))
            {
                SetButtonVisibility(PartHelp, isEnabled, true);
            }
            else
            {
                Task.Factory.StartNew(() => PageExists(url)).ContinueWith(t =>
                {
                    SetButtonVisibility(PartHelp, t.Result, true);
                    HelpPages[url] = t.Result;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        #region Common view features

        public ICommonViewFeatureHandler CurrentViewFeatureHandler
        {
            get { return _currentViewFeatureHandler; }
            set
            {
                // Unsubscribe from property changes
                if (_currentViewFeatureHandler != null && _currentViewFeatureHandler.ViewFeatureExchange != null)
                {
                    _currentViewFeatureHandler.ViewFeatureExchange.PropertyChanged
                        -= WeakDelegate.MakeWeak(OnViewFeatureHandlingPropertyChanged);
                }
                _currentViewFeatureHandler = value;
                // Subscribe to property changes, so that we can re-apply them
                if (_currentViewFeatureHandler != null && _currentViewFeatureHandler.ViewFeatureExchange != null)
                {
                    _currentViewFeatureHandler.ViewFeatureExchange.PropertyChanged
                        += WeakDelegate.MakeWeak(OnViewFeatureHandlingPropertyChanged);
                }

                // Update now
                UpdateCommonFeatureState();
            }
        }

        void OnViewFeatureHandlingPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateCommonFeatureState();
        }

        /// <summary>
        /// Assigns the handler for element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="handler">The handler.</param>
        public void AssignHandlerForElement(FrameworkElement element, ICommonViewFeatureHandler handler)
        {
            var autoSelectHandlerBasedOnVisibility = new MakeHandlerCurrentWhenVisibleBehavior(handler, this);
            element.AddOrReplaceBehavior(autoSelectHandlerBasedOnVisibility);
        }

        private void UpdateCommonFeatureState()
        {
            var exchange = CurrentViewFeatureHandler.IfNotNull(h => h.ViewFeatureExchange);

            // Show/hide functionality buttons
            _settingsButton.IfNotNull(b => b.Visibility = exchange != null && exchange.OpenSettings != null
                                             ? Visibility.Visible
                                             : Visibility.Collapsed);
            _alertsButton.IfNotNull(b => b.Visibility = exchange != null && exchange.ShowPendingAlert != null
                                             ? Visibility.Visible
                                             : Visibility.Collapsed);
            _refreshButton.IfNotNull(b => b.IsEnabled = exchange != null && exchange.Refresh != null);

            // Update pending alert state
            if (exchange != null && exchange.ShowPendingAlert != null && _alertsButton != null)
            {
                DependencyProperties.SetRequiresAttention(_alertsButton, exchange.HasPendingAlert);
            }
        }
       
        void OnSettings(object sender, RoutedEventArgs e)
        {
            CurrentViewFeatureHandler.IfNotNull(h => h.ViewFeatureExchange).IfNotNull(fh => fh.OpenSettings.IfNotNull(a => a()));
        }

        void OnRefreshScreen(object sender, RoutedEventArgs e)
        {
            CurrentViewFeatureHandler.IfNotNull(h => h.ViewFeatureExchange).IfNotNull(fh => fh.Refresh.IfNotNull(a => a()));
        }

        void OnAlerts(object sender, RoutedEventArgs e)
        {
            CurrentViewFeatureHandler.IfNotNull(h => h.ViewFeatureExchange).IfNotNull(fh => fh.ShowPendingAlert.IfNotNull(a => a()));
        }

        void OnLogOut(object sender, RoutedEventArgs e)
        {
            
            _externalFeatureIntegrationManager.ExecuteFeature("Application_LogOut");
        }

        /// <summary>
        /// Special behavior to switch current handler based on it's visibility
        /// </summary>
        internal class MakeHandlerCurrentWhenVisibleBehavior : Behavior<FrameworkElement>
        {
            readonly ICommonViewFeatureHandler _handler;
            readonly ICommonViewFeatureManager _manager;

            public MakeHandlerCurrentWhenVisibleBehavior(ICommonViewFeatureHandler handler, ICommonViewFeatureManager manager)
            {
                _handler = handler;
                _manager = manager;
            }

            protected override void OnAttached()
            {
                base.OnAttached();

                AssociatedObject.IsVisibleChanged += WeakDelegate.MakeWeak<DependencyPropertyChangedEventHandler>(OnIsVisibleChanged,
                    weakHandler => AssociatedObject.IsVisibleChanged -= weakHandler);

                MakeCurrentIfVisible();
            }

            protected override void OnDetaching()
            {
                base.OnDetaching();

                AssociatedObject.IsVisibleChanged -= WeakDelegate.MakeWeak<DependencyPropertyChangedEventHandler>(OnIsVisibleChanged);
            }

            void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
            {
                MakeCurrentIfVisible();
            }

            void MakeCurrentIfVisible()
            {
                if (AssociatedObject.IsVisible)
                {
                    _manager.CurrentViewFeatureHandler = _handler;
                }
                // If our view is not visible, but we are current handler -> assign current to null
                else if (_manager.CurrentViewFeatureHandler == _handler)
                {
                    _manager.CurrentViewFeatureHandler = null;
                }
            }
        }

        #endregion

        #region Print screen related
        private void OnPrintScreen(object sender, RoutedEventArgs e)
        {
            var imageControl = new Image();
            imageControl.MaxWidth = 800;

            imageControl.Source = BitmapSource(this);

            var viewManager = new PrintingDialogViewManager();
            viewManager.PrintPreviewPreparedElement(imageControl, PrintPreview_Text, System.Printing.PageOrientation.Portrait);
        }

        public static Bitmap GetBitmap(Visual visual)
        {
            // Get height and width
            var height = (int)(double)visual.GetValue(ActualWidthProperty);
            var width = (int)(double)visual.GetValue(ActualHeightProperty);

            // Render
            var rtb =
                new RenderTargetBitmap(
                    height,
                    width,
                    96,
                    96,
                    PixelFormats.Default);
            rtb.Render(visual);

            // Encode
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));
            var stream = new MemoryStream();
            encoder.Save(stream);

            // Create Bitmap
            var bmp = new Bitmap(stream);
            stream.Close();

            return bmp;
        }

        public static BitmapSource BitmapSource(Visual visual)
        {
            Bitmap bmp = GetBitmap(visual);
            IntPtr hBitmap = bmp.GetHbitmap();
            BitmapSizeOptions sizeOptions = BitmapSizeOptions.FromEmptyOptions();
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                                hBitmap,
                                IntPtr.Zero,
                                Int32Rect.Empty,
                                sizeOptions);
        }

        #endregion

        #region Dragging related
        private void OnMoveRectanglePreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            // 0. Register OnMouseUp
            // On MouseDown
            // 1. Record that mouse is pressed (in a boolean, let's say "isPressed = true")
            // 2. Register the Mouse Move Handler
            // 3. Inside the mouse move handler (implicit that it's already moved), 
            //  check if "isPressed == true", if so:
            // this.Top = 1, WindowState = normal
            // 4. OnMouseUp event, we'll need to unregister the MouseMoveHandler

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                //Check for e.ClickCount - otherwise, we're attaching the following handlers for each mouse down (twice, on double 
                //click.  Then the second attached handler doesn't get detached until after the user clicks anywhere on the window (so
                //the MouseMove method noticeably hangs up things like attempting to scroll, because it calls DragMove() on the window,
                //when the user is dragging the scrollbar).
                if (WindowState.Equals(WindowState.Maximized) && (Mouse.LeftButton.Equals(MouseButtonState.Pressed)) && e.ClickCount.Equals(1))
                {
                    Mouse.AddMouseMoveHandler(this, OnMouseMove);
                    Mouse.AddMouseUpHandler(this, OnMouseUp);
                }
                DragMove();
            }

            // Not handling resize actions if set to not resize
            if (IsWindowSetToNotResize()) return;

            if (WindowState.Equals(WindowState.Maximized) && e.ClickCount.Equals(2))
            {
                WindowState = WindowState.Normal;
            }
            else if (WindowState.Equals(WindowState.Normal) && e.ClickCount.Equals(2))
            {
                WindowState = WindowState.Maximized;
            }
        }

        private bool IsWindowSetToNotResize()
        {
            var realResizeMode = _realResizeMode ?? ResizeMode;
            return realResizeMode == ResizeMode.NoResize || realResizeMode == ResizeMode.CanMinimize;
        }

        void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            // Remove registrations for mouse events (they will be re-initialized next time
            if (Mouse.LeftButton == MouseButtonState.Released)
            {
                Mouse.RemoveMouseMoveHandler(this, OnMouseMove);
                Mouse.RemoveMouseUpHandler(this, OnMouseUp);
            }
        }

        protected void OnMouseMove(Object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                WindowState = WindowState.Normal;
                Top = 1;
                DragMove();
            }
        }
        #endregion
        #region Help handling
        private void OnHelp(object sender, RoutedEventArgs e)
        {
            string url = GetAboutUrl();

            if (url != null)
            {
                try
                {
                    Mouse.OverrideCursor = Cursors.AppStarting;
                    Process.Start(url);
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
                // ReSharper restore EmptyGeneralCatchClause
                {
                }
                finally
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private string GetAboutUrl()
        {
            string serverUrl = ConfigurationManager.SystemAppSettings["HelpServerUrl"];
            if (!serverUrl.IsNullOrEmpty() && Content != null)
            {
                serverUrl = serverUrl.TrimEnd("/");

                object content = Content;

                if (content is Grid)
                {
                    content = ((Grid)content).Children.OfType<ContentControl>().LastOrDefault() ?? content;
                }

                if (content.GetType() == typeof(ContentControl))
                {
                    content = ((ContentControl)content).Content ?? content;
                }

                if (content is WindowsFormsHost)
                {
                    content = ((WindowsFormsHost)content).Child ?? content;
                }

                if (content != null)
                {
                    Type contentType = content.GetType();

                    string contentTypeName = contentType.Name + ".htm";

                    return "{0}/{1}".FormatWith(serverUrl, contentTypeName);
                }
            }
            return null;
        }

        [DebuggerNonUserCode]
        private static bool PageExists(string url)
        {
            try
            {
                using (var wc = new WebClient())
                {
                    string s = wc.DownloadString(url);
                    return !string.IsNullOrWhiteSpace(s);
                }
            }
            catch
            {
                return false;
            }
        }

        #endregion
        #region Resize handling
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            _hwndSource = (HwndSource)PresentationSource.FromVisual(this);
        }
        protected void OnResizeRectangleMouseMove(Object sender, MouseEventArgs e)
        {
            var rectangle = sender as Rectangle;
            if (rectangle == null || IsWindowSetToNotResize()) return;

            switch (rectangle.Name)
            {
                case "top":
                    break;
                case "bottom":
                    break;
                case "left":
                    break;
                case "right":
                    break;
                case "topLeft":
                    break;
                case "topRight":
                    break;
                case "bottomLeft":
                    break;
                case "bottomRight":
                    break;
            }
        }

        protected void OnPreviewMouseMoveResetCursor(object sender, MouseEventArgs e)
        {
            // Reset mouse cursor when no longer resizing
            if (Mouse.LeftButton != MouseButtonState.Pressed)
            {
                Cursor = Cursors.Arrow;
            }
        }

        protected void OnResizeRectanglePreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var rectangle = sender as Rectangle;
            if (rectangle == null || IsWindowSetToNotResize()) return;

            switch (rectangle.Name)
            {
                case "top":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.Top);
                    break;
                case "bottom":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.Bottom);
                    break;
                case "left":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.Left);
                    break;
                case "right":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.Right);
                    break;
                case "topLeft":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.TopLeft);
                    break;
                case "topRight":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.TopRight);
                    break;
                case "bottomLeft":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.BottomLeft);
                    break;
                case "bottomRight":
                    ResizeWindow(WindowInteropUtility.WindowResizeDirection.BottomRight);
                    break;
            }
        }

        void ResizeWindow(WindowInteropUtility.WindowResizeDirection direction)
        {
            WindowInteropUtility.ResizeWindow(_hwndSource, direction);
        }
        #endregion


    }
}