using IO.Practiceware.Model;
using System;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public partial class QuestionChoiceView
    {
        private int _choiceId1;
        private int _choiceId2;
        private Choice[] _choices;
        private int _selChoiceId;

        public QuestionChoiceView()
        {
            InitializeComponent();
            Choice1Button.Checked += Choice1ButtonCheckedChanged;
            Choice1Button.Unchecked += Choice1ButtonCheckedChanged;

            Choice2Button.Checked += Choice2ButtonCheckedChanged;
            Choice2Button.Unchecked += Choice2ButtonCheckedChanged;
        }

        public int QuestionId { get; set; }

        public int ChoiceId1
        {
            get { return _choiceId1; }
            set { _choiceId1 = value; }
        }

        public int ChoiceId2
        {
            get { return _choiceId2; }
            set { _choiceId2 = value; }
        }

        public int SelChoiceId
        {
            get { return _selChoiceId; }
            set
            {
                _selChoiceId = value;
                if (_selChoiceId > 0)
                {
                    if (_selChoiceId == _choiceId1)
                    {
                        Choice1Button.IsChecked = true;
                    }
                    if (_selChoiceId == _choiceId2)
                    {
                        Choice2Button.IsChecked = true;
                    }
                }
            }
        }

        public string Note
        {
            get { return NoteTextBox.Text; }
            set { NoteTextBox.Text = value; }
        }

        private void Choice1ButtonCheckedChanged(object sender, EventArgs e)
        {
            if (Choice1Button.IsChecked == true)
            {
                _selChoiceId = ChoiceId1;
            }
        }

        private void Choice2ButtonCheckedChanged(object sender, EventArgs e)
        {
            if (Choice2Button.IsChecked == true)
            {
                _selChoiceId = ChoiceId2;
            }
        }

        public void AddQuestion(Question question)
        {
            _choices = question.Choices.OrderBy(c => c.OrdinalId).ToArray();
            if (_choices.Count() > 1)
            {
                QuestionId = question.Id;
                ChoiceId1 = _choices[0].Id;
                ChoiceId2 = _choices[1].Id;
                QuestionLabel.Text = question.Label;
                Choice1Button.Text = _choices[0].Name;
                Choice1Button.GroupName = question.Id.ToString();
                Choice2Button.Text = _choices[1].Name;
                Choice2Button.GroupName = question.Id.ToString();
                if (_choices[0].IsDefault)
                {
                    Choice1Button.IsChecked = true;
                }
                if (_choices[1].IsDefault)
                {
                    Choice2Button.IsChecked = true;
                }
            }
            else
            {
                QuestionId = question.Id;
                QuestionLabel.Text = question.Label;
                Choice1Button.Visible = false;
                Choice2Button.Visible = false;
            }
            Refresh();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Refresh();
        }
    }
}