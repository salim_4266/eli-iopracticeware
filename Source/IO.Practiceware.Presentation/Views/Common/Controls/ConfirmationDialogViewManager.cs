﻿using System;
using Soaf;
using Soaf.Presentation;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WindowStartupLocation = Soaf.Presentation.WindowStartupLocation;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public class ConfirmationDialogViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public ConfirmationDialogViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowConfirmationDialogView(ConfirmationDialogLoadArguments loadArgs)
        {
            var view = new ConfirmationDialogView();
            var dataContext = view.DataContext.EnsureType<ConfirmationDialogViewContext>();
            dataContext.LoadArguments = loadArgs;

            var interactionArguments = new WindowInteractionArguments
                {
                    Content = view,
                    IsModal = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    WindowFrameStyle = WindowFrameStyle.None,
                };
            _interactionManager.ShowModal(interactionArguments);
        }
    }

    public class ConfirmationDialogLoadArguments
    {
        /// <summary>
        /// Gets or sets the cancel command.
        /// </summary>
        public ICommand Cancel { get; set; }

        /// <summary>
        /// Gets or sets the yes command.
        /// </summary>
        public ICommand Yes { get; set; }

        /// <summary>
        /// Gets or sets the no command.
        /// </summary>
        public ICommand No { get; set; }

        /// <summary>
        /// Gets or sets the text of the confirmation.
        /// </summary>
        public object Content { get; set; }

        /// <summary>
        /// Gets or sets the owner of the confirmation.
        /// </summary>
        public object Owner { get; set; }

        /// <summary>
        /// Gets or sets the content of Yes button
        /// </summary>
        public String YesContent { get; set; }

        /// <summary>
        /// Gets or sets the content of No button
        /// </summary>
        public String NoContent { get; set; }

        /// <summary>
        /// Gets or sets the content of Cancel button
        /// </summary>
        public String CancelContent { get; set; }
    }

    /// <summary>
    /// A command that creates a new confirmation dialog whose commands yes, no, and cancel use a nullable boolean as a command parameter.
    /// </summary>
    public class ConfirmationDialogCommand : InteractionCommand
    {
        public static readonly DependencyProperty YesCommandProperty = DependencyProperty.Register("YesCommand", typeof(ICommand), typeof(ConfirmationDialogCommand));
        public static readonly DependencyProperty NoCommandProperty = DependencyProperty.Register("NoCommand", typeof(ICommand), typeof(ConfirmationDialogCommand));
        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(ConfirmationDialogCommand));
        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(ConfirmationDialogCommand));

        public override void Execute(object parameter)
        {
            var confirmationDialog = new ConfirmationDialogView();
            var dataContext = confirmationDialog.DataContext.EnsureType<ConfirmationDialogViewContext>();
            dataContext.LoadArguments = new ConfirmationDialogLoadArguments
            {
                Cancel = CancelCommand,
                No = NoCommand,
                Yes = YesCommand,
                Content = Content,
                Owner = System.Windows.Application.Current.Windows.OfType<Soaf.Presentation.Window>().FirstOrDefault(w => w.IsActive)
            };

            Arguments = new WindowInteractionArguments
            {
                Content = confirmationDialog,
                IsModal = true,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            base.Execute(parameter);
        }

        /// <summary>
        /// Gets or sets the content of the confirmation dialog
        /// </summary>
        public object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the yes command of the confirmation dialog
        /// </summary>
        public ICommand YesCommand
        {
            get { return GetValue(YesCommandProperty) as ICommand; }
            set { SetValue(YesCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the no command of the confirmation dialog
        /// </summary>
        public ICommand NoCommand
        {
            get { return GetValue(NoCommandProperty) as ICommand; }
            set { SetValue(NoCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the cancel command of the confirmation dialog
        /// </summary>
        public ICommand CancelCommand
        {
            get { return GetValue(CancelCommandProperty) as ICommand; }
            set { SetValue(CancelCommandProperty, value); }
        }
    }
}
