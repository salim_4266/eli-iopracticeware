using System;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{

	partial class QuestionAnswerView : Soaf.Presentation.Controls.WindowsForms.UserControl
	{
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.QuestionPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
			this.NoteTextBox = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.ValueTextBox = new Soaf.Presentation.Controls.WindowsForms.TextBox();
			this.QuestionLabel = new Soaf.Presentation.Controls.WindowsForms.Label();
			this.QuestionPanel.SuspendLayout();
			this.SuspendLayout();
			//
			//QuestionPanel
			//
			this.QuestionPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(243)), Convert.ToInt32(Convert.ToByte(248)), Convert.ToInt32(Convert.ToByte(252)));
			this.QuestionPanel.Controls.Add(this.NoteTextBox);
			this.QuestionPanel.Controls.Add(this.ValueTextBox);
			this.QuestionPanel.Controls.Add(this.QuestionLabel);
			this.QuestionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.QuestionPanel.Location = new System.Drawing.Point(0, 0);
			this.QuestionPanel.Name = "QuestionPanel";
			this.QuestionPanel.Size = new System.Drawing.Size(356, 48);
			this.QuestionPanel.TabIndex = 0;
			//
			//NoteTextBox
			//
			this.NoteTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NoteTextBox.Location = new System.Drawing.Point(180, 0);
			this.NoteTextBox.Name = "NoteTextBox";
			this.NoteTextBox.Size = new System.Drawing.Size(176, 48);
			this.NoteTextBox.TabIndex = 2;
			//
			//TimeTextBox
			//
			this.ValueTextBox.Dock = System.Windows.Forms.DockStyle.Left;
			this.ValueTextBox.Location = new System.Drawing.Point(120, 0);
			this.ValueTextBox.Name = "ValueTextBox";
			this.ValueTextBox.Size = new System.Drawing.Size(60, 48);
			this.ValueTextBox.TabIndex = 1;
			//
			//QuestionLabel
			//
			this.QuestionLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.QuestionLabel.Location = new System.Drawing.Point(0, 0);
			this.QuestionLabel.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
			this.QuestionLabel.Name = "QuestionLabel";
			this.QuestionLabel.Size = new System.Drawing.Size(120, 48);
			this.QuestionLabel.TabIndex = 0;
			this.QuestionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
			//
			//QuestionAnswerUserControl
			//
			this.Controls.Add(this.QuestionPanel);
			this.Name = "QuestionAnswerUserControl";
			this.Size = new System.Drawing.Size(356, 48);
			this.QuestionPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		internal Soaf.Presentation.Controls.WindowsForms.Panel QuestionPanel;
		internal Soaf.Presentation.Controls.WindowsForms.Label QuestionLabel;
		internal Soaf.Presentation.Controls.WindowsForms.TextBox NoteTextBox;
	    internal Soaf.Presentation.Controls.WindowsForms.TextBox ValueTextBox;
	}
}
