﻿using Soaf.Presentation;
using System;
using System.Windows.Input;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for SampleWizardView.xaml
    /// </summary>
    public partial class SampleWizardView
    {
        public SampleWizardView()
        {
            InitializeComponent();
        }
    }

    public class SampleWizardViewContext : IViewContext
    {
        private readonly INavigableInteractionManager _navigationManager;

        public SampleWizardViewContext(INavigableInteractionManager navigationManager)
        {
            _navigationManager = navigationManager;

            var rand = new Random();
            FillColor = Color.FromArgb(255, (byte)rand.Next(255), (byte)rand.Next(255), (byte)rand.Next(255));
            
            Next = Command.Create(ExecuteNext);
            Previous = Command.Create(ExecutePrevious);
            Close = Command.Create(ExecuteClose);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(true);
        }

        private void ExecutePrevious()
        {
            var args = new NavigableInteractionArguments
                {
                    Direction = Direction.Backward,
                    Content = new SampleWizardView()
                };
            _navigationManager.Show(args);
        }

        private void ExecuteNext()
        {
            var args = new NavigableInteractionArguments
            {
                Direction = Direction.Forward,
                Content = new SampleWizardView()
            };
            _navigationManager.Show(args);
        }

        public IInteractionContext InteractionContext { get; set; }

        public virtual Color FillColor { get; set; }

        public ICommand Next { get; protected set; }
        
        public ICommand Previous { get; protected set; }

        public ICommand Close { get; protected set; }
    }
}
