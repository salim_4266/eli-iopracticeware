﻿using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public partial class UserAdministrationView
    {

        private readonly PresentationBackgroundWorker _worker;
        private bool _suspendEvents;

        public UserAdministrationView()
        {
            InitializeComponent();

            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            UsersList.SelectionChanged += OnUsersListSelectionChanged;
            UsersStateButton.StateChanged += OnUsersStateButtonButtonStateChanged;
        }



        public UserAdministrationViewPresenter Presenter { get; set; }

        private void LoadPresenter()
        {
            var presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InteractionContext.Completed += OnInteractionContextCompleted;
            _worker.RunWorkerAsync();
        }

        private void OnInteractionContextCompleted(object sender, EventArgs<bool?> e)
        {
            if (e.Value == true)
            {
                InteractionContext.Complete();
            }
        }

        private void OnPresenterLoaded()
        {
            _suspendEvents = true;
            UsersList.ItemsSource = null;
            UsersList.ItemsSource = Presenter.Users;
            UsersList.DisplayMemberPath = "DisplayName";

            if (UsersList.SelectedItem != null)
            {
                Presenter.SetIsUserExcluded(UsersList.SelectedItem.CastTo<User>(), Presenter.IsUserExcluded);
            }
            _suspendEvents = false;
        }


        private void OnUsersListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                if(UsersList.SelectedItem != null)
                {
                    UsersStateButton.CurrentState = Presenter.GetIsUserExcluded(UsersList.SelectedItem.CastTo<User>())
                                                        ? "Exclude User"
                                                        : "Include User";
                }
            }
        }


        private void OnUsersStateButtonButtonStateChanged(object sender, EventArgs e)
        {
            string args = UsersStateButton.CurrentState;
            if (!_suspendEvents)
            {
                switch ((args))
                {
                    case "Include User":
                        // display patient name
                        Presenter.IsUserExcluded = false;
                        break;
                    case "Exclude User":
                        Presenter.IsUserExcluded = true;
                        break;
                }
                _worker.RunWorkerAsync();
            }
        }
    }

    public class UserAdministrationViewPresenter
    {
        private readonly IPracticeRepository _practiceRepository;

        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public UserAdministrationViewPresenter(IUnitOfWorkProvider unitOfWorkProvider, IPracticeRepository practiceRepository)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<User> Users { get; set; }

        public Boolean IsUserExcluded { get; set; }

        private List<TaskManagerUserConfiguration> TaskManagerUserConfigurations { get; set; }

        public void Load()
        {
            using (_unitOfWorkProvider.Create())
            {
                var users =
                    _practiceRepository.Users.Where(u => !u.IsArchived).OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToExtendedObservableCollection();
                Users = users;

                if (TaskManagerUserConfigurations == null)
                {
                    TaskManagerUserConfigurations = new List<TaskManagerUserConfiguration>();
                    TaskManagerUserConfigurations.AddRange(_practiceRepository.TaskManagerUserConfigurations);
                }
            }
        }

        private TaskManagerUserConfiguration GetTaskManagerUserConfiguration(User user)
        {
            TaskManagerUserConfiguration taskManagerUserConfig = TaskManagerUserConfigurations.FirstOrDefault(tmuc => tmuc.UserId == user.Id);
            if(taskManagerUserConfig == null)
            {
                taskManagerUserConfig = new TaskManagerUserConfiguration { UserId = user.Id };
                TaskManagerUserConfigurations.Add(taskManagerUserConfig);
            }
            return taskManagerUserConfig;
        }

        public void SetIsUserExcluded(User user, bool isExcluded)
        {
            GetTaskManagerUserConfiguration(user).IsDisabled = isExcluded;
        }

        public bool GetIsUserExcluded(User user)
        {
            return GetTaskManagerUserConfiguration(user).IsDisabled;
        }
    }
}
