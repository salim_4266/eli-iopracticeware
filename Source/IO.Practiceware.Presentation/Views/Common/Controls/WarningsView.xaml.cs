using System.Collections.Generic;
using System.Windows;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
	/// <summary>
	/// Interaction logic for WarningsView.xaml
	/// </summary>
	public partial class WarningsView
	{
		public WarningsView()
		{
            InitializeComponent();
		}
	}

    public class WarningsViewContext : IViewContext
    {
        public WarningsViewContext()
        {
            Close = Command.Create(ExecuteClose);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        public virtual string Message { get; set; }

        /// <summary>
        /// Warnings to display
        /// </summary>
        public virtual ObservableCollection<string> Warnings { get; set; }

        /// <summary>
        /// Command to close the form
        /// </summary>
        public ICommand Close { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }
    }

    public class WarningsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public WarningsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowWarnings(IEnumerable<string> warnings, string message = null)
        {
            // Show warning dialog
            var view = new WarningsView();
            var viewDataContext = view.DataContext.EnsureType<WarningsViewContext>();
            viewDataContext.Message = message;
            viewDataContext.Warnings = warnings.ToObservableCollection();

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }
}