﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using Soaf;


namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for EmailAddressInputView.xaml
    /// </summary>
    public partial class EmailAddressInputView
    {
        public static readonly DependencyProperty EmailAddressProperty = 
            DependencyProperty.Register("EmailAddress", typeof (EmailAddressViewModel), typeof(EmailAddressInputView));

        public static readonly DependencyProperty EmailAddressTypesProperty =
            DependencyProperty.Register("EmailAddressTypes", typeof(IEnumerable<NamedViewModel>), typeof(EmailAddressInputView));

        public static readonly DependencyProperty IsPrimaryCheckboxVisibilityProperty =
            DependencyProperty.Register("IsPrimaryCheckboxVisibility", typeof (Visibility), typeof (EmailAddressInputView), new PropertyMetadata(Visibility.Collapsed));

        public EmailAddressInputView()
        {
            InitializeComponent();
        }

        public EmailAddressViewModel EmailAddress
        {
            get { return GetValue(EmailAddressProperty) as EmailAddressViewModel; }
            set { SetValue(EmailAddressProperty, value); }
        }

        public IEnumerable<NamedViewModel> EmailAddressTypes
        {
            get { return GetValue(EmailAddressTypesProperty) as IEnumerable<NamedViewModel>; }
            set { SetValue(EmailAddressTypesProperty, value); }
        }

        public Visibility IsPrimaryCheckboxVisibility
        {
            get { return GetValue(IsPrimaryCheckboxVisibilityProperty).IfNotNull(x => (Visibility)x); }
            set { SetValue(IsPrimaryCheckboxVisibilityProperty, value); }
        }
    }
}
