﻿using System.Windows;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for WatermarkMaskedTextBox.xaml
    /// </summary>
    public partial class WatermarkMaskedTextBox
    {
        public static readonly DependencyProperty IsLastPositionEditableProperty = DependencyProperty.Register("IsLastPositionEditable", typeof(bool), typeof(WatermarkMaskedTextBox));
        public static readonly DependencyProperty AllowInvalidValuesProperty = DependencyProperty.Register("AllowInvalidValues", typeof(bool), typeof(WatermarkMaskedTextBox));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(WatermarkMaskedTextBox));
        public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register("WatermarkText", typeof(string), typeof(WatermarkMaskedTextBox));
        public static readonly DependencyProperty MaskProperty = DependencyProperty.Register("Mask", typeof(string), typeof(WatermarkMaskedTextBox));

        public WatermarkMaskedTextBox()
        {
            InitializeComponent();
        }

        public bool IsLastPositionEditable
        {
            get { return (bool)GetValue(IsLastPositionEditableProperty); }
            set { SetValue(IsLastPositionEditableProperty, value); }
        }

        public bool AllowInvalidValues
        {
            get { return (bool) GetValue(AllowInvalidValuesProperty); }
            set { SetValue(AllowInvalidValuesProperty, value); }
        }

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        public string WatermarkText
        {
            get { return GetValue(WatermarkTextProperty) as string; }
            set { SetValue(WatermarkTextProperty, value); }
        }

        public string Mask
        {
            get { return GetValue(MaskProperty) as string; }
            set { SetValue(MaskProperty, value); }
        }
    }
}
