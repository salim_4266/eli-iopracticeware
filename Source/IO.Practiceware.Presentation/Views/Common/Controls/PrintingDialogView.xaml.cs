using Soaf;
using Soaf.Presentation;
using System;
using System.Printing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
	/// <summary>
    /// Interaction logic for PrintingDialogView.xaml
	/// </summary>
	public partial class PrintingDialogView
	{
        public PrintingDialogView()
		{
            InitializeComponent();
		}

        /// <summary> 
        /// Scale viewer to size specified by zoom 
        /// </summary> 
        /// <param name="zoom">Zoom Enumeration to specify how pages are stretched in print and preview</param> 
        public void ZoomViewer(ZoomType zoom)
        {
            switch (zoom)
            {
                case ZoomType.Height: DocumentViewer.FitToHeight(); break;
                case ZoomType.Width: DocumentViewer.FitToWidth(); break;
                case ZoomType.TwoWide: DocumentViewer.FitToMaxPagesAcross(2); break;
                case ZoomType.Full: break;
            }
        }
	}

    public class PrintingDialogViewContext : IViewContext
    {
        public PrintingDialogViewContext()
        {
            Close = Command.Create(ExecuteClose);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        /// <summary>
        /// The document to show
        /// </summary>
        public virtual IDocumentPaginatorSource Document { get; set; }

        /// <summary>
        /// Command to close the form
        /// </summary>
        public ICommand Close { get; protected set; }

        public IInteractionContext InteractionContext { get; set; }
    }

    public class PrintingDialogViewManager
    {
        /// <summary>
        /// Perform a Print Preview on Framework element source
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="header">The header.</param>
        /// <param name="orientation">Page Orientation (i.e. Portrait vs. Landscape)</param>
        /// <param name="zoom">Zoom Enumeration to specify how pages are stretched in print and preview</param>
        /// <param name="fitPageWidth">if set to <c>true</c> [fits page width].</param>
        public void PrintPreviewPreparedElement(FrameworkElement element, string header = "Print Preview", PageOrientation orientation = PageOrientation.Landscape,
            ZoomType zoom = ZoomType.Width, bool fitPageWidth = true)
        {
            // Prepare a document
            var fixedDocument = ToFixedDocument(element, new PrintDialog(), orientation, fitPageWidth);

            PrintPreview(header, zoom, fixedDocument);
        }

        public void PrintPreview(string header, ZoomType zoom, FixedDocument fixedDocument)
        {
            Action previewAction = () =>
                                   {
                                       var view = new PrintingDialogView();
                                       var viewDataContext = view.DataContext.EnsureType<PrintingDialogViewContext>();
                                       viewDataContext.Document = fixedDocument;
                                       view.FontSize = 11;
                                       // Update zoom
                                       view.ZoomViewer(zoom);

                                       var interactionArguments = new WindowInteractionArguments
                                                                  {
                                                                      Header = header,
                                                                      Content = view,
                                                                      IsModal = true,
                                                                      ResizeMode = ResizeMode.CanResize
                                                                  };

                                       InteractionManager.Current.Show(interactionArguments);
                                   };

            // Show the dialog and ensure it's executed on Dispatcher's thread
            if (System.Windows.Application.Current.Dispatcher.CheckAccess()) previewAction();
            else System.Windows.Application.Current.Dispatcher.Invoke(previewAction);
        }

        /// <summary>
        /// Print element to a document
        /// </summary>
        /// <param name="element">GUI Element to Print</param>
        /// <param name="dialog">Reference to Print Dialog</param>
        /// <param name="orientation">Page Orientation (i.e. Portrait vs. Landscape)</param>
        /// <param name="fitPageWidth">if set to <c>true</c> [fit page width].</param>
        /// <returns>
        /// Destination document
        /// </returns>
        private static FixedDocument ToFixedDocument(FrameworkElement element, PrintDialog dialog,
            PageOrientation orientation = PageOrientation.Portrait, bool fitPageWidth = true)
        {
            dialog.PrintTicket.PageOrientation = orientation;
            PrintCapabilities capabilities = dialog.PrintQueue.GetPrintCapabilities(dialog.PrintTicket);
            const int margin = 16;
            var pageSize = new Size(dialog.PrintableAreaWidth + margin, dialog.PrintableAreaHeight + margin);

            if (capabilities.PageImageableArea != null)
            {
                var extentSize = new Size(capabilities.PageImageableArea.ExtentWidth - margin, capabilities.PageImageableArea.ExtentHeight - margin);

                var fixedDocument = new FixedDocument();

                element.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                element.Arrange(new Rect(new Point(0, 0), element.DesiredSize));

                // Calculate scaling in Fit width mode
                var scale = fitPageWidth && element.DesiredSize.Width > extentSize.Width
                    ? extentSize.Width / element.DesiredSize.Width
                    : 1;

                // Calculate width & height for the canvas where element will drawn with visual brush
                // It depends on whether we are going to scale the canvas
                var scaledCanvasHeight = extentSize.Height * (1 / scale);
                var targetCanvasWidth = fitPageWidth ? element.DesiredSize.Width : extentSize.Width;

                for (double y = 0; y < element.DesiredSize.Height; y += scaledCanvasHeight)
                {
                    for (double x = 0; x < element.DesiredSize.Width; x += targetCanvasWidth)
                    {
                        var brush = new VisualBrush(element);
                        brush.Stretch = Stretch.None;
                        brush.AlignmentX = AlignmentX.Left;
                        brush.AlignmentY = AlignmentY.Top;
                        brush.ViewboxUnits = BrushMappingMode.Absolute;
                        brush.TileMode = TileMode.None;
                        // Create a view of the element to draw it as canvas's background
                        brush.Viewbox = new Rect(x, y, targetCanvasWidth, scaledCanvasHeight);

                        var pageContent = new PageContent();
                        var page = new FixedPage();
                        ((IAddChild)pageContent).AddChild(page);

                        fixedDocument.Pages.Add(pageContent);
                        page.Width = pageSize.Width;
                        page.Height = pageSize.Height;

                        var canvas = new Canvas();
                        FixedPage.SetLeft(canvas, capabilities.PageImageableArea.OriginWidth);
                        FixedPage.SetTop(canvas, capabilities.PageImageableArea.OriginHeight + margin);
                        canvas.Width = targetCanvasWidth;
                        canvas.Height = scaledCanvasHeight;
                        canvas.Background = brush;
                        canvas.LayoutTransform = new ScaleTransform(scale, scale);

                        page.Children.Add(canvas);
                    }
                }
                return fixedDocument;
            }

            return null;
        }
    }

    /// <summary> 
    /// Zoom Enumeration to specify how pages are stretched in print and preview 
    /// </summary> 
    public enum ZoomType
    {
        /// <summary> 
        /// 100% of normal size 
        /// </summary> 
        Full,

        /// <summary> 
        /// Page Width (fit so one page stretches to full width) 
        /// </summary> 
        Width,

        /// <summary> 
        /// Page Height (fit so one page stretches to full height) 
        /// </summary> 
        Height,

        /// <summary> 
        /// Display two columns of pages 
        /// </summary> 
        TwoWide
    };
}