﻿using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interactivity;

[assembly:Component(typeof(CommonViewFeatureExchange), typeof(ICommonViewFeatureExchange), AddAllServices = false)]

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Manager which is responsible for providing access to common view features to end-user
    /// </summary>
    public interface ICommonViewFeatureManager
    {
        /// <summary>
        /// Gets or sets the current view feature handler.
        /// </summary>
        /// <value>
        /// The current view feature handler.
        /// </value>
        ICommonViewFeatureHandler CurrentViewFeatureHandler { get; set; }

        /// <summary>
        /// Assigns the handler for element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="handler">The handler.</param>
        void AssignHandlerForElement(FrameworkElement element, ICommonViewFeatureHandler handler);
    }

    /// <summary>
    /// An interface ViewContext must support in order for <see cref="CommonViewFeatures.ApplyCommonViewFeaturesProperty"/>
    /// to carry over <see cref="ICommonViewFeatureExchange"/> value from Window to a View
    /// </summary>
    public interface ICommonViewFeatureHandler
    {
        /// <summary>
        /// A shared definition of common features supported by a view
        /// </summary>
        ICommonViewFeatureExchange ViewFeatureExchange { get; }
    }

    /// <summary>
    /// Allows view to communicate support of common features to the owner of exchange information
    /// </summary>
    public interface ICommonViewFeatureExchange : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets a value indicating whether this view has pending alert.
        /// </summary>
        /// <value>
        /// <c>true</c> if this view has pending alert; otherwise, <c>false</c>.
        /// </value>
        bool HasPendingAlert { get; set; }

        /// <summary>
        /// Callback to show pending alerts. Setting to <c>null</c> disables alerts functionality
        /// </summary>
        Action ShowPendingAlert { get; set; }

        /// <summary>
        /// Callback to open settings dialog. Setting to <c>null</c> disables settings functionality
        /// </summary>
        Action OpenSettings { get; set; }

        /// <summary>
        /// Callback to refresh the screen. Setting to <c>null</c> disables refresh functionality
        /// </summary>
        Action Refresh { get; set; }
    }

    [SupportsNotifyPropertyChanged(true, false)]
    [SupportsDispatcherThread]
    internal class CommonViewFeatureExchange : ICommonViewFeatureExchange
    {
        /// <summary>
        /// <see cref="SupportsNotifyPropertyChangedAttribute"/> will handle actual logic
        /// </summary>
        public virtual event PropertyChangedEventHandler PropertyChanged = delegate { };

        [DispatcherThread]
        public virtual bool HasPendingAlert { get; set; }

        [DispatcherThread]
        public virtual Action ShowPendingAlert { get; set; }

        [DispatcherThread]
        public virtual Action OpenSettings { get; set; }

        [DispatcherThread]
        public Action Refresh { get; set; }
    }

    public class CommonViewFeatures
    {
        /// <summary>
        /// Inheritable property allowing all Views to gain access to view feature manager
        /// </summary>
        public static readonly DependencyProperty ViewFeatureManagerProperty =
            DependencyProperty.RegisterAttached("ViewFeatureManager", 
            typeof (ICommonViewFeatureManager),
            typeof (CommonViewFeatures),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.Inherits));

        /// <summary>
        /// Enables common view features usage defined by datacontext
        /// Note: DataContext must implement <see cref="ICommonViewFeatureHandler"/>
        /// </summary>
        public static readonly DependencyProperty ApplyCommonViewFeaturesProperty =
            DependencyProperty.RegisterAttached("ApplyCommonViewFeatures", 
            typeof (bool), 
            typeof (CommonViewFeatures), 
            new PropertyMetadata(false, OnApplyCommonViewFeaturesContextChanged));

        static void OnApplyCommonViewFeaturesContextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var behavior = (bool) args.NewValue ? new ApplyCommonViewFeaturesBehavior() : null;
            dependencyObject.AddOrReplaceBehavior(behavior);
        }

        public static void SetApplyCommonViewFeatures(UIElement element, bool value)
        {
            element.SetValue(ApplyCommonViewFeaturesProperty, value);
        }

        public static bool GetApplyCommonViewFeatures(UIElement element)
        {
            return (bool) element.GetValue(ApplyCommonViewFeaturesProperty);
        }

        public static void SetViewFeatureManager(UIElement element, ICommonViewFeatureManager value)
        {
            element.SetValue(ViewFeatureManagerProperty, value);
        }

        public static ICommonViewFeatureManager GetViewFeatureManager(UIElement element)
        {
            return (ICommonViewFeatureManager)element.GetValue(ViewFeatureManagerProperty);
        }
    }

    /// <summary>
    /// Will hook up common view feature handling
    /// </summary>
    public class ApplyCommonViewFeaturesBehavior : Behavior<FrameworkElement>
    {
        PropertyChangeNotifier _dataContextChangeNotifier;
        PropertyChangeNotifier _exchangeChangeNotifier;

        protected override void OnAttached()
        {
            base.OnAttached();

            // Subscribe to data context changes
            _dataContextChangeNotifier = new PropertyChangeNotifier(AssociatedObject, FrameworkElement.DataContextProperty);
            _dataContextChangeNotifier.AddValueChanged(OnRelatedPropertyChangedChanged);

            // Subscribe to exchange property changes
            _exchangeChangeNotifier = new PropertyChangeNotifier(AssociatedObject, CommonViewFeatures.ViewFeatureManagerProperty);
            _exchangeChangeNotifier.AddValueChanged(OnRelatedPropertyChangedChanged);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _dataContextChangeNotifier.RemoveValueChanged(OnRelatedPropertyChangedChanged);
            _exchangeChangeNotifier.RemoveValueChanged(OnRelatedPropertyChangedChanged);
        }

        void OnRelatedPropertyChangedChanged(object sender, EventArgs e)
        {
            RefreshViewFeatureExchange();
        }

        void RefreshViewFeatureExchange()
        {
            var frameworkElement = AssociatedObject;

            // Update exchange information if DataContext implements ICommonViewFeatureHandler
            var handler = frameworkElement.DataContext as ICommonViewFeatureHandler;
            if (frameworkElement.DataContext != null && handler == null)
            {
                throw new ArgumentException("ViewContext must implement ICommonViewFeatureHandler interface");
            }

            var manager = CommonViewFeatures.GetViewFeatureManager(frameworkElement);
            if (manager != null)
            {
                manager.AssignHandlerForElement(frameworkElement, handler);
            }
        }
    }
}
