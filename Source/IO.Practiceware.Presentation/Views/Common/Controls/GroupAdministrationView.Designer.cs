﻿using System;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    partial class GroupAdministrationView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.AddGroupButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.DeleteGroupButton = new Soaf.Presentation.Controls.WindowsForms.Button();
            this.ListPanel = new Soaf.Presentation.Controls.WindowsForms.Panel();
            this.GroupsList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.UsersList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.ListPanel.SuspendLayout();
            this.SuspendLayout();
            //
            //AddGroupButton
            //
            this.AddGroupButton.Anchor = (System.Windows.Forms.AnchorStyles) (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.AddGroupButton.Location = new System.Drawing.Point(5, 8);
            this.AddGroupButton.Name = "AddGroupButton";
            this.AddGroupButton.Size = new System.Drawing.Size(100, 60);
            this.AddGroupButton.TabIndex = 4;
            this.AddGroupButton.Text = "Add Group";
            //
            //DeleteGroupButton
            //
            this.DeleteGroupButton.Anchor = (System.Windows.Forms.AnchorStyles) (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.DeleteGroupButton.Location = new System.Drawing.Point(120, 8);
            this.DeleteGroupButton.Name = "DeleteGroupButton";
            this.DeleteGroupButton.Size = new System.Drawing.Size(100, 60);
            this.DeleteGroupButton.TabIndex = 5;
            this.DeleteGroupButton.Text = "Delete Group";
            //
            //ListPanel
            //
            this.ListPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(129)), Convert.ToInt32(Convert.ToByte(178)), Convert.ToInt32(Convert.ToByte(220)));
            this.ListPanel.Controls.Add(this.DeleteGroupButton);
            this.ListPanel.Controls.Add(this.AddGroupButton);
            this.ListPanel.Controls.Add(this.GroupsList);
            this.ListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListPanel.Location = new System.Drawing.Point(0, 0);
            this.ListPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ListPanel.Name = "ListPanel";
            this.ListPanel.Size = new System.Drawing.Size(231, 337);
            this.ListPanel.TabIndex = 7;
            //
            //GroupsList
            //
            this.GroupsList.Anchor = (System.Windows.Forms.AnchorStyles) (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.GroupsList.DisplayMemberPath = "";
            this.GroupsList.Location = new System.Drawing.Point(2, 80);
            this.GroupsList.Margin = new System.Windows.Forms.Padding(0);
            this.GroupsList.Name = "GroupsList";
            this.GroupsList.SelectedIndex = -1;
            this.GroupsList.SelectedItem = null;
            this.GroupsList.SelectedValue = null;
            this.GroupsList.SelectedValuePath = "";
            this.GroupsList.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GroupsList.Size = new System.Drawing.Size(218, 250);
            this.GroupsList.TabIndex = 3;
            //
            //UsersList
            //
            this.UsersList.Anchor = (System.Windows.Forms.AnchorStyles) (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.UsersList.DisplayMemberPath = "";
            this.UsersList.Location = new System.Drawing.Point(241, 8);
            this.UsersList.Margin = new System.Windows.Forms.Padding(0);
            this.UsersList.Name = "UsersList";
            this.UsersList.SelectedIndex = -1;
            this.UsersList.SelectedItem = null;
            this.UsersList.SelectedValue = null;
            this.UsersList.SelectedValuePath = "";
            this.UsersList.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.UsersList.Size = new System.Drawing.Size(218, 322);
            this.UsersList.TabIndex = 6;
            //
            //GroupAdministrationControl
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.UsersList);
            this.Controls.Add(this.ListPanel);
            this.Name = "GroupAdministrationControl";
            this.Size = new System.Drawing.Size(485, 337);
            this.ListPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        internal Soaf.Presentation.Controls.WindowsForms.Button AddGroupButton;
        internal Soaf.Presentation.Controls.WindowsForms.Button DeleteGroupButton;
        internal Soaf.Presentation.Controls.WindowsForms.Panel ListPanel;
        internal Soaf.Presentation.Controls.WindowsForms.ListBox GroupsList;

        internal Soaf.Presentation.Controls.WindowsForms.ListBox UsersList;
    }
}
