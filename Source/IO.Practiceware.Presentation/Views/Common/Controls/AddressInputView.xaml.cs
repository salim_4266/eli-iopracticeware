﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Services.Geocode;
using Soaf;
using Soaf.Presentation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;
using Soaf.Threading;
using System.Windows.Media;
using IO.Practiceware.Data;
using System.Data;
using Soaf.Data;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

[assembly: Component(typeof(AutoFillCityAndStateCommand), Initialize = true)]

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    ///     Interaction logic for AddressInputView.xaml
    /// </summary>
    public partial class AddressInputView : IDataErrorInfo
    {
       // public virtual bool IsAddHighlight { get; set; }

        public static readonly DependencyProperty IsAddHighlightProperty =
           DependencyProperty.Register("IsAddHighlight", typeof(bool), typeof(AddressInputView), new PropertyMetadata(OnAddChanged));

        public static readonly DependencyProperty AddressProperty =
            DependencyProperty.Register("Address", typeof(AddressViewModel), typeof(AddressInputView), new PropertyMetadata(OnAddressChanged));

        public static readonly DependencyProperty AddressTypesProperty =
            DependencyProperty.Register("AddressTypes", typeof(IEnumerable<NamedViewModel>), typeof(AddressInputView));

        public static readonly DependencyProperty AddressTypesVisibilityProperty =
            DependencyProperty.Register("AddressTypesVisibility", typeof(Visibility), typeof(AddressInputView), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty AddressTypesDisplayMemberPathProperty =
            DependencyProperty.Register("AddressTypesDisplayMemberPath", typeof(string), typeof(AddressInputView), new PropertyMetadata("Name"));

        private static readonly DependencyPropertyKey CountriesPropertyKey =
            DependencyProperty.RegisterReadOnly("Countries", typeof(IEnumerable<Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>>>), typeof(AddressInputView), new PropertyMetadata());

        public static readonly DependencyProperty CountriesProperty = CountriesPropertyKey.DependencyProperty;

        public static readonly DependencyProperty StateOrProvincesProperty =
            DependencyProperty.Register("StateOrProvinces", typeof(IEnumerable<StateOrProvinceViewModel>), typeof(AddressInputView), new PropertyMetadata(OnStateOrProvincesChanged));

        public static readonly DependencyProperty SelectedCountryProperty =
            DependencyProperty.Register("SelectedCountry", typeof(Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>>), typeof(AddressInputView));

        public static readonly DependencyProperty IsPrimaryCheckboxVisibilityProperty =
            DependencyProperty.Register("IsPrimaryCheckboxVisibility", typeof(Visibility), typeof(AddressInputView), new PropertyMetadata(Visibility.Visible, OnIsPrimaryCheckboxVisibilityChanged));

        public static readonly DependencyProperty AutoFillCityAndStateCommandProperty =
            DependencyProperty.Register("AutoFillCityAndStateCommand", typeof(ICommand), typeof(AddressInputView));

        public static readonly DependencyProperty IsOrganizationComboBoxVisibilityProperty =
            DependencyProperty.Register("IsOrganizationComboBoxVisibility", typeof(Visibility), typeof(AddressInputView), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty Line1LabelProperty =
            DependencyProperty.Register("Line1Label", typeof(String), typeof(AddressInputView));

        public static readonly DependencyProperty EntityNameProperty =
            DependencyProperty.Register("EntityName", typeof(String), typeof(AddressInputView));

        public AddressInputView()
        {
            AutoFillCityAndStateCommand = new AutoFillCityAndStateCommand();

            BindingOperations.SetBinding((AutoFillCityAndStateCommand)AutoFillCityAndStateCommand, Controls.AutoFillCityAndStateCommand.StateOrProvincesProperty, new Binding("StateOrProvinces") { Source = this, Mode = BindingMode.OneWay });

            InitializeComponent();

        }

        public AddressViewModel Address
        {
            get { return GetValue(AddressProperty) as AddressViewModel; }
            set { SetValue(AddressProperty, value); }
        }

        public Visibility IsPrimaryCheckboxVisibility
        {
            get { return GetValue(IsPrimaryCheckboxVisibilityProperty).IfNotNull(x => (Visibility)x); }
            set { SetValue(IsPrimaryCheckboxVisibilityProperty, value); }
        }

        public string AddressTypesDisplayMemberPath
        {
            get { return GetValue(AddressTypesDisplayMemberPathProperty) as string; }
            set { SetValue(AddressTypesDisplayMemberPathProperty, value); }
        }

        public IEnumerable<NamedViewModel> AddressTypes
        {
            get { return GetValue(AddressTypesProperty) as IEnumerable<NamedViewModel>; }
            set { SetValue(AddressTypesProperty, value); }
        }

        public bool IsAddHighlight
        {
            get { return (bool)GetValue(IsAddHighlightProperty) ; }
            set { SetValue(IsAddHighlightProperty, value); }
        }
        public Visibility AddressTypesVisibility
        {
            get { return (Visibility)GetValue(AddressTypesVisibilityProperty); }
            set { SetValue(AddressTypesVisibilityProperty, value); }
        }

        public IEnumerable<StateOrProvinceViewModel> StateOrProvinces
        {
            get { return GetValue(StateOrProvincesProperty) as IEnumerable<StateOrProvinceViewModel>; }
            set { SetValue(StateOrProvincesProperty, value); }
        }

        public IEnumerable<Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>>> Countries
        {
            get { return GetValue(CountriesProperty) as IEnumerable<Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>>>; }
            protected set { SetValue(CountriesPropertyKey, value); }
        }

        public Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>> SelectedCountry
        {
            get { return GetValue(SelectedCountryProperty) as Tuple<NameAndAbbreviationViewModel, IEnumerable<NameAndAbbreviationViewModel>>; }
            set { SetValue(SelectedCountryProperty, value); }
        }

        public ICommand AutoFillCityAndStateCommand
        {
            get { return GetValue(AutoFillCityAndStateCommandProperty) as ICommand; }
            set { SetValue(AutoFillCityAndStateCommandProperty, value); }
        }

        public Visibility IsOrganizationComboBoxVisibility
        {
            get { return GetValue(IsOrganizationComboBoxVisibilityProperty).IfNotNull(x => (Visibility)x); }
            set { SetValue(IsOrganizationComboBoxVisibilityProperty, value); }
        }

        public String Line1Label
        {
            get { return GetValue(Line1LabelProperty) as String; }
            set { SetValue(Line1LabelProperty, value); }
        }

        public String EntityName
        {
            get { return GetValue(EntityNameProperty) as String; }
            set { SetValue(EntityNameProperty, value); }
        }


        private static void OnStateOrProvincesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = ((AddressInputView)d);
            if (view.StateOrProvinces != null)
            {
                var stateOrProvince = view.Address.IfNotNull(a => a.StateOrProvince);

                view.Countries = view.StateOrProvinces
                    .GroupBy(s => s.Country.Id)
                    .Select(group => Tuple.Create(group.First().Country, group.OfType<NameAndAbbreviationViewModel>().ToArray().AsEnumerable()))
                    .OrderBy(x => (x.Item1.Abbreviation == "USA" ? 0 : 1))
                    .ThenBy(y => y.Item1.Abbreviation)
                    .ToObservableCollection();

                // set back the previous value, since resetting countries will have null'd it
                if (stateOrProvince != null && view.Address != null) view.Address.StateOrProvince = stateOrProvince;
            }

            view.SyncAddressViewModelSelectedItems();
            view.HandleAutoSelectOfFirstCountry();
        }

        private static void OnAddressChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = ((AddressInputView)d);
            view.HandleAutoSelectOfFirstCountry();

            view.SyncAddressViewModelSelectedItems();
        }
        private static void OnAddChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = ((AddressInputView)d);
            if (view.AddressLine1.Text == string.Empty)
            {
                view.AddressLine1.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                view.AddressLine1.BorderBrush = Brushes.LightGray;
            }
            if(view.AddressTypeComboBox.Text==string.Empty)
            {
                view.AddressType.BorderBrush = Brushes.DeepSkyBlue;
                view.AddressType.BorderThickness = new Thickness(1, 1, 1, 1);
            }
            else
            {
                view.AddressType.BorderBrush = Brushes.LightGray;
                view.AddressType.BorderThickness = new Thickness(0, 0, 0, 0);
            }
        }
        private void SyncAddressViewModelSelectedItems()
        {
            if (Address == null) return;

            if (Address.StateOrProvince != null && StateOrProvinces != null) Address.StateOrProvince = StateOrProvinces.WithId(Address.StateOrProvince.Id);
            if (Address.AddressType != null && AddressTypes != null) Address.AddressType = AddressTypes.WithId(Address.AddressType.Id);
            if (Address.StateOrProvince != null && Countries != null) SelectedCountry = Countries.FirstOrDefault(c => c.Item1.Id == Address.StateOrProvince.Country.Id);
        }

        private void HandleAutoSelectOfFirstCountry()
        {
            if (Address != null && Address.StateOrProvince == null && Countries != null)
            {
                // auto select first country
                SelectedCountry = Countries.FirstOrDefault();
            }
        }

        private static void OnIsPrimaryCheckboxVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as AddressInputView;
            if (view != null)
            {
                if (view.IsPrimaryCheckboxVisibility == Visibility.Visible) view.Width += 65;
                else view.Width -= 65;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (SelectedCountry == null && columnName == Reflector.GetMember(() => SelectedCountry).Name)
                {
                    return "Country is required.";
                }
                return null;
            }
        }

        public string Error { get; private set; }
        
        private void AddressLine1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (AddressLine1.Text == string.Empty)
            {
                AddressLine1.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                AddressLine1.BorderBrush = Brushes.LightGray;
            }
        }
        private void AddressType_LostFocus(object sender, RoutedEventArgs e)
        {
            if (AddressTypeComboBox.Text == string.Empty)
            {
                AddressType.BorderBrush = Brushes.DeepSkyBlue;
                AddressType.BorderThickness = new Thickness(1, 1, 1, 1);
            }
            else
            {
                AddressType.BorderBrush = Brushes.LightGray;
                AddressType.BorderThickness = new Thickness(0, 0, 0, 0);
            }
        }
    }

    /// <summary>
    ///     A default command which auto-fills in city and state based on zip code.
    /// </summary>
    [Singleton]
    public class AutoFillCityAndStateCommand : Freezable, ICommand
    {
        public static readonly DependencyProperty StateOrProvincesProperty =
            DependencyProperty.Register("StateOrProvinces", typeof(IEnumerable<StateOrProvinceViewModel>), typeof(AutoFillCityAndStateCommand), new PropertyMetadata());
        public virtual ObservableCollection<AddressViewModel> StateCityData { get; set; }
        private static IGeocodingService _geocodingService;


        /// <summary>
        ///     Constructor used for default injection upon component registration.
        /// </summary>
        /// <param name="geocodingService">The geocoding service.</param>
        public AutoFillCityAndStateCommand(IGeocodingService geocodingService)
        {
            _geocodingService = geocodingService;
        }

        public AutoFillCityAndStateCommand()
        {
        }

        public IEnumerable<StateOrProvinceViewModel> StateOrProvinces
        {
            get { return GetValue(StateOrProvincesProperty) as IEnumerable<StateOrProvinceViewModel>; }
            set { SetValue(StateOrProvincesProperty, value); }
        }

        public virtual ICommand AutoFillCityAndState { get; protected set; }

        public void Execute(object parameter)
        {
            var address = parameter as AddressViewModel;
            if(null!=address)
                if (!string.IsNullOrEmpty(address.PostalCode))
            //ZipCode via DB
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "select top 1 * from dbo.ZipCodes where ZIPCode = '"+ address.PostalCode +"'";
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    StateCityData = new ObservableCollection<AddressViewModel>();
                    
                    foreach (DataRow row in dataTable.Rows)
                    {
                        foreach (var item in StateOrProvinces)
                        {
                            if (item.Abbreviation ==row["StateAbbr"].ToString())
                            {
                                  address.StateOrProvince = item;
                                break;
                            }
                        }
                        StateCityData.Add(new AddressViewModel() { Id = int.Parse(row["Id"].ToString()), City=  row["CityName"].ToString() });
                    }
                    if (StateCityData != null)
                    {
                        foreach (var item in StateCityData)
                        {
                                address.City = item.City;
                        }
                    }
                }
                else
                {
                if (!string.IsNullOrEmpty(address.PostalCode))
                    if (address.PostalCode.Length>=5)
                        address.PostalCode = address.PostalCode.Substring(0, 5) + "-" + address.PostalCode.Substring(5);
                    try
                    {
                        // Can fill in?
                        if (_geocodingService == null || address == null || address.PostalCode.IsNullOrWhiteSpace()) return;

                        Task.Factory.StartNewWithCurrentTransaction(() =>
                        {
                                    GeocodeInformation geocodeInformation = null;
                            try
                            {
                                geocodeInformation = _geocodingService.GetGeocodeInformation(address.PostalCode);
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError(ex.ToString());
                            }
                            if (geocodeInformation != null && geocodeInformation.IsValid())
                            {
                                Dispatcher.BeginInvoke(() =>
                                {
                                    // overwrite city if different
                                    if (address.City == null || !string.Equals(address.City, geocodeInformation.City, StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        address.City = geocodeInformation.City;
                                    }

                                    if (StateOrProvinces != null)
                                    {
                                        // overwrite state if different
                                        var stateByPostalCode = StateOrProvinces.FirstOrDefault(i => i.Id == geocodeInformation.StateId);
                                        if (address.StateOrProvince == null || !Equals(address.StateOrProvince, stateByPostalCode))
                                        {
                                            address.StateOrProvince = stateByPostalCode;
                                        }
                                    }
                                });
                            }
                        }).ContinueWith(t=>InsertZipCodeData(address));
                    }
                    catch (Exception ex)
                    {
                        var exception = new Exception("Could not get geocoding information for {0}.".FormatWith(address), ex);
                        Trace.TraceWarning(exception.ToString());
                    }
                }
            }
        }
        public void InsertZipCodeData(AddressViewModel avm)
        {
            //Save ZipCode, State and City in Table
            if (avm.City != null && avm.StateOrProvince != null)
            {
                using (var dbCon = DbConnectionFactory.PracticeRepository)
                {
                    avm.PostalCode = avm.PostalCode.Substring(0, 5);
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("@ZipCode", avm.PostalCode);
                    param.Add("@CityName", avm.City);
                    param.Add("@StateAbbr", avm.StateOrProvince.Abbreviation);
                    string qryInsert = "Insert into dbo.ZipCodes (ZipCode,CityName,StateAbbr) values (@ZipCode,@CityName,@StateAbbr)";
                    dbCon.Execute(qryInsert, param);
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return _geocodingService != null && parameter is AddressViewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }
}