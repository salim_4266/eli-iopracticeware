﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Lightweight text element that uses the highlight text cacher to highlight characters.
    /// </summary>
    public class HighlightTextElement : FrameworkElement
    {
        public static readonly DependencyProperty FontFamilyProperty = DependencyProperty.Register("FontFamily", typeof(FontFamily), typeof(HighlightTextElement), new PropertyMetadata(new FontFamily("Segoe UI")));
        public static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register("FontSize", typeof(double), typeof(HighlightTextElement), new PropertyMetadata(13.333));
        public static readonly DependencyProperty FontStretchProperty = DependencyProperty.Register("FontStretch", typeof(FontStretch), typeof(HighlightTextElement), new PropertyMetadata(default(FontStretch)));
        public static readonly DependencyProperty FontStyleProperty = DependencyProperty.Register("FontStyle", typeof(FontStyle), typeof(HighlightTextElement), new PropertyMetadata(default(FontStyle)));
        public static readonly DependencyProperty FontWeightProperty = DependencyProperty.Register("FontWeight", typeof(FontWeight), typeof(HighlightTextElement), new PropertyMetadata(default(FontWeight)));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(HighlightTextElement), new PropertyMetadata(null, OnPropertyChangedInvalidateVisual));
        public static readonly DependencyProperty HighlightForegroundProperty = DependencyProperty.Register("HighlightForeground", typeof(Brush), typeof(HighlightTextElement), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
        public static readonly DependencyProperty HighlightBackgroundProperty = DependencyProperty.Register("HighlightBackground", typeof(Brush), typeof(HighlightTextElement), new PropertyMetadata(new SolidColorBrush(Colors.Yellow)));
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof(Brush), typeof(HighlightTextElement), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
        public static readonly DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(HighlightTextElement), new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(HighlightTextElement), new PropertyMetadata(TextAlignment.Center));
        public static readonly DependencyProperty IsHighlightingEnabledProperty = DependencyProperty.Register("IsHighlightingEnabled", typeof(bool), typeof(HighlightTextElement), new PropertyMetadata(true, OnPropertyChangedInvalidateVisual));
        public static readonly DependencyProperty TermsToHighlightProperty = DependencyProperty.Register("TermsToHighlight", typeof(IEnumerable<string>), typeof(HighlightTextElement), new PropertyMetadata(null, OnTermsToHighlightPropertyChanged));

        private static HighlightTextCacher _highlightTextCacher;

        private static void OnTermsToHighlightPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (_highlightTextCacher == null)
            {
                _highlightTextCacher = new HighlightTextCacher();
            }
            _highlightTextCacher.TermsToHighlight = e.NewValue as IEnumerable<string>;
        }

        private Size _actualSize;

        public HighlightTextElement()
        {
            SizeChanged += HighlightTextElementSizeChanged;
        }

        private void HighlightTextElementSizeChanged(object sender, SizeChangedEventArgs e)
        {
            _actualSize = e.NewSize;
            InvalidateVisual();
        }

        private static void OnPropertyChangedInvalidateVisual(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var highlightTextElement = (HighlightTextElement)d;
            highlightTextElement.InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (string.IsNullOrEmpty(Text) || _highlightTextCacher == null)
            {
                base.OnRender(drawingContext);
                return;
            }

            #region getHighlightTextLines

            Func<IEnumerable<HighlightTextLine>> getHighlightTextLines = () =>
            {
                IEnumerable<HighlightTextLine> highlightTextLines;
                if (!_highlightTextCacher.TryGetHighlightText(Text, out highlightTextLines))
                {
                    _highlightTextCacher.AddAndProcessString(Text);
                    highlightTextLines = _highlightTextCacher.GetHighlightText(Text);
                }
                return highlightTextLines;
            };

            #endregion

            #region tryResizeHeightToMultiLineText

            Func<bool> tryResizeHeightToMultiLineText = () =>
            {
                var height = GetTextHeight() * getHighlightTextLines().Count();
                if (!Height.Equals(height))
                {
                    Height = height;
                    return true;
                }
                return false;
            };

            #endregion

            if (tryResizeHeightToMultiLineText())
            {
                base.OnRender(drawingContext);
                return;
            }

            var typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);
            double currentTopTextPoint = 0;
            double maxTextHeight = GetTextHeight();
            foreach (var line in getHighlightTextLines())
            {
                double currentLeftTextPoint = GetInitialLeftPoint(TextAlignment);
                double widthRemaining = _actualSize.Width - currentLeftTextPoint;

                foreach (var textChunk in line.LinkedHighlightText)
                {
                    var foreground = textChunk.IsHighlighted && IsHighlightingEnabled ? HighlightForeground : Foreground;
                    var background = textChunk.IsHighlighted && IsHighlightingEnabled ? HighlightBackground : Background;

                    var formattedText = new FormattedText(textChunk.Text,
                                                      CultureInfo.InvariantCulture,
                                                      FlowDirection.LeftToRight,
                                                      typeface,
                                                      FontSize,
                                                      foreground);

                    formattedText.MaxTextWidth = (widthRemaining - formattedText.Width <= 0) ? widthRemaining : formattedText.Width;
                    formattedText.MaxTextHeight = maxTextHeight;
                    widthRemaining -= formattedText.Width;

                    drawingContext.DrawRectangle(background, null, new Rect(currentLeftTextPoint, currentTopTextPoint, formattedText.Width, formattedText.Height));
                    drawingContext.DrawText(formattedText, new Point(currentLeftTextPoint, currentTopTextPoint));

                    currentLeftTextPoint += formattedText.Width;
                    if (widthRemaining <= 0) break;
                }
                currentTopTextPoint += maxTextHeight;
            }
        }

        private double GetTextWidth(string text)
        {
            var typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);

            var formattedText = new FormattedText(text,
                                                  CultureInfo.InvariantCulture,
                                                  FlowDirection.LeftToRight,
                                                  typeface,
                                                  FontSize,
                                                  Background);
            return formattedText.Width;
        }

        private double GetTextHeight()
        {
            var typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);

            // Use single letter so no text overflow occurs.  Get height of single line.
            var formattedText = new FormattedText("a",
                                                  CultureInfo.InvariantCulture,
                                                  FlowDirection.LeftToRight,
                                                  typeface,
                                                  FontSize,
                                                  Background);
            return formattedText.Height;
        }

        private double GetInitialLeftPoint(TextAlignment textAlignment)
        {
            switch (textAlignment)
            {
                case TextAlignment.Left:
                    {
                        return 0;
                    }
                case TextAlignment.Center:
                    {
                        var totalTextWidth = GetTextWidth(Text);
                        var actualWidth = _actualSize.Width;
                        if (totalTextWidth >= actualWidth) { return 0; }

                        var leftAndRightMargin = actualWidth - totalTextWidth;
                        return leftAndRightMargin / 2;
                    }
                case TextAlignment.Right:
                    {
                        var totalTextWidth = GetTextWidth(Text);
                        var actualWidth = _actualSize.Width;
                        if (totalTextWidth >= actualWidth) return 0;

                        var leftMargin = actualWidth - totalTextWidth;
                        return leftMargin;
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        public FontFamily FontFamily
        {
            get { return GetValue(FontFamilyProperty) as FontFamily; }
            set { SetValue(FontFamilyProperty, value); }
        }

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public FontStretch FontStretch
        {
            get { return (FontStretch)GetValue(FontStretchProperty); }
            set { SetValue(FontStretchProperty, value); }
        }

        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        public Brush HighlightForeground
        {
            get { return GetValue(HighlightForegroundProperty) as Brush; }
            set { SetValue(HighlightForegroundProperty, value); }
        }

        public Brush HighlightBackground
        {
            get { return GetValue(HighlightBackgroundProperty) as Brush; }
            set { SetValue(HighlightBackgroundProperty, value); }
        }

        public Brush Foreground
        {
            get { return GetValue(ForegroundProperty) as Brush; }
            set { SetValue(ForegroundProperty, value); }
        }

        public Brush Background
        {
            get { return GetValue(BackgroundProperty) as Brush; }
            set { SetValue(BackgroundProperty, value); }
        }

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public bool IsHighlightingEnabled
        {
            get { return (bool)GetValue(IsHighlightingEnabledProperty); }
            set { SetValue(IsHighlightingEnabledProperty, value); }
        }

        public IEnumerable<string> TermsToHighlight
        {
            get { return GetValue(TermsToHighlightProperty) as IEnumerable<string>; }
            set { SetValue(TermsToHighlightProperty, value); }
        }
    }

    public class HighlightText
    {
        public string Text { get; set; }
        public bool IsHighlighted { get; set; }
    }

    public class HighlightTextLine
    {
        public LinkedList<HighlightText> LinkedHighlightText { get; set; }
    }

    /// <summary>
    /// Calculates and caches information on which characters to highlight using a given string and "terms to highlight".
    /// </summary>
    public class HighlightTextCacher : Freezable
    {
        public static readonly DependencyProperty TermsToHighlightProperty = DependencyProperty.Register("TermsToHighlight", typeof(IEnumerable<string>), typeof(HighlightTextCacher), new PropertyMetadata(null, OnTermsToHighlightPropertyChanged));
        private readonly Dictionary<string, IEnumerable<HighlightTextLine>> _highlightTextCache = new Dictionary<string, IEnumerable<HighlightTextLine>>();

        public void Clear()
        {
            _highlightTextCache.Clear();
        }

        public bool TryGetHighlightText(string input, out IEnumerable<HighlightTextLine> highlightLines)
        {
            return _highlightTextCache.TryGetValue(input, out highlightLines);
        }

        public void AddAndProcessString(string text)
        {
            var key = text;
            if (!_highlightTextCache.ContainsKey(key))
            {
                var highlightLines = new List<HighlightTextLine>();
                foreach (var line in text.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                {
                    highlightLines.Add(new HighlightTextLine { LinkedHighlightText = ProcessInput(line) });
                }
                _highlightTextCache.Add(key, highlightLines);
            }
        }

        public IEnumerable<HighlightTextLine> GetHighlightText(string input)
        {
            return _highlightTextCache[input];
        }

        private static void OnTermsToHighlightPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var highlightTextCacher = (HighlightTextCacher)d;
            highlightTextCacher.Clear();
        }

        private LinkedList<HighlightText> ProcessInput(string input)
        {
            var originalInput = input;
            var indexesToHighlight = GetIndexesToHighlight(input).ToArray();

            var result = new LinkedList<HighlightText>();
            var highlightStringBuilder = new StringBuilder();
            var nonHighlightStringBuilder = new StringBuilder();

            // Scan through each character in the input, and build the characters to highlight (and to not highlight) using the 'indexesToHighlight'
            for (int i = 0; i < originalInput.Length; i++)
            {
                if (indexesToHighlight.Contains(i))
                {
                    if (nonHighlightStringBuilder.Length > 0)
                    {
                        result.AddLast(new HighlightText { IsHighlighted = false, Text = nonHighlightStringBuilder.ToString() });
                        nonHighlightStringBuilder.Clear();
                    }
                    highlightStringBuilder.Append(originalInput[i]);
                }
                else
                {
                    if (highlightStringBuilder.Length > 0)
                    {
                        result.AddLast(new HighlightText { IsHighlighted = true, Text = highlightStringBuilder.ToString() });
                        highlightStringBuilder.Clear();
                    }
                    nonHighlightStringBuilder.Append(originalInput[i]);
                }
            }

            // If any values remain in the string builders, append the values to the final result
            if (nonHighlightStringBuilder.Length > 0)
            {
                result.AddLast(new HighlightText { IsHighlighted = false, Text = nonHighlightStringBuilder.ToString() });
                nonHighlightStringBuilder.Clear();
            }

            if (highlightStringBuilder.Length > 0)
            {
                result.AddLast(new HighlightText { IsHighlighted = true, Text = highlightStringBuilder.ToString() });
                highlightStringBuilder.Clear();
            }

            return result;
        }

        private IEnumerable<int> GetIndexesToHighlight(string input)
        {
            var indexesToHighlight = new HashSet<int>();
            if (TermsToHighlight != null)
            {
                foreach (var highlightTerm in TermsToHighlight.Distinct())
                {
                    int indexToStartHighlighting = 0;
                    int trimmedInputLength = 0;
                    while (indexToStartHighlighting < input.Length)
                    {
                        // Get the start index, and the length, to highlight using the given input.
                        var highlightIndexAndLength = GetHighlightIndexStartAndLength(input, highlightTerm);
                        if (highlightIndexAndLength == null)
                        {
                            break;
                        }

                        indexToStartHighlighting = highlightIndexAndLength.Item1;
                        var highlightLength = highlightIndexAndLength.Item2;
                        for (int i = indexToStartHighlighting; i < indexToStartHighlighting + highlightLength; i++)
                        {
                            indexesToHighlight.Add(i + trimmedInputLength);
                        }

                        // Remove characters up to the index of the last highlighted character
                        // We will then get the new highlight index and length using this "trimmed input"
                        input = input.Substring(indexToStartHighlighting + highlightLength);

                        // Need to adjust the next highlight index found with the "trimmed input" by the amount of characters trimmed from the input.
                        // (Since the highlight index is relative to the original input, not the "trimmed input")
                        trimmedInputLength += indexToStartHighlighting + highlightLength;
                        indexToStartHighlighting = 0;
                    }
                }
            }
            return indexesToHighlight;
        }

        private static Tuple<int, int> GetHighlightIndexStartAndLength(string input, string highlightTerm)
        {
            Func<int> getIndexOfHighlightTermIgnoreHyphen = () => input.Replace("-", "").IndexOf(highlightTerm.Replace("-", ""), 0, StringComparison.OrdinalIgnoreCase);

            // Get the start index to start highlighting
            var currentIndex = getIndexOfHighlightTermIgnoreHyphen();
            if (currentIndex == -1) return null;

            int matchingHighlightCharacterCount = 0;
            int startHighlightIndex = currentIndex;

            // Scan through the input and increment the matching highlight character count until the count is equal to the highlight length
            // The following loop will get the startHighlightIndex and the currentIndex.  
            // With these 2 values, we can get the start index, and length, of the characters to highlight.
            while (matchingHighlightCharacterCount < highlightTerm.Length)
            {
                var currentChar = input[currentIndex];
                if (Char.ToLower(currentChar) == Char.ToLower(highlightTerm[matchingHighlightCharacterCount]))
                {
                    matchingHighlightCharacterCount++;
                    currentIndex++;
                }
                else if (currentChar == '-')
                {
                    currentIndex++;
                }
                else
                {
                    matchingHighlightCharacterCount = (Char.ToLower(currentChar) == Char.ToLower(highlightTerm[0])) ? 1 : 0;
                    startHighlightIndex = currentIndex;
                    currentIndex++;
                }
            }

            int highlightLength = currentIndex - startHighlightIndex;
            if (highlightLength == 0) highlightLength = 1;
            return new Tuple<int, int>(startHighlightIndex, highlightLength);
        }

        public IEnumerable<string> TermsToHighlight
        {
            get { return GetValue(TermsToHighlightProperty) as IEnumerable<string>; }
            set
            {
                if (value != null)
                {
                    value = value.Select(i => i.ToLowerInvariant());
                    SetValue(TermsToHighlightProperty, value);
                }
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }
}
