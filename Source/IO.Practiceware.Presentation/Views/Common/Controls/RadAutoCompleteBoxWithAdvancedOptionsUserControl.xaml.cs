﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for RadAutoCompleteBoxWithAdvancedOptionsUserControl.xaml
    /// </summary>
    public partial class RadAutoCompleteBoxWithAdvancedOptionsUserControl
    {


        public static readonly DependencyProperty AdvancedOptionsTextProperty =
            DependencyProperty.Register("AdvancedOptionsText", typeof(String), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl), new PropertyMetadata("Manage List"));


        public String AdvancedOptionsText
        {
            get { return (String)GetValue(AdvancedOptionsTextProperty); }
            set { SetValue(AdvancedOptionsTextProperty, value); }
        }


        public static readonly DependencyProperty IsTouchProperty =
            DependencyProperty.Register("IsTouch", typeof(bool), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public bool IsTouch
        {
            get { return (bool)GetValue(IsTouchProperty); }
            set { SetValue(IsTouchProperty, value); }
        }


        public static readonly DependencyProperty AdvancedOptionsCommandProperty =
            DependencyProperty.Register("AdvancedOptionsCommand", typeof(ICommand), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public ICommand AdvancedOptionsCommand
        {
            get { return (ICommand)GetValue(AdvancedOptionsCommandProperty); }
            set { SetValue(AdvancedOptionsCommandProperty, value); }
        }

        public static readonly DependencyProperty AdvancedOptionsDetailsCommandProperty =
            DependencyProperty.Register("AdvancedOptionsDetailsCommand", typeof(ICommand), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public ICommand AdvancedOptionsDetailsCommand
        {
            get { return (ICommand)GetValue(AdvancedOptionsDetailsCommandProperty); }
            set { SetValue(AdvancedOptionsDetailsCommandProperty, value); }
        }



        public static readonly DependencyProperty AdvancedOptionsDetailsCommandParameterProperty =
            DependencyProperty.Register("AdvancedOptionsDetailsCommandParameter", typeof(object), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public object AdvancedOptionsDetailsCommandParameter
        {
            get { return GetValue(AdvancedOptionsDetailsCommandParameterProperty); }
            set { SetValue(AdvancedOptionsDetailsCommandParameterProperty, value); }
        }


        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }


        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl));


        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }


        public static readonly DependencyProperty AccentColorProperty =
            DependencyProperty.Register("AccentColor", typeof(Brush), typeof(RadAutoCompleteBoxWithAdvancedOptionsUserControl), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 100, 100, 100))));


        public Brush AccentColor
        {
            get { return (Brush)GetValue(AccentColorProperty); }
            set { SetValue(AccentColorProperty, value); }
        }
        

        public RadAutoCompleteBoxWithAdvancedOptionsUserControl()
        {
            InitializeComponent();
        }
    }
}
