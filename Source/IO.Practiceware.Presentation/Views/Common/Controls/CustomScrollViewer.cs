﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// The ScrollViewer control will set the Handled property of the Mouse Wheel Scroll event to true even if the scroll bars are not visible.  
    /// This CustomScrollViewer overrides the MouseWheel event and checks the vertical scroll bar visibility to determine whether or not it passes the event onward.
    /// This is useful in situations where you have a scroll viewer nested inside a main scroll viewer and you want the main viewer to capture the mouse wheel event
    /// when the inner viewer does not have its scroll bars visible
    /// </summary>
    public class CustomScrollViewer : ScrollViewer
    {
        private ScrollBar _verticalScrollbar;

        public override void OnApplyTemplate()
        {
            // Call base class
            base.OnApplyTemplate();

            // Obtain the vertical scrollbar
            _verticalScrollbar = GetTemplateChild("PART_VerticalScrollBar") as ScrollBar;
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            // Only handle this message if the vertical scrollbar is in use
            if ((_verticalScrollbar != null) && (_verticalScrollbar.Visibility == Visibility.Visible) && _verticalScrollbar.IsEnabled)
            {
                // Perform default handling
                base.OnMouseWheel(e);
            }
        }
    }
}
