﻿using IO.Practiceware.Model;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public partial class GroupAdministrationView
    {
        private readonly PresentationBackgroundWorker _worker;
        private bool _suspendEvents;

        public GroupAdministrationView()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _worker = new PresentationBackgroundWorker(this);
            _worker.DoWork += (sender, e) => LoadPresenter();
            _worker.RunWorkerCompleted += (sender, e) => OnPresenterLoaded();

            UsersList.SelectionChanged += OnUsersListSelectionChanged;
            GroupsList.SelectionChanged += OnGroupsListSelectionChanged;
            AddGroupButton.Click += OnAddGroupButtonClick;
            DeleteGroupButton.Click += OnDeleteGroupButtonClick;
        }

        public GroupAdministrationPresenter Presenter { get; set; }

        private void LoadPresenter()
        {
            var presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            presenter.Load();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InteractionContext.Completed += OnInteractionContextCompleted;
            _worker.RunWorkerAsync();
        }

        private void OnInteractionContextCompleted(object sender, EventArgs<bool?> e)
        {
            if (e.Value == true)
            {
                Presenter.Save();
            }
        }

        private void OnPresenterLoaded()
        {
            _suspendEvents = true;
            GroupsList.ItemsSource = null;
            GroupsList.ItemsSource = Presenter.Groups;
            GroupsList.DisplayMemberPath = "Name";
            UsersList.ItemsSource = null;
            UsersList.ItemsSource = Presenter.Users;
            UsersList.DisplayMemberPath = "DisplayName";
            GroupsList.SelectedItem = null;
            _suspendEvents = false;
            OnGroupsListSelectionChanged(GroupsList, new EventArgs());
        }

        private void OnAddGroupButtonClick(object sender, EventArgs e)
        {
            var groups = GroupsList.ItemsSource as ICollection<Group>;
            if (groups != null)
            {
                var result = InteractionManager.Current.Prompt("Please enter a name");
                if ((result.Input.IsNotNullOrEmpty()))
                {
                    _suspendEvents = true;
                    groups.Add(new Group { Name = result.Input });
                    _suspendEvents = false;
                }
            }
        }

        private void OnDeleteGroupButtonClick(object sender, EventArgs e)
        {
            var groups = GroupsList.ItemsSource as ICollection<Group>;
            var @group = GroupsList.SelectedItem as Group;
            if (groups != null && @group != null)
            {
                groups.Remove(@group);
                Presenter.Delete(@group);
            }
            GroupsList.SelectedIndex = 0;
        }


        private void OnGroupsListSelectionChanged(object sender, EventArgs e)
        {
            if (_suspendEvents)
            {
                return;
            }

            var @group = GroupsList.SelectedItem as Group;
            var suspendedEvents = _suspendEvents == false;
            _suspendEvents = true;

            // Clear selections for any previous group
            UsersList.UnselectAll();
            if ((@group != null))
            {
                // Select group users

                var array = UsersList.ItemsSource.OfType<object>().ToArray();

                foreach (var gr in @group.GroupUsers)
                {
                    for (int i = 0; i <= array.Length - 1; i++)
                    {
                        var user = array[i] as User;
                        if ((user != null && gr.User != null && gr.User.Id == user.Id))
                        {
                            // Group contains this user
                            UsersList.SelectedItems.Add(user);
                        }
                    }
                }
            }

            if ((suspendedEvents))
            {
                _suspendEvents = false;
            }
        }

        private void OnUsersListSelectionChanged(object sender, EventArgs e)
        {
            if (!_suspendEvents)
            {
                var @group = GroupsList.SelectedItem as Group;
                if ((@group != null))
                {
                    foreach (var user in UsersList.SelectedItems.Cast<User>())
                    {
                        if ((!@group.GroupUsers.Any(gu => gu.User.Id == user.Id)))
                        {
                            // Need to add a new GroupUser for the selected User
                            @group.GroupUsers.Add(new GroupUser { User = user });
                        }
                    }

                    var selectedUsers = UsersList.SelectedItems.Cast<User>();
                    // Must call to list to ensure no modifying collection during enumeration
                    foreach (var item in @group.GroupUsers.ToList())
                    {
                        var gu = item;
                        if (!selectedUsers.Any(user => user.Id == gu.User.Id))
                        {
                            @group.GroupUsers.Remove(gu);
                            Presenter.Delete(gu);
                        }
                    }
                }
            }
        }
    }

    public class GroupAdministrationPresenter
    {
        private readonly IPracticeRepository _practiceRepository;

        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public GroupAdministrationPresenter(IPracticeRepository practiceRepository, IUnitOfWorkProvider unitOfWorkProvider)
        {
            _practiceRepository = practiceRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public ICollection<Group> Groups { get; set; }

        public IEnumerable<User> Users { get; set; }

        public void Load()
        {
            using (_unitOfWorkProvider.Create())
            {
                var groups = _practiceRepository.Groups.Include(g => g.GroupUsers.Select(gr => gr.User)).OrderBy(g => g.Name).ToList().ToExtendedObservableCollection();

                var users = _practiceRepository.Users.Where(u => !u.IsArchived).OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList().ToExtendedObservableCollection();

                Groups = groups;
                Users = users;
            }
        }

        public void Delete(Group @group)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                @group.GroupUsers.Where(r => r.Id > 0).ToList().ForEach(_practiceRepository.Delete);
                _practiceRepository.Delete(@group);
                work.AcceptChanges();
            }
        }

        public void Delete(GroupUser groupUser)
        {
            if (groupUser.Id > 0) _practiceRepository.Delete(groupUser);
        }

        public void Save()
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                Groups.ToList().ForEach(g => _practiceRepository.Save(g));
                work.AcceptChanges();
            }
        }
    }
}