﻿using System.Collections;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for RadAutoCompleteBoxWithClearItemButtonUserControl.xaml
    /// </summary>
    public partial class RadAutoCompleteBoxWithClearItemButtonUserControl
    {
        public RadAutoCompleteBoxWithClearItemButtonUserControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ContentPaddingProperty =
            DependencyProperty.Register("ContentPadding", typeof(Thickness), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl), new PropertyMetadata(new Thickness(3, 0, 20, 0)));

        public Thickness ContentPadding
        {
            get { return (Thickness)GetValue(ContentPaddingProperty); }
            set { SetValue(ContentPaddingProperty, value); }
        }

        public static readonly DependencyProperty DropDownWidthProperty =
            DependencyProperty.Register("DropDownWidth", typeof(GridLength), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public GridLength DropDownWidth
        {
            get { return (GridLength)GetValue(DropDownWidthProperty); }
            set { SetValue(DropDownWidthProperty, value); }
        }

        public static readonly DependencyProperty IsTouchProperty =
            DependencyProperty.Register("IsTouch", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public static readonly DependencyProperty IsClearButtonEnabledProperty =
            DependencyProperty.Register("IsClearButtonEnabled", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool IsTouch
        {
            get { return (bool)GetValue(IsTouchProperty); }
            set { SetValue(IsTouchProperty, value); }
        }

        public bool IsClearButtonEnabled
        {
            get { return (bool)GetValue(IsClearButtonEnabledProperty); }
            set { SetValue(IsClearButtonEnabledProperty, value); }
        }

        public static readonly DependencyProperty ClearItemsAccentColorProperty =
            DependencyProperty.Register("ClearItemsAccentColor", typeof(Brush), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 100, 100, 100))));

        public Brush ClearItemsAccentColor
        {
            get { return (Brush)GetValue(ClearItemsAccentColorProperty); }
            set { SetValue(ClearItemsAccentColorProperty, value); }
        }

        public static readonly DependencyProperty TextSearchPathProperty =
            DependencyProperty.Register("TextSearchPath", typeof(string), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public string TextSearchPath
        {
            get { return (string)GetValue(TextSearchPathProperty); }
            set { SetValue(TextSearchPathProperty, value); }
        }

        public static readonly DependencyProperty SelectFirstItemProperty =
           DependencyProperty.Register("SelectFirstItem", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool SelectFirstItem
        {
            get { return (bool)GetValue(SelectFirstItemProperty); }
            set { SetValue(SelectFirstItemProperty, value); }
        }

        public static readonly DependencyProperty SyncSearchTextProperty =
           DependencyProperty.Register("SyncSearchText", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool SyncSearchText
        {
            get { return (bool)GetValue(SyncSearchTextProperty); }
            set { SetValue(SyncSearchTextProperty, value); }
        }

        public static readonly DependencyProperty EnableEnterKeyNavigationProperty =
           DependencyProperty.Register("EnableEnterKeyNavigation", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool EnableEnterKeyNavigation
        {
            get { return (bool)GetValue(EnableEnterKeyNavigationProperty); }
            set { SetValue(EnableEnterKeyNavigationProperty, value); }
        }

        public static readonly DependencyProperty SelectionChangedCommandProperty =
           DependencyProperty.Register("SelectionChangedCommand", typeof(ICommand), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public ICommand SelectionChangedCommand
        {
            get { return (ICommand)GetValue(SelectionChangedCommandProperty); }
            set { SetValue(SelectionChangedCommandProperty, value); }
        }

        public static readonly DependencyProperty SelectionChangedCommandIsEnabledProperty =
           DependencyProperty.Register("SelectionChangedCommandIsEnabled", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool SelectionChangedCommandIsEnabled
        {
            get { return (bool)GetValue(SelectionChangedCommandIsEnabledProperty); }
            set { SetValue(SelectionChangedCommandIsEnabledProperty, value); }
        }

        public static readonly DependencyProperty SelectionChangedCommandParameterProperty =
           DependencyProperty.Register("SelectionChangedCommandParameter", typeof(object), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public object SelectionChangedCommandParameter
        {
            get { return GetValue(SelectionChangedCommandParameterProperty); }
            set { SetValue(SelectionChangedCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty SearchCommandProperty =
           DependencyProperty.Register("SearchCommand", typeof(ICommand), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public ICommand SearchCommand
        {
            get { return (ICommand)GetValue(SearchCommandProperty); }
            set { SetValue(SearchCommandProperty, value); }
        }

        public static readonly DependencyProperty SearchButtonEnabledProperty =
           DependencyProperty.Register("SearchButtonEnabled", typeof(bool), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public bool SearchButtonEnabled
        {
            get { return (bool)GetValue(SearchButtonEnabledProperty); }
            set { SetValue(SearchButtonEnabledProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty DropDownItemTemplateProperty =
            DependencyProperty.Register("DropDownItemTemplate", typeof(DataTemplate), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public DataTemplate DropDownItemTemplate
        {
            get { return (DataTemplate)GetValue(DropDownItemTemplateProperty); }
            set { SetValue(DropDownItemTemplateProperty, value); }
        }

        public static readonly DependencyProperty WatermarkContentProperty =
            DependencyProperty.Register("WatermarkContent", typeof(string), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public string WatermarkContent
        {
            get { return (string)GetValue(WatermarkContentProperty); }
            set { SetValue(WatermarkContentProperty, value); }
        }

        public static readonly DependencyProperty DisplayMemberPathProperty =
            DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public string DisplayMemberPath
        {
            get { return (string)GetValue(DisplayMemberPathProperty); }
            set { SetValue(DisplayMemberPathProperty, value); }
        }

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(string), typeof(RadAutoCompleteBoxWithClearItemButtonUserControl));

        public string SearchText
        {
            get { return (string)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }
    }
}