﻿using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for HtmlBrowserView.xaml
    /// </summary>
    public partial class HtmlBrowserView
    {
        public HtmlBrowserView()
        {
            InitializeComponent();
        }

        #region HtmlSource Dependency Property

        /// <summary>
        /// Gets or sets the source string of the browser.  The string should be a valid Url or an html document.
        /// </summary>
        public static readonly DependencyProperty HtmlSourceProperty =
            DependencyProperty.Register("HtmlSource", typeof(string), typeof(HtmlBrowserView), new FrameworkPropertyMetadata(OnHtmlSourceChanged));

        /// <summary>
        /// Gets or sets the source string of the browser.  The string should be a valid Url or an html document.
        /// </summary>
        public string HtmlSource
        {
            get { return (string)GetValue(HtmlSourceProperty); }
            set { SetValue(HtmlSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the post data for the browser.  
        /// </summary>
        public static readonly DependencyProperty PostDataProperty =
            DependencyProperty.Register("PostData", typeof(byte[]), typeof(HtmlBrowserView), new FrameworkPropertyMetadata(OnHtmlSourceChanged));

        /// <summary>
        /// Gets or sets the post data for the browser.  
        /// </summary>
        public byte[] PostData
        {
            get { return (byte[])GetValue(PostDataProperty); }
            set { SetValue(PostDataProperty, value); }
        }



        private static void OnHtmlSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var htmlBrowserView = d as HtmlBrowserView;
            if (htmlBrowserView != null)
            {
                var webBrowser = htmlBrowserView.WebBrowser;
                if (webBrowser != null)
                {
                    var source = e.NewValue as string;

                    if (source == null) // if null go to blank page
                    {
                        webBrowser.Navigate("about:blank");
                    }
                    else if (Uri.IsWellFormedUriString(source, UriKind.RelativeOrAbsolute)) // if source is a valid url, then navigate to that url
                    {
                        if (htmlBrowserView.PostData != null)
                        {
                            webBrowser.Navigate(source, null, htmlBrowserView.PostData, null);
                        }
                        else
                        {
                            webBrowser.Navigate(source);
                        }
                    }
                    else // assume source is a valid html document
                    {
                        webBrowser.NavigateToString(source);
                    }

                }
            }
        }

        #endregion

    }

}
