﻿namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    partial class UserAdministrationView : Soaf.Presentation.Controls.WindowsForms.UserControl
    {
       //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.UsersList = new Soaf.Presentation.Controls.WindowsForms.ListBox();
            this.UsersStateButton = new Soaf.Presentation.Controls.WindowsForms.StateButton();
            this.SuspendLayout();
            // 
            // UsersList
            // 
            this.UsersList.DisplayMemberPath = "";
            this.UsersList.Location = new System.Drawing.Point(8, 8);
            this.UsersList.Name = "UsersList";
            this.UsersList.SelectedIndex = -1;
            this.UsersList.SelectedValuePath = "";
            this.UsersList.Size = new System.Drawing.Size(218, 322);
            this.UsersList.TabIndex = 0;
            // 
            // UsersStateButton
            // 
            this.UsersStateButton.CurrentState = "Exclude User";
            this.UsersStateButton.Location = new System.Drawing.Point(255, 8);
            this.UsersStateButton.Name = "UsersStateButton";
            this.UsersStateButton.Size = new System.Drawing.Size(100, 67);
            this.UsersStateButton.States = new string[] {
        "Exclude User",
        "Include User"};
            this.UsersStateButton.TabIndex = 3;
            // 
            // UserAdministrationView
            // 
            this.Controls.Add(this.UsersStateButton);
            this.Controls.Add(this.UsersList);
            this.Name = "UserAdministrationView";
            this.Size = new System.Drawing.Size(371, 337);
            this.ResumeLayout(false);

        }

        private Soaf.Presentation.Controls.WindowsForms.ListBox UsersList;
        private Soaf.Presentation.Controls.WindowsForms.StateButton UsersStateButton;
    }
}
