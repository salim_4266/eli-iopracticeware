﻿using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public class DropDownButtonCommands
    {
        // TODO: create on get:
        private static readonly RoutedCommand DropDownItemClickCommand = new RoutedCommand("DropDownItemClick", typeof(DropDownButtonCommands));

        public static RoutedCommand DropDownItemClick
        {
            get
            {
                return DropDownItemClickCommand;
            }
        }
    }
}