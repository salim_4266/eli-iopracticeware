﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for ConfirmationUserControl.xaml
    /// </summary>
    public partial class ConfirmationDialogView
    {
        public ConfirmationDialogView()
        {
            InitializeComponent();
        }
    }

    public class ConfirmationDialogViewContext : IViewContext
    {
        public ConfirmationDialogViewContext()
        {
            Load = Command.Create(ExecuteLoad);
            Closing = Command.Create(ExecuteClosing);
            Cancel = Command.Create(ExecuteCancel);
            Yes = Command.Create(ExecuteYes);
            No = Command.Create(ExecuteNo);
        }

        /// <summary>
        /// Execute Yes command.
        /// </summary>
        private void ExecuteYes()
        {
            LoadArguments.Yes.IfNotNull(c => c.Execute());
            // Close
            InteractionContext.Complete(true);
        }

        /// <summary>
        /// Execute No Command
        /// </summary>
        void ExecuteNo()
        {
            // No
            LoadArguments.No.IfNotNull(c => c.Execute());

            // Close
            InteractionContext.Complete(true);
        }

        /// <summary>
        /// Execute Cancel Command.
        /// </summary>
        private void ExecuteCancel()
        {
            LoadArguments.Cancel.IfNotNull(c => c.Execute());
            // Close
            InteractionContext.Complete(false);
        }

        private void ExecuteLoad()
        {
            if (LoadArguments == null) throw new Exception("LoadArguments are required.");

            Content = LoadArguments.Content ?? "Save Changes?";
            YesContent = LoadArguments.YesContent;
            NoContent = LoadArguments.NoContent;
            CancelContent = LoadArguments.CancelContent;
        }

        void ExecuteClosing()
        {
            // Cancel if non-positive closing result
            if (InteractionContext.DialogResult == null
                || InteractionContext.DialogResult == false)
            {
                // Cancel
                LoadArguments.Cancel.IfNotNull(c => c.Execute());
            }
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the load arguments
        /// </summary>
        public virtual ConfirmationDialogLoadArguments LoadArguments { get; set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public virtual ICommand Load { get; protected set; }

        /// <summary>
        /// Gets the closing command.
        /// </summary>
        public virtual ICommand Closing { get; protected set; }

        /// <summary>
        /// Gets the yes command.
        /// </summary>
        public virtual ICommand Yes { get; protected set; }

        /// <summary>
        /// Gets the no command.
        /// </summary>
        public virtual ICommand No { get; protected set; }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        public virtual ICommand Cancel { get; protected set; }

        /// <summary>
        /// Gets or sets the content of the confirmation.
        /// </summary>
        [DispatcherThread]
        public virtual object Content { get; protected set; }

        /// <summary>
        ///  Gets or sets the content of Yes button.
        /// </summary>
        [DispatcherThread]
        public virtual String YesContent { get; protected set; }

        /// <summary>
        ///  Gets or sets the content of No button.
        /// </summary>
        [DispatcherThread]
        public virtual String NoContent { get; protected set; }

        /// <summary>
        ///  Gets or sets the content of cancel button.
        /// </summary>
        [DispatcherThread]
        public virtual String CancelContent { get; protected set; }
    }
}
