﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for MultipleSelectionDropDownFilteringBox.xaml
    /// </summary>
    public partial class DropDownFilteringUserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DropDownFilteringUserControl"/> class.
        /// </summary>
        public DropDownFilteringUserControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty AccentColorProperty =
            DependencyProperty.Register("AccentColor", typeof(Brush), typeof(DropDownFilteringUserControl));

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IEnumerable), typeof(DropDownFilteringUserControl));

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(DropDownFilteringUserControl));

        public static readonly DependencyProperty ScrollPositionTemplateDataMemberProperty =
            DependencyProperty.Register("ScrollPositionTemplateDataMember", typeof(String), typeof(DropDownFilteringUserControl));

        public static readonly DependencyProperty WatermarkTextProperty =
            DependencyProperty.Register("WatermarkText", typeof(String), typeof(DropDownFilteringUserControl));

        public String ScrollPositionTemplateDataMember
        {
            get { return (String)GetValue(ScrollPositionTemplateDataMemberProperty); }
            set { SetValue(ScrollPositionTemplateDataMemberProperty, value); }
        }

        public String WatermarkText
        {
            get { return (String)GetValue(WatermarkTextProperty); }
            set { SetValue(WatermarkTextProperty, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public IEnumerable SelectedItems
        {
            get { return (IEnumerable)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        public Brush AccentColor
        {
            get { return (Brush) GetValue(AccentColorProperty); }
            set { SetValue(AccentColorProperty, value); }
        }
    }
}
