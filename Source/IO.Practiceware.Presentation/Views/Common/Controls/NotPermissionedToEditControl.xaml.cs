﻿
namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    /// Interaction logic for NotPermissionedToEditControl.xaml
    /// </summary>
    public partial class NotPermissionedToEditControl
    {
        public NotPermissionedToEditControl()
        {
            InitializeComponent();
        }
    }
}
