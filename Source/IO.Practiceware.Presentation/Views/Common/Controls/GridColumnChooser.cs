﻿using System;
using System.Globalization;
using Soaf.ComponentModel;
using Soaf.Reflection;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using Telerik.Windows.Controls;
using GridViewColumn = Telerik.Windows.Controls.GridViewColumn;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public class GridColumnChooser : ContextMenu
    {
        public static readonly DependencyProperty GridProperty =
            DependencyProperty.Register("Grid", typeof(RadGridView), typeof(GridColumnChooser), new PropertyMetadata(default(RadGridView), OnGridChanged));

        private static DataTemplate _headerTemplate;

        public RadGridView Grid
        {
            get { return (RadGridView)GetValue(GridProperty); }
            set { SetValue(GridProperty, value); }
        }

        private static DataTemplate HeaderTemplate
        {
            get { return _headerTemplate ?? (_headerTemplate = GetHeaderTemplate()); }
        }

        private static void OnGridChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (GridColumnChooser)d;

            var grid = e.OldValue as RadGridView;
            if (grid != null)
            {
                grid.Columns.CollectionChanged -= WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(instance.OnColumnsChanged);
            }

            instance.Items.Clear();

            grid = e.NewValue as RadGridView;

            if (grid != null)
            {
                grid.Columns.CollectionChanged += WeakDelegate.MakeWeak<NotifyCollectionChangedEventHandler>(instance.OnColumnsChanged);
                instance.UpdateMenuItems(grid);
            }
        }

        private void OnColumnsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateMenuItems(Grid);
        }

        private void UpdateMenuItems(RadGridView grid)
        {
            DataTemplate template = HeaderTemplate;

            Items.Clear();
            foreach (GridViewColumn column in grid.Columns)
            {
                string groupName = null;
                var dataColumn = column as GridViewDataColumn;
                if (dataColumn != null && dataColumn.DataMemberBinding != null && dataColumn.DataMemberBinding.Path != null)
                {
                    MemberInfo boundMemberInfo = dataColumn.ItemType.GetMember(dataColumn.DataMemberBinding.Path.Path, MemberTypes.Field | MemberTypes.Property, BindingFlags.Instance | BindingFlags.Public).FirstOrDefault();
                    if (boundMemberInfo != null) groupName = boundMemberInfo.GetAttributes<DisplayAttribute>().Select(a => a.GroupName).FirstOrDefault();
                }

                var menuItem = new MenuItem { IsCheckable = true };
                menuItem.HeaderTemplate = template;
                menuItem.HorizontalAlignment = HorizontalAlignment.Stretch;
                menuItem.HorizontalContentAlignment = HorizontalAlignment.Left;
                BindingOperations.SetBinding(menuItem, MenuItem.IsCheckedProperty, new Binding("IsVisible") { Source = column, Mode = BindingMode.TwoWay });
                BindingOperations.SetBinding(menuItem, HeaderedItemsControl.HeaderProperty, new Binding("Header") { Source = column, Converter = new HeaderConverter() });

                if (groupName == null)
                {
                    Items.Add(menuItem);
                }
                else
                {
                    string groupUniqueName = groupName.IndexOf(" ") > 0 ? groupName.Replace(" ", "") : groupName;
                    MenuItem groupMenuItem = Items.OfType<MenuItem>().FirstOrDefault(i => i.Name == groupUniqueName);
                    if (groupMenuItem == null)
                    {
                        groupMenuItem = new MenuItem { Header = groupName, Name = groupUniqueName };
                        groupMenuItem.HeaderTemplate = template;
                        groupMenuItem.HorizontalAlignment = HorizontalAlignment.Stretch;
                        groupMenuItem.HorizontalContentAlignment = HorizontalAlignment.Left;
                        Items.Add(groupMenuItem);
                    }
                    groupMenuItem.Items.Add(menuItem);
                }
            }
        }

        private static DataTemplate GetHeaderTemplate()
        {
            DataTemplate template;
            const string templateText = @"            
            <DataTemplate DataType=""{x:Type system:String}"" xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"" xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:system=""clr-namespace:System;assembly=mscorlib"">
                <TextBlock Text=""{Binding}"" TextAlignment=""Left"" />
            </DataTemplate>";
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(templateText)))
            {
                template = (DataTemplate)XamlReader.Load(ms);
            }
            return template;
        }

        private class HeaderConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (value is string) return value;

                var dependencyObject = value as DependencyObject;
                if (dependencyObject != null)
                {
                    var text = dependencyObject.GetChildren<ContentControl>().Select(contentControl => contentControl.Content).FirstOrDefault();
                    if (text != null) return text;
                }
                return value;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return value;
            }
        }
    }
}