using IO.Practiceware.Model;
using System;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public partial class QuestionAnswerView
    {
        public QuestionAnswerView()
        {
            InitializeComponent();

            ValueTextBox.Control.PreviewMouseLeftButtonDown += ValueTextBoxClick;
        }

        public int QuestionId { get; set; }

        public string Value
        {
            get { return ValueTextBox.Text; }
            set { ValueTextBox.Text = value; }
        }

        public string Note
        {
            get { return NoteTextBox.Text; }
            set { NoteTextBox.Text = value; }
        }

        public void AddQuestion(Question question)
        {
            QuestionId = question.Id;
            QuestionLabel.Text = question.Label;
        }

        private void ValueTextBoxClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ValueTextBox.Text.Trim()))
            {
                ValueTextBox.Text = DateTime.Now.ToClientTime().ToShortTimeString();
            }
        }
    }
}