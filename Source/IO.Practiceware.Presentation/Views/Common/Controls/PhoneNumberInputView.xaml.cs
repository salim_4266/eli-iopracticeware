﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    /// <summary>
    ///   Interaction logic for PhoneNumberInputView.xaml
    /// </summary>
    public partial class PhoneNumberInputView
    {

        public static readonly DependencyProperty IsPhoneHighlightProperty =
           DependencyProperty.Register("IsPhoneHighlight", typeof(bool), typeof(PhoneNumberInputView), new PropertyMetadata(OnPhoneChanged));

        public static readonly DependencyProperty PhoneNumberProperty =
            DependencyProperty.Register("PhoneNumber", typeof(PhoneNumberViewModel), typeof(PhoneNumberInputView));

        public static readonly DependencyProperty PhoneTypesProperty =
            DependencyProperty.Register("PhoneTypes", typeof(IEnumerable<object>), typeof(PhoneNumberInputView));

        public static readonly DependencyProperty PhoneTypesDisplayMemberPathProperty =
            DependencyProperty.Register("PhoneTypesDisplayMemberPath", typeof(string), typeof(PhoneNumberInputView), new PropertyMetadata("Name"));

        public static readonly DependencyProperty PhoneTypesStaysOpenOnEditProperty =
            DependencyProperty.Register("PhoneTypesStaysOpenOnEdit", typeof(bool), typeof(PhoneNumberInputView));

        public static readonly DependencyProperty IsPrimaryCheckboxVisibilityProperty =
            DependencyProperty.Register("IsPrimaryCheckboxVisibility", typeof(Visibility), typeof(PhoneNumberInputView), new PropertyMetadata(Visibility.Collapsed, OnIsPrimaryCheckboxVisibilityChanged));

        private static void OnIsPrimaryCheckboxVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var phoneNumberInputView = d as PhoneNumberInputView;
            if (phoneNumberInputView != null)
            {
                //this is now handled in xaml
                //phoneNumberInputView.Width = phoneNumberInputView.IsPrimaryCheckboxVisibility == Visibility.Visible ? 505 : 440;
            }
        }

        public PhoneNumberInputView()
        {
            InitializeComponent();
        }

        public PhoneNumberViewModel PhoneNumber
        {
            get { return GetValue(PhoneNumberProperty) as PhoneNumberViewModel; }
            set { SetValue(PhoneNumberProperty, value); }
        }

        public Visibility IsPrimaryCheckboxVisibility
        {
            get { return (Visibility)GetValue(IsPrimaryCheckboxVisibilityProperty); }
            set { SetValue(IsPrimaryCheckboxVisibilityProperty, value); }
        }

        public bool PhoneTypesStaysOpenOnEdit
        {
            get { return (bool)GetValue(PhoneTypesStaysOpenOnEditProperty); }
            set { SetValue(PhoneTypesStaysOpenOnEditProperty, value); }
        }

        public string PhoneTypesDisplayMemberPath
        {
            get { return GetValue(PhoneTypesDisplayMemberPathProperty) as string; }
            set { SetValue(PhoneTypesDisplayMemberPathProperty, value); }
        }

        public IEnumerable<object> PhoneTypes
        {
            get { return GetValue(PhoneTypesProperty) as IEnumerable<object>; }
            set { SetValue(PhoneTypesProperty, value); }
        }
        public bool IsPhoneHighlight
        {
            get { return (bool)GetValue(IsPhoneHighlightProperty); }
            set { SetValue(IsPhoneHighlightProperty, value); }
        }
        private static void OnPhoneChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            var view = ((PhoneNumberInputView)d);
            if (view.Extension.Text == string.Empty)
            {
                view.Extension.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                view.Extension.BorderBrush = Brushes.LightGray;
            }

            if (view.Area.Text == string.Empty)
            {
                view.Area.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                view.Area.BorderBrush = Brushes.LightGray;
            }

            if (view.ExchangeAndSuffix.Text == string.Empty)
            {
                view.ExchangeAndSuffix.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                view.ExchangeAndSuffix.BorderBrush = Brushes.LightGray;
            }

            if (string.IsNullOrEmpty(view.TypeComboBox.Text))
            {
                view.brdType.BorderBrush= Brushes.DeepSkyBlue;
                view.brdType.BorderThickness = new Thickness(1, 1, 1, 1); 
            }
            else
            {
                view.brdType.BorderBrush = Brushes.LightGray;
                view.brdType.BorderThickness = new Thickness(0, 0, 0, 0);
            }
            
        }

        private void Area_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Area.Text == string.Empty)
            {
                Area.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                Area.BorderBrush = Brushes.LightGray;
            }
        }

        private void ExchangeAndSuffix_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ExchangeAndSuffix.Text == string.Empty)
            {
                ExchangeAndSuffix.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                ExchangeAndSuffix.BorderBrush = Brushes.LightGray;
            }
        }

        private void Extension_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Extension.Text == string.Empty)
            {
                Extension.BorderBrush = Brushes.DeepSkyBlue;
            }
            else
            {
                Extension.BorderBrush = Brushes.LightGray;
            }
        }

        private void TypeComboBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                brdType.BorderBrush = Brushes.DeepSkyBlue;
                brdType.BorderThickness = new Thickness(1, 1, 1, 1);
            }
            else
            {
                brdType.BorderBrush = Brushes.LightGray;
                brdType.BorderThickness = new Thickness(0, 0, 0, 0);
            }
        }
    }
}