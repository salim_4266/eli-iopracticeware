﻿using Soaf.ComponentModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace IO.Practiceware.Presentation.Views.Common.Controls
{
    public class RadDockingResetLayoutButton : Button
    {
        public static readonly DependencyProperty DockingProperty =
            DependencyProperty.Register("Docking", typeof(DockingManager), typeof(RadDockingResetLayoutButton), new PropertyMetadata(default(DockingManager), OnDockingChanged));

        private MemoryStream _ms;

        public DockingManager Docking
        {
            get { return (DockingManager)GetValue(DockingProperty); }
            set { SetValue(DockingProperty, value); }
        }

        private static void OnDockingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
#if DEBUG
            // special code used to check if the current execution context is the visual studio designer
            if (DesignerProperties.GetIsInDesignMode(new Button()))
            {
                return;
            }
#endif

            var docking = e.NewValue as DockingManager;
            if (docking == null) return;

            docking.Loaded += new RoutedEventHandler(((RadDockingResetLayoutButton)d).OnDockingLoaded).MakeWeak<RoutedEventHandler>();
        }

        private void OnDockingLoaded(object sender, RoutedEventArgs e)
        {
            _ms = new MemoryStream();

            new XmlLayoutSerializer(Docking).Serialize(_ms);
        }

        protected override void OnClick()
        {
            if (Docking != null && _ms != null)
            {

                var layoutSerializer = new XmlLayoutSerializer(Docking);
                _ms.Seek(0, SeekOrigin.Begin);
                layoutSerializer.Deserialize(_ms);
            }
            base.OnClick();
        }
    }
}