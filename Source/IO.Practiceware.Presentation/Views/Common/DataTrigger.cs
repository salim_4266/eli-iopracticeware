﻿using System.Windows;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    ///   A DataTrigger that supports additional functionality, such as firing only when the old value is different than the new value.
    /// </summary>
    public class DataTrigger : Microsoft.Expression.Interactivity.Core.DataTrigger
    {
        public DataTrigger()
        {
            OnlyFireValueDifferent = true;
        }

        /// <summary>
        ///   Gets or sets a value indicating whether to only fire if value is different. Default is true.
        /// </summary>
        /// <value> <c>true</c> if [only fire value different]; otherwise, <c>false</c> . </value>
        public bool OnlyFireValueDifferent { get; set; }

        protected override void EvaluateBindingChange(object args)
        {
            if (args is DependencyPropertyChangedEventArgs)
            {
                var e = (DependencyPropertyChangedEventArgs) args;
                if (Equals(e.NewValue, e.OldValue)) return;
            }
            base.EvaluateBindingChange(args);
        }
    }
}