﻿using System.Windows.Data;
using IO.Practiceware.Presentation.Views.Common.Converters;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    ///     A custom markup extension that allows binding using complex expressions.
    /// </summary>
    public class BindingEx : BindingDecoratorBase
    {
        /// <summary>
        /// Gets or sets an expression to apply.
        /// </summary>
        /// <value>
        /// The expression.
        /// </value>
        public string Expression { get; set; }

        public override object ProvideValue(System.IServiceProvider provider)
        {
            var result = base.ProvideValue(provider);
            var binding = result as Binding;
            if (binding != null && Expression != null)
            {
                binding.Converter = new ExpressionConverter { InnerConverter = binding.Converter, Expression = Expression };
            }
            return result;
        }
    }
}