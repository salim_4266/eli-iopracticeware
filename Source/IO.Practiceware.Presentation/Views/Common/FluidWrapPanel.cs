﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace IO.Practiceware.Presentation.Views.Common
{

    // -------------------------------------------------------------------------------
    // 
    // This file is part of the WPFSpark project: http://wpfspark.codeplex.com/
    //
    // Author: Ratish Philip
    // 
    // WPFSpark v1.1
    //
    // -------------------------------------------------------------------------------

    /*
     * 
     * IOP: Additional support was added to re-arrange items in the bound source collections and support binding to non-visual elements (see UpdateItemsSourceIndex).
     * 
     */

    /// <summary>
    /// Defines the Drag Behavior in the FluidWrapPanel using the Mouse
    /// </summary>
    public class FluidMouseDragBehavior : Behavior<UIElement>
    {
        public FluidMouseDragBehavior()
        {
            timer = new DispatcherTimer(TimeSpan.FromMilliseconds(200), DispatcherPriority.Normal, new EventHandler(delegate { StartDragging(); }), Dispatcher);
        }

        #region Fields

        FluidWrapPanel parentFWPanel = null;
        ListBoxItem parentLBItem = null;

        #endregion

        #region Dependency Properties

        #region DragButton

        /// <summary>
        /// DragButton Dependency Property
        /// </summary>
        public static readonly DependencyProperty DragButtonProperty =
            DependencyProperty.Register("DragButton", typeof(MouseButton), typeof(FluidMouseDragBehavior),
                new FrameworkPropertyMetadata(MouseButton.Left));

        /// <summary>
        /// Gets or sets the DragButton property. This dependency property 
        /// indicates which Mouse button should participate in the drag interaction.
        /// </summary>
        public MouseButton DragButton
        {
            get { return (MouseButton)GetValue(DragButtonProperty); }
            set { SetValue(DragButtonProperty, value); }
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// 
        /// </summary>
        protected override void OnAttached()
        {
            // Subscribe to the Loaded event
            (this.AssociatedObject as FrameworkElement).Loaded += new RoutedEventHandler(OnAssociatedObjectLoaded);
        }

        void OnAssociatedObjectLoaded(object sender, RoutedEventArgs e)
        {
            // Get the parent FluidWrapPanel and check if the AssociatedObject is
            // hosted inside a ListBoxItem (this scenario will occur if the FluidWrapPanel
            // is the ItemsPanel for a ListBox).
            GetParentPanel();

            // Subscribe to the Mouse down/move/up events
            if (parentLBItem != null)
            {
                parentLBItem.PreviewMouseDown += new MouseButtonEventHandler(OnPreviewMouseDown);
                parentLBItem.PreviewMouseMove += new MouseEventHandler(OnPreviewMouseMove);
                parentLBItem.PreviewMouseUp += new MouseButtonEventHandler(OnPreviewMouseUp);
            }
            else
            {
                this.AssociatedObject.PreviewMouseDown += new MouseButtonEventHandler(OnPreviewMouseDown);
                this.AssociatedObject.PreviewMouseMove += new MouseEventHandler(OnPreviewMouseMove);
                this.AssociatedObject.PreviewMouseUp += new MouseButtonEventHandler(OnPreviewMouseUp);
            }
        }

        /// <summary>
        /// Get the parent FluidWrapPanel and check if the AssociatedObject is
        /// hosted inside a ListBoxItem (this scenario will occur if the FluidWrapPanel
        /// is the ItemsPanel for a ListBox).
        /// </summary>
        private void GetParentPanel()
        {
            FrameworkElement ancestor = this.AssociatedObject as FrameworkElement;

            while (ancestor != null)
            {
                if (ancestor is ListBoxItem)
                {
                    parentLBItem = ancestor as ListBoxItem;
                }

                if (ancestor is FluidWrapPanel)
                {
                    parentFWPanel = ancestor as FluidWrapPanel;
                    // No need to go further up
                    return;
                }

                // Find the visual ancestor of the current item
                ancestor = VisualTreeHelper.GetParent(ancestor) as FrameworkElement;
            }
        }

        protected override void OnDetaching()
        {
            (this.AssociatedObject as FrameworkElement).Loaded -= OnAssociatedObjectLoaded;
            if (parentLBItem != null)
            {
                parentLBItem.PreviewMouseDown -= OnPreviewMouseDown;
                parentLBItem.PreviewMouseMove -= OnPreviewMouseMove;
                parentLBItem.PreviewMouseUp -= OnPreviewMouseUp;
            }
            else
            {
                this.AssociatedObject.PreviewMouseDown -= OnPreviewMouseDown;
                this.AssociatedObject.PreviewMouseMove -= OnPreviewMouseMove;
                this.AssociatedObject.PreviewMouseUp -= OnPreviewMouseUp;
            }
        }

        #endregion

        #region Event Handlers

        bool isMouseDown = false;
        bool canDrag = false;
        MouseEventArgs lastMouseEventArgs;
        readonly DispatcherTimer timer;

        private void StartDragging()
        {
            timer.Stop();

            if (!isMouseDown || lastMouseEventArgs == null) return;

            Point position = parentLBItem != null ? lastMouseEventArgs.GetPosition(parentLBItem) : lastMouseEventArgs.GetPosition(this.AssociatedObject);

            FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
            if ((fElem != null) && (parentFWPanel != null))
            {
                if (parentLBItem != null)
                    parentFWPanel.BeginFluidDrag(parentLBItem, position);
                else
                    parentFWPanel.BeginFluidDrag(this.AssociatedObject, position);
            }
            canDrag = true;
        }

        void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == DragButton)
            {
                lastMouseEventArgs = e;
                isMouseDown = true;

                timer.Start();
            }
        }

        void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            lastMouseEventArgs = e;

            if (!canDrag) return;

            bool isDragging = false;

            switch (DragButton)
            {
                case MouseButton.Left:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;
                case MouseButton.Middle:
                    if (e.MiddleButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;
                case MouseButton.Right:
                    if (e.RightButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;
                case MouseButton.XButton1:
                    if (e.XButton1 == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;
                case MouseButton.XButton2:
                    if (e.XButton2 == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;
                default:
                    break;
            }

            if (isDragging)
            {
                Point position = parentLBItem != null ? e.GetPosition(parentLBItem) : e.GetPosition(this.AssociatedObject);

                FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
                if ((fElem != null) && (parentFWPanel != null))
                {
                    Point positionInParent = e.GetPosition(parentFWPanel);
                    if (parentLBItem != null)
                        parentFWPanel.FluidDrag(parentLBItem, position, positionInParent);
                    else
                        parentFWPanel.FluidDrag(this.AssociatedObject, position, positionInParent);
                }
            }
        }

        void OnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            isMouseDown = false;
            canDrag = false;

            if (e.ChangedButton == DragButton)
            {
                Point position = parentLBItem != null ? e.GetPosition(parentLBItem) : e.GetPosition(this.AssociatedObject);

                FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
                if ((fElem != null) && (parentFWPanel != null))
                {
                    Point positionInParent = e.GetPosition(parentFWPanel);
                    if (parentLBItem != null)
                        parentFWPanel.EndFluidDrag(parentLBItem, position, positionInParent);
                    else
                        parentFWPanel.EndFluidDrag(this.AssociatedObject, position, positionInParent);
                }
            }
        }

        #endregion
    }
    /// <summary>
    /// Class which helps in the layout management for the FluidWrapPanel
    /// </summary>
    internal sealed class FluidLayoutManager
    {
        #region Fields

        private Size panelSize;
        private Size cellSize;
        private Orientation panelOrientation;
        private Int32 cellsPerLine;

        #endregion

        #region APIs

        /// <summary>
        /// Calculates the initial location of the child in the FluidWrapPanel
        /// when the child is added.
        /// </summary>
        /// <param name="index">Index of the child in the FluidWrapPanel</param>
        /// <returns></returns>
        internal Point GetInitialLocationOfChild(int index)
        {
            Point result = new Point();

            int row, column;

            GetCellFromIndex(index, out row, out column);

            int maxRows = (Int32)Math.Floor(panelSize.Height / cellSize.Height);
            int maxCols = (Int32)Math.Floor(panelSize.Width / cellSize.Width);

            bool isLeft = true;
            bool isTop = true;
            bool isCenterHeight = false;
            bool isCenterWidth = false;

            int halfRows = 0;
            int halfCols = 0;

            halfRows = (int)((double)maxRows / (double)2);

            // Even number of rows
            if ((maxRows % 2) == 0)
            {
                isTop = row < halfRows;
            }
            // Odd number of rows
            else
            {
                if (row == halfRows)
                {
                    isCenterHeight = true;
                    isTop = false;
                }
                else
                {
                    isTop = row < halfRows;
                }
            }

            halfCols = (int)((double)maxCols / (double)2);

            // Even number of columns
            if ((maxCols % 2) == 0)
            {
                isLeft = column < halfCols;
            }
            // Odd number of columns
            else
            {
                if (column == halfCols)
                {
                    isCenterWidth = true;
                    isLeft = false;
                }
                else
                {
                    isLeft = column < halfCols;
                }
            }

            if (isCenterHeight && isCenterWidth)
            {
                double posX = (halfCols) * cellSize.Width;
                double posY = (halfRows + 2) * cellSize.Height;

                return new Point(posX, posY);
            }

            if (isCenterHeight)
            {
                if (isLeft)
                {
                    double posX = ((halfCols - column) + 1) * cellSize.Width;
                    double posY = (halfRows) * cellSize.Height;

                    result = new Point(-posX, posY);
                }
                else
                {
                    double posX = ((column - halfCols) + 1) * cellSize.Width;
                    double posY = (halfRows) * cellSize.Height;

                    result = new Point(panelSize.Width + posX, posY);
                }

                return result;
            }

            if (isCenterWidth)
            {
                if (isTop)
                {
                    double posX = (halfCols) * cellSize.Width;
                    double posY = ((halfRows - row) + 1) * cellSize.Height;

                    result = new Point(posX, -posY);
                }
                else
                {
                    double posX = (halfCols) * cellSize.Width;
                    double posY = ((row - halfRows) + 1) * cellSize.Height;

                    result = new Point(posX, panelSize.Height + posY);
                }

                return result;
            }

            if (isTop)
            {
                if (isLeft)
                {
                    double posX = ((halfCols - column) + 1) * cellSize.Width;
                    double posY = ((halfRows - row) + 1) * cellSize.Height;

                    result = new Point(-posX, -posY);
                }
                else
                {
                    double posX = ((column - halfCols) + 1) * cellSize.Width;
                    double posY = ((halfRows - row) + 1) * cellSize.Height;

                    result = new Point(posX + panelSize.Width, -posY);
                }
            }
            else
            {
                if (isLeft)
                {
                    double posX = ((halfCols - column) + 1) * cellSize.Width;
                    double posY = ((row - halfRows) + 1) * cellSize.Height;

                    result = new Point(-posX, panelSize.Height + posY);
                }
                else
                {
                    double posX = ((column - halfCols) + 1) * cellSize.Width;
                    double posY = ((row - halfRows) + 1) * cellSize.Height;

                    result = new Point(posX + panelSize.Width, panelSize.Height + posY);
                }
            }

            return result;
        }

        /// <summary>
        /// Initializes the FluidLayoutManager
        /// </summary>
        /// <param name="panelWidth">Width of the FluidWrapPanel</param>
        /// <param name="panelHeight">Height of the FluidWrapPanel</param>
        /// <param name="cellWidth">Width of each child in the FluidWrapPanel</param>
        /// <param name="cellHeight">Height of each child in the FluidWrapPanel</param>
        /// <param name="orientation">Orientation of the panel - Horizontal or Vertical</param>
        internal void Initialize(double panelWidth, double panelHeight, double cellWidth, double cellHeight, Orientation orientation)
        {
            if (panelWidth <= 0.0d)
                panelWidth = cellWidth;
            if (panelHeight <= 0.0d)
                panelHeight = cellHeight;
            if ((cellWidth <= 0.0d) || (cellHeight <= 0.0d))
            {
                cellsPerLine = 0;
                return;
            }

            if ((panelSize.Width != panelWidth) ||
                (panelSize.Height != panelHeight) ||
                (cellSize.Width != cellWidth) ||
                (cellSize.Height != cellHeight))
            {
                panelSize = new Size(panelWidth, panelHeight);
                cellSize = new Size(cellWidth, cellHeight);
                panelOrientation = orientation;

                // Calculate the number of cells that can be fit in a line
                CalculateCellsPerLine();
            }
        }

        /// <summary>
        /// Provides the index of the child (in the FluidWrapPanel's children) from the given row and column
        /// </summary>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        /// <returns>Index</returns>
        internal int GetIndexFromCell(int row, int column)
        {
            int result = -1;

            if ((row >= 0) && (column >= 0))
            {
                switch (panelOrientation)
                {
                    case Orientation.Horizontal:
                        result = (cellsPerLine * row) + column;
                        break;
                    case Orientation.Vertical:
                        result = (cellsPerLine * column) + row;
                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Provides the index of the child (in the FluidWrapPanel's children) from the given point
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal int GetIndexFromPoint(Point p)
        {
            int result = -1;
            if ((p.X > 0.00D) &&
                (p.X < panelSize.Width) &&
                (p.Y > 0.00D) &&
                (p.Y < panelSize.Height))
            {
                int row;
                int column;

                GetCellFromPoint(p, out row, out column);
                result = GetIndexFromCell(row, column);
            }

            return result;
        }

        /// <summary>
        /// Provides the row and column of the child based on its index in the FluidWrapPanel.Children
        /// </summary>
        /// <param name="index">Index</param>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        internal void GetCellFromIndex(int index, out int row, out int column)
        {
            row = column = -1;

            if (index >= 0)
            {
                switch (panelOrientation)
                {
                    case Orientation.Horizontal:
                        row = (int)(index / (double)cellsPerLine);
                        column = (int)(index % (double)cellsPerLine);
                        break;
                    case Orientation.Vertical:
                        column = (int)(index / (double)cellsPerLine);
                        row = (int)(index % (double)cellsPerLine);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Provides the row and column of the child based on its location in the FluidWrapPanel
        /// </summary>
        /// <param name="p">Location of the child in the parent</param>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        internal void GetCellFromPoint(Point p, out int row, out int column)
        {
            row = column = -1;

            if ((p.X < 0.00D) ||
                (p.X > panelSize.Width) ||
                (p.Y < 0.00D) ||
                (p.Y > panelSize.Height))
            {
                return;
            }

            row = (int)(p.Y / cellSize.Height);
            column = (int)(p.X / cellSize.Width);
        }

        /// <summary>
        /// Provides the location of the child in the FluidWrapPanel based on the given row and column
        /// </summary>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        /// <returns>Location of the child in the panel</returns>
        internal Point GetPointFromCell(int row, int column)
        {
            Point result = new Point();

            if ((row >= 0) && (column >= 0))
            {
                result = new Point(cellSize.Width * column, cellSize.Height * row);
            }

            return result;
        }

        /// <summary>
        /// Provides the location of the child in the FluidWrapPanel based on the given row and column
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>Location of the child in the panel</returns>
        internal Point GetPointFromIndex(int index)
        {
            Point result = new Point();

            if (index >= 0)
            {
                int row;
                int column;

                GetCellFromIndex(index, out row, out column);
                result = GetPointFromCell(row, column);
            }

            return result;
        }

        /// <summary>
        /// Creates a TransformGroup based on the given Translation, Scale and Rotation
        /// </summary>
        /// <param name="transX">Translation in the X-axis</param>
        /// <param name="transY">Translation in the Y-axis</param>
        /// <param name="scaleX">Scale factor in the X-axis</param>
        /// <param name="scaleY">Scale factor in the Y-axis</param>
        /// <param name="rotAngle">Rotation</param>
        /// <returns>TransformGroup</returns>
        internal TransformGroup CreateTransform(double transX, double transY, double scaleX, double scaleY, double rotAngle = 0.0D)
        {
            TranslateTransform translation = new TranslateTransform();
            translation.X = transX;
            translation.Y = transY;

            ScaleTransform scale = new ScaleTransform();
            scale.ScaleX = scaleX;
            scale.ScaleY = scaleY;

            //RotateTransform rotation = new RotateTransform();
            //rotation.Angle = rotAngle;

            TransformGroup transform = new TransformGroup();
            // THE ORDER OF TRANSFORM IS IMPORTANT
            // First, scale, then rotate and finally translate
            transform.Children.Add(scale);
            //transform.Children.Add(rotation);
            transform.Children.Add(translation);

            return transform;
        }

        /// <summary>
        /// Creates the storyboard for animating a child from its old location to the new location.
        /// The Translation and Scale properties are animated.
        /// </summary>
        /// <param name="element">UIElement for which the storyboard has to be created</param>
        /// <param name="newLocation">New location of the UIElement</param>
        /// <param name="period">Duration of animation</param>
        /// <param name="easing">Easing function</param>
        /// <returns>Storyboard</returns>
        internal Storyboard CreateTransition(UIElement element, Point newLocation, TimeSpan period, EasingFunctionBase easing)
        {
            Duration duration = new Duration(period);

            // Animate X
            DoubleAnimation translateAnimationX = new DoubleAnimation();
            translateAnimationX.To = newLocation.X;
            translateAnimationX.Duration = duration;
            if (easing != null)
                translateAnimationX.EasingFunction = easing;

            Storyboard.SetTarget(translateAnimationX, element);
            Storyboard.SetTargetProperty(translateAnimationX,
                new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[1].(TranslateTransform.X)"));

            // Animate Y
            DoubleAnimation translateAnimationY = new DoubleAnimation();
            translateAnimationY.To = newLocation.Y;
            translateAnimationY.Duration = duration;
            if (easing != null)
                translateAnimationY.EasingFunction = easing;

            Storyboard.SetTarget(translateAnimationY, element);
            Storyboard.SetTargetProperty(translateAnimationY,
                new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[1].(TranslateTransform.Y)"));

            // Animate ScaleX
            DoubleAnimation scaleAnimationX = new DoubleAnimation();
            scaleAnimationX.To = 1.0D;
            scaleAnimationX.Duration = duration;
            if (easing != null)
                scaleAnimationX.EasingFunction = easing;

            Storyboard.SetTarget(scaleAnimationX, element);
            Storyboard.SetTargetProperty(scaleAnimationX,
                new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)"));

            // Animate ScaleY
            DoubleAnimation scaleAnimationY = new DoubleAnimation();
            scaleAnimationY.To = 1.0D;
            scaleAnimationY.Duration = duration;
            if (easing != null)
                scaleAnimationY.EasingFunction = easing;

            Storyboard.SetTarget(scaleAnimationY, element);
            Storyboard.SetTargetProperty(scaleAnimationY,
                new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)"));

            Storyboard sb = new Storyboard();
            sb.Duration = duration;
            sb.Children.Add(translateAnimationX);
            sb.Children.Add(translateAnimationY);
            sb.Children.Add(scaleAnimationX);
            sb.Children.Add(scaleAnimationY);

            return sb;
        }

        /// <summary>
        /// Gets the total size taken up by the children after the Arrange Layout Phase
        /// </summary>
        /// <param name="childrenCount">Number of children</param>
        /// <param name="finalSize">Available size provided by the FluidWrapPanel</param>
        /// <returns>Total size</returns>
        internal Size GetArrangedSize(int childrenCount, Size finalSize)
        {
            if ((cellsPerLine == 0.0) || (childrenCount == 0))
                return finalSize;

            int numLines = (Int32)(childrenCount / (double)cellsPerLine);
            int modLines = childrenCount % cellsPerLine;
            if (modLines > 0)
                numLines++;

            if (panelOrientation == Orientation.Horizontal)
            {
                return new Size(cellsPerLine * cellSize.Width, numLines * cellSize.Height);
            }

            return new Size(numLines * cellSize.Width, cellsPerLine * cellSize.Height);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Calculates the number of child items that can be accommodated in a single line
        /// </summary>
        private void CalculateCellsPerLine()
        {
            double count = (panelOrientation == Orientation.Horizontal) ? panelSize.Width / cellSize.Width :
                                                                          panelSize.Height / cellSize.Height;
            cellsPerLine = (Int32)Math.Floor(count);
            if ((1.0D + cellsPerLine - count) < Double.Epsilon)
                cellsPerLine++;
        }

        #endregion
    }

    /// <summary>
    /// Interactions for the FluidWrapPanel
    /// </summary>
    public class FluidWrapPanel : Panel
    {
        #region Constants

        private const double NORMAL_SCALE = 1.0d;
        private const double DRAG_SCALE_DEFAULT = 1.3d;
        private const double NORMAL_OPACITY = 1.0d;
        private const double DRAG_OPACITY_DEFAULT = 0.6d;
        private const double OPACITY_MIN = 0.1d;
        private const Int32 Z_INDEX_NORMAL = 0;
        private const Int32 Z_INDEX_INTERMEDIATE = 1;
        private const Int32 Z_INDEX_DRAG = 10;
        private static TimeSpan DEFAULT_ANIMATION_TIME_WITHOUT_EASING = TimeSpan.FromMilliseconds(200);
        private static TimeSpan DEFAULT_ANIMATION_TIME_WITH_EASING = TimeSpan.FromMilliseconds(400);
        private static TimeSpan FIRST_TIME_ANIMATION_DURATION = TimeSpan.FromMilliseconds(320);

        #endregion

        #region Fields

        Point dragStartPoint = new Point();
        UIElement dragElement = null;
        UIElement lastDragElement = null;
        List<UIElement> fluidElements = null;
        FluidLayoutManager layoutManager = null;
        bool isInitializeArrangeRequired = false;

        #endregion

        public UIElement DraggedElement
        {
            get { return dragElement; }
        }

        #region Dependency Properties

        #region DragEasing

        /// <summary>
        /// DragEasing Dependency Property
        /// </summary>
        public static readonly DependencyProperty DragEasingProperty =
            DependencyProperty.Register("DragEasing", typeof(EasingFunctionBase), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata((new PropertyChangedCallback(OnDragEasingChanged))));

        /// <summary>
        /// Gets or sets the DragEasing property. This dependency property 
        /// indicates the Easing function to be used when the user stops dragging the child and releases it.
        /// </summary>
        public EasingFunctionBase DragEasing
        {
            get { return (EasingFunctionBase)GetValue(DragEasingProperty); }
            set { SetValue(DragEasingProperty, value); }
        }

        /// <summary>
        /// Handles changes to the DragEasing property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnDragEasingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            EasingFunctionBase oldDragEasing = (EasingFunctionBase)e.OldValue;
            EasingFunctionBase newDragEasing = panel.DragEasing;
            panel.OnDragEasingChanged(oldDragEasing, newDragEasing);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DragEasing property.
        /// </summary>
        /// <param name="oldDragEasing">Old Value</param>
        /// <param name="newDragEasing">New Value</param>
        protected virtual void OnDragEasingChanged(EasingFunctionBase oldDragEasing, EasingFunctionBase newDragEasing)
        {

        }

        #endregion

        #region DragOpacity

        /// <summary>
        /// DragOpacity Dependency Property
        /// </summary>
        public static readonly DependencyProperty DragOpacityProperty =
            DependencyProperty.Register("DragOpacity", typeof(double), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(DRAG_OPACITY_DEFAULT,
                                              new PropertyChangedCallback(OnDragOpacityChanged),
                                              new CoerceValueCallback(CoerceDragOpacity)));

        /// <summary>
        /// Gets or sets the DragOpacity property. This dependency property 
        /// indicates the opacity of the child being dragged.
        /// </summary>
        public double DragOpacity
        {
            get { return (double)GetValue(DragOpacityProperty); }
            set { SetValue(DragOpacityProperty, value); }
        }


        /// <summary>
        /// Coerces the FluidDrag Opacity to an acceptable value
        /// </summary>
        /// <param name="d">Dependency Object</param>
        /// <param name="value">Value</param>
        /// <returns>Coerced Value</returns>
        private static object CoerceDragOpacity(DependencyObject d, object value)
        {
            double opacity = (double)value;

            if (opacity < OPACITY_MIN)
            {
                opacity = OPACITY_MIN;
            }
            else if (opacity > NORMAL_OPACITY)
            {
                opacity = NORMAL_OPACITY;
            }

            return opacity;
        }

        /// <summary>
        /// Handles changes to the DragOpacity property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnDragOpacityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            double oldDragOpacity = (double)e.OldValue;
            double newDragOpacity = panel.DragOpacity;
            panel.OnDragOpacityChanged(oldDragOpacity, newDragOpacity);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DragOpacity property.
        /// </summary>
        /// <param name="oldDragOpacity">Old Value</param>
        /// <param name="newDragOpacity">New Value</param>
        protected virtual void OnDragOpacityChanged(double oldDragOpacity, double newDragOpacity)
        {

        }

        #endregion

        #region DragScale

        /// <summary>
        /// DragScale Dependency Property
        /// </summary>
        public static readonly DependencyProperty DragScaleProperty =
            DependencyProperty.Register("DragScale", typeof(double), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(DRAG_SCALE_DEFAULT, new PropertyChangedCallback(OnDragScaleChanged)));

        /// <summary>
        /// Gets or sets the DragScale property. This dependency property 
        /// indicates the factor by which the child should be scaled when it is dragged.
        /// </summary>
        public double DragScale
        {
            get { return (double)GetValue(DragScaleProperty); }
            set { SetValue(DragScaleProperty, value); }
        }

        /// <summary>
        /// Handles changes to the DragScale property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnDragScaleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            double oldDragScale = (double)e.OldValue;
            double newDragScale = panel.DragScale;
            panel.OnDragScaleChanged(oldDragScale, newDragScale);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the DragScale property.
        /// </summary>
        /// <param name="oldDragScale">Old Value</param>
        /// <param name="newDragScale">New Value</param>
        protected virtual void OnDragScaleChanged(double oldDragScale, double newDragScale)
        {

        }

        #endregion

        #region ElementEasing

        /// <summary>
        /// ElementEasing Dependency Property
        /// </summary>
        public static readonly DependencyProperty ElementEasingProperty =
            DependencyProperty.Register("ElementEasing", typeof(EasingFunctionBase), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata((new PropertyChangedCallback(OnElementEasingChanged))));

        /// <summary>
        /// Gets or sets the ElementEasing property. This dependency property 
        /// indicates the Easing Function to be used when the elements are rearranged.
        /// </summary>
        public EasingFunctionBase ElementEasing
        {
            get { return (EasingFunctionBase)GetValue(ElementEasingProperty); }
            set { SetValue(ElementEasingProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ElementEasing property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnElementEasingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            EasingFunctionBase oldElementEasing = (EasingFunctionBase)e.OldValue;
            EasingFunctionBase newElementEasing = panel.ElementEasing;
            panel.OnElementEasingChanged(oldElementEasing, newElementEasing);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ElementEasing property.
        /// </summary>
        /// <param name="oldElementEasing">Old Value</param>
        /// <param name="newElementEasing">New Value</param>
        /// 
        protected virtual void OnElementEasingChanged(EasingFunctionBase oldElementEasing, EasingFunctionBase newElementEasing)
        {

        }

        #endregion

        #region IsComposing

        /// <summary>
        /// IsComposing Dependency Property
        /// </summary>
        public static readonly DependencyProperty IsComposingProperty =
            DependencyProperty.Register("IsComposing", typeof(bool), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata((new PropertyChangedCallback(OnIsComposingChanged))));

        /// <summary>
        /// Gets or sets the IsComposing property. This dependency property 
        /// indicates if the FluidWrapPanel is in Composing mode.
        /// </summary>
        public bool IsComposing
        {
            get { return (bool)GetValue(IsComposingProperty); }
            set { SetValue(IsComposingProperty, value); }
        }

        /// <summary>
        /// Handles changes to the IsComposing property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnIsComposingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            bool oldIsComposing = (bool)e.OldValue;
            bool newIsComposing = panel.IsComposing;
            panel.OnIsComposingChanged(oldIsComposing, newIsComposing);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the IsComposing property.
        /// </summary>
        /// <param name="oldIsComposing">Old Value</param>
        /// <param name="newIsComposing">New Value</param>
        protected virtual void OnIsComposingChanged(bool oldIsComposing, bool newIsComposing)
        {

        }

        #endregion

        #region ItemHeight

        /// <summary>
        /// ItemHeight Dependency Property
        /// </summary>
        public static readonly DependencyProperty ItemHeightProperty =
            DependencyProperty.Register("ItemHeight", typeof(double), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(0.0,
                    new PropertyChangedCallback(OnItemHeightChanged)));

        /// <summary>
        /// Gets or sets the ItemHeight property. This dependency property 
        /// indicates the height of each item.
        /// </summary>
        public double ItemHeight
        {
            get { return (double)GetValue(ItemHeightProperty); }
            set { SetValue(ItemHeightProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ItemHeight property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnItemHeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel fwPanel = (FluidWrapPanel)d;
            double oldItemHeight = (double)e.OldValue;
            double newItemHeight = fwPanel.ItemHeight;
            fwPanel.OnItemHeightChanged(oldItemHeight, newItemHeight);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ItemHeight property.
        /// </summary>
        /// <param name="oldItemHeight">Old Value</param>
        /// <param name="newItemHeight">New Value</param>
        protected void OnItemHeightChanged(double oldItemHeight, double newItemHeight)
        {

        }

        #endregion

        #region ItemWidth

        /// <summary>
        /// ItemWidth Dependency Property
        /// </summary>
        public static readonly DependencyProperty ItemWidthProperty =
            DependencyProperty.Register("ItemWidth", typeof(double), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(0.0,
                    new PropertyChangedCallback(OnItemWidthChanged)));

        /// <summary>
        /// Gets or sets the ItemWidth property. This dependency property 
        /// indicates the width of each item.
        /// </summary>
        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ItemWidth property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnItemWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel fwPanel = (FluidWrapPanel)d;
            double oldItemWidth = (double)e.OldValue;
            double newItemWidth = fwPanel.ItemWidth;
            fwPanel.OnItemWidthChanged(oldItemWidth, newItemWidth);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ItemWidth property.
        /// </summary>
        /// <param name="oldItemWidth">Old Value</param>
        /// <param name="newItemWidth">New Value</param>
        protected void OnItemWidthChanged(double oldItemWidth, double newItemWidth)
        {

        }

        #endregion

        #region ItemsSource

        /// <summary>
        /// ItemsSource Dependency Property
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OnItemsSourceChanged)));

        /// <summary>
        /// Gets or sets the ItemsSource property. This dependency property 
        /// indicates the bindable collection.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Handles changes to the ItemsSource property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            IEnumerable oldItemsSource = (IEnumerable)e.OldValue;
            IEnumerable newItemsSource = panel.ItemsSource;
            panel.OnItemsSourceChanged(oldItemsSource, newItemsSource);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the ItemsSource property.
        /// </summary>
        /// <param name="oldItemsSource">Old Value</param>
        /// <param name="newItemsSource">New Value</param>
        protected void OnItemsSourceChanged(IEnumerable oldItemsSource, IEnumerable newItemsSource)
        {
            // Clear the previous items in the Children property
            this.ClearItemsSource();

            // Add the new children
            foreach (var child in newItemsSource)
            {
                var element = child as UIElement;
                if (element == null)
                {
                    element = new ContentControl { Content = child };
                }

                Children.Add(element);
            }

            isInitializeArrangeRequired = true;

            InvalidateVisual();
        }

        #endregion

        #region Orientation

        /// <summary>
        /// Orientation Dependency Property
        /// </summary>
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FluidWrapPanel),
                new FrameworkPropertyMetadata(Orientation.Horizontal, new PropertyChangedCallback(OnOrientationChanged)));

        /// <summary>
        /// Gets or sets the Orientation property. This dependency property 
        /// indicates the orientation of arrangement of items in the panel.
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        /// <summary>
        /// Handles changes to the Orientation property.
        /// </summary>
        /// <param name="d">FluidWrapPanel</param>
        /// <param name="e">DependencyProperty changed event arguments</param>
        private static void OnOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FluidWrapPanel panel = (FluidWrapPanel)d;
            Orientation oldOrientation = (Orientation)e.OldValue;
            Orientation newOrientation = panel.Orientation;
            panel.OnOrientationChanged(oldOrientation, newOrientation);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the Orientation property.
        /// </summary>
        /// <param name="oldOrientation">Old Value</param>
        /// <param name="newOrientation">New Value</param>
        protected virtual void OnOrientationChanged(Orientation oldOrientation, Orientation newOrientation)
        {
            InvalidateVisual();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// Override for the Measure Layout Phase
        /// </summary>
        /// <param name="availableSize">Available Size</param>
        /// <returns>Size required by the panel</returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            Size availableItemSize = new Size(Double.PositiveInfinity, Double.PositiveInfinity);
            double rowWidth = 0.0;
            double maxRowHeight = 0.0;
            double colHeight = 0.0;
            double maxColWidth = 0.0;
            double totalColumnWidth = 0.0;
            double totalRowHeight = 0.0;

            // Iterate through all the UIElements in the Children collection
            for (int i = 0; i < InternalChildren.Count; i++)
            {
                UIElement child = InternalChildren[i];
                if (child != null)
                {
                    // Ask the child how much size it needs
                    child.Measure(availableItemSize);
                    // Check if the child is already added to the fluidElements collection
                    if (!fluidElements.Contains(child))
                    {
                        AddChildToFluidElements(child);
                    }

                    if (this.Orientation == Orientation.Horizontal)
                    {
                        // Will the child fit in the current row?
                        if (rowWidth + child.DesiredSize.Width > availableSize.Width)
                        {
                            // Wrap to next row
                            totalRowHeight += maxRowHeight;

                            // Is the current row width greater than the previous row widths
                            if (rowWidth > totalColumnWidth)
                                totalColumnWidth = rowWidth;

                            rowWidth = 0.0;
                            maxRowHeight = 0.0;
                        }

                        rowWidth += child.DesiredSize.Width;
                        if (child.DesiredSize.Height > maxRowHeight)
                            maxRowHeight = child.DesiredSize.Height;
                    }
                    else // Vertical Orientation
                    {
                        // Will the child fit in the current column?
                        if (colHeight + child.DesiredSize.Height > availableSize.Height)
                        {
                            // Wrap to next column
                            totalColumnWidth += maxColWidth;

                            // Is the current column height greater than the previous column heights
                            if (colHeight > totalRowHeight)
                                totalRowHeight = colHeight;

                            colHeight = 0.0;
                            maxColWidth = 0.0;
                        }

                        colHeight += child.DesiredSize.Height;
                        if (child.DesiredSize.Width > maxColWidth)
                            maxColWidth = child.DesiredSize.Width;
                    }
                }
            }

            if (this.Orientation == Orientation.Horizontal)
            {
                // Add the height of the last row
                totalRowHeight += maxRowHeight;
                // If there is only one row, take its width as the total width
                if (totalColumnWidth == 0.0)
                {
                    totalColumnWidth = rowWidth;
                }
            }
            else
            {
                // Add the width of the last column
                totalColumnWidth += maxColWidth;
                // If there is only one column, take its height as the total height
                if (totalRowHeight == 0.0)
                {
                    totalRowHeight = colHeight;
                }
            }

            Size resultSize = new Size(totalColumnWidth, totalRowHeight);

            return resultSize;
        }

        /// <summary>
        /// Override for the Arrange Layout Phase
        /// </summary>
        /// <param name="finalSize">Available size provided by the FluidWrapPanel</param>
        /// <returns>Size taken up by the Panel</returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            if (layoutManager == null)
                layoutManager = new FluidLayoutManager();

            // Initialize the LayoutManager
            layoutManager.Initialize(finalSize.Width, finalSize.Height, ItemWidth, ItemHeight, Orientation);

            bool isEasingRequired = !isInitializeArrangeRequired;

            // If the children are newly added, then set their initial location before the panel loads
            if ((isInitializeArrangeRequired) && (this.Children.Count > 0))
            {
                InitializeArrange();
                isInitializeArrangeRequired = false;
            }

            // Update the Layout
            UpdateFluidLayout(isEasingRequired);

            // Return the size taken up by the Panel's Children
            return layoutManager.GetArrangedSize(fluidElements.Count, finalSize);
        }

        #endregion

        #region Construction / Initialization

        /// <summary>
        /// Ctor
        /// </summary>
        public FluidWrapPanel()
        {
            fluidElements = new List<UIElement>();
            layoutManager = new FluidLayoutManager();
            isInitializeArrangeRequired = true;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Adds the child to the fluidElements collection and initializes its RenderTransform.
        /// </summary>
        /// <param name="child">UIElement</param>
        private void AddChildToFluidElements(UIElement child)
        {
            // Add the child to the fluidElements collection
            fluidElements.Add(child);
            // Initialize its RenderTransform
            child.RenderTransform = layoutManager.CreateTransform(-ItemWidth, -ItemHeight, NORMAL_SCALE, NORMAL_SCALE);
        }

        /// <summary>
        /// Intializes the arrangement of the children
        /// </summary>
        private void InitializeArrange()
        {
            foreach (UIElement child in fluidElements)
            {
                // Get the child's index in the fluidElements
                int index = fluidElements.IndexOf(child);

                // Get the initial location of the child
                Point pos = layoutManager.GetInitialLocationOfChild(index);

                // Initialize the appropriate Render Transform for the child
                child.RenderTransform = layoutManager.CreateTransform(pos.X, pos.Y, NORMAL_SCALE, NORMAL_SCALE);
            }
        }

        /// <summary>
        /// Iterates through all the fluid elements and animate their
        /// movement to their new location.
        /// </summary>
        private void UpdateFluidLayout(bool showEasing = true)
        {
            // Iterate through all the fluid elements and animate their
            // movement to their new location.
            for (int index = 0; index < fluidElements.Count; index++)
            {
                UIElement element = fluidElements[index];
                if (element == null)
                    continue;

                // If an child is currently being dragged, then no need to animate it
                if (dragElement != null && index == fluidElements.IndexOf(dragElement))
                    continue;

                element.Arrange(new Rect(0, 0, element.DesiredSize.Width,
                      element.DesiredSize.Height));

                // Get the cell position of the current index
                Point pos = layoutManager.GetPointFromIndex(index);

                Storyboard transition;
                // Is the child being animated the same as the child which was last dragged?
                if (element == lastDragElement)
                {
                    if (!showEasing)
                    {
                        // Create the Storyboard for the transition
                        transition = layoutManager.CreateTransition(element, pos, FIRST_TIME_ANIMATION_DURATION, null);
                    }
                    else
                    {
                        // Is easing function specified for the animation?
                        TimeSpan duration = (DragEasing != null) ? DEFAULT_ANIMATION_TIME_WITH_EASING : DEFAULT_ANIMATION_TIME_WITHOUT_EASING;
                        // Create the Storyboard for the transition
                        transition = layoutManager.CreateTransition(element, pos, duration, DragEasing);
                    }

                    // When the user releases the drag child, it's Z-Index is set to 1 so that 
                    // during the animation it does not go below other elements.
                    // After the animation has completed set its Z-Index to 0
                    transition.Completed += (s, e) =>
                    {
                        if (lastDragElement != null)
                        {
                            lastDragElement.SetValue(Canvas.ZIndexProperty, 0);
                            lastDragElement = null;
                        }
                    };
                }
                else // It is a non-dragElement
                {
                    if (!showEasing)
                    {
                        // Create the Storyboard for the transition
                        transition = layoutManager.CreateTransition(element, pos, FIRST_TIME_ANIMATION_DURATION, null);
                    }
                    else
                    {
                        // Is easing function specified for the animation?
                        TimeSpan duration = (ElementEasing != null) ? DEFAULT_ANIMATION_TIME_WITH_EASING : DEFAULT_ANIMATION_TIME_WITHOUT_EASING;
                        // Create the Storyboard for the transition
                        transition = layoutManager.CreateTransition(element, pos, duration, ElementEasing);
                    }
                }

                // Start the animation
                transition.Begin();
            }
        }

        /// <summary>
        /// Moves the dragElement to the new Index
        /// </summary>
        /// <param name="newIndex">Index of the new location</param>
        /// <returns>True-if dragElement was moved otherwise False</returns>
        private bool UpdateDragElementIndex(int newIndex)
        {
            // Check if the dragElement is being moved to its current place
            // If yes, then no need to proceed further. (Improves efficiency!)
            int dragCellIndex = fluidElements.IndexOf(dragElement);
            if (dragCellIndex == newIndex)
                return false;

            fluidElements.RemoveAt(dragCellIndex);
            fluidElements.Insert(newIndex, dragElement);

            return true;
        }

        /// <summary>
        /// Removes all the children from the FluidWrapPanel
        /// </summary>
        private void ClearItemsSource()
        {
            fluidElements.Clear();
            Children.Clear();
        }

        #endregion

        #region FluidDrag Event Handlers

        /// <summary>
        /// Handler for the event when the user starts dragging the dragElement.
        /// </summary>
        /// <param name="child">UIElement being dragged</param>
        /// <param name="position">Position in the child where the user clicked</param>
        internal void BeginFluidDrag(UIElement child, Point position)
        {
            if ((child == null) || (!IsComposing))
                return;

            // Call the event handler core on the Dispatcher. (Improves efficiency!)
            Dispatcher.BeginInvoke(new Action(() =>
            {
                child.Opacity = DragOpacity;
                child.SetValue(Canvas.ZIndexProperty, Z_INDEX_DRAG);
                // Capture further mouse events
                child.CaptureMouse();
                dragElement = child;
                lastDragElement = null;

                // Since we are scaling the dragElement by DragScale, the clickPoint also shifts
                dragStartPoint = new Point(position.X * DragScale, position.Y * DragScale);
            }));
        }

        /// <summary>
        /// Handler for the event when the user drags the dragElement.
        /// </summary>
        /// <param name="child">UIElement being dragged</param>
        /// <param name="position">Position where the user clicked w.r.t. the UIElement being dragged</param>
        /// <param name="positionInParent">Position where the user clicked w.r.t. the FluidWrapPanel (the parentFWPanel of the UIElement being dragged</param>
        internal void FluidDrag(UIElement child, Point position, Point positionInParent)
        {
            if ((child == null) || (!IsComposing))
                return;

            // Call the event handler core on the Dispatcher. (Improves efficiency!)
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if ((dragElement != null) && (layoutManager != null))
                {
                    dragElement.RenderTransform = layoutManager.CreateTransform(positionInParent.X - dragStartPoint.X,
                                                                                  positionInParent.Y - dragStartPoint.Y,
                                                                                  DragScale,
                                                                                  DragScale);

                    // Get the index in the fluidElements list corresponding to the current mouse location
                    Point currentPt = positionInParent;
                    int index = layoutManager.GetIndexFromPoint(currentPt);

                    // If no valid cell index is obtained, add the child to the end of the 
                    // fluidElements list.
                    if ((index == -1) || (index >= fluidElements.Count))
                    {
                        index = fluidElements.Count - 1;
                    }

                    // If the dragElement is moved to a new location, then only
                    // call the updation of the layout.
                    if (UpdateDragElementIndex(index))
                    {
                        UpdateFluidLayout();
                    }
                }
            }));
        }

        /// <summary>
        /// Handler for the event when the user stops dragging the dragElement and releases it.
        /// </summary>
        /// <param name="child">UIElement being dragged</param>
        /// <param name="position">Position where the user clicked w.r.t. the UIElement being dragged</param>
        /// <param name="positionInParent">Position where the user clicked w.r.t. the FluidWrapPanel (the parentFWPanel of the UIElement being dragged</param>
        internal void EndFluidDrag(UIElement child, Point position, Point positionInParent)
        {
            if ((child == null) || (!IsComposing))
                return;

            // Call the event handler core on the Dispatcher. (Improves efficiency!)
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if ((dragElement != null) && (layoutManager != null))
                {
                    dragElement.RenderTransform = layoutManager.CreateTransform(positionInParent.X - dragStartPoint.X,
                                                                                positionInParent.Y - dragStartPoint.Y,
                                                                                DragScale,
                                                                                DragScale);

                    child.Opacity = NORMAL_OPACITY;
                    // Z-Index is set to 1 so that during the animation it does not go below other elements.
                    child.SetValue(Canvas.ZIndexProperty, Z_INDEX_INTERMEDIATE);
                    // Release the mouse capture
                    child.ReleaseMouseCapture();

                    // Reference used to set the Z-Index to 0 during the UpdateFluidLayout
                    lastDragElement = dragElement;

                    UpdateItemsSourceIndexes();

                    dragElement = null;
                }

                UpdateFluidLayout();
            }));
        }


        #endregion



        #region IO Custom Code
        /// <summary>
        /// Moves the item in the itemsSource.
        /// </summary>
        /// <param name="itemsSource">The items source.</param>
        /// <param name="oldIndex">The old index.</param>
        /// <param name="newIndex">The new index.</param>
        private void UpdateItemsSourceIndexes()
        {

            var itemsSource = ItemsSource as IList;
            if (itemsSource == null)
            {
                var owner = ItemsControl.GetItemsOwner(this);
                if (owner != null) itemsSource = owner.ItemsSource as IList;
            }
            if (itemsSource == null) return;

            var fluidElementContexts = fluidElements.OfType<FrameworkElement>().Select(e => e.DataContext).ToList();

            isMoving = true;
            Sort(itemsSource, o => fluidElementContexts.IndexOf(o));
            isMoving = false;
        }

        private static void Sort<TKey>(IList source, Func<object, TKey> keySelector)
        {
            var moveMethod = source.GetType().GetMethod("Move", BindingFlags.Instance | BindingFlags.Public, null, new[] { typeof(int), typeof(int) }, null);

            for (int i = source.Count - 1; i >= 0; i--)
            {
                for (int j = 1; j <= i; j++)
                {
                    object o1 = source[j - 1];
                    object o2 = source[j];

                    TKey x = keySelector(o1);
                    TKey y = keySelector(o2);

                    var comparer = Comparer<TKey>.Default;

                    if (comparer.Compare(x, y) > 0)
                    {
                        if (moveMethod != null)
                        {
                            moveMethod.Invoke(source, new object[] { j - 1, j });
                        }
                        else
                        {
                            source.Remove(o1);
                            source.Insert(j, o1);
                        }
                    }
                }
            }
        }

        private bool isMoving;

        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            if (visualRemoved is UIElement && !isMoving)
            {
                fluidElements.Remove((UIElement)visualRemoved);
                UpdateFluidLayout(true);
            }

            base.OnVisualChildrenChanged(visualAdded, visualRemoved);
        }

        #endregion
    }
}
