﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    ///     Returns true if the value is of the specified type.
    /// </summary>
    public class IsTypeToBooleanConverter : IValueConverter
    {
        public Type Type { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Type != null && Type.IsInstanceOfType(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}