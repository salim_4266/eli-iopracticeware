﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class StyleToStyleSelectorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue)
            {
                var style = value as Style;
                if (style != null)
                {
                    return new SingleSelectStyleSelector {Style = style};
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SingleSelectStyleSelector : StyleSelector
    {
        public Style Style { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            return Style ?? base.SelectStyle(item, container);
        }
    }
}