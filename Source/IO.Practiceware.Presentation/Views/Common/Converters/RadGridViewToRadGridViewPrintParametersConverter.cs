﻿using System;
using System.Globalization;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class RadGridViewToPrintGridParametersConverter : IValueConverter
    {
        public RadGridViewToPrintGridParametersConverter()
        {
            IsPrintSelectionEnabled = true;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var grid = value as RadGridView;
            if (grid == null) return null;

            return new PrintGridParameters(grid) { IsFooterIncluded = IsFooterIncluded, IsPrintSelectionEnabled = IsPrintSelectionEnabled };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets or sets a value whether print selection enabled.
        /// If false, prints entire ItemsSource
        /// </summary>
        /// <remarks>Defaults to true.</remarks>
        public bool IsPrintSelectionEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value whether the footer is included in the print
        /// </summary>
        public bool IsFooterIncluded { get; set; }
    }
}
