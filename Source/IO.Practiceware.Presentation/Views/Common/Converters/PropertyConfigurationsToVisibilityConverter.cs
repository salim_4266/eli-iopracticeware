﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts a collection of PropertyConfigurations with a specified property name to a Visibility.
    /// </summary>
    public class PropertyConfigurationToVisibilityConverter : IValueConverter
    {
        public bool IsInverse { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = value as IEnumerable<PropertyConfiguration>;

            var propertyName = parameter as string;

            if (source != null && parameter != null)
            {
                var configuration = source.FirstOrDefault(c => c.PropertyName == propertyName);
                if (configuration != null)
                {
                    var flag = IsInverse ? !configuration.IsVisible : configuration.IsVisible;
                    return flag ? Visibility.Visible : Visibility.Collapsed;
                }
            }
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
