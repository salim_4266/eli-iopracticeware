﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converter to set the scroll bar visibility of the search results in various search screens.
    /// If there are 2 or more search results, the scroll bar visibility is set to 'auto'.
    /// </summary>
    /// <remarks>
    /// This is currently used in the PatientSearch screen and the InsurancePlanSearch screen.
    /// </remarks>
    public class SearchScreenScrollBarVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null || value == DependencyProperty.UnsetValue) { return ScrollBarVisibility.Disabled; }

            var collection = value as IEnumerable<object>;
            return collection != null && collection.Count() >= 2 
                        ? ScrollBarVisibility.Auto 
                        : ScrollBarVisibility.Disabled;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
