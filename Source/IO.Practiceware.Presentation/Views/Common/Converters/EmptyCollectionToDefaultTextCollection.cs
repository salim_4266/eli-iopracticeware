﻿using System.Windows;
using IO.Practiceware.Exceptions;
using IO.Practiceware.Presentation.Views.Common.Converters;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Collections;
using System.Linq;
using System.Windows.Data;
using Soaf.Presentation;

[assembly:Component(typeof(EmptyCollectionToDefaultTextCollection), Initialize=true)]

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Takes an empty generic collection and returns a new collection with one single default item that has the Empty Text in one of its specified properties.
    /// You specify this property through the 'parameter' parameter.
    /// 
    /// Note: This returns a new instance of the collection.  This may have adverse effects when binding depending on your usage.
    /// </summary>
    public class EmptyCollectionToDefaultTextCollection : IValueConverter
    {                
        public EmptyCollectionToDefaultTextCollection()
        {            
        }

        public EmptyCollectionToDefaultTextCollection(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        private static IServiceProvider ServiceProvider { get; set; }     

        /// <summary>
        /// The empty text value that should appear (e.g. 'None', 'No records found', etc...)
        /// </summary>
        public string EmptyText { get; set; }

        /// <summary>
        /// The name of the property to set the empty text to.  You may also pass this value as a ConverterParameter, which will take precedence
        /// </summary>
        public string PropertyNameToSetEmptyText { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {            
            if (value == null)
            {
                return null;
            }

            if (!(value is ICollection))
            {
                return null;
            }

            if (value.CastTo<ICollection>().Count > 0)
            {
                return value;
            }

            var emptyText = EmptyText.IsNullOrEmpty() ? "None" : EmptyText;
            
            var type = value.GetType();
            if (type.IsGenericType)
            {
                // get the type of 
                var genericParameterType = type.GetGenericArguments().First();

                var newList = ServiceProvider != null ? ServiceProvider.GetService(type) ?? Activator.CreateInstance(type) : Activator.CreateInstance(type);
                var objToAdd = ServiceProvider != null ? ServiceProvider.GetService(genericParameterType) ?? Activator.CreateInstance(genericParameterType) : Activator.CreateInstance(type);

                if (genericParameterType == typeof(string))
                {
                    objToAdd = emptyText;
                } 
                else
                {
                    var propertyNameToSetEmptyText = (parameter == null || !(parameter is string) || parameter.ToString() == string.Empty) ? PropertyNameToSetEmptyText : parameter.ToString();

                    if (propertyNameToSetEmptyText.IsNullOrEmpty())
                    {
                        return value;
                    }

                    // this is wrapped in try catch because reflection invocation can be very unpredictable (as code changes) and 
                    // in this case, it's preferable to display nothing then throw an exception
                    try
                    {
                        // set the specified parameter property (which needs to be a string) to the value "None"
                        genericParameterType.InvokeMember(propertyNameToSetEmptyText, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.SetProperty, Type.DefaultBinder, objToAdd, new[] {emptyText});
                    }                    
#if DEBUG
                    catch (Exception e)
                    {
                        if (Environment.UserInteractive)
                        {
                            System.Windows.Application.Current.Dispatcher.Invoke((Action)(() => InteractionManager.Current.DisplayDetailedException(e)));
                        }
#else
                    catch
                    {
#endif
                        return DependencyProperty.UnsetValue;
                    }
                }

                using (new SuppressPropertyChangedScope())
                {                                   
                    type.InvokeMember("Add", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.InvokeMethod, Type.DefaultBinder, newList, new[] { objToAdd });
                }

                return newList;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
