﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts a null, an empty ienumerable, or empty string to a Collapsed visibility.
    /// Specify IsInverse=True to return Visibility.Visible for the listed cases.
    /// </summary>
    public class IsNullOrEmptyToVisibilityConverter : IValueConverter
    {
        public bool IsInverse { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return IsInverse ? Visibility.Visible : Visibility.Collapsed;
            }

            if (value is IEnumerable)
            {
                if (!((IEnumerable)value).Cast<Object>().Any())
                {
                    return IsInverse ? Visibility.Visible : Visibility.Collapsed;
                }
            }

            if (value is string)
            {
                string s = value.ToString();
                if (s.Trim() == string.Empty)
                {
                    return IsInverse ? Visibility.Visible : Visibility.Collapsed;
                }
            }

            return IsInverse ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}