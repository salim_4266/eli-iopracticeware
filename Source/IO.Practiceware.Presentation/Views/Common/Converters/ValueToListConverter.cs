﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class ValueToListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerable = value as IEnumerable<object>;
            if (enumerable != null)
            {
                var objects = enumerable as object[] ?? enumerable.ToArray();
                if(objects.Any())
                {
                    return GetList(objects);
                }
            }

            return value != null ? GetList(new[] {value}) : new List<object>();
        }

        private static IList GetList(object[] values)
        {
            Type elementType = values.First().GetType();
            var list = (IList)Activator.CreateInstance((typeof(List<>).MakeGenericType(elementType)));
            
            foreach (var value in values)
            {
                list.Add(value);
            }
            return list;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
