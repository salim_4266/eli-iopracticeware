﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Reflection;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts an instance method result to a value.     
    /// </summary>
    /// 
    /// <remarks>
    /// The type of the value object will be used as the reference point to invoke the method. 
    /// If you supply targetType, then that will be used as well.  parameter is an optional method parameter that
    /// will be passed to the method invocation.
    /// </remarks>    
    public class InstanceMethodToValueConverter : Freezable, IValueConverter
    {
        public InstanceMethodToValueConverter()
        {
            // Set a default Binding onto the private ElementDataContextProperty.
            // A default binding just binds to the inherited DataContext.  This is how
            // MethodCommand typically gets the object on which to invoke the method.

            BindingOperations.SetBinding(this, ElementDataContextProperty, new Binding());
        }

        // (see it used in the MethodCommand constructor).
        private static readonly DependencyProperty ElementDataContextProperty =
            DependencyProperty.Register("ElementDataContext", typeof(object), typeof(InstanceMethodToValueConverter), null);

        public static readonly DependencyProperty InstanceProperty =
            DependencyProperty.Register("Instance", typeof (object), typeof (InstanceMethodToValueConverter), null);
    

        /// <summary>
        /// Gets or sets the Instance on which to invoke the method
        /// </summary>
        public object Instance
        {
            get { return GetValue(InstanceProperty); }
            set { SetValue(InstanceProperty, value); }
        }


        /// <summary>
        /// Gets or sets the MethodName you would like to call on the value instance.
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var target = Instance ?? GetValue(ElementDataContextProperty);

            if (target == null || MethodName == null)
                return value;

            var typeWithMethod = target.GetType();
            
            var methodInfo = typeWithMethod.GetMethod(MethodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
                        
            if (methodInfo == null)
                return value;

            var methodParameters = new object[methodInfo.GetParameters().Length];
            if (methodParameters.Length > 0)
            {
                methodParameters[0] = value;
            }

            return methodInfo.Invoke(target, methodParameters);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Converts a static method result to a value.         
    /// </summary>
    /// 
    /// <remarks>
    /// The targetType parameter will be used as the reference point to invoke the method 
    /// represented by the parameter object. The value object will be passed as a parameter to the static method
    /// </remarks>    
    public sealed class StaticMethodToValueConverter : IValueConverter
    {
        /// <summary>
        /// Gets or sets the static MethodName you would like to call.
        /// </summary>
        public string StaticMethodName { get; set; }

        /// <summary>
        /// Gets or sets the full name of the type in which the <see cref="StaticMethodName"/> resides.
        /// If this property is not set, the type of the object 'value' parameter in the <see cref="Convert"/> method will be used.
        /// </summary>
        public string TargetTypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var methodName = StaticMethodName;
            var type = string.IsNullOrEmpty(TargetTypeName) ? value.GetType() : Type.GetType("TargetTypeName") ?? Assembly.GetExecutingAssembly().GetType(TargetTypeName);

            if (type == null || methodName == null)
                return value;

            var methodInfo = type.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);
            if (methodInfo == null)
                return value;
            return methodInfo.Invoke(null, new[] { value });
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }
}
