﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{  
    /// <summary>
    /// A converter class that provides the ability to convert a number comparison expression to a visibility value.
    /// </summary>
    public class NumberComparerToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Set the operation
        /// </summary>
        public LogicalOperator Operation { get; set; }

        public int? NumberToCompareAgainst { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converter = new NumberComparerToBooleanConverter();
            converter.NumberToCompareAgainst = NumberToCompareAgainst;
            converter.Operation = Operation;

            if ((bool)converter.Convert(value, targetType, parameter, culture))
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("No reverse conversion");
        }
    }
}
