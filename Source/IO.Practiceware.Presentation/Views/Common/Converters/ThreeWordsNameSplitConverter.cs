﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Breaks provided value into 3 words and returns the one which is specified in the parameter field
    /// </summary>
    public class ThreeWordsNameSplitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringValue = System.Convert.ToString(value);
            var intParameter = System.Convert.ToInt32(parameter);

            // Split by space character and filter out empty ones
            var splitBySpace = stringValue.Split(' ')
                                          .Where(p => p != null && !string.IsNullOrWhiteSpace(p))
                                          .Select(p => p.Trim())
                                          .ToArray();

            // If we got more than 3 words -> concat the rest of words into 3rd word
            if (intParameter > 2 && splitBySpace.Length > 3)
            {
                splitBySpace[2] = string.Join(" ", splitBySpace, 2, splitBySpace.Length - 2);
            }

            // Return the requested word
            return intParameter > splitBySpace.Length 
                       ? string.Empty 
                       : splitBySpace[intParameter - 1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}