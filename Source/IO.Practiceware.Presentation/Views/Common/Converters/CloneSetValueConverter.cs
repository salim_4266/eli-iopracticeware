﻿using System;
using System.Globalization;
using System.Windows.Data;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Returns a cloned copy of value set on the UI when sending it to target
    /// </summary>
    public class CloneSetValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Clone();
        }
    }
}
