﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Data;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Storage;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converter that takes a string value in the form of a Uri (local file path or http Url), 
    /// which represents a path to a Cda xml file, transforms the xml into human readable Html
    /// </summary>
    public class CdaXmlFilePathToHtmlStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           if (value != DependencyProperty.UnsetValue && value != null && value is string)
           {
               var filePath = value.ToString();
               if (FileManager.Instance.FileExists(filePath))
               {
                   return CdaXmlToHtmlHelper.CdaXmlToHtmlString(new Uri(filePath));
               }
           }

           return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converter that takes a string representing Cda xml content, 
    /// and transforms the xml string into human readable Html
    /// </summary>
    public class CdaXmlStringToHtmlStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != DependencyProperty.UnsetValue && value != null && value is string)
            {               
                 return CdaXmlToHtmlHelper.CdaXmlToHtmlString(value.ToString());                
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
