﻿using System.Linq;
using Soaf;
using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class CollectionToFirstOrDefaultConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && value != DependencyProperty.UnsetValue)
            {
                var list = value as IEnumerable;
                if (list == null) throw new ArgumentException("Value is of type {0}".FormatWith(value.GetType().Name));

                return list.Cast<object>().FirstOrDefault();               
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
