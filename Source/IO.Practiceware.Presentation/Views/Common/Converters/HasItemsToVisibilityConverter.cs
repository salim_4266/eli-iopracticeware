﻿using Soaf.Collections;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class HasItemsToVisibilityConverter : IValueConverter
    {
        private const Visibility TrueStateVisibility = Visibility.Visible;
        private Visibility _falseStateVisibility = Visibility.Collapsed;

        public bool IsInverse { get; set; }

        /// <summary>
        /// The returned Visibility when the passed in value is False.
        /// Defaulted to Collapsed.
        /// </summary>
        public Visibility FalseStateVisibility
        {
            get { return _falseStateVisibility; }
            set { _falseStateVisibility = value; }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value == DependencyProperty.UnsetValue)
            {
                return GetVisibilityToReturn(false);
            }

            if (value is ValueType)
            {
                var obj = Activator.CreateInstance(value.GetType());
                if (value.Equals(obj))
                {
                    return GetVisibilityToReturn(false);
                }
            }

            var enumerator = value as IEnumerator;
            if (enumerator != null)
            {
                if (!enumerator.CreateEnumerable().Any())
                {
                    return GetVisibilityToReturn(false);
                }
            }

            var enumerable = value as IEnumerable;
            if (enumerable != null && !enumerable.Cast<object>().Any())
            {
                return GetVisibilityToReturn(false);
            }

            return GetVisibilityToReturn(true);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private Visibility GetVisibilityToReturn(bool result)
        {
            return IsInverse
                ? (result ? FalseStateVisibility : TrueStateVisibility)
                : (result ? TrueStateVisibility : FalseStateVisibility);
        }
    }
}
