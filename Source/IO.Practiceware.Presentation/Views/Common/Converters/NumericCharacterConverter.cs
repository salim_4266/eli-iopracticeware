﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    ///   Converts a string to only numeric characters.
    /// </summary>
    public class NumericCharacterConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;

            if (s != null)
            {
                value = new string(s.Where(char.IsNumber).ToArray());
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        #endregion
    }
}