﻿using System.ComponentModel.DataAnnotations;
using Soaf;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class EnumToDisplayNameConverter : IValueConverter
    {
        public Type EnumType { get; set; }

        public bool UseShortName { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue)
            {
                if (!(value is Enum)) return value.ToString();

                if (UseShortName)
                    return ((Enum)value).GetAttribute<DisplayAttribute>().ShortName;
                    
                return ((Enum)value).GetDisplayName();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
