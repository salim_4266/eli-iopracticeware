﻿using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class AggregateValidationErrorsToStringConverter : IValueConverter
    {
        public string Separator { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var validationErrors = value as IEnumerable<ValidationError>;
            if (validationErrors != null)
            {
                return validationErrors.Select(e => e.ErrorContent).Join(Environment.NewLine);
            }

            var errorEnumerator = value as IEnumerator<String>;
            if (errorEnumerator != null)
            {
                var errors = new List<string>();
                
                if(errorEnumerator.Current.IsNotNullOrEmpty())
                {
                    errors.Add(errorEnumerator.Current);
                }
                while (errorEnumerator.MoveNext())
                {
                    errors.Add(errorEnumerator.Current);
                }
                return errors.Join(Separator ?? Environment.NewLine);
            }

            var errorString = value as String;
            if (errorString != null)
            {
                return errorString;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
