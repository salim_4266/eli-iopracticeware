﻿using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Concatonates a collection of strings into a single string using the provided delimiter and removes empty values.
    /// </summary>
    public class ConcatStringCollectionToStringConverter : IValueConverter
    {
        public string Delimeter { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && value != DependencyProperty.UnsetValue)
            {
                var strings = value as IEnumerable<string>;
                if (strings != null)
                {
                    var enumerable = strings as string[] ?? strings.ToArray();
                    if(enumerable.Any())
                    {
                        return enumerable.Join(Delimeter, true);
                    }
                }
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
