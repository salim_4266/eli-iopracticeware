﻿using System;
using System.Collections;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// A MultiValue converter that takes a dictionary and key and returns the value. 
    /// 
    /// <remarks>
    /// It takes the first object in the values array and assumes it's a dictionary. Then takes the second binding in the array and 
    /// assumes it's the key value for the dictionary.  It uses the key to get data out of your dictionary and passes that retrieved value back.    
    /// </remarks>
    /// 
    /// </summary>
    public class DictionaryItemConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values != null && values.Length >= 2)
            {
                var myDict = values[0] as IDictionary;
                var myKey = values[1];
                if (myDict != null && myKey != null)
                {                    
                    return myDict[myKey];
                }
            }
            return Binding.DoNothing;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
