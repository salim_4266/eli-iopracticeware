﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class CustomFunctionValueConverter : DependencyObject, IValueConverter
    {
        public Func<object, object> ConvertFunc
        {
            get { return (Func<object, object>)GetValue(ConvertFuncProperty); }
            set { SetValue(ConvertFuncProperty, value); }
        }

        public Func<object, object> ConvertBackFunc
        {
            get { return (Func<object, object>)GetValue(ConvertBackFuncProperty); }
            set { SetValue(ConvertBackFuncProperty, value); }
        }

        public static readonly DependencyProperty ConvertBackFuncProperty =
            DependencyProperty.Register("ConvertBackFunc", typeof(Func<object, object>), typeof(CustomFunctionValueConverter), new PropertyMetadata());

        public static readonly DependencyProperty ConvertFuncProperty =
            DependencyProperty.Register("ConvertFunc", typeof(Func<object, object>), typeof(CustomFunctionValueConverter), new PropertyMetadata());

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ConvertFunc == null) throw new NotSupportedException();
            return ConvertFunc(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ConvertBackFunc == null) throw new NotSupportedException();
            return ConvertBackFunc(value);
        }
    }
}
