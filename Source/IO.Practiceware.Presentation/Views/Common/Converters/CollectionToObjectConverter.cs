﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class CollectionToObjectConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerable = value as IEnumerable;
            if (enumerable != null)
                return (enumerable).OfType<object>().FirstOrDefault();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable && !(value is string)) return value;
            var list = (IList) Activator.CreateInstance(targetType);
            list.Add(value);
            return list;
        }

        #endregion
    }
}