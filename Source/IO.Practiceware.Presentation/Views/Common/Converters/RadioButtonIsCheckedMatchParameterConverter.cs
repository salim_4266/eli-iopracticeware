﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Allows binding a value to IsChecked state of RadioButton (two-way).
    /// The RadioButton will be checked when Value is equal to ConverterParameter value.
    /// When RadioButton is checked -> it will be set to ConverterParameter
    /// When RadioButton is unchecked -> it will be set to FalseValue
    /// </summary>
    public class RadioButtonIsCheckedMatchParameterConverter : Freezable, IValueConverter
    {
        public static readonly DependencyProperty FalseValueProperty =
            DependencyProperty.Register("FalseValue", typeof (object), typeof (RadioButtonIsCheckedMatchParameterConverter), new PropertyMetadata(default(object)));

        public object FalseValue
        {
            get { return GetValue(FalseValueProperty); }
            set { SetValue(FalseValueProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Equals(value, parameter) || ReferenceEquals(value, parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? parameter : FalseValue;
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }
}
