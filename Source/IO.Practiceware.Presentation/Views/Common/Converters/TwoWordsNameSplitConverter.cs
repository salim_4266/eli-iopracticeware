﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class TwoWordsNameSplitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringValue = System.Convert.ToString(value);
            var intParameter = System.Convert.ToInt32(parameter);

            // Split by space character and filter out empty ones
            var splitBySpace = stringValue.Split(' ')
                                          .Where(p => p != null && !string.IsNullOrWhiteSpace(p))
                                          .Select(p => p.Trim())
                                          .ToArray();

            // If we got more than 3 words -> concat the rest of words into 3rd word
            if (intParameter > 1 && splitBySpace.Length > 2)
            {
                splitBySpace[1] = string.Join(" ", splitBySpace, 1, splitBySpace.Length - 1);
            }

            // Return the requested word
            return intParameter > splitBySpace.Length
                       ? string.Empty
                       : splitBySpace[intParameter - 1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}