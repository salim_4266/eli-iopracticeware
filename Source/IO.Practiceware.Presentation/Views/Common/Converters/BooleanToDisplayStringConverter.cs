﻿using System;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{

    /// <summary>
    /// Returns display string based on whether value is <c>true</c> or <c>false</c>
    /// </summary>
    public class BooleanToDisplayStringConverter : IValueConverter
    {
        public string TrueDisplayString { get; set; }
        public string FalseDisplayString { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? TrueDisplayString : FalseDisplayString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
