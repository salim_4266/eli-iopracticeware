﻿using System;
using System.Globalization;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts a GridViewRow to its index within the grid.
    /// </summary>
    public class GridViewRowToIndexConverter : IValueConverter
    {
        public int IndexOffset { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var row = value as GridViewRow;
            if (row == null) return null;
            var grid = row.ParentOfType<RadGridView>();
            if (grid == null) return null;

            return grid.Items.IndexOf(row.DataContext) + IndexOffset;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
