﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class ColorToContrastColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush)
            {
                Color contrastColor = GetContrastColor(((SolidColorBrush)value).Color);
                return new SolidColorBrush(contrastColor);
            }

            if (value is Color)
            {
                Color contrastColor = GetContrastColor((Color) value);
                return new SolidColorBrush(contrastColor);
            }
            
            return GetDefaultColorBrush(parameter);
        }

        private static SolidColorBrush GetDefaultColorBrush(object parameter)
        {
            if (parameter == null) { return new SolidColorBrush(); }

            if (parameter is SolidColorBrush) { return parameter as SolidColorBrush; }

            if (parameter is Color) { return new SolidColorBrush((Color)parameter); }

            var colorObj = ColorConverter.ConvertFromString(parameter.ToString());
            if(colorObj != null) { return new SolidColorBrush((Color) colorObj); }

            return new SolidColorBrush();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        private static Color GetContrastColor(Color color)
        {
            byte d = 0;

            // Using Rec. 601 Luma Formula
            // http://en.wikipedia.org/wiki/Luma_%28video%29
            double a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

            if (a < 0.35)
                d = 0; // bright colors - black font
            else
                d = 255; // dark colors - white font
            
            return Color.FromArgb(255, d, d, d);
        }
    }

}
