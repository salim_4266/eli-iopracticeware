﻿

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    class ColorToOpaqueColorConverter : IValueConverter
    {

        public byte Opacity { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush)
            {
                return new SolidColorBrush(Color.FromArgb(Opacity, ((SolidColorBrush)value).Color.R, ((SolidColorBrush)value).Color.G, ((SolidColorBrush)value).Color.B));
            }

            if (value is Color)
            {
                return new SolidColorBrush(Color.FromArgb(Opacity, ((Color)value).R, ((Color)value).G, ((Color)value).B));
            }

            if (value is String)
            {
                var convertFromString = ColorConverter.ConvertFromString(value as String);
                if (convertFromString == null) return new NullReferenceException();
                var color = (Color) convertFromString;
                return new SolidColorBrush(Color.FromArgb(Opacity, color.R, color.G, color.B));
            }
            return new NullReferenceException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
