﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    class CustomDecimalToStringConverter : IValueConverter
    {
        /// <summary>
        /// Converts a decimal to a formatted string based on parity
        /// </summary>

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var toReturn = (Decimal) value;
                if (toReturn < 0)
                    return "(" + (toReturn*(-1)).ToString("N2") + ")";
                return toReturn.ToString("N2");
            }
            catch (InvalidCastException)
            {
                return "CustomDecimalToStringConverter is valid only for converting Decimal type values";
            }
            catch (OverflowException)
            {
                return "The value is out of range";
            }
            catch (FormatException)
            {
                return "The value is not recognized as a Decimal and cannot be converted using CustomDecimalToStringConverter";
            }


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
