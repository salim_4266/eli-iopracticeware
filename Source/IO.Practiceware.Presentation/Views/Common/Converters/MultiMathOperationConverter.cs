﻿using Soaf.Collections;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class MultiMathOperationConverter : IMultiValueConverter
    {
        /// <summary>
        /// Gets or sets the math operation.
        /// Defaults to Add.
        /// </summary>
        public MathOperation MathOperation { get; set; }

        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values != null && values != DependencyProperty.UnsetValue && values.IsNotNullOrEmpty())
            {
                switch (MathOperation)
                {
                    case MathOperation.Add:
                        {
                            return Math.Round(values.Cast<double>().Sum());
                        }
                    case MathOperation.Subtract:
                        {
                            var total = values.Cast<double>().Aggregate((current, v) => current - v);
                            return Math.Round(total);
                        }
                    case MathOperation.Multiply:
                        {
                            return Math.Round(values.Cast<double>().Aggregate((x, y) => x * y));
                        }
                    case MathOperation.Divide:
                        {
                            return Math.Round(values.Cast<double>().Aggregate((x, y) => x / y));
                        }
                }
            }
            return default(double);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
