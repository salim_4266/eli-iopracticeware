﻿using System;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{

    /// <summary>
    /// Returns the specified parameter object if value is true.  null if value is false.
    /// </summary>
    public class BooleanToObjectParameterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is ValueType))
            {
                throw new ArgumentException("object must be a boolean value");
            }

            return (bool)value ? parameter : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
