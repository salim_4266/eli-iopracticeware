﻿using Soaf;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class EnumToValueConverter : IValueConverter
    {
        public Type EnumType { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && value != DependencyProperty.UnsetValue)
            {
                if (value.GetType() != EnumType) throw new Exception("Input value {0} does not match type {1}".FormatWith(value, EnumType));
                return System.Convert.ChangeType(value, typeof (int));
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue)
            {
                var stringValue = value.ToString();
                if (Enum.IsDefined(EnumType, stringValue))
                {
                    return Enum.Parse(EnumType, stringValue);
                }
                return Enum.ToObject(EnumType, value);
            }

            // Return default value
            return Activator.CreateInstance(EnumType);
        }
    }
}
