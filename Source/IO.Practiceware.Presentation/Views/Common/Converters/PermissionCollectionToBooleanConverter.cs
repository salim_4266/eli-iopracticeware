﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using IO.Practiceware.Presentation.ViewModels.Permissions;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class PermissionCollectionToBooleanConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values==null || values.Length < 2) return null;

            var userPermissionsCollection = values[0] as IEnumerable<UserPermissionViewModel>;
            var groupName = values[1] as string;
            if (userPermissionsCollection != null && groupName != null)
            {
                var categorypermissions = userPermissionsCollection.Where(c => c.Category.Equals(groupName));

                var userPermissionViewModels = categorypermissions as UserPermissionViewModel[] ?? categorypermissions.ToArray();

                var selectedPermissions = userPermissionViewModels.Count(c => c.IsSelected);

                if (selectedPermissions.Equals(userPermissionViewModels.Count())) return true;

                if (selectedPermissions > 0) return null;
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
