﻿using IO.Practiceware.Model;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class InvertedPermissionToVisibilityConverter : IValueConverter
    {
        public PermissionId Permission { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Designer.IsContextInDebugAndDesignMode()) return Visibility.Collapsed;

            return Permission.PrincipalContextHasPermission() ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
