﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts a UI element to its index within an ItemsControl.
    /// </summary>
    public class ItemsControlItemToIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var item = value as FrameworkElement;

            if (item == null) return null;

            var itemsControl = item.GetParents().OfType<ItemsControl>().FirstOrDefault();

            if (itemsControl == null) return null;

            return itemsControl.Items.IndexOf(item.DataContext);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
