﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Returns the IsRequired of PropertyConfigurations with a specified property name to a Boolean.
    /// </summary>
    public class PropertyConfigurationsIsRequiredToBooleanConverter : IValueConverter
    {
        public bool IsInverse { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = value as IEnumerable<PropertyConfiguration>;

            var propertyName = parameter as string;

            if (source != null && parameter != null)
            {
                var configuration = source.FirstOrDefault(c => c.PropertyName == propertyName);
                if (configuration != null)
                {
                    return IsInverse ? !configuration.IsRequired : configuration.IsRequired;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}