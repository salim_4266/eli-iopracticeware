﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public enum MathOperation
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }
    
    /// <summary>
    /// Converter to perform math operations on a double and rounds the result.
    /// The operand is always on the right hand side of the operation.
    /// </summary>
    /// <remarks>
    /// Defaults to an Add operation.
    /// If dividing by zero, returns zero.
    /// </remarks>
    [ValueConversion(typeof(object), typeof(double))]
    public class DoubleToMathOperationConverter : IValueConverter
    {
        /// <summary>
        /// Gets or sets the math operation.
        /// Defaults to Add.
        /// </summary>
        public MathOperation MathOperation { get; set; }        

        /// <summary>
        /// Gets or sets the right hand operand of the math operation.  If you set ConverterParameter to a double value in your
        /// binding, then that value will be used instead of Operand
        /// </summary>
        public double Operand { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var operand = parameter != null ? (double)parameter : Operand;            

            if (value != null && value != DependencyProperty.UnsetValue)
            {
                var @double = System.Convert.ToDouble(value);
                
                switch (MathOperation)
                {
                    case MathOperation.Add:
                        {
                            return Math.Round(@double + operand);
                        }
                    case MathOperation.Subtract:
                        {
                            return Math.Round(@double - operand);
                        }
                    case MathOperation.Multiply:
                        {
                            return Math.Round(@double * operand);
                        }
                    case MathOperation.Divide:
                        {
                            return operand.Equals(0) ? 0.0 : Math.Round(@double / operand);
                        }
                }
            }
            return default(double);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
