﻿using System;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Compares 2 object values for equality. Throws argument exception if types of both values do not match
    /// </summary>
    public class ObjectEqualityToBooleanConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {          
            if (value.GetType() != parameter.GetType())
            {
                throw new ArgumentException("Both values must be of the same type");
            }

            return value.Equals(parameter);           
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
