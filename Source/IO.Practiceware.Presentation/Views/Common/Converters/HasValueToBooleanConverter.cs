﻿using System;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class HasValueToBooleanConverter : IValueConverter
    {
        public bool IsInverse { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return IsInverse ? !ConvertInternal(value) : ConvertInternal(value);
        }

        private static bool ConvertInternal(object value)
        {
            if (value == null || value == DependencyProperty.UnsetValue)
            {
                return false;
            }

            if (value is ValueType)
            {
                var obj = Activator.CreateInstance(value.GetType());
                if (value.Equals(obj))
                {
                    return false;
                }
            }

            if (value is string)
            {
                if (value.ToString() == string.Empty) return false;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

}
