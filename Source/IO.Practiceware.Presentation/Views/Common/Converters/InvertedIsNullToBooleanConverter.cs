﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class InvertedIsNullToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
