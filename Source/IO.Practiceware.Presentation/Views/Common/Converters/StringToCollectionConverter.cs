﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Soaf.Collections;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class StringToCollectionConverter : IValueConverter
    {
        /// <summary>
        /// The character or set of characters used to determine each set
        /// </summary>
        public string Separator { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringToConvert = value as string;

            if (stringToConvert == null) return value;

            // split the string
            var list = stringToConvert.Split(new[] {Separator ?? Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();

            return list;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collectionToConvert = value as IList<string>;

            if (collectionToConvert == null) return value;

            // split the string
            var list = collectionToConvert.Join(Separator ?? Environment.NewLine);

            return list;
        }
    }
}
