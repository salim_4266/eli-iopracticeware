﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class FirstNonNullStringSelectorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null)
            {
                string firstNonNullString = values.OfType<string>().FirstOrDefault(v => !string.IsNullOrEmpty(v) );
                if (!string.IsNullOrEmpty(firstNonNullString))
                {
                    return firstNonNullString;
                }
            }
            return string.Empty;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
