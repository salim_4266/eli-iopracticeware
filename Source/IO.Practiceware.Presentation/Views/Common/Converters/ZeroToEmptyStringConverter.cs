﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class ZeroToEmptyStringConverter : IValueConverter
    {
        private bool _hasEnteredZero;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal d;
            if (!_hasEnteredZero && value != null && decimal.TryParse(value.ToString(), out d) && d == 0)
            {
                return string.Empty;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal d = 0;
            if (value != null && decimal.TryParse(value.ToString(), out d) && d == 0)
            {
                _hasEnteredZero = true;
            }
            else
            {
                _hasEnteredZero = false;
            }
            return d;
        }
    }
}
