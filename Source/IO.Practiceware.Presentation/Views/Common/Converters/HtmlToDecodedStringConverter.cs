﻿using Soaf;
using System;
using System.Globalization;
using System.Net;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class HtmlToDecodedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringInput = value.IfNotNull(i => i.ToString(), string.Empty);
            return WebUtility.HtmlDecode(stringInput);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
