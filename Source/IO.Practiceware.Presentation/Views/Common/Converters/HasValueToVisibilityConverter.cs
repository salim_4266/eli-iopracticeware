﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class HasValueToVisibilityConverter : IValueConverter
    {
        private Visibility _trueStateVisibility = Visibility.Visible;
        private Visibility _falseStateVisibility = Visibility.Collapsed;

        public bool IsInverse { get; set; }

        /// <summary>
        /// The returned Visibility when the passed in value is True.
        /// Defaulted to Visible.
        /// </summary>
        public Visibility TrueStateVisibility
        {
            get { return _trueStateVisibility; }
            set { _trueStateVisibility = value; }
        }

        /// <summary>
        /// The returned Visibility when the passed in value is False.
        /// Defaulted to Collapsed.
        /// </summary>
        public Visibility FalseStateVisibility
        {
            get { return _falseStateVisibility; }
            set { _falseStateVisibility = value; }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value == DependencyProperty.UnsetValue)
            {
                return IsInverse ? TrueStateVisibility : FalseStateVisibility;
            }

            if (value is ValueType)
            {
                var obj = Activator.CreateInstance(value.GetType());
                if (value.Equals(obj)) { return IsInverse ? TrueStateVisibility : FalseStateVisibility; }
            }

            if(value is string)
            {
                if (value.ToString() == string.Empty) return IsInverse ? TrueStateVisibility : FalseStateVisibility;
            }

             return IsInverse ? FalseStateVisibility : TrueStateVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
