﻿using Soaf.Reflection;
using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converter that can evaluate a string expression.
    /// The input binding is resolved as 'it' in the expression.
    /// </summary>
    /// <example>
    /// For example, let us say we have the expression "it + 2" and our input binding has a value of 2.
    /// Then this ExpressionConverter will output a value of 4.
    /// </example>
    public class ExpressionConverter : IValueConverter
    {
        /// <summary>
        /// Gets or sets an inner converter to use after expressions are evaluated.
        /// </summary>
        /// <value>
        /// The inner converter.
        /// </value>
        public IValueConverter InnerConverter { get; set; }

        /// <summary>
        /// Gets or sets the string expression to be evaluated.
        /// If the expression contains a variable called 'it', the input binding will resolve to it.
        /// </summary>
        /// <example>
        /// For example, let us say we have the expression "it + 2" and our input binding has a value of 2.
        /// Then this ExpressionConverter will output a value of 4.
        /// </example>
        public string Expression { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to evaluate on null values. If false, will return null and not evaluate expressions when the value to convert is null.
        /// </summary>
        /// <value>
        /// <c>true</c> if [evaluate on null value]; otherwise, <c>false</c>.
        /// </value>
        public bool EvaluateOnNullValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value == null && !EvaluateOnNullValue ? null : Evaluator.Current.Evaluate(Expression, value);

            if (InnerConverter != null) result = InnerConverter.Convert(result, targetType, parameter, culture);

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value;

            if (InnerConverter != null) result = InnerConverter.ConvertBack(result, targetType, parameter, culture);

            return result;
        }
    }
}
