﻿using System;
using System.Globalization;
using System.Windows.Data;
using Telerik.Windows.Controls.ScheduleView;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class TimeSpanToTickLengthConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan)
            {
                var ts = (TimeSpan) value;
                return new FixedTickProvider(new DateTimeInterval((int) ts.TotalMinutes, 0, 0, 0, 0));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}