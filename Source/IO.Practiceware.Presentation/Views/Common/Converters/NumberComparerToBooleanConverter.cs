﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// A converter class that provides the ability to convert a number comparison expression to a boolean value.
    /// </summary>
    public class NumberComparerToBooleanConverter : IValueConverter
    {
        /// <summary>
        /// Set the operation
        /// </summary>
        public LogicalOperator Operation { get; set; }

        /// <summary>
        /// The static number to compare against
        /// </summary>
        public int? NumberToCompareAgainst { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return false;

            var compareNumber = NumberToCompareAgainst ?? 0;

            if (value is String)
            {
                Decimal outValue;
                Decimal.TryParse(value.ToString(), out outValue);
                return Compare(outValue, compareNumber);
            }

            if (value is Enum)
            {
                var underLyingType = Enum.GetUnderlyingType(value.GetType());
                value = System.Convert.ChangeType(value, underLyingType);
            }

            return Compare(value, compareNumber);          
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("No reverse conversion");
        }

        private bool Compare(dynamic d, int numberToCompareAgainst)
        {
            try
            {
                if (Operation == LogicalOperator.EqualTo)
                {
                    return d == numberToCompareAgainst;
                }
                else if (Operation == LogicalOperator.NotEqualTo)
                {
                    return d != numberToCompareAgainst;
                }
                else if (Operation == LogicalOperator.GreaterThan)
                {
                    return d > numberToCompareAgainst;
                }
                else if (Operation == LogicalOperator.GreaterThanOrEqualTo)
                {
                    return d >= numberToCompareAgainst;
                }
                else if (Operation == LogicalOperator.LessThan)
                {
                    return d < numberToCompareAgainst;
                }
                else if (Operation == LogicalOperator.LessThanOrEqualTo)
                {
                    return d <= numberToCompareAgainst;
                }

                return false;
            }
            catch
            {
                throw new Exception("Cannot perform conversion for non-numeric values");
            }
        }
    }   
}
