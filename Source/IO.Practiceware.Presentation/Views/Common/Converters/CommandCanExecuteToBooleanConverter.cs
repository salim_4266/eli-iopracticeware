﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// A converter that returns whether or not the command can execute.
    /// </summary>
    public sealed class CommandCanExecuteToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var command = value as ICommand;
            if (command != null && command.CanExecute(parameter)) return true;

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
