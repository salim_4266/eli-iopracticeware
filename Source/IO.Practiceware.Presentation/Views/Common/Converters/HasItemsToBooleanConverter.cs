﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class HasItemsToBooleanConverter : IMultiValueConverter, IValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length == 0) { return false; }

            foreach (var value in values)
            {
                if (value == null || value == DependencyProperty.UnsetValue) { return false; }
                if (value is ValueType)
                {
                    var obj = Activator.CreateInstance(value.GetType());
                    if (value.Equals(obj)) { return false; }
                }

                var enumerable = value as IEnumerable;
                if (enumerable != null && !enumerable.Cast<object>().Any()) { return false; }
            }

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(new[] { value }, targetType, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
