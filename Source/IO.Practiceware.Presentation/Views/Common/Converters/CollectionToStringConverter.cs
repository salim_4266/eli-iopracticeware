﻿using Soaf;
using Soaf.Collections;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Converts a collection to a string and back, by calling ToString() on each item. The <see cref="ExcludePropertyName"/> property can be used as an optional
    /// name of a boolean property to determine whether or not to include the item in the list.
    /// </summary>
    [ValueConversion(typeof(IEnumerable), typeof(string))]
    public class CollectionToStringConverter : IValueConverter
    {
        /// <summary>
        /// Get or set the separator string using to separate the values of the delimited string list.
        /// </summary>
        public string Separator { get; set; }

        /// <summary>
        /// Get or set the name of the property which will be invoked on each item and used to determine whether or not to exclude that item from the list.
        /// Must be a boolean.
        /// </summary>
        public string ExcludePropertyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return string.Empty;

            var collection = value as IEnumerable;
            if (collection == null) throw new InvalidOperationException("Source must be a collection.");

            
            var syncRoot = collection.As<ICollection>().IfNotNull(c => c.SyncRoot);
            if (syncRoot != null)
            {                
                lock (syncRoot)
                {
                    return GetDelimitedString(collection, ExcludePropertyName);
                }
            }

            return GetDelimitedString(collection, ExcludePropertyName);
        }

        private string GetDelimitedString(IEnumerable collection, string propertyName)
        {
            var listOfObjects = collection.ToInferredElementTypeArray().OfType<object>();
            if (propertyName.IsNotNullOrEmpty())
            {
                var propertyInfoForBoolean = collection.FindElementType().GetProperty(propertyName);
                return listOfObjects.Where(i => (bool)propertyInfoForBoolean.GetGetMethod().Invoke(i, null)).Select(i => i == null ? string.Empty : i.ToString()).Join(Separator ?? Environment.NewLine);
            }

            return listOfObjects.Select(i => i == null ? string.Empty : i.ToString()).Join(Separator ?? Environment.NewLine);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;

            return value.ToString().Split(new[] { Separator ?? Environment.NewLine }, StringSplitOptions.None);
        }
    }
}
