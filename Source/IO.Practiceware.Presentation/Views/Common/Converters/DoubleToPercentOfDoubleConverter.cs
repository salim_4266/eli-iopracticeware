﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    public class DoubleToPercentOfDoubleConverter : IValueConverter
    {
        public int Percent { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double percent = (double)Percent / 100;
            double result = ((double)value) * percent;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
