﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.Common.Converters
{
    /// <summary>
    /// Performs an 'And' on all incoming values.  If all are true then return Visibility.Visible
    /// </summary>
    public class MultiBooleanToVisibilityAndConverter : IMultiValueConverter
    {
        /// <summary>
        /// Allow the true and false state visibility to be set directly via xaml 
        /// 
        /// Default the following values in the below constructor to the corresponding visibility
        /// 
        /// Renamed "NotVisibleValue" & checked that all renames in xaml were updated accordingly for consistency 
        /// with other visibility converters that support this level of flexibility
        /// </summary>
        public Visibility FalseStateVisibility { get; set; }

        public Visibility TrueStateVisibility { get; set; }



        public MultiBooleanToVisibilityAndConverter()
        {
            FalseStateVisibility = Visibility.Collapsed;
            TrueStateVisibility = Visibility.Visible;
        }

        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return values.All(x => x != DependencyProperty.UnsetValue && (bool)x) ? TrueStateVisibility : FalseStateVisibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

   
    public class MultiBooleanToVisibilityOrConverter : IMultiValueConverter
    {
         /// <summary>
        /// This is the value that will be displayed when the converter evaluates to false; default value is collapsed.
        /// </summary>
        public Visibility NotVisibleValue { get; set; }

        public MultiBooleanToVisibilityOrConverter()
        {
            NotVisibleValue = Visibility.Collapsed;
        }

        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {

            return values.Any(x => x != DependencyProperty.UnsetValue && (bool)x) ? Visibility.Visible : NotVisibleValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
   
}
