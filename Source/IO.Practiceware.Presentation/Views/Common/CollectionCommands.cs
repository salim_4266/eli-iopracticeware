﻿using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;

[assembly: Component(typeof (CollectionCommands), Initialize = true)]

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// Common commands for collections.
    /// </summary>
    public class CollectionCommands
    {
        private static IServiceProvider _serviceProvider;

        static CollectionCommands()
        {
            AddNewItem = Command.Create<object>(ExecuteAddNewItem);
            AddItem = Command.Create<AddItemArguments>(ExecuteAddItem);
            RemoveItem = Command.Create<RemoveItemArguments>(ExecuteRemoveItem);
        }

        public CollectionCommands(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Add an item to collection.
        /// </summary>
        /// <value>
        /// The add item.
        /// </value>
        public static ICommand AddItem { get; private set; }

        /// <summary>
        /// Creates an empty item of collection type and adds it.
        /// </summary>
        /// <value>
        /// The add item.
        /// </value>
        public static ICommand AddNewItem { get; private set; }

        /// <summary>
        /// Removes an item from a collection.
        /// </summary>
        /// <value>
        /// The remove item.
        /// </value>
        public static ICommand RemoveItem { get; private set; }

        private static void ExecuteRemoveItem(RemoveItemArguments args)
        {
            if (args != null && args.Collection != null)
            {
                if (args.Index.HasValue)
                {
                    if (args.Collection is IList)
                    {
                        ((IList) args.Collection).RemoveAt(args.Index.Value);
                    }
                    else
                    {
                        ((dynamic) args.Collection).RemoveAt(args.Index.Value);
                    }
                }
                else
                {
                    if (args.Collection is IList)
                    {
                        ((IList) args.Collection).Remove(args.Item);
                    }
                    else
                    {
                        ((dynamic) args.Collection).Remove(args.Item);
                    }
                }
            }
        }

        private static void ExecuteAddItem(AddItemArguments args)
        {
            if (args != null && args.Collection != null)
            {
                if (args.Collection is IList)
                {
                    ((IList)args.Collection).Add(args.Item);
                }
                else
                {
                    ((dynamic)args.Collection).Add(args.Item);
                }
            }
        }

        private static void ExecuteAddNewItem(object obj)
        {
            if (obj != null)
            {
                // Find the type of items stored in collection
                Type itemType = obj.GetType().FindElementType();
                if (itemType != null)
                {
                    // Create new instance
                    object newInstance = _serviceProvider == null
                                             ? Activator.CreateInstance(itemType)
                                             : _serviceProvider.GetService(itemType);

                    // Add it
                    ExecuteAddItem(new AddItemArguments
                        {
                            Collection = obj,
                            Item = newInstance
                        });
                }
            }
        }
    }

    /// <summary>
    /// Information required to add an item.
    /// </summary>
    public class AddItemArguments : Freezable
    {
        public static readonly DependencyProperty CollectionProperty =
            DependencyProperty.Register("Collection", typeof(object), typeof(AddItemArguments),
                                        new PropertyMetadata(default(object)));

        public static readonly DependencyProperty ItemProperty =
            DependencyProperty.Register("Item", typeof(object), typeof(AddItemArguments),
                                        new PropertyMetadata(default(object)));

        public object Collection
        {
            get { return GetValue(CollectionProperty); }
            set { SetValue(CollectionProperty, value); }
        }

        public object Item
        {
            get { return GetValue(ItemProperty); }
            set { SetValue(ItemProperty, value); }
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }

    /// <summary>
    /// Information required to remove an item.
    /// </summary>
    public class RemoveItemArguments : Freezable
    {
        public static readonly DependencyProperty CollectionProperty =
            DependencyProperty.Register("Collection", typeof (object), typeof (RemoveItemArguments),
                                        new PropertyMetadata(default(object)));

        public static readonly DependencyProperty ItemProperty =
            DependencyProperty.Register("Item", typeof (object), typeof (RemoveItemArguments),
                                        new PropertyMetadata(default(object)));

        public static readonly DependencyProperty IndexProperty =
            DependencyProperty.Register("Index", typeof (int?), typeof (RemoveItemArguments),
                                        new PropertyMetadata(default(int?)));

        public object Collection
        {
            get { return GetValue(CollectionProperty); }
            set { SetValue(CollectionProperty, value); }
        }

        public object Item
        {
            get { return GetValue(ItemProperty); }
            set { SetValue(ItemProperty, value); }
        }

        public int? Index
        {
            get { return (int?) GetValue(IndexProperty); }
            set { SetValue(IndexProperty, value); }
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }
}