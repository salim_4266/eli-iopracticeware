﻿using System;
using System.Collections.Generic;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class RowEditEndedArgs
    {
        /// <summary>
        /// The data context bound to the row
        /// </summary>
        public object DataContext { get; set; }

        /// <summary>
        /// The edited item
        /// </summary>
        public object EditedItem { get; set; }

        /// <summary>
        /// The new cell values in the row have been committed
        /// </summary>
        public bool RowCommitted { get; set; }

        /// <summary>
        /// The collection of cell values (PropertyName, Value) before the commit was made
        /// </summary>
        public IDictionary<string, object> OldValues { get; set; }       

        /// <summary>
        /// The action (e.g. commit, cancel) 
        /// </summary>
        public string EditAction { get; set; }

        /// <summary>
        /// The type of operation (e.g. Edit, Insert) performed
        /// </summary>
        public string EditOperationType { get; set; }

        /// <summary>
        /// Set to true to notify the gridView that this event is user-handled
        /// </summary>
        public bool Handled { get; set; }

        /// <summary>
        /// Set to true to try refreshing the UI of the grid view (Sometimes the gridview thinks certain cells are still invalid). 
        /// Note: <see cref="RowIsValid"/> must be true for this property to apply.
        /// </summary>        
        public bool TryRefreshGridView { get; set; }

        /// <summary>
        /// Set this to true to notify the control that the row is valid.
        /// </summary>
        public bool RowIsValid { get; set; }
    }
}
