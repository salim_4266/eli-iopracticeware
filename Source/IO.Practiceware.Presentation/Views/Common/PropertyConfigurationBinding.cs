﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using IO.Practiceware.Presentation.Views.Common.Converters;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// A binding for IHasPropertyConfigurations objects that also bindds Visibility based on the PropertyConfigurations.
    /// </summary>
    public class PropertyConfigurationBinding : BindingDecoratorBase
    {
        public PropertyConfigurationBinding(string path)
        {
            Path = new PropertyPath(path);
        }

        public override object ProvideValue(IServiceProvider provider)
        {
            if (Path != null && !string.IsNullOrEmpty(Path.Path))
            {
                 var service = (IProvideValueTarget)provider.GetService(typeof(IProvideValueTarget));
                if (service == null) return false;

                var target = service.TargetObject as UIElement;

                if (target != null)
                {
                    var propertyConfigurationsPath = Path.Path;

                    var pathParts = propertyConfigurationsPath.Split('.');
                    pathParts[pathParts.Length - 1] = "PropertyConfigurations";

                    propertyConfigurationsPath = string.Join(".", pathParts);

                    var visibilityBinding =
                        new Binding(propertyConfigurationsPath)
                            {
                                Converter = new PropertyConfigurationToVisibilityConverter(),
                                ConverterParameter = Path.Path.Split('.').Last()
                            };

                    // bind visibility
                    BindingOperations.SetBinding(target, UIElement.VisibilityProperty, visibilityBinding);
                }
            }

            return base.ProvideValue(provider);
        }
    }
}
