﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// Launches an external process using the command parameter.
    /// </summary>
    public class StartProcessCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var processToStart = parameter is string 
                ? new StartProcessArgument { Target = (string)parameter}
                : parameter as StartProcessArgument;

            if (processToStart == null) return;

            try
            {
                Process.Start(processToStart.Target, processToStart.Arguments);             
            }
            catch (Exception ex)
            {
                InteractionManager.Current.Alert(processToStart.CustomErrorMessage ?? ex.Message);
            }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged = delegate { };
    }

    public class StartProcessArgument
    {

        /// <summary>       
        /// Name of the process which needs to be initiated.
        /// </summary>
        public string Target { get; set; }

        /// <summary>       
        /// Arguments that needs to be passed with process name.
        /// </summary>
        public string Arguments { get; set; }

        /// <summary>
        /// Error message to show in case target cannot be started
        /// </summary>
        public string CustomErrorMessage { get; set; }
    }
}
