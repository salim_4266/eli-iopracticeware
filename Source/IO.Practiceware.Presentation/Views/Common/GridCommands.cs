﻿using System.Windows.Controls;
using System.Windows.Documents;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;
using IO.Practiceware.Presentation.Views.Common.Controls;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.IO;
using System.Linq;
using System.Printing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using DataGrid = System.Windows.Controls.DataGrid;
using PrintDialog = System.Windows.Controls.PrintDialog;
using StyleManager = Telerik.Windows.Controls.StyleManager;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class PrintGridParameters
    {
        public PrintGridParameters(RadGridView grid)
        {
            IsPrintSelectionEnabled = true;
            Grid = grid;
        }

        /// <summary>
        /// Gets or sets the RadGridView to print.
        /// </summary>
        public RadGridView Grid { get; private set; }

        /// <summary>
        /// Gets or sets a value whether print selection enabled.
        /// If false, prints entire ItemsSource
        /// </summary>
        /// <remarks>Defaults to true.</remarks>
        public bool IsPrintSelectionEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value whether the footer is included in the print
        /// </summary>
        public bool IsFooterIncluded { get; set; }
    }

    public static class GridCommands
    {
        private static readonly ICommand ExportInternal = Command.Create<RadGridView>(ExportGrid);
        private static readonly ICommand PrintInternal = Command.Create<object>(PrintGrid);
        private static readonly ICommand PrintNoFitToWidthInternal = Command.Create<object>(PrintGridNoFitToWidth);
        private static readonly ICommand SelectAllRowsInternal = Command.Create<object>(ExecuteSelectAllRows);
        private static readonly ICommand DeSelectAllRowsInternal = Command.Create<object>(ExecuteDeSelectAllRows);
        private static readonly ICommand PrintSelectedRecordsInternal = Command.Create<object>(PrintGridSelectedRecordsOnly);

        /// <summary>
        ///   Exports the grid to Excel.
        /// </summary>
        public static ICommand Export
        {
            get { return ExportInternal; }
        }

        /// <summary>
        ///   Printing the <see cref="RadGridView"/> and scales it fit the width of the page in landscape mode.
        /// </summary>
        public static ICommand Print
        {
            get { return PrintInternal; }
        }

        /// <summary>
        ///   Printing the <see cref="RadGridView"/> in the original size and in landscape mode.
        /// </summary>
        public static ICommand PrintNoFitToWidth
        {
            get { return PrintNoFitToWidthInternal; }
        }

        /// <summary>
        /// Selects all current items in the <see cref="RadGridView"/> after filtering has been applied.
        /// </summary>
        public static ICommand SelectAllRows
        {
            get { return SelectAllRowsInternal; }
        }

        /// <summary>
        /// De-Selects all current items in the <see cref="RadGridView"/>
        /// </summary>
        public static ICommand DeselectAllRows
        {
            get { return DeSelectAllRowsInternal; }
        }

        /// <summary>
        /// Printing selected rows of grid
        /// </summary>
        public static ICommand PrintSelected
        {
            get { return PrintSelectedRecordsInternal; }
        }

        /// <summary>
        /// Exports grid to a file chosen by user
        /// </summary>
        /// <param name="grid"></param>
        private static void ExportGrid(RadGridView grid)
        {
            RadGridView radGridView = grid;
            var oldPageSize = radGridView.Items.PageSize;
            radGridView.Items.PageSize = radGridView.Items.TotalItemCount;
            const string extension = "xls";
            var dialog = new SaveFileDialog
            {
                DefaultExt = extension,
                Filter =
                    String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() != DialogResult.OK) return;
            using (Stream stream = dialog.OpenFile())
            {
                radGridView.ElementExporting += ElementExporting;
                radGridView.Export(stream,
                    new GridViewExportOptions
                    {
                        Format = ExportFormat.ExcelML,
                        ShowColumnHeaders = true,
                        ShowColumnFooters = true,
                        ShowGroupFooters = false
                    });
                radGridView.Items.PageSize = oldPageSize;

            }
        }

        static void ElementExporting(object sender, GridViewElementExportingEventArgs e)
        {
            if (e.Element == ExportElement.HeaderCell)
            {
                var column = e.Context as GridViewDataColumn;
                if (column != null)
                {
                    string exportedValue = string.Empty;
                    if (column.Header is string)
                    {
                        exportedValue = (string)column.Header;
                    }
                    else
                    {
                        var dependencyObject = column.Header as DependencyObject;
                        if (dependencyObject != null)
                        {
                            var text = dependencyObject.GetChildren<ContentControl>().Select(contentControl => contentControl.Content).FirstOrDefault();
                            if (text != null)
                            {
                                exportedValue = text.ToString();
                            }
                        }
                    }
                    e.Value = exportedValue + " " + ToFormattedString(column.ColumnFilterDescriptor);
                }
            }
        }

        private static string ToFormattedString(IColumnFilterDescriptor columnFilterDescriptor)
        {
            #region helper funcs

            Func<string, string> trimPostfix = input =>
                {
                    // Telerik appends this to FieldDescriptor.ToString() when it is CaseSensitive
                    const string caseSensitivePostfix = "MC";
                    return input.TrimEnd(caseSensitivePostfix);
                };

            Func<IDistinctValuesFilterDescriptor, string> distinctFilterToString = filter => filter.FilterDescriptors.Select(fd => "({0})".FormatWith(trimPostfix(fd.ToString()))).Join("OR ");

            Func<IFieldFilterDescriptor, string> fieldFilterToString = filter =>
                {
                    if (!filter.IsActive) return string.Empty;

                    if (filter.Filter1.IsActive && filter.Filter2.IsActive)
                    {
                        return "({0}) {1} ({2})".FormatWith(trimPostfix(filter.Filter1.ToString()), filter.LogicalOperator, trimPostfix(filter.Filter2.ToString()));
                    }

                    if (filter.Filter1.IsActive)
                    {
                        return "({0})".FormatWith(trimPostfix(filter.Filter1.ToString()));
                    }

                    return "({0})".FormatWith(trimPostfix(filter.Filter2.ToString()));
                };

            #endregion

            if (!columnFilterDescriptor.IsActive) return string.Empty;

            var distinctFilter = columnFilterDescriptor.DistinctFilter;
            var fieldFilter = columnFilterDescriptor.FieldFilter;
            if (distinctFilter.IsActive && fieldFilter.IsActive)
            {
                return "({0}) AND ({1})".FormatWith(distinctFilterToString(distinctFilter), fieldFilterToString(fieldFilter));
            }

            if (distinctFilter.IsActive)
            {
                return distinctFilterToString(distinctFilter);
            }

            return fieldFilterToString(fieldFilter);
        }

        /// <summary>
        /// Opens print preview and allows to print grid contents
        /// </summary>
        /// <param name="parameters">Parameters that can be of type 'RadGridView' or 'PrintGridParameters'</param>
        private static void PrintGrid(object parameters)
        {
            PrintPreview(parameters);
        }

        /// <summary>
        /// Prints the grid using no fit to width.
        /// </summary>
        /// <param name="parameters">Parameters that can be of type 'RadGridView' or 'PrintGridParameters'</param>
        private static void PrintGridNoFitToWidth(object parameters)
        {
            PrintPreview(parameters, PageOrientation.Landscape, ZoomType.TwoWide, false);
        }

        /// <summary>
        /// Opens print preview and allows to print grid contents ( only selected rows)
        /// </summary>
        /// <param name="prarameters">Parameters that can be of type 'RadGridView' or 'PrintGridParameters'</param>
        private static void PrintGridSelectedRecordsOnly(object prarameters)
        {
            PrintPreview(prarameters, PageOrientation.Landscape, ZoomType.Width, true, true);
        }


        /// <summary>
        /// Selects all current items in the <see cref="RadGridView"/> after filtering has been applied.
        /// </summary>
        /// <param name="gridView"></param>
        private static void ExecuteSelectAllRows(object gridView)
        {
            ExecuteDeSelectAllRows(gridView);
            var radGridView = gridView as RadGridView;
            if (radGridView != null)
            {
                radGridView.SelectedItems.AddRange(radGridView.Items);
            }
            var dataGridView = gridView as DataGrid;
            if (dataGridView != null)
            {
                dataGridView.SelectedItems.AddRange(dataGridView.Items.OfType<InvoiceViewModel>());
                SetIsCheckedValue(dataGridView, true);
            }
        }

        /// <summary>
        /// Set's the isChecked value for each DataGridRow in the provided DataGrid to the boolean value provided
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="value"></param>
        private static void SetIsCheckedValue(DataGrid dataGridView, bool value)
        {

            dataGridView.ChildrenOfType<DataGridRow>().ForEach(x =>
            {
                var toggleButtons = x.ChildrenOfType<RadToggleButton>().ToList();
                var toggleButton = toggleButtons.Any() ? toggleButtons.First() : null;
                if (toggleButton != null) toggleButton.IsChecked = value;

            });
        }

        /// <summary>
        /// De-Selects all current items in the <see cref="RadGridView"/> after filtering has been applied.
        /// </summary>
        /// <param name="gridView"></param>
        private static void ExecuteDeSelectAllRows(object gridView)
        {
            var radGridView = gridView as RadGridView;
            if (radGridView != null)
            {
                radGridView.SelectedItems.Clear();
            }
            var dataGridView = gridView as DataGrid;
            if (dataGridView != null)
            {
                dataGridView.SelectedItems.Clear();
                SetIsCheckedValue(dataGridView, false);
            }
        }

        #region RadGridView printing (old way)

        /// <summary> 
        /// Convert GridView to Printer-Friendly version of a GridView 
        /// </summary> 
        /// <param name="parameters">Parameters that can be of type 'RadGridView' or 'PrintGridParameters'</param>
        /// <returns>Printer-Friendly version of source</returns> 
        private static GridViewDataControl ToPrintFriendlyGrid(object parameters)
        {
            RadGridView source = null;
            RadGridView grid = null;
            if (parameters is PrintGridParameters)
            {
                var printGridParameters = parameters as PrintGridParameters;
                source = printGridParameters.Grid;
                grid = new RadGridView
                {
                    ItemsSource = printGridParameters.IsPrintSelectionEnabled && source.SelectedItems.Count > 0 ? source.SelectedItems : source.ItemsSource,
                    HeaderRowStyle = source.HeaderRowStyle,
                    RowIndicatorVisibility = Visibility.Collapsed,
                    ShowGroupPanel = false,
                    CanUserFreezeColumns = true,
                    IsFilteringAllowed = false,
                    AutoExpandGroups = true,
                    AutoGenerateColumns = false,
                    ShowColumnFooters = printGridParameters.IsFooterIncluded
                };
            }
            else if (parameters is RadGridView)
            {
                source = parameters as RadGridView;
                grid = new RadGridView
                    {
                        ItemsSource = source.SelectedItems.Count > 0 ? source.SelectedItems : source.ItemsSource,
                        HeaderRowStyle = source.HeaderRowStyle,
                        RowIndicatorVisibility = Visibility.Collapsed,
                        ShowGroupPanel = false,
                        CanUserFreezeColumns = true,
                        IsFilteringAllowed = false,
                        AutoExpandGroups = true,
                        AutoGenerateColumns = false,
                        ShowColumnFooters = false
                    };
            }

            if (source == null || grid == null) throw new Exception("parameters is not of type 'RadGridView' or 'PrintGridParameters'");

            var displayIndex = 0;
            foreach (GridViewDataColumn column in source.Columns
                .OfType<GridViewDataColumn>()
                .Where(p => p.IsVisible)
                .OrderBy(p => p.DisplayIndex))
            {
                var newColumn = new GridViewDataColumn
                {
                    Width = column.ActualWidth,
                    DisplayIndex = displayIndex++,
                    DataMemberBinding = column.DataMemberBinding,
                    DataFormatString = column.DataFormatString,
                    TextAlignment = column.TextAlignment,
                    Header = column.Header,
                    Footer = column.Footer
                };
                newColumn.AggregateFunctions.AddRange(column.AggregateFunctions);
                grid.Columns.Add(newColumn);
            }

            StyleManager.SetTheme(grid, new Windows7Theme());

            grid.SortDescriptors.AddRange(source.SortDescriptors);
            grid.GroupDescriptors.AddRange(source.GroupDescriptors);
            grid.FilterDescriptors.AddRange(source.FilterDescriptors);

            return grid;
        }

        /// <summary>
        /// Perform a Print Preview on GridView source
        /// </summary>
        /// <param name="parameters">Parameters that can be of type 'RadGridView' or 'PrintGridParameters'</param>
        /// <param name="orientation">Page Orientation (i.e. Portrait vs. Landscape)</param>
        /// <param name="zoom">Zoom Enumeration to specify how pages are stretched in print and preview</param>
        /// <param name="fitPageWidth">if set to <c>true</c> [fits page width].</param>
        /// <param name="printSelectedOnly">if set to <c>true</c>[ prints selected records only].</param>
        private static void PrintPreview(object parameters, PageOrientation orientation = PageOrientation.Landscape,
            ZoomType zoom = ZoomType.Width, bool fitPageWidth = true, bool printSelectedOnly = false)
        {
            RadGridView grid = parameters.As<RadGridView>().IfNull(() => parameters.As<PrintGridParameters>().IfNotNull(i => i.Grid));
            var viewManager = new PrintingDialogViewManager();
            var header = "Print Preview";
            if (grid != null)
            {
                // Prepare a document
                var preparedDocument = ToPrintFriendlyGrid(parameters);
                if (!string.IsNullOrWhiteSpace(grid.ToolTip as string)) header += " of " + grid.ToolTip;
                // Perform print preview
                viewManager.PrintPreviewPreparedElement(preparedDocument, header, orientation, zoom, fitPageWidth);
            }

            var dataGrid = parameters.As<DataGrid>();
            if (dataGrid != null)
            {
                // Prepare a DataGrid Document paginator
                var printDialog = new PrintDialog();
                var pageSize = new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight);
                var paginator = new CustomDataGridDocumentPaginator(dataGrid, "", pageSize, new Thickness(30, 20, 30, 20), printSelectedOnly);
                // Perform print preview
                viewManager.PrintPreview(header, zoom, paginator.Source as FixedDocument);
            }
        }


        #endregion
    }
}
