﻿using Soaf.ComponentModel;
using System;
using System.Windows;
using Xceed.Wpf.AvalonDock.Layout;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class AvalonDockHelper
    {
        public static readonly DependencyProperty IsSelectedProperty =
           DependencyProperty.RegisterAttached("IsSelected", typeof(bool?), typeof(AvalonDockHelper), new FrameworkPropertyMetadata(null, OnIsSelectedChanged));

        public static bool? GetIsSelected(DependencyObject dependencyObject)
        {
            return (bool?)dependencyObject.GetValue(IsSelectedProperty);
        }

        public static void SetIsSelected(DependencyObject dependencyObject, bool? isSelected)
        {
            dependencyObject.SetValue(IsSelectedProperty, isSelected);
        }

        private static void OnIsSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var layoutDocument = d as LayoutDocument;

            if (layoutDocument != null)
            {
                layoutDocument.IsSelectedChanged -= WeakDelegate.MakeWeak<EventHandler>(LayoutDocumentIsSelectedChanged);

                if (e.NewValue is bool)
                {
                    layoutDocument.IsSelected = (bool)e.NewValue;
                    // this is necessary also due to an apparent timing issue or bug in AvanlonDock
                    layoutDocument.IsActive = (bool)e.NewValue;
                }

                layoutDocument.IsSelectedChanged += WeakDelegate.MakeWeak<EventHandler>(LayoutDocumentIsSelectedChanged);
            }
        }

        static void LayoutDocumentIsSelectedChanged(object sender, EventArgs e)
        {
            var layoutDocument = sender as LayoutDocument;

            if (layoutDocument != null)
            {
                SetIsSelected(layoutDocument, layoutDocument.IsSelected);
            }
        }
    }
}
