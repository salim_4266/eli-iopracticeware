﻿
namespace IO.Practiceware.Presentation.Views.Common
{
    public enum ReferralMode
    {
        None,
        Previous,
        Required,
        Existing
    }

    public enum PatientInsuranceAuthorizationMode
    {
        None,
        Existing
    }
}
