﻿using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// Defines attached property which is used by custom buttons defining dot near them
    /// </summary>
    public class ExtraElementProperties
    {
        public static readonly DependencyProperty DotNearButtonColorProperty =
            DependencyProperty.RegisterAttached("DotNearButtonColor", typeof(Brush), typeof(ExtraElementProperties), new PropertyMetadata(default(Brush)));

        public static void SetDotNearButtonColor(UIElement element, Brush value)
        {
            element.SetValue(DotNearButtonColorProperty, value);
        }

        public static Brush GetDotNearButtonColor(UIElement element)
        {
            return (Brush) element.GetValue(DotNearButtonColorProperty);
        }
    }
}
