﻿using System.Windows.Controls;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class SuppressingKeyBinding : KeyBinding
    {
        public override InputGesture Gesture
        {
            get { return Keyboard.FocusedElement is TextBox ? null : base.Gesture; }
            set { base.Gesture = value; }
        }
    }
}