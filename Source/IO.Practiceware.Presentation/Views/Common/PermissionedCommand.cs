﻿using System;
using System.Windows.Input;
using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class PermissionedCommand : ICommand
    {
        public PermissionId PermissionId { get; set; }
        public ICommand Command { get; set; }

        public void Execute(object parameter)
        {
            if (PermissionId.EnsurePermission())
            {
                if (Command != null && Command.CanExecute(parameter)) Command.Execute(parameter);
            }
        }

        public bool CanExecute(object parameter)
        {
            return Command != null && Command.CanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (Command != null) Command.CanExecuteChanged += value;
            }
            remove
            {
                if (Command != null) Command.CanExecuteChanged -= value;
            }
        }

    }

}