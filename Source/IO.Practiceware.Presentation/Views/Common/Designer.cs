﻿using System.ComponentModel;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class Designer
    {
        /// <summary>
        /// special code used to check if the current execution context is the visual studio designer 
        /// </summary>
        /// <returns></returns>
        public static bool IsContextInDesignMode()
        {
            // special code used to check if the current execution context is the visual studio designer
            if ((bool)(DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// special code used to check if the current execution context is the visual studio designer and the project is a debug version
        /// </summary>
        /// <returns></returns>
        public static bool IsContextInDebugAndDesignMode()
        {
#if DEBUG
            return IsContextInDesignMode();
#else
            return false;
#endif
        }
    }
}
