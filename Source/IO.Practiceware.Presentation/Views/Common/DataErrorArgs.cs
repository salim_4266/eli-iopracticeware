﻿using System;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class DataErrorArgs
    {
        /// <summary>
        /// Any exceptions thrown while data binding occurred
        /// </summary>
        public Exception Exception { get; private set; }        

        /// <summary>
        /// Set to true to notify the GridView to keep the row in edit mode.
        /// </summary>
        public bool KeepRowInEditMode { get; set; }

        /// <summary>
        /// Set to true to inform the event route that this event has been handled
        /// </summary>
        public bool Handled { get; set; }

        public DataErrorArgs (Exception exception)
        {
            Exception = exception;
        }
    }
}
