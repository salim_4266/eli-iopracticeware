﻿using Soaf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Windows.Markup;

namespace IO.Practiceware.Presentation.Views.Common
{
    /// <summary>
    /// Markup extension to bind to an Enum Type.
    /// </summary>
    /// <remarks>
    /// If property type is not found, please see the following link.
    /// http://www.hardcodet.net/2008/04/nested-markup-extension-bug
    /// </remarks>
    public class EnumMarkupExtension : MarkupExtension
    {
        private Type _enumType;

        public EnumMarkupExtension() { }

        public EnumMarkupExtension(Type enumType)
        {
            EnumType = enumType;
        }

        public EnumMarkupExtension(Type enumType, bool useDisplayAttribute)
        {
            EnumType = enumType;
            UseDisplayAttribute = useDisplayAttribute;
        }

        public EnumMarkupExtension(bool useDisplayAttribute, Type enumType)
        {
            EnumType = enumType;
            UseDisplayAttribute = useDisplayAttribute;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (EnumType == null) { throw new Exception("EnumType property must be specified"); }

            if(!UseDisplayAttribute)
            {
                Array enumValues = Enum.GetValues(EnumType);
                return enumValues;
            }

            var enumDisplayValues = new List<string>();
            foreach (var enumValue in Enum.GetValues(EnumType))
            {
                FieldInfo field = EnumType.GetField(enumValue.ToString());
                object[] attribs = field.GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attribs.Length == 0) { throw new Exception("UseDisplayAttribute is true and Enum of type {0} has no DisplayAttributes".FormatWith(EnumType)); }

                enumDisplayValues.Add(((DisplayAttribute)attribs[0]).Name);
            }

            return enumDisplayValues.ToArray();
        }

        public Type EnumType
        {
            get { return _enumType; }
            set
            {
                if (!value.IsEnum) { throw new Exception("Type {0} must be an Enum".FormatWith(value.FullName)); }
                _enumType = value;
            }
        }

        public bool UseDisplayAttribute { get; set; }
    }
}
