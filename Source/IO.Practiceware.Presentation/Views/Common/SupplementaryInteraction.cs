﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Interactivity;
using TriggerBase = System.Windows.Interactivity.TriggerBase;

namespace IO.Practiceware.Presentation.Views.Common
{
    public class BehaviorCollection : List<Behavior>
    {
    }

    public class TriggerCollection : List<TriggerBase>
    {
    }

    /// <summary>
    /// Allows setting of behaviors and triggers via a style (since default Interaction.Behaviors/Triggers doesn't allow this).
    /// </summary>
    public static class SupplementaryInteraction
    {
        public static readonly DependencyProperty BehaviorsProperty =
            DependencyProperty.RegisterAttached("Behaviors", typeof (BehaviorCollection), typeof (SupplementaryInteraction), new UIPropertyMetadata(null, OnPropertyBehaviorCollectionChanged));

        public static readonly DependencyProperty TriggersProperty =
            DependencyProperty.RegisterAttached("Triggers", typeof (TriggerCollection), typeof (SupplementaryInteraction), new UIPropertyMetadata(null, OnPropertyTriggerCollectionChanged));

        public static BehaviorCollection GetBehaviors(DependencyObject obj)
        {
            return (BehaviorCollection) obj.GetValue(BehaviorsProperty);
        }

        public static void SetBehaviors(DependencyObject obj, BehaviorCollection value)
        {
            obj.SetValue(BehaviorsProperty, value);
        }

        public static TriggerCollection GetTriggers(DependencyObject obj)
        {
            return (TriggerCollection) obj.GetValue(TriggersProperty);
        }

        public static void SetTriggers(DependencyObject obj, TriggerCollection value)
        {
            obj.SetValue(TriggersProperty, value);
        }

        /// <summary>
        /// Adds or replaces a behavior of a specific type
        /// </summary>
        /// <typeparam name="TBehavior"></typeparam>
        /// <param name="dependencyObject"></param>
        /// <param name="newBehavior"></param>
        public static void AddOrReplaceBehavior<TBehavior>(this DependencyObject dependencyObject, TBehavior newBehavior)
            where TBehavior : Behavior
        {
            // See if we already have this behavior set
            var behaviorCollection = Interaction.GetBehaviors(dependencyObject);
            var existingBehavior = behaviorCollection.FirstOrDefault(p => p is TBehavior);
            if (existingBehavior != null)
            {
                // Force detach and remove it
                behaviorCollection.Remove(existingBehavior);
            }

            if (newBehavior != null)
            {
                // Add new one
                behaviorCollection.Add(newBehavior);
            }
        }

        private static void OnPropertyBehaviorCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            System.Windows.Interactivity.BehaviorCollection behaviors = Interaction.GetBehaviors(d);
            foreach (Behavior behavior in (BehaviorCollection)e.NewValue) behaviors.Add(behavior);
        }

        private static void OnPropertyTriggerCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            System.Windows.Interactivity.TriggerCollection triggers = Interaction.GetTriggers(d);
            foreach (TriggerBase trigger in (TriggerCollection) e.NewValue) triggers.Add(trigger);
        }
    }
}