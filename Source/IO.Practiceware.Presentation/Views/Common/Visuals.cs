﻿
using System.Windows;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.Common
{


    public static class Visuals
    {

        public static T GetFirstUserVisibleChild<T>(Visual referenceVisual) where T : Visual
        {
            Visual child = null;
            //get all children
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(referenceVisual); i++)
            {
                //for each child, check if it's visible
                child = VisualTreeHelper.GetChild(referenceVisual, i) as Visual;
                if (child is T && IsElementHeightIntersecting(child, referenceVisual, 30, 0, (int)(double)child.GetValue(FrameworkElement.ActualHeightProperty)))
                {
                    break;
                }
                if (child != null)
                {
                    //if no immediate children are visible, get the child's child & repeat
                    child = GetFirstUserVisibleChild<T>(child);
                    if (child != null && IsElementHeightIntersecting(child, referenceVisual, 30, 0, (int)(double)child.GetValue(FrameworkElement.ActualHeightProperty)))
                    {
                        break;
                    }
                }
            }
            //return the first visual child found
            return child as T;
        }

        //transform objTop and objHeight onto the container; given those values, determine if the yValue is contained at the yValue
        public static bool IsElementHeightIntersecting(Visual obj, Visual container, double yValue, double objTop, double objHeight)
        {
            Point elementTransformedLeftTop = obj.TransformToAncestor(container).Transform(new Point(0.0, objTop));
            Point elementTransformedLeftBottom = obj.TransformToAncestor(container).Transform(new Point(0.0, objHeight));

            return ((elementTransformedLeftTop.Y <= yValue) && (elementTransformedLeftBottom.Y >= yValue));
        }

    }
}
