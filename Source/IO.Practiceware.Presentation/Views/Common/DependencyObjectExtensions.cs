﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace IO.Practiceware.Presentation.Views.Common
{
    public static class DependencyObjectExtensions
    {
        /// <summary>
        /// Gets all the children of type 'T' for the given element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetChildren<T>(this DependencyObject obj) where T : DependencyObject
        {
            var children = obj.GetLogicalChildren<T>();
            var frameworkElement = obj as FrameworkElement ?? obj.GetLogicalChildren<FrameworkElement>().FirstOrDefault();
            if (frameworkElement != null)
            {
                children = children.Union(frameworkElement.GetVisualChildren<FrameworkElement>().OfType<T>()).Distinct();
            }
            foreach(var child in children)
            {
                yield return child;
            }
        }

        /// <summary>
        /// Gets all the children for given element.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetChildren(this DependencyObject obj)
        {
            foreach(var child in obj.GetChildren<DependencyObject>())
            {
                yield return child;
            }
        }

        /// <summary>
        /// Gets children of type 'T' that resides in the given element's LogicalTree.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetLogicalChildren<T>(this DependencyObject obj) where T : DependencyObject
        {
            if (obj == null) yield break;

	        IEnumerable children = null;
			// special case for raddocking control.  
	        if (obj is RadDocking)
	        {
		        var documentHost = (obj as RadDocking).DocumentHost as DependencyObject;
		        if (documentHost != null) children = LogicalTreeHelper.GetChildren(documentHost);
	        }
	        else
	        {
				children = LogicalTreeHelper.GetChildren(obj);					        
	        }

            foreach (var child in children.OfType<DependencyObject>())
            {
                if (child != null)
                {
                    if (child as T != null)
                    {
                        yield return (T)child;
                    }
                    foreach(var grandChild in child.GetLogicalChildren<T>())
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        /// <summary>
        /// Gets children of type 'T' that resides in the given element's VisualTree.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetVisualChildren<T>(this FrameworkElement element) where T : FrameworkElement
        {
            if (element == null) yield break;

            int childCount = VisualTreeHelper.GetChildrenCount(element);
            for (int i = 0; i < childCount; i++)
            {
                var child = VisualTreeHelper.GetChild(element, i) as FrameworkElement;
                if (child != null)
                {
                    if (child as T != null)
                    {
                        yield return (T)child;
                    }
                    foreach(var grandChild in child.GetVisualChildren<T>())
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        /// <summary>
        /// Finds a Parent of type 'T' where the Parent is in the Visual Tree.
        /// </summary>
        /// <remarks>
        /// Of the DependencyObject is not in the visual tree, we will find the first parent of the DependencyObject that
        /// is in the Visual Tree and search from there.
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T ParentOfType<T>(DependencyObject obj) where T : DependencyObject
        {
            if (obj == null)
                return default(T);

            // If the dependency object is a framework element, then use Telerik's ParentOfType.
            // The Telerik ParentOfType searches the visual tree.
            var frameworkElement = obj as FrameworkElement;
            if (frameworkElement != null)
            {
                return frameworkElement.ParentOfType<T>();
            }

            // Get the first parent that is a FrameworkElement
            var frameworkElementParent = obj.GetLogicalParents().OfType<FrameworkElement>().FirstOrDefault();
            if (frameworkElementParent != null)
            {
                return frameworkElementParent.ParentOfType<T>();
            }

            return null;
        }

        /// <summary>
        /// Gets all the parents of the given dependency object.  This includes both logical and visual parents.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetParents(this DependencyObject obj)
        {
            var parents = obj.GetLogicalParents();
            var frameworkElement = obj as FrameworkElement ?? obj.GetLogicalParents().OfType<FrameworkElement>().FirstOrDefault();

            if (frameworkElement != null)
            {
                parents = parents.Union(frameworkElement.GetVisualParents()).Distinct();
            }
            return parents;
        }

        /// <summary>
        /// Gets the visual parents of the given framework element.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static IEnumerable<FrameworkElement> GetVisualParents(this FrameworkElement element)
        {
            if (element == null)
                throw new ArgumentNullException("element");
            while ((element = GetVisualParent(element)) != null)
                yield return element;
        }

        /// <summary>
        /// Gets the visual parent of the given framework element.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static FrameworkElement GetVisualParent(this FrameworkElement element)
        {
            var parent = VisualTreeHelper.GetParent(element) as FrameworkElement ?? element.Parent as FrameworkElement;
            return parent;
        }

        /// <summary>
        /// Gets the logical parents of the given dependency object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetLogicalParents(this DependencyObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            while ((obj = GetLogicalParent(obj)) != null)
                yield return obj;
        }

        /// <summary>
        /// Gets the logical parent of the given dependency object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static DependencyObject GetLogicalParent(this DependencyObject obj)
        {
            DependencyObject parent = LogicalTreeHelper.GetParent(obj);
            if (parent == null)
            {
                var frameworkElement = obj as FrameworkElement;
                if (frameworkElement != null)
                    parent = frameworkElement.Parent;
            }
            return parent;
        }
    }
}
