﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Referrals;
using IO.Practiceware.Presentation.ViewServices.PatientReferralPopup;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.PatientReferralPopup
{
    /// <summary>
    /// Interaction logic for PatientReferralView.xaml
    /// </summary>
    public partial class PatientReferralPopupView
    {

        public PatientReferralPopupView()
        {
            InitializeComponent();
        }

    }
    public class PatientReferralPopupViewContextReference : DataContextReference
    {
        public new PatientReferralPopupViewContext DataContext
        {
            get { return (PatientReferralPopupViewContext)base.DataContext; }
            set { base.DataContext = value; }
        }
    }

    public class PatientReferralPopupViewContext : IViewContext
    {

        private ObservableCollection<ReferralViewModel> _listOfAttachedReferral;
        private ReferralViewModel _selectedActiveReferral;
        private readonly IPatientReferralPopupViewService _patientReferralPopupViewService;

        private readonly Func<ReferralsViewManager> _createReferralsViewManager;
        private bool _isRemovingAttachedReferral;
        private bool _isAddingAttachedReferral;
        public ICommand ShowAddReferralScreen { get; protected set; }

        public ICommand EditAttachedReferral { get; protected set; }

        public ICommand RemoveAttachedReferral { get; protected set; }

        public ICommand ChangeAttachedReferral { get; protected set; }

        private ICommand InteractionReferralScreen { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ReferralViewModel> ListOfActiveReferral { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ReferralViewModel> ListOfAttachedReferral
        {
            get { return _listOfAttachedReferral; }
            set
            {
                _listOfAttachedReferral = value;
                SavePending = true;
            }
        }

        [DispatcherThread]
        public virtual ReferralViewModel SelectedActiveReferral
        {
            get { return _selectedActiveReferral; }
            set
            {
                _selectedActiveReferral = value;
                SavePending = true;
            }
        }


        public virtual DateTime? AppointmentStartDateTime { get; set; }

        public virtual int? AppointmentId { get; set; }

        public virtual IList<int> InvoiceReceivableIds { get; set; }

        public int? PatientId { get; set; }

        public bool SavePending { get; set; }

        public bool SaveOnNewReferrals { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<ReferralViewModel> ExistingReferralInformation { get; set; }

        [DispatcherThread]
        public virtual bool IsReferralInformationRequired { get; set; }

        [DependsOn("ExistingReferralInformation", "IsReferralInformationRequired", "ListOfAttachedReferral")]
        public virtual ReferralMode ReferralButtonMode
        {
            get
            {
                if (ListOfAttachedReferral != null && ListOfAttachedReferral.Count != 0) return ReferralMode.Existing;
                if (IsReferralInformationRequired) return ReferralMode.Required;
                if (ExistingReferralInformation.Any(r => r.EncountersRemaining != 0)) return ReferralMode.Previous;
                return ReferralMode.None;
            }
        }

        [DependsOn("ListOfAttachedReferral")]
        public virtual string ReferralButtonText
        {
            get
            {
                return ListOfAttachedReferral == null || ListOfAttachedReferral.Count == 0 ? null : String.Format("{0}", ListOfAttachedReferral.First().ReferralCode);
            }
        }
        public PatientReferralPopupViewContext(Func<ReferralsViewManager> createReferralsViewManager, IPatientReferralPopupViewService patientReferralPopupViewService)
        {
            _createReferralsViewManager = createReferralsViewManager;
            _patientReferralPopupViewService = patientReferralPopupViewService;
            Init();
            InteractionReferralScreen = Command.Create<ReferralsReturnArguments>(InteractReferralScreen).Async(() => InteractionContext);
            ShowAddReferralScreen = Command.Create(ExecuteShowAddReferralScreen);
            EditAttachedReferral = Command.Create<ReferralViewModel>(ExecuteEditAttachedReferral);
            RemoveAttachedReferral = Command.Create<ReferralViewModel>(ExecuteRemoveAttachedReferral).Async(() => InteractionContext);
            ChangeAttachedReferral = Command.Create(ExecuteChangeAttachedReferral).Async(() => InteractionContext);
        }

        private void Init()
        {
            ExistingReferralInformation = new ExtendedObservableCollection<ReferralViewModel>();
            _listOfAttachedReferral = new ExtendedObservableCollection<ReferralViewModel>();
            ListOfActiveReferral = new ExtendedObservableCollection<ReferralViewModel>();
            SavePending = true;
        }

        private void ExecuteShowAddReferralScreen()
        {
            if (!AppointmentStartDateTime.HasValue) return;
            var manager = _createReferralsViewManager();
            var loadArguments = new ReferralLoadArguments { PatientId = PatientId, AppointmentDate = AppointmentStartDateTime.Value };
            var returnArguments = manager.ShowReferrals(loadArguments);
            InteractionReferralScreen.Execute(returnArguments);
        }



        private void ExecuteEditAttachedReferral(ReferralViewModel referral)
        {
            if (!AppointmentStartDateTime.HasValue) return;
            var manager = _createReferralsViewManager();
            var loadArguments = new ReferralLoadArguments { ReferralId = referral.Id, PatientId = PatientId, AppointmentDate = AppointmentStartDateTime.Value };
            ReferralsReturnArguments returnArguments = manager.ShowReferrals(loadArguments);
            InteractionReferralScreen.Execute(returnArguments);
        }

        private void ExecuteRemoveAttachedReferral(ReferralViewModel referral)
        {
            _isRemovingAttachedReferral = true;
            if (InvoiceReceivableIds != null && InvoiceReceivableIds.Count > 0)
            {
                _patientReferralPopupViewService.RemoveReferralFromInvoiceReceivables(InvoiceReceivableIds, referral.Id);
            }
            else if (AppointmentId.HasValue)
            {
                _patientReferralPopupViewService.RemoveReferralFromEncounter(AppointmentId.Value, referral.Id);
            }
            ListOfActiveReferral.Add(referral);
            ListOfAttachedReferral.Remove(referral);
            referral.EncountersRemaining = referral.EncountersRemaining + 1;
            _isRemovingAttachedReferral = false;
        }

        private void ExecuteChangeAttachedReferral()
        {
            if (_isRemovingAttachedReferral || _isAddingAttachedReferral || SelectedActiveReferral == null) return;

            if (SelectedActiveReferral.EncountersRemaining <= 0)
            {
                InteractionManager.Current.Alert("You cannot attach this referral. It has reached its max limit of appointments.");
                return;
            }

            _isAddingAttachedReferral = true;
            try
            {
                var existingAttachedReferrals = ListOfAttachedReferral.Select(r => r).ToList();
                existingAttachedReferrals.ForEach(r => r.EncountersRemaining = r.EncountersRemaining + 1);
                ListOfActiveReferral.AddRangeWithSinglePropertyChangedNotification(existingAttachedReferrals);
                ListOfAttachedReferral.RemoveRangeWithSinglePropertyChangedNotification(existingAttachedReferrals);

                ListOfAttachedReferral.Add(SelectedActiveReferral);
                SelectedActiveReferral.EncountersRemaining = SelectedActiveReferral.EncountersRemaining - 1;
                if (InvoiceReceivableIds != null && InvoiceReceivableIds.Count > 0)
                {
                    _patientReferralPopupViewService.AddReferralToInvoiceReceivables(InvoiceReceivableIds, SelectedActiveReferral.Id);
                }
                else if (AppointmentId.HasValue)
                {
                    _patientReferralPopupViewService.AddReferralToEncounter(AppointmentId.Value, SelectedActiveReferral.Id);
                }
                ListOfActiveReferral.Remove(SelectedActiveReferral);
            }
            finally
            {
                _isAddingAttachedReferral = false;
            }


        }

        private void InteractReferralScreen(ReferralsReturnArguments returnArguments)
        {
            var referrals = new List<ReferralViewModel>();
            
            var editedReferrals = _patientReferralPopupViewService.GetReferrals(returnArguments.EditedReferralIds);
            
            if (editedReferrals != null)
            {
                referrals.AddRange(editedReferrals);
            }

            var newReferrals = _patientReferralPopupViewService.GetReferrals(returnArguments.NewReferralIds);
            if (newReferrals != null)
            {
                referrals.AddRange(newReferrals);
            }

            if (referrals.IsNullOrEmpty()) return;

            Update(referrals);

            if (returnArguments.LastSavedId.HasValue)
            {
                var referralToAttach = referrals.SingleOrDefault(x => x.Id == returnArguments.LastSavedId.Value);
                if (referralToAttach != null)
                {
                    var validated = Validate(new List<ReferralViewModel> { referralToAttach }).ToList();
                    if (validated.IsNotNullOrEmpty())
                    {
                        Switch(validated.First());
                    }
                }

            }

            ListOfActiveReferral = Validate(ListOfActiveReferral.ToList()).ToExtendedObservableCollection();
            ListOfAttachedReferral = Validate(ListOfAttachedReferral.ToList()).ToExtendedObservableCollection();

            if (SaveOnNewReferrals)
            {
                if (InvoiceReceivableIds != null && InvoiceReceivableIds.Count > 0 && ListOfAttachedReferral.Count > 0)
                {
                    _patientReferralPopupViewService.AddReferralToInvoiceReceivables(InvoiceReceivableIds, ListOfAttachedReferral.First().Id);
                }
                else if (AppointmentId.HasValue && ListOfAttachedReferral.Count > 0)
                {
                    _patientReferralPopupViewService.AddReferralToEncounter(AppointmentId.Value, ListOfAttachedReferral.First().Id);
                }
            }
        }

        private void Update(IEnumerable<ReferralViewModel> referrals)
        {
            var listOfAttached = ListOfAttachedReferral.ToList();
            var listOfActive = ListOfActiveReferral.ToList();

            foreach (var referral in referrals)
            {
                var remove = listOfAttached.SingleOrDefault(x => x.Id == referral.Id);
                if (remove != null)
                {
                    listOfAttached.Remove(remove);
                    if (!ReferenceEquals(remove, referral))
                    {
                        referral.EncountersRemaining -= 1;
                    }
                    listOfAttached.Add(referral);
                    continue;
                }

                remove = listOfActive.SingleOrDefault(x => x.Id == referral.Id);
                if (remove != null)
                {
                    listOfActive.Remove(remove);
                }

                listOfActive.Add(referral);
            }

            ListOfAttachedReferral = listOfAttached.ToExtendedObservableCollection();
            ListOfActiveReferral = listOfActive.ToExtendedObservableCollection();
        }

        private void Switch(ReferralViewModel referral)
        {
            var listOfAttachedReferral = ListOfAttachedReferral.ToList();
            var listOfActiveReferral = ListOfActiveReferral.ToList();

            if (listOfAttachedReferral.Any(x => x.Id == referral.Id)) return;

            referral.EncountersRemaining = referral.EncountersRemaining - 1;
            if (listOfAttachedReferral.Any())
            {
                var last = listOfAttachedReferral.Last();
                listOfAttachedReferral.Remove(last);

                if (listOfActiveReferral.All(x => x.Id != last.Id))
                {
                    listOfActiveReferral.Add(last);
                }
            }
            listOfAttachedReferral.Add(referral);
            var remove = listOfActiveReferral.SingleOrDefault(x => x.Id == referral.Id);
            if (remove != null)
            {
                listOfActiveReferral.Remove(remove);
            }

            ListOfAttachedReferral = listOfAttachedReferral.ToExtendedObservableCollection();
            ListOfActiveReferral = listOfActiveReferral.ToExtendedObservableCollection();
        }

        private IEnumerable<ReferralViewModel> Validate(IEnumerable<ReferralViewModel> referrals)
        {
            var appointmentDate = AppointmentStartDateTime.HasValue ? AppointmentStartDateTime.Value.Date : (DateTime?)null; 
            // a) for a policy that is active on the date of the appointment, and
            var validPolicy = referrals.Where(r => r.Insurance.StartDateTime <= appointmentDate && (r.Insurance.EndDateTime >= appointmentDate || r.Insurance.EndDateTime == null));
            // b) the appointment must be within the start and expiration dates defined for the referral 
            var validReferrals = validPolicy.Where(r => r.StartDateTime <= appointmentDate && (r.EndDateTime >= appointmentDate || r.EndDateTime == null));

            return validReferrals;
        }
        public void ValidateReferralAndIncludeInExisting(IEnumerable<ReferralViewModel> referrals)
        {
            var validReferrals = Validate(referrals);

            var existingReferrals = ExistingReferralInformation.ToList();
            foreach (var referral in validReferrals)
            {
                if (ListOfAttachedReferral.All(x => x.Id != referral.Id))
                {
                    // remove old reference and add new
                    existingReferrals.RemoveAll(x => x.Id == referral.Id);
                    existingReferrals.Add(referral);
                }
            }
            ListOfActiveReferral = existingReferrals.Where(r => r.EncountersRemaining > 0).ToExtendedObservableCollection();
        }
        public IInteractionContext InteractionContext { get; set; }
    }
}
