﻿using System;
using System.Linq;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Wizard
{
    /// <summary>
    /// Interaction logic for WizardNavigationView.xaml
    /// </summary>
    public partial class WizardNavigationView
    {
        public WizardNavigationView()
        {
            InitializeComponent();            
        }

        public static readonly DependencyProperty BackButtonVisibilityProperty =
            DependencyProperty.Register("BackButtonVisibility", typeof(Visibility), typeof(WizardNavigationView), new PropertyMetadata(OnBackButtonVisibilityChanged));

        public static readonly DependencyProperty UtilityButtonTextProperty =
            DependencyProperty.Register("UtilityButtonText", typeof(string), typeof(WizardNavigationView), new PropertyMetadata("Create;New/Edit", OnUtilityButtonTextChanged));

        public static readonly DependencyProperty UtilityButtonVisibilityProperty =
            DependencyProperty.Register("UtilityButtonVisibility", typeof(Visibility), typeof(WizardNavigationView), new PropertyMetadata(OnUtilityButtonVisibilityChanged));

        public static readonly DependencyProperty UtilityButtonTooltipProperty =
            DependencyProperty.Register("UtilityButtonTooltip", typeof(object), typeof(WizardNavigationView), new PropertyMetadata(OnUtilityButtonTooltipChanged));

        public static readonly DependencyProperty NextButtonVisibilityProperty =
            DependencyProperty.Register("NextButtonVisibility", typeof(Visibility), typeof(WizardNavigationView), new PropertyMetadata(OnNextButtonVisibilityChanged));

        public static readonly DependencyProperty WizardVisibilityProperty =
            DependencyProperty.Register("WizardVisibility", typeof(Visibility), typeof(WizardNavigationView), new PropertyMetadata(OnWizardVisibilityChanged));


        private static void IfDependencyObjectIsNotNull(DependencyObject d, DependencyPropertyChangedEventArgs e, Action<WizardNavigationView> action)
        {
            if (e.NewValue == DependencyProperty.UnsetValue) return;

            var wizardView = d as WizardNavigationView;
            if (wizardView != null)
            {
                action(wizardView);
            }
        }

        private static void OnWizardVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, w =>
                                                  {
                                                      w.Visibility = (Visibility) e.NewValue;
                                                  });
        }

        private static void OnUtilityButtonTooltipChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, w => w.UtilityButton.ToolTip = e.NewValue);
        }

        private static void OnUtilityButtonTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, wizardView =>
                                                  {
                                                      if (e.NewValue is string && !string.IsNullOrEmpty(e.NewValue.ToString()))
                                                      {
                                                          var text = e.NewValue.ToString().Split(new[] { ';' });
                                                          wizardView.UtilityText1.Text = text[0];
                                                          wizardView.UtilityText2.Text = text.Count() > 1 ? text[1] : null;
                                                      }
                                                      else
                                                      {
                                                          wizardView.UtilityText1.Text = null;
                                                          wizardView.UtilityText2.Text = null;
                                                      }
                                                  });
        }

        private static void OnBackButtonVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, wizardView => wizardView.BackButton.Visibility = (Visibility)e.NewValue);
        }

        private static void OnUtilityButtonVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, wizardView => wizardView.UtilityButton.Visibility = (Visibility)e.NewValue);
        }

        private static void OnNextButtonVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IfDependencyObjectIsNotNull(d, e, wizardView => wizardView.NextButton.Visibility = (Visibility)e.NewValue);
        }

        public Visibility BackButtonVisibility
        {
            get { return (Visibility)GetValue(BackButtonVisibilityProperty); }
            set { SetValue(BackButtonVisibilityProperty, value); }
        }

        public Visibility UtilityButtonVisibility
        {
            get { return (Visibility)GetValue(UtilityButtonVisibilityProperty); }
            set { SetValue(UtilityButtonVisibilityProperty, value); }
        }

        public Visibility NextButtonVisibility
        {
            get { return (Visibility)GetValue(NextButtonVisibilityProperty); }
            set { SetValue(NextButtonVisibilityProperty, value); }
        }

        public Visibility WizardVisibility
        {
            get { return (Visibility)GetValue(WizardVisibilityProperty); }
            set { SetValue(WizardVisibilityProperty, value); }
        }

    }
}
