﻿using System.Windows.Input;
using Soaf;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Wizard
{
    /// <summary>
    /// Represents an implementation of a WizardDataContext that can be bound to the <see cref="WizardNavigationView"/> control 
    /// which provides wizard functions such as Back, Next, Cancel.    
    /// </summary>
    public interface IWizardDataContext
    {
        ICommand Next { get; set; }

        ICommand Back { get; set; }

        ICommand CancelWizard { get; set; }

        ICommand WizardComplete { get; set; }

        /// <summary>
        /// An optional button on the wizard view (the button between back and next).
        /// </summary>
        /// <value>The utility.</value>
        ICommand Utility { get; set; }

        /// <summary>
        /// Flag that determines whether or not the current view is in wizard mode
        /// </summary>
        bool IsInWizardMode { get; }

        /// <summary>
        /// Gets the NavigableInteractionManager
        /// </summary>
        INavigableInteractionManager NavigableInteractionManager { get; }

        /// <summary>
        /// Gets the ParentViewContext
        /// </summary>
        IViewContext ParentViewContext { get; }

        /// <summary>
        /// Gets the InteractionContext
        /// </summary>
        IInteractionContext InteractionContext { get; }
    }

    /// <summary>
    /// Represents a ViewContext that has an <see cref="IWizardDataContext"/> property
    /// which provides commands for the <see cref="WizardNavigationView"/> control 
    /// </summary>
    public interface IHasWizardDataContext
    {
        IWizardDataContext Wizard { get; set; }        
    }

    /// <summary>
    /// A basic <see cref="IWizardDataContext"/> implementation.  Provides many common properties, commands, and methods 
    /// that are re-used across wizards
    /// </summary>
    /// <typeparam name="TNavigationViewModel">The type of the NavigationViewModel.</typeparam>
    /// <typeparam name="TParentViewContext">The type of the ParentViewContext</typeparam>
    public class WizardDataContext<TNavigationViewModel, TParentViewContext> : IWizardDataContext where TParentViewContext : IViewContext, IHasWizardDataContext
    {
        public INavigableInteractionManager NavigableInteractionManager { get; protected set; }

        /// <summary>
        /// Gets or sets the navigation view model that is shared between all screens in the wizard.
        /// </summary>
        /// <value>The navigation view model.</value>
        public virtual TNavigationViewModel NavigationViewModel { get; protected set; }

        /// <summary>
        /// Gets or sets the parent ViewContext for this wizard
        /// </summary>
        IViewContext IWizardDataContext.ParentViewContext
        {
            get { return ParentViewContext; }
        }

        public TParentViewContext ParentViewContext { get; protected set; }

        public virtual IInteractionContext InteractionContext { get; protected set; }

        public WizardDataContext()
        {
            Next = Command.Create(ExecuteNext, CanExecuteNext);
            Back = Command.Create(ExecuteBack, CanExecuteBack);
            CancelWizard = Command.Create(ExecuteCancelWizard, CanExecuteCancelWizard);
            WizardComplete = Command.Create(ExecuteWizardComplete, CanExecuteWizardComplete);
            Utility = Command.Create(ExecuteUtilityCommand, CanExecuteUtilityCommand);

            IsInWizardMode = true;
        }     

        public WizardDataContext(INavigableInteractionManager navigableInteractionManager, IInteractionContext interactionContext)
            : this()
        {
            NavigableInteractionManager = navigableInteractionManager;
            InteractionContext = interactionContext;
        }

        public WizardDataContext(INavigableInteractionManager navigableInteractionManager, IInteractionContext interactionContext,
                                TNavigationViewModel viewModel)
            : this(navigableInteractionManager, interactionContext)
        {
            NavigationViewModel = viewModel;
        }

        public WizardDataContext(INavigableInteractionManager navigableInteractionManager, IInteractionContext interactionContext,
                                    TParentViewContext parentViewContext)
            : this(navigableInteractionManager, interactionContext)
        {
            ParentViewContext = parentViewContext;
            ParentViewContext.InteractionContext = InteractionContext;            
        }

        public WizardDataContext(INavigableInteractionManager navigableInteractionManager, IInteractionContext interactionContext,
                                 TParentViewContext parentViewContext, TNavigationViewModel viewModel)
            : this(navigableInteractionManager, interactionContext, parentViewContext)
        {            
            NavigationViewModel = viewModel;
        }        

        #region IWizardDataContext Members

        public ICommand Next { get; set; }
        public ICommand Back { get; set; }
        public ICommand CancelWizard { get; set; }
        public ICommand WizardComplete { get; set; }
        public ICommand Utility { get; set; }
        public bool IsInWizardMode { get; set; }

        #endregion

        #region Virtual Class Members

        protected virtual bool CanExecuteNext()
        {
            return true;
        }
        protected virtual void ExecuteNext()
        {
        }
        protected virtual bool CanExecuteBack()
        {
            return true;
        }
        protected virtual void ExecuteBack()
        {
            NavigableInteractionManager.ShowPrevious();
        }
        protected virtual bool CanExecuteCancelWizard()
        {
            return true;
        }
        protected virtual void ExecuteCancelWizard()
        {
            if (InteractionContext != null)
            {
                InteractionContext.Complete(false);
            }
        }
        protected virtual bool CanExecuteWizardComplete()
        {
            return true;
        }
        protected virtual void ExecuteWizardComplete()
        {
            if (InteractionContext != null)
            {
                InteractionContext.Complete(true);
            }
        }
        protected virtual bool CanExecuteUtilityCommand()
        {
            return true;
        }
        protected virtual void ExecuteUtilityCommand()
        {
        }

        #endregion
    }

    public static class WizardDataContextExtensions
    {


        public static T ParentViewContext<T>(this IWizardDataContext wizardDataContext)
        {
            return wizardDataContext.ParentViewContext.CastTo<T>();
        }
    }
}
