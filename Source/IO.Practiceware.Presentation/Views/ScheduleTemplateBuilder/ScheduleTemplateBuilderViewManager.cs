﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.Views.Common;
using Soaf;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateBuilderViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public ScheduleTemplateBuilderViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowScheduleTemplateBuilder()
        {
            ShowScheduleTemplateBuilder(null);
        }

        public void ShowScheduleTemplateBuilder(ScheduleTemplateBuilderLoadArguments loadArgs)
        {
            if (loadArgs == null) loadArgs = new ScheduleTemplateBuilderLoadArguments();            

            if (loadArgs.SelectedMode == null) // if there is no selected mode set, determine which one to set based on the users permission
            {
                if(PermissionId.CreateScheduleTemplates.PrincipalContextHasPermission())
                {
                    loadArgs.SelectedMode = ScheduleTemplateBuilderMode.AddEditTemplate;
                }
                else if(PermissionId.ApplyTemplatesToSchedules.PrincipalContextHasPermission())
                {
                    loadArgs.SelectedMode = ScheduleTemplateBuilderMode.ApplyTemplate;
                }
                else
                {
                    _interactionManager.Alert("You do not have permission to access this area of the software.  Please contact your local administrator.");
                    return;
                }
            }
            else
            {
                // now that the mode is set, check the users permission
                if (loadArgs.SelectedMode == ScheduleTemplateBuilderMode.AddEditTemplate && !PermissionId.CreateScheduleTemplates.EnsurePermission())
                {
                    return;
                }

                if (loadArgs.SelectedMode == ScheduleTemplateBuilderMode.ApplyTemplate && !PermissionId.ApplyTemplatesToSchedules.EnsurePermission())
                {
                    return;
                }
            }

            var view = new ScheduleTemplateBuilderView();
            view.DataContext.EnsureType<ScheduleTemplateBuilderViewContext>().LoadArguments = loadArgs;
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }
    }

    public class ScheduleTemplateBuilderLoadArguments
    {
        public ScheduleTemplateBuilderMode? SelectedMode { get; set; }
        public int? SelectedResourceId { get; set; }
        public DateTime? SelectedDate { get; set; }
    }
}