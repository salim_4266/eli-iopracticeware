﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.ScheduleTemplateBuilder;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    /// <summary>
    /// Interaction logic for ScheduleTemplateBuilderInsertCategoryView.xaml
    /// </summary>
    public partial class ScheduleTemplateBuilderInsertCategoryView
    {
        public ScheduleTemplateBuilderInsertCategoryView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// The root view model (the view context) for the ScheduleTemplateBuilderInsertCategoryView
    /// </summary>
    public class ScheduleTemplateBuilderInsertCategoryViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IScheduleTemplateBuilderViewService _scheduleTemplateBuilderViewService;
        private readonly ColoredViewModel _dummyEllipsis = new ColoredViewModel { Id = 0, Name = "...", Color = Colors.Transparent };

        public ScheduleTemplateBuilderInsertCategoryViewContext(IScheduleTemplateBuilderViewService scheduleTemplateBuilderViewService)
        {
            _scheduleTemplateBuilderViewService = scheduleTemplateBuilderViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            IncrementScheduleBlockCategories = Command.Create(ExecuteIncrementScheduleBlockCategories);
            Close = Command.Create(ExecuteClose);
            Save = Command.Create(ExecuteSave);
            Closing = Command.Create<Action>(ExecuteClosing);
        }

        private void ExecuteClosing(Action cancelClose)
        {
            SelectedCategory = null;

            if (InteractionContext.DialogResult.HasValue)
            {
                var selectedBlocks = ScheduleBlocks.Where(b => b.IsSelected).ToList();
                if (!InteractionContext.DialogResult.Value)
                {
                    selectedBlocks.ForEach(b =>
                        {
                            b.CastTo<IEditableObject>().CancelEdit();
                            b.IsSelected = true;
                        });
                }
                else
                {
                    selectedBlocks.ToList().ForEach(b =>
                        {
                            b.IsSelected = false;
                            b.CastTo<IEditableObject>().EndEdit();
                        });
                }
            }
        }

        private void ExecuteSave()
        {
            ApplyLocationChange();
            InteractionContext.Complete(true);
        }

        private void ExecuteLoad()
        {
            var info = _scheduleTemplateBuilderViewService.LoadInformation(ScheduleTemplateBuilderLoadTypes.Categories | ScheduleTemplateBuilderLoadTypes.Locations);
            ListOfCategory = info.Categories;
            ListOfCategory.Insert(0, new UnavailableCategoryViewModel());
            ListOfLocation = info.Locations;

            var locations = ScheduleBlocks.Where(s => s.IsSelected).Select(s => s.Location).Distinct().ToList();

            if (!locations.Any()) locations = ScheduleBlocks.Select(s => s.Location).Distinct().ToList();

            if (locations.Count == 1)
            {
                SelectedLocation = ListOfLocation.First(l => l.Id == locations.First().Id);
            }
            else if (locations.Count > 1)
            {
                if (!ListOfLocation.Contains(_dummyEllipsis)) ListOfLocation.Add(_dummyEllipsis);
                SelectedLocation = _dummyEllipsis;
            }
            ScheduleBlocks.Where(b => b.IsSelected).ToList().ForEach(b => b.CastTo<IEditableObject>().BeginEdit());
        }

        private void ExecuteIncrementScheduleBlockCategories()
        {
            var scheduleBlocks = ScheduleBlocks.Where(scheduleBlock => scheduleBlock.IsSelected);
            foreach (var scheduleBlock in scheduleBlocks)
            {
                if (SelectedCategory != null)
                {
                    if (SelectedCategory is UnavailableCategoryViewModel || scheduleBlock.Categories.OfType<UnavailableCategoryViewModel>().Any())
                    {
                        scheduleBlock.Categories.Clear();
                    }
                    scheduleBlock.Categories.Add(SelectedCategory);
                }
            }
        }

        private void ApplyLocationChange()
        {
            if (SelectedLocation != null && SelectedLocation.Id != _dummyEllipsis.Id && HasLocationChange)
            {
                var selectedBlocks = ScheduleBlocks.Where(scheduleBlock => scheduleBlock.IsSelected).ToList();
                foreach (var scheduleBlock in selectedBlocks.Where(b => b.Location != SelectedLocation))
                {
                    scheduleBlock.Location = SelectedLocation;
                    if(ListOfLocation.Contains(_dummyEllipsis)) ListOfLocation.Remove(_dummyEllipsis);
                }
            }
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(false);
        }

        /// <summary>
        /// Gets or sets whether this can save the selected location
        /// </summary>
        public virtual bool HasLocationChange { get; set; }

        /// <summary>
        /// Series of items composting a schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ScheduleBlockViewModel> ScheduleBlocks { get; set; }

        /// <summary>
        /// List of categories that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<CategoryViewModel> ListOfCategory { get; set; }

        /// <summary>
        /// Category selected by user to add to schedule template
        /// </summary>
        [DispatcherThread]
        public virtual CategoryViewModel SelectedCategory { get; set; }

        /// <summary>
        /// List of locations that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ColoredViewModel> ListOfLocation { get; set; }

        /// <summary>
        /// Location selected by user to add to schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel SelectedLocation { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to add a single given category for the selected scheduleBlocks
        /// </summary>
        public ICommand IncrementScheduleBlockCategories { get; private set; }

        /// <summary>
        /// Command to close UI
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Gets a command that is executed when the associated view is closing.
        /// It is able to cancel the closing event through the passed in bool Action.
        /// </summary>
        public ICommand Closing { get; protected set; }

        /// <summary>
        /// Command to save
        /// </summary>
        public ICommand Save { get; protected set; }
    }
}