﻿using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
        /// <summary>
    /// Converter that determines whether the overwrite save button should be visible.
    /// It is visible when the selected template an existing template.
    /// </summary>
    class OverwriteSaveButtonVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selectedTemplate = value as ScheduleTemplateViewModel;
            if (selectedTemplate != null && selectedTemplate.Id != 0)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
