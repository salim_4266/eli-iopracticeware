﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateModeToTabIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var mode = (ScheduleTemplateBuilderMode)value;
            switch (mode)
            {
                case ScheduleTemplateBuilderMode.AddEditTemplate:
                    return 0;
                case ScheduleTemplateBuilderMode.ApplyTemplate:
                    return 1;
                default:
                    return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = (int)value;
            switch (index)
            {
                case 0:
                    return ScheduleTemplateBuilderMode.AddEditTemplate;
                case 1:
                    return ScheduleTemplateBuilderMode.ApplyTemplate;
                default:
                    return ScheduleTemplateBuilderMode.AddEditTemplate;
            }
        }
    }
}
