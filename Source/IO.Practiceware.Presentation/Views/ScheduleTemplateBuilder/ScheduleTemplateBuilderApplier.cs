﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.ScheduleTemplateBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using Enumerable = System.Linq.Enumerable;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    /// <summary>
    /// Helper view model (the view context) for the ScheduleTemplateBuilderView
    /// </summary>
    [SupportsDataErrorInfo]
    public class ScheduleTemplateBuilderApplier : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IScheduleTemplateBuilderViewService _scheduleTemplateBuilderViewService;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;
        private readonly ObservableCollection<DayOfWeek> _selectedDayOfWeekList;
        private TimeSpan _defaultSpanSize;

        private DateTime _applyBeginDate;
        private DateTime _applyEndDate;

        public ScheduleTemplateBuilderApplier(IScheduleTemplateBuilderViewService scheduleTemplateBuilderViewService,
            ISchedulingConfigurationViewService schedulingConfigurationViewService)
        {
            _scheduleTemplateBuilderViewService = scheduleTemplateBuilderViewService;
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _selectedDayOfWeekList = new ObservableCollection<DayOfWeek>();
            Init();

            ListOfTemplate = new ObservableCollection<Tuple<string, int>>();
            ListOfResource = new ObservableCollection<Tuple<string, int>>();
            RadioDatesIsChecked = true;
            RadioRepeatsIsChecked = false;
            ListOfFrequency = FrequencyPerMonthWrapper.GetListOfFrequencyPerMonthWrapper();
            ApplyBeginDate = DateTime.Today;
            ApplyEndDate = ApplyBeginDate.AddYears(1);

            Load = Command.Create<int?>(ExecuteLoad).Async(() => InteractionContext);
            TemplateChanged = Command.Composite(true,
                                                Command.Create(ExecuteTemplateChanged, () => SelectedTemplate != null).Async(() => InteractionContext),
                                                Command.Create(ExecuteClearTemplate, () => SelectedTemplate == null));
        
                
            SetDayOfWeek = Command.Create<DayOfWeek>(ExecuteSetDayOfWeek);
            SetFrequencyPerMonth = Command.Create(ExecuteSetFrequencyPerMonth);
            Cancel = Command.Create(ExecuteCancel);
            Apply = Command.Create(ExecuteApply, CheckCanExecuteApply).Async(() => InteractionContext);
            ResolveScheduleBlockConflict = Command.Create<ScheduleBlockConflictResolutionMode>(ExecuteResolveScheduleBlockConflict).Async(() => InteractionContext);
            ReplaceTemplate = Command.Create(ExecuteReplaceTemplate).Async(() => InteractionContext);
        }

        private void Init()
        {
            DefaultSpanLocation = new ColoredViewModel();
            SpanSize = new TimeSpan(0, 30, 0);
            AreaStart = new TimeSpan(0, 0, 0);
            AreaEnd = new TimeSpan(23, 59, 59);
            ScheduleBlocks = new ObservableCollection<ScheduleBlockViewModel>();
        }

        internal void ExecuteLoad(int? selectedTemplateId=null)
        {
            var configuration = _schedulingConfigurationViewService.GetSchedulingConfiguration();
            _defaultSpanSize = configuration.SpanSize;
            _applyEndDate = _applyBeginDate.AddMonths(configuration.MaximumApplyTemplateMonths);

            var info = _scheduleTemplateBuilderViewService.LoadInformation(ScheduleTemplateBuilderLoadTypes.Templates | ScheduleTemplateBuilderLoadTypes.Resources);
            info.Templates.RemoveAt(0);
            var localListOfTemplates = info.Templates.Select(t => new Tuple<string, int>(t.Name, t.Id)).ToExtendedObservableCollection();
            localListOfTemplates.Insert(0, new Tuple<string, int>("No Schedule", 0));
            ListOfTemplate = localListOfTemplates;
            ListOfResource = info.Resources;
            
            SelectedTemplate = ListOfTemplate.FirstOrDefault(i => !selectedTemplateId.HasValue || selectedTemplateId == 0 || i.Item2 == selectedTemplateId);
        }

        private void ExecuteSetDayOfWeek(DayOfWeek day)
        {
            if (_selectedDayOfWeekList.Contains(day))
                _selectedDayOfWeekList.Remove(day);
            else
                _selectedDayOfWeekList.Add(day);
            ProcessAndGenerateDates();
        }

        private void ExecuteSetFrequencyPerMonth()
        {
            ProcessAndGenerateDates();
        }

        private void ExecuteTemplateChanged()
        {
            if (SelectedTemplate == null) return;

            if (SelectedTemplate.Item2 == 0)
            {
                AreaStart = TimeSpan.Zero;
                AreaEnd = TimeSpan.FromDays(1);
                ScheduleBlocks = new ExtendedObservableCollection<ScheduleBlockViewModel>();
                SpanSize = _defaultSpanSize;
            }
            else
            {
                var template = _scheduleTemplateBuilderViewService.GetTemplate(SelectedTemplate.Item1);
                ScheduleBlocks = template.ToExtendedObservableCollection();
                if (template.Count > 0)
                {
                    var firstItem = template.First();
                    SpanSize = firstItem.EndTime - firstItem.StartTime;
                    AreaStart = template.Min(s => s.StartTime);
                    AreaEnd = template.Max(s => s.EndTime);
                }
                else
                {
                    AreaEnd = AreaStart = TimeSpan.FromMinutes(0);
                }
            }
        }

        private void ExecuteClearTemplate()
        {
            ScheduleBlocks = new ExtendedObservableCollection<ScheduleBlockViewModel>();
            AreaEnd = AreaStart = TimeSpan.FromMinutes(0);
        }

        private void ExecuteCancel()
        {
            ListOfFrequency = FrequencyPerMonthWrapper.GetListOfFrequencyPerMonthWrapper();
            ApplyBeginDate = DateTime.Today;
            ApplyEndDate = ApplyBeginDate.AddYears(1);
            GeneratedDates.Clear();
        }

        private bool CheckCanExecuteApply()
        {
            if (RadioDatesIsChecked) return SelectedTemplate != null && (ScheduleBlocks.Count != 0 || SelectedTemplate.Item2 == 0) && SelectedResource != null && Dates != null && Dates.Count != 0;
            if (RadioRepeatsIsChecked) return SelectedTemplate != null && (ScheduleBlocks.Count != 0 || SelectedTemplate.Item2 == 0) && SelectedResource != null && _selectedDayOfWeekList != null && _selectedDayOfWeekList.Count != 0 && SelectedFrequency != null && Dates != null && Dates.Count != 0;
            return false;
        }

        private void ExecuteApply()
        {
            if(SelectedTemplate.Item2 == 0)
            {
                PromptToReplaceTemplateDataTrigger = true;
            }
            else
            {
                HasScheduleBlockConflict = !_scheduleTemplateBuilderViewService.TryApplyTemplate(ScheduleBlocks.ToList(), ScheduleBlockConflictResolutionMode.Abort, SelectedTemplate.Item1, Dates, SelectedResource.Item2);
                HasScheduleBlockConflict = false;
            }
        }

        private void ExecuteResolveScheduleBlockConflict(ScheduleBlockConflictResolutionMode mode)
        {
            HasScheduleBlockConflict = !_scheduleTemplateBuilderViewService.TryApplyTemplate(ScheduleBlocks.ToList(), mode, SelectedTemplate.Item1, Dates, SelectedResource.Item2);
            HasScheduleBlockConflict = false;
        }

        private void ExecuteReplaceTemplate()
        {
            _scheduleTemplateBuilderViewService.TryApplyTemplate(ScheduleBlocks.ToList(), ScheduleBlockConflictResolutionMode.Replace, SelectedTemplate.Item1, Dates, SelectedResource.Item2);
        }

        private void ProcessAndGenerateDates()
        {
            if (_selectedDayOfWeekList.Count == 0 || SelectedFrequency == null) return;

            #region helper function getRangeOfDates

            Func<FrequencyPerMonthType, List<DateTime>> getRangeOfDates = frequencyType =>
                {
                    var dates = new List<DateTime>();
                    if (frequencyType == FrequencyPerMonthType.All || frequencyType == FrequencyPerMonthType.Other)
                    {
                        dates.AddRange(Enumerable.Range(0, 1 + ApplyEndDate.Subtract(ApplyBeginDate).Days).Select(offset => ApplyBeginDate.AddDays(offset)));
                    }
                    else
                    {
                        // Increased the range of dates to include the beginning of the start month for for all types except for 'All' and 'Every Other'.
                        // This is needed so we can calculate what is actually the first Friday of the Month within the given range (example).
                        var localApplyBeginDate = new DateTime(ApplyBeginDate.Year, ApplyBeginDate.Month, 1);
                        dates.AddRange(Enumerable.Range(0, 1 + ApplyEndDate.Subtract(localApplyBeginDate).Days).Select(offset => localApplyBeginDate.AddDays(offset)));
                    }
                    return dates.Where(d => _selectedDayOfWeekList.Contains(d.DayOfWeek)).ToList();
                };

            #endregion

            GeneratedDates.Clear();

            switch (SelectedFrequency.Type)
            {
                case FrequencyPerMonthType.All:
                    {
                        getRangeOfDates(SelectedFrequency.Type).ForEach(d => GeneratedDates.Add(d));
                        break;
                    }
                case FrequencyPerMonthType.Other:
                    {
                        var rangeOfDates = getRangeOfDates(SelectedFrequency.Type).AsQueryable();

                        var isBeginDateEvenWeek = ApplyBeginDate.GetWeekOfYear()%2 == 0;
                        // Gets a predicate that gets dates that are on every even week of the year, or odd week of the year
                        var predicate = isBeginDateEvenWeek ? rangeOfDates.CreatePredicate(d => d.GetWeekOfYear()%2 == 0)
                                                            : rangeOfDates.CreatePredicate(d => d.GetWeekOfYear()%2 != 0);

                        rangeOfDates.Where(predicate).ForEachWithSinglePropertyChangedNotification(d => GeneratedDates.Add(d));
                        break;
                    }
                default:
                    {
                        var groupByYearAndMonth = getRangeOfDates(SelectedFrequency.Type)
                            .GroupBy(d => d.Year)
                            .Select(g => new {Year = g.Key, Months = g.GroupBy(d => d.Month)});

                        foreach (var year in groupByYearAndMonth)
                        {
                            foreach (var month in year.Months)
                            {
                                // Skip count helps determine which 'week number' to pick
                                int skipCount = SelectedFrequency.GetSkipCount();
                                var selectedDay = month.Skip(skipCount).FirstOrDefault();
                                if (selectedDay != default(DateTime) && selectedDay >= ApplyBeginDate)
                                {
                                    GeneratedDates.Add(selectedDay);
                                }
                            }
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// The default location for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel DefaultSpanLocation { get; set; }

        /// <summary>
        /// Smallest unit of schedulable time
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan SpanSize { get; set; }

        /// <summary>
        /// Start of range for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaStart { get; set; }

        /// <summary>
        /// End of range for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaEnd { get; set; }

        /// <summary>
        /// Series of items composting a schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ScheduleBlockViewModel> ScheduleBlocks { get; set; }

        /// <summary>
        /// List of templates to show in the dropdown
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<Tuple<string, int>> ListOfTemplate { get; set; }

        /// <summary>
        /// Store the template that was selected in the dropdown
        /// </summary>
        [DispatcherThread]
        public virtual Tuple<string, int> SelectedTemplate { get; set; }

        /// <summary>
        /// List of resources to which templates can be applied to
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<Tuple<string, int>> ListOfResource { get; set; }

        /// <summary>
        /// Selected user that template will be applied to
        /// </summary>
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a resource to apply the template")]
        public virtual Tuple<string, int> SelectedResource { get; set; }

        /// <summary>
        /// Track if RadioDates item is seleted
        /// </summary>
        [DispatcherThread]
        public virtual bool RadioDatesIsChecked { get; set; }

        /// <summary>
        /// Track if RadioRepeats item is seleted
        /// </summary>
        [DispatcherThread]
        public virtual bool RadioRepeatsIsChecked { get; set; }

        /// <summary>
        /// List of options for when a given template will be applied
        /// </summary>
        public virtual ObservableCollection<FrequencyPerMonthWrapper> ListOfFrequency { get; set; }

        /// <summary>
        /// Selected option for when template will be applied
        /// </summary>
        [Required(ErrorMessage = "Please select which weeks to repeat monthly")]
        public virtual FrequencyPerMonthWrapper SelectedFrequency { get; set; }

        /// <summary>
        /// Beginning range for when template will be applied
        /// </summary>
        public virtual DateTime ApplyBeginDate
        {
            get { return _applyBeginDate; }
            set
            {
                _applyBeginDate = value;
                ApplyEndDate = ApplyBeginDate.AddYears(1);
                ProcessAndGenerateDates();
            }
        }

        /// <summary>
        /// Ending range for when template will be applied
        /// </summary>
        public virtual DateTime ApplyEndDate
        {
            get { return _applyEndDate; }
            set
            {
                _applyEndDate = value;
                ProcessAndGenerateDates();
            }
        }

        /// <summary>
        /// Hook for custom behavior to store multi selected dates
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<DateTime> GeneratedDates { get; set; }

        /// <summary>
        /// Get list of dates that template will be applied to
        /// </summary>
        public virtual List<DateTime> Dates
        {
            get { return GeneratedDates == null ? null : GeneratedDates.ToList(); }
        }

        /// <summary>
        /// Flag to prompt the user to replace the template
        /// </summary>
        public virtual bool PromptToReplaceTemplateDataTrigger { get; set; }

        /// <summary>
        /// Flag to control prompt when schedule blocks conflict
        /// </summary>
        [DispatcherThread]
        public virtual bool HasScheduleBlockConflict { get; set; }

        /// <summary>
        /// Command to set various day that template will be applied to
        /// </summary>
        public ICommand SetDayOfWeek { get; protected set; }

        /// <summary>
        /// Command to set the option within a month that template will be applied to
        /// </summary>
        public ICommand SetFrequencyPerMonth { get; protected set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to process the list template dropdown selection changed action
        /// </summary>
        public ICommand TemplateChanged { get; protected set; }

        /// <summary>
        /// Remove dates selected for template to be applied to
        /// </summary>
        public ICommand Cancel { get; protected set; }

        /// <summary>
        /// Command to create schedule blocks from a list of dates for a schedule template
        /// </summary>
        public ICommand Apply { get; protected set; }

        /// <summary>
        /// Command to process the end user choice on how to deal with schedule block conflict
        /// </summary>
        public ICommand ResolveScheduleBlockConflict { get; protected set; }

        /// <summary>
        /// Command to apply "no template"
        /// </summary>
        public ICommand ReplaceTemplate { get; protected set; }
    }

    public enum FrequencyPerMonthType
    {
        All,
        Other,
        First,
        Second,
        Third,
        Fourth,
        Fifth
    }

    public class FrequencyPerMonthWrapper
    {
        public string DisplayName { get; private set; }
        public int WeekOfMonth { get; private set; }
        public FrequencyPerMonthType Type { get; private set; }

        public FrequencyPerMonthWrapper(FrequencyPerMonthType type)
        {
            Type = type;
            switch (type)
            {
                case FrequencyPerMonthType.All:
                    DisplayName = "Every Week";
                    WeekOfMonth = -1;
                    break;
                case FrequencyPerMonthType.Other:
                    DisplayName = "Every Other Week";
                    WeekOfMonth = 0;
                    break;
                case FrequencyPerMonthType.First:
                    DisplayName = "Every First Week";
                    WeekOfMonth = 1;
                    break;
                case FrequencyPerMonthType.Second:
                    DisplayName = "Every Second Week";
                    WeekOfMonth = 2;
                    break;
                case FrequencyPerMonthType.Third:
                    DisplayName = "Every Third Week";
                    WeekOfMonth = 3;
                    break;
                case FrequencyPerMonthType.Fourth:
                    DisplayName = "Every Fourth Week";
                    WeekOfMonth = 4;
                    break;
                case FrequencyPerMonthType.Fifth:
                    DisplayName = "Every Fifth Week";
                    WeekOfMonth = 5;
                    break;
            }
        }

        public int GetSkipCount()
        {
            switch(Type)
            {
                case FrequencyPerMonthType.First:
                    return 0;
                case FrequencyPerMonthType.Second:
                    return 1;
                case FrequencyPerMonthType.Third:
                    return 2;
                case FrequencyPerMonthType.Fourth:
                    return 3;
                case FrequencyPerMonthType.Fifth:
                    return 4;
                default:
                    throw new Exception("Skip count no used with {0}".FormatWith(Type));
            }
        }

        public static ObservableCollection<FrequencyPerMonthWrapper> GetListOfFrequencyPerMonthWrapper()
        {
            return new ObservableCollection<FrequencyPerMonthWrapper> { new FrequencyPerMonthWrapper(FrequencyPerMonthType.All), new FrequencyPerMonthWrapper(FrequencyPerMonthType.Other), new FrequencyPerMonthWrapper(FrequencyPerMonthType.First), new FrequencyPerMonthWrapper(FrequencyPerMonthType.Second), new FrequencyPerMonthWrapper(FrequencyPerMonthType.Third), new FrequencyPerMonthWrapper(FrequencyPerMonthType.Fourth), new FrequencyPerMonthWrapper(FrequencyPerMonthType.Fifth) };
        }
    }

}