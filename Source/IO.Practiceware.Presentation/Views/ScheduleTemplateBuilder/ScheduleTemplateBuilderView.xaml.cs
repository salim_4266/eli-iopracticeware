﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Common.Controls;
using IO.Practiceware.Presentation.Views.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.ScheduleTemplateBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using Soaf.Data;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    /// <summary>
    /// Interaction logic for ScheduleTemplateBuilderView.xaml
    /// </summary>
    public partial class ScheduleTemplateBuilderView
    {
        public ScheduleTemplateBuilderView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the ScheduleTemplateBuilderView
    /// </summary>
    [SupportsDataErrorInfo]
    public class ScheduleTemplateBuilderViewContext : IViewContext
    {
        #region IViewContext Members

        public IInteractionContext InteractionContext { get; set; }

        #endregion

        private readonly IScheduleTemplateBuilderViewService _scheduleTemplateBuilderViewService;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;
        private readonly IScheduleTemplateBuilderViewContextHelper _scheduleTemplateBuilderViewContextHelper;
        private readonly Func<MultiCalendarViewManager> _createMultiCalendarViewManager;
        private readonly ColoredViewModel _dummyEllipsis = new ColoredViewModel { Id = 0, Name = "...", Color = Colors.Transparent };
        private readonly ConfirmationDialogViewManager _confirmationDialogViewManager;
        internal readonly ICommand SaveCommand;

        private bool _isInitPhase;
        private TimeSpan _areaStart;
        private TimeSpan _areaEnd;
        private bool _isTemplateChanging;
        private bool _isUpdatingBulkSelectionOfScheduleBlocks;

        public ScheduleTemplateBuilderViewContext(IScheduleTemplateBuilderViewService scheduleTemplateBuilderViewService, ISchedulingConfigurationViewService schedulingConfigurationViewService,
                                                  IScheduleTemplateBuilderViewContextHelper scheduleTemplateBuilderViewContextHelper,
                                                  Func<MultiCalendarViewManager> createMultiCalendarViewManager,
                                                  ScheduleTemplateBuilderApplier applier,
                                                  ConfirmationDialogViewManager confirmationDialogViewManager)
        {
            _scheduleTemplateBuilderViewService = scheduleTemplateBuilderViewService;
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _scheduleTemplateBuilderViewContextHelper = scheduleTemplateBuilderViewContextHelper;
            _createMultiCalendarViewManager = createMultiCalendarViewManager;
            Applier = applier;
            Init();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            TemplateChange = Command.Create(ExecuteTemplateChange).Async(() => InteractionContext);
            BulkSelectionScheduleBlocks = Command.Create<ScheduleTemplateBuilderMinuteIncrementSelector>(ExecuteBulkSelectionScheduleBlocks);
            IncrementScheduleBlockCategories = Command.Create<CategoryViewModel>(ExecuteIncrementScheduleBlockCategories);
            DecrementScheduleBlockCategories = Command.Create<CategoryViewModel>(ExecuteDecrementScheduleBlockCategories);
            SetUnavailableScheduleBlockCategory = Command.Create(ExecuteSetUnavailableScheduleBlockCategory);
            RemoveUnavailableScheduleBlockCategory = Command.Create(ExecuteRemoveUnavailableScheduleBlockCategory);
            ClearScheduleBlocks = Command.Create(ExecuteClearScheduleBlocks, () => ScheduleBlocks.Any(s => s.IsSelected && s.Categories != null && s.Categories.Count > 0));
            LocationChanged = Command.Create(ExecuteLocationChanged);
            ConfirmCreateTemplateSave = Command.Create(ExecuteConfirmCreateTemplateSave, () => SelectedTemplate != null && !string.IsNullOrWhiteSpace(TemplateName) && (SelectedTemplate.Id == 0 || HasChanges(SelectedTemplate)));
            CreateTemplateSaveAs = Command.Create(ExecuteCreateTemplateSaveAs, () => SelectedTemplate != null && !string.IsNullOrWhiteSpace(TemplateName) && (SelectedTemplate.Id == 0 || HasChanges(SelectedTemplate)));
            CreateTemplateDelete = Command.Create(ExecuteCreateTemplateDelete, () => SelectedTemplate != null && SelectedTemplate.Id != 0 && !string.IsNullOrWhiteSpace(TemplateName) && SelectedTemplate.Name.Equals(TemplateName, StringComparison.OrdinalIgnoreCase)).Async(() => InteractionContext);
            CreateTemplateCancel = Command.Create(ExecuteCreateTemplateCancel, () => ScheduleBlocks.Any(s => s.Categories.Count != 0)).Async(() => InteractionContext);
            ShowMultiCalender = Command.Create(ExecuteShowMultiCalendar);
            Close = Command.Create<IInteractionContext>(ExecuteClose);
            Closing = Command.Create<Action>(ExecuteClosing);

            ShowAMPM = Command.Create(ToggleAMPM);

            SelectApplyMode = Command.Create(ExecuteSelectApplyMode);
            TabChange = Command.Create(ExecuteTabChange);
            SaveCommand = Command.Create(Save, () => SelectedTemplate != null && !string.IsNullOrWhiteSpace(TemplateName) && (SelectedTemplate.Id == 0 || HasChanges(SelectedTemplate))).Async(() => InteractionContext);
            _confirmationDialogViewManager = confirmationDialogViewManager;
        }

        private void ExecuteClosing(Action cancelClose)
        {
            CancelViewClosingAction = cancelClose;
            bool hasChanges = SelectedTemplate.CastTo<IChangeTracking>().IsChanged;

            if (hasChanges)
            {
                var loadArgument = new ConfirmationDialogLoadArguments
                {
                    Cancel = Command.Create(cancelClose),
                    Content = "You have unsaved changes.  Would you like to save your changes?",
                    YesContent = "Save",
                    NoContent = "Don't Save",
                    CancelContent = "Cancel",
                };

                bool isValid = Validate();
                if (!isValid)
                {
                    loadArgument.Yes = Command.Create(HandleValidations);
                }
                else
                {
                    loadArgument.Yes = SaveCommand.CanExecute(null) ? SaveCommand : Command.Create(cancelClose);
                }

                _confirmationDialogViewManager.ShowConfirmationDialogView(loadArgument);
            }
        }

        private void ToggleAMPM()
        {
            var toggle = ApplyAMPM;
            UpdateToggle(toggle);
        }

        private void UpdateToggle(bool toggle)
        {
            string amPMSettingName = "AMPMCounter";
            string _qryUpdate = " Update model.applicationsettings set value ='" + toggle.ToString() + "' where Name ='" + amPMSettingName + "'";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Execute(_qryUpdate);
            }
        }

        private bool Validate()
        {
            bool isValid = true;

            isValid = !(string.IsNullOrWhiteSpace(TemplateName));

            return isValid;
        }

        private void HandleValidations()
        {
            if (string.IsNullOrWhiteSpace(TemplateName))
            {
                InteractionManager.Current.Alert("Invalid entries have been entered and cannot be saved.  Please correct the errors marked in red");
                
                if (CancelViewClosingAction != null)
                {
                    CancelViewClosingAction();
                }
            }
        }

        private void Init()
        {
            _isInitPhase = true;
            DefaultSpanLocation = new ColoredViewModel();
            SpanSize = new TimeSpan(0, 30, 0);
            AreaStart = new TimeSpan(0, 0, 0);
            AreaEnd = new TimeSpan(23, 59, 59);
        }

        private void ExecuteLoad()
        {
            var info = _scheduleTemplateBuilderViewService.LoadInformation(ScheduleTemplateBuilderLoadTypes.Categories |
                                                                           ScheduleTemplateBuilderLoadTypes.Locations |
                                                                           ScheduleTemplateBuilderLoadTypes.Templates);
            ListOfLocation = info.Locations.OrderBy(l => l.Name).ToExtendedObservableCollection();
            ListOfCategory = info.Categories;
            ListOfTemplate = info.Templates;
            SetupScheduleView();
            Applier.DefaultSpanLocation = DefaultSpanLocation;
            Applier.SpanSize = SpanSize;
            Applier.AreaStart = AreaStart;
            Applier.AreaEnd = AreaEnd;
            Applier.ExecuteLoad();
            string toggle = GetAMPMSettings();
            ApplyAMPM = bool.Parse(toggle);

            if (LoadArguments != null && LoadArguments.SelectedMode == ScheduleTemplateBuilderMode.ApplyTemplate)
            {
                SelectedMode = ScheduleTemplateBuilderMode.ApplyTemplate;
                if (LoadArguments.SelectedResourceId.HasValue)
                {
                    Applier.SelectedResource = Applier.ListOfResource.FirstOrDefault(u => u.Item2 == LoadArguments.SelectedResourceId);
                }
                if (LoadArguments.SelectedDate.HasValue)
                {
                    Applier.GeneratedDates = new ObservableCollection<DateTime> { LoadArguments.SelectedDate.Value };
                    Applier.RadioDatesIsChecked = true;
                }
            }

            _isInitPhase = false;
        }
        public string GetAMPMSettings()
        {
            string token = string.Empty;
            string key = "AMPMCounter";
            string _query = "select top 1 value from model.applicationsettings where name = '" + key + "'";
            try
            {
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    token = dbConnection.Execute<object>(_query) as string;
                }
            }
            catch (SqlException)
            {

            }
            finally
            {

            }
            return token;
        }
        private void SetupScheduleView()
        {
            var configuration = _schedulingConfigurationViewService.GetSchedulingConfiguration();
            DefaultSpanLocation = configuration.DefaultLocation;
            SpanSize = configuration.SpanSize;
            AreaStart = configuration.AreaStart;
            AreaEnd = configuration.AreaEnd;
            ListOfMinuteIncrementSelector = _scheduleTemplateBuilderViewContextHelper.LoadMinuteIncrementSelector(SpanSize);
        }

        private void ExecuteTemplateChange()
        {
            try
            {
                _isTemplateChanging = true;

                if (SelectedTemplate == null) return;

                ObservableCollection<ScheduleBlockViewModel> scheduleBlocks;
                IsConfirmationRequired = false;
                SuppressItemSelectedCommand = true;

                if (SelectedTemplate.Id != 0)
                {
                    TemplateName = SelectedTemplate.Name;
                    var template = _scheduleTemplateBuilderViewService.GetTemplate(SelectedTemplate.Name);
                    if (template.Count > 0)
                    {
                        var firstItem = template.First();
                        SpanSize = firstItem.EndTime - firstItem.StartTime;
                        AreaStart = template.Min(s => s.StartTime);
                        AreaEnd = template.Max(s => s.EndTime);
                        scheduleBlocks = template.ToExtendedObservableCollection();
                    }
                    else
                    {
                        AreaStart = TimeSpan.FromHours(9);
                        AreaEnd = TimeSpan.FromHours(17);
                        scheduleBlocks = new ScheduleBlockViewModel[0].ToExtendedObservableCollection();
                    }
                }
                else
                {
                    TemplateName = null;
                    var configuration = _schedulingConfigurationViewService.GetSchedulingConfiguration();
                    DefaultSpanLocation = configuration.DefaultLocation;
                    SpanSize = configuration.SpanSize;
                    AreaStart = configuration.AreaStart;
                    AreaEnd = configuration.AreaEnd;
                    scheduleBlocks = _scheduleTemplateBuilderViewContextHelper.GenerateScheduleBlocks(AreaStart, AreaEnd, SpanSize, DefaultSpanLocation).ToExtendedObservableCollection();
                }

                SelectedTemplate.ScheduleBlocks.CastTo<INotifyPropertyChanged>().PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnScheduleBlocksPropertyChanged);
                SelectedTemplate.ScheduleBlocks = scheduleBlocks;
                SelectedTemplate.ScheduleBlocks.CastTo<INotifyPropertyChanged>().PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(OnScheduleBlocksPropertyChanged);
                SelectedTemplate.BeginEdit();
                SelectedTemplate.AcceptChanges();

                IsConfirmationRequired = true;
                SuppressItemSelectedCommand = false;
            }
            finally
            {
                if (!_isInitPhase)
                {
                    ListOfMinuteIncrementSelector.ToList().ForEach(s => s.IsChecked = false);
                    UpdateSelectedLocation();
                    ScheduleBlocks = null;
                }

                _isTemplateChanging = false;
            }
        }

        private void ExecuteBulkSelectionScheduleBlocks(ScheduleTemplateBuilderMinuteIncrementSelector selector)
        {
            if (_isTemplateChanging || _isUpdatingBulkSelectionOfScheduleBlocks) return;

            try
            {
                _isUpdatingBulkSelectionOfScheduleBlocks = true;
                HasMadeChangesToSelection = false;
                bool isDeselectAll = selector.Increment == TimeSpan.MinValue;
                bool isSelectAll = selector.Increment == TimeSpan.MaxValue;
                if (isDeselectAll) selector.IsChecked = false;
                ScheduleBlocks.Where(s => isDeselectAll || isSelectAll || s.StartTime.Minutes == selector.Increment.Minutes).ToList().ForEach(s => s.IsSelected = selector.IsChecked);
                ScheduleBlocks = ScheduleBlocks.ToExtendedObservableCollection();
            }
            finally
            {
                _isUpdatingBulkSelectionOfScheduleBlocks = false;
                UpdateBulkSelectionStates();
                UpdateSelectedLocation();
            }
        }

        private void OnScheduleBlocksPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Item.IsSelected")
            {
                UpdateBulkSelectionStates();

                if (HasMadeChangesToSelection)
                {
                    HasMadeChangesToSelection = false;
                    var sourceBlock = ((CascadingPropertyChangedEventArgs)e).OriginalSender as ScheduleBlockViewModel;
                    if (sourceBlock != null)
                    {
                        ScheduleBlocks.Where(b => b != sourceBlock && b.IsSelected).ToList().ForEach(b => b.IsSelected = false);
                        // If changes were made to the selection, and a new selection is made, always keep the new selection selected
                        sourceBlock.IsSelected = true;
                    }
                }
                UpdateSelectedLocation();
            }
        }

        private void UpdateSelectedLocation()
        {
            var locations = ScheduleBlocks.Where(s => s.IsSelected).Select(s => s.Location).Distinct().ToList();

            if (!locations.Any()) locations = ScheduleBlocks.Select(s => s.Location).Distinct().ToList();

            if (locations.Count == 1)
            {
                if (locations.First().Id != DefaultSpanLocation.Id) DefaultSpanLocation = ListOfLocation.First(l => l.Id == locations.First().Id);
            }
            else if (locations.Count > 1)
            {
                if (!ListOfLocation.Contains(_dummyEllipsis)) ListOfLocation.Add(_dummyEllipsis);
                DefaultSpanLocation = _dummyEllipsis;
            }
            HasMadeChangesToSelection = false;
        }

        /// <summary>
        /// Keeps the bulk increment selectors in sync with the schedule block selection states.
        /// </summary>
        private void UpdateBulkSelectionStates()
        {
            if (_isUpdatingBulkSelectionOfScheduleBlocks) return;
            try
            {
                _isUpdatingBulkSelectionOfScheduleBlocks = true;

                foreach (var ms in ListOfMinuteIncrementSelector.Skip(2))
                {
                    var selector = ms;
                    selector.IsChecked = ScheduleBlocks.Where(s => s.StartTime.Minutes == selector.Increment.Minutes).All(i => i.IsSelected);
                }
                ListOfMinuteIncrementSelector.First(ms => ms.Increment == TimeSpan.MaxValue).IsChecked = ScheduleBlocks.All(s => s.IsSelected);
                ListOfMinuteIncrementSelector.First(ms => ms.Increment == TimeSpan.MinValue).IsChecked = ScheduleBlocks.All(s => !s.IsSelected);
            }
            finally
            {
                _isUpdatingBulkSelectionOfScheduleBlocks = false;
            }
        }


        private void ExecuteIncrementScheduleBlockCategories(CategoryViewModel category)
        {
            ScheduleBlocks.ForEachWithSinglePropertyChangedNotification(scheduleBlock =>
            {
                if (!scheduleBlock.IsSelected) return;
                var unavailableCategoryViewModel = scheduleBlock.Categories.FirstOrDefault(sc => sc.Id == UnavailableCategoryViewModel.UnavailableCategoryId && sc.Name == UnavailableCategoryViewModel.UnavailableCategoryName);
                if (unavailableCategoryViewModel != null) return;
                scheduleBlock.Categories.Add(category);
            });
        }

        private void ExecuteDecrementScheduleBlockCategories(CategoryViewModel category)
        {
            ScheduleBlocks.ForEachWithSinglePropertyChangedNotification(scheduleBlock =>
            {
                if (!scheduleBlock.IsSelected) return;
                scheduleBlock.Categories.Remove(category);
            });
        }

        private void ExecuteSetUnavailableScheduleBlockCategory()
        {
            ScheduleBlocks.ForEachWithSinglePropertyChangedNotification(scheduleBlock =>
            {
                if (!scheduleBlock.IsSelected) return;
                scheduleBlock.Categories.Clear();
                scheduleBlock.Categories.Add(new UnavailableCategoryViewModel());
            });
        }

        private void ExecuteRemoveUnavailableScheduleBlockCategory()
        {
            var blocks = ScheduleBlocks.Where(b => b.IsSelected && b.Categories.OfType<UnavailableCategoryViewModel>().Any()).ToList();
            foreach(var block in blocks)
            {
                var unavailableCategory = block.Categories.OfType<UnavailableCategoryViewModel>().First();
                block.Categories.Remove(unavailableCategory);
            }
        }

        private void ExecuteClearScheduleBlocks()
        {
            ScheduleBlocks.ForEachWithSinglePropertyChangedNotification(scheduleBlock =>
            {
                if (!scheduleBlock.IsSelected) return;
                scheduleBlock.Categories.Clear();
            });
        }

        private void ExecuteLocationChanged()
        {
            if (DefaultSpanLocation.Id == _dummyEllipsis.Id) return;

            foreach (var scheduleBlock in ScheduleBlocks.Where(scheduleBlock => scheduleBlock.IsSelected))
            {
                if (!Equals(scheduleBlock.Location, DefaultSpanLocation))
                {
                    scheduleBlock.Location = DefaultSpanLocation;
                }
            }

            ListOfLocation.Remove(_dummyEllipsis);
        }

        private void ExecuteCreateTemplateSaveAs()
        {
            if (ListOfTemplate.Where(t => t.Id != SelectedTemplate.Id).Any(t => t.Name.Equals(TemplateName, StringComparison.OrdinalIgnoreCase)))
            {
                ConfirmSaveTemplateFlag = true;
            }
            else
            {
                SaveCommand.Execute();
            }
        }

        private void ExecuteConfirmCreateTemplateSave()
        {
            if (!ConfirmSaveTemplateFlag)
            {
                if (ListOfTemplate.Where(t => t.Id != SelectedTemplate.Id).Any(t => t.Name.Equals(TemplateName, StringComparison.OrdinalIgnoreCase)))
                {
                    ConfirmSaveTemplateFlag = true;
                    return;
                }
            }
            else
            {
                ConfirmSaveTemplateFlag = false;
            }

            SaveCommand.Execute();
        }

        private void ExecuteTabChange()
        {
            if (SelectedMode == ScheduleTemplateBuilderMode.AddEditTemplate)
            {
                TemplateName = null;
                if (ListOfTemplate.First().Id == SelectedTemplate.Id)
                {
                    TemplateChange.Execute();
                }
                else
                {
                    TemplateName = SelectedTemplate.Name;
                    SelectedTemplate = ListOfTemplate.First();
                }
            }
            else if (!_isInitPhase)
            {

                Applier.RadioRepeatsIsChecked = true;
                Applier.Load.Execute(SelectedTemplate.Id);
            }
        }

        private void Save()
        {
            _scheduleTemplateBuilderViewService.SaveTemplate(TemplateName, ScheduleBlocks.ToList());
            SelectedTemplate.CastTo<IEditableObject>().EndEdit();
            SelectedTemplate.CastTo<IChangeTracking>().AcceptChanges();
            //while Creating new template
            if (SelectedTemplate.Id == 0) TemplateName = null;

            if (!InteractionContext.IsComplete) InteractionContext.Complete(true);
        }

        private void ExecuteCreateTemplateDelete()
        {
            _scheduleTemplateBuilderViewService.DeleteTemplate(TemplateName);
            SelectedTemplate.CastTo<IEditableObject>().EndEdit();
            SelectedTemplate.CastTo<IChangeTracking>().AcceptChanges();
            var deletedTemplate = SelectedTemplate;
            SelectedTemplate = ListOfTemplate.First();
            ListOfTemplate.Remove(deletedTemplate);
        }

        private void ExecuteCreateTemplateCancel()
        {
            TemplateName = null;
            var configuration = _schedulingConfigurationViewService.GetSchedulingConfiguration();
            DefaultSpanLocation = configuration.DefaultLocation;
            SpanSize = configuration.SpanSize;
            AreaStart = configuration.AreaStart;
            AreaEnd = configuration.AreaEnd;

            if (SelectedTemplate.Id == ListOfTemplate.First().Id)
            {
                SelectedTemplate = null;
            }
            SelectedTemplate = ListOfTemplate.First();
        }

        private void ExecuteShowMultiCalendar()
        {
            /*
            This will always always launch Multi-Days
            - A single resource will be passed (optional)
            - One or more dates will be passed (optional)
            - If many dates then a max of the first 7 dates
            */
            var passLoadArguments = Applier.SelectedResource != null || (Applier.Dates != null && Applier.Dates.Count != 0);
            var loadArgs = new MultiCalendarLoadArguments
            {
                LoadMode = MultiCalendarLoadMode.Days,
                Dates = (Applier.Dates == null || Applier.Dates.Count == 0) ? null : Applier.Dates.Take(7).ToList(),
                ResourceIds = Applier.SelectedResource == null ? null : new List<int> { Applier.SelectedResource.Item2 }
            };

            var manager = _createMultiCalendarViewManager();
            if (passLoadArguments)
                manager.ShowMultiCalendar(loadArgs);
            else
                manager.ShowMultiCalendar();
        }

        private static void ExecuteClose(IInteractionContext context)
        {
            context.Complete(false);
        }

        private void ExecuteSelectApplyMode()
        {
            if (!PermissionId.ApplyTemplatesToSchedules.EnsurePermission()) return;

            var switchTabAndAcceptChanges = Command.Create(() =>
            {
                SelectedTemplate.CastTo<IEditableObject>().EndEdit();
                SelectedTemplate.CastTo<IChangeTracking>().AcceptChanges();
                SelectedMode = ScheduleTemplateBuilderMode.ApplyTemplate;
            });

            if (HasChanges())
            {
                var loadArgument = new ConfirmationDialogLoadArguments
                {
                    No = switchTabAndAcceptChanges,
                    Content = "You have unsaved changes.  Would you like to save your changes?",
                    YesContent = "Save",
                    NoContent = "Don't Save",
                    CancelContent = "Cancel",
                };

                bool isValid = Validate();
                if (!isValid)
                {
                    loadArgument.Yes = Command.Create(HandleValidations);
                }
                else
                {
                    loadArgument.Yes = SaveCommand.CanExecute(null) ? SaveCommand : switchTabAndAcceptChanges;
                }

                _confirmationDialogViewManager.ShowConfirmationDialogView(loadArgument);
            }
            else
            {
                switchTabAndAcceptChanges.Execute();
            }
        }

        /// <summary>
        /// Flag determines whether or not to set off the confirm command for saving templates
        /// </summary>
        [DispatcherThread]
        public virtual bool ConfirmSaveTemplateFlag { get; set; }

        /// <summary>
        /// The default location for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual ColoredViewModel DefaultSpanLocation { get; set; }

        /// <summary>
        /// Smallest unit of schedulable time
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan SpanSize { get; set; }

        /// <summary>
        /// Start of range for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaStart
        {
            get { return _areaStart; }
            set
            {
                if (!_isTemplateChanging && value >= AreaEnd)
                {
                    return;
                }
                if (!_isInitPhase) AreaStartChanged(_areaStart, value);
                _areaStart = value;
            }
        }

        private void AreaStartChanged(TimeSpan oldValue, TimeSpan newValue)
        {
            if (ScheduleBlocks.IsNullOrEmpty()) return;

            if (newValue < oldValue)
            {
                var location = ReferenceEquals(DefaultSpanLocation, _dummyEllipsis) ? ListOfLocation.First() : DefaultSpanLocation;
                var newBlocks = _scheduleTemplateBuilderViewContextHelper.GenerateScheduleBlocks(newValue, oldValue, SpanSize, location);
                var blocksToAdd = new List<ScheduleBlockViewModel>();

                var firstExistingBlock = ScheduleBlocks.OrderBy(b => b.StartTime).First();
                var lastExistingBlock = ScheduleBlocks.OrderByDescending(b => b.EndTime).First();

                blocksToAdd.AddRange(newBlocks.Where(b => b.StartTime < firstExistingBlock.StartTime));
                blocksToAdd.AddRange(newBlocks.Where(b => b.StartTime > lastExistingBlock.StartTime));
                ScheduleBlocks.AddRangeWithSinglePropertyChangedNotification(blocksToAdd);
            }
            else if (oldValue < newValue)
            {
                var deletedScheduleBlocks = ScheduleBlocks.Where(s => s.StartTime < newValue).ToList();
                ScheduleBlocks.RemoveRangeWithSinglePropertyChangedNotification(deletedScheduleBlocks);
            }
            UpdateBulkSelectionStates();
            UpdateSelectedLocation();
        }

        /// <summary>
        /// End of range for schedule blocks
        /// </summary>
        [DispatcherThread]
        public virtual TimeSpan AreaEnd
        {
            get { return _areaEnd; }
            set
            {
                if (!_isTemplateChanging && value <= AreaStart)
                {
                    return;
                }
                if (!_isInitPhase) AreaEndChanged(_areaEnd, value);
                _areaEnd = value;
            }
        }

        private void AreaEndChanged(TimeSpan oldValue, TimeSpan newValue)
        {
            if (ScheduleBlocks.IsNullOrEmpty()) return;

            if (oldValue < newValue)
            {
                var location = ReferenceEquals(DefaultSpanLocation, _dummyEllipsis) ? ListOfLocation.First() : DefaultSpanLocation;
                var newBlocks = _scheduleTemplateBuilderViewContextHelper.GenerateScheduleBlocks(oldValue, newValue, SpanSize, location);
                var blocksToAdd = new List<ScheduleBlockViewModel>();

                var firstExistingBlock = ScheduleBlocks.OrderBy(b => b.StartTime).First();
                var lastExistingBlock = ScheduleBlocks.OrderByDescending(b => b.EndTime).First();

                blocksToAdd.AddRange(newBlocks.Where(b => b.StartTime < firstExistingBlock.StartTime));
                blocksToAdd.AddRange(newBlocks.Where(b => b.StartTime > lastExistingBlock.StartTime));
                ScheduleBlocks.AddRangeWithSinglePropertyChangedNotification(blocksToAdd);
            }
            else if (newValue < oldValue)
            {
                var deletedScheduleBlocks = ScheduleBlocks.Where(s => s.EndTime > newValue).ToList();
                ScheduleBlocks.RemoveRangeWithSinglePropertyChangedNotification(deletedScheduleBlocks);
            }
            UpdateBulkSelectionStates();
            UpdateSelectedLocation();
        }

        private bool HasChanges()
        {
            return HasChanges(SelectedTemplate);
        }

        private bool HasChanges(ScheduleTemplateViewModel scheduleTemplate)
        {
            return scheduleTemplate.CastTo<IChangeTracking>().IsChanged || (scheduleTemplate.Id == 0 ? string.Empty : scheduleTemplate.Name) != (TemplateName ?? string.Empty);
        }

        /// <summary>
        /// Series of items composting a schedule template
        /// </summary>
        [DispatcherThread]
        [DependsOn("SelectedTemplate")]
        public virtual ObservableCollection<ScheduleBlockViewModel> ScheduleBlocks
        {
            get
            {
                return SelectedTemplate != null ? SelectedTemplate.ScheduleBlocks : new ExtendedObservableCollection<ScheduleBlockViewModel>();
            }
            set { }
        }

        /// <summary>
        /// List of time increment that can be used to bulk select time schedule blocks in UI
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ScheduleTemplateBuilderMinuteIncrementSelector> ListOfMinuteIncrementSelector { get; set; }

        /// <summary>
        /// List of locations that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ColoredViewModel> ListOfLocation { get; set; }

        /// <summary>
        /// List of categories that can be used when creating schedule template
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<CategoryViewModel> ListOfCategory { get; set; }

        /// <summary>
        /// List of templates to show in the dropdown
        /// </summary>
        [DispatcherThread]
        public virtual ObservableCollection<ScheduleTemplateViewModel> ListOfTemplate { get; set; }

        /// <summary>
        /// Store the template that was selected in the dropdown
        /// </summary>
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a template to apply")]
        public virtual ScheduleTemplateViewModel SelectedTemplate { get; set; }

        /// <summary>
        /// For the data entry textbox
        /// </summary>
        [DispatcherThread]
        [Required(ErrorMessage = "Please enter a name to save the template")]
        public virtual string TemplateName { get; set; }

        /// <summary>
        /// For the Save/SaveAs button
        /// </summary>
        [DependsOn("SelectedTemplate", "TemplateName")]
        public virtual string CreateTemplateSaveButtonContent
        {
            get
            {
                if (SelectedTemplate == null) return "Save";
                return SelectedTemplate.Id != 0 && !SelectedTemplate.Name.Equals(TemplateName, StringComparison.OrdinalIgnoreCase) ? "Save As" : "Save";
            }
        }

        /// <summary>
        /// For the Save/SaveAs button confirm dialog message
        /// </summary>
        [DependsOn("SelectedTemplate", "TemplateName")]
        public virtual string CreateTemplateSaveButtonMessage
        {
            get
            {
                const string standard = "Are you sure?";
                const string overwrite = "There is an existing template with the same name. Are you sure you want to overwrite the existing template? This will not affect any days where the template has already been applied to the schedule.";
                if (SelectedTemplate == null) return standard;
                return ListOfTemplate.Any(t => t.Name.Equals(TemplateName, StringComparison.OrdinalIgnoreCase)) ? overwrite : standard;
            }
        }

        [DispatcherThread]
        public virtual bool IsConfirmationRequired { get; set; }

        [DispatcherThread]
        public virtual bool SuppressItemSelectedCommand { get; set; }

        [DispatcherThread]
        public virtual bool HasMadeChangesToSelection { get; set; }

        /// <summary>
        /// Command to load the UI dropdowns
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Command to confirm a template change
        /// </summary>
        public ICommand TemplateChange { get; protected set; }

        /// <summary>
        /// Command to bulk add a single given cateogry for the selected schedule blocks
        /// </summary>
        public ICommand BulkSelectionScheduleBlocks { get; private set; }

        /// <summary>
        /// Command to add a single given category for the selected schedule blocks
        /// </summary>
        public ICommand IncrementScheduleBlockCategories { get; private set; }

        /// <summary>
        /// Command to remove a single given category from the selected schedule blocks
        /// </summary>
        public ICommand DecrementScheduleBlockCategories { get; private set; }

        /// <summary>
        /// Command to add the unavailable for the selected schedule blocks
        /// </summary>
        public ICommand SetUnavailableScheduleBlockCategory { get; private set; }

        /// <summary>
        /// Command to remove the unavailable from the selected schedule blocks
        /// </summary>
        public ICommand RemoveUnavailableScheduleBlockCategory { get; private set; }

        /// <summary>
        /// Command to remove all items and reset the schedule view
        /// </summary>
        public ICommand ClearScheduleBlocks { get; private set; }

        /// <summary>
        /// Command to process the list location dropdown selection changed action
        /// </summary>
        public ICommand LocationChanged { get; private set; }

        /// <summary>
        /// Command to save a template. Will pop up confirmation first
        /// </summary>
        public ICommand CreateTemplateSaveAs { get; protected set; }

        /// <summary>
        /// Command to confirm the save an new schedule template
        /// </summary>
        public ICommand ConfirmCreateTemplateSave { get; protected set; }

        /// <summary>
        /// Command to delete an existing schedule template
        /// </summary>
        public ICommand CreateTemplateDelete { get; protected set; }

        /// <summary>
        /// Command to cancel current template creation
        /// </summary>
        public ICommand CreateTemplateCancel { get; protected set; }

        /// <summary>
        /// Command to show the Calendar
        /// </summary>
        public ICommand ShowMultiCalender { get; set; }


        public ICommand ShowAMPM { get; set; }

        /// <summary>
        /// Gets a command that is executed when the associated view is closing.
        /// It is able to cancel the closing event through the passed in bool Action.
        /// </summary>
        public ICommand Closing { get; protected set; }

        /// <summary>
        /// Command to close UI
        /// </summary>
        public ICommand Close { get; protected set; }

        public ICommand SelectApplyMode { get; set; }

        /// <summary>
        /// Command that is called after a tab has been selected
        /// </summary>
        public ICommand TabChange { get; protected set; }

        /// <summary>
        /// Underlying object to capture apply options
        /// </summary>
        public virtual ScheduleTemplateBuilderApplier Applier { get; set; }

        [DispatcherThread]
        public virtual bool ApplyAMPM { get; set; }

        /// <summary>
        /// The currently selected mode.
        /// </summary>
        /// <remarks>
        /// This corresponds to the selected tab on the left hand panel.
        /// </remarks>
        [DispatcherThread]
        public virtual ScheduleTemplateBuilderMode SelectedMode { get; set; }

        public virtual ScheduleTemplateBuilderLoadArguments LoadArguments { get; set; }

        public Action CancelViewClosingAction { get; set; }
    }

    public enum ScheduleTemplateBuilderMode
    {
        AddEditTemplate,
        ApplyTemplate
    }
}