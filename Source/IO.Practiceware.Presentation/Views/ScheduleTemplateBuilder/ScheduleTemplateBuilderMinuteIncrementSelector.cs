﻿using Soaf.Presentation;
using System;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateBuilderMinuteIncrementSelector : IViewModel
    {
        [DispatcherThread]
        public virtual bool IsChecked { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual TimeSpan Increment { get; set; }
    }
}