﻿using System;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateBuilderDeselectAllSelector : ScheduleTemplateBuilderMinuteIncrementSelector
    {
        public ScheduleTemplateBuilderDeselectAllSelector()
        {
            IsChecked = true;
            DisplayName = "Deselect All";
            Increment = TimeSpan.MinValue;
        }
    }
}
