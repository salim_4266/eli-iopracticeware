﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.Views.Common.Converters;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;
using DataTrigger = System.Windows.DataTrigger;
using WindowStartupLocation = Soaf.Presentation.WindowStartupLocation;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateBuilderCustomRadScheduleViewBehavior : Behavior<RadScheduleView>
    {
        private readonly ExtendedObservableCollection<CustomAppointment> _listOfTelerikAppointment = new ExtendedObservableCollection<CustomAppointment>();

        # region Telerik Slot Wrapper

        private sealed class CustomSlot : Slot
        {
            private ScheduleBlockViewModel _scheduleBlock;

            [UsedImplicitly]
            public ScheduleBlockViewModel ScheduleBlock
            {
                get { return _scheduleBlock; }
                private set
                {
                    _scheduleBlock = value;
                    var today = DateTime.Today;
                    Start = today + value.StartTime;
                    End = today + value.EndTime;

                    OnPropertyChanged("ScheduleBlock");
                }
            }

            public CustomSlot(ScheduleBlockViewModel scheduleBlock)
            {
                ScheduleBlock = scheduleBlock;
            }

            public override Slot Copy()
            {
                return this;
            }

            public override void CopyFrom(Slot other)
            {
                var customSlot = other as CustomSlot;
                if (customSlot != null) ScheduleBlock = customSlot.ScheduleBlock;
            }
        }

        #endregion

        # region Telerik Appointment Wrapper

        private sealed class CustomAppointment : Appointment
        {
            private readonly CategoryViewModel _categoryViewModel;
            private readonly bool _useRightMouseButtonForScheduleBlockSelection;
            private readonly ICommand _saveComment;
            private readonly ICommand _removeComment;
            private readonly ScheduleBlockViewModel _scheduleBlock;
            private readonly int _categoryIndex;

            public ScheduleBlockViewModel ScheduleBlock
            {
                get { return _scheduleBlock; }
            }

            public CategoryViewModel CategoryViewModel
            {
                get { return _categoryViewModel; }
            }

            public int CategoryIndex
            {
                get { return _categoryIndex; }
            }

            public CustomAppointment(CategoryViewModel categoryViewModel, int categoryIndex, ScheduleBlockViewModel scheduleBlock, bool useRightMouseButtonForScheduleBlockSelection)
            {
                _categoryIndex = categoryIndex;
                _categoryViewModel = categoryViewModel;
                _scheduleBlock = scheduleBlock;
                _useRightMouseButtonForScheduleBlockSelection = useRightMouseButtonForScheduleBlockSelection;
                Category = new Category { CategoryBrush = new SolidColorBrush(categoryViewModel.Color) };
                _saveComment = Command.Create(OnSaveComment);
                _removeComment = Command.Create(OnRemoveComment);
                ToggleScheduleBlockSelection = Command.Create(() => IsScheduleBlockSelected = !IsScheduleBlockSelected);

                Start = DateTime.Today.Add(_scheduleBlock.StartTime);
                End = DateTime.Today.Add(_scheduleBlock.EndTime);
            }

            [UsedImplicitly]
            public ICommand ToggleScheduleBlockSelection { get; private set; }

            [UsedImplicitly]
            public bool SupportsComments
            {
                get { return _categoryViewModel is UnavailableCategoryViewModel; }
            }

            [UsedImplicitly]
            public bool NoComment
            {
                get { return String.IsNullOrWhiteSpace(Comment); }
            }

            [UsedImplicitly]
            public bool HasComment
            {
                get { return !String.IsNullOrWhiteSpace(Comment); }
            }

            public override string Subject
            {
                get { return _categoryViewModel.Name; }
                set { _categoryViewModel.Name = value; }
            }

            private string Comment
            {
                get { return _scheduleBlock.Comment; }
                set
                {
                    _scheduleBlock.Comment = value;
                    OnPropertyChanged("Comment");
                    OnPropertyChanged("DisplayName");
                    OnPropertyChanged("NoComment");
                    OnPropertyChanged("HasComment");
                }
            }

            private bool IsScheduleBlockSelected
            {
                get { return _scheduleBlock.IsSelected; }
                set { _scheduleBlock.IsSelected = value; }
            }

            [UsedImplicitly]
            public bool UseRightMouseButtonForScheduleBlockSelection
            {
                get { return _useRightMouseButtonForScheduleBlockSelection; }
            }

            [UsedImplicitly]
            public string DisplayName
            {
                get { return String.IsNullOrWhiteSpace(Comment) ? Subject : String.Format("{0}: {1}", Subject, Comment); }
            }

            [UsedImplicitly]
            public ICommand SaveComment
            {
                get { return _saveComment; }
            }

            [UsedImplicitly]
            public ICommand RemoveComment
            {
                get { return _removeComment; }
            }

            private void OnSaveComment()
            {
                var prompt = InteractionManager.Current.Prompt("Enter the reason this is unavailable below:", "Comment", Comment);
                if (prompt.DialogResult.HasValue && (bool)prompt.DialogResult)
                {
                    Comment = prompt.Input;
                }
            }

            private void OnRemoveComment()
            {
                Comment = null;
            }
        }

        #endregion

        #region DependencyProperty: DefaultSpanLocation

        public ColoredViewModel DefaultSpanLocation
        {
            get { return GetValue(DefaultSpanLocationProperty) as ColoredViewModel; }
            set { SetValue(DefaultSpanLocationProperty, value); }
        }

        public static readonly DependencyProperty DefaultSpanLocationProperty = DependencyProperty.Register("DefaultSpanLocation", typeof(ColoredViewModel), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty: SpanSize

        public TimeSpan? SpanSize
        {
            get { return GetValue(SpanSizeProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(SpanSizeProperty, value); }
        }

        public static readonly DependencyProperty SpanSizeProperty = DependencyProperty.Register("SpanSize", typeof(TimeSpan?), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertySpanSizeChanged));

        private static void OnPropertySpanSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ScheduleTemplateBuilderCustomRadScheduleViewBehavior)d;
            if (behavior.SpanSize.HasValue && behavior.AssociatedObject != null)
            {
                ((DayViewDefinition)behavior.AssociatedObject.ActiveViewDefinition).MinorTickLength = new FixedTickProvider(new DateTimeInterval(behavior.SpanSize.Value.Minutes, 0, 0, 0, 0));
            }
        }

        #endregion

        #region DependencyProperty: AreaStart

        public TimeSpan? AreaStart
        {
            get { return GetValue(AreaStartProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(AreaStartProperty, value); }
        }

        public static readonly DependencyProperty AreaStartProperty = DependencyProperty.Register("AreaStart", typeof(TimeSpan?), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertyAreaStartChanged));

        private static void OnPropertyAreaStartChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ScheduleTemplateBuilderCustomRadScheduleViewBehavior)d;
            if (behavior.AreaStart.HasValue && behavior.AssociatedObject != null)
            {
                behavior.AssociatedObject.ActiveViewDefinition.DayStartTime = behavior.AreaStart.Value;
                if (behavior.AssociatedObject.ActiveViewDefinition.DayStartTime == behavior.AssociatedObject.ActiveViewDefinition.DayEndTime) behavior.AssociatedObject.ActiveViewDefinition.DayEndTime += TimeSpan.FromSeconds(1);

                behavior.UpdateTimeRulerExtents();
            }
        }

        #endregion

        #region DependencyProperty: AreaEnd

        public TimeSpan? AreaEnd
        {
            get { return GetValue(AreaEndProperty).IfNotNull(x => (TimeSpan)x); }
            set { SetValue(AreaEndProperty, value); }
        }

        public static readonly DependencyProperty AreaEndProperty = DependencyProperty.Register("AreaEnd", typeof(TimeSpan?), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior), new PropertyMetadata(new TimeSpan?(), OnPropertyAreaEndChanged));

        private static void OnPropertyAreaEndChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ScheduleTemplateBuilderCustomRadScheduleViewBehavior)d;
            if (behavior.AreaEnd.HasValue && behavior.AssociatedObject != null)
            {
                behavior.AssociatedObject.ActiveViewDefinition.DayEndTime = behavior.AreaEnd.Value;

                if (behavior.AssociatedObject.ActiveViewDefinition.DayStartTime == behavior.AssociatedObject.ActiveViewDefinition.DayEndTime) behavior.AssociatedObject.ActiveViewDefinition.DayEndTime += TimeSpan.FromSeconds(1);

                behavior.UpdateTimeRulerExtents();
            }
        }

        #endregion

        #region DependencyProperty: ScheduleBlocks

        public ObservableCollection<ScheduleBlockViewModel> ScheduleBlocks
        {
            get { return GetValue(ScheduleBlocksProperty) as ObservableCollection<ScheduleBlockViewModel>; }
            set { SetValue(ScheduleBlocksProperty, value); }
        }

        public static readonly DependencyProperty ScheduleBlocksProperty = DependencyProperty.Register("ScheduleBlocks", typeof(ICollection<ScheduleBlockViewModel>), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior), new PropertyMetadata(new ObservableCollection<ScheduleBlockViewModel>(), OnPropertyScheduleBlocksChanged));

        private static void OnPropertyScheduleBlocksChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (ScheduleTemplateBuilderCustomRadScheduleViewBehavior)d;

            var oldCollectionChanged = e.OldValue as INotifyPropertyChanged;
            if (oldCollectionChanged != null)
            {
                oldCollectionChanged.PropertyChanged -= WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.SetSourcesFromProperty);
            }

            var newCollectionChanged = e.NewValue as INotifyPropertyChanged;
            if (newCollectionChanged != null)
            {
                newCollectionChanged.PropertyChanged += WeakDelegate.MakeWeak<PropertyChangedEventHandler>(behavior.SetSourcesFromProperty);
            }

            if (e.NewValue != null) behavior.SetSourcesFromProperty(behavior, null);
        }

        private void SetSourcesFromProperty(object sender, PropertyChangedEventArgs e)
        {
            if (e == null || e.PropertyName != "Item.IsSelected")
            {
                var allCategories = ScheduleBlocks.SelectMany(b => b.Categories.Select((c, i) => new { ScheduleBlock = b, CategoryIndex = i, Category = c })).ToArray();

                _listOfTelerikAppointment.RemoveRangeWithSinglePropertyChangedNotification(_listOfTelerikAppointment.Where(a => !allCategories.Any(g => g.ScheduleBlock == a.ScheduleBlock & g.Category == a.CategoryViewModel && g.CategoryIndex == a.CategoryIndex)));

                _listOfTelerikAppointment.AddRangeWithSinglePropertyChangedNotification(allCategories.Where(c => !_listOfTelerikAppointment.Any(a => a.ScheduleBlock == c.ScheduleBlock && a.CategoryViewModel == c.Category && a.CategoryIndex == c.CategoryIndex))
                    .Select(c => new CustomAppointment(c.Category, c.CategoryIndex, c.ScheduleBlock, UseRightMouseButtonForScheduleBlockSelection.GetValueOrDefault())));
            }

            if (AssociatedObject != null && (e == null || e.PropertyName == "Count" || e.PropertyName == "Item.IsSelected"))
            {
                AssociatedObject.SpecialSlotsSource = ScheduleBlocks.Select(sb => new CustomSlot(sb)).ToExtendedObservableCollection();

                // reset ruler
                var styleSelector = AssociatedObject.SpecialSlotStyleSelector;
                AssociatedObject.SpecialSlotStyleSelector = null;
                AssociatedObject.SpecialSlotStyleSelector = styleSelector;
                AssociatedObject.UpdateLayout();
            }

            if (e == null || e.PropertyName == "Item.Location" || e.PropertyName == "Count")
            {
                ScheduleBlocksByStartTime = ScheduleBlocks.ToDictionary(b => b.StartTime);
            }
        }

        #endregion


        #region DependencyProperty: ScheduleBlocksByStartTime

        public IDictionary<TimeSpan, ScheduleBlockViewModel> ScheduleBlocksByStartTime
        {
            get { return GetValue(ScheduleBlocksByStartTimeProperty) as IDictionary<TimeSpan, ScheduleBlockViewModel>; }
            set { SetValue(ScheduleBlocksByStartTimeProperty, value); }
        }

        public static readonly DependencyProperty ScheduleBlocksByStartTimeProperty = DependencyProperty.Register("ScheduleBlocksByStartTime", typeof(IDictionary<TimeSpan, ScheduleBlockViewModel>), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior));

        #endregion

        #region DependencyProperty: UseRightMouseButtonForScheduleBlockSelection

        public bool? UseRightMouseButtonForScheduleBlockSelection
        {
            get { return GetValue(UseRightMouseButtonForScheduleBlockSelectionProperty) as bool?; }
            set { SetValue(UseRightMouseButtonForScheduleBlockSelectionProperty, value); }
        }

        public static readonly DependencyProperty UseRightMouseButtonForScheduleBlockSelectionProperty = DependencyProperty.Register("UseRightMouseButtonForScheduleBlockSelection", typeof(bool?), typeof(ScheduleTemplateBuilderCustomRadScheduleViewBehavior));

        #endregion

        protected override void OnAttached()
        {
            base.OnAttached();
            SetDayViewDefinition();
            SetTelerikSlotMouseOverHighlight();
            SetHideToolTip();
            SetDisableAppointmentDragDrop();
            SetHiddenGroupHeader();
            SetSpecialTelerikSlotColor();
            SetAppointmentStyle();
            SetAppointmentContent();
            SetTimeRulerBackgroundColor();
            AssociatedObject.IsInlineEditingEnabled = false;
            AssociatedObject.NavigationHeaderVisibility = Visibility.Collapsed;
            AssociatedObject.AppointmentsSource = _listOfTelerikAppointment;

            AssociatedObject.ShowDialog += WeakDelegate.MakeWeak<EventHandler<ShowDialogEventArgs>>(ShowDialogHandler);
            AssociatedObject.AppointmentDeleted += WeakDelegate.MakeWeak<EventHandler<AppointmentDeletedEventArgs>>(AppointmentDeletedHandler);
            AssociatedObject.MouseLeftButtonUp += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseLeftButtonUp);
            AssociatedObject.MouseRightButtonUp += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseRightButtonUp);
            AssociatedObject.PreviewMouseRightButtonDown += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseRightButtonDown);
            AssociatedObject.PreviewMouseLeftButtonDown += WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseLeftButtonDown);

            _selectedSlotNotifier = new PropertyChangeNotifier(AssociatedObject, ScheduleViewBase.SelectedSlotProperty)
                .AddValueChanged(OnSelectedSlotChanged);
        }

        private void OnSelectedSlotChanged(object sender, EventArgs e)
        {
            if (AssociatedObject.SelectedSlot == null || SpanSize == null) return;

            var isDragging = (AssociatedObject.SelectedSlot.End - AssociatedObject.SelectedSlot.Start) > SpanSize.Value;

            var currentTime = AssociatedObject.SelectedSlot.Start;
            while (currentTime < AssociatedObject.SelectedSlot.End)
            {
                ScheduleBlockViewModel scheduleBlock;
                if (ScheduleBlocksByStartTime.TryGetValue(currentTime.TimeOfDay, out scheduleBlock))
                {
                    if (isDragging)
                    {
                        scheduleBlock.IsSelected = true;
                    }
                    else
                    {
                        scheduleBlock.IsSelected = !scheduleBlock.IsSelected;
                    }
                }

                currentTime += SpanSize.Value;
            }
        }

        private void UpdateTimeRulerExtents()
        {
            const int appointmentPixelHeight = 25;

            var extent = AssociatedObject.ActualHeight;
            int appointmentCount = 0;
            if (AreaStart.HasValue && AreaEnd.HasValue && SpanSize.HasValue)
            {
                appointmentCount = Convert.ToInt32(((AreaEnd.Value - AreaStart.Value).TotalMinutes) / SpanSize.Value.TotalMinutes);
            }

            extent = Math.Max(extent, appointmentPixelHeight * appointmentCount);

            AssociatedObject.ActiveViewDefinition.MinTimeRulerExtent = extent;
        }

        private void OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (UseRightMouseButtonForScheduleBlockSelection.GetValueOrDefault())
            {
                foreach (var sb in ScheduleBlocks) sb.IsSelected = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var source = e.OriginalSource as Visual;
            if (UseRightMouseButtonForScheduleBlockSelection.GetValueOrDefault() && !(source is Microsoft.Windows.Themes.ScrollChrome) &&
                (source == null || (source.ParentOfType<Button>() == null && source.ParentOfType<System.Windows.Controls.Primitives.ScrollBar>() == null)))
            {
                e.Handled = true;
            }
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            AssociatedObject.SelectedSlot = null;
        }

        private void OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            AssociatedObject.SelectedSlot = null;

            if (UseRightMouseButtonForScheduleBlockSelection.GetValueOrDefault())
            {
                SelectAppointment(e.OriginalSource as FrameworkElement);

                var scheduleBlock = ScheduleBlocks.FirstOrDefault(s => s.IsSelected);

                if (scheduleBlock == null) return;

                ShowInsertCategoryView();

                ScheduleBlocks.Where(s => s.IsSelected).ForEachWithSinglePropertyChangedNotification(s => s.IsSelected = false);
            }
            else if (ScheduleBlocks != null && ScheduleBlocks.Any(s => s.IsSelected))
            {
                ShowInsertCategoryView();
            }
        }

        private static void SelectAppointment(FrameworkElement element)
        {
            if (element == null) return;

            var appointmentItem = element.DataContext as AppointmentItemProxy;
            if (appointmentItem == null) return;

            var appointment = appointmentItem.Appointment as CustomAppointment;
            if (appointment == null) return;

            appointment.ScheduleBlock.IsSelected = true;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            RemoveDayViewDefinition();
            RemoveTelerikSlotMouseOverHighlight();
            RemoveHideToolTip();
            RemoveDisableAppointmentDragDrop();
            RemoveHiddenGroupHeader();
            RemoveSpecialTelerikSlotColor();
            RemoveAppointmentStyle();
            RemoveAppointmentContent();
            RemoveTimeRulerBackgroundColor();
            AssociatedObject.IsInlineEditingEnabled = true;
            AssociatedObject.NavigationHeaderVisibility = Visibility.Visible;
            AssociatedObject.SpecialSlotsSource = null;
            AssociatedObject.AppointmentsSource = null;

            AssociatedObject.ShowDialog -= WeakDelegate.MakeWeak<EventHandler<ShowDialogEventArgs>>(ShowDialogHandler);
            AssociatedObject.AppointmentDeleted -= WeakDelegate.MakeWeak<EventHandler<AppointmentDeletedEventArgs>>(AppointmentDeletedHandler);
            AssociatedObject.MouseLeftButtonUp -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseLeftButtonUp);
            AssociatedObject.MouseRightButtonUp -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseRightButtonUp);
            AssociatedObject.PreviewMouseRightButtonDown -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseRightButtonDown);
            AssociatedObject.PreviewMouseLeftButtonDown -= WeakDelegate.MakeWeak<MouseButtonEventHandler>(OnMouseLeftButtonDown);

            _selectedSlotNotifier.RemoveValueChanged(OnSelectedSlotChanged);
        }

        #region OnAttachedHelpers

        private void SetDayViewDefinition()
        {
            var dayViewDefinition = new DayViewDefinition();
            if (AreaStart.HasValue) dayViewDefinition.DayStartTime = AreaStart.Value;
            if (AreaEnd.HasValue) dayViewDefinition.DayEndTime = AreaEnd.Value;
            dayViewDefinition.ShowAllDayArea = true;
            if (SpanSize.HasValue) dayViewDefinition.MinorTickLength = new FixedTickProvider(new DateTimeInterval(SpanSize.Value.Minutes, 0, 0, 0, 0));
            dayViewDefinition.MajorTickLength = new FixedTickProvider(new DateTimeInterval(0, 1, 0, 0, 0));
            AssociatedObject.ViewDefinitions.Add(dayViewDefinition);
        }

        private void SetTelerikSlotMouseOverHighlight()
        {
            _selectionHighlightStyle = AssociatedObject.SelectionHighlightStyle;
            _mouseOverHighlightStyle = AssociatedObject.MouseOverHighlightStyle;
            var style = new Style();
            style.TargetType = typeof(HighlightItem);
            style.Setters.Add(new Setter(Control.BackgroundProperty, new SolidColorBrush(Colors.Transparent)));
            AssociatedObject.SelectionHighlightStyle = style;
            AssociatedObject.MouseOverHighlightStyle = style;
        }

        private void SetHideToolTip()
        {
            _defaultToolTipTemplate = AssociatedObject.ToolTipTemplate;
            AssociatedObject.ToolTipTemplate = null;
        }

        private void SetDisableAppointmentDragDrop()
        {
            _dragDropBehavior = AssociatedObject.DragDropBehavior;
            AssociatedObject.DragDropBehavior = new AppointmentDragDropBehavior();
        }

        private void SetHiddenGroupHeader()
        {
            _groupHeaderStyleSelector = AssociatedObject.GroupHeaderStyleSelector;
            var style = new Style();
            style.TargetType = typeof(GroupHeader);
            style.Setters.Add(new Setter(FrameworkElement.HeightProperty, (double)0));
            var orientedGroupHeaderStyleSelector = new GroupHeaderStyleSelector();
            orientedGroupHeaderStyleSelector.GroupHeaderStyle = style;
            AssociatedObject.GroupHeaderStyleSelector = orientedGroupHeaderStyleSelector;
        }

        private void SetSpecialTelerikSlotColor()
        {
            _specialTelerikSlotStyleSelector = AssociatedObject.SpecialSlotStyleSelector;
            var color = AssociatedObject.FindResource("StandardFocusBrush") as Brush;

            AssociatedObject.SpecialSlotStyleSelector = new SpecialTelerikSlotStyleSelector(color);
        }

        private void SetAppointmentStyle()
        {
            _appointmentStyleSelector = AssociatedObject.AppointmentStyleSelector;
            var appointmentStyleSelector = new AppointmentStyleSelector
                {
                    AppointmentStyle = (Style)_resources["ScheduleBlockSelectableAppointmentStyle"]
                };
            AssociatedObject.AppointmentStyleSelector = appointmentStyleSelector;
        }

        private void SetAppointmentContent()
        {
            _appointmentItemContentTemplateSelector = AssociatedObject.AppointmentItemContentTemplateSelector;
            AssociatedObject.AppointmentItemContentTemplateSelector = new AppointmentItemContentTemplateSelector((DataTemplate)_resources["AppointmentScheduleBlockToggleTemplate"]);
        }

        private void SetTimeRulerBackgroundColor()
        {
            _timeRulerStyleSelector = AssociatedObject.TimeRulerItemStyleSelector;
            var timeRulerItemStyleSelector = new CustomRadSchedulerTimeRulerItemStyleSelector(this);
            AssociatedObject.TimeRulerItemStyleSelector = timeRulerItemStyleSelector;
        }

        #endregion

        #region OnDetachingHelpers

        private void RemoveDayViewDefinition()
        {
            AssociatedObject.ViewDefinitions.RemoveAll();
        }

        private void RemoveTelerikSlotMouseOverHighlight()
        {
            AssociatedObject.SelectionHighlightStyle = _selectionHighlightStyle;
            AssociatedObject.MouseOverHighlightStyle = _mouseOverHighlightStyle;
        }

        private void RemoveHideToolTip()
        {
            AssociatedObject.ToolTipTemplate = _defaultToolTipTemplate;
        }

        private void RemoveDisableAppointmentDragDrop()
        {
            AssociatedObject.DragDropBehavior = _dragDropBehavior;
        }

        private void RemoveHiddenGroupHeader()
        {
            AssociatedObject.GroupHeaderStyleSelector = _groupHeaderStyleSelector;
        }

        private void RemoveSpecialTelerikSlotColor()
        {
            AssociatedObject.SpecialSlotStyleSelector = _specialTelerikSlotStyleSelector;
        }

        private void RemoveAppointmentStyle()
        {
            AssociatedObject.AppointmentStyleSelector = _appointmentStyleSelector;
        }

        private void RemoveAppointmentContent()
        {
            AssociatedObject.AppointmentItemContentTemplateSelector = _appointmentItemContentTemplateSelector;
        }

        private void RemoveTimeRulerBackgroundColor()
        {
            AssociatedObject.TimeRulerItemStyleSelector = _timeRulerStyleSelector;
        }

        #endregion

        #region HelpersVariables

        private static Style _selectionHighlightStyle;
        private static Style _mouseOverHighlightStyle;
        private static DataTemplate _defaultToolTipTemplate;
        private static ScheduleViewDragDropBehavior _dragDropBehavior;
        private static ScheduleViewStyleSelector _groupHeaderStyleSelector;
        private static ScheduleViewStyleSelector _specialTelerikSlotStyleSelector;
        private static ScheduleViewStyleSelector _appointmentStyleSelector;
        private static ScheduleViewDataTemplateSelector _appointmentItemContentTemplateSelector;
        private static ScheduleViewStyleSelector _timeRulerStyleSelector;
        private readonly ResourceDictionary _resources = new ResourceDictionary { Source = new Uri("IO.Practiceware.Presentation;component/Views/ScheduleTemplateBuilder/ScheduleTemplateBuilderViewResources.xaml", UriKind.Relative) };
        private PropertyChangeNotifier _selectedSlotNotifier;

        #endregion

        #region HelpersInnerClasses

        // Used to disable appointment drag and drop
        private class AppointmentDragDropBehavior : ScheduleViewDragDropBehavior
        {
            public override bool CanDrop(DragDropState state)
            {
                return false;
            }

            public override bool CanStartDrag(DragDropState state)
            {
                return false;
            }

            public override bool CanResize(DragDropState state)
            {
                return false;
            }

            public override bool CanStartResize(DragDropState state)
            {
                return false;
            }
        }

        // Used to fix Today 's digit and name showing in view area
        private class GroupHeaderStyleSelector : OrientedGroupHeaderStyleSelector
        {
            public Style GroupHeaderStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDeifinition)
            {
                var groupHeader = container as GroupHeader;
                if (groupHeader != null && groupHeader.GroupKey is DateTime) return GroupHeaderStyle;
                return base.SelectStyle(item, container, activeViewDeifinition);
            }
        }

        // Used to set telerik slot to specific color
        private class SpecialTelerikSlotStyleSelector : ScheduleViewStyleSelector
        {
            private readonly Brush _isSelectedBrush;

            public SpecialTelerikSlotStyleSelector(Brush isSelectedBrush)
            {
                _isSelectedBrush = isSelectedBrush;
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                var style = new Style();
                style.TargetType = typeof(HighlightItem);

                style.Setters.Add(new Setter(Control.BackgroundProperty, new SolidColorBrush(Colors.Transparent)));
                style.Setters.Add(new Setter(Control.BorderBrushProperty, new SolidColorBrush(Colors.Transparent)));

                var isSelectedBinding = new Binding("Slot.ScheduleBlock.IsSelected");
                var isSelectedTrigger = new DataTrigger { Binding = isSelectedBinding, Value = true };
                isSelectedTrigger.Setters.Add(new Setter(Control.BackgroundProperty, _isSelectedBrush));
                isSelectedTrigger.Setters.Add(new Setter(Control.BorderBrushProperty, _isSelectedBrush));
                style.Triggers.Add(isSelectedTrigger);

                return style;
            }
        }

        // Used to set appointment to specific color 
        private class AppointmentStyleSelector : OrientedAppointmentItemStyleSelector
        {
            public Style AppointmentStyle
            {
                [UsedImplicitly]
                get;
                set;
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                var appointmentItem = container as AppointmentItem;
                var customAppointment = item as CustomAppointment;
                if (appointmentItem != null)
                {
                    if (customAppointment != null)
                    {
                        return AppointmentStyle;
                    }
                }
                return base.SelectStyle(item, container, activeViewDefinition);
            }
        }

        // Used to enable inline commenting on appointment
        private class AppointmentItemContentTemplateSelector : ScheduleViewDataTemplateSelector
        {
            private readonly DataTemplate _template;

            public AppointmentItemContentTemplateSelector(DataTemplate template)
            {
                _template = template;
            }

            public override DataTemplate SelectTemplate(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                var occurrence = item as Occurrence;
                if (occurrence == null) return base.SelectTemplate(item, container, activeViewDefinition);

                return _template;
            }
        }

        // Used to customize color of various ruler sections
        private class CustomRadSchedulerTimeRulerItemStyleSelector : OrientedTimeRulerItemStyleSelector
        {
            private readonly ScheduleTemplateBuilderCustomRadScheduleViewBehavior _behavior;
            private readonly ColorToBrushConverter _colorToBrushConverter;
            private readonly ColorToContrastColorConverter _colorToContrastColorConverter;
            private readonly HasValueToVisibilityConverter _hasValueToVisibilityConverter;
            private readonly IDictionary<TimeSpan, Style> _stylesByStartTime;

            public CustomRadSchedulerTimeRulerItemStyleSelector(ScheduleTemplateBuilderCustomRadScheduleViewBehavior behavior)
            {
                _behavior = behavior;
                _colorToBrushConverter = new ColorToBrushConverter();
                _colorToContrastColorConverter = new ColorToContrastColorConverter();
                _hasValueToVisibilityConverter = new HasValueToVisibilityConverter();
                _stylesByStartTime = new Dictionary<TimeSpan, Style>();
            }

            public override Style SelectStyle(object item, DependencyObject container, ViewDefinitionBase activeViewDefinition)
            {
                var style = base.SelectStyle(item, container, activeViewDefinition);
                if (style == null)
                {
                    var tickData = (TickData)item;
                    var timeRulerItem = container as TimeRulerItemBase;

                    if (timeRulerItem != null && timeRulerItem.IsVisible && timeRulerItem.IsEnabled &&
                        _behavior.AreaStart <= tickData.DateTime.TimeOfDay && _behavior.AreaEnd >= tickData.DateTime.TimeOfDay && !_stylesByStartTime.TryGetValue(tickData.DateTime.TimeOfDay, out style))
                    {
                        var colorConverter = new TimeOfDayToLocationColorConverter(tickData.DateTime.TimeOfDay);
                        var nameConverter = new TimeOfDayToLocationNameConverter(tickData.DateTime.TimeOfDay);

                        style = new Style(typeof(Control));

                        style.Setters.Add(new Setter(Control.BackgroundProperty, new Binding("ScheduleBlocksByStartTime") { Converter = new ValueConverterGroup { Converters = { colorConverter, _colorToBrushConverter } }, Source = _behavior }));
                        style.Setters.Add(new Setter(Control.ForegroundProperty, new Binding("ScheduleBlocksByStartTime") { Converter = new ValueConverterGroup { Converters = { colorConverter, _colorToContrastColorConverter } }, Source = _behavior }));
                        style.Setters.Add(new Setter(Control.PaddingProperty, new Thickness(0)));
                        style.Setters.Add(new Setter(Control.FontSizeProperty , (double) 12));
                        style.Setters.Add(new Setter(Control.FontFamilyProperty, new FontFamily("Segoe UI")));
                        style.Setters.Add(new Setter(FrameworkElement.MarginProperty, new Thickness(0, 0, 0, -1)));

                        var toolTip = new ToolTip();
                        toolTip.SetBinding(ContentControl.ContentProperty, new Binding("ScheduleBlocksByStartTime") { Source = _behavior, Converter = nameConverter });
                        toolTip.SetBinding(UIElement.VisibilityProperty, new Binding("ScheduleBlocksByStartTime") { Converter = new ValueConverterGroup { Converters = { nameConverter, _hasValueToVisibilityConverter } }, Source = _behavior });
                        ToolTipService.SetBetweenShowDelay(container, 100);
                        ToolTipService.SetInitialShowDelay(container, 100);
                        style.Setters.Add(new Setter(FrameworkElement.ToolTipProperty, toolTip));

                        _stylesByStartTime[tickData.DateTime.TimeOfDay] = style;
                    }
                }

                return style;
            }

            private class TimeOfDayToLocationColorConverter : IValueConverter
            {
                private readonly TimeSpan _timeOfDay;

                public TimeOfDayToLocationColorConverter(TimeSpan timeOfDay)
                {
                    _timeOfDay = timeOfDay;
                }

                public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
                {
                    var blocks = value as IDictionary<TimeSpan, ScheduleBlockViewModel>;
                    if (blocks == null) return null;
                    ScheduleBlockViewModel block;
                    if (blocks.TryGetValue(_timeOfDay, out block))
                    {
                        return block.Location.Color;
                    }
                    return null;
                }

                public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
                {
                    throw new NotSupportedException();
                }
            }

            private class TimeOfDayToLocationNameConverter : IValueConverter
            {
                private readonly TimeSpan _timeOfDay;

                public TimeOfDayToLocationNameConverter(TimeSpan timeOfDay)
                {
                    _timeOfDay = timeOfDay;
                }

                public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
                {
                    var blocks = value as IDictionary<TimeSpan, ScheduleBlockViewModel>;
                    if (blocks == null) return null;
                    ScheduleBlockViewModel block;
                    if (blocks.TryGetValue(_timeOfDay, out block))
                    {
                        return block.Location.Name;
                    }
                    return null;
                }

                public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
                {
                    throw new NotSupportedException();
                }
            }
        }

        #endregion

        #region EventHandlers

        private static void ShowDialogHandler(object sender, ShowDialogEventArgs e)
        {
            if (e.DialogViewModel is AppointmentDialogViewModel) e.Cancel = true;
            if (e.DialogViewModel is ConfirmDialogViewModel)
            {
                e.DefaultDialogResult = true;
                e.Cancel = true;
            }
        }

        private void AppointmentDeletedHandler(object sender, AppointmentDeletedEventArgs e)
        {
            var scheduleBlock = ScheduleBlocks.First(s => s.StartTime == e.Appointment.Start.TimeOfDay && s.EndTime == e.Appointment.End.TimeOfDay);
            var category = scheduleBlock.Categories.First(sc => sc.Name.Equals(e.Appointment.Subject, StringComparison.OrdinalIgnoreCase));
            scheduleBlock.Categories.Remove(category);
        }

        #endregion

        private void ShowInsertCategoryView()
        {
            var view = new ScheduleTemplateBuilderInsertCategoryView();
            var context = (ScheduleTemplateBuilderInsertCategoryViewContext)view.DataContext;
            context.ScheduleBlocks = ScheduleBlocks;
            var window = new WindowInteractionArguments();
            window.Content = view;
            window.IsModal = true;
            window.Owner = AssociatedObject;
            window.WindowStartupLocation = WindowStartupLocation.MousePosition;
            window.WindowFrameStyle = WindowFrameStyle.None;
            InteractionManager.Current.ShowModal(window);
        }

    }
}