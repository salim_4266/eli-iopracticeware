﻿using System;

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public class ScheduleTemplateBuilderSelectAllSelector : ScheduleTemplateBuilderMinuteIncrementSelector
    {
        public ScheduleTemplateBuilderSelectAllSelector()
        {
            IsChecked = false;
            DisplayName = "Select All";
            Increment = TimeSpan.MaxValue;
        }
    }
}