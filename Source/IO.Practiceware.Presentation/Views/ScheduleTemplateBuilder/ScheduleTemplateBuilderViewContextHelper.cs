﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

[assembly: Component(typeof(ScheduleTemplateBuilderViewContextHelper), typeof(IScheduleTemplateBuilderViewContextHelper))]

namespace IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder
{
    public interface IScheduleTemplateBuilderViewContextHelper
    {
        /// <summary>
        /// Initial list of schedule blocks
        /// </summary>
        /// <param name="startTime">Start of range</param>
        /// <param name="endTime">End of range</param>
        /// <param name="spanSize">Smallest unit of schedulable time</param>
        /// <param name="defaultSpanLocation">The default location for schedule blocks</param>
        /// <returns>List of initial schedule blocks</returns>
        List<ScheduleBlockViewModel> GenerateScheduleBlocks(TimeSpan startTime, TimeSpan endTime, TimeSpan spanSize, ColoredViewModel defaultSpanLocation);

        /// <summary>
        /// Get the list of time increment that can be bulk selected in UI
        /// </summary>
        /// <param name="spanSize">The smallest unit of schedulable time</param>
        /// <returns>Smallest unit of schedulable time</returns>
        ObservableCollection<ScheduleTemplateBuilderMinuteIncrementSelector> LoadMinuteIncrementSelector(TimeSpan spanSize);
    }

    internal class ScheduleTemplateBuilderViewContextHelper : IScheduleTemplateBuilderViewContextHelper
    {
        private readonly Func<ScheduleBlockViewModel> _createScheduleBlockViewModel;
        private readonly Func<ScheduleTemplateBuilderDeselectAllSelector> _createScheduleTemplateBuilderDeselectAllSelector;
        private readonly Func<ScheduleTemplateBuilderSelectAllSelector> _createScheduleTemplateBuilderSelectAllSelector;
        private readonly Func<ScheduleTemplateBuilderMinuteIncrementSelector> _createScheduleTemplateBuilderMinuteIncrementSelector;

        public ScheduleTemplateBuilderViewContextHelper(Func<ScheduleBlockViewModel> createScheduleBlockViewModel,
                                                        Func<ScheduleTemplateBuilderDeselectAllSelector> createScheduleTemplateBuilderDeselectAllSelector,
                                                        Func<ScheduleTemplateBuilderSelectAllSelector> createScheduleTemplateBuilderSelectAllSelector,
                                                        Func<ScheduleTemplateBuilderMinuteIncrementSelector> createScheduleTemplateBuilderMinuteIncrementSelector)
        {
            _createScheduleBlockViewModel = createScheduleBlockViewModel;
            _createScheduleTemplateBuilderDeselectAllSelector = createScheduleTemplateBuilderDeselectAllSelector;
            _createScheduleTemplateBuilderSelectAllSelector = createScheduleTemplateBuilderSelectAllSelector;
            _createScheduleTemplateBuilderMinuteIncrementSelector = createScheduleTemplateBuilderMinuteIncrementSelector;
        }

        public List<ScheduleBlockViewModel> GenerateScheduleBlocks(TimeSpan startTime, TimeSpan endTime, TimeSpan spanSize, ColoredViewModel defaultSpanLocation)
        {
            var today = DateTime.Today;
            var startDateTime = today.Add(startTime);
            var endDateTime = today.Add(endTime);
            var result = new List<ScheduleBlockViewModel>();

            while (startDateTime < endDateTime)
            {
                var scheduleBlockViewModel = _createScheduleBlockViewModel();
                scheduleBlockViewModel.StartTime = startDateTime.Add(new TimeSpan(0, 0, 0)).TimeOfDay;
                scheduleBlockViewModel.EndTime = scheduleBlockViewModel.StartTime.Add(spanSize);
                scheduleBlockViewModel.Location = defaultSpanLocation;
                scheduleBlockViewModel.Categories = new ExtendedObservableCollection<CategoryViewModel>();
                result.Add(scheduleBlockViewModel);
                startDateTime = startDateTime.Add(spanSize);
                scheduleBlockViewModel.CastTo<IChangeTracking>().AcceptChanges();
            }

            return result;
        }

        public ObservableCollection<ScheduleTemplateBuilderMinuteIncrementSelector> LoadMinuteIncrementSelector(TimeSpan spanSize)
        {
            var none = _createScheduleTemplateBuilderDeselectAllSelector();
            var all = _createScheduleTemplateBuilderSelectAllSelector();
            var incrementList = new List<ScheduleTemplateBuilderMinuteIncrementSelector> { none, all };

            var chunkStartAt = new TimeSpan(0, 0, 0);
            var chunkEndAt = new TimeSpan(1, 0, 0);
            while (chunkStartAt < chunkEndAt)
            {
                var entry = _createScheduleTemplateBuilderMinuteIncrementSelector();
                entry.IsChecked = false;
                entry.DisplayName = "*:" + chunkStartAt.ToString("mm");
                entry.Increment = chunkStartAt;
                incrementList.Add(entry);
                chunkStartAt = chunkStartAt.Add(spanSize);
            }

            return incrementList.ToExtendedObservableCollection();
        }

    }

    internal class CustomScheduleBlockViewModelComparer : IEqualityComparer<ScheduleBlockViewModel>
    {
        public bool Equals(ScheduleBlockViewModel x, ScheduleBlockViewModel y)
        {
            bool commentEqual = !(x.Comment == null && y.Comment != null);
            if (y.Comment == null && x.Comment != null) commentEqual = false;
            if (x.Comment != null && y.Comment != null)
            {
                commentEqual = x.Comment.Equals(y.Comment, StringComparison.OrdinalIgnoreCase);
            }

            var locationEqual = x.Location.Id == y.Location.Id && x.Location.Name.Equals(y.Location.Name, StringComparison.OrdinalIgnoreCase) && x.Location.Color.ToString().Equals(y.Location.Color.ToString(), StringComparison.OrdinalIgnoreCase);
            var xExcept = x.Categories.Except(y.Categories, new CustomCategoryViewModelComparer()).ToList();
            var yExcept = y.Categories.Except(x.Categories, new CustomCategoryViewModelComparer()).ToList();
            var temp = x.StartTime == y.StartTime && x.EndTime == y.EndTime && locationEqual && commentEqual && xExcept.Count == yExcept.Count;
            return temp;
        }

        public int GetHashCode(ScheduleBlockViewModel obj)
        {
            var commentHashCode = obj.Comment == null ? 0 : obj.Comment.GetHashCode();
            var locationHashCode = obj.Location.Id.GetHashCode() ^ obj.Location.Name.GetHashCode() ^ obj.Location.Color.ToString().GetHashCode();
            var customCategoryViewModelComparer = new CustomCategoryViewModelComparer();
            var categoriesHashCode = 0;
            foreach (var category in obj.Categories)
            {
                var categoryHashCode = customCategoryViewModelComparer.GetHashCode(category);
                categoriesHashCode = categoriesHashCode ^ categoryHashCode;
            }
            return obj.StartTime.GetHashCode() ^ obj.EndTime.GetHashCode() ^ locationHashCode ^ categoriesHashCode ^ commentHashCode;
        }
    }

    internal class CustomLocationViewModelComparer : IEqualityComparer<ColoredViewModel>
    {
        public bool Equals(ColoredViewModel x, ColoredViewModel y)
        {
            var temp = x.Id == y.Id && x.Name.Equals(y.Name, StringComparison.OrdinalIgnoreCase) && x.Color.ToString().Equals(y.Color.ToString(), StringComparison.OrdinalIgnoreCase);
            return temp;
        }

        public int GetHashCode(ColoredViewModel obj)
        {
            return obj.Id.GetHashCode() ^ obj.Name.GetHashCode() ^ obj.Color.ToString().GetHashCode();
        }
    }

    internal class CustomCategoryViewModelComparer : IEqualityComparer<CategoryViewModel>
    {
        public bool Equals(CategoryViewModel x, CategoryViewModel y)
        {
            var temp = x.Id == y.Id && x.Name.Equals(y.Name, StringComparison.OrdinalIgnoreCase) && x.Color.ToString().Equals(y.Color.ToString(), StringComparison.OrdinalIgnoreCase);
            return temp;
        }

        public int GetHashCode(CategoryViewModel obj)
        {
            return obj.Id.GetHashCode() ^ obj.Name.GetHashCode() ^ obj.Color.ToString().GetHashCode();
        }
    }
}