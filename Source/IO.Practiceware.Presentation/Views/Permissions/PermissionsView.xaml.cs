﻿using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Permissions;
using IO.Practiceware.Presentation.ViewServices.Permissions;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Permissions
{
    /// <summary>
    /// Interaction logic for PermissionsView.xaml
    /// </summary>
    public partial class PermissionsView
    {
        public PermissionsView()
        {
            InitializeComponent();
        }
    }

    public class PermissionsViewContext : IViewContext
    {
        private readonly IPermissionsViewService _viewService;
        public IInteractionContext InteractionContext { get; set; }

        public PermissionsViewContext(IPermissionsViewService viewService)
        {
            _viewService = viewService;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            SelectPermission = Command.Create<UserPermissionViewModel>(ExecuteSelectPermission).Async(() => InteractionContext);
            SelectGroup = Command.Create<string>(ExecuteSelectGroup).Async(() => InteractionContext);
            DeselectGroup = Command.Create<string>(ExecuteDeSelectGroup).Async(() => InteractionContext);
        }

        private void ExecuteDeSelectGroup(string groupName)
        {
            if (string.IsNullOrEmpty(groupName)) return;

            foreach (UserPermissionViewModel userPermission in UserPermissions.Where(userPermission => userPermission.Category == groupName))
            {
                userPermission.IsSelected = false;
                _viewService.SavePermission(userPermission);
            }
        }

        private void ExecuteSelectGroup(string groupName)
        {
            if (string.IsNullOrEmpty(groupName)) return;

            foreach (UserPermissionViewModel userPermission in UserPermissions.Where(userPermission => userPermission.Category == groupName))
            {
                userPermission.IsSelected = true;
                _viewService.SavePermission(userPermission);
            }
        }

        private void ExecuteSelectPermission(UserPermissionViewModel userPermission)
        {
            _viewService.SavePermission(userPermission);
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(true);
        }

        private void ExecuteLoad()
        {
            var loadInformation = _viewService.GetLoadInformation(LoadArguments.UserId);
            User = loadInformation.User;
            UserPermissions = loadInformation.UserPermissions;
            if(User == null)
            {
                ValidationWarningTrigger = true;
            }
        }

        public ICommand Load { get; protected set; }
        public ICommand Close { get; protected set; }
        public ICommand SelectPermission { get; protected set; }
        public ICommand SelectGroup { get; protected set; }
        public ICommand DeselectGroup { get; protected set; }

        public PermissionLoadArguments LoadArguments { get; set; }

        [DispatcherThread]
        public virtual NamedViewModel User { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<UserPermissionViewModel> UserPermissions { get; set; }

        [DispatcherThread]
        public virtual bool ValidationWarningTrigger { get; set; }
    }
}