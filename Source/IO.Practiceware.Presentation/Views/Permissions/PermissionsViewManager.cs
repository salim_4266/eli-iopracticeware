﻿using Soaf;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Permissions
{
    public class PermissionsViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public PermissionsViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowPermissionsView(PermissionLoadArguments loadArguments)
        {
            var view = new PermissionsView();
            var dataContext= view.DataContext.EnsureType<PermissionsViewContext>();

            if(loadArguments!=null) dataContext.LoadArguments=loadArguments;

            _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = view,
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.CanResizeWithGrip
                });
        }
    }

    public class PermissionLoadArguments
    {
        public int UserId { get; set; }
    }
}
