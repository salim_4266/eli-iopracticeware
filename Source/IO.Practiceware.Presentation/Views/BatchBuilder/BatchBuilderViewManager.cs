﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    [Singleton]
    public class BatchBuilderViewManager
    {
        public static string SavedBatchTemplateName;
        private readonly IInteractionManager _interactionManager;

        public BatchBuilderViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public event EventHandler<BatchBuilderSelectedEventArgs> RequestingOpenBatchBuilderPatient;

        public void ShowBatchBuilder()
        {
            var view = new BatchBuilder();
            SavedBatchTemplateName = string.Empty;
            _interactionManager.ShowModal(new WindowInteractionArguments
                                              {
                                                  Content = view,
                                                  WindowState = WindowState.Maximized,
                                                  ResizeMode = ResizeMode.CanResizeWithGrip
                                              });
        }

        public void ShowBatchBuilder(string savedBatchTemplateName)
        {
            var view = new BatchBuilder();
            SavedBatchTemplateName = savedBatchTemplateName;
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Maximized,
                ResizeMode = ResizeMode.CanResizeWithGrip
            });
        }

        internal void OnRequestingOpenBatchBuilderPatient(int patientId)
        {
            if (RequestingOpenBatchBuilderPatient != null)
            {
                RequestingOpenBatchBuilderPatient(this, new BatchBuilderSelectedEventArgs(patientId));
            }
            else
            {
                MessageBox.Show("Handler is null");
            }
        }
    }
}