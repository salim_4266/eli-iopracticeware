﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    ///   Interaction logic for BatchBuilderActionsView.xaml
    /// </summary>
    public partial class BatchBuilderActionsView
    {
        public BatchBuilderActionsView()
        {
            InitializeComponent();
        }
    }

    public class BatchBuilderActionsViewContext : IViewContext
    {
        private readonly IBatchBuilderActionsViewService _batchBuilderActionsViewService;
        private IEnumerable<BatchBuilderActionsGroupViewModel> _appointmentActions;
        private IEnumerable<BatchBuilderResultViewModel> _currentBatchResults;
        private IEnumerable<BatchBuilderActionsGroupViewModel> _patientActions;
        private BatchBuilderSearchType _selectedSearchType;

        public BatchBuilderActionsViewContext(IBatchBuilderActionsViewService batchBuilderActionsViewService)
        {
            _batchBuilderActionsViewService = batchBuilderActionsViewService;

            _currentBatchResults = _selectedSearchType.CreateEmptyResults();

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);

            TransmitConfirmations =
                Command.Create(ExecuteTransmitConfirmations, CanTransmitConfirmations).Async(() => InteractionContext);
        }

        private BatchBuilderConfirmationsActionsGroupViewModel ConfirmationsActionsGroup
        {
            get
            {
                return ActionGroups == null
                           ? null
                           : ActionGroups.OfType<BatchBuilderConfirmationsActionsGroupViewModel>().FirstOrDefault();
            }
        }

        public ICommand Load { get; protected set; }

        ///<summary>
        ///  command to Transmit selected Confirmations
        ///</summary>
        public ICommand TransmitConfirmations { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<BatchBuilderActionsGroupViewModel> ActionGroups { get; set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private bool CanTransmitConfirmations()
        {
            return ConfirmationsActionsGroup.IfNotNull(i => i.TextMessageAction.IsSelected || i.EmailAction.IsSelected)
                   && _currentBatchResults.IsNotNullOrEmpty();
        }

        private void ExecuteLoad()
        {
            InteractionContext.Messenger.Subscribe<BatchBuilderCurrentBatchResultsMessage>((sender, token, message) =>
                                                                                               {
                                                                                                   _currentBatchResults = message.CurrentBatchResults;
                                                                                                   _selectedSearchType = message.SearchType;
                                                                                               });
            InteractionContext.Messenger.Subscribe<BatchBuilderSelectedSearchTypeMessage>(
               (sender, token, message) =>
               {
                   _selectedSearchType = message.SearchType;
                   OnBatchBuilderSearchTypeChanged();
               });

            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateSavingMessage>(
                (sender, token, message) => { message.Template.ClickedActions = GetClickedActions(); });

            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (sender, token, message) =>
                {
                    if (message.Template.ClickedActions != null && message.Template.ClickedActions.Any())
                        SetClickedActions(message.Template.ClickedActions);
                    else ResetClickedActionstoDefault();
                });

            InteractionContext.Messenger.Subscribe<BatchBuilderResetSelectedTemplateMessage>(
                (sender, token, message) => ResetClickedActionstoDefault());

            _patientActions = _batchBuilderActionsViewService.GetPatientActions();

            _appointmentActions = _batchBuilderActionsViewService.GetAppointmentActions();

            ActionGroups =
                (_selectedSearchType == BatchBuilderSearchType.Patient ? _patientActions : _appointmentActions).
                    ToExtendedObservableCollection();
        }

        private void OnBatchBuilderSearchTypeChanged()
        {
            ActionGroups =
               (_selectedSearchType == BatchBuilderSearchType.Patient ? _patientActions : _appointmentActions).
                   ToExtendedObservableCollection();
        }

        private void SetClickedActions(IEnumerable<BatchBuilderActionViewModel> actions)
        {
            foreach (BatchBuilderActionViewModel action in actions)
            {
                var confirmationAction = action as BatchBuilderConfirmationActionViewModel;
                if (confirmationAction != null) SetClickedConfirmationAction(confirmationAction);
            }
        }

        private void SetClickedConfirmationAction(BatchBuilderConfirmationActionViewModel action)
        {
            BatchBuilderConfirmationActionViewModel existingAction =
                ConfirmationsActionsGroup.IfNotNull(g => g.Actions.OfType<BatchBuilderConfirmationActionViewModel>()
                                                             .FirstOrDefault(
                                                                 a => a.ConfirmationType == action.ConfirmationType));

            if (existingAction != null)
            {
                existingAction.IsSelected = action.IsSelected;
                if (existingAction.Templates.Contains(action.SelectedTemplate))
                    existingAction.SelectedTemplate = action.SelectedTemplate;
            }
        }

        private void ResetClickedActionstoDefault()
        {
            if (ActionGroups != null)
            {
                foreach (
                    BatchBuilderActionViewModel action in
                        ActionGroups.SelectMany(a => a.Actions).Where(a => a.IsSelected).ToExtendedObservableCollection())
                {
                    var confirmationAction = action as BatchBuilderConfirmationActionViewModel;
                    if (confirmationAction != null) ResetClickedConfirmationAction(confirmationAction);
                }
            }
        }

        private void ResetClickedConfirmationAction(BatchBuilderConfirmationActionViewModel action)
        {
            BatchBuilderConfirmationActionViewModel existingAction =
                ConfirmationsActionsGroup.IfNotNull(g => g.Actions.OfType<BatchBuilderConfirmationActionViewModel>()
                                                             .FirstOrDefault(
                                                                 a => a.ConfirmationType == action.ConfirmationType));

            if (existingAction != null)
            {
                existingAction.IsSelected = false;
                existingAction.SelectedTemplate = null;
            }
        }

        private ObservableCollection<BatchBuilderActionViewModel> GetClickedActions()
        {
            return ActionGroups.SelectMany(a => a.Actions).Where(a => a.IsSelected).ToExtendedObservableCollection();
        }

        private void ExecuteTransmitConfirmations()
        {
            if (ConfirmationsActionsGroup != null)
            {
                foreach (
                    BatchBuilderActionViewModel action in ConfirmationsActionsGroup.Actions.Where(a => a.IsSelected))
                {
                    switch (_selectedSearchType)
                    {
                        case BatchBuilderSearchType.Appointment:
                            _batchBuilderActionsViewService.PerformAppointmentAction(
                                _currentBatchResults.Select(r => r.AppointmentId).ToArray(), action);
                            break;
                        case BatchBuilderSearchType.Patient:
                            _batchBuilderActionsViewService.PerformPatientAction(
                                _currentBatchResults.Select(r => r.PatientId).ToArray(), action);
                            break;
                    }
                }
            }
        }
    }
}