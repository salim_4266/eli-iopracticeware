﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    /// Interaction logic for BatchBuilderTemplateView.xaml
    /// </summary>
    public partial class BatchBuilderTemplateView
    {
        public BatchBuilderTemplateView()
        {
            InitializeComponent();
        }
    }

    public class BatchBuilderTemplateViewContext : IViewContext
    {
        private readonly IBatchBuilderTemplatesViewService _batchBuilderTemplatesViewService;

        public BatchBuilderTemplateViewContext(IBatchBuilderTemplatesViewService batchBuilderTemplatesViewService)
        {
            _batchBuilderTemplatesViewService = batchBuilderTemplatesViewService;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            ChangeBatchTemplate = Command.Create(OnBatchTemplateSelectionChanged);
        }

        [DispatcherThread]
        public virtual ObservableCollection<BatchBuilderTemplateViewModel> BatchBuilderTemplates { get; set; }

        public virtual BatchBuilderTemplateViewModel SelectedBatchBuilderTemplate { get; set; }

        public ICommand Load { get; protected set; }

        public ICommand ChangeBatchTemplate { get; protected set; }

        public bool AutoRun { get; set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            InteractionContext.Messenger.Subscribe<BatchBuilderReloadTemplatesMessage>(
                (sender, token, message) => OnSaveBatchBuilderTemplate(message));
            InteractionContext.Messenger.Subscribe<BatchBuilderResetSelectedTemplateMessage>(
                (sender, token, message) => SelectedBatchBuilderTemplate = message.Template);
            BatchBuilderTemplates = _batchBuilderTemplatesViewService.GetBatchBuilderTemplates().ToExtendedObservableCollection();
            if (BatchBuilderTemplates != null) BatchBuilderTemplates = BatchBuilderTemplates.Where(b => b.SearchType == BatchBuilderSearchType.Patient).ToExtendedObservableCollection();
            InteractionContext.Messenger.Publish(new BatchBuilderDefaultTemplatesMessage
                                                     {Templates = BatchBuilderTemplates});
            if (!BatchBuilderViewManager.SavedBatchTemplateName.IsNullOrEmpty())
                SelectedBatchBuilderTemplate = GetAutoRunTemplate(BatchBuilderViewManager.SavedBatchTemplateName);
            InteractionContext.Messenger.Subscribe<BatchBuilderSelectedSearchTypeMessage>(
                (sender, token, message) => ReloadBatchBuilderTemplatesOnSearchType(message.SearchType));
        }

        private void ReloadBatchBuilderTemplatesOnSearchType(BatchBuilderSearchType searchType)
        {
            BatchBuilderTemplates = _batchBuilderTemplatesViewService.GetBatchBuilderTemplates().Where(b => b.SearchType == searchType).ToExtendedObservableCollection();
            InteractionContext.Messenger.Publish(new BatchBuilderDefaultTemplatesMessage {Templates = BatchBuilderTemplates});
        }

        private BatchBuilderTemplateViewModel GetAutoRunTemplate(string templateName)
        {
            BatchBuilderTemplates = _batchBuilderTemplatesViewService.GetBatchBuilderTemplates().ToExtendedObservableCollection();
            BatchBuilderTemplateViewModel template = BatchBuilderTemplates.FirstOrDefault(bt => bt.Name == templateName);
            if (template != null && BatchBuilderTemplates != null) BatchBuilderTemplates = BatchBuilderTemplates.Where(b => b.SearchType == template.SearchType).ToExtendedObservableCollection();
            AutoRun = template != null;
            return template;
        }

        private void OnSaveBatchBuilderTemplate(BatchBuilderReloadTemplatesMessage message)
        {
            BatchBuilderTemplates = message.Templates.ToExtendedObservableCollection();
            SelectedBatchBuilderTemplate = message.SelectedTemplate;
        }

        private void OnBatchTemplateSelectionChanged()
        {
            if (SelectedBatchBuilderTemplate != null)
                InteractionContext.Messenger.Publish(new BatchBuilderTemplateLoadingMessage
                                                         {
                                                             AutoRun = AutoRun,
                                                             Template = SelectedBatchBuilderTemplate
                                                         });
            AutoRun = false;
        }
    }
}