﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    /// Interaction logic for BatchBuilderResultsView.xaml
    /// </summary>
    public partial class BatchBuilderResultsView
    {
        public BatchBuilderResultsView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the BatchBuilderResultsView.
    /// </summary>
    public class BatchBuilderResultsViewContext : IViewContext
    {
        private readonly BatchBuilderViewManager _batchBuilderViewManager;

        public BatchBuilderResultsViewContext(BatchBuilderViewManager batchBuilderViewManager)
        {
            _batchBuilderViewManager = batchBuilderViewManager;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            SelectAll = Command.Create(ExecuteSelectAll, () => Results.Any());
            AddToBatch = Command.Create(AddSelectedPrimaryResultsToCurrentBatch, () => SelectedResults.Any());
            RemoveSelectedBatch = Command.Create(RemoveSelectedCurrentBatch, () => CurrentBatchSelectedResults.Any());
            ClearBatch = Command.Create(ClearCurrentBatch, () => CurrentBatchResults.Any());
            SearchResultSelection = Command.Create(OnSearchResultSelectionChanged);
            CurrentBatchSelection = Command.Create(OnCurrentBatchSelectionChanged);

            PrimaryVisibleColumns = typeof(BatchBuilderResultViewModel).GetProperties().Select(i => i.Name).ToExtendedObservableCollection();
            CurrentBatchVisibleColumns = typeof(BatchBuilderResultViewModel).GetProperties().Select(i => i.Name).ToExtendedObservableCollection();

            SetDefaultResults();
        }

        public ICommand Load { get; protected set; }

        /// <summary>
        /// Columns paired with boolean visibility indicator.
        /// </summary>
        public virtual ObservableCollection<string> PrimaryVisibleColumns { get; set; }

        public virtual ObservableCollection<string> CurrentBatchVisibleColumns { get; set; }


        /// <summary>
        ///  command to selects all results by pressing this button
        /// </summary>
        /// 
        public ICommand SelectAll { get; protected set; }

        /// <summary>
        ///  command to Add Selected search results to Current Batch
        /// </summary>
        /// 
        public ICommand AddToBatch { get; set; }

        /// <summary>
        ///  command to Remove Selected Batch results
        /// </summary>
        /// 
        public ICommand RemoveSelectedBatch { get; set; }

        /// <summary>
        ///  command to Clears total Current Batch results
        /// </summary>
        /// 
        public ICommand ClearBatch { get; set; }

        /// <summary>
        /// Double-click command on search results to go to patient demographics screen
        /// </summary>
        /// 
        public ICommand SearchResultSelection { get; set; }

        /// <summary>
        /// Double-click command on Current Batch to go to patient demographics screen
        /// </summary>
        /// 
        public ICommand CurrentBatchSelection { get; set; }

        public virtual BatchBuilderSearchType SelectedSearchType { get; set; }

        /// <summary>
        ///   Gets or sets Appointments Results.
        /// </summary>
        /// 
        [DispatcherThread]
        public virtual IEnumerable<BatchBuilderResultViewModel> Results { get; protected set; }

        /// <summary>
        ///   Gets or sets selected Appointments in the search results.
        /// </summary>
        /// <value> The Appointments. </value>
        [DispatcherThread]
        public virtual IEnumerable<BatchBuilderResultViewModel> SelectedResults { get; protected set; }

        /// <summary>
        ///   Gets or sets a value of CurrentBatch Results.
        /// </summary>
        /// 
        [DispatcherThread]
        public virtual IEnumerable<BatchBuilderResultViewModel> CurrentBatchResults { get; protected set; }

        /// <summary>
        ///   Gets or sets selected CurrentBatch Results.
        /// </summary>
        /// <value> The Appointments. </value>
        [DispatcherThread]
        public virtual IEnumerable<BatchBuilderResultViewModel> CurrentBatchSelectedResults { get; protected set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void SetDefaultResults()
        {
            Results = SelectedSearchType.CreateEmptyResults();
            SelectedResults = SelectedSearchType.CreateEmptyResults();
            CurrentBatchResults = SelectedSearchType.CreateEmptyResults();
            CurrentBatchSelectedResults = SelectedSearchType.CreateEmptyResults();
        }

        private void OnSearchResultSelectionChanged()
        {
            if (SelectedResults.Any())
                _batchBuilderViewManager.OnRequestingOpenBatchBuilderPatient(SelectedResults.First().PatientId);
        }

        private void OnCurrentBatchSelectionChanged()
        {
            if (CurrentBatchSelectedResults.Any())
                _batchBuilderViewManager.OnRequestingOpenBatchBuilderPatient(CurrentBatchSelectedResults.First().PatientId);
        }

        /// <summary>
        /// Executes the load.
        /// </summary>
        private void ExecuteLoad()
        {
            InteractionContext.Messenger.Subscribe<BatchBuilderResultsMessage>((sender, token, message) =>
                                                                                   {
                                                                                       Results = message.Results;
                                                                                       SelectedSearchType = message.SearchType;
                                                                                   });
            InteractionContext.Messenger.Subscribe<BatchBuilderSelectedSearchTypeMessage>(
                (sender, token, message) =>
                {
                    SelectedSearchType = message.SearchType;
                    SetDefaultResults();
                });
            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateSavingMessage>((sender, token, message) =>
                                                                                          {
                                                                                              message.Template.PrimaryGridColumns = PrimaryVisibleColumns;
                                                                                              message.Template.CurrentBatchGridColumns = CurrentBatchVisibleColumns;
                                                                                          });

            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>((sender, token, message) =>
                                                                                           {
                                                                                               if (message.Template !=
                                                                                                   null)
                                                                                               {
                                                                                                   AddPrimaryVisibleColumns
                                                                                                       (
                                                                                                           message.
                                                                                                               Template.
                                                                                                               PrimaryGridColumns
                                                                                                               );
                                                                                                   AddCurrentBatchVisibleColumns
                                                                                                       (
                                                                                                           message.
                                                                                                               Template.
                                                                                                               CurrentBatchGridColumns);
                                                                                               }
                                                                                           });
        }

        private void ExecuteSelectAll()
        {
            SelectedResults = Results.ToExtendedObservableCollection();
        }

        private void AddSelectedPrimaryResultsToCurrentBatch()
        {
            CurrentBatchResults = !CurrentBatchResults.Any() ? SelectedResults.ToExtendedObservableCollection() : CurrentBatchResults.CastTo<IList>().AddRangeWithSinglePropertyChangedNotification(SelectedResults.ToExtendedObservableCollection()).OfType<BatchBuilderResultViewModel>();
            if (SelectedSearchType == BatchBuilderSearchType.Appointment)
            {
                if (CurrentBatchResults != null)
                    CurrentBatchResults =
                        CurrentBatchResults.Distinct(new BatchBuilderResultViewModelAppointmentComparer()).
                            ToExtendedObservableCollection();
            }
            else
                CurrentBatchResults =
                    CurrentBatchResults.Distinct(new BatchBuilderResultViewModelPatientComparer()).
                        ToExtendedObservableCollection();
            InteractionContext.Messenger.Publish(new BatchBuilderCurrentBatchResultsMessage
                                                     {
                                                         CurrentBatchResults = CurrentBatchResults,
                                                         SearchType = SelectedSearchType
                                                     });
        }

        private void RemoveSelectedCurrentBatch()
        {
            foreach (BatchBuilderResultViewModel result in CurrentBatchSelectedResults.ToExtendedObservableCollection())
            {
                BatchBuilderResultViewModel selectedResult = result;
                if (CurrentBatchResults.FirstOrDefault(c => c == selectedResult) !=
                    null)
                {
                    CurrentBatchResults.CastTo<IList>().Remove(selectedResult);
                    InteractionContext.Messenger.Publish(new BatchBuilderCurrentBatchResultsMessage
                                                             {
                                                                 CurrentBatchResults = CurrentBatchResults,
                                                                 SearchType = SelectedSearchType
                                                             });
                }
            }
            CurrentBatchSelectedResults.CastTo<IList>().Clear();
        }

        private void ClearCurrentBatch()
        {
            CurrentBatchResults.CastTo<IList>().Clear();
            InteractionContext.Messenger.Publish(new BatchBuilderCurrentBatchResultsMessage
                                                     {
                                                         CurrentBatchResults = CurrentBatchResults,
                                                         SearchType = SelectedSearchType
                                                     });
        }

        private void AddPrimaryVisibleColumns(ObservableCollection<String> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (items.Count > 0)
            {
                PrimaryVisibleColumns.Clear();
                foreach (string item in items) if (item != null) PrimaryVisibleColumns.Add(item);
            }
        }

        private void AddCurrentBatchVisibleColumns(ObservableCollection<String> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (items.Count > 0)
            {
                CurrentBatchVisibleColumns.Clear();
                foreach (string item in items) if (item != null) CurrentBatchVisibleColumns.Add(item);
            }
        }
    }
}