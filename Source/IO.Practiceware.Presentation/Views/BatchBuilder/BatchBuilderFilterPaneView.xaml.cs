﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    ///   Interaction logic for BatchBuilderFilterPaneView.xaml
    /// </summary>
    public partial class BatchBuilderFilterPaneView
    {
        public BatchBuilderFilterPaneView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    ///   The root view model (the view context) for the BatchBuilderFilterPaneView.
    /// </summary>
    public class BatchBuilderFilterPaneViewContext : IViewContext
    {
        private readonly IBatchBuilderFiltersViewService _batchBuilderFiltersViewService;
        private readonly IBatchBuilderResultsViewService _batchBuilderResultsViewService;
        private readonly IBatchBuilderTemplatesViewService _batchBuilderTemplatesViewService;

        public BatchBuilderFilterPaneViewContext(IBatchBuilderFiltersViewService batchBuilderFiltersViewService,
                                                 IBatchBuilderResultsViewService batchBuilderResultsViewService,
                                                 IBatchBuilderTemplatesViewService batchBuilderTemplatesViewService)
        {
            _batchBuilderResultsViewService = batchBuilderResultsViewService;
            _batchBuilderTemplatesViewService = batchBuilderTemplatesViewService;
            _batchBuilderFiltersViewService = batchBuilderFiltersViewService;

            AvailableFilters = GetAllAvailableFilters();
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            SelectedFilters = new ObservableCollection<BatchBuilderFilterViewModel>();
            BatchBuilderTemplates = new ObservableCollection<BatchBuilderTemplateViewModel>();
            AddFilter = Command.Create<BatchBuilderFilterViewModel>(f => AddSelectedFilter(f));
            RemoveFilter = Command.Create<BatchBuilderFilterViewModel>(RemoveSelectedFilter);
            Clear = Command.Create(ClearSelectedFilters, () => SelectedFilters.Count > 0);
            Run = Command.Create(RunFilters, () => SelectedFilters.Count > 0).Async(() => InteractionContext,
                                                                                    supportsCancellation: true);
            SaveBatchTemplate =
                Command.Create(ExecuteSaveBatchTemplate, () => BatchBuilderTemplateName.IsNotNullOrEmpty()).Async(
                    () => InteractionContext);

            RemoveBatchTemplate =
                Command.Create(ExecuteRemoveBatchBuilderTemplate, () => SelectedBatchBuilderTemplate != null).Async(
                    () => InteractionContext);
        }

        [DispatcherThread]
        public virtual ObservableCollection<BatchBuilderFilterViewModel> SelectedFilters { get; set; }

        public virtual ObservableCollection<BatchBuilderFilterGroupViewModel> AvailableFilters { get; set; }
        public ObservableCollection<BatchBuilderDropDownFilterViewModel> DropDownFilters { get; set; }
        public virtual string BatchBuilderTemplateName { get; set; }

        [DispatcherThread]
        public virtual IEnumerable<BatchBuilderTemplateViewModel> BatchBuilderTemplates { get; set; }

        public BatchBuilderSearchType SelectedSearchType { get; set; }

        public ICommand Load { get; protected set; }

        // Filter add / Remove commands
        public ICommand AddFilter { get; protected set; }
        public ICommand RemoveFilter { get; protected set; }

        // Filter Run Options
        public ICommand Clear { get; protected set; }
        public ICommand Run { get; protected set; }

        // Save Batch Templates
        public ICommand SaveBatchTemplate { get; protected set; }

        // Remove Batch Templates
        public ICommand RemoveBatchTemplate { get; protected set; }

        [DependsOn("BatchBuilderTemplateName")]
        public virtual bool TemplateAlreadyExists
        {
            get { return BatchBuilderTemplates.Any(t => t.Name == BatchBuilderTemplateName); }
        }

        public BatchBuilderTemplateViewModel SelectedBatchBuilderTemplate { get; set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            InteractionContext.Messenger.Subscribe<BatchBuilderDefaultTemplatesMessage>(
                (sender, token, message) => BatchBuilderTemplates = message.Templates);
            InteractionContext.Messenger.Subscribe<BatchBuilderSelectedSearchTypeMessage>(
                (sender, token, message) =>
                {
                    SelectedSearchType = message.SearchType;
                    OnBatchBuilderSearchTypeChanged();
                });
            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (sender, token, message) => OnBatchTemplateSelectionChanged(message.AutoRun, message.Template));
            InteractionContext.Messenger.Subscribe<BatchBuilderSelectedSearchTypeMessage>(
                (sender, token, message) => OnBatchBuilderSearchTypeChanged());
            DropDownFilters = _batchBuilderFiltersViewService.GetAllDropDownFilterChoices(SelectedSearchType);
        }

        private void OnBatchBuilderSearchTypeChanged()
        {
            ClearSelectedFilters();
            AvailableFilters = GetAllAvailableFilters();
            DropDownFilters = _batchBuilderFiltersViewService.GetAllDropDownFilterChoices(SelectedSearchType);
        }

        private BatchBuilderFilterViewModel AddSelectedFilter(BatchBuilderFilterViewModel addFilter)
        {
            // Creating a filter 
            var filter = (BatchBuilderFilterViewModel)Activator.CreateInstance(addFilter.GetType(), new[] { addFilter.Name, addFilter.GroupName });

            if (filter is BatchBuilderDropDownFilterViewModel)
            {
                filter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices =
                    DropDownFilters.First(d => d.Name == addFilter.Name).Choices;
            }
            SelectedFilters.Add(filter);

            return filter;
        }

        private void RemoveSelectedFilter(BatchBuilderFilterViewModel filter)
        {
            SelectedFilters.Remove(filter);
        }

        private void ClearSelectedFilters()
        {
            SelectedFilters.Clear();
            // clears Filter Name Field
            BatchBuilderTemplateName = "";
            // clears Batch builder selected template
            InteractionContext.Messenger.Publish(new BatchBuilderResetSelectedTemplateMessage { Template = null });
            // clears Search Results 
            InteractionContext.Messenger.Publish(new BatchBuilderResultsMessage
                                                     {
                                                         Results = SelectedSearchType.CreateEmptyResults(),
                                                         SearchType = SelectedSearchType
                                                     });
        }

        private void RunFilters()
        {
            var filters = (IEnumerable<BatchBuilderFilterViewModel>)System.Windows.Application.Current.Dispatcher.Invoke(new Func<object>(() => GetDistinctFilters(SelectedFilters)));

            IEnumerable<BatchBuilderResultViewModel> results =
                _batchBuilderResultsViewService.GetBatchResults(filters, SelectedSearchType);

            InteractionContext.Messenger.Publish(new BatchBuilderResultsMessage { Results = results, SearchType = SelectedSearchType });
        }

        private ObservableCollection<BatchBuilderFilterViewModel> GetDistinctFilters(IEnumerable<BatchBuilderFilterViewModel> selectedFilters)
        {
            var filters = new ObservableCollection<BatchBuilderFilterViewModel>();
            filters.Clear();
            if (selectedFilters != null)
            {
                foreach (BatchBuilderFilterViewModel selectedFilter in SelectedFilters)
                {
                    if (filters.Count == 0)
                        AddNewFilter(filters, selectedFilter);
                    else
                    {
                        BatchBuilderFilterViewModel filter = filters.FirstOrDefault(f => f.Name == selectedFilter.Name);
                        if (filter != null)
                        {
                            UpdateExistingFilter(filter, selectedFilter);
                        }
                        else
                            AddNewFilter(filters, selectedFilter);
                    }
                }
            }
            return filters;
        }

        private static void UpdateExistingFilter(BatchBuilderFilterViewModel filter, BatchBuilderFilterViewModel selectedFilter)
        {
            if (selectedFilter is BatchBuilderTextFilterViewModel && selectedFilter.CastTo<BatchBuilderTextFilterViewModel>().Text.IsNotNullOrEmpty())
            {
                if (filter.CastTo<BatchBuilderTextFilterViewModel>().Text.IsNotNullOrEmpty())
                    filter.CastTo<BatchBuilderTextFilterViewModel>().Text += ";" + selectedFilter.CastTo<BatchBuilderTextFilterViewModel>().Text;
                else
                    filter.CastTo<BatchBuilderTextFilterViewModel>().Text = selectedFilter.CastTo<BatchBuilderTextFilterViewModel>().Text;
            }
            else if (selectedFilter is BatchBuilderDropDownFilterViewModel && selectedFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.Any())
            {
                foreach (string selectedChoice in selectedFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.ToArray())
                {
                    // append selected choices of same filter type
                    if (!filter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.Contains(selectedChoice))
                        filter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.Add(selectedChoice);
                }
            }
            else if (selectedFilter is BatchBuilderDateRangeFilterViewModel && (selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().Start != null || selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().End != null))
            {
                filter.CastTo<BatchBuilderDateRangeFilterViewModel>().DateRanges.Add(new DateRange(selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().Start, selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().End));
            }
        }

        private static void AddNewFilter(ObservableCollection<BatchBuilderFilterViewModel> filters, BatchBuilderFilterViewModel selectedFilter)
        {
            var newFilter = (BatchBuilderFilterViewModel)Activator.CreateInstance(selectedFilter.GetType(), new[] { selectedFilter.Name, selectedFilter.GroupName });
            if (selectedFilter is BatchBuilderDropDownFilterViewModel)
            {
                // set choices since they're not saved as part of the template       
                newFilter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices =
                    selectedFilter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices.ToExtendedObservableCollection();
                if (selectedFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices != null)
                {
                    newFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices = selectedFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices;
                    foreach (string selectedChoice in newFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.ToArray())
                    {
                        // available choices may have changed
                        if (!newFilter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices.Contains(selectedChoice))
                            newFilter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.Remove(selectedChoice);
                    }
                }
            }
            else if (selectedFilter is BatchBuilderTextFilterViewModel)
                newFilter.CastTo<BatchBuilderTextFilterViewModel>().Text = selectedFilter.CastTo<BatchBuilderTextFilterViewModel>().Text;
            else if (selectedFilter is BatchBuilderDateRangeFilterViewModel)
            {
                newFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().Start = selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().Start;
                newFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().End = selectedFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().End;
                newFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().DateRanges.Add(new DateRange(newFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().Start, newFilter.CastTo<BatchBuilderDateRangeFilterViewModel>().End));
            }
            filters.Add(newFilter);
        }

        private ObservableCollection<BatchBuilderFilterGroupViewModel> GetAllAvailableFilters()
        {
            ObservableCollection<BatchBuilderFilterGroupViewModel> filters = null;
            switch (SelectedSearchType)
            {
                case BatchBuilderSearchType.Patient:
                    var batchBuilderPatientFilters = new BatchBuilderPatientFilterSource();
                    filters = batchBuilderPatientFilters.Filters.ToExtendedObservableCollection()
                        .GroupBy(f => f.GroupName)
                        .Select(g => new BatchBuilderFilterGroupViewModel
                                         {
                                             Name = g.Key,
                                             Filters = g.ToExtendedObservableCollection()
                                         }).ToExtendedObservableCollection();

                    break;
                case BatchBuilderSearchType.Appointment:
                    var batchBuilderAppointmentFilters = new BatchBuilderAppointmentFilterSource();
                    filters = batchBuilderAppointmentFilters.Filters.ToExtendedObservableCollection()
                        .GroupBy(f => f.GroupName)
                        .Select(g => new BatchBuilderFilterGroupViewModel
                                         {
                                             Name = g.Key,
                                             Filters = g.ToExtendedObservableCollection()
                                         }).ToExtendedObservableCollection();
                    break;
            }
            return filters;
        }

        private void ExecuteSaveBatchTemplate()
        {
            // match existing by name and update else insert new 
            BatchBuilderTemplateViewModel template =
                BatchBuilderTemplates.FirstOrDefault(t => t.Name == BatchBuilderTemplateName) ??
                new BatchBuilderTemplateViewModel();
            SaveBatchBuilderTemplate(template);
        }

        private void SaveBatchBuilderTemplate(BatchBuilderTemplateViewModel template)
        {
            template.Name = BatchBuilderTemplateName;
            template.SelectedFilters = SelectedFilters.ToExtendedObservableCollection();
            template.SearchType = SelectedSearchType;
            InteractionContext.Messenger.Publish(new BatchBuilderTemplateSavingMessage { Template = template });

            // save and refetch saved template
            BatchBuilderTemplateViewModel savedTemplate =
                _batchBuilderTemplatesViewService.SaveBatchBuilderTemplate(template);

            List<BatchBuilderTemplateViewModel> templates = BatchBuilderTemplates.ToList();
            templates.Remove(template);
            templates.Add(savedTemplate);
            BatchBuilderTemplates = templates.OrderBy(i => i.Name).ToExtendedObservableCollection();

            BatchBuilderTemplateName = "";

            InteractionContext.Messenger.Publish(new BatchBuilderReloadTemplatesMessage
                                                     {
                                                         Templates = BatchBuilderTemplates,
                                                         SelectedTemplate = savedTemplate
                                                     });
        }

        private void OnBatchTemplateSelectionChanged(bool autoRun, BatchBuilderTemplateViewModel selectedBatchBuilderTemplate)
        {
            SelectedBatchBuilderTemplate = selectedBatchBuilderTemplate;
            if (selectedBatchBuilderTemplate != null && selectedBatchBuilderTemplate.SelectedFilters != null)
            {
                BatchBuilderTemplateName = selectedBatchBuilderTemplate.Name;
                SelectedFilters.Clear();
                foreach (BatchBuilderFilterViewModel filter in selectedBatchBuilderTemplate.SelectedFilters)
                {
                    if (filter is BatchBuilderDropDownFilterViewModel)
                    {
                        // set choices since they're not saved as part of the template       
                        filter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices =
                            DropDownFilters.First(d => d.Name == filter.Name).Choices;
                        if (filter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices != null)
                        {
                            foreach (string selectedChoice in filter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.ToArray())
                            {
                                // available choices may have changed
                                if (!filter.CastTo<BatchBuilderDropDownFilterViewModel>().Choices.Contains(selectedChoice))
                                    filter.CastTo<BatchBuilderDropDownFilterViewModel>().SelectedChoices.Remove(selectedChoice);
                            }
                        }
                        SelectedFilters.Add(filter);
                    }
                    else SelectedFilters.Add(filter);
                }
            }
            if (autoRun) RunFilters();
        }

        private void ExecuteRemoveBatchBuilderTemplate()
        {
            if (SelectedBatchBuilderTemplate != null && BatchBuilderTemplates != null)
            {
                BatchBuilderTemplateViewModel template =
                    BatchBuilderTemplates.FirstOrDefault(t => t.Id == SelectedBatchBuilderTemplate.Id);
                _batchBuilderTemplatesViewService.RemoveBatchBuilderTemplate(template);

                List<BatchBuilderTemplateViewModel> templates = BatchBuilderTemplates.ToList();
                templates.Remove(template);
                BatchBuilderTemplates = templates.OrderBy(i => i.Name).ToExtendedObservableCollection();
                SelectedBatchBuilderTemplate = null;

                System.Windows.Application.Current.Dispatcher.Invoke(ClearSelectedFilters);
                InteractionContext.Messenger.Publish(new BatchBuilderReloadTemplatesMessage
                                                         {
                                                             Templates = BatchBuilderTemplates,
                                                             SelectedTemplate = null
                                                         });
            }
        }
    }
}