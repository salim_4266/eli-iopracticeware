﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    /// Interaction logic for BatchBuilderSearchView.xaml
    /// </summary>
    public partial class BatchBuilderSearchView
    {
        public BatchBuilderSearchView()
        {
            InitializeComponent();
        }
    }

    public class BatchBuilderSearchViewContext : IViewContext
    {
        public virtual BatchBuilderSearchType SearchType { get; protected set; }

        public BatchBuilderSearchViewContext()
        {
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            SelectPatientSearchType = Command.Create(() => SelectSearchType(BatchBuilderSearchType.Patient));
            SelectAppointmentSearchType = Command.Create(() => SelectSearchType(BatchBuilderSearchType.Appointment));
        }

        [DependsOn("SearchType")]
        public virtual bool IsPatientSearchChecked
        {
            get { return SearchType == BatchBuilderSearchType.Patient; }
        }

        [DependsOn("SearchType")]
        public virtual bool IsAppointmentSearchChecked
        {
            get { return SearchType == BatchBuilderSearchType.Appointment; }
        }

        public ICommand Load { get; protected set; }
        public ICommand SelectPatientSearchType { get; protected set; }
        public ICommand SelectAppointmentSearchType { get; protected set; }

        #region IViewContext Members

        public virtual IInteractionContext InteractionContext { get; set; }

        #endregion

        private void ExecuteLoad()
        {
            InteractionContext.Messenger.Subscribe<BatchBuilderTemplateLoadingMessage>(
                (sender, token, message) => { SearchType = message.Template.SearchType; });
        }

        private void SelectSearchType(BatchBuilderSearchType searchType)
        {
            SearchType = searchType;
            InteractionContext.Messenger.Publish(new BatchBuilderSelectedSearchTypeMessage { SearchType = SearchType });
        }
    }
}