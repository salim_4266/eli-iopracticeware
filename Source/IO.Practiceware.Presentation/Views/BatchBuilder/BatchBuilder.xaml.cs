﻿using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using Soaf.Presentation;
using System.Collections.Generic;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    /// <summary>
    /// Interaction logic for BatchBuilder.xaml
    /// </summary>
    public partial class BatchBuilder
    {
        public BatchBuilder()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// Public classes for Interaction between 2 xamls to send data using IMessenger 
    /// </summary>
    public class BatchBuilderResultsMessage
    {
        public IEnumerable<BatchBuilderResultViewModel> Results { get; set; }
        public BatchBuilderSearchType SearchType { get; set; }
    }

    public class BatchBuilderCurrentBatchResultsMessage
    {
        public IEnumerable<BatchBuilderResultViewModel> CurrentBatchResults { get; set; }
        public BatchBuilderSearchType SearchType { get; set; }
    }

    public class BatchBuilderDefaultTemplatesMessage
    {
        public IEnumerable<BatchBuilderTemplateViewModel> Templates { get; set; }
    }

    public class BatchBuilderReloadTemplatesMessage
    {
        public IEnumerable<BatchBuilderTemplateViewModel> Templates { get; set; }
        public BatchBuilderTemplateViewModel SelectedTemplate { get; set; }
    }

    public class BatchBuilderTemplateSavingMessage
    {
        public BatchBuilderTemplateViewModel Template { get; set; }
    }

    public class BatchBuilderTemplateLoadingMessage
    {
        public bool AutoRun { get; set; }
        public BatchBuilderTemplateViewModel Template { get; set; }
    }

    public class BatchBuilderResetSelectedTemplateMessage
    {
        public BatchBuilderTemplateViewModel Template { get; set; }
    }
    
    public class BatchBuilderSelectedSearchTypeMessage
    {
        public BatchBuilderSearchType SearchType { get; set; }
    }

    /// <summary>
    ///   The root view model (the view context) for the BatchBuilderView.
    /// </summary>
    public class BatchBuilderViewContext : IViewContext
    {
        #region IViewContext Members

        /// <summary>
        ///   Gets or sets the interaction context.
        /// </summary>
        /// <value> The interaction context. </value>
        public IInteractionContext InteractionContext { get; set; }

        #endregion
    }
}