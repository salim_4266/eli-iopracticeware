﻿using System;

namespace IO.Practiceware.Presentation.Views.BatchBuilder
{
    public class BatchBuilderSelectedEventArgs : EventArgs
    {
        private readonly int _patientId;

        public BatchBuilderSelectedEventArgs(int patientId)
        {
            _patientId = patientId;
        }

        public int PatientId
        {
            get { return _patientId; }
        }
    }
}