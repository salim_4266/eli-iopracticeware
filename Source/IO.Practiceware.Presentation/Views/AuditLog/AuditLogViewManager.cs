﻿using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.AuditLog
{
    public class AuditLogViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public AuditLogViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowAuditLogView()
        {
            var view = new AuditLogView();
            _interactionManager.ShowModal(new WindowInteractionArguments { Content = view, WindowState = WindowState.Normal, ResizeMode = ResizeMode.CanResizeWithGrip });
        }
    }
}
