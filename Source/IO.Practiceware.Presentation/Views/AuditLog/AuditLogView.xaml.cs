﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Interactivity;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.AuditLog;
using IO.Practiceware.Presentation.ViewServices.AuditLog;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace IO.Practiceware.Presentation.Views.AuditLog
{
    /// <summary>
    /// Interaction logic for AuditLogView.xaml
    /// </summary>
    public partial class AuditLogView
    {
        public AuditLogView()
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// Information about distinct values for a grid view column.
    /// </summary>
    public class AuditLogViewColumnDistinctValues
    {
        public AuditLogViewColumnDistinctValues(string columnName)
        {
            ColumnName = columnName;
        }
        public string ColumnName { get; private set; }

        public IEnumerable ItemsSource { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AuditLogViewColumnDistinctValuesBehavior : Behavior<RadGridView>
    {
        private readonly EventHandler<GridViewDistinctValuesLoadingEventArgs> _onDistinctValuesLoadingHandler;

        /// <summary>
        /// Defines a bound command that accepts an instance of AuditLogViewColumnDistinctValues with an opportunity to provide the ItemsSource.
        /// </summary>
        public static readonly DependencyProperty GetColumnDistinctValuesProperty = DependencyProperty.Register("GetColumnDistinctValues", typeof(ICommand), typeof(AuditLogViewColumnDistinctValuesBehavior));

        /// <summary>
        /// Defines a bound command that accepts an instance of AuditLogViewColumnDistinctValues with an opportunity to provide the ItemsSource.
        /// </summary>
        /// <value>
        /// The get column distinct values.
        /// </value>
        public ICommand GetColumnDistinctValues
        {
            get { return GetValue(GetColumnDistinctValuesProperty) as ICommand; }
            set { SetValue(GetColumnDistinctValuesProperty, value); }
        }

        public AuditLogViewColumnDistinctValuesBehavior()
        {
            _onDistinctValuesLoadingHandler = WeakDelegate.MakeWeak<EventHandler<GridViewDistinctValuesLoadingEventArgs>>(OnDistinctValuesLoading);
        }

        protected override void OnAttached()
        {
            AssociatedObject.DistinctValuesLoading += _onDistinctValuesLoadingHandler;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.DistinctValuesLoading -= _onDistinctValuesLoadingHandler;
            base.OnDetaching();
        }

        private void OnDistinctValuesLoading(object sender, GridViewDistinctValuesLoadingEventArgs e)
        {
            var parameter = new AuditLogViewColumnDistinctValues(e.Column.UniqueName) { ItemsSource = e.ItemsSource };
            var c = GetColumnDistinctValues;
            if (c != null && c.CanExecute(parameter))
            {
                c.Execute(parameter);
                e.ItemsSource = parameter.ItemsSource;
            }
        }
    }

    public class AuditLogViewContext : IViewContext
    {
        private readonly IAuditLogViewService _auditLogViewService;
        private readonly IInteractionManager _interactionManager;
        private int _pageIndex;
        private string _additionalFilters;
        private IEnumerable<string> _accessLocations;
        private IEnumerable<string> _accessDevices;
        private IEnumerable<string> _sourceOfAccess;

        public AuditLogViewContext(IAuditLogViewService auditLogViewService, IInteractionManager interactionManager)
        {
            _auditLogViewService = auditLogViewService;
            _interactionManager = interactionManager;
            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            GetAuditLogs = Command.Create(ExecuteGetAuditLogs, CanGenerate).Async(() => InteractionContext);
            GetPage = Command.Create(ExecuteGetNextPage, () => CanGenerate() && PageIndex != -1).Async(() => InteractionContext);
            Close = Command.Create(ExecuteClose);
            ViewImageContent = Command.Create<AuditLogViewModel>(ExecuteViewImageContent);
            AuditLogs = new ExtendedObservableCollection<AuditLogViewModel>();
            PageSize = 100;
            GetColumnDistinctValues = LoadingWindow.Run(() => Command.Create<AuditLogViewColumnDistinctValues>(ExecuteGetColumnDistinctValues));
        }

        public virtual ICommand GetColumnDistinctValues { get; set; }

        private void ExecuteGetColumnDistinctValues(AuditLogViewColumnDistinctValues o)
        {
            switch (o.ColumnName)
            {
                case "AccessLocation":
                    if (_accessLocations == null) _accessLocations = _auditLogViewService.GetAllAccessLocations();
                    o.ItemsSource = _accessLocations;
                    break;
                case "AccessDevice":
                    if (_accessDevices == null) _accessDevices = _auditLogViewService.GetAllAccessDevices();
                    o.ItemsSource = _accessDevices;
                    break;
                case "SourceOfAccess":
                    if (_sourceOfAccess == null) _sourceOfAccess = _auditLogViewService.GetAllSourceOfAccess();
                    o.ItemsSource = _sourceOfAccess;
                    break;
                default:
                    o.ItemsSource = null;
                    break;
            }
        }

        private void ExecuteViewImageContent(AuditLogViewModel auditLog)
        {
            if (auditLog.ContentType == ContentType.Jpg)
            {
                var bmp = new BitmapImage();
                var binaryImage = SoapHexBinary.Parse(auditLog.NewValue ?? auditLog.OldValue).Value;
                using (var memoryStream = new MemoryStream(binaryImage))
                {
                    bmp.BeginInit();
                    bmp.StreamSource = memoryStream;
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.EndInit();
                }

                _interactionManager.ShowModal(new WindowInteractionArguments
                {
                    Content = new Image { Source = bmp, Stretch = Stretch.None },
                    WindowState = WindowState.Normal,
                    ResizeMode = ResizeMode.NoResize
                });
            }
        }

        private void ExecuteClose()
        {
            InteractionContext.Complete(true);
        }

        private void ExecuteGetAuditLogs()
        {
            AdditionalFilters = null;
            if (PageIndex != 0)
            {
                PageIndex = 0;
            }
            else
            {
                if (GetPage.CanExecute(null)) { ExecuteGetNextPage(); }
            }
            HasPerformedSearch = true;
        }

        private void ExecuteGetNextPage()
        {
            bool updateTotalCount = PageIndex == 0;

            var logInformation = _auditLogViewService.GetAuditLogs(AuditFilter, AdditionalFilters, PageIndex, PageSize, updateTotalCount);

            if (updateTotalCount)
            {
                TotalLogCount = logInformation.TotalCount;
            }
            AuditLogs = logInformation.AuditLogs.ToExtendedObservableCollection();
        }

        private void ExecuteLoad()
        {
            var auditFilter = _auditLogViewService.GetFilterViewModel();
            auditFilter.EndDate = DateTime.Now.ToClientTime().Date;
            AuditFilter = auditFilter;
        }

        private bool CanGenerate()
        {
            return AuditFilter != null && AuditFilter.IsValid();
        }

        /// <summary>
        /// Gets or sets the interaction context
        /// </summary>
        public IInteractionContext InteractionContext { get; set; }

        /// <summary>
        /// Gets or sets the audit log filter
        /// </summary>
        [DispatcherThread]
        public virtual AuditLogFilterViewModel AuditFilter { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<AuditLogViewModel> AuditLogs { get; set; }

        /// <summary>
        /// Gets or sets the additional filters (set by columns in the grid or text in the textbox).
        /// </summary>
        /// <value>
        /// The additional filters.
        /// </value>
        public virtual string AdditionalFilters
        {
            get { return _additionalFilters; }
            set
            {
                bool isChanged = _additionalFilters != value;
                _additionalFilters = value;
                if (isChanged) PageIndex = 0;
            }
        }

        /// <summary>
        /// Gets or sets the current zero-based page index
        /// </summary>
        [DispatcherThread]
        public virtual int PageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value;
                if (GetPage.CanExecute(null)) { GetPage.Execute(); }
            }
        }

        /// <summary>
        /// Gets or sets the size of a page
        /// </summary>
        [DispatcherThread]
        public virtual int PageSize { get; set; }

        [DispatcherThread]
        public virtual long TotalLogCount { get; protected set; }

        /// <summary>
        /// Flag that indicates whether this control has performed a search yet
        /// </summary>
        /// <remarks>
        /// This was added so that the "Perform a search to see the results here" message can disappear on performing a search.
        /// </remarks>
        [DispatcherThread]
        public virtual bool HasPerformedSearch { get; protected set; }

        /// <summary>
        /// Gets a command to load
        /// </summary>
        public ICommand Load { get; protected set; }

        /// <summary>
        /// Gets a command to get audit logs
        /// </summary>
        public ICommand GetAuditLogs { get; protected set; }

        /// <summary>
        /// Gets a command to close
        /// </summary>
        public ICommand Close { get; protected set; }

        /// <summary>
        /// Gets a command to view content
        /// </summary>
        public ICommand ViewImageContent { get; protected set; }

        /// <summary>
        /// Gets a command to get the next page of audit logs
        /// </summary>
        public ICommand GetPage { get; protected set; }
    }
}