﻿using IO.Practiceware.Model;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace IO.Practiceware.Presentation.Views.AuditLog
{
    public class AuditImageButtonVisibilityConverter : IMultiValueConverter
    {
        public ContentType ContentType { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null 
                || values == DependencyProperty.UnsetValue 
                || values.Count() != 2 
                || values.Any(v => v == null 
                || v == DependencyProperty.UnsetValue))
            {
                return Visibility.Collapsed;
            }

            var contentType = (ContentType)values[1];
            return contentType == ContentType ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
