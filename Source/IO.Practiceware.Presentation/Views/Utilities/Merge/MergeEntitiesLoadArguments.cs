﻿using IO.Practiceware.Presentation.ViewModels.Utilities.Merge;

namespace IO.Practiceware.Presentation.Views.Utilities.Merge
{
    public class MergeEntitiesLoadArguments
    {
        public MergeEntityType EntityType { get; set; }
    }
}