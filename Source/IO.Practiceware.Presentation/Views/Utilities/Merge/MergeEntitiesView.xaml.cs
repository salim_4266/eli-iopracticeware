﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Utilities.Merge
{
    /// <summary>
    /// Interaction logic for MergeEntitiesView.xaml
    /// </summary>
    public partial class MergeEntitiesView
    {
        public MergeEntitiesView()
        {
            InitializeComponent();
        }
    }
}
