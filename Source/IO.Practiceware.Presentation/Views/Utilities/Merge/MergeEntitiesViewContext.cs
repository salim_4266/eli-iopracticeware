﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using IO.Practiceware.Presentation.ViewModels.Utilities.Merge;
using IO.Practiceware.Presentation.ViewServices.Utilities.Merge;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.Views.Utilities.Merge
{
    public class MergeEntitiesViewContext : IViewContext
    {
        private readonly IInteractionManager _interactionManager;
        private readonly IMergeEntitiesViewService _viewService;

        public MergeEntitiesViewContext(IInteractionManager interactionManager, IMergeEntitiesViewService viewService)
        {
            _interactionManager = interactionManager;
            _viewService = viewService;

            Merge = Command
                .Create(ExecuteMerge, () => SelectedEntityToUse != null 
                    && SelectedEntityToOverwrite != null
                    && SelectedEntityToUse.Id != SelectedEntityToOverwrite.Id)
                .Async(() => InteractionContext);
            SearchEntityToUse = Command
                .Create(ExecuteSearchEntityToUse, () => !string.IsNullOrEmpty(EntityToUseSearchText))
                .Async();
            SearchEntityToOverwrite = Command
                .Create(ExecuteSearchEntityToOverwrite, () => !string.IsNullOrEmpty(EntityToOverwriteSearchText))
                .Async();

            Closing = Command.Create(() => { });
        }

        private void ExecuteSearchEntityToOverwrite()
        {
            if (string.IsNullOrEmpty(EntityToOverwriteSearchText)) return;

            EntityToOverwriteSelection = _viewService.Find(EntityToOverwriteSearchText, LoadArguments.EntityType).ToList();
        }

        private void ExecuteSearchEntityToUse()
        {
            if (string.IsNullOrEmpty(EntityToUseSearchText)) return;

            EntityToUseSelection = _viewService.Find(EntityToUseSearchText, LoadArguments.EntityType).ToList();
        }

        private void ExecuteMerge()
        {
            if (SelectedEntityToUse == null 
                || SelectedEntityToOverwrite == null
                || SelectedEntityToUse.Id == SelectedEntityToOverwrite.Id) return;

            _viewService.Merge(SelectedEntityToUse, SelectedEntityToOverwrite);

            SelectedEntityToUse = null;
            SelectedEntityToOverwrite = null;
            EntityToUseSelection.Clear();
            EntityToOverwriteSelection.Clear();

            _interactionManager.Alert("Merged");
        }

        [DependsOn("LoadArguments")]
        public virtual string EntityName
        {
            get { return LoadArguments.IfNotNull(a => a.EntityType.ToString()); }
        }

        [DependsOn("LoadArguments")]
        public virtual string SearchForEntityWatermarkText
        {
            get { return string.Format("Search for {0}...", LoadArguments.IfNotNull(a => a.EntityType.ToString())); }
        }

        [Dependency]
        public virtual MergeEntitiesLoadArguments LoadArguments { get; set; }

        [Dependency]
        public virtual IList<EntityToMergeViewModel> EntityToUseSelection { get; set; }

        [Dependency]
        public virtual IList<EntityToMergeViewModel> EntityToOverwriteSelection { get; set; }

        public virtual EntityToMergeViewModel SelectedEntityToUse { get; set; }

        public virtual EntityToMergeViewModel SelectedEntityToOverwrite { get; set; }

        public virtual string EntityToUseSearchText { get; set; }

        public virtual string EntityToOverwriteSearchText { get; set; }

        public virtual ICommand SearchEntityToUse { get; set; }

        public virtual ICommand SearchEntityToOverwrite { get; set; }

        public virtual ICommand Merge { get; set; }

        public virtual ICommand Closing { get; set; }

        public virtual IInteractionContext InteractionContext { get; set; }
    }
}
