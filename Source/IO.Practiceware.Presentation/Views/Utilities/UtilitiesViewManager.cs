﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Utilities.Merge;
using IO.Practiceware.Presentation.Views.Common;
using IO.Practiceware.Presentation.Views.Utilities.Merge;
using Soaf;
using Soaf.Presentation;
using System.Windows;

namespace IO.Practiceware.Presentation.Views.Utilities
{
    public class UtilitiesViewManager
    {
        private readonly IInteractionManager _interactionManager;

        public UtilitiesViewManager(IInteractionManager interactionManager)
        {
            _interactionManager = interactionManager;
        }

        public void ShowEditTemplates()
        {
            if (!PermissionId.GenerateClaimsAndStatements.EnsurePermission())
            {
                return;
            }

            var view = new EditTemplatesView();
            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view, 
                WindowState = WindowState.Normal, 
                ResizeMode = ResizeMode.CanResizeWithGrip, 
                Header = "Edit Templates"
            });
        }

        public void ShowMergePatients()
        {
            Practiceware.Utilities.UtilitiesViewManager.OpenMergePatients();
        }

        public void ShowMergeInsurers()
        {
            ShowMergeEntities(MergeEntityType.Insurer, PermissionId.MergeInsurancePlans);
        }

        public void ShowMergeProviders()
        {
            ShowMergeEntities(MergeEntityType.Provider, PermissionId.MergeProviders);
        }

        private void ShowMergeEntities(MergeEntityType entityType, PermissionId governedPermission)
        {
            if (!governedPermission.EnsurePermission())
            {
                return;
            }

            var view = new MergeEntitiesView();
            var viewContext = view.DataContext.EnsureType<MergeEntitiesViewContext>();
            viewContext.LoadArguments = new MergeEntitiesLoadArguments
            {
                EntityType = entityType
            };

            _interactionManager.ShowModal(new WindowInteractionArguments
            {
                Content = view,
                WindowState = WindowState.Normal,
                ResizeMode = ResizeMode.NoResize,
                Header = string.Format("Merge {0}", viewContext.LoadArguments.EntityType)
            });
        }
    }
}
