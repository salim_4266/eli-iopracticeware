﻿using IO.Practiceware.Exceptions;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Utilities;
using IO.Practiceware.Presentation.ViewServices.Utilities;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Utilities;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.Views.Utilities
{
    /// <summary>
    /// Interaction logic for EditTemplates.xaml
    /// </summary>
    public partial class EditTemplatesView
    {
        public EditTemplatesView()
        {
            InitializeComponent();
        }
    }

    public class EditTemplatesViewContext : IViewContext
    {
        private readonly IEditTemplatesViewService _editTemplatesViewService;
        private readonly IDocumentGenerationService _documentGenerationService;

        public EditTemplatesViewContext(IEditTemplatesViewService editTemplatesViewService, IDocumentGenerationService documentGenerationService)
        {
            _editTemplatesViewService = editTemplatesViewService;
            _documentGenerationService = documentGenerationService;

            Load = Command.Create(ExecuteLoad).Async(() => InteractionContext);
            LoadTemplateContent = Command.Create(ExecuteLoadTemplateContent).Async(() => InteractionContext);
            Save = Command.Create(ExecuteSave).Async(() => InteractionContext);
            AcceptDiscardUnsavedChanges = Command.Create(ExecuteAcceptDiscardUnsavedChanges);
            GenerateHtml = Command.Create(ExecuteGenerateHtml).Async(() => InteractionContext);
            GeneratePdf = Command.Create<Tuple<string, object>>(ExecuteGeneratePdf);
            ConfirmResetContentTemplate = Command.Create(ExecuteConfirmResetContentTemplate).Async(() => InteractionContext);
            ResetTemplateContent = Command.Create(ExecuteResetTemplateContent);
        }

        private void ExecuteResetTemplateContent()
        {
            if (SelectedTemplateId <= 0) return;

            ResetContentTemplateTrigger = true; // show confirm command
        }

        private void ExecuteConfirmResetContentTemplate()
        {
            if (SelectedTemplateId <= 0) return;

            if (TemplateDetails != null)
            {
                _editTemplatesViewService.ResetTemplateBackToOriginal(TemplateDetails.TemplateDocumentId);

                TemplateDetails = _editTemplatesViewService.GetTemplate(SelectedTemplateId);
                TemplateDetails.CastTo<IEditableObject>().BeginEdit();
                TemplateDetails.CastTo<IChangeTracking>().AcceptChanges();
            }
           
        }

        private void ExecuteGeneratePdf(Tuple<string, object> t)
        {
            var filePath = t.Item1;
            if (TemplateDetails.ContentType != ContentType.RazorTemplate) return;

            var docText = TemplateDetails.DocumentText;

            try
            {
                var htmlPages = _editTemplatesViewService.ProduceMessageTest(docText, TemplateDetails.ContentType).ToList();
                if (htmlPages.IsNotNullOrEmpty())
                {
                    byte[] pdfBytes = WkHtmlToPdfWrapperService.GeneratePdf(htmlPages);

                    FileManager.Instance.CommitContents(filePath, pdfBytes);
                }
            }
            catch (Exception ex)
            {
                this.Dispatcher().BeginInvoke(() => InteractionManager.Current.DisplayDetailedException(ex.InnerException ?? ex));
                throw;
            }
        }


        private void ExecuteGenerateHtml()
        {
            if (TemplateDetails == null || TemplateDetails.ContentType != ContentType.RazorTemplate) return;

            var docText = TemplateDetails.DocumentText;
            try
            {
                var htmlPages = _editTemplatesViewService.ProduceMessageTest(docText, TemplateDetails.ContentType).ToList();
                if (htmlPages.IsNotNullOrEmpty())
                {
                    GeneratedHtml = htmlPages.First();
                }
            }
            catch (Exception ex)
            {
                this.Dispatcher().BeginInvoke(() => InteractionManager.Current.DisplayDetailedException(ex.InnerException ?? ex));
                throw;
            }
        }

        private void ExecuteAcceptDiscardUnsavedChanges()
        {
            HasUnsavedChangesTrigger = false;
        }

        private void ExecuteLoadTemplateContent()
        {
            if (SelectedTemplateId <= 0) return;

            if (TemplateDetails != null && TemplateDetails.CastTo<IChangeTracking>().IsChanged)
            {
                HasUnsavedChangesTrigger = true;
            }

            if (!HasUnsavedChangesTrigger)
            {
                // user hit ok 
                TemplateDetails = _editTemplatesViewService.GetTemplate(SelectedTemplateId);
                TemplateDetails.CastTo<IEditableObject>().BeginEdit();
                TemplateDetails.CastTo<IChangeTracking>().AcceptChanges();
            }
            else
            {
                HasUnsavedChangesTrigger = false;
            }
        }

        private void ExecuteSave()
        {
            if (TemplateDetails != null && TemplateDetails.ContentType == ContentType.RazorTemplate)
            {
                // validate the razor code
                try
                {
                    _documentGenerationService.CompileRazorAndRun(TemplateDetails.DocumentText, new PaperClaim());
                }
                catch (Exception ex)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(() => InteractionManager.Current.Alert("Invalid Razor Template.  Message: {0}".FormatWith(ex.Message)));
                    return;
                }

                _editTemplatesViewService.SaveTemplate(TemplateDetails.TemplateDocumentId, TemplateDetails.DocumentText);
                TemplateDetails.CastTo<IEditableObject>().EndEdit();
                TemplateDetails.CastTo<IChangeTracking>().AcceptChanges();
            }
        }

        private void ExecuteLoad()
        {
            var list = _editTemplatesViewService.GetListOfTemplates();
            TemplateList = list.ToExtendedObservableCollection();
        }

        [DispatcherThread]
        public virtual EditTemplatesViewModel TemplateDetails { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> TemplateList { get; set; }

        [DispatcherThread]
        public virtual int SelectedTemplateId { get; set; }

        [DispatcherThread]
        public virtual bool HasUnsavedChangesTrigger { get; set; }

        [DispatcherThread]
        public virtual bool ResetContentTemplateTrigger { get; set; }

        [DispatcherThread]
        public virtual string GeneratedHtml { get; set; }

        public IInteractionContext InteractionContext { get; set; }
        public ICommand Load { get; set; }
        public ICommand LoadTemplateContent { get; set; }
        public ICommand Save { get; set; }
        public ICommand Reset { get; set; }
        public ICommand AcceptDiscardUnsavedChanges { get; set; }
        public ICommand GenerateHtml { get; set; }
        public ICommand GeneratePdf { get; set; }
        public ICommand ConfirmResetContentTemplate { get; set; }
        public ICommand ResetTemplateContent { get; set; }
    }
}
