using IO.Practiceware.Exceptions;
using IO.Practiceware.Presentation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;
using Soaf.Web.Client;
using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;

[assembly: XmlnsDefinition("http://io/presentation", "IO.Practiceware.Presentation.Views.Common.Controls")]
[assembly: XmlnsDefinition("http://io/presentation", "IO.Practiceware.Presentation.Views.Common.Converters")]
[assembly: XmlnsDefinition("http://io/presentation", "IO.Practiceware.Presentation.Views.Common.Behaviors")]
[assembly: XmlnsDefinition("http://io/presentation", "IO.Practiceware.Presentation.Views.Common")]

[assembly: Component(typeof(PresentationBootstrapper))]

namespace IO.Practiceware.Presentation
{
    [Singleton]
    public class PresentationBootstrapper
    {
        private static readonly object SyncRoot = new object();
        private static bool _hasRun;

        [DebuggerNonUserCode]
        public static void AddViewModelKnownTypes()
        {
            Type[] types;
            try
            {
                types = Assembly.GetExecutingAssembly().GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                types = ex.Types;
            }

            types
                .Where(t => t.Is<IViewModel>()
                    && (t.HasAttribute<DataContractAttribute>() || t.HasAttribute<CollectionDataContractAttribute>()))
                .ToArray()
                .ForEach(ServiceModel.AddKnowntype);
        }

        private static void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Trace.TraceError(e.Exception.ToString());
            if (Environment.UserInteractive && System.Windows.Application.Current != null)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() => InteractionManager.Current.DisplayDetailedException(e.Exception)));
            }
            e.Handled = true;
        }

        public static void Run()
        {
            lock (SyncRoot)
            {
                if (!_hasRun)
                {
                    _hasRun = true;
                    try
                    {
                        // force load of AvalonDock Metro Theme
                        typeof(Xceed.Wpf.AvalonDock.Themes.MetroTheme).EnsureNotDefault();

                        System.Windows.Application application = System.Windows.Application.Current ?? new System.Windows.Application();
                        application.DispatcherUnhandledException += OnDispatcherUnhandledException;
                        SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));

                        var styleResources = System.Windows.Application.Current.SafeLoadComponent<ResourceDictionary>(
                            typeof(PresentationBootstrapper).Assembly.BuildComponentUri("/Resources/StyleResources.xaml"));

                        foreach (var entry in new[] { styleResources }.Recurse(x => x.MergedDictionaries).Reverse().SelectMany(i => i.OfType<DictionaryEntry>()))
                        {
                            application.Resources[entry.Key] = entry.Value;
                        }

                        AddViewModelKnownTypes();

                        // set the timezone info in the current operation context.
                        DateTimes.SetClientTimeZone(TimeZoneInfo.Local.Id);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.ToString());
                        throw;
                    }
                }
            }
        }
    }
}