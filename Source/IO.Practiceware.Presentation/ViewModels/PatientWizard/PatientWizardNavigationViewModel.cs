﻿using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewsModels.NonClinicalPatient;
using Soaf;
using Soaf.Presentation;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.PatientWizard
{
    [DataContract]
    [SupportsDataErrorInfo]
    [EditableObject]
    public class PatientWizardNavigationViewModel : IViewModel
    {      
        /// <summary>
        /// The id of the patient who the chosen patient is for.  E.g. it would be the id of a patient if we are trying to add a contact to a patient.
        /// </summary>
        public int? PatientId { get; set; }

        /// <summary>
        /// The type of the patient that the user is searching for. e.g. PatientContact or Policyholder
        /// </summary>
        public PatientRelationshipType PatientRelationshipType { get; set; }

        /// <summary>
        /// The name of the specific type of relationship the user is searching for.  e.g. PatientContact or Policyholder
        /// </summary>
        [DependsOn("PatientRelationshipType")]
        public string PatientTypeName
        {
            get { return PatientRelationshipType.GetDisplayName(); }
        }

        /// <summary>
        /// The id of the patient selected from the wizard.
        /// </summary>
        public int? SelectedtId { get; set; }    

        /// <summary>
        /// True if the user is trying to add a Policyholder
        /// </summary>
        [DependsOn("PatientRelationshipType")]
        public bool IsPolicyholderRelationship
        {
            get { return PatientRelationshipType == PatientRelationshipType.Policyholder; }
        }

        /// <summary>
        /// The chosen pre-defined relationship when adding a Policyholder relationship.
        /// </summary>
        public int? PolicyholderRelationshipTypeId { get; set; }

        /// <summary>
        /// The user defined description of the relationship between the Entity and the Contact they are searching for.
        /// Does not apply to Policyholder relationships
        /// </summary>
        public string RelationshipDescription { get; set; }
    }

}
