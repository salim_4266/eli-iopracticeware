﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.BatchBuilder
{
    /// <summary>
    /// A view model for a batch builder template.
    /// </summary>
    [DataContract]
    public class BatchBuilderTemplateViewModel : IViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the selected filters.
        /// </summary>
        /// <value>
        /// The selected filters.
        /// </value>
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<BatchBuilderFilterViewModel> SelectedFilters { get; set; }

        /// <summary>
        /// Gets or sets the clicked actions.
        /// </summary>
        /// <value>
        /// The clicked actions.
        /// </value>
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<BatchBuilderActionViewModel> ClickedActions { get; set; }

        /// <summary>
        /// Gets or sets the Primary grid columns.
        /// </summary>
        /// <value>
        /// The Primary grid columns.
        /// </value>
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<String> PrimaryGridColumns { get; set; }

        /// <summary>
        /// Gets or sets the Current Batch grid columns.
        /// </summary>
        /// <value>
        /// The Current Batch grid columns.
        /// </value>
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<String> CurrentBatchGridColumns { get; set; }

        /// <summary>
        /// Gets or sets the Search Type
        /// </summary>
        /// <value>
        /// The Current Batch grid columns.
        /// </value>
        [DataMember]
        public virtual BatchBuilderSearchType SearchType { get; set; }
    }
}