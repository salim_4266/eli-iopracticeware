﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.BatchBuilder
{
    [DataContract]
    [KnownType(typeof(BatchBuilderTextFilterViewModel))]
    [KnownType(typeof(BatchBuilderDropDownFilterViewModel))]
    [KnownType(typeof(BatchBuilderDateRangeFilterViewModel))]
    public class BatchBuilderFilterViewModel : IViewModel
    {
        public BatchBuilderFilterViewModel()
        {

        }
        
        public BatchBuilderFilterViewModel(string name, string groupName)
        {
            Name = name;
            GroupName = groupName;
        }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string GroupName { get; set; }
    }

    [DataContract]
    public class BatchBuilderFilterGroupViewModel : IViewModel
    {
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<BatchBuilderFilterViewModel> Filters { get; set; }

        [DataMember]
        public virtual string Name { get; set; }
    }

    [DataContract]
    public static class BatchBuilderFilterNames
    {
        public const string PatientLastName = "Patient Last Name";
        public const string PatientFirstName = "Patient First Name";
        public const string PatientId = "Patient Id";
        public const string PatientType = "Patient Type";
        public const string PatientAge = "Patient Age";
        public const string PatientDob = "Patient DOB";
        public const string AppointmentsDateRange = "Appointment Date Range";
        public const string AppointmentsToday = "Appointments Today";
        public const string AppointmentsTommorrow = "Appointments Tommorrow";
        public const string AppointmentsWeek = "Appointments Next 7 Days";
        public const string AppointmentsMonth = "Appointments Next 30 Days";
        public const string LastAppointment = "Last Appointment";
        public const string AppointmentType = "Appointment Type";
        public const string AppointmentStatus = "Appointment Status";
        public const string ScheduledDoctor = "Scheduled Doctor";
        public const string ScheduledLocation = "Scheduled Location";
        public const string ReferringDoctor = "Referring Doctor";
        public const string Diagnosis = "Diagnosis";
        public const string InsurerName = "Insurer Name";
        public const string InsuranceType = "Insurance Type";
        public const string PatientPhone = "Patient Phone Number";
        public const string PrimaryInsuranceName = "Primary Insurance Name";
        public const string NonPrimaryInsuranceName = "Non-Primary Insurance Name";
        public const string InsurerPlanType = "Insurer Plan Type";
        public const string ActiveDateRange = "Insurance Active Date Range";
        public const string ResponseDateRange = "Response Date Range";
        public const string ResponsesToday = "Responses Received Today";
        public const string ResponsesYesterday = "Responses Received Yesterday";
        public const string ResponsesLastWeek = "Responses Received Last Week";
    }

    [DataContract]
    public static class BatchBuilderFilterGroupNames
    {
        public const string Patient = "Patient";
        public const string AppointmentDate = "Appointment Date";
        public const string Appointment = "Appointment";
        public const string Insurance = "Insurance";
        public const string Eligibility = "Eligibility Responses";
    }

    public interface IBatchBuilderFilterSource
    {
        IEnumerable<BatchBuilderFilterViewModel> Filters { get; }
    }

    [DataContract]
    public class BatchBuilderPatientFilterSource : IBatchBuilderFilterSource
    {
        #region IBatchBuilderFilterSource Members

        public IEnumerable<BatchBuilderFilterViewModel> Filters
        {
            get
            {
                return new BatchBuilderFilterViewModel[]
                           {
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientId, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.PatientType, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientAge, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.PatientDob, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.LastAppointment, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.ReferringDoctor, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientPhone, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsDateRange, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsToday, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsTommorrow, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsWeek, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsMonth, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.AppointmentType, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.AppointmentStatus, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.ScheduledDoctor, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.ScheduledLocation, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.Diagnosis, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PrimaryInsuranceName, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.NonPrimaryInsuranceName, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.InsurerName, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.InsuranceType, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.InsurerPlanType, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ActiveDateRange, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponseDateRange, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesToday, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesYesterday, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesLastWeek, BatchBuilderFilterGroupNames.Eligibility)
                           };
            }
        }

        #endregion
    }

    [DataContract]
    public class BatchBuilderAppointmentFilterSource : IBatchBuilderFilterSource
    {
        #region IBatchBuilderFilterSource Members

        public IEnumerable<BatchBuilderFilterViewModel> Filters
        {
            get
            {
                return new BatchBuilderFilterViewModel[]
                           {
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientLastName, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientFirstName, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.PatientId, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.PatientDob, BatchBuilderFilterGroupNames.Patient),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsDateRange, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsToday, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsTommorrow, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsWeek, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.AppointmentsMonth, BatchBuilderFilterGroupNames.AppointmentDate),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.AppointmentType, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.AppointmentStatus, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.ScheduledDoctor, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.ScheduledLocation, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.Diagnosis, BatchBuilderFilterGroupNames.Appointment),
                               new BatchBuilderTextFilterViewModel(BatchBuilderFilterNames.InsurerName, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.InsuranceType, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDropDownFilterViewModel(BatchBuilderFilterNames.InsurerPlanType, BatchBuilderFilterGroupNames.Insurance),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponseDateRange, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesToday, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesYesterday, BatchBuilderFilterGroupNames.Eligibility),
                               new BatchBuilderDateRangeFilterViewModel(BatchBuilderFilterNames.ResponsesLastWeek, BatchBuilderFilterGroupNames.Eligibility)
                           };
            }
        }

        #endregion
    }

    [DataContract]
    public enum DateRangeType
    {
        [EnumMember] Yesterday,
        [EnumMember] Today,
        [EnumMember] Tomorrow,
        [EnumMember] LastWeek,
        [EnumMember] Week,
        [EnumMember] Month
    }


    [DataContract]
    public class BatchBuilderTextFilterViewModel : BatchBuilderFilterViewModel
    {
        public BatchBuilderTextFilterViewModel()
        {
            
        }

        public BatchBuilderTextFilterViewModel(string name, string groupName) : base(name, groupName)
        {
        }

        [DataMember]
        public virtual string Text { get; set; }
    }

    [DataContract]
    public class BatchBuilderDropDownFilterViewModel : BatchBuilderFilterViewModel
    {
        public BatchBuilderDropDownFilterViewModel()
        {
            
        }

        public BatchBuilderDropDownFilterViewModel(string name, string groupName) : base(name, groupName)
        {
            SelectedChoices = new ObservableCollection<string>();
        }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<string> Choices { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<string> SelectedChoices { get; set; }
    }

    [DataContract]
    public class BatchBuilderDateRangeFilterViewModel : BatchBuilderFilterViewModel
    {
        public BatchBuilderDateRangeFilterViewModel()
        {
            
        }

        public BatchBuilderDateRangeFilterViewModel(string name, string groupName) : base(name, groupName)
        {
            DateRanges = new ObservableCollection<DateRange>();
            if (name == BatchBuilderFilterNames.ResponsesYesterday)
                DateRangeType = DateRangeType.Yesterday;
            else if (name == BatchBuilderFilterNames.AppointmentsToday || name == BatchBuilderFilterNames.ResponsesToday)
                DateRangeType = DateRangeType.Today;
            else if (name == BatchBuilderFilterNames.AppointmentsTommorrow)
                DateRangeType = DateRangeType.Tomorrow;
            else if (name == BatchBuilderFilterNames.ResponsesLastWeek)
                DateRangeType = DateRangeType.LastWeek;
            else if (name == BatchBuilderFilterNames.AppointmentsWeek)
                DateRangeType = DateRangeType.Week;
            else if (name == BatchBuilderFilterNames.AppointmentsMonth)
                DateRangeType = DateRangeType.Month;
        }

        [DataMember]
        public virtual DateTime? Start { get; set; }

        [DataMember]
        public virtual DateTime? End { get; set; }

        public DateRangeType DateRangeType
        {
            set
            {
                switch (value)
                {
                    case DateRangeType.Yesterday:
                        Start = DateTime.Now.ToClientTime().AddDays(-1);
                        End = DateTime.Now.ToClientTime().AddDays(-1);
                        break;
                    case DateRangeType.Today:
                        Start = DateTime.Now.ToClientTime();
                        End = DateTime.Now.ToClientTime();
                        break;
                    case DateRangeType.Tomorrow:
                        Start = DateTime.Now.ToClientTime().AddDays(1);
                        End = DateTime.Now.ToClientTime().AddDays(1);
                        break;
                    case DateRangeType.LastWeek:
                        Start = DateTime.Now.ToClientTime().AddDays(-7);
                        End = DateTime.Now.ToClientTime();
                        break;
                    case DateRangeType.Week:
                        Start = DateTime.Now.ToClientTime();
                        End = DateTime.Now.ToClientTime().AddDays(7);
                        break;
                    case DateRangeType.Month:
                        Start = DateTime.Now.ToClientTime();
                        End = DateTime.Now.ToClientTime().AddMonths(1);
                        break;
                }
            }
        }

        [DataMember]
        [Dependency]
        public ObservableCollection<DateRange> DateRanges { get; set; }
    }

    [DataContract]
    public class DateRange
    {
        public DateRange(DateTime? start, DateTime? end)
        {
            Start = start;
            End = end;
        }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }
    }
}