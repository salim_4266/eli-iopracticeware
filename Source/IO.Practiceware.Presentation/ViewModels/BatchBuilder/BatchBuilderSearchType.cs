﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.BatchBuilder
{
    [DataContract]
    public enum BatchBuilderSearchType
    {
        [EnumMember] Patient,
        [EnumMember] Appointment
    }

    public static class BatchBuilderSearchTypes
    {
        /// <summary>
        /// Creates an empty results collection for the specified search type.
        /// </summary>
        /// <param name="searchType">Type of the search.</param>
        /// <returns></returns>
        public static IEnumerable<BatchBuilderResultViewModel> CreateEmptyResults(this BatchBuilderSearchType searchType)
        {
            switch (searchType)
            {
                case BatchBuilderSearchType.Appointment:
                    return new ObservableCollection<BatchBuilderAppointmentResultViewModel>();
                case BatchBuilderSearchType.Patient:
                    return new ObservableCollection<BatchBuilderPatientResultViewModel>();
                default:
                    return new ObservableCollection<BatchBuilderResultViewModel>();
            }
        }
    }
}
