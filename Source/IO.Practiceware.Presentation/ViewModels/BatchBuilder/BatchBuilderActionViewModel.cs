﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.BatchBuilder
{
    /// <summary>
    ///   A group of actions for the batch builder.
    /// </summary>
    [DataContract]
    [KnownType(typeof(BatchBuilderConfirmationsActionsGroupViewModel))]
    [KnownType(typeof(BatchBuilderEligibilityActionsGroupViewModel))]
    [KnownType(typeof(BatchBuilderRecallsActionsGroupViewModel))]
    [KnownType(typeof(BatchBuilderLettersAndFormsActionsGroupViewModel))]
    public abstract class BatchBuilderActionsGroupViewModel : IViewModel
    {
        public abstract string Name { get; }

        public abstract bool IsEnabled { get; }

        public abstract IEnumerable<BatchBuilderActionViewModel> Actions { get; }
    }

    [DataContract]
    public class BatchBuilderConfirmationsActionsGroupViewModel : BatchBuilderActionsGroupViewModel
    {
        public override string Name
        {
            get { return "Confirmations"; }
        }

        public override bool IsEnabled
        {
            get { return true; }
        }

        public override IEnumerable<BatchBuilderActionViewModel> Actions
        {
            get
            {
                yield return EmailAction;
                yield return TextMessageAction;
            }
        }

        [DataMember]
        public virtual BatchBuilderConfirmationActionViewModel EmailAction { get; set; }

        [DataMember]
        public virtual BatchBuilderConfirmationActionViewModel TextMessageAction { get; set; }
    }

    [DataContract]
    public class BatchBuilderEligibilityActionsGroupViewModel : BatchBuilderActionsGroupViewModel
    {
        public override string Name
        {
            get { return "Eligibility"; }
        }

        public override bool IsEnabled
        {
            get { return false; }
        }

        public override IEnumerable<BatchBuilderActionViewModel> Actions
        {
            get { return new BatchBuilderActionViewModel[0]; }
        }
    }

    [DataContract]
    public class BatchBuilderRecallsActionsGroupViewModel : BatchBuilderActionsGroupViewModel
    {
        public override string Name
        {
            get { return "Recalls"; }
        }

        public override bool IsEnabled
        {
            get { return false; }
        }

        public override IEnumerable<BatchBuilderActionViewModel> Actions
        {
            get { return new BatchBuilderActionViewModel[0]; }
        }
    }

    [DataContract]
    public class BatchBuilderLettersAndFormsActionsGroupViewModel : BatchBuilderActionsGroupViewModel
    {
        public override string Name
        {
            get { return "Letters / Forms"; }
        }

        public override bool IsEnabled
        {
            get { return false; }
        }

        public override IEnumerable<BatchBuilderActionViewModel> Actions
        {
            get { return new BatchBuilderActionViewModel[0]; }
        }
    }

    [DataContract]
    public abstract class BatchBuilderActionViewModel : IViewModel
    {
        [DataMember]
        public virtual bool IsSelected { get; set; }
    }

    [DataContract]
    public enum BatchBuilderConfirmationType
    {
        [EnumMember] [Display(Name = "Email")] Email,
        [EnumMember] [Display(Name = "Text Message")] TextMessage
    }

    [DataContract]
    public class BatchBuilderConfirmationActionViewModel : BatchBuilderActionViewModel
    {
        [DataMember]
        public virtual BatchBuilderConfirmationType ConfirmationType { get; set; }

        [DataMember]
        public virtual string SelectedTemplate { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<string> Templates { get; set; }

        [DataMember]
        public override bool IsSelected { get; set; }
    }
}