﻿using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.BatchBuilder
{
    [DataContract]
    [KnownType(typeof(BatchBuilderPatientResultViewModel))]
    [KnownType(typeof(BatchBuilderAppointmentResultViewModel))]
    public abstract class BatchBuilderResultViewModel : IViewModel
    {
        [DataMember]
        [Display(Name = "Patient Id", GroupName = "Patient")]
        public abstract int PatientId { get; set; }

        [DataMember]
        [Display(Name = "Last Name", GroupName = "Patient")]
        public abstract string LastName { get; set; }

        [DataMember]
        [Display(Name = "First Name", GroupName = "Patient")]
        public abstract string FirstName { get; set; }

        [DataMember]
        [Display(Name = "DOB", GroupName = "Patient")]
        public abstract DateTime? DateOfBirth { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public abstract int AppointmentId { get; set; }
    }

    /// <summary>
    ///   A view model for a single Patient returned in a Batch results search.
    /// </summary>
    [DataContract]
    public class BatchBuilderPatientResultViewModel : BatchBuilderResultViewModel
    {
        [DataMember]
        [Display(Name = "Id", GroupName = "Patient")]
        public override int PatientId { get; set; }

        [DataMember]
        [Display(Name = "Last Name", GroupName = "Patient")]
        public override string LastName { get; set; }

        [DataMember]
        [Display(Name = "First Name", GroupName = "Patient")]
        public override string FirstName { get; set; }

        [DataMember]
        [Display(Name = "DOB", GroupName = "Patient")]
        public override DateTime? DateOfBirth { get; set; }

        [DataMember]
        [Display(Name = "Patient Type", GroupName = "Patient")]
        public virtual string PatientType { get; set; }

        [DataMember]
        [Display(Name = "Age", GroupName = "Patient")]
        public virtual string Age { get; set; }

        [DataMember]
        [Display(Name = "Insurance Name", GroupName = "Insurance")]
        public virtual string Insurance { get; set; }

        [DataMember]
        [Display(Name = "Insurance Type", GroupName = "Insurance")]
        public virtual string InsuranceType { get; set; }

        [DataMember]
        [Display(Name = "Plan Type", GroupName = "Insurance")]
        public virtual string PlanType { get; set; }

        [DataMember]
        [Display(Name = "Referring Doctor")]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        [Display(Name = "Last Appointment")]
        public virtual DateTime? LastAppointment { get; set; }

        [DataMember]
        [Display(Name = "Policy Start date", GroupName = "Insurance")]
        public virtual DateTime? PolicyStartDate { get; set; }

        [DataMember]
        [Display(Name = "Policy End date", GroupName = "Insurance")]
        public virtual DateTime? PolicyEndDate { get; set; }

        //Eligibility columns
        [DataMember]
        [Display(Name = "Subscriber Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Entity Type", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Last Name", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberLastName { get; set; }

        [DataMember]
        [Display(Name = "Subscriber First Name", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberFirstName { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Id Code", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberIdCode { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Trace Reference Identifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberTraceReferenceIdentifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Trace Originating Company Id", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberTraceOriginatingCompanyId { get; set; }

        [DataMember]
        [Display(Name = "Patient Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string PatientEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Patient Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Last Name", GroupName = "Eligibility Benefits")]
        public virtual string PatientLastName { get; set; }

        [DataMember]
        [Display(Name = "Patient First Name", GroupName = "Eligibility Benefits")]
        public virtual string PatientFirstName { get; set; }

        [DataMember]
        [Display(Name = "Patient Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Id Code", GroupName = "Eligibility Benefits")]
        public virtual string PatientIdCode { get; set; }

        [DataMember]
        [Display(Name = "Patient Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string PatientReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Patient DateOfBirth Format Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientDateOfBirthFormatQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient DateOfBirth", GroupName = "Eligibility Benefits")]
        public virtual string PatientDateOfBirth { get; set; }

        [DataMember]
        [Display(Name = "Patient Gender", GroupName = "Eligibility Benefits")]
        public virtual string PatientGender { get; set; }

        [DataMember]
        [Display(Name = "Patient Policy Holder Relationship", GroupName = "Eligibility Benefits")]
        public virtual string PatientPolicyHolderRelationship { get; set; }

        [DataMember]
        [Display(Name = "Benefit Info Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitInfoCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Coverage Level Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitCoverageLevelCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Insurance Type Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitInsuranceTypeCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Plan Coverage Description", GroupName = "Eligibility Benefits")]
        public virtual string BenefitPlanCoverageDescription { get; set; }

        [DataMember]
        [Display(Name = "Benefit Monetary Amount", GroupName = "Eligibility Benefits")]
        public virtual string BenefitMonetaryAmount { get; set; }

        [DataMember]
        [Display(Name = "Benefit CoInsurance Percent", GroupName = "Eligibility Benefits")]
        public virtual string BenefitCoInsurancePercent { get; set; }

        [DataMember]
        [Display(Name = "Benefit Quantity Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string BenefitQuantityQualifier { get; set; }

        [DataMember]
        [Display(Name = "Benefit Quantity", GroupName = "Eligibility Benefits")]
        public virtual string BenefitQuantity { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Entity Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Name", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverName { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Address Line1", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverAddressLine1 { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Address Line2", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverAddressLine2 { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver City", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverCity { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver State", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverState { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Zip Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverZipCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Country Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverCountryCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Provider Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverProviderCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Info Source Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Source Entity Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Source Name", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceName { get; set; }

        [DataMember]
        [Display(Name = "Info Source Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Source Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceIdCode { get; set; }

        [DataMember]
        [Display(AutoGenerateField = false)]
        public override int AppointmentId { get; set; }
    }

    /// <summary>
    ///   A view model for a single Appointment returned in a Batch results search.
    /// </summary>
    [DataContract]
    public class BatchBuilderAppointmentResultViewModel : BatchBuilderResultViewModel
    {
        [DataMember]
        [Display(Name = "Id", GroupName = "Patient")]
        public override int PatientId { get; set; }

        [DataMember]
        [Display(Name = "Last Name", GroupName = "Patient")]
        public override string LastName { get; set; }

        [DataMember]
        [Display(Name = "First Name", GroupName = "Patient")]
        public override string FirstName { get; set; }

        [DataMember]
        [Display(Name = "DOB", GroupName = "Patient")]
        public override DateTime? DateOfBirth { get; set; }

        [DataMember]
        [Display(Name = "Appointment Date")]
        public virtual DateTime? AppointmentDate { get; set; }

        [DataMember]
        [Display(Name = "Appointment Type")]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual string Status { get; set; }

        [DataMember]
        public virtual string Doctor { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

        [DataMember]
        public virtual string Insurance { get; set; }

        [DataMember]
        [Display(Name = "Insurance Type")]
        public virtual string InsuranceType { get; set; }

        [DataMember]
        [Display(Name = "Referring Doctor")]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        public virtual string Diagnosis { get; set; }

        //Eligibility columns
        [DataMember]
        [Display(Name = "Subscriber Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Entity Type", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Last Name", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberLastName { get; set; }

        [DataMember]
        [Display(Name = "Subscriber First Name", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberFirstName { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Id Code", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberIdCode { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Trace Reference Identifier", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberTraceReferenceIdentifier { get; set; }

        [DataMember]
        [Display(Name = "Subscriber Trace Originating Company Id", GroupName = "Eligibility Benefits")]
        public virtual string SubscriberTraceOriginatingCompanyId { get; set; }

        [DataMember]
        [Display(Name = "Patient Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string PatientEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Patient Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Last Name", GroupName = "Eligibility Benefits")]
        public virtual string PatientLastName { get; set; }

        [DataMember]
        [Display(Name = "Patient First Name", GroupName = "Eligibility Benefits")]
        public virtual string PatientFirstName { get; set; }

        [DataMember]
        [Display(Name = "Patient Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Id Code", GroupName = "Eligibility Benefits")]
        public virtual string PatientIdCode { get; set; }

        [DataMember]
        [Display(Name = "Patient Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string PatientReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Patient DateOfBirth Format Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string PatientDateOfBirthFormatQualifier { get; set; }

        [DataMember]
        [Display(Name = "Patient DateOfBirth", GroupName = "Eligibility Benefits")]
        public virtual string PatientDateOfBirth { get; set; }

        [DataMember]
        [Display(Name = "Patient Gender", GroupName = "Eligibility Benefits")]
        public virtual string PatientGender { get; set; }

        [DataMember]
        [Display(Name = "Patient Policy Holder Relationship", GroupName = "Eligibility Benefits")]
        public virtual string PatientPolicyHolderRelationship { get; set; }

        [DataMember]
        [Display(Name = "Benefit Info Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitInfoCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Coverage Level Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitCoverageLevelCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Insurance Type Code", GroupName = "Eligibility Benefits")]
        public virtual string BenefitInsuranceTypeCode { get; set; }

        [DataMember]
        [Display(Name = "Benefit Plan Coverage Description", GroupName = "Eligibility Benefits")]
        public virtual string BenefitPlanCoverageDescription { get; set; }

        [DataMember]
        [Display(Name = "Benefit Monetary Amount", GroupName = "Eligibility Benefits")]
        public virtual string BenefitMonetaryAmount { get; set; }

        [DataMember]
        [Display(Name = "Benefit CoInsurance Percent", GroupName = "Eligibility Benefits")]
        public virtual string BenefitCoInsurancePercent { get; set; }

        [DataMember]
        [Display(Name = "Benefit Quantity Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string BenefitQuantityQualifier { get; set; }

        [DataMember]
        [Display(Name = "Benefit Quantity", GroupName = "Eligibility Benefits")]
        public virtual string BenefitQuantity { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Entity Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Name", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverName { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Address Line1", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverAddressLine1 { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Address Line2", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverAddressLine2 { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver City", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverCity { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver State", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverState { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Zip Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverZipCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Country Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverCountryCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Provider Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverProviderCode { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Reference Id Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverReferenceIdQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Receiver Reference Id", GroupName = "Eligibility Benefits")]
        public virtual string InfoReceiverReferenceId { get; set; }

        [DataMember]
        [Display(Name = "Info Source Entity Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceEntityIdCode { get; set; }

        [DataMember]
        [Display(Name = "Info Source Entity Type Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceEntityTypeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Source Name", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceName { get; set; }

        [DataMember]
        [Display(Name = "Info Source Id Code Qualifier", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceIdCodeQualifier { get; set; }

        [DataMember]
        [Display(Name = "Info Source Id Code", GroupName = "Eligibility Benefits")]
        public virtual string InfoSourceIdCode { get; set; }

        [DataMember]
        [Display(AutoGenerateField = true)]
        public override int AppointmentId { get; set; }
    }

    // Custom Appointment comparer for the BatchBuilderResultViewModel class
    internal class BatchBuilderResultViewModelAppointmentComparer : IEqualityComparer<BatchBuilderResultViewModel>
    {
        // Results are equal if their AppointmentIds are equal.

        #region IEqualityComparer<BatchBuilderResultViewModel> Members

        public bool Equals(BatchBuilderResultViewModel x, BatchBuilderResultViewModel y)
        {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the Results' properties are equal.
            return x.AppointmentId == y.AppointmentId;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(BatchBuilderResultViewModel result)
        {
            //Check whether the object is null
            if (ReferenceEquals(result, null)) return 0;

            return result.AppointmentId.GetHashCode();
        }

        #endregion
    }

    // Custom Patient comparer for the BatchBuilderResultViewModel class
    internal class BatchBuilderResultViewModelPatientComparer : IEqualityComparer<BatchBuilderResultViewModel>
    {
        // Results are equal if their PatientIds are equal.

        #region IEqualityComparer<BatchBuilderResultViewModel> Members

        public bool Equals(BatchBuilderResultViewModel x, BatchBuilderResultViewModel y)
        {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the Results' properties are equal.
            return x.PatientId == y.PatientId;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(BatchBuilderResultViewModel result)
        {
            //Check whether the object is null
            if (ReferenceEquals(result, null)) return 0;

            return result.PatientId.GetHashCode();
        }

        #endregion
    }
}