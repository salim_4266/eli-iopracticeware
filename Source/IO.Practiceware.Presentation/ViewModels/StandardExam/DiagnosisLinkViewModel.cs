﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.StandardExam
{
    [DataContract]

    public class DiagnosisLinkViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string PointerLetter { get; set; }

        [DataMember]
        public virtual string DiagnosisColor { get; set; }

    }
}
