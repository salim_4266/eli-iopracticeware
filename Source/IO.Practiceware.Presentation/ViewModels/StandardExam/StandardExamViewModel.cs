﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;

namespace IO.Practiceware.Presentation.ViewModels.StandardExam
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class AppointmentInfoViewModel : IViewModel
    {
        

        [DataMember]
        public virtual NamedViewModel SchedulingProvider { get; set; }

        [DataMember]
        public virtual ProviderViewModel RenderingProvider { get; set; }

        [DataMember]
        public virtual NamedViewModel ServiceLocation { get; set; }

        [DataMember]
        public virtual DateTime ScheduleDate { get; set; }
        

        [DataMember]
        [Dependency]
        public virtual AppointmentTypeViewModel AppointmentType { get; set; }
    }


    [DataContract]
    [SupportsDataErrorInfo]
    public class InvoiceInfoViewModel : IViewModel
    {

        [DataMember]
        [Required]
        public virtual ExtendedObservableCollection<NamedViewModel> InvoiceType { get; set; }
    }

    [DataContract]
    public class AppointmentTypeViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual int? CategoryId { get; set; }

        [DataMember]
        public virtual bool IsFirstVisit { get; set; }
    }
    public class StandardExamLoadArguments
    {
        [DataMember]
        public virtual int? PatientId { get; set; }

        [DataMember]
        public virtual int? EncounterId { get; set; }

        [DataMember]
        public List<DiagnosesLoadArguments> DiagnosesLoadArguments { get; set; }

        public OfficeVisitLoadArguments OfficevisitLoadArguments { get; set; }
    }

    public class OfficeVisitLoadArguments
    {
        [DataMember]
        public virtual string EmProcCode { get; set; }

        [DataMember]
        public virtual string EmLevel { get; set; }

        [DataMember]
        public virtual string EyeProcCode { get; set; }

        [DataMember]
        public virtual string EyeLevel { get; set; }
    }

    public class StandardExamReturnArguments
    {
        [DataMember]
        public virtual string FormName { get; set; }

        [DataMember]
        public virtual bool IsIcd10 { get; set; }

    }

    public class DiagnosesLoadArguments
    {
        [DataMember]
        public virtual string DiagnosisCode { get; set; }

        [DataMember]
        public virtual string DiagnosisDescription { get; set; }


        public virtual string Diagnosis { get { return DiagnosisCode + "-" + DiagnosisDescription; } }

        [DataMember]
        public virtual string EyeContext { get; set; }

        [DataMember]
        public virtual string EyeLidLocation { get; set; }
    }

    public class CodingCluesLoadArguments
    {
        [DataMember]
        public virtual string EmLevelQualified { get; set; }

        [DataMember]
        public virtual int TimingSlot1 { get; set; }

        [DataMember]
        public virtual string EyeLevelQualified { get; set; }

        [DataMember]
        public virtual string TypeOfHistory { get; set; }

        [DataMember]
        public virtual bool ReasonForVisit { get; set; }

        [DataMember]
        public virtual bool HPI { get; set; }

        [DataMember]
        public virtual bool ROS { get; set; }

        [DataMember]
        public virtual bool PHX { get; set; }

        [DataMember]
        public virtual string TypeOfExam { get; set; }

        [DataMember]
        public virtual bool VisualAcuity { get; set; }

        [DataMember]
        public virtual bool GeneralMedicalEvaluation { get; set; }

        [DataMember]
        public virtual bool IOP { get; set; }

        [DataMember]
        public virtual bool Pupils { get; set; }

        [DataMember]
        public virtual bool Conj { get; set; }

        [DataMember]
        public virtual bool Adnexa { get; set; }

        [DataMember]
        public virtual bool CVF { get; set; }

        [DataMember]
        public virtual bool Cornea { get; set; }

        [DataMember]
        public virtual bool AC { get; set; }

        [DataMember]
        public virtual bool OnVit { get; set; }

        [DataMember]
        public virtual bool Lens { get; set; }

        [DataMember]
        public virtual bool FundusExam { get; set; }

        [DataMember]
        public virtual string MedicalDecisionMaking { get; set; }

        [DataMember]
        public virtual string ChronicDiagnosis { get; set; }

        [DataMember]
        public virtual string NewDiagnosis { get; set; }

        [DataMember]
        public virtual string TestOrdered { get; set; }

        [DataMember]
        public virtual string ProcedureOrdered { get; set; }

        [DataMember]
        public virtual bool ManagedMedications { get; set; }
    }

    public class DataListViewModel
    {
        [DataMember]
        public AppointmentInfoViewModel AppointmentInfo;

        [DataMember]
        public PatientInfoViewModel PatientInfo;

        [DataMember]
        public bool DefaultDiagnosisType;

        [DataMember]
        public InvoiceInfoViewModel InvoiceInfo;
    }
}
