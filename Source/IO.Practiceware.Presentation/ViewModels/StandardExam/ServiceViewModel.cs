﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.StandardExam
{
    [DataContract]
    public class ServiceViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual bool IsQueuedToBill { get; set; }

        [DataMember]
        public virtual bool IsOfficeVisit { get; set; }

        [DataMember]
        public virtual ServiceCodeViewModel Code { get; set; }
        
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> Modifiers { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<DiagnosisViewModel> DiagnosisLinks { get; set; }

        [DataMember]
        public virtual Decimal UnitCharge { get; set; }
       
        
        [DataMember]
        public virtual Decimal Units { get; set; }

        [DependsOn("Units", "UnitCharge")]
        public virtual Decimal TotalCharge { get { return Math.Round((UnitCharge * Units), 2); } }

        [DataMember]
        public virtual Decimal AllowableAmount { get; set; }

        [DataMember]
        public virtual ObservableCollection<ServiceCodeViewModel> ServiceCodes { get; set; }

    }

    [DataContract]
    public class ServiceCodeViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Timing { get; set; }

        [DataMember]
        public virtual bool IsTime { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DependsOn("Code", "Description")]
        public virtual string FormattedCodeDescription
        {
            get
            {
                if (string.IsNullOrEmpty(Code) && string.IsNullOrEmpty(Description)) return string.Empty;
                return Code + " - " + Description;
            }
        }
    }


}
