﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Soaf.Presentation;
using System.ComponentModel.DataAnnotations;
using Soaf.ComponentModel;
using IO.Practiceware.Validation;

namespace IO.Practiceware.Presentation.ViewModels.StandardExam
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class DiagnosisViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        public virtual DiagnosisCodeViewModel Code { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual bool IsQueuedToBill { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual string DiagnosisCodeWaterMark { get; set; }

        [DataMember]
        public virtual string PointerLetter { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }

        [DataMember]
        public virtual string Color { get; set; }

        [DataMember]
        public virtual ObservableCollection<DiagnosisCodeViewModel> DiagnosisCodes { get; set; }

        [DataMember]
        public virtual DiagnosisCodeViewModel Icd9Code { get; set; }

        [DataMember]
        public virtual bool IsLinked { get; set; }

        public string[] Alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
        public string[] Colors = { "#FF00CD7F", "#FF00CDCD", "#FF00AFFF", "#FF6478FF", "#FFBB78FF", "#FFF57AE8", "#FFDE6498", "#FFFA7776", "#FFEA7460", "#FFFDA46C", "#FFE5B844", "#FFC0D674" };

    }

    [DataContract]
    public class DiagnosisCodeViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        public virtual string FormattedCodeDescription { get { return Code + " - " + Description; } }

        [DataMember]
        public virtual string Side { get; set; }

        [DataMember]
        public virtual string Lingo { get; set; }

        [DataMember]
        public virtual string CodeNL { get; set; }

        [DataMember]
        public virtual string EyeLid { get; set; }
    }
}
