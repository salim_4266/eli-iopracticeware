﻿using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common
{
    [DataContract]
    public class PaymentMethodViewModel : NamedViewModel
    {
        [DataMember]
        public virtual bool IsReferenceRequired { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }
    }
}
