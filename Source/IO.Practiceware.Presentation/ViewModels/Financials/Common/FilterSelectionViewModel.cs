using System.Runtime.Serialization;
using System.Windows.Media;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common
{
    [DataContract]
    public class FilterSelectionViewModel : NamedViewModel
    {
        public virtual Brush Color { get; set; }
    }

}
