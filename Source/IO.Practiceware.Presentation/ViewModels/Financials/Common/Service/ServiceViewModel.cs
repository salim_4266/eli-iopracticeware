﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Service
{
    [EditableObject]
    [DataContract]
    public class ServiceViewModel : IViewModel
    {
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Empty strings not validating...
        /// </summary>
        [DataMember]
        [Required(ErrorMessage = "Please enter a code", AllowEmptyStrings = false)]
        public virtual String Code { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Please enter a description", AllowEmptyStrings = false)]
        public virtual String Description { get; set; }

        [DataMember]
        public virtual bool IsZeroChargeAllowedOnClaim { get; set; }

        [DataMember]
        public virtual decimal UnitFee { get; set; }

        /// <summary>
        /// The FormattedName should be formatted [Code] - [Description]
        /// </summary>
        [DependsOn("Code", "Description")]
        public virtual String FormattedName
        {
            get { return Code + " - " + Description; }
        }
    }
}
