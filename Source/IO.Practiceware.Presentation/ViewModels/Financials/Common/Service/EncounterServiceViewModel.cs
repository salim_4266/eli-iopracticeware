﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Service
{
    [DataContract]
    public class EncounterServiceViewModel : IViewModel
    {

        public const string AcceptableTextInputDoesNotContainTheseCharacters = @"^[^~:|^&\*]+$";

        /// <summary>
        /// Id of the encounter service
        /// </summary>
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// Invoice Id for grouping
        /// </summary>
        [DataMember]
        public virtual int InvoiceId { get; set; }

        /// <summary>
        /// Encounter date
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual DateTime Date { get; set; }

        [DataMember, DispatcherThread]
        public virtual ServiceViewModel Service { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> Modifiers { get; set; }

        [DataMember]
        public virtual Decimal Units { get; set; }

        [DataMember]
        public virtual Decimal Charge { get; set; }

        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        [DataMember, DispatcherThread]
        public virtual Decimal? Allowed { get; set; }

        /// <summary>
        /// The following are calculated values
        /// </summary>
        [DependsOn("Units", "Charge")]
        public virtual Decimal TotalCharge { get { return Math.Round((Units * Charge), 2); } }

        [DependsOn("Units", "Charge")]
        public virtual Decimal Balance { get { return -1 * TotalCharge; } }
    }

    public enum BillingActionType
    {
        [Display(Name = "Queue claim electronic")]
        QueueClaimElectronic,
        [Display(Name = "Queue claim paper")]
        QueueClaimPaper,
        [Display(Name = "Crossover claim")]
        CrossoverClaim,
        [Display(Name = "Claim wait")]
        ClaimWait,
        //[Display(Name = "Claim follow up")]
        //ClaimFollowUp,
        [Display(Name = "Queue statement")]
        QueueStatement,
        [Display(Name = "Statement wait")]    
        StatementWait,
        [Display(Name = "Leave unbilled")]
        LeaveUnbilled
    }
}
