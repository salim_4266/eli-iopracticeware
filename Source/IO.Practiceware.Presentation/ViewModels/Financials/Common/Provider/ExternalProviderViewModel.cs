﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider
{
    [DataContract]
    public class ExternalProviderViewModel : NamedViewModel
    {
        /// <summary>
        /// Eventually, we'll split out the "detail" area into a new dropdown control; for now, we'll use the same convention as is used on patient demographics referring provider selection 
        /// </summary>
        [DataMember]
        public virtual string Detail { get; set; }

        [DataMember]
        public virtual String Specialty { get; set; }

        [DataMember]
        public virtual PhoneNumberViewModel PhoneNumber { get; set; }

        [DataMember]
        public virtual AddressViewModel Address { get; set; }


        /// <summary>
        /// If the external provider has a non-empty DisplayName, use it for display purposes
        /// </summary>
        [DataMember]
        public virtual String DisplayName { get; set; }

        /// <summary>
        /// Otherwise, use the name values to create a FormattedName
        /// </summary>
        [DataMember]
        public virtual String FirstName { get; set; }

        [DataMember]
        public virtual String MiddleName { get; set; }

        [DataMember]
        public virtual String LastName { get; set; }

        [DataMember]
        public virtual String Honorific { get; set; }

        [DependsOn("LastName", "FirstName", "Honorific")]
        public virtual String FormattedName { get { return LastName + ", " + FirstName + " " + Honorific; } }
    }
}
