﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider
{
    [DataContract]
    [SupportsDataErrorInfo]
    [EditableObject]
    public class CareTeamViewModel : IViewModel
    {
        [DataMember]
        public virtual ObservableCollection<ExternalProviderViewModel> ReferringProviders { get; set; }

        [DataMember]
        public virtual NamedViewModel ScheduledProvider { get; set; }

        [DataMember]
        public virtual bool IsNotManualClaim { get; set; }

        [DataMember]
        [Required]
        public virtual ProviderViewModel RenderingProvider { get; set; }
    
    }

    [DataContract]
    public class ProviderViewModel : IViewModel
    {
        [DataMember]
        public virtual NamedViewModel Provider { get; set; }

        [DataMember]
        public virtual NamedViewModel BillingOrganization { get; set; }

        [DataMember]
        public virtual bool IsProviderUser { get; set; }
        public virtual String FormattedName
        {
            get
            {
                return new[]
                {
                    Provider.IfNotNull(u => u.Name), 
                    BillingOrganization.IfNotNull(bo => bo.Name)
                }.WhereNotDefault().Join(" - ");
            }
        }

        [DataMember]
        public virtual int Id { get; set; }
    }
}
