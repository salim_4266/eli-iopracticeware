﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    [DataContract]
    public class BillingTransactionViewModel : TransactionViewModel
    {
        /// <summary>
        /// Claim status & method; read-only, used in GetFormattedStatusString() & GetFormattedMethodString()
        /// displaying the description of a billing transaction on FinancialSummaryScreen
        /// </summary>
        [DataMember]
        public virtual BillingServiceTransactionStatus Status { get; set; }

        [DataMember]
        public virtual MethodSent Method { get; set; }

        /// <summary>
        /// The receiver of the claim (always an insurer)
        /// </summary>
        [DataMember]
        public virtual NamedViewModel Receiver { get; set; }

        /// <summary>
        /// A standard comment field
        /// </summary>
        [DataMember]
        public virtual String Comment { get; set; }

        /// <summary>
        /// Reference number to identify a specific transaction, i.e., check # on a check
        /// </summary>
        [DataMember]
        public virtual String Reference { get; set; }

        public String FormattedStatusString { get { return GetFormattedStatusString(Status, Method, Receiver); } }

        public String FormattedMethodString { get { return GetFormattedMethodString(Status); } }

        private static string GetFormattedStatusString(BillingServiceTransactionStatus status, MethodSent method, NamedViewModel receiver)
        {
            //If receiver id is 0 that means payer is patient.
            string statementAction = receiver.Id == 0 ? "Statement" : "Claim";

            if (status == BillingServiceTransactionStatus.SentCrossOver)
                return string.Format("{0} {1} {2}", method, "Crossover", statementAction);

            if (status == BillingServiceTransactionStatus.ClosedNotSent || status == BillingServiceTransactionStatus.ClosedSent || status == BillingServiceTransactionStatus.ClosedCrossOver)
                return string.Format("{0} {1} {2}", "Closed", method, statementAction);

            if (status == BillingServiceTransactionStatus.Wait)
                return statementAction;

            return string.Format("{0} {1}", method, statementAction);
        }

        private static String GetFormattedMethodString(BillingServiceTransactionStatus status)
        {
            if (status == BillingServiceTransactionStatus.Queued)
                return "queued to";

            if (status == BillingServiceTransactionStatus.Wait)
                return "held to";

            if (status == BillingServiceTransactionStatus.SentCrossOver || status == BillingServiceTransactionStatus.ClosedCrossOver)
                return "crossed over to";

            if (status == BillingServiceTransactionStatus.ClosedNotSent)
                return "not sent to";

            return "sent to";
        }

    }

    public enum BillingServiceTransactionStatus
    {
        Queued = 1,
        Sent = 2,
        Wait = 3,
        SentCrossOver = 4,
        ClosedNotSent = 5,
        ClosedSent = 6,
        ClosedCrossOver = 7,
    }

    public enum MethodSent
    {
        Electronic = 1,
        Paper = 2
    }
}
