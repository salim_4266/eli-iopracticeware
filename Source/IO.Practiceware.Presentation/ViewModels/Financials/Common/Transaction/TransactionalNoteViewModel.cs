﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    [DataContract]
    public class TransactionalNoteViewModel : TransactionViewModel
    {
        [DataMember]
        public virtual String Content { get; set; }

        [DataMember]
        public virtual DateTime LastEditedDateTime { get; set; }

        [DataMember]
        public virtual NamedViewModel LastEditedByUser { get; set; }

        [DataMember]
        public virtual bool IsArchived { get; set; }
    }
}
