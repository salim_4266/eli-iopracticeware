﻿namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    /// <summary>
    /// Well known types of ServiceTransactions
    /// </summary>
    public enum ServiceTransactionType
    {
        Billing = 1,

        Adjustment = 2,

        Information = 3,
    }



}
