﻿namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    public enum FinancialSourceType
    {
        Insurer = 1,
        Patient = 2,
        BillingOrganization = 3,
        ExternalOrganization = 4
    }
}
