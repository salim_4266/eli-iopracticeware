using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    [DataContract]
    [EditableObject]
    public abstract class FinancialTransactionTypeViewModel : NamedViewModel
    {
        /// <summary>
        /// The Group Code and Reason Code set for each adjustment type can be overridden by the user in the Post Adjustments screen
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual NamedViewModel<int> GroupCode { get; set; }

        [DataMember, DispatcherThread]
        public virtual NamedViewModel<int> ReasonCode { get; set; }

        [DependsOn("GroupCode", "ReasonCode")]
        public virtual String FormattedGroupCodeReasonCode
        {
            get
            {
                return new[]
                    {
                        GroupCode.IfNotNull(gc => gc.Name),
                        ReasonCode.IfNotNull(gc => gc.Name)
                    }.WhereNotDefault().Join(" - ");
            }
        }

        [DataMember]
        public virtual int OrdinalId { get; set; }
    }

    [DataContract]
    public class FinancialInformationTypeViewModel : FinancialTransactionTypeViewModel
    {
        
    }

    [DataContract]
    public class AdjustmentTypeViewModel : FinancialTransactionTypeViewModel
    {
        [DataMember]
        public virtual Boolean IsCash { get; set; }

        [DataMember]
        public virtual Boolean IsDebit { get; set; }
    }
}