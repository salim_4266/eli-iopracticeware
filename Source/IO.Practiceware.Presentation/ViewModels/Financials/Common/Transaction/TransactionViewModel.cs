﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    /// <summary>
    /// ServiceTransactions are created whenever various actions occur against a Service, such as a payment made or a claim sent.
    /// There are three types of ServiceTransactions: BillingTransactions, AdjustmentTransactions, and InformationalTransactions
    /// </summary>
    [DataContract]
    public class TransactionViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// Each transaction has a date associated with it; in many cases, this date can be specified by the user
        /// </summary>
        [DataMember]
        public virtual DateTime Date { get; set; }

        /// <summary>
        /// Transactions can have links to Services and Invoices.
        /// </summary>
        [DataMember]
        public virtual int? ServiceId { get; set; }

        [DataMember]
        public virtual int? InvoiceId { get; set; }

        [DataMember]
        public virtual Decimal Amount { get; set; }
    }
}
