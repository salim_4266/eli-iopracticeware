using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction
{
    [SupportsDataErrorInfo]
    [EditableObject]
    [DataContract]
    public class FinancialTransactionViewModel : TransactionViewModel
    {
        [DataMember]
        [Required(ErrorMessage = "Type is required")]
        public virtual FinancialTransactionTypeViewModel FinancialTransactionType { get; set; }

        /// <summary>
        /// Financial source type
        /// </summary>
        [DataMember]
        public virtual NamedViewModel Source { get; set; }

        /// <summary>
        /// Reference number to identify a specific transaction, i.e., check # on a check
        /// </summary>
        [DataMember]
        public virtual String Reference { get; set; }

        /// <summary>
        /// A standard comment field
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual String Comment { get; set; }

        [DataMember, DispatcherThread]
        public virtual Boolean IncludeCommentOnStatement { get; set; }

        /// <summary>
        /// Payer
        /// </summary>
        [DataMember]
        public virtual int? InvoiceReceivableId { get; set; }
    }

    public static class FinancialTransactionExtensions
    {
        private static readonly string FinancialTransactionAmountPropertyName = Reflector.GetMember<FinancialTransactionViewModel>(m => m.Amount).Name;

        /// <summary>
        /// Gets transaction amount before it has been modified in current custom named edit.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public static decimal GetPreviousAmount(this FinancialTransactionViewModel transaction)
        {
            var editableTransactionState = transaction.As<IEditableObjectExtended>();
            if (editableTransactionState == null) return transaction.Amount;

            // Attempt to read original value
            var originalValues = editableTransactionState.GetOriginalValuesForNamedEdit(EditableObjectExtensions.CustomEditName);
            var previousAmount = originalValues.ContainsKey(FinancialTransactionAmountPropertyName)
                ? (decimal)originalValues[FinancialTransactionAmountPropertyName]
                : transaction.Amount;
            return previousAmount;
        }

        /// <summary>
        /// Calculates total over specified transactions
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="isCash"><c>null</c> to calculate grand total or use it as filter</param>
        /// <param name="getAmountToSum"></param>
        /// <returns></returns>
        public static decimal CalculateTotal(this IEnumerable<FinancialTransactionViewModel> transactions, bool? isCash = null, Func<FinancialTransactionViewModel, decimal> getAmountToSum = null)
        {
            // Init default getter of amount to sum
            getAmountToSum = getAmountToSum ?? (t => t.Amount);

            return transactions.Sum(t =>
            {
                var type = t.FinancialTransactionType.As<AdjustmentTypeViewModel>();
                decimal amount = 0;
                if (type != null 
                    // Apply filter by IsCash
                    && (isCash == null || type.IsCash == isCash))
                {
                    amount = type.IsDebit
                        ? getAmountToSum(t) * -1
                        : getAmountToSum(t);
                }
                return amount;
            });
        }

        /// <summary>
        /// List transactions at the beginning of current custom edit.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static IEnumerable<FinancialTransactionViewModel> GetPreviousTransactions(this ExtendedObservableCollection<FinancialTransactionViewModel> collection)
        {
            var originalValues = collection.GetOriginalValuesForNamedEdit(EditableObjectExtensions.CustomEditName);

            return originalValues == null || !originalValues.ContainsKey("Items") 
                ? (IEnumerable<FinancialTransactionViewModel>) collection 
                : originalValues["Items"].CastTo<IEnumerable<FinancialTransactionViewModel>>().ToList();
        }
    }
}