﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common
{
    [DataContract]
    [EditableObject]
    public class NextPayerViewModel : IViewModel
    {
        /// <summary>
        /// Name of the payer (i.e., AETNA or LastName, FirstName), where payers can be 
        /// one of patient's active insurers or the patient him/herself
        /// 
        /// The common abbreviation associated with the next payer as defined throughout the software
        /// (i.e., M1, M2, V1, W1, etc.) 
        /// 
        /// The common abbreviation associated with a patient is "P"
        /// </summary>
        [DataMember]
        public virtual PayerViewModel Payer { get; set; }

        /// <summary>
        /// The relationship between the payer and the patient in context
        /// </summary>
        [DataMember]
        public virtual NamedViewModel Relationship { get; set; }

        [DependsOn("Payer", "Relationship")]
        public virtual string DisplayText 
        {
            get { return string.Format("{0} - {1}", Payer, Relationship); }
        }

        protected bool Equals(NextPayerViewModel other)
        {
            return Equals(Payer, other.Payer) && Equals(Relationship, other.Relationship);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((NextPayerViewModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Payer != null ? Payer.GetHashCode() : 0)*397) ^ (Relationship != null ? Relationship.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return DisplayText;
        }
    }

    /// <summary>
    /// Base class for payers
    /// </summary>
    [DataContract]
    public abstract class PayerViewModel : NameAndAbbreviationViewModel
    {
        protected bool Equals(PayerViewModel other)
        {
            // By default, payers match if id of the payer matches
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as PayerViewModel;
            return other != null && Equals(other);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    /// <summary>
    /// Insurer payer
    /// </summary>
    [DataContract]
    public class InsurerPayerViewModel : PayerViewModel
    {
        /// <summary>
        /// Allows to compare two insurer payers by display name (case insensitive)
        /// </summary>
        public static readonly IEqualityComparer<InsurerPayerViewModel> DisplayNameBasedComparer = EqualityComparer.For<InsurerPayerViewModel>(
            (x, y) => string.Equals(x.DisplayName, y.DisplayName, StringComparison.OrdinalIgnoreCase));

        [DataMember]
        public virtual string PlanName { get; set; }

        [DataMember]
        public virtual string Address { get; set; }

        /// <summary>
        /// Gets the full display name for insurer.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        [DependsOn("Name", "PlanName")]
        public virtual string DisplayName
        {
            get { return new [] { Name, PlanName }.Join(" - ", true); }
        }
    }

    /// <summary>
    /// Patient insurance based Insurer payer
    /// </summary>
    [DataContract]
    public class PatientInsurancePayerViewModel : InsurerPayerViewModel
    {
        [DataMember]
        public virtual NameAndAbbreviationViewModel InsuranceType { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }

        [DataMember]
        public virtual long PatientInsuranceId { get; set; }

        [DataMember]
        public virtual DateTime PolicyStartDate { get; set; }

        [DataMember]
        public virtual DateTime? PolicyEndDate { get; set; }

        [DataMember]
        public virtual int InsurancePolicyId { get; set; }

        [DataMember]
        public virtual int? ClaimFileReceiverId { get; set; }

        [DataMember]
        public virtual bool IsSecondaryClaimPaper { get; set; }

        [DataMember]
        public virtual bool SendZeroCharge { get; set; }

        protected bool Equals(PatientInsurancePayerViewModel other)
        {
            return PatientInsuranceId == other.PatientInsuranceId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PatientInsurancePayerViewModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return PatientInsuranceId.GetHashCode();
            }
        }
    }

    [DataContract]
    public class OfficePayerViewModel : PayerViewModel
    {
        public OfficePayerViewModel()
        {
            Name = "Office";
        }
    }

    /// <summary>
    /// Patient payer. Id = Patient.Id, Name = Patient.FullName
    /// </summary>
    [DataContract]
    public class PatientPayerViewModel : PayerViewModel
    {
    }

    public static class NextPayers
    {
        /// <summary>
        /// Aligns ordinal ids of next payers to be sequential (in case not all insurances are shown)
        /// </summary>
        /// <param name="nextPayers"></param>
        /// <returns></returns>
        public static ICollection<NextPayerViewModel> FixOrdinalIds(this IEnumerable<NextPayerViewModel> nextPayers)
        {
            // Create a copy
            var result = nextPayers.Select(p => p.Clone()).ToExtendedObservableCollection();

            // Fix display name
            var insurerPayers = result.Where(p => p.Payer is PatientInsurancePayerViewModel).ToList();
            foreach (var insurerPayer in insurerPayers)
            {
                NextPayerViewModel payer = insurerPayer;
                var fixedOrdinalId = insurerPayers
                    .Where(p => p.Payer.CastTo<PatientInsurancePayerViewModel>().InsuranceType.Id == payer.Payer.CastTo<PatientInsurancePayerViewModel>().InsuranceType.Id)
                    .IndexOf(p => Equals(p, payer)) + 1;

                payer.Payer.Abbreviation = payer.Payer.CastTo<PatientInsurancePayerViewModel>().InsuranceType.Abbreviation + fixedOrdinalId;
            }

            return result;
        }
    }
}
