﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis
{

    [DataContract]
    [EditableObject]
    public class DiagnosisViewModel : IViewModel
    {
        /// <summary>
        /// The IsLinked bool determines the background color of the diagnosis in the
        /// EditDiagnosisTemplate (used in EditInvoiceView.xaml) and DiagnosisTemplate 
        /// (used in PatientFinancialSummaryView.xaml); it's true when the encounter diagnosis has
        /// been linked to at least one service (i.e., a pointer from a service to the encounter diagnosis exists).
        /// </summary>
        [DataMember]
        public virtual bool IsLinked { get; set; }

        [DataMember]
        public virtual DiagnosisCodeViewModel ActiveDiagnosisCode { get; set; }

        [DataMember]
        public virtual DiagnosisCodeViewModel AlternateDiagnosisCode { get; set; }

        [DataMember]
        public virtual ObservableCollection<DiagnosisCodeViewModel> ActiveDiagnosisCodes { get; set; }

        [DataMember]
        public virtual DiagnosisLinkPointerViewModel DiagnosisLinkPointer { get; set; }
    }

    [DataContract]
    [EditableObject]
    public class DiagnosisCodeViewModel : IViewModel
    {
        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual DiagnosisCodeSet CodeSet { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Key { get; set; }

        public virtual string CodeDescription { get { return Code + " - " + Description; } }
    }

    [DataContract]
    [EditableObject]
    public class DiagnosisLinkPointerViewModel : IViewModel
    {
        protected bool Equals(DiagnosisLinkPointerViewModel other)
        {
            return string.Equals(PointerLetter, other.PointerLetter) && string.Equals(DiagnosisColor, other.DiagnosisColor);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((PointerLetter != null ? PointerLetter.GetHashCode() : 0) * 397) ^ (DiagnosisColor != null ? DiagnosisColor.GetHashCode() : 0);
            }
        }

        [DataMember, DispatcherThread]
        public virtual String PointerLetter { get; set; }

        [DataMember, DispatcherThread]
        public virtual String DiagnosisColor { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DiagnosisLinkPointerViewModel)obj);
        }
    }

    [DataContract]
    public enum DiagnosisCodeSet
    {
        [EnumMember]
        Icd9 = 1,
        [EnumMember]
        Icd10 = 2
    }
}
