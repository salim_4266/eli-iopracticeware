﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Autoposting
{
    [DataContract]
    [Expression("!HasErrors", ErrorMessageExpression = @"AllMessages")]
    public class RemittanceMessageViewModel : IViewModel
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember, Dependency]
        public virtual List<RemittanceAdviceViewModel> RemittanceAdvices { get; set; }

        [DataMember]
        public virtual bool AreRemittanceAdvicesLoaded { get; set; }

        [DataMember]
        public virtual bool IsProcessed { get; set; }

        [DataMember]
        public virtual string FileName { get; set; }

        [DataMember]
        public virtual DateTime PostingDate { get; set; }

        [DataMember]
        public virtual string Errors { get; set; }

        [DependsOn("Errors")]
        public virtual bool HasErrors
        {
            get 
            { 
                return !string.IsNullOrEmpty(Errors) 
                    || RemittanceAdvices.IfNotNull(ras => ras.Any(ra => ra.HasErrors)); 
            }
        }

        [DependsOn("RemittanceAdvices.Item.Informations", "Errors", "RemittanceAdvices.Item.Errors")]
        public virtual string AllMessages
        {
            get
            {
                return Errors.CreateEnumerable()
                    .Union(RemittanceAdvices.IfNotNull(ras => ras.Select(ra => ra.Errors), new string[0]))
                    .Union(RemittanceAdvices.IfNotNull(ras => ras.Select(ra => ra.Informations), new string[0]))
                    .Join(Environment.NewLine, true);
            }
        }
    }
}
