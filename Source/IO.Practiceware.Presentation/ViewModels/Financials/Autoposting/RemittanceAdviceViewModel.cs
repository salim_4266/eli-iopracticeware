﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Autoposting
{
    [DataContract]
    [Expression("!HasErrors", ErrorMessageExpression = @"Errors")]
    public class RemittanceAdviceViewModel : IViewModel
    {
        [Required]
        [DataMember]
        public virtual String InsurerName { get; set; }

        [DataMember]
        public virtual DateTime RemittanceDate { get; set; }

        [DataMember]
        public virtual String RemittanceCode { get; set; }

        [DataMember]
        public virtual Decimal TotalAmount { get; set; }

        [DataMember]
        public virtual string Errors { get; set; }

        [DataMember]
        public virtual string Informations { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has errors.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has errors; otherwise, <c>false</c>.
        /// </value>
        [DependsOn("Errors")]
        public virtual bool HasErrors
        {
            get { return !string.IsNullOrEmpty(Errors); }
        }
    }
}