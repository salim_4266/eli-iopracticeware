using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Autoposting
{
    [DataContract]
    public class RemittanceMessagesLoadInformation
    {
        [DataMember]
        public ExtendedObservableCollection<PaymentMethodViewModel> PaymentMethods { get; set; }

        [DataMember, Dependency]
        public ExtendedObservableCollection<RemittanceMessageViewModel> Remittances { get; set; }
    }
}