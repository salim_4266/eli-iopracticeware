using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.Autoposting
{
    [DataContract]
    public class RemittanceMessageUploadArgument
    {
        [DataMember]
        public virtual string FileName { get; set; }

        [DataMember]
        public virtual DateTime PostingDate { get; set; }

        [DataMember]
        public virtual string Message { get; set; }
    }
}