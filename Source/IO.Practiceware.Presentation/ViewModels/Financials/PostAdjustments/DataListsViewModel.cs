﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    public class DataListsViewModel : IViewModel
    {
        [DataMember, Dependency]
        public virtual ObservableCollection<PaymentMethodViewModel> PaymentMethods { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> BillingActions { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<FinancialTransactionTypeViewModel> FinancialTransactionTypes { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> GroupCodes { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> ReasonCodes { get; set; }
    }
}
