using System.Collections.Generic;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    public class PostAdjustmentsResult
    {
        [DataMember]
        public int PaymentInstrumentId { get; set; }

        [DataMember, Dependency]
        public IList<InvoiceValidationErrors> Errors { get; set; }

        [DataMember, Dependency]
        public IList<AccountViewModel> PostedAccounts { get; set; }

        [DataContract]
        public class InvoiceValidationErrors
        {
            [DataMember]
            public int InvoiceId { get; set; }

            [DataMember]
            public ValidationMessage[] Messages { get; set; }
        }
    }
}