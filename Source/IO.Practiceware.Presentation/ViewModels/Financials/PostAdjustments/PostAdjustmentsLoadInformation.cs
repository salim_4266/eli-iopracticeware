using System.Collections.Generic;
using System.Runtime.Serialization;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    public class PostAdjustmentsLoadInformation
    {
        [DataMember]
        public DataListsViewModel DataLists { get; set; }

        [DataMember]
        public PaymentInstrumentViewModel PaymentInstrument { get; set; }

        [DataMember, Dependency]
        public ExtendedObservableCollection<AccountViewModel> Accounts { get; set; }

        /// <summary>
        /// A list of InsurerId-s which are available for specified InvoiceId, when it is passed as argument to Load method
        /// </summary>
        [DataMember, Dependency]
        public List<int> InvoiceSelectedInsurerIds { get; set; }
    }
}