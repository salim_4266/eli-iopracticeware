using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    public class BalanceForwardSuggestionInformation
    {
        [DataMember]
        public virtual int AccountId { get; set; }
        
        [DataMember]
        public virtual int BillingServiceId { get; set; }

        /// <summary>
        /// Insurer, patient or office
        /// </summary>
        [DataMember]
        public virtual NextPayerViewModel To { get; set; }

        /// <summary>
        /// One of billing action types
        /// </summary>
        [DataMember]
        public virtual NamedViewModel Action { get; set; }
    }
}