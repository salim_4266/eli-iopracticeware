﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PaymentInstrumentViewModel : IViewModel
    {
        /// <summary>
        /// Financial batch Id
        /// </summary>
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// Patient, Insurer or BO
        /// </summary>
        [DataMember]
        public virtual FinancialSourceType FinancialSourceType { get; set; }

        [RequiredWithDisplayName(ErrorMessage = "A payment date is required.", AllowDefaultValuesForValueTypes = false)]
        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual DateTime? CheckDate { get; set; }

        [DataMember]
        [RequiredIf("IsNonOfficeFinancialSource", ErrorMessage = "Payment method is required")]
        public virtual PaymentMethodViewModel PaymentMethod { get; set; }

        /// <summary>
        /// Patient, Insurer or Office
        /// </summary>
        [DataMember]
        [RequiredIf("IsNonOfficeFinancialSource", ErrorMessage = "Payer is required")]
        public virtual PayerViewModel Payer { get; set; }

        [DataMember]
        public virtual Decimal PaymentAmount { get; set; }

        [DataMember]
        [RegexMatch(@"^[^~:|^&\*]+$", "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        [RequiredIf("PaymentMethodRequiresReference", ErrorMessage = "Fill in reference code")]
        public virtual String ReferenceNumber { get; set; }

        /// <summary>
        /// If the invoice in context has no InvoiceReceivables with PatientInsurances, greys out the Insurer choice.
        /// </summary>
        [DataMember]
        public virtual bool InsurerSourceSupported { get; set; }

        /// <summary>
        /// Disbursed paid amount of hidden accounts associated with this payment instrument
        /// </summary>
        [DataMember]
        public virtual decimal FilteredAccountsPaidAmount { get; set; }

        /// <summary>
        /// Disbursed adjusted amount of hidden accounts associated with this payment instrument
        /// </summary>
        [DataMember]
        public virtual decimal FilteredAccountsAdjustedAmount { get; set; }

        /// <summary>
        /// Gets or sets the selected filter of invoice services.
        /// </summary>
        [DataMember]
        public virtual InvoiceServicesFilterType SelectedInvoiceServicesFilter { get; set; }

        /// <summary>
        /// The following three values are calculated
        /// </summary>
        [DependsOn("AccountsInContext.Item.TotalPaid")]
        public virtual Decimal DisbursedPaidAmount
        {
            get
            {
                return AccountsInContext == null
                    ? FilteredAccountsPaidAmount
                    : AccountsInContext.Sum(a => a.TotalPaid) + FilteredAccountsPaidAmount;
            }
        }

        [DependsOn("AccountsInContext.Item.TotalAdjusted")]
        public virtual Decimal DisbursedAdjustedAmount
        {
            get
            {
                return AccountsInContext == null
                    ? FilteredAccountsAdjustedAmount
                    : AccountsInContext.Sum(a => a.TotalAdjusted) + FilteredAccountsAdjustedAmount;
            }
        }

        [DependsOn("DisbursedPaidAmount", "PaymentAmount")]
        public virtual Decimal RemainingPaymentBalance
        {
            get { return PaymentAmount - DisbursedPaidAmount; }
        }

        /// <summary>
        /// Accounts used when calculating totals
        /// </summary>
        [IgnoreChangeTracking]
        [IgnorePropertyChanges]
        public virtual ExtendedObservableCollection<AccountViewModel> AccountsInContext { get; set; }

        #region Validation

        [DependsOn("FinancialSourceType")]
        public virtual bool IsNonOfficeFinancialSource
        {
            get 
            { 
                return FinancialSourceType != FinancialSourceType.BillingOrganization
                    && FinancialSourceType != FinancialSourceType.ExternalOrganization; 
            }
        }

        [DependsOn("PaymentMethod.IsReferenceRequired")]
        public virtual bool PaymentMethodRequiresReference
        {
            get { return PaymentMethod.IfNotNull(pm => pm.IsReferenceRequired); }
        }

        #endregion

        public void SetAccountsInContext(ExtendedObservableCollection<AccountViewModel> accounts)
        {
            AccountsInContext = accounts;
        }
    }

}
