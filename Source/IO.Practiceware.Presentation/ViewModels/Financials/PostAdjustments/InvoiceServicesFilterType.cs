﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    public enum InvoiceServicesFilterType
    {
        [Display(Name = "Services out to the current payer")]
        OpenToNextPayer,
        [Display(Name = "Services with an open balance")]
        AllOpen,
        [Display(Name = "All services")]
        AllServices
    }
}