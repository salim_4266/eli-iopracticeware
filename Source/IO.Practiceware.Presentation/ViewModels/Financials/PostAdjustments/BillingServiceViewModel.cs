﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class BillingServiceViewModel : EncounterServiceViewModel
    {
        private static readonly BillingActionType[] InsurerOnlyActions =
        {
            BillingActionType.QueueClaimElectronic, BillingActionType.QueueClaimPaper, 
            BillingActionType.ClaimWait, BillingActionType.CrossoverClaim
        };
        private static readonly BillingActionType[] PatientOnlyActions =
        {
            BillingActionType.QueueStatement, BillingActionType.StatementWait
        };

        private FinancialTransactionViewModel _newTransaction;
        private IList<NamedViewModel> _billingActions;
        private IList<NextPayerViewModel> _nextPayersSelection;
        private decimal _previousBalance;
        private readonly Func<FinancialTransactionViewModel> _createNewTransaction;

        public BillingServiceViewModel()
        {}

        public BillingServiceViewModel(Func<FinancialTransactionViewModel> createNewTransaction)
        {
            _createNewTransaction = createNewTransaction;
        }

        [DataMember, Dependency]
        public virtual ExtendedObservableCollection<FinancialTransactionViewModel> Transactions { get; set; }

        /// <summary>
        /// Total amount of not shown transactions for this service to properly calculate it's balance
        /// </summary>
        [DataMember]
        public virtual Decimal FilteredAdjustmentsTotal { get; set; }

        #region Set on UI

        /// <summary>
        /// The billing action value will determine the billing logic for sending the resulting claim of 
        /// the current Encounter
        /// 
        /// If a NextPayer is specified, a valid BillingAction (valid for that payer) must also be specified
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual NamedViewModel BillingAction { get; set; }

        [DataMember, DispatcherThread]
        public virtual NextPayerViewModel NextPayer { get; set; }

        /// <summary>
        /// Returns only active next payers for current encounter service. The whole selection must be provided earlier.
        /// </summary>
        [IgnoreChangeTracking]
        public virtual IList<NextPayerViewModel> NextPayersSelection
        {
            get { return _nextPayersSelection; }
        }

        /// <summary>
        /// Returns a list of billing actions valid for selected next payer.
        /// </summary>
        /// <value>
        /// The billing actions selection.
        /// </value>
        [IgnoreChangeTracking]
        [DependsOn("NextPayer")]
        public virtual IList<NamedViewModel> BillingActionsSelection
        {
            get
            {
                if (_billingActions == null || NextPayer == null)
                    return _billingActions;

                var result = _billingActions;
                if (NextPayer.Payer is PatientPayerViewModel)
                {
                    result = result.Where(a => !InsurerOnlyActions.Contains((BillingActionType)a.Id)).ToList();
                }
                else if (NextPayer.Payer is InsurerPayerViewModel)
                {
                    result = result.Where(a => !PatientOnlyActions.Contains((BillingActionType)a.Id)).ToList();
                }
                return result;
            }
        }

        /// <summary>
        /// Balance before any changes made on the screen
        /// </summary>
        public virtual decimal PreviousBalance
        {
            get { return _previousBalance; }
        }

        /// <summary>
        /// Used to manually add new transactions via UI
        /// </summary>
        [DependsOn("Transactions")]
        public virtual FinancialTransactionViewModel NewTransaction
        {
            get
            {
                if (_newTransaction == null
                    || Transactions.Any(x => ReferenceEquals(x, _newTransaction)))
                {
                    _newTransaction = _createNewTransaction();
                }

                return _newTransaction;
            }
        }

        /// <summary>
        /// States whether it's a fake encounter service record (we don't create encounter services on post adjustments screen)
        /// </summary>
        [DependsOn("Id")]
        public virtual bool IsNoEncounterServiceMarker
        {
            get { return Id == null; }
        }

        #endregion

        /// <summary>
        /// Current calculated balance
        /// </summary>
        [DependsOn("Units", "Charge", "Transactions", "FilteredAdjustmentsTotal")]
        public override decimal Balance
        {
            get
            {
                return TotalCharge - (Transactions.CalculateTotal() + FilteredAdjustmentsTotal);
            }
        }

        [DependsOn("Modifiers")]
        public virtual String FormattedModifiers 
        {
            get
            {
                var result = Modifiers.IfNotNull(ms =>
                    string.Join(" ", ms.Select(m => m.Abbreviation)),
                    string.Empty);

                return result;
            }
        }

        public void InitChoices(IList<NextPayerViewModel> nextPayers, IList<NamedViewModel> billingActions)
        {
            _billingActions = billingActions;

            // Filter
            var activePayers = nextPayers
                .Where(np =>
                {
                    // Pass through non-insurer payers
                    var insurerPayer = np.Payer as PatientInsurancePayerViewModel;
                    if (insurerPayer == null) return true;

                    // Check that insurance policy applies for current encounter
                    var isValidInsurance = insurerPayer.PolicyStartDate <= Date
                                           && (insurerPayer.PolicyEndDate == null || insurerPayer.PolicyEndDate >= Date);
                    return isValidInsurance;
                })
                .ToArray();

            _nextPayersSelection = activePayers.FixOrdinalIds().ToList();
        }

        public void SetPreviousBalance()
        {
            _previousBalance = Balance;
        }
    }
}
