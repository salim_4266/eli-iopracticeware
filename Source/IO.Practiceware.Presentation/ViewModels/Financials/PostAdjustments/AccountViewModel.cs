﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class AccountViewModel : IViewModel
    {
        private readonly Func<FinancialTransactionViewModel> _createNewTransaction;
        private FinancialTransactionViewModel _newTransaction;

        public AccountViewModel()
        {}

        public AccountViewModel(Func<FinancialTransactionViewModel> createNewTransaction)
        {
            _createNewTransaction = createNewTransaction;
        }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual DateTime? Birthdate { get; set; }

        [DataMember]
        public virtual string Email { get; set; }

        [DataMember]
        public virtual string Address { get; set; }

        [DataMember]
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// Balance of this account at the time of loading
        /// </summary>
        [DataMember]
        public virtual decimal Balance { get; set; }

        /// <summary>
        /// Specifies payer to be used when selected payer on payment instrument is not among eligible payers for this account
        /// </summary>
        [DataMember]
        [RequiredIf("RequiresOverride", ErrorMessage = "Selected payer is not listed as one of patient insurers")]
        public virtual PayerViewModel PayerOverride { get; set; }

        /// <summary>
        /// Note: On account financial transactions can only be of type IsCash = true
        /// </summary>
        [DataMember, Dependency]
        [Expression("IsNonNegativeOnAccountBalance", ErrorMessage = "The amount distributed cannot be greater than the amount on-account")]
        public virtual ExtendedObservableCollection<FinancialTransactionViewModel> OnAccountTransactions { get; set; }

        [DataMember, Dependency]
        public virtual ExtendedObservableCollection<BillingServiceViewModel> BillingServices { get; set; }

        /// <summary>
        /// Full selection of next payers: either non-deleted patient insurances or patient contacts
        /// </summary>
        [DataMember, Dependency]
        public virtual ExtendedObservableCollection<NextPayerViewModel> NextPayers { get; set; }

        [DataMember, Dependency]
        public virtual ExtendedObservableCollection<FinancialTransactionViewModel> UnassignedTransactions { get; set; }

        /// <summary>
        /// Total paid amount of adjustments for filtered out invoices
        /// </summary>
        [DataMember]
        public virtual decimal FilteredInvoicesTotalPaid { get; set; }

        /// <summary>
        /// Total adjusted amount of adjustments for filtered out invoices
        /// </summary>
        [DataMember]
        public virtual decimal FilteredInvoicesTotalAdjusted { get; set; }

        /// <summary>
        /// Used to manually add new on-account transactions via UI
        /// </summary>
        [DependsOn("OnAccountTransactions")]
        public virtual FinancialTransactionViewModel NewOnAccountTransaction
        {
            get
            {
                if (_newTransaction == null
                    || OnAccountTransactions.Any(x => ReferenceEquals(x, _newTransaction)))
                {
                    _newTransaction = _createNewTransaction();
                }   

                return _newTransaction;
            }
        }

        [DependsOn("BillingServices.Item.Transactions", "OnAccountTransactions", "UnassignedTransactions", "FilteredInvoicesTotalPaid")]
        public virtual decimal TotalPaid
        {
            get { return CalculateTransactionsTotal(true); }
        }

        [DependsOn("BillingServices.Item.Transactions", "OnAccountTransactions", "UnassignedTransactions", "FilteredInvoicesTotalAdjusted")]
        public virtual decimal TotalAdjusted
        {
            get { return CalculateTransactionsTotal(false); }
        }

        [DependsOn("BillingServices.Item.Transactions", "OnAccountTransactions")]
        public virtual decimal NewPaidAmount 
        {
            get { return CalculateTotalChange(true); }
        }

        [DependsOn("BillingServices.Item.Transactions", "OnAccountTransactions")]
        public virtual decimal NewAdjustedAmount
        {
            get { return CalculateTotalChange(false); }
        }

        [DependsOn("PaymentInstrumentInContext.Payer", "PaymentInstrumentInContext.FinancialSourceType", "NextPayers")]
        public virtual bool RequiresOverride
        {
            get
            {
                if (PaymentInstrumentInContext == null) return false;

                var requires = PaymentInstrumentInContext.FinancialSourceType == FinancialSourceType.Insurer
                    && PaymentInstrumentInContext.Payer != null
                    && NextPayers
                        .Select(np => np.Payer)
                        .OfType<InsurerPayerViewModel>()
                        .All(p => !NamedViewModel<int>.NameBasedComparer.Equals(p, PaymentInstrumentInContext.Payer));
                return requires;
            }
        }

        [DependsOn("PaymentInstrumentInContext.FinancialSourceType", "NextPayers")]
        public virtual ObservableCollection<InsurerPayerViewModel> OverridePayers
        {
            get
            {
                if (PaymentInstrumentInContext == null 
                    || PaymentInstrumentInContext.FinancialSourceType != FinancialSourceType.Insurer)
                {
                    return new ObservableCollection<InsurerPayerViewModel>();
                }

                var result = NextPayers
                    .Select(np => np.Payer)
                    .OfType<InsurerPayerViewModel>()
                    .Distinct(NamedViewModel<int>.NameBasedComparer)
                    .Cast<InsurerPayerViewModel>()
                    .OrderBy(p => p.Name)
                    .ToObservableCollection();

                return result;
            }
        }

        [DependsOn("PaymentInstrumentInContext.SelectedInvoiceServicesFilter",
            "PaymentInstrumentInContext.Payer", "BillingServices.Count")]
        public virtual ExtendedObservableCollection<BillingServiceViewModel> FilteredBillingServices 
        {
            get
            {
                switch (PaymentInstrumentInContext.SelectedInvoiceServicesFilter)
                {
                    case InvoiceServicesFilterType.OpenToNextPayer:
                        var nextPayer = PaymentInstrumentInContext.Payer;
                        return BillingServices
                            .Where(bs => bs.PreviousBalance != 0)
                            .Where(bs => nextPayer == null || (bs.NextPayersSelection.Any(np => NamedViewModel<int>.NameBasedComparer.Equals(np.Payer, nextPayer))))
                            .ToExtendedObservableCollection();
                    case InvoiceServicesFilterType.AllOpen:
                        return BillingServices.Where(bs => bs.PreviousBalance != 0).ToExtendedObservableCollection();
                    case InvoiceServicesFilterType.AllServices:
                        return BillingServices;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        [IgnoreChangeTracking]
        [IgnorePropertyChanges]
        public virtual PaymentInstrumentViewModel PaymentInstrumentInContext { get; set; }

        /// <summary>
        /// Lazy loaded when shown on UI
        /// </summary>
        [IgnoreChangeTracking]
        [DispatcherThread]
        public virtual byte[] Photo { get; set; }

        public virtual bool IsNonNegativeOnAccountBalance
        {
            get { return OnAccountTransactions.All(t => t.Amount >= 0); }
        }

        private decimal CalculateTotalChange(bool isCash)
        {
            var currentTransactionsTotal = BillingServices
                .SelectMany(es => es.Transactions)
                .Concat(OnAccountTransactions)
                .CalculateTotal(isCash);

            var originalTransactionsTotal = BillingServices
                .SelectMany(es => es.Transactions.GetPreviousTransactions())
                .Concat(OnAccountTransactions.GetPreviousTransactions())
                .CalculateTotal(isCash, t => t.GetPreviousAmount());

            var amount = currentTransactionsTotal - originalTransactionsTotal;
            return amount;
        }

        public decimal CalculateTransactionsTotal(bool isCash)
        {
            var transactions = BillingServices
                .SelectMany(es => es.Transactions)
                .Concat(OnAccountTransactions)
                .ToList();

            var amount = transactions.CalculateTotal(isCash);
            
            // Include filtered totals
            amount += isCash ? FilteredInvoicesTotalPaid : FilteredInvoicesTotalAdjusted;

            return amount;
        }

        /// <summary>
        /// Required to enable functionality related to payer overrides
        /// </summary>
        /// <param name="paymentInstrument"></param>
        public void SetPaymentInstrumentInContext(PaymentInstrumentViewModel paymentInstrument)
        {
            PaymentInstrumentInContext = paymentInstrument;
        }
    }
}
