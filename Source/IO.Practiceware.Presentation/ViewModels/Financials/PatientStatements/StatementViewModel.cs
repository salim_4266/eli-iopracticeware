﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PatientStatements
{
    [DataContract]
    public class StatementViewModel : IViewModel
    {
        private MemoryStream _stream;
        private byte[] _value;

        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual byte[] Value
        {
            get { return _value; }
            set
            {
                _value = value;
                if (_value != null && _value.Length > 0)
                {
                    _stream = new MemoryStream(_value);
                }
                else
                {
                    _stream = null;
                }
            }
        }

        [DataMember]
        public virtual decimal Amount { get; set; }

        public virtual MemoryStream Stream
        {
            get
            {
               return _stream;
            }
        }
    }
}
