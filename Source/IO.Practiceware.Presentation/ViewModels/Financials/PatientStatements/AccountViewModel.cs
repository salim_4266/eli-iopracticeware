﻿
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.PatientStatements
{
    [DataContract]
    public class AccountViewModel : NamedViewModel
    {
        [DataMember]
        public ObservableCollection<StatementViewModel> Statements { get; set; }
    }
}
