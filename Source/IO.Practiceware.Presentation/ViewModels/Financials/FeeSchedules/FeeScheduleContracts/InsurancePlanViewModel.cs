﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{

    [DataContract]
    public class DetailedInsurerViewModel : IViewModel
    {
        /// <summary>
        /// Note: AutoGenerateField DisplayAttribute doesn't seem to work with DataGrids (they do work 
        /// with telerik:RadGridViews though) - we'll need a way to address this
        /// </summary>
        [DisplayAttribute(AutoGenerateField = false)]
        [DataMember]
        public virtual int Id { get; set; }

        [DisplayAttribute(AutoGenerateField = true, Order = 1)]
        [DataMember]
        public virtual String InsurerName { get; set; }

        [DisplayAttribute(AutoGenerateField = true, Order = 2)]
        [DataMember]
        public virtual String PlanName { get; set; }
        [DisplayAttribute(AutoGenerateField = true, Order = 3)]
        [DataMember]
        public virtual NamedViewModel PlanType { get; set; }

        [DisplayAttribute(AutoGenerateField = true, Order = 4)]
        [DataMember]
        public virtual AddressViewModel Address { get; set; }

        [DisplayAttribute(AutoGenerateField = true, Order = 5)]
        [DataMember]
        public virtual PhoneNumberViewModel PhoneNumber { get; set; }
    }
}
