﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{
    [DataContract]
    public class DataListsViewModel : IViewModel
    {
        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<DetailedInsurerViewModel> Insurers { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Providers { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }
    }
}
