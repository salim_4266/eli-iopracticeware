﻿using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class FeeScheduleContractViewModel : IViewModel
    {
        [DispatcherThread]
        [DataMember]
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Please enter a Fee Schedule Name")]
        [DispatcherThread]
        [DataMember]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Please select a start date")]
        [EqualToLessThan("EndDate", ErrorMessage = "Start date can't be greater than the end date."), DispatcherThread]
        [DataMember]
        public virtual DateTime StartDate { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual String Description { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual Decimal? PaidPercent { get; set; }

        [EqualToGreaterThan("StartDate", ErrorMessage = "End date can't be before the start date."), DispatcherThread]
        [DataMember]
        public virtual DateTime? EndDate { get; set; }

        [DependsOn("EndDate"), DispatcherThread]
        public virtual bool IsExpired { get { return EndDate.IsBeforeToday(); } }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<InsurerFeeScheduleContractViewModel> InsurerContracts { get; set; }

        [DependsOn("Name", "StartDate", "EndDate"), DispatcherThread]
        public virtual bool IsValidRecord
        {
            get
            {
                return this.IsValid();
            }
        }
    }
}
