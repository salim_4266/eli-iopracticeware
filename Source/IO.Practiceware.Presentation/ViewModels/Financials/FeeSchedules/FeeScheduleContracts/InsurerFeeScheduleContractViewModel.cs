﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{
    [DataContract]
    public class InsurerFeeScheduleContractViewModel : IViewModel
    {
        [DataMember]
        public DetailedInsurerViewModel Insurer { get; set; }

        /// <summary>
        /// If location is null, we'll consider the insurer contracted for all locations documented in the db
        /// To reflect this in the UI, we'll display "Any Location" when Location is null (currently handled by a xaml converter)
        /// </summary>
        [DataMember]
        public NamedViewModel ServiceLocation { get; set; }
        /// <summary>
        /// If provider is null, we'll consider the insurer contracted for all providers documented in the db
        /// To reflect this in the UI, we'll display "Any Provider" when Provider is null (currently handled by a xaml converter)
        /// </summary>
        [DataMember]
        public NamedViewModel Provider { get; set; }
    }
}
