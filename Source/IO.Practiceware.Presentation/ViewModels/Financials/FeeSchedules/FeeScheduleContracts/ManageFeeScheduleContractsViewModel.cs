﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class ManageFeeScheduleContractsViewModel : IViewModel
    {
        [DispatcherThread]
        [DataMember]
        public virtual FeeScheduleContractsViewModel FeeScheduleContractsView { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual DataListsViewModel DataLists { get; set; }
    }
}
