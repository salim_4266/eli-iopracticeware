﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class FeeScheduleContractsViewModel : IViewModel
    {
        [DispatcherThread]
        [DataMember]
        public virtual string NewFeeScheduleName { get; set; }

        [DispatcherThread]
        [DataMember, ValidateObject]
        public virtual ObservableCollection<FeeScheduleContractViewModel> Contracts { get; set; }
    }
}
