﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.Services
{
    [DataContract]
    public class ServiceDetailsViewModel : ServiceViewModel
    {
        [DataMember]
        public virtual NamedViewModel RelativeValueUnitCode { get; set; }

        [DataMember]
        public virtual NamedViewModel UnitType { get; set; }

        [DataMember]
        public virtual Decimal Units { get; set; }

        [DataMember]
        public virtual String NdcCode { get; set; }

        [DataMember]
        public virtual String RevenueCode { get; set; }

        [DataMember]
        public virtual NamedViewModel InvoiceType { get; set; }

        [DataMember]
        public virtual String UnclassifiedNote { get; set; }

        [DataMember]
        public virtual DateTime StartDate { get; set; }

        [DataMember]
        public virtual DateTime EndDate { get; set; }

        [DataMember]
        public virtual Boolean OrderingProviderRequired { get; set; }

        [DataMember]
        public virtual Boolean ReferringProviderRequired { get; set; }

        [DataMember]
        public virtual Boolean CliaWaiverRequired { get; set; }

        [DataMember]
        public virtual Boolean AllowZeroAmount { get; set; }

    }
}
