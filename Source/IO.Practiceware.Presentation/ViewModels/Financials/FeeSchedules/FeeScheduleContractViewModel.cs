﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules
{
    [DataContract]
    public class FeeScheduleContractViewModel : NamedViewModel
    {
        [DataMember]
        public virtual DateTime? EndDate { get; set; }

        public virtual bool IsExpired { get { return EndDate.IsBeforeToday(); } }
    }
}
