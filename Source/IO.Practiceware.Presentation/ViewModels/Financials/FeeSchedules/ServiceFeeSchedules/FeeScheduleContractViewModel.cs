﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules.Fees;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules
{
    [DataContract]
    public class FeeScheduleContractViewModel : FeeSchedules.FeeScheduleContractViewModel 
    {
        [DataMember]
        public virtual ObservableCollection<FeeViewModel> Fees { get; set; }
    }
}
