﻿using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules.Fees
{
    [DataContract]
    public class FeeViewModel : IViewModel
    {
        [DataMember]
        public virtual int ServiceId { get; set; }

    }
}
