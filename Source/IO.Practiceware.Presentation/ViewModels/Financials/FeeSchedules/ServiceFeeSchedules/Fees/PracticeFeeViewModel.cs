﻿using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules.Fees
{
    [DataContract]
    public class PracticeFeeViewModel : FeeViewModel
    {
        [DataMember]
        public virtual Decimal? Fee { get; set; }
    }
}
