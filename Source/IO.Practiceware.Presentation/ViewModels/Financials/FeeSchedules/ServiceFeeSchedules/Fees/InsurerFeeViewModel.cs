﻿using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules.Fees
{
    [DataContract]
    public class InsurerFeeViewModel : FeeViewModel
    {

        [DataMember]
        public virtual Decimal? OfficeAllowable { get; set; }
        
        [DataMember]
        public virtual Decimal? FacilityAllowable { get; set; }

    }
}
