﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules
{
    [SupportsDataErrorInfo]
    [DataContract]
    public class ServiceDetailsViewModel : ServiceViewModel
    {
        
        [DataMember]
        public virtual NamedViewModel RelativeValueUnitCode { get; set; }

        [RequiredWithDisplayName(ErrorMessage = "Please enter unit of measure")]
        [DataMember]
        public virtual NamedViewModel UnitType { get; set; }

        [RequiredWithDisplayName(ErrorMessage = "Please enter a unit value")]
        [DataMember]
        public virtual Decimal Units { get; set; }

        [DataMember]
        public virtual String NdcCode { get; set; }

        [DataMember]
        public virtual String RevenueCode { get; set; }

        [DataMember]
        public virtual NamedViewModel InvoiceType { get; set; }

        [DataMember]
        public virtual NamedViewModel Category { get; set; }

        [DataMember]
        public virtual String UnclassifiedNote { get; set; }
        
        //Default value should be DateTime.Today
        [Required(ErrorMessage = "Please select a start date")]
        [DataMember]
        public virtual DateTime StartDate { get; set; }

        [EqualToGreaterThan("StartDate", ErrorMessage = "End date can't be before the start date.")]
        [DataMember]
        public virtual DateTime EndDate { get; set; }

        [DataMember]
        public virtual Boolean OrderingProviderRequired { get; set; }

        [DataMember]
        public virtual Boolean ReferringProviderRequired { get; set; }

        [DataMember]
        public virtual Boolean CliaWaiverRequired { get; set; }

        [DataMember]
        public virtual Boolean AllowZeroAmount { get; set; }

    }

    [DataContract]
    public class ProfessionalEncounterServiceSelections
    {
        [DataMember]
        public virtual Boolean ShowExpired { get; set; }

        [DataMember]
        public virtual IList<int> CategoryIds { get; set; }
    }
}
