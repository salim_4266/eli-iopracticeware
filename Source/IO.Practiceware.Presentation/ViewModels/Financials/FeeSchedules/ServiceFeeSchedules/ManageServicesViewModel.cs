﻿using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules
{
    [DataContract]
    public class ManageServicesViewModel : IViewModel
    {
        [DataMember]
        public virtual ExtendedObservableCollection<ServiceDetailsViewModel> Services { get; set; }

        [DataMember]
        public virtual ExtendedObservableCollection<FeeScheduleContractViewModel> ProviderFeeSchedules { get; set; }

        [DataMember]
        public virtual ExtendedObservableCollection<NamedViewModel> EncounterServiceCategories { get; set; }
        
        [DataMember]
        public virtual ExtendedObservableCollection<NamedViewModel> UnitTypes { get; set; }
    }
}
