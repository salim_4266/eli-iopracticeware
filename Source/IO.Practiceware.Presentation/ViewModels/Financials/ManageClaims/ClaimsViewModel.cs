﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims
{
    [DataContract]
    public class ClaimsViewModel : IViewModel
    {
        [DataMember]
        public virtual Decimal? Balance { get; set; }

        [DataMember]
        public virtual int Count { get; set; }
    }
}