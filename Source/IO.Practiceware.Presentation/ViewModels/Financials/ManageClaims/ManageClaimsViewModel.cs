﻿using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims
{
    [DataContract]
    public class ManageClaimsViewModel : IViewModel
    {
        [DataMember]
        public virtual ClaimsViewModel PendingClaims { get; set; }

        [DataMember]
        public virtual ClaimsViewModel SentClaims { get; set; }

        [DataMember]
        public virtual ClaimsViewModel UnbilledClaims { get; set; }
    }
}