﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common
{
    [DataContract]
    public class InsurerViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual String InsurerName { get; set; }

        [DataMember]
        public virtual String PlanName { get; set; }

        [DataMember]
        public virtual AddressViewModel Address { get; set; }

        [DataMember]
        public virtual PolicyHolderViewModel PolicyHolder { get; set; }

    }
}
