﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common
{
    [DataContract]
    public class InvoiceViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual DateTime? DateOfService { get; set; }

        [DataMember]
        public virtual PatientViewModel Patient { get; set; }

        [DataMember]
        public virtual NamedViewModel ServiceLocation { get; set; }

        [DataMember]
        public virtual CareTeamViewModel CareTeam { get; set; }

    }
}
