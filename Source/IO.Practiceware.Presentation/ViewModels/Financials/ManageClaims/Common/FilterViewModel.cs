﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common
{
    [SupportsDataErrorInfo]
    [DataContract]
    public class FilterViewModel : IViewModel
    {

        [DataMember, DispatcherThread]
        public virtual DateTime? StartDate { get; set; }

        [DataMember]
        [EqualToGreaterThan("StartDate", ErrorMessage = "Please select an end date that falls after the selected start date")]
        public virtual DateTime? EndDate { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<ProviderViewModel> RenderingProviders { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }
    }

}
