﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common
{
    [DataContract]
    public class DataListsViewModel : IViewModel
    {

        [DataMember]
        public virtual ObservableCollection<ProviderViewModel> RenderingProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }

    }

}