﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common
{
    [DataContract]
    public class PatientViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual String FirstName { get; set; }

        [DataMember]
        public virtual String MiddleName { get; set; }

        [DataMember]
        public virtual String LastName { get; set; }
         [DataMember]
        public virtual String FormattedName { get; set; }

        [DataMember]
        public virtual InsurerViewModel PrimaryInsurer { get; set; }
    }
}
