﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims
{
    [DataContract]
    public class InvoiceViewModel : Common.InvoiceViewModel
    {

        [DataMember]
        public virtual bool IsValidated { get; set; }

        [Dependency, DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> Errors { get; set; }

        [Dependency, DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> Alerts { get; set; }

        [DataMember, Dependency]
        public virtual ExtendedObservableCollection<EncounterServiceViewModel> Services { get; set; }

        [DependsOn("Services")]
        public virtual Decimal? Balance { get { return Services.Sum(s => s.Balance); } }

        [DependsOn("Balance", "Services", "IsValidated", "Errors"), DispatcherThread]
        public virtual InvoiceStatusViewModel Status { get { return GetInvoiceStatus(); } }


        /// <summary>
        /// To check if service is has zero balance and validation can be skipped.
        /// </summary>
        [DependsOn("Services")]
        public virtual bool ServicesHaveZeroBalance
        {
            get
            {
                return Services.All(s => s.Balance <= 0);
            }
        }


        public virtual InvoiceStatusViewModel GetInvoiceStatus()
        {
            if (!IsValidated)
            {
                return new InvoiceStatusViewModel
                 {
                     Id = (int)InvoiceStatus.Validate,
                     Name = InvoiceStatus.Validate.ToString(),
                     Detail = InvoiceStatus.Validate.GetDescription(),
                     Color = "#25a0d4"
                 };
            }
            if (Errors.Count == 0 && Services.Count > 0 && !ServicesHaveZeroBalance)
            {
                return new InvoiceStatusViewModel
                 {
                     Id = (int)InvoiceStatus.Bill,
                     Name = InvoiceStatus.Bill.ToString(),
                     Detail = InvoiceStatus.Bill.GetDescription(),
                     Color = "#ffffb0"
                 };
            }
            if (Errors.Count == 0 && Services.Count > 0 && ServicesHaveZeroBalance)
            {
                return new InvoiceStatusViewModel
                {
                    Id = (int)InvoiceStatus.Close,
                    Name = InvoiceStatus.Close.ToString(),
                    Detail = InvoiceStatus.Close.GetDescription(),
                    Color = "#dddddd"
                };
            }
            return new InvoiceStatusViewModel
            {
                Id = (int)InvoiceStatus.Error,
                Name = InvoiceStatus.Error.ToString(),
                Detail = InvoiceStatus.Error.GetDescription(),
                Color = "#ffffff"
            };
        }

        [DataMember]
        public virtual bool OpenForReview { get; set; }
    }

    public class InvoiceStatusViewModel : NamedViewModel
    {
        [DataMember]
        public virtual String Detail { get; set; }

        /// <summary>
        /// Color property defines the row background color when selected; also possible to do using a style selector - but it's 
        /// simple enough of a binding to just define it here - open to suggestions for other ways to implement, but this should work
        /// </summary>
        public virtual String Color { get; set; }
    }

    /// <summary>
    /// Status of the invoice on pre and post validation.
    /// </summary>
    [DataContract]
    public enum InvoiceStatus
    {
        [EnumMember]
        [Display(Description = "This invoice needs to be validated before processing")]
        Validate,
        [EnumMember]
        [Display(Description = "This invoice is ready to be billed")]
        Bill,
        [EnumMember]
        [Display(Description = "This invoice is ready to be closed")]
        Close,
        [EnumMember]
        [Display(Description = "Errors have been detected; click for details")]
        Error
    }
}
