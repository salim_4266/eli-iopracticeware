﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims
{
    [DataContract]
    public class PendingClaimsFilter
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public List<int> ServiceLocationIds { get; set; }

        [DataMember]
        public List<int> RendingProviderIds { get; set; }
    }
}
