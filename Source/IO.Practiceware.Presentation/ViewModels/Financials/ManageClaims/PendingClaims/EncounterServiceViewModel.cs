﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims
{
    [DataContract]
    public class EncounterServiceViewModel : Financials.Common.Service.EncounterServiceViewModel
    {

        [DataMember]
        public virtual ObservableCollection<DiagnosisViewModel> Diagnoses { get; set; }

        public virtual String FormattedDiagnoses { get { return FormatDiagnoses(); } }

        public virtual String FormattedModifiers { get { return FormatModifiers(); } }

        private String FormatDiagnoses()
        {
            var toReturn = "";
            if (Diagnoses != null)
                foreach (var d in Diagnoses)
                {
                    toReturn = toReturn + d.DiagnosisLinkPointer.PointerLetter + ": " + d.ActiveDiagnosisCode.Code + ", ";
                }
            return toReturn;
        }

        private String FormatModifiers()
        {
            var toReturn = "";
            if (Modifiers != null)
                foreach (var m in Modifiers)
                {
                    toReturn = toReturn + m.Abbreviation + "  ";
                }
            return toReturn;
        }

        [DataMember]
        public virtual Decimal AdjustmentAmount { get; set; }

        [DependsOn("Units", "Charge", "AdjustmentAmount")]
        public override Decimal Balance
        {
            get
            {
                return TotalCharge - AdjustmentAmount;
            }
        }
    }
}
