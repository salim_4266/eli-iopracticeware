﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims
{
    [DataContract]
    public class PendingClaimsViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread, Dependency]
        public virtual ObservableCollection<InvoiceViewModel> SelectedInvoices { get; set; }
        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<InvoiceViewModel> Invoices { get; set; }
    }
}
