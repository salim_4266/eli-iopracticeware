﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common;

namespace IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims
{
    [DataContract]
    public class PendingClaimsLoadInformation
    {
        [DataMember]
        public virtual DataListsViewModel DataLists { get; set; }

        [DataMember]
        public virtual DateTime? StartDate { get; set; }
    }
}
