﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [DataContract]
    [SupportsDataErrorInfo]
    [EditableObject]
    public class InvoiceSupplementalViewModel : IViewModel
    {
        [DataMember]
        public virtual int? RelatedCause1Id { get; set; }

        [DataMember]
        public virtual int? RelatedCause2Id { get; set; }

        [DataMember]
        public virtual int? RelatedCause3Id { get; set; }

        [DataMember]
        public virtual NameAndAbbreviationAndDetailViewModel PlaceOfService { get; set; }


        [DataMember]
        public virtual DateTime? AccidentDate { get; set; }

        [DataMember]
        public virtual DateTime? HospitalizationRelatedToServicesStartDate { get; set; }

        [DataMember]
        [RequiredIf("IsAutoAccident", ErrorMessage = "State Required When Auto Accident is Checked")]
        public virtual NameAndAbbreviationViewModel StateOrProvince { get; set; }

        [DataMember]
        public virtual NamedViewModel ClaimDelayReasonCode { get; set; }

        [DataMember, ValidateObject, Dependency]
        public virtual ExtendedObservableCollection<ResubmissionViewModel> Resubmissions { get; set; }

        [DependsOn("RelatedCause1Id", "PlaceOfService", "AccidentDate", "ClaimFrequencyType", "StateOrProvince", "ClaimDelayReasonCode", "HospitalizationRelatedToServicesStartDate", "OrderingProvider", "SupervisingProvider")]
        public virtual bool IsEmpty
        {
            get
            {
                return !RelatedCause1Id.HasValue && !AccidentDate.HasValue && PlaceOfService == null && StateOrProvince == null && ClaimDelayReasonCode == null && !HospitalizationRelatedToServicesStartDate.HasValue && OrderingProvider == null && SupervisingProvider == null;
            }
        }

        [DependsOn("RelatedCause1Id", "RelatedCause2Id", "RelatedCause3Id")]
        internal bool IsAutoAccident
        {
            get
            {
                return (RelatedCause1Id.HasValue && RelatedCause1Id.Value == (int)RelatedCause.AutoAccident)
                    || (RelatedCause2Id.HasValue && RelatedCause2Id.Value == (int)RelatedCause.AutoAccident)
                    || (RelatedCause3Id.HasValue && RelatedCause3Id.Value == (int)RelatedCause.AutoAccident);
            }
        }

        [Expression("IsValidOrderingProvider", ErrorMessage = "Selected Ordering Provider must have a LastName and a 10-digit NPI"), DataMember]
        public virtual CareTeamProviderViewModel OrderingProvider { get; set; }

        [Expression("IsValidSupervisingProvider", ErrorMessage = "Selected Supervising Provider must have a LastName and a 10-digit NPI"), DataMember]
        public virtual CareTeamProviderViewModel SupervisingProvider { get; set; }


        protected virtual bool IsValidOrderingProvider
        {
            get
            {
                if (OrderingProvider != null)
                {
                    if (!string.IsNullOrEmpty(OrderingProvider.LastName) && !string.IsNullOrEmpty(OrderingProvider.NPI) && OrderingProvider.NPI.Length == 10)
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
        }

        protected virtual bool IsValidSupervisingProvider
        {
            get
            {
                if (SupervisingProvider != null)
                {
                    if (!string.IsNullOrEmpty(SupervisingProvider.LastName) && !string.IsNullOrEmpty(SupervisingProvider.NPI) && SupervisingProvider.NPI.Length == 10)
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
        }
    }

    [DataContract]
    [SupportsDataErrorInfo]
    [EditableObject]
    public class ResubmissionViewModel : IViewModel
    {
        [DataMember]
        public virtual NextPayerViewModel NextPayer { get; set; }

        [DataMember]
        [StringLength(18, ErrorMessage = "Maximum length for Resubmission Code is 18")]
        public virtual String PayerClaimControlNumber { get; set; }

        [DataMember]
        public virtual NamedViewModel ClaimFrequencyTypeCode { get; set; }

    }
}
