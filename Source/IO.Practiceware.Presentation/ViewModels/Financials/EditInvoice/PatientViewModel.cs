﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [DataContract]
    public class PatientViewModel : NamedViewModel
    {
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
    }
}
