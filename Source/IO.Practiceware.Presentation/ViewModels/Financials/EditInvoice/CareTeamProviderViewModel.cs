﻿using System.Runtime.Serialization;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [DataContract]
    public class CareTeamProviderViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string NPI { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string Suffix { get; set; }

        [DependsOn(new string[] { "FirstName", "MiddleName", "LastName", "Suffix", "NPI", "DisplayName" })]
        public virtual string DisplayText
        {
            get
            {
                string formattedName = string.Empty;
                if (!string.IsNullOrEmpty(DisplayName))
                {
                    formattedName = DisplayName;
                }
                else
                {
                    bool firstEntry = true;
                    foreach (string name in new[] { FirstName, MiddleName, LastName }.Where(name => !string.IsNullOrEmpty(name)))
                    {
                        formattedName += (firstEntry) ? name : " " + name;
                        firstEntry = false;
                    }
                }
                return string.Format("{0} - ({1})", formattedName, (NPI ?? string.Empty));
            }
        }
    }
}
