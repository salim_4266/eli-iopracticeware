﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    /// <summary>
    /// All data lists on the Edit Invoice screen whose values do not require any specific context
    /// </summary>
    [DataContract]
    public class DataListsViewModel : IViewModel
    {
        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> ExternalProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Providers { get; set; }

        /// <summary>
        /// The set of all provider - linked billing organization combinations, in addition to all billing organizations
        /// </summary>
        [DataMember]
        public virtual ObservableCollection<ProviderViewModel> RenderingProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<CareTeamProviderViewModel> OrderingProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<CareTeamProviderViewModel> SupervisingProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<ServiceViewModel> Services { get; set; }

        [DataMember]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> Modifiers { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> InvoiceTypes { get; set; }

        [DataMember]
        public virtual ObservableCollection<ServiceLocationViewModel> ServiceLocations { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> BillingOrganizations { get; set; }

        [DataMember]
        public virtual ObservableCollection<ServiceLocationViewModel> AttributeToLocations { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<DiagnosisCodeViewModel> Diagnoses { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> BillingActions { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> PlacesOfService { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ClaimFrequencyTypeCodes { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> StatesOrProvinces { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ClaimDelayReasonCodes { get; set; }
    }
}
