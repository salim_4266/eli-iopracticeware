﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class EncounterServiceViewModel : Common.Service.EncounterServiceViewModel
    {
        private NextPayerViewModel _nextPayer;
        /// <summary>
        /// To check  the Charge property changed or not.
        /// </summary>
        public static string ChargePropertyName = Reflector.GetMember<EncounterServiceViewModel>(x => x.Charge).Name;
        /// <summary>
        /// To check  the Units property changed or not.
        /// </summary>
        public static string UnitsPropertyName = Reflector.GetMember<EncounterServiceViewModel>(x => x.Units).Name;

        public EncounterServiceViewModel()
        {
            Units = 1;
        }

        [Required]
        public override ServiceViewModel Service { get; set; }

        [DataMember]
        public virtual double AllocateToPatientAmount { get; set; }

        [DataMember, Dependency, DispatcherThread]
        [Expression(@"LinkedDiagnoses !=  null && LinkedDiagnoses.Count <= 4", ErrorMessage = "You can only select maximum of four diagnosis links per service.")]
        public virtual ObservableCollection<DiagnosisLinkPointerViewModel> LinkedDiagnoses { get; set; }

        [IgnoreChangeTracking]
        [DataMember]
        public virtual NextPayerViewModel NextPayer
        {
            get
            {
                return _nextPayer;
            }
            set
            {
                _nextPayer = value;
                if (_nextPayer != null && AllBillingActions != null)
                {
                    // TODO: move it from here and observe via PropertyChanged

                    //Logic to set the action and corresponding action choices based on selected payer on each service.
                    //Setting the billing action here only as it should not override the manual selection of the user based on Next Payer.
                    var nextPayer = _nextPayer.Payer as PatientInsurancePayerViewModel;
                    AllBillingActions = AllBillingActions.OrderBy(i => i.Id).ToObservableCollection();
                    switch (NextPayer.Payer.Abbreviation)
                    {
                        case "P":
                            AvailableBillingActions = AllBillingActions
                                            .Where(x => x.Id == (int)BillingActionType.LeaveUnbilled || x.Id == (int)BillingActionType.StatementWait || x.Id == (int)BillingActionType.QueueStatement)
                                            .ToObservableCollection();
                            BillingAction = AvailableBillingActions.First(x => x.Id == (int)BillingActionType.QueueStatement);
                            break;
                        case "M1":
                        case "V1":
                        case "W1":
                        case "O1":
                            if (nextPayer != null && nextPayer.ClaimFileReceiverId.HasValue)
                            {                                
                                AvailableBillingActions = AllBillingActions
                                           .Where(x => x.Id == (int)BillingActionType.QueueClaimPaper || x.Id == (int)BillingActionType.ClaimWait || x.Id == (int)BillingActionType.LeaveUnbilled || x.Id == (int)BillingActionType.QueueClaimElectronic)
                                           .ToObservableCollection();                                
                                BillingAction = AvailableBillingActions.First(x => x.Id == (int)BillingActionType.QueueClaimElectronic);
                            }
                            else
                            {
                                AvailableBillingActions = AllBillingActions
                                           .Where(x => x.Id == (int)BillingActionType.QueueClaimPaper || x.Id == (int)BillingActionType.ClaimWait || x.Id == (int)BillingActionType.LeaveUnbilled)
                                           .ToObservableCollection();
                                BillingAction = AvailableBillingActions.First(x => x.Id == (int)BillingActionType.QueueClaimPaper);
                            }
                            break;
                        default:
                            if (nextPayer != null && (nextPayer.IsSecondaryClaimPaper || !nextPayer.ClaimFileReceiverId.HasValue))
                            {
                                AvailableBillingActions = AllBillingActions
                                          .Where(x => x.Id == (int)BillingActionType.QueueClaimPaper || x.Id == (int)BillingActionType.ClaimWait || x.Id == (int)BillingActionType.LeaveUnbilled || x.Id == (int)BillingActionType.CrossoverClaim)
                                          .ToObservableCollection();
                                BillingAction = AvailableBillingActions.First(x => x.Id == (int)BillingActionType.QueueClaimPaper);
                            }
                            else
                            {
                                AvailableBillingActions = AllBillingActions
                                              .Where(x => x.Id == (int)BillingActionType.QueueClaimPaper || x.Id == (int)BillingActionType.ClaimWait || x.Id == (int)BillingActionType.LeaveUnbilled || x.Id == (int)BillingActionType.CrossoverClaim || x.Id == (int)BillingActionType.QueueClaimElectronic)
                                              .ToObservableCollection();
                                BillingAction = AvailableBillingActions.First(x => x.Id == (int)BillingActionType.QueueClaimElectronic);
                            }
                            break;
                    }
                }
            }
        }

        [Expression(@"Modifiers !=  null && Modifiers.Count <= 4", ErrorMessage = "You can only select maximum of four modifiers per service."), Dependency]
        public override ObservableCollection<NameAndAbbreviationAndDetailViewModel> Modifiers { get; set; }

        [RegexMatch(@"^\d{0,8}(\.\d{1,3})?$", "Units textbox allows to enter any decimal value up to 8 characters, max of 3 digits to the right of the decimal")]
        public override decimal Units { get; set; }

        [RegexMatch(@"^\d{1,18}(\.\d{0,3})?$", "Units textbox allows to enter any decimal value up to 18 characters, max of 3 digits to the right of the decimal")]
        public override Decimal Charge { get; set; }

        [DataMember]
        public virtual Decimal AdjustmentAmount { get; set; }

        [DependsOn("Units", "Charge", "AdjustmentAmount")]
        public override Decimal Balance
        {
            get
            {
                return TotalCharge - AdjustmentAmount;
            }
        }

        [DataMember, IgnoreChangeTracking]
        public virtual NamedViewModel BillingAction { get; set; }

        [DataMember]
        [IgnoreChangeTracking]
        public virtual ObservableCollection<NamedViewModel> AvailableBillingActions { get; set; }
        //Caching all the billing action to be used in BillingAction as filter data. 
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> AllBillingActions { get; set; }

        [DataMember]
        public virtual String ClaimNote { get; set; }

        /// <summary>
        /// The following fields are not necessary for round 1 development
        /// </summary>
        [DataMember]
        public virtual DateTime? PatientBillDate { get; set; }

        [DataMember, DispatcherThread]
        public virtual String NdcCode { get; set; }

        [DataMember]
        public virtual bool IsEmg { get; set; }

        [DataMember]
        public virtual bool IsEpsdt { get; set; }
        [DataMember, DispatcherThread]
        public virtual String UnclassifiedServiceDescription { get; set; }

        [DataMember]
        public virtual String FreehandNote { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }

        public virtual bool IsBillingActionValid()
        {
            return BillingAction != null && AvailableBillingActions.Contains(BillingAction);
        }

        [DataMember]
        public virtual bool TransactionsExist { get; set; }
    }

}
