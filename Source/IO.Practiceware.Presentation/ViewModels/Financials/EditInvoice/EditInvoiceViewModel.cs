﻿
using System.Runtime.Serialization;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class EditInvoiceViewModel : IViewModel
    {
       [DataMember, Dependency]
        public virtual InvoiceViewModel Invoice { get; set; }

        [DataMember]
        public virtual DataListsViewModel DataLists { get; set; }
    }

    public class BillingDiagnosisCode
    {
        public int OrdinalId { get; set; }
        public string Code { get; set; }
        public int DiagnosisId { get; set; }
        public string Description { get; set; }
    }
}
