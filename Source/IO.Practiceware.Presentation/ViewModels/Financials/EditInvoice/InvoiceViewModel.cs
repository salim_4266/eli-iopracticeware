﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;


namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class InvoiceViewModel : IViewModel, IValidatableObject
    {
        private ServiceLocationViewModel _attributeTo;

        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual int? EncounterId { get; set; }

        [Expression("IsValidDate", ErrorMessage = "Date can not be future date."), Required, DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember, Dependency]
        public ObservableCollection<int> InvoiceReceivables { get; set; }

        [DataMember, Dependency]
        public virtual NamedViewModel BillingAction { get; set; }

        [Required]
        [DataMember]
        public virtual NamedViewModel InvoiceType { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Modifiers { get; set; }

        [DataMember]
        public virtual PatientViewModel Patient { get; set; }

        /// <summary>
        /// Collection of potential next payers, where next payers are the set of active 
        /// insurers of a patient and the patient him/herself
        /// </summary>
        [DataMember]
        public virtual ObservableCollection<NextPayerViewModel> NextPayers { get; set; }

        [DataMember, Dependency, ValidateObject(ErrorMessage = "Invalid service data."), DispatcherThread]
        public virtual ObservableCollection<EncounterServiceViewModel> EncounterServices { get; set; }

        [DataMember]
        public virtual NameAndAbbreviationAndDetailViewModel ReferringProvider { get; set; }

        [DataMember, DispatcherThread]
        public virtual ReferralViewModel Referral { get; set; }

        [DataMember, DispatcherThread]
        public virtual PatientInsuranceAuthorizationViewModel PatientInsuranceAuthorization { get; set; }

        [DataMember]
        [Required]
        public virtual ServiceLocationViewModel ServiceLocation { get; set; }
       
        [DataMember]
        public virtual NamedViewModel CodesetType { get; set; }

        [DataMember, DependsOn("ServiceLocation")]
        [Required]
        public virtual ServiceLocationViewModel AttributeTo
        {
            get
            {
                if (_attributeTo == null && ServiceLocation != null)
                {
                    _attributeTo = new ServiceLocationViewModel { Id = ServiceLocation.Id, Name = ServiceLocation.Name };
                }
                return _attributeTo;
            }
            set { _attributeTo = value; }
        }

        [DataMember]
        public virtual NamedViewModel BillingOrganization { get; set; }

        [DataMember]
        [Expression("ClaimNote.IsNullOrEmpty() || (!ClaimNote.IsNullOrEmpty() && ClaimNote.Length <= 83) ", ErrorMessage = "Claim note can not be more than 83 character long.")]
        public virtual String ClaimNote { get; set; }

        [DataMember, Dependency]
        [ValidateObject(ErrorMessage = "Rendering provider is required.")]
        public virtual CareTeamViewModel CareTeam { get; set; }

        [DataMember]
        public virtual DateTime? AppointmentStartDateTime { get; set; }

        [DataMember]
        [Dependency, DispatcherThread]
        public virtual ObservableCollection<DiagnosisViewModel> EncounterDiagnoses { get; set; }

        [DependsOn("EncounterDiagnoses")]
        [Dependency]
        public virtual ObservableCollection<DiagnosisLinkPointerViewModel> EncounterDiagnosisLinks
        {
            get { return GetEncounterDiagnosisLinks(); }
        }

        public virtual bool IsValidDate
        {
            get
            {
                return Date.IsBeforeToday();
            }
        }

        private ObservableCollection<DiagnosisLinkPointerViewModel> GetEncounterDiagnosisLinks()
        {

            if (EncounterDiagnoses.Any())
            {
                return new ObservableCollection<DiagnosisLinkPointerViewModel>(EncounterDiagnoses.Select(x => x.DiagnosisLinkPointer));
            }

            return new ObservableCollection<DiagnosisLinkPointerViewModel>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (EncounterDiagnoses.Count > 0)
            {
                var activeDiagnosisDuplicate = EncounterDiagnoses
                    .Where(x => x.ActiveDiagnosisCode != null)
                    .GroupBy(x => new {x.ActiveDiagnosisCode.Code })
                    .Where(g => g.Count() > 1);
                if (activeDiagnosisDuplicate.Any())
                {
                    return new[] { new ValidationResult("Invoice has duplicate diagnosis.", new[] { "EncounterDiagnoses" }) };
                }
            }
            return new[] { ValidationResult.Success };
        }

        [DataMember, ValidateObject]
        public virtual InvoiceSupplementalViewModel InvoiceSupplemental { get; set; }

    }

    [DataContract]
    [EditableObject]
    public class ServiceLocationViewModel : NamedViewModel
    {
        [DataMember]
        public virtual int CodeId { get; set; }
    }
}

