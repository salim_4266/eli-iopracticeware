﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice
{
    [DataContract]
    public class ProvidersDataListsViewModel : IViewModel
    {
        [DataMember]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> ExternalProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<CareTeamProviderViewModel> OrderingProviders { get; set; }
    }

}
