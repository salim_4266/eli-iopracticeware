﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ConfigureCategories
{
    [DataContract]
    public class AppointmentTypeViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Name { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual AppointmentCategoryViewModel AppointmentCategory { get; set; }
    }

}
