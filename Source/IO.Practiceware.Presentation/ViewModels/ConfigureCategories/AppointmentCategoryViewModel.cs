﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.ConfigureCategories
{
    [SupportsDataErrorInfo]
    [DataContract]
    public class AppointmentCategoryViewModel : IViewModel, IEquatable<AppointmentCategoryViewModel>
    {
        private string _originalName = string.Empty;

        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }
        
        [DispatcherThread]
        public virtual Color Color { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please enter a name to save the category")]
        public virtual string Name { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Description { get; set; }
        
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> Templates { get; set; }

        [DispatcherThread]
        public virtual void BeginNameEdit()
        {
            _originalName = Name;
        }

        [DispatcherThread]
        public virtual void CancelNameEdit()
        {
            Name = _originalName;
        }

        public bool Equals(AppointmentCategoryViewModel other)
        {
            if (other == null) return false;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AppointmentCategoryViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(AppointmentCategoryViewModel category1, AppointmentCategoryViewModel category2)
        {
            if (ReferenceEquals(category1, category2)) return true;

            if (((object)category1 == null) || ((object)category2 == null)) return false;

            return category1.Equals(category2);
        }

        public static bool operator !=(AppointmentCategoryViewModel category1, AppointmentCategoryViewModel category2)
        {
            return !(category1 == category2);
        }
    }

    [DataContract]
    public class ArchivedAppointmentCategoryViewModel : AppointmentCategoryViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentTypeViewModel> PreviouslyAssociatedAppointmentTypes { get; set; }
    }
}
