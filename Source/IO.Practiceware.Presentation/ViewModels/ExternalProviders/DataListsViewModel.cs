﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ExternalProviders
{
    [DataContract]
    public class DataListsViewModel : IViewModel
    {
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> AddressTypes { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> PhoneNumberTypes { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> EmailAddressTypes { get; set; }

        [DataMember]
        public virtual ObservableCollection<StateOrProvinceViewModel> StatesOrProvinces { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> NameSuffixes { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SpecialtyTypes { get; set; }

    }
}
