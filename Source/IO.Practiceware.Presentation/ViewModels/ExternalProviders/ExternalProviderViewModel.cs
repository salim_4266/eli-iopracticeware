﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ExternalProviders
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class ExternalProviderViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// If the external provider has a non-empty DisplayName, use it for display purposes
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual String DisplayName { get; set; }

        /// <summary>
        /// Otherwise, use the name values to create a FormattedName
        /// </summary>
        [DataMember]
        public virtual String FirstName { get; set; }

        [DataMember, DispatcherThread]
        public virtual String MiddleName { get; set; }

        [DataMember]
        [Required]
        public virtual String LastNameOrEntityName { get; set; }

        [DataMember]
        public virtual String Honorific { get; set; }

        [DataMember, DispatcherThread]
        public virtual string Title { get; set; }

        [DataMember]
        public virtual NamedViewModel SpecialtyType { get; set; }

        [DataMember, DispatcherThread]
        public virtual String NickName { get; set; }

        [DataMember, DispatcherThread]
        public virtual String Suffix { get; set; }

        [DataMember, Dependency]
        [ValidateObject]
        public virtual AddressViewModel Address { get; set; }

        [DataMember, Dependency, DispatcherThread]
        [ValidateObject]
        public virtual ExtendedObservableCollection<EmailAddressViewModel> EmailAddresses { get; set; }

        [DataMember, Dependency, DispatcherThread]
        [ValidateObject]
        public virtual ExtendedObservableCollection<PhoneNumberViewModel> PhoneNumbers { get; set; }

        [DataMember]
        public virtual bool IsEntity { get; set; }

        [DataMember, DispatcherThread]
        public virtual String Taxonomy { get; set; }

        [DataMember, DispatcherThread]
        [RegularExpression("\\d{10}", ErrorMessage="Npi must have a 10 digit numerical value")]
        public virtual String Npi { get; set; }

        [DataMember, DispatcherThread]
        public virtual bool ExcludeFromClaim { get; set; }

        [DataMember, DispatcherThread]
        public virtual String Comment { get; set; }

        [DependsOn("LastNameOrEntityName", "FirstName", "Honorific", "IsEntity")]
        public virtual String FormattedName
        {
            get { return !IsEntity ? LastNameOrEntityName + ", " + FirstName + " " + Honorific : LastNameOrEntityName ?? String.Empty; }
        }

        [DependsOn("LastNameOrEntityName")]
        public virtual bool IsEmpty
        {
            // Checking for null, because otherwise we get an error when creating an empty External Provider
            get { return String.IsNullOrEmpty(LastNameOrEntityName); }
        }

        [DataMember]
        public virtual bool IsArchived { get; set; }
    }

    public enum ContactTypeId
    {
        [Display(Name = "External Providers")]
        ExternalProviders = 1,
        [Display(Name = "Vendors")]
        Vendors = 2,
        [Display(Name = "Hospitals")]
        Hospitals = 3,
        [Display(Name = "Labs")]
        Labs = 4,
        [Display(Name = "Others")]
        Others = 5,
    }
}
