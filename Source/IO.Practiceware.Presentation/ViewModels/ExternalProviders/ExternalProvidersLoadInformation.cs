﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ExternalProviders
{
    [DataContract]
    public class ExternalProvidersLoadInformation : IViewModel
    {
        [DataMember]
        public virtual ObservableCollection<ExternalProviderViewModel> ExternalProviders { get; set; }

        [DataMember]
        public virtual DataListsViewModel DataLists { get; set; }
    }
}
