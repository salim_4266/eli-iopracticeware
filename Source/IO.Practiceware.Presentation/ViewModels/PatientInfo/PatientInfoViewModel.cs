﻿using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo
{
    [DataContract]
    [EditableObject]
    public class PatientInfoViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int? PatientId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Name { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<PatientTagViewModel> Tags { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime? BirthDate { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Gender { get; set; }

        /// <summary>
        /// Calculates the patient's age in years based on their birthdate and today's date.
        /// </summary>
        [DispatcherThread]
        public virtual int? Age
        {
            get
            {
                return BirthDate.HasValue ? BirthDate.Value.GetDifferenceInYears(DateTime.Now.ToClientTime()) : new int?();
            }
        }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<PolicyViewModel> Insurances { get; set; }  

        [DataMember]
        [DispatcherThread]
        public virtual decimal? CoPay { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual decimal Balance { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Comments { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string HomePhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string WorkPhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string CellPhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsClinical { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual byte[] Photo { get; set; }
        
    }


}
