﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [EditableObject]
    [DataContract]
    public class PatientDemographicsConfigureFieldViewModel : NamedViewModel
    {
        private bool _isRequiredConfigurable;
        private bool _isVisible;

        [DataMember]
        [DispatcherThread]
        public virtual string GroupName { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsVisibleConfigurable { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                if (_isVisible)
                {
                    IsEnabled = IsRequiredConfigurable;
                }
                else
                {
                    IsRequired = false;
                    IsEnabled = false;
                }
            }
        }

        [DataMember, DispatcherThread]
        public virtual bool IsRequiredConfigurable
        {
            get { return _isRequiredConfigurable; }
            set
            {
                _isRequiredConfigurable = value;
                if (IsVisible & _isRequiredConfigurable) //If Required is configurable, the checkbox is enabled 
                {
                    IsEnabled = _isRequiredConfigurable;
                }
                else //If Required is not configurable, the checkbox is disabled 
                {
                    IsEnabled = false;
                }
            }
        }

        [DataMember, DispatcherThread]
        public virtual bool IsEnabled { get; set; }

        [DataMember, DispatcherThread]
        public virtual bool IsRequired { get; set; }
    }

    [EditableObject]
    [DataContract]
    public class PatientDemographicsConfigureFieldGroupViewModel : IViewModel
    {
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<PatientDemographicsConfigureFieldViewModel> ConfigureFields { get; set; }

        [DataMember]
        public virtual string GroupName { get; set; }
    }

    [EditableObject]
    [DataContract]
    public class PatientDemographicsConfigureDefaultValueViewModel : NamedViewModel
    {
        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> Items { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string DefaultValue { get; set; }
    }

    [EditableObject]
    [DataContract]
    public class PatientDemographicsListViewModel : NamedViewModel
    {
        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> FavoriteItems { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> NonFavoriteItems { get; set; }
    }

    [DataContract]
    public class LoadPatientDemographicsFieldsListsInformation
    {
        [DataMember]
        [Dependency]
        public List<PatientDemographicsConfigureFieldViewModel> PatientDemographicsFieldConfigurations { get; set; }

        [DataMember]
        [Dependency]
        public List<PatientDemographicsConfigureDefaultValueViewModel> PatientDemographicsDefaultCheckBoxValueConfigurations { get; set; }

        [DataMember]
        [Dependency]
        public List<PatientDemographicsConfigureDefaultValueViewModel> PatientDemographicsDefaultComboBoxValueConfigurations { get; set; }

        [DataMember]
        [Dependency]
        public List<PatientDemographicsListViewModel> PatientDemographicsLists { get; set; }
    }
}