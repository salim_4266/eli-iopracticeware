﻿using System;
using System.Collections.Generic;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using IO.Practiceware.Services.Imaging;
using Soaf;
using System.Linq;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    public class CardScannerViewModelToPatientViewModelMapper
    {
        private readonly IPatientDemographicsViewService _patientDemoViewService;
        private readonly Func<PhoneNumberViewModel> _createPhoneNumberViewModel;

        public CardScannerViewModelToPatientViewModelMapper(IPatientDemographicsViewService patientDemoViewService, Func<PhoneNumberViewModel> createPhoneNumberViewModel)
        {
            _patientDemoViewService = patientDemoViewService;
            _createPhoneNumberViewModel = createPhoneNumberViewModel;
        }

        /// <summary>
        /// Sets phone number values. Works only with US numbers, since attempts to get area code as first 3 digits
        /// </summary>
        /// <param name="phoneNumbers">The phone numbers.</param>
        /// <param name="phoneNumberValue">The phone number value.</param>
        private void MapPhoneNumbers(ICollection<PhoneNumberViewModel> phoneNumbers, string phoneNumberValue)
        {
            if (PhoneNumberParserExtensions.GetParsedPhoneNumber(phoneNumberValue) == null)
                return;

            var phoneNumber = _createPhoneNumberViewModel();
            phoneNumber.SetValuesFromString(phoneNumberValue);

            if (phoneNumbers.All(pn => pn.PhoneNumber != phoneNumber.PhoneNumber))
            {
                phoneNumbers.Add(phoneNumber);
            }
        }

        private void MapAddresses(ICollection<AddressViewModel> addresses, CardScannerDataModel source)
        {
            var address = addresses.OrderByDescending(a => a.IsPrimary).FirstOrDefault(a => a.AddressType != null && a.AddressType.Id == (int)PatientAddressTypeId.Home);

            if (address == null)
            {
                address = new AddressViewModel();
                address.AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Home, Name = PatientAddressTypeId.Home.GetDisplayName() };
                addresses.Add(address);
            }

            if (address.AddressType == null) address.AddressType = new NamedViewModel { Id = (int)PatientAddressTypeId.Home, Name = PatientAddressTypeId.Home.GetDisplayName() };

            switch (source.Key)
            {
                case CardScannerDataField.AddressLine1:
                    address.Line1 = (string)source.Value;
                    break;
                case CardScannerDataField.AddressLine2:
                    address.Line2 = (string)source.Value;
                    break;
                case CardScannerDataField.City:
                    address.City = (string)source.Value;
                    break;
                case CardScannerDataField.State:
                    address.StateOrProvince = _patientDemoViewService.GetStateOrProvince((string)source.Value);
                    break;
                case CardScannerDataField.PostalCode:
                    address.PostalCode = (string)source.Value;
                    break;
            }

        }

        public void MapCardScannerViewModelToPatientViewModel(CardScannerDataModel source, PatientViewModel patient)
        {
            if (source.Value == null || (source.Value is string && string.IsNullOrWhiteSpace(source.Value as string))) return;

            switch (source.Key)
            {
                case CardScannerDataField.Prefix:
                    patient.Honorific = (string)source.Value;
                    break;
                case CardScannerDataField.Email:
                    var existingEmail = patient.EmailAddresses.FirstOrDefault(e => e.Value.Equals((string)source.Value, StringComparison.OrdinalIgnoreCase));
                    if (existingEmail == null) patient.EmailAddresses.Add(new EmailAddressViewModel { Value = (string)source.Value });
                    break;
                case CardScannerDataField.Telephone:
                    MapPhoneNumbers(patient.PhoneNumbers, (string)source.Value);
                    break;
                case CardScannerDataField.FirstName:
                    patient.FirstName = (string)source.Value;
                    break;
                case CardScannerDataField.LastName:
                    patient.LastName = (string)source.Value;
                    break;
                case CardScannerDataField.MiddleName:
                    patient.MiddleName = (string)source.Value;
                    break;
                case CardScannerDataField.Suffix:
                    patient.Suffix = (string)source.Value;
                    break;
                case CardScannerDataField.Sex:
                    patient.Gender = ((string)source.Value).ToEnumFromDisplayNameOrNull<Gender>() ?? patient.Gender;
                    break;
                case CardScannerDataField.SocialSecurityNumber:
                    patient.SocialSecurityNumber = (string)source.Value;
                    break;
                case CardScannerDataField.DateOfBirth:
                    patient.DateOfBirth = ((string)source.Value).ToDateTime("MM-dd-yy") ?? patient.DateOfBirth;
                    break;
            }

            if (source.Key.IsAddressField())
            {
                MapAddresses(patient.Addresses, source);
            }
        }

    }
}
