﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using IO.Practiceware.Model;
using System.Runtime.Serialization;


namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    public class GenderViewModel : INotifyPropertyChanged
    {
        public GenderViewModel()
        {

        }

        private Gender selectedGender = Gender.Decline; 
        public Gender SelectedGender
        {
            get { return selectedGender; }
            set
            {
                selectedGender = value;
                RaisePropertyChanged("SelectedGender");
            }
        }

        private static List<GenderNamedViewModel> _GenderList;

        public static List<GenderNamedViewModel> GenderList
        {
            get
            {
                if (_GenderList == null)
                {
                    _GenderList = new List<GenderNamedViewModel>();
                    foreach (Gender level in Enum.GetValues(typeof(Gender)))
                    {
                        string Description;
                        FieldInfo fieldInfo = level.GetType().GetField(level.ToString());
                        DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        if (attributes != null && attributes.Length > 0) { Description = attributes[0].Description; }
                        else { Description = string.Empty; }
                        GenderNamedViewModel TypeKeyValue = new GenderNamedViewModel((int)level, Description);
                        _GenderList.Add(TypeKeyValue);
                    }
                }
                return _GenderList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

[DataContract]
public class GenderNamedViewModel : IO.Practiceware.Presentation.ViewModels.Common.NamedViewModel<int>
{
    public GenderNamedViewModel() { }

    public GenderNamedViewModel(int id, string name)
        : base(id, name)
    { }
}
