﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using IO.Practiceware.Model;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    public class SexualOrientationViewModel : INotifyPropertyChanged
    {
        public SexualOrientationViewModel()
        {

        }

        private SexualOrientation selectedSexualOrientation = SexualOrientation.Dont;
        public SexualOrientation SelectedSexualOrientation
        {
            get { return selectedSexualOrientation; }
            set
            {
                selectedSexualOrientation = value;
                RaisePropertyChanged("SelectedSexualOrientation");
            }
        }

        private static List<SexualOrientationNamedViewModel> _SexualOrientationList;

        public static List<SexualOrientationNamedViewModel> SexualOrientationList
        {
            get
            {
                if (_SexualOrientationList == null)
                {
                    _SexualOrientationList = new List<SexualOrientationNamedViewModel>();
                    foreach (SexualOrientation level in Enum.GetValues(typeof(SexualOrientation)))
                    {
                        string Description;
                        FieldInfo fieldInfo = level.GetType().GetField(level.ToString());
                        DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        if (attributes != null && attributes.Length > 0) { Description = attributes[0].Description; }
                        else { Description = string.Empty; }
                        SexualOrientationNamedViewModel TypeKeyValue = new SexualOrientationNamedViewModel((int)level, Description);
                        _SexualOrientationList.Add(TypeKeyValue);
                    }
                }
                return _SexualOrientationList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}


[DataContract]
public class SexualOrientationNamedViewModel : IO.Practiceware.Presentation.ViewModels.Common.NamedViewModel<int>
{
    public SexualOrientationNamedViewModel() { }

    public SexualOrientationNamedViewModel(int id, string name)
        : base(id, name)
    { }
}