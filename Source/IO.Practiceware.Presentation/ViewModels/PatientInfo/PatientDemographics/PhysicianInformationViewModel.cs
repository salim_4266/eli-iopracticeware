﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PhysicianInformationViewModel : IViewModel, IIsValidationEnabled
    {
        [DataMember]
        [DispatcherThread]
        [RequiredWithDisplayName]
        public virtual NameAndAbbreviationAndDetailViewModel Physician { get; set; } 

        [DataMember]
        [DispatcherThread]
        public virtual bool IsReferring { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsPcp { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DependsOn("IsPcp")]
        [DispatcherThread]
        public virtual bool CanDelete
        {
            //we're allowing deleting PCPs (may change)
            get { return true; }
        }

        [DependsOn("Physician")]
        public virtual bool IsEmpty
        {
            get { return Physician == null; }
        }

        [DependsOn("IsEmpty")]
        public virtual bool IsValidationEnabled
        {
            get { return !IsEmpty; }
        }
    }
}
