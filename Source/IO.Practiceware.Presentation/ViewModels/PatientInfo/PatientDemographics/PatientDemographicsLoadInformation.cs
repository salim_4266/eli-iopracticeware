﻿using System.Collections.Generic;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    public class PatientDemographicsLoadInformation : IViewModel
    {
        [DataMember]
        public virtual PatientViewModel PatientViewModel { get; set; }

        [DataMember]
        public virtual PatientDemographicsDataListsViewModel PatientDemographicsDataLists { get; set; }
    }
}
