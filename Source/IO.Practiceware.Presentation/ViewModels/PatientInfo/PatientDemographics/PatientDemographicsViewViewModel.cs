﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [SupportsNotifyPropertyChanged(true)]
    [DataContract]
    public class PatientDemographicsDataListsViewModel : IViewModel
    {
        [Dependency]
        [DataMember]        
        public virtual ObservableCollection<NamedViewModel> PatientStatuses { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> AddressTypes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> NameSuffixes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> PhoneTypes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> EmailAddressTypes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<GenderNamedViewModel> Genders { get; set; }

        //[Dependency]
        //[DataMember]
        //public virtual ObservableCollection<NamedViewModel> Genders { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Sexes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<SexualOrientationNamedViewModel> SexualOrientations { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> MaritalStatuses { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<PatientRaceViewModel> Races { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<RacesAndEthnicitiesNamedViewModel> RacesAndEthnicities { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Ethnicities { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Languages { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<StateOrProvinceViewModel> StateOrProvinces { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NameAndAbbreviationAndDetailViewModel> ExternalProviders { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> Physicians { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }
                
        [Dependency]
        [DataMember]                
        public virtual ObservableCollection<PatientTagViewModel> TagsList { get; set; }          

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> EmploymentStatuses { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> PatientReferralSourceTypes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> PaymentOfBenefitsToProviderOptions { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> ReleaseOfMedicalInformationOptions { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> BillingOrganizations { get; set; }
    }
}
