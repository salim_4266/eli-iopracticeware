﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology
{
   public class ONCPsychologicalFeedBackViewModel
    {
        [DispatcherThread]
        public int Id { get; set; }
        [DispatcherThread]
        public int QuestionId { get; set; }
        [DispatcherThread]
        public int AnswerId { get; set; }
        [DispatcherThread]
        public int PatientId { get; set; }
        [DispatcherThread]
        public int LocationId { get; set; }
        [DispatcherThread]
        public DateTime LastUpdatedDate { get; set; }
        [DispatcherThread]
        public DateTime CreatedDate { get; set; }
    }
}
