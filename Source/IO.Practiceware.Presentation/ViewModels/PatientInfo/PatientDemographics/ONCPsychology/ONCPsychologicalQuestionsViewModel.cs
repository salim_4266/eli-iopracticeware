﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology
{
   public class ONCPsychologicalQuestionsViewModel
    {
       [DispatcherThread]
        public int QuestionId { get; set; }
       [DispatcherThread]
        public string QuestionDescription { get; set; }
       [DispatcherThread]
        public bool IsActive { get; set; }
       [DispatcherThread]
        public int QuestionCategory { get; set; }

    }
}
