﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;
using IO.Practiceware.Validation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology
{
   public class ONCPsychologyServiceLocationsViewModel
    {
        [DispatcherThread]
        public int Id { get; set; }
        [DispatcherThread]
        [RequiredIfIsRequired]
        public string ShortName { get; set; }
    }
}
