﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology
{
   public class ONCPsychologicalAnswersViewModel
    {
        [DispatcherThread]
        public int AnswerId { get; set; }
        [DispatcherThread]
        public string AnswerDescription { get; set; }
        [DispatcherThread]
        public int QuestionId { get; set; }
        [DispatcherThread]
        public string QuestionIdAnswerId { get; set; }
   }
}
