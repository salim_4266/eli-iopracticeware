﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientEthnicityViewModel:NamedViewModel, IEqualityComparer<PatientEthnicityViewModel>, IEquatable<PatientEthnicityViewModel>
    {
        [DataMember]
        public virtual bool IsArchived { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var other = obj as PatientEthnicityViewModel;
            if (other == null) return false;

            return Equals(other);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(PatientEthnicityViewModel x, PatientEthnicityViewModel y)
        {
            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(PatientEthnicityViewModel obj)
        {
            return obj.Id.GetHashCode();
        }

        public bool Equals(PatientEthnicityViewModel other)
        {
            return Id.Equals(other.Id);
        }
    }
}

   
