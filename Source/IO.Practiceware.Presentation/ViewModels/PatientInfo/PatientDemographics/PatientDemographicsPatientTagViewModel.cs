﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class PatientDemographicsPatientTagViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DispatcherThread]
        public virtual Color Color { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please enter a name to save the patient tag")]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "Please enter the tag name within the five character limit.")]
        public virtual string Name { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Description { get; set; }
    }

    [DataContract]
    public class LoadPatientDemographicsTagsInformation
    {
        [DataMember]
        [Dependency]
        public List<PatientDemographicsPatientTagViewModel> ExistingTags { get; set; }

        [DataMember]
        [Dependency]
        public List<PatientDemographicsPatientTagViewModel> ArchivedTags { get; set; }
    }
}
