﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.PatientRepresentative
{
	public class PatientRepresentativeViewModel
	{

		public virtual int PatientId { get; set; }

		public virtual string Title { get; set; }

		[DataMember]
		[Required]
		[RegexMatch(AcceptableNameRegEx, ErrorMessage = "First Name can only contain letters, apostraphes, and dashes.", SuccessOnNullOrEmptyInput = true)]
		public virtual string FirstName { get; set; }

		[DataMember]
		//[RequiredIfIsRequired]
		//[RegexMatch(AcceptableNameRegEx, ErrorMessage = "Middle Name can only contain letters, apostraphes, and dashes.", SuccessOnNullOrEmptyInput = true)]
		public virtual string MiddleName { get; set; }

		[DataMember]
		[RegexMatch(AcceptableNameRegEx, "Last Name can only contain letters, apostrophes, spaces, and dashes.", SuccessOnNullOrEmptyInput = true)]
		[Required]
		//[Expression("HasLastNameAndDobConflict == false", ErrorMessage = "Last name and Date of Birth already exists.")]
		//[DependsOn("HasLastNameAndDobConflict")]
		public virtual string LastName { get; set; }


		
		[DataMember]
		//[Required(AllowEmptyCollections = false, ErrorMessage = "Phone Number is required")]
		public virtual string HomePhone { get; set; }

		
		[DataMember]
		//[RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Phone Number is required")]
		public virtual string WorkPhone { get; set; }

		
		[DataMember]
		//[RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Phone Number is required")]
		public virtual string CellPhone { get; set; }

        [DataMember]
		//[RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Phone Number is required")]
		public virtual string Email { get; set; }
		
		[DataMember]
		[Required]
		public string Relationship { get; set; }

		public string RelationshipCode { get; set; }


		[DataMember]		
		public virtual string DateOfBirth { get; set; }

		[DataMember]		
		public virtual int? PatientStatusId { get; set; }

		//public PatientStatus? PatientStatus
		//{
		//	get { return (PatientStatus?)PatientStatusId; }
		//	set { PatientStatusId = (int?)value; }
		//}

		
		[DataMember]
		public virtual AddressViewModel Addresses { get; set; }

		[DataMember]
		public string State { get; set; }

		[DataMember]
		public string Zip { get; set; }

		[DataMember]
		public string County { get; set; }

		public NameAndAbbreviationViewModel Country { get; set; }

		public string CountryCode { get; set; }


		#region Constant Values

		// The first ^ anchors the search to the beginning of the string. The second ^ in the brackets specifies to match 
		// any character that is not one of the characters in the brackets.  The + means to match one or more characters.
		// The $ means anchor the search to the end of the string.
		// Putting it all together:  Search from the beginning of a string and look at each character to make sure
		// it doesn't match the characters in the character group, looking at each character until the end of the string.
		public const string AcceptableTextInputDoesNotContainTheseCharacters = @"^[^~:|^&\*]+$";

		/// <summary>
		/// Regular Expressions which denotes a pattern to match only letters, apostrophes, spaces, and dashes. This is specific to Patient Names
		/// </summary>
		public const string AcceptableNameRegEx = @"[a-zA-Z '-]*";

		#endregion
	}
}
