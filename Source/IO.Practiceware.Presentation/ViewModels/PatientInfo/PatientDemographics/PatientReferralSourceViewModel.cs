﻿using System.ComponentModel;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientReferralSourceViewModel : IViewModel, IIsValidationEnabled
    {
        public PatientReferralSourceViewModel()
        {
            this.As<INotifyPropertyChanged>().IfNotNull(npc => npc.PropertyChanged += OnPropertyChanged);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // force rebind of all properties on any property changed - this is required because of circular dependencies between
            if (e.PropertyName != string.Empty) this.As<IRaisePropertyChanged>().IfNotNull(rpc => rpc.OnPropertyChanged(new PropertyChangedEventArgs(string.Empty)));
        }

        [DataMember]
        [DispatcherThread]
        public virtual int? Id { get; set; }

        [DataMember]
        [DispatcherThread]
        //no longer requiring referral source contact for a valid referral
        //[RequiredWithDisplayName(AllowEmptyStrings = true)]
        public virtual string ReferralSourceContactName { get; set; }

        [DataMember]
        [DispatcherThread]
        [RequiredWithDisplayNameAttribute]
        public virtual NamedViewModel Category { get; set; }

        [DependsOn("Category")]
        public virtual bool IsEmpty
        {
            get { return Category == null; }
        }
        
        [DependsOn("IsEmpty")]
        public virtual bool IsValidationEnabled
        {
            get { return !IsEmpty; }
        }
    }
}
