﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using IO.Practiceware.Model;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    public class RacesAndEthnicitiesViewModel : INotifyPropertyChanged
    {
        public RacesAndEthnicitiesViewModel()
        {

        }

        private RacesAndEthnicities selectedRacesAndEthnicities = RacesAndEthnicities.Decline;
        public RacesAndEthnicities SelectedRacesAndEthnicities
        {
            get { return selectedRacesAndEthnicities; }
            set
            {
                selectedRacesAndEthnicities = value;
                RaisePropertyChanged("SelectedRacesAndEthnicities");
            }
        }

        private static List<RacesAndEthnicitiesNamedViewModel> _RacesAndEthnicitiesList;

        public static List<RacesAndEthnicitiesNamedViewModel> RacesAndEthnicitiesList
        {
            get
            {
                if (_RacesAndEthnicitiesList == null)
                {
                    _RacesAndEthnicitiesList = new List<RacesAndEthnicitiesNamedViewModel>();
                    foreach (RacesAndEthnicities level in Enum.GetValues(typeof(RacesAndEthnicities)))
                    {
                        string Description;
                        FieldInfo fieldInfo = level.GetType().GetField(level.ToString());
                        DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        if (attributes != null && attributes.Length > 0) { Description = attributes[0].Description; }
                        else { Description = string.Empty; }
                        RacesAndEthnicitiesNamedViewModel TypeKeyValue = new RacesAndEthnicitiesNamedViewModel((int)level, Description);
                        _RacesAndEthnicitiesList.Add(TypeKeyValue);
                    }
                }
                return _RacesAndEthnicitiesList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}



[DataContract]
public class RacesAndEthnicitiesNamedViewModel : IO.Practiceware.Presentation.ViewModels.Common.NamedViewModel<int>
{
    public RacesAndEthnicitiesNamedViewModel() { }

    public RacesAndEthnicitiesNamedViewModel(int id, string name)
        : base(id, name)
    { }
}
