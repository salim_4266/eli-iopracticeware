﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    [SupportsDataErrorInfo]
    [EditableObject]
    [SupportsNotifyPropertyChanged(true, true)]
    public class PatientViewModel : IViewModel, IHasPropertyConfigurations, IValidatableObject
    {
        #region Patient Personal Info

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, ErrorMessage = "Title cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Title { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [RegexMatch(AcceptableNameRegEx, ErrorMessage = "First Name can only contain letters, apostraphes, and dashes.", SuccessOnNullOrEmptyInput = true)]
        public virtual string FirstName { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [RegexMatch(AcceptableNameRegEx, ErrorMessage = "Middle Name can only contain letters, apostraphes, and dashes.", SuccessOnNullOrEmptyInput = true)]
        public virtual string MiddleName { get; set; }

        [DataMember]
        [RegexMatch(AcceptableNameRegEx, "Last Name can only contain letters, apostrophes, spaces, and dashes.", SuccessOnNullOrEmptyInput = true)]
        [RequiredIfIsRequired]
        [Expression("HasLastNameAndDobConflict == false", ErrorMessage = "Last name and Date of Birth already exists.")]
        [DependsOn("HasLastNameAndDobConflict")]
        public virtual string LastName { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, ErrorMessage = "Suffix cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Suffix { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, ErrorMessage = "Nickname cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Nickname { get; set; }

        [DataMember]
        [RegexMatch(@"[a-zA-Z\.]*", ErrorMessage = "Honorific can only contain letters and periods.", SuccessOnNullOrEmptyInput = true)]
        [RequiredIfIsRequired]
        public virtual string Honorific { get; set; }

        [DataMember]
        [RegexMatch(AcceptableNameRegEx, ErrorMessage = "Prior First Name can only contain letters, apostrophes, spaces, and dashes.", SuccessOnNullOrEmptyInput = true)]
        [RequiredIfIsRequired]
        public virtual string PriorFirstName { get; set; }

        [DataMember]
        [RegexMatch(AcceptableNameRegEx, ErrorMessage = "Prior Last Name can only contain letters, apostrophes, spaces, and dashes.", SuccessOnNullOrEmptyInput = true)]
        [RequiredIfIsRequired]
        public virtual string PriorLastName { get; set; }

        [Dependency]
        [DataMember]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Address is required")]
        public virtual ObservableCollection<AddressViewModel> Addresses { get; set; }

        public virtual bool HasMultipleAddresses
        {
            get { return Addresses.Count > 1; }
        }

        [Dependency]
        [DataMember]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Phone Number is required")]
        public virtual ObservableCollection<PhoneNumberViewModel> PhoneNumbers { get; set; }

        public virtual bool HasMultiplePhoneNumbers
        {
            get { return PhoneNumbers.Count > 1; }
        }

        [Dependency]
        [DataMember]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Email Address is required")]
        public virtual ObservableCollection<EmailAddressViewModel> EmailAddresses { get; set; }

        public virtual bool HasMultipleEmailAddresses
        {
            get { return EmailAddresses.Count > 1; }
        }
        [DataMember]
        [DispatcherThread]
        public virtual string ActivationCode
        {
            get;
            set;
        }

       #endregion

        #region Patient Demographic Information

        [DataMember]
        [RequiredIfIsRequired]
        [Expression("HasLastNameAndDobConflict == false", ErrorMessage = "Last name and Date of Birth already exists.")]
        [DependsOn("HasLastNameAndDobConflict")]
        public virtual DateTime? DateOfBirth { get; set; }

        [DependsOn("DateOfBirth")]
        public virtual int? Age
        {
            get { return DateOfBirth.HasValue ? DateOfBirth.Value.GetDifferenceInYears(DateTime.Now.ToClientTime()) : new int?(); }
        }

        public virtual bool HasLastNameAndDobConflict { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? GenderId { get; set; }

        public Gender? Gender
        {
            get { return (Gender?)GenderId; }
            set { GenderId = (int?)value; }
        }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? SexId { get; set; }

        public Sex? Sex
        {
            get { return (Sex?)SexId; }
            set { SexId = (int?)value; }
        }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? SexualOrientationId { get; set; }

        public SexualOrientation? SexualOrientation
        {
            get { return (SexualOrientation?)SexualOrientationId; }
            set { SexualOrientationId = (int?)value; }
        }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? RaceAndEthnicityId { get; set; }

        public RacesAndEthnicities? RacesAndEthnicities
        {
            get { return (RacesAndEthnicities?)RaceAndEthnicityId; }
            set { RaceAndEthnicityId = (int?)value; }
        }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? MaritalStatusId { get; set; }

        public MaritalStatus? MaritalStatus
        {
            get { return (MaritalStatus?)MaritalStatusId; }
            set { MaritalStatusId = (int?)value; }
        }

        [Dependency]
        [DataMember]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Race is required")]
        public virtual ObservableCollection<PatientRaceViewModel> PatientRaces { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? EthnicityId { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? LanguageId { get; set; }

        [DataMember]
        [RegexMatch(Strings.SocialSecurityNumberRegex, SuccessOnNullOrEmptyInput = true, ErrorMessage = "Social Security Number is not in the correct format.")]
        [RequiredIfIsRequired]
        [Expression("HasSocialSecurityConflict == false", ErrorMessage = "Social Security Number already exists.")]
        [DependsOn("HasSocialSecurityConflict")]
        public virtual string SocialSecurityNumber { get; set; }

        public virtual bool HasSocialSecurityConflict { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        [Display(Name = "Prior Id")]
        public virtual string PriorId { get; set; }

        #endregion

        #region Physician Information

        [Dependency]
        [DataMember]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "PCP is required")]
        public virtual ObservableCollection<PhysicianInformationViewModel> ExternalPhysicians { get; set; }

        [DependsOn("ExternalPhysicians")]
        public virtual PhysicianInformationViewModel PrimaryCarePhysician
        {
            get { return ExternalPhysicians.SingleOrDefault(x => x.IsPcp); }
        }

        [DependsOn("ExternalPhysicians")]
        public virtual ObservableCollection<PhysicianInformationViewModel> OtherPhysicians
        {
            get { return ExternalPhysicians.Where(x => !x.IsPcp).ToExtendedObservableCollection(); }
        }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? PreferredPhysicianId { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? PreferredServiceLocationId { get; set; }

        [DataMember]
        [RequiredWithDisplayNameAttribute]
        public virtual int? DefaultBillingOrganizationId { get; set; }

        #endregion

        #region Employment Information

        [DataMember]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, ErrorMessage = "Occupation contains illegal characters", SuccessOnNullOrEmptyInput = true)]
        public virtual string Occupation { get; set; }

        [DataMember]
        public virtual int? EmploymentStatusId { get; set; }

        [DataMember]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, ErrorMessage = "Company name contains illegal characters", SuccessOnNullOrEmptyInput = true)]
        public virtual string CompanyName { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual AddressViewModel EmployerAddress { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual PhoneNumberViewModel EmployerPhoneNumber { get; set; }

        #endregion

        #region Additional Information

        [DataMember]
        public virtual DateTime? LastUpdated { get; set; }

        [DataMember]
        public virtual bool HasHipaaConsent { get; set; }

        [DataMember]
        public virtual int AssignBenefitsToProviderId { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int ReleaseMedicalInformationId { get; set; }

        [DependsOn("ReleaseMedicalInformationId")]
        public virtual bool IsReleaseSignatureDateRequired
        {
            get
            {
                return ReleaseMedicalInformationId == (int) ReleaseOfInformationCode.Y;
            }
        }

        [DataMember]
        [RequiredIfIsRequired]
        [DependsOn("ReleaseMedicalInformationId")]
        public virtual DateTime? ReleaseSignatureDate { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual int? PatientStatusId { get; set; }

        public PatientStatus? PatientStatus
        {
            get { return (PatientStatus?)PatientStatusId; }
            set { PatientStatusId = (int?)value; }
        }

        [DependsOn("PatientStatusId")]
        public virtual bool PatientStatusIsDeceased
        {
            get
            {
                return PatientStatus.HasValue && PatientStatus.Value == Model.PatientStatus.ClosedDeceased;
            }
        }

        [DependsOn("PatientStatusId")]
        [DataMember]
        public virtual DateTime? DateOfDeath
        {
            get
            {
                if (!PatientStatusIsDeceased) _dateOfDeath = null;
                return _dateOfDeath;
            }
            set
            {
                _dateOfDeath = value;
            }
        }
        private DateTime? _dateOfDeath;

        [DataMember]
        public virtual int ReferringContactId { get; set; }

        [DataMember]
        public virtual int ReferringCategoryId { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        public virtual int? ReligionId { get; set; }

        public virtual PatientTagViewModel SelectedTag
        {
            set
            {
                if (Tags == null) return;

                if (Tags.All(x => x.TagId != value.TagId))
                {
                    Tags.Add(new PatientTagViewModel { TagId = value.TagId });
                }
            }
        }

        [Dependency]
        [DataMember]
        [ValidateObject]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Patient tag is required")]
        public virtual ObservableCollection<PatientTagViewModel> Tags { get; set; }

        [Dependency]
        [DataMember]
        [ValidateObject]
        [RequiredIfIsRequired(AllowEmptyCollections = false, ErrorMessage = "Patient referral is required")]
        public virtual ObservableCollection<PatientReferralSourceViewModel> ReferralSources { get; set; }

        #endregion

        #region Constant Values

        // The first ^ anchors the search to the beginning of the string. The second ^ in the brackets specifies to match 
        // any character that is not one of the characters in the brackets.  The + means to match one or more characters.
        // The $ means anchor the search to the end of the string.
        // Putting it all together:  Search from the beginning of a string and look at each character to make sure
        // it doesn't match the characters in the character group, looking at each character until the end of the string.
        public const string AcceptableTextInputDoesNotContainTheseCharacters = @"^[^~:|^&\*]+$";

        /// <summary>
        /// Regular Expressions which denotes a pattern to match only letters, apostrophes, spaces, and dashes. This is specific to Patient Names
        /// </summary>
        public const string AcceptableNameRegEx = @"[a-zA-Z '-]*";

        #endregion

        [DataMember]
        public ICollection<PropertyConfiguration> PropertyConfigurations { get; set; }

        /// <summary>
        /// Validates the specified validation context. We validate for 2 reasons:
        /// 1. To validtae for binding so red error tooltips are shown.
        /// 2. To validate when explicitly calling Validate when saving.
        /// In the second case we need to validate the whole hierarchy of the view model (including child objects),
        /// but if we use the ValidateObject attribute on collections that also have [Required] attributes to disallow
        /// empty collections, we end up getting a red error tooltip on the parent collection area AND on the children,
        /// which is undesirable. Therefore, we validate these child collections separately via IValidatableObject,
        /// which happens AFTER all ValidationAttributes are evaluated.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new[] { GetValidationResult(EmailAddresses.Concat(Addresses.OfType<object>()).Concat(PhoneNumbers).ToArray()) };
        }

        private ValidationResult GetValidationResult(params object[] source)
        {
            return
                source.All(i => i.IsValid()) ?
                    ValidationResult.Success
                    : new ValidationResult("Properties are invalid.", new[] { string.Empty });
        }
    }

    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientRaceViewModel : NamedViewModel, IEqualityComparer<PatientRaceViewModel>, IEquatable<PatientRaceViewModel>
    {
        [DataMember]
        public virtual bool IsArchived { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var other = obj as PatientRaceViewModel;
            if (other == null) return false;

            return Equals(other);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(PatientRaceViewModel x, PatientRaceViewModel y)
        {
            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(PatientRaceViewModel obj)
        {
            return obj.Id.GetHashCode();
        }

        public bool Equals(PatientRaceViewModel other)
        {
            return Id.Equals(other.Id);
        }
    }


    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientTagViewModel : NamedViewModel
    {
        [DataMember]
        [RequiredWithDisplayName]
        public virtual int TagId { get; set; }

        [DataMember]
        public virtual bool IsPrimary { get; set; }

        [DataMember]
        public virtual string HexColor { get; set; }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(obj, this);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
