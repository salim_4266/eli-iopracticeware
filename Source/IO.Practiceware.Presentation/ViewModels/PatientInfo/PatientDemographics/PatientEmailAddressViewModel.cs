﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientEmailAddressViewModel : EmailAddressViewModel
    {
    }
}
