﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments
{
    [DataContract]
    public class InsuranceViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual int PolicyId { get; set; }

        [DataMember]
        public virtual string PolicyCode { get; set; }

        [DataMember]
        public virtual string GroupCode { get; set; }

        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual DateTime? EndDateTime { get; set; }

        [DataMember]
        public virtual int InsurerId { get; set; }

        [DataMember]
        public virtual string InsurerName { get; set; }
    }
}
