﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments
{
    [DataContract]
    public class PatientAppointmentViewModel : IViewModel
    {
        [DataMember]
        public virtual int AppointmentId { get; set; }

        [DataMember]
        public virtual int EncounterId { get; set; }

        [DataMember]
        public virtual DateTime DateTime { get; set; }

        [DataMember]
        public virtual AppointmentTypeViewModel AppointmentType { get; set; }

        [DataMember]
        public virtual string Resource { get; set; }

        [DataMember]
        public virtual int? ResourceId { get; set; }

        [DataMember]
        public virtual NamedViewModel<int> AppointmentLocation { get; set; }


        [DataMember]
        public virtual InsuranceType AppointmentInsuranceType { get; set; }


        [DataMember]
        public virtual ColoredViewModel EncounterStatus { get; set; }

        [DataMember]
        public virtual NamedViewModel ConfirmationStatus { get; set; }

        [DataMember]
        [DispatcherThread]
        [StringLength(127)]
        public virtual string Comments { get; set; }

     
        [DataMember]
        public virtual bool IsReferralInformationRequired { get; set; }
     

        [DataMember]
        [DispatcherThread]
        public virtual PatientInsuranceAuthorizationViewModel AttachedPatientInsuranceAuthorization { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentChangeViewModel> AppointmentChanges { get; set; }

        [DependsOn("AppointmentChanges")]
        public virtual ObservableCollection<AppointmentChangeViewModel> AppointmentUpdates
        {
            get
            {
                if (AppointmentChanges.IsNullOrEmpty()) return null;

                return AppointmentChanges.Where(x => !x.IsInsertChange).ToExtendedObservableCollection();
            }
        }

        [DependsOn("AppointmentChanges")]
        [DispatcherThread]
        public virtual string CreatedOn
        {
            get
            {
                if (AppointmentChanges.IsNullOrEmpty() || !AppointmentChanges.Any(x => x.IsInsertChange)) return null;

                var date = AppointmentChanges.First(x => x.IsInsertChange).DateTime;
                return string.Format("{0:d} @ {1:t}", date, date);
            }
        }

        [DependsOn("AppointmentChanges")]
        [DispatcherThread]
        public virtual string CreatedBy
        {
            get
            {
                if (AppointmentChanges.IsNullOrEmpty() || !AppointmentChanges.Any(x => x.IsInsertChange)) return null;

                return AppointmentChanges.First(x => x.IsInsertChange).UserName;
            }
        }

        [DependsOn("EncounterStatus")]
        [DispatcherThread]
        public virtual bool IsCancelled
        {
            get { return ((EncounterStatus)EncounterStatus.Id).IsCancelledStatus(); }
        }

        public virtual bool CanCancel
        {
            get { return ((EncounterStatus)EncounterStatus.Id).CanCancel(); }
        }

        public virtual bool CanReschedule // Only means can the user at least see the appointment search window.
        {
            get
            {
                return (((EncounterStatus)EncounterStatus.Id) == Model.EncounterStatus.Pending) && PermissionId.ViewSchedule.PrincipalContextHasPermission();
            }
        }
    }
}
