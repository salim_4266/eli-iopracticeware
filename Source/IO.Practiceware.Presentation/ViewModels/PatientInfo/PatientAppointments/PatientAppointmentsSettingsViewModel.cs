﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments
{
    [DataContract]
    public class PatientAppointmentsSettingsViewModel : IViewModel
    {
        /// <summary>
        /// Gets or sets the Settings Id
        /// </summary>
        public virtual int? Id { get; set; }

        /// <summary>
        /// Determines whether or not the "Pending" checkbox is checked.
        /// </summary>
        [DataMember]
        public virtual bool IsPendingChecked { get; set; }

        /// <summary>
        /// Determines whether or not the "Arrived" checkbox is checked.
        /// </summary>
        [DataMember]
        public virtual bool IsArrivedChecked { get; set; }

        /// <summary>
        /// Determines whether or not the "Discharged" checkbox is checked.
        /// </summary>
        [DataMember]
        public virtual bool IsDischargedChecked { get; set; }

        /// <summary>
        /// Determines whether or not the "Cancelled" checkbox is checked.
        /// </summary>
        [DataMember]
        public virtual bool IsCancelledChecked { get; set; }
    }
}
