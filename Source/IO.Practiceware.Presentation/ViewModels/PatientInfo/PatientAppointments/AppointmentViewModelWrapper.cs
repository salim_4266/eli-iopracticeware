﻿using IO.Practiceware.Presentation.Views.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.Views.PatientReferralPopup;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments
{
    public class AppointmentViewModelWrapper
    {
        public PatientAppointmentViewModel PatientAppointment { get; set; }
        public PatientInsuranceAuthorizationPopupViewContext AuthorizationPopupViewContext { get; set; }
        public virtual PatientReferralPopupViewContext Referral { get; set; }
    }
}