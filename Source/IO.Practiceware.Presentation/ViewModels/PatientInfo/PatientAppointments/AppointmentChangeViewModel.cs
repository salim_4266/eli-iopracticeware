﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments
{
    /// <summary>
    /// TODO: if possible, convert to using AuditEntryViewModel
    /// </summary>
    [DataContract]
    public class AppointmentChangeViewModel : IViewModel
    {
        /// <summary>
        /// Determines whther or not this change is of type 'insert' signifying who created the appointment.
        /// </summary>
        [DataMember]
        [DispatcherThread]
        public virtual bool IsInsertChange { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int AppointmentId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime DateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string UserName { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Field { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ChangedFrom { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ChangedTo { get; set; }
    }
}