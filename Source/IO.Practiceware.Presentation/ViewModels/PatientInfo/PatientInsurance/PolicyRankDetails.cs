﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    /// <summary>
    /// A common model to represent the set of properties that are used to determine a policy and how it is ranked in terms of 
    /// invoice billing.
    /// </summary>
    [DataContract]
    public class PolicyRankDetailViewModel : IViewModel
    {
        [DataMember]
        public virtual long PatientInsuranceId { get; set; }

        [DataMember]
        public virtual long PolicyHolderId { get; set; }

        [DataMember]
        public virtual int PolicyRank { get; set; }

        [DataMember]
        public virtual DateTime StartDate { get; set; }

        [DataMember]
        public virtual DateTime? EndDate { get; set; }

        [DataMember]
        public virtual int InsuranceTypeId { get; set; }

        public InsuranceType InsuranceType
        {
            get { return (InsuranceType)InsuranceTypeId; }
        }
    }

    public static class PolicyRankDetailViewModelExtensions
    {
        public static bool HasEnded(this PolicyRankDetailViewModel policyRankDetail)
        {
            return policyRankDetail.EndDate.HasValue && policyRankDetail.EndDate.Value.Date < DateTime.Now.ToClientTime().Date;
        }

        public static bool HasNotEnded(this PolicyRankDetailViewModel policyRankDetail)
        {
            return !policyRankDetail.HasEnded();
        }

        public static IEnumerable<PolicyRankDetailViewModel> WhereNotEnded(this IEnumerable<PolicyRankDetailViewModel> policyRankDetails)
        {
            return policyRankDetails.Where(p => p.HasNotEnded());
        }

        /// <summary>
        /// Returns an ordered list of policies.  Orders first by Insurance Type, then by Policy Rank.
        /// </summary>
        /// <param name="policyRankDetails"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<PolicyRankDetailViewModel> OrderByInsuranceTypeThenByRank(this IEnumerable<PolicyRankDetailViewModel> policyRankDetails)
        {
            return policyRankDetails.OrderBy(p => p.InsuranceType).ThenBy(p => p.PolicyRank);
        }

        /// <summary>
        /// Compacts the rank of each 'active' policy so that they are contiguous and not fragmented. 
        /// Achieves this by ordering them by their current rank, then setting the new rank
        /// based on it's index location in the collection. Returns the new ordered collection.
        /// </summary>
        /// <param name="policyRankDetails"></param>
        public static IEnumerable<PolicyRankDetailViewModel> CompactRankOrder(this IEnumerable<PolicyRankDetailViewModel> policyRankDetails)
        {
            // only re-order the active policies. ranking is applied based what insurance type the policy belongs to.
            var listOfActivePoliciesGroupedByInsuranceType = policyRankDetails.WhereNotEnded().GroupBy(x => x.InsuranceType).ToList();

            foreach (var insuranceGroup in listOfActivePoliciesGroupedByInsuranceType)
            {
                // insuranceGroup represents a list of policies that have the same insurance type. here we are ordering this group based on the current rank
                var orderedListOfPolicies = insuranceGroup.OrderBy(p => p.PolicyRank).ToList();

                // here we set each policy's new rank according to the location in the insurance list.  this achieves contiguous ranking.
                orderedListOfPolicies.ForEach(p => p.PolicyRank = orderedListOfPolicies.IndexOf(p) + 1);
            }

            return policyRankDetails.OrderByInsuranceTypeThenByRank().ToList();
        }
    }

    public static class PolicyRankDetailViewModelValidationExtensions
    {

        /// <summary>
        /// Compare the policy's rank, insurance type, and date range against those of the provided list.
        /// If any policies in the list conflict with this policy, validation fails.
        /// </summary>
        /// <param name="policyToValidate"></param>
        /// <param name="policyRankDetails"></param>
        /// <returns></returns>
        public static ValidationResult ValidatePolicyRankOrderForIntersectingDateRanges(this PolicyRankDetailViewModel policyToValidate, IEnumerable<PolicyRankDetailViewModel> policyRankDetails)
        {
            var conflictPolicy = policyRankDetails
                .FirstOrDefault(p => p.PatientInsuranceId != policyToValidate.PatientInsuranceId
                && p.InsuranceType == policyToValidate.InsuranceType
                && p.PolicyRank == policyToValidate.PolicyRank
                && DateTimes.IsOverlapping(p.StartDate, p.EndDate, policyToValidate.StartDate, policyToValidate.EndDate));

            if (conflictPolicy != null)
            {
                var message = "The date range of (PolicyId: {0}) overlaps with (PolicyId: {1}) which has the same insurance type and rank.  Consider changing the end date of one of the policies"
                                .FormatWith(policyToValidate.PatientInsuranceId, conflictPolicy.PatientInsuranceId);

                return new ValidationResult(message, new[] { "EndDate" });
            }

            return ValidationResult.Success;
        }


        /// <summary>
        /// Makes sure the rank order is correct. i.e. makes sure there are no over lapping date ranges for policies that have the same rank.                
        /// </summary>
        /// <param name="policyRankDetails"></param>        
        /// <returns></returns>
        public static ValidationResult ValidatePolicyRankOrderForIntersectingDateRanges(this IEnumerable<PolicyRankDetailViewModel> policyRankDetails)
        {
            foreach (var policyToValidate in policyRankDetails.ToList())
            {
                 var result = policyToValidate.ValidatePolicyRankOrderForIntersectingDateRanges(policyRankDetails.ToList());
                 if (result.IsNotSuccessful()) return result;
            }

            return ValidationResult.Success;
        }
    }

    public class PolicyRankDetailsComparer : IEqualityComparer<PolicyRankDetailViewModel>
    {
        public enum Type
        {
            DateRangeOverlap,            
            InsuranceType,
            PatientInsuranceId,
            PolicyHolderId,
        }

        private readonly Func<PolicyRankDetailViewModel, PolicyRankDetailViewModel, bool> _currentComparer;

        public PolicyRankDetailsComparer(Type policyEqualityComparerType)
        {
            switch (policyEqualityComparerType)
            {
                case Type.DateRangeOverlap:
                    _currentComparer = (x, y) => DateTimes.IsOverlapping(x.StartDate, x.EndDate, y.StartDate, y.EndDate);
                    break;               
                case Type.InsuranceType:
                    _currentComparer = (x, y) => x.InsuranceType.Equals(y.InsuranceType);
                    break;
                case Type.PatientInsuranceId:
                    _currentComparer = (x, y) => x.PatientInsuranceId.Equals(y.PatientInsuranceId);
                    break;
                case Type.PolicyHolderId:
                    _currentComparer = (x, y) => x.PolicyHolderId.Equals(y.PolicyHolderId);
                    break;
            }
        }

        #region IEqualityComparer<PolicyRankDetailViewModel> Members

        public bool Equals(PolicyRankDetailViewModel x, PolicyRankDetailViewModel y)
        {
            return _currentComparer(x, y);
        }

        public int GetHashCode(PolicyRankDetailViewModel obj)
        {
            return obj.GetHashCode();
        }

        #endregion

        #region Convenience Methods

        public static bool Equals(PolicyRankDetailViewModel x, PolicyRankDetailViewModel y, Type comparerType)
        {
            return Create(comparerType).Equals(x, y);
        }

        public static bool PolicyHolderEquals(PolicyRankDetailViewModel x, PolicyRankDetailViewModel y)
        {
            return Equals(x, y, Type.PolicyHolderId);
        }

        public static bool PatientInsuranceIdEquals(PolicyRankDetailViewModel x, PolicyRankDetailViewModel y)
        {
            return Equals(x, y, Type.PatientInsuranceId);
        }

        #endregion

        #region Static Factory Methods

        public static PolicyRankDetailsComparer CreatePolicyHolderComparer()
        {
            return Create(Type.PolicyHolderId);
        }

        public static PolicyRankDetailsComparer CreatePatientInsuranceIdComparer()
        {
            return Create(Type.PatientInsuranceId);
        }

        public static PolicyRankDetailsComparer Create(Type policyEqualityComparerType)
        {
            return new PolicyRankDetailsComparer(policyEqualityComparerType);
        }

        #endregion
    }
}
