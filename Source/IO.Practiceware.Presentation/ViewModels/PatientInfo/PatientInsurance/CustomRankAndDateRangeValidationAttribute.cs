﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    public class CustomRankAndDateRangeValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var policy = validationContext.ObjectInstance as PolicyViewModel;

            if (policy == null)
            {
                throw new ArgumentException("The instance to which this validation attribute has been applied is not of type PolicyViewModel");
            }

            if (policy.IsDeleted) return ValidationResult.Success; // doesn't apply to deleted policies

            if (policy.Parent != null)
            {
                var validationMessage = policy.ValidatePolicyRankOrderForIntersectingDateRanges(policy.Parent.Where(p => p != policy && p.PatientInsuranceId != policy.PatientInsuranceId));
                if (validationMessage != null) return new ValidationResult(validationMessage.ErrorMessage, new[] { validationContext.MemberName });
            }

            return ValidationResult.Success;
        }
    }

}
