﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [DataContract]
    public class PolicyHolderViewModel : IViewModel
    {
        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual DateTime? BirthDate { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string AddressLine1 { get; set; }

        [DataMember]
        public virtual string AddressLine2 { get; set; }

        [DataMember]
        public virtual string City { get; set; }

        [DataMember]
        public virtual string State { get; set; }

        [DataMember]
        public virtual string Zip { get; set; }

        [DataMember]
        public virtual string PhoneNumber { get; set; }

        [DataMember]
        public virtual string Type { get; set; }

        [DataMember]
        public virtual string PracticeResource { get; set; }

        [DataMember]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        public virtual string PrimaryCarePhysician { get; set; }

        [DispatcherThread]
        public virtual byte[] Photo { get; set; }

        
        [DispatcherThread]
        public virtual bool PhotoIsLoaded
        {
            get;
            set;
        }

        [DataMember]
        [DispatcherThread]
        public virtual PostOperativeViewModel PostOp { get; set; }

        [DispatcherThread]
        [DependsOn("PostOp")]
        public virtual bool ShowPostOp { get { return PostOp != null && PostOp.CurrentlyInPostOperativePeriod; } }
    }
}
