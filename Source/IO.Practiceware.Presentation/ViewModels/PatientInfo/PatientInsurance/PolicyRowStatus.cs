﻿
namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    public enum PolicyRowStatus
    {
        Current = 1,
        Ended,
        Deleted
    }
}
