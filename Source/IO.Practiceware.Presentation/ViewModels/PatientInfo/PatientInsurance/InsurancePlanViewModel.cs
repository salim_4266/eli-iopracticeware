﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [DataContract]
    public class InsurancePlanViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string InsuranceName { get; set; }

        [DataMember]
        public virtual string PlanName { get; set; }

        [DataMember]
        public virtual string PlanType { get; set; }

        [DataMember]
        public virtual string AddressLine1 { get; set; }

        [DataMember]
        public virtual string AddressLine2 { get; set; }

        [DataMember]
        public virtual string AddressLine3 { get; set; }

        [DataMember]
        public virtual string City { get; set; }

        [DataMember]
        public virtual string State { get; set; }

        [DataMember]
        public virtual string Zip { get; set; }

        [DataMember]
        public virtual string PhoneNumber { get; set; }

        [DataMember]
        public virtual string PayerId { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool ReferralRequired { get; set; }

        [DataMember]
        public virtual string DiagnosisType { get; set; }

    }
}
