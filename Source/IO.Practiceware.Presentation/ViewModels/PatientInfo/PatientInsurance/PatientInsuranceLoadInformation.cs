﻿using System.Collections.ObjectModel;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [DataContract]
    public class PatientInsuranceLoadInformation : IViewModel
    {
        [Dependency]
        [DataMember]        
        public virtual ObservableCollection<PolicyViewModel> Policies { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> MedicareSecondaryReasonCodes { get; set; }

        [Dependency]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Relationships { get; set; } 
    }
}
