﻿using System;
using System.ComponentModel.DataAnnotations;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    public class CustomEndDateValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var policy = validationContext.ObjectInstance as PolicyViewModel;

            if (policy == null)
            {
                throw new ArgumentException("The instance to which this validation attribute has been applied is not of type PolicyViewModel");
            }

            var result1 = policy.ValidatePolicyEndDateAgainstPolicyholderEndDate();
            if (result1.IsNotSuccessful()) return result1;

            return ValidationResult.Success;
        }
    }

}
