﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Linq;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [SupportsDataErrorInfo]
    public class PatientInsuranceViewViewModel : IViewModel
    {
        #region Data Members
        
        [DispatcherThread]
        public virtual ObservableCollection<PolicyViewModel> Policies { get; set; }

        [DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> MedicareSecondaryReasonCodes { get; set; }

        [DispatcherThread]  
        public virtual ObservableCollection<NamedViewModel> Relationships { get; set; }

        #endregion

        #region Bound UI Helper Properties

        [DispatcherThread]
        public virtual bool ShowCurrentPolicies { get; set; }

        [DispatcherThread]
        public virtual bool ShowEndedPolicies { get; set; }

        [DispatcherThread]
        public virtual bool ShowDeletedPolicies { get; set; }

        [DispatcherThread]
        public virtual bool ShowMedicareSecondaryReason { get; set; }

        //[DependsOn("SecondaryMedicarePolicy")]
        //public virtual bool ShowMedicareSecondaryReason
        //{
        //    get { return SecondaryMedicarePolicy != null; }
        //}

        [DispatcherThread]
        [DependsOn("Policies")]
        public virtual int? BilledPolicyHolderId
        {
            get
            {
                var policy = Policies.FirstOrDefault(x => !x.IsDeleted && x.IsBilledPolicyHolder && x.PolicyStatus != PolicyRowStatus.Ended);
                return policy != null ? policy.PolicyHolder.Id : new int?();
            }
        }

        public virtual PatientInfoViewModel PatientViewModel { get; set; }

        [DispatcherThread, RequiredIf("ShowMedicareSecondaryReason", ErrorMessage = "Must choose a Medicare Secondary Reason when no medicare policies are ranked 1")]
        [DependsOn("SecondaryMedicarePolicy", "PatientViewModel")]
        public virtual int? SelectedMedicareSecondaryReasonCode
        {
            get
            {
                if (SecondaryMedicarePolicy != null && !SecondaryMedicarePolicy.MedicareSecondaryReasonCode.HasValue && PatientViewModel != null && PatientViewModel.Age.HasValue && PatientViewModel.Age >= 65)
                {
                    return (int) MedicareSecondaryReasonCode.WorkingAged;
                }

                return SecondaryMedicarePolicy != null && SecondaryMedicarePolicy.MedicareSecondaryReasonCode.HasValue ? (int)SecondaryMedicarePolicy.MedicareSecondaryReasonCode.Value : default(int?);
            }
            set
            {
                if (SecondaryMedicarePolicy != null)
                {
                    SecondaryMedicarePolicy.MedicareSecondaryReasonCode = value.HasValue ? (MedicareSecondaryReasonCode)value : default(MedicareSecondaryReasonCode?);
                }
            }
        }

        [DependsOn("Policies")]
        public virtual PolicyViewModel SecondaryMedicarePolicy
        {
            get { return Policies.IsNotNullOrEmpty() ? Policies.FirstOrDefault(x => x.InsuranceType == InsuranceType.MajorMedical && x.IsMedicarePolicy && x.PolicyRank > 1 && !x.IsDeleted) : null; }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual int ShowPolicyStatusCurrent
        {
            get { return ShowCurrentPolicies ? (int)PolicyRowStatus.Current : -1; }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual int ShowPolicyStatusEnded
        {
            get { return ShowEndedPolicies ? (int)PolicyRowStatus.Ended : -1; }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual int ShowPolicyStatusDeleted
        {
            get { return ShowDeletedPolicies ? (int)PolicyRowStatus.Deleted : -1; }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual bool ShowMajorMedicalGridView
        {
            // this needs to be done in code
            get { return ShouldShowGridView(InsuranceType.MajorMedical); }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual bool ShowVisionGridView
        {
            // this needs to be done in code
            get { return ShouldShowGridView(InsuranceType.Vision); }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual bool ShowWorkersCompGridView
        {
            // this needs to be done in code
            get { return ShouldShowGridView(InsuranceType.WorkersComp); }
        }

        [DispatcherThread]
        [DependsOn("ShowCurrentPolicies", "ShowEndedPolicies", "ShowDeletedPolicies")]
        public virtual bool ShowOthersGridView
        {
            // this needs to be done in code
            get { return ShouldShowGridView(InsuranceType.Ambulatory); }
        }

        #endregion

        private bool ShouldShowGridView(InsuranceType insuranceType)
        {
            var count = 0;

            if (ShowCurrentPolicies)
            {
                count += Policies.Count(x => x.InsuranceType == insuranceType && x.PolicyStatus == PolicyRowStatus.Current);
            }

            if (ShowEndedPolicies)
            {
                count += Policies.Count(x => x.InsuranceType == insuranceType && x.PolicyStatus == PolicyRowStatus.Ended);
            }

            if (ShowDeletedPolicies)
            {
                count += Policies.Count(x => x.InsuranceType == insuranceType && x.PolicyStatus == PolicyRowStatus.Deleted);
            }

            return count > 0;
        }
    }
}
