﻿using System;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [DataContract]
    public class PolicyDetailsViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<LinkedPatient> LinkedPatients { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<AttachedImage> AttachedImages { get; set; }

        [DataMember]
        public virtual DateTime? PolicyHoldersMainInsuranceEndDate { get; set; }
    }

    [DataContract]
    public class LinkedPatient : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Name { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsPolicyHolder { get; set; }
    }

    [DataContract]
    public class AttachedImage : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual byte[] Image { get; set; }
    }
}
