﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PolicyViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int PatientInsuranceId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int InsurancePolicyId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int InsuredPatientId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int PolicyRank { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int InsuranceTypeId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int InsurerId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual PolicyHolderViewModel PolicyHolder { get; set; }

        [DataMember]
        [Required]
        public virtual PolicyHolderRelationshipType Relationship { get; set; }

        [DataMember]
        public virtual ObservableCollection<InsurerClaimFormViewModel> InsurerClaimForm { get; set; }

        [DependsOn("PolicyHolderIsSelf")]
        [DispatcherThread]
        public virtual bool PolicyHolderIsNotSelf
        {
            get { return !PolicyHolderIsSelf; }
        }

        [DependsOn("Relationship", "IsDeleted")]
        [DispatcherThread]
        public virtual bool IsNotLinkedPolicyOrIsDeletedOrDoesNotHavePermission
        {
            get { return Relationship == PolicyHolderRelationshipType.Self || IsDeleted || !PermissionId.EditPatientInsuranceForOther.PrincipalContextHasPermission(); }
        }

        [DependsOn("Relationship", "IsDeleted")]
        [DispatcherThread]
        public virtual bool IsLinkedPolicyOrIsDeletedOrDoesNotHavePermission
        {
            get
            {
                return Relationship != PolicyHolderRelationshipType.Self || IsDeleted || !PermissionId.EditPatientInsuranceForSelf.PrincipalContextHasPermission();
            }
        }

        [DependsOn("Relationship", "IsDeleted")]
        [DispatcherThread]
        public virtual bool IsLinkedPolicyOrIsDeleted
        {
            get
            {
                return Relationship != PolicyHolderRelationshipType.Self || IsDeleted;
            }
        }

        [DependsOn("IsDeleted")]
        [DispatcherThread]
        public virtual bool IsDeletedOrDoesNotHavePermission
        {
            get { return IsDeleted || (!PermissionId.EditPatientInsuranceForSelf.PrincipalContextHasPermission() && !PermissionId.EditPatientInsuranceForOther.PrincipalContextHasPermission()); }
        }

        [DependsOn("Relationship")]
        [DispatcherThread]
        public virtual bool PolicyHolderIsSelf
        {
            get { return Relationship == PolicyHolderRelationshipType.Self || InsuredPatientId == PolicyHolder.Id; }
        }

        [DispatcherThread]
        public virtual int RelationshipId
        {
            get { return (int)Relationship; }
            set { Relationship = (PolicyHolderRelationshipType)value; }
        }

        public string RelationshipDisplay
        {
            get { return Relationship.GetDisplayName(); }
        }

        [DataMember]
        [DispatcherThread]
        public virtual InsurancePlanViewModel InsurancePlan { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual NamedViewModel InsurerPlanType { get; set; }

        [Expression("IsValidPolicyCode", ErrorMessageExpression = "string.Format(\"Policy # does not match Policy Number Format {0}\", instance.PolicyNumberFormat)"), Required, DataMember]
        [DispatcherThread]
        public virtual string PolicyCode { get; set; }

        [DataMember]
        public virtual string PolicyNumberFormat { get; set; }

        public virtual bool IsValidPolicyCode
        {
            get
            {
                if (string.IsNullOrEmpty(PolicyNumberFormat)) return true;
                var policyNumberPattern = string.Format("^{0}", PolicyNumberFormat.Replace("N", "([0-9])"));
                policyNumberPattern = policyNumberPattern.Replace("A", "([a-zA-Z])");
                policyNumberPattern = policyNumberPattern.Replace("E", "([0-9]|[a-zA-Z])");
                policyNumberPattern = string.Format("{0}$", policyNumberPattern.Replace("O", "([0-9]|[a-zA-Z]{0,1})"));
                return Regex.IsMatch(PolicyCode, policyNumberPattern);
            }
        }

        [DataMember]
        [DispatcherThread]
        public virtual string GroupCode { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual decimal? Copay { get; set; }

        [DispatcherThread]
        [DependsOn("Copay")]
        public virtual string CopayFormated
        {
            get { return Copay.HasValue && Copay.Value > 0 ? Copay.Value.ToString("c") : string.Empty; }
        }

        [DataMember]
        [DispatcherThread]
        public virtual decimal? Deductible { get; set; }

        [DispatcherThread]
        [DependsOn("Deductible")]
        public virtual string DeductibleFormated
        {
            get { return Deductible.HasValue && Deductible.Value > 0 ? Deductible.Value.ToString("c") : string.Empty; }
        }

        [DataMember]
        [Required]
        [DispatcherThread]
        public virtual DateTime? StartDate { get; set; }

        private const string EndDateErrorMessage = "The end date cannot be before the start date.  It has been reverted to the original value";

        [DataMember]
        [EqualToGreaterThan("StartDate", ErrorMessage = EndDateErrorMessage)]
        [CustomRankAndDateRangeValidation]
        [CustomEndDateValidation]
        [DispatcherThread]
        public virtual DateTime? EndDate { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual InsuranceType InsuranceType { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsMedicarePolicy { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual MedicareSecondaryReasonCode? MedicareSecondaryReasonCode { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual PolicyDetailsViewModel PolicyDetails { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsDeleted { get; set; }

        [DispatcherThread]
        [DependsOn("IsDeleted", "EndDate")]
        public virtual PolicyRowStatus PolicyStatus
        {
            get
            {
                if (IsDeleted) return PolicyRowStatus.Deleted;

                return this.HasNotEnded() ? PolicyRowStatus.Current : PolicyRowStatus.Ended;
            }
        }


        [DispatcherThread]
        [DataMember]
        public virtual bool IsBilledPolicyHolder
        {
            get { return _isBilledPolicyHolder; }
            set
            {
                if (value)
                {
                    if (Parent != null)
                    {
                        foreach (var p in Parent.ToArray())
                        {
                            if (p == this) continue;

                            if (p.IsDeleted) continue;
                            p.IsBilledPolicyHolder = false;
                        }
                    }
                }

                _isBilledPolicyHolder = value;
            }
        }

        /// <summary>
        /// A arbitrary reference to the parent list of policies that this policy belongs to.  Used for custom attribute validation to 
        /// validate the current policy against the list of policies for a patient.
        /// </summary>
        internal List<PolicyViewModel> Parent;

        private bool _isBilledPolicyHolder;
        public ICommand AttachImage { get; set; }
    }


    /// <summary>
    /// Extensions for <see cref="PolicyViewModel"/>s.  Provides methods such as filtering a list of policies based on whether or not they've ended.
    /// </summary>
    public static class PolicyViewModelExtensions
    {
        #region Mappers

        private static readonly IMapper<PolicyViewModel, PolicyRankDetailViewModel> _policyToRankDetailsMapper
            = Mapper.Factory.CreateMapper<PolicyViewModel, PolicyRankDetailViewModel>(policy => new PolicyRankDetailViewModel
            {
                PatientInsuranceId = policy.PatientInsuranceId,
                PolicyHolderId = policy.PolicyHolder != null ? policy.PolicyHolder.Id : 0,
                InsuranceTypeId = (int)policy.InsuranceType,
                PolicyRank = policy.PolicyRank,
                StartDate = policy.StartDate.Value,
                EndDate = policy.EndDate
            });

        private static readonly IMapper<Model.PatientInsurance, PolicyRankDetailViewModel> _insuranceToRankDetailsMapper
            = Mapper.Factory.CreateMapper<Model.PatientInsurance, PolicyRankDetailViewModel>(patientInsurance => new PolicyRankDetailViewModel
            {
                PatientInsuranceId = patientInsurance.Id,
                PolicyHolderId = patientInsurance.InsurancePolicy != null ? patientInsurance.InsurancePolicy.PolicyholderPatientId : 0,
                InsuranceTypeId = (int)patientInsurance.InsuranceType,
                PolicyRank = patientInsurance.OrdinalId,
                StartDate = patientInsurance.InsurancePolicy.StartDateTime,
                EndDate = patientInsurance.EndDateTime
            });



        private static readonly IMapper<Model.PatientInsurance, PolicyViewModel> _patientInsuranceToPolicyViewModelMapper = Mapper.Factory
            .CreateMapper<Model.PatientInsurance, PolicyViewModel>(patientInsurance => new PolicyViewModel
                                {
                                    PatientInsuranceId = patientInsurance.Id,
                                    InsurancePolicyId = patientInsurance.InsurancePolicyId,
                                    InsuredPatientId = patientInsurance.InsuredPatientId,
                                    Copay = patientInsurance.InsurancePolicy.IfNotNull(x => x.Copay),
                                    Deductible = patientInsurance.InsurancePolicy.IfNotNull(x => x.Deductible),
                                    EndDate = patientInsurance.EndDateTime,
                                    GroupCode = patientInsurance.InsurancePolicy.IfNotNull(x => x.GroupCode),
                                    InsuranceType = patientInsurance.InsuranceType,
                                    InsurerId = patientInsurance.InsurancePolicy.IfNotNull(x => x.InsurerId),
                                    InsuranceTypeId = (int)patientInsurance.InsuranceType,
                                    InsurancePlan = new InsurancePlanViewModel
                                                    {
                                                        Id = patientInsurance.InsurancePolicy.IfNotNull(x => x.InsurerId),
                                                        AddressLine1 = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.Line1),
                                                        AddressLine2 = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.Line2),
                                                        AddressLine3 = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.Line3),
                                                        City = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.City),
                                                        State = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.StateOrProvince.IfNotNull(y => y.Abbreviation)),
                                                        Zip = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerAddresses).IfNotNull(x => x.WithAddressType(InsurerAddressTypeId.Claims, NotFoundBehavior.None)).IfNotNull(x => x.PostalCode),
                                                        Comments = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.Comment),
                                                        InsuranceName = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.Name),
                                                        PlanName = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.PlanName),
                                                        PlanType = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerPlanType).IfNotNull(x => x.Name),
                                                        ReferralRequired = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.IsReferralRequired),
                                                        PayerId = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.PayerCode),
                                                        PhoneNumber = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerPhoneNumbers).IfNotNull(x => x.WithPhoneNumberType(InsurerPhoneNumberTypeId.Main, NotFoundBehavior.None)).IfNotNull(x => x.ToString())
                                                    },
                                    InsurerPlanType = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerPlanTypeId).HasValue ? new NamedViewModel { Id = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerPlanType).IfNotNull(x => x.Id), Name = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.InsurerPlanType).IfNotNull(x => x.Name) } : null,
                                    PolicyCode = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyCode),
                                    PolicyNumberFormat = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.PolicyNumberFormat),
                                    PolicyHolder = new PolicyHolderViewModel
                                                    {
                                                        Id = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.Id),
                                                        AddressLine1 = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientAddresses).IfNotNull(x => x.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None)).IfNotNull(x => x.Line1, ""),
                                                        AddressLine2 = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientAddresses).IfNotNull(x => x.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None)).IfNotNull(x => x.Line2, ""),
                                                        City = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientAddresses).IfNotNull(x => x.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None)).IfNotNull(x => x.City, ""),
                                                        State = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientAddresses).IfNotNull(x => x.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None)).IfNotNull(x => x.StateOrProvince.IfNotNull(y => y.Abbreviation), ""),
                                                        Zip = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientAddresses).IfNotNull(x => x.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None)).IfNotNull(x => x.PostalCode, ""),
                                                        BirthDate = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.DateOfBirth),
                                                        DisplayName = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.DisplayName),
                                                        PhoneNumber = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PrimaryPhoneNumber).ToStringIfNotNull(null) ?? patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.HomePhoneNumber).ToStringIfNotNull(null) ?? patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.CellPhoneNumber).ToStringIfNotNull(null) ?? patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.BusinessPhoneNumber).ToStringIfNotNull(null),
                                                        PracticeResource = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.DefaultUser).IfNotNull(x => x.DisplayName),
                                                        PrimaryCarePhysician = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.ExternalProviders).IfNotNull(x => x.FirstOrDefault(p => p.IsPrimaryCarePhysician)).IfNotNull(x => x.ExternalProvider).IfNotNull(y => y.DisplayName, ""),
                                                        ReferringDoctor = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.ExternalProviders).IfNotNull(x => x.FirstOrDefault(p => p.IsReferringPhysician)).IfNotNull(x => x.ExternalProvider).IfNotNull(y => y.DisplayName, ""),
                                                        Type = patientInsurance.InsurancePolicy.IfNotNull(x => x.PolicyholderPatient).IfNotNull(x => x.PatientTags).IfNotNull(x => x.Join(y => y.Tag.DisplayName, ", ", true), "")
                                                    },
                                    PolicyRank = patientInsurance.OrdinalId,
                                    Relationship = patientInsurance.PolicyholderRelationshipType,
                                    StartDate = patientInsurance.InsurancePolicy.IfNotNull(x => x.StartDateTime),
                                    IsMedicarePolicy = patientInsurance.InsurancePolicy.IfNotNull(x => x.Insurer).IfNotNull(x => x.IsMedicare),
                                    MedicareSecondaryReasonCode = patientInsurance.InsurancePolicy.IfNotNull(x => x.MedicareSecondaryReasonCode),
                                    IsDeleted = patientInsurance.IsDeleted
                                }, false);

        #endregion

        #region Helper Methods

        /// <summary>
        /// Returns true if the policy has ended.  Ended in this case means the enddate is before today's date.
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool HasEnded(this PolicyViewModel policy)
        {
            return policy.EndDate.IsBeforeToday();
        }

        /// <summary>
        /// Returns true if the policy HAS NOT ended. Ended in this case means the enddate is before today's date.
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool HasNotEnded(this PolicyViewModel policy)
        {
            return !policy.HasEnded();
        }

        /// <summary>
        /// Returns true if the policy is 'Active'.  'Active' means it is not deleted and it has not ended.
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool IsActive(this PolicyViewModel policy)
        {
            return !policy.IsDeleted && policy.HasNotEnded();
        }

        /// <summary>
        /// Filters and returns a new list of policies that have not ended. 'Ended' means the policy has an EndDate
        /// and the EndDate is before today's date.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IEnumerable<PolicyViewModel> WhereNotEnded(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.Where(p => p.HasNotEnded());
        }

        /// <summary>
        /// Filters and returns a new list of policies that have not been deleted.  
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IEnumerable<PolicyViewModel> WhereNotDeleted(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.Where(p => !p.IsDeleted);
        }

        /// <summary>
        /// Filters and returns a new list of policies that are considered 'Active'.  'Active' means
        /// Not Deleted, and Not Ended.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IEnumerable<PolicyViewModel> WhereActive(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.WhereNotDeleted().WhereNotEnded();
        }

        /// <summary>
        /// Returns a list of policies where the insurance type matches the provided <see cref="type"/>
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IEnumerable<PolicyViewModel> WhereInsuranceTypeIs(this IEnumerable<PolicyViewModel> policies, InsuranceType type)
        {
            return policies.Where(p => p.InsuranceType == type);
        }

        /// <summary>
        /// Returns an ordered list of policies ordered by insurance type
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<PolicyViewModel> OrderByInsuranceType(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.OrderBy(p => p.InsuranceType);
        }

        /// <summary>
        /// Returns an ordered list of policies ordered by PolicyRank.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<PolicyViewModel> OrderByRank(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.OrderBy(p => p.PolicyRank);
        }

        /// <summary>
        /// Applies a subsequent ordering of an ordered list of policies by ascending PolicyRank
        /// </summary>
        /// <param name="orderedPolicies"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<PolicyViewModel> ThenByRank(this IOrderedEnumerable<PolicyViewModel> orderedPolicies)
        {
            return orderedPolicies.ThenBy(p => p.PolicyRank);
        }

        /// <summary>
        /// Returns an ordered list of policies.  Orders first by Insurance Type, then by Policy Rank.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<PolicyViewModel> OrderByInsuranceTypeThenRank(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.OrderByInsuranceType().ThenByRank();
        }

        /// <summary>
        /// Maps each <see cref="PolicyViewModel"/> element in the list to a new List of <see cref="PolicyRankDetailViewModel"/> 
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static List<PolicyRankDetailViewModel> ToPolicyRankDetails(this IEnumerable<PolicyViewModel> policies)
        {
            return _policyToRankDetailsMapper.MapAll(policies).ToList();
        }

        /// <summary>
        /// Maps each <see cref="Model.PatientInsurance"/> element in the list to a new List of <see cref="PolicyRankDetailViewModel"/> 
        /// </summary>
        /// <param name="patientInsurances"></param>
        /// <returns></returns>
        public static List<PolicyRankDetailViewModel> ToPolicyRankDetails(this IEnumerable<Model.PatientInsurance> patientInsurances)
        {
            return _insuranceToRankDetailsMapper.MapAll(patientInsurances).ToList();
        }

        /// <summary>
        /// Uses a custom mapper created by the mapper factory to map all the <see cref="Model.PatientInsurance"/> objects to
        /// <see cref="PolicyViewModel"/> objects and returns the new list.
        /// </summary>
        /// <param name="patientInsurances"></param>
        /// <returns></returns>
        public static List<PolicyViewModel> ToPolicyViewModels(this IEnumerable<Model.PatientInsurance> patientInsurances)
        {
            return _patientInsuranceToPolicyViewModelMapper.MapAll(patientInsurances).ToList();
        }

        /// <summary>
        /// Uses a custom mapper created by the mapper factory to map the <see cref="Model.PatientInsurance"/> object to
        /// a <see cref="PolicyViewModel"/> object.
        /// </summary>
        /// <param name="patientInsurance"></param>
        /// <returns></returns>
        public static PolicyViewModel ToPolicyViewModel(this Model.PatientInsurance patientInsurance)
        {
            return _patientInsuranceToPolicyViewModelMapper.Map(patientInsurance);
        }

        /// <summary>
        /// Assigns a 'PolicyRank' number to each 'active' policy based on the current index of that policy
        /// in the list as it stands now.
        /// 
        /// The index is calculated by the insurance type.  So for instance, all major medical policies will 
        /// be ranked according the index in the list where insurance type = major medical.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns>The original policy list with the policies modified.</returns>
        public static void ApplyIndexToRank(this IEnumerable<PolicyViewModel> policies)
        {
            var policyViewModels = policies as PolicyViewModel[] ?? policies.ToArray();
            policyViewModels
                .ToList()
                .ForEach(policy => policy.PolicyRank = policyViewModels
                    .WhereInsuranceTypeIs(policy.InsuranceType)
                    .ToList()
                    .IndexOf(policy) + 1);
        }

        /// <summary>
        /// Compacts the rank of each 'active' policy so that they are contiguous and not fragmented. 
        /// Achieves this by calling the <see cref="OrderByInsuranceTypeThenRank"/> method then setting the new rank
        /// based on it's index location in the collection within it's insurance type. Returns the new ordered collection.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns>The original list</returns>
        public static void CompactRanking(this IEnumerable<PolicyViewModel> policies)
        {
            policies.OrderByInsuranceTypeThenRank().ApplyIndexToRank();
        }

        /// <summary>
        /// Returns the max 'PolicyRank'.  Only looks at 'Active' policies.
        /// </summary>
        /// <param name="policies"></param>        
        /// <returns>An integer representing the maximum PolicyRank occuring in the list of policies</returns>
        public static int GetMaxRank(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.WhereActive().Max(p => p.PolicyRank);
        }

        /// <summary>
        /// Returns the max 'PolicyRank' out of all the <see cref="insuranceType"/> policies.  Only looks at 'Active' policies.
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="insuranceType"></param>
        /// <returns>An integer representing the maximum PolicyRank occuring in the list of policies</returns>
        public static int GetMaxRank(this IEnumerable<PolicyViewModel> policies, InsuranceType insuranceType)
        {
            var list = policies.WhereActive().WhereInsuranceTypeIs(insuranceType);
            return list.IsNotNullOrEmpty() ? list.Max(p => p.PolicyRank) : 0;
        }

        /// <summary>
        /// Sets the deleted <see cref="policyToReactivate"/> to not deleted, calculates the new PolicyRank
        /// and inserts it into the list at the appropriate position/index.
        /// </summary>
        /// <remarks>
        /// Checks to make sure the <see cref="policyToReactivate"/> is not already in the list, and if it
        /// is, will rectify it.
        /// </remarks>
        /// <param name="policies"></param>
        /// <param name="policyToReactivate"></param>        
        public static void ReactivateAndInsert(this Collection<PolicyViewModel> policies, PolicyViewModel policyToReactivate)
        {
            if (policyToReactivate == null || !policyToReactivate.IsDeleted) return;

            // figure out the next highest rank
            var highestRankedPolicy = policies.WhereActive().WhereInsuranceTypeIs(policyToReactivate.InsuranceType).OrderByRank().LastOrDefault();
            var maxPolicyRank = highestRankedPolicy.IfNotNull(p => p.PolicyRank, 0);

            policyToReactivate.PolicyRank = ++maxPolicyRank;
            policyToReactivate.IsDeleted = false;

            // check for existing reference and remove
            if (policies.Contains(policyToReactivate)) policies.Remove(policyToReactivate);

            // figure out where to insert the policy in the original collection
            if (highestRankedPolicy != null)
            {
                var index = policies.IndexOf(highestRankedPolicy);
                index = index == -1 ? 0 : index + 1;
                policies.Insert(index, policyToReactivate);
            }
            else
            {
                policies.Add(policyToReactivate);
            }

        }

        #endregion
    }

    /// <summary>
    /// Provides validation methods for lists of <see cref="PolicyViewModel"/>
    /// </summary>
    public static class PolicyViewModelValidationLogic
    {

        /// <summary>
        /// Validates the <see cref="policy"/> against the ones in the list <see cref="policies"/>
        /// 
        /// i.e. Check the insurance type, rank, and date range against the ones in the list.  If all 3 of those properties conflict with another policy. Validation fails.
        /// </summary>        
        /// 
        /// <param name="policy">The policy to Validate</param>
        /// <param name="policies">The list of policies to validate against.</param>        
        /// <returns></returns>
        public static ValidationResult ValidatePolicyRankOrderForIntersectingDateRanges(this PolicyViewModel policy, IEnumerable<PolicyViewModel> policies)
        {
            return new List<PolicyViewModel> { policy }.ToPolicyRankDetails().First().ValidatePolicyRankOrderForIntersectingDateRanges(policies.ToPolicyRankDetails());
        }

        /// <summary>
        /// Makes sure the rank order is correct between each policy in the list.  
        /// 
        /// i.e. Checks policies with the same rank do not have overlapping date ranges.
        /// </summary>
        /// <param name="policies"></param>
        /// <returns></returns>
        public static ValidationResult ValidatePolicyRankOrderForIntersectingDateRanges(this IEnumerable<PolicyViewModel> policies)
        {
            return policies.WhereNotDeleted().ToPolicyRankDetails().ValidatePolicyRankOrderForIntersectingDateRanges();
        }

        /// <summary>
        /// Validates the policy's EndDate against the EndDate of the Policyholder's main patient insurance.
        /// 
        /// Checks to make sure the policy has an EndDate IF the Policyholder also has one
        /// Checks to make sure the policy's EndDate is not after the EndDate of the Policyholder.        
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static ValidationResult ValidatePolicyEndDateAgainstPolicyholderEndDate(this PolicyViewModel policy)
        {
            // validation here doesn't apply to deleted policies or self policies
            if (policy.IsDeleted || policy.PolicyHolderIsSelf) return ValidationResult.Success;

            // check if the current policy has no enddate value but the main insurance of the policy holder's enddate does have a value.
            if (!policy.EndDate.HasValue && policy.PolicyDetails.PolicyHoldersMainInsuranceEndDate.HasValue)
            {
                return new ValidationResult("Uh-oh, you can't remove the End Date here because the policyholder's policy still has an End Date. Remove the End Date on the policyholder's policy first", new[] { "EndDate" });
            }

            // check if the current policy's enddate is set to be after the policy holder's end date
            if (policy.EndDate.HasValue && policy.PolicyDetails.PolicyHoldersMainInsuranceEndDate.HasValue
                && policy.EndDate.Value.Date > policy.PolicyDetails.PolicyHoldersMainInsuranceEndDate.Value.Date)
            {
                return new ValidationResult("Whoops, the End Date you are trying to enter is after the policyholder's policy End Date. Update the End Date of the policyholder's policy first", new[] { "EndDate" });
            }

            return ValidationResult.Success;
        }
    }

    public class PolicyComparer : IEqualityComparer<PolicyViewModel>
    {
        public enum Type
        {
            DateRangeOverlap,
            InsurancePolicyId,
            InsuranceType,
            PatientInsuranceId,
            PolicyHolderId,
        }

        private readonly Func<PolicyViewModel, PolicyViewModel, bool> _currentComparer;

        public PolicyComparer(Type policyEqualityComparerType)
        {
            switch (policyEqualityComparerType)
            {
                case Type.DateRangeOverlap:
                    _currentComparer = (x, y) => DateTimes.IsOverlapping(x.StartDate.Value, x.EndDate, y.StartDate.Value, y.EndDate);
                    break;
                case Type.InsurancePolicyId:
                    _currentComparer = (x, y) => x.InsurancePolicyId.Equals(y.InsurancePolicyId);
                    break;
                case Type.InsuranceType:
                    _currentComparer = (x, y) => x.InsuranceType.Equals(y.InsuranceType);
                    break;
                case Type.PatientInsuranceId:
                    _currentComparer = (x, y) => x.PatientInsuranceId.Equals(y.PatientInsuranceId);
                    break;
                case Type.PolicyHolderId:
                    _currentComparer = (x, y) => x.PolicyHolder.Id.Equals(y.PolicyHolder.Id);
                    break;
            }
        }

        #region IEqualityComparer<PolicyRankDetailViewModel> Members

        public bool Equals(PolicyViewModel x, PolicyViewModel y)
        {
            return _currentComparer(x, y);
        }

        public int GetHashCode(PolicyViewModel obj)
        {
            return obj.GetHashCode();
        }

        #endregion

        public static PolicyComparer Create(Type policyEqualityComparerType)
        {
            return new PolicyComparer(policyEqualityComparerType);
        }
    }
   
}
