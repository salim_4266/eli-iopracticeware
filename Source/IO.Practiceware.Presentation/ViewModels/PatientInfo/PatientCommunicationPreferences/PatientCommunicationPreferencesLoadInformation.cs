﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences
{
    [DataContract]
    public class PatientCommunicationPreferencesLoadInformation : IViewModel
    {
        [DataMember]
        [Dependency]
        public virtual List<PatientContactViewModel> Contacts { get; set; }

        [DataMember]
        [Dependency]
        public virtual List<PatientCommunicationPreferenceViewModel> PatientCommunicationPreferences { get; set; }

        [DataMember]
        public virtual string PatientCommunicationPreferencesNotes { get; set; }
    }
}
