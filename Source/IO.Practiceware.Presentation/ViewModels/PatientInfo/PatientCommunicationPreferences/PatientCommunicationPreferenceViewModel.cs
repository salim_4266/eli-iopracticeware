﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class PatientCommunicationPreferenceViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual PatientCommunicationType CommunicationType { get; set; }

        [DependsOn("CommunicationType")]
        public virtual bool TypeHasIoAutomation
        {
            get { return CommunicationType.HasIoAutomation(); }
        }

        [DataMember]        
        [DispatcherThread]
        public virtual PatientContactViewModel PatientContact { get; set; }
        
        [DispatcherThread]
        public virtual CommunicationMethodType? Method
        {
            get
            {
                return (CommunicationMethodType?)MethodId;
            }
            set
            {
                MethodId = (int?)value;
            }
        }

        [DataMember]
        [DispatcherThread]
        public virtual int? MethodId
        {
            get;
            set;
        }

        [DataMember]
        public virtual string SelectedDestination { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual long? DestinationId { get; set; }

        [DependsOn("PatientContact", "DestinationType")]
        public virtual IEnumerable<Tuple<NamedViewModel<long>, bool>> Destinations
        {
            get
            {
                if (PatientContact == null) return null;

                if (DestinationType == typeof (PatientAddressCommunicationPreference))
                {
                    if (PatientContact.Addresses.IsNullOrEmpty()) return null;
                    var primary = PatientContact.Addresses.FirstOrDefault();
                    var list = PatientContact.Addresses.Select(a => new Tuple<NamedViewModel<long>, bool>( new NamedViewModel<long>{ Id = a.Id.EnsureNotDefault().Value, Name = a.ToString()}, a == primary )).ToList();
                    list.Insert(0, new Tuple<NamedViewModel<long>, bool>(null, false));
                    return list;
                }

                if (DestinationType == typeof(PatientEmailAddressCommunicationPreference))
                {
                    if (PatientContact.EmailAddresses.IsNullOrEmpty()) return null;
                    var primary = PatientContact.EmailAddresses.OrderBy(x => x.OrdinalId).FirstOrDefault();
                    var list = PatientContact.EmailAddresses.Select(a => new Tuple<NamedViewModel<long>, bool>( new NamedViewModel<long>{ Id = (long)a.Id, Name = a.Value}, a == primary )).ToList();
                    list.Insert(0, new Tuple<NamedViewModel<long>, bool>(null, false));
                    return list;
                }

                if (DestinationType == typeof(PatientPhoneNumberCommunicationPreference))
                {
                    if (PatientContact.PhoneNumbers.IsNullOrEmpty()) return null;
                    var primary = PatientContact.PhoneNumbers.FirstOrDefault(x => x.IsPrimary);
                    var list = PatientContact.PhoneNumbers.Select(a => new Tuple<NamedViewModel<long>, bool>(new NamedViewModel<long> { Id = a.Id.HasValue ? a.Id.Value : 0, Name = a.ToString() }, a == primary)).ToList();
                    list.Insert(0, new Tuple<NamedViewModel<long>, bool>(null, false));
                    return list;
                }

                return null;
            }
        }

        [DataMember]
        public virtual bool IsDefault { get; set; }

        [DependsOn("MethodId")]
        public virtual Type DestinationType
        {
            get
            {
                return Method.HasValue && Enum.IsDefined(typeof(CommunicationMethodType), Method.Value) ? PatientCommunicationMappings.CommunicationMethodToPatientCommunicationPreferenceMapping[Method.Value] : null;
            }
        }

        [DependsOn("CommunicationType")]
        public virtual IEnumerable<NamedViewModel<int?>> Methods
        {
            get
            {
                var listOfMethods = PatientCommunicationMappings.PatientCommunicationTypeToCommunicationMethodMapping[CommunicationType].Select(x => new NamedViewModel<int?> {Id = (int)x, Name = x.GetDisplayName()}).ToList();                
                listOfMethods.Insert(0, null);
                return listOfMethods;
            }
        }

        [DispatcherThread]
        [DependsOn("CommunicationType")]
        public virtual bool IsPreferenceEnabled {
            get { return CommunicationType == PatientCommunicationType.Emergency; }
        }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsOptOut { get; set; }

        [DispatcherThread]
        [DependsOn("CommunicationType")]
        public virtual bool IsOptOutEnabled
        {
            get { return (CommunicationType == PatientCommunicationType.Collection || CommunicationType == PatientCommunicationType.Statement); }
        }
    }

    public static class PatientCommunicationPreferencesExtensions
    {
        public static int GetOrder(this PatientCommunicationType type)
        {
            return type.GetAttribute<DisplayAttribute>().Order;
        }

        public static IOrderedEnumerable<PatientCommunicationPreferenceViewModel> OrderByCommunicationTypeOrdering(this IEnumerable<PatientCommunicationPreferenceViewModel> preferences)
        {            
            return preferences.OrderBy(p => p.CommunicationType.GetOrder()).ThenBy(p => p.Id.HasValue ? p.Id.Value : Int32.MaxValue);
        }

        public static IOrderedEnumerable<PatientCommunicationType> OrderByAttributeOrder(this IEnumerable<PatientCommunicationType> types)
        {
            return types.OrderBy(t => t.GetOrder());
        }
    }
}
