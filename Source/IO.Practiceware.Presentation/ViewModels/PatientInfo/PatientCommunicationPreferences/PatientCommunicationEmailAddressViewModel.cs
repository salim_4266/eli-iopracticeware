﻿using IO.Practiceware.Model;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences
{
    [DataContract]
    public class PatientCommunicationEmailAddressViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Value { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }

        [DataMember]
        public virtual EmailAddressType EmailAddressType { get; set; }
    }
}
