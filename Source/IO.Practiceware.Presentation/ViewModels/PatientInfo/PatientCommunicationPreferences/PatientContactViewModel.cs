﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences
{
    [DataContract]
    public class PatientContactViewModel : IViewModel
    {
        [DataMember]
        public virtual int PatientRelationshipId { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string RelationshipDescription { get; set; }     

        [DependsOn("Addresses")]
        public virtual AddressViewModel MainAddress
        {
            get { return Addresses != null ? Addresses.FirstOrDefault() : null; }
        }

        [DependsOn("PhoneNumbers")]
        public virtual PhoneNumberViewModel MainPhoneNumber
        {
            get { return PhoneNumbers != null ? PhoneNumbers.FirstOrDefault(x => x.IsPrimary) : null; }
        }

        [DependsOn("EmailAddresses")]
        public virtual EmailAddressViewModel MainEmailAddress
        {
            get { return EmailAddresses != null ? EmailAddresses.OrderBy(x => x.OrdinalId).FirstOrDefault() : null; }
        }

        [DataMember]
        public virtual bool Hippa { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<AddressViewModel> Addresses { get; set; }
  
        [DataMember]
        [Dependency]
        public virtual ObservableCollection<PhoneNumberViewModel> PhoneNumbers { get; set; }      

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<EmailAddressViewModel> EmailAddresses { get; set; }

        [DependsOn("RelationshipDescription")]
        public virtual bool IsSelfContact
        {
            get
            {
                return RelationshipDescription == PatientRelationshipTypeId.Self.ToString();
            }
        }
    }
}
