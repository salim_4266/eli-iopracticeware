﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary
{
    [DataContract]
    public class PatientAccountViewModel : IViewModel
    {
        [DependsOn("Invoices")]
        public virtual Decimal InsurerBalance
        {
            get
            {
                return GetInsurerBilledTotal();
            }
        }

        [DependsOn("Invoices")]
        public virtual Decimal PatientBalance
        {
            get
            {
                return GetPatientBilledTotal();
            }
        }

        [DependsOn("Invoices", "OnAccountTransactions")]
        public virtual Decimal UnbilledTotal
        {
            get
            {
                return GetUnbilledTotal();
            }
        }

        [DependsOn("Invoices", "OnAccountTransactions")]
        public virtual Decimal AccountBalance
        {
            get
            {
                return GetBalance();
            }
        }

        [DependsOn("OnAccountTransactions")]
        public virtual Decimal OnAccountAdjustmentTotal
        {
            get
            {
                return GetOnAccountAdjustmentsTotal();
            }
        }

        [DataMember]
        public virtual ObservableCollection<TransactionViewModel> OnAccountTransactions { get; set; }

        [DataMember]
        public virtual bool FilterByIsOpen { get; set; }

        [DataMember]
        public virtual ObservableCollection<InvoiceViewModel> Invoices { get; set; }


        [DataMember]
        public virtual ObservableCollection<InvoiceViewModel> SelectedInvoices { get; set; }

        #region Totals on top of screen

        public virtual Decimal GetInsurerBilledTotal()
        {
            return Invoices != null ? Invoices.Sum(i => i.InsurerBilled) : 0;
        }

        //Patient Billed
        public virtual Decimal GetPatientBilledTotal()
        {
            return Invoices != null ? Invoices.Sum(i => i.PatientBilled) : 0;
        }

        public virtual Decimal GetUnbilledTotal()
        {
            return GetBalance() - (Invoices != null ? Invoices.Sum(i => i.Billed) : 0);
        }

        /// <summary>
        /// Open Balance total
        /// All open balances in the invoices - all adjustments for the invoice - all on account payments for the patient
        /// </summary>
        /// <returns></returns>
        public virtual Decimal GetBalance()
        {
            return Invoices != null ? GetAdjustmentBalance()
                - GetOnAccountAdjustmentsTotal() : 0;
        }

        public virtual Decimal GetCharges()
        {
            return Invoices != null ? Invoices
                    .ToList()
                    .Sum(i => i.Charge) : 0;
        }

        public virtual Decimal GetAdjustmentBalance()
        {
            return Invoices != null ? Invoices.Sum(i => i.OpenBalance) : 0;
        }

        #endregion
        public virtual Decimal GetOnAccountAdjustmentsTotal()
        {
            return OnAccountTransactions != null
                ? OnAccountTransactions
                    .ToList()
                    .OfType<FinancialTransactionViewModel>()
                    .Where(t => t.FinancialTransactionType is AdjustmentTypeViewModel)
                    .Sum(t => t.Amount)
                : 0;
        }

        public virtual void UpdateInvoice(InvoiceViewModel invoice)
        {
            var currentInvoice = Invoices.Single(i => i.Id == invoice.Id);
            Invoices.Replace(currentInvoice, invoice);
        }
    }
}
