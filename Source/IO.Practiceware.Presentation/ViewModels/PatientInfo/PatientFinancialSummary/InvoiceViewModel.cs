﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.ComponentModel;
using Soaf.Presentation;
using DiagnosisViewModel = IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis.DiagnosisViewModel;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary
{
    [DataContract]
    public class InvoiceViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual ObservableCollection<PaperClaimGenerationInfo> PrintingOptions { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        public virtual bool IsOpen
        {
            get
            {
                return OpenBalance != 0;
            }
        }

        [DataMember, Dependency, DispatcherThread]
        public virtual ObservableCollection<EncounterServiceViewModel> EncounterServices { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> ReferringProviders { get; set; }

        public virtual string ReferringProvider
        {
            get
            {
                return ReferringProviders.Any() ? ReferringProviders[0].Name : "---";
            }
        }

        [DataMember]
        public virtual InvoiceType InvoiceType { get; set; }

        [DataMember]
        public virtual String Referral { get; set; }

        [DataMember]
        public virtual String AuthorizationCode { get; set; }

        [DataMember]
        public virtual NamedViewModel ServiceLocation { get; set; }

        [DataMember]
        public virtual NamedViewModel AttributeTo { get; set; }

        [DataMember]
        public virtual NamedViewModel BillingOrganization { get; set; }

        [DataMember]
        public virtual String ClaimNote { get; set; }

        [DataMember]
        public virtual CareTeamViewModel CareTeam { get; set; }

        [DataMember, Dependency, DispatcherThread]
        public virtual ObservableCollection<TransactionalNoteViewModel> Notes { get; set; }

        [DataMember, Dependency, DispatcherThread]
        public virtual ObservableCollection<DiagnosisViewModel> Diagnoses { get; set; }


        [DataMember, Dependency, DispatcherThread]
        public virtual ObservableCollection<TransactionViewModel> UnassignedTransactions { get; set; }


        [DataMember, Dependency]
        public virtual ObservableCollection<FinancialTransactionViewModel> Adjustments { get; set; }

        [DataMember]
        public virtual int EncounterId { get; set; }

        [DataMember]
        public virtual Decimal InsurerBilled { get; set; }

        [DataMember]
        public virtual Decimal PatientBilled { get; set; }

        [DataMember]
        public virtual DiagnosisTypeId CodesetType { get; set; }

        [DependsOn("EncounterServices")]
        public virtual Decimal Charge
        {
            get
            {
                return GetChargesTotal();
            }
        }

        [DependsOn("EncounterServices")]
        public virtual Decimal Paid
        {
            get
            {
                return GetAdjustmentsTotal();
            }
        }

        [DependsOn("EncounterServices", "Adjustments")]
        public virtual Decimal OpenBalance
        {
            get
            {
                return GetOpenBalanceTotal();
            }
        }

        [DependsOn("EncounterServices")]
        public virtual Decimal Billed
        {
            get
            {
                return GetBilled();
            }
        }

        [DependsOn("EncounterServices")]
        public virtual Decimal Unbilled
        {
            get
            {
                return GetUnbilledTotal();
            }
        }

        #region Invoice Totals Methods
        public virtual Decimal GetChargesTotal()
        {
            return EncounterServices.Sum(i => i.TotalCharge);
        }

        /// <summary>
        /// Sum the Adjustments where adjustment type is cash
        /// We set the amount based on if the adjustment type is debit in the mapper so no need to 
        /// check for if the Adjustment Type IsDebit here
        /// </summary>
        /// <returns></returns>
        public virtual Decimal GetAdjustmentsTotal()
        {
            var result = (Adjustments != null ? Adjustments.Where(a => a.FinancialTransactionType is AdjustmentTypeViewModel
                                                    && ((AdjustmentTypeViewModel)a.FinancialTransactionType).IsCash)
                                                    .Sum(a => a.Amount)
                                                    : 0);
            return result;
        }

        /// <summary>
        /// Get the total of the charges minus all adjustments related to the invoice via invoice receivable
        /// We set the amount based on if the adjustment type is debit in the mapper so no need to 
        /// check for if the Adjustment Type IsDebit here
        /// </summary>
        /// <returns></returns>
        public virtual Decimal GetOpenBalanceTotal()
        {
            return GetChargesTotal() - Adjustments.Sum(x => x.Amount);
        }

        public virtual Decimal GetBilled()
        {
            return EncounterServices.Sum(i => i.BilledTotal);
        }

        public virtual Decimal GetUnbilledTotal()
        {
            return GetOpenBalanceTotal() - GetBilled();
        }
        #endregion
    }
    /// <summary>
    /// All Invoices have an InvoiceType.  This determines what kind of invoice(s) 
    /// </summary>
    public enum InvoiceType
    {
        Professional = 1,
        Facility = 2,
        Vision = 3,
    }
   
}
