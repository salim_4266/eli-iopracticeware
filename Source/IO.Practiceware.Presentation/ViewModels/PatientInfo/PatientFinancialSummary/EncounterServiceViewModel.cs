﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary
{
    [DataContract]
    public class EncounterServiceViewModel : Financials.Common.Service.EncounterServiceViewModel
    {

        [DataMember, DispatcherThread]
        public virtual String ClaimNote { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<TransactionViewModel> Transactions { get; set; }

        [DataMember, DispatcherThread]
        public virtual ObservableCollection<DiagnosisLinkPointerViewModel> LinkedDiagnoses { get; set; }

        [DataMember]
        public virtual ObservableCollection<FinancialTransactionViewModel> Adjustments { get; set; }

        [DependsOn("TotalCharge", "AdjustmentsTotal")]
        public virtual Decimal OpenBalance
        {
            get
            {
                return TotalCharge - AdjustmentsTotal - (Adjustments != null ? 
                                                            Adjustments.Where(a => a.FinancialTransactionType is AdjustmentTypeViewModel
                                                                                && !((AdjustmentTypeViewModel)a.FinancialTransactionType).IsCash)
                                                            .Sum(a => a.Amount)
                                                            : 0);
            }
        }

        public virtual String FormattedModifiers
        {
            get
            {
                return FormatModifiers();
            }
        }

        private String FormatModifiers()
        {
            var toReturn = "";
            if (Modifiers != null)
                foreach (NameAndAbbreviationAndDetailViewModel m in Modifiers)
                {

                    toReturn = toReturn + m.Abbreviation + "  ";
                }
            return toReturn;
        }






        /// <summary>
        /// The following need to be mapped & calculated (Binding in xaml already taken care of), see pg 13-15 of spec for queries that define the logic here
        /// </summary>

        [DependsOn("Adjustments")]
        public virtual Decimal AdjustmentsTotal {
            get
            {
                return GetAdjustmentsTotal();
            }
        }
        public virtual Decimal GetAdjustmentsTotal()
        {
            return Adjustments != null ? Adjustments
                                        .Where(a => a.FinancialTransactionType is AdjustmentTypeViewModel && ((AdjustmentTypeViewModel)a.FinancialTransactionType).IsCash)
                                        .Sum(a => a.Amount) 
                                        : 0;
        }
        [DataMember]
        public virtual Decimal BilledTotal {  get; set; }
        public virtual Decimal UnbilledTotal {
            get
            {
                return OpenBalance - BilledTotal;
            }
        }
    }
}
