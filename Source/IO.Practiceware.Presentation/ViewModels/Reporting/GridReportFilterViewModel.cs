using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Reporting
{
    /// <summary>
    /// Wraps <see cref="GridReportSettings.GridReportFilter"/> to provide binding capabilities
    /// </summary>
    public class GridReportFilterViewModel : IViewModel
    {
        [DependsOn("SourceFilter")]
        public virtual string Name
        {
            get { return SourceFilter.Name; }
            set { SourceFilter.Name = value; }
        }

        public virtual GridReportSettings.GridReportFilter SourceFilter { get; set; }
    }
}