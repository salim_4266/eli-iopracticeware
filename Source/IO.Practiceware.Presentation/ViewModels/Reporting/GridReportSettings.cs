using System.Collections.Generic;
using IO.Practiceware.Presentation.Views.Common.Behaviors;

namespace IO.Practiceware.Presentation.ViewModels.Reporting
{
    public class GridReportSettings
    {
        /// <summary>
        /// Custom filters created and saved by user
        /// </summary>
        public List<GridReportFilter> Filters { get; set; }

        public class GridReportFilter
        {
            /// <summary>
            /// Name of the filter
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Actual filter descriptors collection
            /// </summary>
            public List<FilterSetting> Descriptors { get; set; }
        }
    }
}