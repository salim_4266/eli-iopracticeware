﻿using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    [DataContract]
    public class CdaViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public int Id { get; set; }

        /// <summary>
        ///     The xml content of a Cda (Clinical Document Architecture) file.
        /// </summary>
        [DataMember]
        [DispatcherThread]
        public virtual string CdaXml { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsClinicalSummary { get; set; }
        
        [DataMember]
        [DispatcherThread]
        public virtual string ServiceLocation { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Resource { get; set; }
    }
}