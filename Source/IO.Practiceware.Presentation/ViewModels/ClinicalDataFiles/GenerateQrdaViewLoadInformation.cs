﻿using System.Collections.Generic;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    public class GenerateQrdaViewLoadInformation
    {
        public ICollection<NamedViewModel> Users { get; set; }

        public ICollection<NamedViewModel> Measures { get; set; }
    }
}