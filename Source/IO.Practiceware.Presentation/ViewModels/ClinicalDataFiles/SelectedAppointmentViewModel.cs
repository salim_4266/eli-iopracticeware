﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    /// <summary>
    /// A view model storing the information needed to display a selected appointment.
    /// </summary>
    [DataContract]
    public class SelectedAppointmentViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual DateTime AppointmentDate { get; set; }

        [DataMember]
        public virtual string ScheduledDoctor { get; set; }

        [DataMember]
        public virtual string ServiceLocation { get; set; }

        [DataMember]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        public virtual string PrimaryCarePhysician { get; set; }
    }
}
