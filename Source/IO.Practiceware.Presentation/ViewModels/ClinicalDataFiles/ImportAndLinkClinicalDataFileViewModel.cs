﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    /// <summary>
    /// A view model for a clinical data file being imported.
    /// </summary>
    [DataContract]
    public class ImportAndLinkClinicalDataFileViewModel : IViewModel
    {
        /// <summary>
        /// Gets or sets the patient id. This is the patient id that the imported file will be associated with.
        /// </summary>
        /// <value>The patient id.</value>
        [DataMember]
        public virtual int? PatientId { get; set; }

        /// <summary>
        /// Gets or sets the content of the file being imported.
        /// </summary>
        /// <value>The content.</value>
        [DataMember]
        public virtual string XmlContent { get; set; }

        /// <summary>
        /// Represents the <see cref="XmlContent"/> as human-readable html as a pdf document
        /// </summary>
        [DataMember]
        public virtual byte[] PdfDocument { get; set; }

        /// <summary>
        /// The type of Clinical Data File this file represents.
        /// </summary>
        [DataMember]
        public virtual ClinicalDataFileType ClinicalDataFileType { get; set; }

        /// <summary>
        /// The name of the file imported
        /// </summary>
        public virtual string FileName { get; set; }

        /// <summary>
        /// The full path of the file, including folder location, name, and extension
        /// </summary>
        public virtual string FullPath { get; set; }

        /// <summary>
        /// The last date the file being imported was modified, in Utc format
        /// </summary>
        public virtual DateTime LastModifiedDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the full name of the patient that has been selected for association with this file
        /// </summary>
        public virtual string PatientSelectedFullName { get; set; }    

        /// <summary>
        /// Gets the last modified datetime of the imported file, in local time. Depends on the <see cref="LastModifiedDateUtc"/>
        /// </summary>
        public virtual DateTime LastModifiedDateLocalTime
        {
            get { return LastModifiedDateUtc.ToLocalTime(); }
        }
    }
}
