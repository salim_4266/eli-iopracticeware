using System.Runtime.Serialization;
using System.Xml;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    [DataContract]
    public class CdaEditorViewModel : IViewModel
    {
        private string _originalXml;

        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string OriginalXml 
        { 
            get
            {
                return _originalXml;
            } 
            set
            {
                _originalXml = value;
                CdaXmlDocument = new XmlDocument();
                CdaXmlDocument.LoadXml(_originalXml);
            } 
        }

        public virtual XmlDocument CdaXmlDocument { get; protected set; }
    }
}