﻿using System;
using System.IO;
using IO.Practiceware.Presentation.Resources;
using IO.Practiceware.Storage;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    /// <summary>
    /// Helper class that transforms cda xml data files into human-readable html using transformation stylesheets based on the type of file.
    /// </summary>
    public static class CdaXmlToHtmlHelper
    {
        /// <summary>
        /// Takes a filePath (local computer path or http Url), reads the xml and assuming its a "Clinical Data" xml file,
        /// will perform a transformation on the xml, using the correct xsl stylesheet based on the ClinicalDataType of the file.
        /// </summary>
        /// <param name="cdaFilePath">A <see cref="Uri"/> which represents an absolute system file path or web url</param>
        /// <returns>A string of human-readable Html content</returns>
        public static string CdaXmlToHtmlString(Uri cdaFilePath)
        {
            // read the xml file
            var cdaXmlContent = FileManager.Instance.ReadContentsAsText(cdaFilePath.IsFile ? cdaFilePath.LocalPath : cdaFilePath.AbsoluteUri);

            return CdaXmlToHtmlString(cdaXmlContent);
        }

        /// <summary>
        /// Takes the xml and assuming its a "Clinical Data" type,
        /// will perform a transformation on the xml, using the correct xsl stylesheet based on the ClinicalDataType of the file.
        /// </summary>
        /// <param name="cdaXmlContent"></param>
        /// <returns></returns>
        public static string CdaXmlToHtmlString(string cdaXmlContent)
        {
            // check if its valid xml
            if (!XmlParserHelper.IsValidXml(cdaXmlContent)) return null;

            // we need to determine the type of clinical data file that is represented by the xml
            var fileType = DetermineClinicalDataFileType(cdaXmlContent);

            // get the correct stylesheet
            var styleSheet = GetStylesheetTransform(fileType);
        
            // perform transformation
            return XmlTransform.TransformXml(cdaXmlContent, styleSheet, true).ToString();
        }

        /// <summary>
        /// Determines what type of clinical data file the xml represents.  If type cannot be determined, will return a type of 'Unknown'
        /// </summary>
        /// <param name="xml">A string of xml content</param>
        /// <returns>A <see cref="ClinicalDataFileType"/></returns>
        public static ClinicalDataFileType DetermineClinicalDataFileType(string xml)
        {
            // check for C-cda file
            var isCdaXmlFile = XmlParserHelper.DoesElementExist(xml, CdaRootElementName);
            if (isCdaXmlFile)
            {
                var isConsolidatedContinuityOfCare = XmlParserHelper.DoesElementAndAttributeExist(xml, CdaTemplateIdElementName, CdaTemplateIdAttributeName, CcdaTemplateIdAttributeValue_ContinuityOfCareConstraints);
                if (isConsolidatedContinuityOfCare) return ClinicalDataFileType.Ccda;

                var isOriginalContinuityOfCare = XmlParserHelper.DoesElementAndAttributeExist(xml, CdaTemplateIdElementName, CdaTemplateIdAttributeName, CcdTemplateIdAttributeValue_ContinuityOfCareConstraints);
                if (isOriginalContinuityOfCare) return ClinicalDataFileType.Ccd;

                // not sure of the specific C-CDA type, so just return general type
                return ClinicalDataFileType.Ccda;
            }

            var isCcrXmlFile = XmlParserHelper.DoesElementExist(xml, CcrRootElementName);
            if (isCcrXmlFile) return ClinicalDataFileType.Ccr;

            // cannot determine the clinical data file type
            return ClinicalDataFileType.Unknown;
        }

        // Root Element Identifier for CDA files 
        public const string CdaRootElementName = "ClinicalDocument";

        // Identifiers for the new C-CDA file architecture
        public const string CdaTemplateIdElementName = "templateId";
        public const string CdaTemplateIdAttributeName = "root";

        // these are specific CDA template versions.  We should NOT ONLY rely on template versions for Document identification.  They may change in the future
        public const string CdaTemplateIdAttributeValue_GeneralHeaderConstraints = "2.16.840.1.113883.10.20.22.1.1";
        public const string CcdaTemplateIdAttributeValue_ContinuityOfCareConstraints = "2.16.840.1.113883.10.20.22.1.2";

        // this is the template id root of the original CCD (not the Consolidated one)
        public const string CcdTemplateIdAttributeValue_ContinuityOfCareConstraints = "2.16.840.1.113883.3.88.11.32.1";

        // Root Element Identifier for CCR files
        public const string CcrRootElementName = "ContinuityOfCareRecord";

        /// <summary>
        /// Returns the correct xml stylesheet based on the <see cref="ClinicalDataFileType"/>
        /// </summary>
        /// <param name="clinicalDataFileType"></param>
        /// <returns></returns>
        public static string GetStylesheetTransform(ClinicalDataFileType clinicalDataFileType)
        {
            switch (clinicalDataFileType)
            {
                case ClinicalDataFileType.Ccr:
                    return Xsls.Ccr;
                case ClinicalDataFileType.Ccd:
                case ClinicalDataFileType.Ccda:
                    return Xsls.Cda;
            }

            return string.Empty;
        }

    }
}
