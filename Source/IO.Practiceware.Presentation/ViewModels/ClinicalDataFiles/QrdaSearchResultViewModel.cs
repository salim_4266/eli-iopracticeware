﻿
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    [DataContract]
    public class QrdaSearchResultViewModel : NamedViewModel
    {
        [DataMember]    
        public bool IsInDenominatorExclusion { get; set; }
        
        [DataMember]
        public bool IsInNumerator { get; set; }
        
        [DataMember]
        public bool IsInDenominatorException{ get; set; }

        [DataMember]
        public bool NumeratorExclusions { get; set; }
    }
}
