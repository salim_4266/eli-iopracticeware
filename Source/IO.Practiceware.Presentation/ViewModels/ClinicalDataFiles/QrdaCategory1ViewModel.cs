using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    [DataContract]
    public class QrdaCategory1ViewModel : IViewModel
    {
        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual string QrdaXml { get; set; }
    }
}