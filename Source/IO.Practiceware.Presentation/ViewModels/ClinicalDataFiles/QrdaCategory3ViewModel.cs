﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    [DataContract]
    public class QrdaCategory3ViewModel : IViewModel
    {
        [DataMember]
        public virtual string QrdaXml { get; set; }

        [DataMember]
        public ICollection<QrdaCategory1ViewModel> PatientsSource { get; set; }
    }
}