﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles
{
    /// <summary>
    /// Represents a particular type of clinical data file.
    /// </summary>
    public enum ClinicalDataFileType
    {
        /// <summary>
        /// Describes the general over-arching term for clinical documents (e.g. Continuity of Care (Transition of Care, Visit Summary)).
        /// </summary>
        [Description("Consolidated-Clinical Document Architecture")]
        [Display(Name = "C-CDA")]
        Ccda = 1,

        /// <summary>
        /// The original term for a Continuity of Care document.  Note that a C-CDA document can be a Continuity of Care document.  The new
        /// C-CDA architecture incorporates the old CCD architecture (They're pretty much the same file)
        /// </summary>
        [Description("Continuity of Care Document (Legacy)")]
        [Display(Name = "CCD")]
        Ccd = 2,

        /// <summary>
        /// Older architecture for a Continuity of Care document.  Referred to as "Continuity of Care Record" - CCR.
        /// </summary>
        [Description("Continuity of Care Record")]
        [Display(Name = "CCR")]
        Ccr = 3,

        /// <summary>
        /// Unknown file type
        /// </summary>
        [Display(Name = "Unknown")]
        Unknown = 4,
    }
}
