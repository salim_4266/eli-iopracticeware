﻿using Soaf.Collections;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PrinterSetup
{
    [DataContract]
    public class DocumentPrinterViewModel : IViewModel
    {
        [DataMember]
        public virtual string DocumentTypeName { get; set; }

        [DataMember]
        public virtual string DocumentName { get; set; }

        [DataMember]
        public virtual string PrinterName { get; set; }
    }

    [DataContract]
    public class DocumentViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }
    }

    [DataContract]
    public class PrinterViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string UncPath { get; set; }

        [DataMember]
        public virtual string SiteName { get; set; }

        public virtual string DisplayName
        {
            get { return SiteName.IsNotNullOrEmpty() ? Name + " (" + SiteName + ")" : Name; }
        }
    }
}