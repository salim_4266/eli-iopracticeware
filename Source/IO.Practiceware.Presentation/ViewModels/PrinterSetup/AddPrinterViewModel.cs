﻿
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PrinterSetup
{
    [DataContract]
    public class AddPrinterViewModel : IViewModel
    {
        public AddPrinterViewModel(Printers.Printer printer)
        {
            Name = printer.ShareName + "  on  " + printer.Computer;
            Location = printer.Location;
            UncPath = printer.UncPath;
        }

        public AddPrinterViewModel()
        {
        }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

        [DataMember]
        public virtual string UncPath { get; set; }
    }
}