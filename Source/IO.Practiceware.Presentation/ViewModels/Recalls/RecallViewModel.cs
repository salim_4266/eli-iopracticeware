﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Recalls
{
    [DataContract]    
    public class RecallDisplayViewModel : IViewModel
    {
        [DataMember]
        public virtual int RecallId { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual RecallStatus RecallStatus { get; set; }

        [DataMember]
        public virtual string DueDate { get; set; }

        [DataMember]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual string Resource { get; set; }

        [DataMember]
        public virtual string Action { get; set; }

        [DataMember]
        public virtual string ActionDate { get; set; }
    }

    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class RecallDetailViewModel : IViewModel
    {
        [DataMember]
        public virtual int? RecallId { get; set; }

        [Required]
        [DataMember]
        public virtual int PatientId { get; set; }

        [DispatcherThread]
        [Required(ErrorMessage = "Please select a date for the recall")]
        [DataMember]
        public virtual DateTime? RecallDate { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a doctor resource")]
        public virtual int? ResourceId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int? LocationId { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select an appointment type")]
        public virtual int? AppointmentTypeId { get; set; }
    }

    [DataContract]
    public class RecallSelectListsViewModel : IViewModel
    {        
        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<AppointmentTypeViewModel> AppointmentTypes { get; set; }
     
        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel<int>> Locations { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel<int>> Resources { get; set; }
    }

    [DataContract]
    public class RecallPatientInfo : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int PatientId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string PatientName { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string HomePhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string BusinessPhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string CellPhone { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int? DefaultResourceId { get; set; }
    }
}
