﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Recalls
{
    [DataContract]
    public class AppointmentTypeViewModel : NamedViewModel
    {
        [DataMember]
        public int MaximumRecallsPerEncounter { get; set; }
        
        [DataMember]
        public int FrequencyBetweenRecallsPerEncounter { get; set; }
    }
}
