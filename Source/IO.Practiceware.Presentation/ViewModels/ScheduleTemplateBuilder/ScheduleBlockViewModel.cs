﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder
{
    /// <summary>
    /// A single selected schedule block entry
    /// </summary>
    [DataContract]
    [EditableObject]
    public class ScheduleBlockViewModel : IViewModel, IEquatable<ScheduleBlockViewModel>
    {
        public ScheduleBlockViewModel()
        {
            Categories = new ExtendedObservableCollection<CategoryViewModel>();
        }

        [DataMember]
        public virtual TimeSpan StartTime { get; set; }

        [DataMember]
        public virtual TimeSpan EndTime { get; set; }

        [DataMember]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual ObservableCollection<CategoryViewModel> Categories { get; set; }

        [DataMember]
        [IgnoreChangeTracking]
        public virtual bool IsSelected { get; set; }

        public bool IsUnavailable
        {
            get { return Categories.Select(sc => sc.Name).Any(n => n.Equals(UnavailableCategoryViewModel.UnavailableCategoryName, StringComparison.OrdinalIgnoreCase)); }
        }

        [DataMember]
        public virtual string Comment { get; set; }

        public bool Equals(ScheduleBlockViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return StartTime == other.StartTime
                   && EndTime == other.EndTime
                   && Location == other.Location
                   && Categories.SequenceEqual(other.Categories);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ScheduleBlockViewModel);
        }

        public override int GetHashCode()
        {
            return StartTime.GetHashCode() 
                ^ EndTime.GetHashCode()
                ^ Location.GetHashCode()
                ^ Objects.GetHashCodeWithNullCheck(Categories);
        }

        public static bool operator ==(ScheduleBlockViewModel lhs, ScheduleBlockViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(ScheduleBlockViewModel lhs, ScheduleBlockViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}
