﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder
{
    [EditableObject]
    [DataContract]
    public class ScheduleTemplateViewModel : NamedViewModel
    {
        public ScheduleTemplateViewModel()
        {
            ScheduleBlocks = new ExtendedObservableCollection<ScheduleBlockViewModel>();
        }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<ScheduleBlockViewModel> ScheduleBlocks { get; set; }

        [DispatcherThread]
        public virtual void BeginEdit()
        {
            this.CastTo<IEditableObject>().BeginEdit();
        }

        [DispatcherThread]
        public virtual void CancelEdit()
        {
            this.CastTo<IEditableObject>().CancelEdit();
        }

        [DispatcherThread]
        public virtual void EndEdit()
        {
            this.CastTo<IEditableObject>().EndEdit();
        }

        [DispatcherThread]
        public virtual void AcceptChanges()
        {
            this.CastTo<IChangeTracking>().AcceptChanges();
        }
    }
}
