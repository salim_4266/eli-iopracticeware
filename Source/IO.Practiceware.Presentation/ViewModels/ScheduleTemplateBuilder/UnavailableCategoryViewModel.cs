﻿using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder
{
    [DataContract]
    public class UnavailableCategoryViewModel : CategoryViewModel
    {
        public static int UnavailableCategoryId = 0;
        public static string UnavailableCategoryName = "Unavailable";

        public UnavailableCategoryViewModel()
        {
            Id = UnavailableCategoryId;
            Name = UnavailableCategoryName;
            Color = Colors.Gray;
        }
    }
}