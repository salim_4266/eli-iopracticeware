﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder
{
    /// <summary>
    /// A category from IO DB
    /// </summary>
    [DataContract]
    [EditableObject]
    [KnownType(typeof(UnavailableCategoryViewModel))]
    public class CategoryViewModel : IViewModel, IEquatable<CategoryViewModel>
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        public virtual Color Color { get; set; }

        [DataMember]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        public bool Equals(CategoryViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id
                && Color == other.Color
                && Name == other.Name;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CategoryViewModel);
        }

        public override int GetHashCode()
        {
            return Objects.GetHashCodeWithNullCheck(Id)
                ^ Objects.GetHashCodeWithNullCheck(Name)
                ^ Color.GetHashCode();
        }

        public static bool operator ==(CategoryViewModel lhs, CategoryViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(CategoryViewModel lhs, CategoryViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}
