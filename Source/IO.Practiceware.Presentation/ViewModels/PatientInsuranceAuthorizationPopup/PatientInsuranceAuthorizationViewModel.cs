﻿using System;
using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class PatientInsuranceAuthorizationViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Authorization Code is required")]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Patient Insurance is required")]
        public virtual int PatientInsuranceId { get; set; }

        [DataMember]
        public virtual int? EncounterId { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual int? InvoiceId { get; set; }

    }
}
