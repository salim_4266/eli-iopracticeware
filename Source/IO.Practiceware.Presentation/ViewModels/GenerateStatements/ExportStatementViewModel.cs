﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    [DataContract]
    public class ExportStatementViewModel : IViewModel
    {
        [DataMember]
        public virtual PatientViewModel Patient { get; set; }

        [DataMember, Required]
        public virtual int EncounterId { get; set; }

        [DataMember]
        public virtual DateTime InvoiceDate { get; set; }

        [DataMember]
        public virtual string InvoiceNote { get; set; }

        [DataMember]
        public virtual DateTime PatientBilledDate { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<DiagnosisViewModel> Diagnoses { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<BillingServiceViewModel> BillingServices { get; set; }

        [DataMember, Dependency]
        public virtual AdjustmentInformationViewModel AdjustmentInformation { get; set; }

        [DependsOn("Patient", "EncounterId")]
        public virtual string FormattedInvoice
        {
            get
            {
                return "{0}-{1}".FormatWith(Patient.Id, EncounterId);
            }
        }
    }

    [DataContract]
    public class AdjustmentInformationViewModel : IViewModel
    {
        [DataMember]
        public virtual decimal InsuranceAmountPending { get; set; }

        [DataMember]
        public virtual decimal PatientBalance { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<AgingIntervalViewModel> AgingIntervals { get; set; }
    }

    [DataContract]
    public class AgingIntervalViewModel : IViewModel
    {
        [DataMember]
        public virtual Enum Interval { get; set; }

        [DataMember]
        public virtual decimal Amount { get; set; }
    }

    public enum Interval
    {
        ZeroToThirtyDays = 1,
        ThirtyToSixtyDays = 2,
        SixtyToNinetyDays = 3,
        NinetyToOneHundredTwentyDays = 4,
        GreaterThanOneHundredTwentyDays = 5
    }

    [DataContract]
    public class BillingServiceViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DataMember]
        public virtual decimal UnitCharge { get; set; }

        [DataMember]
        public virtual decimal UnitAmount { get; set; }

        [DataMember]
        public virtual int OrdinalId { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<DiagnosisViewModel> Diagnoses { get; set; }

        [DataMember]
        public virtual BillingServiceAdjustmentInformationViewModel BillingServiceAdjustmentInformation { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal OpenBalance { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<string> ServiceNotes { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<string> Modifiers { get; set; }

        [DataMember, DispatcherThread]
        public virtual DateTime BillingServiceDatetime { get; set; }

    }

    [DataContract]
    public class BillingServiceAdjustmentInformationViewModel : IViewModel
    {
        [DataMember]
        public virtual decimal TotalAdjustments { get; set; }

        [DataMember]
        public virtual decimal PatientPayments { get; set; }

        [DataMember]
        public virtual decimal InsurerPayments { get; set; }

        [DataMember]
        public virtual decimal PrimaryInsurerPayments { get; set; }

        [DataMember]
        public virtual decimal OtherInsurerPayments { get; set; }

        [DependsOn("PatientPayments", "InsurerPayments")]
        public virtual decimal TotalPayments
        {
            get
            {
                return PatientPayments + InsurerPayments;
            }
        }
    }

    [DataContract]
    public class InvoiceAdjustmentInformationViewModel : IViewModel
    {
        [DataMember]
        public virtual int InvoiceId { get; set; }

        [DataMember]
        public virtual decimal TotalAdjustments { get; set; }

        [DataMember]
        public virtual DateTime InvoiceDate { get; set; }
    }

    [DataContract]
    public class DiagnosisViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual int? OrdinalId { get; set; }
    }

    [DataContract]
    public class PatientViewModel : IViewModel
    {
        [Required, DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string Suffix { get; set; }

        [DataMember]
        public virtual AddressViewModel PatientAddress { get; set; }

        [DataMember]
        public virtual PhoneNumberViewModel PhoneNumber { get; set; }

        [DataMember]
        public virtual string PriorPatientCode { get; set; }

        [DataMember]
        public virtual string CanSendCollectionLetter { get; set; }

        [DataMember]
        public virtual string PatientFinancialNote { get; set; }
    }


}