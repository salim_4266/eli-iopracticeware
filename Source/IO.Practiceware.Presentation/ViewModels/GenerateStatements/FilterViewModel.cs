﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class FilterViewModel : IViewModel
    {
        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> RenderingProviders { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> TransactionStatuses { get; set; }

        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> StatmentCommunicationPreferences { get; set; }

        //The minimum BST.AmountSent to filter by
        [DataMember]
        [Expression("MinimumAmount >= 0", ErrorMessage = "The minimum amount must be $0.00 or greater.")]
        public virtual decimal MinimumAmount { get; set; }

        //The maximum number of days since the patient was last billed to filter by
        [DataMember]
        [Expression("!BillingCycleDays.HasValue || BillingCycleDays >= 0", ErrorMessage = "Enter a number of days.")]
        public virtual int? BillingCycleDays { get; set; }
        
        //The starting string for the patient last name filter (i.e., the A in include patients with last names from A - Mc)
        [DataMember]
        public virtual string PatientLastNameFrom { get; set; }

        //The ending string for the patient last name filter (i.e., the Mc in include patients with last names from A - Mc)
        [DataMember]
        public virtual string PatientLastNameTo { get; set; }
    }
}
