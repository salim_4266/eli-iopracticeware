﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    [DataContract]
    public class GenerateStatementsFilter
    {
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public List<int> RenderingProvideIds { get; set; }

        [DataMember]
        public List<int> ServiceLocationIds { get; set; }

        [DataMember]
        public List<int> BillingOrganizationIds { get; set; }

        [DataMember]
        public List<int> TransactionStatusIds { get; set; }

        [DataMember]
        public List<int> StatmentCommunicationPreferenceIds { get; set; }

        [DataMember]
        public decimal MinimumAmount { get; set; }

        [DataMember]
        public int? BillingCycleDays { get; set; }

        [DataMember]
        public string PatientLastNameFrom { get; set; }

        [DataMember]
        public string PatientLastNameTo { get; set; }
    }
}
