﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    [DataContract]
    public class GenerateStatementViewModel : IViewModel
    {
        [DataMember]
        public virtual NamedViewModel Patient { get; set; }

        [DependsOn("BillingServiceTransactions", "BillingServices")]
        public virtual DateTime DateTime
        {
            get
            {
                if (OpenBalanceType == OpenBalanceType.Invoice) return InvoiceAdjustments.Min(i => i.InvoiceDate);
                if (OpenBalanceType == OpenBalanceType.Patient) return BillingServiceTransactions.Min(t => t.DateTime);
                return BillingServices.Min(t => t.BillingServiceDatetime);
            }
        }

        [DependsOn("BillingServiceTransactions", "BillingServices")]
        public virtual Decimal Amount
        {
            get
            {
                if (OpenBalanceType == OpenBalanceType.Invoice) return BillingServices.Sum(i => i.OpenBalance);
                if (OpenBalanceType == OpenBalanceType.Patient) return BillingServiceTransactions.Sum(t => t.Amount);
                return BillingServices.Min(t => t.OpenBalance);
            }
        }

        [DataMember]
        public virtual ObservableCollection<BillingServiceTransactionViewModel> BillingServiceTransactions { get; set; }

        [DataMember]
        public virtual ObservableCollection<BillingServiceViewModel> BillingServices { get; set; }

        [DataMember]
        public virtual ObservableCollection<InvoiceAdjustmentInformationViewModel> InvoiceAdjustments { get; set; }

        [DataMember]
        public virtual OpenBalanceType OpenBalanceType { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal Total_Patient { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal Total_Paid { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal OnAccountBalance { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal NegativeAdjusments  { get; set; }

        [DataMember, DispatcherThread]
        public virtual decimal Total_Charges { get; set; }


        [DataMember, DispatcherThread]
        public virtual decimal BillingTranscationAmount { get; set; }
    }

    [DataContract]
    public class BillingServiceTransactionViewModel : IViewModel
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual NamedViewModel BillingServiceTransactionStatus { get; set; }

        [DataMember]
        public virtual DateTime DateTime { get; set; }

        [DataMember]
        public virtual decimal Amount { get; set; }
    }
}
