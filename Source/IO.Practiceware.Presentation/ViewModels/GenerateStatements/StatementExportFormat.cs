﻿namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    public enum StatementExportFormat
    {
        InsurancePaymentsAggregate = 1,
        InsurancePaymentsBrokenOut = 2,
        InsurancePaymentsBrokenOutAndAging = 3
    }
}