﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.GenerateStatements
{
    [DataContract]
    public class DataListsViewModel
    {
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> TransactionStatuses { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> RenderingProviders { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> StatmentCommunicationPreferences { get; set; }
    }
}
