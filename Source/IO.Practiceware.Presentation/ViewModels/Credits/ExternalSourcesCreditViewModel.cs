﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Credits
{

        public class ExternalSourcesCreditViewModel : IViewModel
        {
            [DataMember]
            public virtual String Version { get; set; }

            [DataMember]
            public virtual String Text { get; set; }

            [DataMember]
            public virtual ObservableCollection<ExternalSourceNameAndDescriptionViewModel> Sources { get; set; }
    }
}
