﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Credits
{
    public class ExternalSourceNameAndDescriptionViewModel : IViewModel
    {
        [DataMember]
        public virtual String Name { get; set; }

        [DataMember]
        public virtual String Text { get; set; }

    }
}
