﻿using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;


namespace IO.Practiceware.Presentation.ViewModels.Notes
{
    [DataContract]
    public class NoteLoadInformation
    {
        /// <summary>
        /// Standard note templates.
        /// </summary>
        [DataMember]
        [Dependency]
        public ExtendedObservableCollection<NameAndAbbreviationAndDetailViewModel> StandardNotes { get; set; }

        /// <summary>
        /// Available alterable screens
        /// </summary>
        [DataMember]
        [Dependency]
        public ExtendedObservableCollection<NamedViewModel> AlertableScreens { get; set; }

        /// <summary>
        /// This note is for the top most part of Manage note user control.
        /// Used for adding standard template and adding to notes collection
        /// </summary>
        [DataMember]
        public NoteViewModel Note { get; set; }

        /// <summary>
        /// Contains the patient financial comments for the specific patient.
        /// </summary>
        [DataMember]
        [Dependency]
        public ExtendedObservableCollection<NoteViewModel> Notes { get; set; }
    }

    [DataContract]
    public class NoteLoadArguments
    {
        /// <summary>
        /// Type of Note for which the note screen will get launched.
        /// </summary>
        [DataMember]
        public NoteType? NoteType { get; set; }

        /// <summary>
        /// Note id for which the note screen will get launched.
        /// </summary>
        [DataMember]
        public int? NoteId { get; set; }

        /// <summary>
        /// Entity id associated with the note.
        /// </summary>
        [DataMember]
        public int? EntityId { get; set; }

        /// <summary>
        /// NoteType Id passed from vb6
        /// </summary>
        public int NoteTypeId
        {
            set { NoteType = (NoteType)value; }
        }
        /// <summary>
        /// Screen id to launch the screen for.
        /// </summary>
        [DataMember]
        public AlertScreen? AlertScreen { get; set; }
    }
}