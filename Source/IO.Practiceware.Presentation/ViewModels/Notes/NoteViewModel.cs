﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.Notes
{
    [DataContract]
    [EditableObject]
    public class NoteViewModel : IViewModel
    {
        /// <summary>
        /// To check  the expiration date time property changed or not.
        /// </summary>
        public static string ExpirationDatePropertyName = Reflector.GetMember<NoteViewModel>(x => x.ExpirationDateTime).Name;

        /// <summary>
        /// Note name in case of template note.
        /// </summary>
        [DataMember]
        public virtual String Name { get; set; }

        /// <summary>
        /// Note id.
        /// </summary>
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// Note value.
        /// </summary>
        [DataMember]
        public virtual string Value { get; set; }

        /// <summary>
        /// Screen associate with note alert.
        /// </summary>
        [DataMember, Dependency]
        public virtual ObservableCollection<NamedViewModel> AlertableScreens { get; set; }

        /// <summary>
        /// Note created by.
        /// </summary>
        [DataMember]
        public virtual NamedViewModel CreatedByUser { get; set; }

        /// <summary>
        /// Note Created on.
        /// </summary>
        [DataMember]
        public virtual DateTime? CreatedOnDateTime { get; set; }

        /// <summary>
        /// Whether alert is active.
        /// </summary>
        [DataMember]
        public virtual bool IsAlertActive { get; set; }

        /// <summary>
        /// Expiration Date of the alert
        /// </summary>
        [DataMember]
        public virtual DateTime? ExpirationDateTime { get; set; }

        /// <summary>
        /// If the note is archived.
        /// </summary>
        [DataMember]
        public virtual bool IsArchived { get; set; }

        //IsIncludedOnStatement is only used for BillingServiceComments and InvoiceComments...  If it's null, we hide the checkbox on the UI
        [DataMember]
        public virtual bool IsIncludedOnStatement { get; set; }

        /// <summary>
        /// Screen type of which the note is associated with.
        /// </summary>
        [DataMember]
        public virtual NoteType? NoteType { get; set; }

        /// <summary>
        /// Alert id of the current note.
        /// </summary>
        [DataMember]
        public virtual int? AlertId { get; set; }

    }

    [DataContract]
    public enum NoteType
    {
        [EnumMember]
        PatientFinancialComment = 1,
        [EnumMember]
        InvoiceComment,
        [EnumMember]
        BillingServiceComment
    }

    [DataContract]
    public enum AlertScreen
    {
        [EnumMember]
        PatientFinancials = 8,
        [EnumMember]
        EditAndBill = 9,
        [EnumMember]
        PostAdjustments = 10,
    }
}
