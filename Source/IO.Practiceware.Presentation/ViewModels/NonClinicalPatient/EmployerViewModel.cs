﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.NonClinicalPatient
{
    [DataContract]
    [SupportsDataErrorInfo]
    public class EmployerViewModel : IViewModel
    {
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual AddressViewModel Address { get; set; }
    }
}
