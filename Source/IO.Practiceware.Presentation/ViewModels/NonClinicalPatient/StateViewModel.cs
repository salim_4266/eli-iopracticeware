﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.NonClinicalPatient
{
    [DataContract]
    public class StateViewModel : IViewModel
    {
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Abbreviation { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        public override bool Equals(object obj)
        {
            return Soaf.Objects.DataMembersEqual(this, obj);
        }

        public override int GetHashCode()
        {
            return Soaf.Objects.GetDataMembersHashCode(this);
        }
    }
}
