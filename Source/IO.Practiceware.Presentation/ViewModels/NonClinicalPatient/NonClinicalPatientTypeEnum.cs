﻿using System.ComponentModel.DataAnnotations;

namespace IO.Practiceware.Presentation.ViewsModels.NonClinicalPatient
{    
    /// <summary>
    /// Denotes the type of relationship a patient can have with another type of patient.  There are 3 "logical" types of patients
    /// within our application: 
    /// 1) "Patient" (which is considered a ClinicalPatient) 
    /// 2) "NonClinicalPatient" (which has the IsClinical flag set to false and requires only first and last name)
    /// 3) "Policyholder" (which is technically a NonClinicalPatient model wise)
    /// 
    /// Numbers 1 & 2 are both considered "Contact" relationship.
    /// Number 3 is a Policyholder relationship.
    /// </summary>
    public enum PatientRelationshipType
    {
        [Display(Name = "Contact")]
        Contact = 1,

        [Display(Name = "Policyholder")]
        Policyholder = 2,
    }
}
