﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.ViewModels.NonClinicalPatient
{
    [SupportsDataErrorInfo]
    [DataContract]
    public class NonClinicalPatientViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string Suffix { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual AddressViewModel Address { get; set; }

        [DataMember]
        [RequiredIfIsRequired]
        public virtual DateTime? DateOfBirth { get; set; }

        [RegexMatch(Strings.SocialSecurityNumberRegex, SuccessOnNullOrEmptyInput = true, ErrorMessage = "Social Security Number is not in the correct format.")]
        [Expression("HasSocialSecurityConflict == false", ErrorMessage = "Social Security Number already exists.")]
        [DataMember]
        [RequiredIfIsRequired]
        public virtual string SocialSecurity { get; set; }

        [DispatcherThread]
        public virtual bool HasSocialSecurityConflict { get; set; }

        [DataMember]
        public virtual bool IsSocialSecurityRequired { get; set; }

        [DataMember]
        public virtual bool IsDateOfBirthRequired { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual PhoneNumberViewModel PhoneNumber { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual EmailAddressViewModel EmailAddress { get; set; }

        [DataMember]
        public virtual string Occupation { get; set; }

        [DataMember]
        public virtual NamedViewModel EmploymentStatus { get; set; }

        [DataMember]
        [ValidateObject]
        public virtual EmployerViewModel Employer { get; set; }

        [DataMember]
        public virtual NamedViewModel SelectedGender { get; set; }
    }
}
