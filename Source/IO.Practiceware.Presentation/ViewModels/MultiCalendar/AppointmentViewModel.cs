﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    [EditableObject]
    public class AppointmentViewModel : IViewModel, IEquatable<AppointmentViewModel>
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime EndDateTime { get; set; }

        [DataMember]
        public virtual int? AppointmentCategoryId { get; set; }

        [DataMember]
        public virtual ColoredViewModel AppointmentType { get; set; }

        [DataMember]
        public virtual int EncounterStatusId { get; set; }

        [DataMember]
        public virtual string EncounterStatusName { get; set; }

        public virtual Color EncounterStatusColor { get; set; }

        [DataMember]
        public virtual string EncounterStatusColorString
        {
            get { return new ColorConverter().ConvertToString(EncounterStatusColor); }
            set { EncounterStatusColor = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        [DependsOn("EncounterStatusColor")]
        public virtual Brush EncounterStatusBrush
        {
            get
            {
                var brush = new SolidColorBrush(EncounterStatusColor);  
                return brush;
            }
        }


        [DataMember]
        public virtual InsuranceType InsuranceTypeId { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ResourceViewModel Resource { get; set; }

        [DataMember]
        public virtual PatientViewModel Patient { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual bool IsCancelled { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsOrphaned { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsReferralRequired { get; set; }

        public virtual bool IsArrived
        {
            get { return (EncounterStatusId >= (int) EncounterStatus.Questions && EncounterStatusId <= (int) EncounterStatus.Checkout); }
        }

        public virtual bool IsDischarged
        {
            get { return EncounterStatusId == (int) EncounterStatus.Discharged; }
        }

        public bool Equals(AppointmentViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id &&
                   StartDateTime == other.StartDateTime &&
                   EndDateTime == other.EndDateTime &&
                   AppointmentCategoryId == other.AppointmentCategoryId &&
                   AppointmentType.Id == other.AppointmentType.Id &&
                   EncounterStatusId == other.EncounterStatusId &&
                   InsuranceTypeId == other.InsuranceTypeId &&
                   Comments == other.Comments &&
                   Resource.Id == other.Resource.Id &&
                   Patient.Id == other.Patient.Id &&
                   Location.Id == other.Location.Id &&
                   IsCancelled == other.IsCancelled &&
                   IsOrphaned == other.IsOrphaned &&
                   IsReferralRequired == other.IsReferralRequired;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AppointmentViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(AppointmentViewModel lhs, AppointmentViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(AppointmentViewModel lhs, AppointmentViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}