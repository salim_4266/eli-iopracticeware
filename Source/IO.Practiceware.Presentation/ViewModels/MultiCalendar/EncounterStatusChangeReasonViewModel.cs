﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class EncounterStatusChangeReasonViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Value { get; set; }

        [DataMember]
        public virtual int EncounterStatusId { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}
