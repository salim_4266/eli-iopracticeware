﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class UnavailableCategoryViewModel : ColoredViewModel
    {
        public static int UnavailableCategoryId = 0;
        public static string UnavailableCategoryName = "Unavailable";

        public UnavailableCategoryViewModel()
        {
            Id = UnavailableCategoryId;
            Name = UnavailableCategoryName;
            Color = Colors.DarkSlateGray;
        }
    }
}
