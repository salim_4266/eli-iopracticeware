﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class LocationHoursViewModel : IViewModel, IEquatable<LocationHoursViewModel>
    {
        [DataMember]
        public virtual int LocationId { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual DateTime StartTime { get; set; }

        [DataMember]
        public virtual DateTime EndTime { get; set; }

        public bool Equals(LocationHoursViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return LocationId == other.LocationId
                   && StartTime == other.StartTime
                   && EndTime == other.EndTime;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as LocationHoursViewModel);
        }

        public override int GetHashCode()
        {
            return LocationId.GetHashCode() ^ StartTime.GetHashCode() ^ EndTime.GetHashCode();
        }

        public static bool operator ==(LocationHoursViewModel lhs, LocationHoursViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(LocationHoursViewModel lhs, LocationHoursViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}
