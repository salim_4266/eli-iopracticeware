﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class PatientViewModel : IViewModel, IEquatable<PatientViewModel>
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DispatcherThread]
        public virtual byte[] Photo { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool HasLoadedPhoto { get; set; }

        [DataMember]
        public virtual string InsurerName { get; set; }

        [DataMember]
        public virtual string HomePhone { get; set; }

        [DataMember]
        public virtual string BusinessPhone { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }

        [DataMember]
        public virtual DateTime? BirthDate { get; set; }

        [DataMember]
        public virtual PatientStatus PatientStatus { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual PostOperativeViewModel PatientPostOp { get; set; }

        [DispatcherThread]
        [DependsOn("PatientPostOp")]
        public virtual bool ShowPostOp { get { return PatientPostOp != null && PatientPostOp.CurrentlyInPostOperativePeriod; } }

        public bool Equals(PatientViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as PatientViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(PatientViewModel lhs, PatientViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(PatientViewModel lhs, PatientViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}