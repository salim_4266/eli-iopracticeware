﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class CalendarCommentViewModel : IViewModel, IEquatable<CalendarCommentViewModel>
    {
        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual string Value { get; set; }

        [DataMember]
        public virtual int? UserId { get; set; }

        public bool Equals(CalendarCommentViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Date == other.Date && Value == other.Value && UserId == other.UserId;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CalendarCommentViewModel);
        }

        public override int GetHashCode()
        {
            return Date.GetHashCode() ^ Objects.GetHashCodeWithNullCheck(Value) ^ Objects.GetHashCodeWithNullCheck(UserId);
        }

        public static bool operator ==(CalendarCommentViewModel lhs, CalendarCommentViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(CalendarCommentViewModel lhs, CalendarCommentViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}