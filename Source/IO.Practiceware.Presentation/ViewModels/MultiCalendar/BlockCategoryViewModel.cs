﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    [EditableObject]
    [KnownType(typeof(UnavailableBlockCategoryViewModel))]
    public class BlockCategoryViewModel : IViewModel, IEquatable<BlockCategoryViewModel>
    {
        [DataMember]
        [DispatcherThread]
        public virtual Guid Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int? BlockId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ColoredViewModel Category { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual AppointmentViewModel Appointment { get; set; }

        public bool Equals(BlockCategoryViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id
                   && BlockId == other.BlockId
                   && Category == other.Category
                   && Appointment == other.Appointment;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as BlockCategoryViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode()
                   ^ BlockId.GetHashCode()
                   ^ Objects.GetHashCodeWithNullCheck(Category)
                   ^ Objects.GetHashCodeWithNullCheck(Appointment);
        }

        public static bool operator ==(BlockCategoryViewModel lhs, BlockCategoryViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(BlockCategoryViewModel lhs, BlockCategoryViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}