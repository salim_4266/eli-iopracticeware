﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    [EditableObject]
    [KnownType(typeof(ExtendedObservableCollection<BlockCategoryViewModel>))]
    public class BlockViewModel : IViewModel, IEquatable<BlockViewModel>
    {
        /// <summary>
        /// Gets or sets the Block's Id.  It is null when the instance represents a block placeholder.
        /// A block placeholder is represented in the UI as a "NoBlockAppointment".
        /// </summary>
        [DataMember]
        [DispatcherThread]
        public virtual int? Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime EndDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual ResourceViewModel Resource { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<BlockCategoryViewModel> Categories { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Comment { get; set; }

        public bool Equals(BlockViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id
                   && StartDateTime == other.StartDateTime
                   && EndDateTime == other.EndDateTime
                   && Location == other.Location
                   && Resource == other.Resource
                   && Categories.OrderBy(i => i.Id).SequenceEqual(other.Categories.OrderBy(i => i.Id));
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as BlockViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode()
                   ^ StartDateTime.GetHashCode()
                   ^ EndDateTime.GetHashCode()
                   ^ Objects.GetHashCodeWithNullCheck(Location)
                   ^ Objects.GetHashCodeWithNullCheck(Resource)
                   ^ Objects.GetHashCodeWithNullCheck(Categories);
        }

        public static bool operator ==(BlockViewModel lhs, BlockViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(BlockViewModel lhs, BlockViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }

    public static class BlockViewModelExtensions
    {
        public static int GetCategoriesUsed(this IEnumerable<BlockViewModel> blocks)
        {
            return blocks.SelectMany(b => b.Categories).Where(c => !(c is UnavailableBlockCategoryViewModel)).Count(c => c.Appointment != null);
        }

        public static int GetTotalCategories(this IEnumerable<BlockViewModel> blocks)
        {
            return blocks.SelectMany(b => b.Categories).Count(c => !(c is UnavailableBlockCategoryViewModel));
        }

        public static ObservableCollection<LocationHoursViewModel> GetLocationHours(this IEnumerable<BlockViewModel> blocks)
        {
            var result = new ObservableCollection<LocationHoursViewModel>();

            blocks = blocks.Where(b => b.Id.HasValue).OrderBy(b => b.StartDateTime);
            if (!blocks.Any()) return result;

            var firstBlock = blocks.First();
            var locationHours = new LocationHoursViewModel
                {
                    StartTime = firstBlock.StartDateTime,
                    EndTime = firstBlock.EndDateTime,
                    LocationId = firstBlock.Location.Id,
                    Name = firstBlock.Location.Name
                };

            foreach (var block in blocks.Skip(1))
            {
                if (locationHours.EndTime == block.StartDateTime && locationHours.LocationId == block.Location.Id)
                {
                    locationHours.EndTime = block.EndDateTime;
                    continue;
                }

                result.Add(locationHours);
                locationHours = new LocationHoursViewModel
                    {
                        StartTime = block.StartDateTime,
                        EndTime = block.EndDateTime,
                        LocationId = block.Location.Id,
                        Name = block.Location.Name
                    };
            }
            result.Add(locationHours);

            return result;
        }
    }
}
