﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.MultiCalendar
{
    [DataContract]
    public class UnavailableBlockCategoryViewModel : BlockCategoryViewModel
    {
        public static Guid UnavailableBlockCategoryId = Guid.Empty;
        public static string UnavailableBlockCategoryName = "Unavailable";

        public UnavailableBlockCategoryViewModel()
        {
            Id = UnavailableBlockCategoryId;
            Category = new ColoredViewModel
                           {
                               Name = UnavailableBlockCategoryName,
                               Color = Colors.DarkSlateGray
                           };
        }

        // NOTE:
        // Because of the force permissions, an appointment can be created on to an unavaiable
        // That is why UnavailableBlockCategoryViewModel inherits from BlockCategoryViewModel
    }
}