﻿using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientImageImporter
{
    /// <summary>
    ///   Information about an import job.
    /// </summary>
    [DataContract]
    public class PatientImageImporterJobViewModel : IViewModel
    {
        public PatientImageImporterJobViewModel()
        {
            Errors = new ObservableCollection<string>();
        }

        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual PatientImageImporterJobStatus Status { get; set; }

        [DataMember]
        public virtual ObservableCollection<string> Errors { get; set; }

        [DataMember]
        public virtual int ImportedFileCount { get; set; }

        [DataMember]
        public virtual int IgnoredFileCount { get; set; }

        [DataMember]
        public virtual int FailedFileCount { get; set; }

        [DataMember]
        public virtual DateTime Started { get; set; }
    }

    [DataContract]
    public enum PatientImageImporterJobStatus
    {
        [EnumMember] Running,
        [EnumMember] [Display(Name = "Cancellation Pending")] CancellationPending,
        [EnumMember] Cancelled,
        [EnumMember] Completed,
        [EnumMember] Error
    }
}