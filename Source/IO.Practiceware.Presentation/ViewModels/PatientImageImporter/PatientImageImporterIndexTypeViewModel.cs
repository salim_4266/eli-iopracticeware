﻿using Soaf;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Presentation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace IO.Practiceware.Presentation.ViewModels.PatientImageImporter
{
    /// <summary>
    ///   Type of index to use when running the importer.
    /// </summary>
    [DataContract]
    [EditableObject, SupportsDataErrorInfo]
    public abstract class PatientImageImporterIndexTypeViewModel : IViewModel
    {
        [Display(AutoGenerateField = false)]
        public abstract PatientImageImporterIndexType IndexType { get; }

        [Display(AutoGenerateField = false)]
        public virtual string Name
        {
            get { return IndexType.GetDisplayName(); }
        }
    }

    [DataContract]   
    public class FlatFilePatientImageImporterIndexTypeViewModel : PatientImageImporterIndexTypeViewModel
    {
        private int? _dateElement;
        private string _delimiter;
        private int? _fileTypeElement;
        private int? _pathElement;
        private int? _patientIdElement;

        public FlatFilePatientImageImporterIndexTypeViewModel()
        {
            _patientIdElement = 0;
            _fileTypeElement = 1;
            _dateElement = 2;
            _pathElement = 3;
            _delimiter = "|";
            UpdateRegularExpression();
        }

        [Display(Order = 0, Name = "Index File Path")]
        [Required]
        [FileExists]
        [DataMember]
        public virtual string IndexFilePath { get; set; }

        [Display(Order = 1)]
        [DataMember]
        public virtual string Delimiter
        {
            get { return _delimiter; }
            set
            {
                _delimiter = value;
                UpdateRegularExpression();
            }
        }

        [Display(Order = 2, Name = "Patient Id Element")]
        [DataMember]
        public virtual int? PatientIdElement
        {
            get { return _patientIdElement; }
            set
            {
                _patientIdElement = value;
                UpdateRegularExpression();
            }
        }

        [Display(Order = 3, Name = "File Type Element")]
        [DataMember]
        public virtual int? FileTypeElement
        {
            get { return _fileTypeElement; }
            set
            {
                _fileTypeElement = value;
                UpdateRegularExpression();
            }
        }

        [Display(Order = 4, Name = "Date Element")]
        [DataMember]
        public virtual int? DateElement
        {
            get { return _dateElement; }
            set
            {
                _dateElement = value;
                UpdateRegularExpression();
            }
        }

        [Display(Order = 5, Name = "Path Element")]
        [DataMember]
        public virtual int? PathElement
        {
            get { return _pathElement; }
            set
            {
                _pathElement = value;
                UpdateRegularExpression();
            }
        }

        private bool _useQuotedElements;

        [Display(Order = 6, Name = "Use Quoted Elements")]
        [DataMember]
        public virtual bool UseQuotedElements
        {
            get { return _useQuotedElements; }
            set
            {
                _useQuotedElements = value;
                UpdateRegularExpression();
            }
        }

        [Display(Order = 7, Name = "Date Format")]
        [DataMember]
        public virtual string DateFormat { get; set; }

        [Display(Order = 8, Name = "Regular Expression")]
        [Required]
        [ValidFlatFilePatientImageImporterIndexTypeRgularExpression]
        [DataMember(Order = 100)] // set last to prevent other properties from resetting
        public virtual string RegularExpression { get; set; }

        [Display(AutoGenerateField = false)]
        public override PatientImageImporterIndexType IndexType
        {
            get { return PatientImageImporterIndexType.FlatFile; }
        }

        private void UpdateRegularExpression()
        {
            if (PatientIdElement == null || FileTypeElement == null || DateElement == null || PathElement == null || Delimiter.IsNullOrEmpty()) return;

            IOrderedEnumerable<KeyValuePair<string, int>> elements = new Dictionary<string, int>
                                                                         {
                                                                             {"PatientId", PatientIdElement.Value},
                                                                             {"FileType", FileTypeElement.Value},
                                                                             {"Date", DateElement.Value},
                                                                             {"Path", PathElement.Value}
                                                                         }.OrderBy(i => i.Value);

            var regex = new StringBuilder("^");

            int index = 0;

            foreach (KeyValuePair<string, int> element in elements)
            {
                regex.Append("(.*?{0}){{{1}}}{3}(?<{2}>.*?){3}({0}|$)".FormatWith(Regex.Escape(Delimiter), element.Value - index, element.Key, UseQuotedElements ? "\"" : string.Empty));
                index += (element.Value - index) + 1;
            }

            RegularExpression = regex.ToString();
        }
    }

    /// <summary>
    ///   Different supported index types.
    /// </summary>
    [DataContract]
    public enum PatientImageImporterIndexType
    {
        [EnumMember] [Display(Name = "Flat File")] FlatFile
    }

    public class ValidFlatFilePatientImageImporterIndexTypeRgularExpressionAttribute : ValidRegularExpressionAttribute
    {
        public override bool IsValid(object value)
        {
            var result = base.IsValid(value);

            if (!result) return false;

            if (value == null || value.ToString().IsNullOrEmpty()) return true;

            var regex = new Regex(value.ToString());
            if (!VerifyGroupPresent(regex, "PatientId")) return false;
            if (!VerifyGroupPresent(regex, "FileType")) return false;
            if (!VerifyGroupPresent(regex, "Date")) return false;
            if (!VerifyGroupPresent(regex, "Path")) return false;

            return true;
        }

        private bool VerifyGroupPresent(Regex regex, string name)
        {
            if (!regex.GetGroupNames().Contains(name))
            {
                ErrorMessage = "Expression must have a {0} group.".FormatWith(name);
                return false;
            }
            return true;
        }
    }
}