﻿using IO.Practiceware.Configuration;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientImageImporter
{
    /// <summary>
    ///   Criteria for running the importer.
    /// </summary>
    [DataContract]
    [EditableObject, SupportsDataErrorInfo]
    public class PatientImageImporterCriteriaViewModel : IViewModel
    {
        public PatientImageImporterCriteriaViewModel()
        {
            DestinationPath = ConfigurationManager.ServerDataPath;
        }

        [Required]
        [Display(Name = "Source Path", Order = 0)]
        [DirectoryExists]
        [DataMember]
        public virtual string SourcePath { get; set; }

        [Required]
        [Display(Name = "Destination Path", Order = 1)]
        [DirectoryExists]
        [DataMember]
        public virtual string DestinationPath { get; set; }

        [Display(Name = "Created After", Order = 2)]
        [DataMember]
        public virtual DateTime? CreatedAfter { get; set; }

        [Display(Name = "Created Before", Order = 3)]
        [DataMember]
        public virtual DateTime? CreatedBefore { get; set; }

        [Display(Name = "Max Files to Import", Order = 4)]
        [DataMember]
        public virtual int? MaximumFilesToImport { get; set; }

        [Display(Name = "Max Run Minutes", Order = 5)]
        [DataMember]
        public virtual int? MaximumRunMinutes { get; set; }

        [Display(Order = 6)]
        [DataMember]
        public virtual string Filter { get; set; }

        [Display(Order = 7, Name = "Move Files")]
        [DataMember]
        public virtual bool MoveFiles { get; set; }

        [Display(Order = 8, Name = "Convert to PDF")]
        [DataMember]
        public virtual bool ConvertToPdf { get; set; }
    }
}