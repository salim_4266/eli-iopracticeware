﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSelector
{
    [DataContract]
    public class AppointmentSelectorFilterViewModel : IViewModel
    {
        #region Dropdown Lists

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> ScheduledDoctors { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> ServiceLocations { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> AppointmentTypes { get; set; }

        #endregion
    }

    [SupportsDataErrorInfo]
    [DataContract]
    public class AppointmentSelectorFilterSelectionViewModel : IViewModel
    {
        private DateTime? _endDate;

        public AppointmentSelectorFilterSelectionViewModel()
        {
            StartDate = DateTime.Now.ToClientTime();
            EndDate = DateTime.Now.ToClientTime(); 
            SelectedScheduledDoctors = new ExtendedObservableCollection<NamedViewModel>();
            SelectedServiceLocations = new ExtendedObservableCollection<NamedViewModel>();
            SelectedAppointmentTypes = new ExtendedObservableCollection<NamedViewModel>();
        }

        [Required(ErrorMessage = "A start date is required.")]
        [DataMember]
        public virtual DateTime? StartDate { get; set; }

        [EqualToGreaterThan("StartDate", ErrorMessage = "Cannot perform search if the end date is before the start date.")]
        [DataMember]
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;

                // Make the end date include the whole day
                if (_endDate.HasValue)
                {
                    var dateOnly = _endDate.Value.Date;
                    _endDate = dateOnly.AddDays(1).Subtract(TimeSpan.FromMinutes(1));
                }
            }
        }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedScheduledDoctors { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedServiceLocations { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedAppointmentTypes { get; set; }

     }
}