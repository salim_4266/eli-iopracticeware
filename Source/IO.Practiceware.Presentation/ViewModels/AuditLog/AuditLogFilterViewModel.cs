﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.AuditLog;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.AuditLog
{
    [DataContract]
    public class ActionTypeViewModel : NamedViewModel
    {
        /// <summary>
        /// Gets or sets whether this instance represents an audit action type
        /// or a log category action type.
        /// </summary>
        [DataMember]
        public virtual bool IsAuditAccessType { get; set; }
    }
    
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class AuditLogFilterViewModel : IViewModel
    {
        public AuditLogFilterViewModel()
        {
            SelectedDetailsChanged = new ExtendedObservableCollection<NamedViewModel>();
            SelectedUsers = new ExtendedObservableCollection<NamedViewModel>();
            SelectedActionTypes = new ExtendedObservableCollection<ActionTypeViewModel>();
        }
        [DispatcherThread]
        [EqualToLessThan("EndDate", ErrorMessage = "Start date can't be greater than the end date")]
        [Required(ErrorMessage = "Please select a start date")]
        [DataMember]
        public virtual DateTime? StartDate { get; set; }

        [DispatcherThread]
        [EqualToGreaterThan("StartDate", ErrorMessage = "End date can't be before the start date.")]
        [Required(ErrorMessage = "Please select a end date")]
        [DataMember]
        public virtual DateTime? EndDate { get; set; }

        [DependsOn("EndDate")]
        public virtual DateTime? AdjustedEndDate
        {
            // 23:59 is actual enddate
            get { return EndDate.HasValue ? EndDate.Value.Add(TimeSpan.FromDays(1)) : (DateTime?)null; }
        }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Users { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedUsers { get; set; }

        private Boolean _isFilteringByUsers;
        [DataMember]
        public virtual bool IsFilteringByUsers
        {
            get { return _isFilteringByUsers; } 
            set
            {
                _isFilteringByUsers = value;
                if (!_isFilteringByUsers) SelectedUsers.Clear();
            }
        }

        [RegexMatch("[0-9.-]+", SuccessOnNullOrEmptyInput = true, ErrorMessage = "Patient Id must be a number.")]
        [DataMember]
        public virtual string PatientId { get; set; }

        private Boolean _isFilteringByPatientId;
        [DataMember]
        public virtual bool IsFilteringByPatientId
        {
            get { return _isFilteringByPatientId; } 
            set
            {
                _isFilteringByPatientId = value;
                if (!_isFilteringByPatientId) PatientId = string.Empty;
            }
        }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> DetailsChanged { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedDetailsChanged { get; set; }

        private Boolean _isFilteringByDetailsChanged;
        [DataMember]
        public virtual bool IsFilteringByDetailsChanged
        {
            get { return _isFilteringByDetailsChanged; } 
            set
            {
                _isFilteringByDetailsChanged = value;
                if (!value) SelectedDetailsChanged.Clear();
            }
        }

        [DataMember]
        public virtual ObservableCollection<ActionTypeViewModel> ActionTypes { get; set; }

        [DataMember]
        public virtual ObservableCollection<ActionTypeViewModel> SelectedActionTypes { get; set; }

        private Boolean _isFilteringByActionTypes;
        [DataMember]
        public virtual bool IsFilteringByActionTypes
        {
            get { return _isFilteringByActionTypes; }
            set
            {
                _isFilteringByActionTypes = value;
                if (!_isFilteringByActionTypes) SelectedActionTypes.Clear();
            }
        }

        [DataMember]
        public virtual OrderByTypeId OrderByTypeId { get; set; }
    }

    [DataContract]
    public class AuditLogViewModel : IViewModel
    {
        [DataMember]
        public virtual DateTime DateTime { get; set; }

        [DataMember]
        public virtual NamedViewModel User { get; set; }
        
        [DataMember]
        public virtual string AccessLocation { get; set; }

        [DataMember]
        public virtual string AccessDevice { get; set; }

        [DataMember]
        public virtual string SourceOfAccess { get; set; }

        [DataMember]
        public virtual NamedViewModel Patient { get; set; }

        [DataMember]
        public virtual string ActionMessage { get; set; }

        [DataMember]
        public virtual string DetailChanged { get; set; }

        [DataMember]
        public virtual string NewValue { get; set; }

        [DataMember]
        public virtual string OldValue { get; set; }

        [DataMember]
        public virtual ContentType ContentType { get; set; }

        public virtual Color Color { get; set; }

        [DataMember]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }
    }
}
