using Soaf;
using System;
using System.ComponentModel;
using System.Windows;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    public class WidgetInContainerContext : Freezable, INotifyPropertyChanged
    {
        public static readonly DependencyProperty WidgetProperty =
            DependencyProperty.Register("Widget", typeof(WidgetViewModel), typeof(WidgetInContainerContext), new PropertyMetadata(default(WidgetViewModel), OnWidgetPropertyChanged));

        public static readonly DependencyProperty ContainerProperty =
            DependencyProperty.Register("Container", typeof(WidgetsContainerViewModel), typeof(WidgetInContainerContext), new PropertyMetadata(default(WidgetsContainerViewModel), OnContainerPropertyChanged));

        bool _isWidgetOwnedByContainer;

        public WidgetsContainerViewModel Container
        {
            get { return (WidgetsContainerViewModel) GetValue(ContainerProperty); }
            set { SetValue(ContainerProperty, value); }
        }

        public WidgetViewModel Widget
        {
            get { return (WidgetViewModel) GetValue(WidgetProperty); }
            set { SetValue(WidgetProperty, value); }
        }

        public bool IsWidgetOwnedByContainer
        {
            get { return _isWidgetOwnedByContainer; }
            set
            {
                if (value.Equals(_isWidgetOwnedByContainer)) return;
                _isWidgetOwnedByContainer = value;
                OnPropertyChanged("IsWidgetOwnedByContainer");
            }
        }

        static void OnContainerPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var instance = (WidgetInContainerContext) dependencyObject;
            instance.UpdateIsWidgetOwned();
        }

        static void OnWidgetPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var instance = (WidgetInContainerContext)dependencyObject;

            var oldWidgetValue = dependencyPropertyChangedEventArgs.OldValue as WidgetViewModel;
            var newWidgetValue = dependencyPropertyChangedEventArgs.NewValue as WidgetViewModel;

            if (oldWidgetValue != null)
            {
                oldWidgetValue.CastTo<INotifyPropertyChanged>().PropertyChanged -= instance.OnWidgetViewModelPropertyChanged;
            }
            if (newWidgetValue != null)
            {
                newWidgetValue.CastTo<INotifyPropertyChanged>().PropertyChanged += instance.OnWidgetViewModelPropertyChanged;
            }

            instance.UpdateIsWidgetOwned();
        }

        private void OnWidgetViewModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "OwnerContainer")
            {
                UpdateIsWidgetOwned();
            }
        }

        private void UpdateIsWidgetOwned()
        {
            // Update the widget owned by container value
            IsWidgetOwnedByContainer =
                Widget != null
                && Container != null
                && Widget.OwnerContainer == Container;
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
