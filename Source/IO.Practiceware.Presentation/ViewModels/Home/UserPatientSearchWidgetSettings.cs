﻿using IO.Practiceware.Presentation.Views.PatientInfo;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    [DataContract]
    public class UserPatientSearchWidgetSettings
    {
        [DataMember]
        public PatientInfoTab PatientInfoTabToOpen { get; set; }
    }
}
