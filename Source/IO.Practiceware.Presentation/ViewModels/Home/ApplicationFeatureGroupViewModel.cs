﻿using Soaf.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    public class ApplicationFeatureGroupViewModel : IViewModel
    {
        ObservableCollection<ApplicationFeatureViewModel> _features;

        public ApplicationFeatureGroupViewModel()
        {
            Features = new ObservableCollection<ApplicationFeatureViewModel>();
        }

        public virtual ObservableCollection<ApplicationFeatureViewModel> Features
        {
            get { return _features; }
            set
            {
                if (Equals(value, _features)) return;

                if (_features != null)
                {
                    _features.CollectionChanged -= OnFeaturesCollectionChanged;
                }
                _features = value;
                if (_features != null)
                {
                    SetFeaturesGroupName(_features);

                    // Subscribe for to keep in sync in future
                    _features.CollectionChanged += OnFeaturesCollectionChanged;
                }
            }
        }

        public virtual string Name { get; set; }

        /// <summary>
        /// Special color for this group
        /// </summary>
        public virtual Brush SpecialColor { get; set; }

        /// <summary>
        /// Special gradient color for this group
        /// </summary>
        public virtual Brush SpecialGradientColor { get; set; }

        void OnFeaturesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            SetFeaturesGroupName(e.NewItems.OfType<ApplicationFeatureViewModel>());
        }

        void SetFeaturesGroupName(IEnumerable<ApplicationFeatureViewModel> features)
        {
            foreach (var featureViewModel in features)
            {
                featureViewModel.GroupName = Name;
                featureViewModel.Group = this;
            }
        }

    }
}
