﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    [DataContract]
    public class UserFeatureSelectionSettings
    {
        public UserFeatureSelectionSettings()
        {
            Features = new List<Feature>();
        }

        /// <summary>
        /// Collection of selected features
        /// </summary>
        [DataMember]
        public List<Feature> Features { get; set; }
        
        /// <summary>
        /// Selected feature description
        /// </summary>
        [DataContract]
        public class Feature
        {
            /// <summary>
            /// Feature identifier
            /// </summary>
            [DataMember]
            public string FeatureTypeKey { get; set; }
        }
    }
}
