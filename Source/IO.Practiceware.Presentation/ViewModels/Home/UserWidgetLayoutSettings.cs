﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    /// <summary>
    /// Persisted settings of current widget layout
    /// </summary>
    [DataContract]
    public class UserWidgetLayoutSettings
    {
        public UserWidgetLayoutSettings()
        {
            Containers = new List<WidgetContainer>();
        }

        /// <summary>
        /// A layout template which was selected by user
        /// </summary>
        [DataMember]
        public string LayoutTemplateKey { get; set; }

        /// <summary>
        /// A collection of containers for Widgets 
        /// </summary>
        [DataMember]
        public List<WidgetContainer> Containers { get; set; }
            
        [DataContract]
        public class WidgetContainer
        {
            public WidgetContainer()
            {
                Widgets = new List<WidgetSettings>();
            }

            /// <summary>
            /// Name of the container
            /// </summary>
            [DataMember]
            public string Name { get; set; }

            /// <summary>
            /// Widgets in the container
            /// </summary>
            [DataMember]
            public List<WidgetSettings> Widgets { get; set; }
        }

        /// <summary>
        /// Settings of the widget added into container
        /// </summary>
        [DataContract]
        public class WidgetSettings
        {
            /// <summary>
            /// Widget type
            /// </summary>
            [DataMember]
            public string WidgetTemplateKey { get; set; }

            /// <summary>
            /// Custom data associated with widget
            /// </summary>
            [DataMember]
            public object CustomData { get; set; }
        }
    }
}
