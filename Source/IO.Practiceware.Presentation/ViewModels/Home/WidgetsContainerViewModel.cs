﻿using Soaf.Presentation;
using System.Collections.Generic;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    public class WidgetsContainerViewModel : IViewModel
    {
        public virtual ICollection<WidgetViewModel> Widgets { get; set; }
    }
}