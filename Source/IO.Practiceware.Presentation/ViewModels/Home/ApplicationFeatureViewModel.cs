﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Windows.Input;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    /// <summary>
    /// Application feature view model
    /// </summary>
    /// <remarks>
    /// NOTE: NotifyChildPropertyChanged is set to false on SupportsNotifyPropertyChanged attribute
    /// to avoid StackOverflow when:
    /// -> IsPinned/IsDragged property changes 
    /// -> ApplicationFeatureViewModel changes 
    /// -> Features collection on ApplicationFeatureGroupViewModel changes
    /// -> ApplicationFeatureGroupViewModel changes 
    /// -> Group property of ApplicationFeatureViewModel changes
    /// -> endless recursion occurs
    /// TODO: think of a better way to handle this
    /// </remarks>
    [SupportsNotifyPropertyChanged(true, false)]
    public class ApplicationFeatureViewModel : IViewModel
    {
        string _featureKey;
        string _shortDisplayName;
        bool _isVisible;        

        /// <summary>
        /// Specifies whether user pinned current feature to the top of toolbar
        /// </summary>
        public virtual bool IsPinned { get; set; }

        /// <summary>
        /// Specifies whether feature is being currently dragged
        /// </summary>
        public virtual bool IsDragging { get; set; }

        public virtual string GroupName { get; set; }

        /// <summary>
        /// TODO: do not serialize it and sync with Group name
        /// </summary>
        public virtual ApplicationFeatureGroupViewModel Group { get; set; }

        public virtual string DisplayName { get; set; }

        [DependsOn("GroupName", "DisplayName")]
        public virtual string FeatureKey
        {
            get
            {
                var temporaryFixFeatureKey = string.Join("_", GroupName, DisplayName);
                return string.IsNullOrEmpty(_featureKey) ? temporaryFixFeatureKey : _featureKey;
            }
            set { _featureKey = value; }
        }

        [DependsOn("DisplayName")]
        public string ShortDisplayName
        {
            get
            {
                return string.IsNullOrEmpty(_shortDisplayName) ? DisplayName : _shortDisplayName;
            }
            set { _shortDisplayName = value; }
        }

        [DispatcherThread]
        public virtual bool IsVisible
        {   
            get
            {
                return _isVisible;
            }
            set { _isVisible = value; }
        }
       
        /// <summary>
        /// Gets or sets the command that handles this feature's execution behavior.
        /// </summary>
        /// <value>The type of the feature.</value>
        public ICommand Command { get; set; }

        /// <summary>
        /// Gets or sets the command parameter.
        /// </summary>
        /// <value>The command parameter.</value>
        public object CommandParameter { get; set; }

        protected bool Equals(ApplicationFeatureViewModel other)
        {
            return string.Equals(FeatureKey, other.FeatureKey) && string.Equals(GroupName, other.GroupName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ApplicationFeatureViewModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((FeatureKey != null ? FeatureKey.GetHashCode() : 0)*397) ^ (GroupName != null ? GroupName.GetHashCode() : 0);
            }
        }

        public static bool operator ==(ApplicationFeatureViewModel left, ApplicationFeatureViewModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ApplicationFeatureViewModel left, ApplicationFeatureViewModel right)
        {
            return !Equals(left, right);
        }
    }
}