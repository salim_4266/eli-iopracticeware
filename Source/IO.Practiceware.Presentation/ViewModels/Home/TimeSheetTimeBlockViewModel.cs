﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    [DataContract]
    public class TimeSheetTimeBlockViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual bool IsBreak { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual DateTime? EndDateTime { get; set; }
    }
}
