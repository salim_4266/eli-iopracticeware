﻿using IO.Practiceware.Model;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Home
{
    public class WidgetViewModel : IViewModel
    {
        public virtual string WidgetTemplateKey { get; set; }

        /// <summary>
        /// Custom data which widget wants to store with the model
        /// </summary>
        public virtual object CustomWidgetData { get; set; }

        /// <summary>
        /// A container which this widget belongs to
        /// </summary>
        public virtual WidgetsContainerViewModel OwnerContainer { get; set; }

        public virtual string PreviewImageSource { get; set; }

        public virtual string DisplayName { get; set; }

        public virtual string Description { get; set; }

        public virtual bool IsAddedByDefault { get; set; }

        public virtual bool IsDragging { get; set; }

        public virtual bool IsVisible { get; set; }

        public virtual PermissionId? PermissionId { get; set; }
    }
}
