using System.Runtime.Serialization;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PostalCodeViewModel : IViewModel
    {
        private const string Zip4Regex = @"(\d{9})|(\d{5})|(_____-____)";

        public PostalCodeViewModel()
        {
            Value = "";
        }
        [DataMember]
        [RegexMatch(Zip4Regex, SuccessOnNullOrEmptyInput = true, ErrorMessage = "Postal Code is not in the correct format.")]
        public virtual string Value { get; set; }

        public override string ToString()
        {
            if (Value == null) return "";
            return (Value.Length > 5) ? Value.Substring(0, 5) + '-' + Value.Substring(5, Value.Length - 5) : Value;
        }
    }
}