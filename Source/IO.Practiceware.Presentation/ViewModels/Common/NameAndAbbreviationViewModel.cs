﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [EditableObject]
    [DataContract]
    public class NameAndAbbreviationViewModel<TKey> : NamedViewModel<TKey>
    {
        public NameAndAbbreviationViewModel()
        {}

        public NameAndAbbreviationViewModel(TKey id, string name, string abbreviation)
            : base(id, name)
        {
            Abbreviation = abbreviation;
        }

        [DataMember]
        [DispatcherThread]
        public virtual string Abbreviation { get; set; }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Abbreviation)
                ? base.ToString()
                : Abbreviation;
        }

        protected bool Equals(NameAndAbbreviationViewModel<TKey> other)
        {
            return base.Equals(other) && string.Equals(Abbreviation, other.Abbreviation);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((NameAndAbbreviationViewModel<TKey>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Abbreviation != null ? Abbreviation.GetHashCode() : 0);
            }
        }
    }

    [EditableObject]
    [DataContract]
    public class NameAndAbbreviationViewModel : NameAndAbbreviationViewModel<int>
    {
        public NameAndAbbreviationViewModel()
        {}

        public NameAndAbbreviationViewModel(int id, string name, string abbreviation) 
            : base(id, name, abbreviation)
        {}
    }

    [EditableObject]
    [DataContract]
    public class NameAndAbbreviationAndDetailViewModel<TKey> : NameAndAbbreviationViewModel<TKey>
    {
        public NameAndAbbreviationAndDetailViewModel()
        {}

        public NameAndAbbreviationAndDetailViewModel(TKey id, string name, string abbreviation, string detail)
            : base(id, name, abbreviation)
        {
            Detail = detail;
        }

        [DataMember]
        [DispatcherThread]
        public virtual string Detail { get; set; }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Detail) 
                ? base.ToString() 
                : string.Format("{0} ({1})", base.ToString(), Detail);
        }

        protected bool Equals(NameAndAbbreviationAndDetailViewModel<TKey> other)
        {
            return base.Equals(other) && string.Equals(Detail, other.Detail);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((NameAndAbbreviationAndDetailViewModel<TKey>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode()*397) ^ (Detail != null ? Detail.GetHashCode() : 0);
            }
        }
    }

    [EditableObject]
    [DataContract]
    public class NameAndAbbreviationAndDetailViewModel : NameAndAbbreviationAndDetailViewModel<int>
    {
        public NameAndAbbreviationAndDetailViewModel()
        {}

        public NameAndAbbreviationAndDetailViewModel(int id, string name, string abbreviation, string detail)
            : base(id, name, abbreviation, detail)
        {}
    }
}
