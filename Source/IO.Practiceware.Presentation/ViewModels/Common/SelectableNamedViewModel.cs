﻿using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    public class SelectableNamedViewModel : NamedViewModel
    {
        [DataMember]
        public virtual bool IsSelected { get; set; }
    }
}
