﻿using System.ComponentModel;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Validation;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class AddressViewModel : IViewModel, IIsValidationEnabled
    {
       // The first ^ anchors the search to the beginning of the string. The second ^ in the brackets specifies to match 
        // any character that is not one of the characters in the brackets.  The + means to match one or more characters.
        // The $ means anchor the search to the end of the string.
        // Putting it all together:  Search from the beginning of a string and look at each character to make sure
        // it doesn't match the characters in the character group, looking at each character until the end of the string.
        public const string AcceptableTextInputDoesNotContainTheseCharacters = @"^[^~:|^&\*]+$";

        public AddressViewModel()
        {
            this.As<INotifyPropertyChanged>().IfNotNull(npc => npc.PropertyChanged += OnPropertyChanged);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // force rebind of all properties on any property changed - this is required because of circular dependencies between
            // properties required for validation - area code may invalidate exchange/suffix and exchange/suffix may invalidate area code.
            if (e.PropertyName != string.Empty) this.As<IRaisePropertyChanged>().IfNotNull(rpc => rpc.OnPropertyChanged(new PropertyChangedEventArgs(string.Empty)));
        }

        [DataMember]
        [DispatcherThread]
        public virtual long? Id { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        [DispatcherThread]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Line1 { get; set; }

        [DataMember]
        [DispatcherThread]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Line2 { get; set; }

        [DataMember]
        [DispatcherThread]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string Line3 { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        [DispatcherThread]
        public virtual NamedViewModel AddressType { get; set; }

        [DataMember]
        [DispatcherThread]
        [RequiredWithDisplayName]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string City { get; set; }

        [DataMember, RequiredWithDisplayName, DispatcherThread]
        public virtual StateOrProvinceViewModel StateOrProvince { get; set; }

        [DataMember]
        [DispatcherThread]
        [RequiredIf("IsInternational")]
        [RegexMatch(AcceptableTextInputDoesNotContainTheseCharacters, "Value cannot contain illegal characters.", SuccessOnNullOrEmptyInput = true)]
        public virtual string PostalCode { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual bool IsPrimary { get; set; }

        [DependsOn("StateOrProvince")]
        protected virtual bool IsInternational
        {
            get { return StateOrProvince != null && StateOrProvince.Country != null && StateOrProvince.Country.Abbreviation == "USA"; }
        }

        [DependsOn("Line1", "City", "StateOrProvince", "PostalCode")]
        public virtual bool IsEmpty
        {
            get { return (Line1.IsNullOrEmpty() && City.IsNullOrEmpty() && StateOrProvince == null && PostalCode.IsNullOrEmpty()); }
        }

        [DependsOn("IsEmpty")]
        public virtual bool IsValidationEnabled
        {
            get { return !IsEmpty; }
        }

        public override string ToString()
        {
            return new object[] { Line1, Line2, Line3, City, StateOrProvince, PostalCode }.Join(", ", true);
        }
    }

    public static class AddressViewModelExtensions
    {
        public static IMapper<T, AddressViewModel> CreateAddressViewModelMapper<T>(this ICustomMapperFactory mapperFactory)
            where T : IAddress
        {
            var stateOrProvinceViewModelMapper = mapperFactory.CreateStateOrProvinceViewModelMapper();

            return mapperFactory.CreateMapper<T, AddressViewModel>(
                address => new AddressViewModel
                           {
                               Id = address.Id,
                               AddressType = address.AddressType == null ? null : new NamedViewModel { Id = address.AddressType.Id, Name = address.AddressType.Name },
                               Line1 = address.Line1,
                               Line2 = address.Line2,
                               Line3 = address.Line3,
                               City = address.City,
                               StateOrProvince = address.StateOrProvince == null ? null : stateOrProvinceViewModelMapper.Map(address.StateOrProvince),
                               PostalCode = address.PostalCode,
                               IsPrimary = !Equals(address, default(T)) && address is IHasOrdinalId && ((IHasOrdinalId)address).OrdinalId == 1
                           }, false);
        }

        public static IMapper<StateOrProvince, StateOrProvinceViewModel> CreateStateOrProvinceViewModelMapper(this ICustomMapperFactory mapperFactory)
        {
            return mapperFactory.CreateMapper<StateOrProvince, StateOrProvinceViewModel>(i => new StateOrProvinceViewModel
                                                                                              {
                                                                                                  Id = i.Id,
                                                                                                  Name = i.Abbreviation + " - " + i.Name,
                                                                                                  Abbreviation = i.Abbreviation,
                                                                                                  Country = i.Country == null ? null : new NameAndAbbreviationViewModel
                                                                                                                                       {
                                                                                                                                           Id = i.CountryId,
                                                                                                                                           Name = i.Country.Name,
                                                                                                                                           Abbreviation = i.Country.Abbreviation3Letters
                                                                                                                                       }
                                                                                              });
        }
    }
}