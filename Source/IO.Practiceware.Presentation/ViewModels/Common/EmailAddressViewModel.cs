﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf;
using IEmailAddress = IO.Practiceware.Model.IEmailAddress;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class EmailAddressViewModel : IViewModel, IIsValidationEnabled
    {
        public EmailAddressViewModel()
        {
            this.As<INotifyPropertyChanged>().IfNotNull(npc => npc.PropertyChanged += OnPropertyChanged);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // force rebind of all properties on any property changed - this is required because of circular dependencies between
            // properties required for validation - area code may invalidate exchange/suffix and exchange/suffix may invalidate area code.
            if (e.PropertyName != string.Empty) this.As<IRaisePropertyChanged>().IfNotNull(rpc => rpc.OnPropertyChanged(new PropertyChangedEventArgs(string.Empty)));
        }

        [DataMember]
        [DispatcherThread]
        public virtual int? Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int? OrdinalId { get; set; }

        [DataMember]
        [DispatcherThread]
        [RequiredWithDisplayName(ErrorMessage = "A value is required.", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public virtual string Value { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int PatientId { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual bool IsPrimary { get; set; }

        [DataMember]
        [DispatcherThread]
        [RequiredWithDisplayName]
        public virtual NamedViewModel EmailAddressType { get; set; }

        [DependsOn("Value", "EmailAddressType")]
        public virtual bool IsEmpty
        {
            get { return Value.IsNullOrEmpty() && EmailAddressType == null; }
        }

        [DependsOn("IsEmpty")]
        public virtual bool IsValidationEnabled
        {
            get { return !IsEmpty; }
        }
    }

    public static class EmailAddressViewModelExtensions
    {
        public static IMapper<T, EmailAddressViewModel> CreateEmailAddressViewModelMapper<T>(this ICustomMapperFactory mapperFactory)
            where T : IEmailAddress
        {
            return mapperFactory.CreateMapper<T, EmailAddressViewModel>(
               i => new EmailAddressViewModel
               {
                   Id = !Equals(i, default(T)) ? i.Id : (int?)null,
                   EmailAddressType = !Equals(i, default(T)) ? new NamedViewModel { Id = (int)i.EmailAddressType, Name = i.EmailAddressType.GetDisplayName() } : null,
                   IsPrimary = !Equals(i, default(T)) && i is IHasOrdinalId && ((IHasOrdinalId)i).OrdinalId == 1,
                   Value = !Equals(i, default(T)) ? i.Value : null,
                   OrdinalId = !Equals(i, default(T)) ? i.OrdinalId : 1
               }, false);
        }
    }
}
