﻿using Soaf;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    public class PersonViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        public override string ToString()
        {
            return "{0}, {1}".FormatWith(LastName, FirstName);
        }
    }
}
