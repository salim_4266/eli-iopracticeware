﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    public class ResourceViewModel : IViewModel, IEquatable<ResourceViewModel>
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string UserName { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        public bool Equals(ResourceViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id
                   && UserName == other.UserName
                   && FirstName == other.FirstName
                   && LastName == other.LastName
                   && DisplayName == other.DisplayName;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ResourceViewModel);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode()
                   ^ Objects.GetHashCodeWithNullCheck(UserName)
                   ^ Objects.GetHashCodeWithNullCheck(FirstName)
                   ^ Objects.GetHashCodeWithNullCheck(LastName)
                   ^ Objects.GetHashCodeWithNullCheck(DisplayName);
        }

        public static bool operator ==(ResourceViewModel lhs, ResourceViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(ResourceViewModel lhs, ResourceViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}
