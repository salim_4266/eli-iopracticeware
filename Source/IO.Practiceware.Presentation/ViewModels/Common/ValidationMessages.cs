﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{

    [DataContract]
    public enum ValidationMessage
    {
        [EnumMember]
        [Display(Name = "The patient must have a valid first name, last name and date of birth.")]
        PatientValidNameAndDob,
        [EnumMember]
        [Display(Name = "The patient must have a valid primary address.")]
        PatientPrimaryAddress,
        [EnumMember]
        [Display(Name = "The billing organization must have a valid address.")]
        BillingOrganizationPrimaryAddress,
        [EnumMember]
        [Display(Name = "Missing insurer address.")]
        InsurerAddress,
        [EnumMember]
        [Display(Name = "Missing NPI, taxonomy, tax id/SSN for billing organization.")]
        BillingOrganizationMissingNpiTaxSsn,
        [EnumMember]
        [Display(Name = "Missing NPI and taxonomy for rendering provider.")]
        RenderingMissingNpiTaxonomy,
        [EnumMember]
        [Display(Name = "Missing NPI for referring provider.")]
        ReferringMissingNpi,
        [EnumMember]
        [Display(Name = "Invoice must have at least one diagnosis.")]
        MissingDiagnosis,
        [EnumMember]
        [Display(Name = "Claim requires referral.")]
        ClaimRequireReferral,
        [EnumMember]
        [Display(Name = "Consultations require a referring provider.")]
        ConsultationsRequireReferringProvider,
        [EnumMember]
        [Display(Name = "Rendering provider must have a valid first name and last name.")]
        RenderingMissingValidName,
        [EnumMember]
        [Display(Name = "Missing last name for referring provider.")]
        ReferringMissingLastName,
        [EnumMember]
        [Display(Name = "Invoice must have at least one billing service.")]
        MissingBillingServices,
        [EnumMember]
        [Display(Name = "The referring provider must have a valid address.")]
        ReferringOrganizationPrimaryAddress,
        [EnumMember]
        [Display(Name = "The policy holder must have a valid first name and last name.")]
        PolicyHolderValidName,
        [EnumMember]
        [Display(Name = "The policy holder must have a valid address.")]
        PolicyHolderPrimaryAddress,
        [EnumMember]
        [Display(Name = "Policy number must be letters and numbers only.")]
        ValidPolicyCode,
        [EnumMember]
        [Display(Name = "Missing NPI for service location.")]
        ServiceLocationMissingNpi,
        [EnumMember]
        [Display(Name = "Service location must have a valid address.")]
        ServiceLocationPrimaryAddress,
        [EnumMember]
        [Display(Name = "Billable rendering provider required.")]
        BillableRenderingProviderRequired
    }
}
