﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common.Configuration
{
    [DataContract]
    public class MinuteIncrementSelectorViewModel : IViewModel
    {
        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual TimeSpan Increment { get; set; }
    }
}