using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common.Configuration
{
    [DataContract]
    public class ScheduleConfigurationViewModel : IViewModel
    {
        public ScheduleConfigurationViewModel()
        {
            SpanSize = new TimeSpan(0, 30, 0);
            AreaStart = new TimeSpan(0, 0, 0);
            AreaEnd = new TimeSpan(23, 59, 59);
            DefaultLocation = new ColoredViewModel();
        }

        [DataMember]
        public ColoredViewModel DefaultLocation { get; set; }

        [DataMember]
        public TimeSpan SpanSize { get; set; }

        [DataMember]
        public TimeSpan AreaStart { get; set; }

        [DataMember]
        public TimeSpan AreaEnd { get; set; }

        [DataMember]
        public int MaximumApplyTemplateMonths { get; set; }
    }
}