﻿using Soaf;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    /// <summary>
    /// A general purpose named view model with a color.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    [DataContract]
    public class ColoredViewModel<TKey> : NamedViewModel<TKey>
    {
        [DispatcherThread]
        public virtual Color Color { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }
    }

    [DataContract]
    public class ColoredViewModel : ColoredViewModel<int>, IEquatable<ColoredViewModel>
    {
        public bool Equals(ColoredViewModel other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (ReferenceEquals(null, other)) return false;

            return Id == other.Id && Color == other.Color;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ColoredViewModel);
        }

        public override int GetHashCode()
        {
            return Objects.GetHashCodeWithNullCheck(Id) ^ Color.GetHashCode();
        }

        public static bool operator ==(ColoredViewModel lhs, ColoredViewModel rhs)
        {
            return ReferenceEquals(lhs, null) ? ReferenceEquals(rhs, null) : lhs.Equals(rhs);
        }

        public static bool operator !=(ColoredViewModel lhs, ColoredViewModel rhs)
        {
            return !(lhs == rhs);
        }
    }
}