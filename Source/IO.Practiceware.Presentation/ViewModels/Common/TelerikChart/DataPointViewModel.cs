﻿using Soaf.Presentation;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.Common.TelerikChart
{
    public class DataPointViewModel : IViewModel
    {
        public virtual Color ShapeFillColor { get; set; }
        public virtual Color ShapeBorderColor { get; set; }
        public virtual double PlotPoint { get; set; }
    }
}
