﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Common.TelerikChart
{
    public class MedicationViewModel : IViewModel
    {
        public virtual string Qualifier { get; set; }
        public virtual string Frequency { get; set; }
        public virtual string Name { get; set; }
        public virtual bool IsNew { get; set; }
    }
}
