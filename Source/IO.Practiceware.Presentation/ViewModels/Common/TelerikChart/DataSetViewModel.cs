﻿using IO.Practiceware.Presentation.Views.Common.Behaviors;
using Soaf.Presentation;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.ViewModels.Common.TelerikChart
{
    public class DataSetViewModel : IViewModel
    {
        public virtual int OrdinalId { get; set; }
        public virtual DataSetShape Shape { get; set; }
        public virtual DataSetLineStyle LineStyle { get; set; }
        public virtual ObservableCollection<DataPointViewModel> DataPoints { get; set; }
    }
}
