﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Common.TelerikChart
{
    public class TestViewModel : IViewModel
    {
        public virtual string Type { get; set; }
        public virtual string FilePath { get; set; }
    }
}
