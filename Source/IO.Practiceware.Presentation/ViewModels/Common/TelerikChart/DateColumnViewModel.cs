﻿using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace IO.Practiceware.Presentation.ViewModels.Common.TelerikChart
{
    public class DateColumnViewModel : IViewModel
    {
        public const string CategoryBindingPropertyName = "Date";
        public const string ValueBindingPropertyName = "HeaderPlotPoint";

        public virtual DateTime Date { get; set; }
        public virtual Visibility Visibility { get; set; }
        public virtual double HeaderPlotPoint { get; set; }
        public virtual ObservableCollection<MedicationViewModel> Medications { get; set; }
        public virtual ObservableCollection<TestViewModel> Tests { get; set; }
        public virtual ObservableCollection<DataSetViewModel> DataSets { get; set; }
        public virtual ObservableCollection<ProcedureViewModel> Procedures { get; set; }
    }
}
