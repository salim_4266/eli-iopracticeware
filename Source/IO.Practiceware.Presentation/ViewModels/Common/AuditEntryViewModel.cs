using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    public class AuditEntryViewModel : IViewModel
    {
        /// <summary>
        /// Id of the audited entity
        /// </summary>
        [DataMember]
        public virtual int EntityId { get; set; }

        [DataMember]
        public virtual DateTime DateTime { get; set; }

        [DataMember]
        public virtual string UserName { get; set; }

        [DataMember]
        public virtual string Field { get; set; }

        [DataMember]
        public virtual string ChangedFrom { get; set; }

        [DataMember]
        public virtual string ChangedTo { get; set; }

        // Type of change: Insert, Delete, Update
        [DataMember]
        public virtual string ChangeType { get; set; }

        /// <summary>
        /// Assists in identifying who created this entity
        /// </summary>
        [DataMember]
        public virtual bool IsInsertChange { get; set; }
    }
}