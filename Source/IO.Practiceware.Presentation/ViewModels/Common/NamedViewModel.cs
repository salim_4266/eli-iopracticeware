﻿using System.ComponentModel.DataAnnotations;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Soaf.Reflection;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    /// <summary>
    /// An all-purpose view model containing a Name and Id.
    /// </summary>
    [EditableObject]
    [DataContract]
    public class NamedViewModel<TKey> : IViewModel
    {
        /// <summary>
        /// Allows to compare two named view models by name (case insensitive)
        /// </summary>
        public static readonly IEqualityComparer<NamedViewModel<TKey>> NameBasedComparer = EqualityComparer.For<NamedViewModel<TKey>>(
            (x, y) => string.Equals(x.Name, y.Name, StringComparison.OrdinalIgnoreCase));

        public NamedViewModel() { }

        public NamedViewModel(TKey id, string name)
        {
            Id = id;
            Name = name;
        }

        [DispatcherThread]
        [DataMember]
        public virtual TKey Id { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual string Name { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as NamedViewModel<TKey>;
            if (other == null) { return false; }

            return Equals(other.Id, Id) && other.Name == Name;
        }

        public override int GetHashCode()
        {
            return (Id.GetHashCode() ^
                    (Name == null ? 0 : Name.GetHashCode()));
        }

        public static NamedViewModel<TKey> Create(TKey id, string name)
        {
            return new NamedViewModel<TKey> { Id = id, Name = name };
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class NamedViewModel : NamedViewModel<int>
    {
        public NamedViewModel() { }

        public NamedViewModel(int id, string name)
            : base(id, name)
        {}
    }

    public static class NamedViewModelExtensions
    {
        /// <summary>
        /// TODO: Returned NamedViewModel-s won't have AOP applied to them
        /// </summary>
        /// <typeparam name="TValueType"></typeparam>
        /// <param name="useDisplayAttribute"></param>
        /// <param name="orderByDisplayOrderAttribute"></param>
        /// <returns></returns>
        public static List<NamedViewModel> ToNamedViewModel<TValueType>(bool useDisplayAttribute = true, bool orderByDisplayOrderAttribute = false) where TValueType : struct
        {
            return ToNamedViewModel<int, TValueType>(useDisplayAttribute, orderByDisplayOrderAttribute).Select(x => new NamedViewModel { Id = x.Id, Name = x.Name }).ToList();
        }

        /// <summary>
        /// TODO: Returned NamedViewModel-s won't have AOP applied to them
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValueType"></typeparam>
        /// <param name="useDisplayAttribute"></param>
        /// <param name="orderByDisplayOrderAttribute"></param>
        /// <returns></returns>
        public static List<NamedViewModel<TKey>> ToNamedViewModel<TKey, TValueType>(bool useDisplayAttribute = true, bool orderByDisplayOrderAttribute = false)
            where TValueType : struct
            where TKey : struct
        {
            if (typeof(TValueType).IsEnum)
            {
                var values = !orderByDisplayOrderAttribute ? Enums.GetValues<TValueType>() : Enums.GetValues<TValueType>().OrderBy(e => e.As<Enum>().GetAttribute<DisplayAttribute>().Order).ToArray();
                
                return values.Select(x => new NamedViewModel<TKey> { Id = x.CastTo<object>().CastTo<TKey>(), Name = useDisplayAttribute ? x.As<Enum>().GetDisplayName() : x.As<Enum>().ToString() }).ToList();
            }
            return typeof(TValueType).GetFields(BindingFlags.Public | BindingFlags.Static).Select(x => new NamedViewModel<TKey> { Id = x.GetValue(null).CastTo<TKey>(), Name = x.GetValue(null).ToString() }).ToList();
        }

        /// <summary>
        /// Creates mapper from Enum value to NamedViewModel
        /// </summary>
        /// <param name="mapperFactory"></param>
        /// <returns></returns>
        public static IMapper<TValueType, NamedViewModel> CreateNamedViewModelMapper<TValueType>(this ICustomMapperFactory mapperFactory)
            where TValueType : struct
        {
            return mapperFactory.CreateMapper<TValueType, NamedViewModel>(
               i => new NamedViewModel
               {
                   Id = i.CastTo<object>().CastTo<int>(),
                   Name = i.As<Enum>().GetDisplayName()
               }, false);
        }

        /// <summary>
        /// Creates mapper from any model having Id and Name properties to NamedViewModel
        /// </summary>
        /// <param name="mapperFactory"></param>
        /// <returns></returns>
        public static IMapper<TModel, NamedViewModel> CreateCommonNamedViewModelMapper<TModel>(this ICustomMapperFactory mapperFactory)
            where TModel : class
        {
            var idProperty = typeof (TModel).GetProperty("Id").EnsureNotDefault("Must have Id property");
            if (!idProperty.PropertyType.Is<int>()) throw new ArgumentException("Id property must be of Integer type");
            
            var nameProperty = typeof (TModel).GetProperty("Name").EnsureNotDefault("Must have Name property");
            if (!nameProperty.PropertyType.Is<string>()) throw new ArgumentException("Name property must be of String type");

            var idPropertyGetter = idProperty.GetGetMethod().GetInvoker();
            var namePropertyGetter = nameProperty.GetGetMethod().GetInvoker();

            return mapperFactory.CreateMapper<TModel, NamedViewModel>(
               i => new NamedViewModel
               {
                   Id = idPropertyGetter(i, null).CastTo<int>(),
                   Name = namePropertyGetter(i, null).CastTo<string>()
               }, false);
        }
    }
}
