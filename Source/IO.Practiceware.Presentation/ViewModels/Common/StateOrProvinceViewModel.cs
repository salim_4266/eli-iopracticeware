﻿using System.Runtime.Serialization;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    /// <summary>
    ///     A view model for a state and its country.
    /// </summary>
    [DataContract]
    [SupportsDataErrorInfo]
    public class StateOrProvinceViewModel : NameAndAbbreviationViewModel
    {
        [DataMember]
        [RequiredWithDisplayName]
        public virtual NameAndAbbreviationViewModel Country { get; set; }

        public override string ToString()
        {
            return new[] { Abbreviation, Country.IfNotNull(x => x.Abbreviation) }.Join(", ", true);
        }
    }
}