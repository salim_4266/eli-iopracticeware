using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Serialization;
using IO.Practiceware.Model;
using IO.Practiceware.Validation;
using PhoneNumbers;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    [ValidCountryCode]
    public class PhoneNumberViewModel : IViewModel, IIsValidationEnabled, IPhoneNumber
    {
        const string USCountryCode = "1";

        bool _isInternational;
        private string _countryCode;
        private string _exchangeAndSuffix;

        public PhoneNumberViewModel()
        {
            this.As<INotifyPropertyChanged>().IfNotNull(npc => npc.PropertyChanged += OnPropertyChanged);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // force rebind of all properties on any property changed - this is required because of circular dependencies between
            // properties required for validation - area code may invalidate exchange/suffix and exchange/suffix may invalidate area code.
            if (e.PropertyName != string.Empty) this.As<IRaisePropertyChanged>().IfNotNull(rpc => rpc.OnPropertyChanged(new PropertyChangedEventArgs(string.Empty)));
        }

        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        public virtual string CountryCode
        {
            get { return !IsInternational ? USCountryCode : _countryCode; }
            set { _countryCode = value; }
        }

        [DataMember]
        public virtual bool IsInternational
        {
            get { return _isInternational; }
            set
            {
                _isInternational = value;
                if (!_isInternational)
                {
                    CountryCode = USCountryCode;
                }
            }
        }

        [DataMember]
        [RequiredIf("!IsInternational")]
        [RegularExpression(@"^(\d)*$", ErrorMessage = "Area Code must only use numbers [0-9]")]
        public virtual string AreaCode { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        [RegularExpression(@"^([0-9 \-]*)$", ErrorMessage = "Exchange and Suffix must only use numbers [0-9], and dashes [-]")]
        public virtual string ExchangeAndSuffix
        {
            get { return _exchangeAndSuffix; }
            set
                {
                var newValue = value;
                if (newValue != null && newValue.Length == 7 && !IsInternational)
                {
                    newValue = "{0}-{1}".FormatWith(newValue.Substring(0, 3), newValue.Substring(3));
                }

                _exchangeAndSuffix = newValue;
            }
        }

        /// <summary>
        /// Actual phone number without country code and extension
        /// </summary>
        /// <remarks>
        /// Depends on CountryCode for validation purposes
        /// </remarks>
        public virtual string PhoneNumber
        {
            get
            {
                var externalPhoneNumber = this.GetExternalLibPhoneNumber();
                return externalPhoneNumber.IfNotNull(x => x.NationalNumber.ToString(CultureInfo.InvariantCulture));
            }
        }

        [DataMember]
        public virtual string Extension { get; set; }

        [DataMember]
        public virtual bool IsPrimary { get; set; }

        [DataMember]
        [RequiredWithDisplayName]
        public virtual NamedViewModel PhoneType { get; set; }

        public override string ToString()
        {
            return this.GetFormattedValue() ?? "{0}{1}".FormatWith(PhoneNumber, Extension);
        }


        /// <summary>
        /// Validates Country Code for content (using a third party library).
        /// </summary>
        private class ValidCountryCodeAttribute : ValidationAttribute
        {
            private static readonly Dictionary<string, ValidationResult> ValidationCache = new Dictionary<string, ValidationResult>();

            public ValidCountryCodeAttribute() : base("Phone number is not well formed for the country code specified.") { }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var model = (PhoneNumberViewModel)validationContext.ObjectInstance;

                return ValidationCache.GetValue(model.CountryCode, () => IsValid(model));
            }

            private static ValidationResult IsValid(PhoneNumberViewModel model)
            {
                var phoneNumberUtility = PhoneNumberUtil.GetInstance();

                int countryCode;
                if (!int.TryParse(model.CountryCode, out countryCode)) return new ValidationResult("Country Code is not valid.", new[] { "CountryCode" });

                var region = phoneNumberUtility.GetRegionCodeForCountryCode(countryCode);
                if (!phoneNumberUtility.GetSupportedRegions().Contains(region))
                {
                    return new ValidationResult("Country Code is not recognized.", new[] { "CountryCode" });
                }

                return ValidationResult.Success;
            }
        }

        [DependsOn("AreaCode", "ExchangeAndSuffix", "PhoneType")]
        public virtual bool IsEmpty
        {
            get { return AreaCode.IsNullOrEmpty() && ExchangeAndSuffix.IsNullOrEmpty() && PhoneType == null; }
        }

        [DependsOn("IsEmpty")]
        public virtual bool IsValidationEnabled
        {
            get { return !IsEmpty; }
        }

        #region Dummy non-supported IPhoneNumber implementation

        int IPhoneNumber.Id { get; set; }

        IPhoneNumberType IPhoneNumber.PhoneNumberType { get; set; }

        int IHasOrdinalId.OrdinalId { get; set; }

        #endregion
    }

    public static class PhoneNumberViewModelExtensions
    {
        const string UnknownRegion = "ZZ";

        public static IMapper<T, PhoneNumberViewModel> CreatePhoneViewModelMapper<T>(this ICustomMapperFactory mapperFactory)
            where T : IPhoneNumber
        {
            return mapperFactory.CreateMapper<T, PhoneNumberViewModel>(
               i => new PhoneNumberViewModel
               {
                   Id = !Equals(i, default(T)) ? i.Id : (int?)null,
                   CountryCode = !Equals(i, default(T)) ? (i.CountryCode.IsNotNullOrEmpty() ? i.CountryCode : (!i.IsInternational ? "1" : string.Empty)) : string.Empty,
                   AreaCode = !Equals(i, default(T)) ? i.AreaCode.IsNotNullOrEmpty() ? i.AreaCode : string.Empty : string.Empty,
                   ExchangeAndSuffix = !Equals(i, default(T)) ? i.ExchangeAndSuffix.IsNotNullOrEmpty() ? i.ExchangeAndSuffix : string.Empty : string.Empty,
                   Extension = !Equals(i, default(T)) ? (i.Extension.IsNotNullOrEmpty() ? i.Extension : i.GetParsedExtension()) : string.Empty,
                   PhoneType = !Equals(i, default(T)) ? new NamedViewModel { Id = i.PhoneNumberType.Id, Name = i.PhoneNumberType.Name } : null,
                   IsPrimary = !Equals(i, default(T)) && i.OrdinalId == 1,
                   IsInternational = !Equals(i, default(T)) && i.IsInternational,
               }, false);
        }


        internal static string GetFormattedValue(this PhoneNumberViewModel phoneNumber)
        {
            var externalLibPhoneNumber = GetExternalLibPhoneNumber(phoneNumber);
            if (externalLibPhoneNumber == null) return null;

            var phoneNumberUtility = PhoneNumberUtil.GetInstance();
            var format = phoneNumber.IsInternational ? PhoneNumberFormat.NATIONAL : PhoneNumberFormat.INTERNATIONAL;
            return phoneNumberUtility.Format(externalLibPhoneNumber, format);
        }

        [DebuggerNonUserCode]
        internal static PhoneNumber GetExternalLibPhoneNumber(this PhoneNumberViewModel phoneNumber)
        {
            int countryCode;
            if (!int.TryParse(phoneNumber.CountryCode, out countryCode))
            {
                // Default country code to North America (US)
                countryCode = 1;
            }
            var phoneNumberUtility = PhoneNumberUtil.GetInstance();
            var region = phoneNumberUtility.GetRegionCodeForCountryCode(countryCode);

            // If region is unknown -> parsing will fail anyway
            if (region == UnknownRegion) return null;

            var phoneNumberString = "{0}{1}x{2}".FormatWith(phoneNumber.AreaCode, phoneNumber.ExchangeAndSuffix, phoneNumber.Extension);
            try
            {
                return phoneNumberUtility.Parse(phoneNumberString, region);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}