﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Common
{
    [DataContract]
    public class PostOperativeViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int PatientId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime EndDate { get; set; }

        public virtual bool CurrentlyInPostOperativePeriod
        {            
            get { return DateTime.Now.ToClientTime().Date < EndDate.Date; }
        }
    }
}
