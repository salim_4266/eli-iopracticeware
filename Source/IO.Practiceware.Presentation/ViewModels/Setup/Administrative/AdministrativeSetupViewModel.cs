﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Setup.Administrative
{

    [DataContract]
    public class AdministrativeDetails : NamedViewModel
    {
    
        [DataMember]
        public virtual ExtendedObservableCollection<StringAndIdViewModel> StringValues { get; set; }


        [DataMember]
        public virtual ExtendedObservableCollection<BoolAndIdViewModel> BooleanValues { get; set; }
        
        
        [DataMember]
        public virtual ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel> ForeignKeyValues { get; set; }

     
        [DataMember]
        public virtual ExtendedObservableCollection<BoolAndIdViewModel> OnOffValues { get; set; }

     
        [DataMember]
        public virtual bool CanDelete { get; set; }


    }

    [DataContract]
    public class AdministrativeSetupListViewModel : NamedViewModel
    {
      
        [DataMember]
        public virtual ExtendedObservableCollection<AdministrativeDetails> List { get; set; }

        
        [DataMember]
        public virtual bool IsLoaded { get; set; }

        [DataMember]
        public virtual List<string> DeletedItems { get; set; }
    }

    [DataContract]
    public class StringAndIdViewModel : NamedViewModel
    {
      
        [DataMember]
        public virtual String Value { get; set; }
    }

    [DataContract]
    public class CollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {
     
        [DataMember]
        public virtual ExtendedObservableCollection<NamedViewModel> Collection { get; set; }

    
        [DataMember]
        public virtual NamedViewModel SelectedItem { get; set; }
    }

    [DataContract]
    public class CollectionOfCollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {
    
        [DataMember]
        public virtual ObservableCollection<CollectionAndSelectedItemAndIdViewModel> Collection { get; set; }
    }

    [DataContract]
    public class BoolAndIdViewModel : NamedViewModel
    {
      
        [DataMember]
        public virtual bool Value { get; set; }
    }

}
