﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Soaf.Presentation;
using Soaf.Collections;
using Soaf.ComponentModel;
using IO.Practiceware.Presentation.ViewModels.Common;
using System.Collections.ObjectModel;


namespace IO.Practiceware.Presentation.ViewModels.Setup.Billing
{
    [DataContract]
    public class BillingSetupViewModel : IViewModel
    {

    }

    [DataContract]
    public enum OpenBalanceType
    {
        [EnumMember]
        [Display(Name = "Patient")]
        Patient,
        [EnumMember]
        [Display(Name = "Invoice")]
        Invoice,
        [EnumMember]
        [Display(Name = "Service")]
        Service,
    }

    [DataContract]
    public class BillingDetails : NamedViewModel
    {

        [DataMember]
        public virtual ExtendedObservableCollection<StringAndIdViewModel> StringValues { get; set; }


        [DataMember]
        public virtual ExtendedObservableCollection<BoolAndIdViewModel> BooleanValues { get; set; }


        [DataMember]
        public virtual ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel> ForeignKeyValues { get; set; }


        [DataMember]
        public virtual ExtendedObservableCollection<BoolAndIdViewModel> OnOffValues { get; set; }


        [DataMember]
        public virtual bool CanDelete { get; set; }


    }
    [DataContract]
    public class BillingSetupListViewModel : NamedViewModel
    {

        [DataMember]
        public virtual ExtendedObservableCollection<BillingDetails> List { get; set; }


        [DataMember]
        public virtual bool IsLoaded { get; set; }

        [DataMember]
        public virtual List<string> DeletedItems { get; set; }
    }
    [DataContract]
    public class StringAndIdViewModel : NamedViewModel
    {

        [DataMember]
        public virtual String Value { get; set; }
    }

    [DataContract]
    public class CollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {

        [DataMember]
        public virtual ExtendedObservableCollection<NamedViewModel> Collection { get; set; }


        [DataMember]
        public virtual NamedViewModel SelectedItem { get; set; }
    }

    [DataContract]
    public class CollectionOfCollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {

        [DataMember]
        public virtual ObservableCollection<CollectionAndSelectedItemAndIdViewModel> Collection { get; set; }
    }

    [DataContract]
    public class BoolAndIdViewModel : NamedViewModel
    {

        [DataMember]
        public virtual bool Value { get; set; }
    }

}
