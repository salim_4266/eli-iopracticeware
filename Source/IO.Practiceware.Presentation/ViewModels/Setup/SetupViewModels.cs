﻿
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.Setup
{

    [DataContract]
    public class ItemAndDetailsViewModel : NamedViewModel
    {
        [DataMember]
        public virtual ObservableCollection<BoolAndIdViewModel> BooleanValues { get; set; }

        /// <summary>
        /// Dev note: the only difference between the BooleanValues collection and the OnOffValuesCollection 
        /// is reflected in the user interface.  If there is a more elegant solution that allows for conditional
        /// styling on a per-object basis, please advise.
        /// </summary>
        [DataMember]
        public virtual ObservableCollection<BoolAndIdViewModel> OnOffValues { get; set; }

        [DataMember]
        public virtual ObservableCollection<StringAndIdViewModel> StringValues { get; set; }

        [DataMember]
        public virtual ObservableCollection<CollectionAndSelectedItemAndIdViewModel> ForeignKeyValues { get; set; }

        [DataMember]
        public virtual ObservableCollection<TwoManyToManyViewModel> TwoManyToManyValues { get; set; }

        [DataMember]
        public virtual bool CanDelete { get; set; }

        [DataMember]
        public virtual bool CanArchive { get; set; }

        [DataMember]
        public virtual bool CanReorder { get; set; }
    }

    [DataContract]
    public class TwoManyToManyViewModel : NamedViewModel
    {
        [DataMember]
        public virtual ObservableCollection<TwoCollectionAndSelectedItemAndIdViewModel> LinkedObjects { get; set; }

    }

    [DataContract]
    public class TwoCollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Collection1 { get; set; }

        [DataMember]
        public virtual ObservableCollection<NamedViewModel> Collection2 { get; set; }

        [DataMember]
        public virtual object SelectedItem1 { get; set; }

        [DataMember]
        public virtual object SelectedItem2 { get; set; }
    }

    [DataContract]
    public class SetupListViewModel : NamedViewModel
    {
        [DataMember]
        public virtual ObservableCollection<ItemAndDetailsViewModel> List { get; set; }
    }

    [DataContract]
    public class BoolAndIdViewModel : NamedViewModel
    {
        public virtual bool Value { get; set; }
    }

    [DataContract]
    public class StringAndIdViewModel : NamedViewModel
    {
        public virtual String Value { get; set; }
    }

    [DataContract]
    public class CollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {
        public virtual ObservableCollection<NamedViewModel> Collection { get; set; }

        public virtual object SelectedItem { get; set; }
    }

    [DataContract]
    public class CollectionOfCollectionAndSelectedItemAndIdViewModel : NamedViewModel
    {
        public virtual ObservableCollection<CollectionAndSelectedItemAndIdViewModel> Collection { get; set; }
    }
}
