﻿using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.RescheduleAppointment
{
    [DataContract]
    public class RescheduleAppointmentViewModel
    {
        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual int? AppointmentTypeId { get; set; }

        [DataMember]
        public virtual int ResourceId { get; set; }

        [DataMember]
        public virtual int LocationId { get; set; }

        [DataMember]
        public virtual Guid? ScheduleBlockCategoryId { get; set; }
    }
}