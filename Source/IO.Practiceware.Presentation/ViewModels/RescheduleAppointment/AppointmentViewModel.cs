﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.RescheduleAppointment
{
    [DataContract]
    public class AppointmentViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string PatientName { get; set; }
    }
}
