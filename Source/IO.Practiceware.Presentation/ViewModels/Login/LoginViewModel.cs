﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace IO.Practiceware.Presentation.ViewModels.Login
{
    [SupportsDataErrorInfo]
    public class LoginViewModel : IViewModel
    {
        [Required(ErrorMessage = "You must enter your ID.")]
        public virtual string PersonalId { get; set; }

        public virtual SqlConnectionStringBuilder SqlConnectionString { get; set; }

        [DependsOn("SqlConnectionString")]
        [RequiredIf("IsSqlServerChecked", ErrorMessage = "Please provide a database name")]
        public virtual string DatabaseName
        {
            get
            {
                return SqlConnectionString != null ? SqlConnectionString.InitialCatalog : null;
            }
            set
            {
                if (SqlConnectionString == null)
                {
                    SqlConnectionString = new SqlConnectionStringBuilder(ConfigurationManager.DefaultConnectionStringFormat);
                }
                SqlConnectionString.InitialCatalog = value;
            }
        }

        [RequiredIf("IsApplicationServerChecked", ErrorMessage = "You must provide a token")]
        public virtual string ApplicationServerToken { get; set; }

        [DependsOn("IsApplicationServerChecked", "IsSqlServerChecked")]
        [Required(ErrorMessage = "Please provide a server name")]
        public virtual string ServerName
        {
            get
            {
                if (IsApplicationServerChecked)
                {
                    return ApplicationServerName;
                }

                if (IsSqlServerChecked && SqlConnectionString != null)
                {
                    return SqlConnectionString.DataSource;
                }

                return null;
            }
            set
            {
                if (IsApplicationServerChecked)
                {
                    ApplicationServerName = value;
                    return;
                }

                SqlConnectionString.DataSource = value;
            }
        }

        public virtual string ServerDataPath { get; set; }
        public virtual string ApplicationServerName { get; set; }
        public virtual bool RemoteFileService { get; set; }

        private bool _isApplicationServerChecked;

        [DependsOn("IsSqlServerChecked")]
        public virtual bool IsApplicationServerChecked
        {
            get { return _isApplicationServerChecked; }
            set
            {
                _isSqlServerChecked = !value;
                _isApplicationServerChecked = value;
            }
        }

        private bool _isSqlServerChecked;

        [DependsOn("IsApplicationServerChecked")]
        public virtual bool IsSqlServerChecked
        {
            get { return _isSqlServerChecked; }
            set
            {
                if (value && SqlConnectionString == null)
                {
                    SqlConnectionString = new SqlConnectionStringBuilder(ConfigurationManager.DefaultConnectionStringFormat);
                }

                _isApplicationServerChecked = !value;
                _isSqlServerChecked = value;
            }
        }

        public virtual bool HideAdvancedOptions
        {
            get
            {
                return ConfigurationManager.ClientApplicationConfiguration.HideAdvancedOptions;
            }
        }
    }

}
