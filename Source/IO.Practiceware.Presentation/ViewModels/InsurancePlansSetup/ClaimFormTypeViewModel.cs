﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    public class ClaimFormTypeViewModel : NamedViewModel
    {
        [DataMember]
        public virtual bool IsElectronic { get; set; }
    }
}