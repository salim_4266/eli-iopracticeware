﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    [EditableObject]
    public class InsurerClaimFormViewModel : IViewModel
    {
        [DataMember]
        [Required]
        public virtual ClaimFormTypeViewModel FormType { get; set; }

        [DataMember]
        [Required]
        public virtual NamedViewModel InvoiceType { get; set; }

        [DataMember]
        public virtual int InsurerId { get; set; }

        public override bool Equals(object obj)
        {
            return Objects.DataMembersEqual(this, obj);
        }

        public override int GetHashCode()
        {
            return Objects.GetDataMembersHashCode(this);
        }
    }
}