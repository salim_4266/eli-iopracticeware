﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class InsurerDoctorAssignmentViewModel : IViewModel
    {
        public InsurerDoctorAssignmentViewModel()
        {
            // Set defaults
            AcceptAssignment = true;
            SuppressPayments = false;
        }

        [DataMember]
        public virtual NamedViewModel Doctor { get; set; }

        [DataMember]
        public virtual bool AcceptAssignment { get; set; }

        [DataMember]
        public virtual bool SuppressPayments { get; set; }

        public bool CanSupressPayments
        {
            get { return PermissionId.ManageInsurancePlanPatientPayments.PrincipalContextHasPermission(); }
        }

        /// <summary>
        /// Returns whether current doctor assignment represents a default assignment
        /// </summary>
        public virtual bool IsDefault {
            get { return AcceptAssignment && !SuppressPayments; }
        }

        #region Grid performance improvements

        [DependsOn("Doctor")]
        [IgnoreChangeTracking]
        public virtual string DoctorName
        {
            get { return Doctor.IfNotNull(p => p.Name); }
            set { }
        }

        #endregion
    }
}
