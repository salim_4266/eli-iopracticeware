﻿using System.Collections.ObjectModel;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    public class ManageInsurancePlansLoadInformation
    {
        public ManageInsurancePlansLoadInformation()
        {
            MatchedInsurancePlans = new Collection<InsurancePlanToManageViewModel>();
            PlanTypes = new Collection<NamedViewModel>();
            States = new Collection<StateOrProvinceViewModel>();
            PhoneTypes = new Collection<NamedViewModel>();
            BusinessClasses = new Collection<NamedViewModel>();
            ClaimFileIndicators = new Collection<NamedViewModel>();
            ClaimFileReceivers = new Collection<NamedViewModel>();
            ClaimFormTypes = new Collection<ClaimFormTypeViewModel>();
            InvoiceTypes = new Collection<NamedViewModel>();
            Doctors = new Collection<NamedViewModel>();
        }

        [DataMember]
        public virtual ICollection<InsurancePlanToManageViewModel> MatchedInsurancePlans { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> PlanTypes { get; set; }

        [DataMember]
        public virtual ICollection<StateOrProvinceViewModel> States { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> PhoneTypes { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> BusinessClasses { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> ClaimFileIndicators { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> ClaimFileReceivers { get; set; }

        [DataMember]
        public virtual ICollection<ClaimFormTypeViewModel> ClaimFormTypes { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> InvoiceTypes { get; set; }

        [DataMember]
        public virtual ICollection<NamedViewModel> Doctors { get; set; }

        [DataMember]
        public virtual ICollection<DiagnosisType> DiagnosisCodeSet { get; set; }
    }
}
