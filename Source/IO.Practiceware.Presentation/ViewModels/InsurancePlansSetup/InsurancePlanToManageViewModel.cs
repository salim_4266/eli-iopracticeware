﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Validation;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class InsurancePlanToManageViewModel : IViewModel
    {
        readonly Func<InsurerClaimFormViewModel> _createInsurerClaimForm;
        readonly Func<PhoneNumberViewModel> _createPhoneNumber;
        readonly Func<PostalCodeViewModel> _createPostalCode;

        public InsurancePlanToManageViewModel()
        {
        }

        public InsurancePlanToManageViewModel(
            Func<InsurerClaimFormViewModel> createInsurerClaimForm,
            Func<PhoneNumberViewModel> createPhoneNumber,
            Func<PostalCodeViewModel> createPostalCode)
        {
            _createInsurerClaimForm = createInsurerClaimForm;
            _createPhoneNumber = createPhoneNumber;
            _createPostalCode = createPostalCode;
        }

        /// <summary>
        /// The Id of Insurance Plan. <c>null</c> for new entries
        /// </summary>
        [DispatcherThread]
        [DataMember]
        public virtual int? Id { get; set; }

        /// <summary>
        /// Temporary Id for new plans. <c>null</c> for existing items
        /// </summary>
        [DispatcherThread]
        [DataMember]
        public virtual Guid? NewPlanId { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Insurer Name is required")]
        public virtual string InsuranceName { get; set; }

        [DataMember]
        public virtual string PlanName { get; set; }

        [DataMember]
        public virtual string InsurerAddressLine1 { get; set; }

        [DataMember]
        public virtual string InsurerAddressLine2 { get; set; }

        [DataMember]
        public virtual string InsurerAddressLine3 { get; set; }

        [DataMember]
        public virtual string InsurerCity { get; set; }

        [DataMember]
        public virtual StateOrProvinceViewModel InsurerState { get; set; }

        [DataMember]
        public virtual PostalCodeViewModel InsurerPostalCode
        {
            get { return _insurerPostalCode ?? (_insurerPostalCode = _createPostalCode()); }
            set
            {
                _insurerPostalCode = value;
            }
        }

        private PostalCodeViewModel _insurerPostalCode;

        [DataMember]
        public virtual NamedViewModel PlanType { get; set; }

        /// <summary>
        /// Claim file indicator
        /// </summary>
        [DataMember]
        public virtual NamedViewModel Cfic { get; set; }

        [DataMember]
        [RequiredIfNotNull("Receiver",
            AllowEmptyStrings = false,
            ErrorMessage = "Payer ID is required when Receiver is set")]
        public virtual string PayerId { get; set; }

        [DataMember]
        public virtual string Hpid { get; set; }

        [DataMember]
        public virtual string Oeid { get; set; }

        [DataMember]
        public virtual NamedViewModel Receiver { get; set; }

        [DataMember]
        public virtual string MedigapId { get; set; }

        [DataMember]
        public virtual NamedViewModel BusinessClass { get; set; }

        [DataMember]
        public virtual string Website { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual bool MedicareAdvantage { get; set; }

        /// <summary>
        /// Policy number formats are defined by entering one of the following for each possible character:
        //A - Alpha [a-z, A-Z]
        //N - Number [0-9]
        //E - Either alpha or number [a-z, A-Z, 0-9]
        //O - Optional character [a-z, A-Z, 0-9]
        /// </summary>
        [DataMember]
        [RegexMatch("[ANEO]+",
            SuccessOnNullOrEmptyInput = true,
            ErrorMessage = "Only 'A','N','E' and 'O' characters allowed for Policy number format")]
        public virtual string PolicyNumberFormat { get; set; }

        /// <summary>
        /// Specifies whether plan can be used by dependent
        /// Validation: unchecking this option on UI triggers a check on the server whether plan is used already by dependents
        /// </summary>
        [DataMember, DispatcherThread]
        public virtual bool CanBeUsedByDependent { get; set; }

        [DataMember]
        public virtual bool ReferralRequired { get; set; }

        [DataMember]
        public virtual bool PaperClaimIfNotPrimary { get; set; }

        [DataMember]
        public virtual bool AllowZeroAmount { get; set; }

        [DataMember]
        public virtual bool DisplayICDTenDecimal { get; set; }
        
        [DataMember]
        public virtual bool DisplayCLIAOnClaims { get; set; }

        [DataMember]
        public virtual bool DisplayTaxonomyOnClaims { get; set; }

        /// <summary>
        /// Archived state of the plan. Ignored from change tracking as it cannot be cancelled.
        /// </summary>
        [DataMember, DispatcherThread]
        [IgnoreChangeTracking]
        public virtual bool IsArchived { get; set; }

        /// <summary>
        /// Phone numbers
        /// </summary>
        /// <remarks>
        /// Will also validate each phone number
        /// </remarks>
        [DataMember, Dependency]
        [ValidateObject]
        public virtual ExtendedObservableCollection<PhoneNumberViewModel> InsurerPhones { get; set; }

        /// <summary>
        /// Doctor assignments
        /// </summary>
        [DataMember, Dependency]
        [ValidateObject]
        public virtual ExtendedObservableCollection<InsurerDoctorAssignmentViewModel> DoctorAssignments { get; set; }

        [DataMember, Dependency]
        public virtual NamedViewModel SelectedDiagnosisCodeSet { get; set; }

        #region Separately fetched

        /// <summary>
        /// Change history. Not marked with <see cref="DataMember"/> and excluded from change tracking, since used client-side only
        /// </summary>
        [DispatcherThread]
        [IgnoreChangeTracking]
        public virtual List<AuditEntryViewModel> ChangeHistory { get; set; }

        /// <summary>
        /// A state, indicating whether claim crossover has been loaded
        /// </summary>
        [DataMember, DispatcherThread]
        [IgnoreChangeTracking]
        public virtual bool IsClaimCrossoversLoaded { get; set; }

        /// <summary>
        /// Receiving claim crossover
        /// </summary>
        /// <remarks>
        /// Skipping recursive validation, since items themselves are not edited
        /// </remarks>
        [DataMember, Dependency, DispatcherThread]
        public virtual ExtendedObservableCollection<InsurancePlanToManageViewModel> ClaimCrossovers { get; set; }

        /// <summary>
        /// A state, indicating whether insurer claim forms were loaded
        /// </summary>
        [DataMember, DispatcherThread]
        [IgnoreChangeTracking]
        public virtual bool IsInsurerClaimFormsLoaded { get; set; }

        /// <summary>
        /// Contains claim forms configuration. Only distinct values will be saved
        /// </summary>
        /// <remarks>
        /// Will validate also entered claim form values
        /// </remarks>
        [DataMember, Dependency, DispatcherThread]
        [ValidateObject]
        public virtual ObservableSet<InsurerClaimFormViewModel> InsurerClaimForms { get; set; }

        #endregion

        #region UI specific

        [DispatcherThread]
        [IgnoreChangeTracking]
        public virtual bool IsShowingDetails { get; set; }

        [DispatcherThread]
        [IgnoreChangeTracking]
        public virtual ObservableCollection<InsurancePlanToManageViewModel> ClaimCrossoversToAdd { get; set; }

        #endregion

        #region Calculated

        /// <summary>
        /// Returns whether plan can be edited
        /// </summary>
        [DependsOn("Id", "IsArchived")]
        public virtual bool CanEditPlan
        {
            get
            {
                return (!IsArchived && PermissionId.EditInsurancePlanInformation.PrincipalContextHasPermission()) // Not archived and editable
                    || (!Id.HasValue && PermissionId.CreateInsurancePlans.PrincipalContextHasPermission());  // New ones for users with create plans permission
            }
        }

        /// <summary>
        /// Returns whether plan cannot be edited at this time
        /// </summary>
        [DependsOn("Id", "IsArchived")]
        public virtual bool IsPlanReadOnly
        {
            get { return !CanEditPlan; }
        }

        /// <summary>
        /// Returns whether plan can be reactivated
        /// </summary>
        [DependsOn("IsArchived")]
        public virtual bool CanReactivatePlan
        {
            get { return IsArchived && PermissionId.ReactivateInsurancePlans.PrincipalContextHasPermission(); }
        }

        /// <summary>
        /// Returns whether plan "CanBeUsedByDependent" property can be changed
        /// </summary>
        public virtual bool CanModifyPlanPatientPayments
        {
            get { return PermissionId.ManageInsurancePlanPatientPayments.PrincipalContextHasPermission(); }
        }

        /// <summary>
        /// Insurer Id tooltip on rows
        /// </summary>
        [DependsOn("Id")]
        public virtual string InsurerIdText
        {
            get { return string.Format("Insurer ID #: {0}", Id); }
        }

        /// <summary>
        /// This field is used when editing phone within the grid. Automatically adds an entry if no found
        /// </summary>
        [DependsOn("InsurerPhones")]
        public virtual PhoneNumberViewModel PrimaryInsurerPhoneForEdit
        {
            get
            {
                if (InsurerPhones.Count <= 0)
                {
                    var newItem = _createPhoneNumber();
                    InsurerPhones.Add(newItem);
                }
                return InsurerPhones.First();
            }
        }

        InsurerClaimFormViewModel _insurerClaimFormToAdd;

        /// <summary>
        /// Used to setup insurer claim form before adding it
        /// </summary>
        [DependsOn("InsurerClaimForms")]
        public virtual InsurerClaimFormViewModel InsurerClaimFormToAdd
        {
            get
            {
                // ReInit new templates whenever it is actually added to Insurer Claim Forms collection
                if (_insurerClaimFormToAdd == null
                    || InsurerClaimForms.IfNotNull(p => p.Any(x => ReferenceEquals(x, _insurerClaimFormToAdd)), true))
                {
                    _insurerClaimFormToAdd = _createInsurerClaimForm();
                }

                return _insurerClaimFormToAdd;
            }
        }

        PhoneNumberViewModel _phoneNumberToAdd;

        /// <summary>
        /// Used to setup phone number before adding it
        /// </summary>
        [DependsOn("InsurerPhones")]
        public virtual PhoneNumberViewModel PhoneNumberToAdd
        {
            get
            {
                // ReInit new templates whenever it is actually added to Insurer Phones collection
                if (_phoneNumberToAdd == null
                    || InsurerPhones.IfNotNull(p => p.Any(x => ReferenceEquals(x, _phoneNumberToAdd)), true))
                {
                    _phoneNumberToAdd = _createPhoneNumber();
                }

                return _phoneNumberToAdd;
            }
        }

        #region Grid performance improvement (direct property access is faster than sub properties)

        /// <summary>
        /// Primary phone for display
        /// </summary>
        /// <remarks>
        /// Excluded from change tracking, since has setter
        /// </remarks>
        [DependsOn("InsurerPhones")]
        [IgnoreChangeTracking]
        public virtual PhoneNumberViewModel PrimaryInsurerPhone
        {
            get { return InsurerPhones.FirstOrDefault(); }
            set
            {
                // Fake setter to allow editing in the grid
            }
        }

        [DependsOn("PlanType")]
        [IgnoreChangeTracking]
        public virtual string PlanTypeName
        {
            get { return PlanType.IfNotNull(p => p.Name); }
            set { }
        }

        [DependsOn("BusinessClass")]
        [IgnoreChangeTracking]
        public virtual string BusinessClassName
        {
            get { return BusinessClass.IfNotNull(p => p.Name); }
            set { }
        }

        [DependsOn("Cfic")]
        [IgnoreChangeTracking]
        public virtual string CficName
        {
            get { return Cfic.IfNotNull(p => p.Name); }
            set { }
        }

        [DependsOn("InsurerState")]
        [IgnoreChangeTracking]
        public virtual string InsurerStateName
        {
            get { return InsurerState.IfNotNull(p => p.Abbreviation); }
            set { }
        }

        [DependsOn("Receiver")]
        [IgnoreChangeTracking]
        public virtual string ReceiverName
        {
            get { return Receiver.IfNotNull(p => p.Name); }
            set { }
        }

        #endregion

        #endregion
    }
}
