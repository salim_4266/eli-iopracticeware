﻿using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup
{
    [DataContract]
    [EditableObject]
    public class PayerMappingViewModel : IViewModel
    {
        [DataMember]
        public virtual string OutgoingPayerCode { get; set; }

        [DataMember]
        public virtual string IncomingPayerCode { get; set; }
    }
}