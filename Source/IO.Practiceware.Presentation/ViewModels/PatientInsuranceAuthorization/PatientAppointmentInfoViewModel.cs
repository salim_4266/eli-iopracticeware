﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorization
{
    [DataContract]
    public class PatientAppointmentInfo : IViewModel
    {
        [DataMember]
        public virtual string PatientName { get; set; }

        [DataMember]
        public virtual string PatientId { get; set; }

        [DataMember]
        public virtual string DateOfBirth { get; set; }

        [DataMember]
        public virtual string PatientType { get; set; }

        [DataMember]
        public virtual string AppointmentDate { get; set; }

        [DataMember]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

        [DataMember]
        public virtual string Resource { get; set; }

        [DataMember]
        public virtual string HomePhone { get; set; }

        [DataMember]
        public virtual string WorkPhone { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel<int>> Insurances { get; set; }
    }
}
