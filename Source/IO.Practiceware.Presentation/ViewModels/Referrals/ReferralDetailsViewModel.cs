﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Referrals
{
    [SupportsDataErrorInfo]
    [DataContract]
    [EditableObject]
    public class ReferralDetailsViewModel : IViewModel
    {
        private ObservableCollection<NameAndAbbreviationViewModel> _insurances;

        [DataMember, DispatcherThread]
        public virtual int? ReferralId { get; set; }

        [DataMember, DispatcherThread]
        public virtual int PatientId { get; set; }

        [DataMember, DispatcherThread]
        public virtual string PatientName { get; set; }

        [DataMember, DispatcherThread]
        [Required(ErrorMessage = "Please enter a Referral Number")]
        public virtual string ReferralNumber { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select an Insurance")]
        public virtual int? InsuranceId { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a Resource")]
        public virtual int? ExternalProviderId { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a Resource")]
        public virtual int? ReferredToDoctorId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ReferredToDoctorName { get; set; }

        [DispatcherThread]
        public virtual string InsuranceName
        {
            get
            {
                if (Insurances.IsNullOrEmpty()) return String.Empty;
                var insurance = Insurances.FirstOrDefault(x => InsuranceId.HasValue && x.Id == InsuranceId.Value);
                return insurance == null ? String.Empty : insurance.Name;
            }
        }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select a Start Date")]
        public virtual DateTime? StartDate { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select an Expiration Date")]
        public virtual DateTime? ExpiresDate { get; set; }

        [DataMember]
        [DispatcherThread]
        [Required(ErrorMessage = "Please select the Total Number of Visits")]
        public virtual int? TotalNumberOfVisits { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int AlreadyVisited { get; set; }

        [DispatcherThread]
        public virtual int? VisitsRemaining
        {
            get { return TotalNumberOfVisits - AlreadyVisited; }
        }

        [DispatcherThread]
        public virtual string TotalVisitsAndVisitsRemaining
        {
            get { return string.Format("{0}/{1}", VisitsRemaining, TotalNumberOfVisits); }
        }

        [DataMember]
        [DispatcherThread]
        [StringLength(1000)]
        public virtual string Comments { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NameAndAbbreviationViewModel> Insurances
        {
            get { return _insurances; }
            set
            {
                _insurances = value;
                if (value != null && value.Count == 1) InsuranceId = value.First().Id;
            }
        }
    }
}
