﻿using System;
using System.Collections.ObjectModel;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanDocument
{
    [DataContract]
    public class ScanSettingsViewModel : IViewModel
    {
        [DataMember]
        public virtual ObservableCollection<String> ScannerNames { get; set; }

        [DataMember]
        public virtual String SelectedScannerName { get; set; }

        [DataMember]
        public virtual ColorSetting ColorSetting { get; set; }

        [DataMember]
        public virtual DpiSetting DpiSetting { get; set; }

        [DataMember]
        public virtual bool IsDuplexMode { get; set; }

        [DataMember]
        public virtual PageSize PageSize { get; set; }

        public virtual long Quality
        {
            get { return PageSize == PageSize.Card ? 30 : 90; }
        }
    }
}
