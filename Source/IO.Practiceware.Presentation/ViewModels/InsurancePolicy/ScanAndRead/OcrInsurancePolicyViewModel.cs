﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanAndRead
{
    [DataContract]
    public class OcrInsurancePolicyViewModel : IViewModel
    {
        [DataMember]
        public virtual string PolicyholderName { get; set; }

        [DataMember]
        public virtual NamedViewModel PolicyholderRelationshipType { get; set; }

        [DataMember]
        public virtual string InsuranceName { get; set; }

        [DataMember]
        public virtual string PlanName { get; set; }

        [DataMember]
        public virtual NamedViewModel PlanType { get; set; }

        [DataMember]
        public virtual string PolicyCode { get; set; }

        [DataMember]
        public virtual string GroupCode { get; set; }

        [DataMember]
        public virtual decimal? CoPay { get; set; }

        [DataMember]
        public virtual decimal? Deductible { get; set; }

        [DataMember]
        public virtual DateTime? StartDate { get; set; }

        [DataMember]
        public virtual DateTime? EndDate { get; set; }
    }
}
