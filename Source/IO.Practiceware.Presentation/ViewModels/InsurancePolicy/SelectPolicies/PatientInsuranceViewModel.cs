﻿using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SelectPolicies
{
    [EditableObject]
    [DataContract]
    public class PatientInsuranceViewModel : IViewModel
    {
        [DataMember]
        public virtual long Id { get; set; }

        [DataMember]
        public virtual string InsurancePlan { get; set; }

        [DataMember]
        public virtual string PlanType { get; set; }

        [DataMember]
        public virtual string PolicyNumber { get; set; }

        [DataMember]
        public virtual string GroupNumber { get; set; }

        [DataMember]
        public virtual decimal? CoPay { get; set; }

        [DataMember]
        public virtual decimal? Deductible { get; set; }

        [DataMember]
        public virtual DateTime StartDate { get; set; }

        [DataMember]
        public virtual DateTime? EndDate { get; set; }

        [DataMember]
        public virtual InsuranceType InsuranceType { get; set; }

        [DependsOn("InsuranceType")]
        public virtual string InsuranceTypeName
        {
            get { return InsuranceType.GetDisplayName(); }
        }

        [DataMember]
        public virtual int Rank { get; set; }

        [DataMember]
        public virtual bool IsLinked { get; set; }

        [DataMember]
        public virtual bool IsDeleted { get; set; }

        [DataMember]
        public virtual int InsurancePolicyId { get; set; }
    }
}
