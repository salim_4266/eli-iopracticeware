﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SearchInsurancePlan
{
    [DataContract]
    public class InsurerViewModel : NamedViewModel
    {
        [DataMember]
        public virtual string PlanName { get; set; }

        [DataMember]
        public virtual string PlanType { get; set; }

        [DataMember]
        public virtual string PayerCode { get; set; }

        [DataMember]
        public virtual string PhoneNumber { get; set; }

        [DataMember]
        public virtual AddressViewModel Address { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        public virtual bool IsReferralRequired { get; set; }
    }

}
