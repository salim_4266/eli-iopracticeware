﻿using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.EnterPolicyDetails
{
    [SupportsDataErrorInfo]
    [DataContract]
    public class InsurancePolicyViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [Expression("IsValidPolicyCode", ErrorMessageExpression = "string.Format(\"Policy # does not match Policy Number Format {0}\", instance.PolicyNumberFormat)"), Required, DataMember]
        [DispatcherThread]
        public virtual string PolicyCode { get; set; }

        [DataMember]
        public virtual string GroupCode { get; set; }

        [DataMember]
        public virtual decimal? CoPay { get; set; }

        [DataMember]
        public virtual decimal? Deductible { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Start Date is required.")]
        public virtual DateTime? StartDate { get; set; }

        [DataMember]
        [EqualToGreaterThan("StartDate", ErrorMessage = "End Date must be greater then the Start Date.")]
        public virtual DateTime? EndDate { get; set; }

        [DataMember]
        public virtual string PolicyNumberFormat { get; set; }

        public virtual bool IsValidPolicyCode
        {
            get
            {
                if (string.IsNullOrEmpty(PolicyNumberFormat)) return true;
                var policyNumberPattern = string.Format("^{0}", PolicyNumberFormat.Replace("N", "([0-9])"));
                policyNumberPattern = policyNumberPattern.Replace("A", "([a-zA-Z])");
                policyNumberPattern = policyNumberPattern.Replace("E", "([0-9]|[a-zA-Z])");
                policyNumberPattern = string.Format("{0}$", policyNumberPattern.Replace("O", "([0-9]|[a-zA-Z]{0,1})"));
                return Regex.IsMatch(PolicyCode, policyNumberPattern);
            }
        }

        [DataMember]
        public virtual int PolicyholderId { get; set; }

        [DataMember]
        public virtual int InsurerId { get; set; }
    }
}
