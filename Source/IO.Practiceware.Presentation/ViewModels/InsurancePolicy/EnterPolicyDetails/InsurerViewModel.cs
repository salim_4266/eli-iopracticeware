﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy.EnterPolicyDetails
{
    [DataContract]
    public class BasicInsurerViewModel : NamedViewModel
    {
        [DataMember]
        public virtual string PlanName { get; set; }

        public override string ToString()
        {
            return "{0} {1}".FormatWith(Name, PlanName);
        }
    }
}
