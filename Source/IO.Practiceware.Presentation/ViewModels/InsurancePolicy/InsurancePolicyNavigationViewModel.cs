﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanAndRead;
using IO.Practiceware.Presentation.ViewsModels.NonClinicalPatient;
using Soaf.Presentation;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.InsurancePolicy
{
    [DataContract]
    public class InsurancePolicyNavigationViewModel : IViewModel
    {
        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual InsuranceType InsuranceType { get; set; }

        [DataMember]
        public virtual PolicyHolderRelationshipType RelationshipType { get; set; }

        [DataMember]
        public virtual int? PolicyholderId { get; set; }

        [DataMember]
        public virtual int? InsurerId { get; set; }

        [DataMember]
        public virtual IEnumerable<byte[]> ScannedDocuments { get; set; }

        [DataMember]
        public virtual OcrInsurancePolicyViewModel OcrResult { get; set; }

        [DataMember]
        public virtual bool PolicySuccessfullyCreated { get; set; }

        public PatientRelationshipType PatientRelationshipType { get { return PatientRelationshipType.Policyholder; } }
    }    
}
