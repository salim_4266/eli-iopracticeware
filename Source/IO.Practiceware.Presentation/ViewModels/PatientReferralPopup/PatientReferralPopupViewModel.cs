﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientReferralPopup
{
    [DataContract]
    public class ReferralViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual int Id { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual List<int> InvoiceReceivableIds { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual List<int> EncounterIds { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime? EndDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string ReferralCode { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int EncountersRemaining { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int TotalEncountersCovered { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ResourceViewModel Resource { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual NamedViewModel Patient { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual InsuranceViewModel Insurance { get; set; }

        [DispatcherThread]
        public virtual string ToolTip
        {
            get { return EncountersRemaining == 0 ? "Cannot attach referral with zero remaining visits" : null; }
        }
    }
}