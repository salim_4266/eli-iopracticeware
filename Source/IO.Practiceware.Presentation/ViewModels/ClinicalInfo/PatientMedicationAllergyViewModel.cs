﻿using System;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalInfo
{
    [DataContract]
    [EditableObject]
    public class PatientMedicationAllergyViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual NamedViewModel Medication { get; set; }

        [DataMember]
        public virtual NamedViewModel ReactionType { get; set; }

        [DataMember]
        public virtual NamedViewModel Severity { get; set; }

        [DataMember]
        public virtual NamedViewModel Status { get; set; }

        [DataMember]
        public virtual DateTime? OnsetDate { get; set; }

        [DataMember]
        public virtual DateTime? ResolutionDate { get; set; }

        [DataMember]
        public virtual bool IsDeactivated { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel SourceType { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel LoggedBy { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual DateTime LoggedDate { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel LastModifiedBy { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual DateTime? LastModifiedDate { get; set; }
    }
}
