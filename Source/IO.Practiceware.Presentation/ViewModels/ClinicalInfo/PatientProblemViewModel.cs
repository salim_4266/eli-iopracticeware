﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalInfo
{
    [DataContract]
    [EditableObject]
    [SupportsDataErrorInfo]
    public class PatientProblemViewModel : IViewModel
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual NamedViewModel Laterality { get; set; }

        [DataMember]
        public virtual NamedViewModel Location { get; set; }

        [DataMember]
        public virtual NamedViewModel Severity { get; set; }

        [DataMember]
        public virtual NamedViewModel Chronicity { get; set; }

        [DataMember]
        [Required]
        public virtual NamedViewModel Status { get; set; }

        [DataMember]
        public virtual bool IsDeactivated { get; set; }

        /// <summary>
        /// Clinical condition
        /// </summary>
        [DataMember]
        [Required]
        public virtual NamedViewModel Description { get; set; }

        [DataMember]
        public virtual DateTime? OnsetDate { get; set; }

        [DataMember]
        public virtual DateTime? ResolutionDate { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel SourceType { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel LoggedBy { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual DateTime LoggedDate { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual NamedViewModel LastModifiedBy { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Quick summary suitable for ToolTip
        /// TODO: why there is no mentioning of this in the spec?
        /// </summary>
        public virtual string QuickSummary
        {
            get
            {
                var result = string.Format("Noted on {0} by {1}\r\nLast modified on {2} by {3}", LoggedDate, LoggedBy, LastModifiedDate, LastModifiedBy);
                return result;
            }
        }

        [DataMember]
        public virtual bool IsEncounterDiagnosis { get; set; }

        /// <summary>
        /// Note: readonly
        /// </summary>
        [DataMember]
        public virtual String AxisQualifier { get; set; }

    }
}
