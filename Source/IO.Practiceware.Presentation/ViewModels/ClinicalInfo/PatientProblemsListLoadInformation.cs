﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalInfo
{
    [DataContract]
    public class PatientProblemsListLoadInformation
    {
        [DataMember]
        [Dependency]
        public virtual IList<PatientProblemViewModel> Problems { get; set; }

        [DataMember]
        [Dependency]
        public virtual IList<NamedViewModel> StatusValues { get; set; }

        [DataMember]
        [Dependency]
        public virtual IList<NamedViewModel> SeverityValues { get; set; }

        [DataMember]
        [Dependency]
        public virtual IList<NamedViewModel> LateralityValues { get; set; }

        [DataMember]
        [Dependency]
        public virtual IList<NamedViewModel> LocationValues { get; set; }

        [DataMember]
        [Dependency]
        public virtual IList<NamedViewModel> ChronicityValues { get; set; }
    }
}
