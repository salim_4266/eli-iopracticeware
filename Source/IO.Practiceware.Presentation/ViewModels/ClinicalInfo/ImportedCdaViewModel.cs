﻿using System;
using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.ClinicalInfo
{
    [DataContract]
    public class ImportedCdaViewModel : IViewModel
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual string Doctor { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }
    }
}
