﻿using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.CancelAppointment
{
    [DataContract]
    public class CancelAppointmentViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual string PatientName { get; set; }

        [DataMember]
        public virtual bool IsCancelled { get; set; }

        [DataMember]
        public virtual int EncounterStatusId { get; set; }

        [DataMember]
        public virtual int? EncounterStatusChangedReasonId { get; set; }

        [DataMember]
        public virtual string EncounterStatusChangedComment { get; set; }
    }
}