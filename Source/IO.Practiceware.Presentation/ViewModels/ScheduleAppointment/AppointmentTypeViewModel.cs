﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleAppointment
{
    [DataContract]
    public class AppointmentTypeViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual int CategoryId { get; set; }

        [DataMember]
        public virtual bool IsOrderRequired { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

}