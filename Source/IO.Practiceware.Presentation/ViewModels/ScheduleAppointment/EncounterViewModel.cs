﻿using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleAppointment
{
    [DataContract]
    public class EncounterViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<ReferralViewModel> Referrals { get; set; }

        [DataMember]
        public virtual PatientInsuranceAuthorizationViewModel Authorization { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}