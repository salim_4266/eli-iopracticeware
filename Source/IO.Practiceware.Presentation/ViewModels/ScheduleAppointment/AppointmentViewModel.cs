﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleAppointment
{
    [DataContract]
    [EditableObject]
    public class AppointmentViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual DateTime EndDateTime { get; set; }

        [DataMember]
        public virtual AppointmentTypeViewModel Type { get; set; }

        [DataMember]
        public virtual int TypeCategoryId { get; set; }

        [DataMember]
        public virtual int InsuranceTypeId { get; set; }

        [DataMember]
        public virtual ResourceViewModel Resource { get; set; }

        [DataMember]
        public virtual NamedViewModel Patient { get; set; }

        [DataMember]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual EncounterViewModel Encounter { get; set; }
    }
}