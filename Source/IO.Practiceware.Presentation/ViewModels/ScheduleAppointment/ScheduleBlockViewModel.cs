﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.ScheduleAppointment
{
    [DataContract]
    public class ScheduleBlockViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual DateTime StartDateTime { get; set; }

        [DataMember]
        public virtual DateTime EndDateTime { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ColoredViewModel Location { get; set; }

        [DataMember]
        public virtual ResourceViewModel Resource { get; set; }

        /// <summary>
        /// Gets or sets the category (the ScheduleBlockAppointmentCategory).
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [DataMember]
        public virtual NamedViewModel<Guid> Category { get; set; }
    }
}
