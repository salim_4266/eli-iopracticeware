﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// An appointment from the IO DB.
    /// </summary>
    [DataContract]
    public class AppointmentSearchAppointmentHistoryViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual AppointmentStatusViewModel AppointmentStatus { get; set; }

        [DataMember]
        public virtual DateTime AppointmentDateTime { get; set; }

        [DataMember]
        public virtual ObservableCollection<string> ReferralNumbers { get; set; }

        [DataMember]
        public virtual NamedViewModel AppointmentType { get; set; }

        [DataMember]
        public virtual NamedViewModel Resource { get; set; }

        [DataMember]
        public virtual NamedViewModel Location { get; set; }

        [DataMember]
        public virtual string Comments { get; set; }
    }

    /// <summary>
    /// The status of an appointment
    /// </summary>
    [DataContract]
    public class AppointmentStatusViewModel : IViewModel
    {
        /// <summary>
        /// Gets or sets Appointment Status Id
        /// </summary>
        [DataMember]
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the Appointment Status Name.
        /// </summary>
        [DataMember]
        public virtual string Status { get; set; }

        /// <summary>
        /// Gets or sets the Appointment Status Code whose value is the first letter of the Appointment Status Name.
        /// </summary>
        [DataMember]
        public virtual char StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the Color representation of the Appointment Status.
        /// </summary>
        [DataMember]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(StatusColor); }
            set { StatusColor = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        public virtual Color StatusColor { get; set; }
    }
}
