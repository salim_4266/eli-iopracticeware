﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    [DataContract]
    public class AppointmentTypeViewModel : IViewModel
    {
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual NamedViewModel AppointmentCategory { get; set; }
    }
}
