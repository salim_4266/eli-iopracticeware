﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// A referral from the IO DB.
    /// </summary>
    [DataContract]
    public class AppointmentSearchReferralsViewModel : IViewModel
    {
        [DataMember]
        public virtual string ReferralNumber { get; set; }

        [DataMember]
        public virtual NamedViewModel Insurer { get; set; }

        [DataMember]
        public virtual NamedViewModel ReferredTo { get; set; }

        [DataMember]
        public virtual DateTime? ReferralExpirationDate { get; set; }

        [DataMember]
        public virtual int EncountersUsed { get; set; }

        [DataMember]
        public virtual int TotalEncountersCovered { get; set; }

        [DataMember]
        public virtual string ReferralReason { get; set; }
    }
}