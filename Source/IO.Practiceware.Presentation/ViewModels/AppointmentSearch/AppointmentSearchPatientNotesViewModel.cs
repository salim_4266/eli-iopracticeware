﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// The comments of the Patient from the IO DB.
    /// </summary>
    [DataContract]
    public class AppointmentSearchPatientCommentsViewModel : IViewModel
    {
        [DataMember]
        public virtual string Comments { get; set; }

        [DataMember]
        public virtual bool HasAlert { get; set; }
    }
}
