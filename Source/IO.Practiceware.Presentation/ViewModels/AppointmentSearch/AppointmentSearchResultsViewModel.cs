﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// A view model storing the information needed to display an available appointment.
    /// </summary>
    [DataContract]
    public class AppointmentSearchResultsViewModel : IViewModel
    {
        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual TimeSpan Time { get; set; }

        [DataMember]
        public virtual AppointmentTypeViewModel AppointmentType { get; set; }

        [DataMember]
        public virtual NamedViewModel Resource { get; set; }

        [DataMember]
        public virtual string Workload { get; set; }

        [DataMember]
        public virtual NamedViewModel Location { get; set; }

        public virtual Color Color { get; set; }

        [DataMember]
        public virtual string ColorString
        {
            get { return new ColorConverter().ConvertToString(Color); }
            set { Color = ColorConverter.ConvertFromString(value).CastTo<Color>(); }
        }

        [DataMember]
        public virtual ObservableCollection<Guid> ScheduleBlockCategoryIds { get; set; }
    }
}
