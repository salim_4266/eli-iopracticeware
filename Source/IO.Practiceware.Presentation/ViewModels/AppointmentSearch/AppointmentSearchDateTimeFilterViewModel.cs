﻿using IO.Practiceware.Validation;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// Final search filter choice for date and time
    /// </summary>
    [DataContract]
    public class AppointmentSearchDateTimeFilterViewModel : IViewModel
    {
        [DataMember]
        [Dependency]
        public virtual DateSelectionViewModel SelectedDateOption { get; set; }

        [DataMember]
        public virtual TimeSelectionViewModel SelectedTimeOption { get; set; }
    }

    /// <summary>
    /// The possible selection options for date
    /// </summary>

    #region Date Selection Options

    [SupportsDataErrorInfo]
    [DataContract]
    public class DateSelectionViewModel : IViewModel
    {
        private DateTime? _endDate;
        [NotEmpty]
        [DataMember]
        public virtual ObservableCollection<DayOfWeek> SelectedDays { get; set; }

        [Required(ErrorMessage="A start date is required.")]
        [DataMember]
        public virtual DateTime? BeginDate { get; set; }
        [EqualToGreaterThan("BeginDate", ErrorMessage = "Cannot perform search if the end date is before the start date.")]
        [DataMember]
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;

                // Make the end date include the whole day
                if (_endDate.HasValue)
                {
                    var dateOnly = _endDate.Value.Date;
                    _endDate = dateOnly.AddDays(1).Subtract(TimeSpan.FromMinutes(1));
                }
            }
        }

        public DateSelectionViewModel()
        {
            SelectedDays = new ObservableCollection<DayOfWeek>();
            BeginDate = DateTime.Now.ToClientTime();
            EndDate = null;
        }
    }

    #endregion

    /// <summary>
    /// The possible selection options for time
    /// </summary>

    #region Time Selection Options

    [DataContract]
    public enum TimeSelectionType
    {
        [EnumMember] [Display(Name = "All Day")] AllDay,
        [EnumMember] [Display(Name = "Range")] Range,
        [EnumMember] [Display(Name = "Exact")] Exact,
        [EnumMember] [Display(Name = "AM")] Am,
        [EnumMember] [Display(Name = "PM")] Pm,
        [EnumMember] [Display(Name = "Before")] Before,
        [EnumMember] [Display(Name = "After")] After
    }

    [SupportsDataErrorInfo]
    [DataContract]
    public class TimeSelectionViewModel : IViewModel
    {
        [DataMember]
        public virtual string Name { get; set; }
        [Required(ErrorMessage="A start time is required.")]
        [DataMember]
        public virtual TimeSpan? BeginTime { get; set; }
        
        [DataMember]        
        [EqualToGreaterThan("BeginTime", ErrorMessage="The end time must be greater then the start time.")]
        [RequiredIf("ShowEnd", ErrorMessage = "An end time is required.")]
        public virtual TimeSpan? EndTime { get; set; }

        [DataMember]
        public virtual bool ShowBegin { get; set; }

        [DataMember]
        public virtual bool ShowEnd { get; set; }

        [DataMember]
        public TimeSelectionType TimeSelectionType { get; set; }

         public TimeSelectionViewModel()
         {

         }

        public TimeSelectionViewModel(TimeSelectionType type)
        {
            TimeSelectionType = type;
            switch (type)
            {
                case TimeSelectionType.AllDay:
                    Name = "All Day";
                    BeginTime = new TimeSpan(0, 0, 0);
                    EndTime = new TimeSpan(23, 59, 0);
                    ShowBegin = false;
                    ShowEnd = false;
                    break;
                case TimeSelectionType.Range:
                    Name = "Range";
                    BeginTime = null;
                    EndTime = null;
                    ShowBegin = true;
                    ShowEnd = true;
                    break;
                case TimeSelectionType.Exact:
                    Name = "Exact";
                    BeginTime = DateTime.Now.ToClientTime().TimeOfDay;
                    EndTime = null;
                    ShowBegin = true;
                    ShowEnd = false;
                    break;
                case TimeSelectionType.Am:
                    Name = "AM";
                    BeginTime = new TimeSpan(0, 0, 0);
                    EndTime = new TimeSpan(12, 0, 0);
                    ShowBegin = false;
                    ShowEnd = false;
                    break;
                case TimeSelectionType.Pm:
                    Name = "PM";
                    BeginTime = new TimeSpan(12, 0, 0);
                    EndTime = new TimeSpan(23, 59, 0);
                    ShowBegin = false;
                    ShowEnd = false;
                    break;
                case TimeSelectionType.Before:
                    Name = "Before";
                    BeginTime = DateTime.Now.ToClientTime().TimeOfDay;
                    EndTime = null;
                    ShowBegin = true;
                    ShowEnd = false;
                    break;
                case TimeSelectionType.After:
                    Name = "After";
                    BeginTime = DateTime.Now.ToClientTime().TimeOfDay;
                    EndTime = null;
                    ShowBegin = true;
                    ShowEnd = false;
                    break;
            }
        }
    }

    #endregion
}
