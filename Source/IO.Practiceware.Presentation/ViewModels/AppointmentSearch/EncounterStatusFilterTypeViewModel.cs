﻿using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    public enum EncounterStatusFilterType
    {
        Pending,
        Arrived,
        Discharged,
        Cancelled
    }

    public class EncounterStatusFilterTypeViewModel : IViewModel
    {
        public virtual EncounterStatusFilterType FilterType { get; set; }
        public virtual bool IsSelected { get; set; }
    }
}
