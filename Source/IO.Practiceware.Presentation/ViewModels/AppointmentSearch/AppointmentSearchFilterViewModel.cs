﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// A store for all the possible selection options via collections and final search filter choice
    /// </summary>
    [DataContract]
    public class AppointmentSearchFilterViewModel : IViewModel
    {
        public AppointmentSearchFilterViewModel()
        {
            SelectedLocations = new ExtendedObservableCollection<NamedViewModel>();
            SelectedResources = new ExtendedObservableCollection<NamedViewModel>();
        }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<AppointmentTypeViewModel> AppointmentTypes { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> Locations { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> Resources { get; set; }

        [DataMember]
        public virtual AppointmentTypeViewModel SelectedAppointmentType { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedLocations { get; set; }

        [DataMember]
        [Dependency]
        public virtual ObservableCollection<NamedViewModel> SelectedResources { get; set; }
        
    }
}
