﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.AppointmentSearch
{
    /// <summary>
    /// The patient from the IO DB.
    /// </summary>
    [DataContract]
    public class AppointmentSearchPatientInfoViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Type { get; set; }

        [DataMember]
        public virtual decimal Balance { get; set; }

        [DataMember]
        public virtual string HomePhone { get; set; }

        [DataMember]
        public virtual string WorkPhone { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }

        [DataMember]
        public virtual int? DefaultResourceId { get; set; }

        [DataMember]
        public virtual bool IsPatientRemoved { get; set; }
    }
}
