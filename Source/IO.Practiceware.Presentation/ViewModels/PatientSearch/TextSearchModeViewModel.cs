﻿using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.PatientSearch
{
    public class TextSearchModeViewModel : NamedViewModel
    {
        public virtual bool IsSelected { get; set; }
        public virtual string ToolTipText { get; set; }
    }
}