﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.Common;

namespace IO.Practiceware.Presentation.ViewModels.PatientSearch
{
    [DataContract]
    public class PatientSearchAdvancedOptionsSettings
    {
        public PatientSearchAdvancedOptionsSettings()
        {
            TextSearchModes = new List<TextSearchModeViewModel>();
            SearchFields = new List<SelectableNamedViewModel>();
        }
        /// <summary>
        /// Collection of selected search modes
        /// </summary>
        [DataMember]
        public List<TextSearchModeViewModel> TextSearchModes { get; set; }

        /// <summary>
        /// Collection of selected search Fields
        /// </summary>
        [DataMember]
        public List<SelectableNamedViewModel> SearchFields { get; set; }
    }
}
