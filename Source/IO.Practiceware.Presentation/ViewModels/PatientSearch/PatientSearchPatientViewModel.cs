﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.PatientSearch
{
    [DataContract]
    public class PatientSearchPatientViewModel : IViewModel
    {
        [DataMember]
        public virtual string PriorId { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Prefix { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string MiddleName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string Suffix { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual DateTime? DateOfBirth { get; set; }

        [DataMember]
        public virtual List<string> ListOfTypeName { get; set; }

        [DataMember]
        public virtual string PrimaryAddress { get; set; }

        [DataMember]
        public virtual string MultiTypeName
        {
            get
            {
                var display = String.Empty;
                if (ListOfTypeName == null) return display;
                display = ListOfTypeName.Aggregate(display, (current, name) => current + (name + Environment.NewLine));
                return display.Trim();
            }
            set { _multiTypeName = value; }
        }

        private string _multiTypeName; // IApplicationSettings requires a setter on properties

        [DataMember]
        public virtual string HomePhone { get; set; }

        [DataMember]
        public virtual string CellPhone { get; set; }

        [DataMember]
        public virtual string WorkPhone { get; set; }

        [DataMember]
        public virtual string MultiPhoneNumber
        {
            get
            {
                var display = String.Empty;
                if (!String.IsNullOrWhiteSpace(HomePhone)) display += String.Format("{0} home", HomePhone) + Environment.NewLine;
                if (!String.IsNullOrWhiteSpace(CellPhone)) display += String.Format("{0} cell", CellPhone) + Environment.NewLine;
                if (!String.IsNullOrWhiteSpace(WorkPhone)) display += String.Format("{0} work", WorkPhone) + Environment.NewLine;
                return display.Trim();
            }
            set { _multiPhoneNumber = value; }
        }

        private string _multiPhoneNumber; // IApplicationSettings requires a setter on properties

        [DataMember]
        public virtual string PrimaryPhoneNumber
        {
            get
            {
                var display = String.Empty;
                if (!String.IsNullOrWhiteSpace(HomePhone)) return HomePhone;
                if (!String.IsNullOrWhiteSpace(CellPhone)) return CellPhone;
                if (!String.IsNullOrWhiteSpace(WorkPhone)) return WorkPhone;
                return display.Trim();
            }
            set { _primaryPhoneNumber = value; }
        }

        private string _primaryPhoneNumber; // IApplicationSettings requires a setter on properties

        [DataMember]
        public virtual DateTime? NextAppointmentDate { get; set; }

        [DataMember]
        public virtual DateTime? LastAppointmentDate { get; set; }

        [DataMember]
        public virtual string PracticeDoctor { get; set; }

        [DataMember]
        public virtual string ReferringDoctor { get; set; }

        [DataMember]
        public virtual string PrimaryCareDoctor { get; set; }

        [DataMember]
        public virtual bool IsClinical { get; set; }

        [DispatcherThread]
        public virtual byte[] Photo { get; set; }

        [DispatcherThread]
        public virtual bool HasLoadedPhoto { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual PostOperativeViewModel PostOp { get; set; }

        [DispatcherThread]
        [DependsOn("PostOp")]
        public virtual bool ShowPostOp
        {
            get { return PostOp != null && PostOp.CurrentlyInPostOperativePeriod; }
        }

        [DispatcherThread]
        public virtual bool HasLoadedPostOp { get; set; }
    }
}