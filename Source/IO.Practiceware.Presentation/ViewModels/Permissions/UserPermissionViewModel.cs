﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Permissions
{
    [DataContract]
    public class UserPermissionViewModel : IViewModel
    {
        [DataMember]
        public virtual bool IsSelected { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual int PermissionId { get; set; }

        [DataMember]
        public virtual int UserId { get; set; }

        [DataMember]
        public virtual string Category { get; set; }
    }
}
