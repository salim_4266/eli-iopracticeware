﻿using IO.Practiceware.Presentation.ViewModels.Common;
using System.Collections.ObjectModel;

namespace IO.Practiceware.Presentation.ViewModels.Permissions
{
    public class UserViewModel : NamedViewModel
    {
        public ObservableCollection<NamedViewModel> UserPermissions { get; set; }
    }
}
