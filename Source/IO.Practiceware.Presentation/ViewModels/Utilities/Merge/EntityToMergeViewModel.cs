﻿using System.Runtime.Serialization;
using Soaf.Presentation;

namespace IO.Practiceware.Presentation.ViewModels.Utilities.Merge
{
    [DataContract]
    public class EntityToMergeViewModel : IViewModel
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual MergeEntityType EntityType { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual string Details { get; set; }
    }

    public enum MergeEntityType
    {
        Insurer,
        Provider,
        // TODO: add support later
        // Patient
    }
}
