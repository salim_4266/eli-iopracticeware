﻿using IO.Practiceware.Model;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.Utilities
{
    [EditableObject]
    [DataContract]
    public class EditTemplatesViewModel : IViewModel
    {       
        [DispatcherThread]
        [DataMember]
        public virtual int TemplateDocumentId { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual string DocumentName { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual string DocumentText { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ContentType ContentType { get; set; }
    }
}
