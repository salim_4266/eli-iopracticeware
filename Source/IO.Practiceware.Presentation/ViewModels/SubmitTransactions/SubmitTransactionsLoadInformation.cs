﻿using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class SubmitTransactionsLoadInformation
    {
        [DataMember]
        public SubmitTransactionsFilterSelectionViewModel FilterSelectionViewModel { get; set; }

        [DataMember]
        public SubmitTransactionsFilterViewModel FilterViewModel { get; set; }

        [DataMember]
        public ClaimsMachineSettingViewModel ClaimsDirectoryPaths { get; set; }
    }
}
