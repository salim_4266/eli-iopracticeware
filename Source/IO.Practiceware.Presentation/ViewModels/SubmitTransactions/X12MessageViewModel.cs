﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class X12MessageViewModel : IViewModel
    {
        [DataMember]
        public virtual string Message { get; set; }

        [DataMember]
        public virtual string Id { get; set; }

        [DataMember]
        public virtual string MessageType { get; set; }
    }
}
