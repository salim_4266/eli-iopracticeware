﻿using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class SubmitTransactionsUserSettingViewModel : IViewModel
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ClaimType { get; set; }

        [DataMember]
        public int? PaperClaimFormId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public List<int> ElectronicClaimFormIds { get; set; }

        [DataMember]
        public virtual List<int> SelectedInsurers { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedScheduledDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedLocations { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingOrganizations { get; set; }
    }
}
