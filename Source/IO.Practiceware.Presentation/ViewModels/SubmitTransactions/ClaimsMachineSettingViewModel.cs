﻿using Soaf.Presentation;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class ClaimsMachineSettingViewModel : IViewModel
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public string ElectronicClaimsFileDirectory { get; set; }

        [DataMember]
        public string PaperClaimsFileDirectory { get; set; }
    }
}
