﻿using Soaf.Presentation;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class SubmitTransactionsInsurersViewModel : IViewModel
    {
        [DataMember]
        [DispatcherThread]
        public virtual List<int> InsurerIds { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual string Name { get; set; }
    }
}
