﻿using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class TransactionSearchResultViewModel : IViewModel
    {
        [DataMember]
        public virtual long InvoiceReceivableId { get; set; }

        [DataMember]
        public virtual List<Guid> BillingServiceTransactionIds { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual int PatientId { get; set; }

        [DataMember]
        public virtual NamedViewModel ClaimFormType { get; set; }

        [DataMember]
        public virtual NamedViewModel ClaimFileReceiver { get; set; }

        [DataMember]
        public virtual string BilledInsurer { get; set; }

        [DataMember]
        public virtual string AppointmentType { get; set; }

        [DataMember]
        public virtual DateTime DateQueued { get; set; }

        [DataMember]
        public virtual DateTime ServiceDate { get; set; }

        [DataMember]
        public virtual decimal AmountBilled { get; set; }

        [DataMember]
        public virtual string ScheduledDoctor { get; set; }

        [DataMember]
        public virtual string BillingDoctor { get; set; }

        [DataMember]
        public virtual string ServiceLocation { get; set; }

        [DataMember]
        public virtual string BillingOrganization { get; set; }

        [DataMember]
        public virtual Boolean DisplayDecimal { get; set; }
    }    
}
