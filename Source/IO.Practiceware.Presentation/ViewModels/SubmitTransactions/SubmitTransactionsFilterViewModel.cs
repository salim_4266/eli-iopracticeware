﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.SubmitTransactions;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;


[assembly: Component(typeof(TransactionFilterSelectionViewModelMap), typeof(IMap<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel>))]

namespace IO.Practiceware.Presentation.ViewModels.SubmitTransactions
{
    [DataContract]
    public class SubmitTransactionsFilterViewModel : IViewModel
    {
        #region Dropdown Lists

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<ClaimSelectionViewModel> TypesOfClaims
        {
            get;
            set;
        }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<ClaimSelectionViewModel> PaperClaimForms
        {
            get;
            set;
        }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<ClaimSelectionViewModel> ElectronicClaimForms
        {
            get;
            set;
        }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<SubmitTransactionsInsurersViewModel> Insurers { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> BillingDoctors { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> ScheduledDoctors { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> Locations { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ObservableCollection<NamedViewModel> BillingOrganizations { get; set; }

        [DataMember]
        [DispatcherThread]
        [Dependency]
        public virtual ObservableCollection<SubmitTransactionsUserSettingViewModel> SavedFilters
        {
            get;
            set;
        }

        #endregion
    }

    [DataContract]
    public class SubmitTransactionsFilterSelectionViewModel : IViewModel, IValidatableObject
    {
        private DateTime? _endDate;

        public SubmitTransactionsFilterSelectionViewModel()
        {
            SelectedInsurers = new ObservableCollection<SubmitTransactionsInsurersViewModel>();
            SelectedBillingDoctors = new ObservableCollection<NamedViewModel>();
            SelectedScheduledDoctors = new ObservableCollection<NamedViewModel>();
            SelectedLocations = new ObservableCollection<NamedViewModel>();
            SelectedBillingOrganizations = new ObservableCollection<NamedViewModel>();
            SelectedElectronicClaims = new ObservableCollection<ClaimSelectionViewModel>();
        }

        [DataMember]
        [DispatcherThread]
        public virtual int ClaimTypeId { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual ClaimType ClaimType
        {
            get { return (ClaimType)ClaimTypeId; }
            set
            {
                ClaimTypeId = (int)value;
            }
        }   

        [DispatcherThread]
        public virtual ClaimType CurrentClaimTypeInSearchResults { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual int? PaperClaimFormId { get; set; }     

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<ClaimSelectionViewModel> SelectedElectronicClaims { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime? StartDate { get; set; }

        [DataMember]
        [DispatcherThread]
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;

                // Make the end date include the whole day
                if (_endDate.HasValue)
                {
                    var dateOnly = _endDate.Value.Date;
                    _endDate = dateOnly.AddDays(1).Subtract(TimeSpan.FromMinutes(1));
                }
            }
        }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<SubmitTransactionsInsurersViewModel> SelectedInsurers { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedBillingDoctors { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedScheduledDoctors { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedLocations { get; set; }

        [DispatcherThread]
        [DataMember]
        public virtual ObservableCollection<NamedViewModel> SelectedBillingOrganizations { get; set; }

        [DependsOn("ClaimType", "ClaimTypeId")]
        public virtual bool PaperSelected
        {
            get { return ClaimType == ClaimType.PaperClaims; }
        }

        [DependsOn("CurrentClaimTypeInSearchResults")]
        public virtual bool PaperSelectedInSearchResults
        {
            get { return CurrentClaimTypeInSearchResults == ClaimType.PaperClaims; }
        }

        [DependsOn("PaperClaimFormId", "PaperSelected")]
        public virtual bool Ub04Selected
        {
            get
            {
                var isSelected = PaperSelected && PaperClaimFormId.HasValue && PaperClaimFormId.Value == (int) ClaimFormTypeId.UB04;
                if (isSelected && SelectedBillingDoctors.IsNotNullOrEmpty())
                {
                    SelectedBillingDoctors.Clear();
                }
                return isSelected;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if ((ClaimType == ClaimType.EClaims && SelectedElectronicClaims.IsNotNullOrEmpty())
                || (ClaimType == ClaimType.PaperClaims && PaperClaimFormId.HasValue))
            {
                yield return null;
            }
            else
            {
                yield return new ValidationResult("", new[] { "ClaimTypeId", "SelectedElectronicClaims", "PaperClaimFormId" });
            }
        }
    }

    [DataContract]
    public enum ClaimType
    {

        [Display(Name = "E-Claims")]
        [EnumMember]
        EClaims = 1,

        [Display(Name = "Paper Claims")]
        [EnumMember]
        PaperClaims = 2

    }

    [DataContract]
    public class ClaimSelectionViewModel : NamedViewModel<int>
    {
        [DataMember]
        public bool HasItemsAvailableForTransmission { get; set; }
    }


    internal class TransactionFilterSelectionViewModelMap : IMap<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel>
    {
        private readonly IEnumerable<IMapMember<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel>> _members;

        public TransactionFilterSelectionViewModelMap()
        {
            _members = this.CreateMembers(Map, "TransactionFilterSelectionViewModelMap");
        }

        public void Map(SubmitTransactionsFilterSelectionViewModel source, SubmitTransactionsUserSettingViewModel destination)
        {
            destination.ClaimType = source.ClaimTypeId;
            destination.StartDate = source.StartDate;
            destination.EndDate = source.EndDate;
            destination.PaperClaimFormId = source.PaperClaimFormId;
            destination.SelectedBillingDoctors = source.SelectedBillingDoctors.Select(x => x.Id).ToList();
            destination.SelectedBillingOrganizations = source.SelectedBillingOrganizations.Select(x => x.Id).ToList();
            destination.SelectedInsurers = source.SelectedInsurers.SelectMany(x => x.InsurerIds).ToList();
            destination.ElectronicClaimFormIds = source.SelectedElectronicClaims.Select(x => x.Id).ToList();
            destination.SelectedLocations = source.SelectedLocations.Select(x => x.Id).ToList();
            destination.SelectedScheduledDoctors = source.SelectedScheduledDoctors.Select(x => x.Id).ToList();
        }

        public IEnumerable<IMapMember<SubmitTransactionsFilterSelectionViewModel, SubmitTransactionsUserSettingViewModel>> Members
        {
            get { return _members; }
        }
    }
}
