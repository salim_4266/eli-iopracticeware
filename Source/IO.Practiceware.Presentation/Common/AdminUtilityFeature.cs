﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Common
{
    public class AdminUtilityFeature
    {
        public const string AuditLog = "Audit Logs";
        public const string DataIntegrity = "Data Integrity";
        public const string Encryption = "Encryption";
        public const string FileConversion = "File Conversion";
        public const string ImportPatientImages = "Import PatientImages";
        public const string ImportSPEXUPCs = "Import SPEXUPCs";
        public const string MachineConfiguration = "Machine Configuration";
        public const string RequeueClaims = "Requeue Claims";
        public const string RequeueRecalls = "Requeue Recalls";
        public const string ClinicalDecisions = "Clinical Decisions";
        public const string MeaningfulUseCalculator = "Meaningful Use Calculator";
        public const string PQRS = "PQRS";
        public const string QRDAExport = "QRDA Export";
        public const string QRDATotals = "QRDA Totals";
    
    }
}
