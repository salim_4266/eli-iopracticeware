﻿using System;
using System.Windows;
using System.Windows.Interop;
using IWin32Window = System.Windows.Forms.IWin32Window;

namespace IO.Practiceware.Presentation.Common
{
    public class WpfWin32Window : IWin32Window
    {
        public IntPtr Handle { get; private set; }

        public WpfWin32Window(Window wpfWindow)
        {
            Handle = new WindowInteropHelper(wpfWindow).Handle;
        }
    }

}
