﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Presentation.Common
{
    public class ResponseEntity
    {
        public string ResponseType { get; set; }
        public string ExternalId { get; set; }
    }

    public class DefaultEntity
    {
        public string PracticeToken { get; set; }
    }
    public class PatientEntity : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string LocationID { get; set; }
        public string EmailAddresses { get; set; }
        public string PhoneNumbers { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string BirthDate { get; set; }
        public string Notes { get; set; }
        public string LanguageName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsSendEmailViaAPI { get; set; }
        public string Gender { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string MaritalStatus { get; set; }
        public PatientRepresentativeDetail RepersentativeDetail { get; set; }
    }

    public class CredentialsEntity
    {
        public string _status { get; set; }
        public string _externalId { get; set; }
        public string _practiceToken { get; set; }
        public string _userName { get; set; }
        public string _password { get; set; }
        public bool _isemailaddress { get; set; }
    }

    public class CDAMessage : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string PatientExternalId { get; set; }
        public string LocationExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string CDAxml { get; set; }
        public string Type = "2";
    }

    public class PatientRepresentativeDetail
    {
        public string RepresentativeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class AppointmentEntity : DefaultEntity
    {
        public string PatientExternalId { get; set; }
        public string LocationExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string AppointmentStatusExternalId { get; set; }
        public string CancelReason { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ExternalId { get; set; }
    }

    public class PortalQueue
    {
        public int ActionTypeLookupId { get; set; }
        public string Exception { get; set; }
        public string ExternalId { get; set; }
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string MessageData { get; set; }
        public string PatientId { get; set; }
        public DateTime ProcessedDate { get; set; }
    }

    public enum ResponseStatus
    {
        Success = 1,
        Error = 2,
        Exception = 3,
        InvalidCredentials = 4,
        Updated = 5,
        InvalidPatient = 6
    }


}
