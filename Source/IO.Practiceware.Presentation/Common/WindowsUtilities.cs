﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Soaf.Collections;
using Soaf.Presentation;
using Soaf.Presentation.Interop;

namespace IO.Practiceware.Presentation.Common
{
    public static class WindowsUtilities
    {
        /// <summary>
        /// Forces closing of all application windows
        /// </summary>
        /// <param name="exclusions"></param>
        public static void ForceCloseAllAppWindows(IntPtr[] exclusions = null)
        {
            var closedWindows = new Dictionary<IntPtr, int>();
            var remainingAttemptsCount = 5;
            do
            {
                // Build a list of windows to close and exit if none
                var windowsToClose = GetWindowsToClose(exclusions);
                if (windowsToClose.Count == 0) break;

                foreach (var window in windowsToClose)
                {
                    int closeAttemptsForWindow;
                    closedWindows.TryGetValue(window.Handle, out closeAttemptsForWindow);

                    switch (closeAttemptsForWindow)
                    {
                        case 0:
                            WindowInteropUtility.User32.SendMessage(window.Handle, (uint)WindowInteropUtility.Enums.WM.CLOSE,
                                IntPtr.Zero, IntPtr.Zero);
                            break;
                        case 1:
                            WindowInteropUtility.User32.SendMessage(window.Handle, (uint)WindowInteropUtility.Enums.WM.ENDSESSION,
                                (IntPtr)0x80000000 /* ENDSESSION_LOGOFF */, IntPtr.Zero);
                            break;
                        default:
                            // Closing window didn't work -> destroying it
                            WindowInteropUtility.User32.DestroyWindow(window.Handle);
                            break;
                    }

                    // Record close attempt
                    closedWindows[window.Handle] = ++closeAttemptsForWindow;
                }

                // Wait until app processes pending messages
                System.Windows.Application.Current.DoEvents();
            } while (--remainingAttemptsCount > 0);

            // Have we successfully closed all windows?
            if (remainingAttemptsCount == 0)
            {
                throw new InvalidOperationException("We couldn't close all windows at this time. Please, close manually or shutdown process via Task Manager");
            }
        }

        /// <summary>
        /// Identifies current process UI windows which needed to be closed
        /// </summary>
        /// <param name="exclusions"></param>
        /// <returns></returns>
        private static List<WindowInteropUtility.ExtendedHwndDetails> GetWindowsToClose(IntPtr[] exclusions)
        {
            var processHandles = WindowInteropUtility.GetProcessWindowHandles(Process.GetCurrentProcess().Id);
            var windowsToClose = processHandles.Select(WindowInteropUtility.GetDetailedHwndInformation).ToList();

            // Exclude which run the app (if present) and windows we are not interested in
            var nonUiWindows = windowsToClose
                .Where(w => w.ClassName == "ThunderRT6Main" // VB6 main thread handle
                            || w.StyleEx == WindowInteropUtility.Enums.WindowStylesEx.WS_EX_LEFT // All "normal" windows have some extended style set
                            || w.WindowType == WindowInteropUtility.ExtendedHwndDetails.WindowTypes.Other
                            || w.WindowType == WindowInteropUtility.ExtendedHwndDetails.WindowTypes.Vb6Form) // We are interested only in windows of know type,also excluded Vb6 Forms and are handled in Vb6 code itself.
                .ToList();
            nonUiWindows.ForEach(w => windowsToClose.Remove(w));

            // Exclude tool windows (they should be handled on their own)
            var toolWindows = windowsToClose
                .Where(w => (w.StyleEx & WindowInteropUtility.Enums.WindowStylesEx.WS_EX_TOOLWINDOW)
                    == WindowInteropUtility.Enums.WindowStylesEx.WS_EX_TOOLWINDOW)
                .ToList();
            var toolWindowDependents = windowsToClose.Where(w => toolWindows
                .Any(tw => w.Owner == tw.Handle
                    || w.Parent == tw.Handle))
                .ToList();
            toolWindows.AddRange(toolWindowDependents);
            toolWindows.ForEach(sw => windowsToClose.Remove(sw));

            // Additionally filter out user hinted windows
            if (exclusions != null && exclusions.Length > 0)
            {
                var excludedWindows = windowsToClose.Where(w => exclusions.Contains(w.Handle)).ToArray();
                excludedWindows.ForEach(ew => windowsToClose.Remove(ew));
            }

            return windowsToClose;
        }
    }
}
