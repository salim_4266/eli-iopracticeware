﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using Soaf.ComponentModel;
using Soaf.Data;
using System.Data.SqlClient;

namespace IO.Practiceware.Presentation.Common
{

    public interface IPortalServices
    {
        string RegisterPatient(PatientEntity objPatient);
        string UpdateDemographics(PatientEntity objPatient);
        string PostHealthSummary(CDAMessage objHealthSummary);
        string NewAppointment(AppointmentEntity objAppointment);
        string UpdateAppointment(AppointmentEntity objAppointment);
        string DeleteAppointment(AppointmentEntity objAppointment);
    }

    public class PortalServices : IPortalServices
    {
        public string RegisterPatient(PatientEntity objPatient)
        {
            string result = string.Empty;
            ResponseEntity objResponseEntity = new ResponseEntity();
            string apiUrl = "https://portal.iopracticeware.com/api/Patient/New/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPatient);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                objResponseEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                if (objResponseEntity.ResponseType.Equals(ResponseStatus.Success.ToString()))
                {
                    PortalQueue pq = new PortalQueue()
                    {
                        ExternalId = objResponseEntity.ExternalId,
                        IsActive = true,
                        ActionTypeLookupId = 97,
                        MessageData = "API",
                        ProcessedDate = DateTime.Now,
                        PatientId = objPatient.ExternalId
                    };
                    InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ee)
            {
                string temp = ee.Message.ToString();
                objResponseEntity.ResponseType = (ResponseStatus.Error.ToString());
            }

            return objResponseEntity.ResponseType;
        }

        public string UpdateDemographics(PatientEntity objPatient)
        {
            string result = string.Empty;
            objPatient.IsSendEmailViaAPI = false;
            ResponseEntity objResponseEntity = new ResponseEntity();
            string apiUrl = "https://portal.iopracticeware.com/api/Patient/UpdateDemographics/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPatient);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                objResponseEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
            }
            catch (Exception)
            {

            }
            return objResponseEntity.ResponseType;
        }

        public string PostHealthSummary(CDAMessage objHealthSummary)
        {
            string result = string.Empty;
            string apiUrl = "https://portal.iopracticeware.com/api/CDAMessage/UpdateHealthSummary/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objHealthSummary);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                if (result.Equals(ResponseStatus.Success.ToString()))
                {
                    PortalQueue pq = new PortalQueue()
                    {
                        ExternalId = objHealthSummary.ExternalId,
                        IsActive = true,
                        ActionTypeLookupId = 19,
                        MessageData = "API",
                        ProcessedDate = DateTime.Now,
                        PatientId = objHealthSummary.PatientExternalId
                    };
                    InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public string NewAppointment(AppointmentEntity objAppointment)
        {
            string result = string.Empty;
            string apiUrl = "https://portal.iopracticeware.com/api/Appointments/New/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objAppointment);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                if (result.Equals(ResponseStatus.Updated.ToString()))
                {
                    PortalQueue pq = new PortalQueue()
                    {
                        ExternalId = objAppointment.ExternalId,
                        IsActive = true,
                        ActionTypeLookupId = 80,
                        MessageData = "API",
                        ProcessedDate = DateTime.Now,
                        PatientId = objAppointment.PatientExternalId
                    };
                    InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public string UpdateAppointment(AppointmentEntity objAppointment)
        {
            string result = string.Empty;
            string apiUrl = "https://portal.iopracticeware.com/api/Appointments/Update/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objAppointment);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                if (result.Equals(ResponseStatus.Updated.ToString()))
                {
                    PortalQueue pq = new PortalQueue()
                    {
                        ExternalId = objAppointment.ExternalId,
                        IsActive = true,
                        ActionTypeLookupId = 99,
                        MessageData = "API" + jsonToBeSent,
                        ProcessedDate = DateTime.Now,
                        PatientId = objAppointment.PatientExternalId
                    };
                    InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public string DeleteAppointment(AppointmentEntity objAppointment)
        {
            string result = string.Empty;
            string apiUrl = "https://portal.iopracticeware.com/api/Appointments/Delete/";
            string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objAppointment);
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                if (result.Equals(ResponseStatus.Success.ToString()))
                {
                    PortalQueue pq = new PortalQueue()
                    {
                        ExternalId = objAppointment.ExternalId,
                        IsActive = true,
                        ActionTypeLookupId = 81,
                        MessageData = "API" + jsonToBeSent,
                        ProcessedDate = DateTime.Now,
                        PatientId = objAppointment.PatientExternalId
                    };
                    InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception)
            {

            }
            return result;
        }


        private void InsertDataIntoPortalQueueResponse(PortalQueue pq)
        {
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string insQry = "Insert into [MVE].[PP_PortalQueueResponse](MessageData,ActionTypeLookupId,ProcessedDate,PatientNo,IsActive,ExternalId)values ('" + pq.MessageData + "','" + pq.ActionTypeLookupId + "','" + pq.ProcessedDate + "','" + pq.PatientId + "','" + pq.IsActive + "','" + pq.ExternalId + "')";
                dbConnection.Execute(insQry);
            }

        }
    }

}