﻿
namespace IO.Practiceware.Presentation.Common
{
    public class Constants
    {
        public const string ElectronicClaimsFileNameFormat = "_EC.txt";

        /// <summary>
        /// The purchase license/serial number that we need to set on each pdf file we create using the O2.Solutions Pdf4View/Pdf4Render library.
        /// </summary>
        public const string Pdf4RenderSerialNumber = "PDFVW4WPF-JMBJV-T5043-VD9BQ-A4M87-BVV2A";
    }
}
