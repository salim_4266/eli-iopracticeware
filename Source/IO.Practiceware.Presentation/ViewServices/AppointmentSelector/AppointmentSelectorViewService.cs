﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.AppointmentSelector;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.AppointmentSelector;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

[assembly: Component(typeof(AppointmentSelectorViewService), typeof(IAppointmentSelectorViewService))]
[assembly:
    Component(typeof(AppointmentSelectorResultViewModelMap),
        typeof(IMap<UserAppointment, AppointmentSelectorResultViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.AppointmentSelector
{
    public interface IAppointmentSelectorViewService
    {
        /// <summary>
        /// Gets the Filter information necessary to search the Appointments
        /// </summary>
        /// <returns>Returns a new instance of AppointmentSelectorFilterViewModel</returns>
        AppointmentSelectorFilterViewModel LoadSearchFilterInformation();

       /// <summary>
        /// Searches the Appointment table for any rows which match the filter criteria
       /// </summary>
       /// <param name="filterSelections"></param>
       /// <param name="getOnlyDischargedEncounters"></param>
       /// <returns></returns>
        IEnumerable<AppointmentSelectorResultViewModel> SearchAppointments(AppointmentSelectorFilterSelectionViewModel filterSelections, bool getOnlyDischargedEncounters = false);
    }

    internal class AppointmentSelectorViewService : BaseViewService, IAppointmentSelectorViewService
    {
        private readonly IMapper<UserAppointment, AppointmentSelectorResultViewModel>
            _appointmentSelectorResultViewModelMapper;

        private readonly Func<AppointmentSelectorFilterViewModel> _createSearchFilter;

        public AppointmentSelectorViewService(Func<AppointmentSelectorFilterViewModel> createSearchFilter, IMapper<UserAppointment, AppointmentSelectorResultViewModel>
                                                                                                               appointmentSelectorResultViewModelMapper)
        {
            _createSearchFilter = createSearchFilter;
            _appointmentSelectorResultViewModelMapper = appointmentSelectorResultViewModelMapper;
        }

        #region IAppointmentSelectorViewService Members

        public AppointmentSelectorFilterViewModel LoadSearchFilterInformation()
        {
            AppointmentSelectorFilterViewModel viewModel = _createSearchFilter();

            ObservableCollection<NamedViewModel> appointmentTypes = null;
            ObservableCollection<NamedViewModel> locations = null;
            ObservableCollection<NamedViewModel> resources = null;

            var a4 = new Action(() => resources = PracticeRepository.Users.OfType<Doctor>().Where(u => !u.IsArchived && u.IsSchedulable).OrderBy(u => u.UserName).Select(u => new NamedViewModel {Id = u.Id, Name = u.UserName}).ToExtendedObservableCollection());
            var a3 = new Action(() => locations = PracticeRepository.ServiceLocations.Where(sl => !sl.IsArchived).OrderBy(sl => sl.ShortName).Select(sl => new NamedViewModel {Id = sl.Id, Name = sl.ShortName}).ToExtendedObservableCollection());
            var a1 = new Action(() => appointmentTypes = PracticeRepository.AppointmentTypes.Where(at => at.AppointmentCategoryId != null && !at.IsArchived).OrderBy(i => i.OrdinalId).ThenBy(i => i.Name).Select(at => new NamedViewModel {Id = at.Id, Name = at.Name}).ToExtendedObservableCollection());


            new[] {a1, a3, a4}.ForAllInParallel(a => a());

            viewModel.ScheduledDoctors = resources;
            viewModel.ServiceLocations = locations;
            viewModel.AppointmentTypes = appointmentTypes;

            return viewModel;
        }

        public IEnumerable<AppointmentSelectorResultViewModel> SearchAppointments(AppointmentSelectorFilterSelectionViewModel filterSelections, bool getOnlyDischargedEncounters = false)
        {
            IQueryable<UserAppointment> appointments = PracticeRepository.Appointments.OfType<UserAppointment>()
                                                                         .Include(ap => ap.Encounter.Patient)
                                                                         .Include(ap => ap.User)
                                                                         .Include(ap => ap.AppointmentType);

            if (appointments != null)
            {
                //Filter the results based on selections
                if (filterSelections.StartDate.HasValue)
                {
                    appointments = appointments.Where(ap => ap.DateTime >= filterSelections.StartDate.Value.Date);
                }

                if (filterSelections.EndDate.HasValue)
                {
                    appointments = appointments.Where(ap => ap.DateTime <= filterSelections.EndDate);
                }

                if (filterSelections.SelectedScheduledDoctors.IsNotNullOrEmpty())
                {
                    // need to do this in memory because the sql that entity framework produces is very nested and causes timeouts on the server
                    List<int> ids = filterSelections.SelectedScheduledDoctors.Select(y => y.Id).ToList();
                    appointments = appointments.Where(ap => ids.Contains(ap.UserId));
                }
                if (filterSelections.SelectedServiceLocations.IsNotNullOrEmpty())
                {
                    List<int> ids = filterSelections.SelectedServiceLocations.Select(y => y.Id).ToList();
                    appointments = appointments.Where(ap => ids.Contains(ap.Encounter.ServiceLocationId));
                }

                if (filterSelections.SelectedAppointmentTypes.IsNotNullOrEmpty())
                {
                    List<int> ids = filterSelections.SelectedAppointmentTypes.Select(y => y.Id).ToList();
                    appointments = appointments.Where(ap => ids.Contains(ap.AppointmentTypeId));
                }

                UserAppointment[] results = appointments.ToArray();

                if (getOnlyDischargedEncounters)
                    results = appointments.Where(ap => ap.Encounter.EncounterStatusId == (int) EncounterStatus.Discharged).ToArray();
               
                PracticeRepository.AsQueryableFactory().Load(results,
                                                             ap => ap.Encounter
                                                             ,
                                                             ap =>
                                                             ap.Encounter.Patient.ExternalProviders.Select(
                                                                 ep => ep.ExternalProvider)
                                                             , ap => ap.Encounter.ServiceLocation);

                AppointmentSelectorResultViewModel[] viewModel =
                    results.Select(app => _appointmentSelectorResultViewModelMapper.Map(app)).ToArray();
                return viewModel;
            }
            return null;
        }

        #endregion
    }

    /// <summary>
    ///   Maps a Appointments to a AppointmentSelectorResultViewModel.
    /// </summary>
    internal class AppointmentSelectorResultViewModelMap : IMap<UserAppointment, AppointmentSelectorResultViewModel>
    {
        private readonly IMapMember<UserAppointment, AppointmentSelectorResultViewModel>[] _members;

        public AppointmentSelectorResultViewModelMap()
        {
            _members = this
                .CreateMembers(source =>
                               new AppointmentSelectorResultViewModel
                                   {
                                       Id = source.Id,
                                       LastName = source.Encounter.Patient.LastName,
                                       FirstName = source.Encounter.Patient.FirstName,
                                       PatientId = source.Encounter.PatientId,
                                       AppointmentDate = source.DateTime,
                                       ScheduledDoctor = source.User.DisplayName,
                                       ServiceLocation = source.Encounter.ServiceLocation.ShortName,
                                       AppointmentType = source.AppointmentType.Name,
                                       ReferringDoctor =
                                           source.Encounter.Patient.ExternalProviders.Where(e => e.IsReferringPhysician).Select(
                                               e =>
                                               e.ExternalProvider != null
                                                   ? e.ExternalProvider.DisplayName
                                                   : string.Empty).FirstOrDefault(),
                                       PrimaryCarePhysician = source.Encounter.Patient.ExternalProviders.Where(e => e.IsPrimaryCarePhysician).Select(
                                           e =>
                                           e.ExternalProvider != null
                                               ? e.ExternalProvider.DisplayName
                                               : string.Empty).FirstOrDefault()
                                   }).ToArray();
        }

        #region IMap<UserAppointment,AppointmentSelectorResultViewModel> Members

        public IEnumerable<IMapMember<UserAppointment, AppointmentSelectorResultViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}