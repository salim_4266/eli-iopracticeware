﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.FeeScheduleContracts;
using IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.FeeScheduleContracts;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Soaf.Linq;
using Soaf.Collections;
using IO.Practiceware.Presentation.ViewModels.Common;

[assembly: Component(typeof(FeeScheduleContractsViewService), typeof(IFeeScheduleContractsViewService))]
namespace IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.FeeScheduleContracts
{
    public interface IFeeScheduleContractsViewService
    {
        ManageFeeScheduleContractsViewModel LoadInfo();
    }

    public class FeeScheduleContractsViewService : BaseViewService, IFeeScheduleContractsViewService
    {
        private IMapper<FeeSchedule, FeeScheduleContractViewModel> _feeSchedulesMapper;
        private readonly Func<ManageFeeScheduleContractsViewModel> _createManageFeeScheduleContractsViewModel;
        private readonly Func<FeeScheduleContractsViewModel> _cerateFeeScheduleContractsViewModel;
        private readonly Func<DataListsViewModel> _createDataListsViewModel;

        public FeeScheduleContractsViewService(Func<ManageFeeScheduleContractsViewModel> createManageFeeScheduleContractsViewModel, Func<FeeScheduleContractsViewModel> cerateFeeScheduleContractsViewModel,
            Func<DataListsViewModel> createDataListsViewModel)
        {
            InitMappers();
            _createManageFeeScheduleContractsViewModel = createManageFeeScheduleContractsViewModel;
            _cerateFeeScheduleContractsViewModel = cerateFeeScheduleContractsViewModel;
            _createDataListsViewModel = createDataListsViewModel;
        }

        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            _feeSchedulesMapper = mapperFactory.CreateMapper<FeeSchedule, FeeScheduleContractViewModel>(feeSchedule => new FeeScheduleContractViewModel
            {
                Id = feeSchedule.Id,
                Name = feeSchedule.Name,
                StartDate = feeSchedule.StartDate,
                EndDate = feeSchedule.EndDate,
                Description = feeSchedule.Description,
                PaidPercent = feeSchedule.Paid
            });
        }

        public ManageFeeScheduleContractsViewModel LoadInfo()
        {
            var manageFeeScheduleContractsViewModel = _createManageFeeScheduleContractsViewModel();

            //  view data loading
            manageFeeScheduleContractsViewModel.FeeScheduleContractsView = _cerateFeeScheduleContractsViewModel();
            manageFeeScheduleContractsViewModel.FeeScheduleContractsView.Contracts = LoadFeeSchedules();
            manageFeeScheduleContractsViewModel.FeeScheduleContractsView.NewFeeScheduleName = string.Empty;

            // data list loading part
            manageFeeScheduleContractsViewModel.DataLists = _createDataListsViewModel();
            manageFeeScheduleContractsViewModel.DataLists.Insurers = LoadInsurers(50);
            manageFeeScheduleContractsViewModel.DataLists.ServiceLocations = LoadServiceLocations("provider", 50);
            manageFeeScheduleContractsViewModel.DataLists.Providers = LoadServiceLocations("location", 50);

            return manageFeeScheduleContractsViewModel;
        }

        public ObservableCollection<FeeScheduleContractViewModel> LoadFeeSchedules()
        {
            var feeSchedules = PracticeRepository.FeeSchedules.ToList();

            PracticeRepository.AsQueryableFactory().Load(feeSchedules,
                fee => fee.FeeScheduleInsurers);

            // mapping to viewmodels
            var feeSchedulesViewModels = _feeSchedulesMapper.MapAll(feeSchedules).ToObservableCollection();

            return feeSchedulesViewModels;
        }

        private ObservableCollection<DetailedInsurerViewModel> LoadInsurers(int n)
        {
            var toReturn = new ObservableCollection<DetailedInsurerViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new DetailedInsurerViewModel
                {
                    Id = i,
                    InsurerName = "insurer" + i % 5,
                    PlanName = "plan " + i % 10,
                    PlanType = new NamedViewModel { Name = "PlanType", Id = i % 2 },
                    Address = new AddressViewModel
                    {
                        Line1 = "line 1",
                        Line2 = "line 2",
                        City = "City",
                        StateOrProvince = new StateOrProvinceViewModel
                        {
                            Abbreviation = "ST",
                        },
                        PostalCode = "000000"
                    },
                    PhoneNumber = new PhoneNumberViewModel
                    {
                        AreaCode = "1123",
                        ExchangeAndSuffix = "1231234",
                        Extension = "123"
                    }
                });
            }
            return toReturn;
        }

        private ObservableCollection<NamedViewModel> LoadServiceLocations(String name, int n)
        {
            var toReturn = new ObservableCollection<NamedViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new NamedViewModel { Name = name + " " + i, Id = i });
            }
            return toReturn;
        }

        private ObservableCollection<InsurerFeeScheduleContractViewModel> LoadInsurerContracts(int n)
        {
            var toReturn = new ObservableCollection<InsurerFeeScheduleContractViewModel>();
            for (var i = 0; i < n; i++)
            {
                toReturn.Add(new InsurerFeeScheduleContractViewModel
                {
                    Insurer = new DetailedInsurerViewModel
                    {
                        Id = i,
                        InsurerName = "insurer" + i % 5,
                        PlanName = "plan " + i % 10,
                        PlanType = new NamedViewModel { Name = "PlanType", Id = i % 2 },
                        Address = new AddressViewModel
                        {
                            Line1 = "line 1",
                            Line2 = "line 2",
                            City = "City",
                            StateOrProvince = new StateOrProvinceViewModel
                            {
                                Abbreviation = "ST",
                            },
                            PostalCode = "000000"
                        },
                        PhoneNumber = new PhoneNumberViewModel
                        {
                            AreaCode = "1123",
                            ExchangeAndSuffix = "1231234",
                            Extension = "123"
                        }
                    },
                    ServiceLocation = (i % 3 == 0) ? new NamedViewModel { Name = "location " + i % 3, Id = i } : null,
                    Provider = (i % 4 == 0) ? new NamedViewModel { Name = "provider " + i % 4, Id = i } : null
                });
            }
            return toReturn;
        }
    }
}
