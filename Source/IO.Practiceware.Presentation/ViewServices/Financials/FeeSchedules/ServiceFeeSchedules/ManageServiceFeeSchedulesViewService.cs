﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.FeeSchedules.ServiceFeeSchedules;
using IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.ServiceFeeSchedules;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(ManageServiceFeeSchedulesViewService), typeof(IManageServiceFeeSchedulesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.FeeSchedules.ServiceFeeSchedules
{
    public interface IManageServiceFeeSchedulesViewService
    {
        ManageServicesViewModel LoadInformation();
        IList<ServiceDetailsViewModel> LoadServices(IList<NamedViewModel> selectedCategories, bool showExpired);
    }
    
    public class ManageServiceFeeSchedulesViewService : BaseViewService, IManageServiceFeeSchedulesViewService
    {
        public ManageServicesViewModel LoadInformation()
        {
            var encounterServiceTypes = PracticeRepository.EncounterServiceTypes.Where(est => !est.IsArchived).ToList()
                                        .Select(s=> new NamedViewModel { Id = s.Id, Name = s.Name });

            return new ManageServicesViewModel
                   {
                       EncounterServiceCategories = encounterServiceTypes.ToExtendedObservableCollection()
                   };
        }

        public IList<ServiceDetailsViewModel> LoadServices(IList<NamedViewModel> selectedCategories, bool showExpired)
        {
            var encounterServices = PracticeRepository.EncounterServices.Include(es => es.EncounterServiceType).Where(es => !es.IsArchived & es.EncounterServiceTypeId != null).ToList();

            if (selectedCategories.Any())
            {
                var categoryIds = selectedCategories.Select(c => c.Id).ToList();
                encounterServices = encounterServices.Where(es => es.EncounterServiceTypeId != null && categoryIds.Contains((int) es.EncounterServiceTypeId)).ToList();
            }
            encounterServices = showExpired ? encounterServices.Where(es => es.EndDateTime < DateTime.Now).ToList() : encounterServices.Where(es => es.EndDateTime == null || es.EndDateTime >= DateTime.Now).ToList();
            return encounterServices.Select(s => new ServiceDetailsViewModel { Id = s.Id, Code = s.Code, Description = s.Description, Category = new NamedViewModel { Id = s.EncounterServiceTypeId ?? 0, Name = s.EncounterServiceType.Name} }).ToList();
        }
    }
}