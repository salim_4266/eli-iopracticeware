﻿using IO.Practiceware.Integration.Common;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;
using IO.Practiceware.Presentation.ViewServices.Financials.Common;
using IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims.PendingClaims;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using InvoiceViewModel = IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims.InvoiceViewModel;
using PendingClaimsViewModels = IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims.PendingClaims;

[assembly: Component(typeof(PendingClaimsViewService), typeof(PendingClaimsViewService))]
namespace IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims.PendingClaims
{
    public interface IPendingClaimsViewService
    {
        PendingClaimsLoadInformation LoadInformation();
        ObservableCollection<InvoiceViewModel> GetInvoices(FilterViewModel filter, out int count);
        InvoiceViewModel LoadInvoice(int id);
        IDictionary<int, IList<ValidationMessage>> ValidateInvoices(IList<int> invoiceIds);
        void BillInvoices(ICollection<InvoiceViewModel> selectedInvoices);
    }

    public class PendingClaimsViewService : BaseViewService, IPendingClaimsViewService
    {
        private readonly Func<PendingClaimsLoadInformation> _pendingClaimsLoadInformation;
        private readonly IMapper<EncounterService, ServiceViewModel> _encounterServiceMapper;
        private readonly InvoiceValidationUtility _validationUtility;
        private readonly IMapper<BillingDiagnosis, DiagnosisViewModel> _diagnosisMapper;
        private readonly IMapper<Invoice, CareTeamViewModel> _careTeamMapper;

        private IMapper<Invoice, InvoiceViewModel> _invoiceMapper;
        private IMapper<Tuple<BillingService, Invoice>, PendingClaimsViewModels.EncounterServiceViewModel> _billingServiceServiceMapper;
        private IMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel> _billingModifierMapper;
        private IMapper<Invoice, PatientViewModel> _patientNamedViewModelMapper;
        private IMapper<ServiceLocation, NamedViewModel> _locationNamedViewMapper;
        private IMapper<Model.InsurancePolicy, InsurerViewModel> _insurerMapper;
        private IMapper<InsurerAddress, AddressViewModel> _insurerAddressMapper;
        private IMapper<Model.InsurancePolicy, PolicyHolderViewModel> _insurerPolicyHolderMapper;

        public PendingClaimsViewService(Func<PendingClaimsLoadInformation> pendingClaimsLoadInformation,
            IMapper<Invoice, CareTeamViewModel> careTeamMapper,
            IMapper<BillingDiagnosis, DiagnosisViewModel> daignosisMapper,
            IMapper<EncounterService, ServiceViewModel> encounterServiceMapper,
            InvoiceValidationUtility validationUtility)
        {
            _pendingClaimsLoadInformation = pendingClaimsLoadInformation;
            _careTeamMapper = careTeamMapper;
            _diagnosisMapper = daignosisMapper;
            _encounterServiceMapper = encounterServiceMapper;
            _validationUtility = validationUtility;
            InitMappers();
        }

        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            //Used in insurer mapper
            _insurerAddressMapper = mapperFactory.CreateAddressViewModelMapper<InsurerAddress>();

            _insurerPolicyHolderMapper = mapperFactory.CreateMapper<Model.InsurancePolicy, PolicyHolderViewModel>(insurerPolicy => new PolicyHolderViewModel
            {
                FirstName = insurerPolicy.PolicyholderPatient.FirstName,
                LastName = insurerPolicy.PolicyholderPatient.LastName,
                MiddleName = insurerPolicy.PolicyholderPatient.LastName,
                FormattedName = insurerPolicy.PolicyholderPatient.DisplayName,
                Id = insurerPolicy.PolicyholderPatient.Id
            });

            //Used in Patient mapper
            _insurerMapper = mapperFactory.CreateMapper<Model.InsurancePolicy, InsurerViewModel>(policy => new InsurerViewModel
            {
                Id = policy.Insurer.Id,
                InsurerName = policy.Insurer.Name,
                PlanName = policy.Insurer.PlanName,
                Address = policy.Insurer.InsurerAddresses.Where(x => x.InsurerAddressTypeId == (int)InsurerAddressTypeId.Claims).Select(x => _insurerAddressMapper.Map(x)).FirstOrDefault(),
                PolicyHolder = _insurerPolicyHolderMapper.Map(policy)
            });

            _billingModifierMapper = mapperFactory.CreateMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel>(billingServiceModifier => new NameAndAbbreviationAndDetailViewModel
            {
                Name = billingServiceModifier.ServiceModifier.Description,
                Abbreviation = billingServiceModifier.ServiceModifier.Code,
                Id = billingServiceModifier.ServiceModifier.Id
            });

            _locationNamedViewMapper = mapperFactory.CreateMapper<ServiceLocation, NamedViewModel>(location => new NamedViewModel
            {
                Name = location.ShortName,
                Id = location.Id,
            });

            _patientNamedViewModelMapper = mapperFactory.CreateMapper<Invoice, PatientViewModel>(invoice => new PatientViewModel
            {
                FirstName = invoice.Encounter.Patient.FirstName,
                LastName = invoice.Encounter.Patient.LastName,
                MiddleName = invoice.Encounter.Patient.MiddleName,
                Id = invoice.Encounter.Patient.Id,
                FormattedName = invoice.Encounter.Patient.DisplayName,
                PrimaryInsurer = invoice.InvoiceReceivables.Where(x => x.OpenForReview).Any(x => x.PatientInsurance != null) ? invoice.InvoiceReceivables.Where(x => x.OpenForReview && x.PatientInsurance != null)
                                                                                                                                                         .Select(x => _insurerMapper.Map(x.PatientInsurance.InsurancePolicy))
                                                                                                                                                         .FirstOrDefault()
                                                                                                                               : null
            });

            _billingServiceServiceMapper = mapperFactory.CreateMapper<Tuple<BillingService, Invoice>, PendingClaimsViewModels.EncounterServiceViewModel>(billingService => new PendingClaimsViewModels.EncounterServiceViewModel
            {
                Diagnoses = billingService.Item1.BillingServiceBillingDiagnoses.OrderBy(x => x.OrdinalId).Select(x => _diagnosisMapper.Map(x.BillingDiagnosis)).ToObservableCollection(),
                Id = billingService.Item1.Id,
                Modifiers = billingService.Item1.BillingServiceModifiers.Select(x => _billingModifierMapper.Map(x)).ToExtendedObservableCollection(),
                Service = billingService.Item1.EncounterService != null ? _encounterServiceMapper.Map(billingService.Item1.EncounterService) : new ServiceViewModel(),
                Units = billingService.Item1.Unit.GetValueOrDefault(),
                Charge = billingService.Item1.UnitCharge.GetValueOrDefault(),
                AdjustmentAmount = billingService.Item1.Adjustments.Where(x => x.AdjustmentType != null).Sum(a => a.AdjustmentType.IsDebit ? a.Amount * -1 : a.Amount),
                Allowed = billingService.Item1.Unit * billingService.Item1.UnitAllowableExpense,
                InvoiceId = billingService.Item2.Id,
                Date = billingService.Item2.Encounter.StartDateTime
            });

            _invoiceMapper = mapperFactory.CreateMapper<Invoice, InvoiceViewModel>(invoice => new InvoiceViewModel
            {
                Id = invoice.Id,
                DateOfService = invoice.Encounter != null ? invoice.Encounter.StartDateTime : (DateTime?)null,
                Services = invoice.BillingServices.OrderBy(x => x.OrdinalId).Select(x => _billingServiceServiceMapper.Map(Tuple.Create(x, invoice))).ToExtendedObservableCollection(),
                CareTeam = _careTeamMapper.Map(invoice),
                Patient = _patientNamedViewModelMapper.Map(invoice),
                ServiceLocation = _locationNamedViewMapper.Map(invoice.Encounter.ServiceLocation),
                IsValidated = false,
                OpenForReview = invoice.InvoiceReceivables.Any(x => x.OpenForReview)
            });
        }

        public PendingClaimsLoadInformation LoadInformation()
        {
            var loadInfo = _pendingClaimsLoadInformation();

            loadInfo.DataLists = new DataListsViewModel();
            new Action[]
                {
                    () =>
                    {
                        loadInfo.StartDate = GetOldestDateOfService();
                    },
                    () =>
                    {
                        loadInfo.DataLists.ServiceLocations = PracticeRepository.ServiceLocations
                                                                                    .Select(x => new NamedViewModel {Id = x.Id, Name = x.ShortName})
                                                                                    .ToObservableCollection();
                    },
                    () =>
                    {
                        loadInfo.DataLists.RenderingProviders = PracticeRepository.Providers.Where(x => (x.User != null && !x.User.IsArchived && x.User is Doctor) || x.ServiceLocation != null)
                                                                                                    .OrderBy(x => x.User.OrdinalId)
                                                                                                    .ThenBy(x => x.Id)
                                                                                                    .ThenBy(x => x.ServiceLocation.ShortName)
                                                                                                    .Select(x => new ProviderViewModel
                                                                                                    {
                                                                                                        
                                                                                                        Provider = new NamedViewModel
                                                                                                        {
                                                                                                            Id = x.User != null && !x.User.IsArchived ? x.User.Id : x.ServiceLocation.Id,
                                                                                                            Name = x.User != null && !x.User.IsArchived ? x.User.UserName : x.ServiceLocation.ShortName
                                                                                                            
                                                                                                        },
                                                                                                        BillingOrganization = new NamedViewModel { Id = x.BillingOrganization.Id, Name = x.BillingOrganization.ShortName },
                                                                                                        Id = x.Id,
                                                                                                        

                                                                                                    })
                                                                                                    .ToExtendedObservableCollection();
                    }
                }.ForAllInParallel(a => a());

            return loadInfo;
        }

        public ObservableCollection<InvoiceViewModel> GetInvoices(FilterViewModel filter, out int count)
        {
            var invoices = LoadInvoices(filter);

            count = invoices.Count;

            var invoiceViewModels = _invoiceMapper
                                .MapAll(invoices.OrderByDescending(x => x.Encounter.StartDateTime).Take(200))
                                .ToObservableCollection();

            return invoiceViewModels;
        }

        public IDictionary<int, IList<ValidationMessage>> ValidateInvoices(IList<int> invoiceIds)
        {
            var invoices = PracticeRepository.Invoices.Where(i => invoiceIds.Contains(i.Id)).ToList();
            _validationUtility.LoadInvoiceRelations(invoices);
            return _validationUtility.ValidateInvoices(invoices, ir => ir.OpenForReview)
                .ToDictionary(i => i.Key.Id, i => i.Value);
        }

        protected virtual IList<Invoice> LoadInvoices(ICollection<int> invoiceIds)
        {
            return LoadInvoicesInternal(invoiceIds).ToList();
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void BillInvoices(ICollection<InvoiceViewModel> selectedInvoices)
        {
            // Load all invoices
            var invoiceIds = selectedInvoices.Select(i => i.Id).ToArray();
            var invoices = LoadInvoices(invoiceIds);

            var billingServiceTransactions = new ObservableCollection<BillingServiceTransaction>();
            foreach (var selectedInvoice in selectedInvoices)
            {
                var invoice = invoices.First(i => i.Id == selectedInvoice.Id);

                if (selectedInvoice.Status.Id == (int)InvoiceStatus.Bill)
                {
                    InvoiceReceivable invoiceReceivable;
                    var methodToSent = MethodSent.Paper;
                    int? claimFileReceiverId = null;
                    var patientInsurances = invoice.Encounter.Patient.PatientInsurances.Where(x => x.InsuranceType == invoice.Encounter.InsuranceType
                                                                                        && x.IsActiveForDateTime(invoice.Encounter.StartDateTime))
                                                                                        .OrderBy(x => x.OrdinalId).ToArray();
                    var insuranceIds = patientInsurances.Select(x => x.Id);
                    if (patientInsurances.Any())
                    {
                        invoiceReceivable = invoice.InvoiceReceivables.FirstOrDefault(x => x.OpenForReview
                                                                                           && x.PatientInsurance != null
                                                                                           && insuranceIds.Contains(x.PatientInsurance.Id));

                        if (invoiceReceivable == null)
                        {
                            var invoiceReceivables = CreateInvoiceReceivables(patientInsurances, invoice);
                            invoiceReceivables.ForEach(invoice.InvoiceReceivables.Add);
                            invoiceReceivable = invoiceReceivables.First();
                        }
                        //To set the method sent and claimFileReceiverId.
                        if (invoiceReceivable.PatientInsurance != null
                            && invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFileReceiverId.HasValue
                            && invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.PayerCode != null)
                        {
                            claimFileReceiverId = invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.ClaimFileReceiverId.Value;
                            methodToSent = MethodSent.Electronic;
                        }
                    }
                    else
                    {
                        invoiceReceivable = invoice.InvoiceReceivables.First(x => x.OpenForReview) ?? CreateInvoiceReceivables(patientInsurances, invoice).First(); //Taking the first on as not patient insurances meaning only on IR will be created and that is for patient.
                        if (invoiceReceivable.Id == 0)
                        {
                            invoice.InvoiceReceivables.Add(invoiceReceivable);
                        }
                    }


                    foreach (var service in selectedInvoice.Services)
                    {
                        if ((service.Balance <= 0 && (service.TotalCharge != 0 || !service.Service.IsZeroChargeAllowedOnClaim)) || !service.Id.HasValue)
                        {
                            if (service.Service.IsZeroChargeAllowedOnClaim && invoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.SendZeroCharge)
                            {
                                if (service.AdjustmentAmount != service.TotalCharge)
                                    continue;
                            }
                            else
                                continue;
                        } 
                        var transaction = new BillingServiceTransaction
                        {
                            DateTime = DateTime.Now.ToClientTime(),
                            AmountSent = service.Balance,
                            BillingServiceTransactionStatus = BillingServiceTransactionStatus.Queued,
                            MethodSent = methodToSent,
                            BillingServiceId = service.Id.Value,
                            ClaimFileReceiverId = claimFileReceiverId,
                            InvoiceReceivable = invoiceReceivable
                        };
                        invoice.InvoiceReceivables.Where(x => x.OpenForReview).ForEachWithSinglePropertyChangedNotification(x => x.OpenForReview = false);
                        billingServiceTransactions.Add(transaction);
                    }
                }
                else
                {
                    //If the status is close, just set the open for review to false for the invoice.
                    invoice.InvoiceReceivables.Where(x => x.OpenForReview).ForEachWithSinglePropertyChangedNotification(x => x.OpenForReview = false);
                }
            }
        }

        private DateTime? GetOldestDateOfService()
        {
            return PracticeRepository.InvoiceReceivables
               .Where(x => x.OpenForReview)
               .Select(x => (DateTime?)CommonQueries.GetDatePart(x.Invoice.Encounter.StartDateTime))
               .OrderBy(x => x)
               .FirstOrDefault();
        }

        private IList<Invoice> LoadInvoices(FilterViewModel filter)
        {
            var invoiceIds = PracticeRepository.InvoiceReceivables.Where(x => x.OpenForReview).Select(x => x.InvoiceId).ToArray();

            if (filter != null)
            {
                var locationIds = filter.ServiceLocations.Select(x => x.Id).ToArray();
                var billingProviderIds = filter.RenderingProviders.Select(x => x.Id).ToArray();
                var startDate = filter.StartDate.HasValue ? filter.StartDate.Value.Date : (DateTime?)null;
                var endDate = filter.EndDate.HasValue ? filter.EndDate.Value.Date : (DateTime?)null;
                var encounterIds = PracticeRepository.Encounters.Where(x => (startDate == null || CommonQueries.GetDatePart(x.StartDateTime) >= startDate.Value)
                                                                            && (endDate == null || CommonQueries.GetDatePart(x.StartDateTime) <= endDate.Value)).Select(x => x.Id);
                //Load the invoices based on the filtering criteria.
                invoiceIds = PracticeRepository.Invoices.Where(x => invoiceIds.Contains(x.Id))
                    .Where(x => invoiceIds.Contains(x.Id) && encounterIds.Contains(x.EncounterId)
                                && (!locationIds.Any() || locationIds.Contains(x.AttributeToServiceLocationId.Value)) &&
                              (!billingProviderIds.Any() || billingProviderIds.Contains(x.BillingProviderId)))
                    .Select(x => x.Id).ToArray();
            }

            var invoices = LoadInvoicesInternal(invoiceIds);
            invoices.Where(i => i.DiagnosisTypeId != null && i.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10).ForEach(invoice => IntegrationCommon.ReplaceInvoiceWithIcd10(invoice));

            return invoices;
        }

        private IList<Invoice> LoadInvoicesInternal(IEnumerable<int> invoiceIds)
        {
            var invoices = PracticeRepository.Invoices.Where(x => invoiceIds.Contains(x.Id)).ToList();
            PracticeRepository.AsQueryableFactory().Load(invoices,
                inv => inv.ReferringExternalProvider.PatientExternalProviders.Select(x => x.ExternalProvider),
                inv => inv.InvoiceReceivables.Select(x => new
                {
                    x.PatientInsurance.InsurancePolicy.PolicyholderPatient,
                    x.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses,
                    Invoice = x.Invoice.ReferringExternalProvider.PatientExternalProviders.Select(y => y.ExternalProvider)
                }),
                inv => inv.BillingServices.Select(x => new
                {
                    BillingServiceBillingDiagnosis = x.BillingServiceBillingDiagnoses
                                                      .Select(bsbd => bsbd.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntity),
                    BillingServiceModifiers = x.BillingServiceModifiers
                                              .Select(y => y.ServiceModifier),
                    x.EncounterService,
                    x.FacilityEncounterService.RevenueCode,
                    Adjustments = x.Adjustments.Select(y => y.AdjustmentType)
                }),
                inv => inv.BillingProvider.BillingOrganization.IdentifierCodes,
                inv => inv.BillingProvider.User,
                inv => inv.Encounter.ServiceLocation,
                inv => inv.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.User),
                inv => inv.Encounter.Patient.PatientAddresses.Select(y => y.StateOrProvince),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer),
                inv => inv.Encounter.PatientInsuranceReferrals,
                inv => inv.Encounter.PatientInsuranceAuthorizations,
                inv => inv.AttributeToServiceLocation,
                inv => inv.DiagnosisType);
            return invoices.ToList();
        }

        public InvoiceViewModel LoadInvoice(int id)
        {
            int[] invoices = { id };
            var invoice = LoadInvoicesInternal(invoices).Single();

            if (invoice != null && invoice.DiagnosisTypeId != null && invoice.DiagnosisTypeId == (int) DiagnosisTypeId.Icd10)
            {
                IntegrationCommon.ReplaceInvoiceWithIcd10(invoice);
            }
            return _invoiceMapper.Map(invoice);
        }

        private List<InvoiceReceivable> CreateInvoiceReceivables(IList<PatientInsurance> insurances, Invoice invoice)
        {
            var dbInvoiceReceivables = new List<InvoiceReceivable>();

            if (insurances.Any())
            {
                foreach (var patientInsurance in insurances)
                {
                    //To avoid the ReSharper warning access to modified closure.
                    PatientInsurance insurance = patientInsurance;
                    var dbInvoiceReceivable = new InvoiceReceivable
                    {
                        PatientInsuranceId = insurance.Id,
                        OpenForReview = false,
                        PatientInsuranceReferralId = invoice.Encounter.PatientInsuranceReferrals.Any(x => x.PatientInsuranceId == insurance.Id) ? invoice.Encounter.PatientInsuranceReferrals.Where(x => x.PatientInsuranceId == insurance.Id).Select(x => x.Id).First() : (int?)null,
                        PatientInsuranceAuthorizationId = invoice.Encounter.PatientInsuranceAuthorizations.Any(x => x.PatientInsuranceId == insurance.Id) ? invoice.Encounter.PatientInsuranceAuthorizations.Where(x => x.PatientInsuranceId == insurance.Id).Select(x => x.Id).First() : (int?)null,
                    };
                    dbInvoiceReceivables.Add(dbInvoiceReceivable);
                }
            }
            else
            {
                var dbPatientInvoiceReceivable = new InvoiceReceivable
                {
                    OpenForReview = false,
                };
                dbInvoiceReceivables.Add(dbPatientInvoiceReceivable);
            }
            return dbInvoiceReceivables;
        }
    }
}

