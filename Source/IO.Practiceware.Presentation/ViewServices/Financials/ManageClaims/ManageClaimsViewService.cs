﻿using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.ManageClaims;
using IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims;
using Soaf.ComponentModel;

[assembly: Component(typeof (ManageClaimsViewService), typeof (IManageClaimsViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.ManageClaims
{
    public interface IManageClaimsViewService
    {
        ManageClaimsViewModel LoadClaimsInformation();
    }

    public class ManageClaimsViewService : BaseViewService, IManageClaimsViewService
    {
        public ManageClaimsViewModel LoadClaimsInformation()
        {
            // Gets the list of pending claims amounts for each invoice
           var amounts =
                (from invoice in PracticeRepository.Invoices
                    join billingService in PracticeRepository.BillingServices on invoice.Id equals billingService.InvoiceId into billingServices
                    from billingService in billingServices.DefaultIfEmpty()
                    let adjustmentAmount = billingService.Adjustments.Where(x => x.AdjustmentType != null).Sum(a => a.AdjustmentType.IsDebit ? a.Amount * -1 : a.Amount)
                    where invoice.InvoiceReceivables.Any(ir => ir.OpenForReview)
                    select new
                           {
                               Amount = (billingService.Unit * billingService.UnitCharge) - adjustmentAmount,
                               InvoiceId = invoice.Id
                           });

            var pendingClaims = new ClaimsViewModel
                                {
                                    Balance = amounts.Sum(a => a.Amount),
                                    Count = amounts.Select(a => a.InvoiceId).Distinct().Count()
                                };

            // Gets the list of sent claims with amounts for each invoice
            amounts =
                (from invoice in PracticeRepository.Invoices
                    join billingService in PracticeRepository.BillingServices on invoice.Id equals billingService.InvoiceId into billingServices
                    from billingService in billingServices.DefaultIfEmpty()
                    let adjustmentAmount = billingService.Adjustments.Where(x => x.AdjustmentType != null).Sum(a => a.AdjustmentType.IsDebit ? a.Amount * -1 : a.Amount)
                    where invoice.InvoiceReceivables.Any(ir => ir.BillingServiceTransactions
                        .Any(bst => bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Sent || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Wait || bst.BillingServiceTransactionStatus == BillingServiceTransactionStatus.SentCrossOver))
                    select new
                           {
                               Amount = (billingService.Unit * billingService.UnitCharge) - adjustmentAmount,
                               InvoiceId = invoice.Id
                           });

            var sentClaims = new ClaimsViewModel
                            {
                                Balance = amounts.Sum(a => a.Amount),
                                Count = amounts.Select(a => a.InvoiceId).Distinct().Count()
                            };

            // Gets the list of unbilled claims with amounts for each invoice
            amounts =
                (from invoice in PracticeRepository.Invoices
                    join billingService in PracticeRepository.BillingServices on invoice.Id equals billingService.InvoiceId into billingServices
                    from billingService in billingServices.DefaultIfEmpty()
                    let adjustmentAmount = billingService.Adjustments.Where(x => x.AdjustmentType != null).Sum(a => a.AdjustmentType.IsDebit ? a.Amount * -1 : a.Amount)
                    where invoice.InvoiceReceivables.Any(ir => !ir.OpenForReview)
                          && (invoice.InvoiceReceivables.Any(ir => ir.BillingServiceTransactions.Any(bst => bst.AmountSent != 0))
                              || invoice.InvoiceReceivables.Any(ir => !ir.BillingServiceTransactions.Any()))
                    select new
                           {
                               Amount = (billingService.Unit * billingService.UnitCharge) - adjustmentAmount,
                               InvoiceId = invoice.Id
                           });

            var unbilledClaims = new ClaimsViewModel
                                {
                                    Balance = amounts.Sum(a => a.Amount),
                                    Count = amounts.Select(a => a.InvoiceId).Distinct().Count()
                                };

            return new ManageClaimsViewModel
                   {
                       PendingClaims = pendingClaims,
                       SentClaims = sentClaims,
                       UnbilledClaims = unbilledClaims
                   };
        }
    }
}