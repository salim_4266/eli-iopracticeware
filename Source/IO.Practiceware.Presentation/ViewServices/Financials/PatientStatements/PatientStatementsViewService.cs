﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Financials.PatientStatements;
using IO.Practiceware.Presentation.ViewServices.Financials.PatientStatements;
using Soaf.Collections;
using Soaf.Linq;
using Soaf.ComponentModel;

[assembly: Component(typeof(PatientStatementsViewService), typeof(IPatientStatementsViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.PatientStatements
{
    public interface IPatientStatementsViewService
    {
        AccountViewModel LoadPatientStatementsInformation(int patientId);
    }

    public class PatientStatementsViewService : BaseViewService, IPatientStatementsViewService
    {

        public AccountViewModel LoadPatientStatementsInformation(int patientId)
        {
            var loadInformation = new AccountViewModel();

            Patient patientRecord = PracticeRepository.Patients.WithId(patientId);
            new Action[]
            {
                () => loadInformation.Id = patientRecord.Id,
                () => loadInformation.Name = patientRecord.DisplayName,
                () => loadInformation.Statements = LoadStatements(patientRecord.Id)
            }
                .ForAllInParallel(a => a());

            return loadInformation;
        }

        private ObservableCollection<StatementViewModel> LoadStatements(int patientId)
        {
            var results = (from esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities
                .Where(e => e.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.BillingServiceTransaction
                            && e.ExternalSystemMessage.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.PatientStatement_Outbound)
                           join bst in PracticeRepository.BillingServiceTransactions.Where(bst => bst.BillingService.Invoice.Encounter.PatientId == patientId) on esmpre.PracticeRepositoryEntityKey equals CommonQueries.ConvertGuidToString(bst.Id)
                           select new
                           {
                               esmpre.ExternalSystemMessageId,
                               //Date = esmpre.ExternalSystemMessage.CreatedDateTime,
                               Date = bst.DateTime,
                               esmpre.ExternalSystemMessage.Value,
                               Amount = bst.AmountSent
                           })
                .GroupBy(m => m.ExternalSystemMessageId)
                .Select(m => new { m.FirstOrDefault().Date, m.FirstOrDefault().Value, Amount = m.Sum(k => k.Amount) })
                .ToArray()
                .OrderByDescending(m => m.Date)
                .Select(x => new StatementViewModel
                {
                    Date = x.Date,
                    Value = Convert.FromBase64String(x.Value),
                    Amount = x.Amount
                }).ToObservableCollection();

            return results;
        }
    }
}