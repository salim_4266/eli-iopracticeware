﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewServices.Financials.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;

[assembly: Component(typeof(CareTeamMap), typeof(IMap<Invoice, CareTeamViewModel>))]
[assembly: Component(typeof(DiagnosisMap), typeof(IMap<BillingDiagnosis, DiagnosisViewModel>))]
[assembly: Component(typeof(EncounterServiceMap), typeof(IMap<EncounterService, ServiceViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.Common
{
    public class CareTeamMap : IMap<Invoice, CareTeamViewModel>
    {
        private readonly IEnumerable<IMapMember<Invoice, CareTeamViewModel>> _members;

        public IEnumerable<IMapMember<Invoice, CareTeamViewModel>> Members
        {
            get { return _members; }
        }

        public CareTeamMap()
        {
            _members = this.CreateMembers(invoice => new CareTeamViewModel
            {
                ScheduledProvider = new NamedViewModel
                {
                    Id = invoice.Encounter.Appointments.Any() ? invoice.Encounter.Appointments.OfType<UserAppointment>().Where(x => x.User != null).Select(x => x.User.Id).FirstOrDefault() : 0,
                    Name = invoice.Encounter.Appointments.Any() ? invoice.Encounter.Appointments.OfType<UserAppointment>().Where(x => x.User != null).Select(x => x.User.UserName).FirstOrDefault() : string.Empty
                },
                RenderingProvider = new ProviderViewModel
                {
                    BillingOrganization = invoice.BillingProvider != null && invoice.BillingProvider.BillingOrganization != null ? new NamedViewModel
                    {
                        Id = invoice.BillingProvider.BillingOrganization.Id,
                        Name = invoice.BillingProvider.BillingOrganization.ShortName
                    } : null,
                    Provider = new NamedViewModel
                    {
                        Id = (invoice.BillingProvider != null && invoice.BillingProvider.User != null) ? invoice.BillingProvider.UserId.Value : invoice.Encounter.ServiceLocationId,
                        Name = (invoice.BillingProvider != null && invoice.BillingProvider.User != null) ? invoice.BillingProvider.User.UserName : invoice.Encounter.ServiceLocation.ShortName
                    },
                    Id = invoice.BillingProviderId
                },
                ReferringProviders = invoice.ReferringExternalProvider != null ? invoice.ReferringExternalProvider.PatientExternalProviders.Where(x => x.ExternalProvider != null).Select(ep => new ExternalProviderViewModel
            {
                //Address TO DO;Need to check about the mapping
                DisplayName = ep.ExternalProvider.DisplayName,
                FirstName = ep.ExternalProvider.FirstName,
                Honorific = ep.ExternalProvider.Honorific,
                Id = ep.ExternalProvider.Id,
                LastName = ep.ExternalProvider.LastNameOrEntityName,
                MiddleName = ep.ExternalProvider.MiddleName,
                Name = ep.ExternalProvider.NickName
                //PhoneNumber TO DO;Need to check about the mapping
                //Specialty = ep.Sp TO DO;Need to check about the mapping
            }).Distinct().ToExtendedObservableCollection() : null,
            });
        }
    }

    public class DiagnosisMap : IMap<BillingDiagnosis, DiagnosisViewModel>
    {
        private readonly IEnumerable<IMapMember<BillingDiagnosis, DiagnosisViewModel>> _members;

        public DiagnosisMap()
        {
            _members = this.CreateMembers(billingDiagnosis => new DiagnosisViewModel
            {
                ActiveDiagnosisCode = new DiagnosisCodeViewModel
                {
                    Code = billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey ?? "",
                    CodeSet = DiagnosisCodeSet.Icd9,
                    Id = billingDiagnosis.ExternalSystemEntityMappingId,
                    Key = billingDiagnosis.ExternalSystemEntityMapping.PracticeRepositoryEntityKey
                }, // ICD9
                AlternateDiagnosisCode = new DiagnosisCodeViewModel
                {
                    CodeSet = DiagnosisCodeSet.Icd10,
                },//ICD10
                DiagnosisLinkPointer = new DiagnosisLinkPointerViewModel
                {
                    PointerLetter = GetDiagnosisLetter(billingDiagnosis),
                    DiagnosisColor = GetDiagnosisHighlightColor(billingDiagnosis)
                },
                IsLinked = billingDiagnosis.BillingServiceBillingDiagnoses != null, //billing diagnoses billing service billing diagnoses if it's not linked to a service
            }); // all
        }

        public IEnumerable<IMapMember<BillingDiagnosis, DiagnosisViewModel>> Members
        {
            get { return _members; }
        }

        /// <summary>
        /// Gets the diagnosis letter.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <returns></returns>
        public static string GetDiagnosisLetter(BillingDiagnosis diagnosis)
        {
            var ordinalId = diagnosis.IfNotNull(d => d.OrdinalId.GetValueOrDefault());
            if (ordinalId < 1 || ordinalId > 12)
            {
                return null;
            }

            return OrdinalIdToLetterBrushMapping[ordinalId].Item1;
        }

        /// <summary>
        /// Gets the color of the diagnosis highlight.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <returns></returns>
        public static string GetDiagnosisHighlightColor(BillingDiagnosis diagnosis)
        {
            var ordinalId = diagnosis.IfNotNull(d => d.OrdinalId.GetValueOrDefault());
            if (ordinalId < 1 || ordinalId > 12)
            {
                return null;
            }

            return OrdinalIdToLetterBrushMapping[ordinalId].Item2;
        }

        /// <summary>
        /// Letters and colors for diagnoses
        /// </summary>
        private static readonly Dictionary<int, Tuple<string, string>> OrdinalIdToLetterBrushMapping = new Dictionary<int, Tuple<string, string>>
        {
            {1, new Tuple<string, string>("A", "#FF00CD7F")},
            {2, new Tuple<string, string>("B", "#FF00CDCD")},
            {3, new Tuple<string, string>("C", "#FF00AFFF")},
            {4, new Tuple<string, string>("D", "#FF6478FF")},
            {5, new Tuple<string, string>("E", "#FFBB78FF")},
            {6, new Tuple<string, string>("F", "#FFF57AE8")},
            {7, new Tuple<string, string>("G", "#FFDE6498")},
            {8, new Tuple<string, string>("H", "#FFFA7776")},
            {9, new Tuple<string, string>("I", "#FFEA7460")},
            {10, new Tuple<string, string>("J", "#FFFDA46C")},
            {11, new Tuple<string, string>("K", "#FFE5B844")},
            {12, new Tuple<string, string>("L", "#FFC0D674")},
        };

    }

    public class EncounterServiceMap : IMap<EncounterService, ServiceViewModel>
    {
        private readonly IEnumerable<IMapMember<EncounterService, ServiceViewModel>> _members;

        public EncounterServiceMap()
        {
            _members = this.CreateMembers(encounterService => new ServiceViewModel
            {
                Code = !string.IsNullOrEmpty(encounterService.Code) ? encounterService.Code : String.Empty,
                Id = encounterService.Id,
                Description = !string.IsNullOrEmpty(encounterService.Description) ? encounterService.Description : String.Empty,
                IsZeroChargeAllowedOnClaim = encounterService.IsZeroChargeAllowedOnClaim,
                UnitFee = encounterService.UnitFee
            });
        }

        public IEnumerable<IMapMember<EncounterService, ServiceViewModel>> Members
        {
            get { return _members; }
        }
    }
}
