﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Linq;
using IO.Practiceware.Services.Icd;

namespace IO.Practiceware.Presentation.ViewServices.Financials.Common
{
    public class InvoiceValidationUtility
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly IIcd10Service _icd10Service;

        public InvoiceValidationUtility(IPracticeRepository practiceRepository, IIcd10Service icd10Service)
        {
            _practiceRepository = practiceRepository;
            _icd10Service = icd10Service;
        }

        public void LoadInvoiceRelations(IList<Invoice> invoices)
        {
            _practiceRepository.AsQueryableFactory().Load(invoices,
                inv => inv.ReferringExternalProvider.PatientExternalProviders.Select(x => x.ExternalProvider),
                inv => inv.ReferringExternalProvider.IdentifierCodes,
                inv => inv.BillingDiagnoses.Select(x => x.ExternalSystemEntityMapping.ExternalSystemEntity),
                inv => inv.InvoiceReceivables.Select(x => new
                {
                    PatientInsurance = x.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses.Select(a => a.StateOrProvince.Country),
                    Invoice = x.Invoice.ReferringExternalProvider.PatientExternalProviders.Select(y => y.ExternalProvider),
                    x.PatientInsuranceReferral
                }),
                inv => inv.BillingServices.Select(x => new
                {
                    x.EncounterService,
                    FacilityEncounterService = x.FacilityEncounterService.RevenueCode,
                }),
                inv => inv.BillingProvider.BillingOrganization.IdentifierCodes,
                inv => inv.BillingProvider.IdentifierCodes,
                inv => inv.BillingProvider.User,
                inv => inv.BillingProvider.BillingOrganization.BillingOrganizationAddresses.Select(a => a.StateOrProvince.Country),
                inv => inv.Encounter.ServiceLocation.ServiceLocationAddresses.Select(a => a.StateOrProvince.Country),
                inv => inv.Encounter.ServiceLocation.IdentifierCodes,
                inv => inv.Encounter.Patient.PatientAddresses.Select(a => a.StateOrProvince.Country),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer.InsurerAddresses.Select(a => a.StateOrProvince.Country)),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.PatientInsuranceReferrals),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.PolicyholderPatient.PatientAddresses.Select(a => a.StateOrProvince.Country)));
        }

        public IDictionary<Invoice, IList<ValidationMessage>> ValidateInvoices(IList<Invoice> invoices, Func<InvoiceReceivable, bool> invoiceReceivablesFilter = null, int? insuranceTypeId = null, int? insuranceId = null, int? referralPatientInsuranceId = null)
        {
            var warnings = new Dictionary<Invoice, IList<ValidationMessage>>();
            foreach (var invoice in invoices)
            {
                var insuranceType = insuranceTypeId.HasValue ? (InsuranceType) insuranceTypeId.Value : invoice.Encounter.InsuranceType;
                var patientInsurances = invoice.Encounter.Patient.PatientInsurances.Where(x =>  x.InsuranceType == insuranceType &&
                                                                                      x.IsActiveForDateTime(invoice.Encounter.StartDateTime))
                                                                                      .OrderBy(x => x.OrdinalId).ToArray();

                bool isIcd10 = invoice.DiagnosisTypeId.HasValue && invoice.DiagnosisTypeId.Value == (int)DiagnosisTypeId.Icd10;
                var icd10DiagnosisCount = isIcd10 ? _icd10Service.GetIcd10DiagnosisCodeCount(invoice.Id) : 0;

                var warningList = new List<ValidationMessage>();
                if (invoice.Encounter.Patient.FirstName.IsNullOrEmpty() ||
                    invoice.Encounter.Patient.LastName.IsNullOrEmpty() ||
                    !invoice.Encounter.Patient.DateOfBirth.HasValue)
                {
                    warningList.Add(ValidationMessage.PatientValidNameAndDob);
                }
                if (!invoice.Encounter.Patient.PatientAddresses.Any()
                    || invoice.Encounter.Patient.PatientAddresses
                        .Where(x => x.OrdinalId == 1)
                        .Any(x => x.Line1.IsNullOrEmpty()
                            || x.City.IsNullOrEmpty()
                            || x.StateOrProvinceId == 0
                            || x.PostalCode.IsNullOrEmpty()
                            || !x.IsValidPostalCode()))
                {
                    warningList.Add(ValidationMessage.PatientPrimaryAddress);
                }



                if (invoice.BillingProvider.BillingOrganization != null
                    && (!invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses.Any()
                        || invoice.BillingProvider.BillingOrganization.BillingOrganizationAddresses
                           .Where(x => x.BillingOrganizationAddressTypeId == (int)BillingOrganizationAddressTypeId.PhysicalLocation)
                           .Any(x => x.Line1.IsNullOrEmpty()
                               || x.City.IsNullOrEmpty()
                               || x.StateOrProvinceId == 0
                               || !x.IsValidPostalCode())))
                {
                    warningList.Add(ValidationMessage.BillingOrganizationPrimaryAddress);
                }

                if (invoice.BillingProvider.BillingOrganization != null &&
                  (!invoice.BillingProvider.BillingOrganization.IdentifierCodes.Any()
                  || (invoice.BillingProvider.BillingOrganization.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.Npi)
                   || invoice.BillingProvider.BillingOrganization.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.Taxonomy)
                   || (invoice.BillingProvider.BillingOrganization.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.TaxId)
                      && invoice.BillingProvider.BillingOrganization.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.Ssn)))))
                {
                    warningList.Add(ValidationMessage.BillingOrganizationMissingNpiTaxSsn);
                }

                //Insurer Address
                //The validation check should only perform on valid services and insurer.
                if (patientInsurances.Select(y => !y.InsurancePolicy.Insurer.InsurerAddresses.Any() ||
                                                                  y.InsurancePolicy.Insurer.InsurerAddresses
                                                              .Any(x => x.InsurerAddressTypeId == (int)InsurerAddressTypeId.Claims &&
                                                                  (x.Line1.IsNullOrEmpty() ||
                                                                  x.City.IsNullOrEmpty() ||
                                                                  x.StateOrProvinceId == 0 ||
                                                                  !x.IsValidPostalCode())))
                                                              .FirstOrDefault())
                {
                    warningList.Add(ValidationMessage.InsurerAddress);
                }

                var insurancePolicy = patientInsurances.Any() ? patientInsurances.Select(x => x.InsurancePolicy).First() : null;
                //Policyholders first name, last name
                if (insurancePolicy != null &&
                    (insurancePolicy.PolicyholderPatient.FirstName.IsNullOrEmpty() ||
                    insurancePolicy.PolicyholderPatient.LastName.IsNullOrEmpty()))
                {
                    warningList.Add(ValidationMessage.PolicyHolderValidName);
                }
                //Policyholders Address.
                if (insurancePolicy != null &&
                    (!insurancePolicy.PolicyholderPatient.PatientAddresses.Any() ||
                                            insurancePolicy.PolicyholderPatient.PatientAddresses
                                            .Where(x => x.OrdinalId == 1)
                                            .Any(x => (x.Line1.IsNullOrEmpty() ||
                                                        x.City.IsNullOrEmpty() ||
                                                        x.StateOrProvinceId == 0 ||
                                                        !x.IsValidPostalCode()))))
                {
                    warningList.Add(ValidationMessage.PolicyHolderPrimaryAddress);
                }

                //PolicyCode confirms to reg ex.
                if (insurancePolicy != null && !insurancePolicy.PolicyCode.IsValidPolicyCode())
                {
                    warningList.Add(ValidationMessage.ValidPolicyCode);
                }

                //First Name, Last Name check only if BillingProvider are the user. 
                if (invoice.BillingProvider != null &&
                    invoice.BillingProvider.User != null &&
                    (invoice.BillingProvider.User.FirstName.IsNullOrEmpty() ||
                     invoice.BillingProvider.User.LastName.IsNullOrEmpty()))
                {
                    warningList.Add(ValidationMessage.RenderingMissingValidName);
                }

                if (invoice.BillingProvider != null &&
                    (!invoice.BillingProvider.IdentifierCodes.Any() ||
                    (invoice.BillingProvider.IdentifierCodes.Any(x => !x.ProviderId.HasValue) ||
                    (invoice.BillingProvider.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.Npi)
                        || invoice.BillingProvider.IdentifierCodes.All(x => x.IdentifierCodeType != IdentifierCodeType.Taxonomy)))))
                {
                    warningList.Add(ValidationMessage.RenderingMissingNpiTaxonomy);
                }

                if (invoice.ReferringExternalProvider != null &&
                  invoice.ReferringExternalProvider.LastNameOrEntityName.IsNullOrEmpty())
                {
                    warningList.Add(ValidationMessage.ReferringMissingLastName);
                }

                //Kath : Exclude in claim logic and First Name, Last Name check. : Kath only last name
                if (invoice.ReferringExternalProvider != null
                    && !invoice.ReferringExternalProvider.ExcludeOnClaim
                    && (!invoice.ReferringExternalProvider.IdentifierCodes.Any()
                        || invoice.ReferringExternalProvider.IdentifierCodes
                            .All(x => x.IdentifierCodeType != IdentifierCodeType.Npi)))
                {
                    warningList.Add(ValidationMessage.ReferringMissingNpi);
                }

                //If patient has has the insurance that means it will bill to insurance.So we need to check billing diagnosis.
                // billing diagnosis code check for ICD-9
                if (patientInsurances.Any() && !isIcd10 && !invoice.BillingDiagnoses.Any())
                {
                    warningList.Add(ValidationMessage.MissingDiagnosis);
                }

                // billing diagnosis code check for ICD-10
                if (patientInsurances.Any() && isIcd10 && icd10DiagnosisCount <= 0)
                {
                    warningList.Add(ValidationMessage.MissingDiagnosis);
                }

                //IR check
                //The validation check should only perform on valid services and insurer.
                if (insuranceId != null)
                {
                    var patientInsurance = patientInsurances.Where(x => x.Id == insuranceId);
                    if (patientInsurance != null &&
                        patientInsurance.All(x => x.InsurancePolicy.Insurer.IsReferralRequired &&
                                                  x.PatientInsuranceReferrals.All(y => y.PatientInsuranceId != referralPatientInsuranceId)
                                                  && (!invoice.ReferringExternalProviderId.HasValue
                                                      || invoice.ReferringExternalProvider.PatientExternalProviders
                                                          .Any(y => y.ExternalProvider.ExcludeOnClaim))))
                    {
                        warningList.Add(ValidationMessage.ClaimRequireReferral);
                    }
                }
                else
                {
                    var invoiceReceivables = invoice.InvoiceReceivables.Where(invoiceReceivable => invoiceReceivablesFilter == null ? invoiceReceivable.OpenForReview : invoiceReceivablesFilter(invoiceReceivable)).ToArray();
                    if (invoiceReceivables.Any() && invoiceReceivables
                        .All(x => x.PatientInsurance != null
                            && x.PatientInsurance.InsurancePolicy.Insurer.IsReferralRequired
                            && !x.PatientInsuranceReferralId.HasValue
                            && (!x.Invoice.ReferringExternalProviderId.HasValue
                                || x.Invoice.ReferringExternalProvider.PatientExternalProviders
                                    .Any(y => y.ExternalProvider.ExcludeOnClaim))))
                    {
                        warningList.Add(ValidationMessage.ClaimRequireReferral);
                    }
                }

                if (!invoice.BillingServices.Any())
                {
                    warningList.Add(ValidationMessage.MissingBillingServices);
                }

                if (invoice.BillingServices.Any(x => x.EncounterService.IfNotNull(y => y.IsReferringDoctorRequiredOnClaim)) &&
                    invoice.InvoiceReceivables.Any(x => !x.PatientInsuranceReferralId.HasValue) &&
                    (invoice.InvoiceReceivables.Any(x => !x.Invoice.ReferringExternalProviderId.HasValue) ||
                     invoice.InvoiceReceivables.Any(x => x.Invoice.ReferringExternalProvider.PatientExternalProviders.Any(y => y.ExternalProvider.ExcludeOnClaim))))
                {
                    warningList.Add(ValidationMessage.ConsultationsRequireReferringProvider);
                }

                //Service Location
                if (invoice.Encounter.ServiceLocation != null &&
                   (!invoice.Encounter.ServiceLocation.IdentifierCodes.Any() ||
                   invoice.Encounter.ServiceLocation.IdentifierCodes.Any(x => x.IdentifierCodeType != IdentifierCodeType.Npi)))
                {
                    warningList.Add(ValidationMessage.ServiceLocationMissingNpi);
                }
                if (invoice.Encounter.ServiceLocation != null && (
                    !invoice.Encounter.ServiceLocation.ServiceLocationAddresses.Any() ||
                    invoice.Encounter.ServiceLocation.ServiceLocationAddresses.Where(x => x.ServiceLocationAddressTypeId == (int)ServiceLocationAddressTypeId.MainOffice)
                                                                            .Any(x => x.Line1.IsNullOrEmpty() ||
                                                                                    x.City.IsNullOrEmpty() ||
                                                                                    x.StateOrProvinceId == 0 ||
                                                                                    !x.IsValidPostalCode())))
                {
                    warningList.Add(ValidationMessage.ServiceLocationPrimaryAddress);
                }

                if (warningList.Any())
                {
                    warnings.Add(invoice, warningList);
                }
            }
            return warnings;
        }
    }
}

