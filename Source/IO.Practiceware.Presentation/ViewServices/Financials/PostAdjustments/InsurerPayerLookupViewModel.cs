﻿using System.Collections.Generic;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;

namespace IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments
{
    /// <summary>
    /// Unresolved Insurer payer whose payer identifiers are known
    /// </summary>
    public class InsurerPayerLookupViewModel : PayerViewModel
    {
        public InsurerPayerLookupViewModel()
        {
            Id = -111;
            Name = "Unresolved payer";
        }

        /// <summary>
        /// Gets or sets the payer identifiers of Insurer for lookup.
        /// </summary>
        /// <value>
        /// The payer identifiers.
        /// </value>
        public List<string> PayerIdentifiers { get; set; }
    }
}
