﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using FinancialSourceType = IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction.FinancialSourceType;

[assembly: Component(typeof(PostAdjustmentsViewService), typeof(IPostAdjustmentsViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments
{
    public interface IPostAdjustmentsViewService
    {
        PostAdjustmentsLoadInformation LoadPostAdjustmentsInformation(int? paymentInstrumentId, FinancialTransactionViewModel transactionOfInterest, int? patientId, int? invoiceId, bool skipDataLists = false);

        AccountViewModel LoadAccount(int patientId, int? invoiceId, int? paymentInstrumentId);

        PaymentInstrumentViewModel FindPaymentInstrumentDuplicate(PaymentInstrumentViewModel paymentInstrument);

        ICollection<BalanceForwardSuggestionInformation> Suggest(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts);

        PostAdjustmentsResult Post(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions);

        PostAdjustmentsResult PostAndRefresh(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions, int? invoiceIdFilter);

        ICollection<PaymentInstrumentViewModel> SearchPaymentInstruments(string reference);

        ICollection<InsurerPayerViewModel> SearchInsurers(string insurersSearchText);

        void SavePaymentInstrument(PaymentInstrumentViewModel paymentInstrument);

        void InsertInvoiceReceivable(int invoiceId);
    }

    public class PostAdjustmentsViewService : BaseViewService, IPostAdjustmentsViewService
    {
        private static IMapper<Patient, AccountViewModel> _patientAsAccountMapper;
        private static IMapper<Adjustment, FinancialTransactionViewModel> _adjustmentMapper;
        private static IMapper<FinancialInformation, FinancialTransactionViewModel> _financialInformationMapper;

        private readonly IPostAdjustmentsService _postAdjustmentService;

        static PostAdjustmentsViewService()
        {
            InitMappers();
        }

        public PostAdjustmentsViewService(IPostAdjustmentsService postAdjustmentService)
        {
            _postAdjustmentService = postAdjustmentService;
        }

        public virtual PostAdjustmentsLoadInformation LoadPostAdjustmentsInformation(int? paymentInstrumentId, FinancialTransactionViewModel transactionOfInterest, int? patientId, int? invoiceId, bool skipDataLists = false)
        {
            var loadInformation = new PostAdjustmentsLoadInformation();

            // Resolve payment instrument id filter by selected transaction
            if (paymentInstrumentId == null && transactionOfInterest != null)
            {
                // Locate transaction and return it's financial batch id
                var transactionRecord = transactionOfInterest.FinancialTransactionType is AdjustmentTypeViewModel
                    ? (IAdjustmentOrFinancialInformation)PracticeRepository.Adjustments.First(a => a.Id == transactionOfInterest.Id)
                    : PracticeRepository.FinancialInformations.First(fi => fi.Id == transactionOfInterest.Id);

                // Open in context of financial batch
                paymentInstrumentId = transactionRecord.FinancialBatchId;
            }

            // Identify a list of patients to display
            int[] batchPatients;
            if (patientId != null)
            {
                batchPatients = new[] {patientId.Value};
            }
            else if (paymentInstrumentId == null)
            {
                batchPatients = new int[0];
            }
            else
            {
                // Fetch patients mentioned in a batch
                batchPatients = PracticeRepository.Adjustments
                    .Select(a => new
                    {
                        a.FinancialBatchId,
                        a.InvoiceReceivable,
                        a.PatientId
                    })
                    .Union(PracticeRepository.FinancialInformations
                    .Select(fi => new
                    {
                        fi.FinancialBatchId,
                        fi.InvoiceReceivable,
                        fi.PatientId
                    }))
                    .Where(t => t.FinancialBatchId == paymentInstrumentId.Value)
                    .Select(t => t.InvoiceReceivable != null
                        ? (int?) t.InvoiceReceivable.Invoice.Encounter.PatientId
                        : t.PatientId)
                    .Distinct()
                    .ToArray()
                    .Where(id => id != null)
                    .Select(id => id.EnsureNotDefault().GetValueOrDefault())
                    .ToArray();
            }

            new Action[]
            {
                () => { if (!skipDataLists) loadInformation.DataLists = LoadDataLists(); },
                () => loadInformation.PaymentInstrument = LoadPaymentInstrument(paymentInstrumentId, invoiceId, batchPatients),
                () => loadInformation.Accounts = LoadAccounts(batchPatients, invoiceId, paymentInstrumentId).ToExtendedObservableCollection(),
                () =>
                {
                    if (invoiceId == null) return;

                    // Get list of insurers via Invoice's invoice receivables
                    loadInformation.InvoiceSelectedInsurerIds = PracticeRepository.InvoiceReceivables
                        .Where(i => i.InvoiceId == invoiceId && i.PatientInsurance != null)
                        .Select(i => i.PatientInsurance.InsurancePolicy.InsurerId)
                        .ToList();
                }
            }
            .ForAllInParallel(a => a());

            return loadInformation;
        }

        public virtual AccountViewModel LoadAccount(int patientId, int? invoiceId, int? paymentInstrumentId)
        {
            var patientRecord = _postAdjustmentService.Utilities.GetPatientWithInsurances(patientId);
            var account = _patientAsAccountMapper.Map(patientRecord);

            // Get list of all open invoices for balance calculation and other logic
            var patientActiveInvoices = PracticeRepository.Invoices
                    .Where(i => i.Encounter.PatientId == patientId)
                    .Select(i => new
                    {
                        InvoiceId = i.Id,
                        HasTransactionsForPaymentInstrument = 
                            i.InvoiceReceivables.SelectMany(ir => ir.Adjustments).Any(a => a.FinancialBatchId == paymentInstrumentId)
                            || i.InvoiceReceivables.SelectMany(ir => ir.FinancialInformations).Any(a => a.FinancialBatchId == paymentInstrumentId),
                        TotalCharge = i.BillingServices
                            .Sum(bs => bs.Unit * bs.UnitCharge) ?? 0,
                        TotalPaid = i.InvoiceReceivables
                            .SelectMany(ir => ir.Adjustments)
                            .Where(a => a.BillingServiceId != null) // Exclude unassigned for TotalPaid calculation
                            .Sum(a => a.AdjustmentType.IsDebit ? (decimal?)a.Amount * -1 : (decimal?)a.Amount) ?? 0,
                        TotalPaidUnassigned = i.InvoiceReceivables
                            .SelectMany(ir => ir.Adjustments)
                            .Where(a => a.BillingServiceId == null)
                            .Sum(a => a.AdjustmentType.IsDebit ? (decimal?)a.Amount * -1 : (decimal?)a.Amount) ?? 0
                    })
                    .Where(i => i.HasTransactionsForPaymentInstrument || i.TotalCharge - i.TotalPaid != 0)
                    .ToArray();

            int?[] invoicesFilter;
            if (invoiceId != null)
            {
                invoicesFilter = new[] { invoiceId };
            }
            else
            {
                // Otherwise show all open invoices
                invoicesFilter = patientActiveInvoices
                    .Select(i => (int?)i.InvoiceId)
                    .ToArray();
            }

            // Load services for this account and invoice
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            var billingServices = PracticeRepository.BillingServices
                .Where(i => invoicesFilter.Contains(i.InvoiceId))
                .ToList();

            queryableFactory.Load(billingServices,
                bs => bs.BillingServiceModifiers.Select(bsm => bsm.ServiceModifier),
                bs => bs.Adjustments.Select(adj => new
                {
                    adj.AdjustmentType,
                    adj.InvoiceReceivable,
                    adj.ClaimAdjustmentReasonCode,
                    adj.FinancialBatch
                }),
                bs => bs.FinancialInformations.Select(fi => new
                {
                    fi.FinancialInformationType,
                    fi.InvoiceReceivable,
                    fi.ClaimAdjustmentReasonCode,
                    fi.FinancialBatch
                }),
                bs => bs.Invoice.Encounter,
                bs => bs.EncounterService
                );

            using (new SuppressPropertyChangedScope())
            using (new SuppressChangeTrackingScope())
            {
                account.BillingServices = billingServices
                    .Select(bs => PostAdjustmentsUtilities.BillingServiceMapper
                        .Map(bs)
                        .Modify(bsm =>
                        {
                            // Map transactions
                            bsm.Transactions = bs.Adjustments
                                .Where(adj => adj.FinancialBatchId == paymentInstrumentId)
                                .Select(adj => _adjustmentMapper.Map(adj))
                                .Concat(bs.FinancialInformations
                                    .Where(fi => fi.FinancialBatchId == paymentInstrumentId)
                                    .Select(fi => _financialInformationMapper.Map(fi)))
                                .ToExtendedObservableCollection();

                            // Set total amount of transactions we filtered out (not for this batch)
                            bsm.FilteredAdjustmentsTotal = bs.Adjustments
                                .Where(adj => adj.FinancialBatchId != paymentInstrumentId)
                                .CalculateTotalAmount();
                        }))
                    .OrderByDescending(x => x.Date)
                    .ToExtendedObservableCollection();


                // We want to always show correct total paid and adjusted on account level, but only if it's for existing financial batch
                if (paymentInstrumentId != null)
                {
                    // Load adjustments for non-included invoices
                    var adjustmentsOfOtherInvoices = PracticeRepository.Adjustments
                        .Where(a => a.FinancialBatchId == paymentInstrumentId
                                    && (
                            // Assigned/unassigned transactions to non-included invoices
                                        (a.InvoiceReceivable != null
                                         && a.InvoiceReceivable.Invoice.Encounter.PatientId == account.PatientId
                                         && !invoicesFilter.Contains(a.InvoiceReceivable.InvoiceId))
                            // Will show all on-account transactions for this payment instrument
                                        )
                                    && a.AdjustmentType != null)
                        .Include(x => x.AdjustmentType)
                        .ToList();

                    account.FilteredInvoicesTotalPaid = adjustmentsOfOtherInvoices
                        .Where(a => a.AdjustmentType.IsCash)
                        .CalculateTotalAmount();

                    account.FilteredInvoicesTotalAdjusted = adjustmentsOfOtherInvoices
                        .Where(a => !a.AdjustmentType.IsCash)
                        .CalculateTotalAmount();

                    // Setting default Payer Override on the account based on the financial batch being loaded and the patient context
                    var adjustment = PracticeRepository.Adjustments.OrderByDescending(a => a.Id)
                                                                    .FirstOrDefault(x => x.FinancialBatchId == paymentInstrumentId
                                                                    && x.InvoiceReceivable != null
                                                                    && x.InvoiceReceivable.Invoice.Encounter.PatientId == patientId
                                                                    && x.InvoiceReceivable.PatientInsurance != null);
                    if (adjustment != null)
                    {
                        queryableFactory.Load(adjustment,
                            a => a.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer
                        );
                        account.PayerOverride = PostAdjustmentsUtilities.InsurerAsNextPayerMapper.Map(adjustment.InvoiceReceivable.PatientInsurance).Payer;
                    }
                }

                // Load on account transactions
                var onAccountAdjustments = PracticeRepository.Adjustments
                        .Where(adj => adj.AdjustmentTypeId == (int)AdjustmentTypeId.Payment
                        && adj.PatientId == patientId
                        && adj.InvoiceReceivableId == null)
                    .ToList();
                queryableFactory.Load(onAccountAdjustments,
                    adj => adj.AdjustmentType,
                    adj => adj.ClaimAdjustmentReasonCode,
                    adj => adj.FinancialBatch
                    );

                // Calculate account balance as sum of all open balances and on-account transactions
                account.Balance = patientActiveInvoices.Sum(i => i.TotalCharge - (i.TotalPaid + i.TotalPaidUnassigned)) 
                    + onAccountAdjustments.CalculateTotalAmount();

                // Map on-account adjustments for this payment instrument
                account.OnAccountTransactions = onAccountAdjustments
                    .Where(a => a.FinancialBatchId == paymentInstrumentId)
                    .Select(adj => _adjustmentMapper.Map(adj))
                    .ToExtendedObservableCollection();

                // Load unassigned transactions
                var unassignedAdjustments = PracticeRepository.Adjustments
                    .Where(adj => invoicesFilter.Contains(adj.InvoiceReceivable.InvoiceId)
                        && adj.FinancialBatchId == paymentInstrumentId
                        && adj.BillingServiceId == null)
                    .ToList();
                queryableFactory.Load(unassignedAdjustments,
                    adj => adj.AdjustmentType,
                    adj => adj.ClaimAdjustmentReasonCode,
                    adj => adj.FinancialBatch,
                    adj => adj.InvoiceReceivable
                    );
                var unassignedFinancialInformations = PracticeRepository.FinancialInformations
                    .Where(fi => invoicesFilter.Contains(fi.InvoiceReceivable.InvoiceId)
                        && fi.FinancialBatchId == paymentInstrumentId
                        && fi.BillingServiceId == null)
                    .ToList();
                queryableFactory.Load(unassignedFinancialInformations,
                    fi => fi.FinancialInformationType,
                    fi => fi.ClaimAdjustmentReasonCode,
                    fi => fi.FinancialBatch,
                    fi => fi.InvoiceReceivable
                    );
                account.UnassignedTransactions = unassignedAdjustments
                    .Select(adj => _adjustmentMapper.Map(adj))
                    .Concat(unassignedFinancialInformations
                        .Select(ufi => _financialInformationMapper.Map(ufi)))
                    .ToExtendedObservableCollection();

                // Load next payers
                var activeInsurances = _postAdjustmentService.Utilities.GetPatientInsurances(patientId);
                account.NextPayers = activeInsurances
                    .Select(i => PostAdjustmentsUtilities.InsurerAsNextPayerMapper.Map(i))
                    .Concat(new[] { patientRecord }
                        .Select(PostAdjustmentsUtilities.PatientAsNextPayerMapper.Map))
                    .ToExtendedObservableCollection();   
            }

            return account;
        }

        public virtual PaymentInstrumentViewModel FindPaymentInstrumentDuplicate(PaymentInstrumentViewModel paymentInstrument)
        {
            var result = _postAdjustmentService.FindPaymentInstrumentDuplicate(paymentInstrument, false);
            return result;
        }

        public virtual void InsertInvoiceReceivable(int invoiceId)
        {
            _postAdjustmentService.InsertInvoiceReceivable(invoiceId);
        }

        public virtual void SavePaymentInstrument(PaymentInstrumentViewModel paymentInstrument)
        {
            _postAdjustmentService.SavePaymentInstrument(paymentInstrument);
        }

        [UnitOfWork(CreateNew = true)]
        public virtual ICollection<BalanceForwardSuggestionInformation> Suggest(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts)
        {
            _postAdjustmentService.Utilities.CacheForAccounts(activeAccounts);
            return _postAdjustmentService.Suggest(paymentInstrument, activeAccounts);
        }

        [UnitOfWork(CreateNew = true)] // Need new unit of work, so that caching adds loaded items as unmodified to it
        public virtual PostAdjustmentsResult Post(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions)
        {
            _postAdjustmentService.Utilities.CacheForAccounts(activeAccounts);

            // Post and retrieve validation errors along with saved payment instrument id
            var errors = _postAdjustmentService.Post(paymentInstrument, activeAccounts, deletedTransactions);

            var result = new PostAdjustmentsResult();
            result.Errors = errors.Select(e => new PostAdjustmentsResult.InvoiceValidationErrors
            {
                InvoiceId = e.Key,
                Messages = e.Value
            }).ToList();

            if (!result.Errors.Any())
            {
                result.PaymentInstrumentId = paymentInstrument.Id
                    .EnsureNotDefault("New payment instruments must get Id after posting")
                    .GetValueOrDefault();
            }

            return result;
        }

        public virtual PostAdjustmentsResult PostAndRefresh(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions, int? invoiceIdFilter)
        {
            var result = Post(paymentInstrument, activeAccounts, deletedTransactions);
            if (!result.Errors.Any())
            {
                // Refresh posted accounts
                _postAdjustmentService.Utilities.ClearCache(); // Clear cache, since we want latest information
                result.PostedAccounts = LoadAccounts(activeAccounts.Select(a => a.PatientId).ToArray(),
                    invoiceIdFilter, result.PaymentInstrumentId).ToList();
            }

            return result;
        }

        public ICollection<PaymentInstrumentViewModel> SearchPaymentInstruments(string reference)
        {
            var foundBatches = PracticeRepository.FinancialBatches
                    .Include(fb => fb.Patient, fb => fb.Insurer, fb => fb.PaymentMethod)
                    .Where(fb => fb.CheckCode.StartsWith(reference))
                    .OrderBy(fb => fb.CheckCode).ThenByDescending(fb => fb.PaymentDateTime)
                    .ToList()
                    .Select(PostAdjustmentsUtilities.FinancialBatchMapper.Map)
                    .ToList();
            return foundBatches;
        }

        public ICollection<InsurerPayerViewModel> SearchInsurers(string insurersSearchText)
        {
            var matchingInsurers = PracticeRepository.Insurers
                .Include(i => i.InsurerAddresses.Select(a => a.StateOrProvince.Country))
                .Where(i => i.Name.Contains(insurersSearchText) || i.PlanName.Contains(insurersSearchText))
                .OrderBy(i => i.Name)
                .ToList()
                .Select(PostAdjustmentsUtilities.InsurerAsPayerMapper.Map)
                .ToList();

            return matchingInsurers;
        }

        private DataListsViewModel LoadDataLists()
        {
            var dataLists = new DataListsViewModel();
            new Action[]
            {
                () => dataLists.BillingActions = Enums.GetValues<BillingActionType>()
                    .Select(PostAdjustmentsUtilities.BillingActionTypeMapper.Map)
                    .OrderBy(p => p.Name)
                    .ToObservableCollection(),
                () => dataLists.FinancialTransactionTypes = PracticeRepository.AdjustmentTypes
                    .ToList()
                    .Select(PostAdjustmentsUtilities.AdjustmentTypeMapper.Map)
                    .Concat<FinancialTransactionTypeViewModel>(PracticeRepository.FinancialInformationTypes
                        .ToList()
                        .Select(PostAdjustmentsUtilities.FinancialInformationTypeMapper.Map))
                    .ToObservableCollection(),
                () => dataLists.GroupCodes = Enums.GetValues<ClaimAdjustmentGroupCode>()
                    .Select(PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMapper.Map)
                    .ToObservableCollection(),
                () => dataLists.ReasonCodes = PracticeRepository.ClaimAdjustmentReasonCodes
                    .ToList()
                    .Select(PostAdjustmentsUtilities.ClaimAdjustmentReasonCodeMapper.Map)
                    .ToObservableCollection(),
                () => dataLists.PaymentMethods = PracticeRepository.PaymentMethods
                    .ToList()
                    .Where (i => !i.IsArchived)
                    .Select(PostAdjustmentsUtilities.PaymentMethodMapper.Map)
                    .OrderBy(p => p.OrdinalId).ThenBy(p => p.Name)
                    .ToObservableCollection()
            }.ForAllInParallel(a => a());

            SetDefaultGroupAndCodeForTransactionTypes(dataLists);

            return dataLists;
        }

        private IEnumerable<AccountViewModel> LoadAccounts(int[] patientIds, int? invoiceId, int? paymentInstrumentId)
        {
            var accounts = new ConcurrentQueue<AccountViewModel>();
            patientIds.ForAllInParallel(patientId =>
            {
                var account = LoadAccount(patientId, invoiceId, paymentInstrumentId);
                accounts.Enqueue(account);
            });
            return accounts;
        }

        private PaymentInstrumentViewModel LoadPaymentInstrument(int? paymentInstrumentId, int? invoiceId, int[] patientIds)
        {
            PaymentInstrumentViewModel paymentInstrument;

            if (paymentInstrumentId != null)
            {
                var financialBatch = PracticeRepository.FinancialBatches
                    .Include(fb => fb.Patient, fb => fb.Insurer, fb => fb.PaymentMethod)
                    .First(fb => fb.Id == paymentInstrumentId);
                paymentInstrument = PostAdjustmentsUtilities.FinancialBatchMapper.Map(financialBatch);

                // TODO: query once editing of payment instrument is allowed
                paymentInstrument.InsurerSourceSupported = true;

                // We want to always show correct balance of payment instrument, so include totals from filtered out accounts
                var adjustmentsOfOtherAccounts = PracticeRepository.Adjustments
                    .Where(a => a.FinancialBatchId == paymentInstrumentId
                        && (
                            // Assigned/unassigned transactions for excluded patients
                            (a.InvoiceReceivable != null
                                && !patientIds.Contains(a.InvoiceReceivable.Invoice.Encounter.PatientId)
                            ||
                            // On-account transactions for non-included patients
                            (a.InvoiceReceivable == null 
                                && a.PatientId != null 
                                && !patientIds.Contains(a.PatientId.Value)))
                        )
                        && a.AdjustmentType != null)
                    .Include(x => x.AdjustmentType)
                    .ToList();

                paymentInstrument.FilteredAccountsPaidAmount = adjustmentsOfOtherAccounts
                    .Where(a => a.AdjustmentType.IsCash)
                    .CalculateTotalAmount();

                paymentInstrument.FilteredAccountsAdjustedAmount = adjustmentsOfOtherAccounts
                    .Where(a => !a.AdjustmentType.IsCash)
                    .CalculateTotalAmount();
            }
            else
            {
                // Find check payment method
                var checkPaymentMethodModel = PracticeRepository.PaymentMethods
                    .First(pm => pm.Name.Trim() == PaymentMethod.Check);
                var checkPaymentMethod = PostAdjustmentsUtilities.PaymentMethodMapper.Map(checkPaymentMethodModel);

                PayerViewModel payer = null;
                var financialSourceType = FinancialSourceType.Insurer;
                
                // In case of single patient -> select it as payer for instrument
                if (patientIds.Length == 1)
                {
                    financialSourceType = FinancialSourceType.Patient;

                    var patientRecord = _postAdjustmentService.Utilities.GetPatientWithInsurances(patientIds.Single());
                    payer = PostAdjustmentsUtilities.PatientAsPayerMapper.Map(patientRecord);
                }

                // New payment instrument
                paymentInstrument = new PaymentInstrumentViewModel
                {
                    Id = null,
                    Date = DateTime.Now.ToClientTime().Date,
                    Payer = payer,
                    PaymentMethod = checkPaymentMethod,
                    FinancialSourceType = financialSourceType,
                    PaymentAmount = 0,
                    CheckDate = null,
                    ReferenceNumber = null,
                    InsurerSourceSupported = invoiceId == null || PracticeRepository.InvoiceReceivables
                        .Any(ir => ir.InvoiceId == invoiceId && ir.PatientInsurance != null)
                };
            }

            return paymentInstrument;
        }

        private void SetDefaultGroupAndCodeForTransactionTypes(DataListsViewModel dataLists)
        {
            var transactionTypeDefaults = PracticeRepository.ClaimAdjustmentReasonAdjustmentTypes.ToList();
            foreach (var financialTransactionType in dataLists.FinancialTransactionTypes)
            {
                // Find configured defaults
                ClaimAdjustmentReasonAdjustmentType defaultGroupAndCode = null;
                if (financialTransactionType is AdjustmentTypeViewModel)
                {
                    defaultGroupAndCode = transactionTypeDefaults
                        .FirstOrDefault(m => m.AdjustmentTypeId == financialTransactionType.Id);
                }
                else if (financialTransactionType is FinancialInformationTypeViewModel)
                {
                    defaultGroupAndCode = transactionTypeDefaults
                        .FirstOrDefault(m => m.FinancialInformationTypeId == financialTransactionType.Id);
                }

                if (defaultGroupAndCode == null) continue;

                // Set default group and code
                financialTransactionType.GroupCode = dataLists.GroupCodes.First(gc => gc.Id == (int)defaultGroupAndCode.ClaimAdjustmentGroupCode);
                financialTransactionType.ReasonCode = dataLists.ReasonCodes.First(rc => rc.Id == defaultGroupAndCode.ClaimAdjustmentReasonCodeId);
            }
        }

        private static void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            var addressMapper = mapperFactory.CreateAddressViewModelMapper<PatientAddress>();

            _patientAsAccountMapper = mapperFactory.CreateMapper<Patient, AccountViewModel>(
                item => new AccountViewModel
                {
                    PatientId = item.Id,
                    DisplayName = item.DisplayName,
                    Birthdate = item.DateOfBirth,
                    Email = item.PatientEmailAddresses
                        .OrderBy(e => e.OrdinalId)
                        .Select(e => e.Value)
                        .FirstOrDefault(),
                    Address = item.PatientAddresses
                        .OrderBy(a => a.OrdinalId)
                        .Select(a => addressMapper.Map(a).ToString())
                        .FirstOrDefault(),
                    PhoneNumber = item.PatientPhoneNumbers
                        .OrderBy(a => a.OrdinalId)
                        .Select(p => p.GetFormattedValue())
                        .FirstOrDefault()
                }, false);

            _adjustmentMapper = mapperFactory.CreateMapper<Adjustment, FinancialTransactionViewModel>(
                item => new FinancialTransactionViewModel
                {
                    Id = item.Id,
                    Amount = item.Amount,
                    Date = item.PostedDateTime,
                    Comment = item.Comment,
                    InvoiceId = item.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                    ServiceId = item.BillingServiceId,
                    Source = PostAdjustmentsUtilities.FinancialSourceTypeMapper.Map(item.FinancialSourceType),
                    FinancialTransactionType = new AdjustmentTypeViewModel
                    {
                        Id = item.AdjustmentTypeId,
                        Name = item.AdjustmentType.Name,
                        IsCash = item.AdjustmentType.IsCash,
                        IsDebit = item.AdjustmentType.IsDebit,
                        GroupCode = item.ClaimAdjustmentGroupCode == null ? null : PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMapper.Map(item.ClaimAdjustmentGroupCode.Value),
                        ReasonCode = item.ClaimAdjustmentReasonCode.IfNotNull(PostAdjustmentsUtilities.ClaimAdjustmentReasonCodeMapper.Map)
                    },
                    IncludeCommentOnStatement = item.IncludeCommentOnStatement,
                    Reference = item.FinancialBatch.IfNotNull(fb => fb.CheckCode),
                    InvoiceReceivableId = item.InvoiceReceivableId
                }, false);

            _financialInformationMapper = mapperFactory.CreateMapper<FinancialInformation, FinancialTransactionViewModel>(
                item => new FinancialTransactionViewModel
                {
                    Id = item.Id,
                    ServiceId = item.BillingServiceId,
                    Amount = item.Amount,
                    Comment = item.Comment,
                    Date = item.PostedDateTime,
                    InvoiceId = item.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                    Source = PostAdjustmentsUtilities.FinancialSourceTypeMapper.Map(item.FinancialSourceType),
                    FinancialTransactionType = new FinancialInformationTypeViewModel
                    {
                        Id = item.FinancialInformationType.Id,
                        Name = item.FinancialInformationType.Name,
                        GroupCode = item.ClaimAdjustmentGroupCode == null ? null : PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMapper.Map(item.ClaimAdjustmentGroupCode.Value),
                        ReasonCode = item.ClaimAdjustmentReasonCode.IfNotNull(PostAdjustmentsUtilities.ClaimAdjustmentReasonCodeMapper.Map)
                    },
                    IncludeCommentOnStatement = item.IncludeCommentOnStatement,
                    Reference = item.FinancialBatch.IfNotNull(fb => fb.CheckCode),
                    InvoiceReceivableId = item.InvoiceReceivableId
                }, false);
        }
    }
}
